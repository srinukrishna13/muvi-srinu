/*
 **** File   : main.js for Audio Streaming
 **** Desc   : It lists all custom JS Codes
 **** Author : Designer Desk
 */
var $win = $(window),
        $doc = $(document),
        $dropdown = $('.nav .pull-right'),
        $sidebar = $('.sidebar-menu');
//var HTTP_ROOT = '{/literal}{$this->siteurl}{$Yii->theme->baseUrl}{literal}';

// Toggles


//Owl Carousel
$doc.ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        nav: true,
        margin: 30,
        lazyLoad: true,
        navText: ["<img src='" + THEME_URL + "/images/Icons/Arrow-Drpdown-Grey.png'>", "<img src='" + THEME_URL + "/images/Icons/Arrow-Drpdown-Grey.png'>"],
        responsive: {
            0: {
                items: 2
            },
            475: {
                items: 3
            },
            700: {
                items: 4
            },
            850: {
                items: 5
            },
            1280: {
                items: 6
            },
            1440: {
                items: 8
            }
        }
    });
    if ($(".alert").length) {
        $(".navbar-fixed-top").css("top", "0px");
        $(".alert .close").click(function () {
            $(".navbar-fixed-top, .navbar-wrapper").css("top", "0px");
        });
    }
	$("#PlaylistOption").mCustomScrollbar();
	getPlaylistName(sdk_user_id);
	//$('#queue_list').mCustomScrollbar();
    $('.more-menu').click(function (event) {
        event.preventDefault();
        $(this).toggleClass('menu-visible');
        $('.more-menu').toggleClass('menu-visible');

    });
    //Search toggle in Small Screen
    $(".search .toggle").click(function () {
        $(this).parent().toggleClass('visible');
        $(this).parent().find('.search-field').focus();
        if ($dropdown.hasClass('menu-visible')) {
            $dropdown.removeClass('menu-visible');
        }
        if ($sidebar.hasClass('menu-visible')) {
            $sidebar.removeClass('menu-visible');
        }
    });
   
    //Close others when search opens
    $(".search-field").focus(function () {
        $dropdown.removeClass('menu-visible');
    });
    // Close all when click on wrapper inner
    $('.wrapper-inner').click(function () {
        $sidebar.removeClass('menu-visible');
        $dropdown.removeClass('menu-visible');
        $('.search').removeClass('visible');
    });
	$('#SuccessPopup').on('hidden.bs.modal', function () {
		$('body').removeAttr('style');
	});
    // Remove sidebar to specific pages
    if ($('.wrapper').hasClass('has-not-sidebar')) {
        $('.hamburger').hide();
    }

    //Tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    //Custom Scroller to Search
    $(function () {
        $('.ui-widget-content').addClass('mCustomScrollbar');
    });
    $('#main').click(function (event) {
        $('#siteSearchField').val('');
    });
    $('#main-top-menu').removeClass('nav navbar-nav');
    $('a.playbtn').text('');
    $('a').attr('data-pjax', '#main');
    $('.share_btn').removeAttr('data-pjax');
     $('#search-form').removeAttr('data-pjax');
    $('a.playbtn').append(" <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='-201 40 467 467'><g><circle class='st0' cx='34' cy='274.1' r='227'/><circle class='st1' cx='34' cy='274.1' r='220'/><g class='st2'><path class='st3' d='M123.7,269.9L-5.2,195.5c-3.3-1.9-7.3-1.9-10.6,0s-5.3,5.4-5.3,9.2v148.8c0,3.8,2,7.3,5.3,9.2c1.6,1,3.5,1.4,5.3,1.4s3.6-0.5,5.3-1.4l128.9-74.4c3.3-1.9,5.3-5.4,5.3-9.2S127,271.8,123.7,269.9z' /></g></g></svg>")
    $('.payment_form').find('h3').remove();
    $('.download_btn a.playbtn').html('');
    $('.download_btn a.playbtn').addClass('btn as-btn-accent as-btn-md');
    $('.download_btn a.playbtn').html(''+download);
    $("#card-section").find('.form-group').each(function (i) {
        if (i == 0)
        {
            $(this).html('<input type="text" required="" name="data[card_name]" id="card_name" placeholder="' + card_name + '" autocomplete="off" class="form-control" value="" autocomplete="false">');
            $('<div class="form-group"><input type="text" required="" name="data[card_number]" id="card_number" placeholder="' + card_number + '" autocomplete="false" class="form-control"></div>"').insertAfter(this);
        }
        if (i == 1)
        {
            $(this).addClass('row bottom45p');
            $(this).find('label').hide();
            $(this).find('div').each(function (i) {
                if (i == 0)
                {
                    $(this).addClass('col-xs-4 col-md-5');
                    $('<div class="select-wrapper">').insertBefore('select#exp_month');
                    $(this).removeClass('col-sm-2');
                }
                if (i == 1) {
                    $(this).html('');
                }
                if (i == 2) {
                    $(this).addClass('col-xs-4 col-md-5 bottom10p');
                    $('<div class="select-wrapper">').insertBefore('select#exp_year');
                    $(this).removeClass('col-sm-2');

                }
                if (i == 4) {
                    $(this).addClass('col-xs-4 col-md-2 bottom10p');
                    $(this).removeClass('col-sm-4');
                    $(this).find('input').attr('placeholder', '' + security_code);
                }
            });
        }


    });

    $('.plan-box').click(function () {
        $('.price-box').removeClass('selected-plan');
        $(this).addClass('selected-plan');
        $('.checked').hide();
        $(this).find('.checked').show();
        $('#plan_id').val($(this).attr('data-id'));
        $('#currency_id').val($(this).attr('data-currency_id'));
    });

    $('.plan-box').each(function (i) {
        if (i === 0) {
            $(this).find(".checked").css("display", "block");
            $(this).addClass("selected-plan");
        } else {
            $(this).find(".checked").css("display", "none");
        }
    });
	soundOn();
    $('.close').click(function () {
        $('.avatar-wrapper').html("");
        $('.avatar-input').val("");
    });
    $(".logout").removeAttr("data-pjax");
    $(".invoice").removeAttr('data-pjax');
    $('#cancelsubscription').find('#cancel_form').attr('data-pjax', '');
    $('#card-info-error').addClass('error-text');
    $('#avatar-modal').on('hidden.bs.modal', function () {
        $('.avatar-wrapper').html("");
        $('#avatarInput').val("");

    });
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this, currentCategory = "";
            $.each(items, function (index, item) {
                /*for(var i in item){
                 alert(item[i]);
                 }*/
                if (item.category != currentCategory) {
                    //alert(item.category);
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }

                self._renderItemData(ul, item);
            });
        }
    });
    var category = "";
    $("#siteSearchField").catcomplete({
        source: HTTP_ROOT + "/search/search/",
        minLength: 1,
        select: function (event, ui) {
            console.log(HTTP_ROOT);
            var I = ui.item;
            var searchCat = I.category;
            searchCat = searchCat.toLowerCase();
            var logurl = HTTP_ROOT + '/search/siteSearchLog';
            $.post(logurl, {'object_category': I.content_types, 'search_string': I.value, 'search_url': window.location.href, 'object_id': I.id}, function (res) {
                if (parseInt(searchCat.search(" - episode")) >= 0) {
                    playAudio(I.stream_id);
                } else if (searchCat == 'star') {
                    $.cookie("audio_star", '1');
                    window.location = HTTP_ROOT + '/star/' + I.title;
                } else {
                    $.cookie("audio_content", '1');
                    window.location = HTTP_ROOT + '/' + I.title;
                }

            });
        }
    });
});

function episodeContent() {
    var movie_id = $('#content_id').val();
    var movie_type = $('#episode_type').val();
    if (movie_type == '6') {
        $(".loader_episode").show();
        var action_url = HTTP_ROOT + "/content/findepisodes";
        $.ajax({
            method: "POST",
            url: action_url,
            dataType: "json",
            data: {'movie_id': movie_id, 'limit': 9999}
        }).done(function (res) {
            $('.loader_episode').hide();
            $("#episodes").html(res.msg);
            var total_track = '';
			var trackLang;
            if (res.status != 'failure') {
                total_track = $('.episode_total').val();
            }
            var date = $('.release_date').html();
            var cast = $('.cast_data').html();
            var cast_symbol = '';
            var release_symbol = '';

            if (cast) {
                cast_symbol = '<span class="symbol">&nbsp;-</span>&nbsp;&nbsp;';
            }
            if (date) {
                var release_symbol = '<span class="symbol">&nbsp;-</span>&nbsp;&nbsp;';
            }
			if(total_track == 1){
				trackLang = track;
			}else{
				trackLang = tracks;
			}
            if (total_track != '') {
                $('.symbol-release').addClass('hide');
                $('.tracks').html(cast_symbol + total_track + '&nbsp;' + trackLang + release_symbol);
            }
        });
    }
}
function changeLanguage(lang_code, lang_name) {
    var cname = "Language";
    var cvalue = lang_code;
    var nameL = lang_name;
    $('.load-container').show();
    $.ajax({
        url: HTTP_ROOT + '/site/setLanguageCookie/',
        type: "POST",
        data: {lang_code: lang_code, lang_name: nameL},
        async: true,
        success: function (data) {
            $('.load-container').hide();
            $('.language_dropdown a#dropdownMenu1').text('');
            $('.language_dropdown a#dropdownMenu1').prepend(nameL + "<img src='" + THEME_URL + "/images/Icons/Arrow-Drpdown-Grey.png' alt='dropdown' width='8' class='img-gray left-5'>" + "<img src='" + THEME_URL + "/images/Icons/Arrow-Drpdown-colored.png' alt='dropdown' width='8' class='img-colored left-5'>");
            $('.language[data-lan=' + lang_code + ']').addClass('lang-selected');
            $('.language').not('[data-lan=' + lang_code + ']').removeClass('lang-selected');
            return false;
        }
    });
}
function setCookie(cname, cvalue, exdays) {
    var domain = window.location.hostname;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=." + domain + ";path=/";
}
function soundOn(){
	$(".bar1").animate({height:'15px'}, 80, soundOn);
	$(".bar2").animate({height:'15px'}, 100);
	$(".bar3").animate({height:'15px'}, 100);
	$(".bar1").animate({height:'1px'}, 100);
	$(".bar2").animate({height:'15px'}, 100);
	$(".bar3").animate({height:'2px'}, 100);
	$(".bar1").animate({height:'3px'}, 100);
	$(".bar2").animate({height:'2px'}, 100);
	$(".bar3").animate({height:'1px'}, 100);
	$(".bar1").animate({height:'15px'}, 100);
	$(".bar2").animate({height:'2px'}, 100);
	$(".bar3").animate({height:'10px'}, 100);
	$(".bar1").animate({height:'12px'}, 100);
	$(".bar2").animate({height:'8px'}, 100);
	$(".bar3").animate({height:'12px'}, 100);
	$(".bar1").animate({height:'9px'}, 100);
	$(".bar2").animate({height:'12.5px'}, 100);
	$(".bar3").animate({height:'3px'}, 100);
}


