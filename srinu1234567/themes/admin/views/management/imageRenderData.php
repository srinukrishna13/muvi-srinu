<input type="hidden" class="data-count" value="<?php echo $total_image ?>" /> 
<input type="hidden" id="data-count1" value="<?php echo $count_searched ?>" /> 
<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);

?>
    <table class="table table-hover" id="image_list_tbl">
        <thead>
            <tr>
                <th style="width:50px;">
                <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" class="sub_chkall" id="check_all"  >
                        <i class="input-helper"></i>

                    </label>
                </div>
                </th>
                <th>File Name</th>
                <th>Image</th>
                <?php if(!empty($checkImageKey) && $checkImageKey->config_value==1){?>
                <th>Image Key</th>
                <?php }?>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($allimage)) {
                foreach ($allimage as $key => $val) {
                    //  $images_list=explode("public",$val['list_image_name']);
                    // $images_cropped=explode("public",$val['image_name']);
                    if ($val['image_name'] == '') {
                        $img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
                    } else {
                        //$img_path = $val['list_image_name'];
                        $img_path = $base_cloud_url . $val['s3_thumb_name'];
                        $orig_img_path = $base_cloud_url . $val['s3_original_name'];
                    }
                    ?>

                    <tr >
                        
                        <td>
                           <div class="checkbox m-b-15">
                            <label>
                            <input type="checkbox" class="sub_chk" data-id="<?php echo $val['id']; ?>" name="delete_all[]" >
                            <i class="input-helper"></i>
                            
                            </label>
                            </div> 
                        </td>
                        <td>
                            <?php echo $val['image_name']; ?>
                        </td>
                        <td>
                            <div class="Box-Container m-b-10"> 
                                <div class="thumbnail"> 
                                    <a href="<?php echo $orig_img_path; ?>" target='_blank' >
                                        <img src="<?php echo $img_path; ?>"  alt="<?php echo "All_IMage"; ?>" >
                                    </a>
                                </div>
                            </div>
                        </td>    
                        <?php if(!empty($checkImageKey) && $checkImageKey->config_value==1){?>
                        <td>
                            <span id="imageKeyText<?php echo $val['id']; ?>"><?php echo $val['image_key']; ?></span>
                            <span id="imageKeyInput<?php echo $val['id']; ?>" style="display: none;">
                                <div class="col-sm-10">
                                    <input type="text" id="image_key<?php echo $val['id']; ?>" class="form-control" />
                                </div>

                                <div class="col-sm-2"><a href="javascript:void(0);" onclick="return updateImageKey(<?php echo $val['id']; ?>);" title="Update"><i class="h3 green icon-check"></i></a></div>
                            </span>
                        </td>
                        <?php }?>


                        <td class="width">
                            <h5><a id="delete_<?php echo $val['id']; ?>" onclick="delete_image(<?php echo $val['id'] ?>,<?php echo $val['No_of_Times_Used']; ?>)" style="cursor:pointer;"><em class="icon-trash"></em>&nbsp;&nbsp;Delete</a>
                            
                            </h5>
                            <?php if(!empty($checkImageKey) && $checkImageKey->config_value==1){?>
                            <h5><a id="edit_<?php echo $val['id']; ?>" onclick="edit_key(<?php echo $val['id'] ?>)" style="cursor:pointer;"><em class="icon-pencil"></em>&nbsp;&nbsp;Edit Key</a>
                            </h5>
                            <?php }?>
                            <!--h5><a href="javascript:void(0);"  onclick="openEmbedBox('embedbox_<?php echo $val['id']; ?>');" class="embed-box"><em class="fa fa-chain"></em>&nbsp;&nbsp; Embed</a></h5>
                            <div style="display:none;" class="animate-div" id="embed_<?php echo $val['id']; ?>">Copied!</div>
                            <h5 class="search-new" id='embedbox_<?php echo $val['id']; ?>' style="display:none;">
                                <div class="form-group input-group">
                                    <input type="text"  class="form-control" placeholder="Embed Code..." value='<?php echo $orig_img_path; ?>'>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-blue copyToClipboard"  data-clipboard-text='<?php echo $orig_img_path; ?>' type="button" onclick="CopytoImageClipeboard('embed_<?php echo $val['id']; ?>');">Copy!</button>
                                    </span>
                                </div>
                            </h5-->	

                        </td>
                    </tr>	
                <?php }
            } else { ?>
                <tr>
                    <td colspan="3"> No Image Found</td>
                </tr>

            <?php } ?>					

        </tbody>
    </table>
<script>
$(document).ready(function(){
$("#Filedata").change(function(){
    readURL(this);
});
$("#check_all").click(function () {
   
        $(".sub_chk").prop('checked', $(this).prop('checked'));
    });
});
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
             var file = e.target.result;
               
                if (file.substr(0, 10) == 'data:image')
                {
                   
                $('#upl_img').html('<img id="upload_preview" src="'+e.target.result+'" data-original="' + e.target.result + '">');
                $("#upload_preview").load(function(){
                        aspectratio();
                    });
                
                }
            
        }

        reader.readAsDataURL(input.files[0]);
    }
}


function aspectratio() {
   
    $("#upload_preview").each(function () {
        var maxWidth = 200;
        var maxHeight = 200;

        var width = $("#upload_preview").width();
        var height = $("#upload_preview").height();
        var ratioW = maxWidth / width;  // Width ratio
        var ratioH = maxHeight / height;  // Height ratio

        // If height ratio is bigger then we need to scale height
        if (ratioH > ratioW) {

            //  $("#preview" + id).css("width", maxWidth);
            //  $("#preview" + id).css("height", height * ratioW);
            $("#upload_preview").css("width", maxWidth);
            $("#upload_preview").css("height", height * ratioW);// Scale height according to width ratio
        }
        else if (ratioH == ratioW)
        {
            $("#upload_preview").css("width", maxWidth);
            $("#upload_preview").css("height", maxHeight);// Scale height according to width ratio

        }
        else { // otherwise we scale width

            // $("#preview" + id).css("height", maxHeight);
            // $("#preview" + id).css("width", height * ratioH); 
            $("#upload_preview").css("width", maxWidth);
            $("#upload_preview").css("height", height * ratioW);// according to height ratio
        }
    });
}


function delete_image(rec_id,No_of_Times_Used)
{
    $("#delete_"+rec_id).after("<img src='"+HTTP_ROOT+"/images/loader.gif' height='20px' class='ajax-loader'/>");
   swal({
                                title: "Delete Image?",
                                text:"Are you sure to delete the image ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html:true
                            }, function(isConfirm) { 
                                
                               // console.log("inside");
                               if(isConfirm){
                               delete_singleimage(rec_id,No_of_Times_Used);
                           }
                           else
                           {
                            $(".ajax-loader").hide();   
                           }
                               
                                }); 
}

function delete_singleimage(rec_id,No_of_Times_Used)
{
    var curent_active_page =  $('#page-selection ul.bootpag li.active').data('lp');
     var prev_page =  $('#page-selection ul.bootpag li.prev').data('lp');
     var searchText = $.trim($(".search").val()); 
     var imagecount_activepage=$("#image_list_tbl > tbody > tr").length;
    var selected_count=1;
    
        if(selected_count<imagecount_activepage)
       {
        var page=curent_active_page;          
       }
       else
       {
        var page=prev_page;           
       }
                         $.ajax({  
                                url:'<?php echo Yii::app()->getBaseUrl(true); ?>/management/DeleteImage',  
                                method:'POST',  
                                data:{rec_id:rec_id,No_of_Times_Used:No_of_Times_Used},  
                               success:function(res)  
                                 {                                 
                                  if(res){
                                    getimageDetails(searchText,page);
                                    $(".ajax-loader").hide();
                                    swal("Image deleted successfully");
                                  }else
                                  {
                                   swal("Image already in use");   
                                  }
                                }
                            });   
}

function getimageDetails(searchText,page)
    {
   
    $.post('/management/searchImagePage',{'search_value':searchText,'page':page},function(res){
        
        $('#image_list_tbl').html(res);
        var mydata = $(res).find('data-count');
        var mydata1=$("#data-count1").val();
        var regex = /\d+/g;
        var count = mydata.prevObject[0].outerHTML.match(regex);       
        var page_size = $('#page_size').val();       
        var total = parseInt(count)/parseInt(page_size);       
        var maxVisible = total/2;
        if(maxVisible <= 5){
            maxVisible = total;
        }
        
        
        if(parseInt(count) < parseInt(page_size)){
          $('#page-selection').parent().hide();
        }else{
            if($('.page-selection-div').length <= 0){
                $('#image_list_tbl').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div><div class="h-40"></div>');
            }
            $('#page-selection').parent().show();
        }
        $('#page-selection').bootpag({
            
            total: Math.ceil(total),
            page: page,
            maxVisible: 5
        }).on('page', function(event, num){
            $.post('/management/searchImagePage',{'page':num},function(res){
                $('#image_list_tbl').html(res);
                $('.loader').hide();
            });
        });
    });
}
</script>