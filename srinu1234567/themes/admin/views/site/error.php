<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div class="error-page">
	<h2 class="headline text-info"><?php echo $code; ?></h2>
	<div class="error-content">
		<h3><i class="fa fa-warning text-yellow"></i><?php echo CHtml::encode($message); ?></h3>
		<p>
			We could not find the page you were looking for. 
			Meanwhile, you may <a href='<?php echo Yii::app()->createUrl('admin/dashboard');?>'>return to dashboard</a> or try using the search form.
		</p>
	</div><!-- /.error-content -->
</div>