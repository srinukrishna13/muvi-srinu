<form class="form-horizontal" method="post" role="form" id="account" name="account" action="<?php echo Yii::app()->baseUrl; ?>/store/saveShippingCost">
    <input type="hidden" name="pg[id]" value="<?php echo $data['id']; ?>">
    <div class="modal-body">                            
        <div class="form-group">
            <label for="country" class="col-md-4 control-label">Country<span class="red"><b>*</b></span>:</label>
            <div class="col-md-8">
                <div class="select">
                    <select id="country" name="pg[country]" class="form-control input-sm">
                        <option value=''>Select Country</option>
                       <?php foreach($countrys as $country) {?> 
                       <option value="<?php echo $country['code'];?>" <?php echo ($country['code']==$data['country'])?'selected':'';?> ><?php echo $country['country'];?></option>
                       <?php } ?>
                    </select>
                </div>
            </div>
        </div> 
        <div class="form-group">
            <label for="method" class="col-md-4 control-label">Shipping Method<span class="red"><b>*</b></span>:</label>
            <div class="col-md-8">
                <div class="select">
                    <select id="method" name="pg[method]" class="form-control input-sm">
                       <?php foreach($methods as $method) {?> 
                       <option value="<?php echo $method['method_unique_name'];?>" <?php echo ($method['method_unique_name']==$data['method'])?'selected':'';?> ><?php echo $method['method'];?></option>
                       <?php } ?>
                    </select>
                </div>
            </div>
        </div> 
        <div class="form-group">
            <label for="size" class="col-md-4 control-label">Shipping Size<span class="red"><b>*</b></span>:</label>
            <div class="col-md-8">
                <div class="select">
                    <select id="size" name="pg[size]" class="form-control input-sm">
                       <?php foreach($sizes as $size) {?> 
                       <option value="<?php echo $size['size_unique_name'];?>" <?php echo ($size['size_unique_name']==$data['size'])?'selected':'';?> ><?php echo $size['size'];?></option>
                       <?php } ?>
                    </select>
                </div>
            </div>
        </div>  
        <div class="form-group">
            <label for="shipping_cost" class="col-md-4 control-label">Shipping Cost<span class="red"><b>*</b></span>:</label>  
            <div class="col-md-3">
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm currency" name="pg[currency]">
                            <?php foreach ($currency as $value) {?>
                            <option value="<?php echo $value['id'];?>"  <?php echo ($value['id']==$data['currency'])?'selected':'';?> ><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>              
            <div class="col-md-5">
                <div class="fg-line">
                    <input type='number' step="any" min="0" id="shipping_cost" name="pg[shipping_cost]" placeholder="Enter shipping cost" value="<?php echo ($data['shipping_cost']!='0.00')?$data['shipping_cost']:''; ?>" class="form-control input-sm" />
                </div>
            </div>    
        </div> 
      </div>
      <div class="modal-footer">
            <button type="submit" name="save" value="save" class="btn btn-primary"><?php echo ($data['id'] !='')?'Update':'Add'; ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>       
      </div>
</form>
<script type="text/javascript">
    $('#account').validate({
        rules: {
            "pg[country]": {required: true},
            "pg[method]": {required: true},
            "pg[size]": {required: true},
            "pg[shipping_cost]": {required: true}
        }, 
        messages: { 
            "pg[country]": 'Please select country.',
            "pg[method]": 'Please select shipping method.',
            "pg[size]": 'Please select size.',
            "pg[shipping_cost]": 'Please enter Shipping Cost.',
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }
    });
    var x = validate.form();
    if (x) {
        $('#account').submit();
    } 
</script>