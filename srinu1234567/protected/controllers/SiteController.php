<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $layout = 'main';
    
    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (Yii::app()->user->id) {
            if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)){ // added for issue due to partner login
                if(!isset($_SERVER['HTTP_REFERER'])) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/partners/video');
                    exit();
                }
            }
        }
        return true;
    }
    
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
//       	if($themeName == 'noaccount' || $themeName=='underconstruction'){
//			$this->render('index');
//		}
		if (HOST_IP == '52.0.232.150' && DOMAIN != 'idogic') {
            $this->loadWordpress();
        }
        //$this->pageTitle = 'Build White Label Video on Demand (VoD) Platform, Streaming Site using Muvi - Open Source VoD Platform or Software & Server Provider';
        $this->pageTitle = 'Build Video on Demand (VOD) Platform, Video Streaming Site, TV Everywhere Apps & Pay OTT TV using Muvi White label Open Source VOD Software service';
        $this->pageKeywords = 'Open Source VoD Platform, VoD Platform Software, Muvi, Build White Label Video on Demand, VoD, PPV, Video on Demand, Video, Pay per View, monetize video, video monetization';
        $this->pageDescription = 'Muvi is an open source VOD software Provider that enables video content owners to build White Label Video Streaming site, Video on Demand Platform & TV Everywhere Apps to monetize their Video content with Zero Investment.';
		if((HOST_IP == '52.211.41.107' || HOST_IP == '52.77.47.122' || HOST_IP == '52.70.64.85' || HOST_IP == '52.65.244.101' ) && isset(Yii::app()->user->id)){
			$this->redirect('admin/dashboard');exit;
		}else if(HOST_IP == '52.211.41.107' || HOST_IP == '52.77.47.122' || HOST_IP == '52.70.64.85' || HOST_IP == '52.65.244.101'){
			Yii::app()->theme = 'admin';
			$this->layout = 'partner';
			$this->enterpriselogo = 1;
			$this->render('enterpise_login');
		}else{
			$this->render('index');
		}
        
    }
    
    public function actionIntrovideo() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
       $host_ip = Yii::app()->params['host_ip'];
      
        if (in_array(HOST_IP,$host_ip)) {
            $this->loadWordpress();
        }
        //$this->pageTitle = 'Build White Label Video on Demand (VoD) Platform, Streaming Site using Muvi - Open Source VoD Platform or Software & Server Provider';
        $this->pageTitle = 'Build Video on Demand (VOD) Platform, Video Streaming Site, TV Everywhere Apps & Pay OTT TV using Muvi White label Open Source VOD Software service';
        $this->pageKeywords = 'Open Source VoD Platform, VoD Platform Software, Muvi, Build White Label Video on Demand, VoD, PPV, Video on Demand, Video, Pay per View, monetize video, video monetization';
        $this->pageDescription = 'Muvi is an open source VOD software Provider that enables video content owners to build White Label Video Streaming site, Video on Demand Platform & TV Everywhere Apps to monetize their Video content with Zero Investment.';

        $this->render('index');
    }     

    public function actiondownload() {
        $this->render('download');
    }

    public function actionDownloadpdf() {
        $file_name = "MuviStudioBrochure.pdf";
        $path = dirname(__FILE__) . '/../../docs/';
        $fullfile = $path . $file_name;
        if (file_exists($fullfile)) {
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=" . urlencode($file_name));
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Description: File Transfer");
            header("Content-Length: " . filesize($fullfile));
            flush(); // this doesn't really matter.
            $fp = fopen($fullfile, "r");
            while (!feof($fp)) {
                echo fread($fp, 65536);
                flush(); // this is essential for large downloads
            }
            fclose($fp);
        } else {
            echo 'File not found.' . $fullfile;
            exit();
        }
    }

    public function actionDownloadsubmit() {
        $response = 'error';
        $msg = 'Error in submitting.';

        $dnld_name = isset($_POST['dnld_name']) ? $_POST['dnld_name'] : '';
        $dnld_email = isset($_POST['dnld_email']) ? $_POST['dnld_email'] : '';
        $dnld_cpmpany = isset($_POST['dnld_cpmpany']) ? $_POST['dnld_cpmpany'] : '';

        if ($dnld_email != '' && $dnld_name != '') {
            $to = 'sales@muvi.com';
            $to_name = 'Muvi';
            $from = $dnld_email;
            $from_name = $dnld_name;
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = 'https://www.muvi.com/themes/bootstrap/images/logo.png';
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';

            $msg = '<p>SDK document downloaded!</p>';
            $msg.= '<p>';
            $msg.= '<b>Name :</b> ' . $dnld_name . '<br />';
            $msg.= '<b>Email :</b> ' . $dnld_email . '<br />';
            $msg.= '<b>Company :</b> ' . $dnld_cpmpany . '<br />';
            $msg.= '</p>';

            $params = array(
                'website_name'=> $site_url,
                'logo'=> $logo,
                'msg'=> $msg
            );

            $subject = 'SDK document downloaded!';
            $message = array('subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => array(
                    array(
                        'email' => $to,
                        'name' => $to_name,
                        'type' => 'to'
                    ),
                )
            );
            $template_name = 'studio_general_contact';
            //$this->mandrilEmail($template_name, $params, $message);
                 Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/studio_general_contact',array('params'=>$params),true);
                $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from); 
        
            $response = 'success';
            //$msg = Yii::app()->getbaseUrl(true).'/sdk/downloadpdf';           
            //$msg = $filepath = Yii::app()->getbaseUrl(true).'/docs/MuviSDKBrochure.pdf';
            //$msg = 'Thank you for downloading the broucher.';
            $msg = Yii::app()->getbaseUrl(true) . '/site/downloadpdf';
        }
        $ret = array('stat' => $response, 'message' => $msg);
        //Yii::app()->user->setFlash('download','SDK document downloaded!');
        //$this->refresh();
        echo json_encode($ret);
    }

    public function actionDemorequest() {
        $response = 'error';
        $msg = 'Error in submitting.';

        $demo_name = isset($_POST['demo_name']) ? $_POST['demo_name'] : '';
        $demo_email = isset($_POST['demo_email']) ? $_POST['demo_email'] : '';
        $demo_company = isset($_POST['demo_company']) ? $_POST['demo_company'] : '';
        if ($demo_email != '' && $demo_name != '') {
            $to = 'sales@muvi.com';
            $to_name = 'Muvi';
            $from = $demo_email;
            $from_name = $demo_name;
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = 'https://www.muvi.com/themes/bootstrap/images/logo.png';
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';

            $msg = '<p>Demo request!</p>';
            $msg.= '<p>';
            $msg.= '<b>Name :</b> ' . $demo_name . '<br />';
            $msg.= '<b>Company :</b> ' . $demo_company . '<br />';
            $msg.= '<b>Email :</b> ' . $demo_email . '<br />';
            $msg.= '</p>';

            $params = array(
               'website_name' => $site_url,
               'logo'=> $logo,
              'msg'=> $msg
            );

            $subject = 'Demo request!';
            $message = array('subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => array(
                    array(
                        'email' => $to,
                        'name' => $to_name,
                        'type' => 'to'
                    ),
                )
            );
            $template_name = 'studio_general_contact';
             Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/studio_general_contact',array('params'=>$params),true);
                $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from); 
        
            $response = 'success';
            $msg = 'Thank you for your interest. Someone from our team will contact you shortly!';
        }
        $ret = array('stat' => $response, 'message' => $msg);
        echo json_encode($ret);
    }

    //Sending Email for the Market place request form
    public function actionMarketplacerequest() {
        $response = 'error';
        $msg = 'Error in submitting.';

        $market_name = isset($_POST['market_name']) ? $_POST['market_name'] : '';
        $market_email = isset($_POST['market_email']) ? $_POST['market_email'] : '';
        $market_company = isset($_POST['market_company']) ? $_POST['market_company'] : '';
        if ($market_email != '' && $market_name != '') {
            $to = 'sales@muvi.com';
            $to_name = 'Muvi';
            $from = $market_email;
            $from_name = $market_name;
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = 'https://www.muvi.com/themes/bootstrap/images/logo.png';
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';

            $msg = '<p>Request for Marketplace!</p>';
            $msg.= '<p>';
            $msg.= '<b>Name :</b> ' . $market_name . '<br />';
            $msg.= '<b>Company :</b> ' . $market_company . '<br />';
            $msg.= '<b>Email :</b> ' . $market_email . '<br />';
            $msg.= '</p>';

            $params = array(
                'website_name'=> $site_url,
                'logo'=> $logo,
               'msg'=> $msg
            );

            $subject = 'Request for Marketplace';
            $message = array('subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => array(
                    array(
                        'email' => $to,
                        'name' => $to_name,
                        'type' => 'to'
                    ),
                )
            );
            $template_name = 'studio_general_contact';
            //$msg = $this->mandrilEmail($template_name, $params, $message);
             Yii::app()->theme = 'bootstrap';
             $thtml = Yii::app()->controller->renderPartial('//email/studio_general_contact',array('params'=>$params),true);
             $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',$market_name); 
        
            $response = 'success';
            $msg = 'Thank you for your interest. Someone from our team will contact you shortly!';
        }
        $ret = array('stat' => $response, 'message' => $msg);
        echo json_encode($ret);
    }

    public function actionvideo_streaming_platform() {
        $host_ip = Yii::app()->params['host_ip'];
      
        if (in_array(HOST_IP,$host_ip)) {
            $this->loadWordpress();
        }
        $this->pageTitle = 'Muvi – White Label OTT Video Streaming Platform, VOD Server and Software Provider';
        $this->pageKeywords = 'Muvi, Digital Asset Management, Video Streaming Site, Video on Demand, VoD, PPV, Pay per view, streaming website, streaming, tv streaming, movie streaming, Digital Asset Management, DAM, Cloud Storage, Cloud Video Storage';
        $this->pageDescription = 'Muvi offers tools to build Video on Demand (VoD) Platform, Video Streaming Server, Software and Platform to launch your own video streaming or VoD site at Zero Cost!';
        $this->render('video_streaming_platform');
    }

    private function loadWordpress() {
        // Include WordPress
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        define('WP_USE_THEMES', false);
        global $wp, $wp_query, $wp_the_query;
        require('wordpress/wp-blog-header.php');
        spl_autoload_register(array('YiiBase', 'autoload'));
    }

    /**
     * This is the action to handle external exceptions.
     */
    /* public function actionError()
      {
      if($error=Yii::app()->errorHandler->error)
      {
      if(Yii::app()->request->isAjaxRequest)
      echo $error['message'];
      else
      $this->render('error', $error);
      }
      } */

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $studio_id = Yii::app()->common->getStudiosId();
                // welcome email                 
                $std = new Studio();                
                $studio = $std->findByPk($studio_id);       

                $site_url = Yii::app()->getBaseUrl(true);
                $logo = '<a href="'.$site_url.'">'.$this->siteLogo.'</a>';

                $site_link = '<a href="'.$site_url.'">'.$studio->name.'</a>';

                if($studio->fb_link != '')
                    $fb_link = '<a href="'.$studio->fb_link.'" ><img src="'.$site_url.'/images/fb.png" alt="Find Us on Facebook"></a>';
                else
                    $fb_link = '';
                if($studio->tw_link != '')
                    $twitter_link = '<a href="'.$studio->tw_link.'" ><img src="'.$site_url.'/images/twitter.png" alt="Find Us on Twitter"></a>';
                else
                    $twitter_link = '';                 
                if($studio->gp_link != '')
                    $gplus_link = '<a href="'.$studio->gp_link.'" ><img src="'.$site_url.'/images/google_plus.png" alt="Find Us on Google Plus"></a>';
                else
                    $gplus_link = '';     
                
                $msg = '<p>'.@$model->name.' has fill up the contact request in '.$studio->name.' website.</p>';
                $msg.= '<p>';
                $msg.= '<b>Email :</b> '.@$model->email.'<br />';
                $msg.= '<b>Message :</b> '.@$model->subject.'<br />';                
                $msg.= '</p>';                
                
                $params = array(
                    'website_name'=> $studio->name,
                    'site_link' => $site_url,
                    'logo' => $logo,
                    'username'=> @$model->name,
                    'email'=> @$model->email,
                    'message'=> $msg,
                    'fb_link'=> @$fb_link,
                    'twitter_link'=> @$twitter_link,
                    'gplus_link'=> @$gplus_link
                );
                
                $studio_email = Yii::app()->common->getStudioEmail();
                $subject = 'New contact from '.$studio->name;
                
                
                $to=array($studio_email);
                $from=$studio_email;
                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/studio_contact_us',array('params'=>$params),true);
                $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',$studio->name);  
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    /* public function actionLogin()
      {
      $model=new LoginForm;

      // if it is ajax validation request
      if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
      {
      echo CActiveForm::validate($model);
      Yii::app()->end();
      }

      // collect user input data
      if(isset($_POST['LoginForm']))
      {
      $model->attributes=$_POST['LoginForm'];
      // validate user input and redirect to the previous page if valid
      if($model->validate() && $model->login())
      $this->redirect(Yii::app()->user->returnUrl);
      }
      // display the login form
      $this->render('login',array('model'=>$model));
      }

      public function actionLogout()
      {
      Yii::app()->user->logout();
      $this->redirect(Yii::app()->homeUrl);
      } */

    /**
     * @method type pagesList(type $paramName) Description
     * @return HTML A list of all static pages
     * @author GDR<support@muvi.com>
     */
    /*
    function actionDefaultpages() {
        $studios = new Studio();                
        $studios = $studios->findAll();   
        echo "Total Studios ".count($studios);
        if(count($studios) > 0)
        {
            foreach($studios as $studio)
            {
                $page = new Page();
                $pages = $page->findAllByAttributes(array('studio_id' => $studio->id, 'permalink' => 'terms-of-use'), array('order' => 'id DESC'));   
                if(count($pages) == 0)
                {
                    $page_name = 'Terms of Use';
                    $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
                    $permalink = Yii::app()->common->formatPermalink($page_name);
                    $page = new Page();  
                    $page->title = htmlspecialchars($page_name);
                    $page->permalink = $permalink;
                    $page->content = htmlspecialchars($page_content);
                    $page->studio_id = $studio->id;
                    $page->created_date = new CDbExpression("NOW()");
                    $page->save();                  
                    $terms_id = $page->id;  
                    echo '<br />Terms page inserted for '.$studio->name;
                }
                else {
                    echo '<br />Terms page already added for '.$studio->name;
                }
            }
        } 
        else {
            echo "No Studio found";
       }
    }*/
    
    /*
    function actionDefaultuser() {
        $enc = new bCrypt();         
        $encrypted_pass = $enc->hash('muvi2015');         
        $user = new SdkUser;             
        $user->email = 'demo@muvi.com';
        $user->studio_id = 0;
        $user->is_developer = 1;
        $user->display_name = htmlspecialchars('Demo Account');
        $user->encrypted_password = $encrypted_pass;
        $user->status = 1;
        $user->created_date = new CDbExpression("NOW()");
        $user_id = $user->save();           
    }    

    public function actionAbout() {
        $this->pageTitle = 'About Us';
        $this->render('about');
    }
     * *
     */

//SDK Actions
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionHome() {
        $studio_id = Yii::app()->common->getStudiosId();
        $is_draft = 0;
        if (isset(Yii::app()->request->cookies['draft']->value) && Yii::app()->request->cookies['draft']->value == 1) {
            $is_draft = 1;
        }
        $studio = $this->studio;
        $BusinessName = $studio->name;
        unset(Yii::app()->session['previous_url']);
        unset(Yii::app()->session['choosen_plan']);
        $meta = Yii::app()->common->pagemeta('home');
        if($meta['title'] != ''){
            $title = $meta['title'];
        }else{
            $title = 'Home | '.$BusinessName;
        }
        if($meta['description'] != ''){
            $description = $meta['description'];
        }else{
            $description = '';
        }
        if($meta['keywords'] != ''){
            $keywords = $meta['keywords'];
        }else{
            $keywords = '';
        }
        
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords; 
        $this->canonicalurl = Yii::app()->getBaseUrl(true).'/home';
        $theme = $this->site_parent_theme;
        $template = Yii::app()->common->getTemplateDetails($theme);    
        
        $banner_sections = json_encode(Yii::app()->general->MainBanners($template->id));        
        $sections = json_encode(Yii::app()->general->FeaturedBlockContentsNew($template->id));  
		$this->render('home', array('sections' => @$sections, 'banner_sections' => $banner_sections));                
    }

    //All studio home pages
    public function actionSdkIndex() {
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }        
        $themeName = Yii::app()->theme->name;
        if($themeName == 'noaccount' || $themeName=='underconstruction' || $themeName=='unavailable' ){
                $this->render('index');exit;
        }
		
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $all_channel_list=Yii::app()->general->getAllChannel();
        $studio = $this->studio;
        $BusinessName = $studio->name;
        $hide_featured = Yii::app()->custom->hideFeaturedSectionsLandingPage();
        unset(Yii::app()->session['previous_url']);
        unset(Yii::app()->session['choosen_plan']);
        $meta = Yii::app()->common->pagemeta('home');
        if($meta['title'] != ''){
            $title = $meta['title'];
        }else{
            $title = 'Home | '.$BusinessName;
        }
        if($meta['description'] != ''){
            $description = $meta['description'];
        }else{
            $description = '';
        }
        if($meta['keywords'] != ''){
            $keywords = $meta['keywords'];
        }else{
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords; 
        $this->canonicalurl = Yii::app()->getBaseUrl(true);
        $theme = $this->site_parent_theme;
        $template = Yii::app()->common->getTemplateDetails($theme);    
        //Templkate for a single TV show
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true);
        Yii::app()->session['backbtnlink'] = $path;
        $EpDetails = array();
        $banner_sections = json_encode(Yii::app()->general->MainBanners($template->id));
        $studio_id = Yii::app()->common->getStudiosIdPreview();
        //$sql = "SELECT * FROM featured_section WHERE studio_id={$studio_id}  AND template_id={$template->id} AND (language_id={$this->language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM featured_section WHERE studio_id={$studio_id} AND template_id={$template->id} AND language_id={$this->language_id})) ORDER BY id_seq ASC";
       // $sql = "SELECT * FROM featured_section WHERE studio_id={$studio_id} AND (language_id={$this->language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM featured_section WHERE studio_id={$studio_id} AND language_id={$this->language_id})) ORDER BY id_seq ASC";
        $sections = FeaturedSection::model()->getFeaturedSections($studio_id, $language_id);  
        $gateways = array();
        $plans = array();   
        $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
        //Added for subcategory list
        $categories = array();
        
        if (Yii::app()->general->getStoreLink() && $template->content_type == 1) {
            if (Yii::app()->custom->HomePageLayout() == 0) {
                $featured_product = Yii::app()->general->FeaturedBlockContentsNew($template->id, @$sections);
                $pagination = '';
            } else {
                $_SESSION['totalqnt'] = isset($_SESSION['totalqnt']) ? $_SESSION['totalqnt'] : 0;
                $pr = self::getProductlist($studio_id);
                $featured_product[] = array(
                    'title' => utf8_encode(Yii::app()->controller->Language['shop']),
                    'total' => count($pr['product']),
                    'contents' => $pr['product']
                );
                $pagination = $pr['pagination'];
            }
        }
        if ($hide_featured > 0 && (!(Yii::app()->user->id) || (Yii::app()->user->id < 1) || ($theme!='byod'))) {
            $gateways = $plans = $plan_payment_gateway = $sections = array();
        } else {
            $fsections = Yii::app()->general->FeaturedBlockContentsNew($template->id, @$sections);
            $sections = json_encode($fsections);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $gateways = $plan_payment_gateway['gateways'];
                $plans = $plan_payment_gateway['plans'];
            }                
        }
        $categories = Yii::app()->general->showCategoriesInHomepage();
        $this->render('index', array('sections' => $sections, 'banner_sections' => $banner_sections, 'all_channel' => $all_channel_list, 'featured_product' => @$featured_product, 'pagination' => @$pagination, 'categories' => json_encode($categories), 'gateways' => $gateways, 'plans' => $plans));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
		$this->pageTitle = 'Muvi - Page Not Found - The content you are searching for is not available';
		$this->pageDescription = 'Page Not Found. The content you are searching for is not available at this moment, please contact us for further details.';
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /* only to reload parent page */

    public function actionReload() {
        $this->layout = 'blank';
        $this->render('blank');
    }

    /**
     * Displays the contact page
     */
    public function actionContactus() {
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = $this->studio;
		if(@$studio->is_csrf_enabled && @$_POST){
			if(@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']){
				unset($_SESSION['csrfToken']);
			}else{
				$url = Yii::app()->createAbsoluteUrl();
				header("HTTP/1.1 301 Moved Permanently");
		        header('Location: '.$url);exit();
			}
		}
        $studio_email = Yii::app()->common->getStudioEmail();
        $studio_emails = Yii::app()->common->getStudioContactEmails();
        //$model=new ContactForm;
        
        if(isset($_REQUEST['mobileview'])){
            $this->is_mobileview =true;
        }
        if (isset($_POST['ContactForm']) && ($studio_email != '') && (@$_REQUEST['ccheck']=='') && (@$_REQUEST['submit-btn']=='contact')){
            $data = $_POST['ContactForm'];
            
            $res = self::sendContactEmail($data, $studio);         
            if ($_SERVER['HTTP_X_PJAX'] == true) {
                $res = $this->ServerMessage['thanks_for_contact'];
                echo $res;
                exit;
            } else {
                Yii::app()->user->setFlash('success', $this->ServerMessage['thanks_for_contact']);
            }
            if ($this->is_mobileview != '') {
                $url = $this->createUrl('contactus/mobile_view/');
                $this->redirect($url);
                exit;
            } else {
                $this->refresh();
            }
        }

        $meta = Yii::app()->common->pagemeta('contact');
        if($meta['title'] != ''){
            $title = $meta['title'];
        }else{
            $title = 'Contact Us | '. $studio->name;
        }

        if($meta['description'] != ''){
            $description = $meta['description'];
        }else{
            $description = '';
        }
        if($meta['keywords'] != ''){
            $keywords = $meta['keywords'];
        }else{
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
       	$this->render('contactus');
    }
    
    public function sendContactEmail($data, $studio) {
        if (isset($data) && isset($studio)) {
            $studio_id = $studio->id;
            $language_id = $this->language_id;
            $subject = $studio->name . ' :: New Contact Us';
            $site_url = Yii::app()->getBaseUrl(true);
            $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);
            $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
            $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
            $msg = htmlspecialchars($data['message']);
            $useremail = $data['email'];
            if (isset($data['name']) && strlen(trim($data['name'])) > 0)
                $username = htmlspecialchars($data['name']);
            else
                $username = '';

            $template_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                "message" => $msg
            );
            if ($studio->contact_us_email) {
                $admin_to [] = $studio_email = $studio->contact_us_email;
                if (strstr($studio_email, ',')) {
                    $admin_to = explode(',', $studio_email);
                    $studio_email = $admin_to[0];
                }
            } else {
                $studio_usr = new User();
                $studio_user = $studio_usr->findByAttributes(array('studio_id' => $studio_id));
                $studio_email = $studio_user->email;                
                $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
                if ($linked_emails->notification_from_email) {
                    $studio_email = $linked_emails->notification_from_email;
                }
                $admin_to[] = $studio_email;
            }
            $studio_name = $StudioName = $studio->name;
            $EndUserName = $username;
            $admin_subject = $subject;
            $admin_from = $useremail;
            //check for cc emails ajit@muvi.com issue id - 5975
            $admin_cc = array();
            $admin_cc = Yii::app()->common->emailNotificationLinks($studio_id, 'contact_us');
            //END - check for cc emails ajit@muvi.com issue id - 5975
            $user_to = array($useremail);
            $user_from = $studio_email;
            $email_type = 'contact_us';
            $content = "SELECT * FROM `notification_templates` WHERE type= '".$email_type."' AND studio_id=".$studio_id." AND (language_id = ".$language_id." OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '".$email_type."' AND studio_id=" . $studio_id." AND language_id = ".$language_id."))";
            $data = Yii::app()->db->createCommand($content)->queryAll();
            if (count($data) > 0) {                
            } else {
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }
            $user_subject = $data[0]['subject'];
            eval("\$user_subject = \"$user_subject\";");
            $user_content = htmlspecialchars($data[0]['content']);
            eval("\$user_content = \"$user_content\";");
            $user_content = htmlspecialchars_decode($user_content); 
            $template_user_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                'content'=>$user_content
            );
            Yii::app()->theme = 'bootstrap';
            $admin_html = Yii::app()->controller->renderPartial('//email/studio_contactus', array('params' => $template_content), true);
            $user_html = Yii::app()->controller->renderPartial('//email/studio_contactus_user', array('params' => $template_user_content), true);
            $returnVal_admin = $this->sendmailViaAmazonsdk($admin_html, $admin_subject, $admin_to, $admin_from,$admin_cc,'','', $username);
            $returnVal_user = $this->sendmailViaAmazonsdk($user_html, $user_subject, $user_to, $user_from,'','','', $studio_name);
            return true;
        } else {
            return false;
        }
    }

    public function actionContactsend(){
        $studio = $this->studio;
        $studio_email = Yii::app()->common->getStudioEmail();    
        $success = 0;
        $message = 'Error in submitting the form';
        if (isset($_POST['ContactForm']) && ($studio_email != '') && (@$_REQUEST['ccheck']=='') && (@$_REQUEST['submit-btn']=='contact')){
            $data = $_POST['ContactForm'];            
            $res = self::sendContactEmail($data, $studio);  
            if($res == true)
            {
                $success = 1;
                $message = 'Thank you for contacting us. We will respond to you as soon as possible.';
            }
        }
        $ret = array('success' => $success, 'message' => $message);
        echo json_encode($ret);
    }
    
    
    

    /**
     * Displays the login page
     */
    public function actionMuvilogin() {
        if (isset($_POST) && isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['user_id']) && $_POST['user_id'] > 0) {
            $email = $_POST['email'];
            $user_id = $_POST['user_id'];
            $display_name = $_POST['display_name'];
            $fb_id = $_POST['fb_id'];
            $agent = $_POST['agent'];
            $address1 = $_POST['address1'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $country = $_POST['country'];
            $zip = $_POST['zip'];
            $phone = $_POST['phone'];
            $user_image = $_POST['user_image'];

            if (isset($_POST['from_page']) && $_POST['from_page'] == 'register')
                unset(Yii::app()->request->cookies['source']);

            if (isset($_POST['remember']) && $_POST['remember'] == TRUE) {
                $cookie = new CHttpCookie('logged_user_id', $user_id);
                $cookie->expire = time() + 60 * 60 * 24 * 7;
                Yii::app()->request->cookies['logged_user_id'] = $cookie;

                $cookie = new CHttpCookie('logged_from', $agent);
                $cookie->expire = time() + 60 * 60 * 24 * 7;
                Yii::app()->request->cookies['logged_from'] = $cookie;

                $cookie = new CHttpCookie('display_name', $display_name);
                $cookie->expire = time() + 60 * 60 * 24 * 7;
                Yii::app()->request->cookies['display_name'] = $cookie;
            }

            Yii::app()->session['logged_in'] = 1;
            Yii::app()->session['token'] = $_POST['token'];
            Yii::app()->session['agent'] = $agent;
            Yii::app()->session['logged_email'] = $email;
            Yii::app()->session['logged_user_id'] = $user_id;
            Yii::app()->session['display_name'] = $display_name;
            Yii::app()->session['logged_fb_id'] = $fb_id;
            Yii::app()->session['address1'] = $address1;
            Yii::app()->session['city'] = $city;
            Yii::app()->session['state'] = $state;
            Yii::app()->session['country'] = $country;
            Yii::app()->session['zip'] = $zip;
            Yii::app()->session['phone'] = $phone;
            Yii::app()->session['profile_image'] = $user_image;
            if (isset($_REQUEST['is_developer']) && $_REQUEST['is_developer']) {
                Yii::app()->session['is_studio_admin'] = $_REQUEST['is_developer'];
            } else {
                if (isset($_REQUEST['is_studio_admin']) && $_REQUEST['is_studio_admin'] == 'false') {
                    Yii::app()->session['is_studio_admin'] = '';
                } else {
                    Yii::app()->session['is_studio_admin'] = @$_REQUEST['is_studio_admin'];
                }
            }

            Yii::app()->session['studio_user_id'] = @$_REQUEST['studio_user_id'];
            if (isset($_REQUEST['first_login']) && $_REQUEST['first_login']) {
                Yii::app()->session['first_login'] = 1;
                // Welcome Email for Registration 
                $agent = Yii::app()->common->getAgent();
                $store_details = Yii::app()->common->storeDetails($agent);
                $store_id = Yii::app()->common->getStudiosId();
                $store = Yii::app()->common->getMuviStudioDetails($store_id);
               
                $username = @$display_name ? ((strstr(trim(@$display_name), ' ')) ? strstr(trim(@$display_name), ' ', TRUE) : trim(@$display_name)) : strstr($email, '@', TRUE);
                
               
                $studio = $this->studio;
                $site_url = Yii::app()->getBaseUrl(true);
     
                //$siteLogo=$site_url. '/images/logos/' . $studio->theme . '/' . $studio->logo_file;
               $siteLogo=$logo_path = Yii::app()->common->getLogoFavPath($store_id);
                $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="' . $agent . '" /></a>';

                $fb_link = '<a href="' . @$store_details['fb_link'] . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                $twitter_link = '<a href="' . @$store_details['twitter_link'] . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                $gplus_link = '<a href="' . @$store_details['gplus_link'] . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
                $faq_link = '<a href="' . $this->createAbsoluteUrl('site/faq') . '" >FAQs</a>';
                $payment_link = '<a href="' . $this->createAbsoluteUrl('user/process') . '" >click here</a>';
                $admin_email = $store['email'];
                $agent_address = $store_details['agent_address'];
                $params = array(
                    'website_name'=> $agent,
                    'website_url'=> $site_url,
                    'logo'=> $logo,
                    'username'=> ucfirst(@$username),
                    'fb_link'=> @$fb_link,
                    'twitter_link'=> @$twitter_link,
                    'gplus_link'=> @$gplus_link,
                    'payment_link'=> @$payment_link,
                    'faq_link'=> @$faq_link,
                    'admin_email'=> @$admin_email,
                    'agent_address'=> @$agent_address
                );

                $subject = "Welcome to " . $store_details['siteName'] . '!';
                $message = array('subject' => $subject,
                    'from_email' => $admin_email,
                    'from_name' => $store_details['name'],
                    'to' => array(
                        array(
                            'email' => $email,
                            'name' => $username,
                            'type' => 'to'
                        )
                    )
                );
                $from_email=$admin_email;
                $to_email=array($email);
                
                Yii::app()->theme = 'bootstrap';
                $html = Yii::app()->controller->renderPartial('//email/studio_user_welcome',array('params'=>$params),true);
                $returnVal_admin=$this->sendmailViaAmazonsdk($html,$subject,$to_email,$store['email'],'','','',''); 

                $template_name = 'studio_user_welcome';
                
            }

            if (@$_REQUEST['permalink']) {
                $url = $this->createUrl('movie/' . @$_REQUEST['permalink']);
                $this->redirect($url);
                exit;
            } else {
                echo 'succeed';
            }
        } else
            echo 'failed';
    }
    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        //Yii::app()->user->logout();
        //$value = (string)Yii::app()->request->cookies['logged_from'];
        //unset(Yii::app()->session['logged_user_id']);
        //unset(Yii::app()->request->cookies['logged_from']);
        Yii::app()->session->clear();
        Yii::app()->session->destroy();

        Yii::app()->request->cookies->clear();

        //$this->redirect(Yii::app()->homeUrl);
        if (Yii::app()->session['logged_user_id'] != '')
            echo 'error';
        else {
            echo 'loggedout';
        }
    }

    public function actionLogmeout() {
        unset(Yii::app()->session['logged_user_id']);
        unset(Yii::app()->request->cookies['logged_from']);
        //Yii::app()->session->clear();
        //Yii::app()->session->destroy();
        echo $_GET['callback'] . "(" . json_encode("loggedout") . ")";
    }

    public function actionAuthorized() {
        $this->render('authorized');
    }

    public function actionchange_css() {
        extract($_REQUEST);
        $chk = "select id from item where site='$site'";
        $connection = Yii::app()->db1;
        $command = $connection->createCommand($chk);
        $rowCount = $command->execute();
        if ($rowCount == 0) {
            $qry = "insert into item (site, css_file) values('$site', '$css_file')";
        } else {
            $item = $command->queryAll();
            $qry = "update item set css_file='$css_file' where id=" . $item[0]["id"];
        }
        $command = $connection->createCommand($qry);
        $command->queryAll();
    }

    /**
     * @method public aboutus() Static Aboutus page
     * @author GDR <info@muvi.com>
     * @return HTML 
     */
    function actionAboutus() {

        $meta = Yii::app()->common->pagemeta('aboutus');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];
        $this->pageKeywords = $meta['keywords'];
		$this->canonicalurl = Yii::app()->getBaseUrl(true).'/aboutus';
        $this->render('aboutus');
    }

    /**
     * @method public aboutmaa() Static Aboutus page
     * @author GDR <info@muvi.com>
     * @return HTML 
     */
    function actionAboutmaa() {

        $meta = Yii::app()->common->pagemeta('aboutmaa');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];
        $this->pageKeywords = $meta['keywords'];

        $this->render('aboutmaa');
    }

    /**
     * @method public terms() Static Aboutus page
     * @author GDR <info@muvi.com>
     * @return HTML 
     */
    function actionTerms() {
        $meta = Yii::app()->common->pagemeta('terms');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];
        $this->pageKeywords = $meta['keywords'];
		$this->canonicalurl = Yii::app()->getBaseUrl(true).'/terms';
        $this->render('terms');
    }

    /**
     * @method public PrivacyPolicy() Static Aboutus page
     * @author GDR <info@muvi.com>
     * @return HTML 
     */
    function actionPrivacypolicy() {
        $meta = Yii::app()->common->pagemeta('privacy');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];
        $this->pageKeywords = $meta['keywords'];
		$this->canonicalurl = Yii::app()->getBaseUrl(true).'/privacypolicy';
        $this->render('privacypolicy');
    }

    /**
     * @method public faq() Static Aboutus page
     * @author GDR <info@muvi.com>
     * @return HTML 
     */
    function actionfaq() {

        $meta = Yii::app()->common->pagemeta('faq');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];
        $this->pageKeywords = $meta['keywords'];

        $this->render('faq');
    }

    /**
     * @method public flushCache() Its used for flushing caching
     * @return void 
     * @author GDR<support@muvi.com>
     */
     function actionFlushCache() {
        if (Yii::app()->cache->flush()) {
            echo "Flushed all the cache";
            exit;
        } else {
            echo "Cache Not flushed.";
            exit;
        }
    }
    

    //To upload updated files only
    public function actionupdateTemplateFiles() {
        if(isset($_REQUEST['updatetemplate']) && $_REQUEST['updatetemplate'] != ''){
            $look_theme = trim($_REQUEST['updatetemplate']);
            $criteria = new CDbCriteria;
            $criteria->select = 't.id, t.parent_theme, t.theme, t.status'; // select fields which you want in output
            $criteria->condition = 't.status = 1 AND t.is_deleted = 0 AND t.theme <> "" AND t.parent_theme = "'.$look_theme.'"';
            $criteria->order = 'id DESC';
            $studios = Studio::model()->findAll($criteria);
            //echo "<pre>";print_r($studios);exit;
            $c = 0;
            foreach ($studios as $studio) {
                $c++;
                $studio_id = $studio->id;
                $theme_folder = trim($studio->theme);
                $parent_theme = trim($studio->parent_theme);
                if ($studio_id > 0 && $studio->status == 1 && $studio->is_deleted == 0 && $theme_folder != '' && strlen($theme_folder) > 1 && $parent_theme != '' && strlen($parent_theme) > 1) { //For Leads, Customers and Demo users
                    $dest = ROOT_DIR . 'themes/' . $theme_folder;
                    $source = ROOT_DIR . 'sdkUpdatedThemes/' . $parent_theme;

                    $criteria = new CDbCriteria;
                    $criteria->select = 't.id'; // select fields which you want in output
                    $criteria->condition = 't.is_active = 1 AND t.role_id = 1 AND t.is_sdk = 1';
                    $criteria->order = 'id DESC';
                    $users = User::model()->findAll($criteria);
                    if ($users && ($studio->id >= 52 || $studio->id == 2)) {
                        $ret = Yii::app()->common->recurse_copy($source, $dest); 
                        echo '<br />Updated:'.$dest;
                    }
                }
            }            
                      
        }
        else{
            $template = new Template;               
            $templates = Template::model()->findAll(array('order' => 'id DESC'));        
            $this->renderPartial('reloadtemplates', array('templates' => $templates));  
        }
    }   
    
    
    /**
 * @method public cleanupTheme() It will clean theme folder 
 * @return boolen True
 * @author GDR<support@muvi.com>
 */
    public function actionCleanThemes(){
		$query = "SELECT u.id,u.first_name,u.email,s.phone,u.phone_no,u.studio_id,s.domain,s.subdomain,s.status,s.theme,u.signup_step FROM `user` u,studios s WHERE  u.studio_id = s.id AND u.signup_step=2 AND s.id >52 AND (DATE(s.created_dt) < '2015-04-13')";
		$db = Yii::app()->db;
		$data = $db->createCommand($query)->queryAll();
		//echo "<pre>";print_r($data);
		foreach($data AS $key=>$studio){
            $theme_folder = $studio['theme'];
            $dest = ROOT_DIR . 'themes/' . $theme_folder;
			echo "<br/> Destination- ".$dest;
            if (is_dir($dest)) {
                $del = ROOT_DIR . "themes/".$theme_folder.'/';
				echo "<br/>Delete folder==".$del.'<br/>';
                //Yii::app()->common->rrmdir($dest);                
            }
		}
	}
        
        /*
        public function actionUploadtos3() {
            require_once "Image.class.php";
            require_once "Config.class.php";
            require_once "Uploader.class.php";
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once "amazon_sdk/sdk.class.php";
            spl_autoload_register(array('YiiBase', 'autoload'));
            defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
            $config = Config::getInstance();
            $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public'); //path for images uploaded
            $config->setBucketName(Yii::app()->params->s3_bucketname);
            $config->setAmount(250);  //maximum paralell uploads
            $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions

            $uploader = new Uploader();
            $ret = $uploader->uploadsample();
            $poster = array($ret);            
        }    
         * 
         */   
        /*
        public function actionCopyuser() {
            $users = Yii::app()->db->createCommand()
                    ->select('id, email, encrypted_password, studio_id')
                    ->from('user')
                    ->order('id desc')
                    ->queryAll();
            
            foreach($users as $user)
            {
                $sdk_users = Yii::app()->db->createCommand()
                        ->select('id, email, encrypted_password, studio_id')
                        ->from('sdk_users')
                        ->where('email=:email and studio_id=:studio_id and encrypted_password!=:encrypted_password', array(':email'=>$user['email'], ':studio_id'=>$user['studio_id'], ':encrypted_password'=>$user['encrypted_password']))
                        ->order('id desc')
                        ->queryAll(); 
                foreach($sdk_users as $usr)
                {
                    $user_cls = new SdkUser;
                    $user_cls = $user_cls->findByPk($usr['id']);
                    $user_cls->encrypted_password = $user['encrypted_password'];
                    $user_cls->save();
                    
                    echo "<br />".$user['id'].' - '.$user['email'].' - '.$user['encrypted_password'].' and '.$usr['encrypted_password'];
                }
            }
        }
         * 
         */ 
        
        public function actionRobotsTxt(){
			foreach (Yii::app()->log->routes as $route) {
				if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
					$route->enabled = false;
				}
			}
            Yii::app()->theme = 'bootstrap';
            if((isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) || SUB_DOMAIN == 'studio'){
                $std = new Studio();
                $studio_id = 4;
                $studio = $std->findByPk($studio_id);
                $this->renderPartial('robots_txt', array('studio' => $studio));
            }else{
                $studio_id = Yii::app()->common->getStudiosId(); 
                $std = new Studio();
                $studio = $std->findByPk($studio_id);
                if($studio_id && ($studio->status!=0)){
					ob_clean();	ob_flush();
                    $this->renderPartial('robots_txt', array('studio' => $studio));
                }else{
                    /*This is default for free site @author manas@muvi.com*/
                    $robots_string = "User-agent: *
Disallow: /";
					ob_clean();	ob_flush();
                    $this->renderPartial('robots_txt',array('robots_text' => $robots_string));
                }
            }
        }
        public function actionloadTestMuvi(){
            Yii::app()->theme = 'bootstrap';
			if(SUB_DOMAIN=='sdimovies'){
				$this->renderPartial('load_test_muvi');
			}else{
				$this->redirect(HTTP_ROOT);exit;
			}
        }
        
        
    //Added by Ratikanta
    public function actionFormatcurrenturl()
    {
        $newurl = '';
        $return = array('newurl');
        if(isset($_REQUEST['pageurl']) && $_REQUEST['pageurl'] != '')
        {
            $old_url = $_REQUEST['pageurl'];
            if(isset($_REQUEST['type']) && $_REQUEST['type'] != '')
            {
                $full_parts = explode('?', $old_url);
                if(count($full_parts) > 1)
                {
                    $end_parts = explode('&', $full_parts[1]);
                    if($_REQUEST['type'] == 'sort')
                    {
                        $newurl = $end_parts;
                        $sort_by = $_REQUEST['attrib'];
                        $order = $_REQUEST['order'];
                        $x = 0;
                        $final = '/?';
                        foreach($end_parts as $end_part)
                        {
                            $x++;
                            if($x > 1)
                                $final.= '&';

                            if(count(explode('sortby', $end_part)) > 1)
                            {
                                $final.= 'sortby='.$sort_by;
                            }
                            else if(count(explode('order', $end_part)) > 1)
                            {
                                $final.= 'order='.$order;
                            } 
                            else                                    
                                $final.= $end_part;
                        }
                        if(count(explode('sortby', $final)) <= 1)
                        {
                            $final.= '&';
                            $final.= 'sortby='.$sort_by;
                            $final.= '&';
                            $final.= 'order='.$order;
                        }                            

                        $last_param = substr($full_parts[0], -1);
                        $final_param = $full_parts[0];
                        if($last_param == '/')
                            $final_param = substr($final_param, 0, -1);
                        $newurl = Yii::app()->getBaseUrl(true). $final_param.$final;
                    }
                    else if($_REQUEST['type'] == 'page_size')
                    {
                        $newurl = $end_parts;
                        $page_size = $_REQUEST['page_size'];
                        $x = 0;
                        $final = '/?';
                        /* foreach($end_parts as $end_part)
                        {
                            $x++;
                            if($x > 1)
                                $final.= '&';

                            if(count(explode('page_size', $end_part)) > 1)
                            {  */
                                $final.= 'page_size='.$page_size;
                           /* }
                            else

                                $final.= $end_part;
                        }*/
                        /*if(count(explode('page_size', $final)) <= 1)
                        {
                            $final.= '&';
                            $final.= 'page_size='.$page_size;
                        }  */                            

                        $last_param = substr($full_parts[0], -1);
                        $final_param = $full_parts[0];
                        if($last_param == '/')
                            $final_param = substr($final_param, 0, -1);
                        $newurl = Yii::app()->getBaseUrl(true). $final_param.$final;
                    }                        
                }
                else
                {
                    if($_REQUEST['type'] == 'sort')
                    {
                        $sort_by = $_REQUEST['attrib'];
                        $order = $_REQUEST['order'];                            
                        $newurl = Yii::app()->getBaseUrl(true). $full_parts[0].'/?sortby='.$sort_by.'&order='.$order;
                    }
                    else if($_REQUEST['type'] == 'page_size')
                    {
                        $page_size = $_REQUEST['page_size'];                         
                        $newurl = Yii::app()->getBaseUrl(true). $full_parts[0].'/?page_size='.$page_size;
                    }                          
                }
            }
        }
        $return = array('newurl' => $newurl);
        echo json_encode($return);
    }
    
    public function actionCreatePostPermalink()
    {
        $newurl = '';
        if(isset($_REQUEST['post_title']) && $_REQUEST['post_title'] != '')
        {
            $post_title = $_REQUEST['post_title'];
            $post_id = isset($_REQUEST['post_id'])?$_REQUEST['post_id']:0;
            $newurl = Yii::app()->common->formatPostPermalink($post_title, $post_id);
        }
        $return = array('newurl' => $newurl);
        echo json_encode($return);
    }  
    
    public function actionCheckpostpermalink() {
        $exists = '';
        if(isset($_REQUEST['permalink']) && $_REQUEST['permalink'] != '')
        {
            $studio_id = Yii::app()->common->getStudiosId();
            $permalink = $_REQUEST['permalink'];
            $post_id = isset($_REQUEST['post_id'])?$_REQUEST['post_id']:0;
            $data = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('blog_posts')
                    ->where('id != :post_id and studio_id = :studio_id and permalink = :permalink', array(':post_id'=>$post_id, ':studio_id'=>$studio_id, ':permalink'=>$permalink))
                    ->order('id desc')
                    ->queryAll();
            if(count($data) == 0)
                $exists = 0;           
            else
                $exists = 1;
        }
        $return = array('exists' => $exists);
        echo json_encode($return);        
    }     
/**
 * @method public facebookLogin() Description
 */
	function actionFacebookLogin(){
		Yii::app()->theme = 'bootstrap';
		$this->render('fblogin');
	}
    public function actionInitiateLog() {
        $studio_id = Yii::app()->common->getStudiosId();        
        $studio = new Studio();
        $studio = $studio->findByPk($studio_id);
        $page ='';
        if (SUB_DOMAIN != 'studio' && SUB_DOMAIN != 'devstudio' && $studio->domain != '') {
            $temp_page =  base64_decode($_REQUEST['pid']);
            $page = preg_replace('/[0-9]+/', '', $temp_page);
            $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
            //if(isset($studio_id) && $studio_id == 52){
                $browser = Yii::app()->common->getBrowser();
                $source = '';
                if (isset(Yii::app()->request->cookies['SITE_REFERRER']) && trim(Yii::app()->request->cookies['SITE_REFERRER'])) {
                    $source = Yii::app()->request->cookies['SITE_REFERRER'];
                }
                $ip_address = CHttpRequest::getUserHostAddress();
                if(isset(Yii::app()->session['log_id']) && Yii::app()->session['log_id']){
                    if(isset(Yii::app()->session['page']) && Yii::app()->session['page'] != $page){
                       $log_id = Yii::app()->session['log_id'];
                       $log = StudioVisitor::model()->findByPk($log_id);
                       
                       $new_log = new StudioVisitor();

                       $new_log->studio_id = $studio_id;
                       $new_log->user_id = $user_id;
                       $new_log->ip = $ip_address;
                       $new_log->page = $page;
                       $new_log->browser = $browser['name'];
                       $new_log->os = $browser['platform'];
                       $new_log->city = $log->city;
                        $new_log->region = $log->region;
                        $new_log->country = $log->country;
                        $new_log->country_code = $log->country_code;
                        $new_log->continent_code = $log->continent_code;
                        $new_log->latitude = $log->latitude;
                        $new_log->longitude = $log->longitude;
                       $new_log->visit_in = date('h:i:s');
                       $new_log->visit_date = date('Y-m-d');
                       $new_log->source = $source;
                       $new_log->save();
                       $new_log_id = $new_log->id;
                       Yii::app()->session['log_id'] = $new_log_id;
                       Yii::app()->session['page'] = $page;
                    }
                    
                }else{
                    $visitor_loc = Yii::app()->common->getVisitorLocation();
                    $city = @$visitor_loc['city'];
                    $region = @$visitor_loc['region'];
                    $country = @$visitor_loc['country_name'];
                    $country_code = @$visitor_loc['country'];
                    $continent_code = @$visitor_loc['continent_code'];
                    $latitude = @$visitor_loc['latitude'];
                    $longitude = @$visitor_loc['longitude'];
                    
                   $log = new StudioVisitor();
                   $log->studio_id = $studio_id;
                   $log->user_id = $user_id;
                   $log->ip = $ip_address;
                   $log->page = $page;
                   $log->browser = $browser['name'];
                   $log->os = $browser['platform'];
                    $log->city = $city;
                    $log->region = $region;
                    $log->country = $country;
                    $log->country_code = $country_code;
                    $log->continent_code = $continent_code;
                    $log->latitude = $latitude;
                    $log->longitude = $longitude;
                   $log->visit_in = date('h:i:s');
                   $log->visit_date = date('Y-m-d');
                   $log->source = $source;
                   $log->save();
                   $log_id = $log->id;
                   Yii::app()->session['log_id'] = $log_id;
                   Yii::app()->session['page'] = $page;
                }
            //}
        }
    }
        
        public function actionLogVisitor()
        {
            $ip_address = CHttpRequest::getUserHostAddress();
            $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
            $studio_id = Yii::app()->common->getStudiosId();
            $browser = Yii::app()->common->getBrowser();
            $temp_page =  base64_decode($_REQUEST['pid']);
            $page = preg_replace('/[0-9]+/', '', $temp_page);
            //$sql = "SELECT id,total_time FROM studio_visitors_log WHERE ip='".$ip_address."' AND user_id=".$user_id." AND studio_id=".$studio_id." AND page='".$page."' AND browser='".$browser['name']."' AND os='".$browser['platform']."' ORDER BY id DESC LIMIT 1";
            $sql = "SELECT id,total_time FROM studio_visitors_log WHERE ip='".$ip_address."' AND user_id=".$user_id." AND studio_id=".$studio_id." AND page='".$page."' ORDER BY id DESC LIMIT 1";
            $data = Yii::app()->db->createCommand($sql)->queryRow();
            if(isset($data) && !empty($data)){
                $total_time = $data['total_time']+$_REQUEST['timeDiff'];
                $new_sql = "UPDATE studio_visitors_log SET total_time='".$total_time."' WHERE id = ".$data['id'];
                $update = Yii::app()->db->createCommand($new_sql)->execute();
                return $update;
            }else{
                return 0;
            }
            
        }
        
        
   function actionSavetemplatepreview() {
           $ip_address = Yii::app()->getRequest()->getUserHostAddress();
              
            $studio_id = Yii::app()->common->getStudiosId();
            $template_name = $_REQUEST['template_name']; 
            $template_color = $_REQUEST['template_color'];
            $in_preview_theme = $_REQUEST['in_preview_theme'];
           
            
                $std = new Studio();
                $std = $std->findByPk($studio_id);
            $old_parent_theme = $std->parent_theme;
            $old_theme = $std->theme;
               
            $theme = $old_theme . '_preview'; 
            $sourcePath = 'themes/' . $theme . '/';
                    
            if (@file_exists(ROOT_DIR . '/' . $sourcePath))
                Yii::app()->common->rrmdir(ROOT_DIR . '/' . $sourcePath);

            @mkdir($sourcePath, 0777, true);
                    
            chmod($sourcePath, 0777);

            $source = ROOT_DIR . '/sdkThemes/' . $template_name . '/';
            $ret = Yii::app()->common->recurse_copy($source, $sourcePath);



            $cookie = new CHttpCookie('in_preview_theme', 1);
            $cookie->expire = time()+60*5;
            Yii::app()->request->cookies['in_preview_theme'] = $cookie; // will send the cookie                    

            $std->in_preview_theme = 1;
            $std->preview_theme = $theme;
            $std->preview_parent_theme = $template_name;
            $std->preview_theme_color = $template_color;
            $std->preview_ip = $ip_address;
            $std->save();

            $preview_template = new Previewtemplatehistory();
            $preview_template->studio_id = $studio_id;
            $preview_template->user_id = Yii::app()->user->id;
            $preview_template->preview_ip = $ip_address;
            $preview_template->is_preview = 1;
            $preview_template->template_name = $template_name;
            $preview_template->template_color = $template_color;
            $preview_template->current_template = $old_parent_theme;
            $preview_template->admin_domain = $_REQUEST['admin_domain'];
            $preview_template->template_status = $_REQUEST['template_status'];
            $preview_template->save();
            return $preview_template->id;



            exit();

//                    $url = $this->createUrl('/template?preview=1');
//                    $this->redirect($url);
//                    exit();
           
      
        }
        function actionApplytemplatepreview() {
            $studio_id = Yii::app()->common->getStudiosId();
            $ip_address = Yii::app()->getRequest()->getUserHostAddress();
      $template_data = Previewtemplatehistory::model()->findByAttributes(array('preview_ip' =>$ip_address, 'studio_id' => $studio_id, 'is_preview'=> 1), array('order' => 'id DESC'));
          if($template_data) {
                $temlpate_name = $template_data->template_name;
                $template_color = $template_data->template_color;
                $in_preview_theme = $_REQUEST['in_preview_theme'];
                $current_template = Template::model()->findByAttributes(array('code'=>$template_name),array('select'=>'id'));
                $current_template_id = $current_template->id;
                if ($studio_id > 0) {
                    $std = new Studio();
                    $std = $std->findByPk($studio_id);
                    $old_parent_theme = $std->parent_theme;
                    $old_theme = $std->theme;


                    $temps = new Template;
                    $temps = $temps->findAllByAttributes(array('code' => $old_parent_theme), array('order' => 'id ASC'));
                    if (count($temps) > 0)
                        $template_id = $temps[0]->id;


                    if ($temlpate_name != $old_parent_theme) {
                        if (Yii::app()->user->id != SAMPLE_DATA_STUDIO) {
                            //Deleting the banners

                            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                            $bucket = $bucketInfo['bucket_name'];
                            $s3dir = $unsignedFolderPathForVideo . "public/system/studio_banner/" . $studio_id;

                            $bn = new StudioBanner;
                            $banners = $bn->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
                            if (count($banners) > 0) {
                                foreach ($banners as $banner) {
                                    $banner_id = $banner->id;
                                        $result = $s3->deleteObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => $s3dir . "/original/" . $banner->image_name
                                        ));

                                        $result = $s3->deleteObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => $s3dir . "/studio_thumb/" . $banner->image_name
                                        ));

                                        $res = $banner->delete();
                                    }
                                }

                            //Deleting banner texts
                            $bnr = new BannerText();
                            $banner_texts = $bnr->findAllByAttributes(array('studio_id' => $studio_id));
                            if (count($banner_texts) > 0) {
                                foreach ($banner_texts as $banner_text) {
                                        $banner_text->delete();
                                    }
                                }
                            //Updating template id of featured sections
                            $feat = new FeaturedSection;
                            $attr= array('template_id'=>$current_template_id);
                            $condition = "studio_id=:studio_id";
                            $params = array(':studio_id'=>$studio_id);
                            $feat = $feat->updateAll($attr,$condition,$params);
                            //Deleting featured sections
                            /*$section = new FeaturedSection;
                            $sections = $section->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id_seq ASC'));
                            if (count($sections) > 0) {
                                foreach ($sections as $section) {
                                        $featureds = Yii::app()->common->getFeaturedContents($section->id);
                                        if (count($featureds) > 0) {
                                            foreach ($featureds as $featured) {
                                                $featured->delete();
                                            }
                                        }
                                        $section->delete();
                                    }
                                }*/

                            //Deleting logo and favicon
                            /*
                              $favicon_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $std->theme;
                              if ($std->logo_file && is_file($favicon_path . '/' . $std->logo_file)) {
                              @unlink($favicon_path . '/' . $std->logo_file);
                            }

                              if ($std->favicon_name && is_file($favicon_path . '/' . $std->favicon_name)) {
                              @unlink($favicon_path . '/' . $std->favicon_name);
                        }
                             * 
                             */
                        }

                        //Creating the themebackup folder if not exists
                        $bakup_path = ROOT_DIR . '/themebackups';
                        if (!file_exists($bakup_path)) {
                            $res = @mkdir($bakup_path, 0777, true);
                            chmod($bakup_path, 0777);
                        }

                        //Collecting the source theme folder
                        $sourcePath = 'themes/' . $old_theme . '/';
                        if (file_exists($sourcePath)) {
                            //Setting the target ZIP file
                            $destPath = 'themebackups/' . $old_theme . '_' . $old_parent_theme . '_' . date("Y-m-d-H-i-s") . '.zip';

                            //Creating ZIP
                            $zip = Yii::app()->zip;
                            $res = $zip->zipDir($sourcePath, $destPath);

                            $sourcePath = ROOT_DIR . '/themes/' . $old_theme . '/';
                            Yii::app()->common->rrmdir($sourcePath);
                        }
                        //Creating the theme folder to ignore duplicacies
                        @mkdir($sourcePath, 0777, true);
                        chmod($sourcePath, 0777);
                        $source = ROOT_DIR . '/sdkThemes/' . $temlpate_name . '/';
                        $ret = Yii::app()->common->recurse_copy($source, $sourcePath);
                    }

                    $std->in_preview_theme = 0;
                    $std->parent_theme = $temlpate_name;
                    $std->default_color = $template_color;
                    $std->save();

                    $preview_template = new Previewtemplatehistory();
                    $previews = $preview_template->findAllByAttributes(array('studio_id' => $studio_id, 'preview_ip' => $ip_address), array('order' => 'id DESC'));
                    foreach ($previews as $preview) {
                        $preview->is_preview = 0;
                        $preview->save();
                    }

                    unset(Yii::app()->request->cookies['in_preview_theme']);

                    // Yii::app()->user->setFlash('success', 'Template updated.');
                    exit;
                
                }

                $url = $this->createUrl('/template');
                $this->redirect($url);
                exit;
          }
          else {
                echo 'already applied';
            }
                    
                    
        }
    
        public function actionReverttemplatepreview() // this method used for update preview history
        {
              $studio_id = Yii::app()->common->getStudiosId();
              $ip_address = Yii::app()->getRequest()->getUserHostAddress();
              $preview_template = new Previewtemplatehistory();
                    $previews = $preview_template->findAllByAttributes(array('studio_id' => $studio_id, 'preview_ip' => $ip_address), array('order' => 'id DESC'));
                    foreach ($previews as $preview){
                        $preview->is_preview = 0;
                        $preview->save();
                    }
                    
                    exit;
        }        
        /*public function actionAdminlogin(){
            //echo $this->is_master_account;
            //$studio_domain = Yii::app()->common->getWebsiteSubdomain();
            //$std = Studio::model();
            //$studio = $std->findAllByAttributes(array('domain' => $studio_domain));
            $logo = "images/sony_DADC.png";//Yii::app()->common->getLogoFavPath($studio[0]['id']);
            Yii::app()->theme = 'admin';
            $this->layout = 'customerlogin';            
            $this->render('adminlogin',array('logo'=>$logo));
        }*/
        public function actionmclogin(){
            if (isset($_POST['LoginForm'])) {
                $model=new LoginForm;
                $model->attributes=$_POST['LoginForm'];

                if ($model->validate() && $model->login()){
                    if(Yii::app()->user->signup_step == 0)
                        $array = array("login" => "success");
                    else
                        $array = array("login" => "step_".Yii::app()->user->signup_step);
                    //Yii::app()->user->setFlash("success", "Successfully logged in.");
                    $json = json_encode($array);
                    echo $json;
                    Yii::app()->end();
                }
                else{
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
            }
        }
    public function actionGetmoviecontentdetails()
    {
        $movie_id = $_REQUEST['movie_id'];
        $content = Yii::app()->general->getContentData($movie_id);
        echo json_encode($content);
    }
    
    public function actionpackageappprice(){
        $code = $_REQUEST['code'];
        $app_price = Yii::app()->general->getAppPrice($code);
        echo json_encode(array('app_price' => $app_price));
    }
    
    public function actionGetStorageofStudios() {
        $studio_id = $_REQUEST['studio'];
        $data = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
        print "Size in GB: ".$data;
    }
    public function actiongetNewSignedUrlForPlayer() {
        $browserName = '';
        if ($_REQUEST['wiki_data'] == "") {

            $bucketInfo = Yii::app()->common->getBucketInfo('', Yii::app()->common->getStudioId());
            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $_REQUEST['video_url']));
            if(isset($_REQUEST['browser_name']) && $_REQUEST['browser_name'] == 'Safari'){
                $browserName = $_REQUEST['browser_name'];
            }
            echo $video_url_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1,$browserName);
            exit;
        } else {
            $rel_path = str_replace(" ", "%20", str_replace(CDN_HTTP."muviassetsdev.s3.amazonaws.com/", "", $_REQUEST['video_url']));
            echo $video_url_path = $this->getsecure_url($rel_path);
            exit;
        }
    }  
    
    public function actioncomingsoon(){
        $this->pageTitle = $this->studio->name;
        $this->pageKeywords = "coming soon";
        $this->pageDescription = "coming soon";
        $studio_id = $this->studio->id;
        
        $settings = ComingsoonSetting::getComingSoonSetting($studio_id);
        if(isset($settings) && count($settings) > 0 && $settings->status == 1){
            $logo_path = $this->getComingSoonPath($studio_id, $settings->logo_file);
            $background_path = $this->getComingSoonPath($studio_id, $settings->background_file);
            $comingsoon_text = $settings->comingsoon_text;
            if($settings->option_enabled == 1)
                $view = 'comingsoon';
            else
                $view = 'passcode';
            $this->render($view, array('logo_path' => $logo_path, 'background_path' => $background_path, 'comingsoon_text' => $comingsoon_text));
        }
    }
    
    public function actioncomingsignup(){
        $studio_id = $this->studio->id;
        $this->layout = false;
        $response = 'error';
        $msg = 'Please enter a correct email'; 
        if(isset($_REQUEST) && count($_REQUEST) > 0 && strlen($_REQUEST['email']) > 0 ){
            $email = trim($_REQUEST['email']);
            $dup = ComingsoonSignup::model()->findByAttributes(
                array('studio_id' => $studio_id, 'email' => $email) 
            ); 
            if(isset($dup) && count($dup) > 0){
                $response = 'error';
                $msg = 'Email already registered.';                
            }else{
                $visitor_loc = Yii::app()->common->getVisitorLocation();                
                $signup = new ComingsoonSignup();
                $signup->studio_id = $studio_id;
                $signup->email = $email;
                $signup->date_subscribed = new CDbExpression("NOW()");
                $signup->from_location = json_encode($visitor_loc);
                $signup->save();
                $response = 'success';
                $msg = 'Your email has been registered successfully.';
                Yii::app()->user->setFlash('success', $msg);
            }
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);        
    }
    
    public function actioncomingpasscode(){
        $this->layout = false;
        $studio_id = $this->studio->id;
        $response = 'error';
        $msg = 'Please enter a correct passcode'; 
        if(isset($_REQUEST) && count($_REQUEST) > 0 && strlen($_REQUEST['passcode']) > 0 ){
            $passcode = trim($_REQUEST['passcode']);
            $rec = ComingsoonSetting::model()->findByAttributes(
                array('studio_id' => $studio_id, 'passcode' => $passcode, 'status' => '1', 'option_enabled' => '2') 
            ); 
            if(isset($rec) && count($rec) > 0){
                $_SESSION['has_valid_passcode'] = 1;
				Yii::app()->session['has_valid_passcode'] = 1;
                $response = 'success';
                $msg = 'You are logged in now.';
            }
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);        
    }      
    
    function actionLocationTest() {
        $limit = (isset($_REQUEST['limit'])) ? $_REQUEST['limit'] : 10000;
        $offset = (isset($_REQUEST['offset'])) ? $_REQUEST['offset'] : 0;
        $sql = "SELECT id,studio_id, location FROM studio_visitors_log WHERE 1 LIMIT {$offset}, {$limit}";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        
        foreach ($res as $value) {
            if (trim($value['location'])) {
                $string = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $value['location']);
                $full_address = unserialize($string);
                $address = json_decode(json_encode($full_address), TRUE);
                $signup_location_address = $address['\0*\0_data']['\0CMap\0_d'];
                if (!count($signup_location_address) || empty($signup_location_address)) {
                    $signup_location_address = $address['*_data']['CMap_d'];
                    if(!count($signup_location_address) || empty($signup_location_address)){
                        $signup_location_address = $address[' * _data'][' CMap _d'];
                    }
                }
                $signup_location = $signup_location_address;
                $id = $value['id'];

                if (trim($signup_location['geoplugin_continentCode'])) {
                    $buff_log = StudioVisitor::model()->findByPk($id);
                    $buff_log->city = @$signup_location['geoplugin_city'];
                    $buff_log->region = @$signup_location['geoplugin_region'];
                    $buff_log->country = @$signup_location['geoplugin_country'];
                    $buff_log->country_code = @$signup_location['geoplugin_countryCode'];
                    $buff_log->continent_code = @$signup_location['geoplugin_continentCode'];
                    $buff_log->latitude = @$signup_location['geoplugin_latitude'];
                    $buff_log->longitude = @$signup_location['geoplugin_longitude'];
                    $buff_log->save();
                }
            }
        }
        
        print 'success';exit;
    }
    function getProductlist($studio_id){
        require_once $_SERVER["DOCUMENT_ROOT"] .'/protected/components/pagination.php';
        //Pagination codes
        $items_per_page = 24;//$this->template->items_per_page;
        $page_size = $limit = $items_per_page;
        $offset = 0;
        if (isset($_REQUEST['p']) && (is_numeric($_REQUEST['p']))) {
            $_REQUEST['p'] = floor($_REQUEST['p']);
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
        } else {
            $page_number = 1;
        }
        $standaloneproduct = PGProduct::getProductList(0, $studio_id,'',$limit,$offset);
        $item_count = $standaloneproduct['count'];
        unset($standaloneproduct['count']);
		$default_currency_id = $this->studio->default_currency_id;
        foreach ($standaloneproduct AS $key => $orderval) {
			$pgProductList[$key] = $orderval->attributes;
            $tempprice = Yii::app()->common->getPGPrices($orderval->id, $default_currency_id);
            if(!empty($tempprice)){
                $pgProductList[$key]['sale_price'] = $tempprice['price'];
                $pgProductList[$key]['currency_id'] = $tempprice['currency_id'];
            }
        }
        $page_url = $url = Yii::app()->getBaseUrl(true);
        $pg = new bootPagination();
        $pg->pagenumber = $page_number;
        $pg->pagesize = $page_size;
        $pg->totalrecords = $item_count;
        $pg->showfirst = true;
        $pg->showlast = true;
        $pg->paginationcss = "pagination-normal";
        $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
        $pg->defaultUrl = $page_url;
        if (strpos($page_url, '?') > -1)
            $pg->paginationUrl = $page_url . "&p=[p]";
        else {
            $pg->paginationUrl = $page_url."?p=[p]";
        }
        $pagination = $pg->process();
        $data['product'] = $pgProductList;
        $data['pagination'] = $pagination;
        return $data;
    }
    public function actionSetLanguageCookie(){
        if(isset($_REQUEST['lang_code'])){
            $language_code = $_REQUEST['lang_code'];
            if($language_code ==""){
                $language_code = 'en';
            }
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//", $domain);
            $domain = $domain[1]; 
            if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                setcookie('Language', '', time() - 3600, '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
                setcookie('Language', $language_code, time() + (60 * 60 * 24 * 90), '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
                echo "Cookie changed Successfully ";exit;
            }
        }else{
            $url = $this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function actionUpdateLoginHistory(){
        if($_REQUEST['login_count']){
            if(isset(Yii::app()->user->id)){
                $user_id = Yii::app()->user->id;
                $studio_id = Yii::app()->common->getStudiosId();
                $login_history_id = Yii::app()->session['login_history_id'];
                if(!$login_history_id){
                    $login_history_cookie_name = "login_history_id_".$studio_id."_".$user_id;
                    Yii::app()->session['login_history_id'] = $login_history_id = $_COOKIE[$login_history_cookie_name];
                }
                if($login_history_id){
                    $login_history    = LoginHistory::model()->findByPk($login_history_id);
                    $login_history->last_login_check = date('Y-m-d H:i:s');
                    $login_history->save();
                    echo "updated";exit;
                }else{
                    echo "Not updated.";exit; 
                }
            }else{
                echo "Not updated.";exit;
            }
        }
    }
    
    /*
     * modified :   Biswajit 
     * email    :   biswajitdas@muvi.com
     * reason   :   Audio data for playing :  
     * functionality : actionGetAudioFile
     * date     :   07-02-2017
     * purpose : get Audio url & information
     */

    public function actionGetAudioFile() {
        $audioid = $_REQUEST['audio_id'];
        $res = array();
        $resultData = array();
        if(@$_REQUEST['studio_id']){
            $studio_id = $_REQUEST['studio_id'];
        } else{
            $studio_id = $this->studio->id;
        }
        if ($audioid) {
            //$base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
            //$sql = "select f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id from films f,movie_streams ms where (f.id=ms.movie_id) and ((ms.movie_id=" . $audioid . ") OR (ms.id=" . $audioid . ")) ";
            //$data = Yii::app()->db->createCommand($sql)->queryAll();
            $command1 = Yii::app()->db->createCommand()
                ->select('f.name,f.permalink,ms.episode_title,ms.id,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id')
                ->from('films f,movie_streams ms')
                ->where('f.id=ms.movie_id AND ((ms.movie_id=' . $audioid . ') OR (ms.id=' . $audioid . '))');
            $data = $command1->QueryAll();
            $k = 0;
            foreach ($data as $audiodata) {
                $movie_id = $audiodata['id'];
                $resultData[$k]['url'] = $this->getAudioUrl($audiodata['id']);
                $resultData[$k]['is_episode'] = $audiodata['is_episode'];

                if ($resultData[$k]['is_episode'] == '0') {
                    $type = 'films';
                    $resultData[$k]['title'] = $audiodata['name'];
                    $audio_id = $audioid;
                    $uniq_id = $audiodata['uniq_id'];
                } else {
                    $type = 'moviestream';
                    $resultData[$k]['title'] = $audiodata['episode_title'];
                    $audio_id = $audiodata['movie_id'];
                    $uniq_id = $audiodata['embed_id'];
                }
            }
            $resultData[$k]['is_favourite'] = Yii::app()->common->isContentFav($studio_id, $user_id, $audio_id, $resultData['is_episode']);
            if(@$_REQUEST['is_embed']){
                $resultData[$k]['audio_poster'] = $this->getPoster($audioid, $type, "original",$studio_id,'',1);  
            }else{
                $resultData[$k]['audio_poster'] = $this->getPoster($audioid, $type);
            }
            $resultData[$k]['content_id'] = $movie_id;
            $resultData[$k]['uniq_id'] = $uniq_id;
            $resultData[$k]['movie_id'] = $audiodata['movie_id'];
            $resultData[$k]['permalink'] = $audiodata['permalink'];
            $cast_details =$this->getCasts($audio_id,1,false,$studio_id);
            $celeb_name = array();
            if (!empty($cast_details)) {
                foreach ($cast_details as $casts) {
                    if (trim($casts['celeb_name']) != "") {
                        $celeb_name[] = $casts['celeb_name'];
                    }
                }
            }
            $casts = implode(',', $celeb_name);
            $resultData[$k]['cast'] = $casts;
            if ($resultData) {
                $res['message'] = 'success';
                $res['data'] = $resultData;
            } else {
                $res['message'] = 'error';
                $res['data'] = $resultData;
                $res['cast'] = $casts;
            }
            echo json_encode($res);
            exit;
        }
    }
    public function getAudioUrl($stream_id = 0) {
        if($stream_id){
            $sql = Yii::app()->db->createCommand()
                    ->select('id,studio_id,full_movie')
                    ->from('movie_streams')
                    ->where('id ='.$stream_id);
            $data = $sql->queryAll();
            $bucketInfo = Yii::app()->common->getBucketInfo('', $data[0]['studio_id']);
            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
            $filePth = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $data[0]['id'] . '/' . $data[0]['full_movie'];
            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
            $url = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,1,"",60000);
            return $url;
        }
    }

    public function actionGetAllAudioFile() {
		$audioid = $_REQUEST['audio_id'];
		$res = array();
		$resultData = array();
		$studio_id = $this->studio->id;
		if ($audioid) {
			$base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
			$sort_order = Yii::app()->custom->episodesortordermultipart();
			$command1 = Yii::app()->db->createCommand()
					->select('f.name,f.permalink,ms.id,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id')
					->from('films f,movie_streams ms')
					->where('f.id=ms.movie_id AND ms.movie_id=' . $audioid . ' AND ms.is_episode = 1 AND ms.is_converted = 1 ORDER BY episode_number '.$sort_order);
			$data = $command1->QueryAll();
			$content_details = array();
			$k = 0;
			foreach ($data as $audiodata) {
				$type       = 'moviestream';
				$audio_id   = $audiodata['id'];
				$is_episode = $audiodata['is_episode'];
				$uniq_id    = $audiodata['embed_id'];
				$audioUrl =$base_cloud_url.$audiodata['full_movie'];
				$poster = $this->getPoster($audio_id, $type);
				$cast_details = $this->getCasts($audioid);
				$celeb_name = array();
				if (!empty($cast_details)) {
					foreach ($cast_details as $casts) {
						if (trim($casts['celeb_name']) != "") {
							$celeb_name[] = $casts['celeb_name'];
						}
					}
				}
				$is_favourite = Yii::app()->common->isContentFav($studio_id,$user_id,$audio_id,$is_episode);
				$casts = implode(',', $celeb_name);
				$content_details[$k]['content_id'] = $audio_id;
				$content_details[$k]['movie_id'] = $audiodata['movie_id'];
				$content_details[$k]['is_episode'] = $is_episode;
				$content_details[$k]['is_favourite'] = $is_favourite;
				$content_details[$k]['url'] = $audioUrl;
				$content_details[$k]['audio_poster'] = $poster;
				$content_details[$k]['title'] = $audiodata['episode_title'];
				$content_details[$k]['permalink'] =  $audiodata['permalink'];
				$content_details[$k]['cast'] =  $casts;
				$content_details[$k]['uniq_id'] = $uniq_id;
				$k++;
			}
		}
		$res['message'] = 'success';
		$res['data'] = $content_details;
		echo json_encode($res);
		exit;
	}
    public function actionTestAPI(){
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
        $response_url = 'https://www.demo.muvi.com/site/HandleClickhereResponse';
        $mobile_number = "959453044694";
        $chk = new clickHereApi();        
        $chk = $chk->doRegistration($response_url, $mobile_number);
        var_dump($chk);
        exit();
    }
    
    public function actionHandleClickhereResponse(){
        $res = file_get_contents('php://input');
        print_r($res);        
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/clickhere.log', "a+");
        fwrite($fp, " \n\n\t Response ".date('m-d-Y H:i:s') . "\n\t " . serialize($res));
        fclose($fp);        
    }
}
