<div class="Block">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-docs icon left-icon "></em>
        </div>
        <h4>Home Page Sections</h4>                      
    </div>
    <hr>
    <div class="Block-Body">
        <div class="row m-b-20">
            <div class="col-sm-12">
                <form action="<?php echo $this->siteurl; ?>/apps/updatehomepagesection" method="post">
                    <div class="radio m-b-20">
                        <label>
                            <input name="home_layout" class="home_layout" value="1" type="radio" <?php echo $allcat; ?> />
                            <i class="input-helper"></i>All Categories
                        </label>
                    </div>
                    <div class="radio m-b-20">
                        <label>
                            <input name="home_layout" class="home_layout" value="2" type="radio" <?php echo $featsection; ?> />
                            <i class="input-helper"></i>Featured Section
                        </label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="Block" id="featured_block" style="<?php echo $fstyle; ?>">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-docs icon left-icon "></em>
        </div>
        <h4>Featured Sections</h4>                     
    </div>
    <hr>
    <div class="Block-Body">
        <div class="row m-b-40">       
            <div class="col-sm-12">
                <button class="btn btn-primary m-b-20" id="nextbtn" onclick="addHomepageSection(); return false;"> Add Featured Section</button>
                <a class="btn m-b-20" href="<?php echo $this->siteurl; ?>/apps/syncwithweb">Sync With Website</a> 
                <form class="form-horizontal" method="post">
                    <input type="hidden" name="sec_id" id="sec_id" />
                    <div class="row m-b-40" id="sort_section">
                        <?php if(count($sections)){
                            foreach($sections as $section){
                                $content_type = $section->content_type;
                                $section_id   = $section->id;
                                if ($section->parent_id > 0) {
                                    $section_id = $section->parent_id;
                                }
                                ?>
                                <div class="col-sm-12" id="sort_<?php echo $section_id; ?>" >
                                    <div class="Collapse-Block">
                                        <div class="Block-Header has-right-icon">
                                            <div class="icon-OuterArea--rectangular">
                                                <a href="javascript:void(0);" id="collap-<?php echo $section_id; ?>" class="feat_blocks"><em class="icon-arrow-up icon left-icon"></em></a>
                                            </div>
                                            <?php if (($this->language_id == $section->language_id) && ($section->parent_id == 0)) { ?>
                                                <div class="icon-OuterArea--rectangular right">
                                                    <a href="javascript:void(0);" onclick="deleteSection(<?php echo $section_id; ?>)"><em class="icon-trash icon" title="Remove the Section"></em></a>
                                                </div>
                                            <?php } ?>
                                            <div class="section_settings form-horizontal">
                                                <div class="editsection hide form-group">
                                                    <div class="col-sm-4">
                                                        <div class="fg-line">
                                                            <input type="text" class="sectionname form-control" value="<?php echo $section->title; ?>" id="sectionname_<?php echo $section_id; ?>" name="sectionname_<?php echo $section_id; ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="javascript:void(0);"  onclick="updateSection(<?php echo $section_id; ?>);" title="Update"><i class="h3 green icon-check"></i></a>
                                                    </div>
                                                </div> 
                                            </div>
                                            <h4 class="showsectionname"><?php echo $section->title;  ?> </h4>
                                        </div>
                                        <div class="Collapse-Content row" id="section_data_<?php echo $section_id; ?>">
                                            <!--Add New Content-->
                                            <div class="col-sm-12 m-t-20 m-b-20">
                                                <a href="javascript:void(0);" class="btn btn-primary m-t-10 p-l-40" onclick="addPopularMovie(<?php echo $section_id ?>, <?php echo $content_type; ?>); return false;">
                                                    Add Content
                                                </a>
                                            </div>
                                            <ul class="row featured_contents" id="sortables_<?php echo $section_id; ?>" >
                                                <?php
                                                $featureds = $section->contents;
                                                $language_id = $this->language_id;
                                                foreach ($featureds as $featured) {
                                                    $is_episode = $featured->is_episode;
                                                    if ($is_episode == 1) {
                                                        $stream_id = $featured->movie_id;
                                                        $langcontent = Yii::app()->custom->getTranslatedContent($stream_id, $is_episode, $language_id);
                                                        $movie_stream = new movieStreams;
                                                        $movie_stream = $movie_stream->findByPk($stream_id);
                                                        if (array_key_exists($stream_id, $langcontent['episode'])) {
                                                            $movie_stream->episode_title = $langcontent['episode'][$stream_id]->episode_title;
                                                        }
                                                        $movie_id = $movie_stream->movie_id;

                                                        $film = new Film();
                                                        $film = $film->findByPk($movie_id);
                                                        if (array_key_exists($movie_id, $langcontent['film'])) {
                                                            $film->name = $langcontent['film'][$movie_id]->name;
                                                        }
                                                        $cont_name = $film->name . ' ';

                                                        $cont_name .= ($movie_stream->episode_title != '') ? $movie_stream->episode_title : "SEASON " . $movie_stream->series_number . ", EPISODE " . $movie_stream->episode_number;
                                                        $poster = $this->getPoster($stream_id, 'moviestream', 'episode');
                                                    } else if ($is_episode == 2) {
                                                        $movie_id = $content_id = $featured->movie_id;
                                                        $poster = PGProduct::getpgImage($content_id, 'standard');
                                                        $content = Yii::app()->custom->getProductDetails($content_id, array('name'));
                                                        $cont_name = @$content[0]['name'];
                                                    } else {
                                                        $movie_id = $featured->movie_id;
                                                        $langcontent = Yii::app()->custom->getTranslatedContent($movie_id, $is_episode, $language_id);
                                                        $film = new Film();
                                                        $film = $film->findByPk($movie_id);
                                                        if (array_key_exists($movie_id, $langcontent['film'])) {
                                                            $film->name = $langcontent['film'][$movie_id]->name;
                                                        }
                                                        $cont_name = $film->name;
                                                        $ctype = $film->content_types_id;
                                                        if ($ctype == 2)
                                                            $poster = $this->getPoster($film->id, 'films', 'episode');
                                                        else if ($ctype == 4)
                                                            $poster = $this->getPoster($film->id, 'films', 'original');
                                                        else
                                                            $poster = $this->getPoster($film->id, 'films', 'standard');
                                                    }
                                                    if (false === file_get_contents($poster)) {
                                                        $poster = "/img/No-Image-Horizontal.png";
                                                    }
                                                    ?>
                                                    <li class="col-sm-3 drag-cursor" id="sort_<?php echo $section_id; ?>_<?php echo $featured->id ?>" >
                                                        <div class="Preview-Block"  >		
                                                            <div class="thumbnail m-b-0"  >
                                                                <div class="relative m-b-10 drag-cursor">
                                                                    <img src="<?php echo $poster ?>" alt="">
                                                                    <div class="overlay-bottom overlay-white text-right">
                                                                        <div class="overlay-Text">
                                                                            <div>
                                                                                <a href="javascript:void(0);"  onclick="deleteFeatured(<?php echo $section_id; ?>,<?php echo $featured->id ?>);" class="btn btn-danger icon-with-fixed-width">
                                                                                    <em class="icon-trash"></em>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <h5><?php echo (strlen($cont_name) > 50) ? strip_tags(substr($cont_name, 0, 50)) . '...' : $cont_name; ?></h5>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        } ?>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>        
