<?php
require_once 'Paypal/paypal_pro.inc.php';

if ((strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'payment/authenticatecard')
    || (strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'payment/purchasesubscribe')) {
    require 'aws/aws-autoloader.php';
}
require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

require_once 'FirstDataApi/FirstData.php';

class PaymentController extends Controller {

    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
             $this->checkPermission();
        }
        return true;
    }

    function actionManagePayment() {
        $this->breadcrumbs = array('Billing', 'Payment Information');
        $this->headerinfo = "Payment Information";
        $this->pageTitle = "Muvi | Payment Information";
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
        $studio = $this->studio;

        $cards = $reseller_account_name = '';
        if (intval($this->studio->reseller_id)) {
            $seller_app_id = PortalUser::model()->findByPk($this->studio->reseller_id)->seller_app_id;
            $reseller_account_name = SellerApplication::model()->findByAttributes(array('id' => $seller_app_id))->company_name;
        } else {
        $cards = CardInfos::model()->findAllByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id), array('order' => 'is_cancelled ASC'));
    }

        $this->render('managepayment', array('cards' => $cards, 'reseller_account_name' => $reseller_account_name, 'studio' => $studio));
    }

	function isAjaxRequest() {
		if (!Yii::app()->request->isAjaxRequest) {
			$url = Yii::app()->createAbsoluteUrl();
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: '.$url);exit();
		}
	}
	
    function actionSaveCard() {
		self::isAjaxRequest();
		$purifier = new CHtmlPurifier();
		$_POST = $purifier->purify($_POST);
		
        $this->layout = false;
		
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->user->studio_id;

        if (isset($_POST['card_number']) && !empty($_POST['card_number']) && (@$_POST['csrfToken'] === @$_SESSION['csrfToken'])) {
            $studio = Studios::model()->findByPk($studio_id);
            $user = User::model()->findByPk($user_id);

            //Check primary card is exist or not
            $cards = CardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'is_cancelled' => 0));
            $is_primary = 0;
            if (isset($cards) && !empty($cards)) {
                $is_primary = 1;
            }

            $data = array();
            $data['card_number'] = $_POST['card_number'];
            $data['card_name'] = $_POST['card_name'];
            $data['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
            $data['amount'] = 0;
            $data['zip'] = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
            $data['cvv'] = $_POST['cvv'];
            $address = $_POST['address1'];
            $address .= (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
            $address .= ", " . $_POST['city'];
            $address .= ", " . $_POST['state'];
            $data['address'] = $address;
            $data['address2'] = $_POST['address2'];

            $firstData_preauth = $this->authenticateToCard($data);
            $card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);

            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['phone'] = $user->phone_no;
            $req['companyname'] = $studio->name;
            $req['domain'] = 'http://' . $studio->domain;
            $req['card_last_fourdigit'] = $card_last_fourdigit;

            if ($firstData_preauth->isError()) {
                $res['isSuccess'] = 0;
                $res['Code'] = $firstData_preauth->getErrorCode();
                $res['Message'] = 'Invalid Card details entered. Please check again';

                $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();

                //Keeping card error logs
                $ceModel = New CardErrors;
                $ceModel->studio_id = $studio_id;
                $ceModel->response_text = serialize($firstData_preauth);
                $ceModel->transaction_type = 'Save card';
                $ceModel->created = new CDbExpression("NOW()");
                $ceModel->save();

                Yii::app()->email->errorMessageMailToSales($req);
            } else {
                if ($firstData_preauth->getBankResponseType() == 'S') {
                    $res['isSuccess'] = 1;
                    $res['TransactionRecord'] = $firstData_preauth->getTransactionRecord();
                    $res['Code'] = $firstData_preauth->getBankResponseCode();

                    $package = Package::model()->getPackages();

                    $ciModel = New CardInfos;
                    $ciModel->user_id = $user_id;
                    $ciModel->studio_id = $studio_id;
                    $ciModel->payment_method = 'first_data';
                    $ciModel->plan_amt = $package['package'][0]['base_price'];
                    $ciModel->card_holder_name = $_POST['card_name'];
                    $ciModel->exp_month = $_POST['exp_month'];
                    $ciModel->exp_year = $_POST['exp_year'];
                    $ciModel->address1 = $_POST['address1'];
                    $ciModel->address2 = $_POST['address2'];
                    $ciModel->city = (isset($_POST['city']) && trim($_POST['city'])) ? $_POST['city'] : '';
                    $ciModel->state = (isset($_POST['state']) && trim($_POST['state'])) ? $_POST['state'] : '';
                    $ciModel->zip = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
                    $ciModel->status = $res['Message'] = $firstData_preauth->getBankResponseMessage();
                    $ciModel->reference_no = $firstData_preauth->getTransactionTag();
                    $ciModel->card_type = $data['card_type'] = $firstData_preauth->getCreditCardType();
                    $ciModel->card_last_fourdigit = $data['card_last_fourdigit'] = $card_last_fourdigit;
                    $ciModel->auth_num = $firstData_preauth->getAuthNumber();
                    $ciModel->token = $data['token'] = $firstData_preauth->getTransArmorToken();
                    $ciModel->is_cancelled = $is_primary;
                    $ciModel->response_text = serialize($firstData_preauth);
                    $ciModel->ip = CHttpRequest::getUserHostAddress();
                    $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                    $ciModel->created = new CDbExpression("NOW()");
                    $ciModel->save();

                    //Check if partial or failed payment record exists for customer only
                    if (isset($studio->status) && ($studio->status == 1) && ($studio->is_subscribed == 1) && isset($studio->payment_status) && ($studio->payment_status != 0)) {
                        $res = $this->chargeDueOnPartialOrFailedTransaction($studio, $user, $ciModel, $data);
                    }
                } else {
                    $res['isSuccess'] = 0;
                    $res['Code'] = 55;
                    $res['Message'] = 'We are not able to process your credit card. Please try another card.';
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        print json_encode($res);exit;
    }

    function chargeDueOnPartialOrFailedTransaction($studio, $user, $card_info, $data = array()) {
        $res = array();

        if (isset($data) && !empty($data)) {
            $studio_id = $studio->id;
            $user_id = $user->id;
            $data['amount'] = $amount = $studio->partial_failed_due;

            if (abs($amount) >= 0.01) {
                $firstData = $this->chargeToCard($data);

                $req['name'] = $user->first_name;
                $req['email'] = $user->email;
                $req['phone'] = $user->phone_no;
                $req['companyname'] = $studio->name;
                $req['domain'] = 'http://' . $studio->domain;
                $req['card_last_fourdigit'] = $data['card_last_fourdigit'];

                if ($firstData->isError()) {
                    $res['isSuccess'] = 0;
                    $res['Code'] = $firstData->getErrorCode();
                    $res['Message'] = 'Invalid Card details entered. Please check again';

                    $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

                    //Keeping card error logs
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData);
                    $ceModel->transaction_type = 'Transaction due on subscription renew';
                    $ceModel->created = new CDbExpression("NOW()");
                    $ceModel->save();

                    //Send an email to developer and support
                    Yii::app()->email->errorMessageMailToSales($req);
                } else {
                    if ($firstData->getBankResponseType() == 'S') {
                        $card_id = $card_info->id;

                        //Update all card to inactive mode to make primary for current card
                        $con = Yii::app()->db;
                        $sql = "UPDATE card_infos SET is_cancelled=1 WHERE id !=" . $card_id . " AND user_id=" . $user_id . " AND studio_id=" . $studio_id;
                        $ciData = $con->createCommand($sql)->execute();

                        //make primary for current card
                        $con = Yii::app()->db;
                        $sql = "UPDATE card_infos SET is_cancelled=0 WHERE id =" . $card_id . " AND user_id=" . $user_id . " AND studio_id=" . $studio_id;
                        $ciData = $con->createCommand($sql)->execute();

                        if (isset($studio->payment_status) && $studio->payment_status == 1) {
                            $payment_status = 'Partial';
                        } else if (isset($studio->payment_status) && $studio->payment_status == 2) {
                            $payment_status = 'Failed';
                        }
                        $invoice_detail = $payment_status . ' transaction due';
                        
                        //Find out the billing detail
                        $biModel = BillingInfos::model()->find(array('condition' => 'is_paid != 2 AND transaction_type =1 AND studio_id = '.$studio_id, 'order'=>'created_date DESC'));
                        $billing_info_id = $biModel->id;
                        $billing_detail = $biModel->detail;
                        
                        //Insert new transaction record in transaction infos;
                        //Set datas for inserting new record in transaction info table whether it returns success or error
                        $tiModel = New TransactionInfos;
                        $tiModel->card_info_id = $card_id;
                        $tiModel->studio_id = $studio_id;
                        $tiModel->user_id = $user_id;
                        $tiModel->billing_info_id = $billing_info_id;
                        $tiModel->billing_amount = $amount;
                        $tiModel->invoice_detail = $invoice_detail;
                        $tiModel->is_success = 1;
                        $tiModel->invoice_id = $firstData->getTransactionTag();
                        $tiModel->order_num = $firstData->getAuthNumber();
                        $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                        $tiModel->response_text = serialize($firstData);
                        $tiModel->transaction_type = $payment_status . ' transaction due on subscription renew';
                        $tiModel->created_date = new CDbExpression("NOW()");
                        $tiModel->save();

                        //Check billing amount and paid amount is same or not, otherwise charge again.
                        if (abs(($amount - $paid_amount) / $paid_amount) < 0.00001) {
                            $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
                            $bandwidth_date = gmdate('Y-m-d', strtotime($studio->bandwidth_date));
                            
                            if ((isset($studio->payment_status) && $studio->payment_status == 2) || (strtotime(gmdate('Y-m-d')) > strtotime($billing_date)) || (strtotime(gmdate('Y-m-d')) > strtotime($bandwidth_date))){
                                $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans', 'condition' => 'status = 1 AND studio_id = '.$studio_id, 'order'=>'application_id ASC'));
                                if (isset($plans) && !empty($plans)) {
                                    $plan = $plans[0]->plans;
                                }
                                $biModel = New BillingInfos;
                                $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
                                if ($plan == 'Month') {
                                    $start_date = Date('Y-m-d H:i:s', strtotime($studio->start_date."+1 Months"));
                                    $end_date = Date('Y-m-d H:i:s', strtotime($studio->end_date."+1 Months -1 Days"));
                                    $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months"))));
                                    $studio->start_date = $start_date;
                                    $studio->end_date = $end_date;
                                    $studio->bandwidth_date = $start_date;
                                } else {
                                    if (strtotime(gmdate('Y-m-d')) > strtotime($billing_date)) {
                                        $start_date = Date('Y-m-d H:i:s', strtotime($studio->start_date."+1 Years"));
                                        $end_date = Date('Y-m-d H:i:s', strtotime($studio->end_date."+1 Years -1 Days"));
                                        $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Years"))));
                                        $studio->start_date = $start_date;
                                        $studio->end_date = $end_date;
                                        $studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime($studio->bandwidth_date."+1 Months"));
                                    } else {
                                        $studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime($studio->bandwidth_date."+1 Months"));
                                    }
                                }
                            }
                            $is_paid = 2;

                            $studio->payment_status = 0;
                            $studio->partial_failed_date = '';
                            $studio->partial_failed_due = 0;
                        } else {
                            $is_paid = 1;
                            $studio->partial_failed_due = $paid_amount;
                        }
                        $studio->save();
                        
                        $invoice['studio'] = $studio;
                        $invoice['html'] = $billing_detail;
                        $invoice['isEmail'] = 1;

                        $pdf = Yii::app()->pdf->generatePdf($invoice);
                        
                        //Update sbscription in studio table if tranasction is going to success or not.
                        $biModel = New BillingInfos;
                        $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => $is_paid));

                        $req['invoice_title'] = $invoice_detail;
                        $req['invoice_amount'] = $paid_amount;

                        Yii::app()->email->raiseInvoicePaidMailToUser($req, $pdf);
                        Yii::app()->email->raiseInvoicePaidMailToSales($req);

                        $res['isSuccess'] = 1;
                        $res['TransactionRecord'] = $firstData->getTransactionRecord();
                        $res['Code'] = $firstData->getBankResponseCode();
                    }
                }
            }
        }

        return $res;
    }

    function actionDeleteCard() {
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            CardInfos::model()->deleteByPk($_REQUEST['id_payment'], 'studio_id=:studio_id', array(':studio_id' => Yii::app()->user->studio_id));

            Yii::app()->user->setFlash('success', 'Credit Card has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be deleted!');
        }

        $url = $this->createUrl('payment/managePayment');
        $this->redirect($url);
    }

    function actionMakePrimaryCard() {
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->user->studio_id;

            //Update card to inactive mode
            $sql = "UPDATE card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
            $con = Yii::app()->db;
            $ciData = $con->createCommand($sql)->execute();

            $model = new CardInfos;
            $card = CardInfos::model()->findByAttributes(array('id' => $_REQUEST['id_payment'], 'studio_id' => $studio_id, 'user_id' => $user_id));
            $card->is_cancelled = 0;
            $card->save();

            //Check if partial or failed payment record exists for customer only
            $studio = Studios::model()->findByPk($studio_id);
            if (isset($studio->status) && ($studio->status == 1) && ($studio->is_subscribed == 1) && isset($studio->payment_status) && ($studio->payment_status != 0)) {
                $user = User::model()->findByPk($user_id);

                $data = array();
                $data['token'] = $card->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['card_name'] = $card->card_holder_name;
                $data['card_type'] = $card->card_type;
                $data['card_last_fourdigit'] = $card->card_last_fourdigit;
                $data['exp'] = ($card->exp_month < 10) ? '0' . $card->exp_month . substr($card->exp_year, -2) : $card->exp_month . substr($card->exp_year, -2);

                $res = $this->chargeDueOnPartialOrFailedTransaction($studio, $user, $card, $data);
            }

            Yii::app()->user->setFlash('success', 'Credit Card has been set as primary successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be made as primary!');
        }

        $url = $this->createUrl('payment/managePayment');
        $this->redirect($url);
    }
    
    function actionPayFromPrimaryCard() {
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;

        $studio = $this->studio;
        
        if(isset($studio->payment_status) && intval($studio->payment_status) !=0) {
            $user = User::model()->findByPk($user_id);
            
            $card = CardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'is_cancelled'=>0), array('order' => 'is_cancelled ASC'));
            if (!empty($card)) {
                $data = array();
                $data['token'] = $card->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['card_name'] = $card->card_holder_name;
                $data['card_type'] = $card->card_type;
                $data['card_last_fourdigit'] = $card->card_last_fourdigit;
                $data['exp'] = ($card->exp_month < 10) ? '0' . $card->exp_month . substr($card->exp_year, -2) : $card->exp_month . substr($card->exp_year, -2);

                $res = self::chargeDueOnPartialOrFailedTransaction($studio, $user, $card, $data);
                
                if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                    $url = $this->createUrl('payment/paymenthistory');
                    Yii::app()->user->setFlash('success', 'Your payment has been processed successfully');
                } else {
                    $url = $this->createUrl('payment/managePayment');
                    Yii::app()->user->setFlash('error', @$res['Message']);
                }
            } else {
                $url = $this->createUrl('payment/managePayment');
                Yii::app()->user->setFlash('error', 'Oops! No primary card has found');
            }
        } else {
            $url = $this->createUrl('payment/managePayment');
            Yii::app()->user->setFlash('error', 'Oops! The payment has not failed');
        }

        $this->redirect($url);
    }
    
    function actionBillDetail() {
        $studio_id = Yii::app()->user->studio_id;

        $studio = $this->studio;
        
        if(isset($studio->payment_status) && intval($studio->payment_status) !=0) {
            $biModel = BillingInfos::model()->find(array('condition' => 'is_paid != 2 AND transaction_type =1 AND studio_id = '.$studio_id, 'order'=>'created_date DESC'));
            if (!empty($biModel)) {
                $billing_detail = $biModel->detail;

                $invoice['studio'] = $studio;
                $invoice['html'] = $billing_detail;
                $invoice['isEmail'] = 0;
                $invoice['billing_date'] = gmdate('F_d_Y', strtotime($biModel->billing_date));

                $pdf = Yii::app()->pdf->generatePdf($invoice);
                print $pdf;
                exit;
            } else {
                $url = $this->createUrl('payment/managePayment');
                Yii::app()->user->setFlash('error', 'Oops! No billing information found');
            }
        } else {
            $url = $this->createUrl('payment/managePayment');
            Yii::app()->user->setFlash('error', 'Oops! The payment has not failed');
        }

        $this->redirect($url);
    }
    
    /*function actionMakePrimaryCardTest() {
        $_REQUEST['id_payment'] = 0;
        
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->user->studio_id;

            $card = CardInfos::model()->findByAttributes(array('id' => $_REQUEST['id_payment'], 'studio_id' => $studio_id, 'user_id' => $user_id));
            //print '<pre>';print_r($card);exit;
            
            //Check if partial or failed payment record exists for customer only
            $studio = Studios::model()->findByPk($studio_id);
            if (isset($studio->status) && ($studio->status == 1) && ($studio->is_subscribed == 1) && isset($studio->payment_status) && ($studio->payment_status != 0)) {
                $user = User::model()->findByPk($user_id);

                $data = array();
                $data['token'] = $card->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['card_name'] = $card->card_holder_name;
                $data['card_type'] = $card->card_type;
                $data['card_last_fourdigit'] = $card->card_last_fourdigit;
                $data['exp'] = ($card->exp_month < 10) ? '0' . $card->exp_month . substr($card->exp_year, -2) : $card->exp_month . substr($card->exp_year, -2);

                $res = $this->chargeDueOnPartialOrFailedTransaction($studio, $user, $card, $data);
            }

            Yii::app()->user->setFlash('success', 'Credit Card has been set as primary successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be made as primary!');
        }

        $url = $this->createUrl('payment/managePayment');
        $this->redirect($url);
    }*/

    function actionSubscription() {
        $this->breadcrumbs = array('Purchase Subscription');
        $this->pageTitle = "Muvi | Purchase Subscription";

        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
        $packages = Package::model()->getPackages();
        if($_REQUEST['modalflag']==1){
            $this->renderpartial('subscription_ajax', array('card' => $card, 'packages' => $packages));
        }else{
            $this->render('subscription', array('card' => $card, 'packages' => $packages));
        }
    }
    
    /*function actionValidatePackageCustomCode() {
        $data = array();
        $data['isExists'] = 0;
        
        if (isset($_REQUEST['package_code']) && trim($_REQUEST['package_code']) && isset($_REQUEST['custom_code']) && trim($_REQUEST['custom_code'])) {
            //(array)$package_codes = PackageCode::model()->findByAttributes(array('package_code' => trim($_REQUEST['package_code']), 'custom_code' => trim($_REQUEST['custom_code'])));
            (array)$package_codes = PackageCode::model()->find(
                    array(
                            'select'=>'*',
                            'condition'=>'package_code=:package_code AND BINARY custom_code=:custom_code',
                            'params'=>array(':package_code'=>trim($_REQUEST['package_code']),'custom_code'=>trim($_REQUEST['custom_code'])),
                        )
                    );
            if (isset($package_codes) && !empty($package_codes)) {
                $data = $package_codes->attributes;
                $data['isExists'] = 1;
            }
        }
        
        print json_encode($data);
        exit;
    }*/

    function actionReactivate() {
        $this->breadcrumbs = array('Reactivation');
        $this->pageTitle = "Muvi | Reactivation";

        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
        $packages = Package::model()->getPackages();

        $this->render('reactivate', array('card' => $card, 'packages' => $packages));
    }

    function actionAuthenticateCard() {
        $user_id = isset($_POST['userid'])?$_POST['userid']:Yii::app()->user->id;// modified by manas
        $studio_id = isset($_POST['studioid'])?$_POST['studioid']:Yii::app()->common->getStudiosId();// modified by manas
        $ip_address = CHttpRequest::getUserHostAddress();

        if (isset($_REQUEST['card_number']) && !empty($_REQUEST['card_number'])) {
            $dataInput = array();

            $dataInput['card_number'] = $_REQUEST['card_number'];
            $dataInput['card_name'] = $_REQUEST['card_name'];
            $dataInput['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
            $dataInput['amount'] = 0;
            $dataInput['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';
            $dataInput['cvv'] = $_REQUEST['cvv'];
            $address = $_REQUEST['address1'];
            $address .= (trim($_REQUEST['address2'])) ? ", " . trim($_REQUEST['address2']) : '';
            $address .= ", " . $_REQUEST['city'];
            $address .= ", " . $_REQUEST['state'];
            $dataInput['address'] = $address;

            $firstData_preauth = $this->authenticateToCard($dataInput);

            $card_last_fourdigit = str_replace(substr($dataInput['card_number'], 0, strlen($dataInput['card_number']) - 4), str_repeat("#", strlen($dataInput['card_number']) - 4), $dataInput['card_number']);
            $studio = Studios::model()->findByPk($studio_id);
            $user = User::model()->findByPk($user_id);

            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['phone'] = $user->phone_no;
            $req['companyname'] = $studio->name;
            $req['domain'] = 'http://' . $studio->domain;
            $req['card_last_fourdigit'] = $card_last_fourdigit;

            if ($firstData_preauth->isError()) {
                $res['isSuccess'] = 0;
                $res['Code'] = $firstData_preauth->getErrorCode();
                $res['Message'] = 'Invalid Card details entered. Please check again';

                $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();

                //Keeping card error logs
                $ceModel = New CardErrors;
                $ceModel->studio_id = $studio_id;
                $ceModel->response_text = serialize($firstData_preauth);
                $ceModel->transaction_type = 'Card Authentication';
                $ceModel->created = new CDbExpression("NOW()");
                $ceModel->save();

                //Send an email to developer and support
                Yii::app()->email->errorMessageMailToSales($req);
            } else {
                $respons_type = $firstData_preauth->getBankResponseType();

                if ($respons_type == 'S') {
                    $token = $firstData_preauth->getTransArmorToken();
                    $card_type = $firstData_preauth->getCreditCardType();

                    //Getting all plans amount which has selected by user
                    $package = Package::model()->getPackages($_POST['packages']);

                    $total_amount = 0;
                    if (isset($package) && !empty($package)) {
                        $package_name = $package['package'][0]['name'];
                        $app_price = Yii::app()->general->getAppPrice($package['package'][0]['code']);
                        
                        $package_codes_id = 0;
                        $total_amount = $base_price = $package['package'][0]['base_price'];

                        $yearly_discount = $package['package'][0]['yearly_discount'];

                        if (isset($_POST['pricing']) && !empty($_POST['pricing'])) {
                            $applications = Package::model()->getApplications($_POST['packages'], $_POST['pricing']);
                            $no_of_applications = count($applications);
                            if (isset($applications) && !empty($applications) && ($no_of_applications > 1)) {
                                $total_amount = number_format((float) $total_amount + (($no_of_applications - 1) * $app_price), 2, '.', '');
                            }
                        }

                        if (isset($_POST['plan']) && trim($_POST['plan']) == 'Year') {
                            $total_amount = number_format((float) (($total_amount * 12) - ((($total_amount * 12) * $yearly_discount) / 100)), 2, '.', '');
                        }
                    }                    
                    $data = array();
                    $data['token'] = $token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                    $data['card_name'] = $_REQUEST['card_name'];
                    $data['card_type'] = $card_type;
                    $data['exp'] = $dataInput['exp'];
                    $data['amount'] = $total_amount;
                    $data['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
                    $data['cvv'] = $_REQUEST['cvv'];

                    $ciModel = New CardInfos;
                    $ciModel->user_id = $user_id;
                    $ciModel->studio_id = $studio_id;
                    $ciModel->payment_method = 'first_data';
                    $ciModel->plan_amt = $total_amount;
                    $ciModel->card_holder_name = $data['card_name'];
                    $ciModel->exp_month = $_REQUEST['exp_month'];
                    $ciModel->exp_year = $_REQUEST['exp_year'];

                    $address = $_REQUEST['address1'];
                    $ciModel->address1 = $address;
                    $ciModel->address2 = $data['address2'] = $_REQUEST['address2'];
                    $ciModel->city = (isset($_REQUEST['city']) && trim($_REQUEST['city'])) ? $_REQUEST['city'] : '';
                    $ciModel->state = (isset($_REQUEST['state']) && trim($_REQUEST['state'])) ? $_REQUEST['state'] : '';
                    $ciModel->zip = $data['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';

                    $firstData = $this->chargeToCard($data);

                    if ($firstData->isError()) {
                        $res['isSuccess'] = 0;
                        $res['Code'] = $firstData->getErrorCode();
                        $res['Message'] = 'Invalid Card details entered. Please check again';

                        $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

                        //Keeping card error logs
                        $ceModel = New CardErrors;
                        $ceModel->studio_id = $studio_id;
                        $ceModel->response_text = serialize($firstData);
                        $ceModel->transaction_type = 'Card Authentication';
                        $ceModel->created = new CDbExpression("NOW()");
                        $ceModel->save();

                        //Send an email to developer and support
                        Yii::app()->email->errorMessageMailToSales($req);
                    } else {
                        $respons_type = $firstData->getBankResponseType();

                        if ($firstData->getBankResponseType() == 'S') {
                            $res['isSuccess'] = 1;
                            $res['TransactionRecord'] = $firstData->getTransactionRecord();
                            $res['Code'] = $firstData->getBankResponseCode();

                            //Update all card to inactive mode
                            $con = Yii::app()->db;
                            $sql = "UPDATE card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                            $ciData = $con->createCommand($sql)->execute();

                            $ciModel->is_cancelled = 0;
                            $ciModel->status = $res['Message'] = $firstData->getBankResponseMessage();
                            $ciModel->reference_no = $firstData->getTransactionTag();
                            $ciModel->card_type = $card_type;
                            $ciModel->card_last_fourdigit = $card_last_fourdigit;
                            $ciModel->auth_num = $firstData->getAuthNumber();
                            $ciModel->token = $token;

                            $ciModel->response_text = serialize($firstData_preauth);
                            $ciModel->ip = CHttpRequest::getUserHostAddress();
                            $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                            $ciModel->created = new CDbExpression("NOW()");
                            $ciModel->save();

                            //Insert new record for billing information
                            $biModel = New BillingInfos;
                            $biModel->studio_id = $studio_id;
                            $biModel->user_id = $user_id;
                            $uniqid = Yii::app()->common->generateUniqNumber();
                            $biModel->uniqid = $uniqid;
                            $biModel->title = 'Purchase Subscription fee for ' . $package_name;
                            $biModel->billing_date = new CDbExpression("NOW()");
                            $biModel->created_date = new CDbExpression("NOW()");
                            $biModel->billing_amount = $billing_amount = $data['amount'];
                            $biModel->transaction_type = 1;
                            $biModel->save();
                            $billing_info_id = $biModel->id;

                            //Insert new records in bill detail and studio pricing plans
                            $allapps = '';
                            if (isset($_POST['pricing']) && !empty($_POST['pricing']) && isset($applications) && !empty($applications)) {
                                
                                $cntapp = 1;
                                foreach ($applications as $key => $value) {
                                    if ($cntapp == 1) {
                                        $title = 'Monthly platform fee';
                                        $description = $package_name.' platform fee';
                                        $packageamount = $base_price;
                                    } else {
                                        $title = $value['name'];
                                        $description = $value['description'];
                                        $packageamount = $value['price'];
                                    }
                                    $cntapp++;

                                    $bdModel = New BillingDetails;
                                    $bdModel->billing_info_id = $billing_info_id;
                                    $bdModel->user_id = $user_id;
                                    $bdModel->title = $title;
                                    $bdModel->description = $description;
                                    $bdModel->amount = $packageamount;
                                    $bdModel->save();
                                    
                                    $sppModel = New StudioPricingPlan;
                                    $sppModel->studio_id = $studio_id;
                                    $sppModel->package_id = $value['parent_id'];
                                    $sppModel->application_id = $value['id'];
                                    $sppModel->plans = $_POST['plan'];
                                    $sppModel->is_cloud_hosting = 0;
                                    $sppModel->package_code_id = $package_codes_id;
                                    $sppModel->created_by = $user_id;
                                    $sppModel->created = new CDbExpression("NOW()");
                                    $sppModel->status = 1;
                                    $sppModel->save();

                                    $allapps.=$value['name'] . ', ';
                                }
                                $allapps = rtrim($allapps, ', ');
                            } else {//Muvi Server
                                $title = 'Monthly platform fee';
                                $description = $package_name.' platform fee';

                                $bdModel = New BillingDetails;
                                $bdModel->billing_info_id = $billing_info_id;
                                $bdModel->user_id = $user_id;
                                $bdModel->title = $title;
                                $bdModel->description = $description;
                                $bdModel->amount = $base_price;
                                $bdModel->save();

                                $sppModel = New StudioPricingPlan;
                                $sppModel->studio_id = $studio_id;
                                $sppModel->package_id = $package['package'][0]['id'];
                                $sppModel->application_id = 0;
                                $sppModel->plans = $_POST['plan'];
                                $sppModel->is_cloud_hosting = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;
                                $sppModel->package_code_id = $package_codes_id;
                                $sppModel->created_by = $user_id;
                                $sppModel->created = new CDbExpression("NOW()");
                                $sppModel->status = 1;
                                $sppModel->save();
                            }

                            //Insert new transaction record in transaction infos;
                            $tiModel = New TransactionInfos;

                            //Set datas for inserting new record in transaction info table whether it returns success or error
                            $tiModel->card_info_id = $ciModel->id;
                            $tiModel->studio_id = $studio_id;
                            $tiModel->user_id = $user_id;
                            $tiModel->billing_info_id = $billing_info_id;
                            $tiModel->billing_amount = $billing_amount;
                            $tiModel->invoice_detail = $_POST['plan'] . "ly Subscription Charge";
                            $tiModel->is_success = 1;
                            $tiModel->invoice_id = $firstData->getTransactionTag();
                            $tiModel->order_num = $firstData->getAuthNumber();
                            $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                            $tiModel->response_text = serialize($firstData);
                            $tiModel->transaction_type = 'Purchase Subscription fee for ' . $package_name;
                            $tiModel->created_date = new CDbExpression("NOW()");
                            $tiModel->save();

                            if (abs(($billing_amount - $paid_amount) / $paid_amount) < 0.00001) {
                                $is_paid = 2;
                            } else {
                                $is_paid = 1;
                            }

                            $invoice['studio'] = $studio;
                            $invoice['user'] = $user;
                            $invoice['card_info'] = $ciModel;
                            $invoice['billing_info_id'] = $billing_info_id;
                            $invoice['billing_amount'] = $billing_amount;
                            $invoice['package_name'] = @$package_name;
                            $invoice['package_code'] = @$package['package'][0]['code'];
                            $invoice['base_price'] = @$base_price;
                            $invoice['plan'] = @$_POST['plan'];
                            $invoice['yearly_discount'] = @$yearly_discount;
                            $invoice['no_of_applications'] = @$no_of_applications;
                            $invoice['applications'] = @$allapps;
                            
                            if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Months -1 Days"));
                            } else {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Years -1 Days"));
                            }
                            
                            $pdf = Yii::app()->pdf->adminSubscriptionReactivationInvoiceDetial($invoice);
                            $file_name = $pdf['pdf'];

                            //Update subscription in studio table if tranasction is going to success or not.
                            $biModel = New BillingInfos;
                            $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => $is_paid, 'detail' => $pdf['html']));

                            //Save subscription info if paymanet has processed successfully
                            //if (intval($is_paid) == 2) {
                            $studio->status = 1;
                            $studio->is_subscribed = 1;
                            $studio->subscription_start = Date('Y-m-d H:i:s');
                            $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
                            if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . "+1 Months -1 Days"));
                                $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                                $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months -1 Days"))));
                                $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                            } else {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . "+1 Years -1 Days"));
                                $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
                                $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Years -1 Days"))));
                                $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
                            }
                            $studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                            //Save payment related info in studio table
                            $studio->save();
                            $scrModel = StudioCancelReason :: model()->find(array("condition" => " studio_id=" . $studio_id));
                            if (!empty($scrModel)) {
                                $scrModel->delete();
                            }
                            //}

                            $req['package_name'] = @$package_name;
                            $req['applications'] = @$allapps;
                            $req['plan'] = @$_POST['plan'];
                            $req['amount_charged'] = @$billing_amount;
                            $req['is_cloud_hosting'] = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;
                            
                            Yii::app()->email->subscriptionPurchaseMailToSales($req);
                            Yii::app()->email->subscriptionPurchaseMailToUser($req, $file_name);
                            //Create Cloudfront Url For studio
                            $this->createClouldfrontUrl($studio);
                            
                            Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');
                        } else {
                            $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                            $res['isSuccess'] = 0;
                            $res['Code'] = 55;
                            $res['Message'] = $error_message;
                        }
                    }
                } else {
                    $error_message = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
                    $res['isSuccess'] = 0;
                    $res['Code'] = 55;
                    $res['Message'] = $error_message;
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        print json_encode($res);
        exit;
    }

    function actionPurchaseSubscribe() {
        if (isset($_POST['payment_info']) && $_POST['payment_info'] == 'saved') {
            //print '<pre>';print_r($_POST);
            
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();

            $card_info = CardInfos::model()->findByAttributes(array('id' => $_POST['creditcard'], 'studio_id' => $studio_id, 'user_id' => $user_id));
            if (isset($card_info) && !empty($card_info)) {
                //Getting all plans amount which has selected by user
                $package = Package::model()->getPackages($_POST['packages']);

                $total_amount = 0;
                if (isset($package) && !empty($package)) {
                    $package_name = $package['package'][0]['name'];
                    $app_price = Yii::app()->general->getAppPrice($package['package'][0]['code']);
                    $package_codes_id = 0;
                    $total_amount = $base_price = $package['package'][0]['base_price'];
                    
                    $yearly_discount = $package['package'][0]['yearly_discount'];
                    
                    if (isset($_POST['pricing']) && !empty($_POST['pricing'])) {
                        $applications = Package::model()->getApplications($_POST['packages'], $_POST['pricing']);
                        $no_of_applications = count($applications);
                        if (isset($applications) && !empty($applications) && ($no_of_applications > 1)) {
                            $total_amount = number_format((float) $total_amount + (($no_of_applications - 1) * $app_price), 2, '.', '');
                        }
                    }

                    if (isset($_POST['plan']) && trim($_POST['plan']) == 'Year') {
                        $total_amount = number_format((float) (($total_amount * 12) - ((($total_amount * 12) * $yearly_discount) / 100)), 2, '.', '');
                    }
                }               
                $data = array();
                $data['token'] = $card_info->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['name'] = $card_info->card_holder_name;
                $data['card_type'] = $card_info->card_type;
                $data['exp'] = ($card_info->exp_month < 10) ? '0' . $card_info->exp_month . substr($card_info->exp_year, -2) : $card_info->exp_month . substr($card_info->exp_year, -2);
                $data['amount'] = $total_amount;

                if (@IS_DEMO_PAYMENT == 1) {
                    $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
                } else {
                    if (IS_LIVE_FIRSTDATA) {
                        $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                    } else {
                        $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                    }
                }

                $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
                $firstData->setTransArmorToken($data['token'])
                        ->setCreditCardType($data['card_type'])
                        ->setCreditCardName($data['name'])
                        ->setCreditCardExpiration($data['exp'])
                        ->setAmount($data['amount']);

                $firstData->process();

                if ($firstData->isError()) {
                    //Keeping card error logs
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData);
                    $ceModel->transaction_type = 'Purchase Subscription fee for ' . $package_name;
                    $ceModel->created = new CDbExpression("NOW()");
                    $ceModel->save();
                } else {
                    if ($firstData->getBankResponseType() == 'S') {

                        //Insert new record for billing information
                        $biModel = New BillingInfos;
                        $biModel->studio_id = $studio_id;
                        $biModel->user_id = $user_id;
                        $uniqid = Yii::app()->common->generateUniqNumber();
                        $biModel->uniqid = $uniqid;
                        $biModel->title = 'Purchase Subscription fee for ' . $package_name;
                        $biModel->billing_date = new CDbExpression("NOW()");
                        $biModel->created_date = new CDbExpression("NOW()");
                        $biModel->billing_amount = $billing_amount = $data['amount'];
                        $biModel->transaction_type = 1;
                        $biModel->save();
                        $billing_info_id = $biModel->id;

                        //Insert new records in bill detail and studio pricing plans
                        $allapps = '';
                        if (isset($_POST['pricing']) && !empty($_POST['pricing']) && isset($applications) && !empty($applications)) {
                            
                            $cntapp = 1;
                            foreach ($applications as $key => $value) {
                                if ($cntapp == 1) {
                                    $title = 'Monthly platform fee';
                                    $description = $package_name.' platform fee';
                                    $packageamount = $base_price;
                                } else {
                                    $title = $value['name'];
                                    $description = $value['description'];
                                    $packageamount = $value['price'];
                                }
                                $cntapp++;
                                
                                $bdModel = New BillingDetails;
                                $bdModel->billing_info_id = $billing_info_id;
                                $bdModel->user_id = $user_id;
                                $bdModel->title = $title;
                                $bdModel->description = $description;
                                $bdModel->amount = $packageamount;
                                $bdModel->save();

                                $sppModel = New StudioPricingPlan;
                                $sppModel->studio_id = $studio_id;
                                $sppModel->package_id = $value['parent_id'];
                                $sppModel->application_id = $value['id'];
                                $sppModel->is_cloud_hosting = 0;
                                $sppModel->package_code_id = $package_codes_id;
                                $sppModel->plans = $_POST['plan'];
                                $sppModel->created_by = $user_id;
                                $sppModel->created = new CDbExpression("NOW()");
                                $sppModel->status = 1;
                                $sppModel->save();

                                $allapps.=$value['name'] . ', ';
                            }
                            $allapps = rtrim($allapps, ', ');
                        } else {//Muvi Server
                            $title = 'Monthly platform fee';
                            $description = $package_name.' platform fee';
                                    
                            $bdModel = New BillingDetails;
                            $bdModel->billing_info_id = $billing_info_id;
                            $bdModel->user_id = $user_id;
                            $bdModel->title = $title;
                            $bdModel->description = $description;
                            $bdModel->amount = $base_price;
                            $bdModel->save();

                            $sppModel = New StudioPricingPlan;
                            $sppModel->studio_id = $studio_id;
                            $sppModel->package_id = $package['package'][0]['id'];
                            $sppModel->application_id = 0;
                            $sppModel->is_cloud_hosting = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;
                            $sppModel->package_code_id = $package_codes_id;
                            $sppModel->plans = $_POST['plan'];
                            $sppModel->created_by = $user_id;
                            $sppModel->created = new CDbExpression("NOW()");
                            $sppModel->status = 1;
                            $sppModel->save();
                        }

                        //Insert new transaction record in transaction infos;
                        $tiModel = New TransactionInfos;

                        //Set datas for inserting new record in transaction info table whether it returns success or error
                        $tiModel->card_info_id = $card_info->id;
                        $tiModel->studio_id = $studio_id;
                        $tiModel->user_id = $user_id;
                        $tiModel->billing_info_id = $billing_info_id;
                        $tiModel->billing_amount = $billing_amount;
                        $tiModel->invoice_detail = $_POST['plan'] . "ly Subscription Charge";
                        $tiModel->is_success = 1;
                        $tiModel->invoice_id = $firstData->getTransactionTag();
                        $tiModel->order_num = $firstData->getAuthNumber();
                        $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                        $tiModel->response_text = serialize($firstData);
                        $tiModel->transaction_type = 'Purchase Subscription fee for ' . $package_name;
                        $tiModel->created_date = new CDbExpression("NOW()");
                        $tiModel->save();

                        if (abs(($billing_amount - $paid_amount) / $paid_amount) < 0.00001) {
                            $is_paid = 2;
                        } else {
                            $is_paid = 1;
                        }

                        $studio = Studios::model()->findByPk($studio_id);
                        $user = User::model()->findByPk($user_id);
                        
                        $invoice['studio'] = $studio;
                        $invoice['user'] = $user;
                        $invoice['card_info'] = $card_info;
                        $invoice['billing_info_id'] = $billing_info_id;
                        $invoice['billing_amount'] = $billing_amount;
                        $invoice['package_name'] = @$package_name;
                        $invoice['package_code'] = @$package['package'][0]['code'];
                        $invoice['base_price'] = @$base_price;
                        $invoice['plan'] = @$_POST['plan'];
                        $invoice['yearly_discount'] = @$yearly_discount;
                        $invoice['no_of_applications'] = @$no_of_applications;
                        $invoice['applications'] = @$allapps;
                        
                        if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
                            $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Months -1 Days"));
                        } else {
                            $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Years -1 Days"));
                        }

                        $pdf = Yii::app()->pdf->adminSubscriptionReactivationInvoiceDetial($invoice);
                        $file_name = $pdf['pdf'];

                        //Update subscription in studio table if tranasction is going to success or not.
                        $biModel = New BillingInfos;
                        $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => $is_paid, 'detail' => $pdf['html']));
                        $studio->status = 1;
                        $studio->is_subscribed = 1;
                        $studio->is_deleted = 0;
                        $studio->subscription_start = Date('Y-m-d H:i:s');
                        $biModel = New BillingInfos;
                        $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
                        if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
                            $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                            $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                            $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months"))));
                        } else {
                            $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
                            $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
                            $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Years"))));
                        }
                        $studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                        $studio->save();
                        
                        //Update card to inactive mode for making primary
                        $sql = "UPDATE card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                        $con = Yii::app()->db;
                        $ciData = $con->createCommand($sql)->execute();
                        //Make primary
                        $card_info->is_cancelled = 0;
                        $card_info->save();

                        $user = User::model()->findByPk($user_id);

                        $req['name'] = $user->first_name;
                        $req['email'] = $user->email;
                        $req['phone'] = $user->phone_no;
                        $req['companyname'] = $studio->name;
                        $req['domain'] = 'http://' . $studio->domain;
                        $req['package_name'] = $package_name;
                        $req['applications'] = @$allapps;
                        $req['plan'] = @$_POST['plan'];
                        $req['amount_charged'] = @$billing_amount;
                        $req['is_cloud_hosting'] = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;

                        Yii::app()->email->subscriptionPurchaseMailToSales($req);
                        Yii::app()->email->subscriptionPurchaseMailToUser($req, $file_name);
                        //Create Cloudfront Url For studio
                        $this->createClouldfrontUrl($studio);
                        $url = $this->createUrl('admin/managecontent');
                        Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');
                    } else {
                        $url = $this->createUrl('payment/subscription');
                        $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                        Yii::app()->user->setFlash('error', $error_message);
                    }
                }
            } else {
                $url = $this->createUrl('payment/managePayment');
                Yii::app()->user->setFlash('error', 'Oops! Please set credit card to primary in payment information');
            }
        } else {
            $url = $this->createUrl('payment/subscription');
            Yii::app()->user->setFlash('error', 'Oops! Invalid Credit Card!');
        }

        $this->redirect($url);
    }

    function actionPackages() {
        $this->breadcrumbs = array('Billing', 'Muvi Subscription');
        $this->pageTitle = "Muvi | Muvi Subscription";

        $studio_id = $this->studio->id;

        (array) $user = User::model()->findByPk(Yii::app()->user->id);
        $studio = $this->studio;
        $package_code = '';
        
        $reseller_account_name = '';
        if (intval($studio->reseller_id)) {
            $seller_app_id = PortalUser::model()->findByPk($studio->reseller_id)->seller_app_id;
            $reseller_account_name = SellerApplication::model()->findByAttributes(array('id' => $seller_app_id))->company_name;
            
            $this->render('packages', array('reseller_account_name' => $reseller_account_name, 'studio' => $studio));
        } else {
        if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 1 && $studio->status == 1) {//For customer only
            //Get default plans price
            $packages = Package::model()->getPackages();
            
            //Get selected plans and applications choose by customer
            $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans, package_code_id, base_price, is_old', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'id ASC'));
            $selpackage = $plans[0]->package_id;
            $selpackages = Package::model()->getPackages($plans[0]->package_id);
            $selplan = $plans[0]->plans;
            $plan = $plans[0];
            $selapplications = array();
            $package_codes = array();
                        
            $package_code = $selpackages['package'][0]['code'];
            $app_price = Yii::app()->general->getAppPrice($package_code);
            $billing_amount = $selpackages['package'][0]['base_price'];
            if (isset($plans) && !empty($plans)) {
                if($plans[0]->is_old && $selpackages['package'][0]['is_old_base_price']){
                    $billing_amount = $plans[0]->base_price;
                }
            }
            if (isset($plans) && !empty($plans)) {
                foreach ($plans as $key => $value) {
                    if (intval($value->application_id)) {
                        $selapplications[] = $value->application_id;
                    }
                }
            }
            
            if (isset($selapplications) && !empty($selapplications)) {
                $no_of_applications = count($selapplications);
                if (!empty($selapplications) && ($no_of_applications > 1)) {
                    $billing_amount = number_format((float) $billing_amount + (($no_of_applications - 1) * $app_price), 2, '.', '');
                }
            }

            if (isset($selplan) && trim($selplan) == 'Year') {
                $yearly_discount = $selpackages['package'][0]['yearly_discount'];
                $billing_amount = number_format((float) (($billing_amount * 12) - ((($billing_amount * 12) * $yearly_discount) / 100)), 2, '.', '');
            }

            //Get next billing Cycle
            //Getting number of days to calculate (Prorated amount)
            if (isset($plans[0]->plans) && $plans[0]->plans == 'Month') {
                $billing_date = gmdate("Y-m-d", strtotime($studio->start_date));
                $billing_date_after_year = gmdate("Y-m-d", strtotime($billing_date . '+1 Years'));
                $total_days = gmdate('t');
            } else {
                $billing_date = gmdate("Y-m-d", strtotime($studio->start_date));
                $total_days = 365;
                $billing_date_after_year = '';
            }

            $last_dt = new DateTime($billing_date);
            $today = new DateTime(gmdate('Y-m-d H:i:s'));
            $interval = $last_dt->diff($today);
            $days = $interval->days + 1;

            $data['billing_date_after_year'] = $billing_date_after_year;
            $data['billing_date'] = $billing_date;
            $data['days'] = $days;
            $data['total_days'] = $total_days;
            $data['billing_amount'] = $billing_amount;
            
            $this->render('packages', array('userdata' => $user->attributes, 'studio' => $studio, 'packages' => $packages, 'selpackage' => $selpackage, 'selplan' => $selplan, 'selapplications' => $selapplications, 'data' => $data, 'package_codes' => $package_codes, 'package_code' => $package_code,'plan'=>$plan));
        } else {
            Yii::app()->user->setFlash('error', 'Please purchase subscription.');
            $url = $this->createUrl('payment/subscription');
            $this->redirect($url);
            exit;
        }
    }
    }

    function actionGetPrice() {
        $res = array();
        $res['billing_amount'] = 0;
        $res['price_total'] = 0;
            
        /*$_REQUEST['package'] = 1;
        $_REQUEST['plan'] = 'Year';
        $_REQUEST['applications'] = '2,3';*/
        
        if (isset($_REQUEST['package']) && trim($_REQUEST['package']) && isset($_REQUEST['plan']) && trim($_REQUEST['plan'])) {
            $studio_id = Yii::app()->user->studio_id;
            $package_id = $_REQUEST['package'];
            $selected_plan = $_REQUEST['plan'];
            
            $package = Package::model()->findByPk($package_id);
            $yearly_discount = $package->yearly_discount;
            $package_code = $package->code;
            $price_total = $base_price = $package->base_price;
            $app_price = $package->price;
            
            $studio = $this->studio;
            
            $selectedpackage = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans', 'condition' => 'status = 1 AND package_id='.$package_id.' AND studio_id = ' . $studio_id, 'order' => 'id ASC'));
            $existing_plan = $selectedpackage[0]->plans;
            $no_of_applications = 0;
            
            if (isset($_REQUEST['applications']) && trim($_REQUEST['applications'])) {
                $newapplications = explode(',', $_REQUEST['applications']);
                $no_of_applications = count($newapplications);
                $allplans = $this->getOldAndNewApplications($selectedpackage, $newapplications);
                
                if ($existing_plan == $selected_plan) {
                    $is_charge = 0;
                    
                    if (!empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//Both new and old applications change (add new and remove old)
                        $is_charge = 1;
                    } else if (empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//No old applications but addition of new applications
                        $is_charge = 1;
                    }
                    
                    if (intval($is_charge)) {
                        $pricing = $this->getNewApplicationsPrice($allplans['newapplications']);
                        $billing_detail = $this->getProratedPriceWithNoPlanChange($studio, $pricing, $existing_plan, $yearly_discount);
                        $res['billing_amount'] = $billing_detail['billing_amount'];
                    }
                } else if ($existing_plan == 'Month' && $selected_plan == 'Year') {
                    
                    if (empty($allplans['oldapplications']) && empty($allplans['newapplications'])) {//No applications change
                        //Charge yearly plans amount
                        $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount,array(),array(),0, $package_code);
                    } else if (!empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//Both new and old applications change (add new and remove old)
                        //Charge yearly plans amount plus prorated amount of new applications to next billing date
                        $pricing = $this->getNewApplicationsPrice($allplans['newapplications']);
                        $total_amount = 0;

                        if (isset($pricing) && !empty($pricing)) {
                            foreach ($pricing as $key => $value) {
                                $total_amount = $total_amount + $value->price;
                            }
                        }

                        $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount, $allplans['oldapplications'], $allplans['newapplications'], $total_amount, $package_code);
                    } else if (!empty($allplans['oldapplications']) && empty($allplans['newapplications'])) {//Remove old applications but no addition of new applications
                        $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount, $allplans['oldapplications'], array(), 0, $package_code);
                    } else if (empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//No old applications but addition of new applications
                        //Charge yearly plans amount plus prorated amount of new applications to next billing date
                        $pricing = $this->getNewApplicationsPrice($allplans['newapplications']);
                        $total_amount = 0;

                        if (isset($pricing) && !empty($pricing)) {
                            foreach ($pricing as $key => $value) {
                                $total_amount = $total_amount + $value->price;
                            }
                        }

                        $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount, array(), $allplans['newapplications'], $total_amount, $package_code);
                    }

                    $res['billing_amount'] = $billing_detail['billing_amount'];
                }
            } else {
                if ($existing_plan == 'Month' && $selected_plan == 'Year') {
                    $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, array(), $base_price, $yearly_discount, array(), array(), 0, $package_code);
                    $res['billing_amount'] = $billing_detail['billing_amount'];
                }
            }
            
            if ($no_of_applications > 1) {
                $price_total = number_format((float) $price_total + (($no_of_applications - 1) * $app_price), 2, '.', '');
            }
            
            if (trim($selected_plan) == 'Year') {
                $price_total = number_format((float) (($price_total * 12) - ((($price_total * 12) * $yearly_discount) / 100)), 2, '.', '');
            }
            $res['price_total'] = $price_total;
        }
        //print '<pre>';print_r($res);exit;
        
        print json_encode($res);
        exit;
    }
    
    function actionUpdatePackages() {
        if (isset($_REQUEST['packages']) && !empty($_REQUEST['packages'])) {
            
            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = Yii::app()->user->id;

            //Get previous selected package, applications, plan
            $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans, package_code_id', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'id ASC'));
            if (isset($plans[0]->package_id) && ($plans[0]->package_id == intval($_REQUEST['packages']))) {
                $selpackages = Package::model()->getPackages($plans[0]->package_id);
                
                $package_name = $selpackages['package'][0]['name'];
                $package_code = $selpackages['package'][0]['code'];
                $package_codes = array();
                $base_price = $selpackages['package'][0]['base_price'];

                $yearly_discount = $selpackages['package'][0]['yearly_discount'];
                $existing_plan = $plans[0]->plans;
                $selected_plan = $_REQUEST['plans'];
                
                $studio = Studio::model()->findByPk($studio_id);

                if (isset($_REQUEST['applications']) && !empty($_REQUEST['applications'])) {
                    $newapplications = $_REQUEST['applications'];
                    $allplans = $this->getOldAndNewApplications($plans, $newapplications);

                    if ($existing_plan == $selected_plan) {
                        $is_charge = 0;

                        if (empty($allplans['oldapplications']) && empty($allplans['newapplications'])) {//No applications change
                            //Do nothing
                            Yii::app()->user->setFlash('error', "Oops! You haven't selected new options from package list");
                        } else if (!empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//Both new and old applications change (add new and remove old)
                            //Inactivate the old applications and charge prorated amount to new applications
                            //Inactivate the old applications
                            $inactivate = $this->inactivateOldApplicatons($allplans['oldapplications'], $studio_id, $user_id);

                            //Flag to charge prorated amount for new applications
                            $is_charge = 1;
                        } else if (!empty($allplans['oldapplications']) && empty($allplans['newapplications'])) {//Remove old applications but no addition of new applications
                            //Inactivate the old applications
                            $inactivate = $this->inactivateOldApplicatons($allplans['oldapplications'], $studio_id, $user_id);
                            Yii::app()->user->setFlash('success', 'Your package has been updated successfully.');
                        } else if (empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//No old applications but addition of new applications
                            //Charge prorated amount to new applications
                            //Flag to charge prorated amount for new applications
                            $is_charge = 1;
                        }

                        if (intval($is_charge)) {
                            $pricing = $this->getNewApplicationsPrice($allplans['newapplications']);
                            $billing_detail = $this->getProratedPriceWithNoPlanChange($studio, $pricing, $existing_plan, $yearly_discount);

                            $billing_amount = $billing_detail['billing_amount'];

                            $invoice['studio'] = $studio;
                            $invoice['package_name'] = @$package_name;
                            $invoice['package_code'] = @$selpackages['package'][0]['code'];
                            $invoice['plan'] = @$selected_plan;
                            $invoice['isPlanChange'] = 0;
                            $invoice['applications'] = @$billing_detail['applications'];
                            $invoice['from_to_date'] = $billing_detail['from_to_date'];
                            $invoice['yearly_discount'] = @$billing_detail['yearly_discount'];
                            $invoice['billing_amount'] = $billing_amount;
                            $invoice['pro_amount'] = $billing_detail['pro_amount'];
                            $invoice['discount'] = @$billing_detail['discount'];

                            $this->chargeProratedAmount($studio, $user_id, $plans, $billing_amount, $package_name, $selected_plan, $allplans['newapplications'], $invoice);
                        }
                    } else if ($existing_plan == 'Month' && $selected_plan == 'Year') {

                        //Getting remaining old applications
                        $allapps = '';

                        if (isset($allplans['remainingoldapplications']) && !empty($allplans['remainingoldapplications'])) {
                            $oldpricing = $this->getNewApplicationsPrice($allplans['remainingoldapplications']);
                            foreach ($oldpricing as $key => $value) {
                                $allapps.=$value->name . ', ';
                            }
                            $allapps = rtrim($allapps, ', ');
                        }

                        if (empty($allplans['oldapplications']) && empty($allplans['newapplications'])) {//No applications change
                            //Charge yearly plans amount
                            $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount,array(),array(),0,$package_code);
                        } else if (!empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//Both new and old applications change (add new and remove old)
                            //Inactivate the old applications and Charge yearly plans amount plus prorated amount of new applications to next billing date
                            //Inactivate the old applications
                            $inactivate = $this->inactivateOldApplicatons($allplans['oldapplications'], $studio_id, $user_id);

                            //Charge yearly plans amount plus prorated amount of new applications to next billing date
                            $pricing = $this->getNewApplicationsPrice($allplans['newapplications']);
                            $total_amount = 0;

                            $newapps = '';
                            if (isset($pricing) && !empty($pricing)) {
                                foreach ($pricing as $key => $value) {
                                    $total_amount = $total_amount + $value->price;
                                    $newapps.=$value->name . ', ';
                                }
                                $newapps = rtrim($newapps, ', ');

                                if (trim($allapps) && trim($newapps)) {
                                    $allapps = $allapps . ', ' . $newapps;
                                }
                            }

                            $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount, $allplans['oldapplications'], $allplans['newapplications'], $total_amount, $package_code);
                        } else if (!empty($allplans['oldapplications']) && empty($allplans['newapplications'])) {//Remove old applications but no addition of new applications
                            //Inactivate the old applications and Charge yearly plans amount for remaining old applications
                            //Inactivate the old applications
                            $inactivate = $this->inactivateOldApplicatons($allplans['oldapplications'], $studio_id, $user_id);

                            $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount, $allplans['oldapplications'], array(), 0, $package_code);
                        } else if (empty($allplans['oldapplications']) && !empty($allplans['newapplications'])) {//No old applications but addition of new applications
                            //Charge yearly plans amount plus prorated amount of new applications to next billing date
                            $pricing = $this->getNewApplicationsPrice($allplans['newapplications']);
                            $total_amount = 0;

                            $newapps = '';
                            if (isset($pricing) && !empty($pricing)) {
                                foreach ($pricing as $key => $value) {
                                    $total_amount = $total_amount + $value->price;
                                    $newapps.=$value->name . ', ';
                                }
                                $newapps = rtrim($newapps, ', ');

                                if (trim($allapps) && trim($newapps)) {
                                    $allapps = $allapps . ', ' . $newapps;
                                }
                            }

                            $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, $allplans['selapplications'], $base_price, $yearly_discount, array(), $allplans['newapplications'], $total_amount, $package_code);
                        }
                        
                        //Delete all pause billing
                        PauseBilling::model()->deleteAll('studio_id = :studio_id', array(
                            ':studio_id' => $studio_id
                        ));

                        $billing_amount = $billing_detail['billing_amount'];

                        $invoice = $billing_detail;
                        $invoice['studio'] = $studio;
                        $invoice['package_name'] = @$package_name;
                        $invoice['package_code'] = @$selpackages['package'][0]['code'];
                        $invoice['plan'] = @$selected_plan;
                        $invoice['isPlanChange'] = 1;
                        $invoice['applications'] = @$allapps;
                        $invoice['newapplications'] = @$newapps;

                        $this->chargeProratedAmount($studio, $user_id, $plans, $billing_amount, $package_name, $selected_plan, $allplans['newapplications'], $invoice);
                    } else if ($existing_plan == 'Year' && $selected_plan == 'Month') {
                        //Do nothing, will implemented after client's demand
                        Yii::app()->user->setFlash('error', 'Plan yearly to monthly can not be updated.');
                    }
                } else {
                    if ($existing_plan == $selected_plan) {
                        //Do nothing
                        Yii::app()->user->setFlash('error', "Oops! You haven't selected new plans from package list");
                    } else if ($existing_plan == 'Month' && $selected_plan == 'Year') {
                        $billing_detail = $this->getProratedPriceWithMonthlyToYearly($studio, array(), $base_price, $yearly_discount, array(), array(), 0, $package_code);
                        
                        $billing_amount = $billing_detail['billing_amount'];
                        
                        $invoice = $billing_detail;
                        $invoice['studio'] = $studio;
                        $invoice['package_name'] = @$package_name;
                        $invoice['package_code'] = @$selpackages['package'][0]['code'];
                        $invoice['plan'] = @$selected_plan;
                        $invoice['isPlanChange'] = 1;
                        $invoice['applications'] = @$allapps;
                        $invoice['newapplications'] = @$newapps;
                        
                        $this->chargeProratedAmount($studio, $user_id, $plans, $billing_amount, $package_name, $selected_plan, array(), $invoice, $package_codes);
                    } else if ($existing_plan == 'Year' && $selected_plan == 'Month') {
                        //Do nothing, will implemented after client's demand
                        Yii::app()->user->setFlash('error', 'Plan yearly to monthly can not be updated.');
                    }
                }
            } else {//Package change and will implement later
                Yii::app()->user->setFlash('error', 'Oops! Package change is not allowed.');
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in updating of packages.');
        }

        $url = $this->createUrl('payment/packages');
        $this->redirect($url);
        exit;
    }

    function getOldAndNewApplications($plans, $newapplications) {
        $res = array();
        $selapplications = array();

        foreach ($plans as $key => $value) {
            $selapplications[] = $value->application_id;
        }

        $res['selapplications'] = $selapplications;
        $res['oldapplications'] = array_diff($selapplications, $newapplications);
        $res['remainingoldapplications'] = array_diff($selapplications, $res['oldapplications']);
        $res['newapplications'] = array_diff($newapplications, $selapplications);

        return $res;
    }

    function inactivateOldApplicatons($nextinactiveapplications, $studio_id, $user_id) {
        foreach ($nextinactiveapplications as $key => $value) {
            $oldplans = StudioPricingPlan::model()->findByAttributes(array('application_id' => $value, 'studio_id' => $studio_id, 'status'=> 1));
            $oldplans->status = 0;
            $oldplans->updated_by = $user_id;
            $oldplans->updated = new CDbExpression("NOW()");
            $oldplans->save();
        }
        
        return true;
    }

    function getNewApplicationsPrice($nextactiveapplications) {
        $ids = implode(',', $nextactiveapplications);
        $pricing = Package::model()->findAll(array('condition' => 'status = 1 AND id IN (' . $ids . ')'));

        return $pricing;
    }

    function getProratedPriceWithNoPlanChange($studio, $pricing, $existing_plan, $yearly_discount = '') {
        $result = array();

        $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
        
        if(isset($existing_plan) && $existing_plan == "Month"){
            $fromDate =  Date('F d, Y', strtotime($billing_date . "-1 Months"));
        }else{
            $fromDate =  Date('F d, Y', strtotime($billing_date . "-1 Years"));
        }
        
        $result['from_to_date'] = 'From: ' . $fromDate . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . '-1 Days'));

        $last_dt = new DateTime($billing_date);
        $today = new DateTime(gmdate('Y-m-d H:i:s'));
        $interval = $last_dt->diff($today);
        $days = $interval->days + 1;

        $total_amount = 0;

        $allapps = '';
        if (isset($pricing) && !empty($pricing)) {
            foreach ($pricing as $key => $value) {
                $total_amount = $total_amount + $value->price;
                $allapps.=$value->name . ', ';
            }
            $allapps = rtrim($allapps, ', ');
            $result['applications'] = $allapps;
        }

        if ($existing_plan == "Year") {
            $total_days = 365;
            $yearly_pro = ($total_amount * 12);
            $billing_amount = ($days * $yearly_pro) / $total_days;
            $billing_amount = number_format((float) $billing_amount, 2, '.', '');
            $result['pro_amount'] = $billing_amount;

            $discount = number_format((float) (($billing_amount * $yearly_discount) / 100), 2, '.', '');
            $result['discount'] = $discount;
            $result['yearly_discount'] = $yearly_discount;

            $billing_amount = number_format((float) ($billing_amount - $discount), 2, '.', '');
        } else {
            $total_days = gmdate('t');
            $billing_amount = ($days * $total_amount) / $total_days;
            $billing_amount = number_format((float) $billing_amount, 2, '.', '');
            $result['pro_amount'] = $billing_amount;
        }

        $result['billing_amount'] = $billing_amount;

        return $result;
    }

    function getProratedPriceWithMonthlyToYearly($studio, $selapplications, $base_price, $yearly_discount = 0, $nextinactiveapplications = array(), $nextactiveapplications = array(), $total_amount = 0, $package_code) {
        $no_of_applications = 0;
        
        //Getting total number of active appliactions
        if (!empty($selapplications) && !empty($nextinactiveapplications) && !empty($nextactiveapplications)) {
            $no_of_applications = (count($selapplications) + count($nextactiveapplications)) - count($nextinactiveapplications);
        } else if (!empty($selapplications) && empty($nextinactiveapplications) && !empty($nextactiveapplications)) {
            $no_of_applications = (count($selapplications) + count($nextactiveapplications));
        } else if (!empty($selapplications) && !empty($nextinactiveapplications) && empty($nextactiveapplications)) {
            $no_of_applications = count($selapplications) - count($nextinactiveapplications);
        } else if (!empty($selapplications) && empty($nextinactiveapplications) && empty($nextactiveapplications)) {
            $no_of_applications = count($selapplications);
        }

        $result = array();
        $result['pro_from_to_date'] = (intval($no_of_applications)) ? 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date)) : '';
        $result['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . "+1 Years -1 Days"));
        $result['no_of_applications'] = $no_of_applications;
        $app_price = Yii::app()->general->getAppPrice($package_code);
        
        //Calculate annual price with discount
        if (intval($no_of_applications) > 1) {
            $anual_amount = number_format((float) (($base_price + (($no_of_applications - 1) * $app_price)) * 12), 2, '.', '');
        } else {
            $anual_amount = number_format((float) ($base_price * 12), 2, '.', '');
        }
        
        $discount = number_format((float) (($anual_amount * $yearly_discount) / 100), 2, '.', '');
        $result['platform_amount'] = $anual_amount;

        $anual_amount = ($anual_amount - $discount);

        //Calculate pro amount of new app for next billing month (not year)
        $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));

        $last_dt = new DateTime($billing_date);
        $today = new DateTime(gmdate('Y-m-d H:i:s'));
        $interval = $last_dt->diff($today);
        $days = $interval->days + 1;

        $total_days = gmdate('t');
        $pro_amount = ($days * $total_amount) / $total_days;
        $result['pro_amount'] = number_format((float) $pro_amount, 2, '.', '');

        $billing_amount = $anual_amount + $pro_amount;
        $billing_amount = number_format((float) $billing_amount, 2, '.', '');

        $result['discount'] = $discount;
        $result['yearly_discount'] = $yearly_discount;
        $result['billing_amount'] = $billing_amount;

        return $result;
    }
    
    function chargeProratedAmount($studio, $user_id, $plans, $billing_amount, $package_name, $selected_plan, $nextactiveapplications = array(), $invoice = array(), $package_codes = array()) {
        $studio_id = $studio->id;
        
        //Geeting card info
        $card_info = CardInfos::model()->find('studio_id=:studio_id AND user_id=:user_id AND is_cancelled=:is_cancelled', array(':is_cancelled' => 0, ':studio_id' => $studio_id, ':user_id' => $user_id));
        if (isset($card_info) && !empty($card_info)) {
            $firstData = $this->processTransaction($card_info, $billing_amount);
            if ($firstData->isError()) {
                //Keeping card error logs
                $ceModel = New CardErrors;
                $ceModel->studio_id = $studio_id;
                $ceModel->response_text = serialize($firstData);
                $ceModel->transaction_type = 'Prorated Amount';
                $ceModel->created = new CDbExpression("NOW()");
                $ceModel->save();

                $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                Yii::app()->user->setFlash('error', $error_message);
            } else {
                if ($firstData->getBankResponseType() == 'S') {
                    //Insert new record for billing information
                    $biModel = New BillingInfos;
                    $biModel->studio_id = $studio_id;
                    $biModel->user_id = $user_id;
                    $uniqid = Yii::app()->common->generateUniqNumber();
                    $biModel->uniqid = $uniqid;
                    $biModel->title = 'Package updation fee for ' . $package_name;
                    $biModel->billing_date = new CDbExpression("NOW()");
                    $biModel->created_date = new CDbExpression("NOW()");
                    $biModel->billing_amount = $billing_amount;
                    $biModel->transaction_type = 2;
                    $biModel->paid_amount = $firstData->getAmount();
                    $biModel->is_paid = 2;
                    $biModel->save();

                    $billing_info_id = $biModel->id;

                    if (isset($nextactiveapplications) && !empty($nextactiveapplications)) {
                        foreach ($nextactiveapplications as $key => $value) {
                            $price = Package::model()->getApplications($plans[0]->package_id, $value);

                            $bdModel = New BillingDetails;
                            $bdModel->billing_info_id = $billing_info_id;
                            $bdModel->user_id = $user_id;
                            $bdModel->title = $price[0]['name'];
                            $bdModel->description = $price[0]['description'];
                            $amount = number_format((float) $price[0]['price'], 2, '.', '');
                            $bdModel->amount = $amount;
                            $bdModel->save();

                            $sppModel = New StudioPricingPlan;
                            $sppModel->studio_id = $studio_id;
                            $sppModel->package_id = $plans[0]->package_id;
                            $sppModel->application_id = $value;
                            $sppModel->plans = $selected_plan;
                            $sppModel->created_by = $user_id;
                            $sppModel->created = new CDbExpression("NOW()");
                            $sppModel->status = 1;
                            $sppModel->save();
                        }
                    } else {//Muvi Server
                        $package = Package::model()->getPackages($plans[0]->package_id);
                        
                        $package_code = $package['package'][0]['code'];
                        
                        $base_price = $package['package'][0]['base_price'];
                        
                        $bdModel = New BillingDetails;
                        $bdModel->billing_info_id = $billing_info_id;
                        $bdModel->user_id = $user_id;
                        $bdModel->title = $package['package'][0]['name'];
                        $bdModel->description = $package['package'][0]['name'].' platform fee';
                        $bdModel->amount = $base_price;
                        $bdModel->save();
                    }

                    //Update plan to all application accordingly
                    $sql = "UPDATE studio_pricing_plans SET plans='" . $selected_plan . "' WHERE status=1 AND studio_id=" . $studio_id;
                    $con = Yii::app()->db;
                    $ciData = $con->createCommand($sql)->execute();

                    //Insert new transaction record in transaction infos;
                    $tiModel = New TransactionInfos;

                    //Set datas for inserting new record in transaction info table whether it returns success or error
                    $tiModel->card_info_id = $card_info->id;
                    $tiModel->studio_id = $studio_id;
                    $tiModel->user_id = $user_id;
                    $tiModel->billing_info_id = $billing_info_id;
                    $tiModel->billing_amount = $billing_amount;
                    $tiModel->invoice_detail = $invoice_detail = "Change Muvi Add-ons";
                    $tiModel->is_success = 1;
                    $tiModel->invoice_id = $firstData->getTransactionTag();
                    $tiModel->order_num = $firstData->getAuthNumber();
                    $tiModel->paid_amount = $firstData->getAmount();
                    $tiModel->response_text = serialize($firstData);
                    $tiModel->transaction_type = 'Package updation fee for ' . $package_name;
                    $tiModel->created_date = new CDbExpression("NOW()");
                    $tiModel->save();
                    $biModel = New BillingInfos;
                    $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
                    
                    if(isset($plans[0]->plans) && $plans[0]->plans == "Month"){
                        $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s', strtotime($billing_date . "-1 Months"))));
                    }else{
                        $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s', strtotime($billing_date . "-1 Years"))));
                    }
                    
                    $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime($billing_date . '-1 Days'))));
                    
                    //Update next billing cycle if change monthly plan to yearly plan
                    if ((isset($selected_plan) && $selected_plan == "Year") && (isset($plans[0]->plans) && $plans[0]->plans == "Month")) {
                        $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
                        $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s', strtotime($billing_date))));
                        $next_billing_date = gmdate("Y-m-d", strtotime($billing_date . '+1 Years'));
                        $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime($next_billing_date . '-1 Days'))));
                        $studio->start_date = Date('Y-m-d H:i:s', strtotime($next_billing_date));
                        $studio->end_date = Date('Y-m-d H:i:s', strtotime($next_billing_date . "+1 Years -1 Days"));
                        $studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                        $studio->save();
                    }
                    
                    $user = User::model()->findByPk($user_id);
                    
                    $invoice['user'] = $user;
                    $invoice['card_info'] = $card_info;
                    $invoice['billing_info_id'] = $billing_info_id;

                    if (isset($invoice['isPlanChange']) && intval($invoice['isPlanChange'])) {
                        $pdf = Yii::app()->pdf->adminUpdatePackageWithPlanChangeInvoiceDetial($invoice);
                    } else {
                        $pdf = Yii::app()->pdf->adminUpdatePackageWithoutPlanChangeInvoiceDetial($invoice);
                    }

                    $file_name = $pdf['pdf'];

                    //Update sbscription in studio table if tranasction is going to success or not.
                    $biModel = New BillingInfos;
                    $biModel->updateByPk($billing_info_id, array('detail' => $pdf['html']));

                    $req['name'] = $user->first_name;
                    $req['email'] = $user->email;
                    $req['companyname'] = $studio->name;
                    $req['package_name'] = @$package_name;
                    $req['applications'] = @$invoice['applications'];
                    $req['plan'] = @$selected_plan;
                    $req['amount_charged'] = @$billing_amount;

                    Yii::app()->email->subscriptionUpdatedMailToUser($req, $file_name);
                    Yii::app()->email->subscriptionUpdatedMailToSales($req);

                    Yii::app()->user->setFlash('success', 'Your package has been updated successfully.');
                } else {
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData);
                    $ceModel->transaction_type = 'Package updation fee for ' . $package_name;
                    $ceModel->created = new CDbExpression("NOW()");
                    $ceModel->save();

                    $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                    Yii::app()->user->setFlash('error', $error_message);
                }
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Please add card information.');
        }

        return true;
    }

    function authenticateToCard($data = array()) {
        if (isset($data) && !empty($data)) {
            if (@IS_DEMO_PAYMENT == 1) {
                $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
            } else {
                if (IS_LIVE_FIRSTDATA) {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                } else {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                }
            }

            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardNumber($data['card_number'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            //->setCreditCardType($data['type'])
            //->setReferenceNumber("Reactivate Charge");
            if ($data['zip']) {
                $firstData->setCreditCardZipCode($data['zip']);
            }

            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }

            if ($data['address']) {
                $firstData->setCreditCardAddress($data['address']);
            }

            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    }

    function chargeToCard($data = array()) {
        if (isset($data) && !empty($data)) {
            if (@IS_DEMO_PAYMENT == 1) {
                $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
            } else {
                if (IS_LIVE_FIRSTDATA) {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                } else {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                }
            }

            $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
            $firstData->setTransArmorToken($data['token'])
                    ->setCreditCardType($data['card_type'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    }

    function processTransaction($card_info, $billing_amount) {
        $data = array();
        $data['token'] = $card_info->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
        $data['name'] = $card_info->card_holder_name;
        $data['card_type'] = $card_info->card_type;
        $data['exp'] = ($card_info->exp_month < 10) ? '0' . $card_info->exp_month . substr($card_info->exp_year, -2) : $card_info->exp_month . substr($card_info->exp_year, -2);
        $data['billing_amount'] = $billing_amount;

        if (@IS_DEMO_PAYMENT == 1) {
            $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
        } else {
            if (IS_LIVE_FIRSTDATA) {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
            } else {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
            }
        }

        $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
        $firstData->setTransArmorToken($data['token'])
                ->setCreditCardType($data['card_type'])
                ->setCreditCardName($data['name'])
                ->setCreditCardExpiration($data['exp'])
                ->setAmount($data['billing_amount']);

        $firstData->process();

        return $firstData;
    }

    function actionChargeManually() {
        if (isset($_REQUEST['studio']) && intval($_REQUEST['studio'])) {
            $card_info = CardInfos::model()->find(array('condition' => 'studio_id = ' . $_REQUEST['studio'] . ' AND is_cancelled= 0', 'order' => 'is_cancelled ASC'));

            if (isset($card_info) && !empty($card_info)) {
                $data = array();
                $data['token'] = $card_info->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['card_name'] = $card_info->card_holder_name;
                $data['card_type'] = $card_info->card_type;
                $data['exp'] = ($card_info->exp_month < 10) ? '0' . $card_info->exp_month . substr($card_info->exp_year, -2) : $card_info->exp_month . substr($card_info->exp_year, -2);

                $data['amount'] = (isset($_REQUEST['amount']) && intval($_REQUEST['amount'])) ? $_REQUEST['amount'] : 0;

                if (intval($data['amount']) == 0) {
                    print "Zero amount can't be transacted!";
                    exit;
                }

                print '<pre>';
                print_r($data);

                $respnce = $this->chargeToCard($data);

                if ($respnce->isError()) {
                    print 'Error';
                    print '<pre>';
                    print_r($respnce);
                    exit;
                } else {
                    if ($respnce->getBankResponseType() == 'S') {
                        print 'Success';
                        print '<pre>';
                        print_r($respnce);
                        exit;
                    } else {
                        print 'Declined Error';
                        print '<pre>';
                        print_r($respnce);
                        exit;
                    }
                }
            } else {
                print 'No card information found!';
                exit;
            }
        }
    }

    function refundAuthCharge($data = array()) {
        $res = array();
        if (isset($data) && !empty($data)) {
            if (@IS_DEMO_PAYMENT == 1) {
                $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
            } else {
                if (IS_LIVE_FIRSTDATA) {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                } else {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                }
            }
            $firstData->setTransactionType(FirstData::TRAN_REFUND);

            $firstData->setCreditCardType($data['cardtype'])
                    ->setCreditCardNumber($data['number'])
                    ->setTransArmorToken($data['token'])
                    ->setCreditCardName($data['name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            $firstData->process();

            // Check
            if ($firstData->isError()) {
                $res['Code'] = $firstData->getErrorCode();
                $res['Message'] = $firstData->getErrorMessage();
            } else {
                $res['Code'] = $firstData->getBankResponseCode();
                $res['Message'] = $firstData->getBankResponseMessage();
            }
        }
        return $res;
    }

    function actionPaymentHistory() {
        $this->breadcrumbs = array('Billing', 'Payment History');
        $this->pageTitle = "Muvi | Payment History";
        $page_size = 6;
        $reseller_account_name = '';
        if (intval($this->studio->reseller_id)) {
            $seller_app_id = PortalUser::model()->findByPk($this->studio->reseller_id)->seller_app_id;
            $reseller_account_name = SellerApplication::model()->findByAttributes(array('id' => $seller_app_id))->company_name;
    }
        $this->render('paymenthistory',array('page_size'=>$page_size, 'reseller_account_name'=>$reseller_account_name));
    }
    
    function actionPaymentHistoryData(){
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $page_size = 6;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $billing_infos = BillingInfos::model()->getBillingInfos($studio_id,$user_id,$offset,$page_size);
        if (isset($billing_infos) && !empty($billing_infos)) {
            foreach ($billing_infos['data'] as $key => $value) {
                (array) $billing_details = BillingInfos::model()->getBillingDetails($value['id']);
                $billing_infos['data'][$key]['billing_details'] = $billing_details;
            }
        }
        $this->renderPartial('paymenthistorydata', array('data' => $billing_infos, 'page_size' => $page_size, 'pages' => $pages));
    }

    function actionPayBill() {
        if (isset($_REQUEST['bill']) && trim($_REQUEST['bill']) != '') {
            $this->breadcrumbs = array('Pay Bill');
            $this->pageTitle = "Muvi | Pay Bill";

            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $billing_infos = BillingInfos::model()->findByAttributes(array('uniqid' => trim($_REQUEST['bill']), 'studio_id' => $studio_id, 'is_paid' => 0));

            if (isset($billing_infos) && !empty($billing_infos)) {
                $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
                $billing_details = BillingDetails::model()->find(array('condition' => 'billing_info_id = ' . $billing_infos->id . ' AND user_id = ' . $user_id));

                $this->render('paybill', array('card' => $card, 'billing_infos' => $billing_infos, 'billing_details' => $billing_details));
            } else {
                $url = $this->createUrl('payment/paymenthistory');
                Yii::app()->user->setFlash('error', 'Oops! There is no bills found!');
                $this->redirect($url);
            }
        } else {
            $url = $this->createUrl('payment/paymenthistory');
            Yii::app()->user->setFlash('error', 'Oops! There is no bills found!');
            $this->redirect($url);
        }
    }

    function actionPrintReceipt() {

        if (isset($_REQUEST['bill']) && trim($_REQUEST['bill']) != '') {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $billing_infos = BillingInfos::model()->findByAttributes(array('uniqid' => trim($_REQUEST['bill']), 'studio_id' => $studio_id, 'is_paid' => 2));

            if (isset($billing_infos) && !empty($billing_infos)) {
                $studio = Studios::model()->findByPk($studio_id);
                
                $invoice['studio'] = $studio;
                $invoice['html'] = $billing_infos->detail;
                $invoice['isEmail'] = 0;
                $invoice['billing_date'] = gmdate('F_d_Y', strtotime($billing_infos->billing_date));

                $pdf = Yii::app()->pdf->generatePdf($invoice);
                print $pdf;
                exit;
            } else {
                $url = $this->createUrl('payment/paymenthistory');
                Yii::app()->user->setFlash('error', 'Oops! There is no bills found!');
                $this->redirect($url);
            }
        } else {
            $url = $this->createUrl('payment/paymenthistory');
            Yii::app()->user->setFlash('error', 'Oops! There is no bills found!');
            $this->redirect($url);
        }
    }

    function actionAuthenticateAndMakePayment() {
        if (isset($_REQUEST['card_number']) && !empty($_REQUEST['card_number'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $config = Yii::app()->common->getConfigValue(array('monthly_charge'), 'value');

            //Getting billing detail
            $uniqid = trim($_POST['billing_uniqid']);
            $billing_infos = BillingInfos::model()->findByAttributes(array('uniqid' => $uniqid, 'studio_id' => $studio_id, 'is_paid' => 0));
            $billing_info_id = $billing_infos->id;
            $billing_amount = $billing_infos->billing_amount;

            $billing_details = BillingDetails::model()->find(array('condition' => 'billing_info_id = ' . $billing_info_id . ' AND user_id = ' . $user_id));
            $invoice_detail = $billing_details->title;

            $dataInput = array();

            $dataInput['card_number'] = $_REQUEST['card_number'];
            $dataInput['card_name'] = $_REQUEST['card_name'];
            $dataInput['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
            $dataInput['amount'] = 0;
            $dataInput['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';
            $dataInput['cvv'] = $_REQUEST['cvv'];
            $address = $_REQUEST['address1'];
            $address .= (trim($_REQUEST['address2'])) ? ", " . trim($_REQUEST['address2']) : '';
            $address .= ", " . $_REQUEST['city'];
            $address .= ", " . $_REQUEST['state'];
            $dataInput['address'] = $address;

            $firstData_preauth = $this->authenticateToCard($dataInput);

            $card_last_fourdigit = str_replace(substr($dataInput['card_number'], 0, strlen($dataInput['card_number']) - 4), str_repeat("#", strlen($dataInput['card_number']) - 4), $dataInput['card_number']);

            $studio = Studios::model()->findByPk($studio_id);
            $user = User::model()->findByPk($user_id);

            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['phone'] = $user->phone_no;
            $req['companyname'] = $studio->name;
            $req['domain'] = 'http://' . $studio->domain;
            $req['card_last_fourdigit'] = $card_last_fourdigit;
            $req['invoice_title'] = $invoice_detail;
            $req['invoice_amount'] = $billing_amount;

            if ($firstData_preauth->isError()) {
                $res['isSuccess'] = 0;
                $res['Code'] = $firstData_preauth->getErrorCode();
                $res['Message'] = 'Invalid Card details entered. Please check again';

                $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();

                //Keeping card error logs
                $ceModel = New CardErrors;
                $ceModel->studio_id = $studio_id;
                $ceModel->response_text = serialize($firstData_preauth);
                $ceModel->transaction_type = 'Card Authentication And Payment';
                $ceModel->created = new CDbExpression("NOW()");
                $ceModel->save();

                //Send an email to developer and support
                Yii::app()->email->errorMessageMailToSales($req);
            } else {
                $respons_type = $firstData_preauth->getBankResponseType();

                if ($respons_type == 'S') {
                    $token = $firstData_preauth->getTransArmorToken();
                    $card_type = $firstData_preauth->getCreditCardType();

                    $data = array();
                    $data['token'] = $token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                    $data['card_name'] = $_REQUEST['card_name'];
                    $data['card_type'] = $card_type;
                    $data['exp'] = $dataInput['exp'];
                    $data['amount'] = $billing_amount;
                    $data['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
                    $data['cvv'] = $_REQUEST['cvv'];

                    $ciModel = New CardInfos;
                    $ciModel->user_id = $user_id;
                    $ciModel->studio_id = $studio_id;
                    $ciModel->payment_method = 'first_data';
                    $ciModel->plan_amt = $config[0]['value'];
                    $ciModel->card_holder_name = $_REQUEST['card_name'];
                    $ciModel->exp_month = $_REQUEST['exp_month'];
                    $ciModel->exp_year = $_REQUEST['exp_year'];
                    $address = $_REQUEST['address1'];
                    $ciModel->address1 = $address;
                    $ciModel->address2 = $data['address2'] = $_REQUEST['address2'];
                    $ciModel->city = (isset($_REQUEST['city']) && trim($_REQUEST['city'])) ? $_REQUEST['city'] : '';
                    $ciModel->state = (isset($_REQUEST['state']) && trim($_REQUEST['state'])) ? $_REQUEST['state'] : '';
                    $ciModel->zip = $data['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';

                    $firstData = $this->chargeToCard($data);

                    if ($firstData->isError()) {
                        $res['isSuccess'] = 0;
                        $res['Code'] = $firstData->getErrorCode();
                        $res['Message'] = 'Invalid Card details entered. Please check again';

                        $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

                        //Keeping card error logs
                        $ceModel = New CardErrors;
                        $ceModel->studio_id = $studio_id;
                        $ceModel->response_text = serialize($firstData);
                        $ceModel->transaction_type = 'Card Authentication And Payment';
                        $ceModel->created = new CDbExpression("NOW()");
                        $ceModel->save();

                        //Send an email to developer and support
                        Yii::app()->email->errorMessageMailToSales($req);
                    } else {

                        if ($firstData->getBankResponseType() == 'S') {
                            $res['isSuccess'] = 1;
                            $res['TransactionRecord'] = $firstData->getTransactionRecord();
                            $res['Code'] = $firstData->getBankResponseCode();

                            //Update all card to inactive mode to make primary for current card
                            $con = Yii::app()->db;
                            $sql = "UPDATE card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                            $ciData = $con->createCommand($sql)->execute();

                            $ciModel->is_cancelled = 0;
                            $ciModel->status = $res['Message'] = $firstData->getBankResponseMessage();
                            $ciModel->reference_no = $firstData->getTransactionTag();
                            $ciModel->card_type = $card_type;
                            $ciModel->card_last_fourdigit = $card_last_fourdigit;
                            $ciModel->auth_num = $firstData->getAuthNumber();
                            $ciModel->token = $token;

                            $ciModel->response_text = serialize($firstData_preauth);
                            $ciModel->ip = CHttpRequest::getUserHostAddress();
                            $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                            $ciModel->created = new CDbExpression("NOW()");
                            $ciModel->save();

                            //Insert new transaction record in transaction infos;
                            $tiModel = New TransactionInfos;

                            //Set datas for inserting new record in transaction info table whether it returns success or error
                            $tiModel->card_info_id = $ciModel->id;
                            $tiModel->studio_id = $studio_id;
                            $tiModel->user_id = $user_id;
                            $tiModel->billing_info_id = $billing_info_id;
                            $tiModel->billing_amount = $billing_amount;
                            $tiModel->invoice_detail = $invoice_detail;
                            $tiModel->is_success = 1;
                            $tiModel->invoice_id = $firstData->getTransactionTag();
                            $tiModel->order_num = $firstData->getAuthNumber();
                            $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                            $tiModel->response_text = serialize($firstData);
                            $tiModel->transaction_type = 'Card Authentication and Payment';
                            $tiModel->created_date = new CDbExpression("NOW()");
                            $tiModel->save();

                            if (abs(($billing_amount - $paid_amount) / $paid_amount) < 0.00001) {
                                $is_paid = 2;
                            } else {
                                $is_paid = 1;
                            }

                            $invoice['studio'] = $studio;
                            $invoice['user'] = $user;
                            $invoice['card_info'] = $ciModel;
                            $invoice['billing_info_id'] = $billing_info_id;
                            $invoice['billing_amount'] = $billing_amount;
                            $pdf = Yii::app()->pdf->adminRaiseInvoiceDetial($invoice);
                            $file_name = $pdf['pdf'];

                            //Update subscription in studio table if tranasction is going to success or not.
                            $billing_infos->paid_amount = $paid_amount;
                            $billing_infos->is_paid = $is_paid;
                            $billing_infos->transaction_type = 5;
                            $billing_infos->detail = $pdf['html'];
                            $billing_infos->billing_date = Date('Y-m-d H:i:s');
                            $billing_infos->save();

                            //Insert new record in StudioPricingPlan if its a lead
                            /* if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 0 && $studio->status ==1) {//For Lead only
                              $pricing = Configuration::model()->findAll(array('condition' => "status = 1 AND type = 1 AND code= 'monthly_charge'"));

                              $sppModel = New StudioPricingPlan;
                              $sppModel->studio_id = $studio_id;
                              $sppModel->config_id = $pricing->id;
                              $sppModel->created_by = $user_id;
                              $sppModel->created = new CDbExpression("NOW()");
                              $sppModel->status = 1;
                              $sppModel->save();

                              //Save subscription info if payment has processed successfully
                              $studio->status = 1;
                              $studio->is_subscribed = 1;
                              $studio->is_deleted = 0;
                              $studio->subscription_start = Date('Y-m-d H:i:s');
                              $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                              $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                              $studio->save();
                              } else { */
                            //Save subscription info if payment has processed successfully
                            $studio->status = 1;
                            $studio->is_subscribed = 1;
                            $studio->is_deleted = 0;
                            $studio->save();
                            //}

                            Yii::app()->email->raiseInvoicePaidMailToUser($req, $file_name);
                            Yii::app()->email->raiseInvoicePaidMailToSales($req);
                        } else {
                            $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                            $res['isSuccess'] = 0;
                            $res['Code'] = 55;
                            $res['Message'] = $error_message;
                        }
                    }
                } else {
                    $error_message = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
                    $res['isSuccess'] = 0;
                    $res['Code'] = 55;
                    $res['Message'] = $error_message;
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        print json_encode($res);
        exit;
    }

    function actionMakePayment() {
        if (isset($_POST['payment_info']) && $_POST['payment_info'] == 'saved') {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $card_info = CardInfos::model()->findByAttributes(array('id' => $_POST['creditcard'], 'studio_id' => $studio_id, 'user_id' => $user_id));

            if (isset($card_info) && !empty($card_info)) {
                //Getting billing detail
                $uniqid = trim($_POST['billing_uniqid']);
                $billing_infos = BillingInfos::model()->findByAttributes(array('uniqid' => $uniqid, 'studio_id' => $studio_id, 'is_paid' => 0));
                $billing_info_id = $billing_infos->id;
                $billing_amount = $billing_infos->billing_amount;

                $billing_details = BillingDetails::model()->find(array('condition' => 'billing_info_id = ' . $billing_info_id . ' AND user_id = ' . $user_id));
                $invoice_detail = $billing_details->title;

                $data = array();
                $data['token'] = $card_info->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['name'] = $card_info->card_holder_name;
                $data['card_type'] = $card_info->card_type;
                $data['exp'] = ($card_info->exp_month < 10) ? '0' . $card_info->exp_month . substr($card_info->exp_year, -2) : $card_info->exp_month . substr($card_info->exp_year, -2);
                $data['amount'] = $billing_infos->billing_amount;

                if (@IS_DEMO_PAYMENT == 1) {
                    $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
                } else {
                    if (IS_LIVE_FIRSTDATA) {
                        $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                    } else {
                        $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                    }
                }

                $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
                $firstData->setTransArmorToken($data['token'])
                        ->setCreditCardType($data['card_type'])
                        ->setCreditCardName($data['name'])
                        ->setCreditCardExpiration($data['exp'])
                        ->setAmount($data['amount']);

                $firstData->process();
                if ($firstData->isError()) {
                    //Keeping card error logs
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData);
                    $ceModel->transaction_type = $invoice_detail;
                    $ceModel->created = new CDbExpression("NOW()");
                    $ceModel->save();

                    $url = $this->createUrl('payment/paybill/bill/' . $uniqid);
                    $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                    Yii::app()->user->setFlash('error', $error_message);
                } else {
                    if ($firstData->getBankResponseType() == 'S') {
                        //Insert new transaction record in transaction infos;
                        $tiModel = New TransactionInfos;

                        //Set datas for inserting new record in transaction info table whether it returns success or error
                        $tiModel->card_info_id = $card_info->id;
                        $tiModel->studio_id = $studio_id;
                        $tiModel->user_id = $user_id;
                        $tiModel->billing_info_id = $billing_info_id;
                        $tiModel->billing_amount = $billing_amount;
                        $tiModel->invoice_detail = $invoice_detail;
                        $tiModel->is_success = 1;
                        $tiModel->invoice_id = $firstData->getTransactionTag();
                        $tiModel->order_num = $firstData->getAuthNumber();
                        $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                        $tiModel->response_text = serialize($firstData);
                        $tiModel->transaction_type = $invoice_detail;
                        $tiModel->created_date = new CDbExpression("NOW()");
                        $tiModel->save();

                        if (abs(($billing_amount - $paid_amount) / $paid_amount) < 0.00001) {
                            $is_paid = 2;
                        } else {
                            $is_paid = 1;
                        }

                        $studio = Studios::model()->findByPk($studio_id);
                        $user = User::model()->findByPk($user_id);
                        
                        $invoice['studio'] = $studio;
                        $invoice['user'] = $user;
                        $invoice['card_info'] = $card_info;
                        $invoice['billing_info_id'] = $billing_info_id;
                        $invoice['billing_amount'] = $billing_amount;
                        $pdf = Yii::app()->pdf->adminRaiseInvoiceDetial($invoice);
                        $file_name = $pdf['pdf'];

                        //Update subscription in studio table if tranasction is going to success or not.
                        $billing_infos->paid_amount = $paid_amount;
                        $billing_infos->is_paid = $is_paid;
                        $billing_infos->detail = $pdf['html'];
                        $billing_infos->billing_date = Date('Y-m-d H:i:s');
                        $billing_infos->transaction_type = 5;
                        $billing_infos->save();

                        //Insert new record in StudioPricingPlan if its a lead
                        /* if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 0 && $studio->status ==1) {//For Lead only
                          $pricing = Configuration::model()->findAll(array('condition' => "status = 1 AND type = 1 AND code= 'monthly_charge'"));

                          $sppModel = New StudioPricingPlan;
                          $sppModel->studio_id = $studio_id;
                          $sppModel->config_id = $pricing->id;
                          $sppModel->created_by = $user_id;
                          $sppModel->created = new CDbExpression("NOW()");
                          $sppModel->status = 1;
                          $sppModel->save();

                          //Save subscription info if payment has processed successfully
                          $studio->status = 1;
                          $studio->is_subscribed = 1;
                          $studio->is_deleted = 0;
                          $studio->subscription_start = Date('Y-m-d H:i:s');
                          $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                          $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                          $studio->save();
                          } else { */
                        //Save subscription info if payment has processed successfully
                        $studio->status = 1;
                        $studio->is_subscribed = 1;
                        $studio->is_deleted = 0;
                        $studio->save();
                        //}
                        //Update card to inactive mode for making primary
                        $sql = "UPDATE card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                        $con = Yii::app()->db;
                        $ciData = $con->createCommand($sql)->execute();

                        //Make primary
                        $card_info->is_cancelled = 0;
                        $card_info->save();

                        $req['name'] = $user->first_name;
                        $req['email'] = $user->email;
                        $req['companyname'] = $studio->name;
                        $req['invoice_title'] = $invoice_detail;
                        $req['invoice_amount'] = $billing_amount;

                        Yii::app()->email->raiseInvoicePaidMailToUser($req, $file_name);
                        Yii::app()->email->raiseInvoicePaidMailToSales($req);

                        $url = $this->createUrl('payment/paymentHistory');
                        Yii::app()->user->setFlash('success', 'Thank you, Your payment has been processed successfully.');
                    } else {
                        $url = $this->createUrl('payment/paybill/bill/' . $uniqid);
                        $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                        Yii::app()->user->setFlash('error', $error_message);
                    }
                }
            } else {
                $url = $this->createUrl('payment/managePayment');
                Yii::app()->user->setFlash('error', 'Oops! Please set credit card to primary in payment information');
            }
        } else {
            $url = $this->createUrl('payment/paymentHistory');
            Yii::app()->user->setFlash('error', 'Oops! Invalid Credit Card!');
        }

        $this->redirect($url);
    }

    function actionCancelSubscription() {
        if (isset($_REQUEST['cancel_reason']) && trim($_REQUEST['cancel_reason'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;
            $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');

            $studio = Studios::model()->findByPk($studio_id);
            $user = User::model()->findByPk($user_id);

            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['phone'] = $user->phone_no;
            $req['companyname'] = $studio->name;
            $req['domain'] = 'http://' . $studio->domain;
            $req['cancel_reason'] = $_REQUEST['cancel_reason'];
            $req['custom_notes'] = trim($_REQUEST['custom_notes']);

            //Deduct the bandwidth charge from studio(Customer) and sent invoice detail to user
            $invoice_file = "";
            if (@IS_DEMO_PAYMENT == 1) {
                
            } else {
                if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 1 && $studio->status == 1) {
                    //if (intval($studio_id) != 4302) {
                        $invoice_file = $this->chargeBandwidthCost($studio, $user_id);
                    //}
                }
            }

            //Insert new record in StudioCancelReason table
            $scrModel = New StudioCancelReason;
            $scrModel->studio_id = $studio_id;
            $scrModel->reason = $_REQUEST['cancel_reason'];
            $scrModel->custom_notes = trim($_REQUEST['custom_notes']);
            $scrModel->ip = CHttpRequest::getUserHostAddress();
            $scrModel->created_by = $user_id;
            $scrModel->created_date = gmdate('Y-m-d');
            $scrModel->save();

            //Update card to inactive mode
            $sql = "UPDATE card_infos SET is_cancelled=2, cancelled_date=NOW() WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
            $con = Yii::app()->db;
            $ciData = $con->createCommand($sql)->execute();

            //Update studio pricing plans
            $sqlplan = "UPDATE studio_pricing_plans SET status=0, updated_by=" . $user_id . ", updated=NOW() WHERE studio_id=" . $studio_id;
            $planData = $con->createCommand($sqlplan)->execute();

            //Delete all pause billing
            PauseBilling::model()->deleteAll('studio_id = :studio_id', array(
                ':studio_id' => $studio_id
            ));
            
            //Deactivate all users like admin and members
            $sql_usr = "UPDATE user SET is_active=0 WHERE studio_id=" . $studio_id . " AND id !=" . $user_id;
            $con->createCommand($sql_usr)->execute();

            //Update studio status with hold period
            $studio->status = 4;
            $studio->is_subscribed = 0;
            $studio->start_date = Date('Y-m-d H:i:s');
            $studio->end_date = Date('Y-m-d H:i:s', strtotime("+{$config[0]['value']} days"));
            $studio->payment_status = 0;
            $studio->partial_failed_date = '';
            $studio->partial_failed_due = 0;
            $studio->save();

            Yii::app()->email->cancelSubscriptionMailToSales($req);
            Yii::app()->email->cancelSubscriptionMailToUser($req, $invoice_file);

            Yii::app()->user->setFlash('success', 'You have successfully cancelled subscription.');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in canceling of subscription.');
        }

        $url = $this->createUrl('admin/account');
        $this->redirect($url);
        exit;
    }
    
    function actionBandwidthTest() {
        $bandwidth = Yii::app()->aws->getLocationBandwidthSize(331, '2016-02-01 09:09:09', '2016-02-12  09:09:09', '', 1);
        $bandwidth['is_bandwidth'] = 1;
        
        $data = Yii::app()->aws->getBandwidthPrice($bandwidth);
        //print '<pre>';print_r($data);exit;
        
        if (isset($data) && !empty($data)) {
            if (intval($bandwidth['is_bandwidth']) && isset($data['bandwidth'])) {
                $bandwidth_detail = Yii::app()->pdf->getBandwidthDetail($data['bandwidth'], 1);
            } else {
                $bandwidth_detail = Yii::app()->pdf->getBandwidthDetail($data, 0);
            }
            
            $bandwidth_charge = isset($data['price']) ? $data['price'] : 0;
        }
        print $bandwidth_detail.'<pre>';print_r($bandwidth_charge);
        
        $bandwidth_usage = (isset($data['total_billable_bandwidth_consumed']) && !empty($data['total_billable_bandwidth_consumed'])) ? $data['total_billable_bandwidth_consumed'] : 0;
        
        $total_storage = 62640;
        //$total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
        $storage = Yii::app()->aws->getStorageCost($total_storage, $bandwidth_usage);
        $storage_detail = Yii::app()->pdf->getStorageDetail($storage);
        print '<pre>';print_r($storage_detail);exit;
    }
    
    function actionStorageTest() {
        $studio_id = $studio->id;
        $bandwidth = 0;
        $total_storage = 11264;
        $$storage_detail = '';
        
        //$total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
        $storage = Yii::app()->aws->getStorageCost($total_storage, $bandwidth);
        $storage_detail = Yii::app()->pdf->getStorageDetail($storage);
        
        print '<pre>';print_r($storage_detail);exit;
    }
    
    function chargeBandwidthCost($studio, $user_id) {
        $studio_id = $studio->id;
        $plans = StudioPricingPlan::model()->find(array('select' => 'package_id', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'application_id ASC'));
        $bill_amount = 0;
        if (isset($plans) && !empty($plans)) {
            $package = Package::model()->getPackages($plans->package_id);
            $package_name = $package['package'][0]['name'];
            $bandwidth['package'] = $package['package'][0]['code'];
            
            if (isset($plans->plans) && $plans->plans == 'Year') {
                $billing_date = gmdate('Y-m-d', strtotime($studio->bandwidth_date));
            } else if (isset($plans->plans) && $plans->plans == 'Month') {
                $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
            }
            
            $last_billing_date = gmdate("Y-m-d", strtotime($billing_date . '-1 Months +1 Days'));
            $today_date = Date('Y-m-d');

            $bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, $last_billing_date, $today_date, '', $package['package'][0]['is_bandwidth']);
            $bandwidth['is_bandwidth'] = $package['package'][0]['is_bandwidth'];
            $bandwidth_price = Yii::app()->aws->getBandwidthPrice($bandwidth);
            
            $bandwidth_charge = 0;
            if (isset($bandwidth_price) && !empty($bandwidth_price)) {
                $bandwidth_charge = isset($bandwidth_price['price']) ? number_format((float) $bandwidth_price['price'], 2, '.', '') : 0;
            }
            if (abs($bandwidth_charge) > 0.01) {
                $billingArr['bandwidth']['bill_amount'] = $bandwidth_charge;
                $billingArr['bandwidth']['title'] = 'Bandwidth charge for ' . $package_name;
                $billingArr['bandwidth']['description'] = 'Bandwidth usage from ' . date('F d, Y', strtotime($last_billing_date)) . ' to ' . date('F d, Y');
                $billingArr['bandwidth']['is_bandwidth_storage'] = 1;
                
                $total_bandwidth_consumed = (isset($bandwidth_price['total_bandwidth_consumed'])) ? number_format((float) ($bandwidth_price['total_bandwidth_consumed']*1024), 3, '.', '') : 0;
                $total_billable_bandwidth_consumed = (isset($bandwidth_price['total_billable_bandwidth_consumed'])) ? number_format((float) ($bandwidth_price['total_billable_bandwidth_consumed']*1024), 3, '.', '') : 0;
                $bandwidth_usage = json_encode(array('total_bandwidth' => $total_bandwidth_consumed, 'total_billable_bandwidth' => $total_billable_bandwidth_consumed));

                $billingArr['bandwidth']['usage'] = $bandwidth_usage;
            }
            $bill_amount = number_format((float) ($bill_amount + $bandwidth_charge), 2, '.', '');
            
            $bandwidth_usage = (isset($bandwidth_price['total_bandwidth_consumed']) && !empty($bandwidth_price['total_bandwidth_consumed'])) ? $bandwidth_price['total_bandwidth_consumed'] : 0;
            $total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
            $storage = Yii::app()->aws->getStorageCost($total_storage, $bandwidth_usage);
            
            $storage_charge = 0;
            if(isset($storage) && !empty($storage)){
                $storage_charge = isset($storage['cost']) ? number_format((float) $storage['cost'], 2, '.', '') : 0;
            }
            if (abs($storage_charge) > 0.01) {
                $billingArr['storage']['bill_amount'] = $storage_charge;
                $billingArr['storage']['title'] = 'Storage charge for ' . $package_name;
                $billingArr['storage']['description'] = 'Storage usage from ' . date('F d, Y', strtotime($last_billing_date)) . ' to ' . date('F d, Y');
                
                $billingArr['storage']['is_bandwidth_storage'] = 2;
                
                $total_storage = (isset($storage['total_storage'])) ? number_format((float) ($storage['total_storage']*1024), 3, '.', '') : 0;
                $total_billable_storage = (isset($storage['billable_storage'])) ? number_format((float) ($storage['billable_storage']*1024), 3, '.', '') : 0;
                $storage_usage = array('total_storage' => $total_storage, 'total_billable_storage' => $total_billable_storage);

                $billingArr['storage']['usage'] = $storage_usage;
            }
            
            $bill_amount = number_format((float) ($bill_amount + $storage_charge), 2, '.', '');
            
            if (abs($bill_amount) > 0.01) {
                $card_info = CardInfos::model()->find('studio_id=:studio_id AND user_id=:user_id AND is_cancelled=:is_cancelled', array(':is_cancelled' => 0, ':studio_id' => $studio_id, ':user_id' => $user_id));
                if (isset($card_info) && !empty($card_info)) {

                    $firstData = $this->processTransaction($card_info, $bill_amount);
                    
                    if(isset($billingArr) && !empty($billingArr)){
                        //Insert new record for billing information
                        $biModel = New BillingInfos;
                        $biModel->studio_id = $studio_id = $studio->id;
                        $biModel->user_id = $user_id;
                        $uniqid = Yii::app()->common->generateUniqNumber();
                        $biModel->uniqid = $uniqid;
                        $biModel->title = 'Bandwidth/Storage charge for ' . $package_name;
                        $biModel->billing_date = new CDbExpression("NOW()");
                        $biModel->created_date = new CDbExpression("NOW()");
                        $biModel->billing_amount = $bill_amount;
                        $biModel->transaction_type = 4;
                        $biModel->save();
                        $billing_info_id = $biModel->id;
                        
                        foreach($billingArr as $value){
                            if(isset($value) && !empty($value)){
                                //Insert new records in bill detail
                                $bdModel = New BillingDetails;
                                $bdModel->billing_info_id = $billing_info_id;
                                $bdModel->user_id = $user_id;
                                $bdModel->title = $value['title'];
                                $bdModel->description = $value['description'];
                                $bdModel->amount = $value['bill_amount'];
                                $bdModel->type = $value['is_bandwidth_storage'];
                                $bdModel->remarks = $value['usage'];
                                $bdModel->save();
                            }
                        }
                    }
                    

                    //Insert new transaction record in transaction infos;
                    $tiModel = New TransactionInfos;

                    //Set datas for inserting new record in transaction info table whether it returns success or error
                    $tiModel->card_info_id = $card_info->id;
                    $tiModel->studio_id = $studio_id;
                    $tiModel->user_id = $user_id;
                    $tiModel->billing_info_id = $billing_info_id;
                    $tiModel->billing_amount = $bill_amount;
                    $tiModel->invoice_detail = 'Bandwidth/Storage charge for Muvi';
                    $tiModel->transaction_type = 'Bandwidth/Storage charge for Muvi';
                    if ($firstData->isError()) {
                        $tiModel->is_success = 0;
                        $tiModel->paid_amount = 0;

                        $isSuccess = 0;
                    } else {
                        $tiModel->is_success = 1;
                        $tiModel->invoice_id = $firstData->getTransactionTag();
                        $tiModel->order_num = $firstData->getAuthNumber();
                        $tiModel->paid_amount = $paid_amount = $firstData->getAmount();

                        $isSuccess = 1;
                    }

                    $tiModel->response_text = serialize($firstData);
                    $tiModel->created_date = new CDbExpression("NOW()");
                    $tiModel->save();

                    if (intval($isSuccess) == 1) {
                        //Check billing amount and paid amount is same or not, otherwise charge again.
                        if (abs(($bill_amount - $paid_amount) / $paid_amount) < 0.00001) {
                            $is_paid = 2;
                        } else {
                            $is_paid = 1;
                        }
                        
                        $user = User::model()->findByPk($user_id);
                        
                        $data['studio'] = $studio;
                        $data['user'] = $user;
                        $data['card_info'] = $card_info;
                        $data['billing_info_id'] = $billing_info_id;
                        $data['storage'] = isset($storage) ? $storage : array();
                        $data['billing_amount'] = $bill_amount;
                        $data['package_code'] = $package['package'][0]['code'];
                        if (abs($bandwidth_charge) > 0.01) {
                            $invoice['bandwidth']['bandwidth_amount'] = $bandwidth_charge;
                            $invoice['bandwidth']['bandwidth_type'] = $package['package'][0]['is_bandwidth'];
                            $invoice['bandwidth']['bandwidth'] = isset($bandwidth_price['bandwidth']) ? $bandwidth_price['bandwidth'] : array();
                            $invoice['bandwidth']['title'] = 'Bandwidth charge for ' . $package_name;
                            $invoice['billing_amount'] = $bill_amount;
                        }
                        if (abs($storage_charge) > 0.01) {
                            $invoice['storage']['storage'] = isset($storage) ? $storage : array();
                            $invoice['storage']['storage_amount'] = $storage_charge;
                            $invoice['storage']['title'] = 'Storage charge for ' . $package_name;
                            $invoice['billing_amount'] = $bill_amount;
                        }
                        $pdf = Yii::app()->pdf->adminRaiseInvoiceDetialOfBandwidthStorage($data,$invoice);

                        //Update sbscription in studio table if tranasction is going to success or not.
                        $biModel = New BillingInfos;
                        $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => $is_paid, 'detail' => $pdf['html']));

                        return $pdf['pdf'];
                    }
                }
            }
        }
    }
    function actionVerifyAndSaveCardTest() {
        $user_id = 1970;
        $studio_id = 1753;;
        $hours_purchased = 10;

        $studio = Studios::model()->findByPk($studio_id);
        $purchased_devhours = ($studio->purchased_devhours) + $hours_purchased;
        $remaining_devhours = ($studio->dev_hours) - $purchased_devhours;

        $user = User::model()->findByPk($user_id);
        $req = array();
        $req['name'] = $user->first_name;
        $req['email'] = 'rasmi@muvi.com';
        $req['dev_hour'] = $hours_purchased;
        $req['total_devhour'] = $purchased_devhours;
        $req['amount_paid'] = 400;

        //generate pdf                       
        $invoice['studio'] = $studio;
        $invoice['user'] = $user;
        $invoice['billing_info_id'] = 111;
        $invoice['billing_amount'] = 400;
        $invoice['hours_purchased'] = $hours_purchased;
        
        $pdf = Yii::app()->pdf->adminPurchaseDevhourInvoiceDetail($invoice);
        
        $file_name = $pdf['pdf'];
        
        $customeremail = Yii::app()->email->purchaseDevhourEmailtoCustomer($req, $file_name);
      
        $adminemail = Yii::app()->email->purchaseDevhourEmailtoAdmin($req);
       
        exit;
    }

    function actionVerifyAndSaveCard() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $hours_purchased = $_REQUEST['dev_hours'];

        $studio = Studios::model()->findByPk($studio_id);
        $purchased_devhours = ($studio->purchased_devhours) + $hours_purchased;
        $remaining_devhours = ($studio->dev_hours) - $purchased_devhours;
        
        //code for charging the new card
        if (isset($_REQUEST['card_number']) && !empty($_REQUEST['card_number']) && $_REQUEST['payment_type'] == 1) {
            $studio = Studios::model()->findByPk($studio_id);
            $user = User::model()->findByPk($user_id);

                //Check primary card is exist or not
                $cards = CardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'is_cancelled' => 0));

                $is_primary = 0;
                if (isset($cards) && !empty($cards)) {
                    $is_primary = 1;
                }

                $data = array();
                $data['card_number'] = $_REQUEST['card_number'];
                $data['card_name'] = $_REQUEST['card_name'];
                $data['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
                $data['amount'] = $_REQUEST['amount'];
                // $data['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';
                $data['cvv'] = $_REQUEST['cvv'];
                $address = $_REQUEST['address1'];
                $address .= (trim($_REQUEST['address2'])) ? ", " . trim($_REQUEST['address2']) : '';
                $address .= ", " . $_REQUEST['city'];
                $address .= ", " . $_REQUEST['state'];
                //$data['address'] = $address;
                //$data['address2'] = $_REQUEST['address2'];

                $firstData_preauth = $this->authenticateToCard($data);

                
               
                $card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);

                $req['name'] = $user->first_name;
                $req['email'] = $user->email;
                $req['phone'] = $user->phone_no;
                $req['companyname'] = $studio->name;
                $req['domain'] = 'http://' . $studio->domain;
                $req['card_last_fourdigit'] = $card_last_fourdigit;
                // echo '@before if';
                if ($firstData_preauth->isError()) {
                    //  echo '@if';
                    $res['isSuccess'] = 0;
                    $res['Code'] = $firstData_preauth->getErrorCode();
                    $res['Message'] = 'Invalid Card details entered. Please check again';

                    $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();

                    //Keeping card error logs
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData_preauth);
                    $ceModel->transaction_type = 'Verify and Save Card';
                    $ceModel->created = new CDbExpression("NOW()");
                    $ceModel->save();

                    Yii::app()->email->errorMessageMailToSales($req);
                } else {
                    //echo '@else';
                    if ($firstData_preauth->getBankResponseType() == 'S') {
                        //echo "test2";
                        $token = $firstData_preauth->getTransArmorToken();
                        $card_type = $firstData_preauth->getCreditCardType();
                        $res['isSuccess'] = 1;
                        $res['TransactionRecord'] = $firstData_preauth->getTransactionRecord();
                        $res['Code'] = $firstData_preauth->getBankResponseCode();

                        $ciModel = New CardInfos;
                        $ciModel->user_id = $user_id;
                        $ciModel->studio_id = $studio_id;
                        $ciModel->payment_method = 'first_data';
                        $ciModel->plan_amt = $_REQUEST['amount'];
                        $ciModel->card_number = $_REQUEST['card_number'];
                        $ciModel->card_holder_name = $_REQUEST['card_name'];
                        $ciModel->exp_month = $_REQUEST['exp_month'];
                        $ciModel->exp_year = $_REQUEST['exp_year'];
                        $ciModel->address1 = $_REQUEST['address1'];
                        $ciModel->address2 = $_REQUEST['address2'];
                        $ciModel->city = (isset($_REQUEST['city']) && trim($_REQUEST['city'])) ? $_REQUEST['city'] : '';
                        $ciModel->state = (isset($_REQUEST['state']) && trim($_REQUEST['state'])) ? $_REQUEST['state'] : '';
                        $ciModel->zip = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';
                        $ciModel->status = $res['Message'] = $firstData_preauth->getBankResponseMessage();
                        $ciModel->reference_no = $firstData_preauth->getTransactionTag();
                        $ciModel->card_type = $data['card_type'] = $firstData_preauth->getCreditCardType();
                        $ciModel->card_last_fourdigit = $data['card_last_fourdigit'] = $card_last_fourdigit;
                        $ciModel->auth_num = $firstData_preauth->getAuthNumber();
                        $ciModel->token = $data['token'] = $firstData_preauth->getTransArmorToken();
                        $ciModel->is_cancelled = $is_primary;
                        $ciModel->response_text = serialize($firstData_preauth);
                        $ciModel->ip = CHttpRequest::getUserHostAddress();
                        $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                        $ciModel->created = new CDbExpression("NOW()");
                        $ciModel->save();
                        $card_info_id = $ciModel->id;
                     
                    $data = array();
                    $data['token'] = $token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                    $data['card_name'] = $_REQUEST['card_name'];
                    $data['card_type'] = $card_type;
                    $data['card_number'] = $_REQUEST['card_number'];
                    $data['amount'] = $_REQUEST['amount'];
                    $data['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
                    $data['cvv'] = $_REQUEST['cvv'];

                        $firstData = $this->chargeToCard($data);
                        
                        if ($firstData->isError()) {
                            $res['isSuccess'] = 0;
                            $res['Code'] = $firstData->getErrorCode();
                            $res['Message'] = 'Invalid Card details entered. Please check again';
                            $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

                            //Keeping card error logs
                            $ceModel = New CardErrors;
                            $ceModel->studio_id = $studio_id;
                            $ceModel->response_text = serialize($firstData);
                            $ceModel->transaction_type = 'DevHours purchase Fee';
                            $ceModel->created = new CDbExpression("NOW()");
                            $ceModel->save();

                            //Send an email to developer and support
                            Yii::app()->email->errorMessageMailToSales($req);
                        } else if ($firstData->getBankResponseType() == 'S') {
                                
                            $res['isSuccess'] = 1;
                            $res['TransactionRecord'] = $firstData_preauth->getTransactionRecord();
                            $res['Code'] = $firstData_preauth->getBankResponseCode();

                            //insert records in to billing info once payment is success

                            $biModel = New BillingInfos;
                            $biModel->studio_id = $studio_id;
                            $biModel->user_id = $user_id;
                            $uniqid = Yii::app()->common->generateUniqNumber();
                            $biModel->uniqid = $uniqid;
                            $biModel->title = 'DevHours purchase Fee';
                            $biModel->billing_date = new CDbExpression("NOW()");
                            $biModel->created_date = new CDbExpression("NOW()");
                            $biModel->billing_amount = $_REQUEST['amount'];
                            $biModel->transaction_type = 6;
                            $biModel->paid_amount = $firstData->getAmount();
                            $biModel->is_paid = 2;
                            $biModel->save();

                            $billing_info_id = $biModel->id;
                            //echo $billing_info_id;
                            //insert records into billing_details table
                            $bidetails=new BillingDetails;
                          
                            $bidetails->user_id = $user_id;
                            $bidetails->billing_info_id=$billing_info_id;
                            $bidetails->title = 'DevHours purchase Fee';
                            $bidetails->description="Payment for DevHours:".$hours_purchased." hours";
                            $bidetails->amount =$_REQUEST['amount'];
                            $bidetails->type=4;
                            $bidetails->remarks=$hours_purchased;
                            $bidetails->save();
                            //Insert new transaction record in transaction infos;
                            $tiModel = New TransactionInfos;

                            //Set datas for inserting new record in transaction info table whether it returns success or error
                            $tiModel->card_info_id = $card_info_id;
                            $tiModel->studio_id = $studio_id;
                            $tiModel->user_id = $user_id;
                            $tiModel->billing_info_id = $billing_info_id;
                            $tiModel->billing_amount = $_REQUEST['amount'];
                            $tiModel->invoice_detail = "DevHours purchase Fee";
                            $tiModel->is_success = 1;
                            $tiModel->invoice_id = $firstData->getTransactionTag();
                            $tiModel->order_num = $firstData->getAuthNumber();
                            $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                            $tiModel->response_text = serialize($firstData);
                            $tiModel->transaction_type = 'DevHours purchase Fee';
                            $tiModel->created_date = new CDbExpression("NOW()");
                            $tiModel->save();
                            //insert record in devhour_purchase_infos table.

                            $purchase_data = array();
                            $dpiModel = New DevhourPurchaseInfo;
                            $dpiModel->user_id = $user_id;
                            $dpiModel->studio_id = $studio_id;
                            $dpiModel->amount_paid = $_REQUEST['amount'];
                            $dpiModel->creation_date = date("Y-m-d H:i:s");
                            $dpiModel->save();

                        $dpiModel_id = $dpiModel->id;

                        //update the remaining dev hours in studio table

                        $studioModel = Studio::model()->updateByPk($studio_id, array("remaining_devhours" => $remaining_devhours, "purchased_devhours" => $purchased_devhours));
                    
                        //send email to customer and admin regarding the payment success.
                        $user = User::model()->findByPk($user_id);
                        $req=array();
                        $req['name'] = $user->first_name;
                        $req['email'] = $user->email;
                        $req['dev_hour']=$hours_purchased;
                        $req['total_devhour']=$purchased_devhours;
                        $req['amount_paid']=@$_REQUEST['amount'];
                        
                        //generate pdf                       
                        $invoice['studio'] = $studio;
                        $invoice['user'] = $user;
                        $invoice['billing_info_id'] = $billing_info_id;
                        $invoice['billing_amount'] = $_REQUEST['amount'];
						$invoice['hours_purchased']=$hours_purchased;
                        $pdf = Yii::app()->pdf->adminPurchaseDevhourInvoiceDetail($invoice);
                        $file_name = $pdf['pdf'];

                         //Update the billing details table with the html part of the Invoice.
                         $biModel = New BillingInfos;
                         $biModel->updateByPk($billing_info_id, array('paid_amount' =>$_REQUEST['amount'], 'is_paid' => 2, 'detail' => $pdf['html']));

                        
                        $customeremail= Yii::app()->email->purchaseDevhourEmailtoCustomer($req,$file_name);
                        $adminemail= Yii::app()->email->purchaseDevhourEmailtoAdmin($req);
                        
                        }
                    } else {
                        $res['isSuccess'] = 0;
                        $res['Code'] = 55;
                        $res['Message'] = 'We are not able to process your credit card. Please try another card.';
                    }
                }
            } 
            //payment for existing card details
            else if ($_REQUEST['payment_type'] == 0) {
                $card_infoid = $_REQUEST['used_creditcard'];
                
                $cardsdetails = CardInfos::model()->findByPk(array('id' => $card_infoid));
                $data = array();
                $data['token'] = $cardsdetails->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                $data['card_name'] = $cardsdetails->card_holder_name;
                $data['card_type'] = $cardsdetails->card_type;
                $data['exp'] = ($cardsdetails->exp_month < 10) ? '0' . $cardsdetails->exp_month . substr($cardsdetails->exp_year, -2) : $cardsdetails->exp_month . substr($cardsdetails->exp_year, -2);
                $data['amount'] = $_REQUEST['amount'];
               
                $firstData = $this->chargeToCard($data);
               
                //do transaction and insert records in transaction table    
                if($firstData->isError()) {
                   
                    $res['isSuccess'] = 0;
                    $res['Code'] = $firstData->getErrorCode();
                    $res['Message'] = 'Invalid Card details entered. Please check again';
                    $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                    //Keeping card error logs
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData);
                    $ceModel->transaction_type = 'DevHours purchase Fee';
                    $ceModel->created = new CDbExpression("NOW()");
                     $ceModel->save();

                    //Send an email to developer and support
                    Yii::app()->email->errorMessageMailToSales($req);
                } else if ($firstData->getBankResponseType() == 'S') {
                
                        $res['isSuccess'] = 1;
                       

                    //insert records in to billing info once payment is success
                    $biModel = New BillingInfos;
                    $biModel->studio_id = $studio_id;
                    $biModel->user_id = $user_id;
                    $uniqid = Yii::app()->common->generateUniqNumber();
                    $biModel->uniqid = $uniqid;
                    $biModel->title = 'DevHours purchase Fee';
                    $biModel->billing_date = new CDbExpression("NOW()");
                    $biModel->created_date = new CDbExpression("NOW()");
                    $biModel->billing_amount = $_REQUEST['amount'];
                    $biModel->transaction_type = 6;
                    $biModel->paid_amount = $firstData->getAmount();
                    $biModel->is_paid = 2;
                    $biModel->save();
                    
                    $billing_info_id = $biModel->id;
                   // echo $billing_info_id;
                   // insert records into billing_details table
                            $bidetails=new BillingDetails;
                            
                            $bidetails->user_id = $user_id;
                            $bidetails->billing_info_id=$billing_info_id;
                            $bidetails->title = 'DevHours purchase Fee';
                            $bidetails->description="Payment for DevHours:".$hours_purchased." hours";
                            $bidetails->amount =$_REQUEST['amount'];
                            $bidetails->type=4;
                            $bidetails->remarks=$hours_purchased;
                            $bidetails->save();
                    //Insert new transaction record in transaction infos;
                    $tiModel = New TransactionInfos;

                    //Set datas for inserting new record in transaction info table whether it returns success or error
                    $tiModel->card_info_id = $card_info_id;
                    $tiModel->studio_id = $studio_id;
                    $tiModel->user_id = $user_id;
                    $tiModel->billing_info_id = $billing_info_id;
                    $tiModel->billing_amount = $_REQUEST['amount'];
                    $tiModel->invoice_detail = "DevHours purchase Fee";
                    $tiModel->is_success = 1;
                    $tiModel->invoice_id = $firstData->getTransactionTag();
                    $tiModel->order_num = $firstData->getAuthNumber();
                    $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                    $tiModel->response_text = serialize($firstData);
                    $tiModel->transaction_type = 'DevHours purchase Fee';
                    $tiModel->created_date = new CDbExpression("NOW()");
                    $tiModel->save();    
                    
                    //save in purchase info table
                    $dpiModel = New DevhourPurchaseInfo;
                    $dpiModel->user_id = $user_id;
                    $dpiModel->studio_id = $studio_id;
                    $dpiModel->amount_paid = $_REQUEST['amount'];
                    $dpiModel->creation_date = date("Y-m-d H:i:s");
                    $dpiModel->save();
                    $dpiModel_id = $dpiModel->id;
                    
                    //update the remaining dev hours in studio table
                   $studioModel=Studio::model()->updateByPk($studio_id,array("remaining_devhours"=>$remaining_devhours,"purchased_devhours"=>$purchased_devhours));
               //send email to customer and admin regarding the payment success.
                $user = User::model()->findByPk($user_id);
                        $req=array();
                        $req['name'] = $user->first_name;
                        $req['email'] = $user->email;
                        $req['dev_hour']=$hours_purchased;
                        $req['total_devhour']=$purchased_devhours;
                        $req['amount_paid']=$_REQUEST['amount'];
                        
                         //generate pdf                       
                        $invoice['studio'] = $studio;
                        $invoice['user'] = $user;
                        $invoice['billing_info_id'] = $billing_info_id;
                        $invoice['billing_amount'] = $_REQUEST['amount'];
						$invoice['hours_purchased']=$hours_purchased;
                        $pdf = Yii::app()->pdf->adminPurchaseDevhourInvoiceDetail($invoice);
                        $file_name = $pdf['pdf'];

                         //Update the billing details table with the html part of the Invoice.
                         $biModel = New BillingInfos;
                         $biModel->updateByPk($billing_info_id, array('paid_amount' =>$_REQUEST['amount'], 'is_paid' => 2, 'detail' => $pdf['html']));

                        
                        $customeremail= Yii::app()->email->purchaseDevhourEmailtoCustomer($req,$file_name);
                        $adminemail= Yii::app()->email->purchaseDevhourEmailtoAdmin($req);
                
            } else {
                $res['isSuccess'] = 0;
                $res['Code'] = 55;
                $res['Message'] = 'We are not able to process your credit card. Please try another card.';

                }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        
        print json_encode($res);
        exit;
    }
    /* 
     * This function actionSaveResellerCard() is for saving reseller card details.same as savecard()
     * @Author manas@muvi.com
     * We are not keep card error log and also remove error mail to sales . 
     * remove studio info as not required. 
     */
    function actionSaveResellerCard(){
        $user_id = Yii::app()->user->id;
        if (isset($_REQUEST['card_number']) && !empty($_REQUEST['card_number'])) {
            $user = PortalUser::model()->findByPk($user_id);

            //Check primary card is exist or not
            $cards = ResellerCardInfos::model()->findByAttributes(array('portal_user_id' => $user_id, 'is_cancelled' => 0));
            $is_primary = 0;
            if (isset($cards) && !empty($cards)) {
                $is_primary = 1;
            }

            $data = array();
            $data['card_number'] = $_REQUEST['card_number'];
            $data['card_name'] = $_REQUEST['card_name'];
            $data['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
            $data['amount'] = 0;
            $data['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';
            $data['cvv'] = $_REQUEST['cvv'];
            $address = $_REQUEST['address1'];
            $address .= (trim($_REQUEST['address2'])) ? ", " . trim($_REQUEST['address2']) : '';
            $address .= ", " . $_REQUEST['city'];
            $address .= ", " . $_REQUEST['state'];
            $data['address'] = $address;
            $data['address2'] = $_REQUEST['address2'];

            $firstData_preauth = $this->authenticateToCard($data);
            $card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);

            if ($firstData_preauth->isError()) {
                $res['isSuccess'] = 0;
                $res['Code'] = $firstData_preauth->getErrorCode();
                $res['Message'] = 'Invalid Card details entered. Please check again';
            } else {
                if ($firstData_preauth->getBankResponseType() == 'S') {
                    $res['isSuccess'] = 1;
                    $res['TransactionRecord'] = $firstData_preauth->getTransactionRecord();
                    $res['Code'] = $firstData_preauth->getBankResponseCode();
                    
                    $ciModel = New ResellerCardInfos;
                    $ciModel->portal_user_id = $user_id;
                    $ciModel->payment_method = 'first_data';
                    $ciModel->plan_amt = 3500;// Reseller monthly amount
                    $ciModel->card_number = $_REQUEST['card_number'];
                    $ciModel->card_holder_name = $_REQUEST['card_name'];
                    $ciModel->exp_month = $_REQUEST['exp_month'];
                    $ciModel->exp_year = $_REQUEST['exp_year'];
                    $ciModel->address1 = $_REQUEST['address1'];
                    $ciModel->address2 = $_REQUEST['address2'];
                    $ciModel->city = (isset($_REQUEST['city']) && trim($_REQUEST['city'])) ? $_REQUEST['city'] : '';
                    $ciModel->state = (isset($_REQUEST['state']) && trim($_REQUEST['state'])) ? $_REQUEST['state'] : '';
                    $ciModel->zip = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';
                    $ciModel->status = $res['Message'] = $firstData_preauth->getBankResponseMessage();
                    $ciModel->reference_no = $firstData_preauth->getTransactionTag();
                    $ciModel->card_type = $data['card_type'] = $firstData_preauth->getCreditCardType();
                    $ciModel->card_last_fourdigit = $data['card_last_fourdigit'] = $card_last_fourdigit;
                    $ciModel->auth_num = $firstData_preauth->getAuthNumber();
                    $ciModel->token = $data['token'] = $firstData_preauth->getTransArmorToken();
                    $ciModel->is_cancelled = $is_primary;
                    $ciModel->response_text = serialize($firstData_preauth);
                    $ciModel->ip = CHttpRequest::getUserHostAddress();
                    $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                    $ciModel->created = new CDbExpression("NOW()");
                    $ciModel->save();
                } else {
                    $res['isSuccess'] = 0;
                    $res['Code'] = 55;
                    $res['Message'] = 'We are not able to process your credit card. Please try another card.';
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        print json_encode($res);
        exit;
    }
    
    /*
     * This is refund method and don't delete it
     */
    
    /*
    function actionRefundToUser() {
        /* Test use use
        $gateway['id'] = 3;
        $gateway['gateway_id'] = 5;
        $gateway['short_code'] = 'stripe';
        $gateway['api_username'] = 'sk_test_J7QceZ04oG973QoKALgaP0mV';
        $gateway['api_password'] = 'pk_test_wrssVHZpxA03Ur0ywBCWnqDI';
        $gateway['api_signature'] = '';
        $gateway['api_mode'] = 'sandbox';
        $gateway['is_primary'] = 1;
        $gateway['is_currency_conversion'] = 0;
        $gateway['non_3d_secure'] = '';
        $gateway['is_pci_compliance'] = '';
        $gateway['is_pci_compliance'] = '';
        */
   /*     
        $data[0] = (object) $gateway;
        $this->setPaymentGatwayVariable($data);
        
        $user['charge'] = 'ch_19bPmxL08oWnDNs3noJkU4Rl';
        $user['amount'] = 65.89;
        
        //Initiallise the payment gateway
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway['short_code']] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $res = $payment_gateway::refundTransaction($user);
        
        print '<pre>';print_r($res);
        exit;
    }
    */
    
    function actionSubscribeToReseller() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $ip_address = CHttpRequest::getUserHostAddress();
        
        if (isset($_POST['card_number']) && !empty($_POST['card_number'])) {
            $dataInput = array();

            $dataInput['card_number'] = $_POST['card_number'];
            $dataInput['card_name'] = $_POST['card_name'];
            $dataInput['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
            $dataInput['amount'] = 0;
            $dataInput['zip'] = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
            $dataInput['cvv'] = $_POST['cvv'];
            $address = $_POST['address1'];
            $address .= (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
            $address .= ", " . $_POST['city'];
            $address .= ", " . $_POST['state'];
            $dataInput['address'] = $address;

            $firstData_preauth = $this->authenticateToCard($dataInput);

            $card_last_fourdigit = str_replace(substr($dataInput['card_number'], 0, strlen($dataInput['card_number']) - 4), str_repeat("#", strlen($dataInput['card_number']) - 4), $dataInput['card_number']);
            
            $studio = Studios::model()->findByPk($studio_id);
            $user = User::model()->findByPk($user_id);

            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['phone'] = $user->phone_no;
            $req['companyname'] = $studio->name;
            $req['domain'] = 'http://' . $studio->domain;
            $req['card_last_fourdigit'] = $card_last_fourdigit;

            if ($firstData_preauth->isError()) {
                $res['isSuccess'] = 0;
                $res['Code'] = $firstData_preauth->getErrorCode();
                $res['Message'] = 'Invalid Card details entered. Please check again';

                $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();

                //Send an email to developer and support
                Yii::app()->email->errorMessageMailToSales($req);
            } else {
                //$respons_type = $firstData_preauth->getBankResponseType();

                if ($respons_type == 'S') {
                    //$token = $firstData_preauth->getTransArmorToken();
                    //$card_type = $firstData_preauth->getCreditCardType();

                    $total_amount = 0;
                    
                    //Getting all plans amount which has selected by user
                    $levels = ResellerPlan::model()->getResellerPlan($_POST['levels']);
                    if (isset($levels) && !empty($levels)) {
                        $level_name = $levels['level'][0]['name'];
                        
                        $total_amount = $base_price = $levels['level'][0]['base_price'];

                        $yearly_discount = $levels['level'][0]['yearly_discount'];

                        if (isset($_POST['plan']) && trim($_POST['plan']) == 'Year') {
                            $total_amount = number_format((float) (($total_amount * 12) - ((($total_amount * 12) * $yearly_discount) / 100)), 2, '.', '');
                        }
                    }
                    
                    $data = array();
                    $data['token'] = $token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                    $data['card_name'] = $_POST['card_name'];
                    $data['card_type'] = $card_type;
                    $data['exp'] = $dataInput['exp'];
                    $data['amount'] = $total_amount;
                    $data['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
                    $data['cvv'] = $_POST['cvv'];

                    $firstData = $this->chargeToCard($data);

                    if ($firstData->isError()) {
                        $res['isSuccess'] = 0;
                        $res['Code'] = $firstData->getErrorCode();
                        $res['Message'] = "We couldn't process your card. Please check again";

                        $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

                        //Send an email to developer and support
                        Yii::app()->email->errorMessageMailToSales($req);
                    } else {
                        $respons_type = $firstData->getBankResponseType();

                        if ($firstData->getBankResponseType() == 'S') {
                            $res['isSuccess'] = 1;
                            $res['TransactionRecord'] = $firstData->getTransactionRecord();
                            $res['Code'] = $firstData->getBankResponseCode();

                            $ciModel = New ResellerCardInfos;
                            $ciModel->portal_user_id = $user_id;
                            $ciModel->payment_method = 'first_data';
                            $ciModel->plan_amt = $total_amount;
                            $ciModel->card_holder_name = $data['card_name'];
                            $ciModel->exp_month = $_POST['exp_month'];
                            $ciModel->exp_year = $_POST['exp_year'];

                            $address = $_POST['address1'];
                            $ciModel->address1 = $address;
                            $ciModel->address2 = $data['address2'] = $_POST['address2'];
                            $ciModel->city = (isset($_POST['city']) && trim($_POST['city'])) ? $_POST['city'] : '';
                            $ciModel->state = (isset($_POST['state']) && trim($_POST['state'])) ? $_POST['state'] : '';
                            $ciModel->zip = $data['zip'] = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
                            $ciModel->is_cancelled = 0;
                            $ciModel->status = $res['Message'] = $firstData->getBankResponseMessage();
                            $ciModel->reference_no = $firstData->getTransactionTag();
                            $ciModel->card_type = $card_type;
                            $ciModel->card_last_fourdigit = $card_last_fourdigit;
                            $ciModel->auth_num = $firstData->getAuthNumber();
                            $ciModel->token = $token;

                            $ciModel->response_text = serialize($firstData_preauth);
                            $ciModel->ip = CHttpRequest::getUserHostAddress();
                            $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                            $ciModel->created = new CDbExpression("NOW()");
                            $ciModel->save();

                            //Insert new record for billing information
                            $biModel = New BillingInfos;
                            $biModel->studio_id = $studio_id;
                            $biModel->user_id = $user_id;
                            $uniqid = Yii::app()->common->generateUniqNumber();
                            $biModel->uniqid = $uniqid;
                            $biModel->title = 'Purchase Subscription fee for ' . $package_name;
                            $biModel->billing_date = new CDbExpression("NOW()");
                            $biModel->created_date = new CDbExpression("NOW()");
                            $biModel->billing_amount = $billing_amount = $data['amount'];
                            $biModel->transaction_type = 1;
                            $biModel->save();
                            $billing_info_id = $biModel->id;

                            //Insert new records in bill detail and studio pricing plans
                            $title = 'Monthly platform fee';
                            $description = $package_name.' platform fee';

                            $bdModel = New BillingDetails;
                            $bdModel->billing_info_id = $billing_info_id;
                            $bdModel->user_id = $user_id;
                            $bdModel->title = $title;
                            $bdModel->description = $description;
                            $bdModel->amount = $base_price;
                            $bdModel->save();
                            
                            $sppModel = New ResellerSubscription;
                            $sppModel->reseller_id = $user_id;
                            $sppModel->reseller_plan_id = $levels['level'][0]['id'];
                            $sppModel->plans = $_POST['plan'];
                            $sppModel->plan_amount = $base_price;
                            $sppModel->created_by = $user_id;
                            $sppModel->created = new CDbExpression("NOW()");
                            $sppModel->status = 1;
                            $sppModel->save();

                            //Insert new transaction record in transaction infos;
                            $tiModel = New ResellerTransaction;

                            //Set datas for inserting new record in transaction info table whether it returns success or error
                            $tiModel->card_info_id = $ciModel->id;
                            $tiModel->user_id = $user_id;
                            $tiModel->billing_info_id = $billing_info_id;
                            $tiModel->billing_amount = $billing_amount;
                            $tiModel->invoice_detail = $_POST['plan'] . "ly Subscription Charge";
                            $tiModel->is_success = 1;
                            $tiModel->invoice_id = $firstData->getTransactionTag();
                            $tiModel->order_num = $firstData->getAuthNumber();
                            $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                            $tiModel->response_text = serialize($firstData);
                            $tiModel->transaction_type = 'Purchase Subscription fee for ' . $package_name;
                            $tiModel->created_date = new CDbExpression("NOW()");
                            $tiModel->save();

                            if (abs(($billing_amount - $paid_amount) / $paid_amount) < 0.00001) {
                                $is_paid = 2;
                            } else {
                                $is_paid = 1;
                            }

                            $invoice['studio'] = $studio;
                            $invoice['user'] = $user;
                            $invoice['card_info'] = $ciModel;
                            $invoice['billing_info_id'] = $billing_info_id;
                            $invoice['billing_amount'] = $billing_amount;
                            $invoice['package_name'] = @$package_name;
                            $invoice['package_code'] = @$package['package'][0]['code'];
                            $invoice['base_price'] = @$base_price;
                            $invoice['plan'] = @$_POST['plan'];
                            $invoice['yearly_discount'] = @$yearly_discount;
                            $invoice['no_of_applications'] = @$no_of_applications;
                            $invoice['applications'] = @$allapps;
                            
                            if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Months -1 Days"));
                            } else {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Years -1 Days"));
                            }
                            
                            $pdf = Yii::app()->pdf->adminSubscriptionReactivationInvoiceDetial($invoice);
                            $file_name = $pdf['pdf'];

                            //Update subscription in studio table if tranasction is going to success or not.
                            $biModel = New BillingInfos;
                            $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => $is_paid, 'detail' => $pdf['html']));

                            //Save subscription info if paymanet has processed successfully
                            //if (intval($is_paid) == 2) {
                            $studio->status = 1;
                            $studio->is_subscribed = 1;
                            $studio->subscription_start = Date('Y-m-d H:i:s');
                            $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
                            if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . "+1 Months -1 Days"));
                                $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                                $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months -1 Days"))));
                                $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                            } else {
                                $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . "+1 Years -1 Days"));
                                $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
                                $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Years -1 Days"))));
                                $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
                            }
                            $studio->save();

                            $req['package_name'] = @$package_name;
                            $req['plan'] = @$_POST['plan'];
                            $req['amount_charged'] = @$billing_amount;
                            
                            Yii::app()->email->subscriptionPurchaseMailToSales($req);
                            Yii::app()->email->subscriptionPurchaseMailToUser($req, $file_name);
                            Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');
                        } else {
                            $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                            $res['isSuccess'] = 0;
                            $res['Code'] = 55;
                            $res['Message'] = $error_message;
                        }
                    }
                } else {
                    $error_message = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
                    $res['isSuccess'] = 0;
                    $res['Code'] = 55;
                    $res['Message'] = $error_message;
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        print json_encode($res);
        exit;
    }    
    /**
     * @method public sendMailFailCloudfront() send mail when cloudfront url not created
     * @author prakash<prakash@muvi.com>
     * @return boolean value     
     */
    function sendMailFailCloudfront($studio){
        if($studio){
            $mailcontent=array();
            $mailcontent['studio_id']=$studio->id;
            $mailcontent['studio_name']=$studio->name;
            $mailcontent['domain_name']=$studio->domain;                
            Yii::app()->email->failedCloudfontUrl($mailcontent);
        }
        return true;
    }  
    /**
     * @method public createClouldfrontUrl() generate clouldfronturl 
     * @input parameter object
     * @author prakash<prakash@muvi.com>
     * @return boolean value     
     */
    
    function createClouldfrontUrl($studio){
        if ($studio->studio_s3bucket_id == 0 && $studio->new_cdn_users == 1 && $studio->signed_url == '' && $studio->unsigned_url == '' && $studio->video_url == '') {            
            $studio_id=$studio->id;
            $bucketDetailsArr = Yii::app()->common->getBucketInfo('', $studio_id);
            $accessKey = Yii::app()->params->s3_key;
            $secretKey = Yii::app()->params->s3_secret;
            $singedpath = '/' . $studio_id . '/EncodedVideo';
            $unsingedpath = '/' . $studio_id . '/public';
            $unsingedpathForVideo = '/' . $studio_id . '/RawVideo';                       
            if ((HOST_IP == '127.0.0.1') || (HOST_IP == '52.0.64.95')) {
                $stagingCDN = 'd2gx0xinochgze.cloudfront.net';
                $stagingID = 'E2P005RPS6TJU0';
                $cloudFrontDetails = $stagingCDN . $unsingedpath . ":::" . $stagingCDN . $singedpath . ":::" . $stagingCDN . $unsingedpathForVideo . ":::" . $stagingID . ":::" . $stagingID . ":::" . $stagingID;
                $studio->cdn_status = 1;
            } else if (DOMAIN == 'idogic') {
                $stagingID = 'E2P005RPS6TJU0';
                $cloudFrontDetails = $bucketDetailsArr['unsigned_cloudfront_url'] . $unsingedpath . ":::" . $bucketDetailsArr['cloudfront_url'] . $singedpath . ":::" . $bucketDetailsArr['videoRemoteUrl'] . $unsingedpathForVideo . ":::" . $stagingID . ":::" . $stagingID . ":::" . $stagingID;
                $studio->cdn_status = 1;
            } else {
                try{
                    $cloudFrontDetails = Yii::app()->aws->createCloudFontUrlForNewSignUp($accessKey, $secretKey, $bucketDetailsArr['bucket_name'], $studio_id);
                }catch (Exception $e) {
                    $cloudFrontDetails='';
}
            }
            $cloudFrontDetails = !empty($cloudFrontDetails)?explode(":::", $cloudFrontDetails):array();
            if(count($cloudFrontDetails)>0){
                $studio->signed_url = $cloudFrontDetails[1];
                $studio->unsigned_url = $cloudFrontDetails[0];
                $studio->video_url = $cloudFrontDetails[2];
                $studioCdnDetails = New StudioCdnDetails;
                $studioCdnDetails->studio_id = $studio_id;
                $studioCdnDetails->signed_url_id = $cloudFrontDetails[4];
                $studioCdnDetails->unsigned_url_id = $cloudFrontDetails[3];
                $studioCdnDetails->video_url_id = $cloudFrontDetails[5];
                $studioCdnDetails->save();
                $studio->save();
            }else{
                //mail section
                $this->sendMailFailCloudfront($studio);                                    
            }
        }
        return true;
    }
	
	//PayPal start ---------------------------------------------------------------------
	//By Pritam
	public function actionAuthenticatePaypal() {
		$studio_name = Studio::model()->findByPk(Yii::app()->user->studio_id)->name;
		$arg['currency_code'] = 'USD';
		$arg['have_paypal'] = 1;
		$arg['paymentAmount'] = 0;
		$arg['billingType'] = 'MerchantInitiatedBilling';
		$arg['paymentType'] = 'AUTHORIZATION';
		$arg['paymentDesc'] = 'PayPal account authorization for reference transaction of ' . $studio_name;
		$arg['returnURL'] = Yii::app()->getBaseUrl(true) . "/payment/returnPaypal";
		$arg['cancelURL'] = Yii::app()->getBaseUrl(true) . "/payment/cancelPaypal";
		$arg['email'] = User::model()->findByPk(Yii::app()->user->id)->email;
		$arg['currencyCodeType'] = 'USD';

		if (IS_LIVE_PAYPAL) {
			$payPalPro = new paypal_pro(trim(PAYPAL_API_LOGIN), trim(PAYPAL_API_KEY), trim(PAYPAL_API_SIGNATURE), '', '', TRUE, FALSE, '57.0');
		} else {
			$payPalPro = new paypal_pro(trim(PAYPAL_API_LOGIN), trim(PAYPAL_API_KEY), trim(PAYPAL_API_SIGNATURE), '', '', FALSE, FALSE, '57.0');
		}
		$amount = $arg['paymentAmount'];
		$currency = strtoupper($arg['currency_code']);

		$arg['paymentAmount'] = $amount;
		$arg['currencyCodeType'] = $currency;
		$arg['currency_code'] = $currency;
		$arg['billingType'] = isset($arg['billingType']) && trim($arg['billingType']) ? $arg['billingType'] : 'RecurringPayments';
		$resArray = $payPalPro->CallShortcutExpressCheckoutRefTransaction($arg);
		if ($resArray['TOKEN']) {
			$payPalPro->RedirectToPayPal($resArray["TOKEN"]);
		}
	}

	public function actionreturnPaypal() {
		$arg['token'] = $_REQUEST['token'];
		if (IS_LIVE_PAYPAL) {
			$payPalPro = new paypal_pro(trim(PAYPAL_API_LOGIN), trim(PAYPAL_API_KEY), trim(PAYPAL_API_SIGNATURE), '', '', TRUE, FALSE, '57.0');
		} else {
			$payPalPro = new paypal_pro(trim(PAYPAL_API_LOGIN), trim(PAYPAL_API_KEY), trim(PAYPAL_API_SIGNATURE), '', '', FALSE, FALSE, '57.0');
		}
		if (isset($_REQUEST['token']) && trim($_REQUEST['token'])) {
			$resArray = $payPalPro->CreateBillingAgreement($arg);
			$paypalDetails = $payPalPro->GetExpressCheckoutDetails($_REQUEST['token']);
			$ack = $resArray["ACK"];
			if ($ack == 'Success') {

				$billing_id = $resArray['BILLINGAGREEMENTID'];
				$studio_id = isset($_POST['studioid']) ? $_POST['studioid'] : Yii::app()->common->getStudiosId();

				//$card_sql = "UPDATE card_infos SET is_cancelled = 1 WHERE studio_id = {$studio_id} AND is_cancelled = 0";
				CardInfos::model()->updateAll(array('is_cancelled' => 1), 'studio_id = ' . $studio_id . ' AND is_cancelled = 0');

				$package = Package::model()->getPackages();
				$ciModel = new CardInfos();
				$ciModel->studio_id = $studio_id;
				$ciModel->payment_method = 'paypal';
				$ciModel->user_id = Yii::app()->user->id;
				$ciModel->card_holder_name = @$paypalDetails['FIRSTNAME'] . ' ' . @$paypalDetails['LASTNAME'];
				$ciModel->card_number = '';
				$ciModel->address1 = '';
				$ciModel->city = '';
				$ciModel->state = '';
				$ciModel->zip = '';
				$ciModel->customer_id = $billing_id;
				$ciModel->plan_amt = $package['package'][0]['base_price'];
				$ciModel->is_cancelled = 0;
				$ciModel->response_text = json_encode($resArray);
				$ciModel->created = Date('Y-m-d H:i:s');
				$ciModel->save();

				Yii::app()->user->setFlash('success', 'Your account authenticated successfully');
			} else {
				Yii::app()->user->setFlash('error', 'Error in authenticating your account. Please try again');
			}
		} else {
			Yii::app()->user->setFlash('error', 'Error in authenticating your account. Please try again');
		}
		$this->redirect($this->createUrl('payment/managePayment'));
		exit;
	}

	public function actioncancelPaypal() {
		Yii::app()->user->setFlash('error', 'Cancelled by the user. Please try again');
		$this->redirect($this->createUrl('payment/managePayment'));
	}
	//PayPal end -----------------------------------------------------------------------
    
}
