<div class="row m-b-40">
    <div class="col-sm-12">
        <ul class="list-inline m-l-0">
            <li>
                <a href="<?php //echo Yii::app()->getBaseUrl(true);    ?>/partner/addTicket" class="btn btn-primary m-t-10">Add Ticket</a>
            </li>
            
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">


        <div class="notification"></div>

        <div class="row m-b-20">
            <div class="col-sm-4">
                <div class="form-group input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                    <div class="fg-line">
                        <input class="search form-control input-sm" placeholder="Search"/></div>
                </div>
            </div>
            <div class="col-sm-2">           

                <div class="select">
                    <select class="form-control input-sm" id="ticket_status" onchange="show_tickets()" >
                        <option value="Open">Open</option>
                        <option value="Closed">Closed</option>
                        <option value="All">All</option>
                    </select>

                </div>

            </div>
            <div class="col-sm-2">           

                <div class="select">
                    <select class="form-control input-sm" id="ticket_priority" onchange="show_tickets()" >
                        <option value="0">High & Critical</option>
                        <option value="1">Medium</option>
                        <option value="2">Low</option>
                    </select>

                </div>

            </div>
            <div class="col-sm-2">           
                 <div class="select">
                    <select class="form-control input-sm" id="all_stores" onchange="show_tickets()" >
                        <option value="0">All</option>
                        <?php foreach($master_id as $key => $val) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                        <?php } ?>  
                     </select>
                 </div>
            </div>
            
        </div> 
        <div id="loaderDiv" class="text-center m-b-20 loaderDiv"  style="display:none;">
            <div class="preloader pls-blue text-center " >
                <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                </svg>
            </div> 
        </div>
        <div class="row m-b-40">


            <input type="hidden" id="page_size" value="<?php echo $page_size ?>" />
            <div class="col-sm-12" id="ticket_container">

            </div>
        </div>
      
    </div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js" type="text/javascript"></script>
<script type="text/javascript" src="/themes/admin/js/bootstrap-typeahead.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootbox.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery.bootpag.min.js"></script>

<script type="text/javascript">

                                        var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
                                        var pricingall = '';
                                        var packagesall = '';
                                        var planall = '';
                                        var custom_codes = '';


                                        $(function () {

                                            var searchText = $.trim($(".search").val());
                                            var ticket_status = $("#ticket_status :selected").val();
                                            var ticket_priority = $("#ticket_priority :selected").val();
                                            var ticket_stores=$("#all_stores :selected").val();
        
                                            getticketDetails(searchText,ticket_priority, ticket_status,ticket_stores);
                                        });

                                        $(document.body).on('keypress', '.search', function (event) {
                                            // alert();
                                            var searchText = $.trim($(".search").val());
                                            var ticket_status = $("#ticket_status :selected").val();
                                            var ticket_priority = $("#ticket_priority :selected").val();
                                            var ticket_stores=$("#all_stores :selected").val();
                                            var c = String.fromCharCode(event.keyCode);
                                            var isWordCharacter = c.match(/\w/);
                                            var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
                                            var isEsearchVideoPagenter = (event.keyCode == 13);
                                            var isEnter = (event.keyCode == 13);
                                            if ((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)) {
                                                getticketDetails(searchText, ticket_priority,ticket_status,ticket_stores, 1);
                                            }


                                        });

                                        function sortby(sortby, order)
                                        {
                                            $("#" + sortby + "_i").toggleClass("fa-sort-asc  fa-sort-desc");
                                            var orderclass = $("#" + sortby + "_i").attr("class");
                                            //console.log(orderclass);   
                                            order = (orderclass == 'fa fa-sort-asc') ? 'asc' : 'desc';

                                            var searchText = $.trim($(".search").val());
                                            var ticket_status = $("#ticket_status :selected").val();
                                            var ticket_priority = $("#ticket_priority :selected").val();
                                            var ticket_stores=$("#all_stores :selected").val();
                                            var sortBy = sortby + '_' + order;
                                            
                                            getticketDetails(searchText, ticket_priority,ticket_status,ticket_stores, 1, sortBy);
                                            //console.log(sortBy);                                                                     
                                        }

                                        function getticketDetails(searchText,priority, ticket_status,ticket_stores, page, sortBy)
                                        {
                                            $('#loaderDiv').show();

                                            $.post('/partner/searchTicket', {'search_value': searchText,'priority':priority, 'ticketstatus': ticket_status,'ticketstores':ticket_stores, 'page': page, 'sortBy': sortBy}, function (res) {
                                                $('#loaderDiv').hide();
                                                $('#ticket_container').html(res);
                                                var mydata = $(res).find('data-count');
                                                var mydata1 = $("#data-count1").val();
                                                var regex = /\d+/g;
                                                var count = mydata.prevObject[0].outerHTML.match(regex);
                                                var page_size = $('#page_size').val();
                                                var total = parseInt(count) / parseInt(page_size);
                                                var maxVisible = total / 2;
                                                if (maxVisible <= 5) {
                                                    maxVisible = total;
                                                }
                                                //alert(mydata1);

                                                if (parseInt(count) < parseInt(page_size) || parseInt(mydata1) < 20) {
                                                    $('#page-selection').parent().hide();
                                                } else {
                                                    if ($('.page-selection-div').length <= 0) {
                                                        $('#ticket_container').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div><div class="h-40"></div>');
                                                    }
                                                    $('#page-selection').parent().show();
                                                }
                                                $('#page-selection').bootpag({
                                                    total: Math.ceil(total),
                                                    page: page,
                                                    maxVisible: 5
                                                }).on('page', function (event, num) {
                                                    $.post('/partner/searchTicket', {'page': num}, function (res) {
                                                        $('#loaderDiv').hide();
                                                        $('#ticket_container').html(res);
                                                        $('.loader').hide();
                                                    });
                                                });
                                            });
                                        }
                                       
                                        
                                        $(function () {
                                            $('#search').each(function () {
                                                $(this).find('input').keypress(function (e) {
                                                    // Enter pressed?
                                                    if (e.which == 10 || e.which == 13) {
                                                        this.form.submit();
                                                    }
                                                });


                                            });
                                        });
                                        function show_tickets()
                                        {
                                            var searchText = $.trim($(".search").val());
                                            var ticket_status = $("#ticket_status :selected").val();
                                            var ticket_priority = $("#ticket_priority :selected").val();   
                                            var ticket_stores=$("#all_stores :selected").val();
                                            getticketDetails(searchText,ticket_priority,ticket_status,ticket_stores);
                                        }
</script>
