<?php $v = RELEASE; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $studio_name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <script type='text/javascript'>
            var muviWaterMark = "";
<?php if (isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != '' && $embedWaterMArk == 1) { ?>
                var muviWaterMark = "<?php echo $_REQUEST['waterMarkOnPlayer'] ?>";
<?php } else if($waterMarkOnPlayer != '') {  ?>
                var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
<?php } ?>
        </script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />          
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.progressTips.js"></script> 

        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.progressTips.css?v=<?php echo $RELEASE?>" rel="stylesheet" type="text/css">
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ads.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/vast-client.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.vast.js"></script>   


        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>        

        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video-trailerbuffer-log.js"></script>  
<!--        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video-buffer-log.js"></script>  -->

        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
            .vjs-fullscreen {padding-top: 0px}  
            .RDVideoHelper{display: none;}
            video::-webkit-media-controls {
                display:none !important;
            }
            video::-webkit-media-controls-panel {
                display: none !important;
            }
			
		body {
			background-color: #1E1E1E;;
			color: #fff;
			font: 12px Roboto,Arial,sans-serif;
			height: 100%;
			margin: 0;
			overflow: hidden;
			padding: 0;
			position: absolute;
			width: 100%;
		}
		html {
			overflow: hidden;
		}
		.vjs-fluid{
			width: 100%;
			height: 0;
			padding-top: 56.25%;
		}
                .temp-bg{
                    position: absolute;
                    z-index: 90;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background-color: #000;
                }
            #pause_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none;
            }
            #play_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none; 
            }
        </style>
    </head>
    <body>
        <center id="videoDivContent">
            <?php if(@$_REQUEST[isApp] == 1) { ?>
            <noscript>
                <style type="text/css">
                    #video_block {display:none; content:none;}
                </style>
                <div style="font-size: 22px; position:absolute; left:0; right:0; top:0; bottom:0; margin:auto; width:100%; height:10%;">
                You don't have javascript enabled.
                </div>
            </noscript>
            <?php } ?>
            <div class="demo-video video-js-responsive-container vjs-hd"  style="position: relative;">
                <input type="hidden" id='prevSeekingTime' value="0" />
                <div class="videocontent" style="overflow:hidden;">
                    <video id="video_block" class="video-js vjs-default-skin vjs-16-9 "  <?php if(isset($_REQUEST['isApp'])){ ?> autoplay ="true" preload="auto" <?php } else{ ?> preload="none" <?php } ?> controls='false'   fluid = 'true' width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}' webkit-playsinline>
                        <?php
                            foreach ($multipleVideo as $key => $val) {
                                echo '<source src="' . $fullmovie_path . '" data-res="' . $key . '" type="video/mp4" />';
                            }
                        ?>
                    </video>
                <input type="hidden" id="full_video_duration" value="" />
                <input type="hidden" id="full_video_resolution" value="" />
                <input type="hidden" id="u_id" value="0" />
                <input type="hidden" id="buff_log_id" value="0" />
                <input type="hidden" id="movie_id" value="<?php echo $movie_id ?>" />

                </div> 
            </div> 			
        </center>
        <style>
			.movie-name{position: absolute;top:0;left: 0;right: 0;}
			.movie-name a{color: #fff;}
        </style>
        <script>
            var nativeVideoControl = document.getElementById("video_block");
            nativeVideoControl.removeAttribute("controls");   
            var myVideoNativeControl = document.getElementsByTagName('video')[0];
            myVideoNativeControl.removeAttribute("controls");  
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = false;
            var movie_id = "";
            var full_movie = "<?php echo $fullmovie_path ?>";
            var can_see = "1";
            var back_url = "";
            var is_mobile =  "";
            var player = "";
            var url = "<?php echo Yii::app()->baseUrl; ?>/report/addTrailerLog";
            var movie_id = "<?php echo $movie_id ?>";
            var previousTime = 0;
            var currentTime = 0;
            var log_id = 0;
            var device_type = "<?php echo $device_type ?>";
            $(document).ready(function () {
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                var full_movie = "<?php echo $fullmovie_path ?>";
                var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                var multipleVideoResolution = new Array();
                multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
                var defaultResolution = "<?php echo $defaultResolution; ?>";
                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                var adavail = 0;
                var previousBufferEnd =0;
                var bufferDurationaas = 0;
                var percen = 5;
                var forChangeRes = 0;
                var bufferenEnddd = 0;
                var bufferenEndd = 0;
                var videoOnPause = 0;
                var nAgt = navigator.userAgent;
                var browserName  = navigator.appName;
                var verOffset;
                // In Opera 15+, the true version is after "OPR/" 
                if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
                 browserName = "Opera";
                }
                // In older Opera, the true version is after "Opera" or after "Version"
                else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
                 browserName = "Opera";
                }
                // In MSIE, the true version is after "MSIE" in userAgent
                else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
                 browserName = "Microsoft Internet Explorer";
                }
                // In Chrome, the true version is after "Chrome" 
                else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
                 browserName = "Chrome";
                }
                // In Safari, the true version is after "Safari" or after "Version" 
                else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
                 browserName = "Safari";
                }
                // In Firefox, the true version is after "Firefox" 
                else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
                 browserName = "Firefox";
                }
                if(full_movie != ''){
                var playerSetup = videojs('video_block',{plugins: {resolutionSelector: {
                        force_types: ['video/mp4'],
                        default_res: "<?php echo $defaultResolution; ?>"
                    }}});
                    
                playerSetup.ready(function () {
                    player = this;
                            player.progressTips(); // Displying duration in the player during playback.
                    $('#video_block').append('<img src="/images/touchpause.png" id="pause_touch" />');
                    $('#video_block').append('<img src="/images/touchplay.png" id="play_touch" />');
                    $('#video_block').append('<img src="/images/pause.png" id="pause"/>');
                    $('#video_block').append('<img src="/images/play-button.png" id="play" />');
<?php if ((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)) { ?>
                        $('.vjs-loading-spinner').attr('style', '-webkit-animation: spin 1.5s infinite linear;-moz-animation: spin 1.5s infinite linear; -o-animation: spin 1.5s infinite linear;animation: spin 1.5s infinite linear');
                                $('.vjs-loading-spinner').show();
                              <?php  if((isset($_REQUEST['device_type'])) && ($_REQUEST['device_type'] == 4)){ ?>
                                    //IF ANDROID TV- To remove an unknown image before video loaded in Android TV
                                        $('.vjs-loading-spinner').show();
                                        $('.vjs-loading-spinner').css('z-index','100');
                                        $('.vjs-default-skin .vjs-control-bar').css('z-index','100');
                                        $("<div class='temp-bg'></div>").appendTo(".videocontent");
                                        $(".vjs-default-skin .vjs-menu-button .vjs-menu .vjs-menu-content").css("right","-20px");
                                        $(".vjs-default-skin .vjs-menu-button .vjs-menu .vjs-menu-content").css("left","initial");
                                <?php } 
                            } ?>
                            $(".vjs-fullscreen-control").click(function () {
                                $(this).blur();
                            });
                        <?php if(!isset($_REQUEST['isApp']) || (isset($_REQUEST['isApp']) && $_REQUEST['isApp'] == 2)) { ?>
                            $("#video_block_html5_api").attr('poster', item_poster);
                            $("#video_block").attr('poster', item_poster);
                        <?php } ?>
                        <?php if((isset($_REQUEST['isApp']) && ($_REQUEST['isApp'] == 2))){ 
                                if(@$_REQUEST['device_type']==3){
                        ?>
                            $('.vjs-fullscreen-control').hide();
                                <?php } 
                                
                                if(@$_REQUEST['device_type'] == 2){
                                        ?>
                            $(".vjs-fullscreen-control").on('touchend', function(event) {
                                full_screen = player.isFullscreen();
                                if (full_screen === false) {
                                    $("#video_block").removeClass("vjs-fullscreen");
                                    full_screen = false;
                                      Android.webViewFullscreenExit();
                                } else {
                                    $("#video_block").addClass("vjs-fullscreen");
                                    full_screen = true;
                                    Android.webViewFullscreen();
                                }
                            });
                                <?php } ?>
                            var thisIframesHeight = $( window ).height();
                            $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                            $( ".vjs-control-bar" ).attr('style','font-size:12px;');
                            $( ".vjs-text-track-display" ).attr('style','font-size:12px;');
                            $( ".vjs-current-time" ).attr('style','margin-left:10px;');
                            $( window ).on( "resize", function( event ) {
                                var thisIframesHeight = $(window).height();
                                setTimeout(function () {
                                    $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                                }, 0);
                            });
                        <?php } else { ?>
                            if($(document).height() != null){
                                var thisIframesHeight = $(document).height();
                                $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                            } else{
                                $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
                            }
                        <?php } ?>
                            $(".vjs-big-play-button").hide();
                            if($("div.vjs-subtitles-button").length)
                            {
                                var divtagg = $(".vjs-subtitles-button .vjs-control-content .vjs-menu .vjs-menu-content li:first-child");
                                var divtaggVal = divtagg.html();
                                if(divtaggVal === 'subtitles off'){
                                    divtagg.html("Subtitles Off");
                                }
                            }
                            $(".vjs-tech").mousemove(function () {
                                if (full_screen === true && show_control === false) {
                                    $("#video_block .vjs-control-bar").show();
                                    show_control = true;
                                    var timeout = setTimeout(function () {
                                        if (full_screen === true) {

                                        }
                                    }, 10000);
                                    $(".vjs-control-bar").mousemove(function () {
                                        event.stopPropagation();
                                    }).mouseout(function () {
                                        event.stopPropagation();
                                    });
                                } else {
                                    clearTimeout(timeout);
                                }
                            });

                            player.on("fullscreenchange", resize_player);
                            <?php if($v_logo != ''){ ?>
                                player.watermark({
                                    file: "<?php echo $v_logo; ?>",
                                    xrepeat: 0,
                                    opacity: 0.75
                                });
                                if (is_mobile !== 0) {
                                    $(".vjs-watermark").attr("style", "bottom:40px;right:1%;width:7%;");
                                }else{
                                    $(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;");
                                }
                            <?php } ?>
                            
                            <?php
                                if((isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] == 1) || ($_REQUEST['isApp'] == 2))){
                            ?>
                                $(".vjs-res-button").hide();
                                $(".vjs-volume-control").hide();
                                $(".vjs-mute-control").hide();
                                <?php  if($_REQUEST['isApp'] == 1){ ?>
                                $(".vjs-fullscreen-control").hide();
                                <?php } else if($_REQUEST['isApp'] == 2){?> 
                                    player.load();
                                    $(".vjs-fullscreen-control").click(function(){
                                        full_screen = player.isFullscreen();
                                        if (full_screen === false) {
                                             $("#video_block").addClass("vjs-fullscreen");
                                             full_screen = true;
                                        }else{
                                             $("#video_block").removeClass("vjs-fullscreen");
                                             full_screen = false;
                                        }
                                     });
                                <?php } ?>
                                player.play();
                                $(".vjs-control-bar").show();
                            <?php 
                                }
                            ?>
<?php if (!isset($_REQUEST['isApp'])) { ?>
                        if (is_mobile === 0) {
                            $('#video_block_html5_api').on('click', function (e) {
                                // document.getElementById('video_block_html5_api').onclick = function(e) {
                                e.preventDefault();
                                if (player.paused()) {
                                    player.play();
                                    $('#pause').show();
                                    $('#play').hide();
                                } else {
                                    player.pause();
                                    $('#play').show();
                                    $('#pause').hide();
                                }
                        });
                        } else {
                            player.on("play", function () {
                                videoOnPause = 0;
                                $('#play_touch').hide();
                                $('#pause_touch').hide();
                            });
                            player.on("pause", function () {
                                videoOnPause = 1;
                                $('#play_touch').show();
                                $('#pause_touch').hide();
                            });
                            $('#pause_touch').bind('touchstart', function (e) {
                                player.pause();
                            });
                            $('#play_touch').bind('touchstart', function (e) {
                                player.play();
                            });
                            $('#video_block_html5_api').bind('touchstart', function (e) {
                                if (player.play()) {
                                    $('#pause_touch').show();
                                    $('#play_touch').hide();
                                    setTimeout(function () {
                                        $('#pause_touch').hide();
                                    }, 3000);
                }
            });
                        }
<?php } ?>

                });
                player.on('timeupdate', function () {
                    var duration = player.duration();
                    previousTime = currentTime;
                    currentTime = player.currentTime();
                });
                player.on("play", function () {
                    $.post(url, {device_type: device_type, movie_id: movie_id, status: "start", log_id: log_id, video_length: player.duration()}, function (res) {
                        if (res > 0)
                        {
                            log_id = res;
                        } else {
                            history.go(-1);
                        }
                    });
                    started = 1;
                    ended = 0;
                    logged = 0
                });

                player.on("ended", function () {
                    $.post(url, {device_type: device_type, movie_id: movie_id, status: "complete", log_id: log_id, played_length: player.currentTime(), video_length: player.duration()}, function (res) {
                        log_id = 0;
                    });
                    ended = 1;
                    started = 0;
                });
                player.on('timeupdate', function () {
                    previousTime = currentTime;
                    currentTime = player.currentTime();
                    previousBufferEnd = bufferDurationaas;
                    var r = player.buffered();
                    var buffLen = r.length;
                    buffLen = buffLen - 1;
                    bufferDurationaas = r.end(buffLen);


                    var curlength = Math.round(player.currentTime());
                    var fulllength = player.duration();
                    fulllength = Math.round((fulllength * percen) / 100);
                    if (curlength === fulllength && fulllength > 1)
                    {
                        percen = parseInt(percen) + 5;
                        var buff_log_id = document.getElementById('buff_log_id').value;
                        updateBuffered(player, curlength, buff_log_id);
                        $.post(url, {device_type: device_type, movie_id: movie_id, status: "halfplay", log_id: log_id, percent: percen, played_length: player.currentTime(), video_length: player.duration()}, function (res) {
                            // console.log(res);
                            log_id = res;
                        });
                    }

                });
                player.on('loadedmetadata', function () {
                    var duration = player.duration();
                    $('#full_video_duration').val(duration);
                    if (typeof player.getCurrentRes === "function") {
                        var video_resolution = player.getCurrentRes();
                    } else {
                        var video_resolution = 144;
                    }
                    $('#full_video_resolution').val(video_resolution);
                    buffered_loaded();
                });

            }
        });
            function resize_player() {
                if (full_screen === false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen === true) {
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                }
            }
            
            function handelAndroidAppPlay(){
                if(player.paused()){
                    player.play();
                } else{
                    player.pause();
                }
            }
            function handelAndroidAppForward(forward){
                var currentTime = player.currentTime();
                if (forward === undefined) {
                    currentTime = currentTime -10;
                    if(currentTime > 0){
                        player.currentTime(currentTime);
                    }else{
                        player.currentTime(0);
                    }

                } else{
                    currentTime = currentTime +10;
                    var playerFullDuration  = player.duration();
                    if(currentTime < playerFullDuration){
                        player.currentTime(currentTime);
                    }else{
                        player.currentTime(playerFullDuration);
                    }
                }
            }
            function handelAppUserActive(){
                player.userActive(true);
            }
            function pausePlayer(){
                player.pause();
            }
            function screenRotationForWebView(androidonRotation){
                if (androidonRotation === 0) {
                    $("#video_block").removeClass("vjs-fullscreen");
                    full_screen = false;
                } else {
                    $("#video_block").addClass("vjs-fullscreen");
                    full_screen = true;
                }
            }
        </script>
    </body>
</html>