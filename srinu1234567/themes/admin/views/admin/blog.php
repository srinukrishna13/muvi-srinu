<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="<?php echo $this->createUrl('admin/blogcontent');?>" class="btn btn-primary m-t-10">Add New Post</a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-note icon left-icon "></em>
                </div>
                <h4>Latest Blog Posts</h4>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                   
                    <div class="dataTables_length" id="example1_length">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-1">Show </label>
                                <div class="col-sm-2 p-r-0">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select onchange="javascript: changePageSize(this.value);" class="form-control input-sm" aria-controls="example1" name="example1_length">
                                                <option value="5" <?php echo ($page_size == 5)?'selected="selected"':'';?>>5</option>
                                                <option value="10" <?php echo ($page_size == 10)?'selected="selected"':'';?>>10</option>
                                                <option value="20" <?php echo ($page_size == 20)?'selected="selected"':'';?>>20</option>
                                                <option value="40" <?php echo ($page_size == 40)?'selected="selected"':'';?>>40</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <label class="control-label col-sm-2">Posts </label>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
         <?php
                            if(count($data) > 0)
                            {
                                ?>
                            <table class="table" id="example1" role="grid" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th><div class="pull-left">Post Title</div> <div class="pull-right"><a style="color:#333;" href="javascript: changeSort('post_title', '<?php echo ($sortby == 'post_title' && $order == 'asc')?'desc':'asc';?>');" ><em class="fa fa-sort-<?php echo ($sortby == 'post_title' && $order == 'asc')?'asc':'desc';?>"></em></a></div></th>
                                        <th>Created By</th>                                        
                                        <th data-hide="phone">Last Updated</th>
                                        <th data-hide="phone">Created Date</th>
                                        <th data-hide="phone">Action</th>                                     
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $c = 0;
                                        foreach($data as $post)
                                        {
                                            $c++;
                                            $cls = ($c%2 == 1)?'odd':'even';
                                        ?>
                                        <tr role="row" class="<?php echo $cls;?>">
                                            <td><?php echo Yii::app()->common->htmlchars_encode_to_html($post['post_title']);?></td>
                                            <td><?php echo $post['author']!=""?$post['author'] : $this->studio->name;?></td>
                                            <td><?php echo Yii::app()->common->phpDate($post['last_updated_date']);?></td>
                                            <td><?php echo Yii::app()->common->phpDate($post['created_date']);?></td>
                                            <td>
                                                <h5><a href="<?php echo $this->createAbsoluteUrl('admin/blogcontent/', array('post_id' => $post['id'])); ?>"  data-toggle="tooltip" data-original-title="Edit" data-placement="top"><em class="icon-pencil"></em>&nbsp;Edit</a></h5>
                                                <h5><a href="<?php echo $this->createAbsoluteUrl('admin/blogcomment/', array('post_id' => $post['id'])); ?>"  data-toggle="tooltip" data-original-title="Comments" data-placement="top"><em class="fa fa-comment" ></em>&nbsp;Comment</a></h5>
                                                <?php if (@IS_LANGUAGE == 1) { ?>
                                                <?php if($language_id == $post['language_id']){ ?>
                                                    <h5><a href="<?php echo $this->createAbsoluteUrl('admin/blog', array('option' => 'delete', 'post_id' => $post['id'])); ?>" data-msg = 'Are you sure, you want to delete this post?' class="confirm"  data-toggle="tooltip" data-original-title="Delete" data-placement="top"><em class="icon-trash"></em>&nbsp;Remove</a></h5>
                                                <?php } ?>
                                                <?php } else { ?>
                                                    <h5><a href="<?php echo $this->createAbsoluteUrl('admin/blog', array('option' => 'delete', 'post_id' => $post['id'])); ?>" data-msg = 'Are you sure, you want to delete this post?' class="confirm"  data-toggle="tooltip" data-original-title="Delete" data-placement="top"><em class="icon-trash"></em>&nbsp;Remove</a></h5>
                                                <?php } ?>
                                                <h5><a href="<?php echo $this->createAbsoluteUrl('admin/blog', array('option' => 'status_change', 'post_id' => $post['id'])); ?>" data-msg = 'Do you want to update the status of this post?' class="statusupdate"  data-toggle="tooltip" data-original-title="Change Status" data-placement="top"><em class="<?php echo ($post['has_published'] == 1)?'icon-check':'icon-ban red';?>" ></em>&nbsp;Change Status</a></h5>
                                            </td>
                                        </tr>
                                        <?php
                                        }                                    
                                    ?>
                                </tbody>
                            </table>
                            <?php }
                            else{?>
                           <p class="error">No Blog added yet.</p>
                            <?php }?>
        </div>
         <div class="pull-right">
                <?php
                if($data){
                    $opts = array('class' => 'pagination m-t-0 m-b-10');
                    $this->widget('CLinkPager', array(
                        'currentPage'=>$pages->getCurrentPage(),
                        'itemCount'=>$item_count,
                        'pageSize'=>$page_size,
                        'maxButtonCount'=>6,
                        'htmlOptions' => $opts,
                        'selectedPageCssClass' => 'active',
                        'nextPageLabel' => '&raquo;',
                        'prevPageLabel'=>'&laquo;',
                        'lastPageLabel'=>'',
                        'firstPageLabel'=>'',
                        'header' => '',
                    ));
                }
                ?>                            
                </div>
    </div>
</div>



<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/bootbox.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true);?>/themes/admin/js/plugins/datatables/dataTables.bootstrap.css">
<script type="text/javascript">
   
    function changePageSize(page_size){        
        var cur = "<?php echo $_SERVER["REQUEST_URI"];?>";
        
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/site/formatcurrenturl",
            data: {'pageurl': cur, 'type' : 'page_size', 'page_size': page_size},
            dataType: 'json',
            success: function (result) {
                console.log(result);
                window.location = result.newurl;
            }
        });        
    }    
    
    function changeSort(attrib, order){        
        var cur = "<?php echo $_SERVER["REQUEST_URI"];?>";
        
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/site/formatcurrenturl",
            data: {'pageurl': cur, 'type' : 'sort', 'attrib' : attrib, 'order' : order},
            dataType: 'json',
            success: function (result) {
                console.log(result);
                window.location = result.newurl;
            }
        });        
    }
    
	var bootboxConfirm = 0;
	$(function () {
            $("a.confirm").bind('click',function(e) {
                e.preventDefault();
                var location = $(this).attr('href');
                var msg = $(this).attr('data-msg');
                confirmOperation(location, msg, 'Confirm Delete?');
            });
            
            $("a.statusupdate").bind('click',function(e) {
                e.preventDefault();
                var location = $(this).attr('href');
               // window.location.replace(location);
               var msg = $(this).attr('data-msg');
                confirmOperation(location, msg, 'Confirm status updation?');
            });            
		
	});
        
        
	function confirmOperation(location, msg, tlt){
            swal({
                    title: tlt,
                    text: msg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true,
                    html:true
                }, function() {
                  window.location.replace(location);
                });
            
	}
        </script>