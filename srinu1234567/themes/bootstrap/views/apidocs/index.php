<div class="container ">
    <div class="row">
		<div class="span3 bs-docs-sidebar">
			<?php $this->renderPartial('apimenu',array('permalink'=>$permalink));?>
		</div>
      <div class="span9">
        <!-- Overview================================================== -->
        <section id="overview">
			<div class="page-header">
				<h2><?php echo ucfirst(@$data->title);?></h2>
			</div>
			<p><?php echo ucfirst(@$data->description);?></p>
			<?php if(count($getItem)>0){?>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
					   <th>Endpoint</th>
					   <th>Description</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($getItem AS $key=>$val){?> 
					<tr>
						<td><?php echo '<a href=\'#'.$val->permalink.'\'>'.strtoupper($val->method_type)." ".$val->api_method.'</a>';?></td>
						<td><?php echo $val->short_desc;?></td>
					</tr>
			<?php }} ?>	
				</tbody>
			</table>
        </section>
		<?php if(count($getItem)>0){
		foreach($getItem AS $k => $v){?>
		<section id="<?php echo $v->permalink;?>">
			<div class="api-head">
				<h3><?php echo ucfirst($v->title);?></h3>
			</div>
			<p><?php echo ucfirst($v->description);?></p>
			<h3>Parameters</h3>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
					   <th>Name</th>
					   <th>Required?</th>
					   <th>Type</th>
					   <th>Description</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($v->api_request_params AS $ky => $vl){?> 
					<tr>
						<td><?php echo $vl->name;?></td>
						<td><?php if($vl->is_optional){echo 'Optional';}else{echo 'Yes';}?></td>
						<td><?php echo $vl->datatype;?></td>
						<td><?php echo $vl->comment;?></td>
					</tr>
			<?php } ?>	
				</tbody>
			</table>
			<h3>Example Response</h3>
			<div class="prityprintjson">
				<pre style="color:#445EF4;font-size: 16px;"><?php $json = json_decode($v->response);echo json_encode($json, JSON_PRETTY_PRINT);?></pre>
			</div>
		</section>
		<?php }}?>
        </div>
    </div>
</div>
<style type="text/css">
	.prityprintjson{
		max-height: 400px;
		max-width: 100%;
		overflow: auto;
	}
</style>

