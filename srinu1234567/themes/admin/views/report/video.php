<style>
.comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}   
.table > thead > tr > th{
    
    text-transform:none;
}
</style>
<?php
    $videoTotal = $viewsDetails['count']/$page_size;
    $videoMaxVisible = $videoTotal/2;
    if($videoMaxVisible < 5){
        $videoMaxVisible = $videoTotal;
    }
?>
<div class="row m-b-40">
    <div class="col-xs-12">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button>
    </div>
</div>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12 ">
                <div class="input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                    <div class="fg-line">
                        <div class="select">
                            <input class="form-control" type="text" id="video_date" name="searchForm[video_date]" value='<?php echo @$dateRange;?>' />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <td colspan="3"><b>Bandwidth</b></td>
                        </tr>
                        <tr>
                            <td style="width:5%"></td>
                            <td id="bd" colspan="2">Buffered Duration: <?php echo $this->timeFormat($bufferDuration['buffered_time']);?></td>
                        </tr>
                        <tr>
                            <td style="width:5%"></td>
                            <td id="bw" colspan="2">Bandwidth: <?php echo $this->formatKBytes($bandwidth['buffered_size']*1024,2);?></td>
                        </tr>
                        <tr style="display:<?php echo $bandwidth_cost?'block':'none';?>">
                            <td style="width:5%"></td>
                            <td id="bwc" colspan="2">Total Cost: $<?php echo $bandwidth_cost?$bandwidth_cost:0;?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>Views</b></td>
                        </tr>
                        <tr>
                            <td style="width:5%"></td>
                            <td id="wh" colspan="2">Hours watched: <?php echo $this->timeFormat($watchedHour['watched_hour']);?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="row-fluid">
            <div class="col-md-12">
                <div class="col-md-6">
                <div id="watchedchart" class="video-chart"></div>
                </div>
                <div class="col-md-6">
                    <div id="bandwidthchart" class="video-chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-t-40">
    <div class="col-sm-4">
        <div class="form-group input-group">
            <span class="input-group-addon">Device</span>
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm" id="device-box">
                        <option value="0">All</option>
                        <option value="1">Web</option>
                        <option value="2">Android</option>
                        <option value="3">iOS</option>
                        <option value="4">Roku</option>
                    </select>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-sm-4">
        <div class="form-group input-group">
            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
            <div class="fg-line">
                <input  class="form-control input-sm search" placeholder="Search" />
            </div>
        </div>
    </div>
    
    <?php if(isset($currency) && !empty($currency) && count($currency) > 1){?>
        <div class="col-sm-4">
            <div class="form-group input-group">
                <span class="input-group-addon">Currency</span>
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm" id="currency-box">
                            <?php foreach ($currency as $curr){?>
                                <option value="<?php echo $curr['id']?>" <?php if($curr['id'] == $default_currency){ echo 'selected=selected';}?> data-symbol = "<?php echo $curr['symbol']?>"><?php echo $curr['code']?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    <?php }else{?>
        <input type="hidden" id="currency-box" data-symbol = "<?php echo $currency_details->symbol?>" value="<?php echo $currency_details->id;?>"/>
    <?php }?>
    
    
</div>
<div class="row"> 
    <div class="table-responsive table-responsive-tabletAbove">
        <table class="table tablesorter" id="video-table-body"  >
            <thead>
            <th style="width:100px;">Name</th>
                <th>Transactions</th>
                <th>Revenue</th>
                <th>Total Views</th>
                <th>Unique Views</th>
                <th>Trailer Views</th>
                <th>Bandwidth</th>
                <!--<th>Top Device</th>
                <th>Top Geography</th>-->
            </thead>
            <tbody class="list">

            </tbody>
        </table>
    </div>
    <div class="loader text-center" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
</div>
<div class="h-40"></div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script src="../../common/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"--->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    $(function() {
        loadDateRangePicker('video_date','<?php echo $lunchDate;?>');
    });
    $('#video_date').change(function(){
        var searchText = $.trim($(".search").val());
        var date_range = $('#video_date').val();
        var device_type = $('#device-box').val();
        var currency_id = $('#currency-box').val();
        getVideoReport(date_range,device_type,searchText,currency_id,1);
    });
    $('#device-box').change(function(){
        var searchText = $.trim($(".search").val());
        var date_range = $('#video_date').val();
        var device_type = $('#device-box').val();
        var currency_id = $('#currency-box').val();
        getVideoReport(date_range,device_type,searchText,currency_id,0);
    });
    $('#currency-box').change(function(){
        var searchText = $.trim($(".search").val());
        var date_range = $('#video_date').val();
        var device_type = $('#device-box').val();
        var currency_id = $('#currency-box').val();
        getVideoReport(date_range,device_type,searchText,currency_id,0);
    });
    $('.report-dd').click(function(){
        var date_range = $('#video_date').val();
        var searchText = $.trim($(".search").val());
        var device_type = $('#device-box').val();
        var currency_id = $('#currency-box').val();
        var type = $(this).val();
        var monetization = $('#monetization').val();
        if(type == 'csv' || type == 'pdf'){
            window.location = '<?php echo Yii::app()->baseUrl."/report/getVideoReport?dt=";?>'+date_range+'&type='+type+'&search_value='+searchText+'&device_type='+device_type+'&currency_id='+currency_id+'&monetization='+monetization;
        }
    });

    $(document.body).on('keydown','.search',function (event){
        var searchText = $.trim($(".search").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            var date_range = $('#video_date').val();
            var device_type = $('#device-box').val();
            var currency_id = $('#currency-box').val();
            getVideoReport(date_range,device_type,searchText,currency_id,0);
        }
    });
  
</script>
<script type="text/javascript">
$(function () {
    Highcharts.setOptions({
        lang: {
            numericSymbols: 'k' //otherwise by default ['k', 'M', 'G', 'T', 'P', 'E']
        }
    });
    $('#watchedchart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: <?php echo $xdata?>
        },
        yAxis: {
            title: {
                text: 'Watched duration (in Seconds)'
            }
        },
        legend: {
            enabled:false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + ''+parseMillisecondsIntoReadableTime(this.y);;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    },
                    formatter: function() 
                    {
                        return ''+ parseMillisecondsIntoReadableTime(this.y);
                    }
                }
            }
        },
        series: <?php echo $graphData;?>,
        credits: {
            enabled: false
        },
        exporting: { 
            enabled: false
        }
    });
    $('#bandwidthchart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: <?php echo $xdata?>
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth consumed (in GB)'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled:false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: <?php echo $bandwidthGraphData;?>,
        credits: {
            enabled: false
        },
        exporting: { 
            enabled: false
        }
    });
});
</script>