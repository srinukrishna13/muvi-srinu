<?php
/**
 * Helper class has been writen for view interaction only
 * @author Gayadhar<support@muvi.com>
 */
class Helper extends AppComponent {
	
/**
 * @method public cagegorynameFromBvalue() It will return the category names based on the binary value
 * @param integer $binaryvalue Sum of the binary value
 * @param array $contentCategory Content Category array with binary value
 * @param string $returnType array or String
 * @author Gayadhar<support@muvi.com>
 * @return string Category list
 */
	function categoryNameFromBvalue($bvalue, $category=array(),$returnType='string'){
		if($bvalue && $category){
			$number = decbin($bvalue);
			$length = strlen($number)-1;
			for($i=0;$i<strlen($number);$i++){
				if($number[$i]){
					$index = pow(2,$length-$i);
					$categoryValue[] = $category[$index];
				}
			}
			if($returnType=='string'){
				return implode(',', @$categoryValue);
			}else{
				return @$categoryValue;
			}
		}else{
			return '';
		}
	}
/**
 * @method public getrelationalField(int $studio_id) It will return the films custom column mapped with custom fields
 * @author Gayadhar<support@muvi.com>
 * @param int $studio_id Description
 * @param int $atype Default it will return array key with fild name, 1-> It will return custom field name as key
 * @return array 
 */
	function getRelationalField($studio_id,$atype='',$formid=''){
		if(!$studio_id){
			$studio_id = Yii::app()->common->getStudioId();
		}
		$cond = 't.custom_field_id = f.id AND t.studio_id ='.$studio_id;
		if($formid){
			$cond .= ' AND t.custom_metadata_form_id ='.$formid;
		}
		$command = Yii::app()->db->createCommand()
				->from('film_custom_metadata t,custom_metadata_field f')
				->select('t.field_name,f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value,f.f_is_required')
				->where($cond);
			$rdata = $command->queryAll();
		if($rdata){
			$rkey = 'f_name';
			if($atype==1){
				$rkey = 'field_name';
			}
			foreach ($rdata AS $key => $val){
				$arr[$val[$rkey]]=$val;
			}
			return $arr;
		}else{
			return FALSE;
		}
	}
    /**
     * @method public getrelationalPGField(int $studio_id) It will return the Physical Goods custom column mapped with custom fields
     * @author Manas<manas@muvi.com>
     * @param int $studio_id Description
     * @return array 
     */
    function getRelationalPGField($studio_id) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $command = Yii::app()->db->createCommand()
                ->from('pg_custom_metadata t,custom_metadata_field f')
                ->select('t.field_name,f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value,f.f_is_required')
                ->where('t.custom_field_id = f.id AND t.studio_id =' . $studio_id);
        $rdata = $command->queryAll();
        if ($rdata) {
            foreach ($rdata AS $key => $val) {
                $arr[$val['f_name']] = $val;
            }
            return $arr;
        } else {
            return FALSE;
        }
    }
    /**
     * @method public getrelationalPGField(int $studio_id) It will return the Physical Goods custom column mapped with custom fields
     * @author Manas<manas@muvi.com>
     * @param int $studio_id Description
     * @return array 
     */
	function getSubcategoryList($category_value,$studio_id){
		if($category_value){
            //$subCatData = ContentSubcategory::model()->findAll('studio_id=:studio_id AND category_value & :category_value', array(':studio_id' => $studio_id, ':category_value' => (int) $category_value));
			$cat_cond = '';
			foreach ($category_value as $key => $value) {
				$cat_cond .= "FIND_IN_SET($value,category_value) OR ";
			}
			$cat_cond = rtrim($cat_cond, 'OR ');
			$subCatData = Yii::app()->db->createCommand()
                ->from('content_subcategory')
                ->select('*')
                ->where("studio_id=".$studio_id." AND (".$cat_cond.")")
				->setFetchMode(PDO::FETCH_OBJ)
				->queryAll();
			return $subCatData;
		}else{
			return false;
		}
	}
/**
 * @method public muviPrint(mix	$variable) It will print any output for Muvi IP only
 * @author Gayadhar<support@muvi.com>
 * @return Mix Depends on the input
 */
	function muviPrint($pinput,$is_exit){
		if($_SERVER['REMOTE_ADDR'] == '111.93.166.194'){
			echo "<!-- ";
			print_r($pinput);
			echo "-->";
			if($is_exit){
				exit;
			}
		}else{
			return false;
		}
	}
    function getRelationalEpisodeField($studio_id){
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $command = Yii::app()->db->createCommand()
                ->from('movie_streams_custom_metadata t,custom_metadata_field f')
                ->select('t.field_name,f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value,f.f_is_required')
                ->where('t.custom_field_id = f.id AND t.studio_id =' . $studio_id);
        $rdata = $command->queryAll();
        if ($rdata) {
            foreach ($rdata AS $key => $val) {
                $arr[$val['f_name']] = $val;
            }
            return $arr;
        } else {
            return FALSE;
        }
    }
    
    function generateUniqNumber($id = NULL) {
        $uniq = uniqid(rand());
        if ($id) {
            return md5($uniq . time() . $id);
        } else {
            return md5($uniq . time() . rand(1, 999));
}
    }

    function rulesInputType($policyArr, $myPolicyArr, $rule=null) {
        if(empty($myPolicyArr) && $rule==null){
            $myPolicyArr['policy_value'] = $policyArr['default_values'];
        }
        $htmlData = '<input type="hidden" name="policy[id][]" value="' . $policyArr['id'] . '" />';
        $htmlData .= '<input type="hidden" name="policy[name][]" value="' . $policyArr['policy_name'] . '" />';
        if ($policyArr['policy_field_type'] == 'text') {
            $htmlData .= '<input type="number" name="policy[value][]" class="form-control input-sm checkInput" value="' . $myPolicyArr['policy_value'] . '" pattern="\d*" ' . ($policyArr['id'] == 3 || $policyArr['id'] == 4 ? 'style="width: 70%; float: left;"' : '') . ' placeholder="Enter ' . $policyArr['policy_name'] . '.." min="0" oninput="validity.valid||(value=\'\');" />';
            if ($policyArr['id'] == 3 || $policyArr['id'] == 4) {
                /* watchd duration type day or hour */
                $htmlData .= '<div class="select" style="width:27%; float:right;"><select name="policy[duration_type][]" class="form-control input-sm">'
                        . ($policyArr['id'] == 3 ? '<option value="1"' . (1 == $myPolicyArr['duration_type'] ? ' selected' : '') . '>Hour(s)</option>' : '')
                        . '<option value="2"' . (2 == $myPolicyArr['duration_type'] ? ' selected' : '') . '>Day(s)</option>'
                        . ($policyArr['id'] == 4 ? '<option value="3"' . (3 == $myPolicyArr['duration_type'] ? ' selected' : '') . '>Month(s)</option>' : '')
                        . '</select></div>';
            } else {
                $htmlData .= '<input type="hidden" name="policy[duration_type][]" value="0" />';
            }
        } else if ($policyArr['policy_field_type'] == 'textarea') {
            $htmlData .= '<textarea name="policy[value][]" class="form-control textarea checkInput" placeholder="Enter ' . $policyArr['policy_name'] . '..">' . $myPolicyArr['policy_value'] . '</textarea><input type="hidden" name="policy[duration_type][]" value="0" />';
        } else if ($policyArr['policy_field_type'] == 'dropdown') {
            $options = explode(',', $policyArr['policy_option']);
            $htmlData .= '<div class="select"><select name="policy[value][]" class="form-control input-sm">';
            $htmlData .= '<option value="">Select</option>';
            foreach ($options as $option) {
                $htmlData .= '<option value="' . $option . '" ' . ($option == $myPolicyArr['policy_value'] ? 'selected' : '') . '>' . ucwords($option) . '</option>';
            }
            $htmlData .= '</select><input type="hidden" name="policy[duration_type][]" value="0" /></div>';
        }
        return $htmlData;
    }

    function rulesSelectBox($rules_id, $policyStatus = null, $isPurchased=false) {
        if (!$policyStatus) {
            $policyStatus = PolicyRules::model()->getConfigRule();
        }
        $policies = Yii::app()->policyrule->getAllPolicyRules();

        $str = '<select class="form-control input-sm rmvdisable" id="policies" name="data[rule]" ' . ($policyStatus == false || $isPurchased==true ? 'disabled' : '') . '>
            <option value="">Select Rule</option>';
        foreach ($policies as $policy) {
            $seleced = !empty($rules_id) && $rules_id == $policy->id ? ' selected' : '';
            $str .= '<option value="' . $policy->id . '"' . $seleced . '>' . ucwords($policy->name) . '</option>';
        }
        $str .= '</select>';
        return $str;
    }
    
	/**
	* @method public categorynameFromCommavalues() It will return the category names based on the comma separated value
	* @param string comma separated content category value
	* @param array $contentCategory Content Category array with comma separated value
	* @param string $returnType array or String
	* @author Manas<manas@muvi.com>
	* @return string Category list
	*/
	function categoryNameFromCommavalues($cvalue, $category=array(),$returnType='string'){
		if($cvalue && $category){
			$data = explode(',', $cvalue);
			foreach ($data as $key => $value) {
				$categoryValue[] = $category[$value];
			}
			if($returnType=='string'){
				return implode(',', @$categoryValue);
			}else{
				return @$categoryValue;
			}
		}else{
			return '';
		}
	}
	function getLanuageCustomValue($data,$langcontent){
		@$data['custom1'] = @$langcontent->custom1;
		@$data['custom2'] = @$langcontent->custom2;
		@$data['custom3'] = @$langcontent->custom3;
		@$data['custom4'] = @$langcontent->custom4;
		@$data['custom5'] = @$langcontent->custom5;
		@$data['custom6'] = @$langcontent->custom6;
		@$data['custom7'] = @$langcontent->custom7;
		@$data['custom8'] = @$langcontent->custom8;
		@$data['custom9'] = @$langcontent->custom9;
		if(@$langcontent->custom10){
            $x = json_decode(@$langcontent->custom10,true);
            foreach ($x as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $data[$key1] = $value1;
				}
            }
        }
		return @$data;
	}
    function getEpisodeLanguageCustomValue($data,$langcontent){
		@$data['custom1'] = @$langcontent->custom1;
		@$data['custom2'] = @$langcontent->custom2;
		@$data['custom3'] = @$langcontent->custom3;
		@$data['custom4'] = @$langcontent->custom4;
		@$data['custom5'] = @$langcontent->custom5;
		if(@$langcontent->custom6){
            $x = json_decode(@$langcontent->custom6,true);
            foreach ($x as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $data[$key1] = $value1;
				}
            }
        }
		return @$data;
	}
	function getAdminEpisodeLanguageCustomValue($data,$langcontent){
		@$data['cs1'] = @$langcontent->custom1;
		@$data['cs2'] = @$langcontent->custom2;
		@$data['cs3'] = @$langcontent->custom3;
		@$data['cs4'] = @$langcontent->custom4;
		@$data['cs5'] = @$langcontent->custom5;
		if(@$langcontent->custom6){
            $x = json_decode(@$langcontent->custom6,true);
            foreach ($x as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $data[$key1] = $value1;
				}
            }
        }
		return @$data;
	}
}
