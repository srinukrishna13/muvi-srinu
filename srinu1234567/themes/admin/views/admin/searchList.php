<style type="text/css">
.btn_add_movie{
	border-radius: 4px !important;
	box-shadow: 2px 2px 2px #999 !important;
}
#addmovie_popup .modal-footer{
	margin-top: 0px !important;
}
#addmovie_popup .modal-body{
	padding-bottom: 0px !important;
}
.ui-autocomplete{
	z-index: 999999;
}
</style>
<div class="box-body table-responsive no-padding">
	<!--<button class="btn bg-olive btn-flat margin btn_add_movie" onclick="addMovie(<?php echo Yii::app()->user->id;?>);"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Movie</button>-->
	<a href="<?php echo $this->createUrl('admin/addMovie');?>"><button class="btn bg-olive btn-flat margin btn_add_movie" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Movie</button></a>
    <table class="table table-hover">
            <tbody>
                <tr>
                    <th>S/L#</th>
                    <th>Preview</th>
                    <th>Movie Name</th>
<!--                    <th>Details</th>
                    <th>Action</th>-->
                </tr>
                <?php
				$cnt=1;
				if($data){
					//foreach($data AS $key=>$val){
					foreach($data AS $key=>$details){
						// Arrange the records for view file
						 //$details = json_decode($val['data'],TRUE);
						if(@$details['0']['thumb_image']){
						   $img_path = $details['0']['thumb_image'];
						}else{
						   $img_path = Yii::app()->baseUrl.'/images/no-image.png';
						}
						$movie_id = $details[0]['id'];
                                                if(@$details[0]['content_types_id']==3){
                                                        $dtlsurl = $this->createUrl('admin/multipartcontent',array('movie_id'=>$movie_id,'uniq_id'=>$details[0]['uniq_id']));
                                                }else{
                                                        $dtlsurl = $this->createUrl('admin/editMovie/',array('movie_id'=>$movie_id,'uniq_id'=>$details[0]['uniq_id']));
                                                }
						
						 //echo $movie_id;exit;
//						 $freeviewcount = isset($viewcount[$movie_id]['f'])?$viewcount[$movie_id]['f']:0;
//						 $paidviewcount = isset($viewcount[$movie_id]['p'])?$viewcount[$movie_id]['p']:0;
					?>
					<tr>
						<td><?php echo $cnt++;?> </td>
						<td>
							
							<a href="<?php echo $dtlsurl;?>" class="" rel="tooltip" data-title="Tooltip"><img src="<?php echo $img_path;?>" alt=""></a></td>
						<td>
							<strong><a href="<?php echo $dtlsurl;?>"><?php echo $details[0]['name'];?></a> </strong><br/>
							<i><?php echo @$details[0]['actor'];?></i>
						</td>
<!--						<td>
							No of free viewer&nbsp; <em><strong><a href="<?php echo $this->createUrl('admin/movieDetailsGraph',array('movieid'=>$movie_id));?>"><?php echo @$freeviewcount;?></a></strong></em>
							br/>No of Youtube viewer <i><b><a href="<?php echo $this->createUrl('admin/movieDetailsGraph',array('movieid'=>$movie_id));?>">10</a></b></i
							<br/>No of Paid viewer &nbsp;<em><strong><a href="<?php echo $this->createUrl('admin/movieDetailsGraph',array('movieid'=>$movie_id))?>"><?php echo @$paidviewcount;?></a></strong></em>
						</td>
						<td><a href="<?php echo $this->createUrl('admin/addMovie',array('movieid'=>$movie_id));?>" ><span class="glyphicon glyphicon-pencil" title="Edit"></span></a> &nbsp; <a href="<?php echo $this->createUrl('admin/removeMovie',array('movie_id'=>$movie_id));?>" onclick="return confirm('Are you sure you want to remove the movie from Studio?')"><span class="glyphicon glyphicon-remove" title="Remove"></span></a></td>-->
					</tr>
			<?php	}?>
			<div class="pull-right">
			<?php 	
				$this->widget('CLinkPager', array(
					'currentPage'=>$pages->getCurrentPage(),
					'itemCount'=>$item_count,
					'pageSize'=>$page_size,
					'maxButtonCount'=>6,
					'nextPageLabel'=>'Next &gt;',
					'header'=>'',
				));
			?>
			</div>	
			<?php }else{?>
					<tr>
						<td colspan="5"><p class="text-red">Opps! No movie found with the searched keyword's.</p></td>
					</tr>
				<?php }  ?>
        </tbody>
    </table>
</div>
<div id="addmovie_popup" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Movie to Studio</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
					<label for="movie_name" class="required">Movie Name</label>
					<input type="text" placeholder="Enter movie name" id="movie_name" class="form-control">
				</div>
                <div class="form-group">
					<label for="youtubeLink">Youtube Link</label>
					<input type="text" placeholder="Enter Youtube link..." id="exampleInputEmail1" class="form-control">
				</div>
                <div class="form-group">
					<label for="">$0.50(You wil be able to change this price in future)</label>
				</div>
				<input type="hidden" value="" name="movie_id" id="movie_id" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
