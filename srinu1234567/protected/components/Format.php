<?php
/**
 * Format is the class which contains some common formating methods Like Validating image, DateFormat etc
 * 
 */
Yii::import('ext.EGeoIP');
class Format{

/**
 *@method checkvalidImages($file,$maxSize,$minWidth,$minHeight,$imageType)
 * @param array $file Input file details  
 * @param array $imagType Type of image you want to check i.e image/jpg,image/png
 * @param ing $maxSize Maximum image size you want to allowed to upload
 * @param int $minWidth Minimum width of the image 
 * @param int $minHeight Minimum height of the image
 * @return array Containing image information
 */
public function checkValidImages($file,$maxFileSize,$minWidth='',$minHeight='',$allowedImages=''){
		$errors             = array();
		if(!$allowedImages){
			$allowedImages      = array('image/jpg','image/jpeg','image/png','img/gif');
		}
		//$invalidSize        = 0;
		//$invalidTypes       = 0;
		//$invalidDimension   = 0;
		if(!$maxFileSize){
			$maxFileSize = 549755813888;
		}
		// Check atleast one image has been selected
		$tmp_name = $file['tmp_name'];
		if (!empty($tmp_name)){
			$image = $file["name"];
			$path_info = pathinfo($image);
			if ($path_info['extension'] == 'jpg' || $path_info['extension'] == 'jpeg' || $path_info['extension'] == 'png' || $path_info['extension'] == 'gif'){
				if ($file["size"] <= $maxFileSize){
					$image_d = getimagesize($file['tmp_name']); 
					if($minWidth && $minHeight && ($image_d[0] < $minWidth) && ($image_d[1] < $minHeight)){
						$errors[]="invalid dimension";
					}else{
						$errors[]="valid";
					}
				}else{
					$errors[]="invalid file size";
				}
			}else{
				$errors[]="format not supported";
			}

		}else{
			$errors[] = 'Please select at least one image.';
		}
		return $errors;
    }
/**
 * @method public methodName(type $paramName) Remove all the files and subdirectory in a dirctory and then finally deleting the directory 
 * @return bool true/false
 * @author GDR<support@muvi.com>
 * @uses actionCropVideoThumbnail,
 */
	function recursiveRemoveDirectory($directory){
		foreach(glob("{$directory}/*") as $file){
			if(is_dir($file)) { 
				recursiveRemoveDirectory($file);
			}else{
				unlink($file);
			}
		}
		rmdir($directory);
	}
/**
 * @method public getCountries() Retrive all the countries in a associative array
 * @author GDR<support@muvi.com>
 * @return array Associative array
 */	
	function getCountries(){
		
		$countries = Countries::model()->findAll();
		$countriesArr = CHtml::listData( $countries, 'code' , 'country');
		return $countriesArr;
	}
/**
 * @param type $store
 * @return array
 * @method Public getStoreDetails(int $store_id) Details of the store
 * @author GDR<support@muvi.com>
 */
	public function getStoreDetails($studio_id=''){
		$studio_id = $studio_id?$studio_id:Yii::app()->user->studio_id?Yii::app()->user->studio_id:'';
		if($studio_id){
			$data = Studio::model()->findByPk($studio_id);
			if($data){
				$data = $data->attributes;
				return $data;
			}
		}else{
			return FALSE;
		}

                $store_details = array(
                    'name' => SITE_NAME,
                    'adminEmail' => 'support@demo.com',
                    'siteName'=>SITE_NAME,
                    'agent'=>SUB_DOMAIN,
                    'lunchDate' => LUNCH_DT
                );
        return $store_details;
    }
 /**
  * @method public randompassword() It will generate random password from a combination of Alpha numeric
  * @author Gayadhar<support@muvi.com>
  * @return String It will return a password string
  */  
	function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
/**
 * 
 */
	function make_links_clickable($text){
		return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $text);
	}
}

