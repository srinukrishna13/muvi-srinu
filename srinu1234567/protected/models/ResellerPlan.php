<?php

class ResellerPlan extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        if (@IS_DEMO_PAYMENT == 1) {
            return 'reseller_plans_demo';
        } else {
            return 'reseller_plans';
        }
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    /*
     * @method getMuviPlansResellers getting the level value
     * @return array
     * @author SNL
     */
    
    public function getResellerPlan($level_id = NULL) {
        $data = array();
        
        $cond = "";
        if (isset($level_id) && intval($level_id)) {
            $cond = " id=$level_id AND ";
        }
        
        if (@IS_DEMO_PAYMENT == 1) {
            $table = 'reseller_plans_demo';
        } else {
            $table = 'reseller_plans';
        }
        
        $level = Yii::app()->db->createCommand()
        ->select('p.*')
        ->from($table.' p')
        ->where(" $cond status = 1 order by p.sequence asc")
        ->queryAll();
        
        if (isset($level) && !empty($level)) {
            $data['level'] = $level;
        }
        
        return $data;
    }
    public function getResellerPlanePrice($planId){
        if (@IS_DEMO_PAYMENT == 1) {
            $table = 'reseller_plans_demo';
        } else {
            $table = 'reseller_plans';
        }
        $price = Yii::app()->db->createCommand()
                ->select('rp.base_price')
                ->from('reseller_plans rp')
                ->where("rp.id='".$planId."'")
                ->queryAll();
        return $price;
    }
    public function getResellerpackeg($id){
            if (@IS_DEMO_PAYMENT == 1) {
                $table = 'reseller_plans_demo';
            } else {
                $table = 'reseller_plans';
            }    
        $packeg = Yii::app()->db->createCommand()
                ->select("p.*")
                ->from($table.' p')
                ->where("id='".$id."'")
                ->queryAll();
        return $packeg;
    }  

}
