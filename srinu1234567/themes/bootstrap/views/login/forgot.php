<div id="login_form">
    <form name="forgot" method="post" role="form" onsubmit="javascript: return send_instruction();">
        <div class="form-group">
            <div id="forgot_loading1" class="hide"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif"></div>
        </div>
        <div class="form-group"><label>Email :</label>
            <input name="email" id="forgot_email" type="text" class="form-control" placeholder="Enter your Email Address" />
        </div>
        <div class="form-group">            
            <div class="error hide">Email can not be blank!</div>
            <div class="clear" style="height:20px;"></div>
        </div>
        <div class="form-group">
            <input type="submit" value="Send reset password instruction" name="yt0" class="btn btn-blue" id="login" />
        </div>
    </form>
</div>
<div id="success" class="alert alert-success hide">Forgot Password instructions are sent to your email. <a href="#" id="clk">Click here</a> to go to login page.</div>
<style>
    .btn.btn-blue{font-size: 18px;}    
    #login_form input[type="text"]{text-transform: none;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#clk').click(function () {
            parent.$('#ForgotModal').modal('hide');
            parent.$('#LoginModal').modal('show');
        });
    });
    function send_instruction() {
        var email = $("#forgot_email").val();
        if (email != "") {
            $.ajax({
                url: "<?php echo Yii::app()->getbaseUrl(true); ?>/login/forgot",
                data: {email: email},
                type: 'POST',
                dataType: "json",
                success: function (data) {
                    if (data.status == 'success')
                    {
                        $('.error').html('');
                        $('.error').hide();
                        $('#login_form').hide();
                        $('#success').show();
                    }
                    else
                    {
                        $('.error').html(data.message);
                        $('.error').show();
                        return false;
                    }
                }
            });
        } else {
            $(".error").show();
        }
        return false;
    }
</script>