<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/icon.png" type="image/png" rel="icon">
    <meta name="robots" content="NOINDEX, NOFOLLOW" />
    <title>Website Under Construction</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,400italic,500italic,500,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <?php Yii::app()->bootstrap->register(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />    
     
        
    <?php
    if(strpos($_SERVER['HTTP_HOST'], 'muvi.in')){
    ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-60904088-1', 'auto');
      ga('send', 'pageview');

    </script>    
    <?php
    }    
    else if($_SERVER['HTTP_HOST'] != 'localhost'){?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-57762605-1', 'auto');
      ga('send', 'pageview');

    </script>
    <?php }
    ?> 
    
</head>

    <?php
    $is_home = 0;
    $class = '';
    if ((Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId()) == 'site/index'  )
    {
        $is_home = 1;
        $class = Yii::app()->controller->getId();
    }
    else
    {
        $class = Yii::app()->controller->getId();
    }    
    ?>
    <body class="<?php echo $class?>">

    <?php //require_once 'header.php';?>
    

    <?php echo $content; ?>

    
    <?php //require_once 'footer.php'; ?>
</body>
</html>
