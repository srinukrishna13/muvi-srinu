<?php
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
$applications = json_encode($packages['appilication']);
$app_price = Yii::app()->general->getAppPrice($packages['package'][0]['code']);
?>
<div class="row m-t-40">
    <div class="col-sm-12">
        <h4>Thank you for coming back!</h4>
        <h4>Please purchase subscription to continue</h4>
    </div>
</div>
<div class="row m-t-20 m-b-40 Purchase-Subscription">
    <form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
        <div class="col-sm-12">
            <div class="row m-t-20" id="packagediv">
                 <div class="col-sm-12">
                     <div class="form-group">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Package </label>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="packages" class="form-control input-sm" onchange="getAppilication(this);">
                                                    <?php 
                                                    $charging_now = 0;
                                                    $yearly_discount = 0;
                                                    if (isset($packages['package']) && !empty($packages['package'])) {
                                                        $cnt = 0;
                                                        foreach ($packages['package'] as $key => $value) {
                                                            if ($cnt == 0) {
                                                                $charging_now = $value['base_price'];
                                                                $yearly_discount = $value['yearly_discount'];
                                                            }
                                                            $cnt++;?>
                                                        <option value="<?php echo $value['id'];?>" data-code="<?php echo $value['code'];?>"  data-price="<?php echo $value['base_price'];?>" data-yearly_discount="<?php echo $value['yearly_discount'];?>" data-is_bandwidth="<?php echo $value['is_bandwidth'];?>"><?php echo $value['name'];?></option>
                                                    <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="appilication_div">

                            <?php if (isset($packages['appilication'][$packages['package'][0]['id']]) && !empty($packages['appilication'][$packages['package'][0]['id']])) { ?>
                                <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Applications</label>
                                <div class="col-sm-12">

                                   <?php foreach ($packages['appilication'][$packages['package'][0]['id']] as $key => $value) {?>
                                        <label class="checkbox checkbox-inline m-r-20">
                                           <input type="checkbox" class="price_chkbox" data-id="<?php echo $value['id'];?>" data-code="<?php echo $value['code'];?>" data-price="<?php echo $value['price'];?>" <?php if ($value['code'] == 'monthly_charge') { ?>checked="checked"<?php } ?> onclick="selectApplication();" />
                                            <i class="input-helper"></i> <?php echo $value['name'];?>
                                        </label>

                                    <?php } ?>
                                </div>
                                <div class="col-sm-12 grey">
                                    First application is charged at $<?php echo $charging_now;?> per month, others are $<?php echo $app_price; ?> per month per app
                                </div>

                            <?php } ?>
                        </div>
                        <div id="cloud_hosting-div" class="form-group" style="display: none;">
                            <div class="col-sm-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" onclick="cloudHosting(this);"/><i class="input-helper"></i> Add Cloud hosting for your application (powered by Amazon Web Services)
                                    </label>
                                </div>
                                <div class="lead">
                                    <label id="cloud_hosting-error" class="error red" for="cloud_hosting" style="display: none;"></label>
                                </div>
                            </div>	
                        </div>
                        <div id="custom_code-div" class="form-group" style="display: none;">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Custom Code</label>
                            <div class="col-sm-12">
                               
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="custom_code" placeholder="Code given to you by Muvi representive" autocomplete="false" />
                                </div>
                                <div class="lead">
                                    <label id="custom_code-error" class="error" for="custom_code" style="display: none;"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4"> Select Plan</label>
                            <div class="col-sm-12">
                                
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="all_plans" value="Month" name="plans" checked="checked" onclick="showPlan(this);" /><i class="input-helper"></i>Monthly
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 grey">
                                        Pay month by month, cancel anytime
                                    </div>
                                </div>
                            
                               
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="all_plans" value="Year" name="plans" onclick="showPlan(this);" /><i class="input-helper"></i>Yearly <?php if (intval($yearly_discount)) { ?>(save <?php echo $yearly_discount; ?>%)<?php } ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 grey">
                                         <?php if (intval($yearly_discount)) { ?>Save <?php echo $yearly_discount;?>% by paying for a year in advance.<?php } ?> No refund if canceled during the year
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Projected Pricing</label>
                            <div class="col-sm-12">

                                <div class="row">
                                    <div class="col-sm-12">
                                        Charging to your card now: <label class="control-label">
                                            $<span class="charge_now"><?php echo $charging_now; ?></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 monthly_payment">
                                        <div>
                                            Monthly payment: 
                                            <label class="control-label">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per month <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                                        </div>
                                        <div>
                                            Next Billing date: <label class="control-label"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 yearly_payment" style="display: none;">
                                        <div>
                                            Yearly payment: 
                                            <label class="control-label">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per year <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                                        </div>
                                        <div>
                                            Next Billing date: <label class="control-label"><?php echo Date('F d, Y', strtotime("+1 Years")); ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 m-t-10">
                                See <a href="<?php echo Yii::app()->baseUrl; ?>/pricing" target="_blank">pricing</a> for more detail. Contact your Muvi representative if you are not sure about the package and add-ons you need.
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 m-t-30">
                                <button type="button" class="btn btn-primary btn-sm" id="nextbutn" onclick="showCards();">Continue to Authorize Payment</button>
                            </div>
                        </div>
                 </div>
            </div>
             <input type="hidden" name="plan" id="plantext" value="" />
            <input type="hidden" name="packages" id="packagetext" value="" />
            <input type="hidden" name="pricing" id="pricingtext" value="" />
            <input type="hidden" name="cloud_hosting" id="cloud_hosting" value="" />
            <input type="hidden" name="custom_code" id="custom_codes" value="" />
            
            <div id="carddetaildiv" class="row" style="display: none;">
                    <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Projected Pricing</label>
                    <div class="col-sm-12">


                        <div class="row">
                            <div class="col-sm-12">
                                Charging to your card now: <label class="control-label">
                                    $<span class="charge_now"><?php echo $charging_now; ?></span>
                                </label>
                            </div>
                            <div class="col-sm-12 monthly_payment">
                                <div>
                                    Monthly payment: 
                                    <label class="control-label">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per month <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                                </div>
                                <div>
                                    Next Billing date: <label class="control-label"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></label>
                                </div>
                            </div>
                            <div class="col-sm-12 yearly_payment" style="display: none;">
                                <div>
                                    Yearly payment: 
                                    <label class="control-label">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per year <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                                </div>
                                <div>
                                    Next Billing date: <label class="control-label"><?php echo Date('F d, Y', strtotime("+1 Years")); ?></label>
                                </div>
                            </div>
                        </div>
<?php if (isset($card) && !empty($card)) { ?>
                            <div class="form-group">
                                <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Payment Information</label>
                                <div class="col-sm-12">


                                    <div class="row">
                                        <div class="col-sm-6">							
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="payment_info" id="used_card" onclick="paymentInfo(this);" value="saved" /><i class="input-helper"></i>Use the saved card 
                                                </label>
                                            </div>
                                        <div class="col-sm-12">	
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select name="creditcard" id="creditcard" class="form-control input-sm">
                                                        <?php foreach ($card as $key => $value) { ?>
                                                            <option value="<?php echo $value->id; ?>" <?php if (isset($value->is_cancelled) && $value->is_cancelled == 0) { ?>selected="selected"<?php } ?>><?php echo $value->card_last_fourdigit . " " . $value->card_type; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                            <label>
                                                <input type="radio" name="payment_info" id="new_card" onclick="paymentInfo(this);" value="new" /><i class="input-helper"></i>Or, add a new card
                                            </label>
                                            </div>
                                        </div>
                                    </div>   


                                    <div>
                                        <label id="payment_info-error" class="error red" for="payment_info" style="display: none;"></label>
                                    </div>
                                </div>    
                            </div>

                        <?php } ?>

                                       <div id="new_card_dv" class="row m-t-30" style="display: none;"></div>

                        <div id="terms-div" class="m-t-10"  style="display: none;">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="terms_of_use" id="terms_of_use"><i class="input-helper"></i>                                  
                                            I agree with the <a href="<?php echo Yii::app()->baseUrl; ?>/signup/signupTerms" target="_blank">terms of use</a> of Muvi
                                        </label>
                                        <div><label id="terms_of_use-error" class="error red" for="terms_of_use" style="display: none;"></label></div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-sm" id="nextbtn" >Reactivate</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </form>
        </div>
    </div>


    <div id="new_card_info"  style="display: none;">
        <div id="card-info-error" class="error red" style="display: none;"></div>
        <div class="col-sm-6">

            <div class="form-group">
                <label class="control-label col-sm-4">Name on Card:</label>                    
                <div class="col-sm-8">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
                    </div>
                </div>

            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Card Number:</label>                    
                <div class="col-sm-8">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Expiry Date:</label>                    
                <div class="col-sm-4">
                    <div class="fg-line">
                        <div class="select">
                            <select name="exp_month" id="exp_month" class="form-control input-sm">
                                <option value="">Expiry Month</option>	
                                <?php for ($i = 1; $i <= 12; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="fg-line">
                        <div class="select">
                            <select name="exp_year" id="exp_year" class="form-control input-sm" onchange="getMonthList();">
                                <option value="">Expiry Year</option>
                                <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-4">Security Code:</label>                    
                <div class="col-sm-8">
                    <div class="fg-line">
                        <input type="password" id="" name="" style="display: none;" />
                        <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" />
                    </div>
                </div>
            </div>
        </div>  
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label col-sm-4">Billing Address:</label>                    
                <div class="col-sm-8">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode" />
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
                 




<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Your account has reactivated by submitting your card information successfully.</h4>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="price_per_app" id="price_per_app" value="<?php echo $app_price; ?>" />
<?php if (isset($card) && empty($card)) { ?>
<script type="text/javascript">
    $(document).ready(function(){
        $( "#new_card_dv" ).html($("#new_card_info").html()).show();
        $("#terms-div").show();
    });
</script>
<?php } ?>

<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    var pricingall = '';
    var packagesall = '';
    var planall = '';
    var custom_codes = '';

    $(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
        
        if($('#custom_code-div').length) {
            $('#custom_code').on('change', function () {
                if ($.trim($("#custom_code").val())) {
                    getPackageCustomeCodePrice(0);
                } else {
                    custom_codes = '';
                    $('option:selected', '#packages').attr('data-price', 0);
                    showPrice();
                }
            });
        }
    });
    
    function showCards() {
        $('#packagediv').hide();
        showPrice();
        $('#carddetaildiv').show();
    }
    
    function validateCustomCode() {
        if ($.trim($("#custom_code").val())) {
            getPackageCustomeCodePrice(1);
        } else {
            $("#custom_code").focus();
            $('#custom_code-error').show().html('Enter custom code');
            return false;
        }
    }
    
    function getPackageCustomeCodePrice(arg) {
        var url = "<?php echo Yii::app()->baseUrl; ?>/payment/validatePackageCustomCode";
        var package_code = $('option:selected', '#packages').attr('data-code')
        var custom_code = $.trim($('#custom_code').val());

        $.post(url, {'package_code': package_code, 'custom_code': custom_code}, function (res) {
            if (parseInt(res.isExists)) {
                $('#custom_code-error').html('').hide();
                
                custom_codes = custom_code;
                $('option:selected', '#packages').attr('data-price', res.price);
                showPrice();
                
                if (parseInt(arg)) {
                    $("#custom_codes").val(custom_codes);
                    showCards();
                }
            } else {
                custom_codes = '';
                $('option:selected', '#packages').attr('data-price', 0);
                showPrice();
                $('#custom_code-error').show().html('Incorrect custom code. Please ask your Muvi representative');
                return false;
            }
        }, 'json');
    }
    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;
        
        if (curyr === selyr) {
            startindex = curmonth;
        }
        
        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" '+selected+'>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    
    function paymentInfo(obj) {
        var res = pricingall.split("");
        
        $('#paymentMethod')[0].reset();
        $(obj).prop('checked', true);
        
        //Reset prices
        $("#packagetext").val(packagesall);
        $("#pricingtext").val(pricingall);
        $("#plantext").val(planall);
        $("#custom_codes").val(custom_codes);
        
        $('.price_chkbox').each(function() {
            var priceid = $(this).attr('data-id');
            var index = res.indexOf(priceid);
            if(index !== -1) {
                $(this).prop('checked', true);
            }
        });
        
        if ($("#new_card:checked").length) {
            $( "#new_card_dv" ).html($("#new_card_info").html());
            $( "#new_card_dv" ).slideDown( "slow");
        } else {
            $( "#new_card_dv" ).html('');
            $( "#new_card_dv" ).slideUp( "slow");
        }
        $("#terms-div").show();
        $("#terms_of_use-error").hide();
    }
    
    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                payment_info: "required",
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                terms_of_use: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                payment_info: "Please select payment information",
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                terms_of_use: "Please select terms of use",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
            switch (element.attr("name")) {
                case 'exp_month':
                    error.insertAfter(element.parent().parent());
                    break;
                case 'exp_year':
                    error.insertAfter(element.parent().parent());
                    break;
                default:
                    error.insertAfter(element.parent());
            }
        }
        });
        var x = validate.form();
        if (x) {
            var is_used_card = 0;
            if ($("#used_card:checked").length) {
                is_used_card = 1;
            }
            
            $("#packagetext").val(packagesall);
            $("#pricingtext").val(pricingall);
            $("#plantext").val(planall);
            $("#custom_codes").val(custom_codes);
            
            if (parseInt(is_used_card)) {
                $('#nextbtn').html('wait!...');
                $('#nextbtn').attr('disabled','disabled');
                var url ="<?php echo Yii::app()->baseUrl; ?>/payment/purchaseSubscribe";
                document.paymentMethod.action = url;
                document.paymentMethod.submit();return false;
            } else {
                $('#nextbtn').html('wait!...');
                $('#nextbtn').attr('disabled','disabled');
                $("#loadingPopup").modal('show');
                var url ="<?php echo Yii::app()->baseUrl; ?>/payment/authenticateCard";
                var card_name = $('#card_name').val();
                var card_number = $('#card_number').val();
                var exp_month = $('#exp_month').val();
                var exp_year = $('#exp_year').val();
                var cvv = $('#security_code').val();
                var address1 = $('#address1').val();
                var address2 = $('#address2').val();
                var city = $('#city').val();
                var state = $('#state').val();
                var zip = $('#zipcode').val();
                var pricing = pricingall;
                var packages = packagesall;
                var plan = planall;
                var cloud_hosting = $.trim($("#cloud_hosting").val());
                var custom_code = $.trim($("#custom_codes").val());
                
                $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'packages': packages, 'pricing': pricing, 'plan': plan, 'cloud_hosting': cloud_hosting, 'custom_code': custom_code}, function (data) {
                    $("#loadingPopup").modal('hide');
                    if (parseInt(data.isSuccess) === 1) {
                        $("#successPopup").modal('show');
                        setTimeout(function() {
                            $('#is_submit').val(1);
                            document.paymentMethod.action = '<?php echo Yii::app()->baseUrl; ?>/admin/managecontent';
                            document.paymentMethod.submit();return false;
                        }, 5000);
                   } else {
                        $('#nextbtn').html('Reactivate');
                        $('#nextbtn').removeAttr('disabled');
                        $('#is_submit').val('');
                        if ($.trim(data.Message)) {
                            $('#card-info-error').show().html(data.Message);
                        } else {
                            $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                        }
                   }
                }, 'json');
            }
        }
    }
    
    function getAppilication(obj) {
        var parent = $(obj).val();
        var code = $.trim($('option:selected', obj).attr('data-code'));
        
        //if (code === 'muvi_studio_enterprise') {
        //    $('option:selected', obj).attr('data-price', 0);
        //}
        
        var base_price = parseFloat($('option:selected', obj).attr('data-price'));
        var is_bandwidth = $('option:selected', obj).attr('data-is_bandwidth');
        
        if (parseInt(is_bandwidth)) {
            $(".bandwidth").show();
        } else {
            $(".bandwidth").hide();
        }
        
        var applications = jQuery.parseJSON('<?php echo $applications;?>');
        var str = '';
        
        if (applications[parent]) {
            str = str +'<div class="pull-left">';
            str = str +'<h3>Select Applications</h3>';
            str = str +'</div>';
            str = str +'<div class="clearfix"></div>';
            
            for (var i in applications[parent]) {
                str = str +'<div class="pull-left col-xs-1" style="width: 20%;">';
                str = str +'<label>';
                str = str +'<div class="pull-left" style="margin-right: 5px;margin-top: -2px;">';
                var checked = '';
                if(applications[parent][i].code === "monthly_charge") {
                    checked = 'checked="checked"';
                }
                str = str +'<input type="checkbox" class="price_chkbox" data-id="'+applications[parent][i].id+'" data-code="'+applications[parent][i].code+'" data-price="'+applications[parent][i].price+'" '+checked+' onclick="selectApplication();" />';
                str = str +'</div>';
                str = str +applications[parent][i].name;
                str = str +'</label>';
                str = str +'</div>';
            }

            var APP_PRICE = 0;
            $.ajax({
              url: "<?php echo Yii::app()->baseUrl; ?>/site/packageappprice",
              method: "POST",
              data: { 'code': code },
              dataType: "json",
              async: false
            }).done(function( res ) {                                
                APP_PRICE = res.app_price;     
                $('#price_per_app').val(APP_PRICE);
            });
            str = str +'<div class="col-lg-9 pull-left" style="color: #929292;">';
            str = str +'First application is charged at $'+base_price+' per month, others are $'+APP_PRICE+' per month per app';
            str = str +'</div>';
        }
        
        $("#cloud_hosting").val(0);
        if (code === 'muvi_studio_server') {
            $("#cloud_hosting-div").show();
        } else {
            $("#cloud_hosting-div").hide();
        }
        
        $("#custom_code").val('');
        $("#custom_code-div").hide();
        $("#nextbutn").attr("onclick","showCards()");
        $("#appilication_div").html(str);
        showPrice();
    }
    
    function selectApplication() {
        var check = $("input:checkbox.price_chkbox:checked").length;
        if (parseInt(check) === 0) {
            $("input:checkbox.price_chkbox[data-code='monthly_charge']").prop('checked', true);
        }
        
        showPrice();
    }
    
    function cloudHosting(obj) {
        if ($(obj).is(":checked")) {
            $("#cloud_hosting").val(1);
        } else {
            $("#cloud_hosting").val(0);
        }
    }
    
    function showPlan(obj) {
        var plan = $(obj).val();
        
        if (plan === 'Year') {
            $(".monthly_payment").hide();
            $(".yearly_payment").show();
        } else {
            $(".yearly_payment").hide();
            $(".monthly_payment").show();
        }
        
        showPrice();
    }
    
    function showPrice() {
        var base_price = parseFloat($('option:selected', '#packages').attr('data-price'));
        var code = parseFloat($('option:selected', '#packages').attr('data-code'));
        var plan = $('.all_plans:checked').val();
        var price_total = base_price;
        var priceids = new Array();
        
        if ($('.price_chkbox').length) {
            var cnt = 0;
            
            $('.price_chkbox').each(function() {
                if($(this).is(":checked")){
                    var priceid = $(this).attr('data-id');
                    priceids.push(priceid);
                    cnt++;
                }
            });
            
            var APP_PRICE = $('#price_per_app').val();           
            if (cnt > 1) {               
                price_total = price_total + ((cnt -1) * APP_PRICE);
            }
        }
        
        if (plan === 'Year') {
            var yearly_discount = parseInt($('option:selected', '#packages').attr('data-yearly_discount'));
            price_total = ((price_total * 12) - (((price_total * 12) * yearly_discount)/100));
        }
        
        $("#pricingtext").val(priceids.toString());
        pricingall = $("#pricingtext").val();
        
        $('#packagetext').val($('option:selected', '#packages').val());
        packagesall = $('#packagetext').val();
        
        $("#plantext").val(plan);
        planall = plan;
        
        price_total = price_total.toFixed(2);
    
        $(".charge_now").html(price_total);
        $(".new_charge").html(price_total);
    }
</script>