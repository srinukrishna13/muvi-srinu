/**
 * Follow's YouTube's `t` URL parameter behavior.
 * Ex: t=16m32s means seek to 16 minutes and 32 seconds
 * modified-19-05-2017
 * modifier - @avi
 */
(function() {
videojs.plugin('urlparams', function(tempvalue,bitval) {
    
    if(bitval==0)
        tempvalue = tempvalue;
    else
        tempvalue = '?t='+parseInt(Math.round(tempvalue))+'s';
    //alert("seeking timevalue :: " + tempvalue);
    // how close must two times be to be close enough?
    var delta = 2,
    closeEnough = function(timeA, timeB) {
      var diff = timeA - timeB;
      if (isNaN(diff)) {
        return false;
      }
      return Math.abs(diff) < delta;
    },
    player = this,
    url = tempvalue,
	//url = '?t=10s',
    m = url.match(/[#&\?]t=((?:\d+h)?(?:\d+m)?(?:\d+s)?)/),
    seconds;
    //alert("M ::::::::::::::::::: " + m)
  if (m) {
    seconds = toSeconds(m[1]);
    if (seconds !== -1) {
      (function(){
        var
          
          // how long to wait before retries
          delay = 150|| 250,
          // timer for the next retry
          timeout,
          // whether we've been successful in setting the currentTime
          success = false,
          // attempt to set the currentTime
          attempt = function(){
            if (success || closeEnough(player.currentTime(), seconds)) {
              success = true;
              deregister();
            } else {
              player.currentTime(seconds);
              timeout = setTimeout(attempt, delay);
            }
          },
          // remove the timer
          deregister = function() {
            clearTimeout(timeout);
            player.off('timeupdate', deregister);
            // one final attempt
            if (!success) {
              player.currentTime(seconds);
            }
          };
        // make the first attempt
        player.on('timeupdate', deregister);
        player.ready(attempt);
        
      })();
    }
  }
  // is there an autoplay param?
  if ((/[#&\?]autoplay(?:$|[#&\?=])/i).test(url)) {
    // try to auto-play
    player.ready(function(){
      player.play();
    });
  }
});

function toSeconds (string) {
  var seconds = 0, m;
  string = string.replace(/^\s+|\s+$/g, '');
  if (!string || !(/^(\d+h)?(\d+m)?(\d+s)?$/).test(string)) {
	return -1;
  }
  m = string.match(/^(\d+)h/);
  if (m) {
	seconds += parseFloat(m[1]) * 3600;
	string = string.replace(/^(\d+)h/, '');
  }
  m = string.match(/^(\d+)m/);
  if (m) {
	seconds += parseFloat(m[1]) * 60;
	string = string.replace(/^(\d+)m/, '');
  }
  m = string.match(/^(\d+)s/);
  if (m) {
	seconds += parseFloat(m[1]);
  }
  return seconds;
}	
})();