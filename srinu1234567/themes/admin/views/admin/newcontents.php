<style>
    .loading_div{display: none;}      
    .jcrop-keymgr{display:none !important;}    
    .addmore-content{display:none;}
    .fixedWidth--Preview img {max-width: none !important;}
    @-moz-document url-prefix() { 
        .castcrew {
           margin-top: -20px;
        }
    }
    .actioncolor{
        color: #F1F1F1;
    }
    .actioncolor:hover{color: #EE0000}
 
</style>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo ASSETS_URL; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" /> -->
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/flatpickr.min.css">
<div id="success_msg"></div>
<input type="hidden" name="is_multi_content" id="is_multi_content" value="<?= @$is_multi_content;?>" />
<input type="hidden" name="is_multi_content_audio" id="is_multi_content_audio" value="<?= @$is_multi_content_audio; ?>" />
<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
$is_episode = @$is_episode;

if($is_episode){
    $addEpisode = 1;
}
if (@$data[0]['id'] && @$data[0]['is_episode']) {
    $is_episode = 1;
    $submitUrl = $this->createUrl('admin/addEpisode');
} elseif (@$data[0]['id'] && ((@$data[0]['content_types_id'] == 4) || (@$data[0]['content_types_id'] == 8))) {
    $submitUrl = $this->createUrl('admin/updateChannel');
} else {
    $submitUrl = $this->createUrl('admin/updateMovieDetails');
}
if(!@$data[0]['id']){
	$content = Yii::app()->general->content_count($studio_id);        
	if(($content['content_count'] == 2) || ($parent_content_type==2)){
		$submitUrl = $this->createUrl('admin/addChannel');
	}else if(($content['content_count'] == 8) || ($parent_content_type==4)){
		$submitUrl = $this->createUrl('admin/addChannel');
	}
}
?>

<?php if ($allow_new < 1) { ?>
    <form class="form-horizontal">
        <div class="overlay" style="background: #FFF;height: 100%;opacity: 0.3;position: absolute;width: 100%;z-index: 7;"></div>
    <?php } else if ((@$data[0]['id']) || (@$content['content_count'] == 2) || ($parent_content_type==2)) { ?>
        <form role="form" action="<?php echo $submitUrl; ?>" method="post" enctype="multipart/form-data" name="movie" id='contentForm' data-toggle="validator" class="form-horizontal" >
        <?php } else if ((@$data[0]['id']) || (@$content['content_count'] == 8) || ($parent_content_type==4)) { ?>
        <form role="form" action="<?php echo $submitUrl; ?>" method="post" enctype="multipart/form-data" name="movie" id='contentForm' data-toggle="validator" class="form-horizontal" >
        <?php } else { ?>	
            <form role="form" action="javascript:void(0);" method="post" enctype="multipart/form-data" name="movie" class="form-horizontal" id='contentForm' data-toggle="validator" onsubmit="return <?php if ($addEpisode) { ?>submitEpisodeForm();<?php } else if($parent_content_type==5) { ?>submitPhysicalForm();<?php } else { ?>submitContentForm();<?php } ?>">
            <?php } ?>		
            <div class="row m-t-40">
                <div class="col-md-4 col-sm-12 pull-right--desktopOnly">
                    <div class="Block">
                        <div class="Block-Header">
                            <?php
                            $disable="";
                            if (isset($data) && $data[0]['id']) {
                                $disable="disabled";
                                if(($data[0]['parent_id']==0) && ($data[0]['language_id'] == $this->language_id)){
                                   $disable=""; 
                                }
                                $movie_stream_id = $_REQUEST['movie_stream_id'];
                                if ($is_episode) {
                                    $posterImg = $this->getPoster($data[0]['movie_stream_id'], 'moviestream');
                                    $obj_type = 1;
                                } else {
                                    $posterImg = $this->getPoster($data[0]['id'], 'films');
                                    $obj_type = 0;
                                }
                                @$is_topbanner = 1;
                                @$topbanner = $this->getPoster($data[0]['id'], 'topbanner');
                            } else {
                                if ($is_episode || (@$content['content_count'] & 2) || ($parent_content_type==2)) {
                                    $posterImg = POSTER_URL . '/no-image-h.png';
                                } else {
                                    $posterImg = POSTER_URL . '/no-image-a.png';
                                }
                            }
                            $dmn = $customData['formData']['poster_size'];
                            if($dmn){
                                $expl = explode('x', strtolower($dmn));
                                $cropDimesion = array('width' => @$expl[0], 'height' => @$expl[1]);
                            }else if ($is_episode) {
                                $cropDimesion = Yii::app()->common->getCropDimension('episode');
                            } else if(@$content && ($content['content_count'] == 2) || ($parent_content_type==2)){
								$cropDimesion = Yii::app()->common->getCropDimension(4);
                            }else if($parent_content_type==5){
                                $cropDimesion = Yii::app()->common->getPgDimension();  
							}else{
                                $cropDimesion = Yii::app()->common->getCropDimension(@$data[0]['content_types_id']);
                            }
                            $defaultbanner = '/img/Np-Image-Top-Banner.png';
                            if (!@$topbanner) {
                                $topbanner = $defaultbanner;
                                $is_topbanner = 0;
                            }
                            ?>                           

                            <?php if (@$data[0]['content_types_id'] != 2 && !$is_episode) { ?>
                                <input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth1" id="reqwidth1" />
                                <input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight1" id="reqheight1" />   
                            <?php } ?>
                            <input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth2" id="reqwidth2" />
                            <input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight2" id="reqheight2" />   
                            <div class="icon-OuterArea--rectangular">
                                <em class="icon-cloud-upload icon left-icon "></em>
                            </div>
                            <h4>Upload Poster</h4>
                        </div>
                        <hr>

                        <div class="border-dotted m-b-40">
                            <div class="text-center">
                                <input type="button" class="btn btn-default-with-bg btn-file" data-toggle="modal" data-target=".bs-example-modal-lg" value="Browse" <?php echo $disable; ?>>

                                <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'X' . $cropDimesion['height']; ?></span></h5>
                            </div>
                            <div class="text-center">
                                <div class="m-b-10 displayInline fixedWidth--Preview">
                                    <?php
                                    $no_image_array = '/img/No-Image-Vertical.png';
                                    if (!in_array($posterImg, $no_image_array)) {
                                        ?>
                                        <div class="poster-cls  avatar-view jcrop-thumb">
                                            <div id="avatar_preview_div">                                                
                                                <?php if ((strpos($posterImg, 'No-Image') > -1) || (strpos($posterImg, 'no-image') > -1)) { ?>
                                                    <img data-src="holder.js/<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" alt="<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" style="max-width: 100% !important;"/>
                                                <?php } else { ?>
                                                    <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $posterImg; ?>" style="max-width: 100% !important;">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?= $this->renderPartial('//layouts/image_gallery', array('cropDimesion' => $cropDimesion, 'all_images' => $all_images, 'celeb' => $celeb, 'base_cloud_url' => $base_cloud_url,'checkImageKey'=>$checkImageKey)); ?>

                                    <?php } else { ?>
                                        <div class="poster-cls  avatar-view jcrop-thumb">
                                            <div id="avatar_preview_div">
                                                <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $posterImg; ?>" style="max-width: 100% !important;">
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <canvas id="previewcanvas" style="overflow:hidden;display: none;margin-left: 10px;"></canvas>
                                    <?php if(@$data[0]['id'] && !(strpos($posterImg, 'No-Image') > -1) ){
                                                                    if($disable == ""){
                                                                    ?>
                                <div class="caption">
                                    <a id="remove-poster-text" class="btn remove_btn" href="javascript:void(0);" onclick="return removeposter(<?php echo $data[0]['id']; ?>, <?php echo $obj_type; ?>, <?php echo $movie_stream_id; ?>);" ><em class="icon-close small-icon"></em>&nbsp;&nbsp;Remove Poster</a>
                                </div>
								<?php } }?>
                            </div>
                            <?php                                
                                $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
                                if($checkfireappletv){
                                    if (isset($data) && $data[0]['id']) {
                                        if ($is_episode) {
                                            $TvAppPosterImg = $this->getPoster($data[0]['movie_stream_id'], 'tvapp');
                                        } else {
                                            $TvAppPosterImg = $this->getPoster($data[0]['id'], 'tvapp');
                                        }
                                        $edit_id = $data[0]['id'];
                                    }else{
                                        $TvAppPosterImg = POSTER_URL . '/no-image-h.png';
                                        $edit_id = 0;
                                    }
                                    $firetvimagesize = array('width'=>800,'height'=>600) ;
                                    $this->renderPartial('firetvimage', array('checkfireappletv' => $checkfireappletv,'cropDimesion' => $firetvimagesize,'all_images' => $all_images,'base_cloud_url' => $base_cloud_url,'TvAppPosterImg'=>$TvAppPosterImg,'edit_id'=>$edit_id,'obj_type'=>$obj_type,'movie_stream_id'=>$movie_stream_id,'disable'=>$disable,'parent_content_type_id'=>$content_types_id['parent_content_type_id']));
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="Block">
                        <div class="Block-Header">
                            <div class="icon-OuterArea--rectangular">
                                <em class="icon-info icon left-icon "></em>
                            </div>
                            <h4>Basic Information</h4>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="contentType" class="col-md-4 control-label">Content Forms:</label>
                            <div class="col-md-8">
                            <?php if (@$data) { ?>
                                <input type="hidden" value="<?= @$data[0]['content_types_id']; ?>" name="movie[content_types_id]"/>
                                    <label for="contentType" class="col-md-6 control-label p-l-0">
                                <?php if($data[0]['metadata_form_id']){
                                        $list = CHtml::listData($formdata,'id', 'name');
                                        $data[0]['parent_content_type_id'] = $data[0]['parent_content_type'];
                                        echo Yii::app()->general->getFormName($data[0],$studio_id,$formdata,$list)." Form";
                                    }else{                                       
                                        if ($data[0]['parent_content_type'] == 2){
                                            echo "Video Live Streaming";
                                        }else if (@$data[0]['parent_content_type'] == 1) {
                                            if ($data[0]['content_types_id'] == 1) {
                                                echo 'Single Part Long Form';
                                            } elseif ($data[0]['content_types_id'] == 2) {
                                                echo "Single Part Short Form";
                                            } elseif ($data[0]['content_types_id'] == 3) {
                                                echo "Multi Part (<small style=\"font-weight: normal;\">";
                                                if (@$is_episode) {
                                                    echo 'Child';
                                                } else {
                                                    echo 'Parent';
                                                } echo ')</small>';
                                            }
                                        }else if (@$data[0]['parent_content_type'] == 3) {
                                            if ($data[0]['content_types_id'] == 5) {
                                                echo "Audio Single Part";
                                            } else if ($data[0]['content_types_id'] == 6) {
                                                echo "Audio Multi Part (<small style=\"font-weight: normal;\">";
                                                if (@$is_episode) {
                                                    echo 'Child';
                                                } else {
                                                    echo 'Parent';
                                                } echo ')</small>';
                                            }
                                        }
                                    }
                                            ?>
                                        </label>
                            <?php } else { ?>
											<div class="fg-line">
												<div class="select">
                                                <select class="form-control input-sm" onchange="getCustomForm(this)" name="movie[custom_metadata_form_id]" id="parent_content_type">
                                                    <?php foreach ($formdata as $key => $value) {
                                                        if ($addEpisode) {
                                                            if($content_types_id==6){if($value['parent_content_type_id']==3 && $value['is_child']==1){$selected="selected";}else{$selected="";}}
                                                            else{if($value['parent_content_type_id']==1 && $value['is_child']==1){$selected="selected";}else{$selected="";}}
																	}
															?>
                                                    <option value="<?= $key;?>" <?=$selected;?> data-value="<?= $value['metadata_form_type_id'];?>"><?= $value['name'].' Form';?></option>
                                    <?php } ?>                                                                        
													</select>
												</div>
											</div>
                                                        <?php }?>
                                            </div>
                                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <div class="loading_div">
                                    <div class="preloader pls-blue preloader-sm">
                                        <svg viewBox="25 25 50 50" class="pl-circular">
                                        <circle r="20" cy="50" cx="50" class="plc-path"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="formContents" style='border:0px solid red;'> <!-- formContent-->
                            <?php
							$issubCat_enabled = Yii::app()->custom->getCatImgOption($studio_id,'subcategory_enabled');
                            if ($is_episode) {
                                if (@$content_id) {
                                    $editflag = 0;
                                    $contentDetails[0]['id'] = $contentId;
                                } else {
                                    $contentTypeId = @$data[0]['content_type_id'];
                                    if(@$data[0]['id'])
                                    $editflag = 1;
                                    $contentDetails = @$data;
                                }
                                if (($data[0]['content_types_id'] == 6) || ($content_types_id == 6)) {
                                    $shows = Yii::app()->common->getShows(6, $studio_id);
                                    $this->renderPartial('audio_episode', array("movieDetails" => $contentDetails, 'shows' => $shows, 'editflag' => $editflag,'content_id'=>@$content_id,'langcontent'=>$langcontent,'customData'=>$customData,'issubCat_enabled'=>$issubCat_enabled,'IsDownloadable' => @$IsDownloadable));
                                } else {
									$shows = Yii::app()->common->getShows(3,$studio_id);
                                    $this->renderPartial('episode', array("movieDetails" => $contentDetails, 'shows' => $shows, 'editflag' => $editflag,'content_id'=>@$content_id,'langcontent'=>$langcontent,'customData'=>$customData,'issubCat_enabled'=>$issubCat_enabled,'IsDownloadable' => @$IsDownloadable));
                                }
                            } else if (($parent_content_type==2) || ($data[0]['content_types_id'] == 4) || (isset($content) && $content['content_count'] == 2)) {
                                $this->renderPartial('channel_form', array("data" => $data[0], 'contentCategories' => @$contentList,'langcontent'=>$langcontent,'customData'=>$customData));
                            } else if (($data[0]['content_types_id'] == 5) || ($data[0]['content_types_id'] == 6) || (isset($content) && $content['content_count'] == 4)) {
                                $this->renderPartial('audio_content', array("data" => $data, 'contentCategories' => @$contentList, 'content_types_id' => $data[0]['content_types_id'],'customData'=>$customData,'langcontent'=>$langcontent,'issubCat_enabled'=>$issubCat_enabled,'IsDownloadable' => @$IsDownloadable));
                            } else if ($content_types_id['parent_content_type_id']==5){//Physical Content
                                $this->renderPartial('physical_content', array("data" => $data, 'contentCategories' => @$contentList, 'content_types_id' => $data[0]['content_types_id'],'customData'=>$customData,'langcontent'=>$langcontent,'pg'=>$pg,'issubCat_enabled'=>$issubCat_enabled,'productize'=>$productize,'studio_id'=>$studio_id));
                            } else if (($parent_content_type==4) || ($data[0]['content_types_id'] == 8) || (isset($content) && $content['content_count'] == 8)) {//Audio Live Streaming
                                $this->renderPartial('audio_channel_form', array("data" => $data[0], 'contentCategories' => @$contentList,'langcontent'=>$langcontent,'customData'=>$customData));							
							}else {
                                $this->renderPartial('basic_content', array("data" => $data, 'contentCategories' => @$contentList, 'content_types_id' => $data[0]['content_types_id'],'customData'=>$customData,'langcontent'=>$langcontent,'issubCat_enabled'=>$issubCat_enabled,'IsDownloadable' => @$IsDownloadable));
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" id="x1" name="jcrop[x1]" />
            <input type="hidden" id="y1" name="jcrop[y1]" />
            <input type="hidden" id="x2" name="jcrop[x2]" />
            <input type="hidden" id="y2" name="jcrop[y2]" />
            <input type="hidden" id="w" name="jcrop[w]">
            <input type="hidden" id="h" name="jcrop[h]">

            <input type="hidden" value="<?php echo @$data[0]['id']; ?>" name="movie_id" id="movie_id"  />
            <input type="hidden" value="<?php echo @$data[0]['movie_stream_id']; ?>" name="movie_stream_id" id="movie_stream_id"  />
            <input type="hidden" value="<?php echo @$data[0]['uniq_id']; ?>" name="uniq_id" id="uniq_id"  />
            <input type="hidden" value="<?php echo @$data[0]['content_types_id']; ?>" name="content_types_id" id="content_types_id"  />
        </form>	
        <?php if ($content_types_id['parent_content_type_id']==5){?>
        <div style="display: none;" id="defaultcurrencydiv">
            <?php 
                if(count($pg['currency_code']) > 1){
                    $pgcurrenyname = "pg[multi_currency_id][]";
                    $pgsalepricename = "pg[multi_sale_price][]";
                }else{
                    $pgcurrenyname = "pg[currency_id]";
                    $pgsalepricename = "pg[sale_price]";                                                    
                }
                echo $this->renderPartial('//store/addmorecurrency', array('currency_code'=>$pg['currency_code'],'pgcurrenyname'=>$pgcurrenyname,'pgsalepricename'=>$pgsalepricename), true);
            ?>
        </div> 
        <?php }?>    
        <input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth" id="reqwidth" />
        <input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight" id="reqheight" />   
        <div style="clear: both;"></div>
        <?php if (@$data[0]['content_types_id'] != 2 && !$is_episode) { ?>
            <div id="callout-navs-tabs-plugin" class="row m-b-40 bs-callout bs-callout-info <?php if (!@$data[0]['id']) { ?>addmore-content<?php } ?>" >
                <div class="col-xs-12">
                    <div class="">
                        <hr>
                        <div class="form-horizontal">
                            <?php if(!in_array(@$data[0]['content_types_id'], array('4', '5','6'))){?>
                                <div class="row hideforaudio">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label" id="trailerLable">
                                            Preview :
                                            <?php 
                                            $trailerConversion = $this->getTrailerIsConverted(@$data[0]['id']);
                                               if ($trailerConversion == 2) { ?>
                                                   <span id="videoCorrupt"><img src="<?php echo Yii::app()->baseUrl; ?>/images/exclamation_point.png" alt="Video is could not encoded" class="vAlignTop"  data-toggle="tooltip" title="Encoding failed. Please check the video file and re-upload."/></span>
                                               <?php } ?>
                                        </label>
                                        <div class="col-sm-8" id="trailerbtn">
                                            <?php 
                                            $trailer = $this->getTrailer(@$data[0]['id']);
                                            if ($trailer) {
                                             $trailerIsConverted = $this->getTrailerIsConverted(@$data[0]['id']);   
                                               if ($trailerIsConverted == 1){
                                                   $info = pathinfo($trailer);
                                                   $pos = stripos($trailer, 'iframe'); 
                                                   $pos1 = stripos($trailer, 'youtube.com'); 
                                                   $pos2 = stripos($trailer, 'vimeo.com'); 
                                                   $pos3 = stripos($trailer, 'dailymotion.com');
                                                   if($info["extension"] =='m3u8'){
                                                       $this->renderPartial('player_third_party',array('third_party_url'=> $trailer));
                                                   }else if(($pos > 0) && ($pos1 > 0 || $pos2 > 0 || $pos3 > 0)){ ?>
                                            <div style="width: 250px; height: 120px;">
                                                <?php echo $trailer; ?>
                                                <style type="text/css">
                                                    iframe{
                                                      width: 250px !important;
                                                      height: 120px !important;  
                                                    }
                                                </style>
                                            </div>
                                            <?php  } else { ?>
                                            <video width="250" height="120" controls style="background: #333;" id="trailerplayer"> 
                                            <source type="video/mp4"  src="<?php echo $trailer; ?>">
                                             Your browser does not support the video tag.
                                            </video>
                                            <?php } ?>
                                            <?php if($disable == ""){ ?>
                                            <div class="fg-line m-b-10">
                                               <a id="remove-trailer-text" class="btn btn-danger btn-file btn-sm" href="javascript:void(0);" onclick="return removeTrailer(<?php echo $data[0]['id']; ?>);"><i class="remove"></i> Remove </a>
                                            </div>  
                                               <?php } } else { ?>
                                            <div class="progress-encoding">
                                              <a href="#" class="f-500">
                                                <em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress
                                              </a>  
                                            </div>    
                                            <?php   } } else {
                                                if($disable == ""){?>
                                            <div class="fg-line m-b-10">
                                                <a data-target="addvideo_popup" data-toggle="modal" class="btn btn-default-with-bg btn-sm" onclick="openUploadpopup('addvideo_popup');" class="btn btn-primary" id="trailer_btn"><i class="icon-upload"></i> Upload Preview </a>
                                            </div>  
                                                <?php }  } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <hr class="hideforaudio">
							<?php } ?>
                            <div class="row bs-callout bs-callout-info <?php if (!@$data[0]['id']) { ?>addmore-content<?php } ?>" id="callout-navs-tabs-plugin">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Top Banner:</label>
                                        <div class="col-sm-8">
                                            <div class="fixedWidth--Preview" id="topbanner-preview-img" >
                                                <img src="<?php echo $topbanner; ?>" rel="tooltip" title="<?php echo @$data[0]['name'] ? @$data[0]['name'] : 'Upload your poster'; ?>" style="max-width: 100% !important;"  >
                                            </div>
                                            <div class="fg-line m-t-20">
                                                <?php if($disable == ""){ ?>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-file btn-sm" onclick="removeTopBanner()"; id="remove-top-banner-text" style="display:<?php if (!$is_topbanner) echo 'none'; ?>">
                                                    Remove Banner 
                                                </a>
                                                <a href="#" class="btn btn-default-with-bg btn-file btn-sm" onclick="globalPopup('topbanner');" id='add_change-top-banner-text'>
                                                    <?php
                                                    if ($is_topbanner) {
                                                        echo " Change Banner ";
                                                    } else {
                                                        echo " Upload Banner ";
                                                    }
                                                    ?>
                                                </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="modal fade" id="myModal_topbnr" role="dialog" style="overflow-y:hidden !important;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size:22px;" >Delete Top Banner?</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <span>Are you sure you want to <b>remove the top banner</b> from Studio?</span> 
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="javascript:void(0);" data-dismiss="modal" id="sub-btn" class="btn btn-default content_id">Yes</a>

                                                <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php //if (@$data[0]['content_types_id'] != 4) { ?>
            <div class="row m-b-40">
                <div class="col-xs-12">
                    <div class="Block <?php if (!@$data[0]['id']) { ?>addmore-content<?php } ?>">
                        <div class="Block-Header">
                            <div class="icon-OuterArea--rectangular">
                                <em class="icon-people icon left-icon "></em>
                            </div>
                            <h4>Cast and Crew</h4>
                        </div>
                        <hr>
                        <?php if($disable == ""){ ?>
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <a  class="btn btn-primary btn-file btn-sm" role="button" data-toggle="collapse" href="#Cast-Crew-Collapse-Block" aria-expanded="false" aria-controls="Cast-Crew-Collapse-Block">
                                    Add New Cast
                                </a>
                            </div>
                        </div>
                         <?php } ?>
                        <div class="row p-t-20 collapse" id="Cast-Crew-Collapse-Block">
                            <div class="col-xs-12">
                                <form  class="row" role="form" name="castncrewform" id="castncrewform" >
                                    <div class="col-sm-4">
                                        <label >Cast/Crew</label>
                                        <div class="input-group form-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-user"></i></span>
                                            <div id="showcastcrew">
                                            <div class="fg-line">
                                                <input type="text" class="form-control" id="castname" name="cast_name" placeholder="Enter Cast/Crew Name">
                                                <input type="hidden" name="cast_id" value="" id="cast_id"/>
                                            </div>
                                        </div>

                                    </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label >Type</label>
                                        <div class="input-group form-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-user"></i></span>
                                            <div class="fg-line" id="showtype">
                                                <div class="btn-group btn-block" id="ajaxshowtype">
                                                </div>
                                            </div>
                                            <div id="showtypeadd" style="display:none">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="fg-line">
                                                            <input type="text" class="form-control" id="typenew" name="typenew" placeholder="Enter Type" value="" width="10px">
                                                            <input type="hidden" id="typenew_id" name="typenew_id" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3" style="margin-top:16px">
                                                        <em class="icon-check" style="cursor:pointer" title="Save" onclick="saveType()"></em>&nbsp;&nbsp;&nbsp;<em class="icon-close" style="cursor:pointer" title="Cancel" onclick="removeshowType();"></em>
                                                    </div>
                                                </div>
                                                
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>&nbsp;</label>
                                        <div class="fg-line">
                                            <button type="button" id="add_btn" class="btn btn-primary btn-sm m-t-5 " onclick="addCast();">Add</button>
                                            <button type="button" class="btn btn-default btn-sm m-t-5 " data-toggle="collapse" onclick="addCastCrewpopup();"  href="#Cast-Crew-Collapse-Block" >Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>        
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="width">Name</th>
                                            <th >Image</th>
                                            <th data-hide="phone">Cast Type</th>
                                            <th data-hide="phone" class="width">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="castcrew_body">
                                        <?php
                                        if (isset($movie_casts) && !empty($movie_casts)) {
                                            foreach ($movie_casts AS $key => $val) {
                                                $img = $this->getPoster($val['celebrity_id'], 'celebrity', 'thumb');
                                                if (getimagesize($img) == false) {
                                                    $img = str_replace('/thumb/', '/medium/', $img);
                                                }
                                                ?>
                                                <tr>
                                                    <td width="40%"><?php echo $val['name']; ?></td>
                                                    <td>
                                                        <div class="Box-Container m-b-10"> 
                                                            <div class="thumbnail thumbnail-small">
                                                                <img src="<?php echo $img; ?>" alt="<?php echo $val['name']; ?>">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td> <?php echo ucwords(implode(',', json_decode($val['cast_type']))); ?></td>
                                                    <td>  
                                                        <?php if($val['parent_id'] == 0 && $val['language_id'] == $this->language_id){ ?>
                                                        
                                                            <a href="javascript:void(0)" onclick="removeCast(<?php echo $val['celebrity_id']; ?>, this);">
                                                                <em class="icon-trash"></em>&nbsp;&nbsp;
                                                                Remove
                                                            </a>
                                                        
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>
<tr>
												<td colspan="4" id="nocast_crew"> No Cast/Crew Found</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="h-50"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php // } ?>
        <?php } ?>
        <!-- Change Poster Popup-->
        <?php
        $movie_id = @$data[0]['id'];
        $movieName = @$data[0]['name'];
        $pimg = $posterImg;
        if (strstr($posterImg, '/original/')) {
            $pimg = str_replace('/orignial/', '/thumb/', $posterImg);
        }
        $banner_image = $pimg;
        ?>
        <?php $banner_image = $banimg; ?>
        <!-- Video gallery Modal -->
        <?= $this->renderPartial('//layouts/video_gallery'); ?>              
        <!-- Modal End  -->

        <!-- Progress Bar popup -->
        <div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
            <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
                  <div style="float:left;font-weight:bold;">File Upload Status</div>
                  <div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
            </div>
            <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
        </div>
        <!-- Progress Bar popup end -->
        <input type="hidden" value="" name="last_selected_content_type" id="last_selected_content_type"  readonly />
        <!-- Top Banner Modal -->
        <?php include_once 'topbanner.php'; ?>

        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>

        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
        <script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
        <!-- <script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script> -->
        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/js/flatpickr.js"></script>
        <script type="text/javascript">
			var celebxhr;
			var STUDIO_ID = '<?= $studio_id; ?>';
			var HOR_POSTER_STUDIO = '<?= HOR_POSTER_STUDIO; ?>';
			var gmtDate = '<?= gmdate('d'); ?>';
			var gmtMonth = '<?= gmdate('m'); ?>';
			var gmtYear = '<?php echo gmdate('Y'); ?>';
			var jcrop_api;
			var bounds, boundx, boundy;
			var settout = "", s3upload = null, s3obj = new Array, bootboxConfirm = 0;
        </script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/addcontent_min.js?v=<?= RELEASE;?>"></script>
        <script type="text/javascript">
                    // Minified Javascript 	
					var defaultbanner = '<?= $defaultbanner;?>';
                <?php if(empty($customData)){?>
                    var genre = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/admin/getTags?taggable_type=1", filter: function (e) {
                                return $.map(e, function (e) {
                                    return{name: e}
                                })
                            }}});
                    genre.clearPrefetchCache(), genre.initialize(), $("#genre").tagsinput({typeaheadjs: {name: "genre", displayKey: "name", valueKey: "name", source: genre.ttAdapter()}});
                    var language = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/admin/getTags?taggable_type=3", filter: function (e) {
                                return $.map(e, function (e) {
                                    return{name: e}
                                })
                            }}});
                    language.clearPrefetchCache(), language.initialize(), $("#language").tagsinput({typeaheadjs: {name: "language", displayKey: "name", valueKey: "name", source: language.ttAdapter()}});
                    var censor_rating = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/admin/getTags?taggable_type=2", filter: function (e) {
                                return $.map(e, function (e) {
                                    return{name: e}
                                })
                            }}});
                    censor_rating.clearPrefetchCache(), censor_rating.initialize(), $("#censor_rating").tagsinput({typeaheadjs: {name: "censor_rating", displayKey: "name", valueKey: "name", source: censor_rating.ttAdapter()}});
                <?php }?>
                    $(".topbanner-thumb-img,.edit-poster").hover(function (e) {
                        $(".edit-poster").show()
                    }, function () {
                        $(".edit-poster").hide()
                    }), $("#content_publish_date").click(function () {
                        0 == this.checked ? $("#content_publish_date_div").hide() : $("#content_publish_date_div").show()
                    }), function () {
                        $("[data-mask]").inputmask(), $("#release_date").datepicker({changeMonth: !0, changeYear: !0}), $("#episode_date").datepicker({changeMonth: !0, changeYear: !0}), $("#publish_date").datepicker({changeMonth: !0, changeYear: !0, minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)}), $("#castname").autocomplete({source: function (e, t) {
                               var castname = $("#castname").val().replace(/^'/g, '');
                               castname = castname.replace(/^"/g, '');
								if(celebxhr && celebxhr.readyState != 4){
									celebxhr.abort();
								}
								celebxhr = $.ajax({url: HTTP_ROOT + "/admin/celebAutocomp?",data:{"term":castname}, dataType: "json", success: function (e) {
                                        t($.map(e.celeb, function (e) {
                                            return{label: e.celeb_name, value: e.celeb_id}
                                        }))
                                    }})
                            }, select: function (e, t) {
                                e.preventDefault(), $("#castname").val(t.item.label), $("#cast_id").val(t.item.value)
                            }, focus: function (e, t) {
                                $("#cast_name").val(t.item.label), $("#cast_id").val(t.item.value), e.preventDefault()
                            }}), $("#dprogress_bar").draggable({appendTo: "body", start: function (e, t) {
                                isDraggingMedia = !0
                            }, stop: function (e, t) {
                                isDraggingMedia = !1
                            }})
                    }();
                    var settout = "", s3upload = null, s3obj = new Array, bootboxConfirm = 0;
                    $.fn.bindFirst = function (e, t) {
                        this.on(e, t), this.each(function () {
                            var t = $._data(this, "events")[e.split(".")[0]], n = t.pop();
                            t.splice(0, 0, n)
                        })
                    }, $(function () {
                        $("html a:not(a.confirm,a.upload-video,a.embed-box)").bindFirst("click", function (e) {
                            if ($("#dprogress_bar").is(":visible")) {
                                e.stopPropagation(), e.preventDefault();
                                var t = "Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.", n = $(this).attr("href");
                                bootbox.hideAll(), bootbox.confirm({title: "Upload In progress", message: t, buttons: {confirm: {label: "Stop Upload", className: "cnfrm-btn cnfrm-succ btn-default pull-right"}, cancel: {label: "Ok", className: "cnfrm-btn cnfrm-cancel btn-default pull-right"}}, callback: function (e) {
                                        e && (bootboxConfirm = 1, window.location.replace(n))
                                    }})
                            }
                        })
                    }), window.addEventListener("beforeunload", function (e) {
                        if ($("#dprogress_bar").is(":visible") && !bootboxConfirm) {
                            var t = "Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.";
                            return(e || window.event).returnValue = t, t
                        }
                    });
                    <?php if(empty($content_child) || (isset($content_child) && ($content_child['content_count'] & 1)) || (isset($content_child) && ($content_child['content_count'] & 8))){}else{?>
                        getChildContentTypes();
                    <?php }?>
        </script>
<script type="text/javascript">
 function checkperma(val)
 {
     if(val != '')
     {
        var newperm = name_to_url(val);
        if (newperm=='')
        {
            $('#plink').html("<input type='text' name='movie[permalink]' id='permalink' class='form-control input-sm checkInput' required placeholder='Please enter english equivalent name' onblur='checkperma(this.value)'>&nbsp;<br />Please provide the english equivalent name which will be used for permalink purpose only.&nbsp;<em class='icon-info'></em>");               
        }         
    }
 }
 function name_to_url(name) {
    name = name.toLowerCase(); // lowercase
    name = name.replace(/^\s+|\s+$/g, ''); // remove leading and trailing whitespaces
    name = name.replace(/\s+/g, ''); // convert (continuous) whitespaces to one -
    name = name.replace(/[^a-z-]/g, ''); // remove everything that is not [a-z] or -
    return name;
}

function saveCastCrew(){ 
    var castname = $('#castnamenew').val();
    var studio_id = <?php echo $studio_id;?>;
    var url = HTTP_ROOT + "/Adminceleb/ajaxcastCrewSave";    
    $.post(url, {'castname': castname, 'studio_id': studio_id}, function (data) {
        if (data.isSuccess) {
            $('#showcastcrew').show();
            $('#showcastcrewnew').hide();
            $('#castname').val('');
            $('#castnamenew').val('');
        }
    }, 'json');    
}
function confirmDelete(id,msg) {
    swal({
        title: "Delete Type?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        
        var studio_id = <?php echo $studio_id;?>;
        var url = HTTP_ROOT + "/Adminceleb/ajaxcastCrewDelete";        
        $.post(url, {'id': id, 'studio_id': studio_id}, function (data) {
            var delid = '#typeliid'+data.id;            
            if (data.isSuccess) {
               ajaxshowType();
            }
        }, 'json'); 
    });
}
function showType(id,name){
    $('#showtype').hide();
    $('#showtypeadd').show();
    $('#typenew').val(name);
    $('#typenew_id').val(id);
}
function removeshowType(){
    $('#showtype').show();
    $('#showtypeadd').hide();
}
function saveType(){ 
    var typename = $('#typenew').val();
    var typename_id = $('#typenew_id').val();
    var studio_id = <?php echo $studio_id;?>;
    var url = HTTP_ROOT + "/Adminceleb/ajaxtypeSave";    
    $.post(url, {'typename': typename,'typename_id':typename_id, 'studio_id': studio_id}, function (data) {
        if (data.isSuccess) {
            $('#showtype').show();
            $('#showtypeadd').hide();
            $('#typenew').val('');
            ajaxshowType();
        }else{
            ajaxshowType();
        }
    }, 'json');    
}
$(document).ready(function(){
    ajaxshowType();
});
function ajaxshowType(){
    var url = HTTP_ROOT + "/Adminceleb/ajaxshowtype";
    $.post(url,function(postresult){    
    $("#ajaxshowtype").html(postresult);    
    });
}
 </script>
<script>
    $(document).ready(function(){
        $('#release_date').datepicker({minDate: '0'});
        $('#publish_date').datepicker({minDate: '0'}),

        $('#publish_time').flatpickr({
            enableTime: true,
            noCalendar: true,
            enableSeconds: false, // disabled by default
            time_24hr: true, // AM/PM time picker is used by default
            // default format
            dateFormat: "H:i",
            // initial values for time. don't use these to preload a date
            defaultHour: 12,
            defaultMinute: 0
        });
    })
</script>
<script type="text/javascript" src="/themes/admin/js/dropdown-enhancement.js"></script>
<link href="/themes/admin/css/dropdown-enhancement.css" rel="stylesheet" type="text/css" />
