<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">How Is Muvi Different</h2>
            <div class="row">
                <div class="span6" style="margin-left: 5px;">
            <div class="item">
                <p>Muvi is the only end-to-end solution available worldwide for launching your custom-branded VOD platform.</p>
                <br/>
                <p>Diagram on the right illustrates different components that go into building a VOD platform. Most other products offer only 1 or 2 of these components, you have to build the rest. </p>
                <p>For example, YouTube and Vimeo offer just a video player which is less than 10% of the total effort, you still have to build rest of the website, host and maintain it.</p>
                <br/>
                <p>Muvi offers the entire solution for a simple, low price. Nothing else required.  </p>       
            </div>
        </div>
        <div class="span6 pull-right txt-right"  style="margin-left: 5px;">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/HowIsSDKDifferent.png" alt="How Is Studio Different" title="How Is Studio Different" />
            </div>
        </div>
            </div>
        </div>
    </div>
</div>