<?php
class UserPaymentLog extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'user_payment_logs';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sdk_user'=>array(self::HAS_ONE, 'SdkUser','user_id'),
            'plan'=>array(self::HAS_ONE, 'SubscriptionPlans', 'plan_id'),
        );
    }
}
