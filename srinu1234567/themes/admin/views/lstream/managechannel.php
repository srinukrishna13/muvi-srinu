<style>
.loaderDiv{position: absolute;left: 30%;top:10%;display: none;}
.comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}
</style>
<?php if(isset(Yii::app()->user->created_at)){
	$expairy_date = strtotime(Yii::app()->user->created_at)+13*24*60*60;
}else{
	$expairy_date = time()+13*24*60*60;
}
$v = RELEASE;
?>
<?php if(isset(Yii::app()->user->status) && Yii::app()->user->status==3){?>
<div class="row m-t-40">
        <div class="col-md-12">
            <div class="Block">
                <div class="Block-Header">
                    <h3><?php if($expairy_date>=strtotime(date('Y-m-d'))){?>Your Trial Period expires on <?php echo date('F d, Y',$expairy_date); }else{?>Site Status<?php }?></h3>
					<div class="row">
						<div class="col-sm-12 pull-right">
							<button data-widget="collapse" class="btn btn-primary btn-xs"><i class="fa fa-minus"></i></button>
							<button data-widget="remove" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button>
						</div>
					</div>
                </div>
                <div class="Block-Body">
    <?php if ($expairy_date >= strtotime(date('Y-m-d'))) { ?>
                        <p>
                            Cancel anytime. Your card will not be charged if account is cancelled before <?php echo date('F d, Y', $expairy_date); ?>. Unless cancelled, Your card will be charged for the first month on <?php echo date('F d, Y', ($expairy_date + 24 * 60 * 60)); ?>
                        </p>
                        <div><strong>Status</strong></div>
    <?php } ?>
                    <table class="table status-tbl" border="1" >
                        <tbody>
                            <tr>
                                <td>Admin Panel</td>
                                <td>Add Content</td>
                                <td>Payment Gateway</td>
                                <td>Go Live</td>
                            </tr>
                            <tr>
                                <td class="greenBackground status-ready">Ready</td>
                                <td><a href="<?php echo $this->createUrl('admin/newContents'); ?>" >Add Content</a> </td>
                                <td>In Progress</td>
                                <td>In Progress</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
<?php } ?>
<div class="row m-b-40">
	<div class="col-sm-12">
        <a href="<?php echo $this->createUrl('lstream/addChannel');?>" class="btn btn-primary">Add Channel</a>
    </div> 
</div>	
<div class="filter-div">
	<div class="row">
		<div class="col-md-12 m-b-10">
			<form action="<?php echo $this->createUrl('lstream/manageChannel');?>" name="searchForm" id="search_content" method="get">
				<div class="row">
					<div class="col-sm-3">
						<div class="input-group form-group">
							<span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
							<div class="fg-line">
								<div class="select">
									<input type="text" id="update_date" name="searchForm[update_date]" value='<?php echo @$dateRange;?>' />
								</div>
							</div>
						</div>
					</div>
					<?php if(count($contentList)>=1){ ?>
					<div class="col-sm-3">
						<div class="form-group input-group">
							 <span class="input-group-addon"><i class="icon-folder"></i></span>
							<div class="fg-line">
								<div class="select">
									<select class="filter_dropdown form-control input-sm" name="searchForm[content_type_id]">
										<option value="">&nbsp;Content Type</option>
									<?php foreach($contentList AS $row=>$value){
											foreach($value AS $key => $cont){
												$selected ='';
												$selected_type ='';
												$content_type = explode('-',@$content_type_id);
												if(count($content_type)>1 && $content_type[1] == strtolower($cont)){
													$selected_type = 'selected="selected"';
												}else{
													if(@$content_type_id == $key){
														$selected = 'selected="selected"';
													}
												}
												echo '<option value="'.$key.'"'.$selected.' >&nbsp;'.$cont.'</option>';
											}}?>
									</select>		
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="col-sm-3">
						<div class="form-group  input-group">
							<span class="input-group-addon"><i class="icon-magnifier"></i></span>
							<div class="fg-line">
								<input type="text" class="filter_input form-control input-sm" name="searchForm[search_text]" value="<?php echo @$searchText;?>" placeholder="What are you searching for?" id="search_content" />
							</div>
					   </div>
					</div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-primary filter-btn" type="submit">Filter</button>
                                    </div>
				</div>
			</form>
		</div>
	</div>
</div>
<table class="table" id="movie_list_tbl">
	<thead>
		<tr>
			<th>Content</th>
			<th>Type</th>
			<th data-hide="phone">Stream</th>
			<th data-hide="phone">Updated</th>
			<th data-hide="phone" class="width">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$cnt = 1;
	if($data){
		$default_img = POSTER_URL.'/no-image-h.png'; 
		$cnt=(($pages->getCurrentPage())*20)+1;
		foreach($data AS $key=>$details){
			$movie_id = $details['id'];
			$stream_id = $details['stream_id'];
			if(isset($posters[$movie_id])){
                                $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
				$img_path = $postUrl.'/system/posters/'.$posters[$movie_id]['id'].'/episode/'.$posters[$movie_id]['poster_file_name'];
			}else{
				$img_path = $default_img;
			}
			$contentName = $details['name'];
			$plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink']:str_replace(' ', "-",strtolower($details['name']));
	?>
	<tr>
		<td>
			<a href="<?php echo 'http://' . Yii::app()->user->siteUrl . '/' . @$details['content_permalink'] . '/' . @$plink; ?>" target='_blank' >
				<div class="Box-Container  m-b-10">
                    <div class="thumbnail">
						<img src="<?php echo $img_path; ?>" alt="<?php echo $details['name']; ?>">
					</div>
				</div>	
				<div class="caption">   
					<?php echo $contentName; ?>
				</div>
			</a>
		</td>
		<td><?php echo $details['display_name']; ?></td>
		<td>NA</td>
		<td><?php echo $details['update_date'] ? date('jS M Y', strtotime($details['update_date'])) : 'NA'; ?></td>
		<td>
			<?php if($details['feed_url']){?>
			<h5 id="<?php echo $stream_id; ?>">
			<a href="<?php echo $this->createUrl('video/play_video', array('movie' => $movie_id, 'movie_stream_id' => $details['stream_id'], 'preview' => true)); ?>" target="_blank"><em class="icon-control-play"></em>&nbsp;&nbsp; Preview stream</a>
			</h5>
			<?php } ?>
			<h5><a href="javascript:void(0);"  onclick="openEmbedBox('embedbox_<?php echo $stream_id; ?>');" class="embed-box"><em class="fa fa-chain"></em>&nbsp;&nbsp; Embed</a></h5>
			<div style="display:none;" class="animate-div" id="embed_<?php echo $stream_id; ?>">Copied!</div>
			<h5 class="search-new" id='embedbox_<?php echo $stream_id; ?>' style="display:none;">
			<div class="form-group input-group">
			<input type="text" class="form-control" placeholder="Embed Code..." value='<iframe width="560" height="315" src="<?php echo Yii::app()->getBaseUrl(TRUE) . '/embed/' . $details['embed_id']; ?>" frameborder="0" allowfullscreen></iframe>'>
			<span class="input-group-btn">
			<button class="btn btn-default  copyToClipboard"  data-clipboard-text='<iframe width="560" height="315" src="<?php echo Yii::app()->getBaseUrl(TRUE) . '/embed/' . $details['embed_id']; ?>" frameborder="0" allowfullscreen></iframe>' type="button" onclick="CopytoClipeboard('embed_<?php echo $stream_id; ?>');">Copy!</button>
			</span>
			</div><!-- /input-group -->
			</h5>	
			<h5><a href="<?php echo $this->createUrl('lstream/editChannel/', array('movie_id' => $movie_id, 'uniq_id' => $details['uniq_id'], 'livestream_id' => $details['livestream_id'])); ?>" ><em class="icon-pencil" ></em>&nbsp;&nbsp; Edit Channel</a></h5>
			<h5><a href="<?php echo $this->createAbsoluteUrl('lstream/removeChannel', array('movie_id' => $movie_id, 'uniq_id' => $details['uniq_id'])); ?>" class="confirm" data-msg ="Deleting content deletes all meta data of the content <b>and the video as well.</b> Are you sure?"><span class="icon-trash" ></span> Remove Channel</a></h5>
		</td>
	</tr>
	<?php	$cnt++;	}
	}else{?>
		<tr>
			<td colspan="5">
				<?php if($contentType){?>
					<p class="text-red">Oops! You don't have any content under this category.</p>
		<?php	}else{?>
					<p class="text-red">Oops! You don't have any content in your Studio.</p>
		<?php	} ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="pull-right">
	<?php
	if ($data) {
		$opts = array('class' => 'pagination m-t-0');
		$this->widget('CLinkPager', array(
			'currentPage' => $pages->getCurrentPage(),
			'itemCount' => $item_count,
			'pageSize' => $page_size,
			'maxButtonCount' => 6,
			"htmlOptions" => $opts,
			'selectedPageCssClass' => 'active',
			'nextPageLabel' => '&raquo;',
			'prevPageLabel'=>'&laquo;',
			'lastPageLabel'=>'',
			'firstPageLabel'=>'',
			'header' => '',
		));
	}
	?>
</div>
<div class="h-40"></div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/bootbox.js"></script>	
<script type="text/javascript">
	var bootboxConfirm = 0;
	$(function () {
		$("a.confirm").bind('click',function(e) {
			e.preventDefault();
			var location = $(this).attr('href');
			var msg = $(this).attr('data-msg');
			if($('#dprogress_bar').is(":visible")){
				var msg = 'Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.';
				var location = $(this).attr('href');
				bootbox.hideAll();
			bootbox.confirm({
					title: "Upload In progress",
					message: msg,
					buttons: {
						'confirm': {
							label: 'Stop Upload',
							className: 'cnfrm-btn cnfrm-succ btn-default pull-right'
						},
						'cancel': {
							label: 'Ok',
							className: 'cnfrm-btn cnfrm-cancel btn-default pull-right'
						}
					},callback: function (confirmed) {
						if(confirmed) {
							bootboxConfirm =1;
							confirmDelete(location,msg);
						}
					}
				}); 	
			}else{
				confirmDelete(location,msg);
			}
		});
		$("#update_date").daterangepicker({
			initialText : 'Update Date'
		}); 
	});

	function confirmDelete(location,msg){
            
             swal({
                    title: "Delete Content?",
                    text: msg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true,
                    html:true
                }, function() {
                    //swal("Deleted!", "Your content has been deleted.", "success");
                    window.location.replace(location);
                });
		
	}
	
    function validateHHMMSS(data)
    {
        var re = /^\d{2}:\d{2}:\d{2}$/;
        var ret = data.match(re);
        if (ret == null) 
            return false;
        else
        {
            console.log(ret);
            var parts = ret[0].split(':');
            if(parts[1] < 0 && $parts[1] > 59 && parts[2] < 0 && $parts[2] > 59)
                return false
            else
                return true;
        }
    }
    
	function click_browse(){ 
		$("#videofile").click();
	}
	
	// Copy to Clip Borad code


clientTarget = new ZeroClipboard( $('.copyToClipboard'), {
	moviePath: "<?php echo Yii::app()->theme->baseUrl;?>/js/zeroclipboard/ZeroClipboard.swf",
	debug: false
} );
	
	

	
	function CopytoClipeboard(id){
		$('#'+id).css('display','block');
		$('#'+id).animate({ 
			opacity: 0,
			top: '-=75',
		  }, {
		easing: 'swing',
		duration: 500,
		complete: function(){
			$('#'+id).css({'display':'none','opacity':1,top:'+=75'});
		}  
		});
	}
	function openEmbedBox(embedid){
		if($('#'+embedid).is(':visible')){
			$('#'+embedid).fadeOut(1000)
		}else{
			$('#'+embedid).fadeIn(1000);
		}
	}
	
</script>