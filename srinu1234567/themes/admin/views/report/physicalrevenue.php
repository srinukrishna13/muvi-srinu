<input type="hidden" class="data-count" value="<?php echo $ppv_subscription_count;?>" />
<thead>
    <th>Item Name</th>
    <th data-hide="phone">Active?</th>
    <th data-hide="phone">Item Price</th>
    <th data-hide="phone">Number of Orders</th>
    <th>Revenue</th>
    
</thead>
<tbody class="list">
    <?php
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
        if(isset($ppv_subscription) && count($ppv_subscription)){
            foreach ($ppv_subscription as $key=>$value){                
               //$price=Yii::app()->common->getPGPrices($value['product_id'],$value['currency_id']); 
    ?>
    <tr>
        <td class="email_ppv"><?php echo ucfirst($value['name']) ?></td>
        <td class="content"><?php if($value['status']==1){echo "YES";}else{echo "NO";}?></td>
        <td class="amount"><?php echo $value['price']?Yii::app()->common->formatPrice($value['price'],$value['currency_id']):Yii::app()->common->formatPrice(0,$value['currency_id']);?></td> 
        <td class="date_time"><?php echo $value['nod'];?></td>
        <td class="revenue"><?php echo $value['revenue']?Yii::app()->common->formatPrice($value['revenue'],$value['currency_id']):Yii::app()->common->formatPrice(0,$value['currency_id']);?></td>
      
    </tr>
    <?php }}else{?>
    <tr>
        <td colspan="4">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>