<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
    define('HTTP', 'https://');
} else {
    define('HTTP', 'http://');
}
$secured_hosturl = HTTP . $_SERVER['HTTP_HOST'];
define('CDN_HTTP', 'https://');
$baseurl = HTTP . $_SERVER['SERVER_NAME'];
$hosturl = HTTP . $_SERVER['HTTP_HOST'];
defined('HOST_URL') ? '' : define('HOST_URL', $hosturl);

$baseurl = 'http://'.$_SERVER['SERVER_NAME'];
$hosturl = 'http://'.$_SERVER['HTTP_HOST'];
defined('HOST_URL')?'':define('HOST_URL', $hosturl);

define('SAMPLE_DATA_STUDIO', 54);
define('HOR_POSTER_STUDIO', 798);

define('CLASSIC_PREVIEW_STUDIO', 1181);
define('TRADITIONAL_PREVIEW_STUDIO', 1187);
define('MODERN_PREVIEW_STUDIO', 1188);
define('CAT_POSTER_SIZE', '400x300');

define('DUMMY_DATA_STUDIO', 1189);
define('ticket_username', 'support@muvi.com'); 
define('ticket_password', 'muvi@1234'); 
define('ticket_username2', 'testmuvi@gmail.com'); 
define('ticket_password2', 'testmuvi123');
define('ticket_hostname', '{imap.gmail.com:993/imap/ssl}INBOX'); 
define('DRM_LICENSE_KEY','5260,2825926886be45b68748a2ecf6e6d88e');
define('WV_URL','https://wv-gen.service.expressplay.com/hms/wv/token');
define('MS3_URL','https://ms3-gen.test.expressplay.com/hms/ms3/token');
define('PR_URL','https://pr-gen.service.expressplay.com/hms/pr/token');
define('FP_URL','https://fp-gen.service.expressplay.com/hms/fp/token');
define('nginxserverip','52.20.205.94');
define('nginxserverlivecloudfronturl','https://d2vo3ozpc06skj.cloudfront.net');
define('nginxserverrecordcloudfronturl','https://d392hcdnl497u1.cloudfront.net');
define('NGINX_IP_HTTP','https://livestream.muvi.com');

//Constant define for FirstData (Live and Demo)
define('IS_LIVE_FIRSTDATA', 1);
if (IS_LIVE_FIRSTDATA) {
    define('FIRSTDATA_API_LOGIN', 'C97062-02');
    define('FIRSTDATA_API_KEY', 'c858kd7n90k2m8g50js5xnv7g6up3p56');
} else {
    define('FIRSTDATA_API_LOGIN', 'AI1623-05');
    define('FIRSTDATA_API_KEY', '8yy4fiq8wm2l5z463r7y43n7d3043yu0');
}
//Constant define for Paypal (Live and Demo)
define('IS_LIVE_PAYPAL', 1);
if(IS_LIVE_PAYPAL){
    define('PAYPAL_API_LOGIN', 'info_api1.muvi.com');
    define('PAYPAL_API_KEY', '7LLDDVXYN8LBSMRG');
    define('PAYPAL_API_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AgH5nP3jcFIO-33kXE3GS9uF6YNS');
}else{
    define('PAYPAL_API_LOGIN', 'developer.sanjeev31-facilitator-1_api1.gmail.com');
    define('PAYPAL_API_KEY', 'EME68W76966NXS5K');
    define('PAYPAL_API_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AKCC9ZVKxmcbdZQCfGmUlnCte.0x');
}
//FFMPEG PATH
if (HOST_IP == '127.0.0.1') {
    define('FFMPEG_PATH', 'ffmpeg');
} else if (HOST_IP == '52.0.64.95') {
    define('FFMPEG_PATH', '/home/bitnami/bin/ffmpeg');
}else if (HOST_IP == '52.211.41.107') {
    define('FFMPEG_PATH', '/root/bin/ffmpeg');
} else {
    define('FFMPEG_PATH', '/home/exelanz/bin/ffmpeg');
}

define('DEMO_FIRSTDATA_API_LOGIN', 'AI1578-05');
define('DEMO_FIRSTDATA_API_KEY', '4cfbpr5x3k9yux227g5a612p800xyae8');

define('IS_LANGUAGE', 2);
define('CLASSIC_STYLE', 2);
define('MODERN_STYLE', 3);
define('TRADITIONAL_STYLE', 4);
$mysql_db = 'studio';
$mysql_user = 'studio';
$mysql_pwd = '#%!!studio%&!%##';
$db_muvi = 'muvi_data';
$db_wiki = 'wiki_data';
$hostip = '54.84.118.3';
define('REPORT_INTERVAL',300);
define('DOMAIN_COOKIE', 'muvi.com');
define('APP_PRICE', 299);
define('EMAIL_LOGO', 'https://'.$_SERVER['SERVER_NAME']. '/themes/signup/images/muvi_studio_logo_1.png');
define('EMAIL_PARTNER_LOGO', 'http://'.$_SERVER['SERVER_NAME'] . '/images/muvi_logo-partner.png');
define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/');
define('DEFAULT_STUDIO_THEME', '../../../bootstrap/views/');
define('GA_ACCOUNT', 'developer@muvi.com');
define('GA_PASS', 'dev@muvi#');
define('RELEASE', 71);
define("live_url", HTTP . 'muvi.com');
define("website_url", HTTP . 'muvi.com');
defined('POSTER_HOST') ? '' : define('POSTER_HOST', "d1yjifjuhwl7lc.cloudfront.net");
defined('POSTER_URL') ? '' : define('POSTER_URL', 'https://'. POSTER_HOST . "/public");

defined('ASSETS_URL') ? '' : define('ASSETS_URL', HTTP . "d1yjifjuhwl7lc.cloudfront.net/public/assets/");

defined('OLD_POSTER_URL') ? '' : define('OLD_POSTER_URL', HTTP . "dfquahprf1i3f.cloudfront.net/public");
defined('TRAILER_URL') ? '' : define('TRAILER_URL', HTTP . 'd1yjifjuhwl7lc.cloudfront.net');
defined('VIDEO_URL') ? '' : define('VIDEO_URL', HTTP . 'd3ff6jhdmx1yhu.cloudfront.net');
defined('S3_TRAILER_BUCKET') ? '' : define('S3_TRAILER_BUCKET', 'muvistudio');
define('internetSpeedSet1', 3);
define('internetSpeedSet2', 2);
define('internetSpeedSet3', 1.5);
define('internetSpeedSet4', 1);
define('internetSpeedSet5', 0.5);
define('internetSpeedSet6', 5);
define('internetSpeedSet7', 8);
define('STORAGE_CHARGE',0.04);
define('BANDWIDTH_CHARGE',0.09);
define('REPORT_INTERVAL',300);
$s3_ticket_attachments = 'ticketing-system';
define('CLOUDFRONT_URL', HTTP . 'dawkijsu20ml4.cloudfront.net/');
define('RTMP_URL', 'rtmp://52.20.205.94/live/');
define('HLS_URL', 'https://video.muvi.com/hls/');
define('HLS_PATH', '/var/www/vhosts/video/hls/');
define('HUBSPOT_API_KEY', 'f2981855-aad5-4bd8-8f3d-0422e1eb2028');
define('MADMIMI_KEY', '30b62bf8f0ebc4032641e0fb7a3c1b37');//5c43f29037c4c5c696b7306b1832dcb2
define('MADMIMI_EMAIL', 'viraj@muvi.com');//info@muvi.com
define('EMAIL_LOGO', 'http://' . $_SERVER['SERVER_NAME'] . '/themes/signup/images/muvi_studio_logo_1.png');
//Since live account is suspended for sometime so commented live keys and using staging keys
define('SENDGRID_APIKEY', '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');
define('SENDGRID_APIID', 'DTdZWLZSSZeXRUt3p7jI0Q');

//define('SENDGRID_APIKEY', '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');
//define('SENDGRID_APIID', 'v_b3cyHrTAGqyw0bxORsEQ');

$hostip_array=array('52.0.232.150','52.211.41.107');
$headtoken = getallheaders();
if (isset($_REQUEST['authToken']) || isset($headtoken['authtoken']) || isset($headtoken['authToken'])) {
    $cururl = substr(strtolower($_SERVER['REQUEST_URI']), 6);
    $cururl = explode("?", $cururl);
    //$base = array('checkemailexistance','getliveuserlist','getchannelonlinestatus','stopupstream','channelstatus','logout',"getmenulist", "getcontentlist", "getimagefordownload", "getcontentdetails");
	$base = array("getmenulist", "getimagefordownload", "getgenrelist", "getcategorylist",
                  "checkemailexistance", "getfbuserstatus", "getliveuserlist", "getchannelonlinestatus",
                  "stopupstream", "channelstatus", "logout","getfeaturedcontent",
                  "getbannerlist", "getbannersectionlist",
                  "getcontentlist", "getcontentdetail", "login", "homepage",'CheckGeoBlock',
                  "getstatbytype", "reviews", "isregistrationenabled", "getembedurl", "getprofiledetails", "GetAPIServer");
    if (in_array($cururl[0], $base)) {
        $basearr = array(
            'import' => array(
                'application.models.*',
                'application.components.*',
                'application.vendor.*'
            ),
            'components' => array(
                'common' => array('class' => 'UserFunction'),
                'general' => array('class' => 'GeneralFunction'),
				'muvipayment' => array('class' => 'MuviPayment'),
                'custom' => array('class' => 'CustomFunction'),
                'mvsecurity' => array('class' => 'Mvsecurity'),
                'urlManager' => array(
                    'urlFormat' => 'path',
                    'showScriptName' => false
                ),
                'db' => array(
					'connectionString' => 'mysql:host=studiomuvi.cagqnk2yhltv.us-east-1.rds.amazonaws.com;dbname=' . $mysql_db,
                    'username' => $mysql_user,
                    'password' => $mysql_pwd,
					'charset' => 'UTF8',
					'autoConnect' => true
				),
                'log' => array(
                    'class' => 'CLogRouter'
				)
            )
        );
        return $basearr;
    }
}
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Muvi',
    'defaultController' => (SUB_DOMAIN == 'partners')?'partner':'site',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.vendor.*',
        'ext.MyLinkPager'
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'sitemap' => array(
            'class' => 'ext.sitemap.SitemapModule', //or whatever the correct path is
            'absoluteUrls' => true,
            'protectedControllers' => array('admin', 'cron', 'report', 'login', 'payment', 'monetization', 'item', 'video', 'star', 'test', 'muvisdk', 'affiliate', 'notifypaypal', 'rest', 'embed', 'adminceleb', 'template', 'mergedata', 'conversion', 'user','awss3','mrss'), //optional
            'protectedActions' => array('site/error', 'sdk/downloadsubmit', 'sdk/demoreques', 'site/reload', 'site/muvilogin', 'site/pageList', '', 'movie/get_episodes', 'media/test', 'search/add', 'movie/comment_add', 'movie/filter_episode', 'media/getphpInfo', 'media/checkspeed', 'media/index', 'site/register', 'site/login', 'site/logout', 'site/logmeout', 'site/authorized', 'site/details', 'site/change_css', 'site/addmoviedetails'), //optional
            'priority' => '0.5', //optional
            'changefreq' => 'weekly', //optional
            'cacheId' => '', //optional
            'cachingDuration' => 3600,
        ),
        'comment' => array(
            'class' => 'ext.comment-module.CommentModule',
            'commentableModels' => array(
                // define commentable Models here (key is an alias that must be lower case, value is the model class name)
                'movie' => 'Movie'
            ),
            // set this to the class name of the model that represents your users
            'userModelClass' => 'User',
            // set this to the username attribute of User model class
            'userNameAttribute' => 'username',
            // set this to the email attribute of User model class
            'userEmailAttribute' => 'email',
        // you can set controller filters that will be added to the comment controller {@see CController::filters()}
        //'controllerFilters'=>array(),
        // you can set accessRules that will be added to the comment controller {@see CController::accessRules()}
        //'controllerAccessRules'=>array(),
        // you can extend comment class and use your extended one, set path alias here
        //'commentModelClass'=>'comment.models.Comment',
        ),
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'pass',
            'generatorPaths' => array(
                'bootstrap.gii',
            ),
        ),
    ),
    // application components
    'components' => array(
        'viewRenderer'=>array(
            'class'=>'application.vendor.Smarty.ESmartyViewRenderer',
            'fileExtension' => '.html',
        ),          
        'request' => array(
            //'baseUrl' => 'https://studio.muvi.com',
            'hostInfo' => $secured_hosturl
            //'baseUrl' => $secured_hosturl,
        ),
        'common' => array('class' => 'UserFunction'),
        'billing' => array('class' => 'Billing'),
        'Helper' => array('class' => 'Helper'),
        'aws'=>array('class'=>'AWS'),
        'pdf'=>array('class'=>'MYPDF'),
        'email'=>array('class'=>'MyEmail'),
		'imap'=>array('class'=>'MyImap'),
        'general' => array('class' => 'GeneralFunction'),
		'muvipayment' => array('class' => 'MuviPayment'),
        'custom' => array('class' => 'CustomFunction'),
        'mvsecurity' => array('class' => 'Mvsecurity'),
        'zip' => array(
            'class' => 'application.extensions.zip.EZip',
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'session' => array(
            'timeout' => 6 * 3600,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'class' => 'MyUrlManager',
            'showScriptName' => false,
            'urlFormat' => 'path',
        ),
        'db' => array(
            'connectionString' => 'mysql:host=studiomuvi.cagqnk2yhltv.us-east-1.rds.amazonaws.com;dbname=' . $mysql_db,
            'username' => $mysql_user,
            'password' => $mysql_pwd,
            'charset' => 'UTF8',
            'autoConnect' => true,
			'enableProfiling'=>true,
			'enableParamLogging'=>true,
        ),
        /*'cache' => array(
            'class' => 'system.caching.CDbCache',
            'connectionID' => 'db'
        ),*/
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters'=>array('127.0.0.1','52.0.232.150','117.247.67.108','203.129.207.98', '111.93.166.194','103.39.240.87'),
                )
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
		'policy' => array('class' => 'ext.Policy'),
		'policyrule' => array('class' => 'Policy'),
		'usercontent'=>array('class'=>'ugc')
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'pg_port' => ($_SERVER['HTTP_HOST'] != 'localhost') ? '5432' : '5432',
        's3_key' => 'AKIAJ4HTSCRXGHFTKRBQ',
        's3_secret' => '7qsZSeYDFueNwFA5m07T/5Oil1WHlzW/Yj7S16aq',
        's3_bucketname' => 'muvistudio',
        'video_bucketname' => 'vimeoassets',
        's3_ticket_attachments' => $s3_ticket_attachments,
        'celeb_img_thumb' => HTTP . 'dfquahprf1i3f.cloudfront.net/public/system/profile_pictures/',
        'host_ip'=>$hostip_array
        ),
);