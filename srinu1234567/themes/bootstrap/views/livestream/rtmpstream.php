<link href="https://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video.js"></script>
<script>
	videojs.options.flash.swf = "<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video-js.swf";
</script>
<style type="text/css">
	.ls-form-heading{
		font-size: 22px;
		color: #000;
		line-height: 30px;
		margin: 10px 0px;
	}
	.ls-first-radio{
		padding: 0px;
	}
	.ls-form{
		line-height: 40px;
	}
	.ls-btn-blue{background: #42b7da none repeat scroll 0 0;
		border-radius: 4px;
		color: #fff;
		font-size: 20px;
		font-weight: 400;
		padding: 10px 20px;
		text-align: center;
		text-shadow: none;
		margin-bottom: 8px;
	}
	.ls-radio{
		margin:-3px 0px 0px !important;
	}
	.playercontent{
		height: 500px;
		border: 5px solid #ccc;
		margin-bottom: 10px;
		width: 100%;
	}
.playercontent video{margin: 5px;}
.info-text{padding-bottom: 20px;line-height: 30px;font-size: 17px;}
.feed-lable{font-size: 20px;width: 10%;}
.feed-input{width:70%;text-transform: none !important;}
.feed-btn{width:10%;margin: 0 !important;}
button.btn, input.btn[type="submit"]{padding-left: 0px;padding-right:0px;}
@media (max-width: 768px) {
	.feed-lable{font-size: 14px;}
}
@media (max-width: 767px) {
	.feed-lable{font-size: 14px;width:auto;}
	.feed-input{width:auto;}
	.feed-btn{width:auto;}
}
</style>
<div class="container" style="min-height: 500px;">
	<div class="span12">
		<div class="btm-bdr ls-head">End-to-end Solution for Live Streaming</div>
	</div>
	<div class="span12">
		<div class="info-text">
			Muvi supports live streaming for HLS and RTMP. You can live stream on multiple devices-mobile, web and TV. Muvi takes care of everything including white-labeled website and mobile app, hosting, billing, analytics and support. See a live demo of live streaming below.
		</div>
		<div>
			<form  role="form" class="ls-form form-inline" action="javascript:void(0);" method="post" name="livestreamForm" id="livestreamForm" onSubmit="liveStream();">
				<div class="form-group col-sm-12">
					<label for="hls_url" class="feed-lable">RTMP Feed:</label>
					<input type="text" class="form-control feed-input" id="rtmp_url"  placeholder="" value="" required="true" pattern="rtmp?://.+" >
					<button type="submit" class="btn btn-default ls-btn-blue feed-btn" >Test It!</button>
				</div>
			</form>
		</div>	
		<div class="playercontent" id="stream_player" >
			<?php $this->renderPartial('rtmp_stream');?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
	function liveStream() {
		loadStream(document.getElementById('rtmp_url').value);
	 }
	 function loadStream(url) {
		 var $vid_obj = _V_("example_video_1");
		 console.log($vid_obj);
		 $vid_obj.src(url);
		 $vid_obj.on('loadstart',function(){
			 $vid_obj.play();
		 });
	 }
</script>
