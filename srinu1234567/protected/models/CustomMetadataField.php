<?php
class CustomMetadataField extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'custom_metadata_field';
    }
    public function getFieldData($studio_id,$customflag=0){
        $cond = 'f.studio_id ='.$studio_id;
        if($customflag){
            $cond.= " AND field_type=1";
        }
        $command = Yii::app()->db->createCommand()
            ->from('custom_metadata_field f')
            ->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value,f.f_is_required,f.field_type')
            ->where($cond);
        $Allcolumn_data = $command->queryAll();
        foreach ($Allcolumn_data as $key1 => $value1) {
            $Allcolumn_data_new[$value1['f_name']] = $value1;
	}
        if($Allcolumn_data_new){
            return $Allcolumn_data_new;
        }else{
            return array();
        }
    }
    public function insertFieldData($studio_id,$value){
        $this->studio_id = $studio_id;
        foreach ($value as $key1 => $value1) {
            $this->$key1 = $value1;
        }
        $this->isNewRecord = TRUE;
        $this->primaryKey = NULL;
        $this->save();
        return $this->id;
    }
}
