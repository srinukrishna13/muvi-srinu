<?php


class PlaylistController extends Controller{
	 public function init() {
        require_once(dirname(__FILE__) . '/ec/ecfunctions.php');
        parent::init();
    }

	public function actionshow() {
       if (@$_COOKIE['audio_content']) {
            $is_audio = $_COOKIE['movie_id'];
        } else {
            $is_audio = '0';
        }
        if($_SERVER['HTTP_X_PJAX'] == true || $is_audio == '1'){            
                $this->layout = false;
        }
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true).Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = $this->studio;
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        $connection = Yii::app()->db;
		$playlist_id = $_REQUEST['playlist_id'];
        $movie = Yii::app()->db->createCommand()
                ->select('u.id,u.playlist_name')
                ->from('user_playlist_name u')
				->where('u.user_id = 0 AND u.studio_id=' . $studio_id . ' AND u.id ='.$playlist_id)
                ->queryRow();
		$poster = $this->getPoster($movie['id'], 'playlist_poster');
		$cond = 'playlist_id=:playlist_id AND user_id=:user_id AND studio_id=:studio_id';
        $params = array(':playlist_id' => $playlist_id, ':user_id' => 0, ':studio_id' => $studio_id);
		$all_list = UserPlaylist::model()->findAll(array('condition' => $cond, 'params' => $params));
        $listCount = count($all_list);
		$user_status['fav_status'] = 1;
		$user_status['login_status'] = 0;
		if ($user_id) {
			$stat = UserFavouriteList::model()->getFavouriteContentStatus($playlist_id, $studio_id, $user_id);
			$user_status['fav_status'] = ($stat == 1) ? 0 : 1;
			$user_status['login_status'] = 1;
		}

	    if ($movie) {
                if ($meta['title'] != '') {
                    $title = $meta['title'];
                } else {
                    $temp = Yii::app()->request->url;
                    $temp = explode('/', $temp);
                    $permalink = $temp[count($temp) - 1];
                    $type = explode('-', $permalink);
                    $default_title = implode(' ', $type);
                    $default_title = ucwords($default_title);

                    $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                    // echo $Words;exit;
                    $title = $Words . ' | ' . $studio->name;
                }

                if ($meta['description'] != '') {
                    $description = $meta['description'];
                } else {
                    $description = '';
                }
                if ($meta['keywords'] != '') {
                    $keywords = $meta['keywords'];
                } else {
                    $keywords = '';
                }
                eval("\$title = \"$title\";");
                eval("\$description = \"$description\";");
                eval("\$keywords = \"$keywords\";");
                $this->pageTitle = $title;
                $this->pageDescription = $description;
                $this->pageKeywords = $keywords;
                $default_currency_id = $this->studio->default_currency_id;

                // Check if Custom filed are there for this Content
                $item = json_encode($movie);
                $this->ogImage = $poster;
                $this->is_twitter = 1;
                $this->contentDisplayName = $movie['playlist_name'];
                $this->ogTitle = $movie['playlist_name'];
                $this->render('show', array('playlist_poster' => $poster, 'playlist_data' => json_encode($movie),'count_list'=>$listCount,'user_status'=>$user_status));
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Content doesn\'t exist');
                if ($_SERVER['HTTP_REFERER']) {
                    $this->redirect($_SERVER['HTTP_REFERER']);
                    exit;
                } else {
                    $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/' . $_REQUEST['content_permalink']);
                    exit;
                }
           }
    }
	public function actionAllPlaylistData(){
		 $this->layout = false;
		 $studio_id = Yii::app()->common->getStudioId();
		if ((isset($_REQUEST['playlist_id']) && $_REQUEST['playlist_id']) && ((isset($_REQUEST['playlist_name']) && $_REQUEST['playlist_name']))) { 
			$playlist_id = $_REQUEST['playlist_id'];
			$playlist_name = $_REQUEST['playlist_name'];
			$list = Yii::app()->db->createCommand()
				 ->select('u.id,u.content_id,u.content_type')
				 ->from('user_playlist u')
				 ->where('u.user_id = 0 AND u.studio_id=' . $studio_id . ' AND  u.playlist_id ='.$playlist_id)
				 ->queryAll();
			$details = array();
			$return = true;
			foreach ($list as $data){
				$content_id = $data['content_id']; 
				$content_type = $data['content_type'];
				if($content_type == '0'){
					$command1 = Yii::app()->db->createCommand()
						->select('ms.movie_id')
						->from('films f,movie_streams ms')
						->where('f.id=ms.movie_id AND ms.id=' . $content_id . ' AND ms.is_episode = '.$content_type.' AND ms.is_converted = 1');
					$list = $command1->QueryRow();
					$content_id = $list['movie_id']; 
				}
				if($content_id != ''){
					$details[] = Yii::app()->general->getContentData($content_id,$content_type);
				}else{
					$status = "failure";
					$msg = '<p class="message">' . $this->Language['not_found'] . '</p>';
				}
			}
			$all_count = count($details);
				if($all_count > 0){
					$status = "Success";
					$msg = $this->renderPartial('playlistdata', array("kvs" => json_encode($details), 'total_items' => @$all_count,'playlist_id'=>$playlist_id), $return);
				}
		}else{
			$status = "failure";
			$msg = '<p class="message">' . $this->Language['not_found'] . '</p>';
		}
		$result = array(
            'status' => $status,
            'msg' => $msg
        );
        echo json_encode($result);
        exit;
	}
	public function actiongetAllPlaylistName(){
        $language_id = $this->language_id;
		$studio_id = Yii::app()->common->getStudiosId();
        $studio = $this->studio;
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
		if ((isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
		$isAdmin = 0;
		$playlist = Yii::app()->common->getAllPlaylistName($studio_id,$user_id,$isAdmin);
		$this->renderpartial('playlistname',array('playlist_name'=>json_encode($playlist)));
		}
	}
	public function actionqueuePlaylist(){
		$result = $_REQUEST;
		if(!empty($result)){
		foreach ($result as $res){
			$data = $res;
		}
		$count = count($data);
		
			for ($i = 0; $i < $count; $i++) {
				$final = explode(':', $data[$i]);
				$movie_id = $final[0];
				$is_episode = $final[1];
				$lists[] = Yii::app()->general->getContentData($movie_id, $is_episode);
			}
		}
		$this->renderpartial('queuelist', array('contents' => json_encode($lists), 'orderby' => @$order, 'item_count' => @$item_count, 'page_size' => @$page_size, 'pages' => @$pages));
	}
}
