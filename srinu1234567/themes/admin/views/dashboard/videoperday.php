<div class="graph-indicator text-center m-t-20 m-b-40">
    <div id="videoperday"></div>
</div>
<div class="indicator-Desc">
    <div class="info-block">

        <p>
            <span class="grey  p-l-20">
                <em class="fa fa-square icon left-icon blue"></em>

            </span>
            <span class="h5 f-500">Video views Today:  <?php echo $tot;?></span>

        </p>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#videoperday').highcharts({
            chart: {
                
                zoomType: 'x',
                height:300
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?php echo $xdata ?>
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -70,
                verticalAlign: 'top',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: { enabled: false },
            series: <?php echo $graphData; ?>,
        });
    });
</script>