<style type="text/css">
    #loader{display:none;}
</style>
<?php
$studio_theme = $studio->parent_theme;
$studio_color = $studio->default_color;
$studio = Studios::model()->findByPk(Yii::app()->user->studio_id);
$user = User::model()->findByPk(Yii::app()->user->studio_id);

$ip_address = Yii::app()->getRequest()->getUserHostAddress();
$date = date('d-m-y-h:s:i');
?>
<script>
    var studio_theme = '';
    var studio_color = '';
</script>

<form method="post" role="form" name="frmTMPL" id="frmTMPL" action="<?php echo $this->siteurl; ?>/template/savetemplate">
    <input type="hidden" name="admin_domain" id="admin_domain" value="<?php echo $_SERVER['SERVER_NAME']; ?>" />
    <input type="hidden" name="studio_id" id="studio_id" value="<?php echo $studio->id; ?>" />
    <input type="hidden" name="domain" id="domain" value="<?php echo $studio->domain; ?>" />
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user->id; ?>" />
    <input type="hidden" name="ipaddress" id="ipaddress" value="<?php echo $ip_address; ?>" />
    <input type="hidden" name="studio_theme" id="studio_theme" value="<?php echo $studio_theme; ?>" />
    <input type="hidden" name="studio_color" id="studio_color" value="<?php echo $studio_color; ?>" />       
    <input type="hidden" name="template_name" id="template_name" required value="<?php echo $studio_theme ?>" />
    <input type="hidden" name="template_color" id="template_color" required value="<?php echo $studio_color ?>" />
    <div id="template_loader" class="loading"></div>
    <div class="row m-t-40">
        <div class="col-md-12">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-cloud-upload icon left-icon "></em>
                    </div>
                    <h4>Active Template</h4>
                </div>
            </div>
            <hr>
            <div class="row m-b-20">
                <div class="col-md-12">
                    <?php
                    foreach ($active_template as $template) {
                            $template_name = $template->name;
                            $template_code = $template->code;
                            $default_color = $template->default_color;
                            $screenshot = $this->siteurl . '/sdkThemes/' . $template_code . '/' . $studio_color . '.jpg';
                            ?>
                            <div class="padding-40 border-dotted">
                                <div class="row">
                                    <div class="col-sm-3 m-b-10">
                                        <div class="template-Sec my-tooltip">
                                            <img src="<?php echo $screenshot ?>" alt="<?php echo $template_name ?>" id="screen_<?php echo $template_code ?>" class="thumbnail img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="m-b-40">
                                            <h4><a><?php echo $template_name; ?></a></h4>
                                            <div class="<?php echo $template_code; ?> theme_btns">                                               
                                                <p>                                                                                            
                                                    <a class="btn m-t-10 btn-primary" href="<?php echo $this->siteurl ?>/template/editor">Edit Template</a>
                                                    <a class="btn m-t-10 btn-primary" data-target="#uploadModal" data-toggle="modal">Upload Changes</a>
                                                    <a class="btn m-t-10 btn-primary" id="download_template">Download</a>
                                                </p>
                                            </div>
                                            <?php echo $template->short_desc; ?>
                                        </div>
                                    </div>

                                </div>

                            </div>                  
                            <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php if(!empty($templates)){ ?>
        <div class="col-md-12">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-cloud-upload icon left-icon "></em>
                    </div>
                    <h4>Available Templates</h4>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <?php
                        $checkcontenttype = Yii::app()->general->content_count($this->studio->id);
                        foreach ($templates as $template) {
                            $colors = $template->template_colors(array('condition' => 'status=1'));
                            if (count($colors) > 0 && $template->code != $studio_theme && $template->code != 'classic' && $template->code != 'byod') {
                                $template_name = $template->name;
                                $template_code = $template->code;
                                $default_color = $template->default_color;
                                $screenshot = $this->siteurl . '/sdkThemes/' . $template_code . '/' . $default_color . '.jpg';
                                ?>
                                <div class="m-b-20 col-sm-6 templatediv">
                                    <input type="hidden" value="<?php echo $template_code ?>" name="temp_<?php echo $template_code ?>" class="tmpl_name">                                
                                    <input type="hidden" value="<?php echo $default_color; ?>" name="temp_color_<?php echo $template_code ?>" class="tmpl_color">
                                    <div class="padding-40 border-dotted template-Block">
                                        <div class="row">
                                            <div class="col-sm-6 m-b-10">
                                                <div class="template-Sec my-tooltip">
                                                    <img src="<?php echo $screenshot ?>" alt="<?php echo $template_name ?>" id="screen_<?php echo $template_code ?>" class="thumbnail img-responsive">
                                                </div>
                                                <div class="<?php echo $template_code; ?> theme_btns">       
                                                    <a href="javascript:void(0);" class="btn btn-primary m-t-10 lunch">Activate</a>
                                                    <a href="javascript: return 0;" data-tname="<?php echo $template_code; ?>" data-tcolor="<?php echo $default_color;?>" <?php if($template_code != 'byod'){?>class="btn btn-primary m-t-10 preview"<?php }?>>Preview</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="m-b-40">
                                                    <h4><a><?php echo $template_name; ?></a></h4>
                                                    <?php echo $template->short_desc; ?>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>   
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>     
            </div>
        </div>  
        <?php } ?>
    </div>
    <div class="row m-b-40">
        <div class="col-xs-12">  

            <div class="row">
                <div class="col-sm-12">
                    <div class="padding-40 border-dotted">
                        <p>*Depends on specific gateway</p>
                        <b>BYOD (Bring Your Own Design)</b>
                        <p>You can customize an existing template or create a new custom template yourself using the Muvi BYOD (Bring Your Own Design) framework. You might need to enlist the help of designers and developers having knowledge of HTML & CSS, please refer the Muvi <a href="<?php echo $this->siteurl; ?>/byod" target="_blank">BYOD Help</a> for more details.</p>
                        <p>Muvi can create a custom template for you, prices start from a one-time fee of $5,000. Add a <a href="#" data-target="#contactModal" data-toggle="modal">Support Ticket</a>  for more details or to request a custom template.</p>
                    </div>
                </div>

            </div>



        </div>
    </div>

</form>

<?php
if (isset($_REQUEST['preview']) && $_REQUEST['preview'] == 1) {
    ?>
    <script type="text/javascript">
        $(function() {
            window.open("<?php echo 'http://' . $this->studio->domain . '?preview=1'; ?>", '_blank');
        });
    </script>
    <?php
}
?>
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content relative">

            <div class="centerLoader-InDiv" id="loader"> 
                <div class="preloader pls-blue">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                    </svg>
                </div>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Template File</h4>
            </div>
            <div class="modal-body">
                <p>Note: Root folder name in your zip file must be <strong><?php echo $this->studio->theme; ?></strong>.</p>
                <form name="temp_upload" id="temp_upload" method="post">  
                    <div id="message"></div>
                    <button type="button" class="btn btn-default-with-bg" onclick="click_browse('upload_temp_file')">Browse</button>
                    <span id="filename"> No file selected</span>
                    <input type="file" name="upload_temp_file" id="upload_temp_file" style="display:none;" />   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  
<div id="contactModal" class="modal fade form-horizontal" role="dialog" aria-hidden="true" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <form name="contact-form" id="contact-form" method="post" role="form">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contact Muvi Support</h4>
                </div>                
                <div class="modal-body">  
                    <div id="contact-message" class="error red"></div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="fg-line">
                                <textarea name="contact_message" id="contact_message" placeholder="Enter your Message here..." class="form-control input-sm"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-sm" id="contact-btn">Submit</button>

                    </div>

                </div>              
            </div>
        </form>
    </div>
</div>       
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js" type="text/javascript"></script>
<script type="text/javascript">
    function hasExtension(fileName, exts) {
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }
    $(document).ready(function() {
        $('#contact-btn').click(function() {
            if ($('#contact_message').val() != '') {
                $.ajax({
                    url: '<?php echo $this->createUrl('template/contactsupport'); ?>',
                    dataType: 'json',
                    cache: false,
                    data: $('#contact-form').serialize(),
                    type: 'post',
                    success: function(res) {
                        if (res.action == 'success') {
                            window.location.reload();
                        } else {
                            $('#contact-message').html(res.message)
                        }
                    }
                });
            } else {
                $('#contact-message').html('Please enter your message!');
                return false;
            }
        });


        $("#upload_temp_file").change(function(e) {
            e.preventDefault();
            $("#message").empty(); // To remove the previous error message
            var file = this.files[0];
            var filename = file.name;
            $("#filename").html(filename);
            var imagefile = file.type;
            var match = ["application/zip", "application/x-zip-compressed", "application/x-compressed", "multipart/x-zip"];
            var aftermatch = inArray(imagefile, match);
            aftermatch = hasExtension(filename, ['.zip']);
            if (aftermatch === false)
            {
                $("#message").html("<span class='error red'>Please select a valid ZIP file</p>");
                return false;
            }
            else
            {
                $('#loader').show();
                var form_data = new FormData();
                form_data.append('temp_file', file);
                $.ajax({
                    url: '<?php echo $this->createUrl('template/uploadtemplate/'); ?>',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(res) {
                        $('#loader').hide();
                        if (res.action == 'success') {
                            window.location.reload();
                        } else {
                            $('#message').html("<span class='error red'>" + res.message + "</span>");
                        }
                    }
                });
                e.stopImmediatePropagation();
                return false;
            }
        });

        $('#download_template').click(function() {
            $.ajax({
                async: false,
                type: "POST",
                url: '<?php echo $this->createUrl('template/downloadtemplate/'); ?>',
                data: {'studio_id': '<?php echo $this->studio->id; ?>', 'template': '<?php echo $studio_theme ?>'},
                dataType: 'json'
            }).done(function(data) {
                window.open(data.message, '_blank');
            });
        });

        $('.lunch').click(function(event) {
            event.preventDefault();
            var frm = $(this).closest('div.templatediv');
            var selected_template = frm.find('.tmpl_name').val();
            var selected_color = frm.find('.tmpl_color').val();
            if(selected_template != '' && selected_color != ''){
                $('#template_name').val(selected_template);
                $('#template_color').val(selected_color);
                swal({
                    title: "Are you sure?",
                    text: "Changing template will remove any current template specific data such as banner and featured content on home page!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm) {
                    if (isConfirm) {
                        $('#template_loader').show();
                        $('#frmTMPL').submit();
                        return true;
                    }
                });            
            }else{
                swal({
                    title: "No Template Selected",
                    text: "Please select the template/change the color to preview the website.!",
                    timer: 5000,
                    showConfirmButton: true,
                    confirmButtonText: "Continue"
                });
                return false;                
            }
        });

        $('.preview').click(function(event) {
            event.preventDefault();
            var selected_template = $(this).attr('data-tname');
            var selected_color = $(this).attr('data-tcolor');
            console.log(selected_template);
            if (selected_color != '' && selected_template != '')
            {
                var admin_domain = $('#admin_domain').val();
                var studio_id = $('#studio_id').val();
                var domain = $('#domain').val();
                var user_id = $('#user_id').val();
                var ipaddress = $('#ipaddress').val();
                var studio_theme = selected_template;
                var studio_color = selected_color;

                $.cookie('template_status', '1', {path: '/'});
                $.cookie('is_color', studio_color, {path: '/'});
                $.cookie('css_file', studio_color, {path: '/'});
                $.cookie('studio_id', studio_id, {path: '/'});
                $.cookie('domain', domain, {path: '/'});
                $.cookie('admin_domain', admin_domain, {path: '/'});
                $.cookie('user_id', user_id, {path: '/'});
                $.cookie('ipaddress', ipaddress, {path: '/'});
                $.cookie('template_name', studio_theme, {path: '/'});
                $.cookie('template_color', studio_color, {path: '/'});
                window.open('http://' + admin_domain + '/preview_templates/' + studio_theme, '_blank');
                return false;
            }
            else
            {
                swal({
                    title: "No Template Selected",
                    text: "Please select the template/change the color to preview the website.!",
                    timer: 5000,
                    showConfirmButton: true,
                    confirmButtonText: "Continue"
                });
                return false;
            }
        });
        $('#template_loader').hide();

        $('.colors_sel').change(function() {
            var admin_domain = $('#admin_domain').val();
            var studio_id = $('#studio_id').val();
            var domain = $('#domain').val();
            var user_id = $('#user_id').val();
            var ipaddress = $('#ipaddress').val();
            var studio_theme = $('#studio_theme').val();
            var studio_color = $('#studio_color').val();
            var v = $(this).val();
            var parts = v.split('_');
            var img_div = parts[0];
            var col = parts[1];
            var template_name = img_div;
            var template_color = col;
            if (studio_theme == template_name && studio_color == template_color)
            {
                $.cookie('template_status', '1', {path: '/'});
                $.cookie('is_color', template_color, {path: '/'});
            }
            else {
                $.cookie('template_status', '0', {path: '/'});
            }

            $.cookie('css_file', template_color, {path: '/'});
            $.cookie('studio_id', studio_id, {path: '/'});
            $.cookie('domain', domain, {path: '/'});
            $.cookie('admin_domain', admin_domain, {path: '/'});
            $.cookie('user_id', user_id, {path: '/'});
            $.cookie('ipaddress', ipaddress, {path: '/'});
            $.cookie('template_name', template_name, {path: '/'});
            $.cookie('template_color', template_color, {path: '/'});
            //$('.theme_btns').html('<a href="javascript:void(0);" class="btn btn-primary m-t-10 prv" onclick="SelectTemp()" style="margin-right:3px;">Preview</a><a href="javascript:void(0);" class="btn btn-success m-t-10 prv" onclick="SelectTemp()">Activate</a>');
            //$("." + template_name).html('<a href="http://' + admin_domain + '/preview_templates/' + template_name + '" target="_blank" class="btn btn-primary m-t-10 preview" style="margin-right:3px;">Preview</a><a href="javascript:void(0);" class="btn btn-success m-t-10"  onclick="apply_template()" >Activate</a>');
            //$("." + template_name + "x").html('<input type="button" class="btn btn-primary" value="Apply" onclick="apply_template()" />');
            //$(this).parent().parent().parent().find('.radiobtn').attr('checked', true);
            $('.radiobtn').removeAttr('checked');
            $("#temp_" + img_div).prop('checked', 'checked');
            $('#template_color').val(col);
            var img_src = $('#screen_' + img_div).attr("src");

            var parts = img_src.split('/');
            var sz = parts.length;
            var img_loc = parts[sz - 1];
            img_loc = img_src.replace(img_loc, col + ".jpg");
            $('#screen_' + img_div).attr("src", img_loc);

        });

    });

    function apply_template()
    {
        //$("#confirmApply").modal('show');
        swal({
            title: "Are you sure?",
            text: "Changing template will remove any current template specific data such as banner and featured content on home page!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                //swal("Changed!", "Your template is  changed now.", "success");
                //event.preventDefault();
                $('#template_loader').show();
                $('#frmTMPL').submit();
                return true;

            } else {
                // swal("Cancelled", "Your template is not changed :)", "");
            }
        });
    }
    function apply_template_exit()
    {
        // $("#selModal").modal('show');
        swal({
            title: "No Template Selected",
            text: "Please select the template/change the color to preview the website.!",
            timer: 5000,
            showConfirmButton: true,
            confirmButtonText: "Continue"
        });
        return false;
    }
    function click_browse(modal_file) {
        $('#' + modal_file).click();
    }
    function SelectTemp() {
        swal({
            title: "No Template Selected",
            text: "Please select the template/change the color to preview the website !",
            timer: 5000,
            showConfirmButton: true,
            confirmButtonText: "Continue"
        });
        //return false;
    }
    function inArray(needle, haystack) {
        var length = haystack.length;
        for (var i = 0; i < length; i++) {
            if (haystack[i] == needle)
                return true;
        }
        return false;
    }
</script>