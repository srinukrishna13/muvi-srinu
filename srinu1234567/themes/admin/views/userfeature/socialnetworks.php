<?php 
$is_subscribed = $status['is_subscribed'];
$is_default = $status['is_default'];
//$is_subscribed = 0;
?>
<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal" class="update_content_type" name="facebook_login" id="facebook_login" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/facebooklogin" method='post'>
            <input type="hidden" value="<?php echo $sociallogininfos['id']; ?>" name="id">
            <?php if($sociallogininfos['whose_fb']!=''){
            ?>
            <input type="hidden" value="1" name="enb_fblogin">
            <?php
            }?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($sociallogininfos['whose_fb'] !='')?'checked':''; ?> name="enb_fblogin" id="enb_fblogin" class="content"  <?php echo ($sociallogininfos['whose_fb']!='')?'disabled':''?>>
                        <i class="input-helper"></i>  Enable Facebook Login for users&nbsp;&nbsp;
                    </label>
                </div>
            </div>
            <div class="form-group" id='existing_facebook' style="<?php echo ($sociallogininfos['whose_fb']=='')?'display:none;':''; ?>">
                <div class="col-md-12">                  
                    <?php if($is_subscribed !=0 || $is_default != 0){ ?>
                    <label class="radio radio-inline">                            
                    <input required type="radio" name="whose_fb" id="whose_fb2" class="radiobtn" value="muvi_fb" <?php echo ($sociallogininfos['whose_fb']=='muvi_fb')?'checked':''; ?> <?php echo ($sociallogininfos['whose_fb']=='')?'checked':''; ?> <?php echo ($sociallogininfos['whose_fb']!='')?'disabled':''?>>                                
                    <i class="input-helper"></i> Muvi to create and manage the Facebook App (recommended)
                    </label>
                    <?php } ?>
                    <label class="radio radio-inline">                                       
                    <input required type="radio" name="whose_fb" id="whose_fb1" class="radiobtn" value="user_fb" <?php echo ($sociallogininfos['whose_fb']=='user_fb')?'checked':''; ?>  <?php echo ($sociallogininfos['whose_fb']!='')?'disabled':''?>>                                
                    <i class="input-helper"></i> Use my Facebook App
                    </label>                      
                </div>
            </div>
            <div class="row" id='user_app' style="<?php echo ($sociallogininfos['whose_fb']=='user_fb')?'':'display:none;'; ?>">
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-md-4 control-label">App ID<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_id" value="<?php echo $sociallogininfos['fb_app_id'];?>" class="form-control input-sm" placeholder="Please enter facebook App Id">
                            </div>
                        </div>              
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label">App Secret<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_secret" value="<?php echo $sociallogininfos['fb_secret'];?>" class="form-control input-sm" placeholder="Please enter App secret key">
                            </div>
                        </div>              
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label">App Version<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_version" value="<?php echo $sociallogininfos['fb_app_verson'];?>" class="form-control input-sm" placeholder="Please enter facebook App version">
                            </div>
                        </div>              
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label">Redirect URL<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_redirect_url" value="<?php echo Yii::app()->getBaseUrl(true);?>/Login/FacebookAuth/" class="form-control input-sm" readonly>
                            </div>
                        </div>              
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php
                    if($sociallogininfos['status']==2)
                    {
                    ?>
                        <h4>Your Facebook Login is <font color="#FFDE7D">PROCESSING</font></h4>
                        <p>Your request is received, Muvi is working to create an App. You can expect a swanky new login in 2 days.</p>
                    <?php
                    }
                    else if($sociallogininfos['status']==3)
                    {
                    ?>
                        <button type="submit" id="fblogin" class="btn btn-primary">Update</button>
                        <button type="submit" id="fblogin" class="btn btn-primary" value="redirect" name="redirect">Redirect URL entered to App</button>
                        <h4>Your Facebook Login is <font color="#27A226">Pending AuthURL</font></h4>
                        <p>Your login will be enabled as soon as the Redirect URL is entered into the App.</p>         
                    <?php            
                    }
                    else if($sociallogininfos['status']==1)
                    {
                        if($sociallogininfos['whose_fb']=='user_fb')
                        {
                        ?>
                        <button type="submit" id="fblogin" class="btn btn-primary">Update</button>
                        <?php
                        }
                    ?>
                        <h4>Your Facebook Login is <font color="#27A226">Active</font></h4>     
                    <?php               
                    }
                    else
                    {
                    ?>
                        <button type="submit" id="fblogin" class="btn btn-primary" <?php echo ($status['status'] != 1)?'disabled':''?>>Confirm</button>
                        <p></p>
                        <p id="msgdis">A Muvi developer will create your App, and link it to the website. takes 2 days.</p>          
                    <?php
                    }
                    ?>                    
                </div>                
            </div>
        </form>
    </div>        
</div>
<?php
if($sociallogininfos['status']==1)
{
?>
<form class="form-horizontal" class="update_content_type" name="facebook_loginactive" id="facebook_loginactive" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/facebookloginactive" method='post'>
<div class="row m-t-20">
    <div class="col-sm-12">
        <?php
        if($status['social_logins'] == 1)
        {
            $confirm_title="Deactive Facebook Login";
            $confirm_message="Are you sure to Deactive Facebook Login";
        ?>
        <input type="hidden" name="social_logins" value="0">
        <button type="button" id="fbact" class="btn btn-primary">Deactive Facebook Login</button>
        <br />Facebook login button will not shown in login page.
        <?php
        }
        else
        {
            $confirm_title="Active Facebook Login";
            $confirm_message="Are you sure to Active Facebook Login";
        ?> 
        <input type="hidden" name="social_logins" value="1">
        <button type="button" id="fbact" class="btn btn-primary">Active Facebook Login</button>
        <br />Facebook login button will shown in login page.
        <?php    
        }
        ?>
    </div>
</div> 
</form>    
<?php
}
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#fbact').click(function(){
            swal({
                title: "<?php echo $confirm_title;?>",
                text: "<?php echo $confirm_message;?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function() {
                $('#facebook_loginactive').submit();
            });

        });
    });   
</script>    
<script type="text/javascript">
     $("#enb_fblogin").click(function () {
        0 == this.checked ? $("#existing_facebook").hide() : $("#existing_facebook").show()
    });
     $("#whose_fb1").click(function () {
        0 == this.checked ? $("#user_app").hide() : $("#user_app").show();
        $('#msgdis').html("The redirect URL needs to be linked to your Facebook account.");
    }); 
     $("#whose_fb2").click(function () {
        0 == this.checked ? $("#user_app").show() : $("#user_app").hide();
        $('#msgdis').html("A Muvi developer will create your App, and link it to the website. takes 2 days.");
    });    
     
    $('#facebook_login').validate({ 
        rules: {
            enb_fblogin: {required: true},
            app_id: {required: true},
            app_secret: {required: true},
            app_version: {required: true},

        }, 
        messages: { 
            enb_fblogin: 'Click on the checkbox to enable facebook login.',
            app_id: 'Please enter app id.',
            app_secret: 'Please enter app secrete key.',
            app_version: 'Please enter app version.',
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }
    });
    var x = validate.form();
    if (x) {
        $('#facebook_login').submit();
    }
</script>