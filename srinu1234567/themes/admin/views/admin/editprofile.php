<div class="col-lg-8">
	<div class="box box-primary">
<!--		<div class="box-header">
			<h3 class="box-title">Quick Example</h3>
		</div>-->
		<!-- form start -->
		<form role="form" name="profile" action="<?php echo $this->createUrl('admin/editprofile');?>" method="post" data-toggle='validator'>
			<div class="box-body">
				<div class="form-group">
					<label for="firstname">First Name</label>
					<input type="text" placeholder="Enter First Name" id="first_name" class="form-control" name="profile[first_name]" required value="<?php echo $userdata['first_name'];?>">
				</div>
				<div class="form-group">
					<label for="lastname">Last Name</label>
					<input type="text" placeholder="Enter Last Name" id="last_name" class="form-control" name="profile[last_name]" value="<?php echo $userdata['last_name'];?>">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" placeholder="Enter Last Name" id="email" class="form-control" name="profile[email]" disabled value="<?php echo $userdata['email'];?>">
				</div>
				<div class="form-group">
					<label for="company">Company</label>
					<input type="text" placeholder="Enter Company Name" id="company" class="form-control" name="profile[company]"  value="<?php echo $userdata['company'];?>">
				</div>
				<div class="form-group">
					<label for="company">Company Type:</label>
					<input type="text" placeholder="Enter Type of Company" id="company_type" class="form-control" name="profile[type_of_company]"  value="<?php echo $userdata['type_of_company'];?>">
				</div>
				<div class="form-group">
					<label for="company">Current Address:</label>
					<textarea placeholder="Enter Your Current Address..." rows="3" class="form-control" name="profile[address_1]"><?php if(isset($userdata['address_1']) && $userdata['address_1'] ){echo $userdata['address_1'];}?></textarea>
				</div>
				<div class="form-group">
					<label for="company">Permanent Address:</label>
					<textarea placeholder="Enter Your Permanent Address..." rows="3" class="form-control" name="profile[address_2]"><?php echo $userdata['address_1'];?></textarea>
				</div>
				<div class="form-group">
					<label for="company">Phone Number:</label>
					<input type="text" placeholder="Enter your phone Number..." id="phone_no" class="form-control" name="profile[phone_no]"  value="<?php echo $userdata['phone_no'];?>" data-maxlength='10'>
				</div>
				<div class="form-group">
					<label for="city">city:</label>
					<input type="text" placeholder="Enter your City.." id="city" class="form-control" name="profile[city]"  value="<?php echo $userdata['city'];?>">
				</div>
				<div class="form-group">
					<label for="state">State:</label>
					<input type="text" placeholder="Enter your State.." id="state" class="form-control" name="profile[state]"  value="<?php echo $userdata['state'];?>">
				</div>
				<div class="form-group">
					<label for="country">Country:</label>
					<select class="form-control"  name="profile[country]">
						<option  value="">-Choose Country-</option>
						<?php foreach($country as $key=>$val){?>
							<option  value="<?php echo $val['countryName'];?>" <?php if($userdata['country']==$val['countryName']){?>Selected='selected'<?php }?>><?php echo $val['countryName'];?></option>
					<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label for="zip">Zip:</label>
					<input type="text" placeholder="Enter your Zip Code..." id="zip" class="form-control" name="profile[zip]"  value="<?php echo $userdata['zip'];?>">
				</div>
			</div>
			<div class="box-footer">
				<button class="btn btn-primary" type="submit">Update</button>
				<button class="btn btn-default" type="reset">Cancel</button>
			</div>
		</form>
	</div>
</div>

