<?php

class Encoding extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'encoding';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    public function getEncodingData($streamIds) {
        $data = Yii::app()->db->createCommand()->select('movie_stream_id,expected_end_time')->from($this->tableName())->where('encoding_server_id !=0 AND expected_end_time !="" AND expected_end_time != "0000-00-00 00:00:00" AND movie_stream_id in ('.$streamIds.')')->queryAll();
        return $data;
    }
    
    public function getEncodingDataForAjax($streamIds) {
        $data = Yii::app()->db->createCommand()->select('movie_stream_id,expected_end_time,encoding_server_id')->from($this->tableName())->where('movie_stream_id in ('.$streamIds.')')->queryAll();
        return $data;
    }  
}
