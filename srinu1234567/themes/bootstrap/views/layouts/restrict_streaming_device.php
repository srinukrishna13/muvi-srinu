<?php if($getDeviceRestriction != '' && $getDeviceRestriction > 0){ ?>
    <script>
        var stream_id = "<?php echo $stream_id; ?>";
        var restrictDeviceUrl = '<?php echo Yii::app()->baseUrl; ?>/videoLogs/addDataToRestrictDevice';
        var deleteRestrictDeviceUrl = '<?php echo Yii::app()->baseUrl; ?>/videoLogs/deleteDataFromRestrictDevice';
    </script>
    <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/js/video-streaming-restriction.js?v=<?php echo RELEASE ?>"></script>
<?php } ?>