<?php if(isset(Yii::app()->user->created_at)){
	$bildate = date('dS',(strtotime(Yii::app()->user->created_at)+15*24*60*60));
}else{
	$bildate = date('dS',(time()+15*24*60*60));
}
$months = array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','N0V','DEC');
?>
<p>
	<span class="payment-span">First 14 days are free. Then, $499 per month + Bandwidth Pricing </span><br />
	<a href="<?php echo Yii::app()->baseUrl;?>/pricing" target="_blank" class="link">Learn more about pricing</a>
	<div style="height:0px;"></div>
	Enter your credit card details. It will be charged as per the above pricing at the end of every billing cycle, <?php echo $bildate;?> of every month. 
	
</p>
<form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" enctype="multipart/form-data" onsubmit="return validateForm();" action="javascript:void(0);">
  <div class="form-group">
    <label for="inputEmail3" class="span2 control-label">Name on Card</label>
    <div class="span10">
		<input type="text" class="form-control" id="card_name" name="card_name" placeholder="Enter Name" required="true">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="span2 control-label">Card Number</label>
    <div class="span10">
		<input type="text" class="form-control" id="card_number" name="card_number" placeholder="Enter Card Number" required="true">
    </div>
  </div>
  <div class="form-group exp-dt-dropdown">
    <label for="inputEmail3" class="span2 control-label">Expiry Date</label>
    <div class="span10">
		<select name="exp_month" id="exp_month" class="form-control" required="true">
		<option value="">Expiry Month</option>	
		<?php for($i=date('m');$i<=12;$i++){?>
			<option value="<?php echo $i;?>"><?php echo $months[$i-1];?></option>
		<?php }?>
		</select>
		<select name="exp_year" id="exp_year" class="form-control" required="true" onchange="getMonthList();">
			<option value="">Expiry Year</option>
		<?php for($i=date('Y');$i<=date('Y')+20;$i++){?>
			<option value="<?php echo $i;?>"><?php echo $i;?></option>
		<?php }?>
		</select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="span2 control-label">Security Code</label>
    <div class="span10">
		<input type="password" class="form-control" id="security_code" name="security_code" placeholder="Enter security code" required="true">
		<label id="email-error" class="error" for="email" style="display: none;"></label>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="span2 control-label">Billing Address</label>
    <div class="span10">
		<input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" required="true">
		<input type="text" class="form-control" id="address2" name="address2" placeholder="Address 2" >
		<input type="text" class="form-control" id="city" name="city" placeholder="City" required="true"><br/>
		<input type="text" class="form-control" id="billing_address" name="state" placeholder="State" required="true" style="width: 23%">
		<input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" required="true" style="width: 24%">
    </div>
  </div>
  <div class="form-group">
	 <label for="" class="span2 control-label"></label> 
    <div class=" span10">
		<button type="button" class="btn btn-grey" id="prvbtn" disabled="disabled">Back</button> &nbsp;&nbsp;
		<button type="submit" class="btn btn-blue" id="nextbtn">Next</button>
	</div>
  </div>
	<input type="hidden" value="0" id="is_submit" name="is_submit"/>
</form>
<div style="display: none;" id="mnth_opt">
	<option value="">Expiry Month</option>
	<option value="1">JAN</option>
	<option value="2">FEB</option>
	<option value="3">MAR</option>
	<option value="4">APR</option>
	<option value="5">MAY</option>
	<option value="6">JUN</option>
	<option value="7">JUL</option>
	<option value="8">AUG</option>
	<option value="9">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
</div>
<script type="text/javascript">
	var months = new Array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','N0V','DEC');
	function getMonthList(){
			var d = new Date();
			var curyr = d.getFullYear();
			var selyr = $('#exp_year').val();
			if(curyr ==selyr){
				var curmonth = d.getMonth()+1;
				var month_opt = '<option value="">Expiry Month</option>';
				for(i=curmonth;i<=12;i++){
					month_opt += '<option value="'+i+'">'+months[i-1]+'</option>';
			    }
				$('#exp_month').html(month_opt);
			}else{
				$('#exp_month').html($('#mnth_opt').html());
			}
		}
	function validateForm(){
		$('#email-error').hide();
		 //form validation rules
          var validate =  $("#paymentMethod").validate({
                rules: {
                    card_name: "required",
                    exp_month: "required",
                    exp_year: "required",
                    security_code: "required",
                    address1: "required",
                    city: "required",
                    state: "required",
                    zipcode: "required",
                    card_number: {
                        required: true,
                        number: true
                    }
                },
                messages: {
                    card_name: "Please enter a valid name",
                    exp_month: "Please select the expiry month",
                    exp_year: "Please select the expiry year",
                    security_code : "Please enter your security code",
                    address1 : "Please enter your address",
                    city : "Please enter your city",
                    state : "Please enter your State",
                    zipcode : "Please enter your Zipcode",
                    card_number: {
                        required: "Please enter a valid card number",
                        number: "Please enter a valid card number"
                    }
                }
            });
			var x = validate.form();
			if(x){
				alert('valid Data');
				/*$('#nextbtn').html('wait!...');
				$('#nextbtn').attr('disabled','disabled');
				var url ="<?php //echo Yii::app()->baseUrl;?>/signup/checkEmail";
				$.post(url,{'email':$('#email').val()},function(res){
					if(res.succ){	
						$('#is_submit').val(1);
						document.userinfo.action = '<?php echo Yii::app()->baseUrl;?>/signup';
						document.userinfo.submit();
					}else{
						$('#nextbtn').html('Next');
						$('#nextbtn').removeAttr('disabled');
						$('#is_submit').val('');
						$('#email-error').show();
						$('#email-error').html('Oops! Sorry Email already exists!');
					}
				},'json');*/
			}
	}
</script>