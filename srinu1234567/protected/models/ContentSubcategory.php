<?php
class ContentSubcategory extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'content_subcategory';
    }
    
    /*Added by - RKS
     * To get the content subcategory properties from Binary Value
     * @params: $studio_id, $binary_value of category, field names separated by comma to select
     */
    public function SubCategoryFromBinaryValue($studio_id = 0, $binary_value, $fields = 'subcat_name') {
        $fields = ContentSubcategory::model()->findAll(array(
            'select'=> $fields,
            'condition' => 'studio_id=:studio_id AND subcat_binary_value = :binary_value',
            'params' => array(':studio_id' => $studio_id, ':binary_value' => $binary_value),
            'order' => 't.id DESC',
            'limit' => '1'            
        )); 
        return(@$fields[0]);        
    }    
	public function SubCategoryFromCommavalue($studio_id = 0, $subcategory_value, $fields = 'subcat_name') {
        $fields = ContentSubcategory::model()->findAll(array(
            'select'=> $fields,
            'condition' => 'studio_id=:studio_id AND id = :subcategory_value',
            'params' => array(':studio_id' => $studio_id, ':subcategory_value' => $subcategory_value),
            'order' => 't.id DESC',
            'limit' => '1'            
        )); 
        return(@$fields[0]);        
	}
}