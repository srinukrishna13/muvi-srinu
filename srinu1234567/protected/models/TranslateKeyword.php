<?php

class TranslateKeyword extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'translate_keyword';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function getMessages($trans_type,$studio_id=false){
        $condition = 'trans_type=:trans_type';
        $params[':trans_type'] = $trans_type;
        if($studio_id){
            $condition .=' AND(studio_id=:studio_id OR studio_id=0)';
            $params[':studio_id'] = $studio_id;
        }
        $msgs = TranslateKeyword::model()->findAll(array(
            'condition'=>$condition,
            'params'=>$params
        ));
        return $this->makeTransKeyValue($msgs);
    }
    public function getAllMessages($studio_id=false,$mobile_app=false){
        $where = '';
        $condition = '';
        if($mobile_app){
          $condition .='(device_type=:device_type OR device_type=:all_device)';  
          $params[':device_type'] = $mobile_app;
          $params[':all_device']  = '2';
        if($studio_id){
              $condition .=" AND ";
          }
        }
        if($studio_id){
            $condition .='(studio_id=:studio_id OR studio_id=0)';
            $params[':studio_id'] = $studio_id;
            $where = array('condition'=>$condition,'params'=>$params);
        }
        $msgs = TranslateKeyword::model()->findAll($where);
        return $this->makeTransKeyValue($msgs);
        
    }
    public function makeTransKeyValue($msgs){
        $message = array();
        if(!empty($msgs)){
            foreach($msgs as $msg){
              $message[$msg->trans_key]  =  $msg->trans_value;
            }
        }
        return $message;
    }
    public function GetKeysofValue($trans_array=array()){
        $duplicate_keys = array();
        if(!empty($trans_array)){
            foreach($trans_array as $key=>$val){
                $trans = Yii::app()->db->createCommand()
                    ->select('trans_key,trans_type')
                    ->from('translate_keyword')
                    ->where('trans_value=:trans_val AND trans_key !=:trans_key', array(':trans_val'=>$val,':trans_key'=>$key))
                    ->queryRow();
                $duplicate_keys[$key] =  $trans['trans_key'];
            }
        }
        return $duplicate_keys;
    }
    public function getLanguageType($trans_key,$studio_id){
        $data = Yii::app()->db->createCommand()
            ->select('trans_type')
            ->from('translate_keyword')
            ->where("trans_key =:trans_key AND (studio_id=:studio_id OR studio_id=0)", array(':studio_id'=>$studio_id,':trans_key'=>$trans_key))
            ->setFetchMode(PDO::FETCH_OBJ)->queryROW();
        if(!empty($data)){
            return $data->trans_type;
        }else{
            return false;
        }
    }
    public function checkUniqueKey($studio_id, $trans_key, $trans_val){
        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from('translate_keyword')
            ->where("trans_key =:trans_key AND (studio_id=:studio_id OR studio_id=0)", array(':studio_id'=>$studio_id,':trans_key'=>$trans_key))
            ->queryAll();
        return $data;
}
    public function insertNewKey($studio_id, $trans_key, $trans_val,$device_type = '0'){
        $trans = new TranslateKeyword;
        $trans->studio_id   = $studio_id;
        $trans->trans_key   = $trans_key;
        $trans->trans_value = $trans_val;
        $trans->trans_type  = '0';
        $trans->device_type = $device_type;
        $trans-> setIsNewRecord(true);
	$trans-> setPrimaryKey(NULL);
        return $trans->save();
    }
    public function UpdateKey($studio_id, $trans_key, $trans_val = ''){
        $trans = TranslateKeyword::model()->findByAttributes(array('studio_id'=>$studio_id,'trans_key'=>$trans_key));
        if(!empty($trans)){
            $trans->trans_value = $trans_val;
            return $trans->save();
        }else{
            $this->insertNewKey($studio_id, $trans_key, $trans_val,'2');
        }
    }
    public function DeleteKey($studio_id, $trans_key){
        $params = array(':studio_id'=>$studio_id, ':trans_key'=>$trans_key);
        return TranslateKeyword::model()->deleteAll('studio_id=:studio_id AND trans_key=:trans_key', $params);
    }    
	
	public function getKeyValue($lang_code, $trans_key, $studio_id){
        $lang      = array();
        $theme     = Studio::model()->findByPk($studio_id, array('select' => 'theme'))->theme;
        if(file_exists(ROOT_DIR."languages/".$theme."/".$lang_code.".php")){
            $lang  = include( ROOT_DIR."languages/".$theme."/".$lang_code.".php");
            $lang  = @$lang["server_messages"] ? @$lang["server_messages"] : $lang;
		}
        if(!isset($lang[$trans_key])){
            $lang = Yii::app()->db->createCommand()->select('trans_value')->from('translate_keyword')->where("(studio_id = :studio_id OR studio_id = 0) AND trans_key = :trans_key",array(':studio_id'=>$this->studio_id, ':trans_key' => $trans_key))->queryColumn();
            return @$lang[0];
        } else {
            return @$lang[$trans_key];
        }
    }
}
