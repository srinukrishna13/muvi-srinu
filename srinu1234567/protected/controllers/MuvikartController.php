<?php
    header("Access-Control-Allow-Origin: *");
    header('content-type: application/json; charset=utf-8');
    Yii::import('application.controllers.RestController');
    class MuvikartController extends RestController{
        /* 
         * This Function using for getting Product Lists
         * We need to send Permalink for getting Product Lists
         */
        public function actionGetProductList() {
        $studio_id = $this->studio_id;       
        $baseurl='https://'.$_SERVER['SERVER_NAME'];
        $permalink = $_REQUEST['permalink'];
        $offset = $_REQUEST['offset'];
        $limit = $_REQUEST['limit'];
        $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
        if ($permalink) {
            if($permalink=='muvikart')  
            {
             $command = Yii::app()->db->createCommand()
                    ->select("SQL_CALC_FOUND_ROWS (0), p.*")
                    ->from("pg_product p")
                    ->where("p.studio_id=:studio_id AND p.is_deleted!=1 AND ((p.publish_date IS NULL) OR  (UNIX_TIMESTAMP(p.publish_date) = 0) OR (p.publish_date <=NOW()))", array(":studio_id" => $this->studio_id))
                    ->order('p.name DESC')
                    ->limit($limit, $offset);
            $list = $command->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
           
            foreach ($list as $key => $val) {
                        $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                        $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val['id'])->queryAll();
                        if (!empty($image)) {
                            $pgposter = $url . '/system/pgposters/' . $val['id'] . '/standard/' . $image[0]['name'];
                            $pgposteroriginal = $url . '/system/pgposters/' . $val['id'] . '/original/' . $image[0]['name'];
                            if (false === file_get_contents($pgposter)) {
                                $pgposteroriginal=$pgposter = $baseurl . '/img/no-image.jpg';
                                
                            }
                        } else {
                           $pgposteroriginal= $pgposter = $baseurl . '/img/no-image.jpg';
                        }
                if($val['currency_id']){
                 $currency_id=$val['currency_id'];
                }else{
                  $currency= Yii::app()->db->createCommand()
                    ->select("currency_id,price")
                    ->from("pg_multi_currency")
                    ->where("product_id =:product_id and studio_id=:studio_id" , array(":product_id"=>$val['id'],':studio_id'=>$this->studio_id))
                    ->queryAll(); 
                  $currency_id=$currency[0]['currency_id'];
                  $list[$key]['sale_price'] = $currency[0]['price'];
                }
                $currencydetails = Yii::app()->db->createCommand()
                    ->select("symbol")
                    ->from("currency")
                    ->where("id =:id" , array(":id"=>$currency_id))
                    ->queryRow();
                $symbol=$currencydetails['symbol'];             
                $list[$key]['currency_symbol'] = $symbol;
                $list[$key]['poster_url'] = $pgposter;
                $list[$key]['poster_original'] = $pgposteroriginal;
                } 
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Content Details';
                $data['itemlist'] = @$list;
                $data['item_count'] = @$item_count;   
            }else{
            if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != 'en')
                $language_id = Yii::app()->custom->getLanguage_id($_REQUEST['lang_code']);
            else
                $language_id = 20;
            $menuItemInfo = MenuItem::model()->find('studio_id=:studio_id AND permalink=:permalink', array(':studio_id' => $this->studio_id, ':permalink' => $_REQUEST['permalink']));
            if ($menuItemInfo) {
                $menuInfo = $menuItemInfo->attributes;               
               
                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }
                $pdo_cond_arr = array(':studio_id' => $this->studio_id, ':content_category_value' => $menuInfo['value']);
                $order = '';
                $cond = ' ';
                $pdo_cond = '';
                $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
                    $order = $_REQUEST['orderby'];
                    if ($_REQUEST['orderby'] == 'releasedate') {
                        $orderby = "  p.release_date DESC";
                    } else if ($_REQUEST['orderby'] == 'sortasc') {
                        $orderby = "  p.name ASC";
                    } else if ($_REQUEST['orderby'] == 'sortdesc') {
                        $orderby = "  p.name DESC";
                    }
                } else {
                    $orderby = "  p.updated_date DESC ";
                    $neworderby = " P.last_updated_date DESC ";
                }
                $cond .= ' AND p.is_deleted!=1 AND ((p.publish_date IS NULL) OR  (UNIX_TIMESTAMP(p.publish_date) = 0) OR (p.publish_date <=NOW()) ) ';

                $command = Yii::app()->db->createCommand()
                        ->select("SQL_CALC_FOUND_ROWS (0), p.*")
                        ->from("pg_product prod p")
                        ->where("p.content_category_value = :content_category_value and p.studio_id = :studio_id" . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                $list = $command->queryAll();
                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
               
                foreach ($list as $key => $val) {
                        
                        $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                        $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val['id'])->queryAll();
                        if (!empty($image)) {
                            $pgposter = $url . '/system/pgposters/' . $val['id'] . '/standard/' . $image[0]['name'];
                            $pgposteroriginal=$url . '/system/pgposters/' . $val['id'] . '/original/' . $image[0]['name'];
                            if (false === file_get_contents($pgposter)) {
                                $pgposteroriginal=$pgposter = $baseurl . '/img/no-image.jpg';
                                
                            }
                        } else {
                           $pgposteroriginal= $pgposter = $baseurl . '/img/no-image.jpg';
                        } 
                       if($val['currency_id']){
                                $currency_id=$val['currency_id'];
                               }else{
                                 $currency= Yii::app()->db->createCommand()
                                   ->select("currency_id,price")
                                   ->from("pg_multi_currency")
                                   ->where("product_id =:product_id and studio_id=:studio_id" , array(":product_id"=>$val['id'],':studio_id'=>$this->studio_id))
                                   ->queryAll(); 
                                 $currency_id=$currency[0]['currency_id'];
                                 $list[$key]['sale_price'] = $currency[0]['price'];
                               }
                               $currencydetails = Yii::app()->db->createCommand()
                                   ->select("symbol")
                                   ->from("currency")
                                   ->where("id =:id" , array(":id"=>$currency_id))
                                   ->queryRow();
                               $symbol=$currencydetails['symbol'];             
                         $list[$key]['currency_symbol'] = $symbol;
                         $list[$key]['poster_url'] = $pgposter;
                        $list[$key]['poster_original'] = $pgposteroriginal;
                }
                $data['code'] = 200;
                $data['status']='OK';
                $data['msg'] = 'Content Details';
                $data['orderby'] = @$order;
                $data['itemlist'] = $list;
                $data['item_count'] = @$item_count;
                $data['limit'] = @$page_size;
            } else {
                $data['code'] = 412;
                $data['status'] = "Invalid Content Type";
                $data['msg'] = "Provided permalink is invalid!";
            }
            }
        }else {
            $command = Yii::app()->db->createCommand()
                    ->select("SQL_CALC_FOUND_ROWS (0), p.*")
                    ->from("pg_product p")
                    ->where("p.studio_id=:studio_id AND p.is_deleted!=1 AND ((p.publish_date IS NULL) OR  (UNIX_TIMESTAMP(p.publish_date) = 0) OR (p.publish_date <=NOW()))", array(":studio_id" => $this->studio_id))
                    ->order('p.name DESC')
                    ->limit($limit, $offset);
            $list = $command->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
             
            foreach ($list as $key => $val) {
                       $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                        $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val['id'])->queryAll();
                        if (!empty($image)) {
                            $pgposter = $url . '/system/pgposters/' . $val['id'] . '/standard/' . $image[0]['name'];
                            $pgposteroriginal= $url . '/system/pgposters/' . $val['id'] . '/original/' . $image[0]['name'];
                            if (false === file_get_contents($pgposter)) {
                               $pgposteroriginal= $pgposter = $baseurl . '/img/no-image.jpg';
                            }
                        } else {
                           $pgposteroriginal= $pgposter = $baseurl . '/img/no-image.jpg';
                        }
                if($val['currency_id']){
                 $currency_id=$val['currency_id'];
                }else{
                  $currency= Yii::app()->db->createCommand()
                    ->select("currency_id,price")
                    ->from("pg_multi_currency")
                    ->where("product_id =:product_id and studio_id=:studio_id" , array(":product_id"=>$val['id'],':studio_id'=>$this->studio_id))
                    ->queryAll(); 
                  $currency_id=$currency[0]['currency_id'];
                  $list[$key]['sale_price'] = $currency[0]['price'];
                }
                $currencydetails = Yii::app()->db->createCommand()
                    ->select("symbol")
                    ->from("currency")
                    ->where("id =:id" , array(":id"=>$currency_id))
                    ->queryRow();
                $symbol=$currencydetails['symbol'];             
                $list[$key]['currency_symbol'] = $symbol;
                $list[$key]['poster_url'] = $pgposter;
                $list[$key]['poster_original'] = $pgposteroriginal;
                } 
                $data['code'] = 200;
                $data['status']='OK';
                $data['msg'] = 'Content Details';                
                $data['itemlist'] = @$list;
                $data['item_count'] = @$item_count;               
        }
        echo json_encode($data);
        exit;
    }

         /* 
         * This Function using for getting Product details
         * We need to send Permalink for getting Product Details
         */
        public function actionGetProductDetails(){
        $baseurl='https://'.$_SERVER['SERVER_NAME'];
        $studio_id  = $this->studio_id;
        $permalink = $_REQUEST['permalink'];
        $offset = $_REQUEST['offset'];
        $limit = $_REQUEST['limit'];
        $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
            if($permalink){
            $productdetails = Yii::app()->db->createCommand()
                    ->select("*")
                    ->from("pg_product")
                    ->where("permalink =:permalink AND studio_id=:studio_id AND is_deleted!=1 AND ((publish_date IS NULL) OR  (UNIX_TIMESTAMP(publish_date) = 0) OR (publish_date <=NOW()))", array(":permalink"=>$permalink,":studio_id" => $this->studio_id))                                       
                    ->queryAll();
                $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $productdetails[0]['id'])->queryAll();
                if (!empty($image)) {
                    $pgposter = $url . '/system/pgposters/' . $productdetails[0]['id'] . '/standard/' . $image[0]['name'];
                   $pgposteroriginal = $url . '/system/pgposters/' . $productdetails[0]['id'] . '/original/' . $image[0]['name'];
                    if (false === file_get_contents($pgposter)) {
                       $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                    }
                } else {
                   $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                }
                $productdetails[0]['poster'] = $pgposter;
                $productdetails[0]['poster_original'] = $pgposteroriginal;
                if($productdetails[0]['currency_id']){
                 $currency_id=$productdetails[0]['currency_id'];
                }else{
                  $currency= Yii::app()->db->createCommand()
                    ->select("currency_id,price")
                    ->from("pg_multi_currency")
                    ->where("product_id =:product_id and studio_id=:studio_id" , array(":product_id"=>$productdetails[0]['id'],':studio_id'=>$this->studio_id))
                    ->queryAll(); 
                  $currency_id=$currency[0]['currency_id'];
                  $productdetails[0]['sale_price'] = $currency[0]['price'];
                }
                $currencydetails = Yii::app()->db->createCommand()
                    ->select("symbol")
                    ->from("currency")
                    ->where("id =:id" , array(":id"=>$currency_id))
                    ->queryRow();
                $symbol=$currencydetails['symbol'];
                $productdetails[0]['currency_symbol'] = $symbol;    
                $data['status'] = 200;
                $data['msg'] = 'OK';
                $data['details'] = @$productdetails;
                
            }else{
            $productdetails = Yii::app()->db->createCommand()
                    ->select("*")
                    ->from("pg_product")
                    ->where("studio_id=:studio_id AND is_deleted!=1 AND ((publish_date IS NULL) OR  (UNIX_TIMESTAMP(publish_date) = 0) OR (publish_date <=NOW()))", array(":studio_id" => $this->studio_id))
                    ->order("name DESC")
                    ->limit($limit, $offset)
                    ->queryAll();
            foreach ($productdetails as $key => $val) {
                $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val['id'])->queryAll();
                if (!empty($image)) {
                    $pgposter = $url . '/system/pgposters/' . $val['id'] . '/standard/' . $image[0]['name'];
                    $pgposteroriginal= $url . '/system/pgposters/' . $val['id'] . '/original/' . $image[0]['name'];
                    if (false === file_get_contents($pgposter)) {
                        $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                    }
                } else {
                   $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                }
                  if($val['currency_id']){
                 $currency_id=$val['currency_id'];
                }else{
                  $currency= Yii::app()->db->createCommand()
                    ->select("currency_id,price")
                    ->from("pg_multi_currency")
                    ->where("product_id =:product_id and studio_id=:studio_id" , array(":product_id"=>$val['id'],':studio_id'=>$this->studio_id))
                    ->queryAll(); 
                  $currency_id=$currency[0]['currency_id'];
                  $productdetails[$key]['sale_price'] = $currency[0]['price'];
                }
                $currencydetails = Yii::app()->db->createCommand()
                    ->select("symbol")
                    ->from("currency")
                    ->where("id =:id" , array(":id"=>$currency_id))
                    ->queryRow();
                $symbol=$currencydetails['symbol'];             
                $productdetails[$key]['currency_symbol'] = $symbol;
                $productdetails[$key]['poster'] = $pgposter;
                $productdetails[$key]['poster_original'] = $pgposteroriginal;
                $productdetails[$key]['cart_status'] = 0;
            }
            $data['code'] = 200;
            $data['msg'] = 'OK';
            $data['limit'] = @$page_size;
            $data['details'] = @$productdetails;
        }
        echo json_encode($data);
        exit;
    }
        /* 
         * This Function using for getting order details
         * We need to send User Id and Transaction Id for getting Order Details
         */
        public function actionGetOrderDetails() {
        $user_id = $_REQUEST['user_id'];
        if (!empty($user_id) && is_numeric($_REQUEST['user_id'])) {

            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();
            if ($userdetail) {
                $studio_id = $this->studio_id;
                $order_id = $_REQUEST['orderid'];
                if (isset($order_id) && is_numeric($order_id)) {
                    $order = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('pg_order')
                            ->where('id = "' . $order_id . '" and studio_id = "' . $studio_id . '" and buyer_id = "' . $user_id . '"');
                    $orderdetailsdata = $order->queryRow();
                    $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);

                    if (!empty($orderdetailsdata)) {
                        $con = Yii::app()->db;
                        $orderdetails = $con->createCommand('SELECT d.*,p.currency_id FROM pg_order_details d,pg_product p WHERE d.product_id=p.id AND d.order_id=' . $orderdetailsdata['id'])->queryAll();
                        //print_r($orderdetails);exit;
                        $productdetails[$key]['poster'] = $pgposter;
                        $productdetails[0]['poster_original'] = $pgposteroriginal;
                        $shiping = $con->createCommand('SELECT * FROM pg_shipping_address WHERE order_id = ' . $orderdetailsdata['id'])->queryRow();

                        $countryname = Countries::model()->findByAttributes(array('code' => $shiping['country']));
                        $shiping['country'] = isset($countryname->country) ? $countryname->country : $shiping['country'];
                        foreach ($orderdetails as $kdetail => $vdetail) {
                            if ($vdetail['currency_id'] == 0) {
                                $ord = $con->createCommand("SELECT currency_id FROM transactions WHERE id=(SELECT transactions_id FROM pg_order WHERE id='" . $vdetail['order_id'] . "')")->queryRow();
                                $orderdetails[$kdetail]['currency_id'] = $ord['currency_id'];
                            }
                            $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $vdetail['product_id'])->queryAll();
                            if (!empty($image)) {
                                $pgposter = $url . '/system/pgposters/' . $vdetail['product_id'] . '/standard/' . $image[0]['name'];
                                $pgposteroriginal = $url . '/system/pgposters/' . $vdetail['product_id'] . '/original/' . $image[0]['name'];
                                if (false === file_get_contents($pgposter)) {
                                    $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                                }
                            } else {
                                $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                            }
                            if ($vdetail['currency_id']) {
                                $currency_id = $vdetail['currency_id'];
                            } else {
                                $currency = Yii::app()->db->createCommand()
                                        ->select("currency_id,price")
                                        ->from("pg_multi_currency")
                                        ->where("product_id =:product_id and studio_id=:studio_id", array(":product_id" => $vdetail['product_id'], ':studio_id' => $this->studio_id))
                                        ->queryAll();
                                $currency_id = $currency[0]['currency_id'];
                            }
                            $currencydetails = Yii::app()->db->createCommand()
                                    ->select("symbol")
                                    ->from("currency")
                                    ->where("id =:id", array(":id" => $currency_id))
                                    ->queryRow();
                            $symbol = $currencydetails['symbol'];
                            $orderdetails[$kdetail]['currency_symbol'] = $symbol;
                            $orderdetails[$kdetail]['poster'] = $pgposter;
                            $orderdetails[$kdetail]['poster_original'] = $pgposteroriginal;
                        }

                        $userinfo = $con->createCommand('SELECT display_name,email FROM sdk_users WHERE id=' . $orderdetailsdata['buyer_id'])->queryRow();

                        $data['code'] = 200;
                        $data['order_details'] = $orderdetails;
                        $data['userinfo'] = $userinfo;
                        $data['shipping'] = $shiping;
                        $data['status'] = 'OK';
                        $data['msg'] = 'Order Details';
                    }
                }
            } else {
                $data['code'] = 411;
                $data['status'] = "Invalid Order Id";
                $data['msg'] = "Order Id is required";
            }
        } else {
            $data['code'] = 412;
            $data['status'] = "Invalid User Id";
            $data['msg'] = "Provided User Id is Required!";
        }
        echo json_encode($data);
        exit;
    }

    /*
         * This Function using for getting Purchase History details
         * We need to send User Id for getting Purchase History Details
        */ 
        public function actionGetPurchaseHistory(){
            $user_id = @$_REQUEST['user_id'];       
            $studio_id = $this->studio_id;
            if (isset($user_id) && is_numeric($user_id)) {
                $items_per_page = 5;
                $page_size = $limit = $items_per_page;
                $offset = 0;
                if (isset($page_number)) {
                    $offset = ($page_number - 1) * $limit;
                    $page_number = $page_number;
                } else {
                    $page_number = 1;
                }
              $transaction = Yii::app()->db->createCommand()
                   ->select("id,invoice_id")
                    ->from("transactions")
                    ->where("user_id=:user_id AND studio_id=:studio_id AND transaction_type=4", array(":studio_id" => $this->studio_id,"user_id"=>$user_id))
                    ->queryAll();
              
             foreach($transaction as $k=>$v){
                 $tid[]=$v['id'];
                 
             }
             $pgorder = Yii::app()->db->createCommand()
                   ->select("t.id,t.order_number,t.studio_id,t.total,t.order_status,t.grand_total,t.transactions_id,t.order_status,
                            t.created_date")
                    ->from("pg_order t")
                    ->where("t.transactions_id in (".implode(',',$tid).")")
                    ->queryAll();
             
             foreach($pgorder as $key=>$val){
                   $orderdetail = Yii::app()->db->createCommand()
                   ->select("od.order_id,od.product_id,od.quantity,od.price,od.name")
                    ->from("pg_order_details od")
                    ->where("od.order_id=".$val['id'])
                    ->queryAll();
                 
              $pgorder[$key]['order_details']= $orderdetail;
              $pgorder[$key]['invoice_id']=$val['transactions_id'];
             }
             
           
                if(count($pgorder)){
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['puchasehistory'] = $pgorder;  
                    $data['msg']='User purchase history';
                }else{
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['puchasehistory'] = array();
                    $data['msg']='No details available';
                }
            } else {
                $data['code'] = 449;
                $data['status'] = "Error";
                $data['msg'] = "User Authentication Error.";
            } 
            echo json_encode($data);
            exit;   
        }
     /**  @@author:suraja@muvi.com
     *  @@use: save PG address    
     *  @@inputs:user_id,studio_id,firstname,address,city,state,country,zip,phoneno,flag
     */
    
     public function actionSavePGAddress() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();
            if ($userdetail['email'] != '') {
                $ip = CHttpRequest::getUserHostAddress();
                
                    $new_address = new PGSavedAddress;
                    $new_address->first_name = $_REQUEST['firstname'];  
                    $new_address->email = $userdetail['email'];
                    $new_address->address = $_REQUEST['address'];
                    $new_address->city = $_REQUEST['city'];
                    $new_address->state = $_REQUEST['state'];
                    $new_address->country = $_REQUEST['country'];
                    $new_address->zip = $$_REQUEST['zip'];
                    $new_address->phone_number = $_REQUEST['phoneno'];
                    $new_address->user_id = $_REQUEST['user_id'];
                    $new_address->studio_id = $this->studio_id;
                    $new_address->created_date = new CDbExpression("NOW()");
                    $new_address->ip = $ip;
                    $new_address->save();
                    $save_id = $new_address->id;
                
                if ($save_id) {
                    $data['code'] = 200;
                    $data['status'] = "Ok";
                    $data['msg'] = "saved";
                }
            }
        }
        //  $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** @@author:suraja@muvi.com
     *  @@use: Get saved address list    
     *  @@inputs:user_id,studio_id
     */
    
    public function actionGetSavedPGAddress() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();
            if ($userdetail['email'] != '') {             
               $saveaddress = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('pg_saved_address')
                    ->where('user_id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']))->queryAll();
               foreach ($saveaddress as $key => $value) {
                    $countryname = Countries::model()->findByAttributes(array('code' => $value['country']));
                    $saveaddress[$key]['country'] = isset($countryname->country) ? $countryname->country : $value['country'];
                }
               
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['alladdress'] = $saveaddress;
                $data['msg'] = "addresslist";
            }
        }
       // $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
     /**@@author:suraja@muvi.com
     *  @@use: Get placed order status   
     *  @@inputs:user_id,studio_id,orderid
     */
    
    public function actionGetOrderStatus() {    
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if(isset($_REQUEST['orderid']) && is_numeric($_REQUEST['orderid'])) {
            $pgorder = new PGOrder();
            $status =$pgorder->getOrderStatus();
          
            $itemstatus = Yii::app()->db->createCommand()
                            ->select('id as item_id,order_id,item_status')
                            ->from('pg_order_details')
                            ->where('order_id=:orderid', array(':orderid' => $_REQUEST['orderid']))
                            ->queryAll();
            
            foreach ($itemstatus as $key => $value) {
                $itemstatus[$key]['item_status'] = $status[$value['item_status']];
            }
           
            $data['code'] = 200;
            $data['status'] = "Ok";
            $data['orderstatus'] = $itemstatus;
            $data['msg'] = "orderstatus";
        }
        echo json_encode($data);
        exit;
    }
    /**@@author:suraja@muvi.com
     *  @@use: Get placed order status   
     *  @@inputs:user_id,productid,flag(delete,empty),pricetype,quantity,personalization_id
     */
    
    public function actionAddToCart() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();
            if ($userdetail['email'] != '') {
                //get the product detailed status.
                if (isset($_REQUEST['productid']) && is_numeric($_REQUEST['productid'])) {
                    $productByCode = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('pg_product')
                            ->where('id =:id AND studio_id=:studio_id', array(':id' => $_REQUEST['productid'], ':studio_id' => $this->studio_id))
                            ->queryRow();
                    if ($productByCode["status"]==1){
                    if ($productByCode["id"]) {
                        if ($_REQUEST['flag'] == 'delete') { //delete a cart item  irrespective of Quantity       
                            if(!isset($_REQUEST['quantity'])){
                            $command = Yii::app()->db->createCommand()
                                    ->select('id,cart_item')
                                    ->from('pg_cart')
                                    ->where('user_id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
                            $usercartdetail = $command->queryRow();
                            if (!empty($usercartdetail)) {
                                $cartitems = json_decode($usercartdetail['cart_item'],true);
                                if (in_array('pg_' . $productByCode["id"], array_keys($cartitems))) {
                                    //echo "test";exit;
                                    foreach ($cartitems as $k => $v) {
                                        if ('pg_' . $productByCode["id"] == $k)
                                            unset($cartitems[$k]);
                                    }
                                }
                            }

                            $pgcart = PGCart::model()->findByPk($usercartdetail['id']);
                            $pgcart->cart_item = json_encode($cartitems);
                            $pgcart->ip = $ip;
                            $pgcart->save();
                            $data['code'] = 200;
                            $data['status'] = "Ok";
                            $data['msg'] = "Cart Updated";
                            }
                        } else {

                            $command = Yii::app()->db->createCommand()
                                    ->select('default_currency_id')
                                    ->from('studios')
                                    ->where('id = ' . $this->studio_id);
                            $studio = $command->queryRow();
                            $default_currency_id = $studio['default_currency_id'];
                            $tempprice = Yii::app()->common->getPGPrices($productByCode['id'], $default_currency_id);
                            if (!empty($tempprice)) {
                                $productByCode['sale_price'] = $tempprice['price'];
                                $productByCode['currency_id'] = $tempprice['currency_id'];
                            }

                            if ($_REQUEST["pricetype"] != 'freeoffer') {

                                $price = ($productByCode["is_preorder"] == 0) ? $productByCode["sale_price"] : self::getPreOrderprice($productByCode["id"], $this->studio_id);

                                if ($productByCode["is_free_offer"] == 1) {

                                    $price = "0.00";
                                }
                            } else {

                                $price = '0.00';
                            }

                            //$personalization_image_url = PGProduct::getPersonalizationImageUrl($productByCode['id']);
                            $itemArray = array('pg_' . $productByCode["id"] =>
                                array('name' => $productByCode["name"],
                                    'id' => $productByCode["id"],
                                    'quantity' => ($_REQUEST["quantity"]) ? $_REQUEST["quantity"] : 1,
                                    'price' => $price,
                                    'currency_id' => $productByCode["currency_id"],
                                    'sku' => $productByCode["sku"],
                                    'uniqid' => $productByCode["uniqid"],
                                    'permalink' => $productByCode["permalink"],
                                    'custom_fields' => $productByCode["custom_fields"],
                                    'is_free_offer' => $productByCode["is_free_offer"],
                                    'size' => $productByCode["size"],
                                    'personalization_id' => @$_REQUEST["personalization_id"]
                                    //'personalization_image' => json_encode($personalization_image_url)
                            ));


                            $ip = CHttpRequest::getUserHostAddress();
                            //check the product existance in cart
                            $command = Yii::app()->db->createCommand()
                                    ->select('id,cart_item')
                                    ->from('pg_cart')
                                    ->where('user_id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
                            $usercartdetail = $command->queryRow();
                            $_REQUEST["quantity"] = ($_REQUEST["quantity"]) ? $_REQUEST["quantity"] : 1;
                            if (!empty($usercartdetail)) {
                                $cartitems = json_decode($usercartdetail['cart_item'], true);

                                if (in_array('pg_' . $productByCode["id"], array_keys($cartitems))) {

                                    foreach ($cartitems as $k => $v) {
                                        if ('pg_' . $productByCode["id"] == $k) {
                                            //  echo   (int)$cartitems[$k]["price"] ; echo ((int)$price * ($_REQUEST["quantity"])?$_REQUEST["quantity"]:1);
                                            $cartitems[$k]["quantity"] =  $_REQUEST["quantity"];
                                            $cartitems[$k]["price"] = ((int) $price * $_REQUEST["quantity"]);
                                        }
                                    }
                                } else {
                                    $cartitems = array_merge($cartitems, $itemArray);
                                }

                                $pgcart = PGCart::model()->findByPk($usercartdetail['id']);
                                $pgcart->cart_item = json_encode($cartitems);
                                $pgcart->ip = $ip;
                                $pgcart->save();
                            } else {
                                $cartitems = $itemArray;                           
                                $pgcart = new PGCart();
                                $pgcart->studio_id = $this->studio_id;
                                $pgcart->user_id = $_REQUEST["user_id"];
                                $pgcart->session_id = session_id();
                                $pgcart->created_date = gmdate('Y-m-d H:i:s'); //new CDbExpression("NOW()");
                                $pgcart->cart_item = json_encode($cartitems);
                                $pgcart->ip = CHttpRequest::getUserHostAddress();
                                $pgcart->save();
                            }
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "Cart Updated";
                        }
                        
                    } else {
                        
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "Cart is Empty";
                        }
                } else {
                        if ($_REQUEST['flag'] == 'delete') { //delete a cart item  irrespective of Quantity       
                            if(!isset($_REQUEST['quantity'])){
                            $command = Yii::app()->db->createCommand()
                                    ->select('id,cart_item')
                                    ->from('pg_cart')
                                    ->where('user_id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
                            $usercartdetail = $command->queryRow();
                            if (!empty($usercartdetail)) {
                                $cartitems = json_decode($usercartdetail['cart_item'],true);
                                if (in_array('pg_' . $productByCode["id"], array_keys($cartitems))) {
                                    //echo "test";exit;
                                    foreach ($cartitems as $k => $v) {
                                        if ('pg_' . $productByCode["id"] == $k)
                                            unset($cartitems[$k]);
                                    }
                                }
                            }

                            $pgcart = PGCart::model()->findByPk($usercartdetail['id']);
                            $pgcart->cart_item = json_encode($cartitems);
                            $pgcart->ip = $ip;
                            $pgcart->save();
                            $data['code'] = 200;
                            $data['status'] = "Ok";
                            $data['msg'] = "Cart Updated";
                            }
                        }else{
                        $data['code'] = 470;
                        $data['status'] = "Failure";
                        $data['msg'] = "Out Of Stock OR Inactive product";
                        }
                     }
                }
            }
        }
        echo json_encode($data);
        exit;
    }

    public function actionGetFromCart() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();
            if ($userdetail['email'] != '') {
                $cartarr = Yii::app()->db->createCommand("SELECT * FROM pg_cart WHERE studio_id = '{$this->studio_id}' AND user_id = '{$_REQUEST['user_id']}'")->queryRow();
                $cartdetails['id'] = $cartarr['id'];
                $cartdetails['studio_id'] = $cartarr['studio_id'];
                $cartdetails['user_id'] = $cartarr['user_id'];
                $cartdetails['session_id'] = $cartarr['session_id'];

                $cart_item = array();
                $cart_item = json_decode($cartarr['cart_item'], true);
                $quantity = 0;
                foreach ($cart_item as $key1 => $val1) {
                    $productByCode = Yii::app()->db->createCommand()
                            ->select('id,status,currency_id')
                            ->from('pg_product')
                            ->where('id =:id', array(':id' => $val1['id']))
                            ->queryRow();
                    $val1['status'] = $productByCode['status'];

                    $quantity = $quantity + $val1['quantity'];
                    $url = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
                    $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val1['id'])->queryAll();
                    if (!empty($image)) {
                        $pgposter = $url . '/system/pgposters/' . $val1['id'] . '/standard/' . $image[0]['name'];
                        $pgposteroriginal = $url . '/system/pgposters/' . $val1['id'] . '/original/' . $image[0]['name'];
                        if (false === file_get_contents($pgposter)) {
                            $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                        }
                    } else {
                        $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                    }

                    if ($productByCode['currency_id']) {
                        $currency_id = $productByCode['currency_id'];
                    } else {
                        $currency = Yii::app()->db->createCommand()
                                ->select("currency_id,price")
                                ->from("pg_multi_currency")
                                ->where("product_id =:product_id and studio_id=:studio_id", array(":product_id" => $productByCode['id'], ':studio_id' => $this->studio_id))
                                ->queryAll();
                        $currency_id = $currency[0]['currency_id'];
                    }
                    $currencydetails = Yii::app()->db->createCommand()
                            ->select("symbol")
                            ->from("currency")
                            ->where("id =:id", array(":id" => $currency_id))
                            ->queryRow();
                    $symbol = $currencydetails['symbol'];
                    $val1['currency_symbol'] = $symbol;
                    $val1['poster'] = $pgposter;
                    $val1['poster_original'] = $pgposteroriginal;
                    $cartdetails['cart_item'][] = $val1;
                }
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['cartitems'] = $cartdetails;
                $data['itemcount'] = $quantity;
                $data['items'] = count($cartdetails['cart_item']);
                $data['msg'] = "Cart Item Details";
            }
        }
        echo json_encode($data);
        exit;
    }

    public function actionDeleteAddress() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (!empty($_REQUEST["addressid"]) && is_numeric($_REQUEST["addressid"])) {
            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('pg_saved_address')
                    ->where('id=:addressid', array(':addressid' => $_REQUEST["addressid"]));
            $addressdetail = $command->queryRow();
            if ($addressdetail['id'] != '') {
                $command = Yii::app()->db->createCommand();
                $command->delete('pg_saved_address', 'id=:addressid', array(':addressid' => $_REQUEST["addressid"]));
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['msg'] = "Address deleted successfully.";
            }
        }
        echo json_encode($data);
        exit;
    }

    public function actionEditAddress() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (!empty($_REQUEST["addressid"]) && is_numeric($_REQUEST["addressid"])) {
            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('pg_saved_address')
                    ->where('id=:addressid', array(':addressid' => $_REQUEST["addressid"]));
            $addressdetail = $command->queryRow();
            
            if ($addressdetail['id'] != '') {
                $new_address = PGSavedAddress::model()->findByPk($addressdetail['id']);
                $new_address->first_name = $_REQUEST['firstname'];
                $new_address->address = $_REQUEST['address'];
                $new_address->city = $_REQUEST['city'];
                $new_address->state = $_REQUEST['state'];
                $new_address->country = $_REQUEST['country'];
                $new_address->zip = $$_REQUEST['zip'];
                $new_address->phone_number = $_REQUEST['phoneno'];
                $new_address->save();
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['msg'] = "Address Updated successfully.";
            }
        }
        echo json_encode($data);
        exit;
    }
    function getPreOrderprice($product_id,$studio_id) {              
        if (intval($product_id)) {                     
            $default_currency_id = $this->studio->default_currency_id;         
            $preordercate = Yii::app()->db->createCommand("SELECT preorder_id FROM pg_preorder_items WHERE item_id=".$product_id)->queryROW();
            $prices = Yii::app()->db->createCommand("SELECT * FROM pg_preorder_price WHERE preorder_id=".$preordercate['preorder_id']." AND currency_id=".$default_currency_id)->queryROW();  
            return $prices['price_for_unsubscribe'];
        }
    }
    /**@@author:suraja@muvi.com
     *  @@use: create the order 
     *  @@inputs:user_id,address,transactionsid,total,paymenttype,shippingcost,discount,discounttype,
     *           amount,couponcode,hearsource,cartid
     */
    public function actionCreateOrder() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();
            $ip = CHttpRequest::getUserHostAddress();
            if ($userdetail['email'] != '') {
                
                $cartdetails = Yii::app()->db->createCommand()
                        ->select("*")
                        ->from("pg_cart")
                        ->where("id =:id )", array(":id" => $_REQUEST['cartid']))
                        ->queryRow();
                if($cartdetails['id']) {
                $ordernumber = date('dmy');
                $pgorder = new PGOrder();
                $pgorder->studio_id = $this->studio_id;
                $pgorder->buyer_id = $_REQUEST['user_id'];
                $pgorder->shipping_address = json_encode($_REQUEST['address']);
                $pgorder->transactions_id = $_REQUEST['transactionsid'];
                $pgorder->tax = '';
                $pgorder->order_status = 'pending';
                $pgorder->payment_status = '';
                $pgorder->total = $_REQUEST['total'];
                $pgorder->payment_type = $_REQUEST['paymenttype'];
                $pgorder->shipping_cost = $_REQUEST['shippingcost'];
                $pgorder->discount = $_REQUEST['discount'];
                $pgorder->discount_type = $_REQUEST['discounttype'];
                $pgorder->grand_total = $_REQUEST['amount'];
                $pgorder->coupon_code = $_REQUEST['couponcode'];
                $pgorder->hear_source = $_REQUEST['hearsource'];
                $pgorder->created_date = new CDbExpression("NOW()");
                $pgorder->ip = $ip;
                if ($pgorder->save())
                    $last_inserted_id = $pgorder->id;
                $pgorder->order_number = $ordernumber . $last_inserted_id;
                $pgorder->save();
                $orderid = $last_inserted_id;
                if((isset($_REQUEST['coupon_code']) && $_REQUEST['coupon_code'] != '')){
                $couponDetails = Coupon::model()->findByAttributes(array('coupon_code' => $_REQUEST['couponcode']));
                if (isset($couponDetails) && !empty($couponDetails)) {
                    if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                        $qry = "UPDATE coupon SET used_by='" . $couponDetails->used_by . "," . $_REQUEST['user_id'] . "',used_date = NOW() WHERE coupon_code='" . $_REQUEST['couponcode'] . "'";
                    } else {
                        $qry = "UPDATE coupon SET used_by=" . $_REQUEST['user_id'] . ",used_date = NOW() WHERE coupon_code='" . $_REQUEST['couponcode'] . "'";
                    }
                    Yii::app()->db->createCommand($qry)->execute();
                }
                }
                if( isset($orderid) && $orderid!='' && $_REQUEST['address']!=''){
                $pgshippingaddr = new PGShippingAddress();
                $pgshippingaddr->insertAddress($this->studio->id, $_REQUEST['user_id'], $orderid, $_REQUEST['address'], $ip);
                }

                if($orderid){
                $cartdetails = Yii::app()->db->createCommand()
                        ->select("*")
                        ->from("pg_cart")
                        ->where("id =:id )", array(":id" => $_REQUEST['cartid']))
                        ->queryRow();
                
                    $cart_item = json_decode($cartdetails['cart_item']);
                    $pgorderdetails = new PGOrderDetails();
                    foreach ($cart_item as $key => $item) {
                        $pgorderdetails->order_id = $orderid;
                        $pgorderdetails->product_id = $item['id'];
                        $pgorderdetails->quantity = $item['quantity'];
                        $pgorderdetails->price = $item['price'];
                        $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $item['id']));
                        $pgorderdetails->name = $item['name'];
                        $pgorderdetails->discount = $pgproduct['discount'];
                        $pgorderdetails->tax = '';
                        $pgorderdetails->description = $pgproduct['description'];
                        $pgproductimage = PGProductImage::model()->find('product_id=:id AND feature=1', array(':id' => $item['id']));
                        $pgorderdetails->image_url = $pgproductimage['name'];
                        $pgorderdetails->sub_total = ($item['price'] * $item['quantity']); //(price*qnt)+tax+shipping-discount
                        $pgorderdetails->item_status = 1; //pending status
                        $pgorderdetails->personalization_id = $item['personalization_id'];
                        $pgorderdetails->isNewRecord = true;
                        $pgorderdetails->primaryKey = NULL;
                        $pgorderdetails->save();
                    }
                }
                //clean the cart table after order placed
                if($last_inserted_id){
                $command = Yii::app()->db->createCommand();
                $command->delete('pg_cart', 'user_id=:user_id AND studio_id=:studio_id', array(':user_id' => $_REQUEST["user_id"],':studio_id' => $this->studio_id)); 
                }
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['msg'] = "Order Placed Successfully";
                
               }
            }
        }
        echo json_encode($data);
        exit;
    }
    
    public function actiongetShippingDetails() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        $studio_id = $this->studio_id;
     if ($_REQUEST['user_id']) {
        $shippingmethods = Yii::app()->db->createCommand()
                ->select("*")
                ->from("pg_shipping_method")
                ->where("studio_id =:studio_id AND is_enabled=1", array(":studio_id" => $studio_id))
                ->queryAll();
        $arr=array();
        $command = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('sdk_users')
                    ->where('id=:user_id and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']));
            $userdetail = $command->queryRow();

            if ($userdetail['email'] != '') {
                //get pg_cart items
                if ($_REQUEST['shipping_method']) {
                    $cartdetails = Yii::app()->db->createCommand()
                            ->select("*")
                            ->from("pg_cart")
                            ->where("studio_id =:studio_id AND user_id=:user_id", array(":studio_id" => $studio_id, ":user_id" => $_REQUEST['user_id']))
                            ->queryRow();

                    $cart_item = json_decode($cartdetails['cart_item'], true);
                    //$studio_id,$ship_address,$ship_item,$ship_method,$currency

                    foreach ($cart_item as $key => $item) {
                        $currency_array[] = $item['currency_id'];
                        $itemsize[] = $item['size'];
                    }
                    $currency_array = array_unique($currency_array);
                    $size = array_unique($itemsize);
                    $country = $_REQUEST['country'];
                    if($country!=''){
                    $countrycode = Yii::app()->db->createCommand()
                            ->select("code")
                            ->from("countries")
                            ->where("country =:country", array(":country" => $country))
                            ->queryRow();

                    if (in_array("large", $size)) {
                        $selected_size = "large";
                    } else if (in_array("medium", $size)) {
                        $selected_size = "medium";
                    } else if (in_array("small", $size)) {
                        $selected_size = "small";
                    }
                    $scost = PGShippinCost::model()->find(array('select' => 'currency,shipping_cost', 'condition' => 'studio_id=:studio_id and country=:country and method=:method and size=:size and currency=:currency and is_deleated=0', 'params' => array(':studio_id' => $studio_id, ':country' => $countrycode['code'], ':method' => $_REQUEST['shipping_method'], ':size' => $selected_size, ':currency' => $currency_array[0])));
                    if (!empty($scost)) {
                        $rev_shipping_cost['currency'] = $scost->currency;
                        $rev_shipping_cost['shipping_cost'] = $scost->shipping_cost;
                    } else {
                        $rev_shipping_cost = array();
                    }
                    $pgorder = new PGOrder();
                    $minimum_shipping_cost = PGMinimumOrderFreeShipping::model()->find(array('select' => 'minimum_order_free_shipping', 'condition' => 'studio_id=:studio_id and currency_id=:currency_id', 'params' => array(':studio_id' => $this->studio_id, ':currency_id' => $currency_array[0])));

                    if ($rev_shipping_cost['shipping_cost']) {
                        $code = Currency::model()->find('id=:id', array(':id' => $rev_shipping_cost['currency']));
                        $arr['error'] = 0;
                        $arr['shipcost'] = $rev_shipping_cost['shipping_cost'];
                        $arr['ship_currency'] = $rev_shipping_cost['currency'];
                        $arr['ship_symbol'] = $code->symbol;
                        $arr['minimum_order_free_shipping'] = $minimum_shipping_cost->minimum_order_free_shipping;
                    } else {
                        $currency_id = $currency_array[0];
                        $default_shipping_cost = PGDefaultShippingCost::model()->find(array('select' => 'default_shipping_cost', 'condition' => 'studio_id=:studio_id and currency_id=:currency_id', 'params' => array(':studio_id' => $this->studio_id, ':currency_id' => $currency_id)));
                        $code = Currency::model()->findByPk($currency_id);
                        $arr['error'] = 0;
                        $arr['shipcost'] = empty($default_shipping_cost) ? '0.00' : $default_shipping_cost->default_shipping_cost;
                        $arr['ship_currency'] = $currency_id;
                        $arr['ship_symbol'] = $code->symbol;
                        $arr['minimum_order_free_shipping'] = $minimum_shipping_cost->minimum_order_free_shipping;
                     }
                    $data['code'] = 200;
                    $data['status'] = "Ok";
                    $data['shipping_details'] = $arr;
                    $data['methods'] = $shippingmethods;
                    $data['msg'] = "Shipping details";
                    }
                } 
            }
        
        }
     else
     {
         $arr=array();
         $shippingmethods = Yii::app()->db->createCommand()
                ->select("*")
                ->from("pg_shipping_method")
                ->where("studio_id =:studio_id AND is_enabled=1", array(":studio_id" => $studio_id))
                ->queryAll();
     
        $data['code'] = 200;
        $data['status'] = "Ok";
        $data['shipping_details'] = $arr;
        $data['methods'] = $shippingmethods;
        $data['msg'] = "Shipping details";
     }
        echo json_encode($data);
        exit;
    }
   public function actionsearchPhysicalData() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['q']) && $_REQUEST['q']) {
            $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);           
            $q = strtolower($_REQUEST['q']);
            $searched_product = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('pg_product')
                    ->where('studio_id=:studio_id AND ( LOWER(name) LIKE  :search_string OR sku LIKE :search_string', array(':studio_id' => $this->studio_id, ':search_string' => "%$q%"))
                    ->queryAll();
           // print_r($searched_product);exit;
            foreach($searched_product as $key=>$val){
            $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val['id'])->queryAll();
                            if (!empty($image)) {
                                $pgposter = $url . '/system/pgposters/' . $val['id'] . '/standard/' . $image[0]['name'];
                                $pgposteroriginal = $url . '/system/pgposters/' . $val['id'] . '/original/' . $image[0]['name'];
                                if (false === file_get_contents($pgposter)) {
                                    $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                                }
                            } else {
                                $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                            }   
            $searched_product[$key]['standard']=$pgposter;
            $searched_product[$key]['original']=$pgposteroriginal;
            }
            
          }
        
        $data['code'] = 200;
        $data['status'] = "Ok";
        $data['searched_items'] = $searched_product;
        $data['msg'] = "searched Items";
       echo json_encode($data);
       exit; 
    }
     
    public function actionsearchsolrData()
    {
     $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['q']) && $_REQUEST['q']) {
        
        }   
    }
	function actionGetFilterList(){
		$studio_id = $this->studio_id;
		$lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
        $translate = $this->getTransData($lang_code, $studio_id);
		$sortby = array(
					array("name"=>$translate['atoz'],"permalink"=>'sortasc'),
					array("name"=>$translate['ztoa'],"permalink"=>'sortdesc'),
					array("name"=>$translate['price_lowtohigh'],"permalink"=>'priceasc'),
					array("name"=>$translate['price_hightolow'],"permalink"=>'pricedesc'),
					array("name"=>$translate['releasedate'],"permalink"=>'releasedate')
				);
		$filterby = array(
					array("name"=>$translate['exclude_out_of_stock'],"permalink"=>'exclude_out_of_stock')
				);
        $data['sort_by']=$sortby;
		$data['filter_by']=$filterby;
		$data['code'] = 200;
		$data['status'] = "OK";
        echo json_encode($data);
        exit;
	}
}
?>

