<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Yii::import('ext.EGeoIP');

class Billing extends AppComponent {
    
    function setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $couponCode, $isadv = 0, $gateway_code = '') {
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) ? Yii::app()->user->id : @$data['user_id'];
        $plan_id = $plan->id;
        $ip_address = CHttpRequest::getUserHostAddress();
        //Save ppv subscription detail
        $ppvsubscription = new PpvSubscription;
        $ppvsubscription->user_id = $user_id;
        $ppvsubscription->studio_id = $studio_id;
        $ppvsubscription->card_id = @$data['card_id'];
        $ppvsubscription->ppv_plan_id = $plan_id;
        $ppvsubscription->studio_payment_gateway_id = Yii::app()->controller->PAYMENT_GATEWAY_ID[$gateway_code];
        $ppvsubscription->currency_id = @$data['currency_id'];
        $ppvsubscription->movie_id = (isset($data['is_bundle']) && intval($data['is_bundle'])) ? 0 : $video_id;
        $ppvsubscription->season_id = @$data['season_id'];
        $ppvsubscription->episode_id = @$data['episode_id'];
        $ppvsubscription->is_advance_purchase = @$isadv;
        $ppvsubscription->token = Yii::app()->common->generateUniqNumber();
        $ppvsubscription->card_holder_name = @$data['card_name'];
        $ppvsubscription->card_last_fourdigit = @$data['card_last_fourdigit'];
        $ppvsubscription->card_type = @$data['card_type'];
        $ppvsubscription->exp_month = @$data['exp_month'];
        $ppvsubscription->exp_year = @$data['exp_year'];
        $ppvsubscription->amount = (isset($data['amount'])) ? $data['amount'] : 0;
        if (trim($start_date)) {
        $ppvsubscription->start_date = $start_date;
		}
		if (trim($end_date)) {
        $ppvsubscription->end_date = $end_date;
		}
        $ppvsubscription->coupon_code = @$couponCode;
        $ppvsubscription->status = 1;
        $ppvsubscription->ip = $ip_address;
        $ppvsubscription->created_by = $user_id;
        $ppvsubscription->created_date = new CDbExpression("NOW()");
        $ppvsubscription->is_ppv_bundle = @$data['is_bundle'];
        $ppvsubscription->timeframe_id = @$data['timeframe_id'];
        $ppvsubscription->view_restriction = @$data['view_restriction'];
        $ppvsubscription->watch_period = @$data['watch_period'];
        $ppvsubscription->access_period = @$data['access_period'];
        $ppvsubscription->save();
        $ppv_subscription_id = $ppvsubscription->id;

        $couponDetails = Coupon::model()->findByAttributes(array('coupon_code' => @$couponCode));
        if (isset($couponDetails) && !empty($couponDetails)) {
            if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                $qry = "UPDATE coupon SET used_by='" . $couponDetails->used_by . "," . $user_id . "',used_date = NOW() WHERE coupon_code='" . $couponCode . "'";
            } else {
                $qry = "UPDATE coupon SET used_by=" . $user_id . ",used_date = NOW() WHERE coupon_code='" . $couponCode . "'";
            }
            Yii::app()->db->createCommand($qry)->execute();
        }
        return $ppv_subscription_id;
    }

    /* function setPpvSubscriptionContents($plan, $ppv_subscription_id) {
      if (isset($plan['ppvadvancecontent']) && !empty($plan['ppvadvancecontent'])) {
      foreach ($plan['ppvadvancecontent'] as $key => $value) {
      $PpvSubscriptionContent = New PpvSubscriptionContent();
      $PpvSubscriptionContent->ppv_subscription_id = $ppv_subscription_id;
      $PpvSubscriptionContent->movie_id = $value->content_id;
      $PpvSubscriptionContent->save();
      }
      }

      return true;
      } */
    
  function setUserSubscriptionBundles($plan, $data, $video_id, $start_date, $end_date, $couponCode, $isadv = 0, $gateway_code = '') {
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) ? Yii::app()->user->id : @$data['user_id'];
        $plan_id = $plan->id;
        $ip_address = CHttpRequest::getUserHostAddress();
        //Save ppv subscription detail
        $card_id=$data['card_id'];
        $payment_gateway_id = $data['PAYMENT_GATEWAY_ID'];
        $currency_id = $data['currency_id'];
        $profile_id = $data['profile_id'];
        $plan_price=$data['amount'];
         $usModel = new UserSubscription;
            $usModel->studio_id = $studio_id;
            $usModel->user_id = $user_id;
            $usModel->plan_id = $plan_id;
            $usModel->card_id = $card_id;
            $usModel->studio_payment_gateway_id = $payment_gateway_id;
            $usModel->currency_id = $currency_id;
            $usModel->profile_id = $profile_id;
            $usModel->amount = $plan_price;
            $usModel->start_date = trim($start_date);
            $usModel->end_date = trim($end_date);
            $usModel->status = 1;
            $usModel->is_success = 1;
            $usModel->device_type = 1;
            $usModel->coupon_code = $couponCode;
            $usModel->ip = $ip_address;
            $usModel->created_by = $user_id;
            $usModel->created_date = new CDbExpression("NOW()");
            $usModel->is_subscription_bundle = $data['is_subscription_bundles'];
            $usModel->discount_amount = $data['discount_amount'];
            $usModel->save();
            $user_subscriptionbundles_id = $usModel->id;
        
      
             $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
        if (isset($couponDetails) && !empty($couponDetails)) {
                $command = Yii::app()->db->createCommand();
            if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                    $qry = $command->update('coupon_subscription', array('used_by'=>$couponDetails->used_by.",".$user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code'=>$couponCode));
            } else {
                    $qry = $command->update('coupon_subscription', array('used_by'=>$user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code'=>$couponCode));
            }
            }
        return $user_subscriptionbundles_id;
    }

    function setVoucherSubscription($data = array()){
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = @$data['user_id'];
        $voucher = @$data['voucher_id'];
        $isadv = @$data['isadv'];
        $content = @$data['content_id'];
        $start_date = @$data['start_date'];
        $end_date = @$data['end_date'];
        $guestId = @$data['guestId'];
        
        $voucherSub = new PpvSubscription;
        $voucherSub->studio_id = $studio_id;
        if($guestId > 0){
            $voucherSub->user_id = $guestId;
            $voucherSub->is_register_user = 0;
        }else{
        $voucherSub->user_id = $user_id;
            if(!intval($user_id)){
                $voucherSub->is_register_user = 0;
            }
        }
        $voucherSub->ppv_plan_id = $voucher;
        $voucherSub->is_voucher = 1;
        $voucher_code = CouponOnly::model()->getVoucherCode($voucher,$studio_id);
        $content_type = substr_count($content,":");
        
        if($content_type > 0){
            $content_id = explode(":", $content);
            
            if(count($content_id)==3){
                $voucherSub->movie_id = $content_id[0];
                $voucherSub->season_id = $content_id[1];
                $voucherSub->episode_id = $content_id[2];
                $video_id  = $content_id[0];
            }elseif(count($content_id)==2){
                $voucherSub->movie_id = $content_id[0];
                $voucherSub->season_id = $content_id[1];
                $video_id  = $content_id[0];
            }
        }else{
            $voucherSub->movie_id = $content;
            $video_id  = $content;
        }
        $voucherSub->created_date = date("Y-m-d H:i:s");
		if (trim($start_date)) {
        $voucherSub->start_date = $start_date;
		}
		if (trim($end_date)) {
        $voucherSub->end_date = $end_date;
		}
        $voucherSub->view_restriction = @$data['view_restriction'];
        $voucherSub->watch_period = @$data['watch_period'];
        $voucherSub->access_period = @$data['access_period'];
        $voucherSub->save();
        
        $ppv_subscription_id = $voucherSubId = $voucherSub->id;
        $temp_plan['id'] = $voucher;
        $plan = (object)$temp_plan;
        $trans_data['transaction_status'] = 'success';
        $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
        $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
        $trans_data['transaction_type'] = 6;
        $transaction_id = self::setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, '', intval($isadv));
        
        $VideoName = CouponOnly::model()->getContentInfo($content,$isadv);
        
        if($voucherSubId){
            $ppv_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'voucher', '', $VideoName, '', '', $voucher_code, '', Yii::app()->controller->language_code);
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('voucher', $studio_id);
            if ($isEmailToStudio) {
                Yii::app()->controller->sendStudioAdminEmails($studio_id, 'admin_voucher_content', $user_id, $VideoName,'','','',$voucher_code);
            }
        }
        return $voucherSubId;
    }
    
    function episodeContent($movie_id,$series){
        $studio_id = Yii::app()->common->getStudiosId();
        $epsql = "SELECT * FROM movie_streams WHERE studio_id=" . $studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series . " ORDER BY episode_number ASC";
        $epdata = Yii::app()->db->createCommand($epsql)->queryAll();
        return $epdata;
    }
    
    function multipartPpvSubscription($movie_id){
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $today = date("Y-m-d H:i:s");
        $data = array();
        
        $ppv_sql_season = "SELECT * FROM ppv_subscriptions WHERE user_id=".$user_id." AND studio_id=".$studio_id." AND start_date <= '" . $today . "' AND movie_id=".$movie_id." AND (season_id!=0 AND episode_id=0)";
        $data_ppv_season = Yii::app()->db->createCommand($ppv_sql_season)->queryAll();

        $arr_season = array();
        for($s=0;$s<count($data_ppv_season);$s++){
            
            $purchased_date = strtotime($data_ppv_season[$s]['created_date']);
            $access_period = @$data_ppv_season[$s]['access_period'];
            
            if (trim($access_period)) {
                $content_access_period = strtotime("+{$access_period}", $purchased_date);
                if ($content_access_period >= strtotime(date('Y-m-d h:i:s'))) {
                array_push($arr_season,$data_ppv_season[$s]['season_id']);
        }
            } else {
                array_push($arr_season,$data_ppv_season[$s]['season_id']);
            }
        }
        $data['arr_season'] = $arr_season;
        $arr_ep_season = array();
        $arr_ep_episode = array();
        $ppv_sql_episode = "SELECT * FROM ppv_subscriptions WHERE user_id=".$user_id." AND studio_id=".$studio_id." AND start_date <= '" . $today . "' AND movie_id=".$movie_id." AND season_id!=0 AND episode_id!=0";
        $data_ppv_episode = Yii::app()->db->createCommand($ppv_sql_episode)->queryAll();

        for($e=0;$e<count($data_ppv_episode);$e++){
            $watch_period = @$data_ppv_episode[$e]['watch_period'];
            if (trim($watch_period)) {
                $arg = $data_ppv_episode[$e];
                $arg['stream_id'] = @$data_ppv_episode[$e]['episode_id'];
                $first_watched_date = VideoLogs::model()->getFirstWatchedDateAndTimeOfContent($arg);

                if (isset($first_watched_date) && trim($first_watched_date) && date('Y-m-d', $first_watched_date) != '0000-00-00' && date('Y-m-d', $first_watched_date) != '1970-01-01') {
                    $content_watch_period = strtotime("+{$watch_period}", $first_watched_date);
                    if ($content_watch_period >= strtotime(date('Y-m-d h:i:s'))) {
                array_push($arr_ep_season, $data_ppv_episode[$e]['season_id']);
                array_push($arr_ep_episode, $data_ppv_episode[$e]['episode_id']);
        }
                }
            } else {
                array_push($arr_ep_season, $data_ppv_episode[$e]['season_id']);
                array_push($arr_ep_episode, $data_ppv_episode[$e]['episode_id']);
            }
        }
        $data['arr_ep_season'] = $arr_ep_season;
        $data['arr_ep_episode'] = $arr_ep_episode;
        
        return $data;
    }
           
    function checkSubscriptionForSeason($movie_id, $season_id){
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $today = date("Y-m-d H:i:s");
        
        $ep_subscription = "SELECT COUNT(id) as sub_id, created_date FROM ppv_subscriptions WHERE user_id=".$user_id." AND studio_id=".$studio_id." AND start_date <= '" . $today . "' AND movie_id=".$movie_id." AND season_id=".$season_id." AND episode_id!=0" ;
        $num_subscription = Yii::app()->db->createCommand($ep_subscription)->queryRow();

        $ep_stream = "SELECT COUNT(id) as str_id FROM movie_streams WHERE studio_id=".$studio_id." AND movie_id=".$movie_id." AND series_number=".$season_id." AND episode_number IS NOT NULL" ;
        $num_stream = Yii::app()->db->createCommand($ep_stream)->queryRow();
        
        $data = FALSE;
        
        if($num_subscription['sub_id']!=$num_stream['str_id']){
            $data = TRUE;
        }
        
        return $data;
    }
    
    function setCardInfo($data = array()) {
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) ? Yii::app()->user->id : @$data['user_id'];
        $ip_address = CHttpRequest::getUserHostAddress();

        $sciModel = New SdkCardInfos;
        $sciModel->studio_id = $studio_id;
        $sciModel->user_id = $user_id;
        $sciModel->gateway_id = Yii::app()->controller->GATEWAY_ID[$data['gateway_code']];
        $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
        $sciModel->card_name = $data['card_name_optional'];
        $sciModel->card_holder_name = $data['card_holder_name'];
        $sciModel->exp_month = $data['exp_month'];
        $sciModel->exp_year = $data['exp_year'];
        $sciModel->card_last_fourdigit = $data['card_last_fourdigit'];
        $sciModel->auth_num = @$data['auth_num'];
        $sciModel->token = @$data['token'];
        $sciModel->profile_id = @$data['profile_id'];
        $sciModel->card_type = $data['card_type'];
        $sciModel->reference_no = @$data['reference_no'];
        $sciModel->response_text = $data['response_text'];
        $sciModel->status = $data['status'];
        $sciModel->is_cancelled = 1;
        $sciModel->ip = $ip_address;
        $sciModel->created = new CDbExpression("NOW()");
        $sciModel->save();
        $sdk_card_info_id = $sciModel->id;

        return $sdk_card_info_id;
    }

    function setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code = '', $isadv = 0) {
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) ? Yii::app()->user->id : @$data['user_id'];
        $plan_id = $plan->id;
        $ip_address = CHttpRequest::getUserHostAddress();

        //Save a transaction detail
        $transaction = new Transaction;
        $transaction->user_id = $user_id;
        $transaction->studio_id = $studio_id;
        $transaction->plan_id = $plan_id;
        $transaction->currency_id = @$data['currency_id'];
        $transaction->transaction_date = new CDbExpression("NOW()");
        $transaction->payment_method = @Yii::app()->controller->PAYMENT_GATEWAY[$gateway_code];
        $transaction->transaction_status = $trans_data['transaction_status'];
        $transaction->invoice_id = @$trans_data['invoice_id'];
        $transaction->order_number = $trans_data['order_number'];
        $transaction->amount = @$trans_data['amount'];
        $transaction->dollar_amount = @$trans_data['dollar_amount'];
        $transaction->response_text = @$trans_data['response_text'];
        $transaction->movie_id = $video_id;
        $transaction->ip = $ip_address;
        $transaction->created_date = new CDbExpression("NOW()");
        if (isset($data['is_subscriptionbundle']) && intval($data['is_subscriptionbundle'])) {
          $transaction->subscription_id = $ppv_subscription_id;  
        }else{
          $transaction->ppv_subscription_id = $ppv_subscription_id;    
        }
        $transaction_type = 2;
        if (isset($data['is_bundle']) && intval($data['is_bundle'])) {
            $transaction_type = 5;
        } else if (isset($isadv) && intval($isadv)) {
            $transaction_type = 3;
        }else if(isset($trans_data) && intval($trans_data['transaction_type'])) {
            $transaction_type = $trans_data['transaction_type'];
        }else if (isset($data['is_subscription_bundles']) && intval($data['is_subscription_bundles'])) {
            $transaction_type = 7;
        }else if($data['is_subscription_bundles']==0 && isset($data['is_subscription_bundles'])){
            $transaction_type = 1;
        }

        $transaction->transaction_type = $transaction_type;
        
        if($gateway_code == 'paypalpro'){
            $transaction->payer_id = $trans_data['PAYERID'];
            $transaction->fullname = $trans_data['SHIPTONAME'];
            $transaction->address1 = $trans_data['SHIPTOSTREET'];
            $transaction->city = $trans_data['SHIPTOCITY'];
            $transaction->state = $trans_data['SHIPTOSTATE'];
            $transaction->country = $trans_data['COUNTRYCODE'];
            $transaction->zip = $trans_data['SHIPTOZIP'];
        }
        
        $transaction->save();
        $transaction_id = $transaction->id;

        return $transaction_id;
    }

    function sendPpvEmail($trans_data = array(), $data = array(), $transaction_id = 0, $isadv = 0, $VideoName = '', $VideoDetails = '') {
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) ? Yii::app()->user->id : @$data['user_id'];
        //Get attachment file for transaction
        $command = Yii::app()->db->createCommand()
                ->select('has_attachment')
                ->from('notification_templates')
                ->where('studio_id = ' . $studio_id . ' AND type="ppv_subscription" AND parent_id=0');
        $template = $command->queryAll();
        if (isset($template) && !empty($template)) {
            
        } else {
            $command = Yii::app()->db->createCommand()
                    ->select('has_attachment')
                    ->from('notification_templates')
                    ->where('studio_id = 0 AND type="ppv_subscription"');
            $template = $command->queryAll();
        }

        $attachment = '';
        if (isset($template[0]['has_attachment']) && intval($template[0]['has_attachment'])) {
            $attachment = Yii::app()->pdf->downloadUserinvoice($studio_id, $transaction_id, $user_id,'', 1);
        }
       
        //End

        if (intval(@$isadv)) {
            $ppv_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'advance_purchase', $attachment, $VideoName, '', '', '', '', Yii::app()->controller->language_code);
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('advance_purchase', $studio_id);
            if ($isEmailToStudio) {
                $trans_amt = Yii::app()->common->formatPrice(@$trans_data['amount'], @$data['currency_id'], 1);
                Yii::app()->controller->sendStudioAdminEmails($studio_id, 'admin_advance_purchase', $user_id, $VideoName, $trans_amt,'');
            }
        } else {
            $ppv_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'ppv_subscription', $attachment, $VideoName, $VideoDetails, '', '', '', Yii::app()->controller->language_code);
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('ppv_subscription', $studio_id);
            if ($isEmailToStudio) {
                $trans_amt = Yii::app()->common->formatPrice(@$trans_data['amount'], @$data['currency_id'], 1);
                Yii::app()->controller->sendStudioAdminEmails($studio_id, 'admin_payment_ppv_content', $user_id, $VideoName, $trans_amt,'');
            }
        }
    }
    function sendSubscriptionBundlesEmail($trans_data = array(), $data = array(), $transaction_id = 0, $isadv = 0, $VideoName = '', $VideoDetails = '',$subscriptionbundlesPlanId) {
        $studio_id = (isset(Yii::app()->user->studio_id) && intval(Yii::app()->user->studio_id)) ? Yii::app()->user->studio_id : @$data['studio_id'];
        $user_id = (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) ? Yii::app()->user->id : @$data['user_id'];
        //Get attachment file for transaction
        $command = Yii::app()->db->createCommand()
                ->select('has_attachment')
                ->from('notification_templates')
                ->where('studio_id = ' . $studio_id . ' AND type="subscription_bundles" AND parent_id=0');
        $template = $command->queryAll();
        if (isset($template) && !empty($template)) {
    
        } else {
            $command = Yii::app()->db->createCommand()
                    ->select('has_attachment')
                    ->from('notification_templates')
                    ->where('studio_id = 0 AND type="subscription_bundles"');
            $template = $command->queryAll();
        }

        $attachment = '';
      
        if (isset($template[0]['has_attachment']) && intval($template[0]['has_attachment'])) {
            $attachment = Yii::app()->pdf->downloadUserinvoice($studio_id, $transaction_id, $user_id,$subscriptionbundlesPlanId, 1);
        }
//End
            $ppv_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_bundles', $attachment, $VideoName, $VideoDetails, '', '', '', Yii::app()->controller->language_code);
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_bundles', $studio_id);
            if ($isEmailToStudio) {
                $trans_amt = Yii::app()->common->formatPrice(@$trans_data['amount'], @$data['currency_id'], 1);
                Yii::app()->controller->sendStudioAdminEmails($studio_id, 'admin_payment_subscription_bundles_content', $user_id, $VideoName, $trans_amt,'');
            }
    }
    
    function isCurrencySupport($currency_id = Null) {
        $is_support = 0;

        if (isset($currency_id) && intval($currency_id)) {
            $payment_gateway_id = Yii::app()->controller->GATEWAY_ID;
            $currency_support = CurrencySupport::model()->findByAttributes(array('payment_gateway_id' => $payment_gateway_id, 'currency_id' => $currency_id));

            if (isset($currency_support) && !empty($currency_support)) {
                $is_support = 1;
            }
        }

        return $is_support;
    }
    function isCouponExistsForSubscription($studio_id) {
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        $isCouponExists = 0;
        if(isset($data['menu']) && !empty($data['menu']) && (($data['menu'] & 1) || ($data['menu'] & 256))) {

            $coupon = Yii::app()->db->createCommand()
                      ->select('id')->from('coupon_subscription')
                      ->where('studio_id=:studio_id AND status=:status',array(':studio_id'=>$studio_id, ':status'=>1))
                      ->queryRow();
            
            if (isset($coupon) && !empty($coupon)) {
                $isCouponExists = 1;
            }
        }        
        return $isCouponExists;
    }
    
    function isValidCouponForSubscription($arg = array()) {
        $studio_id = @$arg['studio_id'];
        $user_id = @$arg['user_id'];
        $coupon = @$arg['coupon'];
        $plan = @$arg['plan'];
        $currencyId = @$arg['currency'];
        
        
        $todayDate = date("Y-m-d");
        $data = Yii::app()->db->createCommand()
                ->select('id, discount_type, discount_amount,used_by,coupon_type,user_can_use,restrict_usage,extend_free_trail')
                ->from('coupon_subscription')
                ->where('studio_id=:studio_id AND coupon_code=:coupon_code AND valid_from<=:valid_from AND valid_until>=:valid_until', array(':studio_id'=>$studio_id, ':coupon_code'=>$coupon, ':valid_from'=>$todayDate, ':valid_until'=>$todayDate))
                ->queryRow();
       
         $tableName='user_subscriptions';    
         $use_condition = "";
         if ($data['coupon_type'] == 1) {
             $use_condition = "AND user_id=".$user_id;
         }
        $user_coupon_data = Yii::app()->db->createCommand()
                            ->select('*')->from($tableName)
                            ->where('coupon_code=:coupon_code '.$use_condition.' AND studio_id=:studio_id', array(':coupon_code'=>$coupon,':studio_id'=>$studio_id))
                            ->queryAll();
        
        $auth = "";
        if (!empty($user_coupon_data) && count($user_coupon_data) > 0) {
            if ($data['coupon_type'] == 1) {
                $no_use = count($user_coupon_data);
                $restrict = $data['restrict_usage'];

                if ($restrict == 0) {
                    $auth = "allow";
                } else if ($no_use >= $restrict) {
                    $auth = "restricted";
                } else {
                    $auth = "allow";
                }
            }else{
                $auth = "restricted";
            }
        }
        $discountDetails = array();
        $sendData = 0;
        if (!empty($data) && count($data) > 0) {
            if ($data['coupon_type'] == 1) {
                if ($data['user_can_use'] == 0) {
                    $userIdArray = array();
                    $userIdArray = explode(",", $data['used_by']);
                    if (in_array($user_id, $userIdArray)) {
                        return 1;
                    } else {
                        $sendData++;
                    }
                } else if ($auth != "" && $auth == "restricted") {
                    return 1;
                } else {
                    $sendData++;
                }
            } else {
                if ($auth != "" && $auth == "restricted"){
                    return 1;
                }else{ 
                    if ($data['used_by'] == 0) {
                        $sendData++;
                    } else {
                        return 1;
                    }
                }
            }
            if ($sendData != 0) {
                $symbol = Currency::model()->findByPK($currencyId)->symbol;
                if ($data['discount_amount'] == '0.00') {
                    $couponCurrency = CouponCurrencySubscription::model()->findByAttributes(array('coupon_id' => $data['id'], 'currency_id' => $currencyId));
                    
                    if ($couponCurrency) {
                        $discountDetails['discount_type'] = $data['discount_type'];
                        $discountDetails['discount_amount'] = $couponCurrency['discount_amount'];
                        $discountDetails['extend_free_trail'] = $data['extend_free_trail'];
                        $discountDetails['symbol'] = $symbol;
                    } else {
                        return 0;
                    }
                } else {
                    $discountDetails['discount_type'] = $data['discount_type'];
                    $discountDetails['discount_amount'] = $data['discount_amount'];
                    $discountDetails['extend_free_trail'] = $data['extend_free_trail'];
                    $discountDetails['symbol'] = $symbol;
                }
                return $discountDetails;
            }
        } else {
            return 0;
        }
    }
    function currencyConversion($from, $to, $amount) {
        $data = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from&to=$to");
        preg_match("/<span class=bld>(.*)<\/span>/", $data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return number_format((float) ($converted), 2, '.', '');
    }

    function convertCurrency1($currency_from, $currency_to, $currency_input) {
        $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
        $yql_query = 'select * from yahoo.finance.xchange where pair in ("' . $currency_from . $currency_to . '")';
        $yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
        $yql_query_url .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        $yql_session = file_get_contents($yql_query_url);
        $yql_json = json_decode($yql_session, true);
        $currency_output = (float) $currency_input * $yql_json['query']['results']['rate']['Rate'];

        return number_format((float) ($currency_output), 2, '.', '');
    }
  public function CouponSubscriptionBundlesCalculation($couponDetails,$planDetails){
            if($couponDetails ==1){
                    $res['isError'] = 2;
                }else{
                    if ($couponDetails["discount_type"] == 1) {//calculate for percentage
                        $final_price = $planDetails[0]['price'] - ($planDetails[0]['price'] * $couponDetails['discount_amount'] / 100);
                        $new_final_price = number_format($final_price, 2, '.', '');
                        $res['full_amount'] = $planDetails[0]['price'];
                        $res['discount_amount'] = $new_final_price;
                        $res['discount_price'] = $couponDetails['discount_amount'];
                        $res['extend_free_trail'] = $couponDetails['extend_free_trail'];
                        $res['symbol'] = $couponDetails['symbol'];
                        $res['is_cash'] = 0;
                    }else{//calculate for cash
                        if($planDetails["currency_id"] != 0){
                            $currencyDetails  = CouponCurrencySubscription::model()->findByPk($planDetails["currency_id"]);
                            
                            $discount_amount = number_format((float) ($couponDetails['discount_amount']), 2, '.', '');
                            $plan_amount = number_format((float) ($planDetails[0]['price']), 2, '.', '');
                            $final_amount = $plan_amount;
                            
                            if (($plan_amount - $discount_amount) > 0.1) {
                                $final_amount = $plan_amount - $discount_amount;
                            } else {
                                $discount_amount = $plan_amount;
                                $final_amount = number_format(0, 2, '.', '');;
}

                            $res['discount_price'] = $discount_amount;
                            $res['full_amount'] = $plan_amount;
                            $res['discount_amount'] = $final_amount;
                            $res['extend_free_trail'] = $couponDetails['extend_free_trail'];
                            $res['symbol'] = $couponDetails['symbol'];
                            $res['is_cash'] = 1;
                        } else {
                            $res['discount_amount'] = $couponDetails['discount_amount'];
                            $res['is_cash'] = 1;
                        }
                    }
                    if (isset($_REQUEST['physical']) && $_REQUEST['physical']) {
                        $_SESSION['couponCode'] = $_REQUEST["couponCode"];
                    }
                }
                    
                    
                return $res;
    }
}
