jQuery(document).ready(function($) {
    $('.popup-data').hide();
    $('.play-box').on("mouseenter", function() {
        $(this).find(".popup-data").show();
    })
    .on("mouseleave", function() {
        $(this).find(".popup-data").hide();
    });
    
    if ($(window).width() > 991)
    {
        $('<div class="clearfix"></div>').insertAfter(".list_page .col-md-5ths:nth-child(5n)");
        $('<div class="clearfix"></div>').insertAfter(".blogcontent:nth-child(3n)");
    }
    else
    {
        $('<div class="clearfix"></div>').insertAfter(".list_page .col-xs-6:nth-child(even)");
        $('<div class="clearfix"></div>').insertAfter(".bottom_pages .col-xs-6:nth-child(4n)");

    }

    $('.ps-caption').addClass('text-left');

    $('.review_form').find('h2').addClass('title1');
    $('.ps-list li').find("span").remove();
    $('.close').click(function() {
        $('.avatar-wrapper').html("");
        $('.avatar-input').val("");
    });
    $('#avatar-modal').on('hidden.bs.modal', function() {
        $('.avatar-wrapper').html("");
        $('#avatarInput').val("");

    });

    $('.dropdown').hover(function() {
        $('.dropdown-toggle', this).trigger('click');
    });
    if ($(window).width() > 1023) {
        $('.searchSec .input-group-addon').click(function() {

            if ($("#siteSearch").val() == "") {
                $('.searchSec input').toggleClass('input-width');
            } else {
                $("#search-form").submit();
            }
        });
    } else {
        $('.searchSec input').addClass('input-width');
    }

    $("#card-section").find('.form-group').each(function(i) {
        if (i == 0)
        {
            $(this).html('<div class="col-md-6 bottom20"><div class="icon-addon addon-lg"><input type="text" required="" name="ContactForm[name]" id="contact_name" placeholder="' + card_name + '" autocomplete="off" class="form-control" value="" autofocus=""><label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-user"></i></label></div></div><div class="col-md-6 bottom20"><div class="icon-addon addon-lg"><input type="text" required="" name="data[card_number]" id="card_number" placeholder="' + card_number + '" autocomplete="false" class="form-control"><label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-credit-card"></i></label></div></div>');
        }
        if (i == 1)
        {
            $(this).find('label').hide();
            $(this).find('div').each(function(i) {

                if (i == 0)
                {
                    $(this).addClass('icon-addon addon-lg col-sm-4');
                    $(this).removeClass('col-sm-2');
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-calendar fa-left-10"></i></label>').insertAfter("#exp_month");
                    $('#exp_month').addClass('select-hack-safari');
                }
                if (i == 1)
                {
                    $(this).html();
                }
                if (i == 2)
                {
                    $(this).addClass('icon-addon addon-lg col-sm-4');
                    $(this).removeClass('col-sm-2');
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-calendar fa-left-10"></i></label>').insertAfter("#exp_year");
                    $('#exp_year').addClass('select-hack-safari');

                }
                if (i == 4) {
                    $(this).addClass('icon-addon addon-lg');
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-lock fa-left-10"></i></label>').insertAfter("#security_code");
                }
            });

        }
    });
    $("#sepa").find('.form-group').each(function(i) {
        if (i == 0)
        {
            $(this).find('label').remove();
            $(this).find('div').addClass('icon-addon addon-lg col-md-6');
            $(this).find('div').removeClass('col-sm-4');
            $(this).find('div').each(function(i) {
                if (i == 0) {
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-user fa-left-10"></i></label>').insertAfter("#account_name");
                }
                if (i == 1) {
                    $('<label data-original-title="name" title="" rel="tooltip for ="email"><i class="fa fa-university fa-left-10" aria-hidden="true"></i></label>').insertAfter("#iban");
                }
            });
        }
        if (i == 1)
        {
            $(this).find('label').remove();
            $(this).find('div').each(function(i) {
                if (i == 0) {
                    $(this).addClass('icon-addon addon-lg col-md-8');
                    $(this).removeClass('col-sm-4');
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-map-marker fa-left-10" aria-hidden="true"></i></label>').insertAfter("#street");
                }
                if (i == 1) {
                    $(this).addClass(' icon-addon addon-lg col-md-4');
                    $(this).removeClass('col-sm-4');
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-building-o fa-left-10" aria-hidden="true"></i></label>').insertAfter("#city");
                }
            });
        }
        if (i == 2) {
            $(this).find('label').remove();
            $(this).find('div').addClass('icon-addon addon-lg col-md-6');
            $(this).find('div').removeClass('col-sm-4');
            $(this).find('div').each(function(i) {
                if (i == 0) {
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-envelope-o fa-left-10" aria-hidden="true"></i></label>').insertAfter("#postalcode");
                }
                if (i == 1) {
                    $('<label data-original-title="name" title="" rel="tooltip" for="email"><i class="fa fa-globe fa-left-10" aria-hidden="true"></i></label>').insertAfter("#country");
                }
            });
        }
    });
    $("div.payment_form").find('h3').addClass('title1');
    $('#cancel_now').prepend('<i class="fa fa-times padding-right-10" aria-hidden="true"></i>');
    $(".play-btn")
            .on("mouseenter", function() {
                $(".playbtn").show();
            })
            .on("mouseleave", function() {
                $(".playbtn").hide();
            });

    // jQuery sticky Menu

    $(".mainmenu-area").sticky({topSpacing: 0});

    $('.product-carousel').owlCarousel({
        loop: true,
        navigation: true,
        navigationText: ['<i class="fa fa-3x fa-chevron-left"></i>', '<i class="fa fa-3x fa-chevron-right"></i>'],
        margin: 20,
        responsiveClass: true,
        rewindNav: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 5,
            }
        }
    });

    $('.related-products-carousel').owlCarousel({
        loop: true,
        nav: true,
        margin: 20,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 2,
            },
            1200: {
                items: 3,
            }
        }
    });

    $('.brand-list').owlCarousel({
        loop: true,
        nav: true,
        margin: 20,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            }
        }
    });


    // Bootstrap Mobile Menu fix


    // jQuery Scroll effect
    /*   $('.navbar-nav li a, .scroll-to-up').bind('click', function(event) {
     var $anchor = $(this);
     var headerH = $('.header-area').outerHeight();
     $('html, body').stop().animate({
     scrollTop : $($anchor.attr('href')).offset().top - headerH + "px"
     }, 1200, 'easeInOutExpo');
     
     event.preventDefault();
     }); */
    // Bootstrap ScrollPSY
    /* $('body').scrollspy({ 
     target: '.navbar-collapse',
     offset: 95
     })*/



});

function showcart(id, flag){
    $('.loader_cart').show();
    var url = HTTP_ROOT+'/shop/addtocart';
    $.post(url, {'quantity':1, 'id':id}, function(res){
        if (flag == 2){
            window.location.href = '/shop/cart';
        } else{
            $('.loader_cart').hide();
            $('html,body').animate({scrollTop:0}, 500);
            $('#round-cart').html(eval($('#round-cart').html()) + 1);
        }
    });
}










