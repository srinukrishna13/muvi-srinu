<?php
if(!empty($featureds)){
    foreach ($featureds as $featured) {
        $is_episode = $featured->is_episode;
        if ($is_episode == 1) {
            $stream_id = $featured->movie_id;
            $langcontent = Yii::app()->custom->getTranslatedContent($stream_id, $is_episode, $language_id);
            $movie_stream = new movieStreams;
            $movie_stream = $movie_stream->findByPk($stream_id);
            if (array_key_exists($stream_id, $langcontent['episode'])) {
                $movie_stream->episode_title = $langcontent['episode'][$stream_id]->episode_title;
            }
            $movie_id = $movie_stream->movie_id;

            $film = new Film();
            $film = $film->findByPk($movie_id);
            if (array_key_exists($movie_id, $langcontent['film'])) {
                $film->name = $langcontent['film'][$movie_id]->name;
            }
            $cont_name = $film->name . ' ';

            $cont_name .= ($movie_stream->episode_title != '') ? $movie_stream->episode_title : "SEASON " . $movie_stream->series_number . ", EPISODE " . $movie_stream->episode_number;
            $poster = $this->getPoster($stream_id, 'moviestream', 'episode');
        } else if ($is_episode == 2) {
            $movie_id = $content_id = $featured->movie_id;
            $poster = PGProduct::getpgImage($content_id, 'standard');
            $content = Yii::app()->custom->getProductDetails($content_id, array('name'));
            $cont_name = @$content[0]['name'];
        }elseif ($is_episode == 4){
			$movie_id = $content_id = $featured->movie_id;
			$poster = $this->getPoster($movie_id, 'playlist_poster');
			$playlist = new UserPlaylistName();
			$playlistdata = $playlist->findByPk($movie_id);
			$cont_name = $playlistdata->playlist_name . ' ';
        } else {
            $movie_id = $featured->movie_id;
            $langcontent = Yii::app()->custom->getTranslatedContent($movie_id, $is_episode, $language_id);
            $film = new Film();
            $film = $film->findByPk($movie_id);
            if (array_key_exists($movie_id, $langcontent['film'])) {
                $film->name = $langcontent['film'][$movie_id]->name;
            }
            $cont_name = $film->name;
            $ctype = $film->content_types_id;
            if ($ctype == 2)
                $poster = $this->getPoster($film->id, 'films', 'episode');
            else if ($ctype == 4)
                $poster = $this->getPoster($film->id, 'films', 'original');
            else
                $poster = $this->getPoster($film->id, 'films', 'standard');
        }
        if (false === file_get_contents($poster)) {
            $poster = "/img/No-Image-Horizontal.png";
        }
        ?>
        <li class="col-sm-3 drag-cursor" id="sort_<?php echo $section_id; ?>_<?php echo $featured->id ?>" >
            <div class="Preview-Block"  >		
                <div class="thumbnail m-b-0"  >
                    <div class="relative m-b-10 drag-cursor">
                        <img src="<?php echo $poster ?>" alt="">
                        <div class="overlay-bottom overlay-white text-right">
                            <div class="overlay-Text">
                                <div>
                                    <a href="javascript:void(0);"  onclick="deleteFeatured(<?php echo $section_id; ?>,<?php echo $featured->id ?>);" class="btn btn-danger icon-with-fixed-width">
                                        <em class="icon-trash"></em>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5><?php echo (strlen($cont_name) > 50) ? strip_tags(substr($cont_name, 0, 50)) . '&hellip;' : $cont_name; ?></h5>
                </div>
            </div>
        </li>
<?php
    }
}
?>
