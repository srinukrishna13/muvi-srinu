<div class="container">
    <h2 class="btm-bdr">Contact Us</h2>
    <div class="row-fluid contact_us">        
        <div class="span8">            
            <form method="post" name="contact-form" id="contact-form" class="form-inline">
				<?php 
                            /*$source = '';
                            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                                $source = Yii::app()->request->cookies['REFERRER'];
                                //Unset source cookie
                                unset($_COOKIE['REFERRER']);
                                setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
                            }
                            $source = Yii::app()->request->getParam('src') ? Yii::app()->request->getParam('src') : $source;
				*/ ?>
<!--				<input type="hidden" value="<?php echo $source;?>" id="source" name="source"/>-->
                <div id="loading" class="loading"></div>
                <div class="row-fluid">
                    <div class="span6 border">
                        <span class="add-on"><i class="icon-name"></i></span>
                        <input type="text" id="fullname" name="fullname" placeholder="Name" />
                    </div>                
                    <div class="span6 border eml">
                        <span class="add-on"><i class="icon-email"></i></span>
                        <input type="text" id="email" name="email" placeholder="Email" />
                    </div>            
                </div>
                <div class="row-fluid">
                    <div class="span6 border martop10">
                        <span class="add-on"><i class="icon-company"></i></span>
                        <input type="text" id="company" name="company" placeholder="Company" />
                    </div>                
                    <div class="span6 border martop10">
                        <span class="add-on"><i class="icon-phone"></i></span>
                        <input type="text" id="phone" name="phone" placeholder="Phone" />
                    </div>            
                </div>
                <div class="row-fluid">
                    <div class="span12 border tarea martop10">
                        <span class="add-on"><i class="icon-message"></i></span>
                        <textarea id="message" name="message" placeholder="Message" rows="12"></textarea>            
                    </div>
                </div>       
                <div class="row-fluid martop10">
					<input type="hidden" id="ccheck" name="ccheck" placeholder="" autocomplete="off">
                                        <input type="hidden" name="submit-btn" id="submit-btn" value="contact" />
                    <div class="span6"><input type="button" id="send" value="Send" class="btn btn-blue"  /></div>
                    <div class="span6">
                        <ul class="pull-right muvi-social">
                            <li><a href="https://www.facebook.com/MuviStudioB2B" target="_blank" class="fb"></a></li>
                            <li><a href="http://www.twitter.com/muvistudiob2b" target="_blank" class="tw"></a></li>
                            <li><a href="https://www.linkedin.com/company/muvi-studio" target="_blank" class="ln"></a></li>
                            <li><a href="https://plus.google.com/118356511554070797931/posts" target="_blank" class="gp"></a></li>
                        </ul>                        
                    </div>
                </div>
                <p class="error" id="errors"></p>
            </form>
        </div>
        <div class="span4">
            <div id="map"></div>
            <div style="margin-top: 10px;">
                <div class="fl addr">
                    <div class="span1"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/address.png" alt="Map" title="Location" class="middle" style="margin-top: 10px;" /></div>
                    <div class="span11">
                        <div id="location">103 Patron Dr, Guilderland, NY 12084, USA</div>
                        <div id="phoneno">Phone: +1 (860)896-5151</div>
                    </div>
                </div>
            </div>         
        </div>
    </div>
</div>

<script type="text/javascript">
    
    function validEmail(email)
    {
        var res = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(email);
        return res;
    }
    
    function validPhone(phone)
    {
        var res = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(phone);
        return res;
    }
    
    jQuery.noConflict();
    jQuery(document).ready(function(){
        jQuery('#loading').hide();    
        jQuery('#send').click(function(){
                    
            if(jQuery('#fullname').val() == '')
            {
                jQuery('#errors').html('Please enter name');
                jQuery('#fullname').focus();
                return false;
            }
            else if(jQuery('#email').val() == '')
            {
                jQuery('#errors').html('Please enter email');
                jQuery('#email').focus();
                return false;
            } 
            else if(!validEmail(jQuery('#email').val()))
            {
                jQuery('#errors').html('Please enter a valid email');
                jQuery('#email').focus();
                return false;
            }   
            else if(jQuery('#phone').val() == '')
            {
                jQuery('#errors').html('Please enter phone number');
                jQuery('#phone').focus();
                return false;
            } 
            else if(!validPhone(jQuery('#phone').val()))
            {
                jQuery('#errors').html('Please enter a valid phone number');
                jQuery('#phone').focus();
                return false;
            }  
            else if(jQuery('#message').val() == '')
            {
                jQuery('#errors').html('Please enter message');
                jQuery('#message').focus();
                return false;
            }             
            else
            {
                jQuery.ajax({
                    url: '<?php echo Yii::app()->baseUrl?>/contact/send',
                    data: jQuery('#contact-form').serialize(),
                    type:'POST',
                    dataType: "json",
                    beforeSend:function(){
                        jQuery('#loading').show();
						ga('send', 'event', { eventCategory: 'contact-us-page', eventAction: 'submit', eventLabel: 'contact-us-page-form'});
                    },

                    success: function(data){
                        jQuery('#loading').hide();
                        if(data.status == 'success')
                        {
                            alert(data.message);
                            window.location = '<?php echo Yii::app()->baseUrl?>/contact';
                        }
                        else
                        {
                            jQuery('#errors').html(data.message);
                            return false;
                        }
                    }                  
                });  
            }  
        });  
    });
</script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
   showGoogleMap(); 
});
var gmarkers = [];
//var defaultIcon = {url: 'http://google.com/mapfiles/ms/micons/blue-dot.png'};
var defaultIcon = {url: '<?php echo Yii::app()->theme->baseUrl; ?>/images/default.png'};
//var activeIcon = {url: 'http://google.com/mapfiles/ms/micons/red-dot.png'};
var activeIcon = {url: '<?php echo Yii::app()->theme->baseUrl; ?>/images/active.png'};

function showGoogleMap() {
    //
    //var sites = [
    //  ['103 Patron Dr, Guildrand, NY 12084, USA', 42.6922433,-73.8945098],
    //  ['55 Ryefield Avenue, Uxbridge, UB10 9BX, UK', 51.54792, -0.44486],
    //  ['4th Floor, North Face, OCAC Tower, Acharya Vihar, Bhubaneswar – 751013', 20.2989938,85.8309715],
    //]; 
    
    var sites = [
      ['103 Patron Dr, Guildrand, NY 12084, USA', 42.6922433,-73.8945098],
      ['4th Floor, North Face, OCAC Tower, Acharya Vihar, Bhubaneswar – 751013', 20.2989938,85.8309715],
    ];     
  
    var centerMap = new google.maps.LatLng(42.6922433,-73.8945098);
    var myOptions = {
        zoom: 1,
        center: centerMap,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    setMarkers(map, sites);
    infowindow = new google.maps.InfoWindow({
        content: "loading..."
    });
}

function setMarkers(map, markers) {
    //var phones = ['Phone: +1 (860)677-8883', 'Phone: +44 (0) 7404 151621', 'Phone: +91 9819735454']; 
    var phones = ['Phone: +1 (860)677-8883', 'Phone: +91 9819735454'];      
    for (var i = 0; i < markers.length; i++) {
        var sites = markers[i];
        var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);
        
        var icon = defaultIcon;
        if (i === 0) {
            icon = activeIcon;
        }
        
        var marker = new google.maps.Marker({
            position: siteLatLng,
            map: map,
            title: sites[0],
            icon: icon,
            html: sites[0],
            customInfo: phones[i]
        });

        google.maps.event.addListener(marker, "click", function () {
            for (var i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setIcon(defaultIcon);                
            }
            this.setIcon(activeIcon);
            jQuery("#location").html(this.html);
            jQuery("#phoneno").html(this.customInfo);
            infowindow.setContent(this.html);
            infowindow.open(map, this);
        });
        gmarkers.push(marker);
    }
}

</script>