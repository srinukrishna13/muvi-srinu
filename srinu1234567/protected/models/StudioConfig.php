<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StudioConfig extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'studio_config';
    }

    public function getconfigvalue($key) {
        $studio_id = Yii::app()->common->getStudiosId();
        $data = Yii::app()->db->createCommand()
                ->select('config_value')
                ->from($this->tableName())
                ->where('studio_id = :studio_id AND config_key = :config_key', array(':studio_id' => $studio_id, ':config_key' => $key))
                ->queryRow();
        return $data;
    }
    public function getconfigvalueForStudio($studio_id,$key) {
        $data = Yii::app()->db->createCommand()
                ->select('config_value')
                ->from(self::tableName())
                ->where('studio_id = :studio_id AND config_key = :config_key', array(':studio_id' => $studio_id, ':config_key' => $key))
                ->queryRow();
        return $data;
    }
    
    public function getConfig($studio_id, $key){
        $config = $this->find('studio_id = :studio_id AND config_key=:config_key',array(':studio_id'=>$studio_id,":config_key"=>trim($key)));
        return $config;
    }    
    public function updatePolicy($studio_id, $policy) {
        if ($studio_id) {
            $config = $this->find('studio_id = :studio_id AND config_key=:config_key', array(':studio_id' => $studio_id, ":config_key" => 'enable_policy'));
            if ($policy && $policy == 1) {
                if (empty($config)) {
                    $config = new StudioConfig();
                    $config->studio_id = $studio_id;
                    $config->config_key = 'enable_policy';
                }
                $config->config_value = 1;
                $config->save();
                return 'enabled';
            } else {
                $this->deleteAll("config_key='enable_policy' AND studio_id='$studio_id'");
                return 'disabled';
            }
        } else {
            return false;
        }
    }
    
    public function getdeviceRestiction($studio_id, $user_id,$stream_id){
        $isRestriction = "";
        $config = $this->find('studio_id = :studio_id AND config_key=:config_key',array(':studio_id'=>$studio_id,':config_key'=>'restrict_streaming_device'));
        if(@$config->config_value > 0){
            $userActive = RestrictDevice::model()->getNoOfUserActive($studio_id,$user_id,$stream_id); 
            if($userActive < $config->config_value){
                $isRestriction = $config->config_value - $userActive;
            } else{
                $isRestriction = 0;
            }
        }
        return trim($isRestriction);
    }
	public function UpdateDownloadable($studio_id, $flag) {
        if ($studio_id) {
            $config = $this->find('studio_id = :studio_id AND config_key=:config_key', array(':studio_id' => $studio_id, ":config_key" => 'is_downloadable'));
            if (empty($config)) {
				$config = new StudioConfig();
				$config->studio_id = $studio_id;
				$config->config_key = 'is_downloadable';
			}
			$config->config_value = ($flag==1)?1:0;
			$config->save();
		}
	}
}
?>