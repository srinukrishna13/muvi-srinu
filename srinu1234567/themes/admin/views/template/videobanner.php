<?php
/*
 * To upload video banner
 * Ratikanta
 * 
 */
?>
<?php
$s3dir = 'public/system/videobanner/';
$bucket = Yii::app()->params->s3_bucketname;
?>
<?php echo $this->renderPartial('//layouts/video_gallery'); ?>
<form id="bannerText" method="post" name="bannerText" action="<?php echo Yii::app()->getBaseUrl(true) ?>/template/saveBannerText">
    <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="row-fluid">
            <div class="col-md-12 box box-primary">
                <br />            
                <input type="hidden" name="section_id"  id="section_id" value="<?php echo $section->id; ?>" />
                <input type="hidden" name="update_text" value="0" id="update_text" />                 
                <div class="row form-group">
                    <div class="col-md-12">
                        <label class="control-label col-md-1">Upload Video</label>                    
                        <div class="col-md-11">
                            <a data-target="addvideo_popup" data-toggle="modal" class="btn btn-default-with-bg btn-sm" onclick="openUploadpopup('addvideo_popup');" class="btn btn-primary" id="trailer_btn"><i class="icon-upload"></i> Upload Video </a>
                        </div>
                    </div>              
                </div>                                 	   
                <div class="clearfix"></div>            
                <?php
                if (count($banners) > 0) {
                    foreach ($banners as $banner) {
                        ?>
                        <input type="hidden" name="banner_id" id="banner_id" value="<?php echo $banner->id; ?>" />
                        <div class="videocontent">
                            <video id="video_block" class="video-js moo-css vjs-default-skin" controls preload="auto" poster="<?php echo $banner->video_placeholder_img; ?>" width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}'>
                                <source src="<?php echo $banner->video_remote_url; ?>" type="video/mp4" />
                            </video>  
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="videocontent">
                        <video id="video_block" class="video-js moo-css vjs-default-skin" controls preload="auto" poster="" width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}'>
                            <source src="http://video-js.zencoder.com/oceans-clip.mp4" type='video/mp4' />
                            <source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
                            <source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
                        </video> 
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php
        $bnr_txt = '';
        $show_join_btn = 0;
        $join_btn_txt = 'JOIN NOW';
        if (!empty($banner_text)) {
            $bnr_txt = $banner_text['bannercontent'];
            $show_join_btn = $banner_text['show_join_btn'];
            $join_btn_txt = $banner_text['join_btn_txt'];

            if (@IS_LANGUAGE == 1) {
                if ($this->language_id != $banner_text['language_id']) {
                    //$bnr_txt = '';
                    $join_btn_txt = '';
                }
            }
        }
        ?>
        <div class="clearfix"></div>
        <div class="row m-b-40 m-t-40">
            <div class="col-md-8">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Text on Banner:</label>
                        <div class="col-md-8">
                            <div class="fg-line"> 
                                <input type="text" class="form-control input-sm" id="bnr_txt" name="bnr_txt" placeholder="<?php echo $placeholder ?>" value="<?php echo html_entity_decode($bnr_txt); ?>">
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="rmvdisable" value="1" name="show_join_btn" <?php echo ($show_join_btn > 0) ? ' checked="checked"' : ''; ?> id="show_join_btn" <?php if (@IS_LANGUAGE == 1) {
            if ($banner_text['parent_id'] > 0) {
                ?>disabled="disabled" <?php }
                                   }
        ?>>
                                    <i class="input-helper"></i> Show button to registration page
                                </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="join_btn_txt" id="join_btn_txt" value="<?php echo $join_btn_txt; ?>">
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <div id="bnr_txt_error" class="error m-b-20"></div>
                            <input type="button" class="btn btn-primary btn-sm" id="savetxt" value="Update" onclick="return removeDisable();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</form>
<!-- Progress Bar popup -->
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
    <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
        <div style="float:left;font-weight:bold;">File Upload Status</div>
        <div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
    </div>
    <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<!-- Progress Bar popup end -->          


<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
<style type="text/css">
    .video-js {height: 50%; padding-top: 47%;}
    .vjs-fullscreen {padding-top: 0px}  
    .RDVideoHelper{display: none;}
    video::-webkit-media-controls {
        display:none !important;
    }
</style>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<!-- Script for Upload Trailer -->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
            $(document).ready(function() {
                var player = videojs('video_block');
            });
            function click_browse_video() {
                $("#videofile").click();
            }
            function openUploadpopup() {
                $('input[type=file]').val('');
                $("#addvideo_popup").modal('show');
                searchvideo('video');
            }
            function searchvideo(type) {
                showLoader();
                var key_word = $("#search_video").val();
                var key_word = key_word.trim();
                $.ajax({
                    url: HTTP_ROOT + "/admin/ajaxSearchVideo",
                    data: {'search_word': key_word,'type':type},
                    contentType: false,
                    cache: false,
                    success: function(result)
                    {
                        showLoader(1);
                        $("#video_content_div").html(result);
                    }
                });
            }
            function checkfileSize() {
                var movie_name = 'bannervideo';
                var stream_id = <?php echo $studio_id; ?>;
                var section_id = <?php echo $section->id; ?>;
                var filename = $('#videofile').val().split('\\').pop();
                var extension = filename.replace(/^.*\./, '');
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                switch (extension) {
                    case 'mp4':
                        break;
                    case 'mov':
                        break;
                    case 'mkv':
                        break;
                    case 'flv':
                        break;
                    case 'vob':
                        break;
                    case 'm4v':
                        break;
                    case 'avi':
                        break;
                    case '3gp':
                        break;
                    case 'mpg':
                        break;
                    case 'wmv':
                        break;
                    default:
                        // Cancel the form submission
                        swal('Sorry! This video format is not supported. \n MP4/MOV/MKV/FLV/VOB/M4V/AVI/3GP/MPG/WMV format video are allowed to upload');
                        return false;
                        break;
                }
                swal({
                    title: "Upload File?",
                    text: "Are you sure to upload " + filename + " file for  " + movie_name + " ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#10CFBD",
                    customClass: "cancelButtonColor",
                    confirmButtonText: "Upload",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                    html: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $("#addvideo_popup").modal('hide');
                        if (!$('#dprogress_bar').is(":visible")) {
                            $('#dprogress_bar').show();
                        }
                        var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload video</a></h5>";
                        $('#' + stream_id).html(rtext);
                        upload('user', 'pass', stream_id, movie_name, section_id);
                        return true;
                    } else {
                        return false;
                    }
                });

            }

// Multipart Upload Code
            var s3upload = null;
            var s3obj = new Array();
            var size = '';
            var sizeleft = 0;
            var sizeName = '';
            function upload(user, pass, stream_id, movie_name, movie_id) {
                //var xhr = new XMLHttpRequest({mozSystem: true});
                var filename = $('#videofile').val().split('\\').pop();
                if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
                    alert("Sorry! You are using an older or unsupported browser. Please update your browser");
                    return;
                }

                /*if (s3upload != null) {
                 alert("Though it is possible to upload multiple files at once, this demonstration does not allowed to do so to demonstrate pause and resume in a simple manner. Sorry :-(");
                 return;
                 }*/

                var file = $('#videofile')[0].files[0];
                size = Math.round(file.size/1000);
                sizeName = 'Kb';
                if(size > 1000){
                    size = Math.round(size/1000);
                    var sizeName = 'MB';
                }
                if(size > 1000){
                    size = size/1000;
                    size = size.toFixed(2);
                    var sizeName = 'GB';
                }
                s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'movie_id': movie_id, 'uploadType': 'videobanner'}, 'template');
                s3upload.onServerError = function(command, jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status === 403) {
                        alert("Sorry you are not allowed to upload");
                    } else {
                        console.log("Our server is not responding correctly");
                    }
                };

                s3upload.onS3UploadError = function(xhr) {
                    s3upload.waitRetry();
                    console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
                };

                //s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
                s3upload.onProgressChanged = function(progPercent) {
                    if (progPercent == 100) {
                        $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
                        $('#upload_'+stream_id+' .uploadFileSizeProgress').html(size);
                    } else {
                        var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
                        $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

                        $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
                
                        if(sizeName == 'GB'){
                            sizeleft = (progper * size)/100;
                            sizeleft = sizeleft.toFixed(2);
                        }else{
                            sizeleft = Math.round((progper * size)/100);
                        }
                        $('#upload_'+stream_id+' .uploadFileSizeProgress').html(sizeleft);
                    }
                };

                s3upload.onUploadCompleted = function(data) {
                    window.location.href = window.location.href;
                };
                s3upload.onUploadCancel = function() {
                    $('#upload_' + stream_id).remove();
                    if ($('#all_progress_bar').is(":empty")) {
                        $('#dprogress_bar').hide();
                    }

                    $('#trailer_btn').removeAttr('disabled');
                    console.log("Upload Cancelled..");
                };

//Start s3 Multipart upload	
                s3upload.start();
                s3obj[stream_id] = s3upload;
                var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5>' + movie_name + ' - ' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="cancel" >Cancel Upload</a></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success" percent="0" style="width: 0%"></div></div></div>';
                $('#all_progress_bar').append(progressbar);
                $('#cancel_' + stream_id).on('click', function() {
                    aid = this.id;
                    sid = aid.split('_');
                    if (confirm('Are you sure you want to cancel this upload?')) {
                        s3obj[sid[1]].cancel();
                    }
                });
            }
            function addvideoFromVideoGallery(videoUrl, galleryId) {
                showLoader();
                var action_url = '<?php echo $this->createUrl('template/addVideoFromVideoGallery'); ?>'
                $('#server_url_error').html('');
                var movie_id = <?php echo $section->id; ?>;
                var movie_stream_id = $('#movie_stream_id').val();
                $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function(res) {
                    showLoader(1);
                    if (res.error) {
                        $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
                    } else {
                        if (res.is_video === 'uploaded') {
                            window.location.href = window.location.href;
                        } else if (res.is_video) {
                            $("#addvideo_popup").modal('hide');
                            $('#server_url').val('');
                            $('#server_url_error').val('');
                            var text = '<h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a></h5>';
                            $('#' + movie_stream_id).html(text);
                        } else {
                            $('#server_url_error').html('There seems to be something wrong with the video');
                        }
                    }
                }, 'json');
                console.log('valid url');
            }

</script>    
