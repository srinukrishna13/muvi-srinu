<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">Download</h2>
            <div id="dwnld_loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif" style="padding-left: 28%;"></div>
            <div class="row" style="padding-left: 35%;" id="download_content">
                <h4 class="modal-title">Fill your information to download the document.</h4>
                
                <div class="clear"></div><br>
                <form class="form-horizontal" id="download_frm">
                  <div class="form-group">
                    <label for="dnld_name" class="col-sm-2 control-label">Name</label>
                    <div class="span3">
                        <input type="text" class="form-control" id="dnld_name" name="dnld_name" placeholder="Name" required autocomplete="off" />
                        <div class="clear"></div>
                    </div>
                  </div> 
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label for="dnld_email" class="col-sm-2 control-label">Email Address</label>
                    <div class="span3">
                        <input type="email" class="form-control" id="dnld_email" name="dnld_email" placeholder="Email Address" required autocomplete="off" />
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label for="dnld_cpmpany" class="col-sm-2 control-label">Company</label>
                    <div class="span3">
                        <input type="text" class="form-control" id="dnld_cpmpany" name="dnld_cpmpany" placeholder="Company" autocomplete="off" />
                    </div>
                  </div> 
                  <div class="clearfix"></div>
                  <div class="form-group pad10">                   
                      <div class="span2" style="margin-left:165px;">
                            <input type="submit" id="download_btn" class="btn btn-blue" value="Submit" />
                      </div>
                  </div>
                </form>               
                <div class="clearfix"></div>
                <div class="error center" id="dwnld-errors"></div>   
                <br />
            </div>
        </div>        
    <script type="text/javascript">
        

        $(function() {
            $('#dwnld_loading').hide();
            $('#download_btn').click(function(){
                
                $("#download_frm").validate({
                    rules: {    
                        dnld_name: {
                            required: true,
                            minlength: 5
                        },                
                        dnld_email: {
                            required: true,
                            mail: true
                        },
                    },
                    messages: {             
                        dnld_name: {
                            required: "Please enter your full name",
                            minlength: "Name must be atleast of 5 characters long",
                        },                
                        dnld_email: {
                            required: "Please enter email address",
                            mail: "Please enter a valid email address",
                        },
                    },                       
                    submitHandler: function(form) {
                        $.ajax({
                            url: "<?php echo Yii::app()->getbaseUrl(true); ?>/site/downloadsubmit",
                            data: $('#download_frm').serialize(),
                            type:'POST',
                            dataType: "json",
                            beforeSend:function(){
                                $('#dwnld_loading').show();
                                $('#download_content').hide();
								ga('send', 'event', { eventCategory: 'brochure', eventAction: 'submit', eventLabel: 'brochuredownload'});
                            },
                            success: function (data) {                                
                                if(data.stat == 'success')
                                {
                                    $('button.close').trigger('click');   
                                    $('#dwnld_loading').hide();
                                    $('#download_content').show();
                                    $('#dnld_name').val('');
                                    $('#dnld_email').val('');
                                    $('#dnld_cpmpany').val('');
                                    //
                                    bootbox.alert("Thank you for downloading the brochure.", function(){
                                        window.location = data.message;
                                    });  
                                }
                                else
                                {
                                    $('#dwnld-errors').html(data.message);
                                    return false;
                                }
                            }
                        });
                    }
                });                                            
            });
        });

    </script>     
    </div>
</div>