<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" rel="stylesheet" />
<div class="col-lg-12">
        <div class="banner">
			<div class="lbimage"><img src="<?php echo Yii::app()->baseurl;?>/images/homepage/engaged-audience.png"></div>	
			<div class="tbimage"><img src="<?php echo Yii::app()->baseurl;?>/images/homepage/headline.png"></div>	
			<div class="rbimage"><img src="<?php echo Yii::app()->baseurl;?>/images/homepage/server.png"></div>	
			<input type="button" name="" id="oazTJ5GcHFc" class="youtube">
        <div class="clear"></div>
		</div>
        <article>
        	<section>
            	<h1>You own your content</h1>
                <hr width="230px" size="2px" color="#000000" align="left">
                <p>Unlike YouTube, Netflix and other popular<br> streaming websites, Muvi offers you<br> your own cloud server.</p>
            	<hr width="360px" size="2px" color="#e5e5e5" align="left">
                <p style="width:357px;">You own all rights and have complete control on your content, who to stream to,<br> when (timing) and how much to charge.</p>
            	<img src="<?php echo Yii::app()->baseurl;?>/images/homepage/pic1.png">
            </section>
        </article>
        <article class="bar2">
        	<section>
            	<img src="<?php echo Yii::app()->baseurl;?>/images/homepage/pic2.png" style="margin:85px 0 0 0">
            	<h1>leverage engaged audience</h1>
                <div style="width:230px;height:2px;float:right;border-top:2px solid #000000;"></div>
                <p>Leverage Hyper-Engaged audience on Muvi who<br> want to pay for your content.</p>
                <div style="width:370px;height:2px;float:right;border-top:2px solid #e5e5e5;"></div>
                <p>Just publish your content and start<br> making money from day 1!</p>
            	
            </section>
        </article>
        <article>
        	<section>
            	<h1>Viral promotion of your content</h1>
                <div style="width:230px;height:2px;float:left;border-top:2px solid #000000;"></div>
                <p>Social features on Muvi virally promote your content,<br> without you doing a single click.</p>
                <div style="width:360px;height:2px;float:left;border-top:2px solid #e5e5e5;"></div>
                <p>Plus, it attracts new viewers from Facebook, Twitter,<br> and other leading social network.</p>
            	<img src="<?php echo Yii::app()->baseurl;?>/images/homepage/pic3.png" style="margin:-290px 0 0 0;">
            </section>
        </article>
        <article class="bar2">
        	<section>
            	<img src="<?php echo Yii::app()->baseurl;?>/images/homepage/pic4.png" style="margin:0">
            	<h1>Reach a global audience</h1>
                <div style="width:230px;height:2px;float:right;border-top:2px solid #000000;"></div>
                <p>Muvi Users are from the<br> across 40 countries over the world.<br> Reach brand new markets that exponentialy grow<br> Your audience base.</p>
            </section>
        </article>
        <article>
        	<section>
            	<h1>Embed anywhere</h1>
                <div style="width:230px;height:2px;float:left;border-top:2px solid #000000;"></div>
                <p>Embed your content on any webpage or blog.<br> Your movie comes with a built in preview.</p>
            	<img src="<?php echo Yii::app()->baseurl;?>/images/homepage/pic5.png" style="margin:-180px 0 0 0;">
            </section>
        </article>
        <article style="height:488px;" class="bar2">
        	<section>
            	<img src="<?php echo Yii::app()->baseurl;?>/images/homepage/pic6.png" style="padding:50px 0 0 0">
            	<h1>Audience relationship managment (ARM)</h1>
                <div style="width:230px;height:2px;float:right;border-top:2px solid #000000;"></div>
                <p>Own your audience not just for one movie, but for a lifetime<br> (more if you belive in second life). Know exactly what they like and promote<br> additional content to them.</p>
            </section>
        </article>

  <footer>
    <section>
      <a href="mailto:studio@muvi.com"><img src="<?php echo Yii::app()->baseurl;?>/images/homepage/icon5.png"></a>
      <p style="color:#fff;">Contact us: Send us an email on <a style="color:#307bab" href="mailto:studio@muvi.com">studio@muvi.com</a> or live chat</p>
    </section>
  </footer>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseurl;?>/js/jquery.youtubepopup.min.js"></script>
  <script type="text/javascript">
        $(function () {
			$("input.youtube").YouTubePopup({ idAttribute: 'id' });
        });
</script>

