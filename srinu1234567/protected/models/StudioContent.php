<?php
class StudioContent extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studio_content';
    }
    public function getContentSettings($studio_id) {
        $sql = "SELECT * FROM studio_content WHERE studio_id = ".$studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function updateContentSettings($studio_id,$menu_sum) {
        $sql = "INSERT INTO studio_content (studio_id, content_count) VALUES(".$studio_id.", ".$menu_sum.") ON DUPLICATE KEY UPDATE studio_id=".$studio_id.", content_count=".$menu_sum;
        $data = Yii::app()->db->createCommand($sql)->execute();
        return $data;
    }
    public function getChildContentSettings($studio_id) {
        $sql = "SELECT * FROM studio_content_child WHERE studio_id = ".$studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function updateChildContentSettings($studio_id,$menu_sum) {
        $sql = "INSERT INTO studio_content_child (studio_id, content_count) VALUES(".$studio_id.", ".$menu_sum.") ON DUPLICATE KEY UPDATE studio_id=".$studio_id.", content_count=".$menu_sum;
        $data = Yii::app()->db->createCommand($sql)->execute();
        return $data;
    }
}