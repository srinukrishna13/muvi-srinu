<?php

class UserfeatureController extends Controller {

    public $defaultAction = 'ratings';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
             $this->checkPermission();
        }
        Yii::app()->theme = 'admin';
        $this->pageTitle = Yii::app()->name . ' | Manage Home';
        return true;
    }

    /**
     * @function to save the rating option of the studio
     * @author RK<support@muvi.in>
     * @return HTML 
     */
    public function actionRatings() {
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);

        $this->breadcrumbs = array('User Features' , 'Ratings');
        $this->headerinfo = "Ratings";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Ratings';

        $rate = new ContentRating();
        $ratings = $rate->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id DESC'));
        $this->render('ratings', array('ratings' => $ratings, 'studio' => $std));
    }

    public function actionSavefeature() {

        $studio = $this->studio;

        if (isset($_REQUEST['rating_status']) && $_REQUEST['rating_status'] != 0) {
            $studio->rating_activated = 1;
        } else
            $studio->rating_activated = 0;
        $studio->save();
        Yii::app()->user->setFlash('success', 'Settings updated successfully!');
        $url = $this->createUrl('userfeature/ratings');
        $this->redirect($url);
    }

    public function actionDeleterating() {

        if (isset($_REQUEST['deleterating']) && $_REQUEST['deleterating'] > 0) {
            $studio_id = Yii::app()->common->getStudioId();
            $rate = new ContentRating();
            $rating = $rate->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['deleterating']));
            if (count($rating) > 0) {
                $rating->delete();
                Yii::app()->user->setFlash('success', 'Review is deleted!');
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            } else {
                Yii::app()->user->setFlash('error', "You don't have access to this review!");
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this review!");
            $url = $this->createUrl('userfeature/ratings');
            $this->redirect($url);
        }
    }

    public function actionReviewstatus() {

        if (isset($_REQUEST['reviewstatus']) && $_REQUEST['reviewstatus'] > 0) {
            $studio_id = Yii::app()->common->getStudioId();
            $rate = new ContentRating();
            $rating = $rate->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['reviewstatus']));
            if (count($rating) > 0) {
                $status = ($rating->status == 1) ? 0 : 1;
                $rating->status = $status;
                $rating->save();
                Yii::app()->user->setFlash('success', 'Review status changes!');
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            } else {
                Yii::app()->user->setFlash('error', "You don't have access to this review!");
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this review!");
            $url = $this->createUrl('userfeature/ratings');
            $this->redirect($url);
        }
    }
    
    public function actionNewsletter(){        
        $studio_id = Yii::app()->common->getStudiosId();
        $this->pageTitle="Muvi | Newsletter Subscribers";
        $this->breadcrumbs = array('User Features' , 'Newsletter Subscribers');
        $this->headerinfo = "Newsletter Subscribers";
        $config = new StudioConfig();
        $config = $config->getconfigvalue('newsletter');
        if($config['config_value'] == 1){
            $sub = new NewsletterSubscribers();
            $subscribers = $sub->findAllByAttributes(array('studio_id' => $studio_id));             
            $this->render('newsletter', array('subscribers' => $subscribers));
        }        
    }   
    
    public function actionExportSubscribersExcel() {
        $studio_id = Yii::app()->common->getStudiosId();
        $headArr[0] = array('Email', 'Subscription Date');
        $sheetName = array('Newsletter Subscribers');
        $sheet = 1;
        
        $sub = new NewsletterSubscribers();
        $subscribers = $sub->findAllByAttributes(array('studio_id' => $studio_id)); 
        $data = array();
        foreach($subscribers as $subscriber){
            $data[0][] = array(
                $subscriber->email,
                Yii::app()->common->phpDate($subscriber->date_subscribed)
            );            
        }

        $filename = 'newsletter_subscribers_' . date('Ymd_His');
        $type = 'xls';
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }    
    /*
     * Author ajit@muvi.com
     * 14/9/2016
     * user can enable facebook login through this function
     * issue id 4005
     */
    public function actionSocialnetworks() {
        $studio_id = Yii::app()->common->getStudioId();

        $std = Studio::model()->find(array('select'=>'status,is_subscribed,social_logins,is_default','condition' => 'id=:studio_id','params' => array(':studio_id' =>$studio_id)));
        $this->breadcrumbs = array('User Features' , 'Social network integration');
        $this->headerinfo = "Social network integration";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Social network integration';

        $social = new SocialLoginInfos();
        $sociallogininfos = $social->findByAttributes(array('studio_id' => $studio_id));
        $this->render('socialnetworks', array('sociallogininfos'=>$sociallogininfos,'status'=>$std));
}

    /*
     * Author ajit@muvi.com
     * 12/9/2016
     * user can enable facebook login through this function
     * issue id 4856
     */
    function actionfacebooklogin() {
        //print_r($_POST);exit;
        @extract($_POST);
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
        //check if enable checkbox is clicked or not
        if($enb_fblogin == 1)
        {            
            //set parameter for email
            $user = User::model()->findByPk($user_id);
            $req['studioname'] = $this->studio->name;
            $req['name'] = $user->first_name;
            $req['email'] = $user->email;

            if(isset($redirect) && $redirect=='redirect')
            {
                Yii::app()->db->createCommand('UPDATE social_login_infos SET status = 1 WHERE studio_id=' . $studio_id)->execute();

                //update status in studio table
                $std = new Studio();
                $studio = $std->findByPk($studio_id);
                $studio->social_logins = 1;
                $studio->save();

                //send email
                $req['emailtype'] = 'active';
                Yii::app()->email->fbLoginRequest($req);
                Yii::app()->user->setFlash('success', "Facebook login activated successfully.");                
                $this->redirect($this->createUrl('userfeature/socialnetworks'));
                exit;
            }
            if($id !='')
            {  
                $login = New SocialLoginInfos;
                $studio = $login->findByPk($id);
                $studio->fb_app_id = $app_id;
                $studio->fb_secret = $app_secret;
                $studio->fb_app_verson = $app_version;
                $studio->save();
                Yii::app()->user->setFlash('success', "Facebook app data updated successfully.");                
                $this->redirect($this->createUrl('userfeature/socialnetworks'));
                exit;
            }
            else {
                //save data to db - social_login_infos               
                $login = New SocialLoginInfos;           
                $login->studio_id = $studio_id;
                $login->fb_app_id = $app_id;
                $login->fb_secret = $app_secret;
                $login->fb_app_verson = $app_version;
                $login->created_date = gmdate('Y-m-d H:i:s');
                $login->created_by = $user_id;
                $login->status = ($whose_fb == 'muvi_fb')?2:3;
                $login->whose_fb = $whose_fb;
                $login->save();
                //send email to admin and customer
                $req['emailtype'] = 'request';
                Yii::app()->email->fbLoginRequest($req);
                Yii::app()->user->setFlash('success','Facebook login request saved successfully');
                $this->redirect($this->createUrl('userfeature/socialnetworks'));
                exit;
            }
        }
    }
    function actionfacebookloginactive() {        
        @extract($_POST);
        $studio_id = Yii::app()->user->studio_id;
        
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $studio->social_logins = $social_logins;
        $studio->save();
        
        Yii::app()->user->setFlash('success', "Status updated successfully.");                
        $this->redirect($this->createUrl('userfeature/socialnetworks'));
        exit;
    }
        
    /*
     * 6372: My Library feature for a website
     * ajit@muvi.com, 31/01/2017
     * @settings for my library menu in frontend
     */    
    /*
    public function actionmyLibrary() {        
        $this->breadcrumbs = array('User Features' , 'My Library');
        $this->headerinfo = "My Library";
        $this->pageTitle = Yii::app()->name . ' | ' . 'My Library';
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        if(count($_POST) && $_POST['sup']=='sup'){
            if (isset($_POST['mylibrary_activated']) && $_POST['mylibrary_activated'] == 1) {
                $std->mylibrary_activated = 1;
            } else {
                $std->mylibrary_activated = 0;
            }
            $std->save();
            Yii::app()->user->setFlash('success', 'Settings updated successfully!');
            $url = $this->createUrl('userfeature/mylibrary');
            $this->redirect($url);
        }
        $this->render('mylibrary', array('studio' => $std));
    } 
    
    public function actionAddToFavourite(){
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'Add to Favorites');
        $this->headerinfo = "Add to Favorites";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Add to Favorites';
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        $status = 0;
        if(!empty($fav_status)){
            $status = $fav_status['config_value'];
        }
        $this->render('addtofavourite', array('status'=>$status));
    }
    public function actionUpdateFavourite(){
        if($_POST){
            $studio_id = Yii::app()->common->getStudioId();
            $fav_status = StudioConfig::model()->getConfig($studio_id,'user_favourite_list');
            $config_value = isset($_POST['favliststatus']) && intval($_POST['favliststatus'])?$_POST['favliststatus']:0;
            if(empty($fav_status)){
                $config = new StudioConfig;
                $config->studio_id = $studio_id;
                $config->config_key = 'user_favourite_list';
            }else{
                $config = StudioConfig::model()->findByPk($fav_status->id);
            }
            $config->config_value = $config_value;
            $config->save();
            Yii::app()->user->setFlash('success', "Status updated successfully.");                
            $this->redirect($this->createUrl('userfeature/addtofavourite'));
            exit; 
        }else{
            Yii::app()->user->setFlash('error', "You do not have access to this page.");                
            $this->redirect($this->createUrl('admin/dashboard'));
            exit;
        }
    }
    */
    public function actionSettings(){
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'Settings');
        $this->headerinfo = "Settings";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Settings';
        //save
        if($_POST){
            //add to favorite            
            $fav_status = StudioConfig::model()->getConfig($studio_id,'user_favourite_list');
            $config_value = isset($_POST['addfav'])?$_POST['addfav']:0;
            $config = new StudioConfig;
            if(empty($fav_status)){                
                $config->studio_id = $studio_id;
                $config->config_key = 'user_favourite_list';
            }else{
                $config = StudioConfig::model()->findByPk($fav_status->id);
            }
            $config->config_value = $config_value;
            $config->save();
            
            //my library
            $std = new Studio();
            $std = $std->findByPk($studio_id);
            $mylibrary_activated = isset($_POST['mylibrary_activated'])?$_POST['mylibrary_activated']:0;
            $std->mylibrary_activated = $mylibrary_activated;            
            $std->save();           
            
            //watch history
            $watch_status = StudioConfig::model()->getConfig($studio_id,'studio_watch_history');
            $watch_value = isset($_POST['watch'])?$_POST['duration']:0;
            $wconfig = new StudioConfig;
            if(empty($watch_status)){
                //$config = new StudioConfig;
                $wconfig->studio_id = $studio_id;
                $wconfig->config_key = 'studio_watch_history';
            }else{
                $wconfig = StudioConfig::model()->findByPk($watch_status->id);
            }
            $wconfig->config_value = $watch_value;
            $wconfig->save(); 
            
            //reminder status
            $reminder_status = StudioConfig::model()->getConfig($studio_id,'reminder');
            $reminder_value  = isset($_POST['reminder'])? $_POST['reminder'] : 0;
            $rconfig = new StudioConfig;
            if(empty($reminder_status)){
                $rconfig->studio_id  = $studio_id;
                $rconfig->config_key = 'reminder';
            }else{
                $rconfig = StudioConfig::model()->findByPk($reminder_status->id);
            }
            $rconfig->config_value = $reminder_value;
            $rconfig->save(); 
            if($reminder_value){
                $notification_status = StudioConfig::model()->getConfig($studio_id, 'notification');
                $notification_value  = isset($_POST['notification'])? $_POST['notification'] : 0;
                $nconfig = new StudioConfig;
                if(empty($notification_status)){
                    $nconfig->studio_id  = $studio_id;
                    $nconfig->config_key = 'notification';
                }else{
                    $nconfig = StudioConfig::model()->findByPk($notification_status->id);
                }
                $nconfig->config_value = $notification_value;
                $nconfig->save(); 
                if($notification_value){
                    $reminder_before_status = StudioConfig::model()->getConfig($studio_id,'reminder_before');
                    $reminder_before_value  = isset($_POST['notify_before'])? $_POST['notify_before'] : 0;
                    $rbconfig = new StudioConfig;
                    if(empty($reminder_before_status)){
                        $rbconfig->studio_id  = $studio_id;
                        $rbconfig->config_key = 'reminder_before';
                    }else{
                        $rbconfig = StudioConfig::model()->findByPk($reminder_before_status->id);
                    }
                    $rbconfig->config_value = $reminder_before_value;
                    $rbconfig->save(); 
                }
            }
			$pllist_status = StudioConfig::model()->getConfig($studio_id,'user_playlist');
            $pllist_value = isset($_POST['enplaylist'])?$_POST['enplaylist']:0;
            $config = new StudioConfig;
            if(empty($pllist_status)){                
                $config->studio_id = $studio_id;
                $config->config_key = 'user_playlist';
            }else{
                $config = StudioConfig::model()->findByPk($pllist_status->id);
            }
            $config->config_value = $pllist_value;
            $config->save();
			$que_status = StudioConfig::model()->getConfig($studio_id,'user_queue_list');
            $queue_value = isset($_POST['adque'])?$_POST['adque']:0;
            $config = new StudioConfig;
            if(empty($que_status)){                
                $config->studio_id = $studio_id;
                $config->config_key = 'user_queue_list';
            }else{
                $config = StudioConfig::model()->findByPk($que_status->id);
            }
            $config->config_value = $queue_value;
            $config->save();
			
            Yii::app()->user->setFlash('success', "Status updated successfully.");                
            $this->redirect($this->createUrl('userfeature/Settings'));
            exit; 
        } 
        
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        $status = 0;
        if(!empty($fav_status)){
            $status = $fav_status['config_value'];
        }
        $watch_status = StudioConfig::model()->getConfig($studio_id,'studio_watch_history');
        $watch_val = 0;
        if(!empty($watch_status)){
            $watch_val = $watch_status['config_value'];
        }  
        $reminder_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder');
        $reminder = 0;
        $notification = 0;
        $reminder_before = "";
        if(!empty($reminder_config)){
            $reminder = $reminder_config['config_value'];
            $notification_config    = StudioConfig::model()->getconfigvalueForStudio($studio_id,'notification');
            if(!empty($notification_config)){
                $notification = $notification_config['config_value'];
                $reminder_before_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder_before');
                if(!empty($reminder_before_config)){
                    $reminder_before = $reminder_before_config['config_value'];
                } 
            }
        }
	
	
		$que_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'user_queue_list');
		$statusQueue = 0;
		if (!empty($que_status)) {
			$statusQueue = $que_status['config_value'];
		}
		$pl_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_playlist');
        $statusPlaylist = 0;
        if(!empty($pl_status)){
            $statusPlaylist= $pl_status['config_value'];
        }
        $std = Studio::model()->find(array('select'=>'mylibrary_activated','condition' => 'id=:studio_id','params' => array(':studio_id' =>$studio_id)));
        $this->render('settings', array('status'=>$status,'studio' => $std,'watch_val' => $watch_val,'reminder' => $reminder, 'notification'=>$notification, 'reminder_before' => $reminder_before,'queue_enabled'=>$statusQueue,'playlist_enable'=>$statusPlaylist,'studio_id'=>$studio_id));
    }
	public function actionUserRestrictions(){		
		$studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'User Restrictions');
        $this->headerinfo = "User Restrictions";
        $this->pageTitle = Yii::app()->name . ' | ' . 'User Restrictions';
		$studio_id = Yii::app()->common->getStudioId();
		 if(isset($_POST) && !empty($_POST)){
			 //Restrict No of devices
			 if(isset($_POST['restrict_no_devices'])){
					$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_no_devices');
					if(isset($sconfig) && count($sconfig) > 0){                
						$sconfig->config_value = 1;
						$sconfig->save();
					}else{                        
						$sconfig = New StudioConfig;
						$sconfig->studio_id = $studio_id;
						$sconfig->config_key = 'restrict_no_devices';
						$sconfig->config_value = 1;
						$sconfig->save();
}
			 }else{
					$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_no_devices');
					if(isset($sconfig) && count($sconfig) > 0){
						$sconfig->config_value = 0;
						$sconfig->save();
					}else{            
						$sconfig = New StudioConfig;
						$sconfig->studio_id = $studio_id;
						$sconfig->config_key = 'restrict_no_devices';
						$sconfig->config_value = 0;
						$sconfig->save();
					}
			 }	
			 //Limit Devices
			if (isset($_POST['limit_devices']) && $_POST['limit_devices'] !="") {
				$limit_devices = trim($_POST['limit_devices']);
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_devices');
				if(isset($sconfig) && count($sconfig) > 0){                
					$sconfig->config_value = $limit_devices > 0 ? $limit_devices : 1;
					$sconfig->save();
				}else{                        
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_devices';
					$sconfig->config_value = $limit_devices > 0 ? $limit_devices : 1;
					$sconfig->save();
				}
			} else {
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_devices');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 1;
					$sconfig->save();
				}else{            
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_devices';
					$sconfig->config_value = 1;
					$sconfig->save();
				}
			} 
			//Device Switch Duration
			if (isset($_POST['restrict_device_switch_duration'])) {
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){                
					$sconfig->config_value = 1;
					$sconfig->save();
				}else{                        
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_device_switch_duration';
					$sconfig->config_value = 1;
					$sconfig->save();
				}            
			}else{
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 0;
					$sconfig->save();
				}else{            
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_device_switch_duration';
					$sconfig->config_value = 0;
					$sconfig->save();
				}
			}
			//Limit Device switch duration
			if (isset($_POST['limit_device_switch_duration']) && $_POST['limit_device_switch_duration'] !="") {
				$limit_device_switch_duration = isset($_POST['restrict_device_switch_duration'])?trim($_POST['limit_device_switch_duration']):0;
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){                
					$sconfig->config_value = $limit_device_switch_duration > 0 ? $limit_device_switch_duration : 0;
					$sconfig->save();
				}else{                        
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_device_switch_duration';
					$sconfig->config_value = $limit_device_switch_duration > 0 ? $limit_device_switch_duration : 0;
					$sconfig->save();
				}
			} else {
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 0;
					$sconfig->save();
				}else{            
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_device_switch_duration';
					$sconfig->config_value = 0;
					$sconfig->save();
				}
			}  
			//Restrict no of streaming devices
			if(isset($_POST['restrict_streaming_devices']) && $_POST['restrict_streaming_devices'] !=""){
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
				$limit_streaming_devices = (isset($_POST['limit_streaming_devices']) && $_POST['limit_streaming_devices']>0)?$_POST['limit_streaming_devices']:0;
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = $limit_streaming_devices;
					$sconfig->save();
				}else{
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_streaming_device';
					$sconfig->config_value = $limit_streaming_devices;
					$sconfig->save();
				}
			}else{
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 0;
					$sconfig->save();
				}else{
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_streaming_device';
					$sconfig->config_value = 0;
					$sconfig->save();
				}				
			}
			 Yii::app()->user->setFlash('success', "Status updated successfully.");                
             $this->redirect($this->createUrl('userfeature/UserRestrictions'));
             exit;
		 }
			 //check app selected or not
            $checkAppSelected=Yii::app()->custom->checkAppSelected($studio_id);
            //Restrict Devices
            $restrict_no_devices=Yii::app()->custom->restrictDevices($studio_id,'restrict_no_devices');
            $limit_devices=($restrict_no_devices>0)?Yii::app()->custom->restrictDevices($studio_id,'limit_devices'):1;           
            //Restrict Device switch Duration
            $restrict_device_switch_duration=Yii::app()->custom->restrictDevices($studio_id,'restrict_device_switch_duration');
            $limit_device_switch_duration=($restrict_device_switch_duration>0)?Yii::app()->custom->restrictDevices($studio_id,'limit_device_switch_duration'):5;
			//Get no streaming devices
			$restrict_streaming_devices = Yii::app()->custom->restrictDevices($studio_id,'restrict_streaming_device');
			$this->render('user_restrictions',array('restrict_no_devices'=>$restrict_no_devices,'limit_devices'=>$limit_devices,'restrict_device_switch_duration'=>$restrict_device_switch_duration,'limit_device_switch_duration'=>$limit_device_switch_duration,'checkAppSelected'=>$checkAppSelected,'restrict_streaming_devices'=>$restrict_streaming_devices));		 
	}
	public function actionmanageQueue() {
		$studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'Manage Queue');
        $this->headerinfo = "Manage Queue";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Manage Queue';
		if($_POST['queuebtn'] == 'save' ){
			//Auto play next episode
			$autoPlay = StudioConfig::model()->getConfig($studio_id, 'autoplay_next_episode');
			$config_value = isset($_POST['autoPlay']) ? $_POST['autoPlay'] : 0;
			$config = new StudioConfig;
			if (empty($autoPlay)) {
				$config->studio_id = $studio_id;
				$config->config_key = 'autoplay_next_episode';
			} else {
				$config = StudioConfig::model()->findByPk($autoPlay->id);
			}
			$config->config_value = $config_value;
			$config->save();
			//Realted content
			$relCon = StudioConfig::model()->getConfig($studio_id, 'related_content');
			$config_rel_value = isset($_POST['related_content']) ? $_POST['related_content'] : 0;
			$configrel = new StudioConfig;
			if (empty($relCon)) {
				$configrel->studio_id = $studio_id;
				$configrel->config_key = 'related_content';
			} else {
				$configrel = StudioConfig::model()->findByPk($relCon->id);
			}
			$configrel->config_value = $config_rel_value;
			$configrel->save();
		}
		$auto_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'autoplay_next_episode');
		$status_auto = 1;
		if ($auto_status != '') {
			$status_auto = $auto_status['config_value'];
		}
		$rel_content_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'related_content');
		$status_rel = 0;
		if ($rel_content_status != '') {
			$status_rel = $rel_content_status['config_value'];
		}
		$this->render('managequeue',array('autoplay_enable'=>$status_auto,'related_content_enable'=>$status_rel));		 
    }
    
    public function actionMobileApps() {
        $chromecast=0;
        $offline=0;
        $studio_id = Yii::app()->common->getStudioId();
        $chromecast_val= StudioConfig::model()->getconfigvalueForStudio($studio_id,'chromecast');
        if($chromecast_val['config_value'] == 1 || $chromecast_val ==''){
            $chromecast=1;
        }
        $offline_val= StudioConfig::model()->getconfigvalueForStudio($studio_id,'offline_view');
        if($offline_val['config_value'] ==1 || $offline_val ==''){
            $offline=1;
        }
        $this->breadcrumbs = ['User Features' , 'Mobile Apps'];
        $this->headerinfo = "Mobile Apps";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Mobile Apps';
        $this->render('mobileapps',array('chromecast'=>$chromecast,'offline'=>$offline));
    }
    
    public function actionSaveApps() { 
        $studio_id = Yii::app()->common->getStudioId();
        $update_chrm=$_REQUEST['chromecast'];
        $update_off=$_REQUEST['offline_view'];
        if($update_chrm==''){
            $update_chrm=0;
        }
        if($update_off==''){
            $update_off=0;
        }
        $chromecast_key= StudioConfig::model()->findByAttributes(array('studio_id'=>$studio_id,'config_key'=>'chromecast'));
        if($chromecast_key!=''){
            Yii::app()->db->createCommand()->update('studio_config',array('config_value'=>$update_chrm),'id = :id',array(':id'=>$chromecast_key['id']));
        }
        else{
            Yii::app()->db->createCommand()->insert('studio_config',array('studio_id'=>$studio_id,'config_key'=>'chromecast','config_value'=>$update_chrm),'id = :id',array(':id'=>$chromecast_key['id']));

        }
        $offline_key=StudioConfig::model()->findByAttributes(array('studio_id'=>$studio_id,'config_key'=>'offline_view'));
        if($offline_key!=''){
            Yii::app()->db->createCommand()->update('studio_config',array('config_value'=>$update_off),'id = :id',array(':id'=>$offline_key['id']));
        }
        else{
            Yii::app()->db->createCommand()->insert('studio_config',array('studio_id'=>$studio_id,'config_key'=>'offline_view','config_value'=>$update_off),'id = :id',array(':id'=>$offline_key['id']));
        }
        Yii::app()->user->setFlash('success', "Status updated successfully.");                
        $this->redirect($this->createUrl('userfeature/mobileapps'));
        exit;
    }

}



