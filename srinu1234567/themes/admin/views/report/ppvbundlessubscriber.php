<input type="hidden" class="data-count" value="<?php echo $ppvbundles_subscription_count;?>" />
<thead>
    <th>User</th>
    <th>GATEWAY REF</th>
    <th>Amount</th>
    <th data-hide="phone">Date & Time</th>
    <th data-hide="phone">Bundle Name</th>
</thead>
<tbody class="list">
    <?php
    
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
        if(isset($ppvbundles_subscription) && count($ppvbundles_subscription)){
            foreach ($ppvbundles_subscription as $key=>$value){
                $date = new DateTime($value['start_date']);
                $date->setTimezone(new DateTimeZone('GMT'));
    ?>
    <tr>
        <td class="email_ppv">
            <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)){ ?>
                <?php echo ucfirst($value['dname']);?>
            <?php } else { ?>
            <a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email']?$value['email']:$value['dname'];?></a>
            <?php } ?>
            
        </td>
        <td class="gatewayref"><?php if($value['payment_method'] != ''){ echo (($value['payment_method'])?$value['payment_method']:'').' : '.(($value['order_number'])?$value['order_number']:''); }else{ echo 'N/A' ;} ?></td>
        <td class="amount"><?php echo $value['amount']?Yii::app()->common->formatPrice($value['amount'],$value['currency_id']):Yii::app()->common->formatPrice(0,$value['currency_id']);?></td>
        <td class="date_time"><?php echo $date->format('F j, Y, g:ia'). ' GMT';?></td>
        <td class="content"><?php echo $value['name']?$value['name']:'';?></td>
    </tr>
    <?php }}else{?>
    <tr>
        <td colspan="4">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>