<input type="hidden" class="data-count" value="<?php echo $count; ?>" />
<?php
    
    $isDate=0;
    if(isset($dateRange) && !empty($dateRange)){
        $isDate = 1;
    }
?>
<thead>
     <th>User</th>
     <th>Email</th>
    <th>Content</th>
    <th>Date And Time</th>
    <th>Duration</th>
    <th>Platform</th>
    <th>Type</th>
   
   
</thead>
<tbody class="list">
    <?php
        if(isset($userdata) && !empty($userdata)){
            foreach ($userdata as $views){
                ?>
                <tr>
                    <td class="content">
                        <?php //if($views['user_id']==0 && $views['trailer_id'] != ''){ echo "Anonymous"; } elseif($views['user_id']==0 && $views['trailer_id'] == ''){ echo "User - Embed"; } else{ echo $views['display_name'];} ?>
                    <?php if($views['played_from'] == 2) { echo "User - Embed";} else if(($views['user_id']==0 && $views['trailer_id'] != '' && $views['movie_id'] > 0 && $views['played_from'] == 1) || ($views['user_id']==0 && $views['trailer_id'] == '' && $views['movie_id'] > 0 && $views['played_from'] == 1)){echo "Anonymous";} else {echo $views['display_name'];} ?>
                    </td>
                    <td>
                     <?php //if(empty($views['email'])){ echo "Not Available"; } else {echo $views['email']; }  ?>
                        <?php if($views['user_id']==0 || $views['played_from'] ==2) { echo "Not Available";} else {echo $views['email']; }  ?></td>
                    <td><?php if($views['content_types_id']!=3){ echo $views['movie_name']; }else if($views['series_number'] != '' && $views['episode_title'] != '' && $views['content_types_id']==3){ echo $views['movie_name'].' -> Season '.$views['series_number'].' -> '.$views['episode_title']; } else {echo $views['movie_name'];} ?></td>
                    <td><?php echo  $views['created_date'];?></td>
                    <td><?php echo  $this->timeFormat($views['played_length']);?></td>
                    <td><?php if($views['device_type']==1){
                        echo 'Web';
                    }elseif($views['device_type']==2){   echo 'Android';}elseif($views['device_type']==3){   echo 'iOS';}elseif($views['device_type']==4){  echo 'Roku';}
                    
                    ;?></td>
                    <td><?php echo ($views['trailer_id']!= '') ? 'Trailer' : 'Content';  ?></td>
                </tr>
    <?php   }
        }else{
            echo '<tr><td colspan="4">No Record found!!!</td></tr>';
        }
    ?>
</tbody>