<?php
if ($third_party_url != '') {
    if ($trailerIsConverted == 1) {
        $info = pathinfo($third_party_url);
        $pos = stripos($third_party_url, 'iframe');
        $pos1 = stripos($third_party_url, 'youtube.com');
        $pos2 = stripos($third_party_url, 'vimeo.com');
        if ($info["extension"] == 'm3u8') {
            ?>
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
            <!--<link href="<?php //echo Yii::app()->getbaseUrl();  ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">-->
            <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/video-js.css" rel="stylesheet" type="text/css"/>
            <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
            <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/common/js/video.js" type="text/javascript"></script>
            <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/common/js/videojs-contrib-hls.js" type="text/javascript"></script>


            <style type="text/css">
                .video-js {width: <?php echo $halfwidth ?>px !important ; padding-top: <?php echo $halfheight ?>px !important;}
                .video_player-dimensions {width: <?php echo $halfwidth ?>px !important ;height: <?php echo $halfheight ?>px !important;}
                #play{
                    display: none;
                    margin: auto;
                    left:0;
                    right: 0;
                    bottom: 0;
                    top: 0;
                    position: absolute;
                    height: 64px;
                    width: 64px;
                    -webkit-animation: fadeinout .5s linear forwards;
                    animation: fadeinout .5s linear forwards;
                }
                @-webkit-keyframes fadeinout {
                    0%,100% { opacity: 0; }
                    50% { opacity: 1; }
                    from { transform: 5%; }
                    50% { transform: scale(1.4); }
                }
                @keyframes fadeinout {
                    0%,100% { opacity: 0; }
                    50% { opacity: 1; }
                    from { transform:  5%; }
                    50% { transform: scale(1.4); }
                }
                #pause{
                    display: none;
                    margin: auto;
                    left:0;
                    right: 0;
                    bottom: 0;
                    top: 0;
                    position: absolute;
                    height: 64px;
                    width: 64px;
                    -webkit-animation: fadeinout .5s linear forwards;
                    animation: fadeinout .5s linear forwards;
                }
                @-webkit-keyframes fadeinout {
                    0%,100% { opacity: 0; }
                    50% { opacity: 1; }
                    from { transform: 5%; }
                    50% { transform: scale(1.4); }
                }

                @keyframes fadeinout {
                    0%,100% { opacity: 0; }
                    50% { opacity: 1; }
                    from { transform:  5%; }
                    50% { transform: scale(1.4); }
                }
                #pause_touch{
                    margin: auto;
                    left:0;
                    right: 0;
                    bottom: 0;
                    top: 0;
                    position: absolute;
                    height: 64px;
                    width: 64px;
                    display: none;
                }
                #play_touch{
                    margin: auto;
                    left:0;
                    right: 0;
                    bottom: 0;
                    top: 0;
                    position: absolute;
                    height: 64px;
                    width: 64px;
                    display: none; 
                }
            </style>
            <div class="videocontent" style="overflow:hidden;">
                <video id="video_player" class="video-js vjs-default-skin" controls autoplay preload="auto" autobuffer   webkit-playsinline>
                    <source src="<?php echo $third_party_url; ?>" type="application/x-mpegURL">
                </video>    
            </div>
            <script>
                var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                var player = videojs('video_player');
                $(document).ready(function () {
                    $('#video_player').append('<img src="/images/touchpause.png" id="pause_touch" />');
                    $('#video_player').append('<img src="/images/touchplay.png" id="play_touch" />');
                    $('#video_player').append('<img src="/images/pause.png" id="pause"/>');
                    $('#video_player').append('<img src="/images/play-button.png" id="play"/>');
                    player.ready(function () {
                        if ($('#video_player').hasClass('video_player-dimensions')) {
                            $('#video_player').removeClass('video_player-dimensions')
                        }
                        player.play();
                        if (is_mobile === 0) {
                            $('#video_player_html5_api').on('click', function (e) {
                                e.preventDefault();
                                if (player.paused()) {
                                    $('#pause').hide();
                                    $('#play').show();
                                } else {
                                    $('#play').hide();
                                    $('#pause').show();
                                }
                            });
                        } else {
                            $('#pause_touch').bind('touchstart', function (e) {
                                player.pause();
                                $('#play_touch').show();
                                $('#pause_touch').hide();
                            });
                            $('#play_touch').bind('touchstart', function (e) {
                                player.play();
                                setTimeout(function () {
                                    $('#play_touch').hide();
                                }, 500);
                            });
                            $('#video_player_html5_api').bind('touchstart', function (e) {
                                if (player.play()) {
                                    $('#pause_touch').show();
                                    $('#play_touch').hide();
                                    setTimeout(function () {
                                        $('#pause_touch').hide();
                                    }, 3000);
                                }
                            });
                        }

                    });

                });

            </script> 
        <?php } else { ?>
            <style> 
                iframe{
                    width: <?php echo $halfwidth; ?>px !important;
                    height: <?php echo $halfheight; ?>px !important;  
                    background: black !important; 
                }
            </style>
            <?php echo $third_party_url;
        } 
    }
}
?>
                    







