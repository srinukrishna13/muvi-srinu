<?php

//include "bootstrap.php";
//
class SearchController extends Controller {

    public function init() {
        require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
        require_once $_SERVER["DOCUMENT_ROOT"] .'/protected/components/pagination.php';
        parent::init();
    }

     public function actionadd() {
        $solr = new Apache_Solr_Service('localhost', '8983', '/solr');
        if (!$solr->ping()) {
            echo 'Solr service not responding.';exit;
        }
		echo "Only Permited to Muvi";exit;
        $search_items = array();
        $pgconnection = Yii::app()->db;
        //$data = Film::model()->with(array('movie_streams' => array('alias' => 'ms', 'select' => 'ms.id as stream_id,is_episode,episode_title,episode_number,embed_id', 'joinType' => 'INNER JOIN', 'condition' => 'ms.movie_id = t.id'), 'studio_content_type'))->findAll('name!=\'\' AND name IS NOT NULL AND status = 1');
		$totalCount = 1;
		echo "<pre>";
		for($itr=0; $itr<$totalCount; $itr++){
			$offset = $itr*1000;

			//PDO Query
			$command = Yii::app()->db->createCommand()
					->select('SQL_CALC_FOUND_ROWS (0),f.id,f.studio_id,f.name,f.uniq_id,f.permalink,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,ms.is_episode,ms.embed_id,f.genre')
					->from('films f ,movie_streams ms ')
					->where('f.id = ms.movie_id AND ms.is_episode=0 AND f.studio_id>=54')
					->limit(1000,$offset);

			$data1 = $command->queryAll();
			//echo "<pre>";print_r($data1);exit;
			$count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
			$totalCount = ceil($count/1000);
			foreach($data1 AS $key=>$val){
				$stream_id = $val['stream_id'];
				$stream_uniq_id = $val['embed_id'];
				$uniq_id = Yii::app()->common->generateUniqNumber();
				$contentName = $val['name'];
				$is_episode = 0;
				if($val['content_types_id']==4){
					$contentTypeName = 'livestream';
				}else{
					$contentTypeName = 'content';
				}
				if($val['genre'] && $val['genre'] !='null'){
					$genreData = json_decode($val['genre'],TRUE);
					foreach($genreData as $gk=>$gval){
						$uniq_id = Yii::app()->common->generateUniqNumber();
						$search_items[] = array($val['permalink'] => array(
							'id' => $uniq_id,
							'content_id' => $val['id'],
							'name' =>$contentName,
							'stream_id' => $stream_id,
							'stream_uniq_id' => $stream_uniq_id,
							'is_episode' => $is_episode,
							'title' => $val['permalink'],
							'cat' => $contentTypeName,
							'features' => $val['permalink'],
							'sku' => $val['studio_id'],
							'genre' => $gval
						));
					}               
				}else{
					$uniq_id = Yii::app()->common->generateUniqNumber();
					$search_items[] = array($val['permalink'] => array(
						'id' => $uniq_id,
						'content_id' => $val['id'],
						'name' =>$contentName,
						'stream_id' => $stream_id,
						'stream_uniq_id' => $stream_uniq_id,
						'is_episode' => $is_episode,
						'title' => $val['permalink'],
						'cat' => $contentTypeName,
						'features' => $val['permalink'],
						'sku' => $val['studio_id'],
                        'genre' => ''
					));
				}
			}
			$documents=array();
			$cntMovie = count($search_items);
			for ($i = 0; $i < $cntMovie; $i++) {
				foreach ($search_items[$i] as $item => $fields) {
					$part = new Apache_Solr_Document();
					foreach ($fields as $key => $value) {
						if (is_array($value)) {
							foreach ($value as $datum) {
								$part->setMultiValue($key, $datum);
							}
						} else {
							$part->$key = $value;
						}
					}
					$documents[] = $part;
				}
			}
			 try {
				$solr->addDocuments($documents);
				$solr->commit();
				$solr->optimize();
			} catch (Exception $e) {
				echo $e->getMessage();exit;
			}
			print_r($documents);
			echo "Added===".$offset."==".$cntMovie." ---Out of ".$totalCount."<br/>";
			unset($search_items);unset($documents);
			$search_items = array();$documents=array();
		}
		
		unset($search_items);unset($documents);
		$search_items = array();$documents=array();
		$totalCount = 1;
		for($itr=0; $itr<$totalCount; $itr++){
			$offset = $itr*1000;
		// Adding Episodes to Solr	
		$command = Yii::app()->db->createCommand()
					->select('SQL_CALC_FOUND_ROWS (0),f.id,f.studio_id,f.name,f.uniq_id,f.permalink,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,ms.is_episode,ms.embed_id')
					->from('films f ,movie_streams ms ')
					->where('f.id = ms.movie_id AND ms.is_episode=1')
					->limit(1000,$offset);

			$data2 = $command->queryAll();
			
			$count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
			$totalCount = ceil($count/1000);
			
			foreach($data2 AS $key=>$val){
				$stream_id = $val['stream_id'];
				$stream_uniq_id = $val['embed_id'];
				$uniq_id = Yii::app()->common->generateUniqNumber();
				
				$contentName = $val['episode_title'] ? $val['episode_title'] : "Episode ".$val['episode_number'];
				$is_episode = 1;
				$contentTypeName = 'content - Episode';
				
				$search_items[] = array($val['permalink'] => array(
						'id' => $uniq_id,
						'content_id' => $val['id'],
						'name' =>$contentName,
						'stream_id' => $stream_id,
						'stream_uniq_id' => $stream_uniq_id,
						'is_episode' => $is_episode,
						'title' => $val['permalink'],
						'cat' => $contentTypeName,
						'features' => $val['permalink'],
						'sku' => $val['studio_id'],
                        'genre' => $val['genre_val']
					));
			}
			$documents=array();
			$cntMovie = count($search_items);
			for ($i = 0; $i < $cntMovie; $i++) {
				foreach ($search_items[$i] as $item => $fields) {
					$part = new Apache_Solr_Document();
					foreach ($fields as $key => $value) {
						if (is_array($value)) {
							foreach ($value as $datum) {
								$part->setMultiValue($key, $datum);
							}
						} else {
							$part->$key = $value;
						}
					}
					$documents[] = $part;
				}
			}
			 try {
				$solr->addDocuments($documents);
				$solr->commit();
				$solr->optimize();
			} catch (Exception $e) {
				echo $e->getMessage();exit;
			}
			print_r($documents);
			echo "Added===".$offset."==".$cntMovie." ---Out of ".$totalCount."<br/>";
			unset($search_items);unset($documents);
			$search_items = array();$documents=array();
		}
		unset($search_items);unset($documents);
		$search_items = array();$documents=array();
		$totalCount = 1;
		// Adding data for products 
		for($itr=0; $itr<$totalCount; $itr++){
			$offset = $itr*1000;
			$command = Yii::app()->db->createCommand()
					->select('SQL_CALC_FOUND_ROWS (0),id,studio_id,name,uniqid,permalink,sku,custom_fields')
					->from('pg_product ')
					->where('status !=2')
					->limit(1000,$offset);
		
			$data4 = $command->queryAll();
			
			$count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
			$totalCount = ceil($count/1000);

			foreach($data4 AS $key=>$val){
				$uniq_id = Yii::app()->common->generateUniqNumber();
				$search_items[] = array($val['permalink'] => array(
						'id' => $uniq_id,
						'content_id' => $val['id'],
						'name' =>$val['name'],
						'stream_id' => '',
						'stream_uniq_id' => $val['uniqid'],
						'is_episode' => '',
						'title' => $val['permalink'],
						'cat' => 'muvikart',
						'features' => $val['permalink'],
						'product_sku' => $val['sku'],
						'format' => $val['custom_fields'],
						'sku' => $val['studio_id']
                        
					));
			}
			
			$documents=array();
			$cntMovie = count($search_items);
			for ($i = 0; $i < $cntMovie; $i++) {
				foreach ($search_items[$i] as $item => $fields) {
					$part = new Apache_Solr_Document();
					foreach ($fields as $key => $value) {
						if (is_array($value)) {
							foreach ($value as $datum) {
								$part->setMultiValue($key, $datum);
							}
						} else {
							$part->$key = $value;
						}
					}
					$documents[] = $part;
				}
			}
			 try {
				$solr->addDocuments($documents);
				$solr->commit();
				$solr->optimize();
			} catch (Exception $e) {
				echo $e->getMessage();exit;
			}
			print_r($documents);
			echo "Added=== Physical product:=".$offset."==".$cntMovie." ---Out of ".$totalCount."<br/>";
			unset($search_items);unset($documents);
			$search_items = array();$documents=array();
		}	
			
		unset($search_items);unset($documents);
		$search_items = array();$documents=array();
		$totalCount = 1;
		
		$celebData = Celebrity::model()->findAll();
		foreach ($celebData AS $ck => $cv) {
			$uniq_id = Yii::app()->common->generateUniqNumber();
			$celeb_item = array($cv->permalink => array(
					'id' => $uniq_id,
					'content_id' => $cv->id,
					'name' => $cv->name,
					'title' => $cv->permalink,
					'stream_id' => '',
					'stream_uniq_id' => '',
					'is_episode' => 0,
					'cat' => 'star',
					'sku' => $cv->studio_id,
					'features' => 'star'
				)
			);
			array_push($search_items, $celeb_item);
		}
        $documents = array();
        $cnt = count($search_items);
		
        for ($i = 0; $i < $cnt; $i++) {
            foreach ($search_items[$i] as $item => $fields) {
				$part = new Apache_Solr_Document();
                foreach ($fields as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $datum) {
                            $part->setMultiValue($key, $datum);
                        }
                    } else {
                        $part->$key = $value;
                    }
                }
                $documents[] = $part;
            }
        }
        try {
            //print_r($documents);exit;
            $solr->addDocuments($documents);
            $solr->commit();
            $solr->optimize();
            echo "added";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
		print_r($documents);exit;
        print "<pre>";echo "Solr data added successfully";exit;
    }
	
    public function actionindex() {
        $this->layout = false;
        $this->render('index');
    }

    public function actionsearch() {
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        $solr = new Apache_Solr_Service('localhost', '8983', '/solr');

        if (!$solr->ping()) {
            echo 'Solr service not responding.';
            exit;
        }
        $offset = 0;
        $limit = 5;
        $studio_id = (isset($_REQUEST['sd_user']) && $_REQUEST['sd_user'] > 0) ? $_REQUEST['sd_user'] : Yii::app()->common->getStudiosId();
		$term = trim($_REQUEST['term']);
        $theme = Studio::model()->find(array('select' => 'parent_theme', 'condition' => 'id=:studio_id', 'params' => array(':studio_id' => $studio_id)));
        if ($theme->parent_theme == 'physical') {
        }else{
            
            $solrnow_time=date("Y-m-d", strtotime(gmdate("Y-m-d H:i:s"))).'T'.date("H:i:s", strtotime(gmdate("Y-m-d H:i:s"))).'Z';          
            $query = "cat: content AND (name: " . $term . "* OR genre: " . $term . "*) AND sku:" . $studio_id . " AND  NOT publish_date:* ";           
        $response = $solr->search($query, $offset, $limit);
		if ($response->getHttpStatus() == 200) {
			if ($response->response->numFound > 0) {
				foreach ($response->response->docs as $doc) {
					$tr = $doc->cat;
					$res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
				}
			}
		}
            $query = "cat: content AND (name: " . $term . "* OR genre: " . $term . "*) AND sku:" . $studio_id . " AND publish_date:[* TO ".$solrnow_time."] ";           
            $response = $solr->search($query, $offset, $limit);         
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                            $tr = $doc->cat;
                            $res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
                        }
                    }
                }
		
		$cat = $this->escapeSolrValueWithoutSpace('content - Episode');
		$query = "cat: ".$cat." AND name: " . $term . "* AND sku:" . $studio_id . " ";
        $response = $solr->search($query, $offset, $limit);
		if ($response->getHttpStatus() == 200) {
			if ($response->response->numFound > 0) {
				foreach ($response->response->docs as $doc) {
					$tr = $doc->cat;
					$res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
				}
			}
		}
		$query = "cat: livestream AND name: " . $term . "* AND sku:" . $studio_id . " ";
        $response = $solr->search($query, $offset, $limit);
		if ($response->getHttpStatus() == 200) {
			if ($response->response->numFound > 0) {
				foreach ($response->response->docs as $doc) {
					$tr = $doc->cat;
					$res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
				}
			}
		}
		$query = "cat:star AND name: " . $term . "* AND sku:" . $studio_id;
        $response = $solr->search($query, $offset, $limit);
        if ($response->getHttpStatus() == 200) {
            if ($response->response->numFound > 0) {
                foreach ($response->response->docs as $doc) {
                    $tr = $doc->cat;
                    $res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
                }
            }
        }
     //  if($theme->parent_theme == 'audio-streaming'){
            $query = "cat:playlist AND name: " . $term . "* AND sku:" . $studio_id;
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $tr = $doc->cat;
                        $res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
					}        
                }
            }
	//	}
        
        
        if (Yii::app()->general->getStoreLink()) {
        $query = "cat:muvikart AND (name: " . $term . "* OR product_sku:" . $term . "* OR genre: " . $term . "* OR format:" . $term . "*) AND sku:" . $studio_id ." AND  NOT publish_date:* ";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $tr = 'search results:';
                        $res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
                    }
                }
            }
        $query = "cat:muvikart AND (name: " . $term . "* OR product_sku:" . $term . "* OR genre: " . $term . "* OR format:" . $term . "*) AND sku:" . $studio_id ." AND publish_date:[* TO ".$solrnow_time."]";
        $response = $solr->search($query, $offset, $limit);
        if ($response->getHttpStatus() == 200) {
            if ($response->response->numFound > 0) {
                foreach ($response->response->docs as $doc) {
                        $tr = 'search results:';
                        $res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
        }
                }
            }
        }
      
        if ($theme->parent_theme == 'physical') {
            $query = "cat:star AND name: " . $term . "* AND sku:" . $studio_id;
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $tr = $doc->cat;
                        $res[] = array('label' => $doc->name, 'id' => $doc->content_id, 'title' => $doc->title, 'category' => ucfirst($tr), 'content_types' => $doc->features, 'stream_id' => $doc->stream_id, 'stream_uniq_id' => $doc->stream_uniq_id, 'is_episode' => $doc->is_episode);
                    }
                }
            }
        }
       
        $res = array_unique($res, SORT_REGULAR);
        echo json_encode(@$res);
        exit;
    }
    }
    public function actionshow_all() {
		$studio_id = Yii::app()->common->getStudiosId();
		$content = Yii::app()->general->content_count($studio_id);
        $this->layout = 'column1';
        //$this->render('new_search');exit;
        $res = array();
        $tv_res = array();
        $movie_res = array();
        $data = array();
        $all_data = array();
        $solr = new Apache_Solr_Service('localhost', '8983', '/solr');

        if (!$solr->ping()) {
            echo 'Solr service not responding.';
            exit;
        }
        $logdata = '';
        $key = 0;
        $offset = 0;
        $limit = 24;
        $stream_ids = '';
		$studioData = $this->studio;
		if(@$studioData->is_csrf_enabled){
			if(@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']){
				unset($_SESSION['csrfToken']);
                $this->generateCsrfToken();
			}else{
				$url = Yii::app()->createAbsoluteUrl();
				header("HTTP/1.1 301 Moved Permanently");
		        header('Location: '.$url);
                        exit();
			}
		}
        if(isset($_REQUEST['studio_user']) && is_numeric($_REQUEST['studio_user'])){
            $studio_user = $_REQUEST['studio_user'];
        }else{
            $studio_user = Yii::app()->common->getStudiosId();
        }
		$purifier = new CHtmlPurifier();
		$_REQUEST['search_field'] = $purifier->purify($_REQUEST['search_field']);
		
        $searck_key = $this->escapeSolrValueWithoutSpace(strtolower(trim($_REQUEST['search_field'])));

        $searck_key_words = array_unique(explode(" ", $searck_key));
		foreach($searck_key_words as $skey=>$sval){
			if(trim($sval)){
				$finalArr[] = trim($sval);
			}
		}
 
        $add_extra_search=(count($finalArr)>1)?implode(" OR name: ",$finalArr):$finalArr[0];
        $add_extra_genre_search=(count($finalArr)>1)?implode(" OR genre: ",$finalArr):$finalArr[0];
                
        $query = '';
        $theme = Studio::model()->find(array('select' => 'parent_theme', 'condition' => 'id=:studio_id', 'params' => array(':studio_id' => $studio_user)));
        if ($theme->parent_theme == 'physical') {
            
        } else {
	$sql = "SELECT COUNT(movie_id) AS cnt, movie_id FROM video_logs WHERE studio_id =" . $studio_user . " GROUP BY movie_id";

             $videoLog = Yii::app()->db->createCommand($sql)->queryAll();
             $viewcount = array();
            foreach ($videoLog as $key => $row) {
				$viewcount[$row['movie_id']] = $row['cnt'];
			}
		
            $query .= " (cat:content AND name: " . $searck_key . "* AND sku:" . $studio_user . ") OR (cat:content - Episode AND name: " . $searck_key . "* AND sku:" . $studio_user . ") OR (cat:livestream AND name: " . $searck_key . "* AND sku:" . $studio_user . ")";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
               // print_r( $response->getRawResponse() );exit;
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        //Added to disallow disabled content
                        if (isset($doc->content_id) && $doc->content_id > 0)
                            $logdata[$key]['object_id'] = $doc->content_id;
                        else
                            $logdata[$key]['object_id'] = $doc->id;
							$logdata[$key]['object_category'] = $doc->features;
							$logdata[$key]['studio_id'] = $doc->sku;
							$logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                        $key++;
                        if (isset($doc->content_id) && $doc->content_id > 0) {
                        $all_stream6[] = $doc->stream_id;
                            $content6[] = $contents[] = $doc->content_id;
                        } else {
                            $content6[] = $contents[] = $doc->id;
                        }
                        if (isset($doc->is_episode) && $doc->is_episode == 1) {
                            $stream_ids[] = $doc->stream_id;
                        }
                    }
                }
            } else {
                echo $response->getHttpStatusMessage();
            }
           
            $content6 = array_unique($content6);
            $filtered_data6=array();
           
            //filter the content with future live dates
            if (count($all_stream6)) {
                $all_stream6 = array_unique($all_stream6);
                $all_streamid = implode(',', $all_stream6);
                $stream = movieStreams::model()->findAll(array("condition"=>'id IN (' . $all_streamid . ') AND (content_publish_date <= NOW() OR content_publish_date IS NULL ) AND is_episode=0'));
                
                foreach ($stream as $key => $val) {
                   $filtered_data6[] = $val->movie_id;
                }
                
                if(count($filtered_data6)>0 && count($content6)>0)
                    $content6 = array_intersect($content6, $filtered_data6);
                else                           
                  $content6=array();
         
           
                foreach ($content6 as $k => $v) {
                    $view_count6[] = $viewcount[$v['movie_id']];
                }
                array_multisort($view_count6, SORT_DESC, $content6);
                foreach ($content6 as $key => $val) {
                    $all_data[] = $val;
                }
            }
            $query = "(cat:content AND genre: " . $searck_key . "* AND sku:" . $studio_user . ")";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                // print_r( $response->getRawResponse() );
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        //Added to disallow disabled content
                        if (isset($doc->content_id) && $doc->content_id > 0)
                            $logdata[$key]['object_id'] = $doc->content_id;
                        else
                            $logdata[$key]['object_id'] = $doc->id;

                        $logdata[$key]['object_category'] = $doc->features;
                        $logdata[$key]['studio_id'] = $doc->sku;
                        $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                        $key++;
                        if (isset($doc->content_id) && $doc->content_id > 0) {
                            $all_stream5[] = $doc->stream_id;
                            $content5[] = $contents[] = $doc->content_id;
                        } else {
                            $content5[] = $contents[] = $doc->id;
                        }

                        if (isset($doc->is_episode) && $doc->is_episode == 1) {
                            $stream_ids[] = $doc->stream_id;
                        }
                    }
                }
            } else {
                echo $response->getHttpStatusMessage();
            }
           
            $content5 = array_unique($content5);
            //filter the content with future live dates
            if(count($all_stream5)){
                $all_stream5 = array_unique($all_stream5);
                $all_streamid = implode(',', $all_stream5);
                $stream = movieStreams::model()->findAll(array("condition"=>'id IN (' . $all_streamid . ') AND (content_publish_date <= NOW() OR content_publish_date IS NULL ) AND is_episode=0'));

                foreach ($stream as $key => $val) {
                    $filtered_data5[] = $val->movie_id;
                }
                if(count($filtered_data5)>0  && count($content5)>0)
                    $content5 = array_intersect($content5, $filtered_data5);
            else
            $content5=array();
                foreach ($content5 as $k => $v) {
                    $view_count5[] = $viewcount[$v['movie_id']];
                }
                array_multisort($view_count5, SORT_DESC, $content5);
                foreach ($content5 as $key => $val) {
                    $all_data[] = $val;
                }
            }
            
            $query = "(cat:star AND name: " . $searck_key . "* AND sku:" . $studio_user . ")";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                // print_r( $response->getRawResponse() );
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $celbId[] = $doc->content_id;
                        if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                            $logdata[$key]['object_id'] = $doc->content_id;
                        $logdata[$key]['object_category'] = $doc->features;
                        $logdata[$key]['studio_id'] = $doc->sku;
                        $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                        $key++;
                        }
                    }
                }
            }
            if ($celbId) {
                $celbId=array_unique($celbId);
                $dbcon = Yii::app()->db;
                $data_cast = $dbcon->createCommand('SELECT movie_id FROM movie_casts WHERE celebrity_id IN(\'' . implode(',', $celbId) . '\') AND product_id IS NULL')->queryAll();
                foreach ($data_cast AS $key => $val) {
                    $content4[] = $contents[] = $val['movie_id'];
                }
            }
            $content4 = array_unique($content4);
            foreach ($content4 as $k => $v) {
                $view_count4[] = $viewcount[$v['movie_id']];
            }
            array_multisort($view_count4, SORT_DESC, $content4);
            foreach ($content4 as $key => $val) {
                $all_data[] = $val;
            }
   
            $query = "(cat:content AND (name: " . $add_extra_search . ") AND sku:" . $studio_user . ") OR (cat:content - Episode AND (name: " . $add_extra_search . ") AND sku:" . $studio_user . ") OR (cat:livestream AND (name: " . $add_extra_search . ") AND sku:" . $studio_user . ")";
           
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                // print_r( $response->getRawResponse() );
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        //Added to disallow disabled content
                        if (isset($doc->content_id) && $doc->content_id > 0)
                            $logdata[$key]['object_id'] = $doc->content_id;
                        else
                            $logdata[$key]['object_id'] = $doc->id;

                        $logdata[$key]['object_category'] = $doc->features;
                        $logdata[$key]['studio_id'] = $doc->sku;
                        $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                        $key++;
                        if (isset($doc->content_id) && $doc->content_id > 0) {
                            $all_stream1[] = $doc->stream_id;
                            $content1[] = $contents[] = $doc->content_id;
                        } else {
                            $content1[] = $contents[] = $doc->id;
                        }
                        if (isset($doc->is_episode) && $doc->is_episode == 1) {
                            $stream_ids[] = $doc->stream_id;
                        }
                    }
                }
            } else {
                echo $response->getHttpStatusMessage();
            }
          
            $content1 = array_unique($content1);
            //filter the content with future live dates
            if(count($all_stream1)){
                $all_stream1 = array_unique($all_stream1);
                $all_streamid = implode(',', $all_stream1);
                $stream = movieStreams::model()->findAll(array("condition"=>'id IN (' . $all_streamid . ') AND (content_publish_date <= NOW() OR content_publish_date IS NULL ) AND is_episode=0'));

                foreach ($stream as $key => $val) {
                    $filtered_data1[] = $val->movie_id;
                }           
                if(count($filtered_data1)>0  && count($content1)>0)
                    $content1 = array_intersect($content1, $filtered_data1);
            else 
                  $content1=array();
            }
            
            foreach ($content1 as $k => $v) {
               $view_count1[] = $viewcount[$v['movie_id']];
            }
            array_multisort($view_count1, SORT_DESC, $content1);
            foreach ($content1 as $key => $val) {
                $all_data[] = $val;
            }
            $query = "cat:star AND (name: " . $add_extra_search . " ) AND sku:" . $studio_user . " ";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $celId[] = $doc->content_id;
                        if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                            $logdata[$key]['object_id'] = $doc->content_id;
                            $logdata[$key]['object_category'] = $doc->features;
                            $logdata[$key]['studio_id'] = $doc->sku;
                            $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                            $key++;
                        }
                    }
                }
            }
            if ($celId) {
                $celId=array_unique($celId);
                $dbcon = Yii::app()->db;
                $data_cast = $dbcon->createCommand('SELECT movie_id FROM movie_casts WHERE celebrity_id IN(\'' . implode(',', $celId) . '\') AND product_id IS NULL')->queryAll();
                foreach ($data_cast AS $key => $val) {
                    $content2[] = $contents[] = $val['movie_id'];
                }
            }
            
            $content2 = array_unique($content2);
          
            foreach ($content2 as $k => $v) {
               $view_count2[] = $viewcount[$v['movie_id']];
            }
            array_multisort($view_count2, SORT_DESC, $content2);
            foreach ($content2 as $key => $val) {
                $all_data[] = $val;
            }
            $query = "cat:content AND (genre:" . $add_extra_genre_search . " ) AND sku:" . $studio_user . " ";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                // print_r( $response->getRawResponse() );
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        //Added to disallow disabled content
                        if (isset($doc->content_id) && $doc->content_id > 0)
                            $logdata[$key]['object_id'] = $doc->content_id;
                        else
                            $logdata[$key]['object_id'] = $doc->id;

                        $logdata[$key]['object_category'] = $doc->features;
                        $logdata[$key]['studio_id'] = $doc->sku;
                        $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                        $key++;
                        if (isset($doc->content_id) && $doc->content_id > 0) {
                           $all_stream3[] = $doc->stream_id;
                            $content3[] = $contents[] = $doc->content_id;
                        } else
                            $content3[] = $contents[] = $doc->id;

                        if (isset($doc->is_episode) && $doc->is_episode == 1) {
                            $stream_ids[] = $doc->stream_id;
                        }
                    }
                }
            } else {
                echo $response->getHttpStatusMessage();
            }
            
            $content3 = array_unique($content3);
            if(count($all_stream3)){
                //filter the content with future live dates
                $all_stream3 = array_unique($all_stream3);
                $all_streamid = implode(',', $all_stream3);
                $stream = movieStreams::model()->findAll(array("condition"=>'id IN (' . $all_streamid . ') AND (content_publish_date <= NOW() OR content_publish_date IS NULL ) AND is_episode=0'));

                foreach ($stream as $key => $val) {
                    $filtered_data3[] = $val->movie_id;
                }
                if(count($filtered_data3)>0  && count($content3)>0)
                    $content3 = array_intersect($content3, $filtered_data3);          
            else
            $content3=array();
            }
          
            foreach ($content3 as $k => $v) {
                $view_count3[] = $viewcount[$v['movie_id']];
            }
            array_multisort($view_count3, SORT_DESC, $content3);
            foreach ($content3 as $key => $val) {
                $all_data[] = $val;
            }
          
            $all_data = array_unique($all_data);
            foreach ($all_data as $key=>$mid) {
                $data[] = Yii::app()->general->getContentData($mid);
            }
            foreach ($data as $k => $v) {
                if($v['title']=='')
                 unset($data[$k]);
            } 
            if ($logdata) {
                foreach ($logdata AS $k => $v) {
                    $this->actionSiteSearchLog($v);
                }
            } else {
                if (@trim(@$_REQUEST['search_field'])) {
                    $logdata['search_string'] = trim($_REQUEST['search_field']);
                    $this->actionSiteSearchLog($logdata);
                }
            }
            $contents = array_values(array_unique($contents));
            if ($stream_ids) {
                $stream_ids = array_values(array_unique($stream_ids));
            }
            
          // if($theme->parent_theme == 'audio-streaming'){
                $playlistdata = array();
                $playlist_key = 0;
                
                $query = "cat:playlist AND name: " . $searck_key . "* AND sku:" . $studio_user;
                $response = $solr->search($query, $offset, $limit);
                if ($response->getHttpStatus() == 200) {
                    if ($response->response->numFound > 0) {
                        foreach ($response->response->docs as $doc) {
                            $playlistId[$playlist_key]=$doc->id;
                            $playlistdata[$playlist_key]['id'] = $doc->id;
                            $playlistdata[$playlist_key]['playlist_name'] = $doc->name;
                            $playlistdata[$playlist_key]['playlist_permalink'] = $doc->features;
                            $playlistdata[$playlist_key]['studio_id'] = $doc->sku;
                            $playlistdata[$playlist_key]['user_id'] = $doc->user_id;
                            $playlistdata[$playlist_key]['content_category_value'] = $doc->content_id;
                            $playlist_key++;
						}
                    }
                }
                $query = "cat:playlist AND (name: " . $add_extra_search . " ) AND sku:" . $studio_user;
                $response = $solr->search($query, $offset, $limit);
                if ($response->getHttpStatus() == 200) {
                    if ($response->response->numFound > 0) {
                        foreach ($response->response->docs as $doc) {
                            $playlistId[$playlist_key]=$doc->id;
                            $playlistdata[$playlist_key]['id'] = $doc->id;
                            $playlistdata[$playlist_key]['playlist_name'] = $doc->name;
                            $playlistdata[$playlist_key]['playlist_permalink'] = $doc->features;
                            $playlistdata[$playlist_key]['studio_id'] = $doc->sku;
                            $playlistdata[$playlist_key]['user_id'] = $doc->user_id;
                            $playlistdata[$playlist_key]['content_category_value'] = $doc->content_id;
                            $playlist_key++;
                        }
                    }
                }
                $playlistId = array_unique($playlistId);
                foreach($playlistId as $key=>$value){
                    $newPlaylist['list'] = array(
                        'id'=>$value,
                        'playlist_name' => $playlistdata[$key]['playlist_name'],
                        'playlist_permalink' => HOST_URL . '/' . $playlistdata[$key]['playlist_permalink'],
                        'studio_id' => $playlistdata[$key]['studio_id'],
                        'user_id' => $playlistdata[$key]['user_id'],
                        'content_category_value'=> $playlistdata[$key]['content_category_value'],
						'is_playlist'=> 1,
						'is_search' => 1
                    );
                    $newPlaylist['poster_playlist'] = $this->getPoster($value,'playlist_poster');
                    $newPlaylist['is_playlist'] = '1';
                    $data[] = $newPlaylist;
                }
                
           // }
            }
        if (Yii::app()->general->getStoreLink()) {
            $query = "cat:muvikart AND (name: " . $searck_key . "* OR genre: " . $searck_key . "* OR product_sku:" . $searck_key . "* OR format:" . $searck_key . "*) AND sku:" . $studio_user;
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $physicalId[] = $doc->content_id;
                        if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                            $logdata[$key]['object_id'] = $doc->content_id;
                            $logdata[$key]['object_category'] = $doc->features;
                            $logdata[$key]['studio_id'] = $doc->sku;
                            $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
							$logdata[$key]['is_physical'] = 1;
							$logdata[$key]['is_search'] = 1;
                            $key++;
                        }
                    }
                }
            }
        }

        if($theme->parent_theme == 'physical'){ 
            $query = "cat:star AND (name: " . $searck_key . "* ) AND sku:" . $studio_user . " ";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $celphysicalId[] = $doc->content_id;
                        if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                            $logdata[$key]['object_id'] = $doc->content_id;
                            $logdata[$key]['object_category'] = $doc->features;
                            $logdata[$key]['studio_id'] = $doc->sku;
                            $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                            $key++;
                        }
                    }
                }
            }
            $query = "cat:star AND (name: " . $add_extra_search . " ) AND sku:" . $studio_user . " ";
            $response = $solr->search($query, $offset, $limit);
            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    foreach ($response->response->docs as $doc) {
                        $celphysicalId[] = $doc->content_id;
                        if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                            $logdata[$key]['object_id'] = $doc->content_id;
                            $logdata[$key]['object_category'] = $doc->features;
                            $logdata[$key]['studio_id'] = $doc->sku;
                            $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                            $key++;
                        }
                    }
                }
            }
            if ($celphysicalId) {
                $celphysicalId=array_unique($celphysicalId);
                $dbcon = Yii::app()->db;
                $data_cast = $dbcon->createCommand('SELECT product_id FROM movie_casts WHERE celebrity_id IN(' . implode(',', $celphysicalId) . ') AND product_id!=0')->queryAll();
                foreach ($data_cast AS $key => $val) {
                    $physicalId[] = $val['product_id'];
                }
            }
        }
                    if ((isset($content) && ($content['content_count'] & 4))) {
                            if (isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == true) {
                                    $this->layout = false;
                            }
                    }else{
        $this->layout = 'main';
                    }
        $physicalId = array_unique($physicalId);
        $default_currency_id = $this->studio->default_currency_id;
        foreach ($physicalId as $item) {
            $product = Yii::app()->db->createCommand("SELECT id,name as title,permalink,sale_price,currency_id,is_preorder,status, custom_fields FROM pg_product WHERE  id=" . $item . " AND( publish_date IS NULL OR publish_date <= now() )")->queryROW();
            if ($product) {
                if (Yii::app()->common->isGeoBlockPGContent($product["id"])) {//Auther manas@muvi.com
                    $product['permalink'] = Yii::app()->getbaseUrl(true) . '/' . $product['permalink'];
                    $product['poster'] = PGProduct::getpgImage($product['id'], 'standard');
					$product['is_physical'] = 1;
					$product['name'] = $product["title"];
					$product['is_search'] = 1;
                    if ($product['poster'] == '/img/no-image.jpg') {
                        $product['poster'] = '/img/No-Image-Vertical.png';
                    }
                    $tempprice = Yii::app()->common->getPGPrices($product['id'], $default_currency_id);
                    if (!empty($tempprice)) {
                        $product['price'] = $tempprice['price'];
                        $product['currency_id'] = $tempprice['currency_id'];
                    }else{
                        $product['price'] = $product['sale_price'];
                        $product['currency_id'] = $product['currency_id'];   
                    }

                    $data[] = $product;
                }
            }
        }

        //Start the pagination
        $items_per_page = Yii::app()->general->itemsPerPage();
        $page_size = $limit = $items_per_page;
        $offset = 0;
        $item_count = count($contents);
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $limit;
        } else if (isset($_REQUEST['p'])) {
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
        } else {
            $page_number = 1;
        }
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $page_url = Yii::app()->general->full_url($url, '?p=');
        $page_url = Yii::app()->general->full_url($page_url, '&p=');
        $pg = new bootPagination();
        $pg->pagenumber = $page_number;
        $pg->pagesize = $page_size;
        $pg->totalrecords = $item_count;
        $pg->showfirst = true;
        $pg->showlast = true;
        $pg->paginationcss = "pagination-normal";
        $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
        $pg->defaultUrl = $page_url;
        if (strpos($page_url, '?') > -1)
            $pg->paginationUrl = $page_url . "&p=[p]";
        else
            $pg->paginationUrl = $page_url . "?p=[p]";
        $pagination = $pg->process();

        //Assign the View
        $this->render('index', array('pagination' => $pagination, 'contents' => json_encode($data), 'item_count' => count($data)));
    }

    /**
     * @method public emptySolr() To reindex solr by deleting all its data
     * @author GDR<support@muvi.com>
     * @return boolen true/false
     */
    function actionEmptySolr() {
		echo "Only For Muvi Use";exit;
        $solr = new Apache_Solr_Service('localhost', '8983', '/solr');
        $solr->deleteByQuery('*: *');
        $solr->commit();
        $solr->optimize();
		echo "Solr is empty Now";
    }

    /**
     * @method public emptySolr() To reindex solr by deleting all its data
     * @author GDR<support@muvi.com>
     * @return boolen true/false
     */
    function actionSiteSearchLog($data = array()) {
        if (($data && $data['search_string']) || (isset($_REQUEST) && $_REQUEST['search_string'])) {
            if (empty($data)) {
                $data = $_REQUEST;
            }
            $searchLogs = new SearchLog();
            $searchLogs->studio_id = $data['studio_id'] ? $data['studio_id'] : Yii::app()->common->getStudioId();
            $searchLogs->search_string = $data['search_string'];
            $searchLogs->search_url = (isset($data['search_url']) && $data['search_url']) ? $data['search_url'] : $_SERVER['HTTP_REFERER'];
            $searchLogs->object_category = @$data['object_category'];
            $searchLogs->object_id = @$data['object_id'];
            $searchLogs->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : '';
            $searchLogs->created_date = gmdate('Y-m-d H:i:s');
            $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
            $searchLogs->save();
            if (@$_REQUEST['isAjax']) {
                echo 'success';
                exit;
            } else {
                return true;
            }
        }
    }
	protected function escapeSolrValue($string){
        $match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ');
        $replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ');
        $string = str_replace($match, $replace, $string);
        return $string;
    }
	protected function escapeSolrValueWithoutSpace($string){
        $match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';','/','%','#');
        $replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;','\\/','\\%','\\#');
        $string = str_replace($match, $replace, $string);
        return $string;
    }
	protected function getEpisodeInfos($stream_ids=''){
		if($stream_ids){
			$pdo_cond_arr =array(":studio_id"=>Yii::app()->common->getStudioId());
			$pdo_cond = " f.studio_id=:studio_id";
			$command = Yii::app()->db->createCommand()
			->select('f.id,f.uniq_id,f.name,f.ppv_plan_id,ms.is_converted,f.permalink,ms.id AS stream_id,ms.full_movie,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_story,ms.embed_id')
			->from('films f ,movie_streams ms ')
			->where('f.id = ms.movie_id AND ms.id IN ('.implode(',',$stream_ids).') AND '.$pdo_cond, $pdo_cond_arr)
			->limit(100,0);
			$records = $command->queryAll();
			return $records;
		}
	}
    public function actionAddPhysical() {
        /*$solrobj = new SolrFunctions();
        $solrobj->deleteSolrQuery("cat:muvikart");
        exit;*/
        error_reporting(E_ALL);
        $solr = new Apache_Solr_Service('localhost', '8983', '/solr');
        if (!$solr->ping()) {
            echo 'Solr service not responding.';
            exit;
        }
        
        $search_items = array();
        $pgconnection = Yii::app()->db;
        //$data = Film::model()->with(array('movie_streams' => array('alias' => 'ms', 'select' => 'ms.id as stream_id,is_episode,episode_title,episode_number,embed_id', 'joinType' => 'INNER JOIN', 'condition' => 'ms.movie_id = t.id'), 'studio_content_type'))->findAll('name!=\'\' AND name IS NOT NULL AND status = 1');
        $totalCount = 1;
        echo "<pre>";
        for ($itr = 0; $itr < $totalCount; $itr++) {
            $offset = $itr * 1000;
            //PDO Query
            $command = Yii::app()->db->createCommand("SELECT SQL_CALC_FOUND_ROWS * FROM `pg_product` WHERE is_deleted=0 AND status != 2 LIMIT $offset,1000");
            $data1 = $command->queryAll();            
            $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();            
            $totalCount = ceil($count / 1000);
            foreach ($data1 AS $key => $val) {                
                $uniq_id = Yii::app()->common->generateUniqNumber();
                $contentName = $val['name'];
                $contentTypeName = 'muvikart';
                $search_items[] = array($val['permalink'] => array(
                        'id' => $uniq_id,
                        'content_id' => $val['id'],
                        'name' => $contentName,
                        'stream_id' => '',
                        'stream_uniq_id' => $uniq_id,
                        'is_episode' => 0,
                        'title' => $val['permalink'],
                        'cat' => $contentTypeName,
                        'features' => $val['permalink'],
                        'sku' => $val['studio_id'],
                        'product_sku' => $val['sku'],
                        'format' => $val['custom_fields']
                ));
            }
            $documents = array();
            $cntMovie = count($search_items);
            for ($i = 0; $i < $cntMovie; $i++) {
                foreach ($search_items[$i] as $item => $fields) {
                    $part = new Apache_Solr_Document();
                    foreach ($fields as $key => $value) {
                        if (is_array($value)) {
                            foreach ($value as $datum) {
                                $part->setMultiValue($key, $datum);
                            }
                        } else {
                            $part->$key = $value;
                        }
                    }
                    $documents[] = $part;
                }
            }
            try {
                $solr->addDocuments($documents);
                $solr->commit();
                $solr->optimize();
            } catch (Exception $e) {
                echo $e->getMessage();
                exit;
            }
            print_r($documents);
            echo "Added===" . $offset . "==" . $cntMovie . " ---Out of " . $totalCount . "<br/>";
            unset($search_items);
            unset($documents);
            $search_items = array();
            $documents = array();
        }
    }
}
