<?php
require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
class StoreController extends Controller {

    public $defaultAction = 'GoodsLibrary';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);

        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            $this->redirect(array('/index.php/'));
        }elseif(!Yii::app()->general->getStoreLink()){
            $this->redirect(array('/index.php/'));
        }

        return true;
    }

    public function actionGoodsLibrary() {
        $this->breadcrumbs = array("Muvi Kart","Store");
        $this->headerinfo = "Goods Library";
        $this->pageTitle = "Muvi | Muvi Kart";

        $pdo_cond_arr = array(":studio_id" => Yii::app()->user->studio_id);
        $pdo_cond = " p.studio_id=:studio_id ";
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {
            $search_form = $_REQUEST['searchForm'];
            if (isset($search_form) && $search_form['search_text'] == '') {
                $url = $this->createUrl('/store');
                $this->redirect($url);
            } else {
                $searchData = $_REQUEST['searchForm'];
                if ($searchData['search_text']) {
                    $pdo_cond .= " AND (p.name LIKE :search_text OR p.sku LIKE :search_text)";
                    $pdo_cond_arr[':search_text'] = "%".strtolower(trim($searchData['search_text'])) . "%";
                }
            }
        }

        //Pagination Implimented ..
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0), p.*')
                ->from('pg_product p')
                ->where(' p.is_deleted = 0 AND ' . $pdo_cond, $pdo_cond_arr)
                ->order(' p.id desc')
                ->limit($page_size, $offset);
        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $pages = new CPagination($count);

        $pages->setPageSize($page_size);
        $url = Yii::app()->common->getPosterCloudFrontPath($this->studio->id);
        foreach ($data AS $key => $orderval) {

            $command = Yii::app()->db->createCommand()
                    ->select('SUM(quantity) AS totquantity, SUM(sub_total) AS totsubtotal')
                    ->from('pg_order_details ')
                    ->where('product_id = ' . $orderval['id']);
            $dataorder = $command->queryAll();
            $data[$key]['totquantity'] = $dataorder[0]['totquantity'];
            $data[$key]['totsubtotal'] = $dataorder[0]['totsubtotal'];

            //get the content name
            if ($orderval['product_type'] == 0) {
                $data[$key]['contentname'] = "Standalone";
            } else {
                $content = Yii::app()->db->createCommand("SELECT movie_id FROM pg_product_content WHERE product_id= " . $orderval['id'])->queryAll();
                $contentname = "Linked to <br />";
                foreach ($content as $val) {
                    $content = Film::model()->findByPk($val);
                    $content_name = $content->name;
                    $permalink = $content->permalink;
                    $contentname .= "<a target='_blank' href='http://" . $this->studio->domain . "/" . $permalink . "'>" . $content_name . "</a><br />";
                }
                $data[$key]['contentname'] = $contentname;
            }
            $dbprimg = PGProductImage::model()->find(array("condition" => 'product_id = ' . $orderval['id']));
            if ($dbprimg) {
                $pgposter = $url . '/system/pgposters/' . $orderval['id'] . '/thumb/' . urlencode($dbprimg['name']);
                if (false === file_get_contents($pgposter)) {
                    $pgposter = '/img/No-Image-Vertical-Thumb.png';
                }
            } else {
                $pgposter = '/img/No-Image-Vertical-Thumb.png';
            }
            $data[$key]['pgposter'] = $pgposter;
            $default_currency_id = $this->studio->default_currency_id;
            $tempprice = Yii::app()->common->getPGPrices($orderval['id'], $default_currency_id);
            if(!empty($tempprice)){
                $data[$key]['sale_price'] = $tempprice['price'];
                $data[$key]['currency_id'] = $tempprice['currency_id'];
            }
            $getMovieStream = PGVideo::model()->findByAttributes(array('studio_id' => $this->studio->id, 'product_id' => $orderval['id']));
            if(!empty($getMovieStream) && $getMovieStream['thirdparty_url'] !=''){
                $data[$key]['thirdparty_url'] = $getMovieStream['thirdparty_url'];
            }
        }
        // simulate the effect of LIMIT in a sql query
        $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
        $sample = range($pages->offset + 1, $end);
        $studio_id = $this->studio->id;
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            $pcontent = Yii::app()->general->getPartnersContentIds();
            $contentid = $pcontent['movie_id'];
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id,"$contentid",Yii::app()->user->id);
        }else{
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id);
            $geoexist = StudioContentRestriction::model()->exists('studio_id=:studio_id',array(':studio_id' => $studio_id));
        }
        $settings = Yii::app()->general->getPgSettings();        
        $this->render('list', array('data' => $data, 'studio' => $studio, 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sample' => @$sample, 'sort' => @$sort,'all_videos' => @$all_videos,'geoexist' => @$geoexist,'view_trailer' => @$settings['view_trailer']));
    }

    public function actionAddItem() {
        $this->breadcrumbs = array('Muvi Kart','Store' => array('/store'), "Add Items");
        $this->headerinfo = "Add Items";
        $this->pageTitle = "Muvi | Muvi Kart > Add Items";
        $studio_id = $this->studio->id;
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $currency_code = PGProduct::getStudioCurrencyCode($this->studio);
        $customComp = new CustomForms();
		$arg['content_type'] = 0;
        $arg['is_child'] = 0;
        $customData = $customComp->getCustomMetadata($studio_id,5, $arg);        
        $settings = Yii::app()->general->getPgSettings();
        if(@$settings['enable_category']==1){
            $language_id = $this->language_id;
            $sql = "SELECT * FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
            $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
            $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        }else{
            $contentCategories = 0;
        }
        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $shipping_size = CHtml::listData($sizes, 'size_unique_name', 'size');
		$productize = Yii::app()->general->checkProductizeExtension($studio_id);
		$formdata = Yii::app()->general->formlist($studio_id);
        $this->render('additem', array('all_images' => $all_images, 'currency_code' => $currency_code,'customData' => $customData,'enable_category' => @$settings['enable_category'],'contentCategories' => @$contentCategories,'shipping_size' => @$shipping_size,'formdata'=>$formdata,'productize'=>$productize));
    }

    public function actiongetContentName() {
        $contentarray = Yii::app()->db->createCommand("SELECT id,name FROM films WHERE studio_id=" . $this->studio->id)->queryAll();
        foreach ($contentarray as $key => $value) {
            $array[$value['id']] = $value['name'];
        }
        echo json_encode($array);
        exit;
    }

    public function actionSaveItems() {
        $data = $_REQUEST['pg'];    
        $itModel = New PGProduct;
        $uniqid = Yii::app()->common->generateUniqNumber();
        $permalink = Yii::app()->general->generatePermalink(stripslashes($data['name']));
        $itModel->uniqid = $uniqid;
        $itModel->permalink = $permalink;
        $itModel->name = $data['name'];
        $itModel->description = $data['description'];
        $itModel->studio_id = Yii::app()->user->studio_id;
        $itModel->user_id = Yii::app()->user->id;
        $itModel->category_id = 0;
        $itModel->sub_cat_id = 0;
        $itModel->brand_id = 0;
        $itModel->sale_price = @$data['sale_price'];
        $itModel->purchase_price = 0.0;
        $itModel->discount = 0;
        $itModel->discount_type = 0;
        $itModel->discount_till_date = '0000-00-00';
        $itModel->shipping_cost = 0.0;
        $itModel->product_type = $data['product_type'];
        $itModel->currency_id = @$data['currency_id'];
        $itModel->units = '';
        $itModel->sku = $data['sku'];
        $itModel->custom_fields = $data['custom_fields'];
        $itModel->rating = $data['rating'];
        $itModel->video = '';
        $itModel->current_stock = 0;
        $itModel->release_date = $data['release_date'] ? date('Y-m-d', strtotime($data['release_date'])) : NULL;
        $itModel->status = $data['status'];
        $itModel->content_category_value = implode(',',@$data['content_category_value']);
        $itModel->size = @$data['size'];
        $itModel->barcode = @$data['barcode'];
        $itModel->genre = (is_array(@$data['genre'])?json_encode(@$data['genre']):@$data['genre']);
        $itModel->audio = @$data['audio'];
        $itModel->subtitles = @$data['subtitles'];
        $itModel->alternate_name = @$data['alternate_name'];
        $itModel->is_deleted = 0;
        $itModel->is_preorder = 0;
        $itModel->productize_flag = @$data['productize_flag'];
        $publish_date = NULL;
        if (@$data['content_publish_date']) {
            if (@$data['publish_date']) {
                $pdate = explode('/', $data['publish_date']);
                $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                if (@$data['publish_time']) {
                    $publish_date .= ' ' . $data['publish_time'];
                }
            }
        }
        $itModel->publish_date = $publish_date;
        $itModel->created_date = gmdate('Y-m-d H:i:s');
        $itModel->updated_date = gmdate('Y-m-d H:i:s');
        $itModel->feature = $data['feature'];
        $itModel->ip = CHttpRequest::getUserHostAddress();
        $itModel->save();
        $ins_id = $itModel->id;
        if(isset($data['multi_sale_price']) && !empty($data['multi_sale_price'])){
            $pgmulti = New PGMultiCurrency;
            foreach ($data['multi_sale_price'] as $key => $val) {
                if ($val != '') {
                    $pgmulti->product_id = $ins_id;
                    $pgmulti->studio_id = Yii::app()->user->studio_id;
                    $pgmulti->price = $val;
                    $pgmulti->currency_id = $data['multi_currency_id'][$key];
                    $pgmulti->isNewRecord = TRUE;
                    $pgmulti->primaryKey = NULL;
                    $pgmulti->save();
                }
            }
        }
        $pcModel = New PGProductContents;
        foreach ($data['contentid'] as $val) {
            if ($val != '') {
                $pcModel->product_id = $ins_id;
                $pcModel->movie_stream_id = 0;
                $pcModel->movie_id = $val;
                $pcModel->isNewRecord = TRUE;
                $pcModel->primaryKey = NULL;
                $pcModel->save();
            }
        }
        /* Physical goods image upload */
        PGImage::processProductImage($ins_id, $_FILES['Filedata'], $_REQUEST);
        /* Physical goods image upload end */
        
        //Adding permalink to url routing 
        $urlRouts['permalink'] = $permalink;
        $urlRouts['mapped_url'] = '/shop/ProductDetails/id/'.$uniqid;
        $urlRoutObj = new UrlRouting();
        $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,Yii::app()->user->studio_id);
        if(HOST_IP !='127.0.0.1'){
            if(!empty($_REQUEST['pg']['genre'])){
                foreach($_REQUEST['pg']['genre'] as $key=>$val){
					$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
					$solrArr['content_id'] = $ins_id;
					$solrArr['stream_id'] = '';
					$solrArr['stream_uniq_id'] = $uniqid;
					$solrArr['is_episode'] = 0;
					$solrArr['name'] = $data['name'];
					$solrArr['permalink'] = $permalink;
					$solrArr['studio_id'] =  Yii::app()->user->studio_id;
					$solrArr['display_name'] = 'muvikart';
					$solrArr['content_permalink'] =  $permalink;
					$solrArr['product_sku'] =  $data['sku'];
					$solrArr['product_format'] = $data['custom_fields'];
					if($publish_date!=NULL)
						$solrArr['publish_date'] = $publish_date;
                    $solrArr['genre_val'] = $val;
                    $solrObj = new SolrFunctions();
                    $ret = $solrObj->addSolrData($solrArr);
                }
            }else{
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $ins_id;
                $solrArr['stream_id'] = '';
                $solrArr['stream_uniq_id'] = $uniqid;
                $solrArr['is_episode'] = 0;
                $solrArr['name'] = $data['name'];
                $solrArr['permalink'] = $permalink;
                $solrArr['studio_id'] =  Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'muvikart';
                $solrArr['content_permalink'] =  $permalink;
                $solrArr['product_sku'] =  $data['sku'];
                $solrArr['product_format'] = $data['custom_fields'];
                if($publish_date!=NULL)
					$solrArr['publish_date'] = $publish_date;
            $solrArr['genre_val'] = '';
            $solrObj = new SolrFunctions();
            $ret = $solrObj->addSolrData($solrArr);
        }
            
        }
     
		if(isset($data['actor']) && $data['actor']){
            $exploded = explode(',', $data['actor']);
            if (sizeof($exploded) > 1) {
              $actor = $exploded;
            }else {
              $actor = $data['actor'];
            }
            self::AddCastForPhysical(Yii::app()->user->studio_id,$actor,$ins_id);
        }
        Yii::app()->user->setFlash('success', "Item added successfully.");
        $url = $this->createUrl('store/GoodsLibrary');
        $this->redirect($url);
        exit;
    }

    public function actiondeleteItem() {
        $id = $_REQUEST['id'];
        if ($id) {
            $studio_id = Yii::app()->user->studio_id;
            Yii::app()->db->createCommand('UPDATE pg_product SET is_deleted = 1 WHERE id =' . $id . ' AND studio_id=' . $studio_id)->execute();
            if (HOST_IP != '127.0.0.1') {
                $solrobj = new SolrFunctions();
                $solrobj->deleteSolrQuery('content_id:' . $id . " AND cat:muvikart");
            }
            FeaturedContent::model()->deleteAll("movie_id =:movie_id AND studio_id =:studio_id AND is_episode=:is_episode", array(':movie_id' => $id, ':studio_id' => $studio_id, ':is_episode' => '2'));
            Yii::app()->user->setFlash('success', "Item deleted successfully.");
            $url = $this->createUrl('admin/managecontent', array('searchForm[contenttype]' => 2));
            $this->redirect($url);
            exit;
        } else {
            $url = $this->createUrl('admin/managecontent', array('searchForm[contenttype]' => 2));
            $this->redirect($url);
            exit;
        }
    }

    public function actionEditItem() {
        $this->pageTitle = Yii::app()->name . ' | ' . 'Edit Content ';
        $this->breadcrumbs = array('Movie List' => array('admin/managecontent'), 'Edit Movie Info',);


        $studio_id = $this->studio->id;
        $fieldval = Yii::app()->db->createCommand("SELECT * FROM pg_product WHERE uniqid= '" . $_REQUEST['uid'] . "' AND studio_id=" . $studio_id)->queryAll();
        $this->headerinfo = 'Edit Content - ' . $fieldval[0]['name'];

        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $currency_code = PGProduct::getStudioCurrencyCode($this->studio);
        //---------------------------------
        //get the existing movie for this product 
        $dbcont = PGProductContents::model()->findAll("product_id=:product_id", array(':product_id' => @$fieldval[0]['id']));
        if ($dbcont) {
            foreach ($dbcont AS $key => $val) {
                $pgcontent[] = $val['movie_id'];
            }
        }
        //get the movie name and id to show in autocomplete       
        $list = Film::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        if ($list) {
            foreach ($list AS $key => $val) {
                $movies[] = array('movie_name' => $val['name'], 'movie_id' => $val['id']);
            }
        }
        //--------------------
        $dbmulti = PGMultiCurrency::model()->findAll("product_id=:product_id", array(':product_id' => @$fieldval[0]['id']));
        if (!empty($dbmulti)) {
            foreach ($dbmulti AS $key => $val) {
                $pgmulti[$val['currency_id']] = $val['price'];
            }
        }
        $dbprimg = PGProductImage::model()->find("product_id=:product_id", array(':product_id' => @$fieldval[0]['id']));
        if ($dbprimg) {
            $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
            $pgposter = $url . '/system/pgposters/' . $fieldval[0]['id'] . '/standard/' . urlencode($dbprimg['name']);
            if (false === file_get_contents($pgposter)) {
                $pgposter = POSTER_URL . '/no-image-a.png';
            }
        } else {
            $pgposter = POSTER_URL . '/no-image-a.png';
        }
        //Check for Custom Form 
        $customComp = new CustomForms();
        $arg['content_type'] = 0;
        $arg['is_child'] = 0;
        $customData = $customComp->getCustomMetadata($studio_id,5, $arg);
        $settings = Yii::app()->general->getPgSettings();
        if(@$settings['enable_category']==1){
            $language_id = $this->language_id;
            $sql = "SELECT * FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
            $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
            $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        }else{
            $contentCategories = 0;
        }
        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $shipping_size = CHtml::listData($sizes, 'size_unique_name', 'size');
        $celeb = MovieCast::model()->findAll(array("select"=>"id,cast_name","condition" =>'product_id='.$fieldval[0]['id']));
        $checkf_type = CHtml::listData($customData, 'f_name', 'f_type');
        if($celeb){
            if($checkf_type['actor']==3){//dropdown or list
                $fieldval[0]['actor'] = json_encode(CHtml::listData($celeb, 'id', 'cast_name'));
            }else{
                $fieldval[0]['actor'] = (count($celeb)>1)?implode(',',CHtml::listData($celeb, 'id', 'cast_name')):$celeb[0]['cast_name'];
            }        
        }
        $fieldval = PGProduct::model()->getSerializeCustomDate($fieldval);
        $formdata = Yii::app()->general->formlist($studio_id);
		$productize = Yii::app()->general->checkProductizeExtension($studio_id);
        $this->render('additem', array('all_images' => $all_images, 'currency_code' => $currency_code, 'fieldval' => $fieldval, 'movies' => $movies, 'pgcontent' => $pgcontent, 'pgposter' => $pgposter,'customData'=>$customData,'pgmulti'=>@$pgmulti,'enable_category' => @$settings['enable_category'],'contentCategories' => @$contentCategories,'shipping_size' => $shipping_size,'productize' => $productize,'formdata'=>$formdata));
    }

    public function actionEditSaveItem() {

        $data = $_REQUEST['pg'];
        
        /*
          if ($data['discount_till_date'] != '') {
          $a = explode('/', $data['discount_till_date']);
          $my_new_date = $a[2] . '-' . $a[0] . '-' . $a[1];
          } else {
          $my_new_date = '0000-00-00';
          } */
        //echo $data['id'];
        $goods = new PGProduct();
        $pggoods = $goods->findByPk($data['id']);
        
        //$permalink = Yii::app()->general->generatePermalink(stripslashes($data['name']));
        //$old_permalink = $pggoods->permalink;
        //$pggoods->permalink = $permalink;
        $pggoods->name = $data['name'];
        $pggoods->description = $data['description'];
        $pggoods->sale_price = @$data['sale_price'];
        /* $pggoods->discount = $data['discount'];
          $pggoods->discount_type = $data['discount_type'];
          $pggoods->discount_till_date = $my_new_date;
         */
        $pggoods->sku = $data['sku'];
        $pggoods->custom_fields = $data['custom_fields'];
	$pggoods->release_date = $data['release_date'] ? date('Y-m-d', strtotime($data['release_date'])) : NULL;
        $pggoods->rating = $data['rating'];
        $pggoods->status = $data['status'];
        $pggoods->content_category_value = implode(',',@$data['content_category_value']);
        $pggoods->size = @$data['size'];
        $pggoods->barcode = @$data['barcode'];
        $pggoods->genre = (is_array(@$data['genre'])?json_encode(@$data['genre']):@$data['genre']);
        $pggoods->audio = @$data['audio'];
        $pggoods->subtitles = @$data['subtitles'];
        $pggoods->alternate_name = @$data['alternate_name'];
        $pggoods->product_type = $data['product_type'];
        $pggoods->currency_id = @$data['currency_id'];
        $pggoods->updated_date=  date('Y-m-d H:i:s');
        $pggoods->feature=  $data['feature'];
		$pggoods->productize_flag = @$data['productize_flag'];
        $publish_date = NULL;
        if (@$data['content_publish_date']) {
            if (@$data['publish_date']) {
                $pdate = explode('/', $data['publish_date']);
                $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                if (@$data['publish_time']) {
                    $publish_date .= ' ' . $data['publish_time'];
                }
            }
        }
        $pggoods->publish_date = $publish_date;
        $data['custom6'] = $goods->createSerializeCustomDate($data);
        $pggoods->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
        $pggoods->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
        $pggoods->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
        $pggoods->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
        $pggoods->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
        $pggoods->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';  
        
        $pggoods->save();
        if(isset($data['multi_sale_price']) && !empty($data['multi_sale_price'])){
            PGMultiCurrency::model()->deleteAll("product_id =:product_id", array(':product_id' => $data['id']));
            $pgmulti = New PGMultiCurrency;
            $pgmulti->addContent($data,$data['id']);
                }
        //delete the data before saving
        PGProductContents::model()->deleteAll("product_id =:product_id", array(':product_id' => $data['id']));
        // re-insert the fresh id
        $pcModel = New PGProductContents;
        if ($data['product_type'] == 1) {
            $pcModel->addContent($data,$data['id']);
                }
        /* Physical goods image upload */
        PGImage::processProductImage($data['id'], $_FILES['Filedata'], $_REQUEST);
        /* Physical goods image upload end */
        /*update url routing table
        if(UrlRouting::model()->exists('permalink=:permalink',array(':permalink' => $permalink))){
            //Yii::app()->db->createCommand("UPDATE url_routing SET permalink ='".$permalink."' WHERE permalink= '" .$old_permalink."'")->execute();
        }else{
            //Adding permalink to url routing 
            $urlRouts['permalink'] = $permalink;
            $urlRouts['mapped_url'] = '/shop/ProductDetails/id/'.$pggoods->uniqid;
            $urlRoutObj = new UrlRouting();
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,Yii::app()->user->studio_id);
        }*/
        if(isset($data['actor']) && $data['actor']){
            $exploded = explode(',', $data['actor']);
            if (sizeof($exploded) > 1) {
              $actor = $exploded;
            }else {
              $actor = $data['actor'];
            }
            self::AddCastForPhysical(Yii::app()->user->studio_id,$actor,$data['id'],1);
        }
        $studio_id=Yii::app()->user->studio_id;
         if(HOST_IP !='127.0.0.1'){
             $solrobj = new SolrFunctions();
             $solrobj->deleteSolrQuery('cat:muvikart AND sku:' . $studio_id .' AND content_id:' . $pggoods->id);
             
            if(!empty($_REQUEST['pg']['genre'])){
                foreach($_REQUEST['pg']['genre'] as $key=>$val){
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $_REQUEST['pg']['id'];
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = $pggoods->uniqid;
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $data['name'];
                    $solrArr['permalink'] = $pggoods->permalink;
                    $solrArr['studio_id'] =  Yii::app()->user->studio_id;
                    $solrArr['display_name'] = 'muvikart';
                    $solrArr['content_permalink'] =  $pggoods->permalink;
                    $solrArr['product_sku'] =  $data['sku'];
                    $solrArr['product_format'] = $data['custom_fields'];
                    $solrArr['genre_val'] = $val;
                    if($publish_date!=NULL)
                    $solrArr['publish_date'] = $publish_date;
                    $solrObjct = new SolrFunctions();
                    $ret = $solrObjct->addSolrData($solrArr);
                }
            }else{
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $_REQUEST['pg']['id'];
                $solrArr['stream_id'] = '';
                $solrArr['stream_uniq_id'] = $pggoods->uniqid;
                $solrArr['is_episode'] = 0;
                $solrArr['name'] = $data['name'];
                $solrArr['permalink'] = $pggoods->permalink;;
                $solrArr['studio_id'] =  Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'muvikart';
                $solrArr['content_permalink'] =$pggoods->permalink;;
                $solrArr['product_sku'] =  $data['sku'];
                $solrArr['product_format'] = $data['custom_fields'];
                $solrArr['genre_val'] = '';
                if($publish_date!=NULL)
                 $solrArr['publish_date'] = $publish_date;
                $solrObjct = new SolrFunctions();
                $ret = $solrObjct->addSolrData($solrArr);
            }
         
        }
        Yii::app()->user->setFlash('success', "Item edited successfully.");
        $url = $this->createUrl('admin/managecontent', array('searchForm[contenttype]' => 2));
        $this->redirect($url);
        exit;
    }

    public function actionorder() {
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size + 1;
        }
        $studio_id = $this->studio->id;
        $this->breadcrumbs = array('Muvi Kart','Order');
        $this->headerinfo = "Order Screen";
        $this->pageTitle = "Muvi | Muvi Kart > Keeping Track";
        $cond = " AND pgo.studio_id=" . $studio_id;
        if (isset($_REQUEST['search']) && trim($_REQUEST['search'])) {
            $search = urldecode(trim($_REQUEST['search']));
            $cond.= " AND ( pgo.order_number LIKE '%" . $search . "%' OR d.name LIKE '%" . $search . "%' OR sdk.display_name LIKE '%" . $search . "%'  ) ";
        }
        if (isset($_REQUEST['sortBy'])) {
            if ($_REQUEST['sortBy'] == "grand_total_desc") {
                $orderby = " ORDER BY pgo.grand_total DESC";
                $sortBy = 'grand_total_asc';
            } else {
                $orderby = " ORDER BY pgo.grand_total ASC";
                $sortBy = 'grand_total_desc';
            }
        } else {
            $orderby = " ORDER BY pgo.id DESC";
        }       
        $sql = "select SQL_CALC_FOUND_ROWS(0),pgo.id,pgo.cds_order_status,pgo.transactions_id,pgo.order_number,pgo.created_date date,sdk.display_name customer_name,pgo.grand_total total_amount,pgo.studio_id,GROUP_CONCAT(d.name) as nm"
                . " FROM pg_order pgo,sdk_users sdk,pg_order_details d  WHERE sdk.id=pgo.buyer_id AND d.order_id=pgo.id $cond GROUP BY pgo.id $orderby limit $offset,$page_size";
        
        $con = Yii::app()->db;
        $data = $con->createCommand($sql)->queryAll();
        $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();        

        foreach ($data as $key => $details) {
            $orderdetail = "SELECT d.name,d.price,d.quantity,d.product_id,d.item_status FROM pg_order_details d,pg_product p WHERE d.product_id=p.id AND d.order_id=" . $details['id'];
            $order = $con->createCommand($orderdetail)->queryAll();
            $data[$key]['details'] = $order;
            $paymentq = "SELECT transaction_status,transaction_status_reason,currency_id FROM transactions WHERE id=" .$details['transactions_id'];           
            $paymentq_res = $con->createCommand($paymentq)->queryAll();            
            $data[$key]['payment_status'] = $paymentq_res[0]['transaction_status']; 
            $data[$key]['currency_id'] = $paymentq_res[0]['currency_id'];           
        }
        $pages = new CPagination($item_count);
        $pages->setPageSize($page_size);
        $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
        $sample = range($pages->offset + 1, $end);
        $status = PGOrder::getOrderStatus();
        $this->render('order', array('data' => $data,'status' => $status,'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample, 'searchText' => $search, 'sortBy' => @$sortBy));
    }
	
	public function actionexportOrders() {
        $studio_id = $this->studio->id;
        
        $data = Yii::app()->db->createCommand()
                ->select('PO.id,PO.order_number,PO.created_date,PO.discount,PO.grand_total, POD.product_id, POD.name AS PNAME,POD.quantity,POD.price,POD.item_status, T.order_number AS TORD')
                ->from('pg_order PO, pg_order_details POD, transactions T')
                ->where("PO.id = POD.order_id AND T.id=PO.transactions_id AND PO.studio_id='$studio_id'")
                ->queryAll();
        foreach($data as $key=>$res1){
            $data1 = Yii::app()->db->createCommand()
                    ->select('PSA.first_name,PSA.address,PSA.address2,PSA.city,PSA.state,C.country,PSA.zip,PSA.phone_number')
                    ->from('pg_shipping_address PSA, countries C')
                    ->where("PSA.country=C.code AND PSA.order_id='".$res1['id']."'")
                    ->queryRow();
            $data2 = Yii::app()->db->createCommand()
                    ->select('POS.name AS SNAME')
                    ->from('pg_order_status POS')
                    ->where("POS.order_status_id='".$res1['item_status']."'")
                    ->queryRow();
            $data[$key] = array_merge($res1,$data1,$data2);
        }        
        
        $headArr[0] = array('Sl No','ORDER NUMBER','DATE','NAME','ADDRESS', 'ADDRESS2','CITY', 'STATE', 'COUNTRY', 'ZIP', 'CONTACT NUMBER', 'PRODUCT NAME', 'SKU', 'QUANTITY', 'PRICE', 'SHIPPING COST', 'DISCOUNT', 'TOTAL', 'ORDER STATUS', 'TRASANCTION ID');
        $sheetName[0] = 'Order';
        $csvData[0] = array();
        $sheet = $i = 1;
        foreach ($data as $key => $details) {            
            $getSku = PGProduct::model()->find(array('select'=>'sku', 'condition'=>'id=:product_id', 'params'=>array(':product_id'=>$details['product_id'])))->sku;
            $csvData[0][] = array(
                $i,
                $details['order_number'],
                $details['created_date'] ? date('jS M ,Y', strtotime($details['created_date'])) : 'NA',
                $details['first_name'],
                $details['address'],
                $details['address2'],
                $details['city'],
                $details['state'],
                $details['country'],
                $details['zip'],
                $details['phone_number'],
                $details['PNAME'],
                $getSku,
                $details['quantity'],
                $details['price'],
                $details['shipping_cost'],
                $details['discount'],
                $details['grand_total'],
                $details['SNAME'],
                $details['TORD']
                
            );
            $i++;
        }
        $filename = 'order_'.date('Ymd_His');
        $type = 'xls'; 
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf'){
           $type = 'pdf'; 
        }

        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$csvData,$filename,$type);
        exit;
    }
	
    public function actionStatus() {
        $studio_id = $this->studio->id;
        $order_id = $_REQUEST['id'];
        $status = $_REQUEST['data'];
        $con = Yii::app()->db;
        $status = "UPDATE pg_order SET order_status= '" . $status . "'  WHERE id=" . $order_id;
        $order = $con->createCommand($status)->queryAll();
    }

    public function actionOrderDetails() {
        $studio_id = $this->studio->id;
        $order_id = $_REQUEST['id'];
        $this->breadcrumbs = array('Muvi Kart','Order' => array('store/order'),'Order Details');
        $this->headerinfo = "Order Details";
        $this->pageTitle = "Muvi | Muvi Kart >Order Details";
        $con = Yii::app()->db;
        $order = $con->createCommand('SELECT * FROM pg_order WHERE id='.$order_id.' AND studio_id='.$studio_id)->queryROW();
        //5877-Payment notification ajit@muvi.com
        $paymentq = "SELECT transaction_status,transaction_status_reason FROM transactions WHERE id=" .$order['transactions_id'];           
        $paymentq_res = $con->createCommand($paymentq)->queryAll(); 
        $order['transaction_status']=$paymentq_res[0]['transaction_status'];
        //5877-Payment notification ajit@muvi.com
        if(!empty($order)){
            $orderdetails = $con->createCommand('SELECT d.*,p.currency_id FROM pg_order_details d,pg_product p WHERE d.product_id=p.id AND d.order_id='.$order_id)->queryAll();
            $shiping = $con->createCommand('SELECT * FROM pg_shipping_address WHERE order_id = '.$order['id'])->queryROW();
            $userinfo = $con->createCommand('SELECT display_name,email FROM sdk_users WHERE id='.$order['buyer_id'])->queryROW();
            $status = PGOrder::getOrderStatus();
            if($order['coupon_code']!=''){
                $d = $con->createCommand("SELECT id,discount_type,discount_amount FROM coupon WHERE coupon_code='" .$order['coupon_code']."'")->queryROW();                    
                $order['discount_type'] = $d['discount_type'];
                if($d['discount_type']==0){
                    $f = $con->createCommand("SELECT currency_id,discount_amount FROM coupon_currency WHERE coupon_id=" .$d['id'])->queryROW();
                    $order['coupon_currency'] = $f['currency_id'];
                    $order['discount_amount'] = $f['discount_amount'];
                }else{
                    $order['discount_amount'] = round($d['discount_amount']);
                }
            }
           
            //get coupon details
            foreach ($orderdetails as $kdetail => $vdetail) {
                if($vdetail['currency_id']==0){
                    $ord = $con->createCommand("SELECT currency_id FROM transactions WHERE id=(SELECT transactions_id FROM pg_order WHERE id='".$vdetail['order_id']."')")->queryROW();
                    $orderdetails[$kdetail]['currency_id'] = $ord['currency_id'];
                }
            }
            $this->render('orderdetails', array('order' => $order,'order_details'=>$orderdetails,'userinfo'=>$userinfo,'status'=>$status,'addr'=>$shiping));
        }
    }

    //check unique sku number
    function actioncheckSkuNumber() {
        $arr = array('succ' => 0);
        if (isset($_REQUEST['skuno']) && $_REQUEST['skuno'] != '') {
            if ($_REQUEST['check'] == 'TRUE') {
                $data = PGProduct::model()->countByAttributes(array('sku' => $_REQUEST['skuno'], 'studio_id' => $this->studio->id));
                if ($data) {
                    $arr['succ'] = 0;
                } else {
                    $arr['succ'] = 1;
                }
            } else {
                //check if more then 1 record
                $con = Yii::app()->db;
                $orderdetail = "select count(*) as cnt from pg_product WHERE sku='" . $_REQUEST['skuno'] . "' AND studio_id =" . $this->studio->id . " AND id != " . $_REQUEST['id'];
                $order = $con->createCommand($orderdetail)->queryAll();
                if ($order[0]['cnt'] == 1) {
                    $arr['succ'] = 0;
                } else {
                    $arr['succ'] = 1;
                }
            }
        }
        echo json_encode($arr);
        exit;
    }

    function actionPreorder() {
        $this->breadcrumbs = "Pre-Order";
        $this->headerinfo = "Pre-Order";
        $this->pageTitle = "Pre-Order";
        $studio_id = $this->studio->id;
        $preorders = PGPreorderCategory::model()->findAllByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0));
        if (isset($preorders) && !empty($preorders)) {
            foreach ($preorders as $key => $value) {
                $data[] = $value;                
            }
        }
        foreach($data as $key=>$val){
            $pricing = self::getPrices($val['id']);
            foreach($pricing as $valprice)
            {
                $data_price[$val['id']][] = $valprice;
            }
            $content = self::getItems($val['id']);
            foreach($content as $valcontent)
            {
                $data_content[$val['id']][] = $valcontent;
            }
        }
                
        $this->render('preorder', array('data' => $data,'data_price' => $data_price,'data_content' => $data_content));
        
    }
    function actionPreorderPopup() {
        $this->layout = false;        
        $studio_id = $this->studio->id;        
        $pricing = $content = array();
        
        if (isset($_REQUEST['id_preorder']) && !empty($_REQUEST['id_preorder'])) {
            
            $data = PGPreorderCategory::model()->findByAttributes(array('id' => $_REQUEST['id_preorder'], 'studio_id' => $studio_id, 'is_deleted' => 0));
            
            if (!empty($data)) {
                $pricing = self::getPrices($_REQUEST['id_preorder']);
                $content = self::getItems($_REQUEST['id_preorder']);
                foreach ($content as $key => $value) {
                    $content[$key] = addslashes($value);
                }
            }        
        }        
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('preorderpopup', array('data' => $data,'pricing' => $pricing, 'content' => $content, 'currency' => $currency));
    }
    function actionPreorderContents() {
        $con = Yii::app()->db;
        $studio_id = $this->studio->id;
        $cond = "(publish_date > CURDATE())";
        $csql = "SELECT id,name from pg_product WHERE is_deleted=0 AND is_preorder=0 AND studio_id=".$studio_id." AND ".$cond." ORDER BY name ASC"; 
        
        $content = $con->createCommand($csql)->queryAll();

        echo json_encode($content);
        exit;
    }
    function actionaddEditPreorder() {
        //check for data availibility
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $studio_id = $this->studio->id;
            $user_id = Yii::app()->user->id;
            $category = new PGPreorderCategory();
            //check for edit or insert
            if (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id'])) {
                //set parameters for edit- id is present
                $category = PGPreorderCategory::model()->findByPk($_REQUEST['data']['id']);
                //delete data from product and price table
                $productitems = self::getItems($_REQUEST['data']['id']);
                if(!empty($productitems)){
                    $pgitems = '';
                    foreach($productitems as $val){
                        $pgitems .= $val['id'].',';
                    }
                    $pgitems = trim($pgitems,',');
                    Yii::app()->db->createCommand("UPDATE pg_product SET is_preorder = 0 WHERE id IN($pgitems)")->execute();
                }
                PGPreorderItems::model()->deleteAll("preorder_id=:preorder_id", array(':preorder_id' => $_REQUEST['data']['id']));
                PGPreorderPrice::model()->deleteAll("preorder_id=:preorder_id", array(':preorder_id' => $_REQUEST['data']['id']));
            }
            else{
                //set parameter for insert                
                $category->studio_id = $studio_id;
                $category->user_id = $user_id;
                $category->is_deleted = 0;                
                $category->created_date = new CDbExpression("NOW()");
                $category->start_date = new CDbExpression("NOW()");
            }
            $category->title = $_REQUEST['data']['title'];
            $category->description = $_REQUEST['data']['description'];
            $category->updated_date = new CDbExpression("NOW()");
            $category->expiry_date = date('Y-m-d', strtotime($_REQUEST['data']['expiry_date']));
            //save data
            $category->save();
            $preorder_id = $category->id;
            //set the preorder id according to edit or insert
            $preorder_id = (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id']))?$_REQUEST['data']['id']:$preorder_id;
            //save data to item table
            if (isset($_REQUEST['data']['content']) && !empty($_REQUEST['data']['content'])) {
                //Save content
                $content = New PGPreorderItems;
                foreach ($_REQUEST['data']['content'] as $key => $value) {                    
                    $content->preorder_id = $preorder_id;
                    $content->item_id = $value;
                    $content->isNewRecord = TRUE;
                    $content->primaryKey = NULL;
                    $content->save();
                    //update pre order status in pg product table
                    $pggoods = PGProduct::model()->findByPk($value);
                    $pggoods->is_preorder = 1;
                    $pggoods->save();                   
                }
            }           
            $status = explode(',', $_REQUEST['data']['allstatus']);
            //Save price in pricing table
            if (!empty($status)) {
                $pgprice = New PGPreorderPrice;
                foreach ($status as $key => $value) {
                    $key = array_search($value, $_REQUEST['data']['currency_id']);
                    $pgprice->preorder_id = $preorder_id;
                    $pgprice->currency_id = $_REQUEST['data']['currency_id'][$key];
                    $pgprice->price_for_unsubscribe = $_REQUEST['data']['price_for_unsubscribed'][$key];
                    $pgprice->price_for_subscribe = $_REQUEST['data']['price_for_subscribed'][$key];
                    $pgprice->isNewRecord = TRUE;
                    $pgprice->primaryKey = NULL;
                    $pgprice->save();
                }
            }   
            if(!empty($_REQUEST['data']['id']))
            {
                Yii::app()->user->setFlash('success', 'Pre-order items updated successfully');
            }
            else{
                Yii::app()->user->setFlash('success', 'Pre-order items saved successfully');
            }
            $url = $this->createUrl('store/Preorder');
            $this->redirect($url);

        }
        else{
            Yii::app()->user->setFlash('error', 'Oops! Error in pre-ordering items');
        }        
    }
    function getPrices($preorder_id = 0) {
        $pricing = '';        
        if (intval($preorder_id)) {
            $con = Yii::app()->db;
            
            $sql = "SELECT ppp.currency_id,ppp.price_for_unsubscribe,ppp.price_for_subscribe,cu.code,cu.symbol FROM pg_preorder_price ppp LEFT JOIN currency AS cu 
            ON (ppp.currency_id=cu.id) WHERE ppp.preorder_id=".$preorder_id;
            $pricing = $con->createCommand($sql)->queryAll();
        }        
        return $pricing;
    }
    
    function getItems($preorder_id = 0) {
        $adv_content = '';        
        if (intval($preorder_id)) {            
            $studio_id = $this->studio->id;           
            $con = Yii::app()->db;
            $csql = "SELECT PR.id,PR.name FROM pg_preorder_items PPI LEFT JOIN pg_product PR ON PPI.item_id=PR.id WHERE PR.studio_id=".$studio_id." AND PPI.preorder_id=".$preorder_id;
            $adv_content = $con->createCommand($csql)->queryAll();
        }        
        return $adv_content;
    }
    function actiondeletePreorder() {
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {            
            $id = $_REQUEST['id_ppv'];
            $studio_id = $this->studio->id;
            
            $content = self::getItems($id);
            foreach($content as $val)
            {
                //update pre order status in pg product table
                $pggoods = PGProduct::model()->findByPk($val);
                $pggoods->is_preorder = 0;
                $pggoods->save(); 
            }
            PGPreorderCategory::model()->deleteAll('id =:id AND studio_id =:studio_id', array(':id' => $id, ':studio_id' => $studio_id));
            PGPreorderPrice::model()->deleteAll('preorder_id =:id', array(':id' => $id));
            PGPreorderItems::model()->deleteAll('preorder_id =:id', array(':id' => $id));
            
            Yii::app()->user->setFlash('success', 'Pre-ordering category has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Pre-ordering category can not be deleted!');
        }

        $url = $this->createUrl('store/Preorder');
        $this->redirect($url);
    }
    public function actionSettings() {        
        $this->breadcrumbs = array('Muvi Kart', "Settings");
        $this->headerinfo = "Settings";
        $this->pageTitle = "Muvi | Muvi Kart > Settings";
        $studio_id = $this->studio->id;
        $settings = Yii::app()->general->getPgSettings();
        $country = Countries::model()->findAll(array("order"=>"country"));

        $product = PGProduct::model()->findAll(array('select'=>'id,name','condition' => 'studio_id=:studio_id and is_free_offer=:is_free_offer AND is_deleted=0','params' => array(':studio_id' =>$studio_id,':is_free_offer'=>1)));
        $cnt =0;        
        if (isset($product) && !empty($product)) {
            foreach ($product as $key => $value) {
                $data[$cnt]['content_id'] = $value['id'];
                $data[$cnt]['name'] = str_replace("'","",$value['name']);
                $cnt++;
            }
        }
        $this->render('settings', array('settings' => $settings,'country' => $country, 'data' => $data));
    }
    public function actionsaveSettings() {        
        if (isset($_REQUEST['restrict_country']) && !empty($_REQUEST['restrict_country'])) {
            $studio_id = $this->studio->id;
            extract($_REQUEST);
            //save data to default shipping cost table
            //delete previous row from table 
            PGDefaultShippingCost::model()->deleteAll('studio_id =:studio_id', array(':studio_id' => $studio_id));
            //insert the new rcords
            $defshipping = New PGDefaultShippingCost();
            foreach($data['status'] AS $key=>$val){
                if($val == 'Y'){
                    $defshipping->studio_id = $studio_id; 
                    $defshipping->currency_id = $data['currency_id'][$key];
                    $defshipping->default_shipping_cost = $data['default_shipping_price'][$key];
                    $defshipping->isNewRecord = TRUE;
                    $defshipping->primaryKey = NULL;
                    $defshipping->save();
                }
            }
            //save data to minimum order for free shipping
            //delete previous row from table 
            PGMinimumOrderFreeShipping::model()->deleteAll('studio_id =:studio_id', array(':studio_id' => $studio_id));
            //insert the new rcords
            $minorder = New PGMinimumOrderFreeShipping();
            foreach($mindata['status'] AS $key=>$val){
                if($val == 'Y'){
                    $minorder->studio_id = $studio_id; 
                    $minorder->currency_id = $mindata['currency_id'][$key];
                    $minorder->minimum_order_free_shipping = $mindata['minimum_order_free_shipping'][$key];
                    $minorder->isNewRecord = TRUE;
                    $minorder->primaryKey = NULL;
                    $minorder->save();
                }
            }
            if($id ==''){
                $settings = new PGSettings();
                $settings->studio_id = $studio_id;
                $settings->shipping_cost = '0.00';               
                $settings->restrict_country = json_encode($restrict_country);                
                $settings->date_added = gmdate('Y-m-d H:i:s'); 
            }
            else {
                $set = new PGSettings();
                $settings = $set->findByPk($id);
                $settings->shipping_cost = '0.00';                
                $settings->restrict_country = json_encode($restrict_country);
                $settings->date_modified = gmdate('Y-m-d H:i:s');
            }
            if($settings->save()){                
                Yii::app()->user->setFlash('success', 'Shipping rules saved successfully');
            }else{
                Yii::app()->user->setFlash('error', 'Shipping rules cannot saved. Please try again');
            }
        }else{
            Yii::app()->user->setFlash('error', 'Settings cannot saved. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url); 
        exit;
    }
    /*
     * Free offer auto suggestion
     * ajit@muvi.com
     */
    public function actionGetContents() {
        $studio_id = $this->studio->id;        
        $res = array();
        $data = array();
        $data = PGProduct::model()->findAll(array('select'=>'id,name','condition' => 'studio_id=:studio_id and is_deleted=:is_deleted AND status=:status','params' => array(':studio_id' =>$studio_id,':is_deleted'=>0,':status'=>1)));
        $cnt =0;
        
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $res[$cnt]['content_id'] = $value['id'];
                $res[$cnt]['name'] = $value['name'];
                $cnt++;
            }
        }
        echo json_encode($res);
        exit;
    }
    /*
     * Author ajit@muvi.com 26/9/2016
     * Add free offer products
     * change the status of free_offer(pg_setting) and is_free_offer(pg_product)
     * When de-activating free offer all the free products by default converted to regular product     
     */
    public function actionfreeOffer() {
        if (count($_POST)) {
            $studio_id = $this->studio->id;
            if($_POST['id'] ==''){
                $settings = new PGSettings();
                $offer_check = (isset($_POST['offer_check']) && $_POST['offer_check']!='')?$_POST['offer_check']:0;
                $settings->studio_id = $studio_id;
                $settings->free_offer = $offer_check;
                $settings->date_added = gmdate('Y-m-d H:i:s'); 
            }
            else
            {
                $set = new PGSettings();
                $settings = $set->findByPk($_POST['id']);
                $offer_check = (isset($_POST['offer_check']) && $_POST['offer_check']!='')?$_POST['offer_check']:0;
                $settings->free_offer = $offer_check;                
                $settings->date_modified = gmdate('Y-m-d H:i:s');                
            }
            if($settings->save()){
                //change the product status
                Yii::app()->db->createCommand("UPDATE pg_product SET is_free_offer=0 WHERE studio_id= " . $studio_id)->execute();
                //update new ids
                if (isset($_POST['data']['content']) && !empty($_POST['data']['content'])&&$_POST['offer_check']==1) {
                    foreach ($_POST['data']['content'] as $key => $content) {                        
                        Yii::app()->db->createCommand("UPDATE pg_product SET is_free_offer=1 WHERE studio_id= ".$studio_id." AND id=".$content)->execute();
                    }
                }                
                Yii::app()->user->setFlash('success', 'Free offer updated successfully');
            }else{
                Yii::app()->user->setFlash('error', 'Free offer cannot updated. Please try again');
            }
        }else{
           Yii::app()->user->setFlash('error', 'Oops,Error detected.'); 
        }
        $url = $this->createUrl('store/Settings');
        $this->redirect($url); 
        exit;
        
    }
    public function actionimportExcel() {
        $this->breadcrumbs = array('Muvi Kart', "Import Excel");
        $this->headerinfo = "Import Excel";
        $this->pageTitle = "Muvi | Muvi Kart > Import Excel";
        $studio_id = $this->studio->id;
        if(isset($_POST) && $_POST['setupload']=='Y')
        {
            if (!$_FILES['upload_file']['error'])
            {
                $file_info=pathinfo ($_FILES['upload_file1']['name']);
                if($file_info['extension']!='xlsx')
                {
                    Yii::app()->user->setFlash('error', 'Invalid file type. Upload only excel file.');
                    $url = $this->createUrl('store/importExcel');
                    $this->redirect($url); 
                    exit;
                }
                $storagename = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/pgexcel';
                $_FILES['upload_file1']['name'] = $file_info['filename']."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_info['extension'];
                $name = Yii::app()->common->fileName($_FILES["upload_file1"]["name"]);
                $path = $storagename . '/' . $name;
                mkdir($storagename, 0777);
                move_uploaded_file($_FILES['upload_file1']['tmp_name'], $path);
                require 'PHPExcel/PHPExcel/IOFactory.php';
                
                //read excel data
                try {
                        $objPHPExcel = PHPExcel_IOFactory::load($path);
                } catch(Exception $e) {
                        die('Error loading file "'.pathinfo($path,PATHINFO_BASENAME).'": '.$e->getMessage());
                }
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet

                $itModel = New PGProduct;
                for($i=2;$i<=$arrayCount;$i++)
                {
                    $count++;            
                    if(!PGProduct::model()->exists('sku=:sku AND studio_id=:studio_id',array(':sku' => $allDataInSheet[$i]["D"],':studio_id'=>$studio_id))){
                        if($allDataInSheet[$i]["A"]!='' && $allDataInSheet[$i]["D"]!=''){
                            $uniqid = Yii::app()->common->generateUniqNumber();
                            $permalink = Yii::app()->general->generatePermalink(stripslashes($allDataInSheet[$i]["A"]));
                            $itModel->uniqid = $uniqid;
                            $itModel->permalink = $permalink;
                            $itModel->name = $allDataInSheet[$i]["A"];
                            $itModel->description = $allDataInSheet[$i]["F"];
                            $itModel->studio_id = Yii::app()->user->studio_id;
                            $itModel->user_id = Yii::app()->user->id;
                            $itModel->sale_price = $allDataInSheet[$i]["E"];                
                            $itModel->product_type = 0;//0-Standalone
                            $itModel->currency_id = $this->studio->default_currency_id;                
                            $itModel->sku = $allDataInSheet[$i]["D"];
                            $itModel->custom_fields = $allDataInSheet[$i]["C"];
                            $itModel->rating = $allDataInSheet[$i]["B"];
                            $itModel->status = 1;//1-Active
                            $itModel->is_free_offer = ($allDataInSheet[$i]["H"]=='')?0:1;
                            $itModel->created_date = gmdate('Y-m-d H:i:s');
                            $itModel->updated_date = gmdate('Y-m-d H:i:s');                
                            $itModel->ip = CHttpRequest::getUserHostAddress();
                            $itModel->isNewRecord = TRUE;
                            $itModel->primaryKey = NULL;
                            $itModel->save();
                            $ins_id = $itModel->id;
                            //Adding permalink to url routing 
                            $urlRouts['permalink'] = $permalink;
                            $urlRouts['mapped_url'] = '/shop/ProductDetails/id/'.$uniqid;
                            $urlRoutObj = new UrlRouting();
                            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,Yii::app()->user->studio_id);
                            if(HOST_IP !='127.0.0.1'){
                                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                                $solrArr['content_id'] = $ins_id;
                                $solrArr['stream_id'] = '';
                                $solrArr['stream_uniq_id'] = $uniqid;
                                $solrArr['is_episode'] = 0;
                                $solrArr['name'] = $allDataInSheet[$i]["A"];
                                $solrArr['permalink'] = $permalink;
                                $solrArr['studio_id'] =  Yii::app()->user->studio_id;
                                $solrArr['display_name'] = 'muvikart';
                                $solrArr['content_permalink'] =  $permalink;
                                $solrArr['product_sku'] =  $allDataInSheet[$i]["D"];
                                $solrObj = new SolrFunctions();
                                $ret = $solrObj->addSolrData($solrArr);
                            }
                        }else{
                            $data[$count]['name']= $allDataInSheet[$i]["A"];
                            $data[$count]['price']= $allDataInSheet[$i]["E"];
                            $data[$count]['sku']= $allDataInSheet[$i]["D"];
                            $data[$count]['error']= 'Missing Data';
                        }
                    }
                    else {
                        $data[$count]['name']= $allDataInSheet[$i]["A"];
                        $data[$count]['price']= $allDataInSheet[$i]["E"];
                        $data[$count]['sku']= $allDataInSheet[$i]["D"];
                        $data[$count]['error']= 'Duplicate SKU Number';               
                    }
                }
                //delete the file from server
                unlink($path);
                if(empty($data))
                    Yii::app()->user->setFlash('success', 'Data imported successfully.');
                else
                    Yii::app()->user->setFlash('error', 'Oops,Error detected. Please check and upload in a seperate file.');
            }
        }
        $this->render('excel', array('data' => $data));
    }    
    public function actionStatusPopup(){
        $status = PGOrder::getOrderStatus();
        $order_status_id = $_POST['order_status_id'];
        $orderdetailsid = $_POST['orderdetailsid'];
        $studio_id = $this->studio->id;
        $muvi_status = $to_status = array('1'=>'Processing','9'=>'Processing','12'=>'Shipped','10'=>'Cancelled');
        if($order_status_id==1){
            unset($to_status[1]);
            unset($to_status[9]);
        }elseif($order_status_id==3){
            unset($to_status[12]);
        }elseif($order_status_id==7){
            unset($to_status[10]);
        }else{
            unset($to_status[$order_status_id]);
        }
        //get last orderstatus from database
        $current_status = PGOrderStatusLog::model()->find(array('select'=>'comments','condition' => 'studio_id=:studio_id and orderdetailsid=:orderdetailsid ORDER by id DESC','params' => array(':studio_id' =>$studio_id,':orderdetailsid'=>$orderdetailsid)));
        $tracking_status = PGOrderDetails::model()->find(array('select'=>'cds_tracking,tracking_url','condition' => 'id=:orderdetailsid','params' => array(':orderdetailsid'=>$orderdetailsid)));        
        $this->renderPartial('statuspopup', array('muvi_status' => $muvi_status,'to_status' => $to_status,'order_status_id' => $order_status_id,'orderdetailsid' => $orderdetailsid,'current_status' => $current_status,'tracking_status' => $tracking_status));
    }
    public function actionChangeStatus() {
        $data = $_POST['pg'];
        $ip = CHttpRequest::getUserHostAddress();
        $PGOrderStatusLog = new PGOrderStatusLog();
        $lastid = $PGOrderStatusLog->insertStatusLog($this->studio->id,$data,$ip);
        if($lastid)
            Yii::app()->user->setFlash('success', 'Status Changed Successfully');
        else
            Yii::app()->user->setFlash('error', 'Oops,Error detected. Please try again');
        $url = $_SERVER['HTTP_REFERER'];
        $this->redirect($url);
    }
    public function actionshippingCost(){
        $this->breadcrumbs = array('Muvi Kart', "Shipping");
        $this->headerinfo = "Shipping";
        $this->pageTitle = "Muvi | Muvi Kart > Shipping";
        $studio_id = $this->studio->id;
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        
        $pdo_cond = " SC.studio_id=:studio_id AND SC.is_deleated=:is_deleated";
        $pdo_cond_arr = array(":studio_id" => $studio_id,':is_deleated' => 0);
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {            
            if ($_REQUEST['country']) {
                $pdo_cond .= " AND SC.country=:country";
                $pdo_cond_arr[':country'] = $_REQUEST['country'];
            }
            if ($_REQUEST['method']) {
                $pdo_cond .= " AND SC.method=:method";
                $pdo_cond_arr[':method'] = $_REQUEST['method'];
            }
            if ($_REQUEST['size']) {
                $pdo_cond .= " AND SC.size=:size";
                $pdo_cond_arr[':size'] = $_REQUEST['size'];
            }             
        }
        $costs = Yii::app()->db->createCommand()
        ->select('SQL_CALC_FOUND_ROWS (0),SC.*,C.country')
                
        ->from('pg_shipping_cost SC')
        ->join('countries C', 'SC.country = C.code')
        ->where($pdo_cond, $pdo_cond_arr)
        ->limit($page_size, $offset)
        ->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $pages = new CPagination($count);
        $pages->setPageSize($page_size);
        $countrys = Countries::model()->findAll(array("order"=>"country"));
        $methods = ShippingMethod::model()->findAll("studio_id=:studio_id AND is_enabled=:is_enabled", array(':studio_id' => $studio_id,':is_enabled' => 1));
        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $default_shipping_cost = PGDefaultShippingCost::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $minimum_order_free_shipping = PGMinimumOrderFreeShipping::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $currency = Yii::app()->common->getStudioCurrency();
        $settings = Yii::app()->general->getPgSettings();
        $product = PGProduct::model()->findAll(array('select'=>'id,name','condition' => 'studio_id=:studio_id and is_free_offer=:is_free_offer AND is_deleted=0','params' => array(':studio_id' =>$studio_id,':is_free_offer'=>1)));
        $cnt =0;        
        if (isset($product) && !empty($product)) {
            foreach ($product as $key => $value) {
                $data[$cnt]['content_id'] = $value['id'];
                $data[$cnt]['name'] = str_replace("'","",$value['name']);
                $cnt++;
    }
        }
        $this->render('list_shipping_cost',array('costs'=>$costs, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages,'countrys'=>$countrys,'methods'=>$methods,'sizes'=>$sizes,'currency'=>$currency,'settings' => $settings,'data' => $data,'default_shipping_cost'=>$default_shipping_cost,'minimum_order_free_shipping'=> $minimum_order_free_shipping));
    }

    public function actionaddShippingCost(){
        $this->breadcrumbs = array('Muvi Kart', "Shipping");
        $this->headerinfo = "Shipping Cost";
        $this->pageTitle = "Muvi | Muvi Kart > Shipping";
        $studio_id = $this->studio->id;
        $countrys = Countries::model()->findAll(array("order"=>"country"));
        $methods = ShippingMethod::model()->findAll("studio_id=:studio_id AND is_enabled=:is_enabled", array(':studio_id' => $studio_id,':is_enabled' => 1));
        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('shipping_cost',array('countrys'=>$countrys,'methods'=>$methods,'sizes'=>$sizes,'currency'=>$currency,'data'=>$data));        
    }
    public function actionsaveShippingCost() {
        $data = $_POST['pg'];
        $studio_id = $this->studio->id;
        if($data){
            $cost = new PGShippinCost();
            $exist='N';
            //check the combination exist or not
            if(PGShippinCost::model()->exists('country=:country AND method=:method AND size=:size AND currency=:currency AND studio_id=:studio_id AND is_deleated=0',array(':country' => $data['country'],':method'=>$data['method'],':size'=>$data['size'],':currency'=>$data['currency'],':studio_id'=>$studio_id))){
                $exist='Y';
            }
            if($data['id']!=''){
                $cost = $cost->findByPk($data['id']);
                $cost->modified_date = gmdate('Y-m-d H:i:s');
                $exist='N';
            }else{
                $cost->uniqueid = Yii::app()->common->generateUniqNumber();
                $cost->created_date = gmdate('Y-m-d H:i:s');
                $cost->studio_id = $studio_id;
                $cost->user_id = Yii::app()->user->id;
                $cost->is_deleated = 0;
            }            
            $cost->country = $data['country'];
            $cost->method = $data['method'];
            $cost->size = $data['size'];
            $cost->currency = $data['currency'];
            $cost->shipping_cost = $data['shipping_cost'];
            if($exist=='N'){
                $cost->save();
                Yii::app()->user->setFlash('success', 'Shipping cost added successfully');
            }else{
                Yii::app()->user->setFlash('error', 'This combination is already exist');
            }
        }else{
            Yii::app()->user->setFlash('error', 'Oops,Error detected. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url);
    }
    public function actiondeleteShippingCost() {
        if($_REQUEST['id']!=''){
           $cost = new PGShippinCost();
           $del = $cost->findByPk($_REQUEST['id']);
           $del->is_deleated=1;
           $del->save();
           Yii::app()->user->setFlash('success', 'Record deleted successfully');
        }else{
            Yii::app()->user->setFlash('error', 'Oops,Error detected. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url);
    }
    public function actionshowShippingPop() {
        $studio_id = $this->studio->id;
        $countrys = Countries::model()->findAll(array("order"=>"country"));
        $methods = ShippingMethod::model()->findAll("studio_id=:studio_id AND is_enabled=:is_enabled", array(':studio_id' => $studio_id,':is_enabled' => 1));
        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $currency = Yii::app()->common->getStudioCurrency();
        if($_REQUEST['scid']){
            $data=PGShippinCost::model()->find("id=:scid", array(':scid' => $_REQUEST['scid']));
        }
        $this->renderPartial('shipping_cost',array('countrys'=>$countrys,'methods'=>$methods,'sizes'=>$sizes,'currency'=>$currency,'data'=>$data));
    }        
    /* @Author manas@muvi.com
     * @Desc Geo-block Content (SKU) can be restricted to ship to specific countries, like we have for Digital
     */
    function actionAddGeoBlockToPGContent() {
        $gc = GeoblockPGContent::model()->findByAttributes(array('pg_product_id' => $_POST['pg_product_id']));
        if (!empty($gc) && !empty($_POST['category_name'])) {
            $gc->geocategory_id = $_POST['category_name'];
            $gc->update();
        } elseif (!empty($gc) && empty($_POST['category_name'])) {
            $gc->delete();
        } elseif (empty($gc) && !empty($_POST['category_name'])) {
            $geo = New GeoblockPGContent;
            $geo->pg_product_id = $_POST['pg_product_id'];
            $geo->geocategory_id = $_POST['category_name'];
            $geo->save();
        } elseif (empty($gc) && empty($_POST['category_name'])) {
            
        }
        echo 1;
        exit;
    }
    function actionOpenGeoCategory(){
        $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $this->studio->id));
        $cat_array = CHtml::listData($grestcat, 'id', 'category_name');
        $gc = GeoblockPGContent::model()->findByAttributes(array('pg_product_id' => $_REQUEST['pg_product_id']));
        $categoryid = !empty($gc)?$gc['geocategory_id']:"";
        $this->renderPartial('geocategory', array('studio' => $this->studio->id,'cat_array'=>$cat_array,'pg_product_id'=>$_REQUEST['pg_product_id'],'categoryid'=>$categoryid));
    }

    /* * * copied from admincontroller 
     * Embed from Third Party ** */

    public function actionembedFromThirdPartyPlatform() {
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_POST['thirdparty_url']) && $_POST['product_id']) {
            $url = $_POST['thirdparty_url'];
            $product_id = $_POST['product_id'];
            $date = date('Y-m-d H:i:s');
            $getMovieStream = PGVideo::model()->findByAttributes(array('studio_id' => $studio_id, 'product_id' => $product_id));
            if (empty($getMovieStream)) {
                $getMovieStream = new PGVideo();
            }
            if ($getMovieStream) {
                $getMovieStream->thirdparty_url = $url;
                $getMovieStream->product_id = $product_id;
                $getMovieStream->is_converted = 1;
                $getMovieStream->studio_id = $studio_id;
                $getMovieStream->full_movie = 'test.mp4';
                $getMovieStream->has_sh = 0;
                $getMovieStream->video_management_id = 0;
                $getMovieStream->full_movie_url = NULL;
                $getMovieStream->video_resolution = NULL;
                $getMovieStream->resolution_size = NULL;
                $getMovieStream->original_file = NULL;
                $getMovieStream->converted_file = NULL;
                $getMovieStream->video_duration = NULL;
                $getMovieStream->upload_start_time = $date;
                $getMovieStream->upload_end_time = $date;
                $getMovieStream->upload_cancel_time = NULL;
                $getMovieStream->encoding_start_time = $date;
                $getMovieStream->encoding_end_time = $date;
                $getMovieStream->encode_fail_time = NULL;
                $getMovieStream->mail_sent_for_video = 1;
                $getMovieStream->save();
                if ($getMovieStream->save()) {
                    Yii::app()->user->setFlash('success', 'Embed URL is added successfully');
                } else {
                    Yii::app()->user->setFlash('error', 'Oops! Sorry, Embed URL is not added!');
                }
            }
        }
    }  
    public function actionimportExcelUsjv() {
       
        $studio_id = 3062;
        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/pgexcel/thei.xlsx';
        
        require 'PHPExcel/PHPExcel/IOFactory.php';
        //read excel data
        try {
                $objPHPExcel = PHPExcel_IOFactory::load($path);
        } catch(Exception $e) {
                die('Error loading file "'.pathinfo($path,PATHINFO_BASENAME).'": '.$e->getMessage());
}
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
       
        $itModel = New PGProduct;
        for($i=2;$i<=$arrayCount;$i++)
        {
            $count++;            
            if(!PGProduct::model()->exists('sku=:sku AND studio_id=:studio_id',array(':sku' => $allDataInSheet[$i]["B"],':studio_id'=>$studio_id))){
                if($allDataInSheet[$i]["A"]!='' && $allDataInSheet[$i]["B"]!=''){
                    $uniqid = Yii::app()->common->generateUniqNumber();
                    $permalink = Yii::app()->general->generatePermalink(stripslashes($allDataInSheet[$i]["A"]));
                    if ($allDataInSheet[$i]["J"] != '') {
                        $a = explode('/', $allDataInSheet[$i]["J"]);                        
                        $my_new_date = $a[2] . '-' . $a[0] . '-' . $a[1];
                    } else {
                        $my_new_date = '0000-00-00';
        }
                    $itModel->uniqid = $uniqid;
                    $itModel->permalink = $permalink;
                    $itModel->name = $allDataInSheet[$i]["A"];
                    $itModel->description = $allDataInSheet[$i]["M"];
                    $itModel->studio_id = $studio_id;
                    $itModel->user_id = 0;
                    $itModel->sale_price = $allDataInSheet[$i]["L"];                
                    $itModel->product_type = 0;//0-Standalone
                    $itModel->currency_id = 52;                
                    $itModel->sku = $allDataInSheet[$i]["B"];
                    $itModel->custom_fields = $allDataInSheet[$i]["C"];
                    $itModel->rating = $allDataInSheet[$i]["K"];
                    $itModel->status = 1;//1-Active
                    $itModel->is_free_offer = 0;
                    $itModel->content_category_value = $allDataInSheet[$i]["D"];
                    $itModel->release_date = $my_new_date;
                    $itModel->size = 'small';
                    //$itModel->barcode = $allDataInSheet[$i]["F"];
                    $itModel->genre = json_encode($allDataInSheet[$i]["E"]);
                    //$itModel->audio = $allDataInSheet[$i]["I"];
                    //$itModel->subtitles = $allDataInSheet[$i]["J"];
                    //$itModel->alternate_name = $allDataInSheet[$i]["P"];
                    
                    $itModel->created_date = gmdate('Y-m-d H:i:s');
                    $itModel->updated_date = gmdate('Y-m-d H:i:s');                
                    $itModel->ip = CHttpRequest::getUserHostAddress();
                    $itModel->isNewRecord = TRUE;
                    $itModel->primaryKey = NULL;
                    $itModel->save();
                    $ins_id = $itModel->id;
                    //Adding permalink to url routing 
                    $urlRouts['permalink'] = $permalink;
                    $urlRouts['mapped_url'] = '/shop/ProductDetails/id/'.$uniqid;
                    $urlRoutObj = new UrlRouting();
                    $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);
                    if(HOST_IP !='127.0.0.1'){
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $ins_id;
                        $solrArr['stream_id'] = '';
                        $solrArr['stream_uniq_id'] = $uniqid;
                        $solrArr['is_episode'] = 0;
                        $solrArr['name'] = $allDataInSheet[$i]["A"];
                        $solrArr['permalink'] = $permalink;
                        $solrArr['studio_id'] =  $studio_id;
                        $solrArr['display_name'] = 'Search results';
                        $solrArr['content_permalink'] =  $permalink;
                        $solrArr['product_sku'] =  $allDataInSheet[$i]["B"];
                        $solrObj = new SolrFunctions();
                        $ret = $solrObj->addSolrData($solrArr);
    }
                    /*
                    $video = New PGVideo;
                    if($allDataInSheet[$i]["G"] !=''){
                        $video->thirdparty_url = $allDataInSheet[$i]["G"];
                        $video->product_id = $ins_id;
                        $video->studio_id = $studio_id;
                        $video->isNewRecord = TRUE;
                        $video->primaryKey = NULL;
                        $video->save();
                    }
                    $currency = New PGMultiCurrency;
                    if($allDataInSheet[$i]["L"] !=''){                        
                        $currency->product_id = $ins_id;
                        $currency->studio_id = $studio_id;
                        $currency->price = $allDataInSheet[$i]["L"];
                        $currency->currency_id = 131;
                        $currency->isNewRecord = TRUE;
                        $currency->primaryKey = NULL;
                        $currency->save();
                    }                    
                    if($allDataInSheet[$i]["M"] !=''){                        
                        $currency->product_id = $ins_id;
                        $currency->studio_id = $studio_id;
                        $currency->price = $allDataInSheet[$i]["M"];
                        $currency->currency_id = 41;
                        $currency->isNewRecord = TRUE;
                        $currency->primaryKey = NULL;
                        $currency->save();
                    }                    
                    if($allDataInSheet[$i]["N"] !=''){                        
                        $currency->product_id = $ins_id;
                        $currency->studio_id = $studio_id;
                        $currency->price = $allDataInSheet[$i]["N"];
                        $currency->currency_id = 110;
                        $currency->isNewRecord = TRUE;
                        $currency->primaryKey = NULL;
                        $currency->save();
                    }                     
                    if($allDataInSheet[$i]["O"] !=''){                        
                        $currency->product_id = $ins_id;
                        $currency->studio_id = $studio_id;
                        $currency->price = $allDataInSheet[$i]["O"];
                        $currency->currency_id = 47;
                        $currency->isNewRecord = TRUE;
                        $currency->primaryKey = NULL;
                        $currency->save();
                    }*/                           
                }else{
                    $data[$count]['name']= $allDataInSheet[$i]["A"];
                    $data[$count]['price']= $allDataInSheet[$i]["E"];
                    $data[$count]['sku']= $allDataInSheet[$i]["D"];
                    $data[$count]['error']= 'Missing Data';
                }
            }
        }
    }
    /*@author manas@muvi.com
     * nSaveKartGeoBlock() setting for Muvi kart:Add 'Kart Geo-block Settings' option
     * mantis id = 5923
     */
    public function actionSaveKartGeoBlock(){        
        if(isset($_POST['id']) && !empty($_POST['id']) && is_numeric($_POST['id']))  {
            $kg = PGSettings::model()->findByPk($_POST['id']);
            $flag = 1;
        }elseif(isset($_POST['id']) && empty($_POST['id'])){
            $kg = new PGSettings;
            $flag = 1;
        }else{
            $flag = 0;
        }
        if($flag){
            $kg->studio_id = $this->studio->id;
            if(isset($_POST['allow_country']) && ($_POST['allow_country']==1)){
                $kg->kart_geo_block=1;
            }else{
                $kg->kart_geo_block=0;
            }
            $kg->save();
            Yii::app()->user->setFlash('success', 'Kart Geo-block Settings updated successfully');
        }else{
            Yii::app()->user->setFlash('error', 'Kart Geo-block cannot updated. Please try again');
        }
        $url = $this->createUrl('store/Settings');
        $this->redirect($url); 
        exit;       
    }
    /*
     * 6063: Muvi Kart: Edit option for Oder Changes
     * ajit@muvi.com
     */
    
    public function actionupdateAddress(){
        
        $req = PGShippingAddress::model()->find("id=:id", array(':id' => $_REQUEST['shipping_id']));
        $countries = Countries::model()->findAll();
        $this->renderPartial('addresscart', array('req'=>$req,'countries'=>$countries));
        exit;
}
    public function actionshowShippingAddress(){
        
        $addr = PGShippingAddress::model()->find("id=:id", array(':id' => $_REQUEST['shipping_id']));
        $this->renderPartial('showshippingaddress', array('addr'=>$addr));
        exit;
    }
    public function actionsaveShippingAddress(){        
        parse_str($_REQUEST['data'],$shipping_addr);        
        $cost = new PGShippingAddress();            
        $ship = $cost->findByPk($shipping_addr['ship']['id']); 
        $ship->first_name = $shipping_addr['ship']['first_name'];
        $ship->last_name = $shipping_addr['ship']['last_name'];
        $ship->address = $shipping_addr['ship']['address'];
        $ship->address2 = $shipping_addr['ship']['address2'];
        $ship->city = $shipping_addr['ship']['city'];
        $ship->state = $shipping_addr['ship']['state'];
        $ship->country = $shipping_addr['ship']['country'];
        $ship->zip = $shipping_addr['ship']['zip'];
        $ship->phone_number = $shipping_addr['ship']['phone_number'];
        $ship->manually_modified = 1;
        $ship->save();
        $data = array('isSuccess'=>1,'id'=>$shipping_addr['ship']['id']);
        $data = json_encode($data);
        echo $data;
        exit;  
    }
    public function actionaddItemPopup(){
        $this->renderPartial('additempopup', array('order_id'=>$_REQUEST['order_id']));
        exit;
    }
    public function actionshowProductDetails(){        
        $skuno = $_POST['skuno']; 
        $studio_id = Yii::app()->common->getStudiosId();
        //get product details
        $product = PGProduct::model()->find(array('select'=>'name','condition' => 'studio_id=:studio_id and sku=:sku AND is_deleted=0 AND status=1','params' => array(':studio_id' =>$studio_id,':sku'=>$skuno)));
        if($product){
            $data = array('isSuccess'=>1,'name'=>$product->name,'message'=>'Success');
        }else {
            $data = array('isSuccess'=>0,'message'=>'SKU number not found.');
        }
        $data = json_encode($data);
        echo $data;
        exit;  
    }
    public function actionsaveItem(){        
        parse_str($_REQUEST['data'],$additem);
        $studio_id = Yii::app()->common->getStudiosId();
        //get product details for this sku
        $product = PGProduct::model()->find(array('condition' => 'studio_id=:studio_id and sku=:sku AND is_deleted=0','params' => array(':studio_id' =>$studio_id,':sku'=>$additem['skuno'])));
        //insert item to existing order
        $pgorder = new PGOrderDetails();
        $pgorder->order_id = $additem['order_id'];
        $pgorder->product_id = $product->id;
        $pgorder->quantity = $additem['quantity'];
        $pgorder->price = '0.00';
        $pgorder->name = $product->name;
        $pgorder->discount = '';
        $pgorder->tax = '';
        $pgorder->description = $product->description;
        $pgproductimage = PGProductImage::model()->find('product_id=:id AND feature=1', array(':id' => $product->id));
        $pgorder->image_url = $pgproductimage['name'];
        $pgorder->sub_total = '0.00';
        $pgorder->item_status = 1;
        if($pgorder->save()){
            //update manual update status
            $order = new PGOrder();
            $order = $order->findByPk($additem['order_id']);
            $order->manually_modified = 1;
            $order->save();
            $data = array('isSuccess'=>1,'order_id'=>$additem['order_id']);
        }else{
            $data = array('isSuccess'=>0);
        }
        $data = json_encode($data);
        echo $data;
        exit;  
    }    
    public function actionremoveItem(){ 
        if($_POST['detail_id']){
            PGOrderDetails::model()->deleteAll("id =:id", array(':id' => $_POST['detail_id']));
            $data = array('isSuccess'=>1,'order_id'=>$_POST['order_id']);
        }else{
            $data = array('isSuccess'=>0);
        }
        $data = json_encode($data);
        echo $data;
        exit;  
    }
    /*
     * END - 6063: Muvi Kart: Edit option for Oder Changes     
     */
    
    /**
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @param studio_id studio_id
     * @param Actor Actor name
     * @param product_id  product_id
     * @example Addcast This fucntion add cast for physical content
     */
    function AddCastForPhysical($studio_id,$castnames,$product_id,$editflag=0){
        $castnames = is_array($castnames)?$castnames:array($castnames);        
        foreach ($castnames as $key => $castname) {
            $celeb = Celebrity::model()->find(array("condition" =>'studio_id='.$studio_id.' AND name like "'.$castname.'"'));
            if(empty($celeb)){
                $celeb = new Celebrity();
                $celeb->name = $castname;
                $celeb->studio_id = $studio_id;
                $celeb->parent_id = 0;
                $celeb->created_by = Yii::app()->user->id;
                $celeb->ip = $_SERVER['REMOTE_ADDR'];
                $celeb->created_date = gmdate('Y-m-d H:i:s');
                $celeb->language_id = 20;
                $celeb->save();
                $castid = $celeb->id;

                if (HOST_IP != '127.0.0.1') {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $castid;
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = '';
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $castname;
                    $solrArr['permalink'] = $celeb->permalink; 
                    $solrArr['studio_id'] = Yii::app()->user->studio_id;
                    $solrArr['display_name'] = 'star';
                    $solrArr['content_permalink'] = 'star';
                    $solrArr['genre_val'] = '';
                    $solrArr['product_format'] = '';
                    $solrObj = new SolrFunctions();
                    $ret_solr = $solrObj->addSolrData($solrArr);
				}
            }else{
                $castid = $celeb->id;
            }
            if($editflag){
               $arr[] = $castid;
            }
            $extdata =MovieCast :: model()->exists('product_id=:product_id AND celebrity_id=:celebrity_id',array(':product_id' => $product_id,':celebrity_id' => $castid));
            if (!$extdata) {
                $movie_cast = new MovieCast();
                $movie_cast->movie_id = '';
                $movie_cast->celebrity_id = $castid;
                $movie_cast->cast_type = json_encode(array('actor'));
                $movie_cast->cast_name = $castname;
                $movie_cast->ip = $_SERVER["REMOTE_ADDR"];
                $movie_cast->created_by = Yii::app()->user->id;
                $movie_cast->product_id = $product_id;
                $movie_cast->created_date = date('Y-m-d H:i:s');
                $movie_cast->save(false);
            }
        }
        if($editflag){
            $arr1 = MovieCast :: model()->findAll(array("condition" =>'product_id='.$product_id));
            $cid = CHtml::listData($arr1, 'celebrity_id', 'celebrity_id');
            $diff = array_diff($cid, $arr);            
            if($diff){
                $diffar = implode(', ', $diff);
                MovieCast :: model()->deleteAll('product_id = '.$product_id.' AND celebrity_id IN (' . $diffar . ')');
			}
        }
    }
	public function actionUpdateExcelThei() {
        ini_set('memory_limit', '1024M');
        $studio_id = 3062;
        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/pgexcel/thei.xlsx';
        
        require 'PHPExcel/PHPExcel/IOFactory.php';
        //read excel data
        try {
                $objPHPExcel = PHPExcel_IOFactory::load($path);
        } catch(Exception $e) {
                die('Error loading file "'.pathinfo($path,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
     
        for($i=2;$i<=$arrayCount;$i++)
        {
            $count++;
            if($allDataInSheet[$i]["A"]!='' && $allDataInSheet[$i]["B"]!=''){
                $pgdata = PGProduct::model()->find('sku=:sku AND studio_id=:studio_id',array(':sku' => $allDataInSheet[$i]["B"],':studio_id'=>$studio_id));
                if($pgdata){
                    if($allDataInSheet[$i]["G"]!='' && $allDataInSheet[$i]["G"]!='N/A'){
                        $actor[0] = $allDataInSheet[$i]["G"];
                    }
                    if($allDataInSheet[$i]["H"]!='' && $allDataInSheet[$i]["H"]!='N/A'){
                        $actor[1] = $allDataInSheet[$i]["H"];
                    }
                    if($allDataInSheet[$i]["I"]!='' && $allDataInSheet[$i]["I"]!='N/A'){
                        $actor[2] = $allDataInSheet[$i]["I"];
                    }
                    self::AddCastForPhysical($studio_id,$actor,$pgdata->id);
                }else{
                    $data[$count]['name']= $allDataInSheet[$i]["A"];
                    $data[$count]['price']= $allDataInSheet[$i]["E"];
                    $data[$count]['sku']= $allDataInSheet[$i]["D"];
                    $data[$count]['error']= 'Missing Data in database';
                }
            }else{
                $data[$count]['name']= $allDataInSheet[$i]["A"];
                $data[$count]['price']= $allDataInSheet[$i]["E"];
                $data[$count]['sku']= $allDataInSheet[$i]["D"];
                $data[$count]['error']= 'Missing Data';
            }
        }
        echo "<pre>";
        print_r($data);
        exit;
    }
    /**
     * @author Ajit <ajit@muvi.com>
     * @desc issue id - 6781
     */
    public function actionsaveMethod() {
        if ($_POST) {
            $studio_id = $this->studio->id;
            if ($_POST['method'] != '') {
                $settings = new ShippingMethod();
                $settings->method = $_POST['method'];
                $settings->method_unique_name = strtolower($_POST['method']);
                $settings->studio_id = $studio_id;
                $settings->is_enabled = 1;
                $settings->save();
            }
            Yii::app()->user->setFlash('success', 'Shipping method saved successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Shipping method cannot saved. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url);
        exit;
    }

    public function actioneditMethod() {
        if ($_POST) {
            if ($_POST['method'] != '' && intval($_POST['id'])) {
                $settings = new ShippingMethod();
                $settings = $settings->findByPk($_POST['id']);
                $settings->method = $_POST['method'];
                $settings->save();
                $data = array('isSuccess' => 1, 'message' => 'Method added successfully');
            } else {
                $data = array('isSuccess' => 0, 'message' => 'Method cannot added successfully');
            }
        } else {
            $data = array('isSuccess' => 0, 'message' => 'Method cannot added successfully');
        }
        $data = json_encode($data);
        echo $data;
        exit;
    }

    public function actionsaveSize() {
        if ($_POST) {
            $studio_id = $this->studio->id;
            if ($_POST['size'] != '') {
                $settings = new ShippingSize();
                $settings->size = $_POST['size'];
                $settings->size_unique_name = strtolower($_POST['size']);
                $settings->studio_id = $studio_id;
                $settings->save();
            }
            Yii::app()->user->setFlash('success', 'Shipping size saved successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Shipping size cannot saved. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url);
        exit;
    }

    public function actioneditSize() {
        if ($_POST) {
            if ($_POST['size'] != '' && intval($_POST['id'])) {
                $settings = new ShippingSize();
                $settings = $settings->findByPk($_POST['id']);
                $settings->size = $_POST['size'];
                $settings->save();
                $data = array('isSuccess' => 1, 'message' => 'Size added successfully');
            } else {
                $data = array('isSuccess' => 0, 'message' => 'Size cannot added successfully');
            }
        } else {
            $data = array('isSuccess' => 0, 'message' => 'Size cannot added successfully');
        }
        $data = json_encode($data);
        echo $data;
        exit;
    }

    public function actiondeleteMethod() {
        if ($_GET['id'] != '' && intval($_GET['id'])) {
            $studio_id = $this->studio->id;
            ShippingMethod :: model()->deleteAll("id =:id AND studio_id=:studio_id", array(':id' => $_GET['id'], 'studio_id' => $studio_id));
            Yii::app()->user->setFlash('success', 'Shipping method deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Shipping method cannot deleted. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url);
        exit;
    }

    public function actiondeleteSize() {
        if ($_GET['id'] != '' && intval($_GET['id'])) {
            $studio_id = $this->studio->id;
            ShippingSize :: model()->deleteAll("id =:id AND studio_id=:studio_id", array(':id' => $_GET['id'], 'studio_id' => $studio_id));
            Yii::app()->user->setFlash('success', 'Shipping size deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Shipping size cannot deleted. Please try again');
        }
        $url = $this->createUrl('store/ShippingCost');
        $this->redirect($url);
        exit;
    }
}
