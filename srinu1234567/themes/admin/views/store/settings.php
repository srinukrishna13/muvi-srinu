<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<?php 
$selcontents = '';
if (isset($data) && !empty($data)) {
    $selcontents = json_encode($data);
}
$restrict_country = json_decode($settings['restrict_country']);
?>
<div class="row m-t-40 m-b-40">
    <div class="col-xs-12 m-b-40">
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="fa fa-gift left-icon "></em>
                </div>
                <h4>Free Offer</h4>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="post" role="form" id="free_content_form" name="free_content_form" action="javascript:void(0);">
                        <input type="hidden" name="id" value="<?php echo $settings['id']; ?>">
                       <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="offer_check" id="offer_check" <?php echo ($settings['free_offer']==1)?'checked':''; ?> /><i class="input-helper"></i><b>Enable free offer on Checkout:</b>
                                    </label>
                                </div>
                                <div class="fg-line" id="offershow">                                    
                                    <select  data-role="tagsinput" name="data[content][]" placeholder="Type to add new item, press enter to select multiple item" id="content" multiple>
                                    </select>
                                    <label id="data[content][]-error" class="error red" for="data[content][]" style="display: <?php echo ($settings['free_offer']==1)?'checked':''; ?>"></label>

                                </div>
                            </div>              
                        </div>                        
                        <div class="form-group col-md-8">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="button" name="saveoffer" value="save" class="btn btn-primary btn-sm m-t-30" onclick="return validateFreeContentForm(this)">Update</button>
                            </div>
                        </div>                

                    </form>        
                </div> 
            </div>
        </div> 
    </div>
    
    <div class="col-xs-12">
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-settings left-icon "></em>
                </div>
                <h4>Kart Geo-Block Settings</h4>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8">
                    <form class="form-horizontal" method="post" role="form" id="kart_geo_block" name="kart_geo_block" action="javascript:void(0);">
                        <input type="hidden" name="id" value="<?php echo $settings['id']; ?>">
                       <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="allow_country" id="allow_country" <?php echo ($settings['kart_geo_block']==1)?'checked':''; ?> /><i class="input-helper"></i><b>Allow Country Selection:</b>
                                    </label>
                                </div>
                            </div>              
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="button" name="saveoffer" value="save" class="btn btn-primary btn-sm m-t-30" onclick="savekartgeoblock()">Update</button>
                            </div>
                        </div>                

                    </form>        
                </div> 
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
    var sel_contents = '<?php echo $selcontents;?>';
    var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/store/getContents", filter: function (e) {
        return e;
    }}});
    content.clearPrefetchCache(),
    content.initialize(),
    $("#content").tagsinput({
        itemValue: function(item) {
            return item.content_id;
        },
        itemText: function(item) {
            var item_name = $.trim(item.name);
            if (item_name) {
                item_name = item_name.replace("u0027", "'");
            }
            return item_name;
        },
        typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});

    var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
    if (contnts.length) {
        for (var i in contnts) {
            var content_name = $.trim(contnts[i].name);
            if (content_name) {
                content_name = content_name.replace("u0027", "'");
                $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": content_name});
            }
        }
    }
    function validateFreeContentForm(obj) {
       
            if (!$.trim($("#content").val()) && $('#offer_check').is(":checked")) {
                swal("Please add content");
                return false;
            } else {
                var url = "<?php echo Yii::app()->baseUrl; ?>/store/freeOffer";
                document.free_content_form.action = url;
                document.free_content_form.submit();
            }
    }

    function savekartgeoblock(){
        var url = "<?php echo Yii::app()->baseUrl; ?>/store/SaveKartGeoBlock";
        document.kart_geo_block.action = url;
        document.kart_geo_block.submit();
    }
</script>