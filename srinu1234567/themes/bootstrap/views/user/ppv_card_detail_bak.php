<!-- PPV Plan Modal -->
<div class="modal-dialog" id="ppvModalPricingMain">
    <div class="modal-content" style="position: relative;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $this->Language['credit_card_detail']; ?></h4>
        </div>            
        <div class="modal-body">
            <div class="row-fluid">
                <div class="col-md-12">
                    <span class="error" id="plan_error"></span>
                    <?php if (isset($data['msg']) && trim($data['msg'])) { ?>
                        <span class="error"><?php echo $data['msg'];?></span>
                    <?php } else { ?>
                    
                    <form id="membership_form" name="membership_form" method="POST" class="form-horizontal" autocomplete="false">
                        <div id="login-loading" class="loader" style="display: none;"></div>
                        <div id="card-info-error" class="error" style="display: block;font-weight: bold; margin: 0 0 15px;"></div>
                        
                        <input type="hidden" id="email" name="data[email]" value="<?php if (isset(Yii::app()->user->email) && trim(Yii::app()->user->email)) { echo Yii::app()->user->email;} ?>" />
                        <input type="hidden" id="payment_gateway" value="<?php echo $gateways[0]->short_code; ?>" />
                        <input type="hidden" name="data[plan_id]" value="<?php echo $plan->id; ?>" />
                        <input type="hidden" name="data[coupon_code]" value="<?php if (isset($data['coupon_code'])) { echo $data['coupon_code'];} ?>" />
                        <input type="hidden" name="data[movie_id]" value="<?php if (isset($data['movie_id']) && trim($data['movie_id'])) { echo $data['movie_id'];} ?>" />
                        <input type="hidden" name="data[season_id]" value="<?php if (isset($data['season_id']) && trim($data['season_id'])) { echo $data['season_id'];} else { echo 0;} ?>" />
                        <input type="hidden" name="data[episode_id]" value="<?php if (isset($data['episode_id']) && trim($data['episode_id'])) { echo $data['episode_id'];} else { echo 0;} ?>" />
                        <input type="hidden" name="data[contentTypePermalink]" value="<?php echo $films['content_permalink'];?>" />
                        <input type="hidden" name="data[permalink]" value="<?php echo $films['permalink'];?>" />

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label><?php echo $this->Language['card_will_charge']; ?> $<?php echo $amount;?></label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 pull-left">
                                <label for="card_name"><?php echo $this->Language['text_card_name']; ?></label>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <label for="card_number"><?php echo $this->Language['text_card_number']; ?></label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6 pull-left">
                                <input type="text" class="form-control" autocomplete="false" placeholder="<?php echo $this->Language['text_card_name']; ?>" id="card_name" name="data[card_name]" required />
                            </div>
                            <div class="col-sm-6 pull-left">
                                <input type="text" class="form-control" autocomplete="false" placeholder="<?php echo $this->Language['text_card_number']; ?>" id="card_number" name="data[card_number]" required />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 pull-left">
                                <label for="exp_month"><?php echo $this->Language['selct_exp_date']; ?></label>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <label for="security_code"><?php echo $this->Language['text_security_code']; ?></label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6 pull-left">
                                <?php
                                $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
                                ?>
                                <div class="col-sm-6" style="padding: 0">
                                    <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                        <option value=""><?php echo $this->Language['select_month']; ?></option>	
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="sbox-err-lbl">
                                        <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
                                    </div>                        
                                </div>
                                <div class="col-sm-6" style="padding-left: 5px;padding-right: 0;">
                                    <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
                                        <option value=""><?php echo $this->Language['select_year']; ?></option>
                                        <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="sbox-err-lbl">
                                        <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>
                                    </div>                          
                                </div>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <input type="password" id="" name="" style="display: none;" />
                                <input type="password" class="form-control" autocomplete="false" placeholder="<?php echo $this->Language['text_security_code']; ?>" id="security_code" name="data[security_code]" required />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="form-group ">
                            <div class="controls col-sm-2">
                                <button id="register_membership" class="btn btn-primary btn-green-small" onclick="validateUserForm();"><?php echo $this->Language['btn_paynow']; ?></button>
                            </div>
                        </div>
                        <div id="card_div"></div>
                    </form>  
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>          
    </div>		
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title success-popup-payment"><?php echo $this->Language['thanks_card_auth_sucess']; ?></div>
            </div>
        </div>
    </div>
</div>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg"><?php echo $this->Language['auth_your_card']; ?></div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" alt="" title="" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    var action = 'ppvpayment';
    var btn = 'Pay Now';
    function validateUserForm() {
        $("#membership_form").validate({
            rules: {
                "data[card_name]": {
                    required: true
                },
                "data[card_number]": {
                    required: true,
                    number: true
                },
                "data[exp_month]": {
                    required: true
                },
                "data[exp_year]": {
                    required: true
                },
                "data[security_code]": {
                    required: true
                },
                "data[plan]": {
                    required: true
                },
                "data[season]": {
                    isseason: true
                }
            },
            messages: {
                "data[card_name]": {
                    required: JSLANGUAGE.card_name_required;
                },
                "data[card_number]": {
                    required: JSLANGUAGE.card_number_required,
                    number: JSLANGUAGE.card_number_required
                },
                "data[exp_month]": {
                    required: JSLANGUAGE.expiry_month_required
                },
                "data[exp_year]": {
                    required: JSLANGUAGE.expiry_year_required
                },
                "data[security_code]": {
                    required: JSLANGUAGE.security_code_required
                },
                "data[plan]": {
                    required: JSLANGUAGE.choose_plan
                },
                "data[season]": {
                    isseason: JSLANGUAGE.choose_season
                }
            },
            submitHandler: function (form) {
                if ($("#payment_gateway").length) {
                    if ($("#payment_gateway").val() !== 'manual') {
                        var class_name = $("#payment_gateway").val()+'()';
                        eval ("var obj = new "+class_name);
                        obj.processCard();
                    }
                } else {
                    document.membership_form.action = HTTP_ROOT+"/user/"+action;
                    document.membership_form.submit();
                    return false;
                }             
            }
        });
    }
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
        
        $(".close").click(function(){
            if (parseInt(reload)) {
                location.reload();
            }
            reload = 0;
            $(".modal-backdrop").remove();
        });
    });
    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">'+JSLANGUAGE.expiry_month+'</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    
    
    jQuery.validator.addMethod("isseason", function (value, element) {
        if ($("#seasontext").is(':checked')) {
            if ($.trim($("#seasonval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.choose_season);
    
    function showPrice(obj) {
        $("#seasonval-error").hide();
        var price = $(obj).attr('data-price');
        $("#charge_now").html(price);
    }
</script>

<!-- As the processing the card in different payment gateway is different. So processCard javascript method is in following included file.-->

<?php if (isset($this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY != 'manual') {  ?>
    <?php if (isset($this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY == 'stripe') {  ?>
        <script type="text/javascript">
            Stripe.setPublishableKey('<?php echo $this->PAYMENT_GATEWAY_API_PASSWORD; ?>');
        </script>
    <?php } ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY.'.js';?>"></script>
<?php } ?>