<style>
	.current_collapse{
		display: none;
	}
	.block-header{
		padding : 0;
	}
	.has-right-icon{
		padding-left : 50px;
	}
	.preloader{
		display: none;
	}
	.icon-OuterArea--rectangular.right{
		width: 80px;
	}
	.btn-cancel{
		margin-top : 10px;
	}
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<div class="row m-b-40">
    <div class="col-xs-12">
		<a href="javascript:void(0);" id="addnew_playlist" onclick="AddNewPlaylist();">
			<button type="submit"  class="btn btn-primary btn-default m-t-10">Create a playlist</button>
        </a>
        <hr>
	</div></div>

<?php
$k = 1;
foreach ($playlist_data as $data) {
	$listData = $data;
	foreach ($listData as $list) {
		$resultData = $list['lists'];
		
		?>
		<script type="text/javascript">
			$(document).ready(function () {
				$('#collap-<?php echo $k; ?>').click(function () {
					$('#playlist_data_<?php echo $k; ?>').toggleClass('current_collapse')
					$('#playlist_data_<?php echo $k; ?>').toggle();
					$('.editPlaylist[data-id=' + <?php echo $k; ?> + ']').toggleClass('hide');
					$('.delete_playlist[data-id=' + <?php echo $k; ?>+ ']').toggleClass('hide');
				
				});
			});
		</script>  
		
		<div class="Collapse-Block"> 
			<div class="Block-Header has-right-icon"> <!-- Icon exist to left :--> 
				<div class="icon-OuterArea--rectangular"> 
					<?php echo $k; ?>
				</div> <!-- Icon exist to Right : if you need it.-->
				<div class="icon-OuterArea--rectangular right icons">
					<a href="javascript:void(0);" id="edit_playlist" class="editPlaylist" data-id="<?php echo $k; ?>" onclick="editPlaylist('<?php echo $list['list_name'] ?>', '<?php echo $list['list_id']; ?>', '<?php echo $list['content_category']; ?>','<?php echo  $list['playlist_poster']; ?>');" data-name="<?php echo $list['list_name'] ?>" data-id="<?php echo $list['list_id'] ?>">
						<em class="fa fa-pencil-square-o m-r-20" title="Edit" aria-hidden="true"></em>
					</a>
					<a href="javascript:void(0);" id="delete_playlist_btn"  data-id="<?php echo $k; ?>" class='delete_icon delete_playlist' data-name="<?php echo $list['list_name']; ?>" data-id="<?php echo $list['list_id']; ?>">
						<em class="fa fa-trash-o" title="Delete" aria-hidden="true"></em>
					</a>
					<a href="javascript:void(0);" id="collap-<?php echo $k; ?>" data-id="<?php echo $k; ?>"> 
						<em class="fa fa-plus plus_icon" title="Open" data-id="<?php echo $list['list_id']; ?>"></em> 
					</a> 



				</div> <h4><?php echo $list['list_name'] ?> <small> <?php echo $list['counts']; ?> <?php if ($list['counts'] > 1){ echo 'Tracks';}else{echo 'Track';}?></small> </h4> 
			</div> 
			<div class="Collapse-Content current_collapse" id="playlist_data_-<?php echo $k; ?>">
				<div class="m-t-20"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="block-body table-responsive">
							<table class="table table-condensed">
								<tbody>
									<?php
									$i = 1;
									foreach ($resultData as $result) {
										$casts = $result['casts'];
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $result['content_title']; ?></td>
											<td><?php echo $result['cast']; ?></td>
											<td><?php echo $result['video_duration']; ?></td>
											<td>
												<a href="javascript:void(0);" id="delete_content_btn" data-stream_id="<?php echo $result['movie_stream_id']; ?>" data-list_id="<?php echo $list['list_id']; ?>">
													<em class="fa fa-trash-o" title='Delete' aria-hidden="true"></em>
												</a>
											</td>
										</tr>
										<?php
										$i++;
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
		<?php
		$k++;
	}
}
?>
<form name="playlist_form" method="post" id="playlist_form" onsubmit="return validate_form();"  enctype="multipart/form-data"  action="<?php echo $this->createUrl('management/addToPlaylist'); ?>">
<div class="modal fade" id="addPlaylist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
		
			<div id="playlist">
				<input type="hidden" id="movie_id" name="movie_id" value="" />
				<input type="hidden" id="user_id" name="user_id" value="0" />
				<input type="hidden" id="is_episode" name="is_episode" value="" />
				<input type="hidden" id="add_content" name="add-content" value="0">
				<input type="hidden" id="list_id" name="list_id" value="">
				<input type="hidden" id="img_width" name="img_width" value="">
				<input type="hidden" id="img_height" name="img_height" value="">
			</div>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><span id='popup-header-txt text_playlist'>Add new playlist</span> </h4>
				</div>
				<div class="modal-body">
					<div class="error red text-red" id="cerror" style="display:none"></div>
					<div class="form-group">
						<label class="control-label col-sm-3">Title<span class="red"><b>*</b></span>:</label>
						<div class="col-sm-9">
							<div class="fg-line">
								<input type="text"  id="new_playlist" name="newplaylist" class="form-control input-sm" value="<?php echo @$data->category_name; ?>" >	                        </div>
							<label class="new_playlist_error red"></label>
						</div>
					</div>
					<div class="form-group">
						<label for="category" class="col-md-3 control-label">Category<span class="red"><b>*</b></span>: </label>
						<div class="col-md-9">
							<div class="fg-line">
								<select  name="content_category_value[]" id="content_category_value" multiple  class="form-control input-sm checkInput" <?php echo $disable; ?>>
									<?php
									foreach ($contentList AS $k => $v) {
										if (@$data && ($data[0]['content_category'] & $k)) {
											$selected = "selected='selected'";
										} else {
											$selected = '';
										}
										echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
									}
									?>
								</select>
								<label class="playlist_category_error red"></label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Poster:</label>
						<div class="col-sm-9">
							<input class="btn btn-default-with-bg btn-sm" type="button" name="playlist_img" value="Upload Image" data-width="<?php echo @$poster_size['width']; ?>" data-height="<?php echo @$poster_size['height']; ?>" id="playlist_img" onclick="openImageModal(this);" <?php echo $disable; ?>/>
							<span>(Upload a image of <?php echo $poster_size['width'].' x '.$poster_size['height'].'px'; ?> )</span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<div id="avatar_preview_div">
									<img class="img-responsive img-playlist-edit">
							</div>
							<div class="canvas-image" style="width:200px;height:200px;display:none">
							<canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
							</div>
						</div>
					</div>
					<div class="preloader pls-blue" id="playlist_loading">
						<svg class="pl-circular" viewBox="25 25 50 50">
						<circle class="plc-path" cx="50" cy="50" r="20"/>
						</svg>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-default m-t-10 save_playlist" name="save_playlist" id="save_playlist" value="save">Save playlist</button>
					<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		
	</div>
</div>

<div class="modal is-Large-Modal" id="category_img" tabindex="-1" role="dialog" aria-labelledby="category_img">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header text-left">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="section_id1" id="section_id1" value="" />
				<div role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active" onclick="hide_file()">
							<a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
						</li>
						<li role="presentation" onclick="hide_gallery()"> 
							<a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="upload_by_browse">
							<div class="row is-Scrollable">
								<div class="col-xs-12 m-t-40 m-b-20 text-center">
									<input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('browse_cat_img')">
									<input id="browse_cat_img" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
									<p class="help-block"></p>
								</div>
								<input type="hidden" class="x1" id="x1" name="fileimage[x1]" />
								<input type="hidden" class="y1" id="y1" name="fileimage[y1]" />
								<input type="hidden" class="x2" id="x2" name="fileimage[x2]" />
								<input type="hidden" class="y2" id="y2" name="fileimage[y2]" />
								<input type="hidden" class="w" id="w" name="fileimage[w]"/>
								<input type="hidden" class="h" id="h" name="fileimage[h]"/>
								<div class="col-xs-12">
									<div class="Preview-Block row">
										<div class="col-md-12 text-center" id="upload_preview">
											<img id="preview" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="upload_from_gallery">
							<input type="hidden" name="g_image_file_name" id="g_image_file_name" />
							<input type="hidden" name="g_original_image" id="g_original_image" />
							<input type="hidden" class="x1" id="x13" name="jcrop_allimage[x13]" />
							<input type="hidden" class="y1" id="y13" name="jcrop_allimage[y13]" />
							<input type="hidden" class="x2" id="x23" name="jcrop_allimage[x23]" />
							<input type="hidden" class="y2" id="y23" name="jcrop_allimage[y23]" />
							<input type="hidden" class="w" id="w3" name="jcrop_allimage[w3]" />
							<input type="hidden" class="h" id="h3" name="jcrop_allimage[h3]" />
							<div class="row  Gallery-Row">
								<div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

								</div>
								<div class="col-md-6  is-Scrollable p-t-40 p-b-40">
									<div class="text-center m-b-20 loaderDiv"  style="display: none;">
										<div class="preloader pls-blue  ">
											<svg class="pl-circular" viewBox="25 25 50 50">
											<circle class="plc-path" cx="50" cy="50" r="20"></circle>
											</svg>
										</div>
									</div>
									<div class="Preview-Block row">
										<div class="col-md-12 text-center" id="gallery_preview">
											<img id="glry_preview" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
			</div>
		</div>
	</div>
</div>
</form>

<script language="javascript" type="text/javascript">
	$('input.ind_image').on('change', function () {
		$('input.ind_image').not(this).prop('checked', false);
	});
	$(document).ready(function () {
		$('body').on('click', '.plus_icon', function (e) {
			e.preventDefault();
			$(this).toggleClass('fa-minus');
			$('.fa-plus').removeAttr('title');
			if (!$(this).hasClass("fa-minus")) {
				$(this).attr('title','Open');
			}
			$('.fa-minus').attr('title','Close');
			$(this).parent().parent().parent().next().slideToggle(200);
		});
		$('body').on('click', '#delete_playlist_btn', function () {
			var playlist_name = $(this).attr('data-name');
			var playlist_id = $(this).attr('data-id');
			var user_id = 0;
			swal({
				title: "Remove Playlist?",
				text: "Are you sure you want to remove the playlist?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
				confirmButtonText: "Yes",
				closeOnConfirm: true,
				html: true
			},
			function (isConfirm) {
				if(isConfirm){
					deletePlaylist(user_id,playlist_name,playlist_id);
				}
			});

		});
		$('body').on('click', '#delete_content_btn', function () {
			var content_id = $(this).attr('data-stream_id');
			var playlist_id = $(this).attr('data-list_id');
			var user_id = 0;
			swal({
				title: "Remove Content?",
				text: "Are you sure you want to remove the content?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
				confirmButtonText: "Yes",
				closeOnConfirm: true,
				html: true
			},
			function (isConfirm) {
				if(isConfirm){
					deleteContent(user_id,playlist_id,content_id);
				}
			});

		});
		$( "#new_playlist" ).keyup(function() {
			$('.new_playlist_error').html('');
	});
		$("#content_category_value").change(function(){
					$('.playlist_category_error').html('');
		});
	});
	function deletePlaylist(user_id,playlist_name,playlist_id){
	var url  = '<?php echo $this->createUrl('management/deletePlaylist'); ?>';
	$.post(url, {'user_id':user_id,'playlist_id': playlist_id, 'playlist_name': playlist_name}, function (data) {
		 var res = JSON.parse(data);
			if (res.status == "Success") {
				swal(res.msg, '', "success");
				location.reload();
			} else {
				swal(res.msg, '', "error");
				location.reload();
			}
		});
	};
	function deleteContent(user_id,playlist_id,content_id){
	var url  = '<?php echo $this->createUrl('management/deleteContent'); ?>';
	$.post(url, {'user_id': user_id, 'playlist_id': playlist_id, 'content_id': content_id}, function (data) {
		var res = JSON.parse(data);
		if (res.status == "Success") {
			swal(res.msg, '', "success");
			location.reload();
		} else {
			swal(res.msg, '', "error");
			location.reload();
		}
	});
}
	function validate_form() {
		var playlist_name = $("#new_playlist").val();
		var playlist_category = $('#content_category_value').val();
		var regex = /^[a-zA-Z ]*$/;
		if (playlist_name == "") {
			$(".new_playlist_error").html("Playlist name cannot be blank");
			return false;
		}
		
		if (playlist_name.trim() === "")
		{
			$(".new_playlist_error").html("Playlist name cannot be blank Space!");
			return false;
		}

		else if (playlist_category == null) {
			$(".playlist_category_error").html("Playlist category cannot be blank");
			return false;
		}
		else if (playlist_name == "" && playlist_category == '') {
			$(".new_playlist_error").html("Name can not be blank!");
			$(".playlist_category_error").html("Please fill the details");
			return false;
		}else if (playlist_name != ""){
			$(".new_playlist_error").html("");
		}else if(playlist_category){
		$(".playlist_category_error").html("");
		}

	}



	function AddNewPlaylist() {
		$('#new_playlist').val('');
		$('#content_category_value option').removeAttr('selected');
		$('.img-playlist-edit').attr('src','');
		$('.canvas-image').addClass('hide');
		$('#addPlaylist').modal('show');
	}
	function editPlaylist(list_name, list_id, category_value,img_playlist) {
		$('#text_playlist').html('Edit Playlist');
		if(img_playlist != ''){
			$('.img-playlist-edit').attr('src',''+img_playlist);
		}
		$('#content_category_value option').each(function () {
			if ($(this).attr('value') == category_value) {
				$(this).attr('selected', 'selected');
			} else {
				$(this).removeAttr('selected');
			}
		});
		$('#playlist_form').attr('action', '<?php echo $this->createUrl('management/editPlaylist'); ?>');
		$('#new_playlist').val(list_name);
		$('#list_id').val(list_id);
		$('#addPlaylist').modal('show');
	}
	function openImageModal(obj) {
		var width = $(obj).attr('data-width');
		var height = $(obj).attr('data-height');
		$(".help-block").html("Upload a transparent image of size " + width + " x " + height + 'px');
		$("#img_width").val(width);
		$("#img_height").val(height);
		$("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
		$("#category_img").modal('show');
	}
	function click_browse(modal_file) {
		$("#" + modal_file).click();
	}
	function fileSelectHandler() {
		document.getElementById("g_original_image").value = "";
		document.getElementById("g_image_file_name").value = "";
		var img_width = $("#img_width").val();
		var img_height = $("#img_height").val();
		$(".jcrop-keymgr").css("display", "none");
		$("#celeb_preview").removeClass("hide");
		$('#uplad_buton').removeAttr('disabled');
		var oFile = $('#browse_cat_img')[0].files[0];
		var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
		if (!rFilter.test(oFile.type)) {
			swal('Please select a valid image file (jpg and png are allowed)');
			$("#celeb_preview").addClass("hide");
			document.getElementById("browse_cat_img").value = "";
			$('#uplad_buton').attr('disabled', 'disabled');
			return;
		}
		var aspectratio = img_width / img_height;
		var img = new Image();
		img.src = window.URL.createObjectURL(oFile);
		var width = 0;
		var height = 0;
		img.onload = function () {
			var width = img.naturalWidth;
			var height = img.naturalHeight;
			window.URL.revokeObjectURL(img.src);
			if (width < img_width || height < img_height) {
				swal('You have selected small file, please select one bigger image file');
				$("#celeb_preview").addClass("hide");
				document.getElementById("browse_cat_img").value = "";
				$('#uplad_buton').attr('disabled', 'disabled');
				return;
			}
			var oImage = document.getElementById('preview');
			var oReader = new FileReader();
			oReader.onload = function (e) {
				$('.error').hide();
				oImage.src = e.target.result;
				oImage.onload = function () { // onload event handler
					if (typeof jcrop_api != 'undefined') {
						jcrop_api.destroy();
						jcrop_api = null;
						$('#preview').width(oImage.naturalWidth);
						$('#preview').height(oImage.naturalHeight);
						$('#glry_preview').width("450");
						$('#glry_preview').height("250");
					}
					$('#preview').css("display", "block");
					$('#celeb_preview').css("display", "block");
					$('#preview').Jcrop({
						minSize: [img_width, img_height], // min crop size
						aspectRatio: aspectratio, // keep aspect ratio 1:1
						boxWidth: 450,
						boxHeight: 250,
						bgFade: true, // use fade effect
						bgOpacity: .3, // fade opacity
						onChange: updateInfo,
						onSelect: updateInfo,
						onRelease: clearInfo
					}, function () {
						var bounds = this.getBounds();
						boundx = bounds[0];
						boundy = bounds[1];
						jcrop_api = this;
						jcrop_api.setSelect([10, 10, img_width, img_height]);
					});
				};
			};
			oReader.readAsDataURL(oFile);
		};
	}
	function showLoader(isShow) {
		if (typeof isShow == 'undefined') {
			$('.loaderDiv').show();
			$('button').attr('disabled', 'disabled');
		} else {
			$('.loaderDiv').hide();
			$('button').removeAttr('disabled');
		}
	}
	function toggle_preview(id, img_src, name_of_image)
	{
		$('#glry_preview').css("display", "block");
		document.getElementById("browse_cat_img").value = "";
		showLoader();
		var img_width = $("#img_width").val();
		var img_height = $("#img_height").val();
		var aspectratio = img_width / img_height;
		var image_file_name = name_of_image;
		var image_src = img_src;
		clearInfo();
		$("#g_image_file_name").val(image_file_name);
		$("#g_original_image").val(image_src);
		var res = image_file_name.split(".");
		var image_type = res[1];
		var img = new Image();
		img.src = img_src;
		var width = 0;
		var height = 0;
		img.onload = function () {
			var width = img.naturalWidth;
			var height = img.naturalHeight;
			window.URL.revokeObjectURL(img.src);
			if (width < img_width || height < img_height) {
				showLoader(1);
				swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
				$("#celeb_preview").addClass("hide");
				$("glry_preview").addClass("hide");
				$("#g_image_file_name").val("");
				$("#g_original_image").val("");
				$('#uplad_buton').attr('disabled', 'disabled');
				return;
			}
			var oImage = document.getElementById('glry_preview');
			showLoader(1)
			oImage.src = img_src;
			oImage.onload = function () {
				if (typeof jcrop_api != 'undefined') {
					jcrop_api.destroy();
					jcrop_api = null;
					$('#glry_preview').width(oImage.naturalWidth);
					$('#glry_preview').height(oImage.naturalHeight);
					$('#preview').width("450");
					$('#preview').height("250");
				}
				$("#glry_preview").css("display", "block");
				$('#gallery_preview').css("display", "block");
				$('#glry_preview').Jcrop({
					minSize: [img_width, img_height], // min crop size
					aspectRatio: aspectratio, // keep aspect ratio 1:1
					boxWidth: 450,
					boxHeight: 250,
					bgFade: true, // use fade effect
					bgOpacity: .3, // fade opacity
					onChange: updateInfoallImage,
					onSelect: updateInfoallImage,
					onRelease: clearInfoallImage
				}, function () {
					var bounds = this.getBounds();
					boundx = bounds[0];
					boundy = bounds[1];
					jcrop_api = this;
					jcrop_api.setSelect([10, 10, img_width, img_height]);
				});
			};
		};

	}
	function hide_file()
	{
		$('#glry_preview').css("display", "none");
		$('#celeb_preview').css("display", "none");
		$('#preview').css("display", "none");
		document.getElementById('browse_cat_img').value = null;
	}
	function hide_gallery()
	{
		$('#preview').css("display", "none");
		$("#glry_preview").css("display", "block");
		$('#gallery_preview').css("display", "none");
		$("#g_image_file_name").val("");
		$("#g_original_image").val("");
	}
	function seepreview(obj) {
		if ($('#g_image_file_name').val() == '')
			curSel = 'upload_by_browse';
		else
			curSel = 'upload_from_gallery';
		if ($('#' + curSel).find(".x13").val() != "") {
			$(obj).html("Please Wait");
			$('#category_img').modal({backdrop: 'static', keyboard: false});
			posterpreview(obj, curSel);
			$('#category_img').modal('hide');
		} else {
			if ($("#celeb_preview").hasClass("hide")) {
				$('#category_img').modal('hide');
				$(obj).html("Next");
			} else {
				$(obj).html("Please Wait");
				$('#category_img').modal({backdrop: 'static', keyboard: false});
				if ($('#' + curSel).find(".x13").val() != "") {
					posterpreview(obj, curSel);
				} else if ($('#' + curSel).find('.x1').val() != "") {
					posterpreview(obj, curSel);
				} else {
					$('#category_img').modal('hide');
					$(obj).html("Next");
				}
			}
		}
	}
	function posterpreview(obj, curSel) {
		$('.canvas-image').show();
		$("#previewcanvas").show();
		var canvaswidth = $("#img_width").val();
		var canvasheight = $("#img_height").val();
		var x1 = $('#' + curSel).find('.x1').eq(0).val();
		var y1 = $('#' + curSel).find('.y1').eq(0).val();
		var width = $('#' + curSel).find('.w').val();
		var height = $('#' + curSel).find('.h').val();
		var canvas = $("#previewcanvas")[0];
		var context = canvas.getContext('2d');
		var img = new Image();
		img.onload = function () {
			canvas.height = canvasheight;
			canvas.width = canvaswidth;
			context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
			
			//$('#imgCropped').val(canvas.toDataURL());
		};
		$("#avatar_preview_div").hide();
		$('.canvas-image').removeClass('hide');
		if ($('#g_image_file_name').val() == '') {
			img.src = $('#preview').attr("src");
		} else {
			img.src = $('#glry_preview').attr("src");
		}
		//$('#category_img').modal('hide');
		//$(obj).html("Next");
	}
</script>
