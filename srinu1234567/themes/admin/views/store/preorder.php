<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <p class="f-300">Set pre order items</p>
                    
                <div class="row m-b-40">
                    <div class="col-xs-12">
                        <button class="topper btn btn-primary" data-type="1" data-id_ppv="" onclick="addEditCategory(this);">Add category</button>
                    </div>
                </div>
                    <div class="Block m-t-40">
                        <?php if (isset($data) && !empty($data)) { ?>
                        <div class="row m-t-20">
                            <div class="col-sm-8">
                                <label>
                                    Pre-Ordering Items Category
                                </label>
                            </div>
                        </div>
                        
                        <div>
                            <?php 
                            if (isset($data) && !empty($data)) {
                                foreach ($data as $key => $value) {?>
                             <div class="row m-b-10" id="main_single_<?php echo $value['id'];?>">
                                
                                    <div class="col-md-12">
                                        <div class="border-solid padding  m-b-10">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <label><?php echo $value['title'];?></label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Validity date: <?php echo date('F d, Y', strtotime($value['expiry_date']));?></label>
                                                </div>
                                                <div class="col-sm-4 text-right">  
                                                    <a href="javascript:void(0);" data-type="1" data-id_ppv="<?php echo $value['id'];?>" onclick="addEditCategory(this);" class="text-black">
                                                        <em class="icon-pencil"></em>&nbsp;Edit
                                                    </a>
                                                    &nbsp;&nbsp;
                                                    <a href="javascript:void(0);" data-manage="<?php echo $value['id'];?>" data-type="delete" data-name="<?php echo $value['title'];?> category" class="text-black" onclick="showConfirmPopup(this);">
                                                        <em class="icon-trash"></em>&nbsp;Delete
                                                    </a>
                                                </div>
                                            </div>
                                       
                                            <div class="divider"></div>
                                  
                                            <div class="row" title="<?php echo $value['description'];?>">
                                                <div class="col-sm-5" style="border-right: 1px solid #e5e5e5;">
                                            <?php 
                                            $cnt = 0;
                                            //print_r($data_price[$value['id']]);
                                            foreach ($data_price[$value['id']] as $key => $value1) {
                                                ?>
                                                <div class="col-sm-2">
                                                    <?php echo $value1['code'];?>(<?php echo $value1['symbol'];?>)
                                                </div>

                                                <div class="col-sm-10">
                                                    <div class="row m-b-10">
                                                        <div class="col-sm-8">
                                                            Non-subscribers
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <?php echo $value1['price_for_unsubscribe'];?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            Subscribers
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <?php echo $value1['price_for_subscribe'];?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php 
                                                $cnt++;
                                                if ($cnt != count($data_price[$value['id']])) { ?>
                                                    <div class="divider"></div>
                                                <?php } ?>
                                            <?php } ?>
                                                </div>
                                                
                                                <div class="col-sm-7">
                                                    <?php 
                                                    $contents = '';
                                                    foreach ($data_content[$value['id']] as $key => $value1) {
                                                        $contents.= ', '.$value1['name'];
                                                    }
                                                    $contents = ltrim($contents, ', ');
                                                    echo $contents;
                                                    ?>
                                                </div>
                                                
                                                <div class="clear-fix"></div>
                                            </div>                                            
                                        </div>  
                                    </div>
                                  </div>
                                <?php  } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal start here -->
<div class="modal fade" id="ppvModal" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal  adv_modal" name="adv_modal" id="adv_modal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_ppv" name="id_ppv" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="ppvbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end here -->

<div class="modal fade" id="advpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>


<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/autosize.min.js"></script>

<script type="text/javascript">
    var sel_contents = '';
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
    
    function addEditCategory(obj) {
        var id_preorder = $(obj).attr('data-id_ppv');
                
        $.post("<?php echo Yii::app()->baseUrl; ?>/store/PreorderPopup",{'id_preorder' : id_preorder},function(res){
            $("#advpopup").html(res).modal('show');
            
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
            
            var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/store/PreorderContents", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.id;
                },
                itemText: function(item) {
                    return item.name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';            
            if (contnts.length) {
                for (var i in contnts) {
                    if (parseInt(contnts[i].content_id)) {
                        $("#content").tagsinput('add', { "content_id": contnts[i].id , "name": contnts[i].name});
                    }
                }
            }
            
            $(".bootstrap-tagsinput ").removeClass('col-md-8');
            
            if ($('.auto-size')[0]) {
                autosize($('.auto-size'));
            }
        });
    }
    
    function auto_grow(element) {
        element.style.height = "65px";
        element.style.height = (element.scrollHeight)+"px";
    }

    function showConfirmPopup(obj) {
        var type = $(obj).attr('data-type');
        var name = " "+$(obj).attr('data-name');
        var title = type.charAt(0).toUpperCase() + type.slice(1)+ name+"?";
        var text = "Are you sure you want to "+type+name+"?";
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },
        function(){
            deleteAdV(obj);
        });
    }
    
    function deleteAdV(obj) {
        $("#id_ppv").val($(obj).attr('data-manage'));
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/store/deletePreorder";
            
        $('#adv_modal').attr("action", action);
        document.adv_modal.submit();
    }
</script>