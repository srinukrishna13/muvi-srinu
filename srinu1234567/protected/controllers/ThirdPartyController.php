<?php class ThirdPartyController extends Controller {
    
     public function init() {
         parent::init();
     }

     public function actionThirdPartyList()
    {
        Yii::app()->theme = 'bootstrap';
        Yii::app()->layout = FALSE;
        $studio_id = Yii::app()->common->getStudiosId();
        
		$halfheight = is_numeric(@$_REQUEST['halfheight'])?$_REQUEST['halfheight']:''; 
        $halfwidth = is_numeric($_REQUEST['halfwidth'])?$_REQUEST['halfwidth']:''; 
        $movie_id = is_numeric($_REQUEST['movie_id'])?$_REQUEST['movie_id']:''; 
        $model = !empty($_POST['type']) && $_POST['type'] == 'physical' ? 'PGMovieTrailer' : 'movieTrailer';
        if ($movie_id){
           $getMovieTrailer = $model::model()->findByAttributes(array('movie_id' => $movie_id));
           $third_party_url = $getMovieTrailer->third_party_url; 
           $trailerIsConverted = $getMovieTrailer->is_converted;
           if($third_party_url!=''){
                if($trailerIsConverted == 1){
                    $info = pathinfo($third_party_url);
                    $ip_address = CHttpRequest::getUserHostAddress();
                    if (Yii::app()->aws->isIpAllowed()) {
                        $isLogEnable = 1;
                        if (isset(Yii::app()->user->add_video_log) && !Yii::app()->user->add_video_log) {
                            $isLogEnable = 0;
                        }
                        if(@$_POST['type'] == 'physical'){
                            $isLogEnable = 0;
                        }
                        if($isLogEnable == 1){
                            $video_log = new VideoLogs();                
                            $video_log->created_date = new CDbExpression("NOW()");
                            $video_log->ip = $ip_address;
                            $video_log->video_length = 0;
                            $video_log->movie_id = $movie_id;
                            $video_log->video_id = 0;
                            $video_log->content_type = 2;
                            $video_log->trailer_id = $getMovieTrailer->id;
                            $video_log->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
                            $video_log->studio_id = Yii::app()->common->getStudioId();
                            $video_log->watch_status = 'start';
                            $video_log->save();
                        }
                    }
                    $this->renderPartial('thirdpartylist', array('info' => $info, 'third_party_url' =>$third_party_url,'trailerIsConverted' => $trailerIsConverted, 'halfheight'=> $halfheight, 'halfwidth' => $halfwidth,));
                }
                    
            }else{
                $video_remote_url = $getMovieTrailer->video_remote_url; 
                $video_resolution = $getMovieTrailer->video_resolution;
                if ($video_remote_url != '') { 
                    if($trailerIsConverted  == 1){
                        
                    $multipleVideo = $this->getvideoResolution($getMovieTrailer['video_resolution'], $getMovieTrailer['video_remote_url']);
                    $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $getMovieTrailer['video_remote_url']);
                    $videoToBeplayed12 = explode(",", $videoToBeplayed);
                    $trailer_path = $videoToBeplayed12[0];
                    $defaultResolution = $videoToBeplayed12[1];
                    $cloudFront = Yii::app()->common->connectToAwsCloudFront($studio_id);
                    if (Yii::app()->common->isMobile()) {
                        $expires = time() + 50;
                    } else {
                        $expires = time() + 50;
                    }

                    $trailer_path = $cloudFront->getSignedUrl(array(
                        'url' =>  $videoToBeplayed12[0],
                        'expires' => $expires,
                    ));
                     $this->renderPartial('mp4trailerlist',array('trailer_path' => $trailer_path,'video_remote_url' => $video_remote_url,'video_resolution' => $video_resolution,'trailerIsConverted' => $trailerIsConverted,'multipleVideo' => $multipleVideo, 'defaultResolution' => $defaultResolution,'halfheight'=> $halfheight, 'halfwidth' => $halfwidth,'movie_id' => $movie_id));  
                        
                    }
                }
            }
        }
    }
}
    ?>
