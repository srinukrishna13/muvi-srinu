(function (factory) {
  if (typeof define === "function" && define.amd) {
    define(["jquery"], factory);
  } else {
    factory(jQuery);
  }
})(function ($) {

  "use strict";
  var cropper_height = 150;
  var cropper_width = 150;
  var log = function (o) {
        try {
		 
         // console.log(o)
        } catch (e) {}
      };

  function CropAvatar($element) {
    this.$container = $element;

    this.$avatarView = this.$container.find(".avatar-view");
    this.$avatar = this.$avatarView.find("img");
    this.$avatarModal = this.$container.find("#avatar-modal");
    this.$loading = this.$container.find(".loading");

    this.$avatarForm = this.$avatarModal.find(".avatar-form");
    this.$avatarUpload = this.$avatarForm.find(".avatar-upload");
    this.$avatarSrc = this.$avatarForm.find(".avatar-src");
    this.$avatarData = this.$avatarForm.find(".avatar-data");
    this.$avatarInput = this.$avatarForm.find(".avatar-input");
    this.$avatarSave = this.$avatarForm.find(".avatar-save");

    this.$avatarWrapper = this.$avatarModal.find(".avatar-wrapper");
    this.$avatarPreview = this.$avatarModal.find(".avatar-preview");

    this.init();
    log(this);
  }

  CropAvatar.prototype = {
    constructor: CropAvatar,

    support: {
      fileList: !!$("<input type=\"file\">").prop("files"),
      fileReader: !!window.FileReader,
      formData: !!window.FormData
    },

    init: function () {
      this.support.datauri = this.support.fileList && this.support.fileReader;

      if (!this.support.formData) {
        this.initIframe();
      }

      this.initTooltip();
      this.initModal();
      this.addListener();
    },

    addListener: function () {
      this.$avatarView.on("click", $.proxy(this.click, this));
      this.$avatarInput.on("change", $.proxy(this.change, this));
      this.$avatarForm.on("submit", $.proxy(this.submit, this));
    },

    initTooltip: function () {
      this.$avatarView.tooltip({
        placement: "bottom"
      });
    },

    initModal: function () {
      this.$avatarModal.modal("hide");
      this.initPreview();
    },

    initPreview: function () {
      var url = this.$avatar.attr("src");

      this.$avatarPreview.empty().html('<img src="' + url + '">');
    },

    initIframe: function () {
      var iframeName = "avatar-iframe-" + Math.random().toString().replace(".", ""),
          $iframe = $('<iframe name="' + iframeName + '" style="display:none;"></iframe>'),
          firstLoad = true,
          _this = this;

      this.$iframe = $iframe;
      this.$avatarForm.attr("target", iframeName).after($iframe);

      this.$iframe.on("load", function () {
        var data,
            win,
            doc;

        try {
          win = this.contentWindow;
          doc = this.contentDocument;

          doc = doc ? doc : win.document;
          data = doc ? doc.body.innerText : null;
        } catch (e) {}

        if (data) {
          _this.submitDone(data);
        } else {
          if (firstLoad) {
            firstLoad = false;
          } else {
            _this.submitFail(JSLANGUAGE.image_upload_failed);
          }
        }

        _this.submitEnd();
      });
    },

    click: function () {
		this.$avatarSrc.val("");
		this.$avatarData.val("");
		this.$avatarInput.val("");
		this.$avatarWrapper.html("");
		this.active = false;
		$('.avater-alert').remove();
		$('.avatar-save').html(JSLANGUAGE.btn_save);
		$('.avatar-save').removeAttr('disabled');
		this.$avatarModal.modal("show");
    },

    change: function () {
	  $('.avater-alert').remove();
	  this.$avatarSrc.val('');
	  this.active = false;
	  this.$avatarData.val('');
	  this.uploaded=false;
      var files, file;	  
      if (this.support.datauri) {
        files = this.$avatarInput.prop("files");

        if (files.length > 0) {
          file = files[0];
		 this.readImage(file);
		 
          if (this.isImageFile(file)) {
            this.read(file);
          }
        }
      } else {
        file = this.$avatarInput.val();

        if (this.isImageFile(file)) {
          this.syncUpload();
        }
      }
    },

    submit: function () {
      if (!this.$avatarSrc.val() && !this.$avatarInput.val()) {
        return false;
      }
	  $('.avatar-save').html(JSLANGUAGE.loading);
	  $('.avatar-save').attr('disabled','disabled');
      if (this.support.formData) {
        this.ajaxUpload();
        return false;
      }
    },

    isImageFile: function (file) {		
      if (file.type) {
        return /^image\/\w+$/.test(file.type);
      } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
      }
    },
	readImage:function(file) {
		var _this = this;
		var reader = new FileReader();
		var image  = new Image();
		reader.readAsDataURL(file);  
		reader.onload = function(_file) {
			image.src    = _file.target.result;              // url.createObjectURL(file);
			image.onload = function() {
				var w = this.width,
					h = this.height,
					t = file.type,                           // ext only: // file.type.split('/')[1],
					n = file.name,
					s = ~~(file.size/1024) +'KB';
					if(parseInt(w)<cropper_width || parseInt(h)<cropper_height){
						_this.$avatarSrc.val("");
						_this.$avatarData.val("");
						_this.$avatarInput.val("");
						_this.$avatarWrapper.html("");
						_this.active = false;
						_this.alert(JSLANGUAGE.invalid_filetype+ ' '+cropper_width+'X'+cropper_height);
					}
			};
			image.onerror= function() {
				_this.$avatarSrc.val("");
				_this.$avatarData.val("");
				_this.$avatarInput.val("");
				_this.active = false;
				_this.alert(JSLANGUAGE.invalid_filetype+ file.type);
			};      
		};    
	},
    read: function (file) {
      var _this = this,
          fileReader = new FileReader();

      fileReader.readAsDataURL(file);

      fileReader.onload = function () {
        _this.url = this.result
        _this.startCropper();
      };
    },

    startCropper: function () {
      var _this = this;

      if (this.active) {
        this.$img.cropper("setImgSrc", this.url);
      } else {
        this.$img = $('<img src="' + this.url + '">');
        this.$avatarWrapper.empty().html(this.$img);
        this.$img.cropper({
          aspectRatio: 1.1,
		  minWidth:cropper_width,
		  minHeight:cropper_height,
          preview: this.$avatarPreview.selector,
          done: function (data) {
            var json = [
                  '{"x":' + data.x,
                  '"y":' + data.y,
                  '"height":' + data.height,
                  '"width":' + data.width + "}"
                ].join();

            _this.$avatarData.val(json);
          }
        });

        this.active = true;
      }
    },

    stopCropper: function () {
      if (this.active) {
        this.$img.cropper("disable");
        this.$img.data("cropper", null).remove();
        this.active = false;
      }
    },

    ajaxUpload: function () {
      var url = this.$avatarForm.attr("action"),
          data = new FormData(this.$avatarForm[0]),
          _this = this;
      $.ajax(url, {
        type: "post",
        data: data,
        processData: false,
        contentType: false,

        beforeSend: function () {
          _this.submitStart();
        },

        success: function (data) {
          _this.submitDone(data);
        },

        error: function (XMLHttpRequest, textStatus, errorThrown) {
          _this.submitFail(textStatus || errorThrown);
        },

        complete: function () {
          _this.submitEnd();
        }
      });
    },

         syncUpload: function () {
      this.$avatarSave.click();
    },

    submitStart: function () {
      this.$loading.fadeIn();
    },

    submitDone: function (data) {
      log(data);

      try {
        data = $.parseJSON(data);
      } catch (e) {};

      if (data && data.state === 200) {
        if (data.result) {
          this.url = data.result;

          if (this.support.datauri || this.uploaded) {
            this.uploaded = false;
            this.cropDone();
          } else {
            this.uploaded = true;
            this.$avatarSrc.val(this.url);
			$('#fileinfo').val(data.fileinfo);
            this.startCropper();
			$('.avatar-save').html(JSLANGUAGE.btn_save);
			$('.avatar-save').removeAttr('disabled');
          }
          this.$avatarInput.val("");
		  window.location = "/user/profile";
        } else if (data.message) {
          this.alert(data.message);
        }
      } else {
		this.$avatarSrc.val("");
		this.$avatarData.val("");
		this.$avatarInput.val("");
		this.$avatarWrapper.html("");
		this.active = false;		
		$('.avatar-save').html(JSLANGUAGE.btn_save);
		$('.avatar-save').removeAttr('disabled');
		if(data.message){
			this.alert(data.message);
		}else{
			this.alert(JSLANGUAGE.error_saving_data);
		}
      }
    },

    submitFail: function (msg) {
      this.alert(msg);
    },

    submitEnd: function () {
      this.$loading.fadeOut();
    },

    cropDone: function () {
      this.$avatarSrc.val("");
      this.$avatarData.val("");
      this.$avatar.attr("src", this.url);
      this.stopCropper();
      this.$avatarModal.modal("hide");
    },

    alert: function (msg) {
        var message = JSLANGUAGE.upload_image;
        if(msg !=""){
            msg = message;
        }
      var $alert = [
            '<div class="alert alert-danger avater-alert">',
              '<button type="button" class="close" data-dismiss="alert">&times;</button>',
              msg,
            '</div>'
          ].join("");
      this.$avatarUpload.after($alert);
    }
  };
    $(document).ready(function () {
        $('#browsefile').click(function () {
          $('.avatar-wrapper').html("");
       });
    });
    
   $(function () {
    var example = new CropAvatar($("#crop-avatar"));
  });
});
