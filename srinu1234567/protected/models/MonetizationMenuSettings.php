<?php
class MonetizationMenuSettings extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'monetization_menu_settings';
    }
    
    public function getStudioSettings($studio_id) {
		if(isset($_SESSION[$studio_id]['MonitizationSettings'])){
			return $_SESSION[$studio_id]['MonitizationSettings'];
		}else{
			$data = $this->findByAttributes(array('studio_id' => $studio_id))->attributes;
			$_SESSION[$studio_id]['MonitizationSettings'] = @$data;
			return $data;
		}
    }
    
    public function updateMenuSettings($studio_id,$menu_sum) {
        $sql = "INSERT INTO monetization_menu_settings (studio_id, menu) VALUES(".$studio_id.", ".$menu_sum.") ON DUPLICATE KEY UPDATE studio_id=".$studio_id.", menu=".$menu_sum;
        $data = Yii::app()->db->createCommand($sql)->execute();
        return $data;
    }
    public function getStudioAdsWithMonitizationDetails($studio_id) {
			$data[0] = $this->findByAttributes(array('studio_id' => $studio_id))->attributes;
            if(count($data)>0){
                $data[1] =StudioAds::model()->getStudioAdsDetails($studio_id);
                //echo "<pre>";print_r($data);
                return $data;
            }
    }
}