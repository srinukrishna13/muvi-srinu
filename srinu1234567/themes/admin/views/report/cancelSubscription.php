<div class="box-body table-responsive no-padding">
	<div class="col-lg-12 box-body">
		<div class="box box-primary report" style="min-height: 300px;">
			<div>
				<div class="box-header">
					<h3 class="box-title">
						<select class="filter_dropdown" onChange="window.location.href=this.value"  id="filterbox">
							<option value="<?php echo $this->createUrl('report/userdata',array('type'=>'subscriber'));?>" <?php if($type=='subscriber'){?>selected="selected"<?php }?> >&nbsp;Subscriber</option>
							<option value="<?php echo $this->createUrl('report/userdata',array('type'=>'register'));?>" <?php if($type=='register'){?>selected="selected"<?php }?> >&nbsp;Registrations</option>
							<option value="<?php echo $this->createUrl('report/userdata',array('type'=>'cancel'));?>" <?php if($type=='cancel'){?>selected="selected"<?php }?>>&nbsp;Cancellations</option>
						</select>
					</h3>
                                        <button class="pull-right topper btn btn-primary" onclick="downloadCsv();">Download CSV </button>
				</div><!-- /.box-header -->
				<div>
					<div class="box box-primary">
						<div id="cancel-chart"></div>
					</div>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover tablesorter">
						<tbody>
						<tr>
							<th>Name</th>
							<th>City</th>
							<th>Country</th>
							<th>Cancel Date</th>
							<th>Subscription</th>
							<th>Reason</th>
							<th>Comment</th>
						</tr>	
						<?php
						if($data){
						foreach ($data as $key => $value) {
							$address 
							?>
						<tr>
							<td><?php echo $value['dname']?$value['dname']:$value['email'];?></td>
							<td><?php echo $value['city'];?></td>
							<td><?php echo $value['country']?@$countries[$value['country']]:'';?></td>
							<td><?php echo date('jS M Y',strtotime($value['cancel_date']));?></td>
							<td><?php echo ($value['plan_id']==1)?'Montly':'Yearly';?></td>
							<td><?php echo @$cancelReason[$value['cancel_reason_id']];?></td>
							<td><?php echo @$value['cancel_note'];?></td>
							<?php } ?>
						</tr>
					<?php	
						if($items_count>10){ ?>
						<tr>
							<td colspan="7" style="text-align: right;">
						 <?php
                                                if ($data) {
                                                    $opts = array('class' => 'pagination m-t-0 m-b-10');
                                                    $this->widget('CLinkPager', array(
                                                        'currentPage' => $pages->getCurrentPage(),
                                                        'itemCount' => $item_count,
                                                        'pageSize' => $page_size,
                                                        'maxButtonCount' => 6,
                                                        "htmlOptions" => $opts,
                                                        'selectedPageCssClass' => 'active',
                                                        'nextPageLabel' => '&raquo;',
                                                        'prevPageLabel'=>'&laquo;',
                                                        'lastPageLabel'=>'',
                                                        'firstPageLabel'=>'',
                                                        'header' => '',
                                                    ));
                                                }
                                                ?>
							</td>
						</tr>
						<?php }
						}else{?>
						<tr>
							<td colspan="5">No Record found!!!</td>
						</tr>	
					<?php	}?>
					</tbody></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
/**
 * Sand-Signika theme for Highcharts JS
 * @author Torstein Honsi
 */

// Load the fonts
Highcharts.createElement('link', {
   href: 'http://fonts.googleapis.com/css?family=Signika:400,700',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);

// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
   proceed.call(this);
   this.container.style.background = 'url(http://www.highcharts.com/samples/graphics/sand.png)';
});


Highcharts.theme = {
   colors: ["#f45b5b", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: "Signika, serif"
      }
   },
   title: {
      style: {
         color: 'black',
         fontSize: '16px',
         fontWeight: 'bold'
      }
   },
   subtitle: {
      style: {
         color: 'black'
      }
   },
   tooltip: {
      borderWidth: 0
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      labels: {
         style: {
            color: '#6e6e70'
         }
      }
   },
   yAxis: {
      labels: {
         style: {
            color: '#6e6e70'
         }
      }
   },
   plotOptions: {
      series: {
         shadow: true
      },
      candlestick: {
         lineColor: '#404048'
      },
      map: {
         shadow: false
      }
   },

   // Highstock specific
   navigator: {
      xAxis: {
         gridLineColor: '#D0D0D8'
      }
   },
   rangeSelector: {
      buttonTheme: {
         fill: 'white',
         stroke: '#C0C0C8',
         'stroke-width': 1,
         states: {
            select: {
               fill: '#D0D0D8'
            }
         }
      }
   },
   scrollbar: {
      trackBorderColor: '#C0C0C8'
   },

   // General
   background2: '#E0E0E8'
   
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);	
$(function () {
    $('#cancel-chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Cancel Reason with its type'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Cancel Reason',
            data:<?php echo $xdata;?>
        }]
    });
});
</script>

<script type="text/javascript">
    function downloadCsv() {
        var optText = $.trim(($("#filterbox option:selected").text()).toLowerCase());
       // alert(optText);
        var type = '';
        if(optText === 'registrations'){//alert(1);
            type = 'register';
        }else if(optText === 'subscriber' ){
            type = 'subscriber';
        }else if(optText === 'cancellations'){//alert(1);
            type = 'cancel';
        }
        window.location.href = HTTP_ROOT+"/report/downloadCsv?type="+type;
      
    }
</script>