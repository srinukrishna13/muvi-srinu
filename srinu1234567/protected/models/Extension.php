<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Extension extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'extensions';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }   
    
    public function findAllExtensions() {
        $exts = Extension::model()->findAll(array(
            'condition' => 'status=:status',
            'params' => array(':status' => 1),
            'order' => 't.id ASC'
        ));          
        return($exts);        
    } 
    
    public function findExtensionsByName() {
        $exts = Extension::model()->findAll(array(           
            'condition' => 'name=:name and status=:status',
            'params' => array(':name' =>'Playout',':status'=>1),
            'order' => 't.id ASC'
        ));          
        return($exts);        
    } 
    
    
}

