<?php
class FeaturedContent extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'featured_content';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
            'section'=>array(self::HAS_ONE, 'FeaturedSection', 'section_id'),
            'movie'=>array(self::BELONGS_TO, 'Film', 'movie_id'),
        );
    } 
    public function getMaxOrd($studio_id,$section_id){
        $cnt = FeaturedContent::model()->find(array(
            'select' => 'id_seq',
            'condition' => 'studio_id=:studio_id AND section_id=:section_id',
            'params' => array(':studio_id' => $studio_id,':section_id'=>$section_id),
            'order' => 'id_seq DESC',
            'limit' => '1'
        ));   
        if(isset($cnt) && count($cnt)){
            return (int) $cnt->id_seq;
        }
        else{
            return 0;
        }      
    }
}

