<?php
class Transaction extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'transactions';
    }    
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sdk_user'=>array(self::HAS_ONE, 'SdkUser','user_id'),
        );
    } 
    
    public function backupAllData($studio_id)
    {
        $dbcon = Yii::app()->db;
        $sql = "INSERT INTO ".$this->tableName()."_history SELECT * FROM ".$this->tableName()." WHERE studio_id=".$studio_id;
        $data = $dbcon->createCommand($sql)->execute();
        return $data;
    }
    public function insertTrasactions($studio_id,$user_id,$currency_id,$trans_data,$transaction_type,$ip,$payment_gateway){
        $this->user_id = $user_id;
        $this->studio_id = $studio_id;
        $this->currency_id = $currency_id;
        $this->transaction_date = new CDbExpression("NOW()");
        $this->payment_method = $payment_gateway;
        $this->transaction_status = $trans_data['transaction_status'];
        $this->transaction_status_reason = $trans_data['transaction_status_reason'];
        $this->invoice_id = $trans_data['invoice_id'];
        $this->order_number = $trans_data['order_number'];
        $this->dollar_amount = $trans_data['dollar_amount'];
        $this->amount = $trans_data['paid_amount'];
        $this->bill_amount = $trans_data['bill_amount'];
        $this->response_text = $trans_data['response_text'];
        $this->ip = $ip;
        $this->created_date = new CDbExpression("NOW()");
        $this->transaction_type = $transaction_type;
        $this->save();
        $last_inserted_id=$this->id;
        return $last_inserted_id;
    }
}
