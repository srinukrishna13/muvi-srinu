<div class="wrapper">
    <div class="container home-page-studio">
        <h2 class="btm-bdr">Muvi SDK</h2>
        <h3>Launch fully featured VOD platform available on web, mobile and TV at ZERO CapEx. Get up<br /> 
            and running in 1 day. Muvi SDK takes care of everything including hosting, streaming, DRM, payment <br />gateway and maintenance.</h3>
    </div>
</div>


<div class="wrapper blubg" id="navblubar" style="z-index:100;">
    <div class="container home-page-customers barlinks">
        <div class="span9">
            <a href="#overview">Features</a>
            <a href="#examples">Examples</a>
            <a href="#tour">Tour</a>
            <a href="#pricing">Pricing</a>
            <a href="#downloads">Downloads</a>
            <a href="#" data-toggle="modal" data-target="#contactModal" data-backdrop="static">Contact Us</a>
        </div>
        
        <div class="span3 pull-right top_social">
            <ul class="social">
                <li class="pull-right"><a href="https://www.linkedin.com/company/muvi-studio" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linkedin.png" alt="linkedin" /></a></li>                
                <li class="pull-right"><a href="https://www.facebook.com/MuviStudioB2B" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/facebook.png" alt="facebook" /></a></li>
                <li class="pull-right"><a href="http://www.twitter.com/muvistudiob2b" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/twitter.png" alt="twitter" /></a></li>
                
            </ul>
        </div>          
    </div>
</div> 

<div class="wrapper whtbg" id="overview">
    <div class="container pad50">
        <div class="row-fluid">
            <div class="span7">
                <div class="item zerocap">
                    <h2 class="blue">Zero CapEX</h2>
                    <p>No capital expenditure required to purchase expensive hardware for hosting or bandwidth to support video streaming. Muvi takes care of 
                        all this. Get your website and mobile app up and running with investing $0, really!</p>  
                </div>
            </div>
            <div class="span4 pull-right txt-right">
                <div class="item">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/zero-capex.png" alt="zero-capex" />
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="second">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/quick-to-lunch.png" class="move_top" alt="quick-to-lunch" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item ql">
                <h2 class="blue">Quick to Launch</h2>
                <p>Get the website up and running within a few hours. <br />Save millions of dollars by reducing time to market.</p>            
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="third">
    <div class="container pad50">

        <div class="span7">
            <div class="item infscl">
                <h2 class="blue">Infinite Scalability</h2>
                <p>Muvi SDK leverages world&rsquo;s leading cloud infrastructure, Amazon Web Services, to deliver unlimited storage and infinite scalability. 
                    Support millions of traffic and views with no worries about bandwidth and maintenance.</p>    
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/scalability.png" alt="scalability" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="fourth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/drm.png" alt="drm" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item drm">
                <h2 class="blue">DRM Across All Devices</h2>
                <p>Muvi offers content encryption across different platforms. It ensures the consistency of your business model across different devices and 
                    applications.</p>       
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="fifth">
    <div class="container pad50">

        <div class="span7">
            <div class="item custlook">
                <h2 class="blue">Customize Look and Feel</h2>
                <p>Customize the entire website and mobile app to match the look and feel of your brand. It&rsquo;s much more than just changing your logo or 
                    color scheme, and much more!</p>  
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/customize-look.png" alt="customize-look" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="sixth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/payperview.png" alt="payperview" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item ppv">
                <h2 class="blue">Pay-per-view and Subscription Models</h2>
                <p>Content management system (CMS) allows complete control on price and geographical distribution. Supports different revenue  models-pay-per-view,
                subscription and hybrid. Advertising platform allows you to integrate different types of ads in your content.</p>    
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="seventh">
    <div class="container pad50">

        <div class="span7">
            <div class="item webmob">
                <h2 class="blue">Web, Mobile and TV</h2>
                <p>Create brand presence across all platforms - web, mobile and TV, Muvi SDK powers your streaming website, mobile app (iPhone, Android, 
                    Windows Phone) and smart TV app.</p>  
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/web-mobile.png" alt="web-mobile" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="eightth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/marketing-tool.png" alt="marketing-tool" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item bltmrkt">
                <h2 class="blue">Built-in Marketing Tools</h2>
                <p>Muvi SDK offers several built-in marketing tools such as SEO-optimized website, user database for email marketing and social media 
                    integration. Custom viral apps are also available to promote your site and content.</p>   
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="nineth">
    <div class="container pad50">

        <div class="span7">
            <div class="item audrel">
                <h2 class="blue">Audience Relationship Management</h2>
                <p>Own your audience not just for one movie, but for a lifetime (and more if you believe in second life :)). Filter using several criteria including 
                    city/country, age group and taste. Send customized emails, sell additional pay-per-view content.</p>       
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/audiance-relationship.png" alt="audiance-relationship" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="examples">
    <div class="container pad50 center">

        <h2 class="blue">Examples</h2>
        <p><a href="http://www.maaflix.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/maatv.png" alt="maatv" /></a></p><br />
        <p>MAA TV is one of India&rsquo;s largest TV channel and media conglomerate. Muvi SDK powers the streaming <br />
            website of MAA TV <a target="_blank" href="http://www.maaflix.com">www.maaflix.com</a>.</p>

    </div>    
</div>

<div class="wrapper whtbg" id="tour">
    <div class="container pad50 center">

        <h2 class="blue">Tour of Admin Panel</h2>
        <div class="container pad50">
            <div class="row">
                <div style="position:relative;">
                <div id="pic_slider" class="pics pull-left" style="position: relative; overflow: hidden;">
                  <?php  
                  $demo_form = '<p>Fill up the following form to request a live demo of Muvi SDK.</p><br /><form class="form-inline" name="demo_form"  method="post"><input type="text" class="form-control" id="demo_name" name="demo_name" placeholder="Name" /><br/><br/><input type="text" class="form-control" id="demo_company" name="demo_company" placeholder="Company" /><br/><br/><input type="text" class="form-control" id="demo_phone" name="demo_phone" placeholder="Phone Number" /><br/><br/><input type="email" class="form-control" id="demo_email" name="demo_email" placeholder="Email Address" /><br/><br/><button type="submit" class="btn btn-blue" id="demo_submit" >Request Live Demo</button><div id="demo_error" class="error"></div></form>';
                  $tour_desc = array(
                      array("img" => "Choose-a-template.jpg", "heading"=>"Chose a template","desc" => "Chose one of the available templates, add your own or ask our design team to custom design a template."),
                      array("img" => "Easily-upload-videos.jpg", "heading"=>"Easily upload videos","desc" => "Easily upload videos using your web browser. Depending on your Internet speed, a full length movie takes less than 30 minutes. "),
                      array("img" => "Manage-meta-data.jpg", "heading"=>"Manage metadata","desc" => "Manage metadata of your content including trailer, actors, poster, genre and more. All metadata becomes visible on the content page making it more interesting for users and are also used for search."),
                      array("img" => "Set-pricing-model.jpg", "heading"=>"Set pricing model (SVOD, TVOD)","desc" => "Set different pricing models for your content including pay-per-view and subscription. A ton of options are available including offers, coupons and discounted pricing for subscribers."),
                      array("img" => "Analytics.jpg", "heading"=>"Analytics","desc" => "View easy to understand and in-depth analytics of subscribers, content, what users are searching for and more."),
                      array("img" => "Reports.jpg", "heading"=>"Reports","desc" => "View more than a dozen reports available including revenue and website traffic."),
                      array("img" => "Email-marketing.jpg", "heading"=>"Subscriber Communication","desc" => "Leverage built-in email marketing tools to send custom emails to subscribers. Great for retaining and building your subscriber base."),
                      array("img" => "live_demo.jpg", "heading"=>"Live Demo", "desc" => $demo_form) 
                );
                $cnt = count($tour_desc);
                for($i = 0; $i < $cnt ; $i++){ ?>
                
                <img id ="img_tour_<?php echo $i; ?>" src="<?php echo Yii::app()->theme->baseUrl."/images/tour/".$tour_desc[$i]["img"]; ?>" width="100%" height="100%" style=" top: 0px; left: 0px; display: block; z-index: 4; opacity: 1; width: 100%; height: 100%; ">
                
<?php  } ?>
                </div>
                <div id="tour_next" class="tour_next"> </div>
                <div id="tour_prev" class="tour_prev"> </div>
            </div>
    <?php for($i = 0; $i < $cnt ; $i++){ ?>
        
                <div id="desc_tour_<?php echo $i; ?>" class="tour_detail">
                    <div class="tour_heading"><?php echo $tour_desc[$i]['heading']?></div>
                    <div class="clear" style="height:10px;"></div>
                    <div class="tour_desc"><?php echo $tour_desc[$i]['desc']?></div>
                </div>
    <?php } ?>
                     
                <div id="output" class="output_tour">
                    
                </div>
            </div>
        </div>
           
        <script type="text/javascript">
            $(document).ready(function(){  
                $('#pic_slider').cycle({ 
                    fx:     'fade', 
                    timeout: 0, 
                    next:   '#tour_next', 
                    prev:   '#tour_prev', 
                    //before:  onBefore, 
                    after:   onAfter 
                 });
            });
            $("#demo_submit").live("click", function(){
                $("#demo_form").validate({
                    wrapper: "li",
                    rules: {    
                        demo_name: {
                            required: true,
                            minlength: 5
                        },                
                        demo_email: {
                            required: true,
                            mail: true
                        },
                        demo_phone: {
                            required: true
                        },
                        demo_company: {
                            required: true,
                            minlength: 5
                        },                        
                    },                      
                    messages: {             
                        demo_name: {
                            required: "Please enter your name, &nbsp;&nbsp;&nbsp;",
                            minlength: "Name must be atleast of 5 characters long, &nbsp;&nbsp;&nbsp;",
                        },                
                        demo_email: {
                            required: "Please enter email address, &nbsp;&nbsp;&nbsp;",
                            mail: "Please enter a valid email address,&nbsp;&nbsp;&nbsp;",
                        },
                        demo_phone: {
                            required: "Please enter phone number, &nbsp;&nbsp;&nbsp;"
                        },
                        demo_company: {
                            required: "Please enter your company name",
                            minlength: "Company name must be atleast of 5 characters long",
                        },                          
                    },
                    errorPlacement: function(error, element) {
                        error.appendTo('#demo_error');
                    },        
                    submitHandler: function(form) {
						$.ajax({
                            url: "<?php echo Yii::app()->getbaseUrl(true); ?>/sdk/demorequest",
                            data: $('#demo_form').serialize(),
                            type:'POST',
                            dataType: "json",
							 beforeSend:function(){
								ga('send', 'event', { eventCategory: 'demo', eventAction: 'submit', eventLabel: 'demoform'});
							},
                            success: function (data) {                                
                                if(data.stat == 'success'){
                                    bootbox.alert(data.message, function(){
                                       // window.location = '<?php echo Yii::app()->getbaseUrl(true); ?>/sdk';
                                    });                                    
                                }else{
                                    $('#demo_error').html('data.message');
                                    return false;
                                }
                            }
                        });
                    }                    
                });                
            });
            
            function onAfter() { 
                var id_spl = this.id.split("_");
                var id_no = id_spl[2];
                var desc_content = $("#desc_tour_"+id_no).html();
                $('#output').html(desc_content);
                $('#demo_error').html("");
                $('#output').find('form').attr("id", "demo_form");
            }
        </script>
        
    </div>    
</div>


<div class="wrapper blubg white martop0" id="pricing">
    <div class="container pad50">
        <div class="row">
            <div class="col-center">
                <h2>ZERO CapEX. ZERO Upfront Investment. Pay As You Use</h2>
            </div>
            <div>
                <p>
                    <span style="font-weight: bold;">$500 per month:</span>
                    <span style="font-weight: 300;">Covers everything including hosting, unlimited storage, DRM, analytics, technical support and upto 2TB bandwidth. Bandwidth consumed more than 2TB is charged at the following rate.</span>
                </p>
            </div>
            <div style="clear:both; height: 10px"></div>
            <table border="1" style="width:100%;border-color: #fff;border:1px solid #fff;" align="center" cellpadding="10px">
                <tr>
                    <th>Bandwidth</th>
                    <th>2TB to 10TB per mo</th>
                    <th>10TB to 100TB per mo</th>
                    <th>100TB to 500TB per mo</th>
                    <th> > 500TB per mo</th>
                </tr>
                <tr>
                    <th>Price</th>
                    <th>$0.16 per GB</th>
                    <th>$0.14 per GB</th>
                    <th>$0.12 per GB</th>
                    <th>Ask Sales</th>
                </tr>
            </table>
            <div style="clear:both; height: 10px"></div>
            <div>
                <a href="<?php echo Yii::app()->baseUrl; ?>/sdk/pricing/calculator" style="margin-right: 10px;">Pricing Calculator:</a><span> Try the pricing calculator to find your estimated revenue and cost</span>
            </div>
            <div style="clear:both; height: 20px"></div> 
        </div>
    </div>    
</div>


<div class="wrapper whtbg" id="howisitdifferent">
    <div class="container pad50">
        <div class="col-center">
        <h2 class="blue">How Is Muvi SDK Different</h2>
        </div>
        <div class="span6" style="margin-left: 5px;">
            <div class="item">
                <p>Muvi SDK is the only end-to-end solution available worldwide for launching your custom-branded VOD platform.</p>
                <br/>
                <p>Diagram on the right illustrates different components that go into building a VOD platform. Most other products offer only 1 or 2 of these components, you have to build the rest. </p>
                <p>For example, YouTube and Vimeo offer just a video player which is less than 10% of the total effort, you still have to build rest of the website, host and maintain it.</p>
                <br/>
                <p>Muvi SDK offers the entire solution for a simple, low price. Nothing else required.  </p>       
            </div>
        </div>
        <div class="span6 pull-right txt-right"  style="margin-left: 5px;">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/HowIsSDKDifferent.png" alt="How Is SDK Different" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="faqs">
    <div class="container pad50">
        <div class="col-center">
        <h2 class="blue">Frequently Asked Questions</h2>
        </div>
        <div class="span12" style="margin-left: 5px;">
            <div id="question_1" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">How is it different from YouTube and Vimeo?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_1" class="ans_block">
                    YouTube and Vimeo offer only a player that you can embed in your VOD platform. You still have to build the rest of the VOD platform which includes website, payment gateway, analytics and much more. Please refer <a href="<?php echo Yii::app()->baseUrl;?>/sdk#howisitdifferent">www.muvi.com/sdk#howisitdifferent</a>. 
With Muvi SDK, you get “www.myname.com” whereas with YouTube or Vimeo, you get “www.vimeo.com/myname”. 

            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_2" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">Does it support all pricing models, SVOD, AVOD and TVOD?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_2" class="ans_block">
                    Yes. Muvi SDK supports all pricing models for your VOD platform including SVOD (subscription), AVOD (advertising) and TVOD (pay-per-view), and a mix and match of all these models. You can configure all these from the Admin Panel.
            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_3" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">Are my content secure? Does it have DRM?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_3" class="ans_block">
                    Yes. Muvi SDK uses world-class DRM, very similar to the one used by Netflix. Your content are encrypted and completely secure. Only the users who have paid for or authorized to view the content can view it.
            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_4" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">Can I customize the look and feel of the website?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_4" class="ans_block">
                Yes, you can completely customize the look and feel of your VOD platform. Customization goes much beyond than just replacing logo and color scheme. You can completely change the layout so that your VOD platform looks very unique like no other.
Select one of the available templates or add your custom design or ask our design team to produce a custom design for you. All options are available.
            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_5" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">How much does Muvi SDK cost?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_5" class="ans_block">
                First month is free. Then, pay a low monthly fee of $500 + a fee based on bandwidth consumed. See pricing to learn more. $500 per month includes everything including hosting, storage, DRM, CDN, player and all website features. 
                The bandwidth based <a href="<?php echo Yii::app()->baseUrl; ?>/sdk#pricing">pricing</a> allows you to pay as you grow. More users you have>more revenue you make>more bandwidth you consume >and only accordingly, you pay more.
            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_6" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">Who collects the money from subscribers?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_6" class="ans_block">
                You. <br/>
It’s your platform and subscribers. Muvi SDK uses your payment gateway, all of the money collected from your users minus the nominal payment gateway fee goes to your bank account. 
            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_7" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">I am interested in launching a mobile app. Does Muvi SDK support this?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_7" class="ans_block">
                Support for mobile app is currently in Beta. It will be publicly available in Q2 2015. 
It will use the same CMS (content management system). All videos, metadata and settings you currently add will apply as it is. You won’t have to re-upload or update anything. 

            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_8" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">Can I try this out?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_8" class="ans_block">
                You can see the user interface (VOD website) in <a href="<?php echo Yii::app()->baseUrl; ?>/sdk#examples">Examples</a>, and screenshots of the Admin Panel in <a href="#">Tours</a> section. 
                Or, <a href="#" data-toggle="modal" data-target="#contactModal" data-backdrop="static">Contact Us</a> for a live demo. A Muvi SDK representative will schedule a live demo with you within 48 hours. 
            </div>
            <div class="clear" style="height: 15px;"></div>
            <div id="question_9" class="faq_block">
                <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                <div class="pull-left que">I am not interested in launching my own VOD platform. What other choices do I have to monetize my content?</div>
            </div>
            <div class="clear"></div>
            <div id="ans_9" class="ans_block">
                You can upload your content on <a href="http://www.muvi.com" target="_blank">Muvi.com</a>. It works very similar to YouTube, i.e., users consume the content on Muvi.com and you earn a revenue for every view. Learn more at <a href="http://www.muvi.com/muvi">http://www.muvi.com/muvi</a>.
            </div>
            <div class="clear" style="height: 15px;"></div>
            
        </div>
        
    </div>    
</div>

<div class="wrapper" id="downloads">
    <div class="container">
        <div class="row-fluid">
            <div class="paddlr20">
                <h2 class="blue center">Downloads</h2>
                <p>
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/pdf.png" /> <a href="javascript: return false;" data-toggle="modal" data-target="#download_pop"  onClick="ga('send', 'event', { eventCategory: 'brochure', eventAction: 'submit', eventLabel: 'brochuredownload'});">Muvi SDK Brochure</a>
                </p>
            </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
$(document).ready(function(){
    $(document).scroll(function(){
        if($(this).scrollTop() > 872){
            $('body').addClass('fixedtop');
        }
        else
        {
            $('body').removeClass('fixedtop');
        }
        //console.log($(this).scrollTop() + ' and ' +$('#navblubar').position().top);
    });
});

$(document).ready(function(){
    $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
    $(".faq_block").click(function(){
        var obj_id = this.id;
        var sl = obj_id.split('_');
        var sl_no = sl[1];
        //$(this).hide();
        $("#ans_"+sl_no).slideToggle("slow");
        $("#question_"+sl_no+" .plus_icon").toggle();
        $("#question_"+sl_no+" .minus_icon").toggle();
        //$("#question_"+sl_no).toggle("slow");
    });
    $(".que").hover(
        function(){
            $(this).css("text-decoration","underline");
        }, function () {
           $(this).css({"text-decoration":"none"});
         }
    );
    
});
</script>
<!--Download Pop up form-->
<div id="download_pop" class="modal fade" style="display: none;" data-backdrop="static">
    <div class="modal-dialog" id="dwnl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Fill your information to download the document.</h4>
            </div>
            <div class="modal-body">
                <div class="loading" id="dwnld_loading"></div>
                <div class="clear"></div><br>
                <form class="form-horizontal" id="download_frm">
                  <div class="form-group">
                    <label for="dnld_name" class="col-sm-2 control-label">Name</label>
                    <div class="span3">
                        <input type="text" class="form-control" id="dnld_name" name="dnld_name" placeholder="Name" required autocomplete="off" />
                        <div class="clear"></div>
                    </div>
                  </div>                    
                  <div class="form-group">
                    <label for="dnld_email" class="col-sm-2 control-label">Email Address</label>
                    <div class="span3">
                        <input type="email" class="form-control" id="dnld_email" name="dnld_email" placeholder="Email Address" required autocomplete="off" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="dnld_cpmpany" class="col-sm-2 control-label">Company</label>
                    <div class="span3">
                        <input type="text" class="form-control" id="dnld_cpmpany" name="dnld_cpmpany" placeholder="Company" autocomplete="off" />
                    </div>
                  </div>                    
                  <div class="form-group">                   
                      <div class="span2" style="margin-left:165px;">
                            <input type="submit" id="download_btn" class="btn btn-blue" value="Submit" />
                      </div>
                  </div>
                </form>               
                <div class="clearfix"></div>
                <div class="error center" id="dwnld-errors"></div>   
                <br />
            </div>
        </div>        
    <script type="text/javascript">
        $(function() {
            $('#dwnld_loading').hide();
            $('#download_btn').click(function(){
                
                $("#download_frm").validate({
                    rules: {    
                        dnld_name: {
                            required: true,
                            minlength: 5
                        },                
                        dnld_email: {
                            required: true,
                            mail: true
                        },
                    },
                    messages: {             
                        dnld_name: {
                            required: "Please enter your full name",
                            minlength: "Name must be atleast of 5 characters long",
                        },                
                        dnld_email: {
                            required: "Please enter email address",
                            mail: "Please enter a valid email address",
                        },
                    },                       
                    submitHandler: function(form) {
                        $.ajax({
                            url: "<?php echo Yii::app()->getbaseUrl(true); ?>/sdk/downloadsubmit",
                            data: $('#download_frm').serialize(),
                            type:'POST',
                            dataType: "json",
                            beforeSend:function(){
                                $('#dwnld_loading').show();
								ga('send', 'event', { eventCategory: 'brochure', eventAction: 'submit', eventLabel: 'brochuredownload'});
                            },
                            success: function (data) {                                
                                if(data.stat == 'success')
                                {
                                    $('button.close').trigger('click');   
                                    $('#dwnld_loading').hide();
                                    $('#dnld_name').val('');
                                    $('#dnld_email').val('');
                                    $('#dnld_cpmpany').val('');
                                    window.location = data.message;
                                }
                                else
                                {
                                    $('#dwnld-errors').html(data.message);
                                    return false;
                                }
                            }
                        });
                    }
                });                                            
            });
        });

    </script>     
    </div>
</div> 

<?php require_once dirname(__FILE__).'/../layouts/contactform.php';?>
