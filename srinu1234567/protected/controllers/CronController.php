<?php

require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Guzzle\Http\EntityBody;
use Aws\Ses\SesClient; //for sending email by amason ses sdk

require_once 'FirstDataApi/FirstData.php';
require_once 'Paypal/paypal_pro.inc.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';

class CronController extends Controller {

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $GLOBALS['video_bitrate'] = array(1210, 900, 317, 242, 108);
        $GLOBALS['video_resolution'] = array(2160, 1440, 1080, 720, 480, 360, 240, 144);
        $GLOBALS['video_screen_resolution'][2160] = 3840;
        $GLOBALS['video_screen_resolution'][1440] = 2560;
        $GLOBALS['video_screen_resolution'][1080] = 1920;
        $GLOBALS['video_screen_resolution'][720] = 1210;
        $GLOBALS['video_screen_resolution'][640] = 900;
        $GLOBALS['video_screen_resolution'][480] = 480;
        $GLOBALS['video_screen_resolution'][360] = 317;
        $GLOBALS['video_screen_resolution'][240] = 242;
        $GLOBALS['video_screen_resolution'][144] = 108;
        $GLOBALS['screen_resolution'][1210] = '1280:720';
        $GLOBALS['screen_resolution'][900] = '1138:640';
        $GLOBALS['screen_resolution'][317] = '640:360';
        $GLOBALS['screen_resolution'][242] = '426:240';
        $GLOBALS['screen_resolution'][108] = '256:114';
        $GLOBALS['frame_per_sec'][1210] = '25';
        $GLOBALS['frame_per_sec'][900] = '25';
        $GLOBALS['frame_per_sec'][317] = '25';
        $GLOBALS['frame_per_sec'][242] = '25';
        $GLOBALS['frame_per_sec'][108] = '12';
        $GLOBALS['default_video_bitrate'] = 0;
        $GLOBALS['video_bandwidth'][2160] = 1000000;
        $GLOBALS['video_bandwidth'][1440] = 750000;
        $GLOBALS['video_bandwidth'][1080] = 650000;
        $GLOBALS['video_bandwidth'][720] = 550000;
        $GLOBALS['video_bandwidth'][480] = 450000;
        $GLOBALS['video_bandwidth'][360] = 300000;
        $GLOBALS['video_bandwidth'][240] = 200000;
        $GLOBALS['video_bandwidth'][144] = 100000;
        $GLOBALS['video_bandwidth_average'][2160] = 10000000;
        $GLOBALS['video_bandwidth_average'][1440] = 9900000;
        $GLOBALS['video_bandwidth_average'][1080] = 740000;
        $GLOBALS['video_bandwidth_average'][720] = 640000;
        $GLOBALS['video_bandwidth_average'][480] = 540000;
        $GLOBALS['video_bandwidth_average'][360] = 440000;
        $GLOBALS['video_bandwidth_average'][240] = 290000;
        $GLOBALS['video_bandwidth_average'][144] = 190000;
        return true;
    }

    function actionCronTest() {
        $email = 'sridhar@muvi.com';
        $FirstName = 'Sridhar';

        $subject = "Cron Test";
        $content = '<p>Dear ' . $FirstName . ',</p>
                    <p>Time: ' . gmdate('Y-m-d H:i:s') . '</p> 
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Muvi Team </p>';

        $from_mail = 'sunil@muvi.com';

        $params = array(
            'name' => $FirstName,
            'mailcontent' => $content
        );

        $message = array(
            'subject' => $subject,
            'from_email' => $from_mail,
            'from_name' => 'Sunil',
            'to' => array($email, 'sunil@muvi.com')
        );

        $template_name = 'sdk_user_welcome_new';

        $obj = new Controller();
        $to = array($email, 'sunil@muvi.com');
        $from = $from_mail;
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new', array('params' => $params), true);
        $returnVal = $obj->sendmailViaAmazonsdk($thtml, $subject, $to, $from);
    }

    function actionTimeTest() {
        $last_dt = new DateTime('2015-05-29 09:36:08');
        $today = new DateTime(gmdate('Y-m-d H:i:s'));
        $interval = $today->diff($last_dt);
        $hours = $interval->h;
        $hours = $hours + ($interval->days * 24);
        print 'Today: ' . gmdate('Y-m-d H:i:s') . '<br/>Total Hours: ' . $hours;
        exit;
    }

    /**
     * @method public Video_logs() It will fetch the list of movies from the Postgre DB and insert them into MYSQL DB
     * @author GDR<support@muvi.com>
     * @return bool ture/false
     */
    function actionUserData() {
        $key = 0;
        $connection = Yii::app()->db;
        $sql = 'SELECT last_update FROM user ORDER BY last_update DESC LIMIT 1';
        $command = $connection->createCommand($sql);
        $rowCount = $command->execute();
        if ($rowCount) {
            $rec = $command->queryAll();
            $cond = " WHERE   updated_at >'" . $rec[0]['last_update'] . "'";
        } else {
            $cond = '';
        }
        $sql_pg = "SELECT * FROM studio_users " . $cond . " ORDER BY id ASC";

        $dbcon = Yii::app()->db1;
        $command_pg = $dbcon->createCommand($sql_pg);
        $rowCount_pg = $command_pg->execute();
        $list = $command_pg->queryAll(); //Execute a query SQL
        //echo "<pre>";print_r($list);exit;
        if ($list) {
            $sql_ins = '';
            foreach ($list as $key => $val) {
                if (!$key) {
                    $sql_ins = "INSERT INTO user VALUES ";
                }
                //`id`, `email`, `encrypted_password`, ` is_admin`, `first_name`, `last_name`, `logo`, `studio_license`, `address_1`, `address_2`, `city`, `state`, `zip`, `phone_no`, `credit_card_type`, `credit_card_no`, `credit_card_cvv`, `credit_card_mm`, `credit_card_yyyy`, 
                //`country`, `company`, `type_of_company`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `created_at`, `updated_at`, `is_active`, `approved`, `password_salt`, `remember_token`, `last_update`
                $sql_ins .= " (" . $val['id'] . ",'" . $val['email'] . "','" . addslashes($val['encrypted_password']) . "','" . $val['is_admin'] . "',"
                        . "	'" . addslashes($val['first_name']) . "','" . addslashes($val['last_name']) . "','" . addslashes($val['logo']) . "','" . addslashes($val['studio_license']) . "',"
                        . "	'" . addslashes($val['address_1']) . "','" . addslashes($val['address_2']) . "','" . addslashes($val['city']) . "','" . addslashes($val['state']) . "','" . addslashes($val['zip']) . "',"
                        . "	'" . addslashes($val['phone_no']) . "',' ','" . addslashes($val['credit_card_no']) . "','" . addslashes($val['credit_card_cvv']) . "','" . addslashes($val['credit_card_mm']) . "','" . addslashes($val['credit_card_yyyy']) . "','" . addslashes($val['country']) . "',"
                        . "	'" . addslashes($val['company']) . "','" . addslashes($val['type_of_company']) . "','" . addslashes($val['reset_password_token']) . "','" . addslashes($val['reset_password_sent_at']) . "','" . addslashes($val['remember_created_at']) . "',"
                        . "	'" . addslashes($val['sign_in_count']) . "','" . addslashes($val['current_sign_in_at']) . "','" . addslashes($val['last_sign_in_at']) . "','" . addslashes($val['current_sign_in_ip']) . "','" . addslashes($val['last_sign_in_ip']) . "','" . addslashes($val['created_at']) . "',"
                        . "	'" . addslashes($val['updated_at']) . "','','" . $val['approved'] . "','" . $val['password_salt'] . "','" . addslashes($val['remember_token']) . "',NOW()),";
                if ($key % 500 == 0 && $key > 0) {
                    $sql_ins = trim($sql_ins, ',');
                    $connection->createCommand($sql_ins)->execute();
                    $sql_ins = "INSERT INTO user VALUES ";
                }
            }
            if ($key < 500) {
                $sql_ins = trim($sql_ins, ',');
                $connection->createCommand($sql_ins)->execute();
            }
        }
        echo "Total data inserted into database=" . $key;
        exit;
    }

    /**
     * @method public movieStreamData() It will fetch the list of movies from the Postgre DB and insert them into MYSQL DB
     * @author GDR<support@muvi.com>
     * @return bool ture/false
     */
    function actionMovieStreamData() {
        $key = 0;
        $connection = Yii::app()->db;
        $sql = 'SELECT last_update FROM movie_streams ORDER BY last_update DESC LIMIT 1';
        $command = $connection->createCommand($sql);
        $rowCount = $command->execute();
        if ($rowCount) {
            $rec = $command->queryAll();
            $cond = " WHERE   updated_at >'" . $rec[0]['last_update'] . "'";
        } else {
            $cond = '';
        }
        $sql_pg = "SELECT * FROM movie_streams " . $cond . " ORDER BY id ASC";

        $dbcon = Yii::app()->db2;
        $command_pg = $dbcon->createCommand($sql_pg);
        $rowCount_pg = $command_pg->execute();
        $list = $command_pg->queryAll(); //Execute a query SQL	
        if ($list) {
            $sql_ins = '';
            foreach ($list as $key => $val) {
                if (!$key) {
                    $sql_ins = "INSERT INTO movie_streams VALUES ";
                }
                //echo "<pre>";print_r($val);exit;
                //id,movie_id,studio_user_id,_full_movie_file_name,movie_content_type,full_movie_file_size,pay_per_view,distribution,youtube,is_hosted,is_streaming,created_at,updated_at,full_movie,time_limit_start,time_limit_end,last_update
                $sql_ins .= " (" . $val['id'] . ",'" . $val['movie_id'] . "','" . addslashes($val['studio_user_id']) . "','" . $val['full_movie_file_name'] . "',"
                        . "	'" . addslashes($val['full_movie_content_type']) . "','" . addslashes($val['full_movie_file_size']) . "','" . addslashes($val['pay_per_view']) . "','" . addslashes($val['distribution']) . "',"
                        . "	'" . addslashes($val['youtube']) . "','" . addslashes($val['is_hosted']) . "','" . addslashes($val['is_streaming']) . "','" . addslashes($val['created_at']) . "','" . addslashes($val['updated_at']) . "'"
                        . ",'" . addslashes($val['full_movie']) . "','" . addslashes($val['time_limit_start']) . "','" . addslashes($val['time_limit_end']) . "',NOW()),";
                if ($key % 500 == 0 && $key > 0) {
                    $sql_ins = trim($sql_ins, ',');
                    $connection->createCommand($sql_ins)->execute();
                    $sql_ins = "INSERT INTO movie_streams VALUES ";
                }
            }
            if ($key < 500) {
                $sql_ins = trim($sql_ins, ',');
                $connection->createCommand($sql_ins)->execute();
            }
        }
        echo "No of Records#===" . ++$key;
        exit;
    }

    function actionGetphpinfo() {
        phpinfo();
    }

    /**
     * @method private convertVideo() It will convert all .mp4 Video to .webm format and stores them in the same directory of s3 and Update the DB.
     * @author GDR <support@muvi.com>
     * @return boolen Sucess/ Failure
     */
    function actionConvertVideo() {
        ini_set('max_execution_time', 72000);
        $con = Yii::app()->db;
        $sql = "SELECT * FROM video_conversion WHERE is_conv=0 and is_upload=0 ORDER BY created_at ASC";
        //$sql = "SELECT * FROM video_conversion WHERE 1 ORDER BY created_at ASC";
        $data = $con->createCommand($sql)->queryAll();
        //echo "<pre>123";print_r($data);exit;
        if ($data) {
            $movie_id = $data[0]['movie_id'];
            $movie_stream_id = $data[0]['movie_stream_id'];
            $filename = $data[0]['video_name'];
            // Saving data to video Conversion table as Conversion starts
            $videod = new videoConversion();
            $upd_conv = "UPDATE video_conversion SET is_conv=1, conv_start_time='" . gmdate('Y-m-d H:i:s') . "' WHERE id=" . $data[0]['id'];
            if ($con->createCommand($upd_conv)->execute()) {
                if ($_SERVER['HTTP_HOST'] == 'localhost') {
                    $s3viewdir = 'https://muviassetsdev.s3.amazonaws.com/movieVideo/' . $movie_stream_id . "/";
                } else {
                    $s3viewdir = 'https://muviassetsdev.s3.amazonaws.com/uploads/movie_stream/full_movie/' . $movie_stream_id . "/";
                }
                //$dir = $_SERVER['DOCUMENT_ROOT']."/".SUB_FOLDER."video/".$movie_id;
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . "video/";
                if (!is_dir($dir)) {
                    mkdir($dir);
                    chmod($dir, 0775);
                }
                $currfileName = $s3viewdir . $filename;
                //echo $currfileName."<br/>";
                //$new_filename = str_replace('.mp4', '.webm', $filename);
                $new_filename = 'test_webm.webm';
                $new_convertedvideo = $dir . '/' . $new_filename;
                $command = "ffmpeg -i " . $currfileName . " -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis " . $new_convertedvideo;
                //echo $command."<br/>";
                $output = '';
                $return_val = 1;
                $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/conversion.log', "a+");
                /* try{
                  exec($command,$output,$return_val);
                  //echo "<pre>";print_r($output);echo "<br>===";var_dump($return_val);exit;
                  }catch(Exception $e){
                  fwrite($fp, "Dt: ".date('d-m-Y H:i:s')."-- Movie id:-".$movie_id." \t\n Error Message: ".$e->getMessage());
                  } */
                //if(!$return_val){
                if ($return_val) {
                    try {
                        $upd_conv1 = "UPDATE video_conversion SET is_conv=2, conv_end_time='" . gmdate('Y-m-d H:i:s') . "',is_upload=1,upload_start_time='" . gmdate('Y-m-d H:i:s') . "' WHERE id=" . $data[0]['id'];
                        $con->createCommand($upd_conv1)->execute();
                    } catch (Exception $e) {
                        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/conversion.log', "a+");
                        fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . "-- Movie id:-" . $movie_id . " \t\n Error Message: " . $e->getMessage());
                    }
                    //Uploading video from local to s3 again as webm 
                    $s3 = S3Client::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                    ));

                    //$bucket = Yii::app()->params->s3_bucketname;
                    $bucket = 'muviassetsdev';
                    if ($_SERVER['HTTP_HOST'] == 'localhost') {
                        $s3dir = 'movieVideo/' . $movie_stream_id . "/";
                    } else {
                        $s3dir = 'uploads/movie_stream/full_movie/' . $movie_stream_id . "/";
                    }
                    $keyname = $s3dir . $new_filename;
                    $ufilename = $new_convertedvideo;

                    $result = $s3->listMultipartUploads(array('Bucket' => $bucket));

                    // 2. Create a new multipart upload and get the upload ID.

                    $result = $s3->createMultipartUpload(array(
                        'Bucket' => $bucket,
                        'Key' => $keyname,
                        'StorageClass' => 'REDUCED_REDUNDANCY',
                        'ACL' => 'public-read',
                        'ContentType' => 'video/webm',
                        'Metadata' => array(
                            'param1' => 'value 1',
                            'param2' => 'value 2',
                            'param3' => 'value 3'
                        )
                    ));
                    $uploadId = $result['UploadId'];

                    // 3. Upload the file in parts.
                    try {
                        $file = fopen($ufilename, 'r');
                        $parts = array();
                        $partNumber = 1;
                        while (!feof($file)) {
                            $result = $s3->uploadPart(array(
                                'Bucket' => $bucket,
                                'Key' => $keyname,
                                'UploadId' => $uploadId,
                                'PartNumber' => $partNumber,
                                'Body' => fread($file, 5 * 1024 * 1024),
                            ));
                            $parts[] = array(
                                'PartNumber' => $partNumber++,
                                'ETag' => $result['ETag'],
                            );
                        }
                        fclose($file);
                    } catch (S3Exception $e) {
                        $result = $s3->abortMultipartUpload(array(
                            'Bucket' => $bucket,
                            'Key' => $keyname,
                            'UploadId' => $uploadId
                        ));

                        //echo "Upload of {$ufilename} failed.\n";
                        fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . "-- Movie id:-" . $movie_id . " \t\n Error Message: " . $e->getMessage());
                    }

                    // 4. Complete multipart upload.
                    $result = $s3->completeMultipartUpload(array(
                        'Bucket' => $bucket,
                        'Key' => $keyname,
                        'UploadId' => $uploadId,
                        'Parts' => $parts,
                        'ContentType' => 'video/webm',
                    ));
                    $dbcon = Yii::app()->db2;
                    $sql_ext = "UPDATE movie_streams SET full_movie_webm='" . pg_escape_string($new_filename) . "' WHERE id=" . $movie_stream_id;
                    fclose($fp);
                    if ($dbcon->createCommand($sql_ext)->execute()) {
                        //unlink($new_convertedvideo);
                        //rmdir($dir);
                        if ($videod->deleteByPk($data[0]['id'])) {
                            echo "Conversion data deleted successfully";
                            exit;
                        }
                    } else {
                        echo "Error";
                        exit;
                    }
                }
            }
        }
    }

    /**
     * @method private convertVideo() It will convert all .mov Video to .mp4 format and stores them in the same directory of s3 and Update the DB.
     * @author GLM <support@muvi.com>
     * @return boolen Sucess/ Failure
     */
    function actionmov_to_mp4() {
        ini_set('max_execution_time', 72000);
        $data = movieStreamTemp::model()->findAll(array("condition" => "is_inprogress != 1"));
        //echo "<pre>";print_r($data);exit;
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/conversion.log', "a+");
        $return_val = false;
        if ($data) {
            fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . " \n\t" . print_r($data, TRUE));
            for ($i = 0; $i < count($data); $i++) {
                $movie_stream_id = $data[$i]->movie_stream_id;
                $filename = $data[$i]->file_name;
                // Saving data to video Conversion table as Conversion starts
                //$videod = new videoConversion();
                $upd_conv = "UPDATE movie_streams_temp SET is_inprogress =1  WHERE id=" . $data[$i]->id;
                yii::app()->db->createCommand($upd_conv)->execute();

                $bucketInfo = Yii::app()->common->getBucketInfo('', $data[$i]->studio_id);
                $dest_bucket = $bucket = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $s3viewdir = 'http://' . $dest_bucket . '.' . $s3url . '/uploads/movie_stream/full_movie/' . $movie_stream_id . "/";

                $dir = $_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . "video_temp/";
                if (!is_dir($dir)) {
                    mkdir($dir);
                    chmod($dir, 0775);
                }
                $currfileName = $s3viewdir . $filename;
                $newVideoExtensions = array('.mov', '.mkv', '.flv', '.vob', '.m4v', '.avi', '.3gp', '.mpg');
                foreach ($newVideoExtensions as $key => $value) {
                    if (strstr(strtolower($filename), $value)) {
                        $new_filename = str_replace($value, '.mp4', strtolower($filename));
                    }
                }
                $new_convertedvideo = $dir . '' . $new_filename;
                fwrite($fp, "\n\t New File name:- " . $new_convertedvideo . " \t Original file name " . $currfileName);
                if (HOST_IP == '127.0.0.1') {
                    $command = 'ffmpeg -async 1 -i ' . $currfileName . ' -movflags faststart -threads 0 -preset ultrafast ' . $new_convertedvideo . ' ';
                    //$command = "ffmpeg -i ".$currfileName." -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis ".$new_convertedvideo;
                } else {
                    $command = 'sudo /home/exelanz/bin/ffmpeg -async 1 -i ' . $currfileName . ' -movflags faststart -threads 0 -preset ultrafast ' . $new_convertedvideo . ' ';
                    //$command = "sudo /home/exelanz/bin/ffmpeg -i ".$currfileName." -vcodec copy -acodec copy ".$new_convertedvideo;
                }

                $output = '';
                //$return_val=1;
                $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/conversion.log', "a+");
                //echo system($command,$return_val);

                echo exec($command, $output, $return_val);
                fwrite($fp, "\n\t Command:- " . $command . " \t " . $currfileName);

                if (is_file($new_convertedvideo)) {
                    fwrite($fp, "\n\t Upload is in progress \n\n\t");
                    //Uploading video from local to s3 again as mp4 
                    $s3 = S3Client::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                    ));
                    //$bucket = Yii::app()->params->video_bucketname;

                    $s3dir = 'uploads/movie_stream/full_movie/' . $movie_stream_id . "/";

                    $keyname = $s3dir . $new_filename;
                    $ufilename = $new_convertedvideo;

                    $result = $s3->listMultipartUploads(array('Bucket' => $bucket));

                    // 2. Create a new multipart upload and get the upload ID.

                    $result = $s3->createMultipartUpload(array(
                        'Bucket' => $bucket,
                        'Key' => $keyname,
                        'StorageClass' => 'REDUCED_REDUNDANCY',
                        'ACL' => 'public-read',
                        'ContentType' => 'video/mp4',
                        'Metadata' => array(
                            'param1' => 'value 1',
                            'param2' => 'value 2',
                            'param3' => 'value 3'
                        )
                    ));
                    $uploadId = $result['UploadId'];
                    // 3. Upload the file in parts.
                    try {
                        $file = fopen($ufilename, 'r');
                        $parts = array();
                        $partNumber = 1;
                        while (!feof($file)) {
                            $result = $s3->uploadPart(array(
                                'Bucket' => $bucket,
                                'Key' => $keyname,
                                'UploadId' => $uploadId,
                                'PartNumber' => $partNumber,
                                'Body' => fread($file, 5 * 1024 * 1024),
                            ));
                            $parts[] = array(
                                'PartNumber' => $partNumber++,
                                'ETag' => $result['ETag'],
                            );
                        }
                        fclose($file);
                    } catch (S3Exception $e) {
                        $result = $s3->abortMultipartUpload(array(
                            'Bucket' => $bucket,
                            'Key' => $keyname,
                            'UploadId' => $uploadId
                        ));

                        //echo "Upload of {$ufilename} failed.\n";
                        fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . "-- Movie id:-" . $movie_stream_id . " \t\n Error Message: " . $e->getMessage());
                    }

                    // 4. Complete multipart upload.
                    $result = $s3->completeMultipartUpload(array(
                        'Bucket' => $bucket,
                        'Key' => $keyname,
                        'UploadId' => $uploadId,
                        'Parts' => $parts,
                        'ContentType' => 'video/mp4',
                    ));
                    $movie_stream = movieStreams::model()->find(array("condition" => "id=" . $movie_stream_id));
                    if ($new_filename != '') {
                        $uniqid = Yii::app()->common->generateUniqNumber();
                        $movie_stream->full_movie = $new_filename;
                        $movie_stream->embed_id = $uniqid;
                        $res = $movie_stream->save(false);
                    }
                    fclose($fp);
                    if ($res) {
                        echo "remove part";
                        $rem_cmd = "sudo rm " . $new_convertedvideo;
                        exec($rem_cmd, $output, $return_val);
                        movieStreamTemp::model()->deleteByPk($data[$i]->id);
                        //unlink($new_convertedvideo);
                        //$sql_rem = "DELETE FROM movie_streams_temp where id = ".$data[$i]->id;
                        //$dbcon->createCommand($sql_rem)->execute();
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $s3dir . $filename
                        ));
                        echo 'success';
                        exit;
                        //rmdir($dir);
                        //if($videod->deleteByPk($data[0]['id'])){
                        //	echo "Conversion data deleted successfully";exit;
                        //}
                    } else {
                        echo "Error";
                        exit;
                    }
                }
            }
        }
    }

    function actionUpdateTrailerNotConverted() {
        $this->layout = false;
        $type = @$_REQUEST['type'];
        $v = explode('MUVIMUVI', $_REQUEST['stream_id']);
        print_r($v);
        if($type=='physical'){
            $model = 'PGMovieTrailer';
            $table = 'pg_movie_trailer';
            $dir = 'physical';
            
            $videoDetails = Yii::app()->db->createCommand()
                ->select('s.s3bucket_id as bucketId,s.id as studioId,s.new_cdn_users as newUser,mt.trailer_file_name as fileName')
                ->from('pg_movie_trailer mt,studios s,pg_product f ')
                ->where('f.id = mt.movie_id AND f.studio_id = s.id AND mt.id=' . $v[0] . ' AND f.uniqid="' . $v[1] . '"')
                ->queryAll();
        }else {
            $model = 'movieTrailer';
            $table = 'movie_trailer';
            $dir = 'trailers';
            
            $videoDetails = Yii::app()->db->createCommand()
                ->select('s.s3bucket_id as bucketId,s.id as studioId,s.new_cdn_users as newUser,mt.trailer_file_name as fileName')
                ->from('movie_trailer mt,studios s,films f ')
                ->where('f.id = mt.movie_id AND f.studio_id = s.id AND mt.id=' . $v[0] . ' AND f.uniq_id="' . $v[1] . '"')
                ->queryAll();
        }
        if ($videoDetails) {
            $bucketInfo = Yii::app()->common->getBucketInfo("", $videoDetails[0]['studioId']);
            $bucket = $bucketInfo['bucket_name'];
            $s3 = Yii::app()->common->connectToAwsS3($videoDetails[0]['studioId']);
            $folderPath = Yii::app()->common->getFolderPath("", $videoDetails[0]['studioId']);
            $signedFolderPath = $folderPath['signedFolderPath'];
            $s3dir = $signedFolderPath . 'uploads/'.$dir.'/' . $v[0] . '/' . $videoDetails[0]['fileName'];
            $videoToBeDeleted = "s3://" . $bucket . "/" . $s3dir;
            $data['url'] = 'http://' . $bucket . '.' . $bucketInfo['s3url'] . '/' . $s3dir;
            $trailerData = $model::model()->findByPk($v[0]);
            $trailerData->is_converted = 2;
            $trailerData->trailer_file_name = '';
            $trailerData->video_remote_url = '';
            $trailerData->video_management_id = 0;
            $trailerData->encode_fail_time = date('Y-m-d H:i:s');
            $trailerData->save();
            if ($v[3] == 0) {
                $this->createUploadVideoSHFOrVideoGallery($data, "", $videoDetails[0]['fileName'], $videoDetails[0]['studioId'], $videoToBeDeleted);
            } else {
                $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir
                ));
            }
        }
    }

    function actionMailRegardingTrailerEncodeing() {
        $this->layout = false;
        $studioIds = Yii::app()->db->createCommand()
                ->select('distinct(f.studio_id) as studioID')
                ->from('movie_trailer mt,films f ')
                ->where('f.id = mt.movie_id AND (mt.is_converted=2 or mt.is_converted=1) and mt.mail_sent_for_trailer =0')
                ->queryAll();
        if ($studioIds) {
            print_r($studioIds);
            foreach ($studioIds as $studioIdKey => $studioId) {
                $videoDetails = Yii::app()->db->createCommand()
                        ->select('mt.id as stream_id,mt.is_converted as isConverted,f.name as film_name')
                        ->from('movie_trailer mt,films f')
                        ->where(' f.id = mt.movie_id AND (mt.is_converted=2 or mt.is_converted=1) and mt.mail_sent_for_trailer =0 AND f.studio_id=' . $studioId['studioID'])
                        ->queryAll();
                if ($videoDetails) {
                    $userDetails = Yii::app()->db->createCommand()
                            ->select('u.email as user_email,u.first_name as user_first_name')
                            ->from('user u')
                            ->where('u.studio_id=' . $studioId['studioID'])
                            ->queryAll();
                    if ($userDetails) {
                        $mailcontent = '<p>Dear ' . $userDetails[0]['user_first_name'] . ',</p>';
                        $host_ip = Yii::app()->params['host_ip'];

                        if (in_array(HOST_IP, $host_ip)) {
                            $adminGroupEmail = array($userDetails[0]['user_email']);
                        } else {
                            $adminGroupEmail = array('rasmi@muvi.com', 'srutikant@muvi.com');

                            $mailcontent.= '<p><strong>Note: This is the staging server.</strong></p>';
                        }
                        $fileEncoded = array();
                        $fileNotEncoded = array();
                        foreach ($videoDetails as $videoDetailsKey => $videoDetailsVal) {
                            $fileName = $videoDetailsVal['film_name'];
                            if ($videoDetailsVal['isConverted'] == 1) {
                                $fileEncoded[] = $fileName;
                            } else if ($videoDetailsVal['isConverted'] == 2) {
                                $fileNotEncoded[] = $fileName;
                            }
                            $trailerData = movieTrailer::model()->findByPk($videoDetailsVal['stream_id']);
                            $trailerData->mail_sent_for_trailer = 1;
                            $trailerData->save();
                        }
                        if (!empty($fileEncoded)) {
                            $mailcontent .= '<p>Following preview video(s) are successfully encoded and now available on your website and mobile apps.</p>';
                            $mailcontent .= '<ul>';
                            foreach ($fileEncoded as $fileEncodedKey => $fileEncodedVal) {
                                $mailcontent .= '<li>' . $fileEncodedVal . '</li>';
                            }
                            $mailcontent .= '</ul>';
                        }
                        if (!empty($fileNotEncoded)) {
                            $mailcontent .= '<p>Following preview video(s) are failed encoding. There maybe something wrong with the video file. Please check and contact your Muvi representative in case of any questions.</p>';
                            $mailcontent .= '<ul>';
                            foreach ($fileNotEncoded as $fileNotEncodedKey => $fileNotEncodedVal) {
                                $mailcontent .= '<li>' . $fileNotEncodedVal . '</li>';
                            }
                            $mailcontent .= '</ul>';
                        }
                        $mailcontent .= "<p>Regards,<br/>Team Muvi</p>";
                        $site_url = Yii::app()->getBaseUrl(true);
                        $logo = EMAIL_LOGO;
                        $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
                        $params = array(
                            'website_name' => 'Muvi',
                            'mailcontent' => $mailcontent,
                            'logo' => $logo
                        );

                        $fromMail = 'studio@muvi.com';

                        $adminSubject = "Preview upload status";
                        $mailAddress = array(
                            'subject' => $adminSubject,
                            'from_email' => $fromMail['email'],
                            'from_name' => $fromMail['name'],
                            'to' => $adminGroupEmail
                        );
                        $cc = array();
                        $bcc = array('rasmi@muvi.com', 'mohan@muvi.com', 'suraja@muvi.com', 'srutikant@muvi.com');

                        $template_name = 'video_conversion';
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('video_upload_status', $studioId['studioID']);
                        if ($isEmailToStudio) {
                            $obj = new Controller();
                            Yii::app()->theme = 'bootstrap';
                            $thtml = Yii::app()->controller->renderPartial('//email/video_conversion', array('params' => $params), true);
                            $returnVal = $obj->sendmailViaAmazonsdk($thtml, $adminSubject, $adminGroupEmail, $fromMail, $cc, $bcc, '', 'Muvi');
                        }
                    }
                }
            }
        }
    }

    /**
     * @method private deleteVideo() Delete all the convertion records older then 2days
     * @return boolen Success/Failure
     * @author GDR<support@muvi.com>
     */
    function actionDeleteVideo() {
        $con = Yii::app()->db;
        $sql = "SELECT * FROM video_conversion WHERE DATE_SUB(CURDATE(),INTERVAL 2 DAY) > created_at ORDER BY created_at ASC";
        $data = $con->createCommand($sql)->queryAll();
        foreach ($array as $key => $value) {
            $dir = $_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . "video/" . $value['movie_id'];
            if (is_dir($dir)) {
                $new_filename = str_replace('.mp4', '.webm', $value['video_name']);
                @unlink($dir . "/" . $new_filename);
                @rmdir($dir);
            }
            videoConversion::model()->deleteByPk($value['id']);
        }
    }

    /**
     * @method public getThumbnail() It will generate video thumbnail from the uploaded video and store 
     * @author GDR<support@muvi.com>
     * @return bool TRUE/FALSE
     */
    function actionGetThumbnail($studio_id = '') {
        if (isset($_REQUEST['studio_id'])) {
            $studio_id = $_REQUEST['studio_id'];
        } else if (isset(Yii::app()->user->studio_id)) {
            $studio_id = Yii::app()->user->studio_id;
        }
        if (!$studio_id) {
            echo "Oops Sorry you don't have access";
            exit;
        }
        $this->episodeThumbnail($studio_id);
    }

    /**
     * @method public generateThumbnail() It will generate video thumbnail from the uploaded video for episode and clips 
     * @author GDR<support@muvi.com>
     * @return bool TRUE/FALSE
     */
    function actionGenerateThumbnail() {
        $data = Studio::model()->findAll(array('condition' => 'status=1 AND id>=53', "select" => "id,name,status", "order" => "id DESC"));
        foreach ($data AS $key => $val) {
            $this->episodeThumbnail($val->id);
        }
        exit;
    }

    /**
     * @method public MuvidbStructure() Description
     * 
     */
    function actionMuvidbStructure() {
        $arr = array('films_dumps', 'films_bkp2004', 'films_bkp0418', 'films_backup', 'films_0427', 'movie_casts_bkp_0422', '');
        $dbcon = Yii::app()->db2;
        $tbls = "SELECT table_name  FROM information_schema.tables WHERE table_schema='public'   AND table_type='BASE TABLE'";
        $tables = $dbcon->createCommand($tbls)->queryAll();
        //echo "<pre>";print_r($tables);exit;
        $tbl = "<table style=width:100%><tr><th>WIKI DATABASE</th></tr>";
        foreach ($tables AS $k => $v) {
            if (!in_array($v['table_name'], $arr)) {
                $tbl .= "<tr><td><table style='width:100%;border:1px solid #000;'><tr><th colspan=3>" . $v['table_name'] . "</th></tr>";
                $sql = "select column_name, data_type, character_maximum_length from INFORMATION_SCHEMA.COLUMNS where table_name = '" . $v['table_name'] . "'";
                $data = $dbcon->createCommand($sql)->queryAll();
                if ($data) {
                    $tbl .="<th style='width:40%;'>Column Name</th><th style='width:40%;'>Data Types</th><th style='width:20%;'>Max Length</th>";
                    foreach ($data AS $key => $val) {
                        $tbl .= "<tr><td style='width:40%;'>" . $val['column_name'] . "</td><td style='width:40%;'>" . $val['data_type'] . "</td><td style='width:40%;'>" . $val['character_maximum_length'] . "</td></tr>";
                    }
                }
                $tbl .= "</table></td></tr>";
            }
        }
        $tbl .="</table>";
        echo $tbl . "<br/><hr/><br/>";
        $dbcon = Yii::app()->db1;
        $tbls = "SELECT table_name  FROM information_schema.tables WHERE table_schema='public'   AND table_type='BASE TABLE'";
        $tables = $dbcon->createCommand($tbls)->queryAll();
        //echo "<pre>";print_r($tables);exit;
        $tbl = "<table style=width:100%><tr><th>WIKI DATABASE</th></tr>";
        foreach ($tables AS $k => $v) {
            $tbl .= "<tr><td><table style='width:100%;border:1px solid #000;'><tr><th colspan=3>" . $v['table_name'] . "</th></tr>";
            $sql = "select column_name, data_type, character_maximum_length from INFORMATION_SCHEMA.COLUMNS where table_name = '" . $v['table_name'] . "'";
            $data = $dbcon->createCommand($sql)->queryAll();
            if ($data) {
                $tbl .="<th style='width:40%;'>Column Name</th><th style='width:40%;'>Data Types</th><th style='width:20%;'>Max Length</th>";
                foreach ($data AS $key => $val) {
                    $tbl .= "<tr><td style='width:40%;'>" . $val['column_name'] . "</td><td style='width:40%;'>" . $val['data_type'] . "</td><td style='width:40%;'>" . $val['character_maximum_length'] . "</td></tr>";
                }
            }
            $tbl .= "</table></td></tr>";
        }
        $tbl .="</table>";
        echo $tbl;
        exit;
    }

    public function actiontransfer_streams() {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        error_reporting(1);
        $source_s3 = S3Client::factory(array(
                    'key' => "AKIAJ3ZLG727M5FK33HQ",
                    'secret' => "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",
        ));
        $source_url = "https://muviassetsdev.s3.amazonaws.com/";
        $dest_s3 = S3Client::factory(array(
                    'key' => "AKIAJ4HTSCRXGHFTKRBQ",
                    'secret' => "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",
        ));
        $dest_bucket = "vimeoassets";
        //$res = movieStreams::model()->findAll(array("condition" => "wiki_data IS NOT NULL"));
        //print_r($res);
        $saveFile = "/video_temp/A Woman Hunted - HD.mp4";
        /* $result = $source_s3->getObject(array(
          'Bucket' => "muviassetsdev",
          'Key'    => '/uploads/movie_stream/full_movie/8652/A Woman Hunted - HD.mp4',
          'command.response_body' => EntityBody::factory(fopen($saveFile, 'wb+'))
          )); */

        echo "starting upload"; //exit;
        //$path = "http://muviassetsdev.s3.amazonaws.com/uploads/movie_stream/full_movie/8652/A%20Woman%20Hunted%20-%20HD.mp4";
        echo $dest_s3->putObject(array(
            'Bucket' => $dest_bucket,
            'Key' => '/uploads/movie_stream/full_movie/1/A Woman Hunted - HD.mp4',
            'SourceFile' => '/video_temp/test.mp4',
            'ContentType' => 'video/mp4'
                //file_get_contents('')
        ));
        unlink('video_temp/test.mp4');
        //rmdir("video_temp");
        echo "finished";
    }

    /* function actionDropOffMail() {
      $sql = "SELECT s.*, u.id AS user_id, u.*, en.studio_id AS email_studio_id FROM studios s LEFT JOIN user u
      ON (s.id = u.studio_id) LEFT JOIN email_notification en ON (s.id = en.studio_id AND en.type = 1)
      WHERE u.is_admin=0 AND u.is_sdk=1 AND u.role_id=1 AND s.status=2 ORDER BY u.created_at DESC";

      $dbcon = Yii::app()->db;
      $data = $dbcon->createCommand($sql)->queryAll();

      if (isset($data) && !empty($data)) {
      foreach ($data as $key => $value) {
      //print 'out<br/>';
      if (trim($value['email_studio_id']) == '') {
      //print 'in<br/>';
      $last_dt = new DateTime($value['created_at']);
      $today = new DateTime(gmdate('Y-m-d H:i:s'));
      $interval = $today->diff($last_dt);
      $hours = $interval->h;
      $hours = $hours + ($interval->days*24);

      if ($hours >= 1 && $hours < 3) {
      //print 'Hour'.$hours.'<br/>';
      //Keeping log in email tables after firing an email
      $enModel = New EmailNotification;
      $enModel->studio_id = $value['studio_id'];
      $enModel->user_id = $value['user_id'];
      $enModel->type = 1;
      $enModel->created = new CDbExpression("NOW()");
      $enModel->save();

      //Sending an email to user
      $req = array();
      $req['name'] = $value['first_name'];
      $req['email'] = $value['email'];
      $req['domain'] = 'http://'.$value['domain'];
      //print '<pre>';print_r($req);
      $this->dropOffMailToUser($req);
      }
      }
      //print '<hr/>';
      }
      }
      }

      function dropOffMailToUser($req = array()) {
      //Finding demo user's password
      //$user = User::model()->findAllByAttributes(array('email'=> 'demo@muvi.com'));

      //Sending Email to Sales Team
      $site_url = Yii::app()->getBaseUrl(true);
      $logo = $site_url . '/themes/signup/images/muvi_studio_logo.png';
      $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
      $store_details = Yii::app()->common->storeDetails('studio');
      $fb_link = '<a href="' . @$store_details['fb_link'] . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
      $twitter_link = '<a href="' . @$store_details['twitter_link'] . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
      $gplus_link = '<a href="' . @$store_details['gplus_link'] . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';

      $params = array(
      array('name' => 'website_name', 'content' => 'Muvi'),
      array('name' => 'logo', 'content' => $logo),
      array('name' => 'fname', 'content' => ucfirst($req['name'])),
      array('name' => 'email', 'content' => $req['email']),
      array('name' => 'domain', 'content' => $req['domain']),
      //array('name' => 'demopassword', 'content' => $user[0]->reference),
      array('name' => 'fb_link', 'content' => @$fb_link),
      array('name' => 'twitter_link', 'content' => @$twitter_link),
      array('name' => 'gplus_link', 'content' => @$gplus_link),
      );

      $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
      $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
      $adminSubject = "Getting started with Muvi";
      $mailAddress = array(
      'subject' => $adminSubject,
      'from_email' => $autosignup_from['email'],
      'from_name' => $autosignup_from['name'],
      'to' => $toEmail
      );
      $template_name = 'dropoff_touser';
      $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
      return $returnVal;
      } */

    function actionFreeTrialExpiring() {
        $sql = "SELECT s.*, u.id AS user_id, u.*, en.studio_id AS email_studio_id FROM studios s LEFT JOIN user u
        ON (s.id = u.studio_id) LEFT JOIN email_notification en ON (s.id = en.studio_id AND en.type = 1)
        WHERE u.is_admin=0 AND u.is_sdk=1 AND u.role_id=1 AND s.status=1 AND s.is_subscribed=0 
        AND s.is_default=0 AND DATE(s.start_date)> '" . date('Y-m-d H:i:s') . "'";

        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                if (trim($value['email_studio_id']) == '') {
                    $date = gmdate("Y-m-d", strtotime($value['start_date']));
                    $last_dt = new DateTime($date);
                    $today = new DateTime(gmdate('Y-m-d'));
                    $interval = $today->diff($last_dt);
                    $days = $interval->days;
                    if ($days == 3) {//Lets free trial expire 30th june 2015, then start date is 1st july 2015. As we sent email before 2 days of expiry of free trial, it should be deducted 3 days from start date.
                        //Keeping log in email tables after firing an email
                        $enModel = New EmailNotification;
                        $enModel->studio_id = $value['studio_id'];
                        $enModel->user_id = $value['user_id'];
                        $enModel->type = 1;
                        $enModel->created = new CDbExpression("NOW()");
                        $enModel->save();

                        //Sending an email to user
                        $req = array();
                        $req['name'] = $value['first_name'];
                        $req['email'] = $value['email'];
                        $req['domain'] = $value['domain'];
                        Yii::app()->email->freeTrialExpiringEmailToUser($req);
                    }
                }
            }
        }
    }

    function actionFreeTrialExpired() {
        $sql = "SELECT s.*, u.id AS user_id, u.*, en.studio_id AS email_studio_id FROM studios s LEFT JOIN user u
        ON (s.id = u.studio_id) LEFT JOIN email_notification en ON (s.id = en.studio_id AND en.type = 2)
        WHERE u.is_admin=0 AND u.is_sdk=1 AND u.role_id=1 AND s.status=4 AND s.is_subscribed=0 
        AND s.is_default=0 AND DATE(s.start_date) < '" . date('Y-m-d H:i:s') . "'";

        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();

        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {        
                
                if (trim($value['email_studio_id']) == '') {
                    $date = gmdate("Y-m-d", strtotime($value['start_date'] . '+1 days'));
                    
                    $last_dt = new DateTime($date);
                    $today = new DateTime(gmdate('Y-m-d'));
                    $interval = $today->diff($last_dt);
                    $days = $interval->days;
                    if ($days == 0) {//Lets free trial has expired 30th june 2015, then start date is 1st july 2015. As we sent email after 1 days of expiry of free trial, it should be added 1 day from start date (2nd july 2015).
                        //Keeping log in email tables after firing an email
                        $enModel = New EmailNotification;
                        $enModel->studio_id = $value['studio_id'];
                        $enModel->user_id = $value['user_id'];
                        $enModel->type = 2;
                        $enModel->created = new CDbExpression("NOW()");
                        $enModel->save();

                        //Sending an email to user
                        $expired_date = gmdate("d F, Y", strtotime($value['start_date'] . '-1 days'));
                            
                        $req = array();
                        $req['name'] = $value['first_name'];
                        $req['email'] = $value['email'];
                        $req['phone'] = $value['phone'];
                        $req['companyname'] = $value['name'];
                        $req['domain'] = $value['domain'];
                        $req['expired_date'] = $expired_date;
                        $req['cancel_reason'] = 'Free Trial Expired';
                        $req['custom_notes'] = '';
                        Yii::app()->email->freeTrialExpiredEmailToUser($req);
                        $req['from_email'] ='noreply@muvi.com';
                        Yii::app()->email->freeTrialExpiredEmailToSales($req);
                    }
                }
            }
        }
    }

    function actionBillingCycle() {
        $sql = "SELECT s.*, u.id AS user_id, u.*, en.studio_id AS email_studio_id FROM studios s LEFT JOIN user u
        ON (s.id = u.studio_id) LEFT JOIN email_notification en ON (s.id = en.studio_id AND en.type = 3) 
        WHERE u.is_admin=0 AND u.is_sdk=1 AND u.role_id=1 AND s.status=1 AND s.is_subscribed=1 
        AND s.is_default=0 AND DATE(s.start_date)= CURDATE() ORDER BY u.created_at DESC";

        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                if (trim($value['email_studio_id']) == '') {

                    //Keeping log in email tables after firing an email
                    $enModel = New EmailNotification;
                    $enModel->studio_id = $value['studio_id'];
                    $enModel->user_id = $value['user_id'];
                    $enModel->type = 3;
                    $enModel->created = new CDbExpression("NOW()");
                    $enModel->save();

                    //Sending an email to user
                    $req = array();
                    $req['name'] = $value['first_name'];
                    $req['email'] = $value['email'];
                    $req['companyname'] = $value['name'];
                    Yii::app()->email->billingCycleMailToSale($req);
                }
            }
        }
    }

    //Resetting the Demo user password in each month by Ratikanta
    public function actionResetdemo() {
        $this->layout = false;
        $ip_address = Yii::app()->request->getUserHostAddress();
        if ($ip_address == '52.0.232.150' || $ip_address == '52.211.41.107') {
            $user_id = 54;
            $new_password = self::getRandomPassword();

            $umodel = new User();
            $encrypted_pass = $umodel->encrypt($new_password);
            $user = User :: model()->findByPk($user_id);
            $user->encrypted_password = $encrypted_pass;
            $user->reference = $new_password;
            $user->save();

            $mailcontent = '<p>Password changed for Demo studio Login! New password is <strong>' . $new_password . '</strong></p>';
            $mailcontent.= '<p><a href="' . Yii::app()->getBaseUrl(true) . '">Click here to login.</a></p>';
            $host_ip = Yii::app()->params['host_ip'];

            if (in_array(HOST_IP, $host_ip)) {
                $adminGroupEmail = array('development@muvi.com', 'viraj@muvi.com', 'anshuman@muvi.com', 'soumya@muvi.com', 'ankit@muvi.com');
            } else {
                $adminGroupEmail = array('rasmi@muvi.com', 'ratikanta@muvi.com');

                $mailcontent.= '<p><strong>Note: This is the staging server.</strong></p>';
            }

            /*
              $adminGroupEmail = array(
              array('email' => 'ratikanta@muvi.com', 'name' => 'Rati Kanta', 'type' => 'to'),
              );
             * 
             */
            //$mailcontent.= '<p><strong>Note: This is the staging server.</strong></p>';        

            $site_url = Yii::app()->getBaseUrl(true);
            $logo = EMAIL_LOGO;
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
            $params = array(
                'website_name' => 'Muvi',
                'mailcontent' => $mailcontent,
                'logo' => $logo
            );

            $billing_from = array('email' => 'info@muvi.com', 'name' => 'Muvi');

            $adminSubject = "Password changed for Demo studio!";
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $billing_from['email'],
                'from_name' => $billing_from['name'],
                'to' => $adminGroupEmail
            );
            $template_name = 'demo_password_reset';
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/demo_password_reset', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $adminSubject, $adminGroupEmail, $billing_from['email'], '', '', '', 'Muvi');
            echo "Reset";
            exit;
        }
    }

    //Resetting the super admin password in each 2weeks by Ratikanta
    public function actionResetsuperadmin() {
        $this->layout = false;
        $ip_address = Yii::app()->request->getUserHostAddress();
        if ($ip_address == '52.0.232.150' || $ip_address == '52.211.41.107') {
            $new_password = self::getRandomPassword();
            $umodel = new User();
            $encrypted_pass = $umodel->encrypt($new_password);

            $user = User::model()->findByAttributes(array('email' => 'admin@muvi.com', 'is_admin' => 1));
            $user->encrypted_password = $encrypted_pass;
            $user->reference = $new_password;
            $user->save();

            $mailcontent = '<p>Password changed for Super Admin! New password is <strong>' . $new_password . '</strong>.</p>';
            $mailcontent.= '<p><strong>PLEASE BE CAREFUL WHEN USING SUPER ADMIN.</strong></p>';


            $host_ip = Yii::app()->params['host_ip'];

            if (in_array(HOST_IP, $host_ip)) {
                $adminGroupEmail = array('all@muvi.com');
            } else {
                $adminGroupEmail = array('rasmi@muvi.com', 'ratikanta@muvi.com');

                $mailcontent.= '<p><strong>Note: This is the staging server.</strong></p>';
            }

            $site_url = Yii::app()->getBaseUrl(true);
            $logo = EMAIL_LOGO;
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
            $params = array(
                'website_name' => 'Muvi',
                'mailcontent' => $mailcontent,
                'logo' => $logo
            );

            $billing_from = 'info@muvi.com';

            $adminSubject = "Password changed for Super Admin!";

            $template_name = 'admin_password_changed';

            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/admin_password_changed', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $adminSubject, $adminGroupEmail, $billing_from, '', '', '', 'Muvi');
            echo "Reset pwd";
            exit;
        }
    }

    function getRandomPassword() {
        $acceptablePasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@#0123456789";
        $randomPassword = "";

        for ($i = 0; $i < 8; $i++) {
            $randomPassword .= substr($acceptablePasswordChars, rand(0, strlen($acceptablePasswordChars) - 1), 1);
        }
        return $randomPassword;
    }

    /**
     * @method public videoConversionSH() Reads the video file and creates a .sh file and put that into the highend server.
     * @author GDR<support@muvi.com>
     * @return bool true/false
     */
    function actionVideoConversionSH() {
        $this->layout = false;
        $condition = '';
        $videoFolder = 'video';
        $shFolder = 'progress';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
            if (isset($_REQUEST['movie_stream_id'])) {
                $condition = ' AND ms.id=' . $_REQUEST['movie_stream_id'];
            } else {
                echo "Pls provide the movie_stream_id";
                exit;
            }
        } else if (HOST_IP == '52.0.64.95') {
            if (isset($_REQUEST['movie_stream_id'])) {
                $condition = ' AND ms.id=' . $_REQUEST['movie_stream_id'];
            } else {
                echo "Pls provide the movie_stream_id";
                exit;
            }
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
        }
        $listOfStream = "";
        $studioIds = "";
        $drmEnable = array();
        $drmMobileDisable = array();
        $alertStatus = array();
        $encodingServer = "select id, threads, ip, s3_video_folder from encoding_server_details where code_server_ip='" . HOST_IP . "'";
        $encodingServerDetails = Yii::app()->db->createCommand($encodingServer)->queryAll();
        $movieSrtQry = "select ms.id as stream_id,ms.studio_id,s.s3bucket_id , s.studio_s3bucket_id,ms.upload_start_time from movie_streams ms, studios s where ms.studio_id= s.id AND ms.has_sh=0 AND ms.is_converted =0 AND ms.full_movie !='' and ms.id not in (select movie_stream_id from encoding)" . $condition;
        $movieSrtData = Yii::app()->db->createCommand($movieSrtQry)->queryAll();
        if (isset($movieSrtData) && !empty($movieSrtData) && count($movieSrtData) > 0) {
            foreach ($movieSrtData as $movieSrtDatakey => $movieSrtDatavalue) {
                if ($encodingServerDetails) {
                    $studioIds[] = $movieSrtDatavalue['studio_id'];
                    $listOfStream[] = $movieSrtDatavalue['stream_id'];
                } else {
                    if ($movieSrtDatavalue['s3bucket_id'] != 6) {
                        $alertStatus[] = array(
                            'studio_id' => $movieSrtDatavalue['studio_id'],
                            'movie_stream_id' => $movieSrtDatavalue['stream_id'],
                            'upload_time' => $movieSrtDatavalue['upload_start_time']
                        );
                    } else {
                        $studioIds[] = $movieSrtDatavalue['studio_id'];
                        $listOfStream[] = $movieSrtDatavalue['stream_id'];
                    }
                }
            }
        }
        if (!empty($alertStatus)) {
            $connection = Yii::app()->db->getSchema()->getCommandBuilder();
            $command = $connection->createMultipleInsertCommand('encoding', $alertStatus);
            $command->execute();
        }

        $totalLimit = 0;
        $fileInQue = 0;
        $limit = 0;
        $severLimitLeft = array();
        $serverIds = '';
        $serverDataArray = array();
        $singaporeServerIp = 0;
        $isEntreprise = 0;
        if ($encodingServerDetails) {
            $serverIds = $encodingServerDetails[0]['id'];
            $serverDataArray[$encodingServerDetails[0]['id']]['threads'] = $encodingServerDetails[0]['threads'];
            $serverDataArray[$encodingServerDetails[0]['id']]['s3folder_name'] = $encodingServerDetails[0]['s3_video_folder'];
            $isEntreprise = 1;
        } else {
            $encodingServer = "select es.id,count(e.encoding_server_id) as noOfFile, es.active, es.threads, es.max_load, es.max_load-count(e.encoding_server_id) AS pending, es.ip,es.s3_video_folder from encoding_server_details es LEFT JOIN encoding e on es.id = e.encoding_server_id group by es.id";
            $encodingServerDetails = Yii::app()->db->createCommand($encodingServer)->queryAll();
            if ($encodingServerDetails) {
                foreach ($encodingServerDetails as $encodingServerDetailskey => $encodingServerDetailsvalue) {
                    if ($encodingServerDetailsvalue['active'] == 1) {
                        $totalLimit = $totalLimit + $encodingServerDetailsvalue['max_load'];
                        $fileInQue = $fileInQue + $encodingServerDetailsvalue['noOfFile'];
                        if ($encodingServerDetailsvalue['pending'] > 0) {
                            $severLimitLeft[$encodingServerDetailskey] = $encodingServerDetailsvalue['pending'];
                        }
                        $serverIds .= $encodingServerDetailsvalue['id'] . ",";
                    }
                    $serverDataArray[$encodingServerDetailsvalue['id']]['threads'] = $encodingServerDetailsvalue['threads'];
                    $serverDataArray[$encodingServerDetailsvalue['id']]['s3folder_name'] = $encodingServerDetailsvalue['s3_video_folder'];
                    if ($encodingServerDetailsvalue['ip'] == '52.220.157.212') {
                        $singaporeServerIp = $encodingServerDetailsvalue['id'];
                    }
                }
                if ($serverIds != '') {
                    $serverIds = substr($serverIds, 0, -1);
                }
                $limit = $totalLimit - $fileInQue;
            }
        }
        if ($limit > 0) {
            $encodingDataQuery = "SELECT min(e.id) AS id, e.movie_stream_id as stream_id, e.studio_id FROM encoding e,
                    (SELECT DISTINCT studio_id FROM encoding WHERE encoding_server_id=0 AND studio_id NOT IN 
                    (SELECT studio_id FROM encoding WHERE encoding_server_id IN (" . $serverIds . ") GROUP BY studio_id HAVING COUNT(*)>1)
                    ORDER BY encoding_start_time LIMIT " . $limit . ") s
                    WHERE e.encoding_server_id=0 AND e.studio_id=s.studio_id
                    GROUP BY e.studio_id 
                    ORDER BY e.studio_id, e.id";
            $encodingData = Yii::app()->db->createCommand($encodingDataQuery)->queryAll();
            if (isset($encodingData) && !empty($encodingData) && count($encodingData) > 0) {
                foreach ($encodingData as $encodingDatakey => $encodingDatavalue) {
                    $studioIds[] = $encodingDatavalue['studio_id'];
                    $listOfStream[] = $encodingDatavalue['stream_id'];
                }
            }
        }
        $studioIds = implode(",", $studioIds);
        $listOfStream = implode(",", $listOfStream);
        if ($studioIds != "") {
            $drmData = Yii::app()->db->createCommand()
                    ->select('config_value,studio_id')
                    ->from('studio_config')
                    ->where('config_key = "drm_enable" AND studio_id IN (' . $studioIds . ')')
                    ->queryAll();
            foreach ($drmData as $drmDataKey => $drmDataVal) {
                $drmEnable[$drmDataVal['studio_id']] = $drmDataVal['config_value'];
            }
            $drmMobileData = Yii::app()->db->createCommand()
                    ->select('config_value,studio_id')
                    ->from('studio_config')
                    ->where('config_key = "drm_mobile_disable" AND studio_id IN (' . $studioIds . ')')
                    ->queryAll();
            foreach($drmMobileData as $drmMobileDataKey => $drmMobileDataVal) {
                $drmMobileDisable[$drmMobileDataVal['studio_id']] = $drmMobileDataVal['config_value'];
            }
        }
        if ($listOfStream != '') {
            $videoResolutionData = VideoResolution::model()->findAll();
            $videoResolution = array();
            if ($videoResolutionData) {
                foreach ($videoResolutionData as $videoResolutionDatakey => $videoResolutionDatavalue) {
                    $videoResolution[$videoResolutionDatavalue['resolution']] = explode(",", $videoResolutionDatavalue['resolution_to_be_encoded']);
                }
            }
            $streamData = Yii::app()->db->createCommand()
                    ->select('ms.id AS stream_id,ms.movie_id,ms.studio_id as studio_id,s.priority,s.new_cdn_users,s.s3bucket_id,s.studio_s3bucket_id,ms.full_movie,f.uniq_id,f.content_types_id,ms.video_management_id,ms.upload_start_time')
                    ->from('movie_streams ms,studios s,films f ')
                    ->where('ms.studio_id = s.id AND f.id = ms.movie_id AND ms.has_sh=0 AND ms.is_converted =0 AND ms.full_movie!=\'\' AND ms.id IN (' . $listOfStream . ')')
                    ->queryAll();
            if ($streamData) {
                $lastbucketid = '';
                $lastStudiobucketid = '';
                $bucketName = '';
                foreach ($streamData AS $key => $val) {
                    $videoName = $val['full_movie'];
                    $movieID = $val['stream_id'];
                    if ($lastbucketid != $val['s3bucket_id'] || $lastStudiobucketid != $val['studio_s3bucket_id']) {
                        $bucketInfo = Yii::app()->common->getBucketInfo("", $val['studio_id']);
                        $bucketName = $bucketInfo['bucket_name'];
                        $s3url = $bucketInfo['s3url'];
                        $s3cfg = $bucketInfo['s3cmd_file_name'];
                    }
                    $lastbucketid = $val['s3bucket_id'];
                    $lastStudiobucketid = $val['studio_s3bucket_id'];
                    $updateUrl = BASE_URL . "/conversion/UpdateMovieInfo?stream_id=" . $val['stream_id'] . "MUVIMUVI" . $val['uniq_id'];
                    $urlForVideoNotConverted = BASE_URL . "/cron/UpdateMovieVideoNotConverted?stream_id=" . $val['stream_id'] . "MUVIMUVI" . $val['uniq_id'] . "MUVIMUVI" . $val['video_management_id'];
                    $s3 = S3Client::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                    ));
                    $vidoInfo = new SplFileInfo($videoName);
                    $videoExt = $vidoInfo->getExtension();
                    $folderPath = Yii::app()->common->getFolderPath($val['new_cdn_users'], $val['studio_id']);
                    $signedBucketPath = $folderPath['signedFolderPath'];
                    $bucketHttpUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/';
                    $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "uploads/movie_stream/full_movie/" . $movieID . "/";
                    $videoUrl = $bucketHttpUrl . $videoName;
                    $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
                    $updateUrl .= "MUVIMUVI" . $videoOnlyName . "MUVIMUVI" . $val['video_management_id'];
                    $output = Yii::app()->common->get_video_resolution($videoUrl, $ffmpeg_path, $videoExt);
                    $video_Resolution = array();
                    $videoWidth = 0;
                    $videoHeight = 0;
                    if (isset($output['width']) && isset($output['height']) && $output['height'] != '' && $output['width'] != '') {
                        $videoHeight = $output['height'];
                        $videoWidth = $output['width'];
                        if ($videoHeight >= 2160) {
                            $GLOBALS['default_video_resolution'] = 2160;
                        } else if ($videoHeight >= 1440) {
                            $GLOBALS['default_video_resolution'] = 1440;
                        } else if ($videoHeight >= 1080) {
                            $GLOBALS['default_video_resolution'] = 1080;
                        } else if ($videoHeight >= 720) {
                            $GLOBALS['default_video_resolution'] = 720;
                        } else if ($videoHeight >= 480) {
                            $GLOBALS['default_video_resolution'] = 480;
                        } else if ($videoHeight >= 360) {
                            $GLOBALS['default_video_resolution'] = 360;
                        } else if ($videoHeight >= 240) {
                            $GLOBALS['default_video_resolution'] = 240;
                        } else if ($videoHeight >= 144) {
                            $GLOBALS['default_video_resolution'] = 144;
                        } else if ($videoHeight < 144) {
                            $GLOBALS['default_video_resolution'] = $videoHeight;
                        }

                        if ($GLOBALS['default_video_resolution'] != 0 && $videoWidth != 0) {
                            if ($GLOBALS['default_video_resolution'] >= 144) {
                                $video_Resolution = $videoResolution[$GLOBALS['default_video_resolution']];
                            } else {
                                $video_Resolution = Yii::app()->common->getvideoResolutionForTheVideo($GLOBALS['video_resolution'], $GLOBALS['default_video_resolution']);
                            }

                            $conversioServerffmpegPath = '/root/bin/ffmpeg';
                            $server_id = 0;
                            if ($isEntreprise == 1) {
                                $server_id = $serverIds;
                            } else if ($val['s3bucket_id'] == 6 && $singaporeServerIp != 0) {
                                $server_id = $singaporeServerIp;
                            } else {
                                $serverDetailsKey = array_search(max($severLimitLeft), $severLimitLeft);
                                $server_id = $encodingServerDetails[$serverDetailsKey]['id'];
                                $severLimitLeft[$serverDetailsKey] = $severLimitLeft[$serverDetailsKey] - 1;
                            }
                            $s3shFolderPath = $serverDataArray[$server_id]['s3folder_name'];
                            $threads = $serverDataArray[$server_id]['threads'];
                            //Create shell script
                            $file = date("Y_m_d_H_i_s") . '_' . $movieID . HOST_IP . '.sh';
                            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
                            $cf = 'echo "${file%???}"';
                            $originalFileConversion = '';
                            $moveFile = '';
                            $checkFileExistIfCondition = '';
                            $checkFileExistElseCondition = '';
                            $originalFileConversion .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/original \n";
                            $originalFileConversion .= "wget " . $videoUrl . " \n";
                            $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/original/" . $videoName . " " . $bucket_url . $videoOnlyName . "_original." . $videoExt . " \n";
                            $moveFile .= "/usr/local/bin/s3cmd del --config /usr/local/bin/.".$s3cfg." --acl-public --add-header='content-type':'video/mp4' /var/www/html/".$videoFolder."/".$movieID."/original/".$videoName." ".$bucket_url.$videoOnlyName.".".$videoExt." \n";
                            $checkFileExistIfCondition .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';

                            $updateUrl .="MUVIMUVIBEST";
                            $copyCmd = '';
                            $encryptionKey = '';
                            $contentKey = '';
                            if (@$drmEnable[$val['studio_id']] == 1) {
                                $moveFile = '';
                                $conversion_cmd = "";
                                $fragmentCmd = "";
                                $checkFrangmentedFileExistIf = '';
                                $checkFrangmentedFileElseCondition = '';
                                $packaginofVideo = '';
                                $packaginofVideoHls = '';
                                $multiBitrateMlv = "";
                                $encryptionKey = Yii::app()->general->moreRand(32);
                                $contentKey = Yii::app()->general->moreRand(32);
                                $conversion_cmd .= $conversioServerffmpegPath . ' -y -i /var/www/html/' . $videoFolder . '/' . $movieID . '/original/' . $videoName . ' -c:v libx264 -x264opts \'keyint=24:min-keyint=24:no-scenecut\' -movflags faststart -threads ' . $threads . ' /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                                $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$videoOnlyName.".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$videoOnlyName . ".mlv \n";
                                $newVideoName = '';
                                //Video Fragment
                                $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4 \n";
                                $checkFrangmentedFileExistIf .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mf4';
                                //For multiple type conversion and Fragment and package
                                $packaginofVideo .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                                $packaginofVideoHls .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/hls \n";


                                $packaginofVideo .= "/var/www/html/bento4New/bin/mp4dash --encryption-key=" . $encryptionKey . ":" . $contentKey . " --marlin --widevine --widevine-header=provider:intertrust#content_id:2a --playready --playready-header=https://expressplay-licensing.axprod.net/LicensingService.ashx " . $videoOnlyName . ".mf4";
                                $packaginofVideoHls .= "/var/www/html/bento4New/bin/mp4hls --encryption-mode=SAMPLE-AES --encryption-key=" . $encryptionKey . $contentKey . " --encryption-iv-mode=fps --encryption-key-format=com.apple.streamingkeydelivery --encryption-key-uri=skd://expressplay_token /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4";
                                foreach ($video_Resolution as $key => $value) {
                                    $videoFileName = $GLOBALS['video_screen_resolution'][$value];
                                    $updateUrl .="MUVIMUVI" . $videoFileName;
                                    $screenWidth = round(($value / $videoHeight) * $videoWidth);
                                    if ($screenWidth % 2 != 0) {
                                        $screenWidth = $screenWidth + 1;
                                    }

                                    $bandwidth = @$GLOBALS['video_bandwidth'][$value];
                                    $bandwidthAverage = @$GLOBALS['video_bandwidth_average'][$value];
                                    $moveFile .= "sed -i '/height=\"" . $value . "\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"" . $bandwidth . "\"/' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                                    $moveFile .= "sed -i '/RESOLUTION=" . $screenWidth . "x" . $value . "/s/BANDWIDTH=*[0-9]*/BANDWIDTH=" . $bandwidthAverage . "/g; /RESOLUTION=" . $screenWidth . "x" . $value . "/s/AVERAGE-BANDWIDTH=*[0-9]*/AVERAGE-BANDWIDTH=" . $bandwidth . "/g;' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";

                                    $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                                    $mlvFileName = $videoOnlyName . '_' . $videoFileName . '.mlv';
                                    $videoFrangmentName = $videoOnlyName . '_' . $videoFileName . '.mf4';
                                    $conversion_cmd.= " -c:v libx264 -x264opts 'keyint=24:min-keyint=24:no-scenecut' -movflags faststart -vf \"scale=" . $screenWidth . ":" . $value . "\" -threads " . $threads . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName;
                                    $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$newVideoName." /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$mlvFileName . " \n";
                                    $checkMlv .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/output/'.$mlvFileName;
                                    $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $newVideoName;
                                    $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName . " \n";
                                    $checkFrangmentedFileExistIf .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoFrangmentName;
                                    $packaginofVideo .= " " . $videoFrangmentName;
                                    $packaginofVideoHls .= " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName;
                                    if(@$drmMobileDisable[$val['studio_id']] == 1){
                                        $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " " . $bucket_url . " \n";
                                        $isMobileDRMDisable ++;
                                    }
                                }
                                $packaginofVideo .= " \n";
                                $packaginofVideo .=$multiBitrateMlv;
                                $packaginofVideoHls .= " \n";
                                $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/dash+xml' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd " . $bucket_url . " \n";
                                $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                                $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/output/ " . $bucket_url . " \n";
                                $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/x-mpegURL' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 " . $bucket_url . "hls/ \n";
                                $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";
                                $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/ " . $bucket_url . "hls/ \n";
                                $checkFrangmentedFileExistIf .= " ]\n\nthen\n\n";
                                $checkFileExistIfCondition .= " ]\n\nthen\n\n";

                                $checkFileExistElseCondition .="\n\nelse\n\n";
                                $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                                $checkFileExistElseCondition .="# remove directory \n";
                                $checkFileExistElseCondition .="rm -rf /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                                $checkFileExistElseCondition .="# remove the process copy file \n";
                                $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/conversionCompleted/" . $file . " \n";
                                $checkFileExistElseCondition .="# remove the process file \n";
                                $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/" . $file . " \n";
                                $checkFileExistElseCondition .="fi\n\n";

                                $file_data = "file=`echo $0`\n" .
                                        "cf='" . $file . "'\n\n" .
                                        "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                        "then\n\n" .
                                        "echo \"$file is running\"\n\n" .
                                        "else\n\n" .
                                        "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                        "# create directory\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID/original\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID/original\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID/hls\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID/hls\n" .
                                        "# convertion command\n" .
                                        $originalFileConversion .
                                        "\n" .
                                        "# For multiple video resolution\n" .
                                        $conversion_cmd .
                                        "\n\n\n# Check all the video file's are created\n" .
                                        $checkFileExistIfCondition .
                                        "\n\n\n# Fragement the file\n" .
                                        $fragmentCmd .
                                        "\n\n\n# Check all the Fragemented file's are created \n" .
                                        $checkFrangmentedFileExistIf .
                                        "\n\n\n# Packaging the file using encryption key and content key for multi drm \n" .
                                        $packaginofVideo .
                                        $packaginofVideoHls .
                                        "if [ -s /var/www/html/$videoFolder/$movieID/output/stream.mpd$checkMlv ]\n\n" .
                                        "then\n\n" .
                                        "\n# upload to s3\n" .
                                        $moveFile .
                                        "# update query\n" .
                                        "curl $updateUrl\n" .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        $checkFileExistElseCondition .
                                        $checkFileExistElseCondition .
                                        $checkFileExistElseCondition .
                                        "fi";
                            } else {
                                $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mp4 " . $bucket_url . " \n";

                                $conversion_cmd = "";
                                $conversion_cmd .= $conversioServerffmpegPath . ' -async 1 -i /var/www/html/' . $videoFolder . '/' . $movieID . '/original/' . $videoName . ' -movflags faststart -threads ' . $threads . ' /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                                $newVideoName = '';

                                //For multiple type conversion
                                foreach ($video_Resolution as $key => $value) {
                                    $videoFileName = $GLOBALS['video_screen_resolution'][$value];
                                    $updateUrl .="MUVIMUVI" . $videoFileName;
                                    $screenWidth = round(($value / $videoHeight) * $videoWidth);
                                    if ($screenWidth % 2 != 0) {
                                        $screenWidth = $screenWidth + 1;
                                    }
                                    $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                                    $conversion_cmd.= " -s " . $screenWidth . ":" . $value . " -movflags faststart -threads " . $threads . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName;
                                    $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " " . $bucket_url . " \n";
                                    $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $newVideoName;
                                }
                                $checkFileExistIfCondition .= " ]\n\nthen\n\n";
                                $checkFileExistElseCondition .="\n\nelse\n\n";
                                $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                                $file_data = "file=`echo $0`\n" .
                                        "cf='" . $file . "'\n\n" .
                                        "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                        "then\n\n" .
                                        "echo \"$file is running\"\n\n" .
                                        "else\n\n" .
                                        "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                        "# create directory\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID/original\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID/original\n" .
                                        "# convertion command\n" .
                                        $originalFileConversion .
                                        "\n" .
                                        "# For BEST resolution\n" .
                                        $copyCmd .
                                        "\n" .
                                        "# For multiple video resolution\n" .
                                        $conversion_cmd .
                                        "\n\n\n# Check all the video file's are created\n" .
                                        $checkFileExistIfCondition .
                                        "\n# upload to s3\n" .
                                        $moveFile .
                                        "# update query\n" .
                                        "curl $updateUrl\n" .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        $checkFileExistElseCondition .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        "fi\n\n" .
                                        "fi";
                            }
                            fwrite($handle, $file_data);
                            fclose($handle);
                            //Uploading conversion script from local to s3 
                            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file;
                            if ($s3->upload($conversionBucket, $s3shFolderPath . "/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                                echo "Sh file created successfully";
                                if (unlink($filePath)) {
                                    $head = array_change_key_case(get_headers($videoUrl, TRUE));
                                    $filesize = ($head['content-length'] / 1048576);
                                    if (($val['s3bucket_id'] == 6 && $singaporeServerIp != 0) || $isEntreprise == 1) {
                                        $encodingNew = new Encoding;
                                        $encodingNew->studio_id = $val['studio_id'];
                                        $encodingNew->movie_stream_id = $movieID;
                                        $encodingNew->upload_time = $val['upload_start_time'];
                                    } else {
                                        $encodingNew = Encoding::model()->findByAttributes(array('movie_stream_id' => $movieID));
                                    }
                                    $encodingTotalsizeqry = "select sum(size) as totalSize from encoding where encoding_server_id=" . $server_id;
                                    $encodingTotalsize = Yii::app()->db->createCommand($encodingTotalsizeqry)->queryAll();
                                    $videoEncodingTime = (ceil(@$encodingTotalsizeqry[0]['totalSize'] + $filesize)) * 4;
                                    if ($videoEncodingTime < 900) {
                                        $videoEncodingTime = 900;
                                    }
                                    $videoEncodingTime = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) + $videoEncodingTime);

                                    $encodingNew->size = $filesize;
                                    $encodingNew->encoding_server_id = $server_id;
                                    $encodingNew->encoding_start_time = date('Y-m-d H:i:s');
                                    $encodingNew->expected_end_time = $videoEncodingTime;
                                    $encodingNew->save();

                                    $epData = movieStreams::model()->findByPk($movieID);
                                    $epData->encryption_key = $encryptionKey;
                                    $epData->content_key = $contentKey;
                                    $epData->has_sh = 1;
                                    if ($encryptionKey != '' && $contentKey != '') {
                                        $epData->is_offline = 1;
                                        $epData->is_multibitrate_offline = 1;
                                    }
                                    if($isMobileDRMDisable == 0){
                                        $epData->is_mobile_drm_disable = 0;
                                    } else{
                                        $epData->is_mobile_drm_disable = 1;
                                    }
                                    $epData->encoding_start_time = date('Y-m-d H:i:s');
                                    $return = $epData->save();
                                }
                            }
                        } else {
                            Yii::app()->db->createCommand("delete from encoding WHERE movie_stream_id=" . $movieID)->execute();
                            $epData = movieStreams::model()->findByPk($movieID);
                            $epData->is_converted = 2;
                            $epData->is_offline = 0;
                            $epData->full_movie = '';
                            $epData->encode_fail_time = date('Y-m-d H:i:s');
                            $return = $epData->save();
                            $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                            $videoToBeDeleted = "s3://" . $bucketName . "/" . $s3dir;
                            $data['url'] = 'http://' . $bucketName . '.' . $bucketInfo['s3url'] . '/' . $s3dir;
                            if ($val['video_management_id'] == 0) {
                                $this->createUploadVideoSHFOrVideoGallery($data, "", $videoName, $val['studio_id'], $videoToBeDeleted);
                            } else {
                                $s3ForStudio = Yii::app()->common->connectToAwsS3($val['studio_id']);
                                $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                                $s3ForStudio->deleteObject(array(
                                    'Bucket' => $bucketName,
                                    'Key' => $s3dir
                                ));
                            }
                        }
                    } else {
                        Yii::app()->db->createCommand("delete from encoding WHERE movie_stream_id=" . $movieID)->execute();
                        $epData = movieStreams::model()->findByPk($movieID);
                        $epData->is_converted = 2;
                        $epData->is_offline = 0;
                        $epData->full_movie = '';
                        $epData->encode_fail_time = date('Y-m-d H:i:s');
                        $return = $epData->save();
                        $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                        $videoToBeDeleted = "s3://" . $bucketName . "/" . $s3dir;
                        $data['url'] = 'http://' . $bucketName . '.' . $bucketInfo['s3url'] . '/' . $s3dir;
                        if ($val['video_management_id'] == 0) {
                            $this->createUploadVideoSHFOrVideoGallery($data, "", $videoName, $val['studio_id'], $videoToBeDeleted);
                        } else {
                            $s3ForStudio = Yii::app()->common->connectToAwsS3($val['studio_id']);
                            $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                            $s3ForStudio->deleteObject(array(
                                'Bucket' => $bucketName,
                                'Key' => $s3dir
                            ));
                        }
                    }
                }
            }
        }
    }

    /**
     * @method public videoConversionSH() Reads the video file and creates a .sh file and put that into the highend server.
     * @author GDR<support@muvi.com>
     * @return bool true/false
     */
    function actionVideoConversionForPerticularStudioSH() {
        $this->layout = false;
        $condition = '';
        $videoFolder = 'video';
        $shFolder = 'progressDRM';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
            if (isset($_REQUEST['movie_stream_id'])) {
                $condition = ' AND ms.id=' . $_REQUEST['movie_stream_id'];
            } else {
                echo "Pls provide the movie_stream_id";
                exit;
            }
        } else if (HOST_IP == '52.0.64.95') {
            if (isset($_REQUEST['movie_stream_id'])) {
                $condition = ' AND ms.id=' . $_REQUEST['movie_stream_id'];
            } else {
                echo "Pls provide the movie_stream_id";
                exit;
            }
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
        }
        if (@$_REQUEST['studio_id'] == '' || @$_REQUEST['encoding_server_id'] == '') {
            echo "Pls provide the studio_id and encoding_server_id";
            exit;
        }
        $encodingServerId = $_REQUEST['encoding_server_id'];
        $listOfStream = '';
        $studioIds = '';
        $drmEnable = array();
        $drmMobileDisable = array();
        $alertStatus = array();
        $encodingServer = "select * from encoding_server_details where id=" . $encodingServerId;
        $encodingServerDetails = Yii::app()->db->createCommand($encodingServer)->queryAll();
        $totalLimit = 0;
        $fileInQue = 0;
        $limit = 0;
        $severLimitLeft = array();
        $serverIds = '';
        $serverDataArray = array();
        if ($encodingServerDetails) {
            foreach ($encodingServerDetails as $encodingServerDetailskey => $encodingServerDetailsvalue) {
                if (@$encodingServerDetailsvalue['threads'] == '') {
                    $serverDataArray[$encodingServerDetailsvalue['id']]['threads'] = 28;
                } else {
                    $serverDataArray[$encodingServerDetailsvalue['id']]['threads'] = $encodingServerDetailsvalue['threads'];
                }
                $serverDataArray[$encodingServerDetailsvalue['id']]['s3folder_name'] = $encodingServerDetailsvalue['s3folder_name'];
            }
        }
        print_r($serverDataArray);
        if ($serverDataArray) {
            $encodingDataQuery = "SELECT movie_stream_id,studio_id FROM encoding where encoding_server_id =0 and studio_id=" . $_REQUEST['studio_id'] . " ORDER BY id";
            $encodingData = Yii::app()->db->createCommand($encodingDataQuery)->queryAll();
            if (isset($encodingData) && !empty($encodingData) && count($encodingData) > 0) {
                foreach ($encodingData as $encodingDatakey => $encodingDatavalue) {
                    $studioIds .= $encodingDatavalue['studio_id'] . ",";
                    $listOfStream .= $encodingDatavalue['movie_stream_id'] . ",";
                }
                $listOfStream = substr($listOfStream, 0, -1);
                $studioIds = substr($studioIds, 0, -1);
            }
        }
        if ($studioIds != "") {
            $drmData = Yii::app()->db->createCommand()
                    ->select('config_value,studio_id')
                    ->from('studio_config')
                    ->where('config_key = "drm_enable" AND studio_id IN (' . $studioIds . ')')
                    ->queryAll();
            foreach ($drmData as $drmDataKey => $drmDataVal) {
                $drmEnable[$drmDataVal['studio_id']] = $drmDataVal['config_value'];
            }
            $drmMobileData = Yii::app()->db->createCommand()
                    ->select('config_value,studio_id')
                    ->from('studio_config')
                    ->where('config_key = "drm_mobile_disable" AND studio_id IN (' . $studioIds . ')')
                    ->queryAll();
            foreach($drmMobileData as $drmMobileDataKey => $drmMobileDataVal) {
                $drmMobileDisable[$drmMobileDataVal['studio_id']] = $drmMobileDataVal['config_value'];
            }
        }
        if ($listOfStream != '') {
            $videoResolutionData = VideoResolution::model()->findAll();
            $videoResolution = array();
            if ($videoResolutionData) {
                foreach ($videoResolutionData as $videoResolutionDatakey => $videoResolutionDatavalue) {
                    $videoResolution[$videoResolutionDatavalue['resolution']] = explode(",", $videoResolutionDatavalue['resolution_to_be_encoded']);
                }
            }
            $streamData = Yii::app()->db->createCommand()
                    ->select('ms.id AS stream_id,ms.movie_id,s.id as studio_id,s.priority,s.new_cdn_users,s.s3bucket_id,s.studio_s3bucket_id,ms.full_movie,f.uniq_id,ms.video_management_id,ms.upload_start_time')
                    ->from('movie_streams ms,studios s,films f ')
                    ->where('ms.studio_id = s.id AND f.id = ms.movie_id AND ms.has_sh=0 AND ms.is_converted =0 AND ms.full_movie!=\'\' AND ms.id IN (' . $listOfStream . ')')
                    ->queryAll();
            if ($streamData) {
                $lastbucketid = '';
                $lastStudiobucketid = '';
                $bucketName = '';
                foreach ($streamData AS $key => $val) {
                    $videoName = $val['full_movie'];
                    $movieID = $val['stream_id'];
                    $isMobileDRMDisable = 0;
                    if ($lastbucketid != $val['s3bucket_id'] && $lastStudiobucketid != $val['studio_s3bucket_id']) {
                        $bucketInfo = Yii::app()->common->getBucketInfo("", $val['studio_id']);
                        $bucketName = $bucketInfo['bucket_name'];
                        $s3url = $bucketInfo['s3url'];
                        $s3cfg = $bucketInfo['s3cmd_file_name'];
                    }
                    $lastbucketid = $val['s3bucket_id'];
                    $lastStudiobucketid = $val['studio_s3bucket_id'];
                    $updateUrl = BASE_URL . "/conversion/UpdateMovieInfo?stream_id=" . $val['stream_id'] . "MUVIMUVI" . $val['uniq_id'];
                    $urlForVideoNotConverted = BASE_URL . "/cron/UpdateMovieVideoNotConverted?stream_id=" . $val['stream_id'] . "MUVIMUVI" . $val['uniq_id'] . "MUVIMUVI" . $val['video_management_id'];
                    $s3 = S3Client::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                    ));
                    $vidoInfo = new SplFileInfo($videoName);
                    $videoExt = $vidoInfo->getExtension();
                    $folderPath = Yii::app()->common->getFolderPath($val['new_cdn_users'], $val['studio_id']);
                    $signedBucketPath = $folderPath['signedFolderPath'];
                    $bucketHttpUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/';
                    $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "uploads/movie_stream/full_movie/" . $movieID . "/";
                    $videoUrl = $bucketHttpUrl . $videoName;
                    $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
                    $updateUrl .= "MUVIMUVI" . $videoOnlyName . "MUVIMUVI" . $val['video_management_id'];
                    $output = Yii::app()->common->get_video_resolution($videoUrl, $ffmpeg_path, $videoExt);
                    $video_Resolution = array();
                    $videoWidth = 0;
                    $videoHeight = 0;
                    if (isset($output['width']) && isset($output['height']) && $output['height'] != '' && $output['width'] != '') {
                        $videoHeight = $output['height'];
                        $videoWidth = $output['width'];
                        if ($videoHeight >= 2160) {
                            $GLOBALS['default_video_resolution'] = 2160;
                        } else if ($videoHeight >= 1440) {
                            $GLOBALS['default_video_resolution'] = 1440;
                        } else if ($videoHeight >= 1080) {
                            $GLOBALS['default_video_resolution'] = 1080;
                        } else if ($videoHeight >= 720) {
                            $GLOBALS['default_video_resolution'] = 720;
                        } else if ($videoHeight >= 480) {
                            $GLOBALS['default_video_resolution'] = 480;
                        } else if ($videoHeight >= 360) {
                            $GLOBALS['default_video_resolution'] = 360;
                        } else if ($videoHeight >= 240) {
                            $GLOBALS['default_video_resolution'] = 240;
                        } else if ($videoHeight >= 144) {
                            $GLOBALS['default_video_resolution'] = 144;
                        } else if ($videoHeight < 144) {
                            $GLOBALS['default_video_resolution'] = $videoHeight;
                        }

                        if ($GLOBALS['default_video_resolution'] != 0 && $videoWidth != 0) {
                            if ($GLOBALS['default_video_resolution'] >= 144) {
                                $video_Resolution = $videoResolution[$GLOBALS['default_video_resolution']];
                            } else {
                                $video_Resolution = Yii::app()->common->getvideoResolutionForTheVideo($GLOBALS['video_resolution'], $GLOBALS['default_video_resolution']);
                            }

                            $conversioServerffmpegPath = '/root/bin/ffmpeg';
                            $s3shFolderPath = $serverDataArray[$encodingServerId]['s3folder_name'];
                            $server_id = $encodingServerId;
                            $threads = $serverDataArray[$encodingServerId]['threads'];

                            $file = date("Y_m_d_H_i_s") . '_' . $movieID . HOST_IP . '.sh';
                            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
                            $cf = 'echo "${file%???}"';
                            $originalFileConversion = '';
                            $moveFile = '';
                            $checkFileExistIfCondition = '';
                            $checkFileExistElseCondition = '';
                            $originalFileConversion .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/original \n";
                            $originalFileConversion .= "wget " . $videoUrl . " \n";
                            $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/original/" . $videoName . " " . $bucket_url . $videoOnlyName . "_original." . $videoExt . " \n";
                            $moveFile .= "/usr/local/bin/s3cmd del --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/original/" . $videoName . " " . $bucket_url . $videoOnlyName . "." . $videoExt . " \n";
                            $checkFileExistIfCondition .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';

                            $updateUrl .="MUVIMUVIBEST";
                            $copyCmd = '';
                            $encryptionKey = '';
                            $contentKey = '';
                            if (@$drmEnable[$val['studio_id']] == 1) {
                                $moveFile = '';
                                $conversion_cmd = "";
                                $fragmentCmd = "";
                                $checkFrangmentedFileExistIf = '';
                                $checkFrangmentedFileElseCondition = '';
                                $packaginofVideo = '';
                                $packaginofVideoHls = '';
                                $multiBitrateMlv = "";
                                $encryptionKey = Yii::app()->general->moreRand(32);
                                $contentKey = Yii::app()->general->moreRand(32);
                                $conversion_cmd .= $conversioServerffmpegPath . ' -y -i /var/www/html/' . $videoFolder . '/' . $movieID . '/original/' . $videoName . ' -c:v libx264 -x264opts \'keyint=24:min-keyint=24:no-scenecut\' -movflags faststart -threads ' . $threads . ' /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                                $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$videoOnlyName.".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$videoOnlyName . ".mlv \n";
                                $newVideoName = '';
                                //Video Fragment
                                $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4 \n";
                                $checkFrangmentedFileExistIf .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mf4';
                                //For multiple type conversion and Fragment and package
                                $packaginofVideo .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                                $packaginofVideoHls .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/hls \n";


                                $packaginofVideo .= "/var/www/html/bento4New/bin/mp4dash --encryption-key=" . $encryptionKey . ":" . $contentKey . " --marlin --widevine --widevine-header=provider:intertrust#content_id:2a --playready --playready-header=https://expressplay-licensing.axprod.net/LicensingService.ashx " . $videoOnlyName . ".mf4";
                                $packaginofVideoHls .= "/var/www/html/bento4New/bin/mp4hls --encryption-mode=SAMPLE-AES --encryption-key=" . $encryptionKey . $contentKey . " --encryption-iv-mode=fps --encryption-key-format=com.apple.streamingkeydelivery --encryption-key-uri=skd://expressplay_token /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4";
                                foreach ($video_Resolution as $key => $value) {
                                    $videoFileName = $GLOBALS['video_screen_resolution'][$value];
                                    $updateUrl .="MUVIMUVI" . $videoFileName;
                                    $screenWidth = round(($value / $videoHeight) * $videoWidth);
                                    if ($screenWidth % 2 != 0) {
                                        $screenWidth = $screenWidth + 1;
                                    }

                                    $bandwidth = @$GLOBALS['video_bandwidth'][$value];
                                    $bandwidthAverage = @$GLOBALS['video_bandwidth_average'][$value];
                                    $moveFile .= "sed -i '/height=\"" . $value . "\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"" . $bandwidth . "\"/' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                                    $moveFile .= "sed -i '/RESOLUTION=" . $screenWidth . "x" . $value . "/s/BANDWIDTH=*[0-9]*/BANDWIDTH=" . $bandwidthAverage . "/g; /RESOLUTION=" . $screenWidth . "x" . $value . "/s/AVERAGE-BANDWIDTH=*[0-9]*/AVERAGE-BANDWIDTH=" . $bandwidth . "/g;' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";

                                    $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                                    $mlvFileName = $videoOnlyName . '_' . $videoFileName . '.mlv';
                                    $videoFrangmentName = $videoOnlyName . '_' . $videoFileName . '.mf4';
                                    $conversion_cmd.= " -c:v libx264 -x264opts 'keyint=24:min-keyint=24:no-scenecut' -movflags faststart -vf \"scale=" . $screenWidth . ":" . $value . "\" -threads " . $threads . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName;
                                    $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $newVideoName;
                                    $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$newVideoName." /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$mlvFileName . " \n";
                                    $checkMlv .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/output/'.$mlvFileName;
                                    $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName . " \n";
                                    $checkFrangmentedFileExistIf .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoFrangmentName;
                                    $packaginofVideo .= " " . $videoFrangmentName;
                                    $packaginofVideoHls .= " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName;
                                    if(@$drmMobileDisable[$val['studio_id']] == 1){
                                        $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " " . $bucket_url . " \n";
                                        $isMobileDRMDisable ++;
                                    }
                                }
                                $packaginofVideo .= " \n";
                                $packaginofVideo .=$multiBitrateMlv;
                                $packaginofVideoHls .= " \n";
                                $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/dash+xml' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd " . $bucket_url . " \n";
                                $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                                $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/output/ " . $bucket_url . " \n";
                                $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/x-mpegURL' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 " . $bucket_url . "hls/ \n";
                                $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";
                                $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/ " . $bucket_url . "hls/ \n";
                                $checkFrangmentedFileExistIf .= " ]\n\nthen\n\n";
                                $checkFileExistIfCondition .= " ]\n\nthen\n\n";

                                $checkFileExistElseCondition .="\n\nelse\n\n";
                                $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                                $checkFileExistElseCondition .="# remove directory \n";
                                $checkFileExistElseCondition .="rm -rf /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                                $checkFileExistElseCondition .="# remove the process copy file \n";
                                $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/conversionCompleted/" . $file . " \n";
                                $checkFileExistElseCondition .="# remove the process file \n";
                                $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/" . $file . " \n";
                                $checkFileExistElseCondition .="fi\n\n";

                                $file_data = "file=`echo $0`\n" .
                                        "cf='" . $file . "'\n\n" .
                                        "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                        "then\n\n" .
                                        "echo \"$file is running\"\n\n" .
                                        "else\n\n" .
                                        "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                        "# create directory\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID/original\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID/original\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID/hls\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID/hls\n" .
                                        "# convertion command\n" .
                                        $originalFileConversion .
                                        "\n" .
                                        "# For multiple video resolution\n" .
                                        $conversion_cmd .
                                        "\n\n\n# Check all the video file's are created\n" .
                                        $checkFileExistIfCondition .
                                        "\n\n\n# Fragement the file\n" .
                                        $fragmentCmd .
                                        "\n\n\n# Check all the Fragemented file's are created \n" .
                                        $checkFrangmentedFileExistIf .
                                        "\n\n\n# Packaging the file using encryption key and content key for multi drm \n" .
                                        $packaginofVideo .
                                        $packaginofVideoHls .
                                        "if [ -s /var/www/html/$videoFolder/$movieID/output/stream.mpd$checkMlv ]\n\n" .
                                        "then\n\n" .
                                        "\n# upload to s3\n" .
                                        $moveFile .
                                        "# update query\n" .
                                        "curl $updateUrl\n" .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        $checkFileExistElseCondition .
                                        $checkFileExistElseCondition .
                                        $checkFileExistElseCondition .
                                        "fi";
                            } else {
                                $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mp4 " . $bucket_url . " \n";

                                $conversion_cmd = "";
                                $conversion_cmd .= $conversioServerffmpegPath . ' -async 1 -i /var/www/html/' . $videoFolder . '/' . $movieID . '/original/' . $videoName . ' -movflags faststart -threads ' . $threads . ' /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                                $newVideoName = '';

                                //For multiple type conversion
                                foreach ($video_Resolution as $key => $value) {
                                    $videoFileName = $GLOBALS['video_screen_resolution'][$value];
                                    $updateUrl .="MUVIMUVI" . $videoFileName;
                                    $screenWidth = round(($value / $videoHeight) * $videoWidth);
                                    if ($screenWidth % 2 != 0) {
                                        $screenWidth = $screenWidth + 1;
                                    }
                                    $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                                    $conversion_cmd.= " -s " . $screenWidth . ":" . $value . " -movflags faststart -threads " . $threads . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName;
                                    $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " " . $bucket_url . " \n";
                                    $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $newVideoName;
                                }
                                $checkFileExistIfCondition .= " ]\n\nthen\n\n";
                                $checkFileExistElseCondition .="\n\nelse\n\n";
                                $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                                $file_data = "file=`echo $0`\n" .
                                        "cf='" . $file . "'\n\n" .
                                        "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                        "then\n\n" .
                                        "echo \"$file is running\"\n\n" .
                                        "else\n\n" .
                                        "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                        "# create directory\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                                        "mkdir /var/www/html/$videoFolder/$movieID/original\n" .
                                        "chmod 0777 /var/www/html/$videoFolder/$movieID/original\n" .
                                        "# convertion command\n" .
                                        $originalFileConversion .
                                        "\n" .
                                        "# For BEST resolution\n" .
                                        $copyCmd .
                                        "\n" .
                                        "# For multiple video resolution\n" .
                                        $conversion_cmd .
                                        "\n\n\n# Check all the video file's are created\n" .
                                        $checkFileExistIfCondition .
                                        "\n# upload to s3\n" .
                                        $moveFile .
                                        "# update query\n" .
                                        "curl $updateUrl\n" .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        $checkFileExistElseCondition .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        "fi\n\n" .
                                        "fi";
                            }
                            fwrite($handle, $file_data);
                            fclose($handle);
                            //Uploading conversion script from local to s3 
                            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file;
                            if ($s3->upload($conversionBucket, $s3shFolderPath . "/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                                echo "Sh file created successfully";
                                if (unlink($filePath)) {
                                    $head = array_change_key_case(get_headers($videoUrl, TRUE));
                                    $filesize = ($head['content-length'] / 1048576);
                                    $encodingNew = Encoding::model()->findByAttributes(array('movie_stream_id' => $movieID));
                                    $encodingTotalsizeqry = "select sum(size) as totalSize from encoding where encoding_server_id=" . $server_id;
                                    $encodingTotalsize = Yii::app()->db->createCommand($encodingTotalsizeqry)->queryAll();
                                    $videoEncodingTime = (ceil(@$encodingTotalsizeqry[0]['totalSize'] + $filesize)) * 4;
                                    if ($videoEncodingTime < 900) {
                                        $videoEncodingTime = 900;
                                    }
                                    $videoEncodingTime = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) + $videoEncodingTime);

                                    $encodingNew->size = $filesize;
                                    $encodingNew->encoding_server_id = $server_id;
                                    $encodingNew->encoding_start_time = date('Y-m-d H:i:s');
                                    $encodingNew->expected_end_time = $videoEncodingTime;
                                    $encodingNew->save();

                                    $epData = movieStreams::model()->findByPk($movieID);
                                    $epData->encryption_key = $encryptionKey;
                                    $epData->content_key = $contentKey;
                                    if ($encryptionKey != '' && $contentKey != '') {
                                        $epData->is_offline = 1;
                                        $epData->is_multibitrate_offline = 1;
                                    }
                                    if($isMobileDRMDisable == 0){
                                        $epData->is_mobile_drm_disable = 0;
                                    } else{
                                        $epData->is_mobile_drm_disable = 1;
                                    }
                                    $epData->has_sh = 1;
                                    $epData->encoding_start_time = date('Y-m-d H:i:s');
                                    $return = $epData->save();
                                }
                            }
                        } else {
                            Yii::app()->db->createCommand("delete from encoding WHERE movie_stream_id=" . $movieID)->execute();
                            $epData = movieStreams::model()->findByPk($movieID);
                            $epData->is_converted = 2;
                            $epData->is_offline = 0;
                            $epData->full_movie = '';
                            $epData->encode_fail_time = date('Y-m-d H:i:s');
                            $return = $epData->save();
                            $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                            $videoToBeDeleted = "s3://" . $bucketName . "/" . $s3dir;
                            $data['url'] = 'http://' . $bucketName . '.' . $bucketInfo['s3url'] . '/' . $s3dir;
                            if ($val['video_management_id'] == 0) {
                                $this->createUploadVideoSHFOrVideoGallery($data, "", $videoName, $val['studio_id'], $videoToBeDeleted);
                            } else {
                                $s3ForStudio = Yii::app()->common->connectToAwsS3($val['studio_id']);
                                $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                                $s3ForStudio->deleteObject(array(
                                    'Bucket' => $bucketName,
                                    'Key' => $s3dir
                                ));
                            }
                        }
                    } else {
                        Yii::app()->db->createCommand("delete from encoding WHERE movie_stream_id=" . $movieID)->execute();
                        $epData = movieStreams::model()->findByPk($movieID);
                        $epData->is_converted = 2;
                        $epData->is_offline = 0;
                        $epData->full_movie = '';
                        $epData->encode_fail_time = date('Y-m-d H:i:s');
                        $return = $epData->save();
                        $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                        $videoToBeDeleted = "s3://" . $bucketName . "/" . $s3dir;
                        $data['url'] = 'http://' . $bucketName . '.' . $bucketInfo['s3url'] . '/' . $s3dir;
                        if ($val['video_management_id'] == 0) {
                            $this->createUploadVideoSHFOrVideoGallery($data, "", $videoName, $val['studio_id'], $videoToBeDeleted);
                        } else {
                            $s3ForStudio = Yii::app()->common->connectToAwsS3($val['studio_id']);
                            $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/' . $videoName;
                            $s3ForStudio->deleteObject(array(
                                'Bucket' => $bucketName,
                                'Key' => $s3dir
                            ));
                        }
                    }
                }
            }
        }
    }

    function actionVideoConversionDRMSH() {
        $this->layout = false;
        $condition = '';
        $videoFolder = 'video';
        $shFolder = 'progressDRM';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
        } else if (HOST_IP == '52.0.64.95') {
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
        }
        if (!isset($_REQUEST['studio_id'])) {
            echo "Please provide studio_id";
            exit;
        }
        $studio_id = $_REQUEST['studio_id'];
        $condition = ' AND ms.studio_id =' . $studio_id;
        $drmData = Yii::app()->db->createCommand()
                ->select('config_value,studio_id')
                ->from('studio_config')
                ->where('config_key = "drm_enable" AND studio_id = ' . $studio_id)
                ->queryAll();
        if (@$drmData[0]['config_value'] != 1) {
            echo "DRM is not enabled for this studio";
            exit;
        }
        $drmMobileDisable = array();
        $drmMobileData = Yii::app()->db->createCommand()
                    ->select('config_value,studio_id')
                    ->from('studio_config')
                    ->where('config_key = "drm_mobile_disable" AND studio_id =' . $studio_id)
                    ->queryAll();
        foreach($drmMobileData as $drmMobileDataKey => $drmMobileDataVal) {
            $drmMobileDisable[$drmMobileDataVal['studio_id']] = $drmMobileDataVal['config_value'];
        }
        $streamData = Yii::app()->db->createCommand()
                ->select('ms.id AS stream_id,ms.movie_id,s.id as studio_id,s.priority,s.new_cdn_users,s.s3bucket_id,s.studio_s3bucket_id,ms.full_movie,f.uniq_id,ms.video_management_id')
                ->from('movie_streams ms,studios s,films f ')
                ->where('ms.studio_id = s.id AND f.id = ms.movie_id AND ms.is_demo="0" AND  ms.is_converted =1 AND ms.encryption_key="" AND ms.content_key ="" AND ms.full_movie!=\'\'' . $condition)
                ->queryAll();
        if ($streamData) {
            $videoResolutionData = VideoResolution::model()->findAll();
            $videoResolution = array();
            if ($videoResolutionData) {
                foreach ($videoResolutionData as $videoResolutionDatakey => $videoResolutionDatavalue) {
                    $videoResolution[$videoResolutionDatavalue['resolution']] = explode(",", $videoResolutionDatavalue['resolution_to_be_encoded']);
                }
            }
            $lastbucketid = '';
            $lastStudiobucketid = '';
            $bucketName = '';
            foreach ($streamData AS $keyval => $val) {
                $streamData[$keyval]['shfileCreated'] = 'No';
                $videoName = $val['full_movie'];
                $movieID = $val['stream_id'];
                $isMobileDRMDisable = 0;
                if ($lastbucketid != $val['s3bucket_id'] && $lastStudiobucketid != $val['studio_s3bucket_id']) {
                    $bucketInfo = Yii::app()->common->getBucketInfo("", $val['studio_id']);
                    $bucketName = $bucketInfo['bucket_name'];
                    $s3url = $bucketInfo['s3url'];
                    $s3cfg = $bucketInfo['s3cmd_file_name'];
                }
                $lastbucketid = $val['s3bucket_id'];
                $lastStudiobucketid = $val['studio_s3bucket_id'];
                $updateUrl = BASE_URL . "/conversion/UpdateMovieInfoDRM?stream_id=" . $val['stream_id'] . "MUVIMUVI" . $val['uniq_id'];
                $urlForVideoNotConverted = "";
                $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                ));
                $vidoInfo = new SplFileInfo($videoName);
                $videoExt = $vidoInfo->getExtension();
                $folderPath = Yii::app()->common->getFolderPath($val['new_cdn_users'], $val['studio_id']);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $bucketHttpUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/';
                $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "uploads/movie_stream/full_movie/" . $movieID . "/";
                $videoUrl = $bucketHttpUrl . $videoName;
                $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
                $output = Yii::app()->common->get_video_resolution($videoUrl, $ffmpeg_path, $videoExt);
                $video_Resolution = array();
                $videoWidth = 0;
                $videoHeight = 0;
                if (isset($output['width']) && isset($output['height']) && $output['height'] != '' && $output['width'] != '') {
                    $videoHeight = $output['height'];
                    $videoWidth = $output['width'];
                    if ($videoHeight >= 2160) {
                        $GLOBALS['default_video_resolution'] = 2160;
                    } else if ($videoHeight >= 1440) {
                        $GLOBALS['default_video_resolution'] = 1440;
                    } else if ($videoHeight >= 1080) {
                        $GLOBALS['default_video_resolution'] = 1080;
                    } else if ($videoHeight >= 720) {
                        $GLOBALS['default_video_resolution'] = 720;
                    } else if ($videoHeight >= 480) {
                        $GLOBALS['default_video_resolution'] = 480;
                    } else if ($videoHeight >= 360) {
                        $GLOBALS['default_video_resolution'] = 360;
                    } else if ($videoHeight >= 240) {
                        $GLOBALS['default_video_resolution'] = 240;
                    } else if ($videoHeight >= 144) {
                        $GLOBALS['default_video_resolution'] = 144;
                    } else if ($videoHeight < 144) {
                        $GLOBALS['default_video_resolution'] = $videoHeight;
                    }

                    if ($GLOBALS['default_video_resolution'] != 0 && $videoWidth != 0) {
                        if ($GLOBALS['default_video_resolution'] >= 144) {
                            $video_Resolution = $videoResolution[$GLOBALS['default_video_resolution']];
                        } else {
                            $video_Resolution = Yii::app()->common->getvideoResolutionForTheVideo($GLOBALS['video_resolution'], $GLOBALS['default_video_resolution']);
                        }

                        $s3shFolderPath = 'conversionScriptDRM';
                        $conversioServerffmpegPath = '/root/bin/ffmpeg';
                        $threads = 28;
                        //Create shell script
                        $file = date("Y_m_d_H_i_s") . '_' . $movieID . HOST_IP . '.sh';
                        $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
                        $cf = 'echo "${file%???}"';
                        $originalFileConversion = '';
                        $moveFile = '';
                        $checkFileExistIfCondition = '';
                        $checkFileExistElseCondition = '';
                        $encryptionKey = '';
                        $contentKey = '';
                        $originalFileConversion .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/original \n";
                        $originalFileConversion .= "wget " . $videoUrl . " \n";
                        $deletefileFromFolder = '';
                        $s3GetObjForDelete = Yii::app()->common->connectToAwsS3($val['studio_id']);
                        $folderPathh = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/';
                        $response = $s3GetObjForDelete->listObjects(array('Bucket' => $bucketName, 'MaxKeys' => 1000, 'Prefix' => $folderPathh));
                        $getfiles = $response->getPath('Contents');
                        foreach ($getfiles as $filePathh) {
                            if (($filePathh['Key'] != $folderPathh . $videoName) && ($filePathh['Key'] != $folderPathh)) {
                                $deletefileFromFolder .= "/usr/local/bin/s3cmd  del --config /usr/local/bin/." . $s3cfg . "  " . "s3://" . $bucketName . "/" . $filePathh['Key'] . " \n";
                            }
                        }
                        $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/original/" . $videoName . " " . $bucket_url . $videoName . " \n";
                        $checkFileExistIfCondition .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                        $encryptionKey = Yii::app()->general->moreRand(32);
                        $contentKey = Yii::app()->general->moreRand(32);
                        $updateUrl .="MUVIMUVI" . $encryptionKey;
                        $updateUrl .="MUVIMUVI" . $contentKey;
                        $copyCmd = '';
                        $conversion_cmd = "";
                        $fragmentCmd = "";
                        $checkFrangmentedFileExistIf = '';
                        $checkFrangmentedFileElseCondition = '';
                        $packaginofVideo = '';
                        $packaginofVideoHls = '';
                        $multiBitrateMlv = "";
                        $conversion_cmd .= $conversioServerffmpegPath . ' -y -i /var/www/html/' . $videoFolder . '/' . $movieID . '/original/' . $videoName . ' -c:v libx264 -x264opts \'keyint=24:min-keyint=24:no-scenecut\' -movflags faststart -threads ' . $threads . ' /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                        $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$videoOnlyName.".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$videoOnlyName . ".mlv \n";
                        $newVideoName = '';
                        //Video Fragment
                        $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4 \n";
                        $checkFrangmentedFileExistIf .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mf4';
                        //For multiple type conversion and Fragment and package
                        $packaginofVideo .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                        $packaginofVideoHls .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/hls \n";

                        $packaginofVideo .= "/var/www/html/bento4New/bin/mp4dash --encryption-key=" . $encryptionKey . ":" . $contentKey . " --marlin --widevine --widevine-header=provider:intertrust#content_id:2a --playready --playready-header=https://expressplay-licensing.axprod.net/LicensingService.ashx " . $videoOnlyName . ".mf4";
                        $packaginofVideoHls .= "/var/www/html/bento4New/bin/mp4hls --encryption-mode=SAMPLE-AES --encryption-key=" . $encryptionKey . $contentKey . " --encryption-iv-mode=fps --encryption-key-format=com.apple.streamingkeydelivery --encryption-key-uri=skd://expressplay_token /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4";
                        foreach ($video_Resolution as $key => $value) {
                            $videoFileName = $GLOBALS['video_screen_resolution'][$value];
                            $screenWidth = round(($value / $videoHeight) * $videoWidth);
                            if ($screenWidth % 2 != 0) {
                                $screenWidth = $screenWidth + 1;
                            }

                            $bandwidth = @$GLOBALS['video_bandwidth'][$value];
                            $bandwidthAverage = @$GLOBALS['video_bandwidth_average'][$value];
                            $moveFile .= "sed -i '/height=\"" . $value . "\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"" . $bandwidth . "\"/' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                            $moveFile .= "sed -i '/RESOLUTION=" . $screenWidth . "x" . $value . "/s/BANDWIDTH=*[0-9]*/BANDWIDTH=" . $bandwidthAverage . "/g; /RESOLUTION=" . $screenWidth . "x" . $value . "/s/AVERAGE-BANDWIDTH=*[0-9]*/AVERAGE-BANDWIDTH=" . $bandwidth . "/g;' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";

                            $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                            $mlvFileName = $videoOnlyName . '_' . $videoFileName . '.mlv';
                            $videoFrangmentName = $videoOnlyName . '_' . $videoFileName . '.mf4';
                            $conversion_cmd.= " -c:v libx264 -x264opts 'keyint=24:min-keyint=24:no-scenecut' -movflags faststart -vf \"scale=" . $screenWidth . ":" . $value . "\" -threads " . $threads . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName;
                            $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $newVideoName;
                            $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$newVideoName." /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$mlvFileName . " \n";
                            $checkMlv .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/output/'.$mlvFileName;
                            $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName . " \n";
                            $checkFrangmentedFileExistIf .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoFrangmentName;
                            $packaginofVideo .= " " . $videoFrangmentName;
                            $packaginofVideoHls .= " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName;
                            if(@$drmMobileDisable[$val['studio_id']] == 1){
                                $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " " . $bucket_url . " \n";
                                $isMobileDRMDisable ++;
                            }
                        }
                        $packaginofVideo .= " \n";
                        $packaginofVideo .=$multiBitrateMlv;
                        $packaginofVideoHls .= " \n";
                        $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/dash+xml' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd " . $bucket_url . " \n";
                        $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                        $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/output/ " . $bucket_url . " \n";
                        $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/x-mpegURL' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 " . $bucket_url . "hls/ \n";
                        $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";
                        $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/ " . $bucket_url . "hls/ \n";
                        $moveFile .= $deletefileFromFolder;
                        $checkFrangmentedFileExistIf .= " ]\n\nthen\n\n";
                        $checkFileExistIfCondition .= " ]\n\nthen\n\n";

                        $checkFileExistElseCondition .="\n\nelse\n\n";
                        $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                        $checkFileExistElseCondition .="# remove directory \n";
                        $checkFileExistElseCondition .="rm -rf /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                        $checkFileExistElseCondition .="# remove the process copy file \n";
                        $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/conversionCompleted/" . $file . " \n";
                        $checkFileExistElseCondition .="# remove the process file \n";
                        $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/" . $file . " \n";
                        $checkFileExistElseCondition .="fi\n\n";

                        $file_data = "file=`echo $0`\n" .
                                "cf='" . $file . "'\n\n" .
                                "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                "then\n\n" .
                                "echo \"$file is running\"\n\n" .
                                "else\n\n" .
                                "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                "# create directory\n" .
                                "mkdir /var/www/html/$videoFolder/$movieID\n" .
                                "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                                "mkdir /var/www/html/$videoFolder/$movieID/original\n" .
                                "chmod 0777 /var/www/html/$videoFolder/$movieID/original\n" .
                                "mkdir /var/www/html/$videoFolder/$movieID/hls\n" .
                                "chmod 0777 /var/www/html/$videoFolder/$movieID/hls\n" .
                                "# convertion command\n" .
                                $originalFileConversion .
                                "\n" .
                                "# For multiple video resolution\n" .
                                $conversion_cmd .
                                "\n\n\n# Check all the video file's are created\n" .
                                $checkFileExistIfCondition .
                                "\n\n\n# Fragement the file\n" .
                                $fragmentCmd .
                                "\n\n\n# Check all the Fragemented file's are created \n" .
                                $checkFrangmentedFileExistIf .
                                "\n\n\n# Packaging the file using encryption key and content key for multi drm \n" .
                                $packaginofVideo .
                                $packaginofVideoHls .
                                "if [ -s /var/www/html/$videoFolder/$movieID/output/stream.mpd$checkMlv ]\n\n" .
                                "then\n\n" .
                                "\n# upload to s3\n" .
                                $moveFile .
                                "# update query\n" .
                                "curl $updateUrl\n" .
                                "# remove directory\n" .
                                "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                "# remove the process copy file\n" .
                                "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                "# remove the process file\n" .
                                "rm /var/www/html/$shFolder/$file\n" .
                                $checkFileExistElseCondition .
                                $checkFileExistElseCondition .
                                $checkFileExistElseCondition .
                                "fi";
                        fwrite($handle, $file_data);
                        fclose($handle);
                        //Uploading conversion script from local to s3 
                        $filePath = $_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file;
                        if ($s3->upload($conversionBucket, $s3shFolderPath . "/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                            echo "Sh file created successfully";
                            $streamData[$keyval]['shfileCreated'] = 'yes';
                            if (unlink($filePath)) {
                                $epData = movieStreams::model()->findByPk($movieID);
                                $epData->encoding_start_time = date('Y-m-d H:i:s');
                                $epData->is_multibitrate_offline = 1;
                                if($isMobileDRMDisable == 0){
                                    $epData->is_mobile_drm_disable = 0;
                                } else{
                                    $epData->is_mobile_drm_disable = 1;
                                }
                                $return = $epData->save();
                            }
                        }
                    }
                }
            }
            print_r($streamData);
        }
    }

    function actionVideoConversionDRMVideoReencodeSH() {
        $this->layout = false;
        $condition = '';
        $videoFolder = 'video';
        $shFolder = 'progressDRM';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
        } else if (HOST_IP == '52.0.64.95') {
            $videoFolder = 'staging/video';
            $shFolder = 'staging/progress';
            $conversionBucket = 'stagingstudio';
        }
        if (!isset($_REQUEST['studio_id'])) {
            echo "Please provide studio_id";
            exit;
        }
        $studio_id = $_REQUEST['studio_id'];
        $condition = ' AND ms.studio_id =' . $studio_id;
        if(@$_REQUEST['movie_stream_id'] != ''){
            $condition = ' AND ms.id =' . $_REQUEST['movie_stream_id'];
        }
        $drmData = Yii::app()->db->createCommand()
                ->select('config_value,studio_id')
                ->from('studio_config')
                ->where('config_key = "drm_enable" AND studio_id = ' . $studio_id)
                ->queryAll();
        if (@$drmData[0]['config_value'] != 1) {
            echo "DRM is not enabled for this studio";
            exit;
        }
        $drmMobileDisable = array();
        $drmMobileData = Yii::app()->db->createCommand()
                    ->select('config_value,studio_id')
                    ->from('studio_config')
                    ->where('config_key = "drm_mobile_disable" AND studio_id = '. $studioIds)
                    ->queryAll();
        foreach($drmMobileData as $drmMobileDataKey => $drmMobileDataVal) {
            $drmMobileDisable[$drmMobileDataVal['studio_id']] = $drmMobileDataVal['config_value'];
        }
        $streamData = Yii::app()->db->createCommand()
                ->select('ms.id AS stream_id,ms.movie_id,s.id as studio_id,s.priority,s.new_cdn_users,s.s3bucket_id,s.studio_s3bucket_id,ms.full_movie,f.uniq_id,ms.video_management_id')
                ->from('movie_streams ms,studios s,films f ')
                ->where('ms.studio_id = s.id AND f.id = ms.movie_id AND ms.is_demo="0" AND  ms.is_converted =1 AND ms.encryption_key !="" AND ms.content_key !="" AND ms.full_movie!=\'\'' . $condition)
                ->queryAll();
        if ($streamData) {
            $videoResolutionData = VideoResolution::model()->findAll();
            $videoResolution = array();
            if ($videoResolutionData) {
                foreach ($videoResolutionData as $videoResolutionDatakey => $videoResolutionDatavalue) {
                    $videoResolution[$videoResolutionDatavalue['resolution']] = explode(",", $videoResolutionDatavalue['resolution_to_be_encoded']);
                }
            }
            $lastbucketid = '';
            $lastStudiobucketid = '';
            $bucketName = '';
            foreach ($streamData AS $keyval => $val) {
                $streamData[$keyval]['shfileCreated'] = 'No';
                $videoName = $val['full_movie'];
                $movieID = $val['stream_id'];
                $isMobileDRMDisable = 0;
                if ($lastbucketid != $val['s3bucket_id'] && $lastStudiobucketid != $val['studio_s3bucket_id']) {
                    $bucketInfo = Yii::app()->common->getBucketInfo("", $val['studio_id']);
                    $bucketName = $bucketInfo['bucket_name'];
                    $s3url = $bucketInfo['s3url'];
                    $s3cfg = $bucketInfo['s3cmd_file_name'];
                }
                $lastbucketid = $val['s3bucket_id'];
                $lastStudiobucketid = $val['studio_s3bucket_id'];
                $updateUrl = BASE_URL . "/conversion/UpdateMovieInfoDRM?stream_id=" . $val['stream_id'] . "MUVIMUVI" . $val['uniq_id'];
                $urlForVideoNotConverted = "";
                $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                ));
                $vidoInfo = new SplFileInfo($videoName);
                $videoExt = $vidoInfo->getExtension();
                $folderPath = Yii::app()->common->getFolderPath($val['new_cdn_users'], $val['studio_id']);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $bucketHttpUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/';
                $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "uploads/movie_stream/full_movie/" . $movieID . "/";
                $videoUrl = $bucketHttpUrl . $videoName;
                $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
                $output = Yii::app()->common->get_video_resolution($videoUrl, $ffmpeg_path, $videoExt);
                $video_Resolution = array();
                $videoWidth = 0;
                $videoHeight = 0;
                if (isset($output['width']) && isset($output['height']) && $output['height'] != '' && $output['width'] != '') {
                    $videoHeight = $output['height'];
                    $videoWidth = $output['width'];
                    if ($videoHeight >= 2160) {
                        $GLOBALS['default_video_resolution'] = 2160;
                    } else if ($videoHeight >= 1440) {
                        $GLOBALS['default_video_resolution'] = 1440;
                    } else if ($videoHeight >= 1080) {
                        $GLOBALS['default_video_resolution'] = 1080;
                    } else if ($videoHeight >= 720) {
                        $GLOBALS['default_video_resolution'] = 720;
                    } else if ($videoHeight >= 480) {
                        $GLOBALS['default_video_resolution'] = 480;
                    } else if ($videoHeight >= 360) {
                        $GLOBALS['default_video_resolution'] = 360;
                    } else if ($videoHeight >= 240) {
                        $GLOBALS['default_video_resolution'] = 240;
                    } else if ($videoHeight >= 144) {
                        $GLOBALS['default_video_resolution'] = 144;
                    } else if ($videoHeight < 144) {
                        $GLOBALS['default_video_resolution'] = $videoHeight;
                    }

                    if ($GLOBALS['default_video_resolution'] != 0 && $videoWidth != 0) {
                        if ($GLOBALS['default_video_resolution'] >= 144) {
                            $video_Resolution = $videoResolution[$GLOBALS['default_video_resolution']];
                        } else {
                            $video_Resolution = Yii::app()->common->getvideoResolutionForTheVideo($GLOBALS['video_resolution'], $GLOBALS['default_video_resolution']);
                        }

                        $s3shFolderPath = 'conversionScriptDRM';
                        $conversioServerffmpegPath = '/root/bin/ffmpeg';
                        $threads = 28;
                        //Create shell script
                        $file = date("Y_m_d_H_i_s") . '_' . $movieID . HOST_IP . '.sh';
                        $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
                        $cf = 'echo "${file%???}"';
                        $originalFileConversion = '';
                        $moveFile = '';
                        $checkFileExistIfCondition = '';
                        $checkFileExistElseCondition = '';
                        $encryptionKey = '';
                        $contentKey = '';
                        $originalFileConversion .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/original \n";
                        $originalFileConversion .= "wget " . $videoUrl . " \n";
                        $checkFileExistIfCondition .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                        $encryptionKey = Yii::app()->general->moreRand(32);
                        $contentKey = Yii::app()->general->moreRand(32);
                        $updateUrl .="MUVIMUVI" . $encryptionKey;
                        $updateUrl .="MUVIMUVI" . $contentKey;
                        $copyCmd = '';
                        $conversion_cmd = "";
                        $fragmentCmd = "";
                        $checkFrangmentedFileExistIf = '';
                        $checkFrangmentedFileElseCondition = '';
                        $packaginofVideo = '';
                        $packaginofVideoHls = '';
                        $multiBitrateMlv = "";
                        $conversion_cmd .= $conversioServerffmpegPath . ' -y -i /var/www/html/' . $videoFolder . '/' . $movieID . '/original/' . $videoName . ' -c:v libx264 -x264opts \'keyint=24:min-keyint=24:no-scenecut\' -movflags faststart -threads ' . $threads . ' /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mp4';
                        $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$videoOnlyName.".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$videoOnlyName . ".mlv \n";
                        $newVideoName = '';
                        //Video Fragment
                        $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mp4 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4 \n";
                        $checkFrangmentedFileExistIf .= 'if [ -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoOnlyName . '.mf4';
                        //For multiple type conversion and Fragment and package
                        $packaginofVideo .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                        $packaginofVideoHls .= "cd /var/www/html/" . $videoFolder . "/" . $movieID . "/hls \n";

                        $packaginofVideo .= "/var/www/html/bento4New/bin/mp4dash --encryption-key=" . $encryptionKey . ":" . $contentKey . " --marlin --widevine --widevine-header=provider:intertrust#content_id:2a --playready --playready-header=https://expressplay-licensing.axprod.net/LicensingService.ashx " . $videoOnlyName . ".mf4";
                        $packaginofVideoHls .= "/var/www/html/bento4New/bin/mp4hls --encryption-mode=SAMPLE-AES --encryption-key=" . $encryptionKey . $contentKey . " --encryption-iv-mode=fps --encryption-key-format=com.apple.streamingkeydelivery --encryption-key-uri=skd://expressplay_token /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoOnlyName . ".mf4";
                        foreach ($video_Resolution as $key => $value) {
                            $videoFileName = $GLOBALS['video_screen_resolution'][$value];
                            $screenWidth = round(($value / $videoHeight) * $videoWidth);
                            if ($screenWidth % 2 != 0) {
                                $screenWidth = $screenWidth + 1;
                            }

                            $bandwidth = @$GLOBALS['video_bandwidth'][$value];
                            $bandwidthAverage = @$GLOBALS['video_bandwidth_average'][$value];
                            $moveFile .= "sed -i '/height=\"" . $value . "\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"" . $bandwidth . "\"/' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                            $moveFile .= "sed -i '/RESOLUTION=" . $screenWidth . "x" . $value . "/s/BANDWIDTH=*[0-9]*/BANDWIDTH=" . $bandwidthAverage . "/g; /RESOLUTION=" . $screenWidth . "x" . $value . "/s/AVERAGE-BANDWIDTH=*[0-9]*/AVERAGE-BANDWIDTH=" . $bandwidth . "/g;' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";

                            $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                            $mlvFileName = $videoOnlyName . '_' . $videoFileName . '.mlv';
                            $videoFrangmentName = $videoOnlyName . '_' . $videoFileName . '.mf4';
                            $conversion_cmd.= " -c:v libx264 -x264opts 'keyint=24:min-keyint=24:no-scenecut' -movflags faststart -vf \"scale=" . $screenWidth . ":" . $value . "\" -threads " . $threads . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName;
                            $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $newVideoName;
                            $multiBitrateMlv .= "/var/www/html/bento4New/bin/mp4encrypt --method OMA-PDCF-CTR --key 1:".$encryptionKey.":random --key 2:".$encryptionKey.":random --property 1:ContentId:".$contentKey." --property 2:ContentId:".$contentKey." /var/www/html/" . $videoFolder . "/" . $movieID . "/".$newVideoName." /var/www/html/" . $videoFolder . "/" . $movieID . "/output/".$mlvFileName . " \n";
                            $checkMlv .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/output/'.$mlvFileName;
                            $fragmentCmd .= "/var/www/html/bento4New/bin/mp4fragment --fragment-duration 4000 /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName . " \n";
                            $checkFrangmentedFileExistIf .= ' -a -s /var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoFrangmentName;
                            $packaginofVideo .= " " . $videoFrangmentName;
                            $packaginofVideoHls .= " /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoFrangmentName;
                            if(@$drmMobileDisable[$val['studio_id']] == 1){
                                $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $newVideoName . " " . $bucket_url . " \n";
                                $isMobileDRMDisable ++;
                            }
                        }
                        $packaginofVideo .= " \n";
                        $packaginofVideo .=$multiBitrateMlv;
                        $packaginofVideoHls .= " \n";
                        $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/dash+xml' /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd " . $bucket_url . " \n";
                        $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/output/stream.mpd \n";
                        $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/output/ " . $bucket_url . " \n";
                        $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/x-mpegURL' /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 " . $bucket_url . "hls/ \n";
                        $moveFile .= "rm /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/master.m3u8 \n";
                        $moveFile .= "/usr/local/bin/s3cmd put  --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/hls/output/ " . $bucket_url . "hls/ \n";
                        $checkFrangmentedFileExistIf .= " ]\n\nthen\n\n";
                        $checkFileExistIfCondition .= " ]\n\nthen\n\n";

                        $checkFileExistElseCondition .="\n\nelse\n\n";
                        $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                        $checkFileExistElseCondition .="# remove directory \n";
                        $checkFileExistElseCondition .="rm -rf /var/www/html/" . $videoFolder . "/" . $movieID . " \n";
                        $checkFileExistElseCondition .="# remove the process copy file \n";
                        $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/conversionCompleted/" . $file . " \n";
                        $checkFileExistElseCondition .="# remove the process file \n";
                        $checkFileExistElseCondition .="rm /var/www/html/" . $shFolder . "/" . $file . " \n";
                        $checkFileExistElseCondition .="fi\n\n";

                        $file_data = "file=`echo $0`\n" .
                                "cf='" . $file . "'\n\n" .
                                "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                "then\n\n" .
                                "echo \"$file is running\"\n\n" .
                                "else\n\n" .
                                "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                "# create directory\n" .
                                "mkdir /var/www/html/$videoFolder/$movieID\n" .
                                "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                                "mkdir /var/www/html/$videoFolder/$movieID/original\n" .
                                "chmod 0777 /var/www/html/$videoFolder/$movieID/original\n" .
                                "mkdir /var/www/html/$videoFolder/$movieID/hls\n" .
                                "chmod 0777 /var/www/html/$videoFolder/$movieID/hls\n" .
                                "# convertion command\n" .
                                $originalFileConversion .
                                "\n" .
                                "# For multiple video resolution\n" .
                                $conversion_cmd .
                                "\n\n\n# Check all the video file's are created\n" .
                                $checkFileExistIfCondition .
                                "\n\n\n# Fragement the file\n" .
                                $fragmentCmd .
                                "\n\n\n# Check all the Fragemented file's are created \n" .
                                $checkFrangmentedFileExistIf .
                                "\n\n\n# Packaging the file using encryption key and content key for multi drm \n" .
                                $packaginofVideo .
                                $packaginofVideoHls .
                                "if [ -s /var/www/html/$videoFolder/$movieID/output/stream.mpd$checkMlv ]\n\n" .
                                "then\n\n" .
                                "\n# upload to s3\n" .
                                $moveFile .
                                "# update query\n" .
                                "curl $updateUrl\n" .
                                "# remove directory\n" .
                                "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                                "# remove the process copy file\n" .
                                "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                "# remove the process file\n" .
                                "rm /var/www/html/$shFolder/$file\n" .
                                $checkFileExistElseCondition .
                                $checkFileExistElseCondition .
                                $checkFileExistElseCondition .
                                "fi";
                        fwrite($handle, $file_data);
                        fclose($handle);
                        //Uploading conversion script from local to s3 
                        $filePath = $_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file;
                        if ($s3->upload($conversionBucket, $s3shFolderPath . "/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                            echo "Sh file created successfully";
                            $streamData[$keyval]['shfileCreated'] = 'yes';
                            if (unlink($filePath)) {
                                $epData = movieStreams::model()->findByPk($movieID);
                                $epData->encoding_start_time = date('Y-m-d H:i:s');
                                $epData->is_multibitrate_offline = 1;
                                if($isMobileDRMDisable == 0){
                                    $epData->is_mobile_drm_disable = 0;
                                } else{
                                    $epData->is_mobile_drm_disable = 1;
                                }
                                $return = $epData->save();
                            }
                        }
                    }
                }
            }
            print_r($streamData);
        }
    }

    #Action for video could not encoded

    function actionUpdateMovieVideoNotConverted() {
        $this->layout = false;
        $v = explode('MUVIMUVI', $_REQUEST['stream_id']);
        print_r($v);
        $videoDetails = Yii::app()->db->createCommand()
                ->select('s.s3bucket_id as bucketId,s.id as studioId,s.new_cdn_users as newUser,ms.full_movie as fileName')
                ->from('movie_streams ms,studios s,films f ')
                ->where('ms.studio_id = s.id AND f.id = ms.movie_id AND ms.id=' . $v[0] . ' AND f.uniq_id="' . $v[1] . '"')
                ->queryAll();
        if ($videoDetails) {
            $bucketInfo = Yii::app()->common->getBucketInfo("", $videoDetails[0]['studioId']);
            $bucket = $bucketInfo['bucket_name'];
            $s3 = Yii::app()->common->connectToAwsS3($videoDetails[0]['studioId']);
            $folderPath = Yii::app()->common->getFolderPath("", $videoDetails[0]['studioId']);
            $signedFolderPath = $folderPath['signedFolderPath'];
            $s3dir = $signedFolderPath . 'uploads/movie_stream/full_movie/' . $v[0] . '/' . $videoDetails[0]['fileName'];
            $videoToBeDeleted = "s3://" . $bucket . "/" . $s3dir;
            $data['url'] = 'http://' . $bucket . '.' . $bucketInfo['s3url'] . '/' . $s3dir;
            $epData = movieStreams::model()->findByPk($v[0]);
            $epData->is_converted = 2;
            $epData->full_movie = '';
            $epData->video_management_id = 0;
            $epData->encode_fail_time = date('Y-m-d H:i:s');
            $return = $epData->save();
            $encodingData = Encoding::model()->findByAttributes(array('movie_stream_id' => $v[0], 'studio_id' => $videoDetails[0]['studioId']));
            if ($encodingData) {
                $encodingLog = new EncodingLog;
                $encodingLog->studio_id = $encodingData->studio_id;
                $encodingLog->movie_stream_id = $encodingData->movie_stream_id;
                $encodingLog->size = $encodingData->size;
                $encodingLog->upload_time = $encodingData->upload_time;
                $encodingLog->encoding_server_id = $encodingData->encoding_server_id;
                $encodingLog->encoding_start_time = $encodingData->encoding_start_time;
                $encodingLog->expected_end_time = $encodingData->expected_end_time;
                $encodingLog->encoding_end_time = date('Y-m-d H:i:s');
                $encodingLog->status = "Failed";
                $encodingLog->save();
                $encodingData->delete();
            }
            if ($v[3] == 0) {
                $this->createUploadVideoSHFOrVideoGallery($data, "", $videoDetails[0]['fileName'], $videoDetails[0]['studioId'], $videoToBeDeleted);
            } else {
                $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir
                ));
            }
        }
    }

    /**
     * @method public MailRegardingVideoEncodeing() Email to studio user about video encoding.
     * @author SKP<support@muvi.com>
     * @return Email
     */
    function actionMailRegardingVideoEncodeing() {
        $this->layout = false;
        $studioIds = Yii::app()->db->createCommand()
                ->select('distinct(ms.studio_id) as studioID')
                ->from('movie_streams ms')
                ->where('(ms.is_converted=2 or ms.is_converted=1) and ms.mail_sent_for_video =0')
                ->queryAll();
        if (count($studioIds) > 0) {
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/mailForvideoencoding.log', "a+");
            fwrite($fp, "Studio Dt: " . date('d-m-Y H:i:s') . '-----' . print_r($studioIds, true));
            foreach ($studioIds as $studioIdKey => $studioId) {
                $videoDetails = Yii::app()->db->createCommand()
                        ->select('ms.mail_sent_for_video,ms.id as stream_id,ms.series_number as series,ms.is_converted as isConverted,ms.is_episode as episode,ms.episode_title as eps_title,ms.episode_number as epi_number,f.name as film_name')
                        ->from('movie_streams ms,films f')
                        ->where(' f.id = ms.movie_id AND (ms.is_converted=2 or ms.is_converted=1) and ms.mail_sent_for_video =0 AND ms.studio_id=' . $studioId['studioID'])
                        ->queryAll();
                if (count($videoDetails) > 0) {
                    $total_videos = count($videoDetails);
                    fwrite($fp, "Run for " . $total_videos . "Videos Dt: " . date('d-m-Y H:i:s') . '-----' . print_r($videoDetails, true));
                    $userDetails = Yii::app()->db->createCommand()
                            ->select('u.email as user_email,u.first_name as user_first_name')
                            ->from('user u')
                            ->where('u.studio_id=' . $studioId['studioID'])
                            ->queryAll();
                    if (count($userDetails) > 0) {
                        fwrite($fp, "User Dt: " . date('d-m-Y H:i:s') . '-----' . print_r($userDetails, true));
                        $mailcontent = '<p>Dear ' . $userDetails[0]['user_first_name'] . ',</p>';
                        $host_ip = Yii::app()->params['host_ip'];

                        if (in_array(HOST_IP, $host_ip)) {
                            $adminGroupEmail = array($userDetails[0]['user_email']);
                            $cc = array();
                            $bcc = array('rasmi@muvi.com', 'srutikant@muvi.com', 'mohan@muvi.com');
                        } else {
                            $adminGroupEmail = array('rasmi@muvi.com', 'srutikant@muvi.com');

                            $mailcontent.= '<p><strong>Note: This is the staging server.</strong></p>';
                            $cc = array();
                            $bcc = array('rasmi@muvi.com', 'srutikant@muvi.com', 'mohan@muvi.com');
                        }
                        $fileEncoded = array();
                        $fileNotEncoded = array();
                        foreach ($videoDetails as $videoDetailsKey => $videoDetailsVal) {
                            $fileName = $videoDetailsVal['film_name'];
                            if ($videoDetailsVal['episode'] == 1) {
                                if ($videoDetailsVal['series'] != '') {
                                    $fileName .= ' - Season ' . $videoDetailsVal['series'];
                                }
                                if ($videoDetailsVal['epi_number'] != '') {
                                    $fileName .= ' - Episode ' . $videoDetailsVal['epi_number'];
                                }
                                $fileName .= ' - ' . $videoDetailsVal['eps_title'];
                            }
                            if ($videoDetailsVal['isConverted'] == 1) {
                                $fileEncoded[] = $fileName;
                            } else if ($videoDetailsVal['isConverted'] == 2) {
                                $fileNotEncoded[] = $fileName;
                            }
                            $epData = movieStreams::model()->findByPk($videoDetailsVal['stream_id']);
                            $epData->mail_sent_for_video = 1;
                            $return = $epData->save();
                            if ($return) {
                                fwrite($fp, "Database Updated Dt: " . date('d-m-Y H:i:s') . '-----' . $videoDetailsVal['stream_id']);
                            } else {
                                fwrite($fp, "Database not Updated Dt: " . date('d-m-Y H:i:s') . '-----' . $videoDetailsVal['stream_id']);
                            }
                        }
                        if (!empty($fileEncoded)) {
                            $mailcontent .= '<p>Following content are successfully encoded and now available on your website and mobile apps.</p>';
                            $mailcontent .= '<ul>';
                            foreach ($fileEncoded as $fileEncodedKey => $fileEncodedVal) {
                                $mailcontent .= '<li>' . $fileEncodedVal . '</li>';
                            }
                            $mailcontent .= '</ul>';
                        }
                        if (!empty($fileNotEncoded)) {
                            $mailcontent .= '<p>Following content are failed encoding. There maybe something wrong with the video file. Please check and contact your Muvi representative in case of any questions.</p>';
                            $mailcontent .= '<ul>';
                            foreach ($fileNotEncoded as $fileNotEncodedKey => $fileNotEncodedVal) {
                                $mailcontent .= '<li>' . $fileNotEncodedVal . '</li>';
                            }
                            $mailcontent .= '</ul>';
                        }
                        $mailcontent .= "<p>Regards,<br/>Team Muvi</p>";
                        $site_url = Yii::app()->getBaseUrl(true);
                        $logo = EMAIL_LOGO;
                        $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
                        $params = array(
                            'website_name' => 'Muvi',
                            'mailcontent' => $mailcontent,
                            'logo' => $logo
                        );

                        $fromMail = 'studio@muvi.com';

                        $adminSubject = "Video upload status";


                        $template_name = 'video_conversion';
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('video_upload_status', $studioId['studioID']);
                        $obj = new Controller();

                        $to = $adminGroupEmail;
                        $subject = $adminSubject;
                        $from = $fromMail;

                        Yii::app()->theme = 'bootstrap';
                        $thtml = Yii::app()->controller->renderPartial('//email/video_conversion', array('params' => $params), true);


                        if ($isEmailToStudio) {
                            $returnVal = $obj->sendmailViaAmazonsdk($thtml, $subject, $to, $from, $cc, $bcc, '', 'Muvi');
                        }
                    }
                }
            }
        }
    }

    function actionTodaysChargeToUser() {
        //Find out all active studios
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            $dbcon = Yii::app()->db;

            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $is_process = 0;

                //Filter Customer, Lead and Demo studios otherwise no transaction process
                if ($studio_id > 0 && $studio->is_subscribed == 1 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Customer
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Lead
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 1) {//Default
                    $is_process = 1;
                }

                if (intval($is_process)) {
                    //Find out Pyament gateway detail
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

                    $activate = 0; //No plan and payment gateway set by studio
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $studio_payment_gateway = $plan_payment_gateway['gateways'];
                        $this->setPaymentGatwayVariable($studio_payment_gateway);
                        $activate = 1;
                    }

                    $cond = '';
                    $cond1 = " AND DATE_FORMAT(us.start_date, '%Y-%m-%d') = '" . gmdate('Y-m-d') . "'";
                    if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                        $cond = " AND u.id=" . $_REQUEST['user'];
                        $cond1 = " ";
                    }

                    $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*
                         FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND 
                        u.studio_id=us.studio_id AND us.status=1 AND us.payment_status=0 " . $cond1 . "), sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND 
                        u.is_deleted='0' AND u.studio_id=" . $studio_id . $cond . " AND sci.user_id=us.user_id AND 
                        sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id GROUP BY u.id";

                    $users = $dbcon->createCommand($sql)->queryAll();

                    //Find out all active subscribed sdk user with card detail
                    if (isset($users) && !empty($users)) {
                        foreach ($users as $key1 => $user) {
                            //Find out manual payment gateway which run by cron job
                            $gateway_info = StudioPaymentGateways::model()->findByPk($user['studio_payment_gateway_id']);

                            if (intval($activate) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
                                $gateway = PaymentGateways::model()->findByAttributes(array('id' => $this->GATEWAY_ID[$gateway_info->short_code], 'status' => 1, 'is_payment_gateway' => 1, 'is_automated' => 0));

                                //Find out all active subscribed sdk user with card detail
                                if (isset($gateway) && !empty($gateway)) {
                                    $uniqid = Yii::app()->common->generateUniqNumber();
                                    $sub = new UserSubscription;
                                    $subsc = $sub->findByPk($user['user_subscription_id']);
                                    $subsc->payment_key = $uniqid;
                                    $subsc->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * 
     * Process new tranasction
     * @param
     * @return 
     * @author Sunil Kund <sunil@muvi.com>
     */
    function actionProcessTransactionsOfUsers() {
        $cond = '';
        $limit = 50;

        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
            $cond = " AND u.id=" . $_REQUEST['user'];

            $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,sp.name AS plan_name,us.profile_id as profile_id  
            FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 ),
            sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id AND us.is_3rd_party_coupon = 0 GROUP BY u.id LIMIT 0, " . $limit;
        } else {
            $sql = "SELECT u.*, u.id AS user_id, us.*,us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,sp.name AS plan_name,us.profile_id as profile_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND 
            u.studio_id=us.studio_id AND us.status=1 AND us.payment_status=0 AND us.payment_key !='' AND DATE_FORMAT(us.start_date, '%Y-%m-%d') = '" . gmdate('Y-m-d') . "'),
            sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id  AND us.is_3rd_party_coupon = 0 GROUP BY u.id LIMIT 0, " . $limit;
        }

        $dbcon = Yii::app()->db;
        $users = $dbcon->createCommand($sql)->queryAll();

        if (isset($users) && !empty($users)) {
            foreach ($users as $key1 => $user) {
                $studio_id = $user['studio_id'];
                $studio = Studios::model()->findByPk($studio_id);

                $user_id = $user['user_id'];
                $user['studio_name'] = $studio->name;

                $currency = Currency::model()->findByPk($user['currency_id']);
                $user['currency_id'] = $currency->id;
                $user['currency_code'] = $currency->code;
                $user['currency_symbol'] = $currency->symbol;

                //Find out Pyament gateway detail
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $studio_payment_gateway = $plan_payment_gateway['gateways'];
                    $this->setPaymentGatwayVariable($studio_payment_gateway);
                }
                $gateway_info = StudioPaymentGateways::model()->findByPk($user['studio_payment_gateway_id']);
                $user['gateway_code'] = $gateway_info->short_code;
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $trans_data = $payment_gateway::processTransactions($user);
                $is_paid = $trans_data['is_success'];

                $sub = new UserSubscription;
                $subsc = $sub->findByPk($user['user_subscription_id']);
                $bill_amount = $user['amount'];

                if (intval($is_paid)) {
                    //Getting Ip address
                    $ip_address = Yii::app()->request->getUserHostAddress();

                    //Save a transaction detail
                    $transaction = new Transaction;
                    $transaction->user_id = $user_id;
                    $transaction->studio_id = $studio_id;
                    $transaction->plan_id = $user['plan_id'];
                    $transaction->currency_id = $user['currency_id'];
                    $transaction->transaction_date = new CDbExpression("NOW()");
                    $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    $transaction->transaction_status = $trans_data['transaction_status'];
                    $transaction->invoice_id = $trans_data['invoice_id'];
                    $transaction->order_number = $trans_data['order_number'];
                    $transaction->dollar_amount = $trans_data['dollar_amount'];
                    $transaction->amount = $trans_data['paid_amount'];
                    $transaction->response_text = $trans_data['response_text'];
                    $transaction->subscription_id = $user['user_subscription_id'];
                    $transaction->ip = $ip_address;
                    $transaction->created_date = new CDbExpression("NOW()");
                    $transaction->save();

                    //Update next subscription date according to studio subscription plan
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $user['frequency'] . ' ' . strtolower($user['recurrence']) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));

                    $paid_amount = $trans_data['paid_amount'];

                    if (abs(($bill_amount - $paid_amount) / $paid_amount) < 0.00001) {
                        $isPaid = 1;
                    } else {
                        $isPaid = 0;
                        $subsc->payment_status = 1; //Partial Payment
                        $subsc->partial_failed_date = gmdate('Y-m-d h:i:s'); //Partial Payment date
                        $subsc->partial_failed_due = number_format((float) ($bill_amount - $paid_amount), 2, '.', ''); //Remaining due on partial payment
                    }

                    $subsc->start_date = $start_date;
                    $subsc->end_date = $end_date;
                    $subsc->payment_key = '';
                    $subsc->save();

                    if (intval($isPaid)) {
                        //Get invoice detail
                        $trans_amt = Yii::app()->common->formatPrice($trans_data['paid_amount'], $user['currency_id'], 1);
                        $user['amount'] = $trans_amt;
                        $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $trans_data['invoice_id']);
                        //Send an email with invoice detail
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_renew', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_monthly_payment', $user_id, '', $trans_amt, $user['plan_name']);
                        }
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                        }
                    }
                } else {
                    $teModel = New TransactionErrors;
                    $teModel->studio_id = $studio_id;
                    $teModel->user_id = $user_id;
                    $teModel->plan_id = $user['plan_id'];
                    $teModel->subscription_id = $user['user_subscription_id'];
                    $teModel->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    $teModel->response_text = $trans_data['response_text'];
                    $teModel->created_date = new CDbExpression("NOW()");
                    $teModel->save();

                    $subsc->payment_key = '';
                    $subsc->payment_status = 2; //Payment failed
                    $subsc->partial_failed_date = gmdate('Y-m-d h:i:s'); //Payment failed date
                    $subsc->partial_failed_due = $bill_amount; //Due on failed payment
                    $subsc->save();

                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                    if ($isEmailToStudio) {
                        $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                    }
                }
            }
        }
    }
 /**
     * 
     * Process new tranasction
     * @param
     * @return 
     * @author Asis Barik <asis@muvi.com>
     */
    function actionProcessTransactionsOfUsersTest() {
        $cond = '';
        $limit = 50;

        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
            $cond = " AND u.id=" . $_REQUEST['user'];

            $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,sp.name AS plan_name,us.profile_id as profile_id  
            FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 ),
            sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id AND us.is_3rd_party_coupon = 0 LIMIT 0, " . $limit;
        } else {
            $sql = "SELECT u.*, u.id AS user_id, us.*,us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,sp.name AS plan_name,us.profile_id as profile_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND 
            u.studio_id=us.studio_id AND us.status=1 AND us.payment_status=0 AND us.payment_key !='' AND DATE_FORMAT(us.start_date, '%Y-%m-%d') = '" . gmdate('Y-m-d') . "'),
            sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.is_cancelled=0  AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id  AND us.is_3rd_party_coupon = 0  LIMIT 0, " . $limit;
        }

        $dbcon = Yii::app()->db;
        $users = $dbcon->createCommand($sql)->queryAll();

        if (isset($users) && !empty($users)) {
            foreach ($users as $key1 => $user) {
                $studio_id = $user['studio_id'];
                $studio = Studios::model()->findByPk($studio_id);

                $user_id = $user['user_id'];
                $is_subscription_bundle=$user['is_subscription_bundle'];
                
                if($user['use_discount'] == 1){
                    $user['amount'] = $user['discount_amount'];
                }
                $user['studio_name'] = $studio->name;

                $currency = Currency::model()->findByPk($user['currency_id']);
                $user['currency_id'] = $currency->id;
                $user['currency_code'] = $currency->code;
                $user['currency_symbol'] = $currency->symbol;

                //Find out Pyament gateway detail
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $studio_payment_gateway = $plan_payment_gateway['gateways'];
                    $this->setPaymentGatwayVariable($studio_payment_gateway);
                }
                if(abs($user['amount']) > 0.01){
                $gateway_info = StudioPaymentGateways::model()->findByPk($user['studio_payment_gateway_id']);
                $user['gateway_code'] = $gateway_info->short_code;
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $trans_data = $payment_gateway::processTransactions($user);
                $is_paid = $trans_data['is_success'];
                }

                if(abs($user['amount']) == 0.00 && $user['use_discount'] == 1){
                    $trans_data['transaction_status'] = 'succeeded';
                    $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['dollar_amount'] = 0;
                    $trans_data['paid_amount'] = 0;
                    $trans_data['response_text'] = '';
                    $is_paid = 1;
                }

                $sub = new UserSubscription;
                $subsc = $sub->findByPk($user['user_subscription_id']);
                $bill_amount = $user['amount'];
                 
                if (intval($is_paid)) {
                    //Getting Ip address
                    $ip_address = Yii::app()->request->getUserHostAddress();

                    //Save a transaction detail
                    $transaction = new Transaction;
                    $transaction->user_id = $user_id;
                    $transaction->studio_id = $studio_id;
                    $transaction->plan_id = $user['plan_id'];
                    $transaction->currency_id = $user['currency_id'];
                    $transaction->transaction_date = new CDbExpression("NOW()");
                    $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    $transaction->transaction_status = $trans_data['transaction_status'];
                    $transaction->invoice_id = $trans_data['invoice_id'];
                    $transaction->order_number = $trans_data['order_number'];
                    $transaction->dollar_amount = $trans_data['dollar_amount'];
                    $transaction->amount = $trans_data['paid_amount'];
                    $transaction->response_text = $trans_data['response_text'];
                    $transaction->subscription_id = $user['user_subscription_id'];
                    if($is_subscription_bundle=1){
                     $transaction->transaction_type = 7;
                    }
                     if($is_subscription_bundle=0){
                     $transaction->transaction_type = 1;
                    }
                    $transaction->ip = $ip_address;
                    $transaction->created_date = new CDbExpression("NOW()");
                    $transaction->save();

                    //Update next subscription date according to studio subscription plan
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $user['frequency'] . ' ' . strtolower($user['recurrence']) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));

                    $paid_amount = $trans_data['paid_amount'];

                    if (abs(($bill_amount - $paid_amount) / $paid_amount) < 0.00001) {
                        $isPaid = 1;
                        $subsc->discount_amount = 0.00;
                        $subsc->use_discount = 0;
                    }else if(abs($user['amount']) == 0.00 && $user['use_discount'] == 1){
                        $isPaid = 1;
                        $subsc->discount_amount = 0.00;
                        $subsc->use_discount = 0;
                    } else {
                        $isPaid = 0;
                        $subsc->payment_status = 1; //Partial Payment
                        $subsc->partial_failed_date = gmdate('Y-m-d h:i:s'); //Partial Payment date
                        $subsc->partial_failed_due = number_format((float) ($bill_amount - $paid_amount), 2, '.', ''); //Remaining due on partial payment
                    }
                    $subsc->count_discount = $subsc->count_discount+1;
                    $subsc->start_date = $start_date;
                    $subsc->end_date = $end_date;
                    $subsc->payment_key = '';
                    $subsc->save();

                    if (intval($isPaid)) {
                        //Get invoice detail
                        $trans_amt = Yii::app()->common->formatPrice($trans_data['paid_amount'], $user['currency_id'], 1);
                        $user['amount'] = $trans_amt;
                        $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $trans_data['invoice_id']);
                        //Send an email with invoice detail
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_renew', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_monthly_payment', $user_id, '', $trans_amt, $user['plan_name']);
                        }
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                        }
                    }
                } else {
                    $teModel = New TransactionErrors;
                    $teModel->studio_id = $studio_id;
                    $teModel->user_id = $user_id;
                    $teModel->plan_id = $user['plan_id'];
                    $teModel->subscription_id = $user['user_subscription_id'];
                    $teModel->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    $teModel->response_text = $trans_data['response_text'];
                    $teModel->created_date = new CDbExpression("NOW()");
                    $teModel->save();

                    $subsc->payment_key = '';
                    $subsc->payment_status = 2; //Payment failed
                    $subsc->partial_failed_date = gmdate('Y-m-d h:i:s'); //Payment failed date
                    $subsc->partial_failed_due = $bill_amount; //Due on failed payment
                    $subsc->save();

                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                    if ($isEmailToStudio) {
                        $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                    }
                }
            }
        }
    }
    /*

     * for testing of subscription and subscription with limited content
     * by sunil nayak
     * 
     *      */
    function actionTodaysChargeToUserTest() {
        //Find out all active studios
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            $dbcon = Yii::app()->db;

            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $is_process = 0;

                //Filter Customer, Lead and Demo studios otherwise no transaction process
                if ($studio_id > 0 && $studio->is_subscribed == 1 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Customer
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Lead
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 1) {//Default
                    $is_process = 1;
                }

                if (intval($is_process)) {
                    //Find out Pyament gateway detail
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

                    $activate = 0; //No plan and payment gateway set by studio
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $studio_payment_gateway = $plan_payment_gateway['gateways'];
                        $this->setPaymentGatwayVariable($studio_payment_gateway);
                        $activate = 1;
                    }

                    $cond = '';
                    $cond1 = " AND DATE_FORMAT(us.start_date, '%Y-%m-%d') = '" . gmdate('Y-m-d') . "'";
                    if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                        $cond = " AND u.id=" . $_REQUEST['user'];
                        $cond1 = " ";
                    }

                    $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*
                         FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND 
                        u.studio_id=us.studio_id AND us.status=1 AND us.payment_status=0 " . $cond1 . "), sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND 
                        u.is_deleted='0' AND u.studio_id=" . $studio_id . $cond . " AND sci.user_id=us.user_id AND 
                        sci.studio_id=us.studio_id  AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id";

                    $users = $dbcon->createCommand($sql)->queryAll();

    
                    //Find out all active subscribed sdk user with card detail
                    if (isset($users) && !empty($users)) {
                        foreach ($users as $key1 => $user) {
                            //Find out manual payment gateway which run by cron job
                            $gateway_info = StudioPaymentGateways::model()->findByPk($user['studio_payment_gateway_id']);
                            if (intval($activate) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
                                $gateway = PaymentGateways::model()->findByAttributes(array('id' => $this->GATEWAY_ID[$gateway_info->short_code], 'status' => 1, 'is_payment_gateway' => 1, 'is_automated' => 0));

                                //Find out all active subscribed sdk user with card detail
                                if (isset($gateway) && !empty($gateway)) {
                                    $uniqid = Yii::app()->common->generateUniqNumber();
                                    $sub = new UserSubscription;
                                    $subsc = $sub->findByPk($user['user_subscription_id']);
                                    $subsc->payment_key = $uniqid;
                                    $subsc->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    
    
    function actionPaymentFailedReminderToUsers() {
        //Find User whose payment has failed or partially done
        $sql = "SELECT tot.* FROM (SELECT u.id AS user_id, u.studio_id, en.studio_id AS email_studio_id, en.created,  
        DATEDIFF(DATE('" . date('Y-m-d') . "'), DATE(DATE_FORMAT(us.partial_failed_date, '%Y-%m-%d'))) AS days FROM sdk_users u 
        LEFT JOIN user_subscriptions us ON (u.id=us.user_id) LEFT JOIN email_notification_user en 
        ON (u.studio_id = en.studio_id AND u.id = en.user_id AND en.type = 1) WHERE us.status=1 AND us.payment_status!=0 AND 
        (DATEDIFF(DATE('" . date('Y-m-d') . "'), DATE(DATE_FORMAT(us.partial_failed_date, '%Y-%m-%d'))) = 2 OR DATEDIFF(DATE('" . date('Y-m-d') . "'), DATE(DATE_FORMAT(us.partial_failed_date, '%Y-%m-%d'))) = 4) 
        ORDER BY en.created DESC) AS tot GROUP BY tot.user_id";

        $dbcon = Yii::app()->db;
        $users = $dbcon->createCommand($sql)->queryAll();

        if (isset($users) && !empty($users)) {
            foreach ($users as $key => $value) {
                $is_email = 1;
                $days = $value['days'];

                if ((($days == 2) || ($days == 4)) && gmdate('Y-m-d') == gmdate("Y-m-d", strtotime($value['created']))) {
                    $is_email = 0;
                }

                if (intval($is_email)) {
                    $studio_id = $value['studio_id'];
                    $user_id = $value['user_id'];

                    //Keeping log in email tables after firing an email
                    $enModel = New EmailNotificationUser;
                    $enModel->studio_id = $studio_id;
                    $enModel->user_id = $user_id;
                    $enModel->type = 1;
                    $enModel->created = new CDbExpression("NOW()");
                    $enModel->save();

                    if ($days == 2) {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'first_reminder_card_fail');
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'second_reminder_card_fail');
                    }
                }
            }
        }
    }

    function actionCancelAccountofUsers() {
        $cond = '';
        if (isset($_REQUEST['user']) && trim($_REQUEST['user'])) {
            $cond = " AND u.id=" . trim($_REQUEST['user']);
        }
        $sql = "SELECT u.*, us.*, u.id AS user_id,sci.id AS card_id, DATEDIFF(DATE('" . date('Y-m-d H:i:s') . "'), DATE(us.partial_failed_date)) AS days 
        FROM sdk_users u LEFT JOIN (user_subscriptions us, sdk_card_infos sci) ON (u.id=us.user_id AND sci.user_id=us.user_id AND sci.studio_id=us.studio_id AND sci.is_cancelled=0) 
        WHERE us.status=1 AND us.payment_status!=0 AND DATEDIFF(DATE('" . date('Y-m-d H:i:s') . "'), DATE(us.partial_failed_date)) >= 6" . $cond;

        $dbcon = Yii::app()->db;
        $users = $dbcon->createCommand($sql)->queryAll();

        if (isset($users) && !empty($users)) {
            foreach ($users as $key => $value) {
                $days = $value['days'];
                if (intval($days) >= 6) {
                    $studio_id = $value['studio_id'];
                    $user_id = $value['user_id'];

                    $usersub = new UserSubscription;
                    $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));

                    //Getting card detail
                    $card = new SdkCardInfos;
                    $card = $card->findByPk($value['card_id']);
                    $gateway_info = StudioPaymentGateways::model()->findByPk($value['studio_payment_gateway_id']);
                    //Cancel customer's account from payment gateway
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $studio_payment_gateway = $plan_payment_gateway['gateways'];
                        $this->setPaymentGatwayVariable($studio_payment_gateway);

                        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                        $payment_gateway = new $payment_gateway_controller();
                        $data = $payment_gateway::cancelCustomerAccount($usersub, $card);
                    }

                    //Delete card detail of user
                    SdkCardInfos::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                        ':studio_id' => $studio_id,
                        ':user_id' => $user_id
                    ));

                    //Inactivate user subscription
                    $usersub->status = 0;
                    $usersub->cancel_date = new CDbExpression("NOW()");
                    $usersub->cancel_reason_id = '';
                    $usersub->cancel_note = 'Subscription cancelled on payment failure';
                    $usersub->payment_status = 0;
                    $usersub->partial_failed_date = '';
                    $usersub->partial_failed_due = 0;
                    $usersub->save();

                    //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
                    $usr = new SdkUser;
                    $usr = $usr->findByPk($user_id);
                    $usr->is_deleted = 1;
                    $usr->deleted_at = new CDbExpression("NOW()");
                    $usr->save();

                    //Send an email to user
                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'cancellation_card_fail');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('cancellation_card_fail', $studio_id);
                    if ($isEmailToStudio) {
                        $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_subscription_cancelled_on_payment_failed', $user_id, '', '', $value['card_id']);
                    }
                }
            }
        }
    }

    function actionDeleteUsersAccount() {
        $cond = '';
        if (isset($_REQUEST['user']) && trim($_REQUEST['user'])) {
            $cond = " AND u.id=" . trim($_REQUEST['user']);
        }
        $sql = "SELECT u.*, us.*, u.id AS user_id, DATEDIFF(DATE('" . date('Y-m-d H:i:s') . "'), DATE(u.deleted_at)) AS days 
        FROM sdk_users u LEFT JOIN user_subscriptions us ON (u.id=us.user_id AND u.studio_id=us.studio_id) 
        WHERE us.status=0 AND u.status=1 AND u.is_deleted=1 AND DATEDIFF(DATE('" . date('Y-m-d H:i:s') . "'), DATE(u.deleted_at)) >= 30" . $cond;

        $dbcon = Yii::app()->db;
        $users = $dbcon->createCommand($sql)->queryAll();

        if (isset($users) && !empty($users)) {
            foreach ($users as $key => $user) {
                $studio_id = $user['studio_id'];
                $user_id = $user['user_id'];

                //Delete all Email Notification User
                EmailNotificationUser::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                    ':studio_id' => $studio_id,
                    ':user_id' => $user_id
                ));

                //Delete all Transaction Errors
                TransactionErrors::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                    ':studio_id' => $studio_id,
                    ':user_id' => $user_id
                ));

                //Delete all User Subscription
                UserSubscription::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                    ':studio_id' => $studio_id,
                    ':user_id' => $user_id
                ));

                //Delete all PPV Subscription
                PpvSubscription::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                    ':studio_id' => $studio_id,
                    ':user_id' => $user_id
                ));

                //Delete all card infos of users
                $cards = SdkCardInfos::model()->findAllByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));

                if (isset($cards) && !empty($cards)) {
                    foreach ($cards as $key => $card) {
                        //Delete from payment gateway end if account exists
                        if (trim($card->profile_id) && intval($card->gateway_id)) {
                            $payment_gateway_short_code = PaymentGateways::model()->findByPk($card->gateway_id)->short_code;

                            $payment_gateway_controller = 'Api' . $payment_gateway_short_code . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $data = $payment_gateway::cancelCustomerAccount($card, $card);
                        }

                        SdkCardInfos::model()->deleteByPk($card->id);
                    }
                }

                //Deactivate to user
                $usr = SdkUser::model()->findByPk($user_id);
                $usr->email = 'deleted_' . $usr->email;
                $usr->status = 0;
                $usr->is_deleted = 1;
                $usr->deleted_at = gmdate('Y-m-d H:i:s');
                $usr->save();
            }
        }
    }

    /**
     * 
     * Process new tranasction
     * @param
     * @return 
     * @author Sunil Kund <sunil@muvi.com>
     */
    function actionProcessTransactionsOfCustomers_back() {
        exit;
        //Find out all active studios
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            $dbcon = Yii::app()->db;

            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $is_process = 0;
                //Filter Customer, Lead and Demo studios otherwise no transaction process
                if ($studio_id > 0 && $studio->is_subscribed == 1 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Customer
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Lead
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 1) {//Default
                    $is_process = 1;
                }
                if (intval($is_process)) {
                    //Find out Pyament gateway detail
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

                    $activate = 0; //No plan and payment gateway set by studio
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $studio_payment_gateway = $plan_payment_gateway['gateways'][0];
                        $this->setPaymentGatwayVariable($studio_payment_gateway);
                        $activate = 1;
                    }
                    //Find out manual payment gateway which run by cron job
                    if (intval($activate) && $studio_payment_gateway->short_code != 'manual') {

                        $gateway = PaymentGateways::model()->findByAttributes(array('id' => $studio_payment_gateway->gateway_id, 'status' => 1, 'is_payment_gateway' => 1, 'is_automated' => 0));

                        //Find out all active subscribed sdk user with card detail
                        if (isset($gateway) && !empty($gateway)) {
                            $cond = '';
                            if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                                $cond = " AND u.id=" . $_REQUEST['user'];
                            }

                            $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*
                                 FROM sdk_users u LEFT JOIN user_subscriptions us ON (u.id=us.user_id AND 
                                u.studio_id=us.studio_id AND us.status=1), sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND 
                                u.is_deleted='0' AND u.studio_id=" . $studio_id . $cond . " AND sci.user_id=us.user_id AND 
                                sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id";
                            /*
                             * AND sci.id=us.card_id 
                             * 
                              $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id,
                              sp.*, sp.id AS subscription_plan_id FROM sdk_users u LEFT JOIN
                              (user_subscriptions us, sdk_card_infos sci, subscription_plans sp)
                              ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 AND sci.id=us.card_id AND sci.user_id=us.user_id AND sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id)
                              WHERE u.status=1 AND u.is_deleted='0' AND u.studio_id=".$studio_id.$cond;
                             * 
                             */

                            $users = $dbcon->createCommand($sql)->queryAll();
                            //echo "<pre>";print_r($users);exit;

                            if (isset($users) && !empty($users)) {

                                foreach ($users as $key1 => $user) {
                                    $user['currency_code'] = 'USD'; //Hard coded for the time baing as multiple currency is not implemented in subscription plans
                                    $user_id = $user['user_id'];

                                    if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio']) && isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                                        $tranasction_date = gmdate('Y-m-d');
                                    } else {
                                        //Check on which date transaction is process according to prepaid or postpaid set by studio admin
                                        $tranasction_date = '';
                                        if (intval($user['is_post_paid'])) {
                                            $tranasction_date = gmdate('Y-m-d', strtotime($user['end_date']));
                                        } else {
                                            $tranasction_date = gmdate('Y-m-d', strtotime($user['start_date']));
                                        }
                                    }


                                    //If tranasction date is today date, then process transaction
                                    //echo $tranasction_date;exit;

                                    if (gmdate('Y-m-d') == $tranasction_date) {
                                        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY . 'Controller';
                                        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                                        $payment_gateway = new $payment_gateway_controller();
                                        $trans_data = $payment_gateway::processTransactions($user);
                                        //echo '<pre>';print_r($trans_data);exit;
                                        $is_paid = $trans_data['is_success'];
                                        //Save a transaction detail
                                        if (intval($is_paid)) {

                                            //Getting Ip address
                                            $ip_address = Yii::app()->request->getUserHostAddress();

                                            $transaction = new Transaction;
                                            $transaction->user_id = $user_id;
                                            $transaction->studio_id = $studio_id;
                                            $transaction->plan_id = $user['plan_id'];
                                            $transaction->transaction_date = new CDbExpression("NOW()");
                                            $transaction->payment_method = $this->PAYMENT_GATEWAY;
                                            $transaction->transaction_status = $trans_data['transaction_status'];
                                            $transaction->invoice_id = $trans_data['invoice_id'];
                                            $transaction->order_number = $trans_data['order_number'];
                                            $transaction->dollar_amount = $trans_data['amount'];
                                            $transaction->amount = $trans_data['amount'];
                                            $transaction->response_text = $trans_data['response_text'];
                                            $transaction->subscription_id = $user['user_subscription_id'];
                                            $transaction->ip = $ip_address;
                                            $transaction->created_date = new CDbExpression("NOW()");
                                            $transaction->save();

                                            //Update next subscription date according to studio subscription plan
                                            //$start_date = Date('Y-m-d H:i:s');//, strtotime("+ 1 days")

                                            if (intval($user['is_post_paid'])) {
                                                $start_date = Date('Y-m-d H:i:s', strtotime("+1 days"));
                                                $time = strtotime($start_date);
                                                $recurrence_frequency = $user['frequency'] . ' ' . strtolower($user['recurrence']) . "s";
                                                $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                                $end_date = Date('Y-m-d H:i:s', strtotime($end_date . "-1 days"));
                                            } else {
                                                $start_date = Date('Y-m-d H:i:s');
                                                $time = strtotime($start_date);
                                                $recurrence_frequency = $user['frequency'] . ' ' . strtolower($user['recurrence']) . "s";
                                                $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                                $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                                            }

                                            $subsc = new UserSubscription;
                                            $subsc = $subsc->findByPk($user['user_subscription_id']);
                                            $subsc->start_date = $start_date;
                                            $subsc->end_date = $end_date;
                                            $subsc->save();

                                            //Get invoice detail
                                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $trans_data['invoice_id']);

                                            //Send an email with invoice detail
                                            $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name);
                                            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/paymentgateway.log', "a+");
                                            fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . '-----Api' . $this->PAYMENT_GATEWAY . 'Controller/n' . $file_name);
                                            //exit;
                                        } else {
                                            $teModel = New TransactionErrors;
                                            $teModel->studio_id = $studio_id;
                                            $teModel->user_id = $user_id;
                                            $teModel->plan_id = $user['plan_id'];
                                            $teModel->subscription_id = $user['user_subscription_id'];
                                            $teModel->payment_method = $this->PAYMENT_GATEWAY;
                                            $teModel->response_text = $trans_data['response_text'];
                                            $teModel->created_date = new CDbExpression("NOW()");
                                            $teModel->save();

                                            //Send an error email to admin :TBD
                                            //$this->errorMessageMailToSales($req);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function actionCancelAccount() {
        $sql = "SELECT s.*, u.id AS user_id, u.first_name, u.email, u.phone_no FROM studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 AND u.is_active=1) WHERE s.status=1 AND s.is_subscribed=0 AND s.is_deleted=0 AND s.is_default=0";
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {
            $sql = "SELECT s.*, u.id AS user_id, u.first_name, u.email, u.phone_no FROM studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 AND u.is_active=1) WHERE s.status=1 AND s.is_subscribed=0 AND s.is_deleted=0 AND s.is_default=0 AND s.id=" . $_REQUEST['studio'];
        }
        $dbcon = Yii::app()->db;
        $studios = $dbcon->createCommand($sql)->queryAll();
        //print '<pre>';print count($studios);print_r($studios);exit;

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $value) {
                $studio_id = $value['id'];
                $user_id = $value['user_id'];

                $start_date = date("Y-m-d", strtotime($value['start_date']));
                $today_date = gmdate('Y-m-d');

                //If trial period has finished, which has set at the time of registration
                if (strtotime($today_date) > strtotime($start_date)) {
                    $req['name'] = $value['first_name'];
                    $req['email'] = $value['email'];
                    $req['phone'] = $value['phone_no'];
                    $req['companyname'] = $value['name'];
                    $req['domain'] = 'http://' . $value['domain'];
                    $req['cancel_reason'] = 'Free Trial Expired';
                    $req['custom_notes'] = '';

                    //Insert new record in StudioCancelReason table
                    $scrModel = New StudioCancelReason;
                    $scrModel->studio_id = $studio_id;
                    $scrModel->reason = $req['cancel_reason'];
                    $scrModel->custom_notes = $req['custom_notes'];
                    $scrModel->ip = CHttpRequest::getUserHostAddress();
                    $scrModel->created_by = 0;
                    $scrModel->created_date = gmdate('Y-m-d');
                    $scrModel->save();

                    //Update card to inactive mode
                    $sql = "UPDATE card_infos SET is_cancelled=2, cancelled_date=NOW() WHERE studio_id=" . $studio_id . " AND is_cancelled!=2";
                    $con = Yii::app()->db;
                    $ciData = $con->createCommand($sql)->execute();

                    //Update studio pricing plans
                    $sqlplan = "UPDATE studio_pricing_plans SET status=0, updated_by=0, updated=NOW() WHERE studio_id=" . $studio_id;
                    $planData = $con->createCommand($sqlplan)->execute();

                    //Delete all pause billing
                    PauseBilling::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Deactivate all users like admin and members
                    $sql_usr = "UPDATE user SET is_active=0 WHERE studio_id=" . $studio_id . " AND id !=" . $user_id;
                    $con->createCommand($sql_usr)->execute();

                    //Update studio status with hold period
                    $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');

                    $studio = Studios::model()->findByPk($studio_id);
                    $studio->status = 4;
                    $studio->is_subscribed = 0;
                    $studio->start_date = Date('Y-m-d H:i:s');
                    $studio->end_date = Date('Y-m-d H:i:s', strtotime("+{$config[0]['value']} days"));
                    $studio->save();

                    //Send cancellation email to admin and user
                    //Yii::app()->email->cancelSubscriptionMailToSales($req);
                    Yii::app()->email->cancelSubscriptionMailToUser($req, '');
                }
            }
        }
    }

    function actionDeleteAccount() {        
        $sql = "SELECT users.* FROM (SELECT s.*, u.id AS user_id, u.first_name, u.email FROM studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 AND u.is_active=1) WHERE s.status=4 AND s.is_subscribed=0 AND s.is_deleted=0 AND s.is_default=0) AS users WHERE user_id IS NOT NULL";
		if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {
			$sql = "SELECT s.*, u.id AS user_id, u.first_name, u.email FROM studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 AND u.is_active=1) WHERE s.status=4 AND s.is_subscribed=0 AND s.is_deleted=0 AND s.is_default=0 AND s.id=" . $_REQUEST['studio'];
		}
		$dbcon = Yii::app()->db;
		$studios = $dbcon->createCommand($sql)->queryAll();
		//print '<pre>';print count($studios);print_r($studios);exit;
		if (isset($studios) && !empty($studios)) {
			foreach ($studios as $key => $values) {
				$studio_id = $values['id'];
				$user_id = $values['user_id'];
				$today_date = gmdate('Y-m-d');
				if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {
					$end_date = date('Y-m-d', strtotime($today_date . ' -1 day'));
				} else {
					$end_date = date("Y-m-d", strtotime($values['end_date']));
				}
				if (intval($studio_id) && intval($user_id)) {
					//If onhold period has finished, which has set at the time of cancellation account
					if (strtotime($today_date) > strtotime($end_date)) {

						//call delete_studio_team_web to delete all records for web team
						$this->delete_studio_team_web($values);

						//call delete_studio_team_player
						$this->delete_studio_team_player($values);
						//call delete_studio_team_billing() to delete all records for billing team
						$this->delete_studio_team_billing($values);
						//Delete Manage Cookie
						StudioManageCookie::model()->deleteAll('studio_id = :studio_id', array(
							':studio_id' => $studio_id
						));

						$studio = Studios::model()->findByPk($studio_id);
						
						//Delete film,trailer,poster and top banner data
						$film = Film::model()->findAll(array("condition" => "studio_id = " . $studio_id));
						if (isset($film) && !empty($film)) {
							foreach ($film as $key => $flm) {
								$this->removeMovieTrailerEpisode($studio, $flm->id);
							}
						}
						$this->delete_studio_team_content($values);
                        //Delete from studio_s3buckets
                        StudioS3buckets::model()->deleteAll('id = :id', array(':id' => $studio['studio_s3bucket_id']));	

						//Deactivate studio user and delete all users like admin and members
						$sql_usr = "DELETE FROM user WHERE studio_id = " . $studio_id . " AND id !=" . $user_id;
						$dbcon->createCommand($sql_usr)->execute();
						User::model()->updateByPk($user_id, array('is_active' => 0));
						//Delete all studio content
						if (intval($studio->is_live)) {
							$req = array();
							$req['studio_name'] = ucwords($studio > name);
							$req['domain'] = $studio->domain;
							Yii::app()->email->cancelSSLEmailToSystemAdmin($req);
						}
						//Deactivate to studio
						$studio->status = 0;
						$studio->is_subscribed = 0;
						$studio->deleted_date = gmdate('Y-m-d H:i:s');
						$studio->is_deleted = 1;
						$studio->cdn_status = 0;
						$studio->signed_url = '';
						$studio->unsigned_url = '';
						$studio->video_url = '';
						$studio->studio_s3bucket_id = '';
						$studio->save();
					}
				}
			}
		}
	}
	
/**
    * @method delete_studio_teamcontent() Delete all the content related to content team
     * @param Array
     * @author Gayadhar<support@muvi.com>
     * return boolean value
     */
   function delete_studio_team_content($values = array()) {
	   $studio_id = $values['id'];
		$dbcon = Yii::app()->db;
		// Delete Physical goods content  
		$filmsSql = "SELECT GROUP_CONCAT(id) as ids FROM films WHERE studio_id = " . $studio_id;
		$fdata = $dbcon->createCommand($filmsSql)->queryRow();

		$mssql = "SELECT GROUP_CONCAT(id) as ids FROM movie_streams WHERE is_episode =1 AND studio_id = {$studio_id}";
		$msdata = $dbcon->createCommand($mssql)->queryRow();

		$csql = "SELECT GROUP_CONCAT(id) as ids FROM celebrities WHERE  studio_id = {$studio_id}";
		$cdata = $dbcon->createCommand($csql)->queryRow();

		if (isset($fdata['ids']) && $fdata['ids']) {
			$delPosterSql = "DELETE FROM posters WHERE object_id IN(" . $fdata['ids'] . ") AND object_type='films'";
			$dbcon->createCommand($delPosterSql)->execute();

			$delmovieCasts = "DELETE FROM movie_casts WHERE movie_id IN(" . $fdata['ids'] . ") ";
			$dbcon->createCommand($delmovieCasts)->execute();

			$delTrailer = "DELETE FROM movie_trailer WHERE movie_id IN(" . $fdata['ids'] . ")";
			$dbcon->createCommand($delTrailer)->execute();
		}
		if (isset($msdata['ids']) && $msdata['ids']) {
			$delPosterSql1 = "DELETE FROM posters WHERE object_id IN(" . $msdata['ids'] . ") AND object_type='moviestream'";
			$dbcon->createCommand($delPosterSql1)->execute();
		}
		if (isset($cdata['ids']) && $cdata['ids']) {
			$delPosterSql2 = "DELETE FROM posters WHERE object_id IN(" . $cdata['ids'] . ") AND object_type = 'celebrity'";
			$dbcon->createCommand($delPosterSql2)->execute();
		}
		$DeleteDataFromtable = array(
			"MenuItem", "SearchLog", "ContentCategories", "UrlRouting", "Menu", "StudioContentType", "Celebrity", "OuathRegistration", "MovieTag", "Livestream", "LivestreamUsers", "FilmCustomMetadata", "Film", "AudioManagement", "movieStreams");

		$exts = $dbcon->createCommand('SELECT id FROM extensions WHERE name=\'Muvi Kart\' AND status=1')->queryRow();
		$ext = StudioExtension::model()->findByAttributes(array('studio_id' => $studio_id, 'extension_id' => $exts['id']));
		if ($ext && $ext->status) {
			$prodsql = "SELECT GROUP_CONCAT(id) AS ids FROM pg_product where studio_id={$studio_id}";
			$productIds = $dbcon->createCommand($prodsql)->queryRow();
			if (isset($productIds['ids']) && $productIds['ids']) {
				$dbcon->createCommand("DELETE FROM  pg_product_image WHERE product_id IN(" . @$productIds['ids'] . ")")->execute();
				$dbcon->createCommand("DELETE FROM  pg_product_content WHERE product_id IN(" . @$productIds['ids'] . ")")->execute();
				$dbcon->createCommand("DELETE FROM  pg_cds_stock_status WHERE product_id IN(" . @$productIds['ids'] . ")")->execute();
				$dbcon->createCommand("DELETE FROM  geoblockpgcontent WHERE pg_product_id IN(" . @$productIds['ids'] . ")")->execute();
			}
			$sql_orderdetails = "SELECT GROUP_CONCAT(id) AS ids FROM pg_order where studio_id=" . $studio_id;
			$order_ids = $dbcon->createCommand($sql_orderdetails)->queryRow();
			if (isset($order_ids['ids']) && $order_ids['ids']) {
				$delete_order_details = "DELETE FROM pg_order_details WHERE order_id IN(" . @$order_ids['ids'] . ")";
				$dbcon->createCommand($sql_orderdetails)->execute();
			}
			$pgArr = array(
				"PGVideo", "PGShippingAddress", 'PGDefaultShippingCost', 'PGMinimumOrderFreeShipping',
				"PGShippinCost", "PGSettings", "PGSavedAddress", "PGProduct", "PGPreorderCategory", "PGOrderStatusLog", "PGOrder",
				"PGMultiCurrency", "PGCartLog", "PGCart", "PGCustomMetadata"
			);
			$DeleteDataFromtable = array_merge($DeleteDataFromtable, $pgArr);
		}


		foreach ($DeleteDataFromtable as $deletedata) {
			$deletedata::model()->deleteAll('studio_id = :studio_id', array(
				':studio_id' => $studio_id
			));
		}
		return true;
	}
   /**
     * @param Array
     * @author sunil kumar nayak <suniln@muvi.com>
     * return boolean value
     */
   function delete_studio_team_billing($values = array()) {
        $studio_id = $values['id'];
        $DeleteDataFromtable = array(
            "CustomReport", "ReportTemplate", "StudioManageCoupon", "StudioPricingPlan", "StudioPaymentGateways", "SdkUser",
            "CardInfos", "CardErrors", "StudioCancelReason", "SeoInfo", "FreeMonth", "StudioVisitor", "StudioLoginHistory", 
            "Visitorlog", "LoginHistory", "BufferLogs", "CancelReason", "Coupon", "DevhourPurchaseInfo", "CustomReportColumn",
            "DevHourUsedInfos", "EmailNotification", "EmailNotificationUser", "FreeContent", "MonetizationMenuSettings",
            "NotificationTemplates", "NotificationSetting", "OuathRegistration", "PpvBuy", "PpvPlans", "PpvSubscription",
            "PpvValidity", "StudioCountryRestriction", "StudioCurrencySupport", "StudioPricingPlan",
            "TransactionErrors", "UserSubscription","SdkCardInfos"
            );
        foreach ($DeleteDataFromtable as $deletedata) {
            $deletedata::model()->deleteAll('studio_id = :studio_id', array(
                ':studio_id' => $studio_id
            ));
        }
        return true;
    }

    /**
     * @param Array
     * @author Ratikanta <ratikanta@muvi.com>
     * return boolean value
     */
    function delete_studio_team_web($values=array()){
        $studio_id = $values['id'];
        $user_id = $values['user_id'];

        $end_date = date("Y-m-d", strtotime($values['end_date']));
        $today_date = gmdate('Y-m-d');        
        //Delete all StudioBanner
        StudioBanner::model()->deleteAll('studio_id = :studio_id', array(
            ':studio_id' => $studio_id
        ));

        //Delete all BannerText
        BannerText::model()->deleteAll('studio_id = :studio_id', array(
            ':studio_id' => $studio_id
        ));

        //Delete all FeaturedSection
        $section = new FeaturedSection;
        $sections = $section->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id_seq ASC'));
        if (count($sections) > 0) {
            foreach ($sections as $section) {
                $featureds = Yii::app()->common->getFeaturedContents($section->id);
                if (count($featureds) > 0) {
                    foreach ($featureds as $featured) {
                        $featured->delete();
                    }
                }
                $section->delete();
            }
        } 
        
        //Delete all studio content
        $studio = Studios::model()->findByPk($studio_id);      
        //Delete folder from theme and theme backup
        $theme = $studio->theme;
        if (trim($theme)) {
            $preview_theme = $theme . '_preview';
            $theme_path = ROOT_DIR . 'themes/' . $theme;
            $preview_theme_path = ROOT_DIR . 'themes/' . $preview_theme;
            if (file_exists($theme_path)) {
                Yii::app()->common->rrmdir($theme_path);
            }

            if (file_exists($preview_theme_path)) {
                Yii::app()->common->rrmdir($preview_theme_path);
            }
        }    
        
        return true;
    }    
    
    
    /**
     * @param Array
     * @author prakash <prakash@muvi.com>
     * return boolean value
     */
    function delete_studio_team_player($studio=array()){
        if(count($studio)>0){
            $studio_id = ($studio['id']>0)?$studio['id']:0;
            $user_id = $studio['user_id'];
            //Delete from embed_url_restiction table
            EmbedUrlRestriction::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
            //Delete from watermark_player
            WatermarkPlayer::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
            //Delete from livestream 
            $liveStreamData = Livestream::model()->findAllByAttributes(array('studio_id' => $studio_id,'feed_method' => 'push'));
            if($liveStreamData){
                foreach($liveStreamData as $liveStreamDataKey => $liveStreamDataVal){
                    if($liveStreamData->stream_key != ''){
                        $streamName = explode("?",@$liveStreamData->stream_key);
                        if(@$streamName[0] != ''){
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=delete&stream_name=".@$streamName[0]);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $server_output = trim(curl_exec($ch));
                            curl_close($ch);
                        }
                    }
                }
            }
            Livestream::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $studio_id));   
            //Delete from video_sync_temp
            VideoSyncTemp::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
            //Delete from clould fornt 
                    $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                    $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                    $bucket = $bucketInfo['bucket_name'];
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucket,
                        'Prefix' => $studio_id . "/"
                    ));  
                    //Delete from s3 folders against studio
                    foreach ($response as $object) {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $object['Key']
                        ));
                    }
					
					$s3acces_details = Yii::app()->common->getS3Details($studio_id);
                    $client = CloudFrontClient::factory(array(
                            'key' => $s3acces_details['key'],
                            'secret' => $s3acces_details['secret']
                    ));
                    
                    if (HOST_IP != '127.0.0.1' && HOST_IP != '52.0.64.95' && DOMAIN != 'idogic') {
                        $stdCdnDet = StudioCdnDetails::model()->findByAttributes(array('studio_id' => $studio_id));
                        $cdnIdArray = array();
                        if ($stdCdnDet['signed_url_id'] != '' && $studio['signed_url'] != '') {
                            $cdnIdArray['signed'] = $stdCdnDet['signed_url_id'];
                        }
                        if ($stdCdnDet['unsigned_url_id'] != '' && $studio['unsigned_url'] != '') {
                            $cdnIdArray['unsigned'] = $stdCdnDet['unsigned_url_id'];
                        }
                        if ($stdCdnDet['video_url_id'] != '' && $studio['video_url'] != '') {
                            $cdnIdArray['video'] = $stdCdnDet['video_url_id'];
                        }
                        if (!empty($cdnIdArray)) {
                            foreach ($cdnIdArray as $cdnIdArrayKey => $cdnIdArrayVal) {
                                $originIdOfCdn = 'studio-' . $cdnIdArrayKey . '-' . $studio_id;
                                $cdnId = $cdnIdArrayVal;
                                $eTag = $client->getDistribution(array(
                                            'Id' => $cdnId,
                                        ))->get('ETag');
                                $resultforDisabelCdn = $client->updateDistribution(array(
                                        'CallerReference' => $originIdOfCdn,
                                        'Aliases' => array(
                                            'Quantity' => 0,
                                        ),
                                        'DefaultRootObject' => '',
                                        'Origins' => array(
                                            'Quantity' => 1,
                                            'Items' => array(
                                                array(
                                                    'Id' => $originIdOfCdn,
                                                    'DomainName' => $bucket . ".s3.amazonaws.com",
                                                    'OriginPath' => '/' . $studio_id,
                                                    'S3OriginConfig' => array(
                                                        'OriginAccessIdentity' => ''
                                                    )
                                                ),
                                            ),
                                        ),
                                        'DefaultCacheBehavior' => array(
                                            'TargetOriginId' => $originIdOfCdn,
                                            'ForwardedValues' => array(
                                                'QueryString' => true,
                                                'Cookies' => array(
                                                    'Forward' => 'all',
                                                ),
                                            ),
                                            'TrustedSigners' => array(
                                                'Enabled' => false,
                                                'Quantity' => 0,
                                            ),
                                            'ViewerProtocolPolicy' => 'allow-all',
                                            'MinTTL' => 1,
                                            'AllowedMethods' => array(
                                                'Quantity' => 2,
                                                'Items' => array('GET', 'HEAD'),
                                            ),
                                            'SmoothStreaming' => false,
                                        ),
                                        'CacheBehaviors' => array(
                                            'Quantity' => 0,
                                        ),
                                        'CustomErrorResponses' => array(
                                            'Quantity' => 0,
                                        ),
                                        'Comment' => 'Unsigned url for studio -' . $studio_id,
                                        'Logging' => array(
                                            'Enabled' => false,
                                            'IncludeCookies' => false,
                                            'Bucket' => $bucket . ".s3.amazonaws.com",
                                            'Prefix' => '',
                                        ),
                                        'PriceClass' => 'PriceClass_All',
                                        'Enabled' => false,
                                        'Id' => $cdnId,
                                        'IfMatch' => $eTag,
                                        'ViewerCertificate' => array(
                                            'CloudFrontDefaultCertificate' => true,
                                            'SSLSupportMethod' => 'vip',
                                            'MinimumProtocolVersion' => 'TLSv1',
                                        ),
                                        'Restrictions' => array(
                                            'GeoRestriction' => array(
                                                'RestrictionType' => 'none',
                                                'Quantity' => 0,
                                            ),
                                        ),
                                    ))->get('ETag');
                                    if ($resultforDisabelCdn != '') {
                                        $deleteCdn = new DeleteCdn();
                                        $deleteCdn->cdn_id = $cdnId;
                                        $deleteCdn->etag_id = $resultforDisabelCdn;
										$deleteCdn->key = @$s3acces_details['key'];
										$deleteCdn->secret = @$s3acces_details['secret'];
                                        $deleteCdn->save();
                                    }
                            }
                        }
                 }
            //Delete Studio S3 bucket Details
            $studioS3BucketDetails = StudioS3buckets::model()->findByPk($studio['studio_s3bucket_id']);
            if (isset($studioS3BucketDetails->pem_file_path) && $studioS3BucketDetails->pem_file_path != '') {              
                $theme_folder = $studio['theme'];
                $pemFilePathfirstPath = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'pem/' ;
                $pemFilePath = $pemFilePathfirstPath . $theme_folder . '/';
                @unlink($pemFilePath . $studioS3BucketDetails->pem_file_path);
            }
			$DeleteDataFromtable=array("StudioCdnDetails", "VideoSubtitle", "StudioConfig", "VideoManagement", "ImageManagement", "StudioAds", "VideoLogs", "BufferLogs", "AudioManagement", "Device", "RestrictDevice", "RestrictDeviceLog", "Encoding", "EncodingLog");
			foreach ($DeleteDataFromtable as $deletedata) {
				$deletedata::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
            }
        }
        return true;
    }

    function actionYearlyPlanRenewalReminder() {
        //Find out all active studios (Customer Only)
        $sql = "SELECT res.* FROM (SELECT u.studio_id, u.id AS user_id, u.email, u.first_name, s.start_date,
        en.studio_id AS email_studio_id, en.created, DATEDIFF(DATE(DATE_FORMAT(s.start_date, '%Y-%m-%d')), DATE('" . date('Y-m-d') . "')) AS days
        FROM studios s LEFT JOIN user u ON (s.id = u.studio_id) LEFT JOIN email_notification en ON (s.id = en.studio_id AND en.type = 4) 
        WHERE u.is_admin=0 AND u.is_sdk=1 AND u.role_id=1 AND s.reseller_id=0 AND s.status=1 AND s.is_subscribed=1 AND s.is_default=0 AND s.is_deleted=0 AND
        (DATEDIFF(DATE(DATE_FORMAT(s.start_date, '%Y-%m-%d')), DATE('" . date('Y-m-d') . "')) = 30 OR DATEDIFF(DATE(DATE_FORMAT(s.start_date, '%Y-%m-%d')), DATE('" . date('Y-m-d') . "')) = 14) 
        ORDER BY en.created DESC) AS res GROUP BY res.studio_id";

        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();

        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $is_email = 1;
                $days = $value['days'];

                if ((($days == 30) || ($days == 14)) && gmdate('Y-m-d') == gmdate("Y-m-d", strtotime($value['created']))) {
                    $is_email = 0;
                }

                if (intval($is_email)) {

                    $studio_id = $value['studio_id'];
                    $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'id ASC'));

                    if (isset($plans) && !empty($plans)) {
                        $selplan = $plans[0]->plans;

                        if ($selplan == 'Year') {
                            $selpackages = Package::model()->getPackages($plans[0]->package_id);
                            $package_name = $selpackages['package'][0]['name'];
                            $billing_amount = $selpackages['package'][0]['base_price'];
                            $app_price = Yii::app()->general->getAppPrice($selpackages['package'][0]['code']);

                            $no_of_applications = count($plans);
                            if ($no_of_applications > 1) {
                                $billing_amount = $billing_amount + (($no_of_applications - 1) * $app_price);
                            }

                            $yearly_discount = $selpackages['package'][0]['yearly_discount'];
                            $total_amount_to_be_charged = number_format((float) (($billing_amount * 12) - ((($billing_amount * 12) * $yearly_discount) / 100)), 2, '.', '');

                            //Keeping log in email tables after firing an email
                            $enModel = New EmailNotification;
                            $enModel->studio_id = $value['studio_id'];
                            $enModel->user_id = $value['user_id'];
                            $enModel->type = 4;
                            $enModel->created = new CDbExpression("NOW()");
                            $enModel->save();

                            //Sending an email to user
                            $req = array();
                            $req['name'] = $value['first_name'];
                            $req['email'] = $value['email'];
                            $req['days'] = $days;
                            $req['next_billing_date'] = gmdate("F m, Y", strtotime($value['start_date']));
                            $req['package_name'] = $package_name;
                            $req['total_amount_to_be_charged'] = $total_amount_to_be_charged;

                            Yii::app()->email->yearlyPlanRenewalReminderEmailToUser($req);
                        }
                    }
                }
            }
        }
    }

    function isCardErrorExists($studio_id, $billing_date) {
        //Check if card is already showing error for the desired date, then not allow to charge again in same day.
        $sql = "SELECT COUNT(*) AS total_card_errors FROM card_errors WHERE studio_id={$studio_id} 
        AND created LIKE '%{$billing_date}%' GROUP BY studio_id";

        $con = Yii::app()->db;
        $total_card_errors = $con->createCommand($sql)->queryAll();
        if (isset($total_card_errors[0]['total_card_errors']) && intval($total_card_errors[0]['total_card_errors'])) {
            $isNoCardError = 0;
        } else {
            $isNoCardError = 1;
        }

        return $isNoCardError;
    }

    function getMonthlyPackageDetail($package, $plans) {
        $package_detail = array();
        $package_detail['package_id'] = $package['package'][0]['id'];
        $package_detail['package_code'] = $package['package'][0]['code'];
        $package_detail['package'] = $package['package'][0]['name'];
        $package_detail['base_price'] = $package['package'][0]['base_price'];
        $package_detail['monthly_discount'] = $package['package'][0]['monthly_discount'];
        $package_detail['yearly_discount'] = $package['package'][0]['yearly_discount'];
        $package_detail['is_bandwidth'] = $package['package'][0]['is_bandwidth'];

        //Getting base price of package
        $package_amount = $package['package'][0]['base_price'];
        if (isset($plans) && !empty($plans)) {
            if ($plans[0]->is_old && $package['package'][0]['is_old_base_price']) {
                $package_amount = $plans[0]->base_price;
                $package_detail['base_price'] = $plans[0]->base_price;
            }
        }

        if (isset($plans) && !empty($plans)) {
            $applications_string = '';
            foreach ($plans as $key => $value) {
                if (intval($value->application_id)) {
                    $applications = Package::model()->getApplications($plans[0]->package_id, $value->application_id);
                    $package_detail['applications'][$key]['id'] = $applications[0]['id'];
                    $package_detail['applications'][$key]['name'] = $applications[0]['name'];
                    $package_detail['applications'][$key]['description'] = $applications[0]['description'];
                    $applications_string.=$applications[0]['name'] . ', ';
                }
            }

            if (isset($package_detail['applications']) && !empty($package_detail['applications'])) {
                $applications_string = rtrim($applications_string, ', ');
                $app_price = Yii::app()->general->getAppPrice($package['package'][0]['code']);

                $package_detail['application_price'] = $app_price;

                $no_of_applications = count($package_detail['applications']);
                $package_detail['no_of_applications'] = $no_of_applications;
                $package_detail['applications_string'] = $applications_string;

                if ($no_of_applications > 1) {
                    $package_amount = $package_amount + (($no_of_applications - 1) * $app_price);
                }
            }
        }
        $package_detail['monthly_package_amount'] = $package_amount;

        return $package_detail;
    }

    function getPausedBillingDetail($package, $plans, $PauseBilling) {
        $total_apps = array();
        $paused_apps = array();

        $paused_package_detail = array();
        $package_amount = 0;
        $today_date = gmdate('Y-m-d');

        if (isset($PauseBilling) && !empty($PauseBilling)) {
            $paused_package_detail['package_id'] = $package['package'][0]['id'];
            $paused_package_detail['package'] = $package['package'][0]['name'];
            $paused_package_detail['base_price'] = $package['package'][0]['base_price'];
            if ($package['package'][0]['is_old_base_price']) {
                $paused_package_detail['base_price'] = $plans[0]->base_price;
            }
            $paused_package_detail['application_price'] = $package['package'][0]['price'];
            $paused_package_detail['monthly_discount'] = $package['package'][0]['monthly_discount'];
            $paused_package_detail['yearly_discount'] = $package['package'][0]['yearly_discount'];
            $paused_package_detail['is_bandwidth'] = $package['package'][0]['is_bandwidth'];

            if (isset($PauseBilling[0]->application_id) && intval($PauseBilling[0]->application_id) == 0) {//Package having no applications
                $package_amount = $paused_package_detail['base_price'];
                $paused_package_detail['paused_package_string'] = $package['package'][0]['name'];
            } else {//Package having applications
                $applications_string = '';
                foreach ($PauseBilling as $key => $value) {
                    if (intval($value->application_id)) {
                        $isapplications = 0;
                        if (trim($value->resume_on) == '' || trim($value->resume_on) == '0000-00-00' || trim($value->resume_on) == '1970-01-01') {
                            $isapplications = 1;
                        } else if (trim($value->resume_on) != '' && strtotime($today_date) < strtotime($value->resume_on)) {
                            $isapplications = 1;
                        }

                        if (intval($isapplications)) {
                            $paused_apps[] = $value->application_id;
                            $applications = Package::model()->getApplications($value->package_id, $value->application_id);
                            $paused_package_detail['applications'][$key]['id'] = $applications[0]['id'];
                            $paused_package_detail['applications'][$key]['name'] = $applications[0]['name'];
                            $paused_package_detail['applications'][$key]['description'] = $applications[0]['description'];
                            $applications_string.=$applications[0]['name'] . ', ';
                        }
                    }
                }
                $applications_string = rtrim($applications_string, ', ');

                if (isset($plans) && !empty($plans)) {
                    foreach ($plans as $key => $value) {
                        if (intval($value->application_id)) {
                            $total_apps[] = $value->application_id;
                        }
                    }
                }

                $app_price = Yii::app()->general->getAppPrice($package['package'][0]['code']);

                if (count($total_apps) == count($paused_apps)) {
                    $package_amount = $paused_package_detail['base_price'];
                    $package_amount = $package_amount + ((count($total_apps) - 1) * $app_price);
                    $paused_package_detail['paused_package_string'] = $package['package'][0]['name'] . ' with ' . $applications_string;
                } else if (count($total_apps) > count($paused_apps)) {
                    $package_amount = (count($paused_apps) * $app_price);
                    $paused_package_detail['paused_package_string'] = $applications_string;
                }
            }

            $paused_package_detail['paused_package_amount'] = $package_amount;

            if (isset($PauseBilling[0]->application_id) && intval($PauseBilling[0]->application_id) == 0) {
                if (trim($PauseBilling[0]->resume_on) != '' && trim($PauseBilling[0]->resume_on) != '0000-00-00' && trim($PauseBilling[0]->resume_on) != '1970-01-01' && strtotime($today_date) >= strtotime($PauseBilling[0]->resume_on)) {
                    $paused_package_detail = array();
                }
            } else {
                if (empty($paused_apps)) {
                    $paused_package_detail = array();
                }
            }
        }

        return $paused_package_detail;
    }

    function getBandwidthCost($studio, $package) {
        $last_date = Date('Y-m-d', strtotime("-1 Months +1 Days"));
        $today_date = Date('Y-m-d');
        $studio_id = $studio->id;

        $bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, $last_date, $today_date, '', $package['package'][0]['is_bandwidth']);
        $bandwidth['is_bandwidth'] = $package['package'][0]['is_bandwidth'];

        return $bandwidth;
    }

    function saveCardError($studio, $user, $card_info, $firstData, $plans, $bill_amount) {
        $studio_id = $studio->id;
        $plan = $plans[0]->plans;

        //Keeping card error logs
        $ceModel = New CardErrors;
        $ceModel->studio_id = $studio_id;
        $ceModel->response_text = serialize($firstData);
        $ceModel->transaction_type = 'Save Card';
        $ceModel->created = new CDbExpression("NOW()");
        $ceModel->save();

        $req['name'] = $user->first_name;
        $req['email'] = $user->email;
        $req['companyname'] = $studio->name;
        $req['plan'] = @$plan;
        $req['card_last_fourdigit'] = $card_info->card_last_fourdigit;
        $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

        $studio->payment_key = '';
        $studio->payment_status = 2; //Payment failed
        $studio->partial_failed_date = gmdate('Y-m-d h:i:s'); //Payment failed date
        $studio->partial_failed_due = $bill_amount; //Due on failed payment
        $studio->save();

        Yii::app()->email->cardFailureMailToUser($req);
        Yii::app()->email->cardFailureMailToAdmin($req);
    }

    function actionBandwidthStorageTest() {
        //Find out all active studios (Customer Only)
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studio = Studios::model()->findByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));

            if (isset($studio) && !empty($studio)) {
                $studio_id = $studio->id;
                $plans = StudioPricingPlan::model()->find(array('select' => 'package_id', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'application_id ASC'));
            }

            if (isset($plans->plans) && $plans->plans == 'Year') {
                $billing_date = gmdate('Y-m-d', strtotime($studio->bandwidth_date));
            } else if (isset($plans->plans) && $plans->plans == 'Month') {
                $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
            }

            $last_billing_date = gmdate("Y-m-d", strtotime($billing_date . '-1 Months +1 Days'));
            $today_date = Date('Y-m-d');

            $bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, $last_billing_date, $today_date, '', 1);
            //$bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, '2016-05-01', '2016-06-24', '', 1);

            if (empty($bandwidth)) {
                if (isset($_REQUEST['bandwidth']) && trim($_REQUEST['bandwidth'])) {//test case
                    $region = (isset($_REQUEST['region']) && trim($_REQUEST['region'])) ? strtoupper(trim($_REQUEST['region'])) : 'NA';
                    $bandwidth['usage'][$region]['continent'] = 'North america';
                    $bandwidth['usage'][$region]['bandwidth'] = $_REQUEST['bandwidth'];
                }
            }

            $bandwidth['is_bandwidth'] = 1;
            $bandwidth_price = Yii::app()->aws->getBandwidthPrice($bandwidth);

            $bandwidth_detail = Yii::app()->pdf->getBandwidthDetail($bandwidth_price['bandwidth'], 1);
            print 'Bandwidth Detail: ' . $bandwidth_detail . "<br/>";

            $bandwidth_usage = (isset($bandwidth_price['total_bandwidth_consumed']) && !empty($bandwidth_price['total_bandwidth_consumed'])) ? $bandwidth_price['total_bandwidth_consumed'] : 0;
            $total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
            $storage = Yii::app()->aws->getStorageCost($total_storage, $bandwidth_usage);

            $storage_detail = Yii::app()->pdf->getStorageDetail($storage);
            print 'Storage Detail: ' . $storage_detail . "<br/>";
        } else {
            print 'Invalid stuidio id!!!';
        }

        exit;
    }

    function setBillingInfoDetail($studio_id, $user_id, $title, $bill_amount) {
        $biModel = New BillingInfos;
        $biModel->studio_id = $studio_id;
        $biModel->user_id = $user_id;
        $uniqid = Yii::app()->common->generateUniqNumber();
        $biModel->uniqid = $uniqid;
        $biModel->title = $title;
        $biModel->billing_date = new CDbExpression("NOW()");
        $biModel->created_date = new CDbExpression("NOW()");
        $biModel->billing_amount = $bill_amount;
        $biModel->transaction_type = 1;
        $biModel->is_paid = 0;
        $biModel->save();

        return $biModel->id;
    }

    function setBillingDetail($bdarg = array()) {
        $bdModel = New BillingDetails;
        $bdModel->billing_info_id = @$bdarg['billing_info_id'];
        $bdModel->user_id = @$bdarg['user_id'];
        $bdModel->title = @$bdarg['title'];
        $bdModel->description = @$bdarg['description'];
        $bdModel->amount = @$bdarg['amount'];
        $bdModel->type = @$bdarg['type'];
        $bdModel->remarks = @$bdarg['remarks'];
        $bdModel->save();
    }

    function updateBillingInfoDetail($data = array()) {
        if (isset($data) && !empty($data)) {

            $biModel = BillingInfos::model()->findByPk($data['id']);
            foreach ($data as $key => $value) {
                $biModel->$key = $value;
            }
            $biModel->save();
        }
    }

    function generateInvoice($arg = array()) {
        $studio = @$arg['studio'];
        $user = @$arg['user'];
        $card_info = @$arg['card_info'];
        $plans = @$arg['plans'];
        $package_detail = @$arg['package_detail'];
        $paused_package_detail = @$arg['paused_package_detail'];
        $bandwidth_detail = @$arg['bandwidth_detail'];
        $storage_detail = @$arg['storage_detail'];
        $drm_detail = @$arg['drm_detail'];
        $muvi_kart_detail = @$arg['muvi_kart_detail'];
        $billing_info_id = @$arg['billing_info_id'];
        $bill_amount = @$arg['bill_amount'];
        $billing_date = @$arg['billing_date'];
        $isEmail = @$arg['isEmail'];
        $is_bandwidth_storage_only = @$arg['is_bandwidth_storage_only'];

        $plan = $plans[0]->plans;

        //Set bandwidth detail
        if (isset($bandwidth_detail) && !empty($bandwidth_detail)) {

            $bandwidth_charge = isset($bandwidth_detail['price']) ? number_format((float) $bandwidth_detail['price'], 2, '.', '') : 0;
            if (abs($bandwidth_charge) > 0.00001) {
                $bandwidth = $bandwidth_detail;

                $invoice['isBandwidth'] = 1;
                $invoice['bandwidth_charge'] = $bandwidth_charge;
                $invoice['bandwidth'] = $bandwidth;
                $invoice['bandwidth_type'] = $package_detail['is_bandwidth'];

                $last_bandwwidth_date = Date('Y-m-d', strtotime("-1 Months +1 Days"));
                $invoice['bandwidth_period'] = date('F d, Y', strtotime($last_bandwwidth_date)) . " to " . date('F d, Y');
            }
        }

        //Set storage detail
        if (isset($storage_detail) && !empty($storage_detail)) {
            $storage_charge = isset($storage_detail['cost']) ? number_format((float) $storage_detail['cost'], 2, '.', '') : 0;

            if (abs($storage_charge) > 0.00001) {
                $invoice['isStorage'] = 1;
                $invoice['storage_charge'] = $storage_charge;
                $invoice['storage'] = $storage_detail;
            }
        }

        //Set Drm Price
        if (isset($drm_detail) && !empty($drm_detail)) {
                $invoice['isDrm_device'] = 0;
            $invoice['isDrm'] = 1;
           if($drm_detail['specified_device_type'] == 0){      
            $invoice['drm_price_per_view'] = $drm_detail['drm_price_per_view'];
           }
            $invoice['drm_total_views'] = $drm_detail['drm_total_views'];
            $invoice['drm_price'] = $drm_detail['drm_price'];
            if($drm_detail['specified_device_type'] == 1){
                $invoice['isDrm_device'] = 1;
                $invoice['drm_price_per_web_view'] = $drm_detail['drm_price_per_web_view'];
                $invoice['no_of_web_view'] = $drm_detail['drm_total_web_views'];
                $invoice['web_view_price'] = $drm_detail['drm_price_web'];
                $invoice['drm_price_per_android_view'] = $drm_detail['drm_price_per_android_view'];
                $invoice['no_of_android_view'] = $drm_detail['drm_total_android_views'];
                $invoice['android_view_price'] = $drm_detail['drm_price_android'];
                $invoice['drm_price_per_ios_view'] = $drm_detail['drm_price_per_ios_view'];
                $invoice['no_of_ios_view'] = $drm_detail['drm_total_ios_views'];
                $invoice['ios_view_price'] = $drm_detail['drm_price_ios'];                
                $invoice['drm_price_per_raku_view'] = $drm_detail['drm_price_per_raku_view'];
                $invoice['no_of_raku_view'] = $drm_detail['drm_total_raku_views'];
                $invoice['raku_view_price'] = $drm_detail['drm_price_raku'];    
        }
        }

        //Set Muvi Kart Price
        if (isset($muvi_kart_detail) && !empty($muvi_kart_detail)) {
            $invoice['isMuviKart'] = 1;
            $invoice['muvi_kart_price_of_transaction'] = $muvi_kart_detail['muvi_kart_price_of_transaction'];
            $invoice['muvi_kart_total_transactions'] = $muvi_kart_detail['muvi_kart_total_transactions'];
            $invoice['muvi_kart_price'] = $muvi_kart_detail['muvi_kart_price'];
            $invoice['amount_in_currency'] = $muvi_kart_detail['amount_in_currency'];
        }

        //Set paused item detail
        if (isset($paused_package_detail) && !empty($paused_package_detail)) {
            $invoice['is_paused'] = 1;
            $invoice['paused_package_string'] = $paused_package_detail['paused_package_string'];
            $invoice['paused_package_amount'] = $paused_package_detail['paused_package_amount'];
        }

        $invoice['studio'] = $studio;
        $invoice['user'] = $user;
        $invoice['card_info'] = $card_info;
        $invoice['billing_info_id'] = $billing_info_id;
        $invoice['billing_amount'] = $bill_amount;

        $invoice['package_code'] = @$package_detail['package_code'];
        $invoice['package_name'] = @$package_detail['package'];
        $invoice['base_price'] = @$package_detail['base_price'];
        $invoice['plan'] = @$plan;
        $invoice['yearly_discount'] = @$package_detail['yearly_discount'];
        $invoice['no_of_applications'] = @$package_detail['no_of_applications'];
        $invoice['applications'] = @$package_detail['applications_string'];

        if ($plan == 'Month') {
            $billing_end = Date('Y-m-d H:i:s', strtotime($billing_date . "+1 Months -1 Days"));
        } else {
            $billing_end = Date('Y-m-d H:i:s', strtotime($billing_date . "+1 Years -1 Days"));
        }

        $billing_period = "Billing Period " . date('F d, Y', strtotime($billing_date)) . " to " . date('F d, Y', strtotime($billing_end));

        if ($is_bandwidth_storage_only) {
            $billing_end = $billing_date;
            $billing_date = Date('Y-m-d H:i:s', strtotime($billing_date . "-1 Months +1 Days"));

            $billing_period = "Bandwidth and storage charge period " . date('F d, Y', strtotime($billing_date)) . " to " . date('F d, Y', strtotime($billing_end));
        }

        $invoice['billing_period'] = $billing_period;
        $invoice['isRenew'] = 1;
        $invoice['isEmail'] = $isEmail;
        $invoice['is_bandwidth_storage_only'] = $is_bandwidth_storage_only;
        $invoice['isReseller'] = @$arg['isReseller'];
        $pdf = Yii::app()->pdf->adminMonthlyYearlyRenewInvoiceDetial($invoice);
        return $pdf;
    }

    function setTransactionDetail($trandata = array()) {
        $tiModel = New TransactionInfos;
        $tiModel->card_info_id = @$trandata['card_info_id'];
        $tiModel->studio_id = @$trandata['studio_id'];
        $tiModel->user_id = @$trandata['user_id'];
        $tiModel->billing_info_id = @$trandata['billing_info_id'];
        $tiModel->billing_amount = @$trandata['bill_amount'];
        $tiModel->invoice_detail = @$trandata['invoice_detail'];
        $tiModel->is_success = 1;
        $tiModel->invoice_id = @$trandata['invoice_id'];
        $tiModel->order_num = @$trandata['order_num'];
        $tiModel->paid_amount = @$trandata['paid_amount'];
        $tiModel->response_text = @$trandata['response_text'];
        $tiModel->transaction_type = @$trandata['transaction_type'];
        $tiModel->created_date = new CDbExpression("NOW()");
        $tiModel->save();
    }

    function setStudioDetail($studio, $arg) {
        if (isset($arg) && !empty($arg)) {
            foreach ($arg as $key => $value) {
                if (trim($value)) {
                    $studio->$key = $value;
                }
            }
        }
        $studio->status = 1;
        $studio->is_subscribed = 1;
        $studio->is_deleted = 0;
        $studio->save();
    }

    function sendEmailsToAll($user, $studio, $package_detail, $plan = '', $billing_date = '', $paid_amount = 0, $file_name = '', $isBandwidthOnly = 0, $payment_method = 1) {
        if ($isBandwidthOnly) {
            $billing_end = $billing_date;
            $billing_date = Date('Y-m-d H:i:s', strtotime($billing_date . "-1 Months +1 Days"));
        } else {
            $billing_end = Date('Y-m-d H:i:s', strtotime($billing_date . "+1 Months"));
        }

        $req = array();
        $req['name'] = $user->first_name;
        $req['email'] = $user->email;
        $req['companyname'] = $studio->name;
        $req['package_name'] = @$package_detail['package'];
        $req['plan'] = @$plan;
        $req['billing_period'] = date('F d, Y', strtotime($billing_date)) . " to " . date('F d, Y', strtotime($billing_end));
        $req['billing_amount'] = $paid_amount;
        $req['isBandwidthOnly'] = $isBandwidthOnly;

        //Send an email to user and super admin
        if (intval($payment_method) == 2) {
            Yii::app()->email->monthlyYearlyManualRenewMailToSales($req, $file_name);
        } else {
            Yii::app()->email->monthlyYearlyRenewMailToUser($req, $file_name);
            Yii::app()->email->monthlyYearlyRenewMailToSales($req);
        }
    }

    function monthlyYearlyPayment($studio, $plans, $billing_date, $test_studio_id, $payment_method = 1) {
        $studio_id = $studio->id;
        $plan = @$plans[0]->plans;
        //Get package detail
        $package = Package::model()->getPackages($plans[0]->package_id);
        $package_detail = self::getMonthlyPackageDetail($package, $plans);
        $bill_amount = @$package_detail['monthly_package_amount'];
        if ($plan == 'Year') {
            $yearly_discount = @$package_detail['yearly_discount'];
            $yearly_discount_amount = ((($bill_amount * 12) * $yearly_discount) / 100);
            $bill_amount = (($bill_amount * 12) - $yearly_discount_amount);
        }

        //Get paused item detail
        $PauseBilling = PauseBilling::model()->findAllByAttributes(array('studio_id' => $studio_id, 'package_id' => $plans[0]->package_id, 'is_paused' => 1));
        $paused_package_detail = array();
        $paused_amount = 0;
        if (!empty($PauseBilling)) {
            $paused_package_detail = self::getPausedBillingDetail($package, $plans, $PauseBilling);

            if (!empty($paused_package_detail)) {
                $paused_amount = @$paused_package_detail['paused_package_amount'];

                if ($plan == 'Year') {
                    $paused_amount = (($paused_amount * 12) - ((($paused_amount * 12) * $yearly_discount) / 100));
                }
                $paused_package_detail['paused_package_amount'] = $paused_amount;
            }
        }
        $bill_amount = $bill_amount - $paused_amount;

        //Getting bandwidth detail
        $bandwidth = $this->getBandwidthCost($studio, $package);
        $bandwidth_detail = Yii::app()->aws->getBandwidthPrice($bandwidth);

        $bandwidth_charge = 0;
        if (isset($bandwidth_detail) && !empty($bandwidth_detail)) {
            $bandwidth_charge = isset($bandwidth_detail['price']) ? number_format((float) $bandwidth_detail['price'], 2, '.', '') : 0;
        }
        $bill_amount = number_format((float) ($bill_amount + $bandwidth_charge), 2, '.', '');

        if (intval($package['package'][0]['is_bandwidth'])) {
            $bandwidth_usage = (isset($bandwidth_detail['total_bandwidth_consumed']) && !empty($bandwidth_detail['total_bandwidth_consumed'])) ? $bandwidth_detail['total_bandwidth_consumed'] : 0;

            //Getting storage detail
            $total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
            $storage_detail = Yii::app()->aws->getStorageCost($total_storage, $bandwidth_usage);
            $storage_charge = 0;
            if (isset($storage_detail) && !empty($storage_detail)) {
                $storage_charge = isset($storage_detail['cost']) ? number_format((float) $storage_detail['cost'], 2, '.', '') : 0;
            }

            $bill_amount = $bill_amount + $storage_charge;
        }

        //Getting DRM pricing detail
        $drm_price = 0;
        $is_drm_enable = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'drm_enable');
		
        if (isset($is_drm_enable) && !empty($is_drm_enable)) {
			$get_drm_device_type = array('web'=> 1,'andriod' => 2, 'ios'=>3,'raku'=>4);
            if(count($get_drm_device_type) > 0 ){
			$total_views = array();
			foreach($get_drm_device_type as $key => $val){
				$views_for_device = VideoLogs::model()->getTotalViewsForStudio_with_devicetype($studio_id,$val);
				$total_views[] = $views_for_device;
			}
			$drm_detail = Yii::app()->aws->getDRMCostForStudioWithDevice($studio_id, $total_views);
			if (isset($drm_detail) && !empty($drm_detail)) {
				$drm_price = isset($drm_detail['drm_price']) ? number_format((float) $drm_detail['drm_price'], 2, '.', '') : 0;
			}
			$bill_amount = $bill_amount + $drm_price;
            }
        }

        //Getting Muvi Kart pricing detail
        $muvi_kart_price = 0;
        $is_muvi_kart_enable = Yii::app()->general->getStoreLink($studio_id, 1);
        if (isset($is_muvi_kart_enable) && intval($is_muvi_kart_enable)) {
            $total_transactions = Yii::app()->general->getTotalTransactionsOfStudioForMuviKart($studio_id);
            if (isset($total_transactions) && !empty($total_transactions)) {
                $muvi_kart_detail = Yii::app()->general->getMuviKartCostForStudio($studio_id, $total_transactions);

                if (isset($muvi_kart_detail) && !empty($muvi_kart_detail)) {
                    $muvi_kart_detail['amount_in_currency'] = $total_transactions['amount_in_currency'];
                    $muvi_kart_price = isset($muvi_kart_detail['muvi_kart_price']) ? number_format((float) $muvi_kart_detail['muvi_kart_price'], 2, '.', '') : 0;
                }
                $bill_amount = $bill_amount + $muvi_kart_price;
            }
        }

        $bill_amount = number_format((float) ($bill_amount), 2, '.', '');

        //Getting user info
        $user = User::model()->findByAttributes(array('studio_id' => $studio_id, 'is_admin' => 0, 'is_sdk' => 1, 'role_id' => 1, 'is_active' => 1));
        $user_id = $user->id;

        //Get card information detail
        if (intval($payment_method) == 1 || intval($payment_method) == 3) {
            $card_info = CardInfos::model()->find('studio_id=:studio_id AND user_id=:user_id AND is_cancelled=:is_cancelled', array(':is_cancelled' => 0, ':studio_id' => $studio_id, ':user_id' => $user_id));
        }

        //Set billing information detail
        $bititle = 'Purchase Subscription fee for ' . @$package_detail['package'];
        if (intval($test_studio_id)) {
            $arg['isEmail'] = 0;
        } else {
            $billing_info_id = self::setBillingInfoDetail($studio_id, $user_id, $bititle, $bill_amount);
            $arg['isEmail'] = 1;
        }

        //Generate invoice detail
        $arg['studio'] = @$studio;
        $arg['user'] = @$user;
        $arg['card_info'] = @$card_info;
        $arg['plans'] = @$plans;
        $arg['package_detail'] = @$package_detail;
        $arg['paused_package_detail'] = @$paused_package_detail;
        $arg['bandwidth_detail'] = @$bandwidth_detail;
        $arg['storage_detail'] = @$storage_detail;
        $arg['drm_detail'] = @$drm_detail;
        $arg['muvi_kart_detail'] = @$muvi_kart_detail;
        $arg['billing_info_id'] = @$billing_info_id;
        $arg['bill_amount'] = @$bill_amount;
        $arg['billing_date'] = @$billing_date;

        $invoice = self::generateInvoice($arg);
        $file_name = $invoice['pdf'];
        if (intval($test_studio_id) && intval($payment_method) != 3) {
            print $file_name;
            exit;
        }

        //Update invoice in billing information detail
        $upddata = array();
        $upddata['id'] = @$billing_info_id;
        $upddata['detail'] = $invoice['html'];

        self::updateBillingInfoDetail($upddata);

        //Set billing detail
        if (isset($package_detail['applications']) && !empty($package_detail['applications'])) {
            $cntapp = 1;
            foreach ($package_detail['applications'] as $key => $value) {
                if ($cntapp == 1) {
                    $title = 'Monthly platform fee';
                    $description = @$package_detail['package'] . ' platform fee';
                    $amount = @$package_detail['base_price'];
                } else {
                    $title = $value['name'];
                    $description = $value['description'];
                    $amount = @$package_detail['application_price'];
                }

                $bdarg = array();
                $bdarg['billing_info_id'] = $billing_info_id;
                $bdarg['user_id'] = $user_id;
                $bdarg['title'] = $title;
                $bdarg['description'] = $description;
                $bdarg['amount'] = $amount;
                $bdarg['type'] = 0;
                $bdarg['remarks'] = '';
                $cntapp++;
                self::setBillingDetail($bdarg);
            }
        } else if (isset($package_detail) && !empty($package_detail)) {
            $title = 'Monthly platform fee';
            $description = @$package_detail['package'] . ' platform fee';
            $amount = @$package_detail['base_price'];
            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = $title;
            $bdarg['description'] = $description;
            $bdarg['amount'] = $amount;
            $bdarg['type'] = 0;
            $bdarg['remarks'] = '';
            self::setBillingDetail($bdarg);
        }

        if ($plan == 'Year') {
            $title = 'Discount on platform';
            $description = 'Discount on platform';
            $amount = number_format((float) ($yearly_discount_amount), 2, '.', '');
            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = $title;
            $bdarg['description'] = $description;
            $bdarg['amount'] = $amount;
            $bdarg['type'] = 0;
            $bdarg['remarks'] = '';
            self::setBillingDetail($bdarg);
        }

        if (abs($paused_amount) > 0.00001) {
            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = 'Paused billing';
            $bdarg['description'] = 'Paused billing';
            $bdarg['amount'] = $paused_amount;
            $bdarg['type'] = 0;
            $bdarg['remarks'] = '';

            self::setBillingDetail($bdarg);
        }

        if (abs($bandwidth_charge) > 0.00001) {
            if (@$package_detail['is_bandwidth'] == 1) {
                $total_bandwidth_consumed = (isset($bandwidth_detail['bandwidth']['total_bandwidth_consumed'])) ? number_format((float) ($bandwidth_detail['bandwidth']['total_bandwidth_consumed'] * 1024), 3, '.', '') : 0;
                $total_billable_bandwidth_consumed = (isset($bandwidth_detail['bandwidth']['total_billable_bandwidth_consumed'])) ? number_format((float) ($bandwidth_detail['bandwidth']['total_billable_bandwidth_consumed'] * 1024), 3, '.', '') : 0;
                $bandwidth_usage = array('total_bandwidth' => $total_bandwidth_consumed, 'total_billable_bandwidth' => $total_billable_bandwidth_consumed);
            } else {
                $total_bandwidth_consumed = (isset($bandwidth_detail['hour'])) ? $bandwidth_detail['hour'] : 0;
                $bandwidth_usage = array('total_bandwidth' => $total_bandwidth_consumed, 'total_billable_bandwidth' => $total_bandwidth_consumed);
            }

            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = 'Bandwidth Charge';
            $bdarg['description'] = 'Bandwidth Charge';
            $bdarg['amount'] = $bandwidth_charge;
            $bdarg['type'] = 1;
            $bdarg['remarks'] = json_encode($bandwidth_usage);

            self::setBillingDetail($bdarg);
        }

        if (abs($storage_charge) > 0.00001) {
            $total_storage = (isset($storage_detail['storage']['total_storage'])) ? number_format((float) ($storage_detail['storage']['total_storage'] * 1024), 3, '.', '') : 0;
            $total_billable_storage = (isset($storage_detail['storage']['billable_storage'])) ? number_format((float) ($storage_detail['storage']['billable_storage'] * 1024), 3, '.', '') : 0;
            $storage_usage = array('total_storage' => $total_storage, 'total_billable_storage' => $total_billable_storage);

            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = 'Storage Charge';
            $bdarg['description'] = 'Storage Charge';
            $bdarg['amount'] = $storage_charge;
            $bdarg['type'] = 2;
            $bdarg['remarks'] = json_encode($storage_usage);

            self::setBillingDetail($bdarg);
        }

        if (abs($drm_price) >= 0.01) {
            $drm_usage = array('drm_price_per_view' => $drm_detail['drm_price_per_view'], 'total_views' => $drm_detail['drm_total_views'], 'drm_price' => $drm_detail['drm_price']);

            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = 'Drm Price';
            $bdarg['description'] = 'Drm Price';
            $bdarg['amount'] = $drm_price;
            $bdarg['type'] = 5;
            $bdarg['remarks'] = json_encode($drm_usage);

            self::setBillingDetail($bdarg);
        }

        if (abs($muvi_kart_price) >= 0.01) {
            $muvi_kart_usage = array('muvi_kart_price_of_transaction' => $muvi_kart_detail['muvi_kart_price_of_transaction'], 'muvi_kart_total_transactions' => $muvi_kart_detail['muvi_kart_total_transactions'], 'muvi_kart_price' => $muvi_kart_detail['muvi_kart_price']);

            $bdarg = array();
            $bdarg['billing_info_id'] = $billing_info_id;
            $bdarg['user_id'] = $user_id;
            $bdarg['title'] = 'Muvi Kart';
            $bdarg['description'] = 'Muvi Kart';
            $bdarg['amount'] = $muvi_kart_price;
            $bdarg['type'] = 6;
            $bdarg['remarks'] = json_encode($muvi_kart_usage);

            self::setBillingDetail($bdarg);
        }

        //Process transaction
        $today = Date('Y-m-d H:i:s');
        if ($plan == 'Year') {
            $start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
            $end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
        } else {
            $start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
            $end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
        }
        if (abs($bill_amount) < 0.00001) {
            //Update billing infos status
            $upddata = array();
            $upddata['id'] = @$billing_info_id;
            $upddata['paid_amount'] = 0;
            $upddata['is_paid'] = 2;
            $upddata['billing_period_start'] = $today;
            $upddata['billing_period_end'] = $start_date;
            self::updateBillingInfoDetail($upddata);

            //Set Studio Detail
            $studio_data = array();
            $studio_data['start_date'] = $start_date;
            $studio_data['end_date'] = $end_date;
            $studio_data['bandwidth_date'] = $start_date;
            self::setStudioDetail($studio, $studio_data);

            //Drop email to customer and studio support
            self::sendEmailsToAll($user, $studio, $package_detail, $plan, $billing_date, $bill_amount, $file_name);
        } else {
            if (isset($card_info) && !empty($card_info) && intval($payment_method) == 1) {//Payment by card
                $firstData = self::processStudioTransaction($card_info, $bill_amount);

                if ($firstData->isError()) {
                    self::saveCardError($studio, $user, $card_info, $firstData, $plans, $bill_amount);
                } else {
                    if ($firstData->getBankResponseType() == 'S') {
                        //Set transaction detail
                        $trandata = array();
                        $trandata['studio_id'] = @$studio_id;
                        $trandata['user_id'] = @$user_id;
                        $trandata['card_info_id'] = @$card_info->id;
                        $trandata['billing_info_id'] = @$billing_info_id;
                        $trandata['bill_amount'] = @$bill_amount;
                        $trandata['invoice_detail'] = $plan . "ly Billing Charge";
                        $trandata['paid_amount'] = $paid_amount = $firstData->getAmount();
                        $trandata['invoice_id'] = $firstData->getTransactionTag();
                        $trandata['order_num'] = $firstData->getAuthNumber();
                        $trandata['response_text'] = serialize($firstData);
                        $trandata['transaction_type'] = 'Purchase Subscription fee for ' . @$package_detail['package'];
                        self::setTransactionDetail($trandata);

                        //Update billing information detail
                        $upddata = array();
                        $upddata['id'] = @$billing_info_id;
                        $upddata['paid_amount'] = @$paid_amount;
                        $upddata['is_paid'] = 2;
                        $upddata['billing_period_start'] = $today;
                        $upddata['billing_period_end'] = $start_date;
                        self::updateBillingInfoDetail($upddata);

                        //Set Studio Detail
                        $studio_data = array();
                        $studio_data['start_date'] = $start_date;
                        $studio_data['end_date'] = $end_date;
                        $studio_data['bandwidth_date'] = $start_date;
                        self::setStudioDetail($studio, $studio_data);

                        //Drop email to customer and studio support
                        self::sendEmailsToAll($user, $studio, $package_detail, $plan, $billing_date, $paid_amount, $file_name);
                    } else {
                        self::saveCardError($studio, $user, $card_info, $firstData, $plans, $bill_amount);
                    }
                }
            } else if (intval($payment_method) == 2) {//Payment by Manual method
                //Update billing information detail
                $upddata = array();
                $upddata['id'] = @$billing_info_id;
                $upddata['paid_amount'] = 0;
                $upddata['is_paid'] = 0;
                $upddata['billing_period_start'] = $today;
                $upddata['billing_period_end'] = $start_date;
                self::updateBillingInfoDetail($upddata);

                //Set Studio Detail
                $studio_data = array();
                $studio_data['start_date'] = $start_date;
                $studio_data['end_date'] = $end_date;
                $studio_data['bandwidth_date'] = $start_date;
                self::setStudioDetail($studio, $studio_data);

                //Drop email to customer and studio support
                self::sendEmailsToAll($user, $studio, $package_detail, $plan, $billing_date, $bill_amount, $file_name, 0, $payment_method);
            } else if (intval($payment_method) == 3) {         //Payment by Paypal method
                $paypal = self::processPaypalStudioTransaction($card_info, $bill_amount);

                if ($paypal["ACK"] == 'Failure') {
                    //self::saveCardError($studio, $user, $card_info, $paypal, $plans, $bill_amount);
                } else {
                    if ($paypal["ACK"] == 'Success') {
                        //Set transaction detail
                        $trandata = array();
                        $trandata['studio_id'] = @$studio_id;
                        $trandata['user_id'] = @$user_id;
                        $trandata['card_info_id'] = @$card_info->id;
                        $trandata['billing_info_id'] = @$billing_info_id;
                        $trandata['bill_amount'] = @$bill_amount;
                        $trandata['invoice_detail'] = $plan . "ly Billing Charge";
                        $trandata['paid_amount'] = $paid_amount = $paypal['AMT'];
                        $trandata['invoice_id'] = $paypal['TRANSACTIONID'];
                        $trandata['order_num'] = $paypal['CORRELATIONID'];
                        $trandata['response_text'] = serialize($paypal);
                        $trandata['transaction_type'] = 'Purchase Subscription fee for ' . @$package_detail['package'];
                        self::setTransactionDetail($trandata);

                        //Update billing information detail
                        $upddata = array();
                        $upddata['id'] = @$billing_info_id;
                        $upddata['paid_amount'] = @$paid_amount;
                        $upddata['is_paid'] = 2;
                        $upddata['billing_period_start'] = $today;
                        $upddata['billing_period_end'] = $start_date;
                        self::updateBillingInfoDetail($upddata);

                        //Set Studio Detail
                        $studio_data = array();
                        $studio_data['start_date'] = $start_date;
                        $studio_data['end_date'] = $end_date;
                        $studio_data['bandwidth_date'] = $start_date;
                        self::setStudioDetail($studio, $studio_data);

                        //Drop email to customer and studio support
                        self::sendEmailsToAll($user, $studio, $package_detail, $plan, $billing_date, $paid_amount, $file_name);
                    } else {
                        self::saveCardError($studio, $user, $card_info, $firstData, $plans, $bill_amount);
                    }
                }
            }
        }
    }

    function processPaypalStudioTransaction($card_info, $billing_amount) {
        if (IS_LIVE_PAYPAL) {
            $payPalPro = new paypal_pro(trim(PAYPAL_API_LOGIN), trim(PAYPAL_API_KEY), trim(PAYPAL_API_SIGNATURE), '', '', TRUE, FALSE, '57.0');
        } else {
            $payPalPro = new paypal_pro(trim(PAYPAL_API_LOGIN), trim(PAYPAL_API_KEY), trim(PAYPAL_API_SIGNATURE), '', '', FALSE, FALSE, '57.0');
        }

        $resArray = $payPalPro->CreateReferenceTransaction($card_info, $billing_amount);
        return $resArray;
    }

    function monthlySubscriptionRenewalOfStudios($studio = array(), $plans = array(), $test_studio_id = 0) {
        $studio_id = $studio->id;
        //Finding billing date
        if (isset($test_studio_id) && intval($test_studio_id)) {
            $billing_date = gmdate('Y-m-d');
        } else {
            $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
        }
        $payment_methods = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'payment_method');
        $payment_method = (isset($payment_methods['config_value']) && intval($payment_methods['config_value'])) ? $payment_methods['config_value'] : 1;
        if ((isset($test_studio_id) && intval($test_studio_id)) || $payment_method == 3) {
            $isNoCardError = 1;
        } else {
            $isNoCardError = self::isCardErrorExists($studio_id, $billing_date);
        }
        if ((gmdate('Y-m-d') == $billing_date) && intval($isNoCardError)) {
            self::monthlyYearlyPayment($studio, $plans, $billing_date, $test_studio_id, $payment_method);
        }
    }

    function yearlySubscriptionRenewalOfStudios($studio = array(), $plans = array(), $test_studio_id = 0) {
        $studio_id = $studio->id;
        //Finding billing date
        if (isset($test_studio_id) && intval($test_studio_id)) {
            $billing_date = '';
            $billing_date = gmdate('Y-m-d');
            //$bandwidth_date = gmdate('Y-m-d');
        } else {
            $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
            $bandwidth_date = gmdate('Y-m-d', strtotime($studio->bandwidth_date));
        }
        if (gmdate('Y-m-d') == $billing_date) {
            $payment_methods = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'payment_method');
            $payment_method = (isset($payment_methods['config_value']) && intval($payment_methods['config_value'])) ? $payment_methods['config_value'] : 1;

            if ((isset($test_studio_id) && intval($test_studio_id)) || $payment_method == 3) {
                $isNoCardError = 1;
            } else {
                $isNoCardError = self::isCardErrorExists($studio_id, $billing_date);
            }
            if (intval($isNoCardError)) {
                self::monthlyYearlyPayment($studio, $plans, $billing_date, $test_studio_id, $payment_method);
            }
        } else if (gmdate('Y-m-d') == $bandwidth_date) {
            $isNoCardError = self::isCardErrorExists($studio_id, $bandwidth_date);
            if (isset($test_studio_id) && intval($test_studio_id)) {
                $isNoCardError = 1;
            }
            if (intval($isNoCardError)) {
                $bill_amount = 0;
                $package = Package::model()->getPackages($plans[0]->package_id);
                $package_detail = self::getMonthlyPackageDetail($package, $plans);

                //Getting bandwidth cost
                $bandwidth = $this->getBandwidthCost($studio, $package);
                $bandwidth_detail = Yii::app()->aws->getBandwidthPrice($bandwidth);

                $bandwidth_charge = 0;
                if (isset($bandwidth_detail) && !empty($bandwidth_detail)) {
                    $bandwidth_charge = isset($bandwidth_detail['price']) ? number_format((float) $bandwidth_detail['price'], 2, '.', '') : 0;
                }
                $bill_amount = number_format((float) ($bill_amount + $bandwidth_charge), 2, '.', '');

                if (intval($package['package'][0]['is_bandwidth'])) {
                    $bandwidth_usage = (isset($bandwidth_detail['total_bandwidth_consumed']) && !empty($bandwidth_detail['total_bandwidth_consumed'])) ? $bandwidth_detail['total_bandwidth_consumed'] : 0;

                    //Getting storage detail
                    $total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
                    $storage_detail = Yii::app()->aws->getStorageCost($total_storage, $bandwidth_usage);
                    $storage_charge = 0;
                    if (isset($storage_detail) && !empty($storage_detail)) {
                        $storage_charge = isset($storage_detail['cost']) ? number_format((float) $storage_detail['cost'], 2, '.', '') : 0;
                    }
                    $bill_amount = $bill_amount + $storage_charge;
                }

                //Getting DRM pricing detail
                $drm_price = 0;
                $is_drm_enable = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'drm_enable');
                if (isset($is_drm_enable) && !empty($is_drm_enable)) {
                 $get_drm_device_type = array('web'=> 1,'andriod' => 2, 'ios'=>3,'raku'=>4);
                if(count($get_drm_device_type) > 0 ){
                    $total_views = array();
                   foreach($get_drm_device_type as $key => $val){
                       $views_for_device = VideoLogs::model()->getTotalViewsForStudio_with_devicetype($studio_id,$val);
                       $total_views[] = $views_for_device;
                   }
                   $drm_detail = Yii::app()->aws->getDRMCostForStudioWithDevice($studio_id, $total_views);
                   if (isset($drm_detail) && !empty($drm_detail)) {
                        $drm_price = isset($drm_detail['drm_price']) ? number_format((float) $drm_detail['drm_price'], 2, '.', '') : 0;
                    }
                  $bill_amount = $bill_amount + $drm_price;
                }
                }

                //Getting Muvi Kart pricing detail
                $muvi_kart_price = 0;
                $is_muvi_kart_enable = Yii::app()->general->getStoreLink($studio_id, 1);
                if (isset($is_muvi_kart_enable) && intval($is_muvi_kart_enable)) {
                    $total_transactions = Yii::app()->general->getTotalTransactionsOfStudioForMuviKart($studio_id);
                    if (isset($total_transactions) && !empty($total_transactions)) {
                        $muvi_kart_detail = Yii::app()->general->getMuviKartCostForStudio($studio_id, $total_transactions);

                        if (isset($muvi_kart_detail) && !empty($muvi_kart_detail)) {
                            $muvi_kart_detail['amount_in_currency'] = $total_transactions['amount_in_currency'];
                            $muvi_kart_price = isset($muvi_kart_detail['muvi_kart_price']) ? number_format((float) $muvi_kart_detail['muvi_kart_price'], 2, '.', '') : 0;
                        }
                        $bill_amount = $bill_amount + $muvi_kart_price;
                    }
                }

                $bill_amount = number_format((float) ($bill_amount), 2, '.', '');

                $today = Date('Y-m-d H:i:s');
                $start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                $end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));

                if (abs($bill_amount) < 0.00001) {//Do nothing but simply update the bandwidth 
                    $studio_data = array();
                    $studio_data['bandwidth_date'] = $start_date;
                    self::setStudioDetail($studio, $studio_data);
                } else {
                    //Getting user info
                    $user = User::model()->findByAttributes(array('studio_id' => $studio_id, 'is_admin' => 0, 'is_sdk' => 1, 'role_id' => 1, 'is_active' => 1));
                    $user_id = $user->id;

                    //Get card information detail
                    $payment_methods = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'payment_method');
                    $payment_method = (isset($payment_methods['config_value']) && intval($payment_methods['config_value'])) ? $payment_methods['config_value'] : 1;

                    //Get card information detail
                    if (intval($payment_method) == 1) {
                        $card_info = CardInfos::model()->find('studio_id=:studio_id AND user_id=:user_id AND is_cancelled=:is_cancelled', array(':is_cancelled' => 0, ':studio_id' => $studio_id, ':user_id' => $user_id));
                    }

                    //if (isset($card_info) && !empty($card_info)) {
                    //Set billing information detail
                    $bandwidth_end = Date('Y-m-d H:i:s', strtotime($bandwidth_date . "-1 Months -1 Days"));
                    $bititle = "Bandwidth/Storage charge period " . date('F d, Y', strtotime($bandwidth_end)) . " to " . date('F d, Y', strtotime($bandwidth_date));
                    if (intval($test_studio_id)) {
                        $arg['isEmail'] = 0;
                    } else {
                        $billing_info_id = self::setBillingInfoDetail($studio_id, $user_id, $bititle, $bill_amount);
                        $arg['isEmail'] = 1;
                    }

                    //Generate invoice detail
                    $arg['studio'] = @$studio;
                    $arg['user'] = @$user;
                    $arg['card_info'] = @$card_info;
                    $arg['package_detail'] = @$package_detail;
                    $arg['plans'] = @$plans;
                    $arg['bandwidth_detail'] = @$bandwidth_detail;
                    $arg['storage_detail'] = @$storage_detail;
                    $arg['drm_detail'] = @$drm_detail;
                    $arg['muvi_kart_detail'] = @$muvi_kart_detail;
                    $arg['billing_info_id'] = @$billing_info_id;
                    $arg['bill_amount'] = @$bill_amount;
                    $arg['billing_date'] = @$bandwidth_date;
                    $arg['is_bandwidth_storage_only'] = 1;

                    $invoice = self::generateInvoice($arg);
                    $file_name = $invoice['pdf'];
                    if (intval($test_studio_id)) {
                        print $file_name;
                        exit;
                    }

                    //Update invoice in billing information detail
                    $upddata = array();
                    $upddata['id'] = @$billing_info_id;
                    $upddata['detail'] = $invoice['html'];

                    self::updateBillingInfoDetail($upddata);

                    if (abs($bandwidth_charge) > 0.00001) {
                        if (@$package_detail['is_bandwidth'] == 1) {
                            $total_bandwidth_consumed = (isset($bandwidth_detail['bandwidth']['total_bandwidth_consumed'])) ? number_format((float) ($bandwidth_detail['bandwidth']['total_bandwidth_consumed'] * 1024), 3, '.', '') : 0;
                            $total_billable_bandwidth_consumed = (isset($bandwidth_detail['bandwidth']['total_billable_bandwidth_consumed'])) ? number_format((float) ($bandwidth_detail['bandwidth']['total_billable_bandwidth_consumed'] * 1024), 3, '.', '') : 0;
                            $bandwidth_usage = array('total_bandwidth' => $total_bandwidth_consumed, 'total_billable_bandwidth' => $total_billable_bandwidth_consumed);
                        } else {
                            $total_bandwidth_consumed = (isset($bandwidth_detail['hour'])) ? $bandwidth_detail['hour'] : 0;
                            $bandwidth_usage = array('total_bandwidth' => $total_bandwidth_consumed, 'total_billable_bandwidth' => $total_bandwidth_consumed);
                        }

                        $bdarg = array();
                        $bdarg['billing_info_id'] = $billing_info_id;
                        $bdarg['user_id'] = $user_id;
                        $bdarg['title'] = 'Bandwidth Charge';
                        $bdarg['description'] = 'Bandwidth Charge';
                        $bdarg['amount'] = $bandwidth_charge;
                        $bdarg['type'] = 1;
                        $bdarg['remarks'] = json_encode($bandwidth_usage);

                        self::setBillingDetail($bdarg);
                    }

                    if (abs($storage_charge) > 0.00001) {
                        $total_storage = (isset($storage_detail['storage']['total_storage'])) ? number_format((float) ($storage_detail['storage']['total_storage'] * 1024), 3, '.', '') : 0;
                        $total_billable_storage = (isset($storage_detail['storage']['billable_storage'])) ? number_format((float) ($storage_detail['storage']['billable_storage'] * 1024), 3, '.', '') : 0;
                        $storage_usage = array('total_storage' => $total_storage, 'total_billable_storage' => $total_billable_storage);

                        $bdarg = array();
                        $bdarg['billing_info_id'] = $billing_info_id;
                        $bdarg['user_id'] = $user_id;
                        $bdarg['title'] = 'Storage Charge';
                        $bdarg['description'] = 'Storage Charge';
                        $bdarg['amount'] = $storage_charge;
                        $bdarg['type'] = 2;
                        $bdarg['remarks'] = json_encode($storage_usage);

                        self::setBillingDetail($bdarg);
                    }

                    if (abs($drm_price) >= 0.01) {
                        $drm_usage = array('drm_price_per_view' => $drm_detail['drm_price_per_view'], 'total_views' => $drm_detail['drm_total_views'], 'drm_price' => $drm_detail['drm_price']);

                        $bdarg = array();
                        $bdarg['billing_info_id'] = $billing_info_id;
                        $bdarg['user_id'] = $user_id;
                        $bdarg['title'] = 'Drm Price';
                        $bdarg['description'] = 'Drm Price';
                        $bdarg['amount'] = $drm_price;
                        $bdarg['type'] = 5;
                        $bdarg['remarks'] = json_encode($drm_usage);

                        self::setBillingDetail($bdarg);
                    }

                    if (abs($muvi_kart_price) >= 0.01) {
                        $muvi_kart_usage = array('muvi_kart_price_of_transaction' => $muvi_kart_detail['muvi_kart_price_of_transaction'], 'muvi_kart_total_transactions' => $muvi_kart_detail['muvi_kart_total_transactions'], 'muvi_kart_price' => $muvi_kart_detail['muvi_kart_price']);

                        $bdarg = array();
                        $bdarg['billing_info_id'] = $billing_info_id;
                        $bdarg['user_id'] = $user_id;
                        $bdarg['title'] = 'Muvi Kart';
                        $bdarg['description'] = 'Muvi Kart';
                        $bdarg['amount'] = $muvi_kart_price;
                        $bdarg['type'] = 6;
                        $bdarg['remarks'] = json_encode($muvi_kart_usage);

                        self::setBillingDetail($bdarg);
                    }

                    if (isset($card_info) && !empty($card_info) && intval($payment_method) == 1) {//Payment by card
                        $firstData = self::processStudioTransaction($card_info, $bill_amount);

                        if ($firstData->isError()) {
                            self::saveCardError($studio, $user, $card_info, $firstData, $plans, $bill_amount);
                        } else {
                            if ($firstData->getBankResponseType() == 'S') {
                                //Set transaction detail
                                $trandata = array();
                                $trandata['studio_id'] = @$studio_id;
                                $trandata['user_id'] = @$user_id;
                                $trandata['card_info_id'] = @$card_info->id;
                                $trandata['billing_info_id'] = @$billing_info_id;
                                $trandata['bill_amount'] = @$bill_amount;
                                $trandata['invoice_detail'] = "Bandwidth/Storage Billing Charge";
                                $trandata['paid_amount'] = $paid_amount = $firstData->getAmount();
                                $trandata['invoice_id'] = $firstData->getTransactionTag();
                                $trandata['order_num'] = $firstData->getAuthNumber();
                                $trandata['response_text'] = serialize($firstData);
                                $trandata['transaction_type'] = "Bandwidth/Storage Billing Charge";
                                self::setTransactionDetail($trandata);

                                //Update billing information detail
                                $upddata = array();
                                $upddata['id'] = @$billing_info_id;
                                $upddata['paid_amount'] = @$paid_amount;
                                $upddata['is_paid'] = 2;
                                self::updateBillingInfoDetail($upddata);

                                //Set Studio Detail
                                $studio_data = array();
                                $studio_data['bandwidth_date'] = $start_date;
                                self::setStudioDetail($studio, $studio_data);

                                //Drop email to customer and studio support
                                $plan = $plans[0]->plans;
                                self::sendEmailsToAll($user, $studio, $package_detail, $plan, $bandwidth_date, $paid_amount, $file_name, 1);
                            } else {
                                self::saveCardError($studio, $user, $card_info, $firstData, $plans, $bill_amount);
                            }
                        }
                    } else if (intval($payment_method) == 2) {//Payment by Manual method
                        //Update billing information detail
                        $upddata = array();
                        $upddata['id'] = @$billing_info_id;
                        $upddata['paid_amount'] = 0;
                        $upddata['is_paid'] = 0;
                        self::updateBillingInfoDetail($upddata);

                        //Set Studio Detail
                        $studio_data = array();
                        $studio_data['bandwidth_date'] = $start_date;
                        self::setStudioDetail($studio, $studio_data);

                        //Drop email to customer and studio support
                        $plan = $plans[0]->plans;
                        self::sendEmailsToAll($user, $studio, $package_detail, $plan, $bandwidth_date, $bill_amount, $file_name, 1, $payment_method);
                    } else if (intval($payment_method) == 3) {//Payment by Paypal method
                    }
                    //}
                }
            }
        }
    }

    function actionSubscriptionRenewalOfStudios() {
        //Find out all active studios (Customer Only)
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0, 'reseller_id' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0, 'reseller_id' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;

                //Get packages selected by studio admin
                $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans, package_code_id, base_price, is_old', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'application_id ASC'));
                if (isset($plans) && !empty($plans)) {
                    $plan = $plans[0]->plans;

                    if ($plan == 'Month') {
                        $this->monthlySubscriptionRenewalOfStudios($studio, $plans, $_REQUEST['studio']);
                    } else if ($plan == 'Year') {
                        $this->yearlySubscriptionRenewalOfStudios($studio, $plans, $_REQUEST['studio']);
                    }
                }
            }
        }
    }

    function actionPaymentFailedReminder() {
        //Find Customer whose payment has failed or partially done
        $sql = "SELECT res.* FROM (SELECT s.id as studio_id, s.status, s.is_subscribed, s.payment_status, s.partial_failed_date,
        s.partial_failed_due, u.id AS user_id, u.first_name, u.email, en.studio_id AS email_studio_id, 
        en.created, DATEDIFF(DATE('" . date('Y-m-d') . "'), DATE(DATE_FORMAT(s.partial_failed_date, '%Y-%m-%d'))) AS days FROM
        studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 
        AND u.is_active=1) LEFT JOIN email_notification en ON (s.id = en.studio_id AND en.type = 5) 
        WHERE s.status=1 AND s.is_subscribed=1 AND s.is_deleted=0 AND s.is_default=0 AND s.payment_status!=0 AND 
        (DATEDIFF(DATE('" . date('Y-m-d') . "'), DATE(DATE_FORMAT(s.partial_failed_date, '%Y-%m-%d'))) = 2 OR DATEDIFF(DATE('" . date('Y-m-d') . "'), DATE(DATE_FORMAT(s.partial_failed_date, '%Y-%m-%d'))) = 9) 
        ORDER BY en.created DESC) AS res GROUP BY res.studio_id";

        $dbcon = Yii::app()->db;
        $studios = $dbcon->createCommand($sql)->queryAll();

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $value) {
                $is_email = 1;
                $days = $value['days'];

                if ((($days == 2) || ($days == 9)) && gmdate('Y-m-d') == gmdate("Y-m-d", strtotime($value['created']))) {
                    $is_email = 0;
                }

                if (intval($is_email)) {
                    $studio_id = $value['studio_id'];
                    $user_id = $value['user_id'];

                    //Keeping log in email tables after firing an email
                    $enModel = New EmailNotification;
                    $enModel->studio_id = $studio_id;
                    $enModel->user_id = $user_id;
                    $enModel->type = 5;
                    $enModel->created = new CDbExpression("NOW()");
                    $enModel->save();

                    $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'id ASC'));
                    $selplan = $plans[0]->plans;
                    $card_info = CardInfos::model()->find('studio_id=:studio_id AND user_id=:user_id AND is_cancelled=:is_cancelled', array(':is_cancelled' => 0, ':studio_id' => $studio_id, ':user_id' => $user_id));

                    //Sending an email to user
                    $req = array();
                    $req['name'] = $value['first_name'];
                    $req['email'] = $value['email'];
                    $req['plan'] = $selplan;
                    $req['card_last_fourdigit'] = $card_info->card_last_fourdigit;

                    if ($days == 2) {
                        Yii::app()->email->paymentFailedReminderEmailToUser($req);
                    } else {
                        $req['cancelled_date'] = date('F d, Y', strtotime('+2 days'));
                        Yii::app()->email->paymentFailedFinalReminderEmailToUser($req);
                    }
                }
            }
        }
    }

    function actionCancelAccountAfterPaymentFailed() {
        //Find Customer whose payment has failed or partially done
        $sql = "SELECT s.*, u.*, u.id AS user_id, DATEDIFF(DATE('" . date('Y-m-d H:i:s') . "'), DATE(s.partial_failed_date)) AS days FROM
        studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 
        AND u.is_active=1) WHERE s.status=1 AND s.is_subscribed=1 AND s.is_deleted=0 AND s.is_default=0 AND s.payment_status!=0 AND 
        (DATEDIFF(DATE('" . date('Y-m-d H:i:s') . "'), DATE(s.partial_failed_date)) = 11)";

        $dbcon = Yii::app()->db;
        $studios = $dbcon->createCommand($sql)->queryAll();

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $value) {
                $days = $value['days'];
                if ($days == 11) {
                    $studio_id = $value['studio_id'];
                    $user_id = $value['user_id'];
                    $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');

                    $req['name'] = $value['first_name'];
                    $req['email'] = $value['email'];
                    $req['phone'] = $value['phone_no'];
                    $req['companyname'] = $value['name'];
                    $req['domain'] = 'http://' . $value['domain'];
                    $req['cancel_reason'] = 'Payment Failed';
                    $req['custom_notes'] = "Customer doesn't update his card after payment failed";

                    //Insert new record in StudioCancelReason table
                    $scrModel = New StudioCancelReason;
                    $scrModel->studio_id = $studio_id;
                    $scrModel->reason = $req['cancel_reason'];
                    $scrModel->custom_notes = $req['custom_notes'];
                    $scrModel->ip = CHttpRequest::getUserHostAddress();
                    $scrModel->created_by = $user_id;
                    $scrModel->created_date = gmdate('Y-m-d');
                    $scrModel->save();

                    //Update card to inactive mode
                    $sql = "UPDATE card_infos SET is_cancelled=2, cancelled_date=NOW() WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                    $con = Yii::app()->db;
                    $ciData = $con->createCommand($sql)->execute();

                    //Update studio pricing plans
                    $sqlplan = "UPDATE studio_pricing_plans SET status=0, updated_by=" . $user_id . ", updated=NOW() WHERE studio_id=" . $studio_id;
                    $planData = $con->createCommand($sqlplan)->execute();

                    //Delete all pause billing
                    PauseBilling::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Deactivate all users like admin and members
                    $sql_usr = "UPDATE user SET is_active=0 WHERE studio_id=" . $studio_id . " AND id !=" . $user_id;
                    $con->createCommand($sql_usr)->execute();

                    //Update studio status with hold period
                    $studio = Studios::model()->findByPk($studio_id);
                    $studio->status = 4;
                    $studio->is_subscribed = 0;
                    $studio->start_date = Date('Y-m-d H:i:s');
                    $studio->end_date = Date('Y-m-d H:i:s', strtotime("+{$config[0]['value']} days"));
                    $studio->payment_status = 0;
                    $studio->partial_failed_date = '';
                    $studio->partial_failed_due = 0;
                    $studio->save();

                    Yii::app()->email->cancelSubscriptionMailToSales($req);
                    Yii::app()->email->cancelSubscriptionMailToUser($req, '');
                }
            }
        }
    }

    function processStudioTransaction($card_info, $billing_amount) {
        $data = array();
        $data['token'] = $card_info->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
        $data['name'] = $card_info->card_holder_name;
        $data['card_type'] = $card_info->card_type;
        $data['exp'] = ($card_info->exp_month < 10) ? '0' . $card_info->exp_month . substr($card_info->exp_year, -2) : $card_info->exp_month . substr($card_info->exp_year, -2);
        $data['billing_amount'] = $billing_amount;

        if (IS_LIVE_FIRSTDATA) {
            $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
        } else {
            $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
        }

        $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
        $firstData->setTransArmorToken($data['token'])
                ->setCreditCardType($data['card_type'])
                ->setCreditCardName($data['name'])
                ->setCreditCardExpiration($data['exp'])
                ->setAmount($data['billing_amount']);

        $firstData->process();

        return $firstData;
    }

    /**
     * @method public UploadVideoNotDownloaded() If not downloaded then status will be changed
     * @author GDR<support@muvi.com>
     * @return bool true/false 
     */
    function actionUploadVideoNotDownloaded() {
        $this->layout = false;
        $v = explode('MUVIMUVI', $_REQUEST['stream_id']);
        $sql = 'SELECT f.name,f.uniq_id,ms.* FROM films f, movie_streams ms WHERE f.id = ms.movie_id AND ms.id = ' . $v[0] . ' AND f.uniq_id = "' . $v[1] . '"';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        if ($data) {
            $userSql = 'SELECT u.*,s.name company_name FROM user u, studios s WHERE s.id = u.studio_id AND s.id = ' . $data[0]['studio_id'];
            $userDetails = Yii::app()->db->createCommand($userSql)->queryAll();
            $qry = "UPDATE movie_streams ms JOIN films f SET ms.full_movie_url='',is_download_progress=0 WHERE  f.id = ms.movie_id AND ms.id=" . $v[0] . " AND f.uniq_id='" . $v[1] . "'";
            Yii::app()->db->createCommand($qry)->execute();
            //Send An email to the admin 
            $mailcontent = '<p>Dear ' . $userDetails[0]['user_first_name'] . ',</p>';
            $host_ip = Yii::app()->params['host_ip'];

            if (in_array(HOST_IP, $host_ip)) {
                $adminGroupEmail = array($userDetails[0]['user_email'], 'rasmi@muvi.com', 'gayadhar@muvi.com', 'mohan@muvi.com');
            } else {
                $adminGroupEmail = array('rasmi@muvi.com', 'gayadhar@muvi.com');

                $mailcontent.= '<p><strong>Note: This is the staging server.</strong></p>';
            }

            $mailcontent .= '<p>We are sorry, We are not able to fetch the video from the given URL. So Please check it again and submit the correct URL. Please contact your Muvi representative.</p>';
            $mailcontent .= '<p>URL: ' . $data[0]['url'] . '</p>';

            $mailcontent .= "<p>Regards,<br/>Team Muvi</p>";
            $site_url = Yii::app()->getBaseUrl(true);
            $logo = EMAIL_LOGO;
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
            $params = array(
                'website_name' => 'Muvi',
                'mailcontent' => $mailcontent,
                'logo' => $logo
            );

            $fromMail = 'studio@muvi.com';

            $adminSubject = "Video Download Status";

            $subject = $adminSubject;
            $to = $adminGroupEmail;


            $template_name = 'video_conversion';
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/video_conversion', array('params' => $params), true);
            $returnVal = $obj->sendmailViaAmazonsdk($thtml, $subject, $to, $fromMail, '', '', '', 'Muvi');
        } else {
            return false;
        }
    }

    function actionDisablePPV() {
        //Find out all active studios (Customer Only)
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;

                $sql = "SELECT US.* FROM ppv_subscriptions US WHERE US.studio_id = '" . $studio_id . "' AND US.status = 1 AND US.end_date < NOW()";
                $dbcon = Yii::app()->db;
                $ppvs = $dbcon->createCommand($sql)->queryAll();
                if (isset($ppvs) && !empty($ppvs)) {
                    foreach ($ppvs as $key1 => $ppv) {
                        if (trim($ppv['end_date']) && $ppv['end_date'] != '' && $ppv['end_date'] != '0000-00-00 00:00:00' && $ppv['end_date'] != '1970-01-01 00:00:00') {
                            $upd = "UPDATE ppv_subscriptions SET status=0 WHERE id=" . $ppv['id'];
                            $dbcon->createCommand($upd)->execute();
                        }
                    }
                }
            }
        }
    }

    function actionUpdatePaypalProfileStatus() {
        //echo '<pre>';
        //Find out all active studios
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->getPaypalproStudios($_REQUEST['studio']);
        } else {
            $studios = Studios::model()->getPaypalproStudios();
        }
        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $studio) {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio['studio_id']);

                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $studio_payment_gateway = $plan_payment_gateway['gateways'][0];
                    $this->setPaymentGatwayVariable($studio_payment_gateway);
                }

                $payment_gateway_controller = 'Api' . $studio['short_code'] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $resArray = $payment_gateway::profileDetails($studio['profile_id']);
                //print_r($resArray);

                $studio_id = $studio['studio_id'];
                $user_id = $studio['user_id'];
                $profile_status = strtoupper($resArray['STATUS']);

                //Getting user subscription detail to find out plan, card detail
                $usersub = new UserSubscription;
                $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
                //Getting card detail
                $card = new SdkCardInfos;
                $card = $card->findByPk($usersub->card_id);

                if (isset($profile_status) && $profile_status == 'CANCELLED') {
                    $payment_gateway_controller = 'Api' . $studio['short_code'] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $data = $payment_gateway::cancelCustomerAccount($usersub);

                    //Inactivate card detail of user
                    $card->is_cancelled = 2;
                    $card->cancelled_date = new CDbExpression("NOW()");
                    $card->save();

                    //Inactivate user subscription
                    $reason_id = 0;
                    $cancel_note = 'Paypal recurring profile cancelled.';

                    $usersub->status = 0;
                    $usersub->cancel_date = new CDbExpression("NOW()");
                    $usersub->cancel_reason_id = $reason_id;
                    $usersub->cancel_note = htmlspecialchars($cancel_note);
                    $usersub->save();

                    //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
                    $usr = new SdkUser;
                    $usr = $usr->findByPk($user_id);
                    $usr->is_deleted = 1;
                    $usr->deleted_at = new CDbExpression("NOW()");
                    $usr->save();

                    //Send an email to user
                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation', $file_name = '');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_cancellation', $studio_id);
                    if ($isEmailToStudio) {
                        $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_subscription_cancelled', $user_id, '', 0);
                    }
                } elseif (isset($profile_status) && $profile_status == 'ACTIVE') {

                    $ip_address = Yii::app()->request->getUserHostAddress();
                    $transaction = new Transaction;
                    $transaction->user_id = $user_id;
                    $transaction->studio_id = $studio_id;
                    $transaction->plan_id = $usersub->plan_id;
                    $transaction->transaction_date = new CDbExpression("NOW()");
                    $transaction->payment_method = $studio['short_code'];
                    $transaction->transaction_status = $resArray['ACK'];
                    $transaction->invoice_id = $resArray['CORRELATIONID'];
                    $transaction->order_number = $resArray['CORRELATIONID'];
                    $transaction->amount = $resArray['LASTPAYMENTAMT'];
                    $transaction->response_text = json_encode($resArray);
                    $transaction->subscription_id = $usersub->id;
                    $transaction->fullname = $resArray['SHIPTONAME'];
                    $transaction->address1 = $resArray['SHIPTOSTREET'];
                    $transaction->city = $resArray['SHIPTOCITY'];
                    $transaction->state = $resArray['SHIPTOSTATE'];
                    $transaction->country = $resArray['SHIPTOCOUNTRYNAME'];
                    $transaction->zip = $resArray['SHIPTOZIP'];
                    $transaction->ip = $ip_address;
                    $transaction->created_by = $user_id;
                    $transaction->created_date = new CDbExpression("NOW()");
                    $transaction->save();

                    //Getting plan detail
                    $plan = new SubscriptionPlans;
                    $plan = $plan->findByPk($usersub->plan_id);
                    $start_date = date('Y-m-d H:i:s', strtotime($resArray['LASTPAYMENTDATE']));
                    if ($plan->recurrence == 'Month') {
                        $end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($start_date)));
                    } elseif ($plan->recurrence == 'Year') {
                        $end_date = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($start_date)));
                    }

                    $usersub->is_success = 1;
                    $usersub->start_date = $start_date;
                    $usersub->end_date = $end_date;
                    $usersub->save();

                    $studioDetails = $studios = Studios::model()->findByPk($studio_id);
                    //Get invoice detail
                    $file_name = Yii::app()->pdf->invoiceDetial($studioDetails, $studio, $resArray['CORRELATIONID']);

                    //Send an email with invoice detail
                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name);
                    $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/paymentgateway.log', "a+");
                    fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . '-----Api' . $this->PAYMENT_GATEWAY . 'Controller/n' . $file_name);
                }
            }
        }
    }

    /**
     * 
     * @method public RaiseNewticket_frommail() This functionalirty has been resolved for the time being
     * as per discussion with Mr Anshuman on ticket no #3596
     * Cron Job has been stopped 
     * @author SD<support@muvi.in>
     */
      public function actionRaiseNewticket_frommail() {
        $res = Yii::app()->db->createCommand("SELECT last_updated_date FROM  `ticket_logging_date` where id = 2")->queryAll();
        $updated_date = date("d-M-Y", strtotime($res[0]['last_updated_date']));
        
        $inbox = imap_open(ticket_hostname, ticket_username, ticket_password) or die('Cannot connect to Gmail:' . imap_last_error());
        
        $all_emails = imap_search($inbox, "ALL UNDELETED SINCE \"$updated_date\""); // get all emails
        $reply_emails = imap_search($inbox, "ALL UNDELETED SUBJECT \"Re:*\" SINCE \"$updated_date\""); // get only reply emails
        //$reply_emails = imap_search($inbox, "ALL UNDELETED SUBJECT \"/^((?!Re:).)*$/\" SINCE \"$updated_date\""); // get only reply emails

        $email_diff1 = array_diff($all_emails, $reply_emails);
        $email_diff2 = array_diff($reply_emails, $all_emails);
        $emails = array_merge($email_diff1, $email_diff2);
        if(array_sum($reply_emails)== 0) 
                  $emails = $all_emails;
        
        if (!empty($emails)) {
            foreach ($emails as $email_number) {
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 1);
                $header = imap_headerinfo($inbox, $email_number);
                $fromaddr = trim($header->from[0]->mailbox) . "@" . trim($header->from[0]->host);

                $date_exp = explode(", ", $overview[0]->date);
                $last_date = date('Y-m-d H:i:s', strtotime($date_exp[1]));

                $date1 = strtotime($last_date);
                $date2 = strtotime($res[0]['last_updated_date']);

                $datetime1 = new DateTime($last_date);
                $datetime2 = new DateTime($res[0]['last_updated_date']);
                $fromaddr_arr = explode('@', $fromaddr);
                 if ($date1 > $date2 && $fromaddr_arr[1] != "muvi.com") {
                     $message = imap_fetchbody($inbox, $email_number, 1);
                     $structure = imap_fetchstructure($inbox, $email_number);
                             $attachments = array();
                            /** *********** check if any attachments found... ************ */
                            if (isset($structure->parts) && count($structure->parts)) {
                                for ($i = 0; $i < count($structure->parts); $i++) {
                                    $attachments[$i] = array(
                                        'is_attachment' => false,
                                        'filename' => '',
                                        'name' => '',
                                        'attachment' => ''
                                    );

                                    if ($structure->parts[$i]->ifdparameters) {
                                        foreach ($structure->parts[$i]->dparameters as $object) {
                                            if (strtolower($object->attribute) == 'filename') {
                                                $attachments[$i]['is_attachment'] = true;
                                                $attachments[$i]['filename'] = $object->value;
                                            }
                                        }
                                    }

                                    if ($structure->parts[$i]->ifparameters) {
                                        foreach ($structure->parts[$i]->parameters as $object) {
                                            if (strtolower($object->attribute) == 'name') {
                                                $attachments[$i]['is_attachment'] = true;
                                                $attachments[$i]['name'] = $object->value;
                                            }
                                        }
                                    }

                                    if ($attachments[$i]['is_attachment']) {
                                        $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);

                                        /* 3 = BASE64 encoding */
                                        if ($structure->parts[$i]->encoding == 3) {
                                            $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                        }
                                        /* 4 = QUOTED-PRINTABLE encoding */ elseif ($structure->parts[$i]->encoding == 4) {
                                            $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                        }
                                    }
                                }
                            }

                /* * *********** check if any attachments found... ************ */
                if (!empty($attachments[1]['attachment'])) {    
                    $message = imap_fetchbody($inbox, $email_number, 1.1);
                }
                $message = Yii::app()->imap->decode7Bit($message);
                $body_array = explode("\n", $message);
                $msg = "";
                foreach ($body_array as $key => $value) {

                    //remove hotmail sig
                    if ($value == "_________________________________________________________________") {
                        break;
                    } elseif (preg_match("/^(.*)--(.*)/i", $value, $matches)) {                        
                        break;
                    } elseif (preg_match("/^(.*)Sent from(.*)/i", $value, $matches)) {
                        break;
                    } elseif (preg_match("/^(.*)-*Original Message-*(.*)/i", $value, $matches)) {
                        break;

                        //check for date wrote string
                    } elseif (preg_match("/^On(.*)wrote:(.*)/i", $value, $matches)) {
                        break;

                        //check for best regards
                    } elseif (preg_match("/^(.*)Best regards(.*)/i", $value, $matches)) {
                        break;
                    } elseif (preg_match("/^(.*)Best Regards(.*)/i", $value, $matches)) {
                        break;
                        //check for From Email section
                    } elseif (preg_match("/^(.*)<support@muvi.com>(.*)wrote:(.*)/i", $value, $matches)) {
                        break;

                        //check for From Email section
                    } elseif (preg_match("/^(.*)<support@muvi.com>(.*)/i", $value, $matches)) {
                        break;
                    } elseif (preg_match("/^(.*)support@muvi.com(.*)/i", $value, $matches)) {
                        break;
                        //check for email with attachments
                    } elseif (preg_match("/^>--(.*)Content-Type:(.*)charset=(.*) /i", $value, $matches)) {
                        break;

                        //check for date wrote string with dashes
                    } elseif (preg_match("/^---(.*)On(.*)wrote:(.*)/i", $value, $matches)) {
                        break;
                        //add line to body
                    } else {
                        $msg .= $value . "\n";
                    }
                 }
                $message = $msg;
             /** ************ iterate through each attachment and upload attachments into s3 ********* */
                $all_attachment = array();
                foreach ($attachments as $attachment) {
                    if ($attachment['is_attachment'] == 1) {
                     $ticket_id = Yii::app()->db->createCommand("SELECT id FROM  `ticket` order by id desc limit 1")->queryAll();
                     $ticket_id = $ticket_id[0][id] + 1;
                        $all_attachment[] = $attachment['name'];
                        $filename = $attachment['name'];
                        if (empty($filename))
                            $filename = $attachment['filename'];

                        if (empty($filename))
                            $filename = time() . ".dat";
                        if (!empty($attachment['is_attachment']))
                            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/" . $attachment['filename'], 'w') or die('Cannot open file:  ' . $attachment['filename']);
                        fwrite($handle, $attachment['attachment']);
                        fclose($handle);
                        $src_path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/" . trim($filename);
                        $dest_file = "uploads/" . $ticket_id . "/" . trim($filename);
                        //defined bcket for ticketing system
                        $bucket = Yii::app()->params->s3_ticket_attachments;

                        $acl = 'public-read';

                        $client = S3Client::factory(array(
                                    'key' => Yii::app()->params->s3_key,
                                    'secret' => Yii::app()->params->s3_secret
                        ));

                        $result = $client->putObject(array(
                            'Bucket' => $bucket,
                            'Key' => $dest_file,
                            'SourceFile' => $src_path,
                            'ACL' => $acl,
                        ));
                        //print_r($result);
                        unlink($src_path);
                    }
                }
                /** ******* iterate through each attachment and upload attachments into s3 ********* */
                if (strpos($overview[0]->subject, "Re: Ticket") === false) {
                    
                    //$user = CronController::checkUserExists(trim($fromaddr));
                    //$ticket_master = TicketMaster::model()->findByAttributes(array('studio_email' => $fromaddr));
                    $ticket_master = Yii::app()->db->createCommand("SELECT * FROM  `ticket_master` where studio_email like '%$fromaddr%'  or studio_admin_email like '%$fromaddr%'")->queryAll();
                    $model = new TicketForm;
                    if (!empty($ticket_master)) {
                        $post['ticket_master_id'] = $ticket_master[0]['id'];
                        $ticket_master_user = Yii::app()->db->createCommand("SELECT count(*) as total FROM  `ticket_master` where studio_email like '%$fromaddr%' or studio_admin_email like '%$fromaddr%'")->queryAll();
                        if($ticket_master_user[0]['total'] > 1) {
                           $post['ticket_master_id'] = $ticket_master[0]['parent_id']; 
                        }
                        $post['title'] = $overview[0]->subject;
                        $post['description'] = $message;
                        $post['studio_id'] = $ticket_master[0]['studio_id'];
                        //$post['studio_id'] =(array('studio_id' => $user['studio_id']));
                        $post['priority'] = 'Medium';
                        $all_files = implode(",", $all_attachment); ;
                        $post['attachment'] = $all_files;
                        $post['attachment_url'] = "";
                        $ticketNumber = $model->insertTicket($post);
                        $ticketForm = TicketForm::model()->findByPk($ticketNumber);
                        $ticketForm->attachment_url = "https://dawkijsu20ml4.cloudfront.net/uploads/" . $ticketNumber.'/';
                        $return = $ticketForm->save();
                        Yii::app()->db->createCommand("UPDATE ticket_logging_date SET last_updated_date='" . $last_date . "' WHERE id = 2")->execute();
                        $subject = "Ticket_" . $ticketNumber ."_Created";
                        $subject1 = "Ticket_" . $ticketNumber . substr(str_replace("New Ticket", "_", $overview[0]->subject), 0, 15) . "_Created";

                        $url = Yii::app()->getBaseUrl(true);
                        $url1 = "<a target='_blank' href='".$url."/ticket/viewTicket/id/".$ticketNumber."'>$ticketNumber</a>";
                        $url2 = "<a target='_blank' href='https://admin.muvi.com/ticket/viewTicket/id/".$ticketNumber."'>$ticketNumber</a>";
                        $req['name'] = $ticket_master[0]['user_name'];
                        $req['email'] = $fromaddr;
                        $req['description'] = $message;
                        $req['url1'] = $url1;
                        $req['url2'] = $url2;
                        $req['priority'] = 'medium';
                        $req['ticket_id'] = $ticketNumber;
                        $req['subject'] = $subject;
                        
                        $returnVal = Yii::app()->email->RaiseNewticket_frommailtoadmin($req);
                        $returnVal2 = Yii::app()->email->RaiseNewticket_frommailtouser($req);
                        
                    } else {
                        echo "Its not a valid ticket user! - ".$fromaddr.'<br />';
                    }
                } 
                 }
            }
        }
       // Yii::app()->db->createCommand("Update ticket_logging_date SET last_updated_date='" . date('Y-m-d H:i:s') . "' WHERE id = 2")->execute();
        imap_close($inbox);
        exit();
    }

    public function actionUpdateTicketNotes() {

        $res = Yii::app()->db->createCommand("SELECT last_updated_date FROM  `ticket_logging_date` WHERE id = 1")->queryAll();

        $updated_date = date("d-M-y", strtotime($res[0]['last_updated_date']));


        $inbox = imap_open(ticket_hostname, ticket_username, ticket_password) or die('Cannot connect to Gmail:' . imap_last_error());

        if (empty($updated_date))
            $emails = imap_search($inbox, 'ALL');
        else
            $emails = imap_search($inbox, "ALL UNDELETED SINCE \"$updated_date\"");
        //$emails = imap_search($inbox,'UNSEEN "$updated_date"');


        if (!empty($emails)) {
            foreach ($emails as $email_number) {
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 0);
                $header = imap_headerinfo($inbox, $email_number);
                $domain_name = $header->from[0]->host;
                $fromaddr = trim($header->from[0]->mailbox) . "@" . trim($header->from[0]->host);
                /* get mail structure */
                $date_exp = explode(", ", $overview[0]->date);
                $last_date = date('Y-m-d H:i:s', strtotime($date_exp[1]));

                $date1 = strtotime($last_date);
                $date2 = strtotime($res[0]['last_updated_date']);

                if ($date1 > $date2 && $fromaddr != 'support@muvi.com') {
                    //ADD UPDATE TO EXISTING TICKET
                    if (strpos(strtolower($overview[0]->subject), strtolower("Re: Ticket")) !== false) {//echo 11111111111111111111;
                        $ticketNumber = explode('_', trim($overview[0]->subject));
                        $ticketNumber = preg_replace("/[^0-9]/", "", $ticketNumber[0]);
                        //attachment for file

                        $structure = imap_fetchstructure($inbox, $email_number);

                        $ticket = TicketForm::model()->findByPk($ticketNumber); //Yii::app()->db->createCommand("SELECT * FROM ticket WHERE id=$tktNo")->queryAll();

                        $studio_id = $ticket->studio_id;
                        if (!empty($ticket)) {

                            $attachments = array();

                            /*                             * *********** check if any attachments found... ************ */
                            if (isset($structure->parts) && count($structure->parts)) {
                                for ($i = 0; $i < count($structure->parts); $i++) {
                                    $attachments[$i] = array(
                                        'is_attachment' => false,
                                        'filename' => '',
                                        'name' => '',
                                        'attachment' => ''
                                    );

                                    if ($structure->parts[$i]->ifdparameters) {
                                        foreach ($structure->parts[$i]->dparameters as $object) {
                                            if (strtolower($object->attribute) == 'filename') {
                                                $attachments[$i]['is_attachment'] = true;
                                                $attachments[$i]['filename'] = $object->value;
                                            }
                                        }
                                    }

                                    if ($structure->parts[$i]->ifparameters) {
                                        foreach ($structure->parts[$i]->parameters as $object) {
                                            if (strtolower($object->attribute) == 'name') {
                                                $attachments[$i]['is_attachment'] = true;
                                                $attachments[$i]['name'] = $object->value;
                                            }
                                        }
                                    }

                                    if ($attachments[$i]['is_attachment']) {
                                        $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);

                                        /* 3 = BASE64 encoding */
                                        if ($structure->parts[$i]->encoding == 3) {
                                            $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                        }
                                        /* 4 = QUOTED-PRINTABLE encoding */ elseif ($structure->parts[$i]->encoding == 4) {
                                            $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                        }
                                    }
                                }
                            }

                            /*                             * *********** check if any attachments found... ************ */



                            $note['id_ticket'] = $ticketNumber;
                            $subject = explode(':', trim($overview[0]->subject));

                            $added_by = substr($subject[1], strpos($subject[1], '_') + 1);

                            $note_added_by = substr($added_by, 0, strpos($added_by, '_'));
                            echo "SELECT id,first_name,email,studio_id FROM  `user` WHERE  `email` LIKE '" . trim($fromaddr);
                            $user = Yii::app()->db->createCommand("SELECT id,first_name,email,studio_id FROM  `user` WHERE  `email` LIKE '" . trim($fromaddr) . "' and studio_id=" . $studio_id)->queryAll();

                            if (count($user)) {
                                $userEmailId = $user[0]['email'];
                                //$added_by=$note['added_by'] = $userEmailId != trim($fromaddr) ? 1 : $user[0]['id'];
                                $added_by = $note['added_by'] = $user[0]['id'];
                                $note['add_date'] = date('Y-m-d H:i:s');

                                $note['updated_date'] = date('Y-m-d H:i:s');
                                $note['added_via'] = 'email';
                                // $note['id_user'] = $userEmailId != trim($fromaddr) ? 1 : $user[0]['studio_id'];
                                $note['id_user'] = $user[0]['studio_id'];
                                $message = imap_fetchbody($inbox, $email_number, 1);

                                if (!empty($attachments['attachment'])) {
                                    $message = imap_fetchbody($inbox, $email_number, 1.1);
                                }
                                $message = Yii::app()->imap->decode7Bit($message);
                                //get rid of any quoted text in the email body
                                $body_array = explode("\n", $message);
                                $msg = "";
                                $fromEmail = 'support@muvi.com';
                                
                                if (strpos($body_array[0], "<hr />")) {
                                    $value = $body_array[0];
                                    unset($body_array);
                                    $body_array[0] = substr($value, 0, strpos($value, "<hr />"));

                                }
                                foreach ($body_array as $key => $value) {

                                    //remove hotmail sig
                                    if ($value == "_________________________________________________________________") {
                                        break;
                                    } elseif (preg_match("/^(.*)Sent from(.*)/i", $value, $matches)) {
                                        break;
                                    } elseif (preg_match("/^(.*)-*Original Message-*(.*)/i", $value, $matches)) {
                                        break;

                                        //check for date wrote string
                                    } elseif (preg_match("/^On(.*)wrote:(.*)/i", $value, $matches)) {
                                        break;

                                        //check for best regards
                                    } elseif (preg_match("/^(.*)Best regards(.*)/i", $value, $matches)) {
                                        break;
                                    } elseif (preg_match("/^(.*)Best Regards(.*)/i", $value, $matches)) {
                                        break;
                                        //check for From Email section
                                    } elseif (preg_match("/^(.*)<support@muvi.com>(.*)wrote:(.*)/i", $value, $matches)) {
                                        break;

                                        //check for From Email section
                                    } elseif (preg_match("/^(.*)<support@muvi.com>(.*)/i", $value, $matches)) {
                                        break;
                                    } elseif (preg_match("/^(.*)support@muvi.com(.*)/i", $value, $matches)) {
                                        break;
                                        //check for email with attachments
                                    } elseif (preg_match("/^>--(.*)Content-Type:(.*)charset=(.*) /i", $value, $matches)) {
                                        break;

                                        //check for date wrote string with dashes
                                    } elseif (preg_match("/^---(.*)On(.*)wrote:(.*)/i", $value, $matches)) {
                                        break;
                                        //add line to body

                                    } else {
                                        $msg .= $value . "\n";
                                    }
                                }

                                $updated_message = $note['note'] = addslashes($msg);
                                /*                                 * ************ iterate through each attachment and upload attachments into s3 ********* */

                                $all_attachment = array();
                                foreach ($attachments as $attachment) {
                                    if ($attachment['is_attachment'] == 1) {
                                        $all_attachment[] = $attachment['name'];
                                        $filename = $attachment['name'];
                                        if (empty($filename))
                                            $filename = $attachment['filename'];

                                        if (empty($filename))
                                            $filename = time() . ".dat";
                                        if (!empty($attachment['is_attachment']))
                                            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/" . $attachment['filename'], 'w') or die('Cannot open file:  ' . $attachment['filename']);
                                        fwrite($handle, $attachment['attachment']);
                                        fclose($handle);
                                        $src_path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/" . trim($filename);
                                        $dest_file = "uploads/" . $ticketNumber . "/" . trim($filename);
                                        $studio_id = $ticket->studio_id;
                                        //defined bcket for ticketing system
                                        $bucket = Yii::app()->params->s3_ticket_attachments;

                                        $acl = 'public-read';
                                        $studio_id = $ticket->studio_id;

                                        $client = S3Client::factory(array(
                                                    'key' => Yii::app()->params->s3_key,
                                                    'secret' => Yii::app()->params->s3_secret
                                        ));

                                        $result = $client->putObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => $dest_file,
                                            'SourceFile' => $src_path,
                                            'ACL' => $acl,
                                        ));
                                        unlink($src_path);
                                    }
                                }
                                /*                                 * ******* iterate through each attachment and upload attachments into s3 ********* */
                                $all_files = implode(",", $all_attachment);
                                $note['attachment'] = $all_files;
                                $note['attachment_url'] = "https://dawkijsu20ml4.cloudfront.net/uploads/" . $ticketNumber . "/";


                                $query = "INSERT INTO ticket_notes ";
                                $k = "(id,";
                                $value = "'',";
                                foreach ($note as $key => $val) {
                                    $k.="$key" . ",";
                                    $value.= "'" . $val . "',";
                                }

                                $k = trim($k, ',') . ")";
                                $value = trim($value, ',');
                                $query.="$k VALUES($value)";

                                Yii::app()->db->createCommand($query)->execute();
                                $tkt['id_ticket'] = $ticketNumber;
                                //$tkt['last_updated_by'] = $userEmailId != $fromaddr ? 1 : $user[0]['id'];
                                $tkt['last_updated_by'] = $user[0]['id'];
                                $model = new TicketForm();
                                $model->updateTicket($tkt);

                                $ticketDetails = $model->ticketList("id=" . $ticketNumber);

                                $html = $this->ticketDetails($ticketDetails);
                                $mailsubject = "Ticket#" . $ticketNumber . "_" . ucfirst($user[0]['first_name']) . "_" . addslashes(substr($msg, 0, 15));

                                $std = new User();
                                $userInfo = $std->findByPk($user[0]['id']);

                                $name = $userInfo->first_name;

                                $subject = "Ticket" . $ticketNumber . "_" . addslashes(substr($subject[1], 0, 15)) . " Updated";
                                $url = Yii::app()->getBaseUrl(true);
                                $url1 = "<a target='_blank' href='" . $url . "'>Muvi</a>";
                                $url2 = "<a target='_blank' href='" . $url . "'>here</a>";
                                $req['name'] = $userInfo->first_name;
                                $req['email'] = $userInfo->email;
                                $req['description'] = $updated_message;
                                $req['url1'] = $url1;
                                $req['url2'] = $url2;
                                $req['priority'] = 'medium';
                                $req['ticket_id'] = $ticketNumber;
                                $req['subject'] = $subject;
                                echo $added_by;
                                if ($added_by != 1) {
                                    $returnVal = Yii::app()->email->addnote_frommailtoadmin($req);
                                }
                                /* Restricted to update ticket by super admin.
                                 * else
                                  {
                                  $returnVal2 = Yii::app()->email->addnote_frommailtouser($req);
                                  } */
                            }
                        }
                    } else {
                        echo "$ticketNumber not found, it might have been deleted<br />";
                    }
                }
            }
        }
        Yii::app()->db->createCommand("Update ticket_logging_date SET last_updated_date='" . date('Y-m-d H:i:s') . "' WHERE id=1")->execute();
        imap_close($inbox);
    }

    public function actionUpdateTestTicketNotes() {
        $res = Yii::app()->db->createCommand("SELECT last_updated_date FROM  `ticket_logging_date`")->queryAll();

        $updated_date = date("d-M-y", strtotime($res[0]['last_updated_date']));


        $inbox = imap_open(ticket_hostname, ticket_username2, testmuvi123) or die('Cannot connect to Gmail:' . imap_last_error());

        if (empty($updated_date))
            $emails = imap_search($inbox, 'ALL');
        else
            $emails = imap_search($inbox, "ALL UNDELETED SINCE \"$updated_date\"");
        //$emails = imap_search($inbox,'UNSEEN "$updated_date"');


        if (!empty($emails)) {
            foreach ($emails as $email_number) {
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 0);
                $header = imap_headerinfo($inbox, $email_number);
                $domain_name = $header->from[0]->host;
                $fromaddr = trim($header->from[0]->mailbox) . "@" . trim($header->from[0]->host);
                /* get mail structure */
                $date_exp = explode(", ", $overview[0]->date);
                $last_date = date('Y-m-d h:i:s', strtotime($date_exp[1]));

                $date1 = strtotime($last_date);
                $date2 = strtotime($res[0]['last_updated_date']);

                if ($date1 > $date2 && $fromaddr != 'support@muvi.com') {
                    //ADD UPDATE TO EXISTING TICKET
                    if (strpos($overview[0]->subject, "Re: Ticket") !== false) {//echo 11111111111111111111;
                        $ticketNumber = explode('_', trim($overview[0]->subject));
                        $ticketNumber = preg_replace("/[^0-9]/", "", $ticketNumber[0]);
                        //attachment for file

                        $structure = imap_fetchstructure($inbox, $email_number);

                        $ticket = TicketForm::model()->findByPk($ticketNumber); //Yii::app()->db->createCommand("SELECT * FROM ticket WHERE id=$tktNo")->queryAll();

                        $studio_id = $ticket->studio_id;
                        if (!empty($ticket)) {

                            $attachments = array();

                            /*                             * *********** check if any attachments found... ************ */
                            if (isset($structure->parts) && count($structure->parts)) {
                                for ($i = 0; $i < count($structure->parts); $i++) {
                                    $attachments[$i] = array(
                                        'is_attachment' => false,
                                        'filename' => '',
                                        'name' => '',
                                        'attachment' => ''
                                    );

                                    if ($structure->parts[$i]->ifdparameters) {
                                        foreach ($structure->parts[$i]->dparameters as $object) {
                                            if (strtolower($object->attribute) == 'filename') {
                                                $attachments[$i]['is_attachment'] = true;
                                                $attachments[$i]['filename'] = $object->value;
                                            }
                                        }
                                    }

                                    if ($structure->parts[$i]->ifparameters) {
                                        foreach ($structure->parts[$i]->parameters as $object) {
                                            if (strtolower($object->attribute) == 'name') {
                                                $attachments[$i]['is_attachment'] = true;
                                                $attachments[$i]['name'] = $object->value;
                                            }
                                        }
                                    }

                                    if ($attachments[$i]['is_attachment']) {
                                        $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);

                                        /* 3 = BASE64 encoding */
                                        if ($structure->parts[$i]->encoding == 3) {
                                            $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                        }
                                        /* 4 = QUOTED-PRINTABLE encoding */ elseif ($structure->parts[$i]->encoding == 4) {
                                            $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                        }
                                    }
                                }
                            }

                            /*                             * *********** check if any attachments found... ************ */



                            $note['id_ticket'] = $ticketNumber;
                            $subject = explode(':', trim($overview[0]->subject));

                            $added_by = substr($subject[1], strpos($subject[1], '_') + 1);

                            $note_added_by = substr($added_by, 0, strpos($added_by, '_'));
                            echo "SELECT id,first_name,email,studio_id FROM  `user` WHERE  `email` LIKE '" . trim($fromaddr);
                            $user = Yii::app()->db->createCommand("SELECT id,first_name,email,studio_id FROM  `user` WHERE  `email` LIKE '" . trim($fromaddr) . "' and studio_id=" . $studio_id)->queryAll();

                            if (count($user)) {
                                $userEmailId = $user[0]['email'];
                                //$added_by=$note['added_by'] = $userEmailId != trim($fromaddr) ? 1 : $user[0]['id'];
                                $added_by = $note['added_by'] = $user[0]['id'];
                                $note['add_date'] = date('Y-m-d H:i:s');

                                $note['updated_date'] = date('Y-m-d H:i:s');
                                $note['added_via'] = 'email';
                                // $note['id_user'] = $userEmailId != trim($fromaddr) ? 1 : $user[0]['studio_id'];
                                $note['id_user'] = $user[0]['studio_id'];
                                $message = imap_fetchbody($inbox, $email_number, 1);

                                if (!empty($attachments['attachment'])) {
                                    $message = imap_fetchbody($inbox, $email_number, 1.1);
                                }

                                //get rid of any quoted text in the email body
                                $body_array = explode("\n", $message);
                                $msg = "";
                                $fromEmail = 'support@muvi.com';
                                foreach ($body_array as $key => $value) {

                                    //remove hotmail sig
                                    if ($value == "_________________________________________________________________") {
                                        break;

                                        //original message quote
                                    } elseif (preg_match("/^-*(.*)Original Message(.*)-*/i", $value, $matches)) {
                                        break;

                                        //check for date wrote string
                                    } elseif (preg_match("/^On(.*)wrote:(.*)/i", $value, $matches)) {
                                        break;
                                    }
                                    //check for From Name email section Not necessary for our requirement

                                    /* elseif(preg_match("/^On(.*)$fromName(.*)/i",$value,$matches)) {
                                      break;

                                      //check for To Name email section
                                      } elseif(preg_match("/^On(.*)$toName(.*)/i",$value,$matches)) {
                                      break;

                                      //check for To Email email section
                                      } elseif(preg_match("/^(.*)$toEmail(.*)wrote:(.*)/i",$value,$matches)) {
                                      break;
                                      } */
                                    //check for From Email email section
                                    elseif (preg_match("/^(.*)$fromEmail(.*)wrote:(.*)/i", $value, $matches)) {
                                        break;

                                        //check for quoted ">" section
                                    } elseif (preg_match("/^>(.*)/i", $value, $matches)) {
                                        break;
                                        //check for email with attachments
                                    } elseif (preg_match("/^>--(.*)Content-Type:(.*)charset=(.*) /i", $value, $matches)) {
                                        break;

                                        //check for date wrote string with dashes
                                    } elseif (preg_match("/^---(.*)On(.*)wrote:(.*)/i", $value, $matches)) {
                                        break;

                                        //add line to body
                                    } else {
                                        $msg .= "$value\n";
                                    }
                                }

                                $updated_message = $note['note'] = $msg;
                                /*                                 * ************ iterate through each attachment and upload attachments into s3 ********* */

                                $all_attachment = array();
                                foreach ($attachments as $attachment) {
                                    if ($attachment['is_attachment'] == 1) {
                                        $all_attachment[] = $attachment['name'];
                                        $filename = $attachment['name'];
                                        if (empty($filename))
                                            $filename = $attachment['filename'];

                                        if (empty($filename))
                                            $filename = time() . ".dat";
                                        if (!empty($attachment['is_attachment']))
                                            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/" . $attachment['filename'], 'w') or die('Cannot open file:  ' . $attachment['filename']);
                                        fwrite($handle, $attachment['attachment']);
                                        fclose($handle);
                                        $src_path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/" . trim($filename);
                                        $dest_file = "uploads/" . $ticketNumber . "/" . trim($filename);
                                        $studio_id = $ticket->studio_id;
                                        //defined bcket for ticketing system
                                        $bucket = Yii::app()->params->s3_ticket_attachments;

                                        $acl = 'public-read';
                                        $studio_id = $ticket->studio_id;

                                        $client = S3Client::factory(array(
                                                    'key' => Yii::app()->params->s3_key,
                                                    'secret' => Yii::app()->params->s3_secret
                                        ));

                                        $result = $client->putObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => $dest_file,
                                            'SourceFile' => $src_path,
                                            'ACL' => $acl,
                                        ));
                                        unlink($src_path);
                                    }
                                }
                                /*                                 * ******* iterate through each attachment and upload attachments into s3 ********* */
                                $all_files = implode(",", $all_attachment);
                                $note['attachment'] = $all_files;
                                $note['attachment_url'] = "https://dawkijsu20ml4.cloudfront.net/uploads/" . $ticketNumber . "/";


                                $query = "INSERT INTO ticket_notes ";
                                $k = "(id,";
                                $value = "'',";
                                foreach ($note as $key => $val) {
                                    $k.="$key" . ",";
                                    $value.= "'" . $val . "',";
                                }

                                $k = trim($k, ',') . ")";
                                $value = trim($value, ',');
                                $query.="$k VALUES($value)";

                                Yii::app()->db->createCommand($query)->execute();
                                $tkt['id_ticket'] = $ticketNumber;
                                //$tkt['last_updated_by'] = $userEmailId != $fromaddr ? 1 : $user[0]['id'];
                                $tkt['last_updated_by'] = $user[0]['id'];
                                $model = new TicketForm();
                                $model->updateTicket($tkt);

                                $ticketDetails = $model->ticketList("id=" . $ticketNumber);

                                $html = $this->ticketDetails($ticketDetails);
                                $mailsubject = "Ticket#" . $ticketNumber . "_" . ucfirst($user[0]['first_name']) . "_" . addslashes(substr($msg, 0, 15));

                                $std = new User();
                                $userInfo = $std->findByPk($user[0]['id']);

                                $name = $userInfo->first_name;

                                $subject = "Ticket" . $ticketNumber . "_" . addslashes(substr($subject[1], 0, 15)) . " Updated";
                                $url = Yii::app()->getBaseUrl(true);
                                $url1 = "<a target='_blank' href='" . $url . "'>Muvi</a>";
                                $url2 = "<a target='_blank' href='" . $url . "'>here</a>";
                                $req['name'] = $userInfo->first_name;
                                $req['email'] = $userInfo->email;
                                $req['description'] = $updated_message;
                                $req['url1'] = $url1;
                                $req['url2'] = $url2;
                                $req['priority'] = 'medium';
                                $req['ticket_id'] = $ticketNumber;
                                $req['subject'] = $subject;
                                echo $added_by;
                                if ($added_by != 1) {
                                    $returnVal = Yii::app()->email->addnote_frommailtoadmin($req);
                                }
                                /* Restricted to update ticket by super admin.
                                 * else
                                  {
                                  $returnVal2 = Yii::app()->email->addnote_frommailtouser($req);
                                  } */
                            }
                        }
                    } else {
                        echo "$ticketNumber not found, it might have been deleted<br />";
                    }
                }
            }
        }
        Yii::app()->db->createCommand("Update ticket_logging_date SET last_updated_date='" . date('Y-m-d H:i:s') . "' WHERE 1")->execute();
        imap_close($inbox);
    }

    function checkUserExists($from) {
        $user = User::model()->findByAttributes(array('email' => $from));
        return $user;
    }

    function actionvideoEncodeingMail() {
        $studioDataForMovie = Yii::app()->db->createCommand()
                ->select('ms.studio_id as studioId,s.name as studioName')
                ->from('movie_streams ms, studios s')
                ->where('ms.studio_id = s.id and (ms.upload_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or ms.encoding_start_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or ms.encoding_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or ms.encode_fail_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or ((ms.has_sh = 0 OR ms.has_sh = 1) AND ms.is_converted = 0 AND ms.full_movie !="" AND (ms.encoding_end_time IS NULL or ms.encoding_end_time = "0000-00-00 00:00:00")) '
                        . 'or (ms.is_download_progress=1 AND ms.is_converted = 0 AND (ms.full_movie ="" or ms.full_movie is NULL) AND ms.full_movie_url!="" AND (ms.encoding_end_time IS NULL or ms.encoding_end_time = "0000-00-00 00:00:00")))')
                ->group('ms.studio_id')
                ->queryAll();
        $studioDataForTrailer = Yii::app()->db->createCommand()
                ->select('f.studio_id as studioId,s.name as studioName,group_concat(mt.id) as trailer_id')
                ->from('movie_trailer mt, films f, studios s')
                ->where('f.studio_id = s.id and mt.movie_id = f.id and (mt.upload_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or mt.encoding_start_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or mt.encoding_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or mt.encode_fail_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) '
                        . 'or (mt.has_sh = 1 AND mt.is_converted = 0 AND (mt.encoding_end_time IS NULL or mt.encoding_end_time = "0000-00-00 00:00:00")))')
                ->group('studio_id')
                ->queryAll();

        $movie = '';
        $trailer = '';
        $addContentToTable = '';
        if (HOST_IP == '52.211.41.107') {
            $addContentToTable = "<tr style='text-align:center'><td colspan='6'><b>Sony Data</b></td></tr>";
        }
        if (HOST_IP == '52.77.47.122') {
            $addContentToTable = "<tr style='text-align:center'><td colspan='6'><b>Digi Osmosis LLP</b></td></tr>";
        }
        $movie .= "<table border='1' cellspacing='0' cellpadding='10' >"
                . $addContentToTable
                . "<tr style='text-align:center'><td colspan='6'><b>Movie</b></td></tr>"
                . "<tr><th>Studio Name</th><th>Uploaded</th><th>Encoding Started</th><th>Encoding Completed</th><th>Encoding Pending</th><th>Encoding Failed</th></tr>";
        if ($studioDataForMovie) {
            $totalvideoCount = 0;
            $totalencodingStart = 0;
            $totalencodingCompleted = 0;
            $totalencodingPending = 0;
            $totalvideoNotencoded = 0;
            foreach ($studioDataForMovie as $studioDataForMovieKey => $studioDataForMovieVal) {
                $videoUploaded = Yii::app()->db->createCommand()
                        ->select('count(*) as videocount')
                        ->from('movie_streams')
                        ->where('upload_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) and studio_id =' . $studioDataForMovieVal['studioId'])
                        ->queryAll();
                $videoEncodedStart = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEnccount')
                        ->from('movie_streams')
                        ->where('encoding_start_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) and studio_id =' . $studioDataForMovieVal['studioId'])
                        ->queryAll();
                $videoEncodedPend = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEncPendcount')
                        ->from('movie_streams')
                        ->where('(((has_sh = 0 OR has_sh = 1) AND full_movie!="") or (is_download_progress =1 AND (full_movie = "" or full_movie is NULL) AND full_movie_url !="")) AND is_converted = 0 AND (encoding_end_time IS NULL or  encoding_end_time = "0000-00-00 00:00:00") AND studio_id =' . $studioDataForMovieVal['studioId'])
                        ->queryAll();
                $videoEncodedEnd = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEncEndcount')
                        ->from('movie_streams')
                        ->where('encoding_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) and studio_id =' . $studioDataForMovieVal['studioId'])
                        ->queryAll();
                $videoEncodedFail = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEncFailcount')
                        ->from('movie_streams')
                        ->where('encode_fail_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) and studio_id =' . $studioDataForMovieVal['studioId'])
                        ->queryAll();

                $videoCount = 0;
                if (isset($videoUploaded[0]['videocount'])) {
                    $videoCount = $videoUploaded[0]['videocount'];
                }
                $encodingStart = 0;
                if (isset($videoEncodedStart[0]['videoEnccount'])) {
                    $encodingStart = $videoEncodedStart[0]['videoEnccount'];
                }
                $encodingCompleted = 0;
                if (isset($videoEncodedEnd[0]['videoEncEndcount'])) {
                    $encodingCompleted = $videoEncodedEnd[0]['videoEncEndcount'];
                }
                $encodingPending = 0;
                if (isset($videoEncodedPend[0]['videoEncPendcount'])) {
                    $encodingPending = $videoEncodedPend[0]['videoEncPendcount'];
                }
                $videoNotencoded = 0;
                if (isset($videoEncodedFail[0]['videoEncFailcount'])) {
                    $videoNotencoded = $videoEncodedFail[0]['videoEncFailcount'];
                }
                $totalvideoCount = $totalvideoCount + $videoCount;
                $totalencodingStart = $totalencodingStart + $encodingStart;
                $totalencodingCompleted = $totalencodingCompleted + $encodingCompleted;
                $totalencodingPending = $totalencodingPending + $encodingPending;
                $totalvideoNotencoded = $totalvideoNotencoded + $videoNotencoded;
                $movie .= "<tr>"
                        . "<td>" . $studioDataForMovieVal['studioName'] . "</td>"
                        . "<td style='text-align:center'>" . $videoCount . "</td>"
                        . "<td style='text-align:center'>" . $encodingStart . "</td>"
                        . "<td style='text-align:center'>" . $encodingCompleted . "</td>"
                        . "<td style='text-align:center'>" . $encodingPending . "</td>"
                        . "<td style='text-align:center'>" . $videoNotencoded . "</td>"
                        . "</tr>";
            }
            $movie .= "<tr style='font-weight:bold;'><td>Total</td><td style='text-align:center'>" . $totalvideoCount . "</td>"
                    . "<td style='text-align:center'>" . $totalencodingStart . "</td>"
                    . "<td style='text-align:center'>" . $totalencodingCompleted . "</td>"
                    . "<td style='text-align:center'>" . $totalencodingPending . "</td>"
                    . "<td style='text-align:center'>" . $totalvideoNotencoded . "</td></tr>";
        } else {
            $movie .= "<tr style='text-align:center'><td colspan='6'>No Video Uploaded</td></tr>";
        }
        $movie .= "</table>";
        $trailer .= "<table border='1' cellspacing='0' cellpadding='10' style='margin-top:15px;' >"
                . "<tr style='text-align:center'><td colspan='6'><b>Trailer</b></td></tr>"
                . "<tr><th>Studio Name</th><th>Uploaded</th><th>Encoding Started</th><th>Encoding Completed</th><th>Encoding Pending</th><th>Encoding Failed</th></tr>";
        if ($studioDataForTrailer) {
            $totalvideoCount = 0;
            $totalencodingStart = 0;
            $totalencodingCompleted = 0;
            $totalencodingPending = 0;
            $totalvideoNotencoded = 0;
            foreach ($studioDataForTrailer as $studioDataForTrailerKey => $studioDataForTrailerVal) {
                $videoUploaded = Yii::app()->db->createCommand()
                        ->select('count(*) as videocount')
                        ->from('movie_trailer')
                        ->where('upload_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) AND id IN (' . $studioDataForTrailerVal['trailer_id'] . ')')
                        ->queryAll();
                $videoEncodedStart = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEnccount')
                        ->from('movie_trailer')
                        ->where('encoding_start_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) AND id IN (' . $studioDataForTrailerVal['trailer_id'] . ')')
                        ->queryAll();
                $videoEncodedPend = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEncPendcount')
                        ->from('movie_trailer')
                        ->where('has_sh = 1 AND is_converted = 0 AND (encoding_end_time IS NULL or  encoding_end_time = "0000-00-00 00:00:00") AND id IN (' . $studioDataForTrailerVal['trailer_id'] . ')')
                        ->queryAll();
                $videoEncodedEnd = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEncEndcount')
                        ->from('movie_trailer')
                        ->where('encoding_end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) AND id IN (' . $studioDataForTrailerVal['trailer_id'] . ')')
                        ->queryAll();
                $videoEncodedFail = Yii::app()->db->createCommand()
                        ->select('count(*) as videoEncFailcount')
                        ->from('movie_trailer')
                        ->where('encode_fail_time > DATE_SUB(NOW(), INTERVAL 3 HOUR) AND id IN (' . $studioDataForTrailerVal['trailer_id'] . ')')
                        ->queryAll();

                $trailervideoCount = 0;
                if (isset($videoUploaded[0]['videocount'])) {
                    $trailervideoCount = $videoUploaded[0]['videocount'];
                }
                $trailerencodingStart = 0;
                if (isset($videoEncodedStart[0]['videoEnccount'])) {
                    $trailerencodingStart = $videoEncodedStart[0]['videoEnccount'];
                }
                $trailerencodingCompleted = 0;
                if (isset($videoEncodedEnd[0]['videoEncEndcount'])) {
                    $trailerencodingCompleted = $videoEncodedEnd[0]['videoEncEndcount'];
                }
                $encodingPending = 0;
                if (isset($videoEncodedPend[0]['videoEncPendcount'])) {
                    $encodingPending = $videoEncodedPend[0]['videoEncPendcount'];
                }
                $trailervideoNotencoded = 0;
                if (isset($videoEncodedFail[0]['videoEncFailcount'])) {
                    $trailervideoNotencoded = $videoEncodedFail[0]['videoEncFailcount'];
                }
                $totalvideoCount = $totalvideoCount + $trailervideoCount;
                $totalencodingStart = $totalencodingStart + $trailerencodingStart;
                $totalencodingCompleted = $totalencodingCompleted + $trailerencodingCompleted;
                $totalencodingPending = $totalencodingPending + $encodingPending;
                $totalvideoNotencoded = $totalvideoNotencoded + $trailervideoNotencoded;
                $trailer .= "<tr>"
                        . "<td>" . $studioDataForTrailerVal['studioName'] . "</td>"
                        . "<td style='text-align:center'>" . $trailervideoCount . "</td>"
                        . "<td style='text-align:center'>" . $trailerencodingStart . "</td>"
                        . "<td style='text-align:center'>" . $trailerencodingCompleted . "</td>"
                        . "<td style='text-align:center'>" . $encodingPending . "</td>"
                        . "<td style='text-align:center'>" . $trailervideoNotencoded . "</td>"
                        . "</tr>";
            }
            $trailer .= "<tr style='font-weight:bold;'><td><b>Total</b></td><td style='text-align:center'>" . $totalvideoCount . "</td>"
                    . "<td style='text-align:center'>" . $totalencodingStart . "</td>"
                    . "<td style='text-align:center'>" . $totalencodingCompleted . "</td>"
                    . "<td style='text-align:center'>" . $totalencodingPending . "</td>"
                    . "<td style='text-align:center'>" . $totalvideoNotencoded . "</td></tr>"
                    . "</table>";
        } else {
            $trailer .= "<tr style='text-align:center'><td colspan='6'>No Video Uploaded</td></tr>";
        }
        $trailer .= "</table>";
        $mailcontent.="<p>Hi All,</p>";
        $mailcontent.= $movie . $trailer;
        $mailcontent .= "<p>Regards,<br/>Team Muvi</p>";
        Yii::app()->email->videoEncoding($mailcontent);
    }

    public function actionManuallChargePaypal() {
        //print_r($_REQUEST);
        echo '<pre>';
        $studio_id = $_REQUEST['studio_id'];
        $user_id = $_REQUEST['user_id'];
        $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
        //print_r($plan_payment_gateway); exit; 
        //echo $plan_payment_gateway['gateways'][0]->short_code;exit;
        if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
            $studio_payment_gateway = $plan_payment_gateway['gateways'][0];
            $this->setPaymentGatwayVariable($studio_payment_gateway);
        }
        $payment_gateway_controller = 'Api' . $plan_payment_gateway['gateways'][0]->short_code . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $resArray = $payment_gateway::profileDetails($_REQUEST['profile_id']);

        $profile_status = strtoupper($resArray['STATUS']);

        $usersub = new UserSubscription;
        $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
        //Getting card detail
        $card = new SdkCardInfos;
        $card = $card->findByPk($usersub->card_id);


        if (isset($profile_status) && $profile_status == 'CANCELLED') {
            $payment_gateway_controller = 'Api' . $plan_payment_gateway['gateways'][0]->short_code . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::cancelCustomerAccount($usersub);

            //Inactivate card detail of user
            $card->is_cancelled = 2;
            $card->cancelled_date = new CDbExpression("NOW()");
            $card->save();

            //Inactivate user subscription
            $reason_id = 0;
            $cancel_note = 'Paypal recurring profile cancelled.';

            $usersub->status = 0;
            $usersub->cancel_date = new CDbExpression("NOW()");
            $usersub->cancel_reason_id = $reason_id;
            $usersub->cancel_note = htmlspecialchars($cancel_note);
            $usersub->save();

            //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
            $usr = new SdkUser;
            $usr = $usr->findByPk($user_id);
            $usr->is_deleted = 1;
            $usr->deleted_at = new CDbExpression("NOW()");
            $usr->save();
        } elseif (isset($profile_status) && $profile_status == 'ACTIVE') {

            $ip_address = Yii::app()->request->getUserHostAddress();
            $transaction = new Transaction;
            $transaction->user_id = $user_id;
            $transaction->studio_id = $studio_id;
            $transaction->plan_id = $usersub->plan_id;
            $transaction->transaction_date = new CDbExpression("NOW()");
            $transaction->payment_method = $plan_payment_gateway['gateways'][0]->short_code;
            $transaction->transaction_status = $resArray['ACK'];
            $transaction->invoice_id = $resArray['CORRELATIONID'];
            $transaction->order_number = $resArray['CORRELATIONID'];
            $transaction->amount = $resArray['LASTPAYMENTAMT'];
            $transaction->response_text = json_encode($resArray);
            $transaction->subscription_id = $usersub->id;
            $transaction->fullname = $resArray['SHIPTONAME'];
            $transaction->address1 = $resArray['SHIPTOSTREET'];
            $transaction->city = $resArray['SHIPTOCITY'];
            $transaction->state = $resArray['SHIPTOSTATE'];
            $transaction->country = $resArray['SHIPTOCOUNTRYNAME'];
            $transaction->zip = $resArray['SHIPTOZIP'];
            $transaction->ip = $ip_address;
            $transaction->created_by = $user_id;
            $transaction->created_date = new CDbExpression("NOW()");
            $transaction->save();

            //Getting plan detail
            $plan = new SubscriptionPlans;
            $plan = $plan->findByPk($usersub->plan_id);
            $start_date = date('Y-m-d H:i:s', strtotime($resArray['LASTPAYMENTDATE']));
            if ($plan->recurrence == 'Month') {
                $end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($start_date)));
            } elseif ($plan->recurrence == 'Year') {
                $end_date = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($start_date)));
            }

            $usersub->is_success = 1;
            $usersub->start_date = $start_date;
            $usersub->end_date = $end_date;
            $usersub->save();
        }


        echo '</pre>';
    }

    function actionWeeklyPaymentStatus() {
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAll(array('condition' => 'status=1 AND is_subscribed=1 AND is_default=0 AND is_deleted=0', "order" => "start_date ASC"));
        }

        $internalissues = array();
        $cardfailed = array();
        $partialpayment = array();
        $upcomingpayment = array();

        if (isset($studios) && !empty($studios)) {

            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));

                //if (strtotime(gmdate('Y-m-d')) > strtotime($billing_date)) {
                if (intval($studio->payment_status) == 2) {//Card failed
                    $cardfailed[] = $studio;
                } else if (intval($studio->payment_status) == 1) {//Partial Payment
                    $partialpayment[] = $studio;
                } else if (intval($studio->payment_status) == 0 && strtotime(gmdate('Y-m-d')) > strtotime($billing_date)) {//Internal issues
                    $internalissues[] = $studio;
                } else if (strtotime(gmdate('Y-m-d')) < strtotime($billing_date)) {//upcoming
                    $upcoming_date = new DateTime($billing_date);
                    $today = new DateTime(gmdate('Y-m-d'));
                    $interval = $upcoming_date->diff($today);
                    $days = $interval->days;

                    if ($days >= 0 && $days <= 7) {//Upcoming studios for next 7 days.
                        $upcomingpayment[] = $studio;
                    }
                }
            }

            $str = '';

            if (isset($internalissues) && !empty($internalissues)) {
                $str.='<p style="display:block;margin:0 0 17px">Payments Failed due to internal issues</p>';

                $str.='<p style="display:block;margin:0 0 17px"><table border="1" cellspacing="0" cellpadding="10" style="margin-top:15px;"><tr><th>Studio</th><th>Payment Date</th></tr>';
                foreach ($internalissues as $key => $value) {
                    $str.='<tr><td>' . $value->name . '</td><td>' . date('l F d, Y', strtotime($value->start_date)) . '</td></tr>';
                }
                $str.='</table></p>';
            }

            if (isset($cardfailed) && !empty($cardfailed)) {
                $str.='<p style="display:block;margin:0 0 17px">Payments Failed due to card fail</p>';

                $str.='<p style="display:block;margin:0 0 17px"><table border="1" cellspacing="0" cellpadding="10" style="margin-top:15px;"><tr><th>Studio</th><th>Payment Failed Date</th><th>Due Amount</th></tr>';
                foreach ($cardfailed as $key => $value) {
                    $due_amount = number_format((float) ($value->partial_failed_due), 2, '.', '');
                    $str.='<tr><td>' . $value->name . '</td><td>' . date('l F d, Y', strtotime($value->partial_failed_date)) . '</td><td>$' . $due_amount . '</td></tr>';
                }
                $str.='</table></p>';
            }

            if (isset($partialpayment) && !empty($partialpayment)) {
                $str.='<p style="display:block;margin:0 0 17px">Partial Payments</p>';

                $str.='<p style="display:block;margin:0 0 17px"><table border="1" cellspacing="0" cellpadding="10" style="margin-top:15px;"><tr><th>Studio</th><th>Partial Payment Date</th><th>Due Amount</th></tr>';
                foreach ($partialpayment as $key => $value) {
                    $due_amount = number_format((float) ($value->partial_failed_due), 2, '.', '');
                    $str.='<tr><td>' . $value->name . '</td><td>' . date('l F d, Y', strtotime($value->partial_failed_date)) . '</td><td>$' . $due_amount . '</td></tr>';
                }
                $str.='</table></p>';
            }

            if (isset($upcomingpayment) && !empty($upcomingpayment)) {
                $str.='<p style="display:block;margin:0 0 17px">Upcoming Payments for next week</p>';

                $str.='<p style="display:block;margin:0 0 17px"><table border="1" cellspacing="0" cellpadding="10" style="margin-top:15px;"><tr><th>Studio</th><th>Next Payment Date</th></tr>';
                foreach ($upcomingpayment as $key => $value) {
                    $str.='<tr><td>' . $value->name . '</td><td>' . date('l F d, Y', strtotime($value->start_date)) . '</td></tr>';
                }
                $str.='</table></p>';
            }
            //print $str;exit;

            if (trim($str)) {
                Yii::app()->email->weeklyPaymentStatus($str);
            }
        }
    }

    /*
     * Do not delete following methods. This is a realtime card validation and charging an amount to that card.

      function actionAmexAuthTest() {
      $dataInput['card_number'] = '';
      $dataInput['card_name'] = '';
      $dataInput['exp'] = '';
      $dataInput['amount'] = 0;
      $dataInput['zip'] = '';
      $dataInput['cvv'] = '';
      $dataInput['address'] = '';
      $firstData_preauth = $this->authenticateToCard($dataInput);

      if ($firstData_preauth->isError()) {
      print 'Error Msg: '.$firstData_preauth->getErrorMessage();
      print '<br/>';
      print 'Bank Error Msg: '.$firstData_preauth->getBankResponseMessage();
      } else {
      $respons_type = $firstData_preauth->getBankResponseType();
      if ($respons_type == 'S') {
      print 'Success in authorization.<br/>';
      } else {
      print 'respons_type: '.$respons_type;
      print '<br/>';
      print 'Error Msg: '.$firstData_preauth->getErrorMessage();
      print '<br/>';
      print 'Bank Error Msg: '.$firstData_preauth->getBankResponseMessage();
      }
      }
      print '<pre>';print_r($firstData_preauth);exit;
      }

      function authenticateToCard($data = array()) {
      if (isset($data) && !empty($data)) {
      if (IS_LIVE_FIRSTDATA) {
      $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
      } else {
      $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
      }

      $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
      $firstData->setCreditCardNumber($data['card_number'])
      ->setCreditCardName($data['card_name'])
      ->setCreditCardExpiration($data['exp'])
      ->setAmount($data['amount']);

      if ($data['zip']) {
      $firstData->setCreditCardZipCode($data['zip']);
      }

      if ($data['cvv']) {
      $firstData->setCreditCardVerification($data['cvv']);
      }

      if ($data['address']) {
      $firstData->setCreditCardAddress($data['address']);
      }

      $firstData->process();

      return $firstData;
      } else {
      return '';
      }
      }

      function actionAmexChargeTest() {
      $data = array();
      $data['token'] = '';
      $data['card_type'] = '';
      $data['card_name'] = '';
      $data['exp'] = '';
      $data['amount'] = '';

      $firstData = $this->chargeToCard($data);

      if ($firstData->isError()) {
      print 'Error Msg: '.$firstData->getErrorMessage();
      print '<br/>';
      print 'Bank Error Msg: '.$firstData->getBankResponseMessage();
      } else {
      $respons_type = $firstData->getBankResponseType();
      if ($respons_type == 'S') {
      print 'Success in authorization.<br/>';
      } else {
      print 'respons_type: '.$respons_type;
      print '<br/>';
      print 'Error Msg: '.$firstData->getErrorMessage();
      print '<br/>';
      print 'Bank Error Msg: '.$firstData->getBankResponseMessage();
      }
      }
      print '<pre>';print_r($firstData);exit;
      }

      function chargeToCard($data = array()) {
      if (isset($data) && !empty($data)) {
      if (IS_LIVE_FIRSTDATA) {
      $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
      } else {
      $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
      }

      $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
      $firstData->setTransArmorToken($data['token'])
      ->setCreditCardType($data['card_type'])
      ->setCreditCardName($data['card_name'])
      ->setCreditCardExpiration($data['exp'])
      ->setAmount($data['amount']);

      $firstData->process();

      return $firstData;
      } else {
      return '';
      }
      }
     */

    /**
     * @method public channelLiveStatus() It will check all the channels which don't have any activity since last 5min. It will make them offline
     * @param string $authToken Authtoken
     * @param string $channel_id Channel id of the streaming
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionChannelStatus() {
        $lsuser = Livestream::model()->findAll('is_online=:is_online', array(':is_online' => 1));
        if ($lsuser) {
            foreach ($lsuser AS $key => $val) {
                if ((strtotime($val->stream_update_time) + 2 * 60) < (strtotime(gmdate('Y-m-d H:i:s')))) {
                    LivestreamUsers::model()->updateByPk($val->id, array('is_online' => 0, 'stream_update_time' => gmdate('Y-m-d H:i:s')));
                }
            }
        }
        echo "";
        exit;
    }

    /**
     * @method public getCloudFrontStatus() It will check all the studio paying customer cloud front status which are created fro them
     * @author SKP<support@muvi.com>
     */
    function actiongetCloudFrontStatus() {
        $condition = '';
        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            if (isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] != '') {
                $condition = ' AND id = ' . $_REQUEST['studio_id'];
            } else {
                echo "Please provide studio_id";
                exit;
            }
        }
        $studioData = Yii::app()->db->createCommand()
                ->select('id,signed_url,unsigned_url,video_url')
                ->from('studios')
                ->where('cdn_status=0 AND new_cdn_users = 1 AND signed_url !=\'\' AND unsigned_url!=\'\' AND video_url!=\'\'' . $condition)
                ->queryAll();
        if ($studioData) {
            $client = CloudFrontClient::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret
            ));
            foreach ($studioData as $studioDatakey => $studioDataVal) {
                $studio_id = $studioDataVal['id'];
                $studioCdnDetails = StudioCdnDetails::model()->findByAttributes(array('studio_id' => $studio_id));
                $resultForSigned = $client->getDistribution(array(
                            // Id is required
                            'Id' => $studioCdnDetails['signed_url_id'],
                        ))->get('Status');
                $resultForUnSigned = $client->getDistribution(array(
                            // Id is required
                            'Id' => $studioCdnDetails['unsigned_url_id'],
                        ))->get('Status');
                $resultForVideo = $client->getDistribution(array(
                            // Id is required
                            'Id' => $studioCdnDetails['video_url_id'],
                        ))->get('Status');
                if ($resultForSigned == 'Deployed' && $resultForUnSigned == 'Deployed' && $resultForVideo == 'Deployed') {
                    $std = new Studio();
                    $std = $std->findByPk($studio_id);
                    $std->cdn_status = 1;
                    $std->save();
                    $trailerInfo = Yii::app()->db->createCommand()
                            ->select('mt.id as ID,mt.video_remote_url')
                            ->from('movie_trailer mt, films f')
                            ->where('f.id = mt.movie_id AND f.studio_id= ' . $studio_id)
                            ->queryAll();
                    foreach ($trailerInfo as $trailerInfoKey => $trailerInfoVal) {
                        if ($trailerInfoVal['video_remote_url'] != '') {
                            $fileName = explode($studio_id . '/private', $trailerInfoVal['video_remote_url']);
                            $trailerUrl = CDN_HTTP . $studioDataVal['signed_url'] . 'uploads/trailers/' . $trailerInfoVal['id'] . '/' . $trailerInfoVal['trailer_file_name'];
                            $trailerData = movieTrailer::model()->findByPk($trailerInfoVal['id']);
                            $trailerData->video_remote_url = $trailerUrl;
                            $trailerData->save();
                        }
                    }
                    $ban = new StudioBanner;
                    $bannerData = $ban->findAllByAttributes(array('studio_id' => $studio_id));
                    foreach ($bannerData as $bannerDataKey => $bannerDataVal) {
                        if ($bannerDataVal['video_remote_url'] != '') {
                            $videofileName = explode($studio_id . '/video', $bannerDataVal['video_remote_url']);
                            $imagefileName = explode($studio_id . '/public', $bannerDataVal['video_placeholder_img']);
                            $bannerVideoUrl = 'https://' . $studioDataVal['video_url'] . $videofileName[1];
                            $bannerImageUrl = 'https://' . $studioDataVal['unsigned_url'] . $imagefileName[1];
                            $bannerUpdate = StudioBanner::model()->findByPk($bannerDataVal['id']);
                            $bannerUpdate->video_remote_url = $bannerVideoUrl;
                            $bannerUpdate->video_placeholder_img = $bannerImageUrl;
                            $bannerUpdate->save();
                        }
                    }
                    echo "Status updated";
                } else {
                    echo "Status not updated";
                }
            }
        } else {
            echo "No Data";
        }
    }

    /**
     * @method public deleteCloudFrontDistribution() It will check all the studio paying customer whose account are deleted
     * @author SKP<support@muvi.com>
     */
    function actiondeleteCloudFrontDistribution() {
        $deleteCdn = DeleteCdn::model()->findAll();
        if ($deleteCdn) {
            foreach ($deleteCdn as $deleteCdnKey => $deleteCdnval) {					
                try {
                    $client = CloudFrontClient::factory(array(
                                'key' => @$deleteCdnval['key'],
                                'secret' => @$deleteCdnval['secret']
                    ));
                    $result = $client->deleteDistribution(array(
                                // Id is required
                                'Id' => $deleteCdnval['cdn_id'],
                                'IfMatch' => $deleteCdnval['etag_id'],
                            ))->get('RequestId');
                    //if ($result) {
                    DeleteCdn::model()->deleteByPk($deleteCdnval['id']);
                    // }
                } catch (\Aws\CloudFront\Exception\CloudFrontException $e) {
                    
                }
            }
        }
    }

    //Added by RK Setting studio dummy data
    public function actionCreateDummyData() {

        $dummy = new DummyData();
        $dummies = $dummy->findAllByAttributes(array('status' => '0'));
        foreach ($dummies as $dummy_rec) {
            $dummy_rec->start_date = gmdate('Y-m-d H:i:s');
            $dummy_rec->status = '1';
            $dummy_rec->save();
            $studio_id = $dummy_rec->studio_id;
            $source_studio_id = DUMMY_DATA_STUDIO;
            //Setting the Banners
            Yii::app()->general->setBanners($studio_id, $source_studio_id);

            //Setting Celebrity
            Yii::app()->general->setCelebrity($studio_id, $source_studio_id);

            $has_video_uploaded = 0;
            /* $content = new StudioContentType();
              $content_types = $content->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC')); */
            // foreach ($content_types as $content_data) {
            //  if ($content_data->content_types_id == 1) {
            //add dummy Movie
            $has_video_uploaded = Yii::app()->general->setDummymovie($studio_id, $source_studio_id, 1, $has_video_uploaded);
            //} else if ($content_data->content_types_id == 3) {
            //add dummy Tv
            $has_video_uploaded = Yii::app()->general->setDummytv($studio_id, $source_studio_id, 3, $has_video_uploaded);
            // }
            //}

            Yii::app()->general->setVideoGallery($studio_id, $source_studio_id);


            $dummy_rec->end_date = gmdate('Y-m-d H:i:s');
            $dummy_rec->status = '2';
            $dummy_rec->save();
        }
    }

    /* function actionProcessTransactionsOfCustomers1() {
      //Find out all active studios
      $_REQUEST['studio'] = 1132;
      if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
      $studios = Studios::model()->findAllByAttributes(array('id'=>$_REQUEST['studio'], 'status'=>1, 'is_deleted'=> 0));
      } else {
      $studios = Studios::model()->findAllByAttributes(array('status'=>1, 'is_deleted'=> 0));
      }

      if (isset($studios) && !empty($studios)) {
      $dbcon = Yii::app()->db;

      foreach ($studios as $key => $studio) {
      $studio_id = $studio->id;

      $is_process = 1;

      if (intval($is_process)) {
      //Find out Pyament gateway detail
      $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

      $activate = 0;//No plan and payment gateway set by studio
      if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
      $studio_payment_gateway = $plan_payment_gateway['gateways'][0];
      $this->setPaymentGatwayVariable($studio_payment_gateway);
      $activate = 1;
      }

      //Find out manual payment gateway which run by cron job
      if (intval($activate) && $studio_payment_gateway->short_code != 'manual') {

      $gateway = PaymentGateways::model()->findByAttributes(array('id'=>$studio_payment_gateway->gateway_id, 'status'=>1, 'is_payment_gateway' => 1, 'is_automated' => 0));

      //Find out all active subscribed sdk user with card detail
      if (isset($gateway) && !empty($gateway)) {
      $cond = '';
      //if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
      $cond = " AND u.id IN (12354,12578,12353,11692,11691,11947,11637,13347,13350,12038,12037,11690,12574,11814,11813,11689,12572,11635,12571,12349,11634,12348,12570,11931,11145,12347,11866,12346,11865,12565) ";
      //}

      $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,
      sp.id AS subscription_plan_id FROM sdk_users u LEFT JOIN user_subscriptions us ON (u.id=us.user_id AND
      u.studio_id=us.studio_id AND us.status=1), sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND
      u.is_deleted='0' AND u.studio_id=".$studio_id.$cond." AND sci.user_id=us.user_id AND
      sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id GROUP BY u.id";
      /*
     * AND sci.id=us.card_id 
     * 
      $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id,
      sp.*, sp.id AS subscription_plan_id FROM sdk_users u LEFT JOIN
      (user_subscriptions us, sdk_card_infos sci, subscription_plans sp)
      ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 AND sci.id=us.card_id AND sci.user_id=us.user_id AND sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id)
      WHERE u.status=1 AND u.is_deleted='0' AND u.studio_id=".$studio_id.$cond;
     * 
     *

      $users = $dbcon->createCommand($sql)->queryAll();
      echo "<pre>";
      // print_r($users);exit;
      if (isset($users) && !empty($users)) {

      foreach ($users as $key1 => $user) {
      $user['currency_code'] = 'USD';//Hard coded for the time baing as multiple currency is not implemented in subscription plans
      $user_id = $user['user_id'];
      $tranasction_date = gmdate('Y-m-d', strtotime($user['start_date']));
      $tranasction_date1 = gmdate('Y-m-d H:i:s', strtotime($user['start_date']));



      //If tranasction date is today date, then process transaction
      //$tranasction_date ='2015-07-02';

      //if (gmdate('Y-m-d') == $tranasction_date) {
      $payment_gateway_controller = 'Api'.$this->PAYMENT_GATEWAY.'Controller';
      Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
      $payment_gateway = new $payment_gateway_controller();
      $trans_data = $payment_gateway::processTransactions($user);

      $is_paid = $trans_data['is_success'];
      print_r($trans_data);
      //Save a transaction detail
      if (intval($is_paid)) {

      //Getting Ip address
      $ip_address = Yii::app()->request->getUserHostAddress();

      $transaction = new Transaction;
      $transaction->user_id = $user_id;
      $transaction->studio_id = $studio_id;
      $transaction->plan_id = $user['plan_id'];
      $transaction->transaction_date = new CDbExpression("NOW()");
      $transaction->payment_method = $this->PAYMENT_GATEWAY;
      $transaction->transaction_status = $trans_data['transaction_status'];
      $transaction->invoice_id = $trans_data['invoice_id'];
      $transaction->order_number = $trans_data['order_number'];
      $transaction->dollar_amount = $trans_data['amount'];
      $transaction->amount = $trans_data['amount'];
      $transaction->response_text = $trans_data['response_text'];
      $transaction->subscription_id = $user['user_subscription_id'];
      $transaction->ip = $ip_address;
      $transaction->created_date = new CDbExpression("NOW()");
      $transaction->save();

      //Update next subscription date according to studio subscription plan
      //$start_date = Date('Y-m-d H:i:s');//, strtotime("+ 1 days")

      $start_date = $tranasction_date1;
      $time = strtotime($start_date);
      $recurrence_frequency = $user['frequency'].' '.strtolower($user['recurrence'])."s";
      $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
      $end_date = Date('Y-m-d H:i:s', strtotime($start_date."+{$recurrence_frequency}-1days"));


      $subsc = new UserSubscription;
      $subsc = $subsc->findByPk($user['user_subscription_id']);
      $subsc->start_date = $start_date;
      $subsc->end_date = $end_date;
      $subsc->save();

      //Get invoice detail
      $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $trans_data['invoice_id']);

      //Send an email with invoice detail
      $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name);
      $fp = fopen($_SERVER['DOCUMENT_ROOT']."/".SUB_FOLDER.'protected/runtime/paymentgateway.log', "a+");
      fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----Api'.$this->PAYMENT_GATEWAY.'Controller/n'.$file_name);
      //exit;
      } else {

      $teModel = New TransactionErrors;
      $teModel->studio_id = $studio_id;
      $teModel->user_id = $user_id;
      $teModel->plan_id = $user['plan_id'];
      $teModel->subscription_id = $user['user_subscription_id'];
      $teModel->payment_method = $this->PAYMENT_GATEWAY;
      $teModel->response_text = $trans_data['response_text'];
      $teModel->created_date = new CDbExpression("NOW()");
      $teModel->save();

      //Send an error email to admin :TBD
      //$this->errorMessageMailToSales($req);
      }
      //}
      }
      }
      }
      }
      }
      }
      }
      } */

    function createUploadVideoSHFOrVideoGallery($data, $auth, $videoName, $studio_id, $videoToBeDeleted) {
        $condition = '';
        $videoFolder = 'upload_video';
        $shFolder = 'upload_progress';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        } else if (HOST_IP == '52.0.64.95') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        }

        $con = Yii::app()->db;

        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $videoName . "' and flag_deleted=0";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        $check_video_availability = count($res);
        if ($check_video_availability > 0) {
            $vidoInfo = new SplFileInfo($videoName);
            $videoExt = $vidoInfo->getExtension();
            $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
            $videoName = $videoOnlyName . strtotime(date("d-m-Y H:i:s")) . '.' . $videoExt;
        }

        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $s3cfg = $bucketInfo['s3cmd_file_name'];
        $video_gallery = new VideoManagement();
        $video_gallery->studio_id = $studio_id;
        $video_gallery->video_remote_url = $data['url'];
        $video_gallery->creation_date = new CDbExpression("NOW()");
        $video_gallery->save();
        $gallery_id = $video_gallery->id;

        $updateUrl = Yii::app()->getBaseUrl(true) . "/conversion/UpdateUrlUploadInfo?video_name=" . $gallery_id;

        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
        $signedBucketPath = $folderPath['unsignedFolderPathForVideo'];

        $urlForVideoNotDownloaded = Yii::app()->getBaseUrl(true) . "/conversion/UploadUrlVideoNotDownloaded?rec_id=" . $gallery_id;



        $s3 = S3Client::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
        ));
        $studioData = Studio::model()->getStudioBucketData($studio_id);
        if ($studioData['new_cdn_users'] == 1) {

            $bucketHttpUrl = CDN_HTTP . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'videogallery/';
            $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "videogallery/";
        } else {
            $bucketHttpUrl = CDN_HTTP . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'videogallery/' . $studio_id . '/';
            $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "videogallery/" . $studio_id . "/";
        }
        $updateUrl .= "MUVIMUVI" . $videoName;
        //Create shell script
        $file = 'uploadvideogallerySH_' . $gallery_id . '.sh';
        $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
        $cf = 'echo "${file%???}"';
        // Wget URL 
        $username = '';
        $password = '';
        if ($auth) {
            $username = $auth['access_username'];
            $password = $auth['access_password'];
        }
        $gotDir = 'cd /var/www/html/' . $videoFolder . '/videogallery_' . $gallery_id;
        if ($username && $password) {
            $data['url'] = str_replace('https://', '', $data['url']);
            $data['url'] = str_replace('www.', '', $data['url']);
            $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   https://' . $username . ':' . $password . '@' . $data['url'];
        } else {
            $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   ' . $data['url'];
        }

        $moveFile .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/videogallery_" . $gallery_id . "/" . $videoName . " " . $bucket_url . " \n";
        $moveFile .= "/usr/local/bin/s3cmd del --config /usr/local/bin/." . $s3cfg . " " . $videoToBeDeleted . " \n";
        $checkFileExistIfCondition .= 'if [ -s \'/var/www/html/' . $videoFolder . '/videogallery_' . $gallery_id . '/' . $videoName . "' ] \n then \n";
        $checkFileExistElseCondition .="\n\nelse\n\n";

        $checkFileExistElseCondition .= "curl $urlForVideoNotDownloaded\n";
        $file_data = "file=`echo $0`\n" .
                "cf='" . $file . "'\n\n" .
                "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                "then\n\n" .
                "echo \"$file is running\"\n\n" .
                "else\n\n" .
                "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                "# create directory\n" .
                "mkdir /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "chmod 0777 /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "#Go to Dirctory \n" .
                $gotDir . "\n" .
                "# wget command\n" .
                $wgetCMD .
                "\n" .
                "\n\n\n# Check all the video file's are created\n" .
                $checkFileExistIfCondition .
                "\n# upload to s3\n" .
                $moveFile .
                "# update query\n" .
                "curl $updateUrl\n" .
                "# remove directory\n" .
                "rm -rf /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "# remove the process copy file\n" .
                "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                "# remove the process file\n" .
                "rm /var/www/html/$shFolder/$file\n" .
                $checkFileExistElseCondition .
                "# remove directory\n" .
                "rm -rf /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "# remove the process copy file\n" .
                "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                "# remove the process file\n" .
                "rm /var/www/html/$shFolder/$file\n" .
                "fi\n\n" .
                "fi";
        fwrite($handle, $file_data);
        fclose($handle);
        //Uploading conversion script from local to s3 
        $s3 = S3Client::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
        ));
        $filePath = $_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file;
        $s3folderPath = "uploadScript/";
        if (Yii::app()->general->checkSonyReferal($studio_id) == 1) {
            $s3folderPath = "uploadScriptSony/";
        } else if ($bucketName == 'vimeoassets-singapore') {
            $s3folderPath = "uploadScriptSingaporeBucket/";
        }
        if ($s3->upload($conversionBucket, "uploadScript/" . $file, fopen($filePath, 'rb'), 'public-read')) {
            //echo "Sh file created successfully";
            if (unlink($filePath)) {
                $video_management = VideoManagement::model()->findByPk($gallery_id);
                ;
                $video_management->flag_uploaded = 1;
                $video_management->video_name = $videoName;
                $video_management->save();
            }
        }
    }

    function actionUpdateS3StorageSizeOfStudios() {
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'new_cdn_users' => 1, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $size = Yii::app()->aws->getStudioS3Storage($studio_id);
                $sql = "SELECT * FROM studio_storage WHERE studio_id = " . $studio_id;
                $data = Yii::app()->db->createCommand($sql)->queryRow();
                if (isset($data) && !empty($data)) {
                    $s3sql = " UPDATE studio_storage SET storage_size = '" . $size . "',updated_date='" . Date('Y-m-d H:i:s') . "' WHERE studio_id = " . $studio_id;
                } else {
                    $s3sql = "INSERT INTO studio_storage (studio_id,storage_size,created_date,updated_date) VALUES ('" . $studio_id . "','" . $size . "','" . Date('Y-m-d H:i:s') . "','" . Date('Y-m-d H:i:s') . "')";
                }
                Yii::app()->db->createCommand($s3sql)->execute();
            }
        }
    }

    function actionAdvancePurchaseContentAvailable() {
        //Find out all active studios
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            $dbcon = Yii::app()->db;

            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $is_process = 0;

                //Filter Customer, Lead and Demo studios otherwise no transaction process
                if ($studio_id > 0 && $studio->is_subscribed == 1 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Customer
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Lead
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 1) {//Default
                    $is_process = 1;
                }

                if (intval($is_process)) {
                    $cond = '';
                    if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                        $cond = " AND u.id=" . $_REQUEST['user'];
                    }

                    $sql = "SELECT u.*, u.id AS sdk_user_id, ps.*, ps.id AS subcription_id, pp.*, pp.id AS plan_id
                    FROM sdk_users u, ppv_subscriptions ps LEFT JOIN ppv_plans pp ON (ps.ppv_plan_id=pp.id)
                    WHERE u.id=ps.user_id AND u.status=1 AND ps.status=1 AND ps.is_advance_purchase=1 AND 
                    ps.is_advance_purchase_email=0 " . $cond . " AND u.studio_id=" . $studio_id;

                    $users = $dbcon->createCommand($sql)->queryAll();

                    if (isset($users) && !empty($users)) {
                        foreach ($users as $key1 => $user) {
                            $user_id = $user['sdk_user_id'];
                            $movie_id = $user['movie_id'];
                            $stream = movieStreams::model()->findByAttributes(array("movie_id" => $movie_id));

                            if (intval($stream->is_converted) == 1) {
                                $film = Film::model()->findByPk($movie_id);

                                if (!empty($film)) {
                                    //creating playing url
                                    $playing_url = $film->permalink;

                                    if ($user['content_types_id'] == 3) {
                                        if (intval($user['season_id'])) {
                                            $playing_url.='/season/' . $user['season_id'];
                                        }

                                        if (trim($user['episode_id']) && trim($user['episode_id']) != '0') {
                                            $playing_url.= '/stream/' . $stream->embed_id;
                                        }
                                    }

                                    $VideoName = Yii::app()->common->getVideoname($movie_id);
                                    $VideoName = trim($VideoName);

                                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'advance_purchase_content_available', '', $VideoName, $playing_url);

                                    $ppvsubscription = PpvSubscription::model()->findByPk($user['subcription_id']);
                                    $ppvsubscription->is_advance_purchase_email = 1;
                                    $ppvsubscription->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function actioncronGetOrderStatus() {
        //check stock
        PGOrder::CheckItemStatus(2663);
    }

    public function actioncronGetOrderStatusMuvi() {
        //check stock
        PGOrder::CheckItemStatusMuvi(3063);
    }

    public function actioncronCdsRecreateOrder() {
        $pgorder = new PGOrder();
        $orders = $pgorder->findAllByAttributes(array('cds_order_status' => '', 'studio_id' => 2663));
        if ($orders) {
            foreach ($orders AS $ord) {
                $pgorder->CreateCDSOrder(2663, $ord['id']);
            }
        }
    }

    //cron for usjv 
    public function actioncronGetOrderStatusUSJV() {
        //check stock
        PGOrder::CheckItemStatus(3057);
        PGOrder::CheckItemStatus(3063);
    }

    public function actioncronCdsRecreateOrderUSJV() {
        $pgorder = new PGOrder();
        $orders = $pgorder->findAllByAttributes(array('cds_order_status' => 'ERRWS', 'studio_id' => 3057));
        if ($orders) {
            foreach ($orders AS $ord) {
                $pgorder->CreateCDSOrder(3057, $ord['id']);
            }
        }
    }

    //cron usjv end
    public function actionratitest() {
        $this->layout = false;
        $ip_address = Yii::app()->request->getUserHostAddress();
        if ($ip_address == '52.0.232.150' || $ip_address == '52.211.41.107') {
            $s3sql = "INSERT INTO chkrequest (ip_address) VALUES ('" . $ip_address . "')";
            Yii::app()->db->createCommand($s3sql)->execute();
        }
    }

    function actionSubscriptionRenewalOfResellers() {
        //Find out all active studios (Customer Only)
        if (isset($_REQUEST['reseller']) && !empty($_REQUEST['reseller'])) {//test case
            $resellers = PortalUser::model()->findAllByAttributes(array('id' => $_REQUEST['reseller'], 'status' => 1, 'is_subscribed' => 1));
        } else {
            $resellers = PortalUser::model()->findAllByAttributes(array('status' => 1, 'is_subscribed' => 1));
        }

        if (isset($resellers) && !empty($resellers)) {
            foreach ($resellers as $key => $reseller) {
                $reseller_id = $reseller->id;

                //Get lelvels selected by Reseller
                $plans = ResellerSubscription::model()->find(array("condition" => 'status = 1 AND reseller_id = ' . $reseller_id));

                if (isset($plans) && !empty($plans)) {
                    $plan = $plans->plans;

                    if ($plan == 'Month') {
                        self::monthlySubscriptionRenewalOfResellers($reseller, $plans, $_REQUEST['reseller']);
                    }
                }
            }
        }
    }

    function monthlySubscriptionRenewalOfResellers($reseller = array(), $plans = array(), $test_reseller_id = 0) {
        $reseller_id = $reseller->id;

        //Finding billing date
        if (isset($test_reseller_id) && intval($test_reseller_id)) {
            $billing_date = gmdate('Y-m-d');
        } else {
            $billing_date = gmdate('Y-m-d', strtotime($plans->start_date));
        }

        $isNoCardError = self::isResellerCardErrorExists($reseller_id, $billing_date);
        if (isset($test_reseller_id) && intval($test_reseller_id)) {
            $isNoCardError = 1;
        }

        if ((gmdate('Y-m-d') == $billing_date) && intval($isNoCardError)) {
            self::resellerMonthlyYearlyPayment($reseller, $plans, $billing_date, $test_reseller_id);
        }
    }

    function isResellerCardErrorExists($reseller_id, $billing_date) {
        //Check if card is already showing error for the desired date, then not allow to charge again in same day.
        $sql = "SELECT COUNT(*) AS total_card_errors FROM reseller_card_errors WHERE reseller_id={$reseller_id} 
        AND created LIKE '%{$billing_date}%' GROUP BY reseller_id";

        $con = Yii::app()->db;
        $total_card_errors = $con->createCommand($sql)->queryAll();
        if (isset($total_card_errors[0]['total_card_errors']) && intval($total_card_errors[0]['total_card_errors'])) {
            $isNoCardError = 0;
        } else {
            $isNoCardError = 1;
        }

        return $isNoCardError;
    }

    function getResellersCustomerDetail($reseller_id, $test_reseller_id) {
        $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0, 'reseller_id' => $reseller_id));

        $customers = array();

        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $customers[$studio_id]['studio'] = $studio;

                //Get packages selected by studio admin
                $plans = StudioPricingPlan::model()->findAll(array('select' => 'package_id, application_id, plans, package_code_id, is_storage', 'condition' => 'status = 1 AND studio_id = ' . $studio_id, 'order' => 'application_id ASC'));
                $customers[$studio_id]['plans'] = $plans;

                if (isset($plans) && !empty($plans)) {
                    $plan = $plans[0]->plans;

                    if (isset($test_reseller_id) && intval($test_reseller_id)) {
                        $billing_date = gmdate('Y-m-d');
                        $bandwidth_date = gmdate('Y-m-d');
                    } else {
                        $billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
                        $bandwidth_date = gmdate('Y-m-d', strtotime($studio->bandwidth_date));
                    }

                    if ((gmdate('Y-m-d') == $billing_date) || (gmdate('Y-m-d') == $bandwidth_date)) {
                        //Get package detail
                        $package = Package::model()->getPackages($plans[0]->package_id);
                        $package_detail = self::getMonthlyPackageDetail($package, $plans);
                        $customers[$studio_id]['package_detail'] = $package_detail;

                        if (($plan == 'Year') && (gmdate('Y-m-d') != $billing_date) && (gmdate('Y-m-d') == $bandwidth_date)) {//Plan is yearly but charge line items like bandwidth, storage on monthly basis
                            $bill_amount = 0;
                            $yearly_discount = 0;
                        } else {
                            $bill_amount = @$package_detail['monthly_package_amount'];

                            if ($plan == 'Year') {
                                $yearly_discount = @$package_detail['yearly_discount'];
                                $yearly_discount_amount = ((($bill_amount * 12) * $yearly_discount) / 100);
                                $bill_amount = (($bill_amount * 12) - $yearly_discount_amount);
                            }

                            //Get paused item detail
                            $PauseBilling = PauseBilling::model()->findAllByAttributes(array('studio_id' => $studio_id, 'package_id' => $plans[0]->package_id, 'is_paused' => 1));
                            $paused_package_detail = array();
                            $paused_amount = 0;
                            if (!empty($PauseBilling)) {
                                $paused_package_detail = self::getPausedBillingDetail($package, $plans, $PauseBilling);

                                if (!empty($paused_package_detail)) {
                                    $paused_amount = @$paused_package_detail['paused_package_amount'];

                                    if ($plan == 'Year') {
                                        $paused_amount = (($paused_amount * 12) - ((($paused_amount * 12) * $yearly_discount) / 100));
                                    }

                                    $paused_package_detail['paused_package_amount'] = $paused_amount;
                                    $customers[$studio_id]['paused_package_detail'] = $paused_package_detail;
                                }
                            }
                            $bill_amount = $bill_amount - $paused_amount;
                        }

                        //Getting bandwidth detail
                        $bandwidth = $this->getBandwidthCost($studio, $package);
                        $bandwidth_detail = Yii::app()->aws->getBandwidthPrice($bandwidth);

                        $bandwidth_charge = 0;
                        if (isset($bandwidth_detail) && !empty($bandwidth_detail)) {
                            $bandwidth_charge = isset($bandwidth_detail['price']) ? number_format((float) $bandwidth_detail['price'], 2, '.', '') : 0;
                            $customers[$studio_id]['bandwidth_detail'] = $bandwidth_detail;
                        }
                        $bill_amount = number_format((float) ($bill_amount + $bandwidth_charge), 2, '.', '');

                        if (intval($package['package'][0]['is_bandwidth'])) {
                            $bandwidth_usage = (isset($bandwidth_detail['total_bandwidth_consumed']) && !empty($bandwidth_detail['total_bandwidth_consumed'])) ? $bandwidth_detail['total_bandwidth_consumed'] : 0;

                            $is_storage_exists = @$plans[0]->is_storage;
                            if (intval($is_storage_exists)) {
                                //Getting storage detail
                                $total_storage = Yii::app()->aws->getStorageUsageOfStudio($studio_id);
                                $storage_detail = Yii::app()->aws->getStorageCost($total_storage, $bandwidth_usage);
                                $storage_charge = 0;
                                if (isset($storage_detail) && !empty($storage_detail)) {
                                    $storage_charge = isset($storage_detail['cost']) ? number_format((float) $storage_detail['cost'], 2, '.', '') : 0;
                                    $customers[$studio_id]['storage_detail'] = $storage_detail;
                                }

                                $bill_amount = $bill_amount + $storage_charge;
                            }
                        }

                        //Getting DRM pricing detail
                        $drm_price = 0;
                        $is_drm_enable = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'drm_enable');
                        if (isset($is_drm_enable) && !empty($is_drm_enable)) {
                            $get_drm_device_type = array('web'=> 1,'andriod' => 2, 'ios'=>3,'raku'=>4);
                            if(count($get_drm_device_type) > 0 ){
                                $total_views = array();
                               foreach($get_drm_device_type as $key => $val){
                                   $views_for_device = VideoLogs::model()->getTotalViewsForStudio_with_devicetype($studio_id,$val);
                                   $total_views[] = $views_for_device;
                               }
                               $drm_detail = Yii::app()->aws->getDRMCostForStudioWithDevice($studio_id, $total_views);
                               $customers[$studio_id]['drm_detail'] = $drm_detail;
                               if (isset($drm_detail) && !empty($drm_detail)) {
                                    $drm_price = isset($drm_detail['drm_price']) ? number_format((float) $drm_detail['drm_price'], 2, '.', '') : 0;
                                }
                              $bill_amount = $bill_amount + $drm_price;
                            }
						}

                        //Getting Muvi Kart pricing detail
                        $muvi_kart_price = 0;
                        $is_muvi_kart_enable = Yii::app()->general->getStoreLink($studio_id, 1);

                        if (isset($is_muvi_kart_enable) && intval($is_muvi_kart_enable)) {
                            $total_transactions = Yii::app()->general->getTotalTransactionsOfStudioForMuviKart($studio_id);
                            if (isset($total_transactions) && !empty($total_transactions)) {
                                $muvi_kart_detail = Yii::app()->general->getMuviKartCostForStudio($studio_id, $total_transactions);

                                if (isset($muvi_kart_detail) && !empty($muvi_kart_detail)) {
                                    $muvi_kart_detail['amount_in_currency'] = $total_transactions['amount_in_currency'];
                                    $muvi_kart_price = isset($muvi_kart_detail['muvi_kart_price']) ? number_format((float) $muvi_kart_detail['muvi_kart_price'], 2, '.', '') : 0;
                                    $customers[$studio_id]['muvi_kart_detail'] = $muvi_kart_detail;
                                }
                                $bill_amount = $bill_amount + $muvi_kart_price;
                            }
                        }

                        $bill_amount = number_format((float) ($bill_amount), 2, '.', '');
                        $customers[$studio_id]['customer_total_bill_amount'] = $bill_amount;

                        //Getting user info
                        $user = User::model()->findByAttributes(array('studio_id' => $studio_id, 'is_admin' => 0, 'is_sdk' => 1, 'role_id' => 1, 'is_active' => 1));
                        $customers[$studio_id]['user'] = $user;
                    }
                }
            }
        }

        return $customers;
    }

    function setResellerBillingInfoDetail($reseller_id, $title, $bill_amount) {
        $biModel = New ResellerBillingInfos;
        $biModel->portal_user_id = $reseller_id;
        $uniqid = Yii::app()->common->generateUniqNumber();
        $biModel->uniqid = $uniqid;
        $biModel->title = $title;
        $biModel->billing_date = new CDbExpression("NOW()");
        $biModel->created_date = new CDbExpression("NOW()");
        $biModel->billing_amount = $bill_amount;
        $biModel->transaction_type = 1;
        $biModel->is_paid = 0;
        $biModel->save();

        return $biModel->id;
    }

    function updateResellerBillingInfoDetail($data = array()) {
        if (isset($data) && !empty($data)) {

            $biModel = ResellerBillingInfos::model()->findByPk($data['id']);
            foreach ($data as $key => $value) {
                $biModel->$key = $value;
            }
            $biModel->save();
        }
    }

    function setResellerBillingDetail($bdarg = array()) {
        $bdModel = New ResellerBillingDetails;
        $bdModel->billing_info_id = @$bdarg['billing_info_id'];
        $bdModel->portal_user_id = @$bdarg['portal_user_id'];
        $bdModel->title = @$bdarg['title'];
        $bdModel->description = @$bdarg['description'];
        $bdModel->amount = @$bdarg['amount'];
        $bdModel->save();
    }

    function saveResellerSubscriptionDetail($ResellerSubscription, $arg) {
        if (isset($arg) && !empty($arg)) {
            foreach ($arg as $key => $value) {
                if (trim($value)) {
                    $ResellerSubscription->$key = $value;
                }
            }
        }

        $ResellerSubscription->payment_key = '';
        $ResellerSubscription->save();
    }

    function setResellerTransactionDetail($trandata = array()) {
        $tiModel = New ResellerTransaction;
        $tiModel->card_info_id = @$trandata['card_info_id'];
        $tiModel->portal_user_id = @$trandata['portal_user_id'];
        $tiModel->billing_info_id = @$trandata['billing_info_id'];
        $tiModel->billing_amount = @$trandata['bill_amount'];
        $tiModel->invoice_detail = @$trandata['invoice_detail'];
        $tiModel->is_success = 1;
        $tiModel->invoice_id = @$trandata['invoice_id'];
        $tiModel->order_num = @$trandata['order_num'];
        $tiModel->paid_amount = @$trandata['paid_amount'];
        $tiModel->response_text = @$trandata['response_text'];
        $tiModel->transaction_type = @$trandata['transaction_type'];
        $tiModel->created_date = new CDbExpression("NOW()");
        $tiModel->save();
    }

    function saveResellerCardError($reseller, $reseller_account_name, $reseller_sub, $card_info, $firstData, $bill_amount) {
        //Keeping card error logs
        $ceModel = New ResellerCardErrors;
        $ceModel->reseller_id = $reseller_sub->reseller_id;
        $ceModel->response_text = serialize($firstData);
        $ceModel->transaction_type = 'Subscription renewal';
        $ceModel->created = new CDbExpression("NOW()");
        $ceModel->save();

        $reseller_sub->payment_key = '';
        $reseller_sub->payment_status = 2; //Payment failed
        $reseller_sub->partial_failed_date = gmdate('Y-m-d h:i:s'); //Payment failed date
        $reseller_sub->partial_failed_due = $bill_amount; //Due on failed payment
        $reseller_sub->save();

        //Drop email to reseller and muvi support
        $req['name'] = $reseller->name;
        $req['email'] = $reseller->email;
        $req['companyname'] = $reseller_account_name;
        $req['plan'] = $reseller_sub->plans;
        $req['card_last_fourdigit'] = $card_info->card_last_fourdigit;
        $req['ErrorMessage'] = (isset($firstData) && !empty($firstData)) ? ((trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage()) : '';

        Yii::app()->email->cardFailureMailToReseller($req);
        Yii::app()->email->cardFailureMailResellerToAdmin($req);
    }

    function setCustomerSubscriptionDetail($resellers_customer_detail) {
        //Set reseller's customer(studio) detail
        if (isset($resellers_customer_detail) && !empty($resellers_customer_detail)) {
            foreach ($resellers_customer_detail as $key => $customer) {
                $studio = $customer['studio'];
                $studio_billing_date = gmdate('Y-m-d', strtotime($studio->start_date));
                $studio_bandwidth_date = gmdate('Y-m-d', strtotime($studio->bandwidth_date));

                $studio_plans = $customer['plans'];
                $studio_plan = $studio_plans[0]->plans;

                if ($studio_plan == 'Year') {
                    $cst_start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
                    $cst_end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
                } else {
                    $cst_start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                    $cst_end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                }

                $cst_bandwidth_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));

                $studio_data = array();

                if (($studio_plan == 'Year') && (gmdate('Y-m-d') != $studio_billing_date) && (gmdate('Y-m-d') == $studio_bandwidth_date)) {
                    $studio_data['bandwidth_date'] = $cst_bandwidth_date;
                } else {
                    $studio_data['start_date'] = $cst_start_date;
                    $studio_data['end_date'] = $cst_end_date;
                    $studio_data['bandwidth_date'] = $cst_bandwidth_date;
                }

                self::setStudioDetail($studio, $studio_data);
            }
        }

        return true;
    }

    function setResellerSubscriptionDetail($reseller_subscription, $start_date, $end_date) {
        //Set Reseller subscription Detail
        $reseller_data = array();
        $reseller_data['start_date'] = $start_date;
        $reseller_data['end_date'] = $end_date;
        self::saveResellerSubscriptionDetail($reseller_subscription, $reseller_data);

        return true;
    }

    function sendResellerEmailsToAll($reseller, $reseller_account_name, $level_name, $plan, $billing_period_date, $paid_amount = 0, $file_name = '', $payment_method = 1) {
        $req = array();
        $req['name'] = ucwords($reseller->name);
        $req['email'] = $reseller->email;
        $req['companyname'] = ucwords($reseller_account_name);
        $req['level_name'] = @$level_name;
        $req['plan'] = @$plan;
        $req['billing_period'] = $billing_period_date;
        $req['billing_amount'] = $paid_amount;

        //Send an email to user and super admin
        if (intval($payment_method) == 2) {
            Yii::app()->email->monthlyYearlyManualRenewResellerMailToSales($req, $file_name);
        } else {
            Yii::app()->email->monthlyYearlyRenewMailToReseller($req, $file_name);
            Yii::app()->email->monthlyYearlyRenewResellerMailToSales($req);
        }
    }

    function resellerMonthlyYearlyPayment($reseller, $plans, $billing_date, $test_reseller_id) {
        $reseller_id = $reseller->id;
        $plan = @$plans->plans;

        //Get reseller level detail
        $level = ResellerPlan::model()->getResellerPlan($plans->reseller_plan_id);
        if (isset($level) && !empty($level)) {
            $level_detail = $level['level'][0];
        }

        if (isset($plans->is_custom) && intval($plans->is_custom)) {
            $reseller_bill_amount = @$level_detail['custom_plan_amount'];
            $level_name = 'Custom';
        } else {
            $reseller_bill_amount = @$level_detail['base_price'];
            $level_name = @$level_detail['name'];
        }

        //Get Reseller's paying customer detail
        $resellers_customer_detail = self::getResellersCustomerDetail($reseller_id, $test_reseller_id);

        $customer_total_bill_amount = 0;
        if (isset($resellers_customer_detail) && !empty($resellers_customer_detail)) {

            foreach ($resellers_customer_detail as $key => $customer) {
                $customer_studio_id = $customer['studio']->id;
                if (isset($test_reseller_id) && intval($test_reseller_id)) {
                    $customer_billing_date = gmdate('Y-m-d');
                } else {
                    $customer_billing_date = gmdate('Y-m-d', strtotime($customer['studio']->start_date));
                }

                $invoice['isReseller'] = 1;
                $invoice['studio'] = @$customer['studio'];
                $invoice['user'] = @$customer['user'];
                $invoice['plans'] = @$customer['plans'];
                $invoice['package_detail'] = @$customer['package_detail'];
                $invoice['paused_package_detail'] = @$customer['paused_package_detail'];
                $invoice['bandwidth_detail'] = @$customer['bandwidth_detail'];
                $invoice['storage_detail'] = @$customer['storage_detail'];
                $invoice['drm_detail'] = @$customer['drm_detail'];
                $invoice['muvi_kart_detail'] = @$customer['muvi_kart_detail'];
                $invoice['bill_amount'] = @$customer['customer_total_bill_amount'];
                $invoice['billing_date'] = $customer_billing_date;

                $arg[$customer_studio_id]['customer_html'] = self::generateInvoice($invoice);

                $customer_name = (trim($customer['user']->last_name)) ? $customer['user']->first_name . ' ' . $customer['user']->last_name : $customer['user']->first_name;
                $arg[$customer_studio_id]['customer_name'] = $customer['studio']->name . '(' . ucwords($customer_name) . ')';
                $arg[$customer_studio_id]['customer_total_bill_amount'] = $customer['customer_total_bill_amount'];
                $customer_total_bill_amount = $customer_total_bill_amount + $customer['customer_total_bill_amount'];
            }
        }

        $bill_amount = number_format((float) ($reseller_bill_amount + $customer_total_bill_amount), 2, '.', '');

        //Get Reseller detail
        $reseller_account_detail = SellerApplication::model()->findByAttributes(array('id' => $reseller->seller_app_id, 'status' => 1));
        $reseller_account_name = $reseller_account_detail->company_name;
        $industry = $reseller_account_detail->industry;

        //Set billing information detail
        if (isset($test_reseller_id) && intval($test_reseller_id)) {
            $arg['isEmail'] = 0;
        } else {
            $bititle = 'Purchase Level fee for ' . @$level_name;
            $billing_info_id = self::setResellerBillingInfoDetail($reseller_id, $bititle, $bill_amount);
            $arg['isEmail'] = 1;
        }

        //Generate invoice detail
        if ($plan == 'Month') {
            $billing_end = Date('Y-m-d H:i:s', strtotime($billing_date . "+1 Months -1 Days"));
        } else {
            $billing_end = Date('Y-m-d H:i:s', strtotime($billing_date . "+1 Years -1 Days"));
        }

        $billing_period_date = date('F d, Y', strtotime($billing_date)) . " to " . date('F d, Y', strtotime($billing_end));
        $billing_period = "Billing Period " . $billing_period_date;

        $arg['rseller_name'] = ucwords($reseller->name);
        $arg['account_name'] = ucwords($reseller_account_name);
        $arg['industry'] = ucwords($industry);
        $arg['account_number'] = sprintf("%05d", $reseller_id);
        $arg['invoice_number'] = sprintf("%08d", $billing_info_id);
        $arg['billing_amount'] = $bill_amount;

        $arg['reseller_level_name'] = $level_name;
        $arg['reseller_bill_amount'] = $reseller_bill_amount;
        $arg['billing_period'] = $billing_period;
        $arg['isRenew'] = 1;
        $arg['isReseller'] = 1;

        $pdf = Yii::app()->pdf->resellerMonthlyYearlyRenewInvoiceDetial($arg);
        $file_name = $pdf['pdf'];
        if (intval($test_reseller_id)) {
            print $file_name;
            exit;
        }

        //Update invoice in billing information detail
        $upddata = array();
        $upddata['id'] = @$billing_info_id;
        $upddata['detail'] = $pdf['html'];
        self::updateResellerBillingInfoDetail($upddata);

        //Set billing detail of reseller
        $title = 'Reseller Fee - ' . @$level_name;
        $description = $title;
        $amount = @$reseller_bill_amount;

        $bdarg = array();
        $bdarg['billing_info_id'] = $billing_info_id;
        $bdarg['portal_user_id'] = $reseller_id;
        $bdarg['title'] = $title;
        $bdarg['description'] = $description;
        $bdarg['amount'] = $amount;
        self::setResellerBillingDetail($bdarg);

        //Set billing detail of reseller's customer
        if (isset($resellers_customer_detail) && !empty($resellers_customer_detail)) {
            foreach ($resellers_customer_detail as $key => $customer) {
                $title = $customer['studio']->name . '(' . ucwords($customer_name) . ')';
                $description = $title;
                $amount = @$customer['customer_total_bill_amount'];

                $bdarg = array();
                $bdarg['billing_info_id'] = $billing_info_id;
                $bdarg['portal_user_id'] = $reseller_id;
                $bdarg['title'] = $title;
                $bdarg['description'] = $description;
                $bdarg['amount'] = $amount;
                self::setResellerBillingDetail($bdarg);
            }
        }

        $today = Date('Y-m-d H:i:s');
        if ($plan == 'Year') {
            $start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
            $end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
        } else {
            $start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
            $end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
        }

        if (abs($bill_amount) < 0.00001) {
            //Update billing infos status
            $upddata = array();
            $upddata['id'] = @$billing_info_id;
            $upddata['paid_amount'] = 0;
            $upddata['is_paid'] = 2;
            $upddata['billing_period_start'] = $today;
            $upddata['billing_period_end'] = $start_date;
            self::updateResellerBillingInfoDetail($upddata);

            self::setCustomerSubscriptionDetail($resellers_customer_detail);
            self::setResellerSubscriptionDetail($plans, $start_date, $end_date);

            //Drop email to reseller and muvi support
            self::sendResellerEmailsToAll($reseller, $reseller_account_name, $level_name, $plan, $billing_period_date, $bill_amount, $file_name);
        } else {
            $payment_method = $reseller->payment_method;
            if (intval($payment_method) == 1) {//Payment by Credit card
                //Get card information detail
                $card_info = ResellerCardInfos::model()->find('portal_user_id=:portal_user_id AND is_cancelled=:is_cancelled', array(':is_cancelled' => 0, ':portal_user_id' => $reseller_id));

                //Process transaction
                $firstData = self::processStudioTransaction($card_info, $bill_amount);

                if ($firstData->isError()) {
                    self::saveResellerCardError($reseller, $reseller_account_name, $plans, $card_info, $firstData, $bill_amount);
                } else {
                    if ($firstData->getBankResponseType() == 'S') {

                        //Set transaction detail
                        $trandata = array();
                        $trandata['portal_user_id'] = @$reseller_id;
                        $trandata['card_info_id'] = @$card_info->id;
                        $trandata['billing_info_id'] = @$billing_info_id;
                        $trandata['bill_amount'] = @$bill_amount;
                        $trandata['invoice_detail'] = $plan . "ly Billing Charge";
                        $trandata['paid_amount'] = $paid_amount = $firstData->getAmount();
                        $trandata['invoice_id'] = $firstData->getTransactionTag();
                        $trandata['order_num'] = $firstData->getAuthNumber();
                        $trandata['response_text'] = serialize($firstData);
                        $trandata['transaction_type'] = 'Subscription fee for ' . @$level_name;
                        self::setResellerTransactionDetail($trandata);

                        //Update billing information detail
                        $upddata = array();
                        $upddata['id'] = @$billing_info_id;
                        $upddata['paid_amount'] = @$paid_amount;
                        $upddata['is_paid'] = 2;
                        $upddata['billing_period_start'] = $today;
                        $upddata['billing_period_end'] = $start_date;
                        self::updateResellerBillingInfoDetail($upddata);

                        self::setCustomerSubscriptionDetail($resellers_customer_detail);
                        self::setResellerSubscriptionDetail($plans, $start_date, $end_date);

                        //Drop email to reseller and muvi support
                        self::sendResellerEmailsToAll($reseller, $reseller_account_name, $level_name, $plan, $billing_period_date, $paid_amount, $file_name);
                    } else {
                        self::saveResellerCardError($reseller, $reseller_account_name, $plans, $card_info, $firstData, $bill_amount);
                    }
                }
            } else if (intval($payment_method) == 2) {//Payment by Wire transfer
                //Update billing information detail
                $upddata = array();
                $upddata['id'] = @$billing_info_id;
                $upddata['paid_amount'] = 0;
                $upddata['is_paid'] = 0;
                $upddata['billing_period_start'] = $today;
                $upddata['billing_period_end'] = $start_date;
                self::updateResellerBillingInfoDetail($upddata);

                self::setCustomerSubscriptionDetail($resellers_customer_detail);
                self::setResellerSubscriptionDetail($plans, $start_date, $end_date);

                //Drop email to reseller and muvi support
                self::sendResellerEmailsToAll($reseller, $reseller_account_name, $level_name, $plan, $billing_period_date, $bill_amount, $file_name, $payment_method);
            }
        }
    }

    /**
     * @method public actionDownloadMaxmindAnonymousDB() Auto update maxmind database of anonymous IP 
     * @author Ratikanta<ratikanta@muvi.com>
     */
    public function actionDownloadMaxmindAnonymousDB() {
        $extractPath = $maxmindPath = $_SERVER["DOCUMENT_ROOT"] . "/db/maxmind/";
        $download_path = $maxmindPath . 'downloaded';
        if (!is_dir($download_path)) {
            mkdir($download_path, 0755, true);
        }
        $zipFile = $download_path . "/GeoIP2-Anonymous-IP.tar.gz"; // Local Zip File Path
        $tarFile = $download_path . "/GeoIP2-Anonymous-IP.tar"; // Local Zip File Path
        $anonymous_db_file_name = 'GeoIP2-Anonymous-IP.mmdb';
        $destination_file = $extractPath . $anonymous_db_file_name;
        $extractPath = $download_path;
        $all_deleted = 0;
        $date_raw = date('Y-m-d');
        $yymmdd = date('Ymd', strtotime('-1 day', strtotime($date_raw)));
        $license_key = 'bSzeIBD0i6Gy';
        $url = "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-Anonymous-IP&date=" . $yymmdd . "&suffix=tar.gz&license_key=" . $license_key;
        $zipResource = fopen($zipFile, "w");
        // Get The Zip File From Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FILE, $zipResource);
        $page = curl_exec($ch);
        if (!$page) {
            echo "Error :- " . curl_error($ch);
            curl_close($ch);
        } else {
            echo "Downloaded";
            curl_close($ch);
            fclose($zipResource);
            if (file_exists($tarFile)) {
                try {
                    Phar::unlinkArchive($tarFile);
                } catch (Exception $e) {
                    echo $e->getMessage(), "\n";
                }
            }
            $p = new PharData($zipFile);
            $ret_dc = $p->decompress(); // creates files.tar              
            unset($p);
            if ($ret_dc == true && file_exists($tarFile)) {
                // unarchive from the tar
                $phar = new PharData($tarFile);
                $is_extracted = $phar->extractTo($extractPath);
                if ($is_extracted == true) {
                    $dir = $extractPath;
                    // get files
                    $cdir = scandir($dir);
                    foreach ($cdir as $key => $value) {
                        if (!in_array($value, array(".", ".."))) {
                            if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                                $new_dir = $dir . DIRECTORY_SEPARATOR . $value;
                                $new_cdir = scandir($new_dir);
                                foreach ($new_cdir as $k => $v) {
                                    if (!in_array($v, array(".", ".."))) {
                                        if (is_dir($new_dir . DIRECTORY_SEPARATOR . $v)) {
                                            
                                        } else if ($v == $anonymous_db_file_name) {
                                            $new_file = $new_dir . DIRECTORY_SEPARATOR . $v;
                                            if (@copy($new_file, $destination_file)) {
                                                unset($phar);
                                                $all_deleted = $this->delete_files($new_dir);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @method public actionClearDownloadFiles() Delete All Maxmind Archives 
     * @author Ratikanta<ratikanta@muvi.com>
     */
    public function actionClearDownloadFiles() {
        $extractPath = $maxmindPath = $_SERVER["DOCUMENT_ROOT"] . "/db/maxmind/";
        $download_path = $maxmindPath . 'downloaded';
        $zipFile = $download_path . "/GeoIP2-Anonymous-IP.tar.gz"; // Local Zip File Path
        $tarFile = $download_path . "/GeoIP2-Anonymous-IP.tar"; // Local Zip File Path        
        if (file_exists($tarFile)) {
            try {
                Phar::unlinkArchive($tarFile);
            } catch (Exception $e) {
                echo $e->getMessage(), "\n";
            }
        }
        if (file_exists($zipFile)) {
            try {
                Phar::unlinkArchive($zipFile);
            } catch (Exception $e) {
                echo $e->getMessage(), "\n";
            }
        }
    }

    public function delete_files($dir) {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file))
                self::delete_files($file);
            else
                unlink($file);
        }
        rmdir($dir);
        return true;
    }

    function actionTodaysChargeToUserForSubscriptionBundles() {
        //Find out all active studios
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_deleted' => 0));
        }

        if (isset($studios) && !empty($studios)) {
            $dbcon = Yii::app()->db;

            foreach ($studios as $key => $studio) {
                $studio_id = $studio->id;
                $is_process = 0;

                //Filter Customer, Lead and Demo studios otherwise no transaction process
                if ($studio_id > 0 && $studio->is_subscribed == 1 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Customer
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Lead
                    $is_process = 1;
                } else if ($studio_id > 0 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 1) {//Default
                    $is_process = 1;
                }

                if (intval($is_process)) {
                    //Find out Pyament gateway detail
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndSubscriptionBundlePlanExists($studio_id);
                    $activate = 0; //No plan and payment gateway set by studio
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $studio_payment_gateway = $plan_payment_gateway['gateways'];
                        $this->setPaymentGatwayVariable($studio_payment_gateway);
                        $activate = 1;
                    }

                    $cond = '';
                    $cond1 = " AND DATE_FORMAT(us.start_date, '%Y-%m-%d') = '" . gmdate('Y-m-d') . "'";
                    if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                        $cond = " AND u.id=" . $_REQUEST['user'];
                        $cond1 = " ";
                    }

                    $sql = "SELECT u.id AS user_id, us.id AS user_subscription_id, us.studio_payment_gateway_id
                         FROM user_subscription_bundles us LEFT JOIN sdk_users u ON (u.id=us.user_id AND 
                        u.studio_id=us.studio_id AND us.status=1 AND us.payment_status=0 " . $cond1 . ") WHERE u.status=1 AND 
                        u.is_deleted='0' AND u.studio_id=" . $studio_id . $cond;

                    $users = $dbcon->createCommand($sql)->queryAll();

                    //Find out all active subscribed sdk user with card detail
                    if (isset($users) && !empty($users)) {
                        foreach ($users as $key1 => $user) {
                            //Find out manual payment gateway which run by cron job
                            $gateway_info = StudioPaymentGateways::model()->findByPk($user['studio_payment_gateway_id']);

                            if (intval($activate) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
                                $gateway = PaymentGateways::model()->findByAttributes(array('id' => $this->GATEWAY_ID[$gateway_info->short_code], 'status' => 1, 'is_payment_gateway' => 1, 'is_automated' => 0));

                                //Find out all active subscribed sdk user with card detail
                                if (isset($gateway) && !empty($gateway)) {
                                    $uniqid = Yii::app()->common->generateUniqNumber();
                                    $sub = new UserSubscriptionBundles;
                                    $subsc = $sub->findByPk($user['user_subscription_id']);
                                    $subsc->payment_key = $uniqid;
                                    $subsc->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function actionProcessTransactionsOfUsersForSubscriptionBundles() {
        $cond = '';
        $limit = 50;

        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
            $cond = " AND u.id=" . $_REQUEST['user'];

            $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,sp.name AS plan_name,us.profile_id as profile_id  
            FROM user_subscription_bundles us LEFT JOIN sdk_users u ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 ),
            sdk_card_infos sci, subscriptionbundles_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.id=us.card_id AND sp.studio_id=us.studio_id AND us.plan_id=sp.id LIMIT 0, " . $limit;
        } else {
            $sql = "SELECT u.*, u.id AS user_id, us.*,us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.*,sp.name AS plan_name,us.profile_id as profile_id  
            FROM user_subscription_bundles us LEFT JOIN sdk_users u ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 AND us.payment_status=0 AND us.payment_key !='' AND DATE_FORMAT(us.start_date, '%Y-%m-%d') = '" . gmdate('Y-m-d') . "'),
            sdk_card_infos sci, subscriptionbundles_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.id=us.card_id AND sp.studio_id=us.studio_id AND us.plan_id=sp.id LIMIT 0, " . $limit;
        }

        $dbcon = Yii::app()->db;
        $users = $dbcon->createCommand($sql)->queryAll();

        if (isset($users) && !empty($users)) {
            foreach ($users as $key1 => $user) {
                $studio_id = $user['studio_id'];
                $studio = Studios::model()->findByPk($studio_id);

                $user_id = $user['user_id'];
                $user['studio_name'] = $studio->name;

                $currency = Currency::model()->findByPk($user['currency_id']);
                $user['currency_id'] = $currency->id;
                $user['currency_code'] = $currency->code;
                $user['currency_symbol'] = $currency->symbol;

                //Find out Pyament gateway detail
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndSubscriptionBundlePlanExists($studio_id);

                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $studio_payment_gateway = $plan_payment_gateway['gateways'];
                    $this->setPaymentGatwayVariable($studio_payment_gateway);
                }
                $gateway_info = StudioPaymentGateways::model()->findByPk($user['studio_payment_gateway_id']);
                $user['gateway_code'] = $gateway_info->short_code;
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $trans_data = $payment_gateway::processTransactions($user);
                $is_paid = $trans_data['is_success'];

                $sub = new UserSubscriptionBundles;
                $subsc = $sub->findByPk($user['user_subscription_id']);
                $bill_amount = $user['amount'];

                if (intval($is_paid)) {
                    //Getting Ip address
                    $ip_address = Yii::app()->request->getUserHostAddress();

                    //Save a transaction detail
                    $transaction = new Transaction;
                    $transaction->user_id = $user_id;
                    $transaction->studio_id = $studio_id;
                    $transaction->plan_id = $user['plan_id'];
                    $transaction->currency_id = $user['currency_id'];
                    $transaction->transaction_date = new CDbExpression("NOW()");
                    $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    $transaction->transaction_status = $trans_data['transaction_status'];
                    $transaction->invoice_id = $trans_data['invoice_id'];
                    $transaction->order_number = $trans_data['order_number'];
                    $transaction->dollar_amount = $trans_data['dollar_amount'];
                    $transaction->amount = $trans_data['paid_amount'];
                    $transaction->transaction_type = 7;
                    $transaction->response_text = $trans_data['response_text'];
                    $transaction->subscriptionbundles_id = $user['user_subscription_id'];
                    $transaction->ip = $ip_address;
                    $transaction->created_date = new CDbExpression("NOW()");
                    $transaction->save();

                    //Update next subscription date according to studio subscription plan
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $user['frequency'] . ' ' . strtolower($user['recurrence']) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));

                    $paid_amount = $trans_data['paid_amount'];

                    if (abs(($bill_amount - $paid_amount) / $paid_amount) < 0.00001) {
                        $isPaid = 1;
                    } else {
                        $isPaid = 0;
                        $subsc->payment_status = 1; //Partial Payment
                        $subsc->partial_failed_date = gmdate('Y-m-d h:i:s'); //Partial Payment date
                        $subsc->partial_failed_due = number_format((float) ($bill_amount - $paid_amount), 2, '.', ''); //Remaining due on partial payment
                    }

                    $subsc->start_date = $start_date;
                    $subsc->end_date = $end_date;
                    $subsc->payment_key = '';
                    $subsc->save();

                    if (intval($isPaid)) {
                        //Get invoice detail
                        $trans_amt = Yii::app()->common->formatPrice($trans_data['paid_amount'], $user['currency_id'], 1);
                        $user['amount'] = $trans_amt;


                        $file_name = Yii::app()->pdf->invoiceSubscriptionBundleDetial($studio, $user, $trans_data['invoice_id']);
                        //Send an email with invoice detail
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_bundle_renew', $file_name);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_bundle_renew', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_bundle_monthly_payment', $user_id, '', $trans_amt, $user['plan_name']);
                        }
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                        }
                    }
                } else {
                    $teModel = New TransactionErrors;
                    $teModel->studio_id = $studio_id;
                    $teModel->user_id = $user_id;
                    $teModel->plan_id = $user['plan_id'];
                    $teModel->subscription_bundle_id = $user['user_subscription_id'];
                    $teModel->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    $teModel->response_text = $trans_data['response_text'];
                    $teModel->created_date = new CDbExpression("NOW()");
                    $teModel->save();

                    $subsc->payment_key = '';
                    $subsc->payment_status = 2; //Payment failed
                    $subsc->partial_failed_date = gmdate('Y-m-d h:i:s'); //Payment failed date
                    $subsc->partial_failed_due = $bill_amount; //Due on failed payment

                    $subsc->status = 0; //Cancel account
                    $subsc->cancel_date = new CDbExpression("NOW()");
                    $subsc->cancel_note = "Subscription bundle cancelled on payment failure";
                    $subsc->canceled_by = 0;

                    $subsc->save();

                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                    if ($isEmailToStudio) {
                        $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                    }
                }
            }
        }
    }

    /**
     * @method public action DeleteDevices() Delete All Devices automatically after device switch duration 
     * @author Prakash<prakash@muvi.com>
     */
    public function actionDeleteDevices() {
        $sql = "SELECT studio_id,config_key,config_value as duration FROM studio_config WHERE config_key='limit_device_switch_duration'  ORDER BY studio_id ASC";
        $all_studios = Yii::app()->db->createCommand($sql)->queryAll();
        //Push Notification section
        $this->DevicePushNotificationStudioWise();
        foreach ($all_studios as $key => $value) {
            $update_query = "UPDATE device_management SET flag=9 WHERE NOW() >= DATE_ADD(deleted_date,INTERVAL {$value['duration']} MINUTE) AND studio_id={$value['studio_id']} AND flag=1";
            Yii::app()->db->createCommand($update_query)->execute();
        }
    }

    /**
     * DevicePushNotificationStudioWise is used for push notification
     * @param type $device_details array of device list
     * @return boolean
     */
    private function DevicePushNotificationStudioWise() {
        $device_details_query = "SELECT t.*,d.studio_id,d.user_id,d.device_info,d.google_id,d.device_type FROM(SELECT studio_id,config_key,config_value as duration FROM studio_config WHERE config_key='limit_device_switch_duration'  ORDER BY studio_id ASC) as t INNER JOIN device_management d ON t.studio_id=d.studio_id WHERE d.flag=1 AND d.google_id!='' AND NOW() >= DATE_ADD(d.deleted_date,INTERVAL t.duration MINUTE) ORDER BY d.user_id";
        $device_details = Yii::app()->db->createCommand($device_details_query)->queryAll();
        if ($device_details) {
            foreach ($device_details as $key => $device_value) {
                $push = new Push();
                $title = $device_value['user_id'];
                $reg_ids = array();
                $message = CHtml::encode($device_value['device_info']) . " Device Removed Successfully";
                $push->setTitle($title);
                $push->setMessage($message);
                array_push($reg_ids, $device_value['google_id']);
                //Android Devices
                if ($device_value['device_type'] == 1) {
                    $android_json_data = $push->getPushAndroid();
                    $android_response = $push->sendMultiple($reg_ids, $android_json_data);
                    //IOS devices
                } else if ($device_value['device_type'] == 2) {
                    $ios_json_data = $push->getPushIos();
                    $ios_response = $push->sendMultipleNotify($reg_ids, $ios_json_data);
                }
            }
        }
        return true;
    }
    
    public function actionCancel3rdPartyCouponSubscription() {
        $is_test_user = 0;
        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
            $is_test_user = 1;
            $users = Yii::app()->db->createCommand()
                    ->select(' *,u.id as user_id,us.id as subscription_id ')
                    ->from("sdk_users  u")
                    ->leftJoin('user_subscriptions us', 'us.user_id=u.id')
                    ->where('us.status=1 AND us.is_3rd_party_coupon=1 AND u.id=:user_id', array(':user_id' => $_REQUEST['user']))
                    ->queryAll();
        } else {
            $users = Yii::app()->db->createCommand()
                    ->select(' *,u.id as user_id,us.id as subscription_id ')
                    ->from("sdk_users  u")
                    ->leftJoin('user_subscriptions us', 'us.user_id=u.id')
                    ->where('us.status=1 AND us.is_3rd_party_coupon=1')
                    ->queryAll();
        }
        foreach ($users as $user) {
            $user_id = $user['user_id'];
            $studio_id = $user['studio_id'];
            $today = Date('Y-m-d');
            $due_date = Date('Y-m-d', strtotime($user['start_date']));
            if ($today >= $due_date || $is_test_user) {
                $subscription_id = $user['subscription_id'];
                $usersub = UserSubscription::model()->findByPk($subscription_id);
                $usersub->status = 0;
                $usersub->cancel_date = new CDbExpression("NOW()");
                $usersub->cancel_reason_id = '';
                $usersub->cancel_note = 'Subscription cancelled due to coupon expired';
                $usersub->payment_status = 0;
                $usersub->partial_failed_date = '';
                $usersub->partial_failed_due = 0;
                $usersub->save();
                //Send an email to user
                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'cancellation_coupon_expired');
            }
        }
    }
    
    /**
     * @method public actiondeleteRestrictStreamingDevice() Delete All data from Restrict Streaming Device which has last update time greater than 5 minute
     * @author Srutikant<srutikant@muvi.com>
     */   
    
    public function actiondeleteRestrictStreamingDevice(){
        $this->layout = false;
        RestrictDevice::model()->deleteDataInCron();
    }
    
    //Resetting the Demo user password in each month by Ratikanta
    public function actionResettestuser() {
        $this->layout = false;
        $password = '';
        $test_user_email = 'testmuvi@gmail.com';
        $ip_address = Yii::app()->request->getUserHostAddress();
        if ($ip_address == '52.0.64.95' || $ip_address == '52.0.232.150' || $ip_address == '52.211.41.107') {
            $criteria = new CDbCriteria;
            $criteria->select = 'db_host, db_name, db_user, db_pass, db_port'; // select fields which you want in output
            $criteria->condition = 't.status = 1';
            $servers = (object) AllServer::model()->findAll($criteria);

            if (count($servers) > 0) {
                $new_password = self::getRandomPassword();
                $enc = new bCrypt();
                if ($new_password) {
                    $password = $enc->hash($new_password);
                }
                foreach ($servers as $server) {
                    $newconn = Yii::app()->mvsecurity->createDbConnection($server->db_name, $server->db_user, $server->db_pass, $server->db_host, $server->db_port);
                    if ($newconn) {
                        $studio_data = $newconn->createCommand()->select('id, encrypted_password')->from('sdk_users')->where("email = '" . $test_user_email . "'")->queryAll();
                        if ($studio_data) {
                            foreach ($studio_data as $studio) {
                                $sql_pg = "UPDATE sdk_users SET encrypted_password = '" . $password . "' WHERE email = '" . $test_user_email . "'";
                                $command_pg = $newconn->createCommand($sql_pg);
                                $command_pg->execute();
                            }
                        }
                    }
                }
                $mailcontent = '<p>Password changed for test user Login!</p>';
                $mailcontent.= '<p>Email is <strong>' . $test_user_email . '</strong><br />New password is <strong>' . $new_password . '</strong></p>';
                $adminGroupEmail = array('all@muvi.com');

                $site_url = Yii::app()->getBaseUrl(true);
                $logo = EMAIL_LOGO;
                $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
                $params = array(
                    'website_name' => 'Muvi',
                    'mailcontent' => $mailcontent,
                    'logo' => $logo
                );

                $from_email = 'info@muvi.com';
                $from_name = 'Muvi';

                $adminSubject = "Password changed for test user in Live!";
                $template_name = 'demo_password_reset';
                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/demo_password_reset', array('params' => $params), true);
                $returnVal = $this->sendmailViaAmazonsdk($thtml, $adminSubject, $adminGroupEmail, $from_email, '', '', '', $from_name);
                echo "Password Changes";
            }
            exit;
        }
    }
    function actionInactiveMyLibrary(){
         //Find out all active studios (Customer Only)
        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {//test case
            $studios = Studios::model()->findAllByAttributes(array('id' => $_REQUEST['studio'], 'status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));
        } else {
            $studios = Studios::model()->findAllByAttributes(array('status' => 1, 'is_subscribed' => 1, 'is_default' => 0, 'is_deleted' => 0));
        }
		
        if (isset($studios) && !empty($studios)) {
            $users = array();
            foreach($studios as $key => $val){
                $user_cond = '';
                if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
                 $user_cond = ' AND user_id='.$_REQUEST['user'];
                }
                $sql = "SELECT US.* FROM ppv_subscriptions US WHERE US.studio_id = '" . $val->id . "' AND US.status = 1 AND US.is_advance_purchase=0 AND US.is_ppv_bundle=0 {$user_cond}";
                $dbcon = Yii::app()->db;
                $ppvs = $dbcon->createCommand($sql)->queryAll();
                foreach($ppvs as $pkey => $pval){
					/*$now = strtotime(gmdate('Y-m-d H:i:s'));
					$end_date = strtotime($pval['end_date']);
					print gmdate('Y-m-d H:i:s')."---Current Timestamp: ".$now."----------End date: ".$pval['end_date'].'--- End date Timestamp:'.$end_date."---".$pval['id']."<hr/>";
					*/
                    //if(intval($pval['user_id']) && ($now > $end_date)) {
                    if(intval($pval['user_id'])) {
                        $content_type = Film::model()->findByPk($pval['movie_id'])->content_types_id;
                        if($content_type == 3){
                          // $get_ppv_subscription_data = PpvSubscription::model()->findByAttributes(array('user_id'=>$val['user_id'],'studio_id'=>$val['studio'],'movie_id'=>$val['movie']));
                         //  $muvi_streem = movieStreams::model()->findByAttributes(array('movie_id'=>$get_ppv_subscription_data->movie_id,'studio_id'=>$get_ppv_subscription_data->studio_id));
                            $subscription_data_multi = self::setppv_data($pval);
                           if($pval['movie_id'] != '' && $pval['season_id'] != 0 && $pval['episode_id'] != 0){ //111
                               self::check_validity_for_single_part_content($pval,$subscription_data_multi,$pval['episode_id']);
     
                             }else if($pval['movie_id'] != '' && $pval['season_id'] != 0 && $pval['episode_id'] == 0){//110
                                $subscription_data_multi = self::setppv_data($pval);
                                $available_episods = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$pval['season_id']));
                                if( $pval['access_period'] != '' && $pval['watch_period'] != '' && $pval['view_restriction'] != 0){ //111
                                    $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                    if($return){
                                       $session_avleable = array();
                                       $no_of_view_available = array();
                                        foreach($available_episods as $ekey => $eval){
                                           $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$eval['id']); 

                                           if($return){
                                               $session_avleable[] = 1;
                                                 $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $eval['id']); 
                                                 if($return){
                                                   $no_of_view_available[] = 1;  
                                                 }
                                           }else{
                                              $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $eval['id']); 
                                              if($return){

                                                  $no_of_view_available[] = 1;
                                              }
                                           }
                                        }
                                       if(count($session_avleable) < 1 && count($no_of_view_available) < 1){
                                           PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);  
                                       }
                                       if(count($session_avleable) < 1){
                                           PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);  
                                       }
                                       if(count($no_of_view_available) < 1){
                                           PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);  
                                       }                 

                                    }else{
                                         PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']); 
                                    }
                                }else if($pval['access_period'] != '' && $pval['watch_period'] != '' && $pval['view_restriction'] == 0){ //110
                                   $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                   $session_avleable1 = array();
                                   if($return){
                                        foreach($available_episods as $ekey => $eval){
                                            $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$eval['id']);
                                            if($return){
                                                $session_avleable1[] = 1;
                                            }
                                        }

                                       if(count($session_avleable1) < 1){
                                           PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']); 
                                       }
                                   }else{
                                       PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);   
                                   }
                                }else if( $pval['access_period'] != '' && $pval['watch_period'] == '' && $pval['view_restriction'] != 0){ //101
                                    $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                    if($return){
                                       $session_avleable = array();
                                       $no_of_view_available = array();
                                        foreach($available_episods as $ekey => $eval){
                                              $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $eval['id']); 
                                              if($return){
                                                  $no_of_view_available[] = 1;
                                              }
                                        }
                                       if(count($no_of_view_available) < 1){
                                           PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);  
                                       }
                                    }else{
                                         PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']); 
                                    }
                                }else if($pval['access_period'] != '' && $pval['watch_period'] == '' && $pval['view_restriction'] == 0){ //100
                                    $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                    if(!$return){
                                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                    }

                                }else if($pval['access_period'] == '' && $pval['watch_period'] != '' && $pval['view_restriction'] != 0){ //011
                                    $session_avleable2 = array();
                                    $no_of_view_available2 = array();
                                    foreach($available_episods as $ekey => $eval){
                                        $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$eval['id']);
                                        if($return){
                                            $session_avleable2[] = 1;
                                            $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $eval['id']);
                                           if($return){
                                             $no_of_view_available2[] = 1;
                                           }
                                        }else{
                                            $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $eval['id']); 
                                            if($return){
                                                $no_of_view_available2[] = 1;
                                            }
                                        }
                                     }
                                     if(count($no_of_view_available2) < 1 && count($session_avleable2)< 1){
                                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                    if(count($session_avleable2)< 1){
                                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }  
                                    if(count($no_of_view_available2)< 1){
                                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }                   
                                }else if($pval['access_period'] == '' && $pval['watch_period'] != '' && $pval['view_restriction'] == 0){ //010
                                    $session_avleable3 = array(); 
                                    foreach($available_episods as $ekey => $eval){
                                         $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$eval['id']);
                                         if($return){
                                             $session_avleable3[] = 1;
                                         }
                                     }

                                     if(count($session_avleable3) < 1){
                                         PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                }else if($pval['access_period'] == '' && $pval['watch_period'] == '' && $pval['view_restriction'] != 0){ //001
                                    $no_of_view_available3 = array();
                                     foreach($available_episods as $ekey => $eval){
                                         $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $eval['id']); 
                                         if($return){
                                             $no_of_view_available3[] = 1;
                                         }
                                     } 
                                     if(count($no_of_view_available3) < 1){
                                         PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                }


                             }else if($pval['movie_id'] != '' && $pval['season_id'] == 0 && $pval['episode_id'] == 0){//110
                                 $get_available_session =  movieStreams::model()->get_available_session($subscription_data_multi);
                                if($pval['access_period'] != '' && $pval['watch_period'] != '' && $pval['view_restriction'] != 0){ //111
                                     $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                     if($return){
                                             $availabla_episod = array();
                                             $available_view = array();
                                             foreach($get_available_session as $skey => $sval){
                                                  $available_episod = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$sval['series_number']));
                                                  foreach($available_episod as $key => $aval){

                                                      $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$aval['id']);
                                                      if($return){
                                                          $availabla_episod[] = 1;
                                                          $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $aval['id']);
                                                          if($return){
                                                             $available_view[] = 1; 
                                                          }
                                                      }else{
                                                          $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $aval['id']);
                                                          if($return){
                                                              $available_view[] = 1;
                                                          }
                                                      }
                                                  }

                                              }
                                               if(count($availabla_episod) < 1 && count($available_view) < 1){
                                                   PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                               }
                                                if(count($availabla_episod) < 1){
                                                   PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                               }
                                               if(count($available_view) < 1){
                                                   PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                               }                                               
                                     }else{
                                         PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                }else if($pval['access_period'] != '' && $pval['watch_period'] != '' && $pval['view_restriction'] == 0){ //110
                                    $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                    if($return){
                                        $availabla_episod1 = array();
                                         foreach($get_available_session as $skey => $sval){
                                              $available_episod = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$sval['series_number']));
                                              foreach($available_episod as $key => $aval){
                                                  $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$aval['id']);
                                                  if($return){
                                                      $availabla_episod1[] = 1;
                                                  }
                                              }
                                         }
                                         if(count($availabla_episod1) < 1){
                                              PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                         }
                                    }else{
                                       PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                    }
                                } else if($pval['access_period'] != '' && $pval['watch_period'] == '' && $pval['view_restriction'] != 0){ //101
                                     $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                     if($return){
                                             $available_view_9 = array();
                                             foreach($get_available_session as $skey => $sval){
                                                  $available_episod = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$sval['series_number']));
                                                  foreach($available_episod as $key => $aval){
                                                    $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $aval['id']);
                                                    if($return){
                                                        $available_view_9[] = 1;
                                                    }
                                                  }
                                              }
                                               if(count($available_view_9) < 1){
                                                   PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                               }                                               
                                     }else{
                                         PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                }else if($pval['access_period'] != '' && $pval['watch_period'] == '' && $pval['view_restriction'] == 0){ //100
                                    $return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data_multi);
                                    if(!$return){
                                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                    }

                                }else if($pval['access_period'] == '' && $pval['watch_period'] != '' && $pval['view_restriction'] != 0){ //011
                                    $availabla_episod2 = array();
                                     $available_view2 = array();
                                     foreach($get_available_session as $skey => $sval){
                                          $available_episod = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$sval['series_number']));
                                         foreach($available_episod as $key => $aval){
                                             $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$aval['id']);
                                             if($return){
                                                 $availabla_episod2[] = 1;
                                                 $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $aval['id']);
                                                 if($return){
                                                    $available_view2[] = 1; 
                                                 }
                                             }else{
                                                 $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $aval['id']);
                                                 if($return){
                                                     $available_view2[] = 1;
                                                 }
                                             }
                                         }
                                     }
                                     if(count($availabla_episod2) < 1 && count($available_view2) < 1){
                                          PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                     if(count($availabla_episod2) < 1){
                                          PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }
                                     if(count($available_view2) < 1){
                                          PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                     }                                     
                                }else if($pval['access_period'] == '' && $pval['watch_period'] != '' && $pval['view_restriction'] == 0){ //010
                                    $availabla_episod3 = array();
                                    foreach($get_available_session as $skey => $sval){
                                          $available_episod = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$sval['series_number']));
                                         foreach($available_episod as $key => $aval){
                                             $return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data_multi,$aval['id']);
                                             if($return){
                                                 $availabla_episod3[] = 1;
                                             }
                                         }
                                    }
                                    if(count($availabla_episod3) < 1){

                                       PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                    }
                                }else if($pval['access_period'] == '' && $pval['watch_period'] == '' && $pval['view_restriction'] != 0){ //001
                                    $available_view3 = array();
                                    foreach($get_available_session as $skey => $sval){
                                        $available_episod = movieStreams::model()->get_available_episod(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id'],'series_number'=>$sval['series_number']));
                                        foreach($available_episod as $key => $aval){
                                            $return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data_multi, $aval['id']);
                                            if($return){
                                                $available_view3[] = 1;
                                            }
                                        }
                                    }
                                    if(count($available_view3) < 1){
                                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($pval['id']);
                                    }
                                }
                             }
                        }else {
                           $subscription_data = self::setppv_data($pval); 
                           $muvi_streem = movieStreams::model()->findByAttributes(array('movie_id'=>$pval['movie_id'],'studio_id'=>$pval['studio_id']));   
                           self::check_validity_for_single_part_content($pval,$subscription_data,$muvi_streem->id);
                        }
                    }   
                } 
                
            }

        }
    }
	public function check_validity_for_single_part_content($data = array(), $subscription_data = array(), $stream_id = 0) {
		if (trim($data['access_period']) && trim($data['watch_period']) && intval($data['view_restriction'])) {//111
			$return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data);
			if ($return) {
				$return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data, $stream_id);
				if ($return) {
					$return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data, $stream_id);
					if (!$return) {
						PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
					}
				} else {
					PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
				}
			} else {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		} else if ($data['access_period'] != '' && $data['watch_period'] != '' && $data['view_restriction'] == 0) {//110
			$return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data);
			if ($return) {
				$return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data, $stream_id);
				if (!$return) {
					PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
				}
			} else {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		} else if ($data['access_period'] != '' && $data['watch_period'] == '' && $data['view_restriction'] != 0) {//101
			$return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data);
			if ($return) {
				$return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data, $stream_id);
				if (!$return) {
					PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
				}
			} else {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		} else if ($data['access_period'] != '' && $data['watch_period'] == '' && $data['view_restriction'] == 0) {//100
			$return = Yii::app()->common->isAccessPeriodOnContentExceeds($subscription_data);
			if (!$return) {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		} else if ($data['access_period'] == '' && $data['watch_period'] != '' && $data['view_restriction'] != 0) {//011
			$return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data, $stream_id);
			if ($return) {
				$return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data, $stream_id);
				if (!$return) {
					PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
				}
			} else {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		} else if ($data['access_period'] == '' && $data['watch_period'] != '' && $data['view_restriction'] == 0) {//010
			$return = Yii::app()->common->isWatchPeriodOnContentExceeds($subscription_data, $stream_id);
			if (!$return) {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		} else if ($data['access_period'] == '' && $data['watch_period'] == '' && $data['view_restriction'] != 0) {//001
			$return = Yii::app()->common->isNumberOfViewsOnContentExceeds($subscription_data, $stream_id);
			if (!$return) {
				PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
			}
		}
	}

	public function setppv_data($data){
        $subscription_data['user_id'] = $data['user_id'];
        $subscription_data['movie_id'] = $data['movie_id'];
        $subscription_data['studio_id'] = $data['studio_id'];
        $subscription_data['created_date'] = $data['created_date'];
        $subscription_data['access_period'] = $data['access_period'];
        $subscription_data['watch_period'] = $data['watch_period'];
        $subscription_data['view_restriction'] = $data['view_restriction'];
        return $subscription_data;
    }
	
	public function actionRemoveTestUsers() {
		$cond = '';
		if (isset($_REQUEST['studio']) && trim($_REQUEST['studio']) && isset($_REQUEST['email']) && trim($_REQUEST['email'])) {//test case
			if (strpos($_REQUEST['email'], '@mvrht.') !== false || strpos($_REQUEST['email'], '@muvi.') !== false) {
				$cond=" studio_id= {$_REQUEST['studio']} AND email='{$_REQUEST['email']}'";
			}
		} else {
			$email_domain = array('mvrht.com', 'mvrht.net');
			foreach ($email_domain as $value) {
				$cond.=" email like '%@".$value."%' OR ";
			}
			
			$cond = trim($cond, ' OR ');
		}
		
		$sql = "SELECT GROUP_CONCAT(id) AS user_id from sdk_users where {$cond}";
		
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryRow();

        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $data['user_id'] = trim($data['user_id'], ',');

            $sql_vlog = "DELETE FROM `video_logs` WHERE `video_logs`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_vlog)->execute();

            $sql_blog = "DELETE FROM `bandwidth_log` WHERE `bandwidth_log`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_blog)->execute();

            $sql_trn = "DELETE FROM `transactions` WHERE `transactions`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_trn)->execute();

            $sql_ci = "DELETE FROM `sdk_card_infos` WHERE `sdk_card_infos`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_ci)->execute();

            $sql_us = "DELETE FROM `user_subscriptions` WHERE `user_subscriptions`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_us)->execute();

            $sql_ppv = "DELETE FROM `ppv_subscriptions` WHERE `ppv_subscriptions`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_ppv)->execute();

            $sql_lh = "DELETE FROM `login_history` WHERE `login_history`.`user_id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_lh)->execute();

            $sql_po = "SELECT GROUP_CONCAT(id) AS order_id from pg_order where buyer_id IN ({$data['user_id']})";
            $data_po = $dbcon->createCommand($sql_po)->queryRow();

            if (isset($data_po['order_id']) && !empty($data_po['order_id'])) {
                $data_po['order_id'] = trim($data_po['order_id'], ',');

                $sql_pod = "DELETE FROM `pg_order_details` WHERE `pg_order_details`.`order_id` IN ({$data_po['order_id']})";
                $dbcon->createCommand($sql_pod)->execute();

                $sql_po = "DELETE FROM `pg_order` WHERE `pg_order`.`id` IN ({$data_po['order_id']})";
                $dbcon->createCommand($sql_po)->execute();
            }

            $sql_usr = "DELETE FROM `sdk_users` WHERE `sdk_users`.`id` IN ({$data['user_id']})";
            $dbcon->createCommand($sql_usr)->execute();
        }

        print 'Success';
        exit;
    }
	
}
