<div class="row m-t-20">
    <div class="col-sm-10">		
        <form class="form-horizontal" class="update_restriction" name="update_favourite" id="update_restriction" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/UserRestrictions" method='post'>            
			<div class="form-group">     
				<div class="col-md-4">
					<div class="checkbox">
						<label>
							<input type="checkbox" onClick="activate_limited_devices();" id="restrict_no_devices" name="restrict_no_devices" value="<?php echo ($restrict_no_devices == 0) ? 0 : 1 ?>" <?php if($checkAppSelected){echo ($restrict_no_devices) ? ' checked="checked"' : '';}else{ echo 'disabled="disabled"'; } ?> /> <i class="input-helper"></i>Restrict number of devices</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fg-line">
						<input type="text" name="limit_devices" id="limit_devices" class="form-control input-sm" onkeypress="return isNumberKey(event)" value="<?php echo  @$limit_devices ;?>" <?php if(!$checkAppSelected || !$restrict_no_devices){?> style="display:none;"<?php } ?> maxlength="2" onkeyup="remove_error_msg('error_device');"/>
					</div>
					<span class="error red error_notify" id="error_device" style="display:none;"></span>
				</div>
			</div>
			<div class="form-group" id="delete_duration" <?php if(!$restrict_no_devices){ ?> style="display:none;" <?php } ?>>     
				<div class="col-md-4">
					<div class="checkbox">
						<label>
							<input type="checkbox" onClick="activate_limited_switch_duration();" id="restrict_device_switch_duration" name="restrict_device_switch_duration" value="<?php echo ($restrict_device_switch_duration == 0) ? 0 : 1 ?>" <?php if($checkAppSelected){ echo ($restrict_device_switch_duration) ? ' checked="checked"' : '';}else{echo 'disabled="disabled"';} ?> /> <i class="input-helper"></i>Device delete duration (Minutes)</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fg-line">
						<input type="text" name="limit_device_switch_duration" id="limit_device_switch_duration" class="form-control input-sm" onkeypress="return isNumberKey(event)" value="<?php echo ($limit_device_switch_duration>0) ? $limit_device_switch_duration:5;?>" <?php if(!$checkAppSelected || !$restrict_no_devices || !$restrict_device_switch_duration){?> style="display:none;" <?php } ?> maxlength="5" onkeyup="remove_error_msg('error_duration');"/>                                    
					</div>
					<span class="error red error_notify" id="error_duration" style="display:none;"></span>
				</div>
			</div>                                      
            <div class="form-group">                
				<div class="col-md-4">
					<div class="checkbox">
						<label>
							<input type="checkbox" onClick="activate_limited_streaming_devices();" id="restrict_streaming_devices" name="restrict_streaming_devices" value="<?php echo ($restrict_streaming_devices>0) ? 1 : 0 ?>" <?php echo ($restrict_streaming_devices > 0) ? ' checked="checked"' : ''; ?> /> <i class="input-helper"></i>Restrict no of simultaneous streaming devices</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fg-line">
						<input type="text" name="limit_streaming_devices" id="limit_streaming_devices" class="form-control input-sm" onkeypress="return isNumberKey(event)" value="<?php echo ($restrict_streaming_devices>0) ? $restrict_streaming_devices:1;?>" <?php if(!$restrict_streaming_devices){?> style="display:none;" <?php } ?> maxlength="3" onkeyup="remove_error_msg('error_streaming_devices');"/>                                    
					</div>
					<span class="error red error_notify" id="error_streaming_devices" style="display:none;"></span>
				</div>
			</div>	
            <div class="form-group">                
                <div class="col-md-12">                  
					<input type="button" class="btn btn-primary" name="favbtn" id="favbtn" value="Save"/>
                </div>
            </div>
        </form>
    </div>        
</div>
<script type="text/javascript">   
	function isNumberKey(e){
		var unicode = e.charCode ? e.charCode : e.keyCode;
		if ((unicode !== 8) && (unicode !== 9)) {
			if (unicode >= 48 && unicode <= 57)
				return true;
			else
				return false;
		}
	}
	function activate_limited_devices(){ 
		$(".error_notify").hide();
		if($('#restrict_no_devices').is(':checked')){        			
			$('#delete_duration').show();						
			$('#limit_devices').show();
			$('#limit_device_switch_duration').hide();
			$('#limit_device_switch_duration').val(5);
		}else{	
			 $('#limit_devices').hide();
			 $('#limit_devices').val(1);
			 $('#delete_duration').hide();
			 $('#restrict_device_switch_duration').attr('checked',false);
			 $('#limit_device_switch_duration').val(5);			
		}
	}
	function activate_limited_switch_duration(){
		$(".error_notify").hide();
		if($('#restrict_device_switch_duration').is(':checked')){
			$('#limit_device_switch_duration').show();	
			$('#limit_device_switch_duration').val(5);
		}else{
			$('#limit_device_switch_duration').hide();
			$('#limit_device_switch_duration').val(5);			
		}
	}		
	function activate_limited_streaming_devices(){
		$(".error_notify").hide();
		if($('#restrict_streaming_devices').prop('checked')){
			$('#limit_streaming_devices').show();
		}else{
			$('#limit_streaming_devices').hide();
		}		
	}
    $(document).ready(function(){        
        $("#favbtn").click(function(){					
			//Check restrict no of devices and its value with the value of simultanious login
			if($('#restrict_no_devices').prop('checked')){
				var limit_devices=parseInt($('#limit_devices').val());
				if(limit_devices < 1){
					$('#error_device').show();
					$('#error_device').html('Value should not be less than 1');
					return false; 
				}
			}
			//Check Device Switch Duration and its value
			if($('#restrict_device_switch_duration').prop('checked')){				
				if(parseInt($('#limit_device_switch_duration').val()) < 5){					
					$("#error_duration").show();
                    $("#error_duration").html("Value should not be less than 5");
					return false;
				}
			}
			//Check restrict no of Streaming Devices
			if($('#restrict_streaming_devices').prop('checked')){
				var limit_streaming_devices = parseInt($('#limit_streaming_devices').val());
				if(limit_streaming_devices < 1){					
					$("#error_streaming_devices").show();
                    $("#error_streaming_devices").html("Value should not be less than 1");
					return false;
				}
				//check validation 
				if($('#restrict_no_devices').prop('checked')){
					var limit_devices = parseInt($('#limit_devices').val());
					if(limit_devices < limit_streaming_devices){
						$('#error_streaming_devices').show();
						$('#error_streaming_devices').html('Number of streaming devices can not be greater than number of restricted devices');
						return false;
					}
				}
			}
				$('#update_restriction').submit();
        });
    });
	function remove_error_msg(element_id){	
		$('#'+element_id).hide().html('');
	}
</script>

