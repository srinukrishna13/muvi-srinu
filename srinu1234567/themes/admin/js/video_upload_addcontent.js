function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('#addvideoforcontent button,input[type="text"]').attr('disabled', 'disabled');
        $('.savefile button').attr('disabled', 'disabled');

    } else {
        $('.loaderDiv').hide();
        $('#addvideoforcontent button,input[type="text"]').removeAttr('disabled');
        $('.savefile button').removeAttr('disabled');
    }
}
function videopopupformnewcontent(movie_id, name, movie_stream_id, content_types_id) {
    $('input[type=file]').val('');
    $('#pop_movie_name').html(name);
    $('#content_types_id').html(content_types_id);
    $("#addvideoforcontent").modal('show');
    showLoader();
    var key_word = $("#search_video").val();
    var key_word = key_word.trim();
    $.ajax({
        url: HTTP_ROOT + "/admin/ajaxSearchVideoForContent",
        data: {'search_word': key_word},
        contentType: false,
        cache: false,
        success: function (result)
        {
            showLoader(1);
            $("#video_newcontent_div").html(result);
        }
    });
}
function validateURL(divid) {
    showLoader();
    var action_url = HTTP_ROOT + "/admin/validateVideo";
    if(divid){
        $('#dropbox').html('');
        var url = $('#dropbox').val();
    }else{
        $('#server_url_error').html('');
        var url = $('#server_url').val();
    }
    var movie_id = $('#movie_id').val();
    var movie_stream_id = $('#movie_stream_id').val();
    var ftpusername = $('#ftpusername').val();
    var ftppassword = $('#ftppassword').val();
    if (url && isUrlValid(url)) {
        $.post(action_url, {'url': url, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id,'username':ftpusername,'password':ftppassword}, function (res) {
            showLoader(1);
            if (res.error) {
                if(divid){
                    $('#dropbox_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                }else{
                    $('#server_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                }
            } else {
                if (res.is_video) {
                    $("#addvideoforcontent").modal('hide');
                    if(divid){
                        $('#dropbox').val('');
                        $('#dropbox_url_error').val('');
                    }else{
                        $('#server_url').val('');
                        $('#server_url_error').val('');
                    }  
                    var text = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video download in progress. It will be available within an hour" class="btn btn-default-with-bg btn-sm"><em class="fa fa-download"></em> Download in progress</a>';
                    $('#videodiv').html(text);
                } else {
                    if(divid){
                        $('#dropbox_url_error').html('Please provide the path of a valid video file');
                    }else{
                        $('#server_url_error').html('Please provide the path of a valid video file');
                    } 
                }
            }
        }, 'json');
        console.log('valid url');
    } else {
        showLoader(1);
        if(divid){
            $('#dropbox_url_error').html('Please enter a valid url.');
        }else{
            $('#server_url_error').html('Please enter a valid url.');
        } 
    }
}
$(document).ready(function () {    
    $( "#filetype" ).change(function() {
        $('.savefile').hide();
        $('#'+$(this).val()+'_div').show();
    });
});
function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}
function addvideoFromVideoGalleryFromContent(videoUrl, galleryId) {
    showLoader();
    var action_url = HTTP_ROOT + "/admin/addVideoFromVideoGallery";
    $('#server_url_error').html('');
    var movie_id = $('#movie_id').val();
    var movie_stream_id = $('#movie_stream_id').val();
    $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
        showLoader(1);
        if (res.error) {
            $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
        } else {
            if (res.is_video === 'uploaded') {
                window.location.href = window.location.href;
            } else if (res.is_video) {
                $("#addvideoforcontent").modal('hide');
                $('#server_url').val('');
                $('#server_url_error').val('');
                var text = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour" class="btn btn-default-with-bg btn-sm"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a>';
                $('#videodiv').html(text);
            } else {
                $('#server_url_error').html('There seems to be something wrong with the video');
            }
        }
    }, 'json');
    console.log('valid url');
}
function reset_filter()
{
    $("#video_duration").val(0);
    $("#file_size").val(0);
    $("#is_encoded").val(0);
    $("#uploaded_in").val(0);
    view_filtered_list();
}



function view_filtered_list() {

    showLoader();
    var video_duration = $("#video_duration").val();
    var file_size = $("#file_size").val();
    var is_encoded = $("#is_encoded").val();
    var uploaded_in = $("#uploaded_in").val();

    var url = HTTP_ROOT + "/admin/ajaxFiltervideo/video_duration/" + video_duration + "/file_size/" + file_size + "/is_encoded/" + is_encoded + "/uploaded_in/" + uploaded_in;
    //window.location.href = url;

    $.post(url, {'video_duration': video_duration, 'file_size': file_size, 'is_encoded': is_encoded, 'uploaded_in': uploaded_in}, function (res) {
        if (res) {
            $('#video_content_div').html();
            showLoader(1);

            $('#video_content_div').html(res);
        }
    });
}
function searchvideo1() {
    var key_word = $("#search_video1").val();
    var key_word = key_word.trim();
    if (key_word.length >= 3 || key_word.length == 0) {
        $('.loaderDiv').show();
        $('#profile button').attr('disabled', 'disabled');

        $.ajax({
            url: HTTP_ROOT + "/admin/ajaxSearchVideoForContent",
            data: {'search_word': key_word},
            contentType: false,
            cache: false,
            success: function (result)
            {
                $('.loaderDiv').hide();
                $('#profile button').removeAttr('disabled');
                $("#video_content_div").html(result);
            }
        });
    } else {
        return;
    }
}
// Multipart Upload Code
var s3upload = null;
var s3obj = new Array();
function uploadContent(user, pass, stream_id, movie_name, movie_st_id, content_types_id) {
    var contentType = content_types_id;
    //var xhr = new XMLHttpRequest({mozSystem: true});
    var filename = $('input[type=file]').val().split('\\').pop();
    if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
        alert("Sorry! You are using an older or unsupported browser. Please update your browser");
        return;
    }

    var file = $('#videofilecontent')[0].files[0];
    var filename = file.name;
    filename = filename.replace(/(\.[^/.]+)+$/, "").replace(/[^a-z0-9.\s]/gi, '_').replace(/ /g, "_").replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') + "." + filename.replace(/^.*\./, '');
    file.name = filename;
    s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'movie_id': movie_st_id});
    s3upload.onServerError = function (command, jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 403) {
            alert("Sorry you are not allowed to upload");
        } else {
            if (command == 'CompleteMultipartUpload') {
                window.setTimeout(function () {
                    s3upload.completeMultipartUpload();
                }, s3upload.RETRY_WAIT_SEC * 1000);
            } else if (command == 'CreateMultipartUpload') {
                window.setTimeout(function () {
                    s3upload.createMultipartUpload();
                }, s3upload.RETRY_WAIT_SEC * 1000);
            }
            console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
        }
    };

    s3upload.onS3UploadError = function (xhr) {
        s3upload.waitRetry();
        console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
    };

    //s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
    s3upload.onProgressChanged = function (progPercent) {
        if (progPercent == 100) {
            $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
        } else {
            var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
            $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

            $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
        }
    };

    s3upload.onUploadCompleted = function () {
        $('#upload_' + stream_id).remove();
        var purl = '';
        var playtext = '';
        //purl = HTTP_ROOT + "/video/play_video?movie="+movie_st_id+"&movie_stream_id="+stream_id+"&preview=1";
        //playtext = '<a href="'+purl+'" target="_blank"><span class="glyphicon glyphicon-play-circle"></span></a>&nbsp;';
        var convText = '<h5><a title="Video being encoded. It will be available within an hour" data-toggle="tooltip" href="javascript:void(0);"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a></h5>';
        //var convText = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><div class="col-lg-12 no-padding"><div class="col-lg-1 no-padding"><span class="glyphicon glyphicon-refresh glyphicon-spin glyphicon-2x"></span></div> <div class="col-lg-11 adminaction-icon">Encoding in progress</div></div><div class="cb" style="clear: both;"></div> </a>'; 
        //$('#'+stream_id).html(playtext);
        $('#videodiv').html(convText);
        $('#videodiv a').removeAttr('disabled');
        $('#videoCorrupt' + stream_id).hide();
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }
        if (contentType != 1) {
            $.post(HTTP_ROOT + "/cron/getThumbnail");
        }
        var text = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour" class="btn btn-default-with-bg btn-sm"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a>';
        $('#videodiv').html(text);
        console.log("Congratz, upload is complete now");
    };
    s3upload.onUploadCancel = function () {
        $('#upload_' + stream_id).remove();
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }
        var utext = '<a href="javascript:void(0);" onclick="videopopupformnewcontent(' + movie_st_id + ',\'' + movie_name + '\',' + stream_id + ',' + content_types_id + ');" class="btn btn-default-with-bg btn-sm"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload video</a>';
        //var utext = '<a href="javascript:void(0);" onclick="videopopupformnewcontent('+movie_st_id+',\''+movie_name+'\','+stream_id+','+content_types_id+');"><span class="glyphicon glyphicon-upload" title="Upload Video"></span></a>&nbsp;';
        $('#videodiv').html(utext);
        console.log("Upload Cancelled..");
    };

//Start s3 Multipart upload	
    s3upload.start();
    s3obj[stream_id] = s3upload;
    var progressbar = '<div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header"><div style="float:left;font-weight:bold;">File Upload Status</div><div id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i>&nbsp;</div><div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div></div><div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"><div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + '</h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div></div>';
    $('#dprogress_bar').append(progressbar);
    $('#cancel_' + stream_id).on('click', function () {
        aid = this.id;
        sid = aid.split('_');
        swal({
            title: "Cancel Upload?",
            text: "Are you sure you want to cancel this upload?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            s3obj[sid[1]].cancel();
            document.getElementById("videofilecontent").value = "";
            $('#dprogress_bar').html('');
            $('#dprogress_bar').hide();
        });

    });
}

// Checking File extenson and size
function click_browsecontent(videoid){
    $('#'+videoid).click();
}
function checkfileSizeContent() {
    var movie_name = $('#mname').val();
    var stream_id = $('#movie_stream_id').val();
    var movie_st_id = $('#movie_id').val();
    var filename = $('#videofilecontent').val().split('\\').pop();
    var content_types_id = $('#content_types_id').val();
    var extension = filename.replace(/^.*\./, '');
    if (extension == filename) {
        extension = '';
    } else {
        extension = extension.toLowerCase();
    }
    switch (extension) {
        case 'mp4':
            break;
        case 'mov':
            break;
        case 'mkv':
            break;
        case 'flv':
            break;
        case 'vob':
            break;
        case 'm4v':
            break;
        case 'avi':
            break;
        case '3gp':
            break;
        case 'mpg':
            break;
        case 'wmv':
            break;
        default:
            // Cancel the form submission
            swal('Sorry! This video format is not supported. \n MP4/MOV/MKV/FLV/VOB/M4V/AVI/3GP/MPG/WMV format video are allowed to upload');
            return false;
            break;
    }
    swal({
        title: "Upload File?",
        text: "Are you sure to upload " + filename + " file for  " + movie_name + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#10CFBD",
        customClass: "cancelButtonColor",
        confirmButtonText: "Upload",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true,
        html: true
    },
    function (isConfirm) {
        if (isConfirm) {
            $("#addvideoforcontent").modal('hide');
            if (!$('#dprogress_bar').is(":visible")) {
                $('#dprogress_bar').show();
            }
            var rtext = "<a href='javascript:void(0);' class='disabled-upload btn btn-default-with-bg btn-sm'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload video</a>";
            $('#videodiv').html(rtext);
            $('#videodiv a').attr('disabled', 'disabled');
            uploadContent('user', 'pass', stream_id, movie_name, movie_st_id, content_types_id);
            return true;
        } else {
            return false;
        }
    });

}
$.fn.bindFirst = function (name, fn) {
    // bind as you normally would
    // don't want to miss out on any jQuery magic
    this.on(name, fn);

    // Thanks to a comment by @Martin, adding support for
    // namespaced events too.
    this.each(function () {
        var handlers = $._data(this, 'events')[name.split('.')[0]];
        // take out the handler we just inserted from the end
        var handler = handlers.pop();
        // move it at the beginning
        handlers.splice(0, 0, handler);
    });
};