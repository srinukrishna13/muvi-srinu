<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BlogPost extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'blog_posts';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::BELONGS_TO, 'Studio', 'studio_id'),
            'comments'=>array(self::HAS_MANY, 'BlogComment', 'post_id'),
        );
    } 
    
    public function findAllPosts($studio_id) {
        //Find All Blog Posts        
        $posts = BlogPost::model()->findAll(array(
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id ASC'
        ));          
        return($posts);
    }
}

