<?php
require 's3bucket/aws-autoloader.php';
use Aws\CloudFront\CloudFrontClient;
class PlayerController extends Controller {
	protected function beforeAction($action) {
        parent::beforeAction($action);
		
        if (!(Yii::app()->user->id)) {
        $studio_id = Yii::app()->common->getStudiosId();
			$std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
			$free_content_login = 1;
			if(isset($std_config->config_value) && $std_config->config_value == 0){
				$free_content_login = 0;
            }
			
			if(intval($free_content_login)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
			$this->redirect(Yii::app()->getBaseUrl(TRUE).'/user/login');exit;
        }
        }
        return true;
    }    
	
    public function actionPlayMovie() {
        Yii::app()->theme = 'bootstrap';
		
        if (isset($_REQUEST['contentPlink']) && $_REQUEST['contentPlink']) {
            $permaLink = $_REQUEST['contentPlink'];
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You are trying to play an invalid content');
            $this->redirect(Yii::app()->getBaseUrl(TRUE));
            exit;
        }
        $contentTypePermalink = @$_REQUEST['contentType'];
        $studio = Yii::app()->common->getStudiosId(1);
        $studio_id = $studio->id;
        $authToken = OuathRegistration::model()->findByAttributes(array('studio_id' => $studio_id), array('select'=>'oauth_token'))->oauth_token;
        $user_id = Yii::app()->user->id;        
		if ($permaLink) {
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.content_types_id,f.mapped_id')
                    ->from('films f ')
                    ->where('f.permalink=:permalink AND f.studio_id=:studio_id', array(':permalink' => $permaLink, ':studio_id' => $studio_id));
            $movie = $command->queryAll();
            $mapped_id = $movie[0]['mapped_id'];
            if($movie[0]['mapped_id']){
                $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.content_types_id,f.mapped_id')
                    ->from('films f ')
                    ->where('f.id=:id', array(':id' => $movie[0]['mapped_id']));
                $movie = $command->queryAll();
            }
            if ($movie) {
                $movie_id = $movie[0]['id'];
                if(@$mapped_id && (isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id'])){
                $s_studio_id = $_SESSION[$studio_id]['parent_studio_id'];
               }else{
                $s_studio_id = $studio_id;
               }
               if(!Yii::app()->common->isGeoBlockContent(@$movie_id,'',$s_studio_id)){
                    Yii::app()->user->setFlash('error', $this->ServerMessage['content_not_available_in_your_country']);
                    $this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
                }
            } else {
                Yii::app()->user->setFlash('error', $this->ServerMessage['trying_play_invalid_content']);
                $this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
            }
        }
        if ($movie[0]['content_types_id'] == 4) {
            $liveStream = Livestream::model()->find('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie[0]['id'], ':studio_id' => $studio_id));
            if (!$liveStream || !$liveStream->feed_url) {
                Yii::app()->user->setFlash('error', $this->ServerMessage['no_live_feed_available']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);exit;
            }
        } else {
            $stream_code = isset($_REQUEST['stream']) ? $_REQUEST['stream'] : '0';
            $season = isset($_REQUEST['season']) ? $_REQUEST['season'] : 0;

            $stream_id = 0;
            $purchase_type = '';
            if (trim($permaLink) != '0' && trim($stream_code) != '0') {
                //Get stream Id
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code);
                $purchase_type = 'episode';
            } else if (trim($permaLink) != '0' && trim($stream_code) == '0' && intval($season)) {
                $purchase_type = 'episode';
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id);
            }

            if (!trim($stream_id) || $stream_id <= 1) {
                Yii::app()->user->setFlash('error', $this->ServerMessage['video_content_not_available']);
                $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/' . $movie->permalink);exit;
            }
        }
        $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
        $free_content_login = 1;
        if(isset($std_config) && !empty($std_config)){
            $free_content_login = $std_config->config_value;
        }

    if (STUDIO_USER_ID == 55) {
            $mov_can_see = 'allowed';
        } else {
            $default_currency_id = $this->studio->default_currency_id;
            
            $can_see_data['movie_id'] = $movie_id;
            $can_see_data['stream_id'] = $stream_id;
            $can_see_data['season'] = $season;
            $can_see_data['purchase_type'] = $purchase_type;
            $can_see_data['studio_id'] = $studio_id;
            $can_see_data['user_id'] = $user_id;
            $can_see_data['default_currency_id'] = $default_currency_id;
            $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            
			if(intval($free_content_login) && $user_id == 0) {
               Yii::app()->user->setFlash('error', 'Login to access the page.');
               $this->redirect(Yii::app()->getBaseUrl(TRUE).'/user/login');exit;
            }
        }
        //If a user has never subscribed and trying to play video
        if ($mov_can_see == "unsubscribed") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['activate_subscription_watch_video']);
            $this->redirect(Yii::app()->getBaseUrl(true) . "/user/activate");
            exit();
        }
        //If a user had subscribed and cancel
        else if ($mov_can_see == "cancelled") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['reactivate_subscription_watch_video']);
            $this->redirect(Yii::app()->getBaseUrl(true) . "/user/reactivate");
            exit();
        }
        //If video is not allowed country
        else if ($mov_can_see == "limitedcountry") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['video_restiction_in_your_country']);
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit();
        } else if ($mov_can_see == "advancedpurchased") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['already_purchase_this_content']);
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit();
        } else if ($mov_can_see == 'unpaid') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['ppv_video_pay_and_watch']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'access_period') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['access_period_expired']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'watch_period') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['watch_period_expired']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'maximum') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['crossed_max_limit_of_watching']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'allowed') {
            $getDeviceRestriction = StudioConfig::model()->getdeviceRestiction($studio_id, Yii::app()->user->id,$stream_id);
            if ($getDeviceRestriction != "" && $getDeviceRestriction == 0) {
                Yii::app()->user->setFlash('error', @$this->Language['restrict-streaming-device']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
            } 
            $this->layout = false;
            if ($movie[0]['content_types_id'] == 4) {
                if ($liveStream->feed_type == 2) {                    
                    if (strpos($liveStream->feed_url, 'rtmp://'.nginxserverip.'/live') !== false) {   
                        $feedWithIp = str_replace( "rtmp://".nginxserverip, NGINX_IP_HTTP, $liveStream->feed_url) . "/index.m3u8";                     
                        $liveStream->feed_url = str_replace("rtmp://".nginxserverip."/live", nginxserverlivecloudfronturl, $liveStream->feed_url) . "/index.m3u8";
                        $this->renderPartial('//livestream/livefeed_streaming_hls', array('livestream' => $liveStream, 'movie' => @$movie[0], 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken,'feedWithIp' => $feedWithIp));                        
                    } else if (strpos($liveStream->feed_url, 'rtmp://'.nginxserverip.'/record') !== false) {  
                        $feedWithIp = str_replace( "rtmp://".nginxserverip, NGINX_IP_HTTP, $liveStream->feed_url) . "/index.m3u8";                      
                        $liveStream->feed_url = str_replace("rtmp://".nginxserverip."/record", nginxserverrecordcloudfronturl, $liveStream->feed_url) . "/index.m3u8";
                        $this->renderPartial('//livestream/livefeed_streaming_hls', array('livestream' => $liveStream, 'movie' => @$movie[0], 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken,'feedWithIp' => $feedWithIp));                        
                    } else {
                        $this->renderPartial('//livestream/live_rtmpstreaming', array('livestream' => $liveStream, 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken));
                    }
                } else {
					//Check hls feed url is http or https.For http use flash otherwise contrib-hls					
					if (strtolower(parse_url($liveStream->feed_url, PHP_URL_SCHEME)) == 'https' || ($studio_id == 4185)) {
                        $this->renderPartial('//livestream/livefeed_streaming_hls', array('livestream' => $liveStream, 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken));
					} else {
                        $this->renderPartial('//livestream/livefeed_streaming', array('livestream' => $liveStream, 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken)); 
					}
				}
            } else {
                $movie_stream = new movieStreams();
                $movie_strm = $movie_stream->findByPk($stream_id);
                if($movie_strm->thirdparty_url) {
                    $info = pathinfo($movie_strm->thirdparty_url);
                    if ($info["extension"] == "m3u8"){ 
                        $this->render('//video/play_m3u8', array('thirdparty_url' => $movie_strm->thirdparty_url, 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken));
                    } else{
 		    /*
                     * Third Party Iframe Link changes youtube / vume
                     * @avi 7-11-16 <aravind@muvi.com>
                     */
                       
                        $this->render('//video/play_thirdparty', array('thirdparty_url' => $movie_strm->thirdparty_url, 'stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction, 'movie_id' => $movie_id,'authToken'=>$authToken));
                    }
                        //echo $movie_strm->thirdparty_url;
                        $ip_address = CHttpRequest::getUserHostAddress();
                        if (Yii::app()->aws->isIpAllowed()) {
                            $isLogEnable = 1;
                            if (isset(Yii::app()->user->add_video_log) && !Yii::app()->user->add_video_log) {
                                $isLogEnable = 0;
                            }
                            $movie_id = $movie_strm->movie_id;
                            $stream_id = $movie_strm->id;
                            if($isLogEnable == 1){
                                $video_log = new VideoLogs();                
                                $video_log->created_date = new CDbExpression("NOW()");
                                $video_log->ip = $ip_address;
                                $video_log->video_length = 0;
                                $video_log->movie_id = $movie_id;
                                $video_log->video_id = $stream_id;
                                $video_log->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
                                $video_log->studio_id = Yii::app()->common->getStudioId();
                                $video_log->watch_status = 'start';
                                $video_log->save();
                            }
                        }
                    exit;
                } else {
                    if($mapped_id && (isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id'])){
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $_SESSION[$studio_id]['parent_studio_id']);
                    }else{
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                    }
                    if ($movie_strm && (($movie_strm->is_demo == '1') || ($movie_strm->full_movie !=''))) {
                        if($movie_strm->is_demo == 1){
                            $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/small.mp4';
                        } else{
							if($mapped_id && (isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id'])){
								$fullmovie_path = $this->getFullVideoPath($stream_id, @$starr,$_SESSION[$studio_id]['parent_studio_id']);
							}else{
								$fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $movie_strm->id . '/' . $movie_strm->full_movie;
							}
                            
                        }
                        if($stream_id == 82030){
                            $bucketInfo['cloudfront_url'] = 'd2qe8pp1v9ohei.cloudfront.net';
                            $bucketInfo['signedFolderPath'] = '5745/EncodedVideo/';
                            $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $movie_strm->id . '/' . $movie_strm->full_movie;
                        }
                    }
                    
                                              //  $fullmovie_path = 'https://d2gx0xinochgze.cloudfront.net/488/EncodedVideo/uploads/movie_stream/full_movie/7135/Baaghi_Official_Trailer__Tiger_Shroff___Shraddha_Kapoor__Releasing_April_29__HD__720p_.mp4';

                if (!$fullmovie_path) {
                    Yii::app()->user->setFlash('error', $this->ServerMessage['video_content_not_available']);
                    $this->redirect(Yii::app()->getBaseUrl(TRUE));
                    exit;
                }
				$v_logo = Yii::app()->general->getPlayerLogo($studio_id);
				//Check if Paid DRM enabled or not 
				//$configData = StudioConfig::model()->getconfigvalueForStudio($studio_id,'drm_enable');
                $drmMobileDisable = @$movie_strm->is_mobile_drm_disable;
				if(@$movie_strm->content_key && @$movie_strm->encryption_key && ($drmMobileDisable == 0 || (Yii::app()->common->isMobile() == 0 && $drmMobileDisable == 1))) {
					$folderPath = Yii::app()->common->getFolderPath("",$studio_id);
					$signedBucketPath = $folderPath['signedFolderPath'];
					$fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$movie_strm->id.'/stream.mpd';
					$fullmovie_path_hls = 'https://' . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_strm->id . '/hls/master.m3u8';
					if (HOST_IP != '127.0.0.1' || HOST_IP != '52.0.64.95') {
						$getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id,'drm_cloudfront_url');
						if($getStudioConfig){
							if(@$getStudioConfig['config_value'] != ''){
								$fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$movie_strm->id.'/stream.mpd';
								$fullmovie_path_hls = 'https://' . $getStudioConfig['config_value'] . '/uploads/movie_stream/full_movie/' . $movie_strm->id . '/hls/master.m3u8';
							}
						}
					}
					$tokens = Yii::app()->aws->generateExplayToken($movie_strm->content_key, $movie_strm->encryption_key);
				}else{
					$multipleVideo = array();
					$defaultResolution = 144;
					$showSetting =0;
					if($movie_strm->video_resolution){
						$showSetting =1;
					}
                    $rules = Yii::app()->policy->rules($stream_id);
                    if(@$rules['resolution'] != ''){
                        $setResolutionPicker = $rules['resolution'];
                    }
					$multipleVideo = $this->getvideoResolution($movie_strm->video_resolution, $fullmovie_path,@$setResolutionPicker);
					$videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path);
					$videoToBeplayed12 = explode(",", $videoToBeplayed);
					$fullmovie_path = $videoToBeplayed12[0];
					$defaultResolution = $videoToBeplayed12[1];
					$wiki_data = $movie_strm->wiki_data;
					$movie_id = $movie_strm->movie_id;
					
					$internetSpeedImage = CDN_HTTP . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/check-download-speed.jpg';
					if(Yii::app()->common->isMobile()){
						$videoWithExpTime = array();
						$clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
						foreach ($multipleVideo as $multipleVideokey => $multipleVideovalue) {
							$rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $multipleVideovalue));
							$videoWithExpTime[$multipleVideokey] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl); 
						}
						$multipleVideo = array();
						$multipleVideo = $videoWithExpTime;
					}else if(HOST_IP != '127.0.0.1'){
						$clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
						$rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
						$fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl);
					}
				}

				
                $subtitleFiles = array();
                if(($movie_strm->video_management_id != '') && ($movie_strm->video_management_id != 0)){
                    $subTitlesql = "select vs.id as subId, vs.filename,ls.* from video_subtitle vs,language_subtitle ls where vs.language_subtitle_id = ls.id and vs.video_gallery_id=" . $movie_strm->video_management_id ." and vs.studio_id = ".$studio_id." ORDER BY ( ls.name != 'English' ), ls.name ASC";
                    $subTitleData = Yii::app()->db->createCommand($subTitlesql)->queryAll();
                    if($subTitleData){
                        $i =1 ;
                        foreach ($subTitleData as $subTitleDatakey => $subTitleDatavalue) {
                            $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];
                            $filePth = $clfntUrl."/".$bucketInfo['signedFolderPath']."subtitle/".$subTitleDatavalue['subId']."/".$subTitleDatavalue['filename'];
                            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                            $subtitleFiles[$i]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,1,"",60000);
                            $subtitleFiles[$i]['code'] =  $subTitleDatavalue['code'];
                            if (@$this->Language[$subTitleDatavalue['name']]!=''){
                                $subtitleFiles[$i]['language'] = $this->Language[$subTitleDatavalue['name']];  
                            } else {
                                $subtitleFiles[$i]['language'] =  $subTitleDatavalue['name'];
                            }
                            $i ++;
                        }
                    }
                }
                $item_poster = $this->getPoster($movie_id, 'films', 'standard');
                $content_type = @$data[0]['content_type'];
                $per_link = @$data[0]['permalink'];

                //Adding the codes for the adservers RKS ft @Avi
                $studio_ads = StudioAds::model()->getStudioAdsDetails($studio_id);
                $MonetizationMenuSettings = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($studio_id);
                //spotx activated
                $bit_pre_roll = false;$bit_post_roll = false;  
                if((strlen($movie_strm->roll_after) > 0) && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && (@$MonetizationMenuSettings[0]['menu'] & 4 )){
                        $roll_after = explode(',',$movie_strm->roll_after);
                        //check post-roll
                        if (in_array("end", $roll_after)) {
                            $bit_post_roll = "end";
                            array_pop($roll_after);
                        }
                        //check pre-roll
                        if (in_array("0", $roll_after)) {
                            $bit_pre_roll = "start";
                            $roll_after = array_reverse($roll_after);
                            array_pop($roll_after);
                            $roll_after = array_reverse($roll_after);
                        }
                        unset($movie_strm->roll_after);
                        //convert ads streaming time to seconds
                        $time_seconds ="";$i =0;
                        $midRollvalues = array();
                        while($i<count($roll_after)){
                            $midRollvalues[] = Yii::app()->general->getHHMMSSToseconds($roll_after[$i++]);
                            }
                        $movie_strm->roll_after = implode(",",$midRollvalues);
                            }
                //echo $movie_strm->roll_after;exit;
                $page_title = $movie[0]['name'];
                $contenttype = $movie[0]['content_types_id'];
                if ($contenttype == 3) {
                    $page_title.= ' Season ' . $movie_strm->series_number;
                    if ($movie_strm->episode_title != '')
                        $page_title.= ' ' . $movie_strm->episode_title;
                    else
                        $page_title.= ' Episode ' . $movie_strm->episode_number;
                }
                $page_desc = 'You are watching ' . $page_title . ' at ' . $studio->name . '.';
                
                $ad_timing = array();
                $roll_after = str_replace('00:00:00,', '', $movie_strm->roll_after);
                $roll_after = str_replace('00:00:00', '', $roll_after);
                $roll_afters = explode(',', $roll_after);   
                foreach ($roll_afters as $roll) {
                    $roll_parts = explode(':', $roll);
                    $roll_after = 0;
                    if ($roll_parts[0] > 0)
                        $roll_after += 3600 * $roll_parts[0];
                    if ($roll_parts[1] > 0)
                        $roll_after += 60 * $roll_parts[1];
                    if ($roll_parts[2] > 0)
                        $roll_after += $roll_parts[2];   
                    $ad_timing[] = $roll_after;
                } 

               $waterMarkOnPlayer = Yii::app()->general->playerWatermark();
                
                if(count($ad_timing) > 0)
                    $ad_type = 'vast';  
                if($user_id > 0 ){
                $durationPlayed = VideoLogs::model()->getvideoDurationPlayed($studio_id, $user_id, $movie_id, $stream_id);
                } else{
                     $durationPlayed = VideoLogs::model()->getvideoDurationPlayed($studio_id, 0, $movie_id, $stream_id);
                }
                if($this->studio->need_login == 1 && $std_config->config_value != 0){
                    $durationPlayed = VideoLogs::model()->getvideoDurationPlayed($studio_id, $user_id, $movie_id, $stream_id);
                }                             
                if(@$movie_strm->content_key && @$movie_strm->encryption_key && ($drmMobileDisable == 0 || (Yii::app()->common->isMobile() == 0 && $drmMobileDisable == 1))) {
                    $this->render('//video/play_mped', array('showSetting' => $showSetting, 'subtitleFiles' => $subtitleFiles, 'internetSpeedImage' => @$internetSpeedImage, 'multipleVideo' => $multipleVideo, 'can_see' => 'allowed', 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie_id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc, 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'durationPlayed' => $durationPlayed, 'Authtoken' => $tokens,'fullmovie_path_hls' => $fullmovie_path_hls, 'getDeviceRestriction' => $getDeviceRestriction,'authToken'=>$authToken));
                }
                //DFP Ads
                else if($movie_strm->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0]['menu'] & 4 )){
                   $this->render('//video/playadsdfp', array('showSetting' => $showSetting,'subtitleFiles' => $subtitleFiles,'internetSpeedImage' => @$internetSpeedImage, 'defaultResolution' => $defaultResolution, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => 'allowed', 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie_id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc,'waterMarkOnPlayer' => $waterMarkOnPlayer,'durationPlayed' => $durationPlayed,'MonetizationMenuSettings'=>$MonetizationMenuSettings, 'getDeviceRestriction' => $getDeviceRestriction,'authToken'=>$authToken));
                }
                //VMAP
                else if($movie_strm->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && (@$MonetizationMenuSettings[0]['menu'] & 4 )){
                   $this->render('//video/playads_vamp', array('showSetting' => $showSetting,'subtitleFiles' => $subtitleFiles,'internetSpeedImage' => @$internetSpeedImage, 'defaultResolution' => $defaultResolution, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => 'allowed', 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie_id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc,'waterMarkOnPlayer' => $waterMarkOnPlayer,'durationPlayed' => $durationPlayed,'MonetizationMenuSettings'=>$MonetizationMenuSettings,'response'=>$response,'movie_strm'=>$movie_strm, 'getDeviceRestriction' => $getDeviceRestriction,'authToken'=>$authToken,'bit_pre_roll'=>$bit_pre_roll,'bit_post_roll'=>$bit_post_roll));                        
                }
                //for SPOTX Integration   playAds_video 
                else if($movie_strm->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && $studio->id == 4302){   
                        $this->render('//video/playads_video4', array('showSetting' => $showSetting, 'subtitleFiles' => $subtitleFiles, 'internetSpeedImage' => @$internetSpeedImage, 'defaultResolution' => $defaultResolution, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => 'allowed', 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie_id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc, 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'durationPlayed' => $durationPlayed, 'getDeviceRestriction' => $getDeviceRestriction,'authToken'=>$authToken));
                }else{
                        $this->render('//video/play_video', array('showSetting' => $showSetting,'subtitleFiles' => $subtitleFiles,'internetSpeedImage' => @$internetSpeedImage, 'defaultResolution' => $defaultResolution, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => 'allowed', 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie_id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc,'waterMarkOnPlayer' => $waterMarkOnPlayer,'durationPlayed' => $durationPlayed, 'getDeviceRestriction' => $getDeviceRestriction,'authToken'=>$authToken));
                }
            }
         }
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }  
            
	function getnew_secure_url($resourceKey, $streamHostUrl = "", $isAjax = "",$browserName="") {
        $this->layout = false;
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJYIDWFG3D6CNOYVA.pem',
                    'key_pair_id' => 'APKAJYIDWFG3D6CNOYVA',
        ));
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP.'d3ff6jhdmx1yhu.cloudfront.net';
        }
        if (Yii::app()->common->isMobile() || $browserName == "Safari") {
            $expires = time() + 50;
        }else if ($isAjax == 1) {
            $expires = time() + 20;
        } else {
            $expires = time() + 5;
        }

        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }
	
	function getsecure_url($resourceKey, $streamHostUrl = "") {
        $this->layout = false;
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJ5RUFF7WVAM5VHPQ.pem',
                    'key_pair_id' => 'APKAJ5RUFF7WVAM5VHPQ',
        ));
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP.'d62co02q89w6j.cloudfront.net';
        }
        $expires = time() + 250;
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }
}
