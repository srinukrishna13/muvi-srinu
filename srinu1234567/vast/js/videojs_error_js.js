(function(){
  var defaults, extend;
  defaults = {
    messages: {
      // MEDIA_ERR_ABORTED
      1: you_aborted_the_media_playback,
      // MEDIA_ERR_NETWORK
      2: a_network_error_caused_the,
      // MEDIA_ERR_DECODE
      3: the_media_playback_was_aborted,
      // MEDIA_ERR_SRC_NOT_SUPPORTED
      4: the_media_could_not_be_loaded,
      // MEDIA_ERR_ENCRYPTED (Chrome)
      5: the_media_is_encrypted,
      6:no_compatible_source_was_found,
      unknown: only_watchable_on_pc_and_laptops
    }
  };
  extend = function(obj){
    var arg, prop, source;
    for (arg in arguments) {
      source = arguments[arg];
      for (prop in source) {
        if (source[prop] && typeof source[prop] === 'object') {
          obj[prop] = extend(obj[prop] || {}, source[prop]);
        } else {
          obj[prop] = source[prop];
        }
      }
    };
    return obj;
  };
  
  videojs.plugin('errors', function(options){
    var addEventListener, messages, settings;

    settings = extend(defaults, options);
    messages = settings.messages;
    addEventListener = this.el().addEventListener || this.el().attachEvent;
    this.on('error', function(event){
      var code, dialog, player;
      player = this;
      code = event.target.error ? event.target.error.code : event.code;
      // create the dialog
      dialog = document.createElement('div');
      dialog.className = 'vjs-error-dialog';
      dialog.textContent = messages[code] || messages['unknown'];
      addEventListener.call(dialog, 'click', function(event){
        //player.el().removeChild(dialog);
      }, false);
      // add it to the DOM
      player.el().appendChild(dialog);
    });
  });
})();