<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Muvi.com</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>    
        <script>
            var user_id="<?php echo Yii::app()->user->id; ?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>	
         <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />        

        <link rel="stylesheet" href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video-js.min.css">
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/hls.js/dist/hls.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/src/videojs5.hlsjs.js"></script>
        <script type='text/javascript'>
        // video js error message
            var you_aborted_the_media_playback ="<?php echo $this->Language["you_aborted_the_media_playback"]; ?>";
            var a_network_error_caused_the ="<?php echo $this->Language["a_network_error_caused_the"]; ?>";
            var the_media_playback_was_aborted ="<?php echo $this->Language["the_media_playback_was_aborted"]; ?>";
            var the_media_could_not_be_loaded ="<?php echo $this->Language["the_media_could_not_be_loaded"]; ?>";
            var the_media_is_encrypted ="<?php echo $this->Language["the_media_is_encrypted"]; ?>";
            var no_compatible_source_was_found ="<?php echo $this->Language["no_compatible_source_was_found"];?>";

        </script>
<style type="text/css">
            /*.video-js{height:50%;padding-top:48%}*/.vjs-fullscreen{padding-top:0}.RDVideoHelper{display:none}video::-webkit-media-controls{display:none!important}video::-webkit-media-controls-panel{display:none!important}#backButton,.customized-res{display:none;position:absolute}body{background:#000}#backButton{left:40px;top:40px;z-index:1}.ytp-svg-shadow{stroke:#000;stroke-opacity:.15;stroke-width:2px;fill:none}.ytp-svg-fill{fill:#ccc}.customized-res{z-index:2000;right:10%;padding:0 5px;cursor:pointer;visibility:visible;opacity:.1%;line-height:23px;-webkit-transition:visibility .1s,opacity .1s;-moz-transition:visibility .1s,opacity .1s;-o-transition:visibility .1s,opacity .1s;transition:visibility .1s,opacity .1s;background-color:#07141e;background-color:rgba(7,20,30,.7);border-radius:3px}#video-res-sec ul,#video-res-sec ul li{list-style:none;margin:0;padding:0}.textlftAlg{text-align:left}.textRgtAlg{text-align:right;float:right}
            #play{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }
            @-webkit-keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform: 5%; }
                50% { transform: scale(1.4); }
            }
            @keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform:  5%; }
                50% { transform: scale(1.4); }
            }

            #pause{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }
            @-webkit-keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform: 5%; }
                50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform:  5%; }
                50% { transform: scale(1.4); }
            }
            #pause_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none;
            }
            #play_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none; 
            }
            @media screen and (max-width: 480px) {
                #backButton{
                position:absolute;
                left: 15px !important;
                top: 15px !important;
                width: 30px !important;
                height: 30px !important;
                display: none;
                z-index: 1; 
               }
            }
</style>
 </head>
    <body>
        <input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
   
   <div class="videocontent" style="overflow:hidden;">
          <video id="video_player" class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9" controls preload="auto" webkit-playsinline="true" playsinline="true">
          <source src="<?php echo $thirdparty_url; ?>" type="application/x-mpegURL" data-title="Live stream">
    </video>  
    </div>
        <script type="text/javascript">
            var videoOnPause = 0;
            var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
            var player = videojs('video_player');
            $(document).ready(function () {
                player.play();
                $('#video_player').append('<img src="/images/touchpause.png" id="pause_touch" />');
                $('#video_player').append('<img src="/images/touchplay.png" id="play_touch" />');
                $('#video_player').append('<img src="/images/pause.png" id="pause"/>');
                $('#video_player').append('<img src="/images/play-button.png" id="play"/>');
                $('#video_player').append('<img src="/images/back-button.png" width="40" height="40" id="backButton" style="cursor:pointer" data-toggle="tooltip" title="Back to Browse" allowFullScreen = true/>'); 
               
                player.ready(function () {
                    player = this;
                    // For Mobile devices.
                    if (is_mobile !== 0) {
                        $(".vjs-big-play-button").show();
                        player.pause();
                    } else {
                        $(".vjs-big-play-button").hide();
                    }
                    player.on("loadstart", function () {
                        if (is_mobile !== 0) {
                            $(".vjs-big-play-button").on("touchstart", function () {
                                player.play();
                                $(".vjs-big-play-button").hide();
                                $('.vjs-loading-spinner').show();
                            });
                        }
                    });
                    player.on("play", function () {
                        if (is_mobile !== 0) {
                            $(".vjs-big-play-button").hide();
                            $('.vjs-loading-spinner').hide();
                        }
                    });
                //back-button on player.
                    $('.videocontent').mouseover(function() {
                        $('#backButton').show();
                    });
                    $('.videocontent').mouseout(function() {
                        $('#backButton').hide();
                    });
                    $('#backButton').click(function(){
                        if($('#backbtnlink').val() !='')
                           window.location.href = $('#backbtnlink').val();
                        else
                          parent.history.back();
                          return false;
                    });
                    $('#backButton').bind('touchstart', function(){
                        if($('#backbtnlink').val() !='')
                            window.location.href = $('#backbtnlink').val();
                        else
                           parent.history.back();
                        return false;   
                    });
                    // BACK BUTTON OF VIDEO PLAYER DOESN'T HIDE IN FULLSCREEN MODE.  
                    setInterval(function() {
                        var user_active = player.userActive();
                        if(user_active === true){
                            $('#backButton').show();
                        } else {
                            $('#backButton').hide();
                        }
                    }, 4000);
                    setInterval(function() {
                        $('#video_player_html5_api').bind('touchstart', function (e) {
                            $('#backButton').show();
                        });
                    }, 4000);
                    //play/pause button on player.
                    if (is_mobile === 0) {
                        $('#video_player_html5_api').on('click', function (e) {
                            e.preventDefault();
                            if (player.paused()) {
                                $('#pause').hide();
                                $('#play').show();
                            } else {
                                $('#play').hide();
                                $('#pause').show();
                            }
                        });
                    } else {
                        $('#pause_touch').bind('touchstart', function (e) {
                            player.pause();
                            $('#play_touch').show();
                            $('#pause_touch').hide();
                        });
                        $('#play_touch').bind('touchstart', function (e) {
                            player.play();
                            setTimeout(function () {
                                $('#play_touch').hide();
                            }, 500);
                        });
                        $('#video_player_html5_api').bind('touchstart', function (e) {
                            if (player.play()) {
                                $('#pause_touch').show();
                                $('#play_touch').hide();
                                setTimeout(function () {
                                    $('#pause_touch').hide();
                                }, 4000);
                            }
                        });
                    }
                    // window responsive blog.
                    if ($(document).height() != null) {
                        var thisIframesHeight = $(window).height();//$(document).height();                           
                        $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px');
                    } else {
                        $(".video-js").attr('style', 'padding-top:47%; height:50%;');
                    }
                    $(window).resize(function () {
                        var thisIframesHeight = $(window).height();
                        $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px;width:100%;');
                    });
                    // video js error code functionality.
                    player.on('error', function () {
                        if($('.vjs-modal-dialog-content').html()!==''){
                          $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source_was_found+"</div>");
                        } else {
                        if(document.getElementsByTagName("video")[0].error != null){
                            var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                            if (videoErrorCode === 2) {
                                $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                            } else if (videoErrorCode === 3) {
                                $(".vjs-error-display").html("<div>"+the_media_playback_was_aborted+"</div>");
                            } else if (videoErrorCode === 1) {
                                 $(".vjs-error-display").html("<div>"+you_aborted_the_media_playback+"</div>");
                            } else if (videoErrorCode === 4) {
                                  $(".vjs-error-display").html("<div>"+the_media_could_not_be_loaded+"</div>");
                            } else if (videoErrorCode === 5) {
                                $(".vjs-error-display").html("<div>"+the_media_is_encrypted+"</div>");
                            }
                        }
                        }
                });
            });
            });
</script>
<?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
</body>
</html>
