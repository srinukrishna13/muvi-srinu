<div class="modal-dialog modal-md">
    <form action="javascript:void(0);" method="post" name="status_form" id="status_form" data-toggle="validator">
        <input type="hidden" name="order_id" value="<?php echo $order_id;?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">New Item</h4>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-sm-12" id="error"></div>
                </div>    
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="currentstatus">SKU Number: </label>
                                <div class="col-sm-9">
                                    <div class="fg-line">                                        
                                        <input type='text' placeholder="Enter SKU number." id="skuno" name="skuno" value="" class="form-control input-sm checkInput" onblur="showProductDetails();">                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Name: </label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <input type='text' placeholder="Item name" id="name" name="name" value="" class="form-control input-sm checkInput" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="quantity">Quantity: </label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <input type='number' min="1" placeholder="Enter quantity" id="quantity" name="quantity" value="" class="form-control input-sm checkInput" required >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="return saveItem(this);" id="btn-additem" disabled>Add Item</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>