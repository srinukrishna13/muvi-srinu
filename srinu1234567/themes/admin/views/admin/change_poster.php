<!-- Change Poster Popup-->
<div id="addposter_popup" class="modal fade">
    <div class="modal-dialog">
		<form action="<?php if(@$movie_stream_id){echo $this->createUrl('admin/addEpisodePoster');}else{echo $this->createUrl('admin/addPoster');};?>" method="post" enctype="multipart/form-data" id="addposter_form" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Poster</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
					<label for="movie_name" class="required">Current Poster: <img height="60px" width="100px" src="<?php echo @$banner_image;?>" rel="tooltip" title="<?php echo $movieName;?>" /></label>
				</div>
                <div class="form-group">
					<label for="uploadposter">Upload Poster</label>
					<input type="file" id="poster_id" name="Filedata" required accept="image/*">
					<p class="help-block">Upload image size of 400 x 600</p>
				</div>
				<div id="responseUploadify"></div>
            </div>
			<input type="hidden" value="<?php echo @$movie_id;?>" name="movie_id" id="movie_id"  />
			<input type="hidden" value="<?php echo @$movieName;?>" name="movie_name" id="movie_id"  />
			<input type="hidden" value="<?php echo @$movie_stream_id;?>" name="movie_stream_id" id="movie_stream_id"  />
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Upload</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
		</form>	
    </div>
</div>
<!-- Change Poster Popup end-->