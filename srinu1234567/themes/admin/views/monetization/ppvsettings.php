<div class="row">
        
    <div class="col-sm-12">
        <!--Restriction Section starts--> 

        <div id="success_msg"></div>
        <div class="row m-t-40">
            <div class="col-md-10">
                <form class="form-horizontal" role="form" id="ppvsetting" name="ppvsetting" method="post" action="javascript:void(0);">
                    <div class="form-group col-md-12">
                        <div class="row" >
                            <div class="col-md-7">
                                    <?php
                                    $islimit_video = $isaccess_restriction = $iswatch_restriction = 0;
                                    $limit_video = $watch_period = $access_period = '';
                                    $access_period_recurrence = $watch_period_recurrence = 'Hour';

                                    if (isset($ppvValidity) && !empty($ppvValidity)) {
                                        foreach ($ppvValidity as $key => $validity) {
                                            if ($validity->playbility_access_key == 'limit_video') {
                                                $islimit_video = 1;
                                                $limit_video = $validity->validity_period;
                                            } else if ($validity->playbility_access_key == 'access_period') {
                                                $isaccess_restriction = 1;
                                                $access_period = $validity->validity_period;
                                                $access_period_recurrence = $validity->validity_recurrence;
                                            } else if ($validity->playbility_access_key == 'watch_period') {
                                                $iswatch_restriction = 1;
                                                $watch_period = $validity->validity_period;
                                                $watch_period_recurrence = $validity->validity_recurrence;
                                            }
                                        }
                                    }
                                    ?>
                                    
                                <div class="row" >
                                    <div class="col-md-7">
                                        <div class="checkbox">
                                            <label for="view_restriction"> <input type="checkbox" id="view_restriction" name="view_restriction" <?php if(intval($islimit_video)) {?> value="1" checked="checked"<?php } else {?> value="0"<?php }?> />
                                                <i class="input-helper"></i> Restrict Content by Number of Views
                                            </label> 
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="fg-line">
                                            <input type="text" name="limit_video" id="limit_video" class="form-control input-sm" value="<?php if(intval($islimit_video)) echo $limit_video;?>" onkeypress="return isNumberKey(event)" maxlength="3" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row" >
                                    <div class="col-md-7">
                                        <div class="checkbox">
                                            <label for="access_restriction"> <input type="checkbox" id="access_restriction" name="access_restriction" <?php if(intval($isaccess_restriction)) {?> value="1" checked="checked"<?php } else { ?>value="0"<?php } ?> />
                                                <i class="input-helper"></i> Restrict Content by Access Period
                                            </label> 
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-6">
                                        <div class="fg-line">
                                            <input type="hidden" name="access_period[playbility_access_key]" id="playbility_access_key" value="access_period" />
                                            <input type="text" class="form-control input-sm" name="access_period[validity_period]" id="access_restriction_text" value="<?php echo $access_period; ?>" onkeypress="return isNumberKey(event)" maxlength="3" /> 
                                        </div>
                                        <div id="error"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="select">
                                            <select name="access_period[validity_recurrence]" id="access_period_recurrence" class="form-control input-sm">
                                                <?php if (stristr($_SERVER['HTTP_HOST'], "idogic.com")) { ?>
                                                <option value="Minute" <?php if($access_period_recurrence == 'Minute') { ?>selected="selected"<?php } ?>>Minutes</option>
                                                <?php } ?>
                                                <option value="Hour" <?php if($access_period_recurrence == 'Hour') { ?>selected="selected"<?php } ?>>Hours</option>
                                                <option value="Day" <?php if($access_period_recurrence == 'Day') { ?>selected="selected"<?php } ?>>Days</option>
                                                <option value="Month" <?php if($access_period_recurrence == 'Month') { ?>selected="selected"<?php } ?>>Months</option>
                                            </select>
                                        </div>
                                        <div id="error"></div>
                                    </div>
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="row" >
                                    <div class="col-md-7">
                                        <div class="checkbox">
                                            <label for="watch_restriction"> <input type="checkbox" id="watch_restriction" name="watch_restriction" <?php if(intval($iswatch_restriction)) {?> value="1" checked="checked"<?php } else { ?>value="0"<?php } ?> />
                                                <i class="input-helper"></i> Restrict Content by Watch Period</label> 
                                        </div>
                                    </div>
                                     <div class="col-md-5">
                                        <div class="row">
                                    <div class="col-md-6">
                                        <div class="fg-line">
                                            <input type="hidden" name="watch_period[playbility_access_key]" id="playbility_watch_key" value="watch_period" />
                                            <input type="text" class="form-control input-sm" name="watch_period[validity_period]" id="watch_restriction_text" value="<?php echo $watch_period; ?>" onkeypress="return isNumberKey(event)" maxlength="3" /> 
                                        </div>
                                        <div id="error"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="select">
                                            <select name="watch_period[validity_recurrence]" id="watch_period_recurrence" class="form-control input-sm">
                                                <?php if (stristr($_SERVER['HTTP_HOST'], "idogic.com")) { ?>
                                                <option value="Minute" <?php if($watch_period_recurrence == 'Minute') { ?>selected="selected"<?php } ?>>Minutes</option>
                                                <?php } ?>
                                                <option value="Hour" <?php if($watch_period_recurrence == 'Hour') { ?>selected="selected"<?php } ?>>Hours</option>
                                                <option value="Day" <?php if($watch_period_recurrence == 'Day') { ?>selected="selected"<?php } ?>>Days</option>
                                            </select>
                                        </div>
                                        <div id="error"></div>
                                    </div>
                                    </div></div>
                                </div>
                            </div>

                                <div class="col-md-4 col-md-offset-1">
                                <p class="grey">For Single Part & Multi Part Child content on PPV Purchases, views when used with Access and Watch periods is always considered within Access or Watch Period if defined</p>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="button" id="updatebtn" name="updatebtn" class="btn btn-primary btn-sm" onclick="return validateSettingForm();">Update</button>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
    </div>
</div>
        
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
    $('#limit_video, #access_restriction_text, #watch_restriction_text').on("contextmenu",function(event) {
        return false;
    });

    $('#view_restriction, #access_restriction, #watch_restriction').click(function(){
        if($(this).is(':checked')){
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });

    jQuery.validator.addMethod("isLimitVideoEmpty", function (value, element) {
        if ($("#view_restriction").is(':checked')) {
            if($.trim($("#limit_video").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, "Please enter Number of Views");
    
    jQuery.validator.addMethod("isAccessPeriodEmpty", function (value, element) {
        if ($("#access_restriction").is(':checked')) {
            if($.trim($("#access_restriction_text").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, "Please enter Access Period");
    
    jQuery.validator.addMethod("isWatchPeriodEmpty", function (value, element) {
        if ($("#watch_restriction").is(':checked')) {
            if($.trim($("#watch_restriction_text").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, "Please enter Watch Period");
    
    function validateSettingForm() {
        var validate = $("#ppvsetting").validate({
            rules: {
                'limit_video': {
                    isLimitVideoEmpty: true
                },
                'access_period[validity_period]': {
                    isAccessPeriodEmpty: true
                },
                'watch_period[validity_period]': {
                    isWatchPeriodEmpty: true
                }
            },
            messages: {
                'limit_video': {
                    isLimitVideoEmpty: 'Please enter Number of Views'
                },
                'access_period[validity_period]': {
                    isAccessPeriodEmpty: 'Please enter Access Period'
                },
                'watch_period[validity_period]': {
                    isWatchPeriodEmpty: 'Please enter Watch Period '
                }
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();
        if (x) {
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/ppvSettings";
            document.ppvsetting.action = url;
            document.ppvsetting.submit();
        }
    }
    
    function isNumberKey(e){
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
</script>