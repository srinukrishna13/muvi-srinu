<style type="text/css">
    .table > tbody > tr > th{border-top: 0px solid;}
    .cbd{clear:both;margin: 10px 0;height: 20px;border-top: 1px solid #F4F4F4;}
    .input-group.srch{padding: 5px;margin-top: -50px;position: initial;float: right}
    .input-group.dlt{padding: 5px;margin-top: -50px;position: relative;float: right;margin-right: 25%;}
    .cb10{clear:both;height: 10px;}
</style>
<div class="box-body table-responsive">
    <div class="col-lg-12 box-body">
        <div class="box box-primary report" style="min-height: 200px;">
            <div>
                <?php                
                $arrstatus = array(
                    1 => "Referral under review",
                    2 => "Referral approved, NOT customer yet",
                    3 => "Paying Customer, under 4 months",
                    4 => "Qualified Customer"
                );
                ?>
                <div class="box-body table-responsive no-padding">
                    <div class="pull-left">
                        <a href="<?php echo Yii::app()->baseUrl; ?>/partnerPayment/ReferrerCustomer"><button class="btn btn-primary m-t-10 freeuser" value="csv">Refer New Customer</button></a>
                    </div>
                    <div class="clear"></div>
                    <div class="cb10"></div>
                    <div class="cb10"></div>
                    <form method="post" action="javascript:void(0);" id="mngUserForm" name="mngUserForm">
                        <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                                <tr>                               
                                    <th class="width">Business Name</th>
                                    <th class="width">Referral Customer</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Title</th>
                                    <th>Notes</th>
                                    <th>Referred On</th>   
                                    
                                </tr>	
                                <?php
                                if ($data) {
                                    $first = $cnt = (($pages->getCurrentPage()) * RECORDS_PER_PAGE) + 1;
                                    foreach ($data as $key => $value) {
                                        $cnt++;
                                        $contact = json_decode($value->contacts);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $value->business_name; ?>
                                            </td>
                                            <td>
                                                <?php echo $contact[0]->name; ?>
                                            </td>
                                            <td>
                                                <?php echo $contact[0]->email; ?>
                                            </td>
                                            <td>
                                               <?php echo $contact[0]->phone; ?>
                                            </td>
                                            <td>
                                                <?php echo $contact[0]->title; ?> 
                                            </td>
                                            <td>
                                               <?php echo $value->notes; ?> 
                                            </td>
                                            <td>
                                               <?php echo date("M d,Y",strtotime($value->add_date)) ;?>  
                                            </td>                                            
                                        </tr>
                                        <?php
                                    }
                                    if ($items_count > $page_size) {
                                        ?>
                                        <tr>
                                            <td colspan="7" style="text-align: right;">
                                                <span style="font-size: 10;">Showing <?php echo $first . " to " . --$cnt; ?> of <?php echo $items_count; ?></span>
                                                <?php
                                                $this->widget('CLinkPager', array(
                                                    'currentPage' => $pages->getCurrentPage(),
                                                    'itemCount' => $items_count,
                                                    'pageSize' => $page_size,
                                                    'maxButtonCount' => 6,
                                                    'nextPageLabel' => 'Next &gt;',
                                                    'header' => '',
                                                ));
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="7">No Record found!!!</td>
                                    </tr>	
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Extend trial period Popup start -->
<div id="extendTrialPopup" class="modal fade" data-backdrop="static" data-keyboard="false"></div>
<!-- Extend trial period Popup end <?php echo $domain;?>-->
<!-- Change password Popup start -->
<div id="changePassword" class="modal fade"></div>
<!-- Modal End  -->
<script type="text/javascript">
    $(function () {
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('#from_date').datepicker({
            startDate: today,
            format: 'mm-dd-yyyy'
        });

        $('#to_date').datepicker({
            startDate: today,
            format: 'mm-dd-yyyy'
        });
    });
</script>
    