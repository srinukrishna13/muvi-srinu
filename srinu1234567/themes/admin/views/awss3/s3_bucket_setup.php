<div class="col-lg-9 box box-primary">
    <br />
    <form class="form-horizontal" method="post" enctype="multipart/form-data" role="form" id="<?php echo ($data["id"])?"s3BucketUpdate":"s3Bucket"; ?>" name="s3Bucket" action="<?php echo Yii::app()->baseUrl; ?>/awss3/saveS3BucketDetails">
        <input type="hidden" name="id" value="<?php echo ($data["id"])?$data["id"]:""; ?>" id="id"/>
        <div class="loading" id="subsc-loading"></div>  
        <div class="row form-group">
            <div class="col-lg-6">
                <label class="control-label col-lg-3">Bucket Name</label>                    
                <div class="col-lg-8">
                    <input type="text" id="bucket_name" name="bucket_name" class="form-control" value="<?php echo ($data["bucket_name"])?$data["bucket_name"]:""; ?>" required />
                </div>
            </div>              
        </div> 
        <div class="row form-group"> 
            <div class="col-lg-6">
                <label class="control-label col-lg-3">S3 Url</label>                    
                <div class="col-lg-8">
                    <input type="text" id="s3url" name="s3url" class="form-control" value="<?php echo ($data["s3url"])?$data["s3url"]:""; ?>" required />
                </div>
            </div>                    
        </div>               
        <div class="row form-group">
            <div class="col-lg-6">
                <label class="control-label col-lg-3">Access Key</label>                    
                <div class="col-lg-8">
                    <input type="text" id="access_key" name="access_key" class="form-control" value="<?php echo ($data["access_key"])?$data["access_key"]:""; ?>" />
                </div>
            </div>              
        </div>
        <div class="row form-group">
            <div class="col-lg-6">
                <label class="control-label col-lg-3">Secret Key</label>                    
                <div class="col-lg-8">
                    <input type="text" id="secret_key" name="secret_key" class="form-control" value="<?php echo ($data["secret_key"])?$data["secret_key"]:""; ?>" required />
                </div>
            </div>              
        </div>    
        <div class="row form-group">
            <div class="col-lg-6">
                <label class="control-label col-lg-3">S3cmd config file name in Conversion Server</label>                    
                <div class="col-lg-8">
                    <input type="text" id="secret_key" name="s3cmd_file_name" class="form-control" value="<?php echo ($data["s3cmd_file_name"])?$data["s3cmd_file_name"]:""; ?>" required />
                </div>
            </div>              
        </div>      
        <div class="row form-group">
            <div class="col-lg-6">
                <label class="control-label col-lg-3">Key Pair Id</label>                    
                <div class="col-lg-8">
                    <input type="text" id="secret_key" name="key_pair_id" class="form-control" value="<?php echo ($data["key_pair_id"])?$data["key_pair_id"]:""; ?>" required />
                </div>
            </div>              
        </div>           
        <div class="row form-group"> 
            <div class="col-lg-6">
                <label class="control-label col-lg-3">Pem File</label>                    
                <div class="col-lg-8">
                    <input type="file" name="pem_file_path" id="pem_file_path"  <?php echo ($data["id"])?"":"required"; ?>  onchange="checkfileSize();">
                    <?php
                        if($data["pem_file_path"] && $data["pem_file_path"] != ''){
                            echo '<a href="'.Yii::app()->baseUrl."/pem/".$theme_folder."/".$data["pem_file_path"].'">'.$data["pem_file_path"].'</a>';
                        }
                    ?>
                </div>
            </div>                    
        </div>             
        <div class="form-group">
            <div class="col-lg-6">
                <label class="control-label col-lg-3">&nbsp;</label>
                <div class="col-lg-8"><button type="submit" class="btn btn-default"><?php echo ($data["id"])?"Update":"Submit"; ?></button></div>
            </div>
        </div>    
    </form>   
</div>
 
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 

    $(document).ready(function(){
        $("#s3Bucket").validate({ 
            rules: {    
                bucket_name: {
                    required: true,
                },             
                s3url: {
                    required: true,
                },
                access_key: {
                    required: true
                },
                secret_key: {
                    required: true,
                },  
                pem_file_path:{
                    required: true,
                },
            },         
            submitHandler: function(form) {
                $("#s3Bucket").submit();
            }                
        });
        $("#s3BucketUpdate").validate({ 
            rules: {    
                bucket_name: {
                    required: true,
                },             
                s3url: {
                    required: true,
                },
                access_key: {
                    required: true
                },
                secret_key: {
                    required: true,
                },  
            },         
            submitHandler: function(form) {
                $("#s3BucketUpdate").submit();
            }                
        });
    });
    function checkfileSize() {
            var filename = $('#pem_file_path').val().split('\\').pop();
            var extension = filename.replace(/^.*\./, '');
            if (extension == filename) {
                extension = '';
            } else {
                extension = extension.toLowerCase();
            }
            switch (extension) {
                case 'pem':
                    break;
                default:
                    // Cancel the form submission
                    alert('Please upload a .pem file.');
                    return false;
                    break;
            }
        }
</script>
