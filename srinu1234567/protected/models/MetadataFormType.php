<?php
class MetadataFormType extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'metadata_form_type';
    }
}