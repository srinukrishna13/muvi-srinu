<?php

require 's3bucket/aws-autoloader.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';

use Aws\S3\S3Client;

class CallbackController extends Controller {

    public function actionApiResponse() {
        $this->HandleClickhereResponse(file_get_contents('php://input'));
        $data = file_get_contents('php://input');
        if ($data) {
            //$data = '{"delivered": true, "description": "OK", "thread": "dfa915958c92c0d4769b626a62a243e7", "reason_code": 200, "reason_description": "OK", "message_id": "8LDC4M1HmC3nuyV/kd9jJ1/yXk/GkktEcqSMMeT7", "id": "20efd0dc-aee6-48ad-96e7-824a0b54b4e1", "document_id": "8LDC4M1HmC3nuyV/kd9jJ1/yXk/GkktEcqSMMeT7"}';
            $response = json_decode($data, true);     
            $unique_id = $response['thread'];
            $getCallLog = ApiCallLog::model()->find('unique_id=:unique_id', array(':unique_id' => $unique_id));
            if ($response['delivered'] === true && $response['delivered'] == 200 && $response['reason_description'] == 'OK' && !empty($getCallLog)) {
                $action = $getCallLog['call_for'];
                switch ($action) {
                    case "doRegistration":
                        $tempuser = new TempSdkUser;
                        $getTempUser = $tempuser->find('unique_id=:unique_id', array(':unique_id' => $unique_id));
                        if (!empty($getTempUser)) {
                            $chkMobilenumber = Yii::app()->db->createCommand()
                                    ->select('id')
                                    ->from('sdk_users')
                                    ->where('studio_id=:studio_id AND mobile_number=:mobile_number', array(':studio_id' => $getTempUser['studio_id'], ':mobile_number' => $getTempUser['mobile_number']))
                                    ->queryRow();

                            $std = new Studio();
                            $studio = $std->findByPk($getTempUser['studio_id']);
                            $studio_name = strtoupper($studio->name);
                            if (!empty($chkMobilenumber)) {
                                $params = array(
                                    'name' => $getTempUser['name'],
                                    'link' => Yii::app()->getBaseUrl(true) . '/user/login',
                                    'studio_name' => $studio_name
                                );
                                $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
                                $to = $getTempUser['email'];
                                $from = $fromEmail;
                                $subject = "Registration Failed";
                                $fromName = $studio_name;
                                Yii::app()->theme = 'bootstrap';
                                $thtml = Yii::app()->controller->renderPartial('//email/signup_failed', array('params' => $params), true);
                                $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', $fromName);
                                echo "Register failed";
                                exit;
                            }
                            $ret = TempSdkUser::model()->saveToSdkUser($getTempUser);
                            if ($ret['user_id']) {
                                if ($getTempUser['studio_id'] && $getTempUser['unique_id']) {
                                    $sql = "delete from temp_sdk_users WHERE unique_id='" . $getTempUser['unique_id'] . "' AND studio_id=" . $getTempUser['studio_id'];
                                    $con = Yii::app()->db;
                                    $ciData = $con->createCommand($sql)->execute();
                                }
                                $lang_code = $this->language_code;
                                $welcome_email = Yii::app()->common->getStudioEmails($getTempUser['studio_id'], $ret['user_id'], 'welcome_email', '', '', '', '', '', '', $lang_code, '', $studio_name);
                                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email', $getTempUser['studio_id']);
                                if ($isEmailToStudio) {
                                    $admin_welcome_email = $this->sendStudioAdminEmails($getTempUser['studio_id'], 'admin_new_user_registration', $ret['user_id'], '', 0);
                                }
                            }
                        }
                        echo "registration success";
                        break;
                    case "doPayment":
                        $ret = SdkUser::model()->updateSubscribe($unique_id);
                        echo "payment";
                        break;
                    case "doDeactivate":
                        $ret = SdkUser::model()->apiCancelSubscribe($unique_id);
                        echo "Account deactivated";
                        break;
                    case "doCheckPermissionToPlay":
                        echo "permission for play";
                        break;
                }
            }else{
                $tempuser = new TempSdkUser;
                $getTempUser = $tempuser->find('unique_id=:unique_id', array(':unique_id' => $unique_id));
                if ($getTempUser['studio_id'] && $getTempUser['unique_id']) {
                    $sql = "delete from temp_sdk_users WHERE unique_id='" . $getTempUser['unique_id'] . "' AND studio_id=" . $getTempUser['studio_id'];
                    $con = Yii::app()->db;
                    $ciData = $con->createCommand($sql)->execute();
                }
            }
        }
        exit;
    }

    public function HandleClickhereResponse($res) {
        //$res = file_get_contents('php://input');
        print_r($_REQUEST);
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/clickhere.log', "a+");
        fwrite($fp, " \n\n\t Response " . date('m-d-Y H:i:s') . "\n\t " . serialize($res));
        fclose($fp);
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

