<?php

Yii::import('ext.EGeoIP');
Yii::import('ext.geoPlugin');

require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;

class PartnerController extends Controller {

    public $defaultAction = 'index';
    public $headerinfo = '';
    public $layout = 'partner';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        return true;
    }

    public function actionIndex() {
        if (isset(Yii::app()->user->id) && Yii::app()->user->id) {
            if (isset(Yii::app()->user->role_id) && Yii::app()->user->role_id==5){
                $this->checkpartner();
                /*if (in_array(36, Yii::app()->session['chk_permission'])) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/partner/Master');
                } else */if (in_array(35, Yii::app()->session['chk_permission'])) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/partner/Referrer');
                } else if (in_array(34, Yii::app()->session['chk_permission'])) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/partner/Reseller');
                }
            } else {
                $this->actionLogout();
            }
        }
        $this->pageTitle = 'Partners Portal';
        $this->pageKeywords = 'Partners Portal';
        $this->pageDescription = 'Partners Portal';
        $this->render('index');
    }

    public function actionlogin() {
        $this->redirect(Yii::app()->getbaseUrl(true));
    }

    function checkpartner() {
        //get the dbCONNECTION  
       
        $dbcon=Yii::app()->db;      
        $application = $dbcon->createCommand()
                ->select('*')
                ->from('portal_user')
                ->where('id=:id', array(':id' => Yii::app()->user->id))
                ->queryRow();
        
        //$dbcon->active=false; //close connection by dsn
        $chk_permission = explode(',', $application['permission_id']);
        Yii::app()->session['chk_permission'] = $chk_permission;
    }

    function actionLogout() {
        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();

        $this->redirect(Yii::app()->getbaseUrl(true));
    }

    public function actionCustomlogout() {
        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();
    }

    public function actionReseller() {
        if (in_array(34, Yii::app()->session['chk_permission'])) {
            $relation_type = 'reseller';
            $this->actionList($relation_type);
        } else {
            Yii::app()->user->setFlash('error', 'You are not authorize to access this page!');
            $this->redirect(Yii::app()->getbaseUrl(true));
        }
    }

    /*public function actionMaster() {
        if (in_array(36, Yii::app()->session['chk_permission'])) {
            $relation_type = 'master';
            $this->actionList($relation_type);
        } else {
            Yii::app()->user->setFlash('error', 'You are not authorize to access this page!');
            $this->redirect(Yii::app()->getbaseUrl(true));
        }
    }*/

    public function actionReferrer() {
        if (in_array(35, Yii::app()->session['chk_permission'])) {
            $relation_type = 'referrer';
            $this->actionReferlist($relation_type);
        } else {
            Yii::app()->user->setFlash('error', 'You are not authorize to access this page!');
            $this->redirect(Yii::app()->getbaseUrl(true));
        }
    }

    protected function actionReferlist() {
        $portal_userid = Yii::app()->user->id;
        $this->breadcrumbs = array('Manage referal' => array('partner/referrer'));
        $customer = ReferrerCustomer::model()->findAllByAttributes(array('referrer_id' => $portal_userid));
        if (!empty($customer)) {
            $page_size = 10;
            $offset = 0;
            if (isset($_REQUEST['page'])) {
                $offset = ($_REQUEST['page'] - 1) * $page_size;
            }

            
            $item_count = count($customer);

            $pages = new CPagination($item_count);
            $pages->setPageSize($page_size);
            $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
            $sample = range($pages->offset + 1, $end);
        }
        $this->render('refer_customer_list', array('data' => $customer, 'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample, 'package' => $pack, 'domain' => $dmain, 'relation_type' => $relation_type));
    }

    protected function actionList($relation_type) {
        //get the dbCONNECTION        
        $dbcon=$this->getDbConnection();  
        $this->breadcrumbs = array('reseller', 'reseller customer');
        $this->headerinfo = "Reseller Customer";
        $this->pageTitle = "Reseller | Reseller Customer";
        $studio_id = Yii::app()->user->id;//portal user record _id        
        /*For mulitiple partner*/
        /*For mulitiple partner*/
        $cond_search = '';
        if(isset($_REQUEST['search'])){
            $cond_search = "and u.email like '%".$_REQUEST['search']."%' or u.first_name like '%".$_REQUEST['search']."%'";
        }
        $parent = "SELECT parent_id FROM portal_user WHERE id={$studio_id}";
        $parentrecord = Yii::app()->db->createCommand($parent)->queryROW();
		$dbcon=$this->getDbConnection();  
        $refer_id = ($parentrecord['parent_id']!=0)?$parentrecord['parent_id']:$studio_id;
        /*End*/

        $user_relation = "SELECT * FROM user_relation WHERE refer_id=".$refer_id." and relation_type='".$relation_type."'";
         
        $customer = $dbcon->createCommand($user_relation)->queryAll();        
        /*End*/
        if (!empty($customer)) {
            $usrs = "";
            foreach ($customer as $k=>$val) {
                $usrs = $usrs . "," . $val['user_id'];
            }
            $usrs = ltrim($usrs, ",");

            $page_size = 20;
            $offset = 0;
            if (isset($_REQUEST['page'])) {
                $offset = ($_REQUEST['page'] - 1) * $page_size;
            }

            //$this->breadcrumbs = array('Manage User' => array('partner/list'));

            $cond = " AND u.id IN($usrs) AND (s.status!=4 OR s.is_deleted!=0)";

            $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT s.*, u.id AS user_id, u.* FROM studios s LEFT JOIN user u ON (s.id = u.studio_id) 
            WHERE u.is_admin=0 AND u.is_sdk=1 AND u.role_id=1 $cond $cond_search ORDER BY u.created_at DESC LIMIT $offset,$page_size";
      //$dbcon = Yii::app()->db;
            $data = $dbcon->createCommand($sql)->queryAll();

            $item_count = $dbcon->createCommand('SELECT FOUND_ROWS() AS count')->queryAll();
            $item_count = (isset($item_count[0]['count'])) ? $item_count[0]['count'] : 0;

            $pages = new CPagination($item_count);
            $pages->setPageSize($page_size);
            $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
            $sample = range($pages->offset + 1, $end);
            $packages = Package::model()->getPackages();
            $pack = $packages['package'][0]['base_price'];
            /* get plan */
            foreach ($data as $k => $v) {
                $record = $dbcon->createCommand()
                        ->select('*')
                        ->from('user_relation')
                        ->where('user_id=:id', array(':id' => $v['id']))
                        ->queryRow();
                $newapp = '';
                $p = explode(',', $record['pricing']);
                foreach ($packages['package'] as $key => $value) {
                    if ($value['id'] == $record['packages']) {
                        $np = $value['name'];
                        $pkid = $value['id'];
                    }
                }
                foreach ($packages['appilication'][$pkid] as $key => $value) {
                    if (in_array($value['id'], $p)) {
                        $newprice[] = $value['price'];
                        $newapp = $newapp . "," . $value['name'];
                    }
                }
                $data[$k]['plan'] = $record['plan'];
                $data[$k]['packages'] = $np;
                $data[$k]['pricing'] = array_sum($newprice);
                $data[$k]['app'] = ltrim($newapp, ',');
                $fullname = $v['first_name'];
                $fullname.= (strlen($v['last_name']) > 0)?" ".$v['last_name']:'';                
                $data[$k]['fullname'] = utf8_encode(trim($fullname));
            }
            /* end */
            /*if ($relation_type == 'master') {
                if(Yii::app()->user->email=='maxim.manas@gmail.com'){
                    $dmain = "idogic.com"; //for testing
                }else{
                    $dmain = "d2c-popup-shop.com"; //for sony
                }
            } else {*/
                $dmain = Yii::app()->common->getStudiodomain();
            /*}*/
        }
            $subscription_end_date_sql = "select id,end_date from reseller_subscriptions where reseller_id =".$refer_id." order by id desc limit 1";
            $subscription_end_date = $dbcon->createCommand($subscription_end_date_sql)->queryRow();  
            $end_date = ($subscription_end_date['end_date'] != '')?strtotime($subscription_end_date['end_date']):0;
            $today = strtotime(date('Y-m-d'));
            $subscription_popup_display = 0 ;   
            if($end_date <= $today){
                $subscription_popup_display = 1;
            }       
        $dbcon->active=false;
        $this->render('partner_customer', array('data' => $data, 'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample, 'package' => $pack, 'domain' => $dmain, 'relation_type' => $relation_type,'referer_id'=>$refer_id,'display_subscription_popup'=>$subscription_popup_display ));
    }

    public function actionResellerCustomer() {
        $this->breadcrumbs = array('reseller', 'Add new customer');
        $this->headerinfo = "Add new customer";
        $this->pageTitle = "Reseller | Add new Customer";
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $domain = Yii::app()->common->getStudiodomain();
        $studio = array("id" => $studio_id, "domain" => $domain);
        $packages = Package::model()->getPackages();
        $dbcon=$this->getDbConnection();
       // $number_of_card = ResellerCardInfos::model()->getnumberOfExistingCard($user_id);
        //$existing_card = ResellerCardInfos::model()->getExistingCard($user_id);
        $existing_card_sql = "select * from reseller_card_infos where portal_user_id = {$user_id}";
        $existing_card = $dbcon->createCommand($existing_card_sql)->queryAll();
        $number_of_card = count($existing_card);
        $today = Date('Y-m-d H:i:s');
       // $reseller_billing_date = ResellerSubscription::model()->check_subscription($user_id);
        $reseller_billing_date_sql = "select id,end_date from reseller_subscriptions where reseller_id = {$user_id} order by id desc limit 1";
        $reseller_billing_date_arr = $dbcon->createCommand($reseller_billing_date_sql)->queryRow();
        $reseller_billing_date = $reseller_billing_date_arr['end_date'];
        $date1 = date_create($today);
        $date2 = date_create($reseller_billing_date);
        $diff = date_diff($date2,$date1);  
        $date_diffrence =  $diff->format("%a")+2; 
        $current_month = date('m');
        $current_year = date('Y');
        $num_of_day_in_current_month = cal_days_in_month(CAL_GREGORIAN, $current_month,$current_year);;
        $this->render('resellercustomer', array('studio' => $studio, 'packages' => $packages,'number_of_card'=>$number_of_card,'number_of_day_in_month'=>$num_of_day_in_current_month,'data_diffrence'=>$date_diffrence,'existing_card'=>$existing_card));
    }

    /*public function getmasterdomain() {
        if(Yii::app()->user->email=='maxim.manas@gmail.com'){
            $studio = array("id" => 1, "domain" => 'idogic.com'); //for testing
        }else{
            $studio = array("id" => 1, "domain" => 'd2c-popup-shop.com'); //for sony
        }        
        return $studio;
    }*/

    /*public function actionMasterCustomer() {
        $s = $this->getmasterdomain();
        $studio = array("id" => $s['id'], "domain" => $s['domain']);
        $packages = Package::model()->getPackages();
        $this->render('mastercustomer', array('studio' => $studio, 'packages' => $packages));
    }*/

    public function actionReferrerCustomer() {
        $studio_id = Yii::app()->common->getStudiosId();
        $domain = Yii::app()->common->getStudiodomain();
        $studio = array("id" => $studio_id, "domain" => $domain);
        $this->render('referrercustomer', array('studio' => $studio));
    }

    public function actionInsertCustomer() {
        ob_clean();
        parse_str($_REQUEST['data'],$_REQUEST);
        if ($_REQUEST['is_customer'] == 1) {
            $msg['isSuccess'] = 'success';
            $msg['Message'] = ' Customer added as free trial . Their free trial expires after 14 days .<br> You can Purchase Subscription on customers CMS later OR you can try now';
            $msg['userid'] = $_REQUEST['newuserid'];
            $msg['studioid'] = $_REQUEST['newstudioid'];
            /*if ($_REQUEST['relation_type'] == 'master') {
                $msg['relation_type'] = 'Master';
            } else */if ($_REQUEST['relation_type'] == 'referrer') {
                $msg['relation_type'] = 'Referrer';
            } else if ($_REQUEST['relation_type'] == 'reseller') {
                $msg['relation_type'] = 'Reseller';
            }
            print json_encode($msg);
            exit;
        } else {
            $ip_address = CHttpRequest::getUserHostAddress();
            $geo_data = new EGeoIP();
            $geo_data->locate($ip_address);
            $country = $geo_data->getCountryName();
            $region = $geo_data->getRegion();
            $latitude = $geo_data->getLatitude();
            $longitude = $geo_data->getLongitude();
            $ref = array($latitude, $longitude);
            $s3bucket_id = 1;
            /*if ($_REQUEST['relation_type'] == 'master') {
                $s = $this->getmasterdomain();
                $domain = $s['domain'];
            } else {*/
                $domain = Yii::app()->common->getStudiodomain();
            /*}*/
            if (isset($_REQUEST['email']) && $_REQUEST['email'] != '') {
                $string = str_replace(' ', '', strtolower($_REQUEST['subdomain']));
                $inputDomain = str_replace('www.', '', preg_replace('/[^A-Za-z0-9.]/', '', $string));
                $usr = new User;
                $data = $usr->findByAttributes(array('email' => $_REQUEST['email']));
                if ($data) {
                    $msg['isSuccess'] = 'warning';
                    $msg['Message'] = 'Account already exists!';
                    print json_encode($msg);
                    exit;
                }
                //check Subdomain 
                if (isset($_REQUEST['subdomain']) && $_REQUEST['subdomain']) {
                    $data = Studios::model()->find('domain=:domain OR reserved_domain=:domain', array(':domain' => $inputDomain));
                    if ($data) {
                        $msg['isSuccess'] = 'warning';
                        $msg['Message'] = 'This domain name already exists. Please try a different one.';
                        print json_encode($msg);
                        exit;
                    }
                }
                if (!$data) {
                    //Find the bucket with respect to the client region and assign to that bucket.
                    $countrys3location = new Countrys3location();
                    $buckt_data = $countrys3location->getS3Location($country);
                    if (isset($buckt_data) && count($buckt_data)) {
                        $s3bucket_id = $buckt_data['s3buckets_id'];
                    } else {
                        if ($latitude && $longitude) {
                            $s3buckets = S3bucket::model()->findAll();
                            foreach ($s3buckets AS $key => $val) {
                                $buckts[$val->id][] = $val->bucket_name;
                                $buckts[$val->id][] = $val->region_code;
                                $buckts[$val->id][] = $val->latitude;
                                $buckts[$val->id][] = $val->longitude;
                            }
                            $distances = array_map(function($buckts) use($ref) {
                                $a = array_slice($buckts, -2);
                                return Yii::app()->common->getDistance($a, $ref);
                            }, $buckts);
                            asort($distances);
                            $bucketids = array_keys($distances);
                            $s3bucket_id = $bucketids[0];
                        }
                    }
                    //Get referrence link from where user come this site.
                    $source = '';
                    if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                        $source = Yii::app()->request->cookies['REFERRER'];
                        //Unset source cookie
                        //unset($_COOKIE['REFERRER']);
                        //setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
                    }
                    
                    /*for multiple partner*/
                    $parentsql = "SELECT parent_id FROM portal_user WHERE id=".Yii::app()->user->id;
                    $parentid = Yii::app()->db->createCommand($parentsql)->queryROW();
                    $refer_id = ($parentid['parent_id']!=0)?$parentid['parent_id']:Yii::app()->user->id;                   
                    /**/
                    
                    $subdomain = $this->createUniqSubdomain($inputDomain);
                    $main_domain = $subdomain . '.' . $domain;
                    $studio = new Studio();
                    $studio->uniqid = Yii::app()->common->generateUniqNumber();
                    $studio->name = $_REQUEST['customer_name'];
                    $studio->permalink = $subdomain;
                    $studio->phone = '';
                    $studio->created_dt = gmdate('Y-m-d H:i:s');
                    $studio->subdomain = $subdomain;
                    $studio->domain = $main_domain;
                    $studio->reserved_domain = $inputDomain;
                    $studio->theme = '';
                    $studio->parent_theme = '';
                    $studio->s3bucket_id = ($s3bucket_id > 0) ? $s3bucket_id : 1;
                    $studio->source = $source;
                    $studio->new_cdn_users = 1;
                    $studio->status = 1;
                    $studio->reseller_id = $refer_id;

                    $config = Yii::app()->common->getConfigValue(array('trial_period'), 'value');
                    $studio->start_date = Date('Y-m-d H:i:s', strtotime("+{$config[0]['value']} days"));
                    $end_date = $config[0]['value'] + 30;
                    $studio->end_date = Date('Y-m-d H:i:s', strtotime("+{$end_date} days"));

                    $studio->save();
                    $studio_id = $studio->id;
                    $studio->mrss_id = md5($studio_id);
                    $studio->save();
                    Yii::app()->session['studio_id'] = $studio_id;
                    Yii::app()->session['back_btn'] = $studio_id;
                    //Create Directory in s3 For studio
                    if ($s3bucket_id == '') {
                        $s3bucket_id = 1;
                    }
                    $bucketDetailsArr = Yii::app()->common->getBucketInfo($s3bucket_id);
                    if (isset($bucketDetailsArr['bucket_name']) && $bucketDetailsArr['bucket_name'] != '') {
                        $client = S3Client::factory(array(
                                    'key' => Yii::app()->params->s3_key,
                                    'secret' => Yii::app()->params->s3_secret,
                        ));
                        $key = $studio_id . '/EncodedVideo/blank.txt';
                        $key1 = $studio_id . '/public/blank.txt';
                        $key2 = $studio_id . '/RawVideo/blank.txt';
                        $source_file = Yii::app()->getbaseUrl(true) . '/blank.txt';
                        $acl = 'public-read';
                        $bucket = $bucketDetailsArr['bucket_name'];
                        $client->upload($bucket, $key, $source_file, $acl);
                        $client->upload($bucket, $key1, $source_file, $acl);
                        $client->upload($bucket, $key2, $source_file, $acl);
                    }
                    //Insert into PPV Buy
                    $ppvBuy = new PpvBuy;
                    $ppvBuy->studio_id = $studio_id;
                    $ppvBuy->save();

                    //Insert into Studio Manage Coupon
                    $studioManageCoupon = new StudioManageCoupon;
                    $studioManageCoupon->studio_id = $studio_id;
                    $studioManageCoupon->save();

                    //Insert data into the user table 
                    $relation = new UserRelation;
                    $enc = new bCrypt();
                    $password = $relation->getRandomPassword();
                    $encrypted_pass = $enc->hash($password);

                    $umodel = new User();
                    $umodel->studio_id = $studio->id;
                    $umodel->first_name = $_REQUEST['customer_name'];
                    $umodel->email = $_REQUEST['email'];
                    $umodel->encrypted_password = $encrypted_pass;
                    $umodel->created_at = gmdate('Y-m-d H:i:s');
                    $umodel->phone_no = "";
                    $umodel->signup_ip = $ip_address;
                    $umodel->signup_location = serialize($geo_data);
                    $umodel->is_active = 1;
                    $umodel->is_sdk = 1;
                    $umodel->role_id = 1;
                    $umodel->signup_step = 1;
                    $umodel->save();
                    $newuserid = $umodel->id;

                    //Inserting into the front-end user table
                    $user = new SdkUser;
                    $user->is_studio_admin = 1;
                    $user->email = $_REQUEST['email'];
                    $user->studio_id = $studio_id;
                    $user->signup_ip = $ip_address;
                    $user->display_name = htmlspecialchars($_REQUEST['customer_name']);
                    $user->encrypted_password = $encrypted_pass;
                    $user->status = 1;
                    $user->created_date = new CDbExpression("NOW()");
                    $user->save();

                    //Inserting studio id and email to ticket master table
                    if($user->save()){
                    $tickettingdb=$this->getAnotherDbconnection();
                    $command = $tickettingdb->createCommand();
                    $command->insert('ticket_master', array(
                        'studio_id'=>$studio_id,
                        'studio_email'=>$_REQUEST['email'],
                        'studio_name'=>$_REQUEST['customer_name'],
                        'user_id'=>$newuserid,
                        'user_name'=>$_REQUEST['customer_name'],
                        'user_type'=>1,
                        'user_sdk'=>1,
                        'studio_subscribed'=>0,
                        'creation_date'=>date("Y-m-d H:i:s")
                    ));
                    
                                 
                    $tickettingdb->active=false;
                    }
                    //Inserting into the relation table ( who refer to whom )
                    //$relation = new UserRelation;
                    /*if ($_REQUEST['relation_type'] == 'master') {
                        $mstudio = $s;
                        $referstudioid = $s['id'];
                    } else {*/
                        $mstudio = $this->studio;
                        $referstudioid = Yii::app()->common->getStudiosId();
                    /*}*/
                    
                    $relation->user_id = $newuserid;
                    $relation->studio_id = $studio_id;
                    $relation->refer_id = $refer_id; //from portal user table
                    $relation->refer_studio_id = $referstudioid;
                    $relation->ip = $ip_address;
                    $relation->relation_type = $_REQUEST['relation_type'];
                    $relation->add_date = new CDbExpression("NOW()");
                    $relation->plan = $_REQUEST['plan'] ? $_REQUEST['plan'] : 'Month';
                    $relation->packages = $_REQUEST['packages'] ? $_REQUEST['packages'] : 1;
                    $relation->pricing = $_REQUEST['pricing'] ? $_REQUEST['pricing'] : '2';
                    $relation->save();

                    $relation->addDefaultContentType($studio_id);
                    $relation->setupStudio($newuserid, $studio_id, 'classic');

                    Yii::app()->email->customerByReseller($_REQUEST, Yii::app()->user->first_name, $mstudio);
                    $_REQUEST['password'] = $password;
                    $_REQUEST['domain'] = $domain;
                    $_REQUEST['name'] = $_REQUEST['customer_name'];
                    /*if ($_REQUEST['relation_type'] == "master") {
                        Yii::app()->email->mailToResellerscustomer($_REQUEST, $mstudio);
                    } else {*/
                        Yii::app()->email->mailToResellerscustomer($_REQUEST);
                    /*}*/
                    /*  Add user to Hubspot and madmimi */
                    $nm = explode(" ", $_REQUEST['customer_name'], 2);
                    Hubspot::AddToHubspot($_REQUEST['email'], $nm[0], $nm[1], $_REQUEST['phone']);
                    $user = array('email' => $_REQUEST['email'], 'firstName' => $nm[0], 'lastName' => $nm[1], 'add_list' => 'Muvi All');
                    require('MadMimi.class.php');
                    $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
                    $mimi->AddUser($user);
                    /*                     * * END of hubspot and madmimi *** */
                    $msg['isSuccess'] = 'success';
                    $msg['Message'] = ' Customer added as free trial . Their free trial expires after 14 days .<br> You can Purchase Subscription on customers CMS later OR you can try now';
                    $msg['userid'] = $newuserid;
                    $msg['studioid'] = $studio_id;
                    /*if ($_REQUEST['relation_type'] == 'master') {
                        $msg['relation_type'] = 'Master';
                    } else */if ($_REQUEST['relation_type'] == 'referrer') {
                        $msg['relation_type'] = 'Referrer';
                    } else if ($_REQUEST['relation_type'] == 'reseller') {
                        $msg['relation_type'] = 'Reseller';
                    }
                    print json_encode($msg);
                    exit;
                }
            }
        }
    }

    function actionInsertRefercustomer() {
        /* 1 = Referral under review
         * 2 = Referral approved, NOT customer yet
         * 3 = Paying Customer, under 4 months
         * 4 = Qualified Customer
         */
        //Inserting into the referrer customer table
        if ($_POST['chkemail'] == 1) {
            $usr = new ReferrerCustomer;
            $data = $usr->findByAttributes(array('email' => $_REQUEST['email']));
            if ($data) {
                echo 'Account already exists! you can not refer it.';
                exit;
            } else {
                echo 0;
                exit;
            }
        }
        if ($_FILES['proof']['name'] != '' && $_REQUEST['email'] != '') {
            $usr = new ReferrerCustomer;
            $data = $usr->findByAttributes(array('email' => $_REQUEST['email']));
            if ($data) {
                Yii::app()->user->setFlash('error', 'Account already exists! you can not refer it.');
                $this->redirect($this->createUrl('ReferrerCustomer'));
                exit;
            }
            $user = new ReferrerCustomer;
            $user->company_name = $_REQUEST['company_name'];
            $user->name = $_REQUEST['primary_contact'];
            $user->email = $_REQUEST['email'];
            $user->phone = $_REQUEST['phone'];
            $user->relationship_proof = $_FILES['proof']['name'];
            $user->add_date = new CDbExpression("NOW()");
            $user->ip = CHttpRequest::getUserHostAddress();
            $user->status = 1;
            $user->referrer_id = Yii::app()->user->id;
            $user->save();

            $file_src = "docs/" . $_FILES['proof']['name'];
            move_uploaded_file($_FILES['proof']['tmp_name'], $file_src);

            $studio = $this->studio;
            Yii::app()->email->addReferrermail($_REQUEST, Yii::app()->user->first_name, $studio);

            Yii::app()->user->setFlash('success', 'Your request gone for approval');
            $this->redirect($this->createUrl('Referrer'));
        } else {
            Yii::app()->user->setFlash('error', 'Something wrong happen please try again.');
            $this->redirect($this->createUrl('Referrer'));
        }
    }

    function createUniqSubdomain($domainName) {
        if ($domainName) {
            $domain = str_replace('www.', '', $domainName);
            $domain = explode('.', $domain);
            $subdomain = $domain[0];
            if ($subdomain) {
                if (in_array($subdomain, $this->reservedDomain)) {
                    $subdomain = $subdomain . '1';
                }
                $studio = new Studio();
                $avalDomain = $studio->checkSubdomain($subdomain);
                return $avalDomain;
            } else {
                return FALSE;
            }
        }
    }

    public function actionSettings() {
        if (isset(Yii::app()->user->role_id) && Yii::app()->user->role_id == 5) {
            //$this->pageTitle = ucwords(Yii::app()->user->first_name) . " | Security";
            //$this->breadcrumbs = array('Settings' => array('partner/Settings'), 'Security');
            $user = Yii::app()->db->createCommand()
              ->select('reseller_type')
              ->from('portal_user')
              ->where('id=:id', array(':id' => Yii::app()->user->id))
              ->queryRow();
            $cards = ResellerCardInfos::model()->findAllByAttributes(array('portal_user_id' => Yii::app()->user->id), array('order' => 'is_cancelled ASC'));
            $this->render('account',array('reseller_type'=>$user['reseller_type'],'cards' => $cards));
        } else {
            $this->redirect($this->createUrl('/partner'));
        }        
    }

    public function actionSavepassword() {
        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
        $new_password = isset($_REQUEST['new_password']) ? $_REQUEST['new_password'] : '';
        $conf_password = isset($_REQUEST['conf_password']) ? $_REQUEST['conf_password'] : '';
        $user = Yii::app()->db->createCommand()
                ->select('*')
                ->from('portal_user')
                ->where('id=:id', array(':id' => Yii::app()->user->id))
                ->queryRow();
        $enc = new bCrypt();

        if ($enc->verify($password, $user['encrypted_password']) && $new_password != '' && strcmp($new_password, $conf_password) == 0) {
            $email = $user['email'];
            $pass = $enc->hash($new_password);
            Yii::app()->db->createCommand("UPDATE portal_user SET encrypted_password = '" . $pass . "' WHERE id=" . Yii::app()->user->id)->execute();
            Yii::app()->user->setFlash('success', 'Your password has been updated successfully!');
            $url = $this->createUrl('partner/Settings');
            $this->redirect($url);
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in updating your password.');
            $url = $this->createUrl('partner/Settings');
            $this->redirect($url);
            exit;
        }
    }

    public function actionGetpartnername() {
        $studio_id = Yii::app()->user->id;
        $customer = UserRelation::model()->findAll(
                array(
                    'select' => 'refer_studio_id',
                    'condition' => 'user_id=:id AND relation_type=:type',
                    'params' => array(':id' => $studio_id, ':type' => partner)
        ));
        if (!empty($customer)) {
            $studio = Studio::model()->findAll(
                    array(
                        'select' => 'name',
                        'condition' => 'id=:id',
                        'params' => array(':id' => $customer[0]->refer_studio_id)
            ));
            $msg = $studio[0]->name;
        } else {
            $msg = 'Muvi';
        }
        $ret = array('msg' => $msg);
        echo json_encode($ret);
    }

    public function actionMarketingMaterial() {
        if (isset(Yii::app()->user->role_id) && Yii::app()->user->role_id == 5) {
            $this->render('marketingmaterial');
        } else {
            $this->redirect($this->createUrl('/partner'));
        }
    }
    public function actionresetpassword() {
        if (isset(Yii::app()->user->id) && Yii::app()->user->id) {
            $this->actionLogout();
        }else{
            if($_REQUEST['auth']!='' && $_REQUEST['uid']!=''){
                $sql = "SELECT * FROM portal_user where id = '".base64_decode($_REQUEST['uid'])."' AND token = '".$_REQUEST['auth']."'";
                $conn = Yii::app()->db;
                $datap = $conn->createCommand($sql)->queryAll();
                $userid = $datap[0]['id'];        
                if(!empty($datap)){
                    $this->render('reset_password',array("user"=>1,"user_id"=>$userid));
                }else{
                    $this->render('reset_password',array("user"=>0));
                }
            }else{
                echo "<h3>Invalid Security Token . please try again. </h3>";
            }
        }
    }
    public function actionset_password() {
        $user_id = $_REQUEST['user_id'];
        $user = PortalUser::model()->findByPK($user_id);
        if(!empty($user)){
            $enc = new bCrypt();
            $encrypted_pass = $enc->hash($_REQUEST["new_password"]);
            $user->encrypted_password = $encrypted_pass;
            $user->token = '';
            $resp = $user->update();
            if($resp){
                echo "success";
            }else{
                echo "failure";
            }
        }else{
            echo "failure";
        }
    }
    function actionMakePrimaryCard() {
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            $user_id = Yii::app()->user->id;

            //Update card to inactive mode
            $sql = "UPDATE reseller_card_infos SET is_cancelled=1 WHERE portal_user_id=" . $user_id;
            $con = Yii::app()->db;
            $ciData = $con->createCommand($sql)->execute();

            $card = ResellerCardInfos::model()->findByAttributes(array('id' => $_REQUEST['id_payment']));
            $card->is_cancelled = 0;
            $card->save();

            Yii::app()->user->setFlash('success', 'Credit Card has been set as primary successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be made as primary!');
        }

        $url = $this->createUrl('partner/settings');
        $this->redirect($url);
    }
    function actionDeleteResellerCard() {
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            ResellerCardInfos::model()->deleteByPk($_REQUEST['id_payment']);

            Yii::app()->user->setFlash('success', 'Credit Card has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be deleted!');
        }

        $url = $this->createUrl('partner/settings');
        $this->redirect($url);
    }
    
    function actionSubscription() {
        $levels = ResellerPlan::model()->getResellerPlan();
        //print '<pre>';print_r($packages);exit;
        
        //$this->renderpartial('subscription', array('card' => $card, 'packages' => $packages));
        $this->render('subscription', array('card' => $card, 'levels' => $levels));
}
	function actionsearchTicket() {
        $this->pageTitle = Yii::app()->name . 'Support';
        $this->breadcrumbs = array('Support', 'All Tickets');
        $dbcon=$this->getDbConnection();          
        //get the list of tickets for all reseller and list out.
        $studio_id = Yii::app()->user->id; //portal user record _id     
       
        /* For mulitiple partner */       
        $parent = "SELECT parent_id FROM portal_user WHERE id={$studio_id}";
        $parentrecord = Yii::app()->db->createCommand($parent)->queryRow();
        $refer_id = ($parentrecord['parent_id'] != 0) ? $parentrecord['parent_id'] : $studio_id;        
        $user_relation = "SELECT studio_id FROM user_relation WHERE refer_id=" . $refer_id . " and relation_type='reseller'";
        $customer = $dbcon->createCommand($user_relation)->queryAll();
        $reseller_studioid=array();
        foreach($customer as $key=>$val){
            $reseller_studioid[]=$val['studio_id'];
		}
       
        $tickettingdb=$this->getAnotherDbconnection();
        
        $user_sql="select email from user where studio_id in(".implode(',',$reseller_studioid).") and role_id=1";   
        
        $reg_studioemail = $dbcon->createCommand($user_sql)->queryAll();
        
        $reseller_email=array();
        foreach($reg_studioemail as $key=>$val)
        {
            $reseller_email[]=$val['email'];
        }
       
        $master_sql='select id from ticket_master where studio_email in("'.implode('","',$reseller_email).'")';         
        $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
        
        $all_masterid=array();       
        foreach($master_id as $key=>$val)
        {
            $all_masterid[]=$val['id'];
        } 
        $master_ids=implode(',',$all_masterid);
        
        $num_rec_per_page = 20;
        $start_from = 0;
        if (isset($_REQUEST['page'])) {
                $start_from = ($_REQUEST['page'] - 1) * $num_rec_per_page;
            }
       $model = new TicketForm;
            $sort_by = '';
            if (isset($_REQUEST['sortBy'])) {
                $sort_by = $_REQUEST['sortBy'];
                 if ($_REQUEST['sortBy'] == "last_updated_date_desc")
                    $orderby = "last_updated_date DESC";
                else if ($_REQUEST['sortBy'] == "last_updated_date_asc")
                    $orderby = "last_updated_date ASC";
            } else
            $orderby = 'last_updated_date DESC';
            $limit = " LIMIT $start_from, $num_rec_per_page";
            $cond = "(ticket_master_id in (".$master_ids.") or portal_user_id=".$studio_id.")" ;
           
            if($_REQUEST['ticketstatus']=="Open"){          
            $cond.=" and status in('New','Working','re-opened','Re-opened')";
            }
            else if($_REQUEST['ticketstatus']=="Closed")
            {
               $cond.=" and status ='Closed'";

            }
            if($_REQUEST['priority']==0){          
            $cond.=" and priority in('critical','high')";
            }
            else if($_REQUEST['priority']==1)
            {
               $cond.=" and priority in('medium')";

            }else if($_REQUEST['priority']==2)
            {
               $cond.=" and priority in('low')";
            }
            if($_REQUEST['ticketstores']>0)
            {
             $cond.=" and ticket_master_id=".$_REQUEST['ticketstores'];   
            }
            if (isset($_REQUEST['search_value']) && !empty($_REQUEST['search_value'])) {
                $cond.=" AND (";
                if (is_numeric($_REQUEST['search_value']))
                    $cond.="id=" . $_REQUEST['search_value'] . " OR";
                $cond.=" description LIKE '%" . addslashes($_REQUEST['search_value']) . "%' OR priority LIKE '%" . addslashes($_REQUEST['search_value']) . "%' )";
            }
            $total_query = "SELECT SQL_CALC_FOUND_ROWS(0),i.* from ticket i where ticket_master_id in (".$master_ids.")";
            $res = $tickettingdb->createCommand($total_query)->queryAll();
            $total_records = count($res);
            $total_pages = ceil($total_records / $num_rec_per_page);
            $total_pages = ceil($total_records / $num_rec_per_page);
            $allticket = $model->ticketList($cond, $orderby, $limit);
            $count_searched = count($allticket);
            $tickettingdb->active = false;
            $this->renderPartial('ticketrenderdata',array('list' => $allticket,'page_size' => $num_rec_per_page, 'count_searched' => $count_searched, 'total_pages' => $total_pages, 'total_records' => $total_records,'sort_by'=>$sort_by));

       exit;
    }
    public function actionTicketList() {
        $this->pageTitle = 'Stores - Support';
        $this->breadcrumbs = array('Support');
	$num_rec_per_page=20;
        $dbcon=$this->getDbConnection();                 
        $studio_id = Yii::app()->user->id; //portal user record _id     
        $masterid=array();
        $masterid=self::allMasterIDs($studio_id,$dbcon);
        $all_masterid=array();       
        foreach($masterid as $key=>$val)
        {
            $all_masterid[$val['id']]=$val['studio_name'];
        } 
        $this->render('ticketList', array('page_size' => $num_rec_per_page,'master_id' =>@$all_masterid));
        exit;
        
    }
    public function actionaddTicket() {
        $this->pageTitle = 'Stores - Add Ticket';
        $this->breadcrumbs = array('Support', 'Add Tickets');
        $dbcon=$this->getDbConnection();                 
        $studio_id = Yii::app()->user->id; //portal user record _id     
        $masterid=array();
        $masterid=self::allMasterIDs($studio_id,$dbcon);
        $all_masterid=array();       
        foreach($masterid as $key=>$val)
        {
            $all_masterid[$val['id']]=$val['studio_name'];
        } 
        
        $this->render('addTicket', array('master_id' =>@$all_masterid));
        exit;
    }
    
    public function actioninsertTicket()
    {
        $attachment_patharray=array();
        $tickettingdb=$this->getAnotherDbconnection();
        if($_POST['TicketForm']['master_id']==0)
        {
        $_POST['TicketForm']['ticket_master_id']=0; 
        $_POST['TicketForm']['portal_user_id']=Yii::app()->user->id;; 
        $_POST['TicketForm']['studio_id']= 0;
        $user_id=0;       
        }else {
        $master_sql="select studio_id from ticket_master where id=".$_POST['TicketForm']['master_id'];         
        $master_id=$tickettingdb->createCommand($master_sql)->queryAll();  
        $studio_id=$master_id[0]['studio_id'];
        $tickettingdb->active=false;
        $_POST['TicketForm']['ticket_master_id']=$_POST['TicketForm']['master_id'];
        $_POST['TicketForm']['studio_id']= $master_id[0]['studio_id'];
        $choosed_user="SELECT id FROM `user` WHERE studio_id=".$master_id[0]['studio_id']." and is_active=1 and  role_id=1";
        $user_detail=$tickettingdb->createCommand($choosed_user)->queryAll(); 
        $user_id=$user_detail[0]['id'];
        $_POST['TicketForm']['portal_user_id']=0; 
        }
        $model = new TicketForm;
        if (empty($_POST['TicketForm']['title']) && trim($_POST['TicketForm']['title'])=='') {
            Yii::app()->user->setFlash('error','Ticket title can not be empty');
                $this->redirect('ticketList');  
                exit;
        
        }
        if (isset($_POST['TicketForm']) && !empty($_POST['TicketForm'])) {
           
            $_POST['TicketForm']['creater_id']=$user_id;
            $model->attributes = $_POST['TicketForm'];
            
            $rid = $model->insertTicket($_POST['TicketForm']);
            
            if ($rid) {
                if (!empty($_FILES['upload_file1']['name'])) {
                    Yii::import('application.controllers.TicketController'); 
                    $attachments = TicketController::attachFile($_FILES, $rid);
                    $arr['TicketForm']['attachment'] = implode($attachments, ',');
                    $arr['TicketForm']['id_ticket'] = $rid;
                    $model->updateTicket($arr['TicketForm']);
                    
                }
                $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                foreach ($attachments as $k){
                $attachment_patharray[]=$src."/".$k;
                }
                $std = new Studio();
                $studio = $std->findByPk($studio_id);
                $title =($_POST['TicketForm']['title']);
                if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
                $description =($_POST['TicketForm']['description']);
                if (strlen($description) > 16) 
                {
                    $description = wordwrap($description, 16);
                    $description = substr($description, 0, strpos($description, "\n"));
                    
                }
                $subject = "Ticket" . $rid . "_".$title."_New Ticket Added";
                $url = "http://".DOMAIN_COOKIE; 
                $thtmltoCustomer =  "<p>Hi " . ucfirst($studio->name) . ",<br /><br />
                                    Your support ticket is ".$rid. " added.<br/> ";
                $thtmltoCustomer.= " Ticket details:- <br /><br/><b>Priority</b>:" . $_POST['TicketForm']['priority'] .'</p>';
                $thtmltoCustomer.= "<b>Description:</b>".nl2br(stripslashes(htmlentities($_POST['TicketForm']['description'])))."<br /><br/>";
                $thtmltoCustomer.="You will hear from Muvi Support team soon. Please find updates to your ticket <a target='_blank' href='" .$url. "'>here</a>. 
                                    Add an update to the ticket by logging into <a target='_blank' href='" . $url ."'>Muvi</a>, or simply replying to this email.<br /><br />
                                </p>" ."<p><br/>Regards,<br /> Muvi</p>";               
                $htmltoAdmin =  "<p>Hi Admin,<br /><br />
                               A new ticket is added by $studio->name .<br />";
                $htmltoAdmin.= " Ticket details:- <br /><br/><b>Priority</b>:" . $_POST['TicketForm']['priority'] . "</p>" ;
                $htmltoAdmin.= "<b>Description:</b>". nl2br(stripslashes(htmlentities($_POST['TicketForm']['description'])))."<p><br/>Regards,<br /> Muvi</p>";
                $to_admin='support@muvi.com';
                $from='support@muvi.com';
                 $test_email_subject="Muvitest ticket";
                 
                 if(strpos($_POST['TicketForm']['description'],$test_email_subject)!== false)
                 {
                     $from='testmuvi@gmail.com';
                     $to_admin='testmuvi@gmail.com';
                 }
                $ret_val1=$this->sendAttchmentMailViaAmazonsdk($to_admin,$subject,$from,$htmltoAdmin,$attachment_patharray);
               
                Yii::app()->user->setFlash('success','Ticket Added Successfully');
                $this->redirect('ticketList');
            }
        } else
            $this->render('addTicket', array('model' => $model)); 
        exit;
    }
    
    /* View Ticket Detail */
    public function actionViewTicket() {
        $this->pageTitle = 'Stores - View Ticket';
        $this->breadcrumbs = array('Support', 'Ticket Detail');
        $model = new TicketForm;
        $cond = "id=" . $_GET['id'] ;
        $ticket = $model->ticketList($cond);
        
        if (strlen($ticket['title']) > 35) {
            $page_title = wordwrap($ticket['title'], 35);
            $page_title = substr($page_title, 0, strpos($page_title, "\n"));
        } else {
           $page_title = $ticket['title'];
        }
       
        if (empty($ticket)) {
            $this->render('error');
            exit;
        }
        $ticketnotes = $model->ticketNotes($_GET['id']);
        $this->render('ticketDetail', array('ticket' => $ticket[0], 'notes' => $ticketnotes));
        exit;       
        }
   public function actiondeletenoteImage()
    {
    Yii::import('application.controllers.TicketController'); 
    TicketController::actiondeletenoteImage();
    exit;
    }
    public function actionUpdateTicket() {
        $dbcon = $this->getDbConnection();
        $studio_id = Yii::app()->user->id; //portal user record _id 

        $parent = "SELECT parent_id FROM portal_user WHERE id={$studio_id}";
        $parentrecord = Yii::app()->db->createCommand($parent)->queryRow();
        $refer_id = ($parentrecord['parent_id'] != 0) ? $parentrecord['parent_id'] : $studio_id;
        $user_relation = "SELECT studio_id FROM user_relation WHERE refer_id=" . $refer_id . " and relation_type='reseller'";
        $customer = $dbcon->createCommand($user_relation)->queryAll();
        $reseller_studioid = array();
        foreach ($customer as $key => $val) {
            $reseller_studioid[] = $val['studio_id'];
        }
        $tickettingdb = $this->getAnotherDbconnection();
        $user_sql = "select email from user where studio_id in(" . implode(',', $reseller_studioid) . ") and role_id=1";
        $reg_studioemail = $dbcon->createCommand($user_sql)->queryAll();
        $reseller_email = array();
        foreach ($reg_studioemail as $key => $val) {
            $reseller_email[] = $val['email'];
        }
        $master_sql = 'select id,studio_name from ticket_master where studio_email in("' . implode('","', $reseller_email) . '")';
        $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
        $all_masterid = array();
        foreach ($master_id as $key => $val) {
            $all_masterid[$val['id']] = $val['studio_name'];
        }
        Yii::import('application.controllers.TicketController');
        $model = new TicketForm;
        if (!isset($_POST['TicketForm'])) { //Edit Ticket
            $id = isset($_POST['id']) ? $_POST['id'] : $_GET['id'];
            $cond = "id=" . $id;
            $ticket = $model->ticketList($cond);
            if (empty($ticket)) {
                $this->render('error');
                exit;
            }
            $this->render('updateTicket', array('ticket' => $ticket[0], 'model' => $model, 'master_id' => @$all_masterid));
        } else { //Update Ticket
            if (empty($_POST['TicketForm']['title']) || trim($_POST['TicketForm']['title']) == '') {
                Yii::app()->user->setFlash('error', 'Ticket title can not be empty');
                $this->redirect('ticketList');
                exit;
            }
            $_POST['TicketForm']['portal_user_id'] = ($_POST['TicketForm']['ticket_master_id'] == 0) ? Yii::app()->user->id : 0;

            $master_sql = "select studio_id from ticket_master where id=" . $_POST['TicketForm']['ticket_master_id'];
            $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
            $studio_id = $master_id[0]['studio_id'];

            $_POST['TicketForm']['studio_id'] = $studio_id;
            $ticket_eta_Details = $model->ticketList("id=" . $_POST['TicketForm']['id_ticket']);
            $before_array = $ticket_eta_Details[0];
            $cc_flag = 1;
            if (($before_array['title'] == $_POST['TicketForm']['title']) && ($before_array['description'] == $_POST['TicketForm']['description'])) {
                $cc_flag = 0;
            }
            $_POST['TicketForm']['title'] = addslashes($_POST['TicketForm']['title']);
            $attachments = array();
            $prevfiles = array();
            if (!empty($_FILES['upload_file1']['name']))
                $attachments = TicketController::attachFile($_FILES, $_POST['TicketForm']['id_ticket']);
            //code for uploading attachment with email
            $src = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
            foreach ($attachments as $k) {
                $attachment_patharray[] = $src . "/" . $k;
            }
            $prevfiles = !empty($_POST['prevfiles']) ? explode(',', $_POST['prevfiles']) : $prevfiles;
            $attachments = array_merge($attachments, $prevfiles);
            $_POST['TicketForm']['attachment'] = !empty($attachments) ? implode($attachments, ',') : '';
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            if ($model->updateTicket($_POST['TicketForm'])) {
                $ticketDetails = $model->ticketList("id=" . $_POST['TicketForm']['id_ticket']);
                $description = ($_POST['TicketForm']['description']);
                //send email to cc if title and description changed
                if ($_POST['TicketForm']['portal_user_id'] == 0) {
                    $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id)->queryAll();
                    $cc_email_list = array();
                    if ($before_array['ticket_email_cc'] != '' && $cc_flag == 1) {
                        $cc_email_list = explode(',', $before_array['ticket_email_cc']);
                        foreach ($cc_email_list as $key => $value) {
                            if (trim($value) == 'support@muvi.com' || trim($value) == $user[0]['email']) {
                                unset($cc_email_list[$key]);
                            }
                        }
                    }
                } else { //if user chooses all reseller
                    $cc_email_list = array();
                }
                if (strlen($description) > 16) {
                    $description = wordwrap($description, 16);
                    $description = substr($description, 0, strpos($description, "\n"));
                }
                $title = addslashes($_POST['TicketForm']['title']);
                if (strlen($title) > 16) {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                }
                if ($_POST['TicketForm']['status'] == 'Closed') {
                    $subject = "Ticket" . $_POST['TicketForm']['id_ticket'] . "_" . $title . " Closed";
                } else if ($_POST['TicketForm']['status'] == 'Re-opened') {
                    $subject = "Ticket" . $_POST['TicketForm']['id_ticket'] . "_" . $title . " Re-Opened";
                } else {
                    $subject = "Ticket" . $_POST['TicketForm']['id_ticket'] . "_" . $title . " Updated";
                }
                $html = TicketController::ticketDetails($ticketDetails);
                //$template = TicketController::tempaleFormat($ticketDetails);
                $toStd = "<p> Hi " . ucfirst($studio->name) . ",<br /><br />";
                $toAdmn = "<p> Hi Admin,<br /><br />";
                $to_admin = array('support@muvi.com');
                $test_email_subject = "Muvitest ticket";
                if (strpos($_POST['TicketForm']['description'], $test_email_subject) !== false) {
                    $to_admin = array('testmuvi@gmail.com');
                }
                $from = 'support@muvi.com';
                $this->sendAttchmentMailViaAmazonsdk($to_admin, $subject, $from, $toAdmn . $html, $attachment_patharray, $cc_email_list, '', '');
                $uri = '';
                if (!empty($_REQUEST['search']))
                    $uri.="/search/" . urlencode($_REQUEST['search']);
                if (!empty($_REQUEST['sortBy']))
                    $uri.="/sortBy/{$_REQUEST['sortBy']}";
                if (!empty($_REQUEST['page']))
                    $uri.="/page/{$_REQUEST['page']}";
                Yii::app()->user->setFlash('success', 'Ticket Updated Successfully');
                $this->redirect('ticketList' . $uri);
            }
        }
    }

    public function actionaddNote()
   {
    Yii::import('application.controllers.TicketController'); 
    TicketController::actionAddNote(); 
   }
   public function actionEditNote()
   {
     Yii::import('application.controllers.TicketController'); 
    TicketController::actionEditNote(); 
   } 
   
   function allMasterIDs($studio_id,$dbcon)
   {      
       $parent = "SELECT parent_id FROM portal_user WHERE id={$studio_id}";
        $parentrecord = Yii::app()->db->createCommand($parent)->queryROW();  
		
        $refer_id = ($parentrecord['parent_id'] != 0) ? $parentrecord['parent_id'] : $studio_id;        
        $user_relation = "SELECT studio_id FROM user_relation WHERE refer_id=" . $refer_id . " and relation_type='reseller'";
        $customer = $dbcon->createCommand($user_relation)->queryAll();
        $reseller_studioid=array();
        foreach($customer as $key=>$val)
        {
            $reseller_studioid[]=$val['studio_id'];
        }      
        $tickettingdb=$this->getAnotherDbconnection();   
        $user_sql="select email from user where studio_id in(".implode(',',$reseller_studioid).") and role_id=1";          
        $reg_studioemail=$dbcon->createCommand($user_sql)->queryAll();          
        $reseller_email=array();
        foreach($reg_studioemail as $key=>$val)
        {
            $reseller_email[]=$val['email'];
        } 
        $master_sql='select id,studio_name from ticket_master where studio_email in("'.implode('","',$reseller_email).'")';         
        $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
        
        return $master_id;
        exit;
   }
   public function actionDeleteTicket()
   {
     Yii::import('application.controllers.TicketController'); 
    $tickettingdb=$this->getAnotherDbconnection();
        $model = new TicketForm;
        //$del = $model->deleteRecord($_POST['id'],'ticket');
        
        $id_ticket=$_REQUEST['id'];
        
         $htmlstd ='';
            $last_updated_date = date('Y-m-d H:i:s');
        
            $last_updated_by = Yii::app()->common->getStudiosId();
            
            $status='Closed';
           $update_qry='UPDATE ticket SET last_updated_date="'.$last_updated_date.'", last_updated_by='.$last_updated_by.',status="'.$status.'" where id='.$id_ticket;

            $rid = $tickettingdb->createCommand($update_qry)->execute();
        
        $select_qry='select ticket_master_id,portal_user_id,title,description,studio_id from  ticket where id='.$id_ticket;    
        $data = $tickettingdb->createCommand($select_qry)->queryAll();
        $tickettingdb->active=false;
        $portal_user_id=$data[0]['portal_user_id'];
        $ticket_mater_id=$data[0]['ticket_mater_id'];
        $studio_id=($portal_user_id==0)?$data[0]['studio_id']:0;
        
      //$description=trim($data[0]['description']);
       $description=trim($data[0]['title']);
       
            $std = new Studio();
            if($portal_user_id==0)
            $studioname = ($studio_id != 1) ? $std->findByPk($studio_id)->name : 'Super Admin';
            else
             $studioname='All Stores';
            $ticketDetails = $model->ticketList("id=" . $id_ticket);
            $ticketnotes = $model->ticketNotes($id_ticket);
           
            if (!empty($ticketnotes)) {
            foreach ($ticketnotes as $key => $updates) {
                if ($key == 0) {
                    $update=stripslashes(nl2br(htmlentities($updates['note'])));
                    if (strlen($update) > 16) {
                    $update = wordwrap($update, 16);
                    $update = substr($update, 0, strpos($update, "\n"));
            } 
                }
            }
        }else{
            $update ="Support ticket is";
        }
        
              $subject = "Ticket" . $id_ticket . "_" . $description  . " Closed";
            
            $template = TicketController::tempaleFormat();
            $toStd = "<p> Hi " . ucfirst($studioname) . ",<br /><br />";
            if ($_SERVER['HTTP_HOST'] == "admin.muvi.com")
            $url = "https://www.muvi.com/ticket/viewTicket/id/"."{$id_ticket}";
            else
            $url = "http://www.idogic.com/ticket/viewTicket/id/"."{$id_ticket}";
            
            $htmlstd .= " If you believe the issue/feature is not resolved yet, log into <a target='_blank' href='" . $url . "'>Muvi</a>,  and reopen the ticket. <br /><br />";

            
            $toAdmn = "<p> Hi Admin,<br /><br />";
            $html = TicketController::ticketDetails($ticketDetails);
            
            $adminEmail = array('suraja@muvi.com');
            if ($studio_id != 1) {
                $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id)->queryAll();
                $studioemail = $user[0]['email'];
               // $this->sendmailViaMandrill($toStd . $html . $htmlstd."</p>" . $template[1], $subject, array(array("email" => $studioemail))); //Yii::app()->user->email
                }
                $to_admin=$adminEmail;
                $from='support@muvi.com';
                $this->sendmailViaAmazonsdk($toAdmn . $html . $htmlstd."</p>" . $template[1],$subject,$to_admin,$from);
              
               
        Yii::app()->user->setFlash('success','Ticket Closed Successfully');
        $this->redirect($this->createUrl("partner/ticketList"));
   }
}
