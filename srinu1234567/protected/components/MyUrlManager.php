<?php
class MyUrlManager extends CUrlManager {
    private function getRules() {
        $studio_id = Yii::app()->common->getStudiosId();
        /*
         * RK: Commented these since it affected the Facebook login
		$p = new CHtmlPurifier();
		$p->options = array('URI.AllowedSchemes'=>array('http' => true,'https' => true,));
		$_SERVER['REQUEST_URI'] = $p->purify($_SERVER['REQUEST_URI']);	
         * 
         */	
        // URL Routing 
		if(!(SUB_DOMAIN =='studio' || SUB_DOMAIN =='devstudio')){
			$routs = UrlRouting::model()->findAllByAttributes(array('studio_id' => $studio_id));
			foreach ($routs as $key => $value) {
				$rules[$value->permalink] = $value->mapped_url;
			}
		}else{
			$rules['rtmp-streaming'] = 'livestream/rtmpFeedStreaming';
			$rules['live-streaming'] = 'livestream';
		}

        $rules['embed/livestream/<embed_id>'] = 'embed/livestream/uniq_id/<embed_id>';
        $rules['apps/<apptype>/template'] = 'apps/template/app/<apptype>';
        $rules['embed/<embed_id>'] = 'embed/index/embed_id/<embed_id>';
        $rules['share/<embed_id>'] = 'embed/index/embed_id/<embed_id>/isShare/true';
        $rules['embedTrailer/<uniq_id>'] = 'embedTrailer/index/uniq_id/<uniq_id>';
        $rules['player/<permalink>'] = 'player/playMovie/contentPlink/<permalink>';
        $rules['player/<permalink>/stream/<stream_id>'] = 'player/playMovie/contentPlink/<permalink>/stream/<stream_id>';
        $rules['player/<permalink>/stream/<stream_id>/season/<season_id>'] = 'player/playMovie/contentPlink/<permalink>/stream/<stream_id>/season/<season_id>';
        $rules['player/<permalink>/season/<season_id>/stream/<stream_id>/'] = 'player/playMovie/contentPlink/<permalink>/season/<season_id>/stream/<stream_id>';
        $rules['player/<permalink>/season/<season_id>'] = 'player/playMovie/contentPlink/<permalink>/season/<season_id>';
        $rules['playout'] = 'Tvguide/index';
        $rules['tvguide'] = 'Tvguide/index';				
        $rules['TvGuidePlayer/<permalink>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>';
        $rules['TvGuidePlayer/<permalink>/stream/<stream_id>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/stream/<stream_id>';
        $rules['TvGuidePlayer/<permalink>/stream/<stream_id>/season/<season_id>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/stream/<stream_id>/season/<season_id>';
        $rules['TvGuidePlayer/<permalink>/season/<season_id>/stream/<stream_id>/'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/season/<season_id>/stream/<stream_id>';
        $rules['TvGuidePlayer/<permalink>/season/<season_id>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/season/<season_id>';
        //$rules['howitworks'] = 'page/howitworks';
       // $rules['technology'] = 'page/technology';
        $rules['star/<title>'] = 'star/show/item/<title>';
        $rules['contactus'] = 'site/contactus';
        //Added to make available a home url for studio
        $has_home_url = StudioConfig::model()->getconfigvalue('has_home_url');
        if(@$has_home_url['config_value'] == 1){
            $rules['home'] = 'site/home';
        }        
        $rules["page/<title>"] = "page/show/permalink/<title>";
        $rules["page/<title>/mobile_view"] = "page/show/permalink/<title>/mobileview";
        //$rules['howitworks'] = 'page/howitworks';
        //$rules['about'] = 'page/about';
        //$rules['team'] = 'page/team';
        //$rules['faqs'] = 'page/faqs';
        //$rules['examples'] = 'page/examples';
        //$rules['tour'] = 'page/tour';
        //$rules['pricing'] = 'page/pricing';
        //$rules['howisitdiff'] = 'page/howisitdiff';
        $rules['sitemap.xml'] = 'sitemap/default/index';
        $rules['sitemap.html'] = 'sitemap/default/index/format/html';
        $rules['news-sitemap.xml'] = 'sitemap/default/newsSitemap';
        $rules['mrss_<unique_id>_<feed_id>.xml'] = 'sitemap/default/mrssFeed/mrss_id/<unique_id>/feed_id/<feed_id>';
        //$rules['terms'] = 'site/terms';
        //$rules['privacypolicy'] = 'site/privacypolicy';
        //$rules['pricing_calculator'] = 'page/pricing_calculator';
        //$rules['downloads'] = 'site/download';
        $rules['video-streaming-platform'] = 'site/video_streaming_platform';
        $rules['contactus'] = 'site/contactus';
        $rules['contactus/mobile_view'] = 'site/contactus/mobileview';
       // $rules['faq'] = 'site/faq';
        //$rules['aboutus'] = 'site/aboutus';
        $rules['byod'] = 'byod/index';
        $rules['partners'] = 'partners/index';        
        $rules['application'] = 'page/application';
        $rules['insertapplication'] = 'page/insertapplication';
        $rules['muvikart'] = 'shop/index';
        $rules['shop'] = 'Shop/index';
        //$rules['adminlogin'] = 'site/adminlogin';
        //LInks for Blog by RKS
        $rules['blog'] = 'blog/index';
        $rules['blog/mobile_view'] = 'blog/index/mobileview';
        $rules['blog/saveUserComment'] = 'blog/saveUserComment';
        $rules['blog/savecomment'] = 'blog/savecomment';
        $rules['blog/LoadCommentForm'] = 'blog/LoadCommentForm';
        $rules['blog/<permalink>'] = 'blog/view';
        $rules['blog/<permalink>/mobile_view'] = 'blog/view/mobileview';
        $rules['faqs'] = 'faqs/index';
        $rules['testsetup'] = 'signup/index';
        $rules['faqs/mobile_view'] = 'faqs/index/mobileview';
        $rules['<controller:\w+>/<id:\d+>'] = '<controller>/view';
        $rules['<controller:\w+>/<action:\w+>/<id:\d+>'] = '<controller>/<action>';
        $rules['<controller:\w+>/<action:\w+>'] = '<controller>/<action>';
        $rules['robots.txt'] = 'site/robotsTxt';
        $rules['loaderio-37096f625715b1799d3360cc55c513f5.txt'] = 'site/loadTestMuvi';
		//echo "<pre>";print_r($rules);exit;
        if ((SUB_DOMAIN == 'studio' || SUB_DOMAIN == 'devstudio') && (HOST_IP != '52.77.47.122') && (HOST_IP != '52.211.41.107') && (HOST_IP != '52.70.64.85') && (HOST_IP != '52.65.244.101')) {
            $rules['/'] = Yii::app()->baseUrl . '/wpstudio';
        }
        return @$rules;
    }

    protected function processRules() {
        $ip_address = Yii::app()->request->getUserHostAddress();
        //Setting dummy data during sign up    
        $current_path = $_SERVER['REQUEST_URI'];
        $path_info = pathinfo($current_path);
        //Added by RK to resolve the smap traffic to the sign up
        if ((isset(Yii::app()->user->id) && Yii::app()->user->id > 0) || $path_info['filename'] == 'helps' || (trim($path_info['dirname'],'/') =='rest') || (trim(strtolower($path_info['dirname']),'/') =='restv5')) {
            
        }else if(isset($_POST) && count($_POST) > 0) {
            
        } else if (isset($_GET) && count($_GET) > 0 && SUB_DOMAIN == 'studio' && !isset(Yii::app()->user->id)) {
            foreach ($_GET as $key => $value) {
                    switch (true) {
                        case strpos($path_info['basename'], 'contacts.html') > -1 || strpos($path_info['dirname'], 'contacts.html') > -1:
                            $pattern = '/name|company|company_name|hour|phone|ccheck|email|message|wysija|alert|script/';
                            break;
                        case strpos($path_info['basename'], 'reseller-referral-application.html') > -1 || strpos($path_info['dirname'], 'reseller-referral-application.html') > -1:
                            $pattern = '/name|company|company_name|hour|ccheck|phone|email|message|wysija|alert|script/';
                            break;
                        case strpos($path_info['basename'], 'webinar.html') > -1 || strpos($path_info['dirname'], 'webinar.html') > -1:
                            $pattern = '/name|company|company_name|hour|ccheck|phone|email|message|wysija|alert|script/';
                            break;
                        case strpos($path_info['basename'], 'pricing.html') > -1 || strpos($path_info['dirname'], 'pricing.html') > -1:
                            $pattern = '/name|company|company_name|hour|phone|ccheck|email|message|wysija|alert|script/';
                            break;
                        case strpos($path_info['basename'], 'signup') > -1 || strpos($path_info['dirname'], 'signup') > -1:
                            $pattern = '/name|company_name|hour|companyname|phone|email|subdomain|wysija|alert|script/';
                            break;
                        default:
                            $pattern = '/wysija|alert|script/';
                    }
                    $key_pattern = '/aq|q/';
                    $matchkey = preg_match($pattern, strtolower($key));
                    $matchval = preg_match($pattern, strtolower($value));
                    if ($matchkey) {
                        $redirect_url = Yii::app()->getBaseUrl(true) . '/signup/landing';
                        header("Location: $redirect_url");
                        exit();
                    }
                    if (preg_match($key_pattern, strtolower($key)) > 0) {
                        $redirect_url = Yii::app()->getBaseUrl(true) . '/signup/landing';
                        header("Location: $redirect_url");
                        exit();
                    }
                
            }
        }
        //}


        $this->rules = $this->getRules();
        return parent::processRules();
    }

}
