<input type="hidden" class="data-count" value="<?php echo $total_video ?>" /> 
<input type="hidden" id="data-count1" value="<?php echo $count_searched ?>" /> 
<table class="table" id="list_tbl">
    <thead>
        <tr>
            <th style="width:100px;">
    <div class="checkbox m-b-15">
        <label>
            <input type="checkbox" class="sub_chkall" id="check_all"  >
            <i class="input-helper"></i>

        </label>
    </div>
</th>
<th>File Name</th>
<th>Audio</th>
<th data-hide="phone" class="width">Properties</th>
<th data-hide="phone" class="width">Action</th>
</tr>
</thead>
<?php
if (!empty($allvideo)) {
    foreach ($allvideo as $key => $val) {
        //  $images_list=explode("public",$val['list_image_name']);
        // $images_cropped=explode("public",$val['image_name']);
        if ($val['thumb_image_name'] == '') {
            $img_path = "https://d1yjifjuhwl7lc.cloudfront.net/public/no-image-h-thumb.png";
            $video_remote_url = "https://d1yjifjuhwl7lc.cloudfront.net/public/no-image-h-thumb.png";
        } else {
            //$img_path = $val['list_image_name'];
            $img_path = $base_cloud_url . "videogallery-image/" . $val['thumb_image_name'];
            $video_remote_url = $base_cloud_url . $val['video_name'];
        }
        ?>
        <tbody>
            <tr>
                <td>
                    <div class="checkbox m-b-15">
                        <label>
                            <input type="checkbox" class="sub_chk" data-id="<?php echo $val['id']; ?>" name="delete_all[]" >
                            <i class="input-helper"></i>
                        </label>
                    </div> 
                </td>
                <td>
                    <?php echo $val['audio_name']; ?>
                </td>
                <td>
                    <?php if ($val['is_uploaded'] == 0) { ?>                        
                        <audio controls>
                            <?php 
                                echo '<source src=\''.$base_cloud_url.$val['audio_name'].'\'.mp3\'\' type=\'audio/ogg\'>';
                            ?>
                           Your browser does not support the audio element.
                         </audio>                            
                    <?php } ?>
                </td>    
                <td>
                    <?php
                    $video_prop = json_decode($val['video_properties']);
                    foreach ($video_prop as $k => $v) {
                        if ($k == 'duration') {
                            $k = $k . "(hh:mm:ss.ms)";
                        }
                        echo ucfirst(str_replace('_', ' ', $k)) . ":" . $v;
                        echo "<br/>";
                    }
                    if (($val['flag_uploaded'] != 1 ) && ($val['flag_uploaded'] != 2 )) {
                        echo "Encoded : ";
                        if (($val['movieConverted'] == 1) || ($val['trailerConverted'] == 1)) {
                            echo "Yes";
                        } else {
                            echo "No";
                        }
                    }
                    ?>
                </td>   
                <td> 
                    <?php if ($val['flag_uploaded'] == 1) { ?>
                        <p><a title="Audio download in progress. It will be available within an hour" data-toggle="tooltip" href="javascript:void(0);"><em class="icon-cloud-download"></em>&nbsp; Download in progress</a></p>
        <?php } else if ($val['flag_uploaded'] == 2) {
            ?>
                        <p data-toggle="tooltip" title="Downloading failed"><em class="fa fa-info-circle" style="color:red;"></em></p>
                        <?php
                    } else {
                        ?>
                        <p>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-id="<?php echo $val['id'];?>" title="Delete Audio" data-msg ="Are you sure to delete the Audio ?" class="confirm" id="confirm"><em class="icon-trash" ></em>&nbsp; Delete </a>
                        </p>
                    <?php } ?>
                </td>
            </tr>	
        </tbody>
    <?php }
} else {
    ?>					
    <tbody>
        <tr>
            <td colspan="4">No Audio Found</td>
        </tr>
    </tbody>
<?php } ?> 
</table>
<script>
    $(document).ready(function () {
        $("#check_all").click(function () {
            $(".sub_chk").prop('checked', $(this).prop('checked'));
        });
    });
    $(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('data-id');
            var msg = $(this).attr('data-msg');
            confirmDelete(location, msg,this);
        });
    });
</script>