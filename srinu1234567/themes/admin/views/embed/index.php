<?php 
$v = RELEASE;
$play_length = 0;
$play_percent = 0;
if(isset($durationPlayed['played_length']) && isset($durationPlayed['played_percent'])) {
    $play_length = $durationPlayed['played_length'];
    $play_percent = $durationPlayed['played_percent'];   
}
//this section is used to set resolution for particular studio
if(count($multipleVideo)>0){
    $setNewResolution='144';
    ksort($multipleVideo);
    $resolutionArr= array_keys($multipleVideo);         
    sort($resolutionArr);    
    if(count($resolutionArr)>3){
      $setNewResolution=$resolutionArr[3];  
    }else if(count($resolutionArr)>2){
      $setNewResolution=$resolutionArr[2];
    }else{
      $setNewResolution=$resolutionArr[1];
    }
}
?>
<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# video: http://ogp.me/ns/video#">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
       <?php if(!isset($_REQUEST['isApp'])){ 
             $this->renderPartial('share_meta',array('page_title'=>$page_title,'studio_name'=>$this->studio->name,'page_desc'=>$page_desc,'item_poster'=>$item_poster,'og_video' =>$og_video,'share_fullmovie_path' => @$share_fullmovie_path));             
        } ?>            
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <script type='text/javascript'>
      
       // video js error message
            var you_aborted_the_video_playback ="<?php echo $this->Language["you_aborted_the_video_playback"]; ?>";
            var a_network_error_caused ="<?php echo $this->Language["a_network_error_caused"]; ?>";
            var the_video_playback_was_aborted ="<?php echo $this->Language["the_video_playback_was_aborted"]; ?>";
            var the_video_could_not_be_loaded ="<?php echo $this->Language["the_video_could_not_be_loaded"]; ?>";
            var the_video_is_encrypted ="<?php echo $this->Language["the_video_is_encrypted"]; ?>";
            var no_compatible_source ="<?php echo $this->Language["no_compatible_source"];?>";
            var played_button = 0;
            var muviWaterMark = "";
<?php if (isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != '' && $embedWaterMArk == 1) { ?>
                var muviWaterMark = "<?php echo $_REQUEST['waterMarkOnPlayer'] ?>";
<?php } else if($waterMarkOnPlayer != '') {  ?>
                var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
<?php } ?>
        </script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />          
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.progressTips.js"></script> 
        
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.progressTips.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <!--<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">-->
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <!--<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ads.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/vast-client.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.vast.js"></script>-->   


        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>        

        <!--Added by Sanjeev, for buffer log-->
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/video-buffer-log.js"></script>  
        
        <script data-cfasync="false" type="text/javascript">
            //videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
            .vjs-fullscreen {padding-top: 0px}  
            .RDVideoHelper{display: none;}
            video::-webkit-media-controls {
                display:none !important;
            }
            video::-webkit-media-controls-panel {
                display: none !important;
            }
		body {
			background-color: #1E1E1E;;
			color: #fff;
			font: 12px Roboto,Arial,sans-serif;
			height: 100%;
			margin: 0;
			overflow: hidden;
			padding: 0;
			position: absolute;
			width: 100%;
		}
		html {
			overflow: hidden;
		}
		.vjs-fluid{
			width: 100%;
			height: 0;
			padding-top: 56.25%;
		}
                .temp-bg{
                    position: absolute;
                    z-index: 90;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background-color: #000;
                }
            #pause_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none;
            }
            #play_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none; 
            }
        </style>
        <style type="text/css">
          .vjs-default-skin .vjs-control.vjs-fullscreen-control, 
            .vjs-default-skin .vjs-control.vjs-mute-control,
            .vjs-default-skin .vjs-control.vjs-subtitles-button,
            .vjs-default-skin .vjs-control.vjs-res-button{
                position:relative;
                float:right;
                margin-right:0 !important;
                margin-top:0 !important;
                left:auto;
                right:auto;
            }
            .vjs-res-button .vjs-control-content{
                margin-right:0px;
            }
            .vjs-default-skin .vjs-subtitles-button:before{
                margin-right:0 !important;
            }
            .vjs-res-button .vjs-control-content .vjs-menu,
            .vjs-subtitles-button .vjs-control-content .vjs-menu{
                    left:0px;
             }
            .vjs-res-button .vjs-control-text{
                position:static;
            }                                  
            .vjs-default-skin .vjs-slider{
               left:0;
            }             
        <?php if(isset($_REQUEST['isApp']) && $_REQUEST['isApp']==1){?>        
            .vjs-control-bar{
                padding-right:30px;
            }
            @media only screen and (max-width: 768px) {                                            
                .vjs-control-bar{
                    padding-right:10px;
                }
            } 
        <?php }else if(isset($_REQUEST['isApp']) && $_REQUEST['isApp']==2){?>
        
        
        <?php }else{?>
                /*.vjs-default-skin.vjs-using-native-controls .vjs-control-bar{
                    display:block;
                }*/
                @media only screen and (max-width: 768px) { 
                    .video-js .vjs-volume-control{
                       display:none;
                    }
                }        
        <?php } ?>                   
         </style>
    </head>
    <body>
        <input type="hidden" id="full_video_duration" value="" />
        <input type="hidden" id="full_video_resolution" value="" />
        <input type="hidden" id="u_id" value="0" />
	<input type="hidden" id="device_id" value="" />
	<input type="hidden" id="embed_id" value="<?php echo @$_REQUEST['embed_id'];?>" />
	<input type="hidden" id="user_id" value="" />
	<input type="hidden" id="device_type" value="<?php echo @$_REQUEST['device_type'];?>" />
        <input type="hidden" id="buff_log_id" value="0" />
        <center id="videoDivContent">
            <?php if(@$_REQUEST["isApp"] == 1) { ?>
            <noscript>
                <style type="text/css">
                    #video_block {display:none; content:none;}
                </style>
                <div style="font-size: 22px; position:absolute; left:0; right:0; top:0; bottom:0; margin:auto; width:100%; height:10%;">
                You don't have javascript enabled.
                </div>
            </noscript>
            <?php } ?>
            <div class="demo-video video-js-responsive-container vjs-hd"  style="position: relative;">
                <input type="hidden" id='prevSeekingTime' value="0" />
                <div class="videocontent" style="overflow:hidden;">
                    <video id="video_block" class="video-js vjs-default-skin vjs-16-9 "  <?php if((@$_REQUEST['isApp']== 2 && ($play_percent == 0 || $play_length == 0)) || (@$_REQUEST['isApp']== 1)){ ?> autoplay ="true" preload="auto" <?php } else{ ?> preload="none" <?php } ?> controls='false'   fluid = 'true' width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}' webkit-playsinline playsinline>
                        <?php
                            foreach ($multipleVideo as $key => $val) {
                                echo '<source src="' . $fullmovie_path . '" data-res="' . $key . '" type="video/mp4" />';
                            }
                            if(!isset($_REQUEST['isApp']) || ($_REQUEST['isApp'] != 1) || (@$_REQUEST['isApp'] == 1 && @$_REQUEST['device_type'] == 4) ){
                                foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval){
                                    if($subtitleFilesKey == 1){
                                        echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                                    } else{
                                        echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                                    }
                                }
                            }
                        ?>
                    </video>
                </div> 
                                <div id='play-btn' class="play-btn" <?php if(isset($_REQUEST['isApp'])){ ?> style='display:none;' <?php } ?> ></div>
<!--				<div id='movie-name' class="movie-name">
					<a href="<?php echo 'https://'.$studio->domain.'/'.$movieData['content_permalink'].'/'.$movieData['permalink'];?>" target="_blank">
						<?php if($movieData['content_types_id']==3){
							echo $movieData['name']." - ". ($movieData['episode_title']?$movieData['episode_title']:'Episode#'.$movieData['episode_number']);
						}else{
							 echo $movieData['name'];
						}
						?>
					</a>
				</div>-->
            </div> 
            <?php if($play_percent != 0 && $play_length != 0 && @$_REQUEST['isApp'] == 2) { ?>
                    <div class="modal fade" id="play_confirm" tabindex="-1" role="dialog" >
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel" style="color:#000;"> <?php echo $translateLanguage['resume_watching']; ?></h4>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-danger" id="confirm_yes" type="button" onclick="FromDurationPlayed()"><?php echo $translateLanguage['yes']; ?></button>
                                    <button data-dismiss="modal" class="btn btn-default" type="button" onclick="FromBeginning()"><?php echo $translateLanguage['btn_cancel']; ?></button>
                                </div>  
                            </div>
                        </div>
                    </div>
            <?php } ?>
        </center>
        <style>
            .play-btn{
                background: url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png") no-repeat scroll 0 0;
                bottom: 0;
                height: 60px;
                left: 0;
                margin: auto;
                position: absolute;
                right: 0;
                top: 0;
                width: 60px;
                cursor: pointer;
            }
			.movie-name{position: absolute;top:0;left: 0;right: 0;}
			.movie-name a{color: #fff;}
        </style>
        <script>
            var nativeVideoControl = document.getElementById("video_block");
            nativeVideoControl.removeAttribute("controls");   
            var myVideoNativeControl = document.getElementsByTagName('video')[0];
            myVideoNativeControl.removeAttribute("controls");  
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = false;
            var movie_id = "<?php echo @$video_log_data['movie_id']; ?>";
            var stream_id = "<?php echo @$video_log_data['stream_id']; ?>";
            var studio_id = "<?php echo $studio_id ?>";
            var full_movie = "<?php echo $fullmovie_path ?>";
            var user_id = "<?php echo $user_id ?>";
            var can_see = "1";
            var back_url = "";
            var is_mobile =  "";
            var player = "";
            var percen = 5;
            var p_time = 60;
            var play_length = 0;
            var play_status = 0;
            var load_status = 0;
            
            $(document).ready(function () {
                $('#video_block,.play-btn').hover(function(){
                        $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/red_play_icon.jpg")');
                },function(){
                        $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png")');
                });
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });
                var full_movie = "<?php echo $fullmovie_path ?>";
                var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/user/getNewSignedUrlForEmbeded";
                var wiki_data = "<?php echo isset($wiki_data) ? $wiki_data : '' ?>";
                var multipleVideoResolution = new Array();
                multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
                var defaultResolution = "<?php echo $defaultResolution; ?>";
                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                var adavail = 0;
                var previousBufferEnd =0;
                var bufferDurationaas = 0;
                var forChangeRes = 0;
                var bufferenEnddd = 0;
                var bufferenEndd = 0;
                var videoOnPause = 0;
                var nAgt = navigator.userAgent;
                var browserName  = navigator.appName;
                var verOffset;
                var device_id = document.getElementById('device_id').value;
                var device_type = document.getElementById('device_type').value;
                // In Opera 15+, the true version is after "OPR/" 
                if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
                 browserName = "Opera";
                }
                // In older Opera, the true version is after "Opera" or after "Version"
                else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
                 browserName = "Opera";
                }
                // In MSIE, the true version is after "MSIE" in userAgent
                else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
                 browserName = "Microsoft Internet Explorer";
                }
                // In Chrome, the true version is after "Chrome" 
                else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
                 browserName = "Chrome";
                }
                // In Safari, the true version is after "Safari" or after "Version" 
                else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
                 browserName = "Safari";
                }
                // In Firefox, the true version is after "Firefox" 
                else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
                 browserName = "Firefox";
                }
                if(full_movie != ''){
                var playerSetup = videojs('video_block',{plugins: {resolutionSelector: {
                        force_types: ['video/mp4'],
                        default_res: "<?php echo $defaultResolution; ?>"
                    }}});
                    
                playerSetup.ready(function () {
                            player = this;
                            this.progressTips();
                $('#video_block').append('<img src="/images/touchpause.png" id="pause_touch" />');
                $('#video_block').append('<img src="/images/touchplay.png" id="play_touch" />');
                $('#video_block').append('<img src="/images/pause.png" id="pause"/>'); 
                $('#video_block').append('<img src="/images/play-button.png" id="play" />');
                            <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)){ ?>
                                 $('.vjs-loading-spinner').hide();   
                            <?php } if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ ?>
                                $('.vjs-loading-spinner').attr('style','-webkit-animation: spin 1.5s infinite linear;-moz-animation: spin 1.5s infinite linear; -o-animation: spin 1.5s infinite linear;animation: spin 1.5s infinite linear');
                                $('.vjs-loading-spinner').show();
                              <?php  if((isset($_REQUEST['device_type'])) && ($_REQUEST['device_type'] == 4)){ ?>
                                    //IF ANDROID TV- To remove an unknown image before video loaded in Android TV
                                        $('.vjs-loading-spinner').show();
                                        $('.vjs-loading-spinner').css('z-index','100');
                                        $('.vjs-default-skin .vjs-control-bar').css('z-index','100');
                                        $("<div class='temp-bg'></div>").appendTo(".videocontent");
                                        $(".vjs-default-skin .vjs-menu-button .vjs-menu .vjs-menu-content").css("right","-20px");
                                        $(".vjs-default-skin .vjs-menu-button .vjs-menu .vjs-menu-content").css("left","initial");
                                <?php } 
                            } ?>
                             <?php 
                                if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ 
                                   if((isset($_REQUEST['device_type'])) && ($_REQUEST['device_type'] == 4)){  ?>
                                                player.on('play', function(){
                                                $('.vjs-loading-spinner').hide(); 
                                               });
                              <?php  } 
                            } ?>
                                <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)){ ?>
                                    player.on('play', function(){
                                    $('.vjs-loading-spinner').hide(); 
                                   });   
                               <?php } ?>
                            $(".vjs-fullscreen-control").click(function () {
                                $(this).blur();
                            });
                        <?php if((isset($_REQUEST['isApp']) && ($_REQUEST['isApp'] == 2))){ 
                                if(@$_REQUEST['device_type']==3){
                        ?>
                            $('.vjs-fullscreen-control').hide();
                                <?php } 
                                
                                if(@$_REQUEST['device_type'] == 2){
                                        ?>
                            $(".vjs-fullscreen-control").on('touchend', function(event) {
                                full_screen = player.isFullscreen();
                                if (full_screen === false) {
                                    $("#video_block").removeClass("vjs-fullscreen");
                                    full_screen = false;
                                      Android.webViewFullscreenExit();
                                } else {
                                    $("#video_block").addClass("vjs-fullscreen");
                                    full_screen = true;
                                    Android.webViewFullscreen();
                                }
                            });
                                <?php } ?>
                            var thisIframesHeight = $( window ).height();
                            $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                            $( ".vjs-control-bar" ).attr('style','font-size:12px;');
                            $( ".vjs-text-track-display" ).attr('style','font-size:12px;');
                            $( ".vjs-current-time" ).attr('style','margin-left:10px;');
                            <?php //if(!empty($subtitleFiles)){ ?>
                                //$( ".vjs-subtitles-button" ).attr('style','margin-right:10px;');
                            <?php //} ?>
                            $( window ).on( "resize", function( event ) {
                                var thisIframesHeight = $(window).height();
                                setTimeout(function () {
                                    $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                                }, 0);
                            });
                        <?php } else { ?>
                            if($(document).height() != null){
                                var thisIframesHeight = $(document).height();
                                $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                            } else{
                                $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
                            }
                        <?php } ?>
                            $(".vjs-big-play-button").hide();
                            player.on('error', function () {
                                $(".vjs-error-display").removeAttr('style');
                                if(played_button == 0){
                                    $(".vjs-error-display").attr('style','display:none; ');
                                }else if($('.vjs-modal-dialog-content').html()!==''){
                                    $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source+"</div>");
                                } else {
                                if(document.getElementsByTagName("video")[0].error != null){
                                    var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                                    if (videoErrorCode === 2) {
                                        if(seekStart === null){
                                            var currTim = currentTime;
                                            seekStart = 123;
                                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim);
                                        }
                                        $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                                    }else if (videoErrorCode === 3) {
                                        $(".vjs-error-display").html("<div>"+the_video_playback_was_aborted+"</div>");
                                    } else if (videoErrorCode === 1) {
                                         $(".vjs-error-display").html("<div>"+you_aborted_the_video_playback+"</div>");
                                    } else if (videoErrorCode === 4) {
                                          $(".vjs-error-display").html("<div>"+the_video_could_not_be_loaded+"</div>");
                                    } else if (videoErrorCode === 5) {
                                        $(".vjs-error-display").html("<div>"+the_video_is_encrypted+"</div>");
                                    }
                                }
                              }
                            });
                            <?php if(!isset($_REQUEST['isApp']) || (isset($_REQUEST['isApp']) && $_REQUEST['isApp'] == 2)) { ?>
                                $("#video_block_html5_api").attr('poster', item_poster);
                                $("#video_block").attr('poster', item_poster);
                            <?php } ?>
                            if($("div.vjs-subtitles-button").length)
                            {
                                var divtagg = $(".vjs-subtitles-button .vjs-control-content .vjs-menu .vjs-menu-content li:first-child");
                                var divtaggVal = divtagg.html();
                                if(divtaggVal === 'subtitles off'){
                                    divtagg.html("Subtitles Off");
                                }
                            }
                            $(".vjs-tech").mousemove(function () {
                                if (full_screen === true && show_control === false) {
                                    $("#video_block .vjs-control-bar").show();
                                    show_control = true;
                                    var timeout = setTimeout(function () {
                                        if (full_screen === true) {

                                        }
                                    }, 10000);
                                    $(".vjs-control-bar").mousemove(function () {
                                        event.stopPropagation();
                                    }).mouseout(function () {
                                        event.stopPropagation();
                                    });
                                } else {
                                    clearTimeout(timeout);
                                }
                            });

                            player.on("fullscreenchange", resize_player);
                            <?php if($v_logo != ''){ ?>
                                player.watermark({
                                    file: "<?php echo $v_logo; ?>",
                                    xrepeat: 0,
                                    opacity: 0.75
                                });
                                if (is_mobile !== 0) {
                                    $(".vjs-watermark").attr("style", "bottom:40px;right:1%;width:7%;");
                                }else{
                                    $(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;");
                                }
                            <?php } ?>
                            $("#vid_more_info").hide();
                            
                            <?php
                                if((isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] == 1) || ($_REQUEST['isApp'] == 2))){
                                    if($user_id > 0){
                                        if($play_percent != 0 && $play_length != 0 && @$_REQUEST['isApp'] == 2) { 
                            ?>
                                            if(play_status == 0){
                                                player.pause();
                                                play_status = 1;
                                                $('#play_confirm').modal('show');
                                                $(document).bind('keydown', function(e) {
                                                    if (e.keyCode === 13) {
                                                         $('#confirm_yes').trigger('click');      
                                                    }
                                                });
                                            }
                            <?php 
                                        } 
                                    } 
                            ?>
                            <?php 
                                if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ 
                                   if((isset($_REQUEST['device_type'])) && ($_REQUEST['device_type'] == 4)){ ?>
                                        $(".vjs-res-button").show();
                                 <?php  } 
                            } ?>
                                $('#play-btn').remove();
                                $(".vjs-res-button").hide();
                                $(".vjs-volume-control").hide();
                                $(".vjs-mute-control").hide();
                                <?php  if($_REQUEST['isApp'] == 1){ ?>
                                $(".vjs-fullscreen-control").hide();
                                <?php } else if($_REQUEST['isApp'] == 2){
                                    if($play_percent == 0 && $play_length == 0) { 
                                    ?> 
                                    player.load();
                                    <?php } ?>
                                    $(".vjs-fullscreen-control").click(function(){
                                        full_screen = player.isFullscreen();
                                        if (full_screen === false) {
                                             $("#video_block").addClass("vjs-fullscreen");
                                             full_screen = true;
                                        }else{
                                             $("#video_block").removeClass("vjs-fullscreen");
                                             full_screen = false;
                                        }
                                     });
                                <?php } 
                                    if($play_percent == 0 && $play_length == 0 && @$_REQUEST['isApp'] == 2) { 
                                ?>
                                player.play();
                                <?php } ?>
                                $(".vjs-control-bar").show();
                            <?php 
                                } else{ 
                            ?>
                                $('#play-btn').on('click',function(){
                                    played_button = 1;
                                    if (full_movie.indexOf('https://youtu') > -1) {
                                        player.src({type: "video/youtube", src: "https://youtu.be/32xWXN6Zuio"});
                                        player.play();
                                    }else{
                                        $(".vjs-error-display").addClass("hide");
                                        $('#play-btn').remove();
                                        $('.vjs-loading-spinner').attr('style','animation :spin 1.5s infinite linear');
                                        $('.vjs-loading-spinner').show();
                                        if (typeof player.getCurrentRes === "function") {
                                            //Bypass particular studio without checking internetspeed
                                          if(studio_id==='4302'){                                                
                                                if (is_mobile !== 0) {
                                                    player.play();
                                                    player.pause();
                                                }
                                                player.changeRes(<?php echo $setNewResolution;?>);
                                                player.play();
                                                $(".vjs-control-bar").show();                                                
                                                player.on("loadeddata",function(){
                                                    $('.vjs-loading-spinner').removeAttr('style','animation :spin 1.5s infinite linear'); 
                                                });
                                          }else{
                                            var imageAddr = [], downloadSize = [], desc = []
                                            imageAddr[0] = "<?php echo $internetSpeedImage; ?>";
                                            downloadSize[0] = 1036053; //bytes
                                            desc[0] = "Singapore S3 Bucket";
                                            var startTime, endTime;
                                            var changeUrlAccInternetSpeed = "<?php echo Yii::app()->baseUrl; ?>/user/getNewSignedUrlForInternetSpeedEmb";
                                            for (var i = 0; i < desc.length; i++) {
                                                if (is_mobile !== 0) {
                                                    player.play();
                                                    player.pause();
                                                }
                                                console.log('start');
                                                var download = new Image();
                                                download.i = i;
                                                download.onload = function () {
                                                console.log('end');
                                                    endTime = (new Date()).getTime();
                                                    var duration = (endTime - startTime) / 1000;
                                                    var bitsLoaded = downloadSize[this.i] * 8;
                                                    var speedBps = (bitsLoaded / duration).toFixed(2);
                                                    var speedKbps = (speedBps / 1024).toFixed(2);
                                                    var speedMbps = (speedKbps / 1024).toFixed(2);
                                                    $.post(changeUrlAccInternetSpeed, {multiple_video_url: multipleVideoResolution,user_internet_speed: speedMbps}, function (res) {
                                                        res = res.trim();
                                                        
                                                        if(defaultResolution === res){
                                                            console.log('same');
                                                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, 0);
                                                        }else{
                                                            console.log('new');
                                                            player.changeRes(res);
                                                        }
                                                        $(".vjs-control-bar").show();
                                                        player.on("loadeddata",function(){
                                                            $('.vjs-loading-spinner').removeAttr('style','animation :spin 1.5s infinite linear'); 
                                                        });
                                                        console.log(res);
                                                    });
                                                }
                                                download.onerror = function (err, msg) {
                                                    oProgress.innerHTML = "Invalid image, or error downloading";
                                                }
                                                startTime = (new Date()).getTime();
                                                download.src = imageAddr[i] + "?nnn=" + startTime;
                                            } 
                                          }
                                        } else {
                                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, 0);
                                        }  
                                    }
                                });
                            <?php        
                                }
                            ?>
                            player.on("play", function () {
                                $('.play-btn').remove();
                                if (is_mobile !== 0) {
                                    $(".vjs-control-bar").show();
                                }
                                videoOnPause = 0;
                            });

                            player.on("pause", function () {
                                videoOnPause = 1;
                            });
                            <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ ?>
                            player.on('mousemove', function(){
                                player.userActive(true);
                            });
                            <?php } ?>
                            player.on('timeupdate', function () {
                                <?php 
                                    if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ 
                                        if((isset($_REQUEST['device_type'])) && ($_REQUEST['device_type'] == 4)){ ?>
                                           //IF ANDROID TV
                                           if(player.currentTime() > 0)
                                                $('.temp-bg').remove();
                                              
                                            if($('.vjs-play-control').hasClass('vjs-paused')){
                                               player.play();                                    
                                            }
                                            if(currentTime > 1){ 
                                              //Hide loader after seeking complete
                                              if( player.currentTime() > $("#prevSeekingTime").val()){
                                                  $('.vjs-loading-spinner').hide();
                                              }
                                            }
                                <?php }
                                } ?>
                                previousTime = currentTime;
                                currentTime = player.currentTime();
                                <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ ?>
                                if(currentTime <=1){
                                    if(currentTime >= 0.5){ 
                                        $('.vjs-loading-spinner').hide();
                                    }
                                }
                                <?php } ?>
                                previousBufferEnd = bufferDurationaas;
                                var r = player.buffered();
                                var buffLen = r.length;
                                buffLen = buffLen - 1;
                                bufferDurationaas = r.end(buffLen);
                                var curlength = player.currentTime();
                                if(p_time < curlength)
                                {
                                    p_time = parseInt(p_time)+60;
                                    var buff_log_id = document.getElementById('buff_log_id').value;
                                    updateBuffered(player,curlength,buff_log_id);
                                }
                                
                            });
                            
                            
                            //video log start
                            var started = 0;
                            var ended = 0;
                            var logged = 0;
                            var log_id = 0;
                            var adavail = 0;
                            
                            var play_time = 0;
                            
                            //Player ended...
                            player.on("ended", function() {
                                if (ended === 0 && (adavail === 0 || adavail === 2)) {
                                    var buff_log_id = document.getElementById('buff_log_id').value;
                                    updateBuffered(player,player.currentTime(),buff_log_id);
                                    ended = 1;
                                    started = 0;
                                    play_status = 1;
                                }
                            });
                            var videotimeLogged = 0; 
                            
                            //video log End
                            
                            player.on('changeRes', function () {
                                console.log('change');
                                forChangeRes = 123;
                                seekStart = previousTime;
                                var currTim = previousTime;
                                createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim);
                                forChangeRes = 0;
                            });
                            player.on("progress",function(){
                                if(is_mobile === 0 && videoOnPause === 0 && browserName=== "Safari" ){
                                    //checkBuffer(player);
                                }
                            });                  
                            player.on('loadedmetadata', function() {
                                var duration = player.duration();
                                $('#full_video_duration').val(duration);
                                if (typeof player.getCurrentRes === "function") {
                                    var video_resolution = player.getCurrentRes();
                                } else {
                                    var video_resolution = 144;
                                }
                                $('#full_video_resolution').val(video_resolution);
                                buffered_loaded();
                                //focefully-show-subtitles
                                if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                                {
                                    $('.vjs-subtitles').removeAttr("style");
                                    $('.vjs-text-track').removeAttr("style");
                                    $('.vjs-tt-cue').removeAttr("style");
                                    $('.vjs-text-track').attr("style","display:block");
                                    }
                            });
                            $("video").on("seeking", function () {
                                <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)){ ?>
                                        if(load_status != 1){
                                            $('.vjs-loading-spinner').show(); 
                                            load_status = 1;
                                        }
                                <?php } if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){
                                        if((isset($_REQUEST['device_type']) && ($_REQUEST['device_type'] == 4))){ ?>
                                          $("#prevSeekingTime").val(player.currentTime());
                                          $('.vjs-loading-spinner').show();
                                        <?php }
                                     } 
                                ?>                                                
                                var currTimmm = previousTime;
                                var currTim = player.currentTime();
                                console.log(previousBufferEnd + "-" + currTimmm);
                                console.log( bufferDurationaas + "-" + currTim);
                                if (forChangeRes === 0) {
                                    if (previousBufferEnd < currTim) {
                                        if (seekStart === null) {
                                            console.log("sd");
                                            seekStart = previousTime;
                                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim);
                                        }
                                    }
                                }
                                //focefully-show-subtitles
                                if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                                {
                                    $('.vjs-subtitles').removeAttr("style");
                                    $('.vjs-text-track').removeAttr("style");
                                    $('.vjs-tt-cue').removeAttr("style");
                                    $('.vjs-text-track').attr("style","display:block");
                                    }
                            });

                            $("video").on('seeked', function () {
                                    seekStart = null;
                            });
                            <?php
                             if((!isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] != 1) || ($_REQUEST['isApp'] != 2))){ ?>
                                if(is_mobile === 0){
                                    $('#video_block_html5_api').on('click',function(e){                                    
                                        if(played_button == 1){
                                            e.preventDefault();
                                            if (player.paused()){
                                                player.play();
                                                $('#pause').show(); 
                                                $('#play').hide();
                                            } else{
                                                player.pause();
                                                $('#play').show(); 
                                                $('#pause').hide(); 
                                            }
                                        }
                                    });
                                }else{
                                     player.on("play", function () {
                                         videoOnPause = 0;
                                         $('#play_touch').hide();
                                         $('#pause_touch').hide();
                                     });
                                     player.on("pause", function () {
                                         videoOnPause = 1;
                                         $('#play_touch').show();
                                         $('#pause_touch').hide();
                                     });
                                     $('#pause_touch').bind('touchstart',function(e){
                                            player.pause();
                                     });
                                     $('#play_touch').bind('touchstart',function(e){
                                            player.play();
                                     });
                                     $('#video_block_html5_api').bind('touchstart',function(e){ 
                                        if(played_button == 1){
                                            if(player.play()){
                                                $('#pause_touch').show(); 
                                                $('#play_touch').hide(); 
                                                setTimeout(function(){
                                                $('#pause_touch').hide();   
                                                 },3000); 
                                            }
                                        }
                                    });
                                } 
                            <?php } ?>                             
                    });
                }
            });

            function createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTimess){
                <?php
                    if((isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] == 1) || ($_REQUEST['isApp'] == 2))){
                ?>     
                    return false;
                <?php
                    }
                ?>
                var s3bucket_id = "<?php echo isset($studio->s3bucket_id) ? $studio->s3bucket_id : '' ?>";
                $(".vjs-error-display").addClass("hide");
                if (typeof player.getCurrentRes === "function") {
                    var currentVideoResolution = player.getCurrentRes();
                } else {
                    var currentVideoResolution = 144;
                }
                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                var isAjax = 1;
                if(currentTimess === 0){
                    isAjax = 2;
                }
                $.ajax({
                    url : createSignedUrl,
                    type: 'POST',
                    async: false,
                    data: {video_url: videoToBePlayed, wiki_data: wiki_data,s3bucket_id : s3bucket_id,studio_id:<?php echo $studio_id ?>,isAjax:isAjax},
                    success : function(res) {                        
                        if(res){
                    player.currentTime(currentTimess);
                            player.src(res);
                            player.load();
                            player.play();
                    $(".vjs-error-display").removeClass("hide");
                            //Added by prakash on 28th Dec 2016
                            $(".vjs-control-bar").show();
                            $('.vjs-loading-spinner').hide();
                    player.on("loadeddata", function () {
                    console.log(currentTimess);
                        player.currentTime(currentTimess);
                        //focefully-show-subtitles
                        if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                        {
                            $('.vjs-subtitles').removeAttr("style");
                            $('.vjs-text-track').removeAttr("style");
                            $('.vjs-tt-cue').removeAttr("style");
                            $('.vjs-text-track').attr("style","display:block");
                            }
                    });
                        }
                    },
                    error: function() {
                        
                    }
                });
            }
            function resize_player() {
                if (full_screen === false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen === true) {
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                }
            }
            // If Safari browser
            function checkBuffer(player){
                var currentTime = player.currentTime();
                var playerDuration = player.duration();
                var r = player.buffered();
                var buffLen = r.length;
                buffLen = buffLen - 1;
                var bufferenEnddd = r.end(buffLen);
                var bufferenEndd = bufferenEnddd - 1;
                if(bufferenEndd < currentTime){
                    if(playerDuration !== bufferenEnddd){
                        player.pause();
                    }else{
                        player.play();
                    }
                }else{
                    if(bufferenEnddd !== currentTime){
                        if(player.paused()){
                            player.play();
                        }
                    }
                }
            }

            function get_infoimg_height(percent) {
                var info_height = $("#vid_more_info").css("height");
                var img_px = parseInt(info_height) * (percent / 100);
                return img_px + "px";
            }

            function get_infoimg_width(percent) {
                var info_width = $("#vid_more_info").css("width");
                var img_px = parseInt(info_width) * (percent / 100);
                return img_px + "px";
            }
            function backto_info() {
                var trailer = videojs("trailer_block");
                trailer.pause();
                $("#trailer_div").hide();
                $("#vid_more_info").show();
            }

            function close_info() {
                $("#vid_more_info").hide();
                $("#wannasee_block").hide();
                //$(".random_msg").hide();
            }
            function handelAndroidAppPlay(){
                if(player.paused()){
                    player.play();
                } else{
                    player.pause();
                }
            }
            function handelAndroidAppForward(forward){
                var currentTime = player.currentTime();
                if (forward === undefined) {
                    currentTime = currentTime -10;
                    if(currentTime > 0){
                        player.currentTime(currentTime);
                    }else{
                        player.currentTime(0);
                    }

                } else{
                    currentTime = currentTime +10;
                    var playerFullDuration  = player.duration();
                    if(currentTime < playerFullDuration){
                        player.currentTime(currentTime);
                    }else{
                        player.currentTime(playerFullDuration);
                    }
                }
            }
            function handelAppUserActive(){
                player.userActive(true);
            }
            function pausePlayer(){
                player.pause();
            }
            function screenRotationForWebView(androidonRotation){
                if (androidonRotation === 0) {
                    $("#video_block").removeClass("vjs-fullscreen");
                    full_screen = false;
                } else {
                    $("#video_block").addClass("vjs-fullscreen");
                    full_screen = true;
                }
            }
            function FromDurationPlayed() {  
                percen = <?php echo $play_percent;?> ;
                play_length = <?php echo $play_length;?> ; 
                player.load();
                player.on("loadeddata", function () {
                    player.currentTime(<?php echo $play_length;?>);
                    player.play();
                    //focefully-show-subtitles
                    if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                    {
                        $('.vjs-subtitles').removeAttr("style");
                        $('.vjs-text-track').removeAttr("style");
                        $('.vjs-tt-cue').removeAttr("style");
                        $('.vjs-text-track').attr("style","display:block");
                        }
                });
                play_status = 0;
                $('.modal').hide();
            }

            function FromBeginning() {
                percen = 5 ;
                play_length = 0 ;
                player.play();
                play_status = 0;
                $('.modal').hide();     
            }
        </script>
    <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => @$movieData['stream_id'], 'movie_id' => @$movieData['id'],'studio_id' => @$movieData['studio_id'])); ?>
    </body>
</html>