<?php
$normal_content = "Following are new content on " . $this->studio->domain . ". Enjoy!";
$studio_mail = ($this->studio->marketing_from_email == '') ? 'info@' . $this->studio->domain : $this->studio->marketing_from_email;
?>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#content",
            formats: {
                        bold: {inline: 'b'},  
                    },
        valid_elements : '*[*]',
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : false,
        menubar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor imageupload"
        ],
        extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image imageupload | code",
        setup: function(editor) {
            editor.on('init', function() {
                if (tinymce.editors.length) {
                    $("#content_ifr").removeAttr('title');
                }
            });
        }
    });

    $(document).ready(function(res) {
        //$('#frm_grp').hide();
        $('#movie_name').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: HTTP_ROOT + '/admin/mix_autocomplete?term=' + $('#movie_name').val(),
                    dataType: "json",
                    success: function(data) {
                        response($.map(data.movies, function(item) {
                            return {
                                label: item.movie_name,
                                value: item.movie_id,
                                ms_id: item.movie_stream_id
                            }
                        }));
                    }

                });
            },
            select: function(event, ui) {
                event.preventDefault();
                $("#movie_name").val(ui.item.label);
                $("#movie_id").val(ui.item.value);
                $("#movie_stream_id").val(ui.item.ms_id);
                $('#snd_submit').attr('disabled', true);
                addmovie_block();

            },
            focus: function(event, ui) {
                $("#movie_name").val(ui.item.label);
                $("#movie_id").val(ui.item.value);
                $("#movie_stream_id").val(ui.item.ms_id);
                event.preventDefault(); // Prevent the default focus behavior.
            }
        });

        $('#mail_type').change(function() {
            $('#notif_error').html('');
            $('#notif_error').hide();
            $("#has_attachment_div").val(0).hide();

            if ($.trim($(this).val()) === 'ppv_subscription' || $.trim($(this).val()) === 'advance_purchase') {
                $("#has_attachment_div").show();
            }
            if ($(this).val() != 'new_content')
            {
                $.ajax({
                    url: HTTP_ROOT + '/admin/notificationtemplate?type=' + $(this).val(),
                    dataType: "json",
                    success: function(data) {
                        $('#send_to').val('reg_users');
                        //$('#send_to').attr('disabled', 'disabled');
                        $('#subject').val(data.subject);
                        $('#from_email').val(data.send_from);
                        if ($.trim(data.content)) {
                            tinyMCE.get('content').setContent(data.content);
                        } else {
                            tinyMCE.get('content').setContent('');
                        }

                        if (parseInt(data.has_attachment)) {
                            $("#has_attachment").prop('checked', 'checked');
                            $("#has_attachment").val(1);
                        }

                        var i = "";
                        var res = data.result;

                        var show = '';
                        if ($.trim(res)) {
                            show += '<div class="col-md-12"><div class="col-md-6 fnt-bold">Variable</div><div class="col-md-6 fnt-bold">Meaning</div></div>';
                            for (var prop in res) {
                                show += '<div class="col-md-12"><div class="col-md-6">' + prop + '</div><div class="col-md-6">' + res[prop] + '</div></div>';
                            }
                        }

                        var JSONObject = [data.result];
                        var JSONString = JSON.stringify(JSONObject);
                        if ($.trim(JSONString)) {
                            $("#variables_div").show();
                            $("#all_variables").html(show);
                        } else {
                            $("#all_variables").html('');
                            $("#variables_div").hide();
                        }
                        $("input").on("click", function() {
                            $("#snd_submit").html($("input:checked").val());
                        });
                        $('#nc').hide();
                        $('#frm_grp').show();

                    }
                });
            }
            else
            {
                $('#send_to').attr('disabled', false);
                $('#from_email').val('<?php echo $studio_mail ?>');
                $('#snd_submit').val('Send Mail');
                tinyMCE.get('content').setContent('<?php echo $normal_content; ?>');

                $("#all_variables").html('');
                $("#variables_div").hide();
                $('#nc').show();
                $('#frm_grp').hide();
            }
        });
    });
    function addmovie_block() {
        var movie_id = $("#movie_id").val();
        var movie_stream_id = $("#movie_stream_id").val();
        if (movie_id != "") {
            var url = HTTP_ROOT + '/admin/movie_block';
            $.get(url, {movie_stream_id: movie_stream_id, movie_id: movie_id}, function(res) {
                $("#movie_block").append(res);
                var existing_ids = $("#content_id").val();
                $("#content_id").val(existing_ids + movie_stream_id + ",");
                $("#movie_name").val("");
                $("#movie_id").val("");
                $("#movie_stream_id").val("");
            }).done(function() {
                $('#snd_submit').attr('disabled', false);
            });
        }
    }
    function populate_data() {
        var mail_type = $("#mail_type").val();
        var studio_name = "<?php echo $studio_name; ?>";
        if (mail_type == "new_content") {
            $("#send_to option[value='reg_users']").attr("selected", "selected");
            $("#subject").val("New on " + studio_name);
            $("#content").val("Following are new content on " + studio_name + ". Enjoy!");
        }
        if (mail_type != "") {
            $('.mail_type_error').html('');
            $('#snd_submit').html('Save');
        }
        if (mail_type == "announcement") {
            $('#snd_submit').html('Send');
        }
    }
    $(document).ready(function() {
        $('#notif_error').hide();
        $('#snd_submit').click(function() {
            var text = tinyMCE.activeEditor.getContent({
                format: 'text'
            });
            if ($('#mail_type').val() == "") {
                $('#notif_error').show();
                $('.mail_type_error').html("Please select mail type!");
                return false;
            }
            if ($.trim(text) === '') {
                $('#notif_error').show();
                $('.content_type_error').html("Please add content!");
                return false;
            }

            if ($('#mail_type').val() == 'new_content' && $('#content_id').val() == '') {
                $('#notif_error').show();
                $('.content_type_error').html("Please add content!");
                return false;
            } else {
                $('#notif_error').html('');
                $('#notif_error').hide();
                if ($('#mail_type').val() == "announcement") {
                    $.ajax({
                        url: HTTP_ROOT + '/admin/checkforannouncement',
                        dataType: "json",
                        success: function(data) {
                            if (data.success) {
                                $('#email_noti').submit();
                                return true;
                            } else {
                                $("#announcement_modal").modal('show');
                                $(".announcement_exceed_msg").html(data.msg);
                            }
                        }
                    });
                } else {
                    $('#email_noti').submit();
                    return true;
                }
            }
        });
    });
</script>

<div class="row m-t-40 m-b-40">
    <div class="col-xs-10">
        <form role="form" class="form-horizontal" method="post" data-toggle="validator" id="email_noti" action="<?php echo $this->createUrl('admin/send_emailnoti'); ?>">            
            <input type="hidden" name="mail_type" id="mail_type_hidden" value="new_content" />
            <div class="form-group" id="user_action">
                <label for="send_to" class="col-md-4 control-label">User Action:</label>
                <div class="col-md-8">
                    <div class="fg-line">
                        <div class="select">
                            <select name="mail_type" id="mail_type" class="form-control input-sm" required onchange="populate_data();">
                                <option value="">Select</option>
                                <option value="welcome_email">Welcome on Registration</option>
                                <option value="forgot_password">Forgot Password</option>
                                    
                                <option value="free_user">Free User</option>
                                <option value="contact_us">Contact Us</option>
                                <?php if (Yii::app()->common->isPaymentGatwayExists($this->studio->id)) { ?>
                                    <option value="onetime_payment_failed">Payment Failed</option>
                                <?php } ?>
                                <?php if (Yii::app()->common->isPaymentGatwayAndPlanExists($this->studio->id)) { ?>
                                    <option value="welcome_email_with_subscription">Subscription - Registration & Purchase Subscription</option>
                                    <option value="membership_subscription">Subscription - Purchase subscription</option>
                                    <option value="subscription_cancellation_user">Subscription - Cancel subscription by user</option>
                                    <option value="subscription_cancellation">Subscription - Cancel subscription by admin</option>
                                    <option value="subscription_reactivate">Subscription - Reactive subscription</option>
                                    <option value="subscription_renew">Subscription - Recurring Billing</option>
                                    <option value="payment_failed">Subscription - Failure to pay</option>
                                    <option value="first_reminder_card_fail">Subscription - 1st reminder for card failure</option>
                                    <option value="second_reminder_card_fail">Subscription - 2nd reminder for card failure</option>
                                    <option value="cancellation_card_fail">Subscription - Cancellation for card failure</option>
                                    <?php if(Yii::app()->common->isSubscriptionBundlesExists($this->studio->id)){?>
                                        <option value="subscription_bundles">Subscriptions Bundles </option>
                                    <?php }?>    
                                <?php } ?>

                                <?php if (Yii::app()->common->isPaymentGatwayAndPPVPlanExists($this->studio->id, 0)) { ?>
                                    <option value="ppv_subscription">Pay Per View purchase</option>
                                <?php } ?>
                                <?php if (Yii::app()->common->isPaymentGatwayExists($this->studio->id)) { ?>
                                    <option value="advance_purchase">Pre-Order</option>
                                    <option value="advance_purchase_content_available">Pre-Order - Content is available</option>
                                <?php } ?>
                                <?php
                                $pgconfig = Yii::app()->general->getStoreLink();
                                if ($pgconfig) {
                                    ?>
                                    <option value="mk_after_order_placed">Muvi Kart - After Order is placed</option>
                                    <option value="mk_after_order_cancelled">Muvi Kart - After Order is cancelled</option>                                        
                                    <option value="mk_after_order_out_shipping">Muvi Kart - After Order is out for shipping</option>
                                       
                                    <?php } ?>
                                    <?php if(Yii::app()->common->voucherAvailable($this->studio->id)){?>
                                        <option value="voucher">Voucher Redemption </option>
                                    <?php }?>
                                    
										<?php if($this->studio->id != 2930){?>
                                <option value="announcement">Announcement</option>
									<?php } ?>	
                            </select>
                        </div>
                    </div>
                    <label class="mail_type_error red"></label>
                </div>
            </div>
            <div class="form-group">
                <label for="subject" class="col-md-4 control-label">Email Subject:</label>
                <div class="col-md-8">
                    <div class="fg-line">
                        <textarea name="subject" id="subject" rows="1" placeholder="Subject of Email" class="form-control input-sm textarea" required></textarea>
                    </div>
                    <label class="subject_error red"></label> 
                </div>
            </div>

            <div class="form-group">
                <label for="content" class="col-md-4 control-label">Email Content:</label>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:grey;"><i>(Copy/Paste HTML here)</i></span>
                <div class="col-md-8">
                    <div class="fg-line">
                        <textarea name="content" id="content" rows="10" placeholder="Content of Email" class="form-control input-sm textarea" required></textarea>
                    </div>
                    <label class="content_type_error red"></label>
                    <input type="hidden" name="content_id" id="content_id" value="">
                    <div class="row m-t-20" id="has_attachment_div" style="display: none;">

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="has_attachment" id="has_attachment" value="0" />
                                    <i class="input-helper"></i> Attach corresponding receipt 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-6  text-left">
                            <input type="button" class="btn btn-warning margin-bottom" data-toggle="modal" data-target="#exampleModal" value="System Variables">
                        </div>
                        <div class="col-md-6 text-right">
                            <input type="button" class="btn btn-success margin-bottom "  id="snd_test_submit" title="Send a test email to yourself" value="Send Test Email" />
                            <button type="button" class="btn btn-primary text-right" id="snd_submit" >Save</button>
                        </div>
                    </div>
                </div>
            </div>





        </form>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">
                    System Variables
                </h4>
            </div>
            <div class="modal-body">
                <h5 class="m-b-20"><span class="f-700">Note*:</span> <span class="red f-300">Please don't change the following variable names.</span></h5>

                <form>
                    <div class="form-group">
                        <label for="message-text" class="control-label">System Variables:</label>

                        <div class="col-md-12">
                            <div style="display: block;" id="variables_div">
                                <div id="all_variables" class="form-group"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="announcement_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button> 
                <h4 class="modal-title" id="myModalLabel">Oops!</h4>
            </div>
            <div class="modal-body announcement_exceed_msg"></div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div> 
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#btn1").on('click', function() {
            $('#notif_error').hide();
            $("#snd_submit").val("Send Now");
            if ($(this).is(':checked')) {
                $('#frm_grp').show();
                $('#user_action').hide();
                $('#send_to').show();
                $('#email_noti').trigger("reset");
                $('#mail_type_hidden').removeAttr('disabled', 'disabled');
                $('#mail_type').attr('disabled', 'disabled');
            } else {
                $('#user_action').show();
                $('#send_to').hide();
            }
        });
        $("#btn2").on('click', function() {
            $('#notif_error').hide();
            $("#snd_submit").val("Save");
            $('#email_noti').trigger("reset");
            $(this).prop('checked', 'checked');
            if ($(this).is(':checked')) {
                if ($('#mail_type').val() != "") {
                    $('#frm_grp').show();
                } else {
                    $('#frm_grp').hide();
                }
                $('#mail_type').removeAttr('disabled', 'disabled');
                $('#mail_type_hidden').attr('disabled', 'disabled');
                $('#user_action').show();
                $('#send_to').hide();
                //$('#email_noti').trigger("reset");
            } else {
                $('#user_action').hide();
                $('#send_to').show();
            }
        });

        function sendTestEmailNotification(subject, msg) {
            $.ajax({
                type: 'POST',
                url: "/admin/sendTestEmailNoti",
                data: {
                    msg: msg,
                    subject: subject
                },
                success: function(data) {
                    swal('Test email sent successfully!');
                },
                error: function(req, status, error) {
                    swal('Oops! Some error occurs please try again.');
                }
            });
        }

        $('#snd_test_submit').click(function() {
            var subject = $('#subject').val();
            var msg = tinymce.get('content').getContent();
            if (subject == '') {
                $('#notif_error').show();
                $('.subject_error').html("Subject can't be blank");
                return false;
            } else {
                $('#notif_error').hide();
                sendTestEmailNotification(subject, msg);
            }
        });
    });
</script>
<script>
    $(".but1").click(function() {
        $("#has_attachment_div").val(0).hide();
        var text = $(this).attr("data-text");
        $("#text_for_checkbox").html(text);
    });

    $("#has_attachment").click(function() {
        if ($(this).is(':checked')) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });


    $(document).ready(function() {
        $("#all_variables").html('');
        $.ajax({
            url: HTTP_ROOT + '/admin/notificationtemplate?type=' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#send_to').val('reg_users');
                $('#subject').val(data.subject);
                $('#from_email').val(data.send_from);
                if ($.trim(data.content)) {
                    tinyMCE.get('content').setContent(data.content);
                } else {
                    //tinyMCE.get('content').setContent('');
                }
                var i = "";
                var res = data.result;
                var show = '';

                var show = '';
                if ($.trim(res)) {
                    show += '<div class="row"><div class="col-md-6 f-500 p-l-0">Variable</div><div class="col-md-6 f-500 p-l-0">Meaning</div></div>';
                    for (var prop in res) {
                        show += '<div class="row"><div class="col-md-6 p-l-0">' + prop + '</div><div class="col-md-6 p-l-0">' + res[prop] + '</div></div>';
                    }
                }
                var JSONObject = [data.result];
                var JSONString = JSON.stringify(JSONObject);
                if ($.trim(JSONString)) {
                    $("#variables_div").show();
                    $("#all_variables").html(show);
                } else {
                    $("#all_variables").html('');
                    $("#variables_div").hide();
                }
                $("input").on("click", function() {
                    $("#snd_submit").html($("input:checked").val());
                });
            }
        });
    });
</script>