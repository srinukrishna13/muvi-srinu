<form class="form-horizontal" action="javascript: void(0)" name="save_meta_data" id="save_meta_data" method="post">
    <input type="hidden" id="content_type_xml" name="content_type_xml" value="<?php echo $content_type; ?>" />
    <input type="hidden" id="content_name_xml" name="content_name_xml" value="<?php echo $content_name; ?>" />
    <input type="hidden" id="mrss_feed_url_xml" name="mrss_feed_url_xml" value="<?php echo $mrss_feed_url; ?>" />
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="genre">Fields in MRSS</label>
                </div>
            </div>
             <div class="col-md-12">
                <div class="form-group fg-line">
                        <select name="available_stream" id="available_stream" class="left_stream form-control" multiple style="width: 100%; min-height: 200px; border:solid 1px #ddd;">
                        <?php
                        if(count($mediaAttributes) > 0)
                        {
                            foreach($mediaAttributes as $mediaAttributesKey => $mediaAttributesVal)
                            {


                                echo '<option value="'.$mediaAttributesKey.'">'.$mediaAttributesKey.'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1 text-center">
            <h4>&nbsp;</h4>
            <h4>&nbsp;</h4>
            <input type="button" class="btn btn-primary" value="&#61; " style="margin-bottom:5px;" />
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="genre">Fields in your content</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group fg-line">
                    <select id="selected_stream" name="selected_stream[]" class="right_stream form-control" multiple style="width: 100%; min-height: 200px; border:solid 1px #ddd;">
                        <?php
                         if($content_type)
                        {
                            $contTyp = explode(",",$content_type);
                            if($contTyp[1] ==  1){
                                ?>
                                    <option value="Content Name">Content Name</option>
                                    <option value="Release/Recorded Date">Release/Recorded Date</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Language">Language</option>
                                    <option value="Censor Ratings">Censor Ratings</option>
                                    <option value="Story/Description">Story/Description</option>
                                <?php
                            } else if ($contTyp[1] ==  2) {
                                ?>
                                    <option value="Content Name">Content Name</option>
                                    <option value="Release/Recorded Date">Release/Recorded Date</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Story/Description">Story/Description</option>
                                <?php
                            } else if ($contTyp[1] ==  3) {
                                ?>
                                    <option value="Title">Title</option>
                                    <option value="Story">Story</option>
                                    <option value="Episode Date">Episode Date</option>
                                <?php
                            }
                        }
                        ?>                            
                    </select> 
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-1 text-center">
            <h4>&nbsp;</h4>
            <h4>&nbsp;</h4>
            <input type="button" class="btn btn-primary add" value=" &gt;&gt; " style="margin-bottom:5px;" /><br />
        <input type="button" class="btn btn-default remove" value=" &lt;&lt; " /> 
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="genre">Fields mapping for import</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="fg-line">
                        <select id="selected_matadata" name="selected_matadata[]" required="true" class="form-control"  multiple style="width: 100%; min-height: 200px; border:solid 1px #ddd;">

                        </select> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12 m-t-30">
            <button id="sub-btn" class="btn btn-primary btn-sm" name="sub-btn"  onclick="return validateMetaData();">Save</button>
        </div>
    </div>
</form>
<script>
    // add/remove buttons for combo select boxes
    $('input.add').click(function(){
        var left = $(this).parent().parent().find('select.left_stream option:selected');
        var right = $(this).parent().parent().find('select.right_stream option:selected');
            var leftVal = left.val();
            var rightVal = right.val();
            if((typeof leftVal !== 'undefined') && (typeof rightVal !== 'undefined')){
                var selectedVal = '<option value ="' + leftVal + '=' + rightVal + '">' + leftVal + '=' + rightVal + '</option>';
                left.remove();
                right.remove();
                $('#selected_matadata').append(selectedVal);
                $('#selected_matadata option').prop('selected', true);
            }
    });

    $('input.remove').click(function(){
        var selectedMetadata = $('#selected_matadata').find('option:selected');
        if(selectedMetadata){
            selectedMetadataVal = selectedMetadata.val();
            if(selectedMetadataVal !== 'undefined'){
                var selectedMetadataVal = selectedMetadataVal.split("=");
                var left = $(this).parent().parent().find('select.left_stream').append('<option value ="' + selectedMetadataVal[0] + '">' + selectedMetadataVal[0] + '</option>');
                var right = $(this).parent().parent().find('select.right_stream').append('<option value ="' + selectedMetadataVal[1] + '">' + selectedMetadataVal[1] + '</option>');
                selectedMetadata.remove();
                $('#selected_matadata option').prop('selected', true);
            }
        }
    });
    function validateMetaData() {
        $("#save_meta_data").validate({
            rules: {
                "selected_matadata": {
                    required: true
                }
            },
            messages: {
                "selected_matadata": {
                    required: "Please map Metadata"
                }
            },
            submitHandler: function (form) {
                $('.loading_div').show();
                $('#save_meta_data').css({'opacity': '0.3'});
                $('.btn').attr('disabled', 'disabled');
                $('#selected_matadata option').prop('selected', true);
                var selected_matadata = $("#selected_matadata").val();
                var content_name_xml = $("#content_name_xml").val();
                var content_type_xml = $("#content_type_xml").val();
                var mrss_feed_url_xml = $("#mrss_feed_url_xml").val();
                var url = "<?php echo Yii::app()->baseUrl; ?>/admin/saveMrssData";
                $.post(url,{'selected_matadata':selected_matadata,'content_name_xml':content_name_xml,'content_type_xml':content_type_xml,'mrss_feed_url_xml':mrss_feed_url_xml},function(res){       
                    window.location.href = window.location.href;
                });
            }
        });
    }
</script>