<?php
if (isset($data['content_type']) && strtolower($data['content_type']) == "movie") {
    $item_url = "http://" . $this->studio->domain . "/movie/" . $data['permalink'];
    $video_url = "http://" . $this->studio->domain . "/video/play_fullmovie?movie=" . $data['id'];
} else {
    /*if ($data['is_episode'] == 0) {
        $video_url = "http://" . $this->studio->domain . "/video/play_fullmovie?movie=" . $data['id'] . "&tshow=1";
    } else {
        $video_url = "http://" . $this->studio->domain . "/video/play_fullmovie?movie=" . $data['id'] . "&episode_id=" . $data['movie_stream_id'];
    }*/
    if (isset($data['content_type']) && strtolower($data['content_type']) == "tv") {
        $item_url = "http://" . $this->studio->domain . "/tv-show/" . $data['permalink'];
    } else {
        $item_url = "http://" . $this->studio->domain . "/event/" . $data['permalink'];
    }
}
?>
<table style="margin-top:10px;width:100%;">
    <tr>
        <td style="margin-right: 15px;float:left;" valign="top">
            <img alt="<?php echo $data['name'] ?>" style="height: 140px;width: 90px;border: 1px solid #E5E5E5;padding: 1px;" src="<?php echo $data['thumb_image'] ?>">
        </td>
        <td  style="float:left;padding-left:20px;width:80%;" valign="top">
            <table>
                <tr>
                    <td style="color: #000000;font-size: 20px;font-weight: bold;"><?php echo "<a href='" . $item_url . "'>" . $data['name'] . "</a>" ?></td>
                </tr>
                <tr>
                    <td style="color: #000000;font-size: 15px;font-weight: bold;">
                        <?php
                        if(count($data['cast_detail']) > 1){
                            for ($i = 0; $i < count($data['cast_detail']); $i++) {
                                echo "<a href='http://" . $this->studio->domain . "/star/" . $data['cast_detail'][$i]['permalink'] . "'>" . $data['cast_detail'][$i]['celeb_name'] . "</a>";
                                if ($i == 0) {
                                    echo ",&nbsp;";
                                }
                                if ($i >= 1) {
                                    break;
                                }
                            }
                        }
                        ?>
                    </td>
                </tr>
                <!--<tr  style="padding-top:5px;clear:both;"><td>&nbsp;</td></tr>-->
                <tr>
                    <td style="font-size:14px;">
                        <?php echo $data['story'] ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td  style="height:5px;clear:both;" colspan="2"></td>
    </tr>
</table>


