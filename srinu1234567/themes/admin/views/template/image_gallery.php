<ul class="list-inline text-left">
<?php
foreach ($all_images as $key => $val) {
if ($val['image_name'] == '') {
$img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
} else {
//$img_path = $val['list_image_name'];
$img_path = $base_cloud_url . $val['s3_thumb_name'];
$orig_img_path = $base_cloud_url . $val['s3_original_name'];
}
?> 
    <li>
        <div class="Preview-Block">
            <div class="thumbnail m-b-0">
                <div class="relative m-b-10">
                    <input type="hidden" name="original_image<?php echo $val['id']; ?>" id=original_image<?php echo $val['id']; ?>" value="<?php echo $orig_img_path; ?>">
                    <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['image_name']; ?>"    />
                    <img class="img" src="<?php echo $img_path; ?>" data-src="<?php echo $orig_img_path; ?>"  alt="<?php echo "All_Image"; ?>"  >
                    <div class="caption overlay overlay-white">
                        <div class="overlay-Text">
                            <div>
                                <a id="thumb_<?php echo $val['id']; ?>" onclick="show_img_preview(<?php echo $val['id']; ?>, '<?php echo $orig_img_path; ?>', '<?php echo $val['image_name']; ?>')">
                                    <span class="btn btn-primary icon-with-fixed-width">
                                        <em class="icon-check"></em>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php } ?>
</ul>