<div class="container ">
    <div class="row">
		<div class="span3 bs-docs-sidebar">
			<?php $this->renderPartial('apimenu');?>
		</div>
		<div class="span9">
			<section id="overview">
				<span class="anchor" id="the-introduction"></span>   
				<h3>Introduction</h3>
				<p>
					Muvi API's are designed to help Developers, resellers, SI’s, as well as 3rd party Apps developers to make use of Muvi’s video streaming engine and deliver an enhanced video experience to their customers and end viewers. With Muvi API’s you can build stunning apps, sites as well as businesses that make use of Video as their delivery and communication medium to interact with the end users.<br/>
 					Using Muvi’s API’s developers can make use of Muvi’s Video Streaming Engine and be able to –<br/>
				<ul>
					<li>Add videos of any format to Muvi</li>
					<li>Encode/Transcode them to various formats</li>
					<li>Add as well as Pull meta-data for the videos</li>
					<li>Get Embed codes for the video player to display under your apps</li>
					<li>Add/Manage Subscribers</li>
					<li>Manage user Authentications</li>
					<li>Send search request and get results</li>
					<li>Manage Payment Plans</li>
				</ul>
				Go ahead and start building, if you need any help or have suggestions please feel free to reach out to us on <a href="mailTo:studio@muvi.com">studio@muvi.com</a> and we will be sure to help you.<br/>
				</p>

				<span class="anchor" id="the-api"></span>            
				<h3>The API</h3>
				<p>
					This is the documentation for the Muvi V1 API. The Help Center API documentation supplements it.
				</p>
				<p>
					Read the contents of this page carefully, including the Restrictions and Responsibilities, to understand how to be a good API citizen.
				</p>
				<p>
					Your use and access to the API is expressly conditioned on your compliance with the policies, restrictions, and other provisions related to the API set forth in our API Restrictions and Responsibilities and the other documentation we provide you. You must also comply with the restrictions set forth in the Muvi Terms of Service and the Muvi Privacy Policy, in all uses of the API. If Muvi believes that you have or attempted to violate any term, condition, or the spirit of these policies or agreements, your right to access and use the API may be temporarily or permanently revoked.
				</p>
				<p>
					When documenting a resource, we use curly braces, <code>{}</code>, for identifiers. Example:  <code>http://muvi.com/rest/:method</code>.
				</p>
				<span class="anchor" id="change-policy"></span> 
				<h3 >Change Policy</h3>

				<p>Muvi may modify the attributes and resources available to the API and our policies related to access and use of the API from time to time without advance notice. Muvi will use commercially reasonable efforts to notify you of any modifications to the API or policies through notifications. Modification of the API may have an adverse effect on Muvi Applications, including but not limited to changing the manner in which Muvi Applications communicate with the API and display or transmit Your Data. Muvi will not be liable to you or any third party for such modifications or any adverse effects resulting from such modifications.</p>

				<span class="anchor" id="security-and-authentication"></span>            
				<h3 class="doc-anchor">Security and Authentication</h3>
				<p>
					This is an paid API only, regardless of how you may have your account configured. You must be a verified user to make API requests. You can authorize against the API using authToken. 
				</p>
				<p>
					API tokens are managed by  Muvi Admin interface.
				</p>
				<span class="anchor" id="headers"></span>            
				<h3 class="doc-anchor">Headers</h3>
				<p>
					This is a JSON-only API. You must supply a <code>Content-Type: application/json</code> header on <code>PUT</code> and <code>POST</code> operations. You must set an <code>Accept: application/json</code> header on all requests. You <em>may</em> get a <code>text/plain</code> response in case of error such as a bad request. You should treat this as an error you need to fix.
				</p>

				<span class="anchor" id="requests"></span>
				<h3 class="doc-anchor">Requests</h3>

				<p>
					Muvi responds to successful requests with HTTP status codes in the 200 or 400 range. When you create or update a resource, Muvi renders the resulting JSON representation in the response body and set a <code>Location</code> header pointing to the resource. Example:
				</p>

				<span class="anchor" id="pagination"></span>
				<h3 class="doc-anchor">Pagination</h3>
				<p>
					By default, most list endpoints return a maximum of 100 records per page. You can change the number of records on a per-request basis by passing a <code>per_page</code> parameter in the request URL parameters. Example: <code>per_page=50</code>. However, you can't exceed 100 records per page on most endpoints.
				</p>
				<p>When the response exceeds the per-page maximum, you can paginate through the records by incrementing the <code>page</code> parameter. Example:  <code>page=3</code>. List results include <code>next_page</code> and <code>previous_page</code> URLs in the response body for easier navigation:</p>
			</section>	  
		</div>
	</div>
</div>