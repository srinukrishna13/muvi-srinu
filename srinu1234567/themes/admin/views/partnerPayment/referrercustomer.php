<style type="text/css">
    .toper{padding-bottom: 20px;}
    .no-padding{padding:0px;}
    .large-font{font-size:25px;}
    .small-margin-top{margin-top:10px;}
</style>
<script type="text/javascript">    
    function validateNewUser() {
        //$("#newmembership").html('Saving...');
      //  $("#newmembership").attr("disabled", true);
      //  var validate = $("#new_user_form").validate();
       // var x = validate.form();
        var validate = $("#new_user_form").validate({
           rules: {
                'name[]': {
                    required: true,
                },
               'email[]': {
                    required: true
                },
                'phone[]': {
                    required: true
                },
                 'title[]': {
                    //required: true
                }               
            },
            messages: {
                'name[]': {
                    required: 'Please enter the name'
                },
                'email[]': {
                    required: 'Please enter the email'
                },
                'phone[]': {
                    required: 'Please enter the phone'
                },
                 'title[]': {
                    required: 'Please enter the title'
                }                
                
                
            },
                errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent()); 
            }
        });    
       var x = validate.form();
       var i =0;
       $('.contact_section_cov .arname').each(function(){
           if($(this).val() == ''){
               $(this).parent().parent().find( ".error" ).show();
               $(this).parent().parent().find( ".error" ).html('Please enter the name');
              // preventDefault();
              i =0;
           }else{
               i = 1;
           }
       });
       $('.contact_section_cov .aremail').each(function(){
           var chek_mail = check_mail($(this).val());
           if($(this).val() != '' && chek_mail == 1){
               i = 1;
           }else{
               i =0;
               $(this).parent().parent().find( ".error" ).show();
               $(this).parent().parent().find( ".error" ).html('Please enter the email');             
           }    
       });
        $('.contact_section_cov .arphone').each(function(){
               var chk_phn = check_phone($(this).val());
           if($(this).val() != '' && chk_phn == 1){
               i = 1;
           }else{
               i =0;
               $(this).parent().parent().find( ".error" ).show();
               $(this).parent().parent().find( ".error" ).html('Please enter the phone');             
           } 
       });  
        $('.contact_section_cov .artitle').each(function(){
           if($(this).val() == ''){
               $(this).parent().parent().find( ".error" ).show();
               $(this).parent().parent().find( ".error" ).html('Please enter the title');
              // preventDefault();
              i =0;
           }else{
               i = 1;
           }
       });      
     

     if (x && i == 1) {
             var data = $("#new_user_form").serialize();
            $.post("InsertRefercustomer", {'data':data}, function (res) {
                if(res!=0){
                       window.location.href = "<?php echo Yii::app()->baseUrl; ?>/partner/referrer";
                    return false;
                }else{
                    document.new_user_form.action = 'InsertRefercustomer';
                    document.new_user_form.submit();
                }
            });
        } else {
            $("#newmembership").html('Add Referral');
            $("#newmembership").removeAttr("disabled");
        } 
    }  
    function append_contacts(){
        var is_true = 0;
        $(".contact_section_cov").find(".content_form").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
                if($(this).attr('name') == 'email[]'){
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(regex.test($(this).val())){
                        isTrue = 1;
                    }else{
                        isTrue = 0;
                        $(this).focus();
                        return false;
                    }
                }else if($(this).attr('name') == 'phone[]'){
                     var filter = /^[0-9-+]+$/;
                     if(filter.test($(this).val())){
                         isTrue = 1;
                     }else{
                        isTrue = 0;
                        $(this).focus();
                        return false;                        
                     }
                }
            }else{
                isTrue = 0;
                $(this).focus();
                return false;
            }
        });
        if (parseInt(isTrue)) {
           var str = $(".contact_section_hide").html();
           $(".contact_section_cov").append(str);
           $(".contact_section_cov").find(".form-control").each(function() {
               $(this).addClass('content_form');
           })
       }       
    }
    function append_remove(data){
        $(data).parent().parent().remove();
    }
    function check_mail(value){
         var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         if(regex.test(value)){
             return 1;
         }else{
             return 0;
         }
    }
    function check_phone(value){
       var filter = /^[0-9-+]+$/;
       if(filter.test(value)){
           var len = value.toString().length;
           if(len >= 10){
               return 1;
           }else{
               return 0;
           }
       }else{
           return 0;
       }
    }
</script>
<form action="javascript:void(0);" method="post" name="new_user_form" id="new_user_form" enctype="multipart/form-data" class="form-horizontal">
    <div class="row form-group" id="packagediv">
        <div class="col-lg-10">
            <div class="form-group" >
                <label class="col-sm-2 control-label" for="Company Name">Business Name</label>                
                <div class="col-sm-7">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="company_name" autocomplete="off" required="true" placeholder="Business Name"/>
                    </div>
                    <label id="company_name-error" class="error red" for="company_name"></label>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-2 control-label" for="Contacts">Contacts</label>           
                <div class="col-sm-9 no-padding contact_section_cov">
                        <div class="contact_section">
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <input type="text" class="form-control content_form arname" name="name[]" autocomplete="off" required="true" placeholder="Name"/>
                                </div>
                                
                            </div>
                             <div class="col-sm-3">
                                <div class="fg-line">
                                    <input type="text" class="form-control content_form aremail" name="email[]" autocomplete="off" required="true" placeholder="Email"/>
                                </div>   
                            </div>
                             <div class="col-sm-3">
                                <div class="fg-line">
                                    <input type="text" class="form-control content_form arphone" name="phone[]" autocomplete="off" required="true" placeholder="Phone"/>
                                </div>
                            </div>    
                             <div class="col-sm-2">
                                <div class="fg-line">
                                    <input type="text" class="form-control content_form artitle" name="title[]" autocomplete="off" required="true" placeholder="Title"/>
                                </div>
                              </div>
                            <div class="col-sm-1">
                               <a href='javascript:void(0);' onclick="append_contacts();"><i class="fa fa-plus-square large-font small-margin-top" aria-hidden="true"></i></a>
                           </div>                           
                   </div>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-2 control-label" for="Email">Business Website</label>   
                <div class="col-sm-7">
                    <div class="fg-line">
                        <input type="text" class="form-control  col-lg-5" name="business_website" id="business_website" autocomplete="off" />
                    </div>
                    <label id="email-error" class="error red" for="email"></label>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-2 control-label" for="Phone">Notes</label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <textarea name="notes" class="form-control" placeholder="Add some notes about this referral. What do they do. What are they looking for, should we contact them or not etc."></textarea>
                    </div>
                    <label id="notes-error" class="error red" for="notes"></label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 m-t-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="terms" id="terms" required="true"><i class="input-helper"></i>                                  
                            Don't contact this referral, I am in touch with them.
                        </label>
                        <label id="terms-error" class="error red" for="terms" style="display: none;"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" onclick="return validateNewUser();" id="newmembership">Add Referral</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="contact_section_hide hide">
    <div class="clearfix"></div>
    <div class="contact_section_append small-margin-top">
        <div class="clearfix"></div>
        <div class="col-sm-3">
            <div class="fg-line">
                <input type="text" class="form-control arname" name="name[]" autocomplete="off" required="true" placeholder="Name"/>
            </div>
            <label id="name-error" class="error red" for="name"></label>
        </div>
         <div class="col-sm-3">
            <div class="fg-line">
                <input type="text" class="form-control aremail" name="email[]" autocomplete="off" required="true" placeholder="Email"/>
            </div>
             <label id="email-error" class="error red" for="email"></label>
        </div>
         <div class="col-sm-3">
            <div class="fg-line">
                <input type="text" class="form-control arphone" name="phone[]" autocomplete="off" required="true" placeholder="Phone"/>
            </div>
             <label id="phone-error" class="error red" for="phone"></label>
        </div>    
         <div class="col-sm-2">
            <div class="fg-line">
                <input type="text" class="form-control artitle" name="title[]" autocomplete="off" required="true" placeholder="Title"/>
            </div>
             <label id="title-error" class="error red" for="title"></label>
        </div>
        <div class="col-sm-1">
           <button type="button" class="btn btn-default no-padding small-margin-top" onclick="append_remove(this);"><i class="fa fa-minus-square large-font" aria-hidden="true"></i></button>
       </div>        
    </div>
</div>