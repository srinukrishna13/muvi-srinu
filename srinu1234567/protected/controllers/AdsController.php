<?php
class AdsController extends Controller
{
    public $layout='main';     
    public function actionIndex()
    {
		//header("HTTP/1.1 301 Moved Permanently");
		//$this->redirect(Yii::app()->getBaseUrl(true));exit;
		$this->pageTitle = "Online Video Advertising (Ad) Platform with Support for Out-of-The-Box (AVOD) Ads - Muvi";
        $this->pageKeywords = 'Muvi, Muvi.com, Muvi Contact, Muvi Contact, Contact Muvi.com';
        $this->pageDescription = 'Video Ads Platform offering seamless integration with all Ad Servers and VOD Platforms to give your online video advertising a Muvi boost.';
        $midroll_time = 0;
		if(isset($_REQUEST) && count($_REQUEST) > 0){
			if(@$_POST['hid_val'] == 'active'){
				if(@$_REQUEST['video_type'] == 1){
				   $ad_type = 1; 
				}else if(@$_REQUEST['video_type'] == 2){
					$ad_type = 2; 
					$midroll_time = @$_REQUEST['midroll_time'];
				}else{
					$ad_type = 0; 
				}
			}else{
				$this->redirect(Yii::app()->getBaseUrl(true).'/ads');exit;
			}
        }else{
            $ad_type = 0; 
        }
        $this->render('player', array('ad_type' => $ad_type, 'midroll_time' => $midroll_time));        
    } 
    
    public function actionPlay(){
        Yii::app()->theme = 'bootstrap'; 
        $this->layout = false;
        $fullmovie_path = Yii::app()->getBaseUrl(true).'/video/small.mp4';
        $this->pageTitle = "Test Ad page";
        $ad_network_id = 0;
        $channel_id = '';
        
        $studio_id = Yii::app()->common->getStudiosId();
        $v_logo = Yii::app()->common->getLogoFavPath($studio_id);
        $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = '.$studio_id));
        foreach($studio_ads as $studio_ad){
            $ad_network_id = $studio_ad->ad_network_id;
            $channel_id = $studio_ad->channel_id;
        }
        
        $this->render('ad_player', array('v_logo' => $v_logo,'fullmovie_path' => $fullmovie_path, 'ad_network_id' => $ad_network_id, 'channel_id' => $channel_id, 'ad_type' => 'vpaid')); 
    }    
    
    public function actionAdform(){
        $studio_id = Yii::app()->common->getStudiosId();        
        $stream_id = $_REQUEST['movie_stream_id'];
        $stream = movieStreams::model()->findByAttributes(array( 'id' => $stream_id));
        $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = '.$studio_id)); 
        $chkd = ($stream->enable_ad == 1)?' checked="checked"':'';
        //@arvind-modifired-29-06-2017
        $is_pre_roll = ($stream->rolltype == 1 || $stream->rolltype == 3 || $stream->rolltype == 5 || $stream->rolltype == 7)?' checked="checked"':'';
        $is_mid_roll = ($stream->rolltype == 2 || $stream->rolltype == 3 || $stream->rolltype == 6 || $stream->rolltype == 7)?' checked="checked"':'';
        $is_post_roll = ($stream->rolltype == 4 || $stream->rolltype == 5 || $stream->rolltype == 6 || $stream->rolltype == 7)?' checked="checked"':'';
        $form_action = Yii::app()->getBaseUrl(true)."/ads/saveadconfig";        
        $mid_roll_show = ($stream->rolltype == 2 || $stream->rolltype == 3 || $stream->rolltype == 6 || $stream->rolltype == 7)?'':'style="display:none;"';        
        $roll_after = str_replace('00:00:00,', '', $stream->roll_after);
        $roll_after = str_replace('00:00:00', '', $roll_after);
        $roll_afters = (trim($roll_after))?explode(',', $roll_after):array();
        if (in_array("end", $roll_afters)) {
            array_pop($roll_afters);
        }
        if (in_array("0", $roll_afters)) {
            $roll_afters = array_reverse($roll_afters);
            array_pop($roll_afters);
            $roll_afters = array_reverse($roll_afters);
        }
        $MonetizationMenuSettings = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($studio_id);
        $ad_form = '<style> .ad_active{ background:#fff;} .ads_interval{ background:#fff; } </style>';
        $ad_form = '    <form name="ad-form" id="ad-form" class="form-horizontal" method="post">';
        $ad_form.= '        <input type="hidden" name="ad_stream_id" id="ad_stream_id" value="'.$stream_id.'" />';
        $ad_form.= '        <div class="modal-body">';
        $ad_form.= '            <div class="form-group">';
        $ad_form.= '                <div class="col-sm-12"><span class="error red" id="ad_error"></span></div>';
        $ad_form.= '            </div>';
        $ad_form.= '            <div class="form-group">';
        $ad_form.= '                <div class="col-sm-12">';
        $ad_form.= '                    <div class="checkbox">';
        $ad_form.= '                        <label>';
        $ad_form.= '                            <input type="checkbox" name="enable_ad" id="enable_ad" value="1" '.$chkd.' /><i class="input-helper ad_active"></i>&nbsp; Enable Ads on this video';
        $ad_form.= '                        </label>';
        $ad_form.= '                    </div>';
        $ad_form.= '                </div>';
        $ad_form.= '            </div>';
        //roll-section-start
        if(@$MonetizationMenuSettings[1]['ad_network_id'] !=3 && (@$MonetizationMenuSettings[0]['menu']&4))
        {
        $ad_form.= '            <div id="roll_types">';
        //pre-roll
        $ad_form.= '                <div class="form-group row-fluid">';
        $ad_form.= '                    <div class="col-sm-4">';
        $ad_form.= '                        <div class="checkbox">';
        $ad_form.= '                            <label><input type="checkbox" name="rolltype[]" id="preroll" value="1" '.$is_pre_roll.' class="roll_type ad_active" /><i class="input-helper"></i>&nbsp;Pre-roll</label>';
        $ad_form.= '                        </div>';   
        $ad_form.= '                    </div>';
        //mid-roll
        $ad_form.= '                    <div class="col-sm-4" >'; 
        $ad_form.= '                        <div class="row">';
        $ad_form.= '                            <div class="col-sm-12">';
        $ad_form.= '                                <div class="checkbox">';
        $ad_form.= '                                    <label><input type="checkbox" name="rolltype[]" id="midroll" value="2" '.$is_mid_roll.' class="roll_type ad_active" /><i class="input-helper"></i>&nbsp;Mid-roll</label>';
        $ad_form.= '                                </div>';                               
        $ad_form.= '                             </div>';
        //mid-roll-options
        $ad_form.= '                            <div class="col-sm-12" style="border:0px solid red">';
        $ad_form.= '                                <div id="roll_timing" '.$mid_roll_show.'>';
        $ad_form.= '                                    <div id="roll_after_section">';
        if(count($roll_afters) > 0){
            foreach($roll_afters as $roll_aft=> $value){
                $ad_form.= '                <div class="form-group row-fluid">';
                $ad_form.= '                    <div class="col-sm-8">';   
                $ad_form.= '                         <div class="fg-line">'; 
                $ad_form.= '                            <input type="text" class="form-control input-sm ads_interval" placeholder="HH:MM:SS" name="roll_after[]" onchange="javascript: validateHHMM(this.value,this);" value="'.$value.'" />';
                $ad_form.= '                          </div>';
                $ad_form.= '                    </div>';
                $ad_form.= '                    <div class="col-sm-4">';
                $ad_form.= '                        <button onclick="addMore_new(this); return false;" class="btn btn-default"><i class="icon-plus"></i></button>';
                if(count($roll_afters) > 1){
					if($roll_aft==0)
						$ad_form.= ' ';
					else
						$ad_form.= '                <button onclick="removeBox_new(this); return false;" class="btn btn-default"><i class="icon-trash" aria-hidden="true"></i></button>';
                }
                $ad_form.= '                     </div>';
                $ad_form.= '                    </div>';
            }
        }else{
            $ad_form.= '                        <div class="form-group row-fluid">';
            $ad_form.= '                            <div class="col-sm-8"><div class="fg-line">';   
            $ad_form.= '                                <input type="text" class="form-control input-sm ads_interval" placeholder="HH:MM:SS" name="roll_after[]" onchange="javascript: validateHHMM(this.value,this);"/>';
            $ad_form.= '                            </div></div>';
            $ad_form.= '                            <div class="col-sm-4">';
            $ad_form.= '                            <button onclick="addMore_new(this); return false;" class="btn btn-default"><i class="icon-plus"></i></button>';
            $ad_form.= '                            </div>';
            $ad_form.= '                        </div>';        
        }
        $ad_form.= '                    </div>';    
        $ad_form.= '                    </div>';
        $ad_form.= '                </div>';  
        $ad_form.= '                </div>';
        $ad_form.= '                </div>';                         
		//post_roll
		$ad_form.= '                    <div class="col-sm-4">';
		$ad_form.= '                        <div class="checkbox">';
		$ad_form.= '                            <label><input type="checkbox" name="rolltype[]" id="postroll" value="4" '.$is_post_roll.' class="roll_type ad_active" /><i class="input-helper"></i>&nbsp;Post-roll</label>';
        $ad_form.= '            </div>'; 
        $ad_form.= '        </div>';
        $ad_form.= '        </div>';
        $ad_form.= '        </div>';
        }
        //roll-section-end
        $ad_form.= '        </div>';
        $ad_form.= '        <div class="modal-footer">';
        $ad_form.= '            <button id="sub-btn" onclick="validateAd('.$stream->enable_ad.');" class="btn btn-primary" type="button" disabled="disabled">Save</button>';
        $ad_form.= '        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
        $ad_form.= '        </div>';                    
        $ad_form.= '    </form>';        
        $ad_form.= "<script type='text/javascript'>"; 
        $ad_form.= "$('#enable_ad').change(function(){";
        $ad_form.= "$('#sub-btn').removeAttr('disabled','false');";
        $ad_form.= "    });";
        $ad_form.= "$('.ad_active').change(function(){";
        $ad_form.= "$('#sub-btn').removeAttr('disabled','false');";
        $ad_form.= "});";
        $ad_form.= "$(function () {";
        if($stream->enable_ad != 1)
            $ad_form.= "    $('#roll_types').hide();";
        //role-options
        if(@$MonetizationMenuSettings[1]['ad_network_id'] !=3 && (@$MonetizationMenuSettings[0]['menu']&4))
            $ad_form.= "    $('#enable_ad').change(function () { $('#roll_types').toggle(); });";
        $ad_form.= "    $('#midroll').bind('change', function(e){
                            if($(this).is(':checked')){
                                $('#roll_timing').show();
                            }else{
                                $('#roll_timing').hide();
                            }
                        });";
        $ad_form.= "});";
        $ad_form.= "</script>";
        $ad_form.= '<div id="roll_after_section_hidden" style="display:none;">
    <div class="form-group row-fluid">
        <div class="col-sm-8">
                             <input type="text" class="form-control input-sm ads_interval" placeholder="HH:MM:SS" name="roll_after[]" onchange="javascript: validateHHMM(this.value,this);"/></div>
            <div class="col-sm-4">
                             <button onclick="addMore_new(this); return false;" class="btn btn-default"><i class="icon-plus"></i></button>
                             <button onclick="removeBox_new(this); return false;" class="btn btn-default"><i class="icon-trash"></i></button>
            </div>
    </div>
    </div>
</div> ';
        echo $ad_form;
    }
    
    public function actionSaveadconfig() {
		$studio_id = Yii::app()->common->getStudiosID();
		$stream_id = isset($_POST['ad_stream_id']) ? $_POST['ad_stream_id'] : 0;
		$stream = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $stream_id));
		$MonetizationMenuSettings = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($studio_id);
        if($stream_id > 0 && count($stream) > 0){
			$enable_ad = isset($_REQUEST['enable_ad']) ? $_REQUEST['enable_ad'] : 0;
			if ($enable_ad == 0) {
				//Disable Ads 
				$stream->enable_ad = 0;
				$stream->rolltype = 0;
				$stream->roll_after = '';
				$stream->save();
				Yii::app()->user->setFlash('success', 'Ad disabled successfully.');
				echo 'saved';
				exit();
			} else {
				$streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $stream_id));
				$rolltype = 0;
				$roll_after = '';
				$rolls = $_REQUEST['rolltype'];
				$roll_afters = $_REQUEST['roll_after'];
				if (@$MonetizationMenuSettings[1]['ad_network_id'] != 3 && (@$MonetizationMenuSettings[0]['menu'] & 4)) {
					if (count($rolls) > 0) {
						foreach ($rolls as $roll) {
							$rolltype += $roll;
						}
						// pre || pre-mid || pre-post || pre-mid-post 
						if ($rolltype == 1) {
							$roll_after .= '0';
						}
						// mid || pre-mid || mid-post || pre-mid-post 
						else if ($rolltype == 2 || $rolltype == 3 && count($roll_afters) > 0) {
							if ($rolltype == 3)
								$roll_after = '0';
							foreach ($roll_afters as $roll_aft) {
								if ($roll_after != '')
									$roll_after .= ',' . $roll_aft;
								else
									$roll_after .= $roll_aft;
							}
						}
						// post || pre-post || mid-post || pre-mid-post
						else if ($rolltype == 4 || $rolltype == 5 || $rolltype == 6 || $rolltype == 7) {
							if ($rolltype == 5) {
								$roll_after = '0,end';
							} else if ($rolltype == 7) {
								$roll_after = '0';
								foreach ($roll_afters as $roll_aft) {
									if ($roll_after != '')
										$roll_after .= ',' . $roll_aft;
									else
										$roll_after .= $roll_aft;
								}
								$roll_after .= ',end';
							}
							else if ($rolltype == 6) {
								foreach ($roll_afters as $roll_aft) {
									if ($roll_after != '')
										$roll_after .= ',' . $roll_aft;
									else
										$roll_after .= $roll_aft;
								}
								$roll_after .= ',end';
							}
							else {
								$roll_after .= 'end';
							}
						}
						// echo $enable_ad ."----".$rolltype."----". $roll_after;exit;

						$stream->enable_ad = $enable_ad;
						$stream->rolltype = $rolltype;
						$stream->roll_after = $roll_after;
						$stream->save();
						Yii::app()->user->setFlash('success', 'Ad saved successfully.');
						echo 'saved';
						exit;
					} else {
						echo 'error';
						exit();
					}
				} else {
					//Enable Ads. 
					if ($enable_ad == 1) {
						$stream->enable_ad = $enable_ad;
						$stream->rolltype = null;
						$stream->roll_after = null;
						$stream->save();
						Yii::app()->user->setFlash('success', 'Ad saved successfully.');
						echo 'saved';
						exit;
					} else {
						echo 'error';
						exit();
					}
				}
			}
		} else {
			echo 'error';
			exit;
		}
	}
}