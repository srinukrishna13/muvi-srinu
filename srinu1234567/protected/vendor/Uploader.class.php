<?php
use amazon_sdk\services\s3;
Class Uploader {
    private $actual_dir;    //actual directory
    private $config;
    private $hashed_file_name;  //hashed timestamp of uploaded file's original name
    private $user_home_dir; //home dir of user
    private $uid; //user id
    public $image;
    public $s3;
    public $key;
    public $secret;

    public function __construct($uid) {
        //constructor imports config and sets users dir to his id in upload dir
        $this->config = Config::getInstance();
        $this->s3 = new AmazonS3($this->config->getS3Key(), $this->config->getS3Secret());
        $this->user_home_dir = $this->config->getUploadDir() . "/" . $uid;
        $this->actual_dir = "";
        $this->uid = $uid;
    }

    private function _checkUpload() {
        try {
            if (empty($_FILES)) {
                throw new Exception("This method should be called after a file upload.");
            }
        } catch (Exception $e) {
            echo $e->getMessage() . " in file " . __FILE__ . " at line " . __LINE__;
            die();
        }

        try {
            if (!in_array(strtolower($this->generateExtension()), $this->config->getMimeTypes())) {
                throw new Exception("This file extension is not allowed.");
            }
        } catch (Exception $e) {
            echo $e->getMessage() . " in file " . __FILE__ . " at line " . __LINE__;
            die();
        }
    }
    
    public function uploadsample($uploadpath="public") {
        $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath. '/no-image-a.png', array(
            'fileUpload' => $this->user_home_dir."/".'no-image-a.png',
            'acl' => AmazonS3::ACL_PUBLIC
        ));
        $file_upload_response = $this->s3->batch()->send();   
        //print_r($file_upload_response);
        //print "<pre>";print_r($file_upload_response);exit;
    }    

    private function generateExtension() {
        //return the extension of uploaded file
        return pathinfo($_FILES["Filedata"]["name"], PATHINFO_EXTENSION);
    }

    private function hashFilename() {
        $this->_checkUpload(); //check if a file was uploaded
        $this->hashed_file_name = MD5(date("F/d/Y H:i:s")) . "." . $this->generateExtension();
    }

    private function filter_file_list($arr) {
        return array_values(array_filter(array_map(array($this, 'file_path'), $arr)));
    }

    function file_path($file) {
        return!is_dir($file) ? realpath($file) : null;
    }

    public function deleteAll($directory, $empty = false) {
        //function for deleting a folder an it's contents
        if (substr($directory, -1) == "/") {
            $directory = substr($directory, 0, -1);
        }

        if (!file_exists($directory) || !is_dir($directory)) {
            return false;
        } elseif (!is_readable($directory)) {
            return false;
        } else {
            $directoryHandle = opendir($directory);

            while ($contents = readdir($directoryHandle)) {
                if ($contents !='' && $contents != '.' && $contents != '..') {
                    $path = $directory . "/" . $contents;

                    if (is_dir($path)) {
                        $this->deleteAll($path);
                    } else {
                        unlink($path);
                    }
                }
            }

            closedir($directoryHandle);

            if ($empty == false) {
                if (!rmdir($directory)) {
                    return false;
                }
            }

            return true;
        }
    }
    
    private function determineDirs( $dir )
    	{
    		if ( $this->actual_dir == "" ) {
    			$dirs = explode("/",$dir);
    			if (array_key_exists('1', $dirs)) {
    				$dirs[0] .= " (dir)";
				}
    			return $dirs[0];
    		} else {
    			$dir = str_replace($this->actual_dir, "", $dir);
    			$dirs = explode("/",$dir);
    			if (array_key_exists('2', $dirs)) {
    				$dirs[1] .= " (dir)";
				}
    			return $dirs[1];
    		}
    	}

/**
 * 
 */
	public function uploadStudioLogo($fileinfo,$uploadPath='',$tempPath='') {
		if(!$tempPath){
			$tempPath = $fileinfo['tmp_name'];
		}
		$_FILES["Filedata"] = $fileinfo;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            //check for error
            echo "Return Code: " . $fileinfo["error"] . "<br />";
        } else {
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
            foreach ($this->config->getDimensions() as $key=> $dimensions) {
				if (!is_dir($this->user_home_dir . "/" . $key)) {
					mkdir($this->user_home_dir . "/" . $key);
				}
				$this->hashed_file_name=$key;
                $dimensions = explode("x", $dimensions);
                $this->image->load($tempPath);
                $this->image->resize($dimensions[0],$dimensions[1]);
				$image_info = getimagesize($tempPath);
				$imageType = $image_info[2];
				$this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $fileinfo["name"],$imageType);
                chmod($this->user_home_dir . "/" . $key . "/" . $fileinfo["name"], 0775);
				$sizefolder[]= $key;
            }
            chmod($this->user_home_dir . "/original/".$fileinfo["name"], 0775);
			// Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                $fileName = Yii::app()->common->fileName($fileinfo['name']);
                $this->s3->batch()->create_object($this->config->getBucketName(),$uploadPath. $fileName, array(
                    'fileUpload' => $this->user_home_dir."/".$value."/".$fileinfo["name"],
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }

            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();

            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadpath. $fileName). PHP_EOL . PHP_EOL;
                }
            }

            //delete files on machine
            //$this->deleteAll($this->user_home_dir);
			return $arr;
			return @$this->user_home_dir;
        }
    }
	
   //Upload a file to the own server
    public function uploadLogo($ltype='logo') {
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();        
        $uploadpath = $this->config->getUploadDir(); 
        $ext = $this->generateExtension();
        $file = '';
        
        if ($_FILES["Filedata"]["error"] > 0) {
            //check for error
            echo "Return Code: " . $_FILES["Filedata"]["error"] . "<br />";
        } else {
            //echo $this->user_home_dir;exit;
            //create necessary directories
            if (!is_dir($uploadpath)) {
                mkdir($uploadpath, 0777, TRUE);
                chmod($uploadpath, 0777);
            }
            $img_size = getimagesize($_FILES['Filedata']['tmp_name']);
            $img_width = $img_size[0];
            $img_height = $img_size[1];
            $img_type = $img_size[2];
            foreach ($this->config->getDimensions() as $key => $dimensions) {
                
                $dimensions = explode("x", $dimensions);
                $width = $dimensions[0];
                $height = $dimensions[1];
                $this->image->load($_FILES['Filedata']['tmp_name']);
                //Keeping the aspect ration constant irrespective of height
                if($img_width > $width && $img_height > $height)
                    $this->image->resizeToHeight($height);
                else if($img_width > $width && $img_height > $height)
                    $this->image->resizeToWidth($width);
                else
                    $this->image->resizeToHeight($height);
                //Keeping the dimension constant irrespective of aspect ration

                //$this->image->resize($dimensions[0], $dimensions[1]);
                $file = $ltype.'.'.$ext;
                
                @unlink($uploadpath . $file);             
                
                $ret = $this->image->save($uploadpath . "/" . $file, $img_type);
                chmod($uploadpath . "/" . $file, 0775);
            } 
            //Loader image for video
            //$this->createLoaderImage($uploadpath,$file);            
        }
        return @$file;
    }
    
    /*
    public function createLoaderImage($logo_path, $file) {
        $videoLoaderImage = $_SERVER['DOCUMENT_ROOT'] . '/common/images/muvi_loader.gif';
        $newLogo = $logo_path . $file;
        $newVideoLoaderImage = $logo_path . 'video_loader.gif';
        $command = "convert  $videoLoaderImage -coalesce -gravity South  -geometry +0+100 null: $newLogo -layers composite -layers optimize $newVideoLoaderImage";
        shell_exec($command);
        chmod($newVideoLoaderImage, 0775);
        return true;
    }
     * 
     */
    
    public function upload($uploadpath="public/system/posters/") {
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($_FILES["Filedata"]["error"] > 0) {
            //check for error
            echo "Return Code: " . $_FILES["Filedata"]["error"] . "<br />";
        } else {
			//echo $this->user_home_dir;exit;
            //create necessary directories
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
           /* if (!is_dir($this->user_home_dir . "/" . $this->hashed_file_name)) {
                mkdir($this->user_home_dir . "/" . $this->hashed_file_name);
            }*/
            //resize image for desired dimensions
            foreach ($this->config->getDimensions() as $key=> $dimensions) {
				if (!is_dir($this->user_home_dir . "/" . $key)) {
					mkdir($this->user_home_dir . "/" . $key);
				}
				$this->hashed_file_name=$key;
                $dimensions = explode("x", $dimensions);
                $this->image->load($_FILES['Filedata']['tmp_name']);
                //Keeping the aspect ration constant irrespective of height
				//$this->image->resizeToWidth($dimensions[0]);
				//Keeping the dimension constant irrespective of aspect ration
				
				$this->image->resize($dimensions[0],$dimensions[1]);
                $this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $_FILES["Filedata"]["name"]);
                chmod($this->user_home_dir . "/" . $key . "/" . $_FILES["Filedata"]["name"], 0775);
				$sizefolder[]= $key;
            }
            //upload the image to dir
            //move_uploaded_file($_FILES["Filedata"]["tmp_name"], $this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg");
            //chmod($this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg", 0775);
            if (!is_dir($this->user_home_dir . "/original")) { 
                mkdir($this->user_home_dir. "/original");
            }
			move_uploaded_file($_FILES["Filedata"]["tmp_name"], $this->user_home_dir . "/original/".$_FILES["Filedata"]["name"]);
            chmod($this->user_home_dir . "/original/".$_FILES["Filedata"]["name"], 0775);
			$sizefolder[]= 'original';
			
            //uploading the files to the machine is needed, as image class
            //resizes images and can only save them to the machine.
            //then all files are moved in a batch to CDN
            //and deleted from the machine.
            //move the newly created files to CDN
		//	$sizefolder = array('original','thumb','standard','medium','crop_thumb','crop_medium','crop_standard');
           // $list_of_files = $this->filter_file_list(glob($this->user_home_dir . "/" . $this->hashed_file_name . '/*'));
            // Prepare to hold the individual filenames
           // $individual_filenames = array();
			foreach($sizefolder AS $k=>$v){
				
			}
            // Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                // Grab only the filename part of the path
                //$filename = explode(DIRECTORY_SEPARATOR, $file);
                //$filename = array_pop($filename);

                // Store the filename for later use
                //$individual_filenames[] = $filename;

                /* Prepare to upload the file to our new S3 bucket. Add this
                  request to a queue that we won't execute quite yet. */
                $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath.$this->uid . "/" .$value . "/" . $_FILES["Filedata"]["name"], array(
                    'fileUpload' => $this->user_home_dir."/".$value."/".$_FILES["Filedata"]["name"],
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }

            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();

            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),"public/system/posters/". $this->uid . "/" . $val . "/" . $_FILES['Filedata']['name']). PHP_EOL . PHP_EOL;
                }
            }

            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			return @$arr;
        }
    }

    public function uploadFiletoS3($bucket, $uploadpath = "uploads/videobanner-image/", $imgName = '', $uid, $bnr_id, $src_img = '') {
        //$this->_checkUpload();  //check if a file was uploaded
        //$this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        
        foreach ($this->config->getDimensions() as $key => $dimensions) {
            if (!is_dir($this->user_home_dir . "/" . $key)) {
                mkdir($this->user_home_dir . "/" . $key);
        }
            $this->hashed_file_name = $key;
            $dimensions = explode("x", $dimensions);
            $this->image->load($src_img);
            $this->image->resize($dimensions[0], $dimensions[1]);
            $this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $imgName);
            chmod($this->user_home_dir . "/" . $key . "/" . $imgName, 0775);
            $sizefolder[] = $key;
        }

        foreach ($sizefolder AS $k => $v) {}
        // Loop over the list, referring to a single file at a time
        foreach ($sizefolder as $value) {
            $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath . $uid . "/" . $value . "/" . $imgName, array(
                'fileUpload' => $this->user_home_dir . "/" . $value . "/" . $imgName,
                'acl' => AmazonS3::ACL_PUBLIC,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )
        ));
        }

        /* Execute our queue of batched requests. This may take a few seconds to a
          few minutes depending on the size of the files and how fast your upload
          speeds are. */
        $file_upload_response = $this->s3->batch()->send();

        /* Since a batch of requests will return multiple responses, let's
          make sure they ALL came back successfully using `areOK()` (singular
          responses use `isOK()`). */
        if ($file_upload_response->areOK()) {
            // Loop through the individual filenames
            foreach ($sizefolder as $val) {
                /* Display a URL for each of the files we uploaded. */
                $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(), $uploadpath . $uid . "/" . $val . "/" . $imgName) . PHP_EOL . PHP_EOL;
        }
        }

        //delete files on machine
        $this->deleteAll($this->user_home_dir);
        //echo "<pre>";print_r($arr);//exit;
        return $arr;
    } 
    public function uploadVideoThumbFiletoS3($bucket, $uploadpath = "uploads/videogallery-image/", $imgName = '', $studio_id) {
        //$this->_checkUpload();  //check if a file was uploaded
        //$this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if (!is_dir($this->user_home_dir)) {
            mkdir($this->user_home_dir);
        }

        $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath . $studio_id. "/" . $imgName, array(
            'fileUpload' => $this->user_home_dir . "/" . $imgName,
            'acl' => AmazonS3::ACL_PUBLIC
        ));
        $file_upload_response = $this->s3->batch()->send();

        /* Since a batch of requests will return multiple responses, let's
          make sure they ALL came back successfully using `areOK()` (singular
          responses use `isOK()`). */
        if ($file_upload_response->areOK()) {
            $arr[$val] = $this->s3->get_object_url($bucket, $uploadpath . $studio_id .  "/" . $imgName) . PHP_EOL . PHP_EOL;
        }

        //delete files on machine
        $this->deleteAll($this->user_home_dir);
        //echo "<pre>";print_r($arr);//exit;
        return $arr;
    } 
    
    public function uploadBanner() {
        //echo "test";
      //  print_r($_FILES);
      //  exit;
        $bnrname = Yii::app()->common->fileName($_FILES["Filedata"]["name"]);
        //echo $bnrname;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
      
        if (!is_dir($this->user_home_dir)) {
            mkdir($this->user_home_dir);
        }
       //echo $this->user_home_dir;
        foreach ($this->config->getDimensions() as $key => $dimensions) {
           // echo $this->user_home_dir . "/" . $key;
            if($key !='banner_original'){
				if (!is_dir($this->user_home_dir . "/". $key)){
					mkdir($this->user_home_dir . "/" . $key,0777,true);
				    
				 }
			}
            $this->hashed_file_name = $key;
            $dimensions = explode("x", $dimensions);
            //$this->user_home_dir . "original/" . $bnrname;
            if($key != 'thumb'){
		$this->image->load($this->user_home_dir."/original/".$bnrname);
            }else{
             $this->image->load($this->user_home_dir."/original1/".$bnrname);   
            }
            if($key != 'banner_original'){
                $this->image->resize($dimensions[0], $dimensions[1]);
                $this->image->save($this->user_home_dir ."/".$key."/" . $bnrname);
                chmod($this->user_home_dir . "/" . $key . "/" . $bnrname, 0777);
            }
             $sizefolder[] = $key;
        }
     //print_r($sizefolder);
     //exit;
        $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        // Loop over the list, referring to a single file at a time
        foreach ($sizefolder as $value) {
                if($value == 'banner_original')
                 {
                  $value='original';  
                 }
           // echo $this->config->getBucketName(), $unsignedBucketPath."public/system/studio_banner/" . $this->uid . "/" . $value . "/" . $bnrname;    
            $this->s3->batch()->create_object($this->config->getBucketName(), $unsignedBucketPath."public/system/studio_banner/" . $this->uid . "/" . $value . "/" . $bnrname, array(
                'fileUpload' => $this->user_home_dir . "/" . $value . "/" . $bnrname,
                'acl' => AmazonS3::ACL_PUBLIC,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )                 
            ));
        }

        /* Execute our queue of batched requests. This may take a few seconds to a
          few minutes depending on the size of the files and how fast your upload
          speeds are. */
        $file_upload_response = $this->s3->batch()->send();

        /* Since a batch of requests will return multiple responses, let's
          make sure they ALL came back successfully using `areOK()` (singular
          responses use `isOK()`). */
        if ($file_upload_response->areOK()) {
            // Loop through the individual filenames
            foreach ($sizefolder as $val) {
                
                if($val == 'banner_original')
                 {
                  $val='original';  
                 }
                /* Display a URL for each of the files we uploaded. */
                $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(), $unsignedBucketPath."public/system/studio_banner/" . $this->uid . "/" . $val . "/" . $bnrname) . PHP_EOL . PHP_EOL;
            }
        }

        //delete files on machine
       // $this->deleteAll($this->user_home_dir);
        //echo "<pre>";print_r($arr);exit;
        return $arr;
        // }
    }
    public function uploadBannerGallery($image_name,$studio_id) { 
    
        $bnrname =$image_name;
        //echo $bnrname;
        //$this->_checkUpload();  //check if a file was uploaded
        //$this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        $this->user_home_dir=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagemanagement/'.$studio_id.'/Banner-original';
        
        if (!is_dir($this->user_home_dir)) {
            mkdir($this->user_home_dir);
        }
      
        foreach ($this->config->getDimensions() as $key => $dimensions) {
           
            if (!is_dir($this->user_home_dir . "/". $key)) {
            mkdir($this->user_home_dir . "/" . $key);
            
           }
            $this->hashed_file_name = $key;
            $dimensions = explode("x", $dimensions);
            $this->image->load($this->user_home_dir."/original/".$bnrname);   
            
                $this->image->resize($dimensions[0], $dimensions[1]);
                $this->image->save($this->user_home_dir ."/".$key."/" . $bnrname);
                chmod($this->user_home_dir . "/" . $key . "/" . $bnrname, 0775);
           
             $sizefolder[] = $key;
            }
      
        $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        // Loop over the list, referring to a single file at a time
        foreach ($sizefolder as $value) {
                
            $this->s3->batch()->create_object($this->config->getBucketName(), $unsignedBucketPath."public/system/studio_banner/" . $this->uid . "/" . $value . "/" . $bnrname, array(
                'fileUpload' => $this->user_home_dir . "/" . $value . "/" . $bnrname,
                'acl' => AmazonS3::ACL_PUBLIC,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )                 
            ));
        }
        
       
        /* Execute our queue of batched requests. This may take a few seconds to a
          few minutes depending on the size of the files and how fast your upload
          speeds are. */
        $file_upload_response = $this->s3->batch()->send();

        /* Since a batch of requests will return multiple responses, let's
          make sure they ALL came back successfully using `areOK()` (singular
          responses use `isOK()`). */
        if ($file_upload_response->areOK()) {
            // Loop through the individual filenames
            foreach ($sizefolder as $val) {
        /* Display a URL for each of the files we uploaded. */
                $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(), $unsignedBucketPath."public/system/studio_banner/" . $this->uid . "/" . $val . "/" . $bnrname) . PHP_EOL . PHP_EOL;
            }
        }

        //delete files on machine
       // $this->deleteAll($this->user_home_dir);
        //echo "<pre>";print_r($arr);exit;
        return $arr;
        // }
    }
    
    
    public function uploadPosterGallery($image_name,$studio_id,$uid) { 
    
        $postername =$image_name;
        //echo $bnrname;
        //$this->_checkUpload();  //check if a file was uploaded
        //$this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        $this->user_home_dir=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagemanagement/'.$studio_id.'/poster';
        
        if (!is_dir($this->user_home_dir)) {
            mkdir($this->user_home_dir);
        }
      
        foreach ($this->config->getDimensions() as $key => $dimensions) {
           
            if (!is_dir($this->user_home_dir . "/". $key)) {
            mkdir($this->user_home_dir . "/" . $key);
            
           }
            $this->hashed_file_name = $key;
            $dimensions = explode("x", $dimensions);
            $this->image->load($this->user_home_dir."/original/".$postername);   
            
                $this->image->resize($dimensions[0], $dimensions[1]);
                $this->image->save($this->user_home_dir ."/".$key."/" . $postername);
                chmod($this->user_home_dir . "/" . $key . "/" . $postername, 0775);
           
             $sizefolder[] = $key;
            }
      
        $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        // Loop over the list, referring to a single file at a time
        foreach ($sizefolder as $value) {
                
            $this->s3->batch()->create_object($this->config->getBucketName(), $unsignedBucketPath."public/system/posters/" . $uid . "/" . $value . "/" . $postername, array(
                'fileUpload' => $this->user_home_dir . "/" . $value . "/" . $postername,
                'acl' => AmazonS3::ACL_PUBLIC,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )                 
            ));
        }
        
       
        /* Execute our queue of batched requests. This may take a few seconds to a
          few minutes depending on the size of the files and how fast your upload
          speeds are. */
        $file_upload_response = $this->s3->batch()->send();

        /* Since a batch of requests will return multiple responses, let's
          make sure they ALL came back successfully using `areOK()` (singular
          responses use `isOK()`). */
        if ($file_upload_response->areOK()) {
            // Loop through the individual filenames
            foreach ($sizefolder as $val) {
        /* Display a URL for each of the files we uploaded. */
                $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(), $unsignedBucketPath."public/system/posters/" . $uid . "/" . $val . "/" . $postername) . PHP_EOL . PHP_EOL;
            }
        }

        //delete files on machine
       // $this->deleteAll($this->user_home_dir);
        //echo "<pre>";print_r($arr);exit;
        return $arr;
        // }
    }
    
    public function uploadEpisodeGallery($image_name,$studio_id,$uid) { 
    
        $postername =$image_name;
        //echo $bnrname;
        //$this->_checkUpload();  //check if a file was uploaded
        //$this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        $this->user_home_dir=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagemanagement/'.$studio_id.'/episode';
        
        if (!is_dir($this->user_home_dir)) {
            mkdir($this->user_home_dir);
        }
      
        foreach ($this->config->getDimensions() as $key => $dimensions) {
           
            if (!is_dir($this->user_home_dir . "/". $key)) {
            mkdir($this->user_home_dir . "/" . $key);
            
           }
            $this->hashed_file_name = $key;
            $dimensions = explode("x", $dimensions);
            $this->image->load($this->user_home_dir."/original/".$postername);   
            
                $this->image->resize($dimensions[0], $dimensions[1]);
                $this->image->save($this->user_home_dir ."/".$key."/" . $postername);
                chmod($this->user_home_dir . "/" . $key . "/" . $postername, 0775);
           
             $sizefolder[] = $key;
            }
      
        $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        // Loop over the list, referring to a single file at a time
        foreach ($sizefolder as $value) {
                
            $this->s3->batch()->create_object($this->config->getBucketName(), $unsignedBucketPath."public/system/posters/" . $uid . "/" . $value . "/" . $postername, array(
                'fileUpload' => $this->user_home_dir . "/" . $value . "/" . $postername,
                'acl' => AmazonS3::ACL_PUBLIC,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )                 
            ));
        }
        
       
        /* Execute our queue of batched requests. This may take a few seconds to a
          few minutes depending on the size of the files and how fast your upload
          speeds are. */
        $file_upload_response = $this->s3->batch()->send();

        /* Since a batch of requests will return multiple responses, let's
          make sure they ALL came back successfully using `areOK()` (singular
          responses use `isOK()`). */
        if ($file_upload_response->areOK()) {
            // Loop through the individual filenames
            foreach ($sizefolder as $val) {
        /* Display a URL for each of the files we uploaded. */
                $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(), $unsignedBucketPath."public/system/posters/" . $uid . "/" . $val . "/" . $postername) . PHP_EOL . PHP_EOL;
            }
        }

        //delete files on machine
       // $this->deleteAll($this->user_home_dir);
        //echo "<pre>";print_r($arr);exit;
        return $arr;
        // }
    }
    
      function uploadManagedImages($fileinfo,$filename,$new_cdn_user)
    {
        $upload_file_name = $name = Yii::app()->common->fileName($filename);
        $studio_id= Yii::app()->common->getStudioId();
        //echo $studio_id;
        
    if(!$tempPath){
                $tempPath = $fileinfo['tmp_name'];
		}
		$_FILES["Filedata"] = $fileinfo;
                
        
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            //check for error
            echo "Return Code: " . $fileinfo["error"] . "<br />";
        } else {
            
            
                    if (!is_dir($this->user_home_dir."/thumb/"))
                        {			
                        mkdir($this->user_home_dir ."/thumb/");
                        
			}
         
              //  $this->hashed_file_name=$key;
               
                $this->image->load($this->user_home_dir."/".$filename);
                $dimensions=array(64,64);
               
                $this->image->resize($dimensions[0],$dimensions[1]);		
		//echo $this->user_home_dir ."/thumb/".$filename;
                
                $this->image->save($this->user_home_dir ."/thumb/".$filename);
                chmod($this->user_home_dir ."/thumb/".$filename, 0775);
                $sizefolder[]= $key;
            
            
            $sizefolder= array('original','thumb');
            $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
           
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $s_uploaded_path=$unsignedBucketPath."imagegallery/";// for new user
            if($new_cdn_user==0)//existing user
            {
               $s_uploaded_path=$s_uploaded_path.$studio_id."/"; 
            }
            
            $fileUpload=$this->user_home_dir."/".$filename;
            //echo $fileUpload;
           // exit;
            foreach ($sizefolder as $value)
                {
                if($value=='thumb')
                {
                 $fileUpload=  $this->user_home_dir."/thumb/".$filename;
                }
              // echo $fileUpload;
               
            
                $this->s3->batch()->create_object($this->config->getBucketName(),$s_uploaded_path . $value . "/" . $filename , array(
                    'fileUpload' => $fileUpload,
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
                }
           
            $file_upload_response = $this->s3->batch()->send();
            
          
            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$s_uploaded_path.$val ."/" .$upload_file_name ). PHP_EOL . PHP_EOL;
                }
            }
               
            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			return $arr;
        }    
        
    }
    
     function uploadManagedPosterImages($fileinfo,$filename,$new_cdn_user,$user_home_dir){
        $studio_id= Yii::app()->common->getStudioId();
		$this->image = new Image();
        $this->user_home_dir=$user_home_dir;
		if (!is_dir($this->user_home_dir)){			
			mkdir($this->user_home_dir);
		}
        mkdir($this->user_home_dir."/thumb/");
        $this->image->load($this->user_home_dir."/original/".$filename);
        $dimensions=array(64,64);
        $this->image->resize($dimensions[0],$dimensions[1]);		
		$this->image->save($this->user_home_dir ."/thumb/".$filename);
        chmod($this->user_home_dir ."thumb/".$filename, 0775);
        $sizefolder[]= $key;
        $sizefolder= array('thumb','original');
        $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $s_uploaded_path=Yii::app()->aws->getOrigialPathForGAllery($new_cdn_user,$studio_id);
        $fileUpload=$this->user_home_dir."/".$filename;
        foreach ($sizefolder as $value){
            $fileUpload=  $this->user_home_dir."/".$value."/".$filename;
			$this->s3->batch()->create_object($this->config->getBucketName(),$s_uploaded_path ."/". $value . "/" . $filename , array(
				'fileUpload' => $fileUpload,
                                'acl' => AmazonS3::ACL_PUBLIC,
                                'headers' => array(
                                    "Cache-Control" => "max-age=94608000",
                                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                                )                             
            ));
        }
        $file_upload_response = $this->s3->batch()->send();
        if ($file_upload_response->areOK()) {
			foreach ($sizefolder as $val) {
				$arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$s_uploaded_path."/".$val ."/" .$filename ). PHP_EOL . PHP_EOL;
            }
        }
		return $arr;
    }
    
    function uploadManagedBannerImages($fileinfo,$filename,$new_cdn_user,$user_home_dir)
    {
         
        $studio_id= Yii::app()->common->getStudioId();

        $this->image = new Image();
     
         echo $this->user_home_dir;
        $this->user_home_dir=$user_home_dir;
         echo $this->user_home_dir;
       
                    if (!is_dir($this->user_home_dir))
                        {			
                        mkdir($this->user_home_dir);
                        
			}
                        mkdir($this->user_home_dir."/thumb/");
              //  $this->hashed_file_name=$key;
               
                $this->image->load($this->user_home_dir."/original/".$filename);
                $dimensions=array(64,64);
               
                $this->image->resize($dimensions[0],$dimensions[1]);		
		
                $this->image->save($this->user_home_dir ."/thumb/".$filename);
                chmod($this->user_home_dir ."thumb/".$filename, 0775);
                $sizefolder[]= $key;
            
            
            $sizefolder= array('thumb','original');
            $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
            
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $s_uploaded_path=Yii::app()->aws->getOrigialPathForGAllery($new_cdn_user);
            
            $fileUpload=$this->user_home_dir."/".$filename;
           
            foreach ($sizefolder as $value)
                {
                 $fileUpload=  $this->user_home_dir."/".$value."/".$filename;
                // echo $s_uploaded_path."/" . $value . "/" . $filename;
                
                $this->s3->batch()->create_object($this->config->getBucketName(),$s_uploaded_path ."/". $value . "/" . $filename , array(
                    'fileUpload' => $fileUpload,
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
                }
            
          
            $file_upload_response = $this->s3->batch()->send();
            
          //print_r($file_upload_response);
          //exit;
            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$s_uploaded_path."/".$val ."/" .$filename ). PHP_EOL . PHP_EOL;
                }
            }

             // @unlink($this->user_home_dir."/thumb/".$filename);
             // @unlink($this->user_home_dir."/".$filename);
			return $arr;
         
        
    }
    
    
    public function uploadPoster($fileinfo,$uploadpath="public/system/posters/",$tempPath='') {
		if(!$tempPath){
			$tempPath = $fileinfo['tmp_name'];
		}
		$_FILES["Filedata"] = $fileinfo;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            //check for error
            echo "Return Code: " . $fileinfo["error"] . "<br />";
        } else {
			//echo $this->user_home_dir;exit;
            //create necessary directories
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
           /* if (!is_dir($this->user_home_dir . "/" . $this->hashed_file_name)) {
                mkdir($this->user_home_dir . "/" . $this->hashed_file_name);
            }*/
            //resize image for desired dimensions
            foreach ($this->config->getDimensions() as $key=> $dimensions) {
				if (!is_dir($this->user_home_dir . "/" . $key)) {
					mkdir($this->user_home_dir . "/" . $key);
				}
				$this->hashed_file_name=$key;
                $dimensions = explode("x", $dimensions);
                $this->image->load($tempPath);
                //Keeping the aspect ration constant irrespective of height
				//$this->image->resizeToWidth($dimensions[0]);
				//Keeping the dimension constant irrespective of aspect ration
				
				$this->image->resize($dimensions[0],$dimensions[1]);
                $this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $fileinfo["name"]);
                chmod($this->user_home_dir . "/" . $key . "/" . $fileinfo["name"], 0775);
				$sizefolder[]= $key;
            }
            //upload the image to dir
            //move_uploaded_file($fileinfo["tmp_name"], $this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg");
            //chmod($this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg", 0775);
            if (!is_dir($this->user_home_dir . "/original")) { 
                mkdir($this->user_home_dir. "/original");
            }
			copy($tempPath, $this->user_home_dir . "/original/".$fileinfo["name"]);
            chmod($this->user_home_dir . "/original/".$fileinfo["name"], 0775);
			$sizefolder[]= 'original';
			
            //uploading the files to the machine is needed, as image class
            //resizes images and can only save them to the machine.
            //then all files are moved in a batch to CDN
            //and deleted from the machine.
            //move the newly created files to CDN
		//	$sizefolder = array('original','thumb','standard','medium','crop_thumb','crop_medium','crop_standard');
           // $list_of_files = $this->filter_file_list(glob($this->user_home_dir . "/" . $this->hashed_file_name . '/*'));
            // Prepare to hold the individual filenames
           // $individual_filenames = array();
			foreach($sizefolder AS $k=>$v){
				
			}
            // Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                // Grab only the filename part of the path
                //$filename = explode(DIRECTORY_SEPARATOR, $file);
                //$filename = array_pop($filename);

                // Store the filename for later use
                //$individual_filenames[] = $filename;

                /* Prepare to upload the file to our new S3 bucket. Add this
                  request to a queue that we won't execute quite yet. */
                $fileName = Yii::app()->common->fileName($fileinfo['name']);
                $this->s3->batch()->create_object($this->config->getBucketName(),$uploadpath.$this->uid . "/" .$value . "/" . $fileName, array(
                    'fileUpload' => $this->user_home_dir."/".$value."/".$fileinfo["name"],
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }

            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();

            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadpath. $this->uid . "/" . $val . "/" . $fileName). PHP_EOL . PHP_EOL;
                }
            }

            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			return @$arr;
        }
    }
/**
 * @method public uploadtoImageGallery() It will upload a content image to image Gallery first
 * @author Gayadhar<support@muvi.com>
 * @return array Array of image paths
 */
    public function uploadToImageGallery($fileinfo,$uploadpath="imagegallery/",$tempPath='') {
		if(!$tempPath){
			$tempPath = $fileinfo['tmp_name'];
		}
		$_FILES["Filedata"] = $fileinfo;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            return false;
        } else {
			if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
           foreach ($this->config->getDimensions() as $key=> $dimensions) {
				if (!is_dir($this->user_home_dir . "/" . $key)) {
					mkdir($this->user_home_dir . "/" . $key);
					chmod($this->user_home_dir . "/" . $key,0755);
				}
				$this->hashed_file_name=$key;
                $dimensions = explode("x", $dimensions);
                $this->image->load($tempPath);
				$this->image->resize($dimensions[0],$dimensions[1]);
                $this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $fileinfo["name"]);
                chmod($this->user_home_dir . "/" . $key . "/" . $fileinfo["name"], 0775);
				$sizefolder[]= $key;
            }
            //upload the image to dir
            //move_uploaded_file($fileinfo["tmp_name"], $this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg");
            //chmod($this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg", 0775);
            if (!is_dir($this->user_home_dir . "/original")) { 
                mkdir($this->user_home_dir. "/original");
            }
			copy($tempPath, $this->user_home_dir . "/original/".$fileinfo["name"]);
            chmod($this->user_home_dir . "/original/".$fileinfo["name"], 0775);
			$sizefolder[]= 'original';
			
            // Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                $fileName = Yii::app()->common->fileName($fileinfo['name']);
                $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath .$value . "/" . $fileName, array(
                    'fileUpload' => $this->user_home_dir."/".$value."/".$fileinfo["name"],
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }

            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();

            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadpath. $this->uid . "/" . $val . "/" . $fileName). PHP_EOL . PHP_EOL;
                }
            }

            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			return @$arr;
        }
    }
	
    
   
	
	
    public function uploadepisodeThumb($uploadpath="public/system/posters/") {
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        /*if ($_FILES["Filedata"]["error"] > 0) {
            //check for error
            echo "Return Code: " . $_FILES["Filedata"]["error"] . "<br />";
        } else {*/
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
            //resize image for desired dimensions
            foreach ($this->config->getDimensions() as $key=> $dimensions) {
				if (!is_dir($this->user_home_dir . "/" . $key)) {
					mkdir($this->user_home_dir . "/" . $key);
				}
				$this->hashed_file_name=$key;
                $dimensions = explode("x", $dimensions);
                $this->image->load($this->user_home_dir . "/original/".$_FILES["Filedata"]['name']);
				//Keeping the dimension constant irrespective of aspect ration
				$this->image->resize($dimensions[0],$dimensions[1]);
                $this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $_FILES["Filedata"]["name"]);
                chmod($this->user_home_dir . "/" . $key . "/" . $_FILES["Filedata"]["name"], 0775);
				$sizefolder[]= $key;
            }
			$sizefolder[]= 'original';
			foreach($sizefolder AS $k=>$v){
				
			}
            $folderPath = Yii::app()->common->getFolderPath("",Yii::app()->common->getStudioId());
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            // Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                $this->s3->batch()->create_object($this->config->getBucketName(), $unsignedBucketPath.$uploadpath.$this->uid . "/" .$value . "/" . $_FILES["Filedata"]["name"], array(
                    'fileUpload' => $this->user_home_dir."/".$value."/".$_FILES["Filedata"]["name"],
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }

            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();

            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadpath. $this->uid . "/" . $val . "/" . $_FILES['Filedata']['name']). PHP_EOL . PHP_EOL;
                }
            }

            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			//echo "<pre>";print_r($arr);exit;
			return $arr;
       // }
    }
/**
 * @param type $uploadpath
 * @return string
 * @method public cropVideothumb() Crop the extracted thumbnail from video and upload it
 * @author GDR<support@muvi.com> 
 */
    public function cropVideothumb($uploadpath="public/system/posters/",$imgName='',$uid) {
        //$this->_checkUpload();  //check if a file was uploaded
        //$this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
		if (!is_dir($this->user_home_dir)) { 
			mkdir($this->user_home_dir);
		}
		//resize image for desired dimensions
		foreach ($this->config->getDimensions() as $key=> $dimensions) {
			if (!is_dir($this->user_home_dir . "/" . $key)) {
				mkdir($this->user_home_dir . "/" . $key);
			}
			$this->hashed_file_name=$key;
			$dimensions = explode("x", $dimensions);
			$this->image->load($this->user_home_dir . "/original/".$imgName);
			//Keeping the dimension constant irrespective of aspect ration
			$this->image->resize($dimensions[0],$dimensions[1]);
			$this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $imgName);
			chmod($this->user_home_dir . "/" . $key . "/" . $imgName, 0775);
			$sizefolder[]= $key;
		}
		$sizefolder[]= 'original';
		// Loop over the list, referring to a single file at a time
		foreach ($sizefolder as $value){
			$this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath.$uid . "/" .$value . "/" . $imgName, array(
				'fileUpload' => $this->user_home_dir."/".$value."/".$imgName,
				'acl' => AmazonS3::ACL_PUBLIC,
                                'headers' => array(
                                    "Cache-Control" => "max-age=94608000",
                                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                                )                             
			));
		}

		/* Execute our queue of batched requests. This may take a few seconds to a
		  few minutes depending on the size of the files and how fast your upload
		  speeds are. */
		$file_upload_response = $this->s3->batch()->send();

		/* Since a batch of requests will return multiple responses, let's
		  make sure they ALL came back successfully using `areOK()` (singular
		  responses use `isOK()`). */
		if ($file_upload_response->areOK()) {
			// Loop through the individual filenames
			foreach ($sizefolder as $val) {
				/* Display a URL for each of the files we uploaded. */
			   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),"public/system/posters/". $uid . "/" . $val . "/" . $imgName). PHP_EOL . PHP_EOL;
			}
		}

		//delete files on machine
		$this->deleteAll($this->user_home_dir);
		//echo "<pre>";print_r($arr);//exit;
		return $arr;
    }

    public function setActualDir($dir) {
        $this->actual_dir = $dir;
    }

    public function ls() {
        $bucket = $this->config->getBucketName();
        $response = $this->s3->get_object_list($bucket, array(
                    'prefix' => $this->actual_dir
                ));
        
        echo "List of " . $this->actual_dir . "/<br/><br/>";
        $dirs = array_map(array($this, 'determineDirs'), $response);
        $dirs = array_unique($dirs);
        //var_dump($dirs);
        foreach ($dirs as $entry) {
        	echo $entry . "<br/>";
        }
    }

    public function cd($dirname) {
        if ($dirname == "..") {
            $dirs = explode("/", $this->actual_dir);
            array_pop($dirs);
            $this->actual_dir = implode("/", $dirs);
        } else {
            if ($this->actual_dir == "") {
                $this->actual_dir = $dirname;
            } else {
                $this->actual_dir = $this->actual_dir . "/" . $dirname;
            }
        }
        setcookie("Uploader_dir", $this->actual_dir);
        echo $this->actual_dir;
    }

    public function mkdir($dirname) {
    if ($this->actual_dir == "") {
    		$dirname = $dirname;
    	} else {
    		$dirname = $this->actual_dir . "/" . $dirname;
    	}
        
        
        $response = $this->s3->create_object($this->config->getBucketName(), $dirname . '/', array(
    		'body' => '',
        	'length' => 0,
        	'acl' => AmazonS3::ACL_PUBLIC
		));
        
        if ($response->isOK()) {
            echo "Directory $dirname created";
        } else {
            echo "Directory $dirname already exists!";
        }
    }

    public function rm($name) {
    	if ($this->actual_dir == "") {
    		$path = $name;
    	} else {
    		$path = $this->actual_dir . "/" . $name;
    	}
	    $response = $this->s3->delete_object($this->config->getBucketName(), $path); 
	    
	    echo ($response->isOK()) ?  "Delete of $path successful" :  "Delete of $path failed";
        
    }
    
    public function getLink ($name) {
        if ($this->actual_dir == "") {
    		$path = $name;
    	} else {
    		$path = $this->actual_dir . "/" . $name;
    	}
            echo $this->s3->get_object_url($this->config->getBucketName(), $path);
    }
    
    public function uploadMobileImage($fileinfo,$uploadpath="public/system/mobileapp/",$tempPath='',$type) {
            if(!$tempPath){
                    $tempPath = $fileinfo['tmp_name'];
            }
		$_FILES["Filedata"] = $fileinfo;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            //check for error
            echo "Return Code: " . $fileinfo["error"] . "<br />";
        } else {
			//echo $this->user_home_dir;exit;
            //create necessary directories
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
           /* if (!is_dir($this->user_home_dir . "/" . $this->hashed_file_name)) {
                mkdir($this->user_home_dir . "/" . $this->hashed_file_name);
            }*/
            //resize image for desired dimensions
            foreach ($this->config->getDimensions() as $key=> $dimensions) {
				if (!is_dir($this->user_home_dir . "/" . $key)) {
					mkdir($this->user_home_dir . "/" . $key);
				}
				$this->hashed_file_name=$key;
                $dimensions = explode("x", $dimensions);
                $this->image->load($tempPath);
                //Keeping the aspect ration constant irrespective of height
				//$this->image->resizeToWidth($dimensions[0]);
				//Keeping the dimension constant irrespective of aspect ration
				
				$this->image->resize($dimensions[0],$dimensions[1]);
                $this->image->save($this->user_home_dir . "/" . $this->hashed_file_name . "/" . $fileinfo["name"]);
                chmod($this->user_home_dir . "/" . $key . "/" . $fileinfo["name"], 0775);
				$sizefolder[]= $key;
            }
            //upload the image to dir
            //move_uploaded_file($fileinfo["tmp_name"], $this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg");
            //chmod($this->user_home_dir . "/" . $this->hashed_file_name . "/original.jpg", 0775);
            if (!is_dir($this->user_home_dir . "/original")) { 
                mkdir($this->user_home_dir. "/original");
            }
			copy($tempPath, $this->user_home_dir . "/original/".$fileinfo["name"]);
            chmod($this->user_home_dir . "/original/".$fileinfo["name"], 0775);
			$sizefolder[]= 'original';
			
            //uploading the files to the machine is needed, as image class
            //resizes images and can only save them to the machine.
            //then all files are moved in a batch to CDN
            //and deleted from the machine.
            //move the newly created files to CDN
		//	$sizefolder = array('original','thumb','standard','medium','crop_thumb','crop_medium','crop_standard');
           // $list_of_files = $this->filter_file_list(glob($this->user_home_dir . "/" . $this->hashed_file_name . '/*'));
            // Prepare to hold the individual filenames
           // $individual_filenames = array();
			foreach($sizefolder AS $k=>$v){
				
			}
            // Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                // Grab only the filename part of the path
                //$filename = explode(DIRECTORY_SEPARATOR, $file);
                //$filename = array_pop($filename);

                // Store the filename for later use
                //$individual_filenames[] = $filename;

                /* Prepare to upload the file to our new S3 bucket. Add this
                  request to a queue that we won't execute quite yet. */
                $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath.$this->uid . "/".$type."/" .$value . "/" . $fileinfo["name"], array(
                    'fileUpload' => $this->user_home_dir."/".$value."/".$fileinfo["name"],
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }

            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();

            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadpath. $this->uid . "/".$type."/"  . $val . "/" . $_FILES['Filedata']['name']). PHP_EOL . PHP_EOL;
                }
            }

            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			return @$arr;
        }
    }
    
    public function uploadAttachments($filename,$fileinfo,$uploadpath="uploads/",$id_ticket,$bucket) {
        $tempPath = $fileinfo['tmp_name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $_FILES["Filedata"] = $fileinfo;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            echo "Return Code: " . $fileinfo["error"] . "<br />";
        } else {
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir,0777);
            }
            if($ext=="jpg" || $ext== "gif" || $ext=="png" || $ext=='jpeg'){
                $this->image->load($tempPath);
                $this->image->save($this->user_home_dir.'/'.$filename);
            }else
                @move_uploaded_file($fileinfo['tmp_name'], $this->user_home_dir.'/'.$filename);
            chmod($this->user_home_dir.'/'.$filename, 0775);
            //$sizefolder[]= $id_ticket;
            $this->s3->batch()->create_object($bucket, $uploadpath.$id_ticket."/".$filename, array(
                    'fileUpload' => $this->user_home_dir."/".$filename,
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                 
                ));
            $file_upload_response = $this->s3->batch()->send();
            if ($file_upload_response->areOK()) {
                $arr['url'] = CLOUDFRONT_URL.$uploadpath.$id_ticket."/";
            }
            //delete files on machine
            $this->deleteAll($this->user_home_dir);
			return @$arr;
        }
    }
    
    public function uploadEmailNotiImage($filename,$fileinfo,$uploadpath="email_noti_images/",$tempPath='',$type = 'email') {
        if(!$tempPath){
                $tempPath = $fileinfo['tmp_name'];
        }
        $_FILES["Filedata"] = $fileinfo;
        $this->_checkUpload();  //check if a file was uploaded
        $this->hashFilename();  //hash actual timestamp to be new file name
        $this->image = new Image();
        if ($fileinfo["error"] > 0) {
            //check for error
            echo "Return Code: " . $fileinfo["error"] . "<br />";
        } else {
            //create necessary directories
            if (!is_dir($this->user_home_dir)) { 
                mkdir($this->user_home_dir);
            }
            //upload the image to dir
            copy($tempPath, $this->user_home_dir . "/".$filename);
            chmod($this->user_home_dir . "/".$filename, 0775);
            $sizefolder[]= 'email_noti_images';
            // Loop over the list, referring to a single file at a time
            foreach ($sizefolder as $value){
                /* Prepare to upload the file to our new S3 bucket. Add this
                  request to a queue that we won't execute quite yet. */
                $this->s3->batch()->create_object($this->config->getBucketName(), $uploadpath.$this->uid . "/" . $filename, array(
                    'fileUpload' => $this->user_home_dir."/".$filename,
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                     
                ));
            }
            /* Execute our queue of batched requests. This may take a few seconds to a
              few minutes depending on the size of the files and how fast your upload
              speeds are. */
            $file_upload_response = $this->s3->batch()->send();
            /* Since a batch of requests will return multiple responses, let's
              make sure they ALL came back successfully using `areOK()` (singular
              responses use `isOK()`). */
            if ($file_upload_response->areOK()) {
                // Loop through the individual filenames
                foreach ($sizefolder as $val) {
                    /* Display a URL for each of the files we uploaded. */
                   $arr[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadpath. $this->uid . "/" . $filename). PHP_EOL . PHP_EOL;
                }
            }
            //delete files on machine
            $this->deleteAll($this->user_home_dir);
            return @$arr;
        }
        
    }
    
    public function uploadImagetoAws($fileinfo, $uploadPath = '', $source_path = '',$pgtype='') {
        if($pgtype=='banner'){
            $this->image = new Image();            
            $y = explode('original/', $source_path,-1);
            $folderpath = $y[0]."original/";
            foreach ($this->config->getDimensions() as $key=> $dimensions) {
                if (!is_dir($folderpath . "/" . $key)) {
                    mkdir($folderpath . "/" . $key, 0777);
                }            
                $dimensions = explode("x", $dimensions);
                $this->image->load($source_path);            
                $this->image->resize($dimensions[0],$dimensions[1]);
                $this->image->save($folderpath . "/" . $key . "/" . $fileinfo["name"]);
                chmod($folderpath . "/" . $key . "/" . $fileinfo["name"], 0775);
                $sizefolder[]= $key;
            }
            $fileName = $fileinfo['name'];
            $uploadPath = str_replace('original/', '', $uploadPath);
            foreach ($sizefolder as $value){               
                $this->s3->batch()->create_object($this->config->getBucketName(),$uploadPath. $value . "/" . $fileName, array(
                    'fileUpload' => $folderpath.$value."/".$fileName,
                    'acl' => AmazonS3::ACL_PUBLIC,
                    'headers' => array(
                        "Cache-Control" => "max-age=94608000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                    )                    
                ));
            }
            $file_upload_response = $this->s3->batch()->send();
            if ($file_upload_response->areOK()) {
                foreach ($sizefolder as $val) {
                   $res[$val] = $this->s3->get_object_url($this->config->getBucketName(),$uploadPath. $val . "/" . $fileName). PHP_EOL . PHP_EOL;
                }
            }
        }else{
            $fileName = $fileinfo['name'];
            $this->s3->batch()->create_object($this->config->getBucketName(), $uploadPath.$fileName, array(
                'fileUpload' => $source_path,
                'acl' => AmazonS3::ACL_PUBLIC,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )             
            ));

            $file_upload_response = $this->s3->batch()->send();

            if ($file_upload_response->areOK()) {
                $res = $this->s3->get_object_url($this->config->getBucketName(), $uploadPath.$fileName) . PHP_EOL . PHP_EOL;
            }
        }
        return @$res;
    }      
}

?>