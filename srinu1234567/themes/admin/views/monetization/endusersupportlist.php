<input type="hidden" class="data-count" value="<?php echo $data['count']?>" />
<style>
    a{
        cursor: pointer;
    }
</style>
<table class="table">
    <thead>
    <th>User</th>
    <th>Email</th>
    <th>Type</th>
    <?php if ($type != 'deleted') { ?>
    <th>Action</th>
    <?php } ?>
</thead>
<tbody>
    <?php if(isset($data) && $data['count'] && !empty($data['data'])){
			$user_list_arr=isset($data['delete_requested'])?$data['delete_requested']:array();
            foreach ($data['data'] as $values){
            ?>
        <tr>
            <td><?php echo $values['display_name'];?></td>
			<td><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $values['user_id'];?>');"><?php echo $values['email'];?></a> <?php if ($type == 'all' && in_array($values['user_id'],$user_list_arr)){?><div class="red font-12 m-t-10">Device Delete Requested</div><?php } ?></td>
            <td>
            <?php
                    switch ($type) {
                        case 'registrations':
                            if(isset($values['is_free']) && $values['is_free']){
                                echo 'Free User';
                            }else{
                                echo 'Free Registrations';
                            }
                            break;
                        case 'subscriptions':
                            echo 'Subscriber';
                            break;
                        case 'ppvusers':
                            echo 'PPV Subscriber';
                            break;
                        case 'cancelled':
                            if (isset($values['user_status']) && intval($values['user_status']) && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                                echo 'Cancelled';
                            }
                            break;
                        case 'deleted':
                            if (isset($values['user_status']) && $values['user_status']==0 && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                                echo 'Deleted';
                            }
                            break;
                        case 'device_switch':
                            echo 'Device Delete Requested';
                            break;
                        default :
                            if ($values['status']) {
                                echo 'Subscriber';
                            }elseif($values['ppv_subscription_id']){
                                echo 'PPV Subscriber';
                            }elseif (isset($values['user_status']) && intval($values['user_status']) && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                                echo 'Cancelled';
                            }
                            elseif (isset($values['user_status']) && $values['user_status']==0 && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                                echo 'Deleted';
                            }elseif(isset($values['is_free']) && $values['is_free']){
                                echo 'Free User';
                            }
                            else{
                                echo 'Free Registrations';
                            }
                            break;
                    }
            ?>
            </td>
            <td>
            <?php if ($type != 'deleted') { ?>
            
                <?php if ($type == 'cancelled') { ?>
                        <h5><a id="delete-<?php echo $values['user_id'];?>" onclick="deleteUser('<?php echo $values['user_id'];?>','delete-<?php echo $values['user_id'];?>');"><em class="icon-close"></em>&nbsp;&nbsp;Delete User</a></h5>
                <?php } else { 
                    if (isset($values['user_status']) && $values['user_status']==0 && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                        
                    } elseif (isset($values['user_status']) && intval($values['user_status']) && isset($values['is_deleted']) && intval($values['is_deleted'])) { ?>
                        <h5><a id="delete-<?php echo $values['user_id'];?>" onclick="deleteUser('<?php echo $values['user_id'];?>','delete-<?php echo $values['user_id'];?>');"><em class="icon-close"></em>&nbsp;&nbsp;Delete User</a></h5>
                    <?php } else { ?>
                        <h5><a href="javascript:void(0)" onclick="sendResetPasswordEmail('<?php echo $values['email'];?>')"><em class="icon-paper-plane"></em>&nbsp;&nbsp;Send reset password email</a></h5>
                        <h5><a href="javascript:void(0)" onclick="openEmailPopUp('<?php echo $values['email'];?>')"><em class="icon-envelope"></em>&nbsp;&nbsp;Update email address</a></h5>
                        <?php if(((isset($type) && $type == 'subscriptions') || (isset($values['status']) && $values['status'])) && ($type != 'ppvusers')) {?>
                        <h5><a id="cancel-<?php echo $values['user_id'];?>" onclick="cancelUserSubscription('<?php echo $values['user_id'];?>','cancel-<?php echo $values['user_id'];?>');"><em class="icon-close"></em>&nbsp;&nbsp;Cancel Subscription</a></h5>
                        <?php }?>
                        
                        <?php if ((isset($type) && in_array($type,array('all','registrations'))) && (isset($values['is_free']) && $values['is_free'])) { ?>
                            <h5><a id="delete-<?php echo $values['user_id'];?>" onclick="deleteFree('<?php echo $values['user_id'];?>','delete-<?php echo $values['user_id'];?>');"><em class="icon-close"></em>&nbsp;&nbsp;Delete Free Account</a></h5>
                        <?php } ?>

                        
                        <?php if($values['status'] || $values['ppv_subscription_id']){ ?>
        <!--                    <a href="javascript:void(0)">Refund</a>-->
                        <?php }?>
                    <?php } ?>
                <?php } ?>
        <?php } ?>
        <?php
            $delete = "delete-".$values['user_id'];
            switch ($type) {
                case 'registrations':
                    if(isset($values['is_free']) && $values['is_free']){
                    }else{
                        //echo '<h5><a href="javascript:void(0)" onclick="openEmailPopUp('.$values["email"].')"><em class="icon-close"></em>&nbsp;&nbsp;Delete Free Registration</a></h5>';
                        echo '<h5><a id="delete-'.$values["user_id"].'" onclick="deleteUser('.$values["user_id"].','.$delete.');"><em class="icon-close"></em>&nbsp;&nbsp;Delete Free Registration</a></h5>';
                    }
                    break;
                default :
                    if ($values['status']) {
                    }elseif($values['ppv_subscription_id']){
                    }elseif (isset($values['user_status']) && intval($values['user_status']) && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                    }
                    elseif (isset($values['user_status']) && $values['user_status']==0 && isset($values['is_deleted']) && intval($values['is_deleted'])) {
                    }elseif(isset($values['is_free']) && $values['is_free']){
                    }
                    else{
                        echo '<h5><a id="delete-'.$values["user_id"].'" onclick="deleteUser('.$values["user_id"].','.$delete.');"><em class="icon-close"></em>&nbsp;&nbsp;Delete Free Registration</a></h5>';
                    }
                    break;
            }
        
        ?>

        <h5 style="position:relative"><a href="javascript:void(0)" onclick="openDevicePopUp(<?php echo $values['user_id'];?>)"><em class="fa fa-television"></em><em class="icon-screen-smartphone" style="position: absolute;
        left: 8px;
        background:#fff ;
        top: 4px;
        font-size: 11px;"></em>&nbsp;&nbsp;Device Management</a></h5>
            </td>
            
        </tr>
    <?php }}else{?>
        <tr>
            <td colspan="4">No Record found!!!</td>
        </tr>
    <?php }?>
</tbody>
</table>

<!-- update device management modal part-->
<div id="devicePopup" class="modal fade" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog " >
        <div class="modal-content" id="Device-details">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4>Device Management</h4>

              <!--<a href="javascript:void(0)" class="btn btn-primary" onclick="AddDevice();">&nbsp;&nbsp;Add Device</a>-->          
              <input type="hidden" name="hidden_userid" value="" id="hidden_userid" />
         </div>
             <div class="modal-body">
                  <div class="row">
                      <div class="col-md-12">
                          <div id="device_list">
                            </div>
                      </div>
                  </div>
            </div>
             <div class="modal-footer">
                    <button id="sub-btn" class="btn btn-primary" onclick="return validateSubtitle();">Submit</button>
                    <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                </div>
        </div>
</div>
</div>
<!-- end of device management modal part-->

<!-- add device modal starts-->
<div id="newdevicePopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <form action="javascript:void(0);" method="post" name="new_device_form" id="new_device_form" data-toggle="validator">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Device</h4>
                </div>
                <div class="modal-body">
                    <h5 class="f-300">*You can add some more devices</h5>
                    <div class="form-group">
                        <div id="errors" class="help-block col-sm-12 error red" style="padding-bottom:15px;"></div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 toper  control-label" for="Device Name">Device Name:</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="data[Admin][Device_Name]" id="Device_Name" autocomplete="off" required="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="saveNewDevice()" id="saveDevice">Submit</button>
                </div>
            </div>
        </form>	
    </div>
</div>
<!--end of add device modal part-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/jsnew/custom.js"></script>
<script type="text/javascript">
    function deleteFree(id,el) {
        swal({
            title: "Delete Free Account",
            text: "Do you want to remove this free service from this account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('.loader').show();
            $.post('/user/deleteFreeByAdmin',{'user_id':id},function(res){
                $('.loader').hide();
                var data = $.parseJSON(res);
                if(data.responce){
                    location.reload();
                }else{
                    $('#error-msg').html(data.message);
                    $('#error-msg').parent().show();
                }
            });
        });
    }
	 function AddDevice(){
           $("#newdevicePopup").modal('show');
           $("#saveDevice").html('submit');
           $("#saveDevice").attr("disabled", false);
           $('label.error').remove();
           $('#errors').hide();
           $('#new_device_form')[0].reset();
           
    }
     function saveNewDevice(user_id){
         $("#saveDevice").html('Saving...');
         $("#saveDevice").attr("disabled", true);
         var validate = $("#new_device_form").validate({
            rules: {
            'data[Admin][Device_Name]': "required",
              }, 
            messages: {
            'data[Admin][Device_Name]': "Please Give Device Name",
             },
             errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        },
         });
         var x = validate.form();
         if (x){
             var url = "/monetization/AddDevicelist";
             var devicename = $.trim($('#Device_Name').val());
             var user_id = $.trim($('#hidden_userid').val());            
             $.post(url, {'devicename': devicename,'user_id':user_id}, function (res) {                   
                   $.post('/monetization/devicename',{user_id:user_id},function(res1){                       
                        $('#newdevicePopup').modal('hide');
                        $('#devicePopup').modal('show');
                        $('#device_list').html(res1);
                    });
               });
             };
    }
</script>