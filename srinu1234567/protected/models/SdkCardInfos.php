<?php

class SdkCardInfos extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'sdk_card_infos';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function insertCardInfos($studio_id,$user_id,$req,$ip_address){
        $this->studio_id = $studio_id;
        $this->user_id = $user_id;
        $this->card_holder_name = @$req['card_name'];
        $this->card_uniq_id = Yii::app()->common->generateUniqNumber();
        $this->card_name = @$req['nameoncard'];
        $this->exp_month = @$req['exp_month'];
        $this->exp_year = @$req['exp_year'];
        $this->card_last_fourdigit = @$req['card_last_fourdigit'];
        $this->auth_num = $req['auth_num'];
        $this->token = @$req['token'];
        $this->profile_id = @$req['profile_id'];
        $this->card_type = @$req['card_type'];
        $this->reference_no = @$req['reference_no'];
        $this->response_text = @$req['response_text'];
        $this->status = @$req['status'];
        $this->is_cancelled = 0;
        $this->ip = $ip_address;
        $this->created = new CDbExpression("NOW()");
        $this->save();
    }
}
