<div class="container center">
    <a class="brand" href="<?php echo Yii::app()->getbaseUrl(true); ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/studio-logo.png" alt="" /></a>

    <?php 
    $config = Yii::app()->common->getConfigValue(array('trial_period'), 'value');
    if(strtolower(Yii::app()->controller->getAction()->getId()) =='paymentgateway'){?>
    <h2 class="center cyan-bold">Start your <?php echo $config[0]['value']?>-day free trial!</h2>
    <?php if(isset(Yii::app()->user->created_at)){
            $expairy_date = date('F d, Y',(strtotime(Yii::app()->user->created_at)+13*24*60*60));
    }else{
            $expairy_date = date('F d, Y',(time()+13*24*60*60));
    }?>
    <h4>Your trial period expires on <?php echo $expairy_date;?>, you can easily cancel your account from inside the admin panel anytime,<br />
        you will not be charged anything if cancelled before expiry of your trial period.</h4>
    <?php }elseif(strtolower(Yii::app()->controller->getAction()->getId()) =='typeofcontent') {?>
    <h2 class="center cyan-bold">Select Content Type</h2>
    <h4>Muvi allows you to build a VOD platform like Netflix, YouTube or anything between.</h4>
    <?php }elseif(strtolower(Yii::app()->controller->getAction()->getId()) =='index'){?>
    <h2 class="center cyan-bold">Start your <?php echo $config[0]['value']?>-day free trial!</h2>
    <h4>Launch your Video Streaming Platform within minutes.</h4>
    <?php }elseif(strtolower(Yii::app()->controller->getAction()->getId()) =='signupterms'){?>
    <h2 class="center cyan-bold">Terms of Use</h2>
    <?php }elseif(strtolower(Yii::app()->controller->getAction()->getId()) =='success'){?>
    <h2 class="center cyan-bold">Congratulations! Your Muvi account is ready to use</h2>
    <?php }?>
</div>