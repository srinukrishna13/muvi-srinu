<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class clickHereApi extends CApplicationComponent {

    private $endpoint;
    private $apiClientId;
    private $apiClientPassword;
    private $apiContentType;
    private $response_url;

    const endpoint = 'https://xdn01.clickhere.com.mm/documents';
    const apiClientId = 'u/19/MUVI';
    const apiClientPassword = '65eLsGsa2BAhPvZte8MaY3cvbFw7kmh3w';
    const apiContentType = 'application/vnd.net.wyrls.Document-v3+json';

    public function __construct($endpoint = '', $apiClientId = '', $apiClientPassword = '', $apiContentType = '') {
        if ($endpoint != '')
            $this->endpoint = $endpoint;
        else {
            $this->endpoint = clickHereApi::endpoint;
        }
        if($apiClientId != '')
            $this->apiClientId = $apiClientId;
        else
            $this->apiClientId = clickHereApi::apiClientId;
        if($apiClientPassword != '')
            $this->apiClientPassword = $apiClientPassword;
        else
            $this->apiClientPassword = clickHereApi::apiClientPassword;
        if($apiContentType != '')
            $this->apiContentType = $apiContentType;
        else
            $this->apiContentType = clickHereApi::apiContentType;    
    }

    public function requestAPI($method = "POST", $apiEndpointUrl, $jsonEncodedParams, $headers) {
        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($jsonEncodedParams) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonEncodedParams);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($jsonEncodedParams)
                    $apiEndpointUrl = sprintf("%s?%s", $apiEndpointUrl, http_build_query($jsonEncodedParams));
        }
        curl_setopt($curl, CURLOPT_URL, $apiEndpointUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        if (HOST_IP == '127.0.0.1') {
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        }
        $result = curl_exec($curl);
        if (curl_error($curl)) {
            $responseInfo = curl_errno($curl);
            $result = curl_error($curl);
        } else {
            $responseInfo = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        }
        $response = array('code' => $responseInfo, 'response' => $result);
        return $response;
    }

    public function doRegistration($mobile_number, $message, $uniqueID = '', $getApiDetails = '') {
        $datetime = new DateTime(date('Y-m-d H:i:s'));
        $datet = $datetime->format(DateTime::ATOM);
        $method = 'POST';
        $now = date("Y-m-dTH:i:sO", time());
        $params = array(
            "message_id" => $this->randomUniqueKey(),
            "from" => "8302",
            "to" => $mobile_number,
            "content_type" => "text/plain",
            "body" => $message,
            "usagetype" => "MUVIREG.MO",
            "thread"=> $uniqueID,
            "date" => $datet,
            "delivery_receipt_url" => trim($getApiDetails['api_callback'])
        );
        $jsonEncodedParams = json_encode($params);
        
        $saveApiLog = ApiCallLog::model()->saveApiLog($getApiDetails['studio_id'], $uniqueID, 'doRegistration', $jsonEncodedParams);
        $signature = base64_encode(join(":", array(trim($getApiDetails['access_user']), trim($getApiDetails['access_password']))));
        $headers = array(
            "Authorization: Basic $signature",
            "Accept: /documents",
            "Date: " . $now,
            "Content-Type: " . $this->apiContentType,
            "Content-Length: " . strlen($jsonEncodedParams)
        );
        $response = $this->requestAPI($method, $getApiDetails['endpoint'], $jsonEncodedParams, $headers);
        return $response;
        exit();
    }
    
    /*
     * @params: $mobile_number, $message
     * @author: Ratikanta<developer@muvi.com>
     * @return: array
     * This method will call the Pull API to force user for payment
     */

    public function doPayment($mobile_number, $message, $uniqueID = '', $getApiDetails = '') {
        $datetime = new DateTime(date('Y-m-d H:i:s'));
        $datet = $datetime->format(DateTime::ATOM);
        $method = 'POST';
        $now = date("Y-m-dTH:i:sO", time());
        $params = array(
            "message_id" => $this->randomUniqueKey(),
            "from" => "8302",
            "to" => $mobile_number,
            "content_type" => "text/plain",
            "body" => $message,
            "usagetype" => "MUVIPULL.MO",
            "thread"=> $uniqueID,
            "date" => $datet,
            "delivery_receipt_url" => $getApiDetails['api_callback']
        );
        $jsonEncodedParams = json_encode($params);
        $saveApiLog = ApiCallLog::model()->saveApiLog($getApiDetails['studio_id'], $uniqueID, 'doPayment', $jsonEncodedParams);
        $signature = base64_encode(join(":", array($getApiDetails['access_user'], $getApiDetails['access_password'])));
        $headers = array(
            "Authorization: Basic $signature",
            "Accept: /documents",
            "Date: " . $now,
            "Content-Type: " . $this->apiContentType,
            "Content-Length: " . strlen($jsonEncodedParams)
        );
        $response = $this->requestAPI($method, $getApiDetails['endpoint'], $jsonEncodedParams, $headers);
        return $response;
        exit();
    }
    
    /*
     * @params: $mobile_number, $message
     * @author: Ratikanta<developer@muvi.com>
     * @return: array
     * This method will call the Pull API to force user for payment
     */

    public function doCheckPermissionToPlay($mobile_number, $message, $uniqueID, $getApiDetails) {
        
    $asset_detail = base64_encode(join("|", array($getApiDetails['movie_id'], $getApiDetails['title'])));
        $method = 'GET';
        $now = date("Y-m-dTH:i:sO", time());
        $params = array(
            'msisdn' => $mobile_number,
            'service' => 'muvi.muvi',
            "thread"=> $uniqueID,
            "usagetype" => "MUVIPERM",
            'asset_details' => $asset_detail
        );
        $jsonEncodedParams = json_encode($params);
        $saveApiLog = ApiCallLog::model()->saveApiLog($getApiDetails['studio_id'], $uniqueID, 'doCheckPermissionToPlay', $jsonEncodedParams);
        $signature = base64_encode(join(":", array($getApiDetails['access_user'], $getApiDetails['access_password'])));
        $headers = array(
            "Authorization: Basic $signature",
        );
        $response = $this->requestAPI($method, 'https://xdn01.clickhere.com.mm/muvi/apiv1/permission/', $params, $headers);
        return $response;
        exit();
    }
    
    /*
     * @params: $mobile_number, $message
     * @author: Ratikanta<developer@muvi.com>
     * @return: array
     * This method will deactivate user from any subscription plan
     */

    public function doDeactivate($mobile_number, $message,$uniqueID = '', $getApiDetails = '') {
        $datetime = new DateTime(date('Y-m-d H:i:s'));
        $datet = $datetime->format(DateTime::ATOM);
        $method = 'POST';
        $now = date("Y-m-dTH:i:sO", time());
        $params = array(
            "message_id" => $this->randomUniqueKey(),
            "from" => "8302",
            "to" => $mobile_number,
            "content_type" => "text/plain",
            "body" => $message,
            "usagetype" => "MUVIOFF.MO",
            "thread"=> $uniqueID,
            "date" => $datet,
            "delivery_receipt_url" => $getApiDetails['api_callback']
        );
        $jsonEncodedParams = json_encode($params);
        $saveApiLog = ApiCallLog::model()->saveApiLog($getApiDetails['studio_id'], $uniqueID, 'doDeactivate', $jsonEncodedParams);
        $signature = base64_encode(join(":", array($getApiDetails['access_user'], $getApiDetails['access_password'])));
        $headers = array(
            "Authorization: Basic $signature",
            "Accept: /documents",
            "Date: " . $now,
            "Content-Type: " . $this->apiContentType,
            "Content-Length: " . strlen($jsonEncodedParams)
        );
        $response = $this->requestAPI($method, $getApiDetails['endpoint'], $jsonEncodedParams, $headers);
        return $response;
        exit();
    }
    /*
     * @params: $mobile_number, $message
     * @author: Ratikanta<developer@muvi.com>
     * @return: array
     * This method will verify if the user has tried with his own mobile number
     */

    public function doVerify($mobile_number, $message, $uniqueID = '', $getApiDetails = '') {
        $datetime = new DateTime(date('Y-m-d H:i:s'));
        $datet = $datetime->format(DateTime::ATOM);
        $method = 'POST';
        $now = date("Y-m-dTH:i:sO", time());
        $params = array(
            "message_id" => $this->randomUniqueKey(),
            "from" => "8302",
            "to" => $mobile_number,
            "content_type" => "text/plain",
            "body" => $message,
            "usagetype" => "MUVIVERIF.MO",
            "thread"=> $uniqueID,
            "date" => $datet,
            "delivery_receipt_url" => $getApiDetails['api_callback']
        );
        $jsonEncodedParams = json_encode($params);
        $saveApiLog = ApiCallLog::model()->saveApiLog($getApiDetails['studio_id'], $uniqueID, 'doVerify', $jsonEncodedParams);
        $signature = base64_encode(join(":", array($getApiDetails['access_user'], $getApiDetails['access_password'])));
        $headers = array(
            "Authorization: Basic $signature",
            "Accept: /documents",
            "Date: " . $now,
            "Content-Type: " . $this->apiContentType,
            "Content-Length: " . strlen($jsonEncodedParams)
        );

        $response = $this->requestAPI($method, $getApiDetails['endpoint'], $jsonEncodedParams, $headers);
        return $response;
        exit();
    }
    
    public function getUniqueID() {
        $unique_id = md5(time() . rand(uniqid()));
        return $unique_id;
    }
    
    
    
    
    
    public function randomUniqueKey($length = 30){
        $string = base64_encode(openssl_random_pseudo_bytes($length));
        return $string;
    }
    
    public function doRegistration1($mobile_number, $message, $uniqueID = '', $getApiDetails = '') { echo '--test';
        $method = 'POST';
        $now = date("Y-m-dTH:i:sO", time());
        echo $now.'---2016-02-26T10:54:31+08:00';
        $params = array(
            "message_id" => $this->randomUniqueKey(),
            "from" => "8302",
            "to" => $mobile_number,
            "content_type" => "text/plain",
            "body" => $message,
            "usagetype" => "MUVIREG.MO",
            "date" => "2016-02-26T10:54:31+08:00",
            "thread" => $uniqueID,
            "delivery_receipt_url" => $getApiDetails['api_callback']
        );
        
        $jsonEncodedParams = json_encode($params);
        $saveApiLog = ApiCallLog::model()->saveApiLog($getApiDetails['studio_id'], $uniqueID, 'doRegistration', $jsonEncodedParams);
        $signature = base64_encode(join(":", array(trim($getApiDetails['access_user']), trim($getApiDetails['access_password']))));
        $headers = array(
            "Authorization: Basic $signature",
            "Accept: /documents",
            "Date: " . $now,
            "Content-Type: " . $this->apiContentType,
            "Content-Length: " . strlen($jsonEncodedParams)
        );
        //$response = $this->requestAPI($method, $this->endpoint, $jsonEncodedParams, $headers);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonEncodedParams);
        curl_setopt($curl, CURLOPT_URL, $getApiDetails['endpoint']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($curl);
        $responseInfo = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $response = array('code' => $responseInfo, 'response' => $result);
        print_r($response);
        exit();
    }
    
    public function doCheckPermissionToPlay1($mobile_number, $message) {

        //$this->endpoint = 'https://xdn01.clickhere.com.mm/muvi/apiv1/permission';
        //$this->apiClientId = 'u/19/MUVI';
        //$this->apiClientPassword = '65eLsGsa2BAhPvZte8MaY3cvbFw7kmh3w';
        //$this->apiContentType = 'application/vnd.net.wyrls.Document-v3+json';
        //$this->response_url = 'https://www.muvi.com/site/HandleClickhereResponse';
       $asset_detail = base64_encode('V00001' | 'Movietitle1');

        $method = 'GET';
        $now = date("Y-m-dTH:i:sO", time());

        $params = array(
            'msisdn' => $mobile_number,
            'service' => 'muvi.muvi',
            "thread"=> $uniqueID,
            "usagetype" => "MUVIPERM",
            'asset_details' => $asset_detail
        );

        $jsonEncodedParams = json_encode($params);

        $signature = base64_encode(join(":", array($this->apiClientId, $this->apiClientPassword)));
        $headers = array(
            "Authorization: Basic $signature",
        );

        $curl = curl_init(); 

        //$apiEndpointUrl = sprintf("%s?%s", $this->endpoint, http_build_query($params));
        $apiEndpointUrl = "https://xdn01.clickhere.com.mm/muvi/apiv1/permission/?msisdn=959453044694&service=muvi.muvi&asset_details=MTIzNHxNVVZJ";
        curl_setopt($curl, CURLOPT_URL, $apiEndpointUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);

        $result = curl_exec($curl);
        if (curl_error($curl)) {
            $responseInfo = curl_errno($curl);
            $result = curl_error($curl);
        } else {
            $responseInfo = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        }
        $response = array('code' => $responseInfo, 'response' => $result);
        print_r($response); exit;
        //return $response;
    }
}
