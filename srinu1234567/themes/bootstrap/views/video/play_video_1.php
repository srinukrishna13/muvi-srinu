<?php
$v = 7;
$randomVar =rand();
$studio_ad = (count($studio_ads) > 0)?$studio_ads[0]:array();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Playing Video</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) { document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>'); }</script>
                
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />          
        
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
        
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ads.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/vast-client.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.vast.js"></script>   
        
        
	<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar;?>" rel="stylesheet" type="text/css" />
	<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar;?>"></script>        
        
        
        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
        .video-js {height: 50%; padding-top: 48%;}
        .vjs-fullscreen {padding-top: 0px}  
        .RDVideoHelper{display: none;}
        video::-webkit-media-controls {
    display:none !important;
}
        </style>
    </head>
    <body>
        <div class="wrapper">
            <div class="videocontent">
                <video id="video_block" class="video-js moo-css vjs-default-skin" controls preload="auto" poster="" width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution;?>" } }}'>
                    <?php
                        foreach($multipleVideo as $key => $val){
                            echo '<source src="'.$fullmovie_path.'" data-res="'.$key.'" type="video/mp4" />';
                        }
                    ?>
                </video>
            </div> 
        </div>
        <div id="episode_block" class="episode_block">
            <a href="play_video_backup.php"></a>
        </div>

        <?php
        if (strtolower($movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower($movieData['content_type']);
        }
        ?>
       <?php
        if (strtolower($movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower($movieData['content_type']);
        }
        ?>
        <script>
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;

            $(document).ready(function () {
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });
                var player = videojs( 'video_block', { plugins : { resolutionSelector : {
                    force_types : [ 'video/mp4'],
                    default_res : "<?php echo $defaultResolution;?>"
                } } } );
                
                <?php
                if($mvstream->enable_ad == 1 && $mvstream->rolltype == 1 )
                {
                    if($studio_ad->ad_network_id == 1)
                    {
                        ?>
                        ad_url = "https://search.spotxchange.com/vast/2.00/<?php echo $studio_ad->channel_id?>?VPAID=0&content_page_url="+encodeURIComponent(window.location.href)+"&cb=" + Math.random() + "&player_width=640&player_height=400";
                        <?php
                    }
                    if($studio_ad->ad_network_id == 2)
                    {
                        ?>
                        ad_url = "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=<?php echo $studio_ad->channel_id?>"
                        <?php
                    }                    
                ?>
                        
                    var state = {};
                    var midrollPoint = 0;
                    player.ads();        
                    state.midrollPlayed = false;
                    player.on('timeupdate', function(event) {
                        console.log(state.midrollPlayed);
                      if (state.midrollPlayed) {
                        return;
                      }
                      else
                      {
                          console.log('ad started');
                          state.midrollPlayed = true;                            
                            player.vast({
                                skip: 5,
                                url : ad_url
                            });                           
                      }                        
                    }); 
                    
                    player.one('adended', function() {
                        // play your linear ad content, then when it's finished ...
                        player.ads.endLinearAdMode();
                        state.adPlaying = false;

                        console.log('ad ended');
                        player.pause(); 
                        $(".vjs-loading-spinner").show();
                        if (typeof player.getCurrentRes === "function") { 
                            var currentVideoResolution = player.getCurrentRes();
                        } else{
                            var currentVideoResolution =144;
                        }
                        var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                        $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data}, function (res) {
                            player.ads.endLinearAdMode();
                            player.src(res).play();
                            player.on("loadeddata", function () {
                                $(".vjs-loading-spinner").hide();
                            });
                        });                        
                                                      
                    });                    
                <?php
                }
                ?>
                
                
                var movie_id = "<?php echo $_REQUEST['movie'] ?>";
                var full_movie = "<?php echo $fullmovie_path ?>";
                var can_see = "<?php echo $can_see ?>";
                var back_url = "<?php echo $this->createAbsoluteUrl($ctype . '/' . $per_link); ?>";
                var is_mobile =  "<?php echo Yii::app()->common->isMobile(); ?>";
                var item_poster = "<?php echo $item_poster ?>";
                var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
                var wiki_data = "<?php echo $wiki_data ?>";
                var multipleVideoResolution = new Array();
                multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                if (full_movie != "") {
                    if(is_mobile !=0){
                        $("#video_block_html5_api").attr('poster', item_poster);
                        $("#video_block").attr('poster', item_poster);
                    }
                    videojs( 'video_block').ready(function () {
                        var is_restrict = "<?php echo $is_restrict ?>";
                        if (is_restrict == "1") {
                            alert("This Movie is not allowed to watch in your country.");
                            return;
                        } else {
                            if (can_see) {
                                if(is_mobile !=0){
                                  $(".vjs-loading-spinner").remove();
                                }
                                $(".vjs-tech").mousemove(function () {
                                    if (full_screen == true && show_control == false) {
                                        $("#video_block .vjs-control-bar").show();
                                        if (is_mobile == 0){
                                                $(".vjs-watermark").hide();
                                        }
                                        show_control = true;
                                        var timeout = setTimeout(function () {
                                            if (full_screen == true) {
                                                $("#video_block .vjs-control-bar").hide();
                                                $(".vjs-watermark").hide();
                                                show_control = false;

                                            }
                                        }, 10000);
                                        $(".vjs-control-bar").mousemove(function () {
                                            event.stopPropagation();
                                        }).mouseout(function () {
                                            event.stopPropagation();
                                        });
                                    } else {
                                        clearTimeout(timeout);
                                    }
                                });
                                videojs("video_block").on("fullscreenchange", resize_player);
                                var video_id = videojs("video_block");
                                video_id.watermark({
                                    file: "<?php echo Yii::app()->getBaseUrl(true)?>/common/images/back.png",
                                    xpos: 93,
                                    ypos: 88,
                                    xrepeat: 0
                                });
                                $(".vjs-watermark").hide();
                                $(".vjs-watermark").click(function () {
                                    window.history.back();
                                });
                                $("#video_block").mouseenter(function () {
                                    if (is_mobile == 0){
                                        $(".vjs-watermark").show();
                                    }
                                }).mouseleave(function () {
                                    $(".vjs-watermark").hide();
                                });
                                $(".vjs-watermark").mouseover(function () {
                                    event.stopPropagation();
                                }).mouseout(function () {
                                    event.stopPropagation();
                                });
                                play_video(full_movie, movie_id);
                                var player = videojs("video_block");
                                this.on('timeupdate', function() {
                                    previousTime = currentTime;
                                    currentTime = player.currentTime();
                                });
                                player.on( 'changeRes', function() {
                                    seekStart = previousTime;
                                    player.pause();
                                    $(".vjs-loading-spinner").show();
                                    var currTimeisisi = currentTime;
                                    var currentVideoResolution = player.getCurrentRes();
                                    var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                                    var video = document.getElementsByTagName("video")[0];
                                    $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data}, function (res) {
                                        player.src(res).play();
                                        player.on("loadeddata", function () {
                                            player.currentTime(currTimeisisi);
                                            $(".vjs-loading-spinner").hide();
                                        });
                                    });
                                });
                                $("video").on("seeking",function(){
                                    var bufferDuration = this.buffered.end(0);
                                    var currTim = player.currentTime();
                                    if(bufferDuration < currTim){
                                        if(seekStart === null) {
                                            player.pause(); 
                                            $(".vjs-loading-spinner").show();
                                            if (typeof player.getCurrentRes === "function") { 
                                                var currentVideoResolution = player.getCurrentRes();
                                            } else{
                                                var currentVideoResolution =144;
                                            }
                                            var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                                            $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data}, function (res) {
                                                seekStart = previousTime;
                                                player.src(res).play();
                                                player.on("loadeddata", function () {
                                                    player.currentTime(currTim);
                                                    $(".vjs-loading-spinner").hide();
                                                });
                                            });
                                        }
                                    }
                                });
                                $("video").on('seeked', function() {
                                    seekStart = null;
                                });
                            } else {
                                show_warning_video();
                            }
                        }
                    });
                } else {
                    //show_more_info(movie_id);
                }
            });
            function resize_player() {
                if (full_screen == false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen == true) {
                            show_control = false;
                            $(".vjs-watermark").hide();
                            $("#video_block .vjs-control-bar").hide();
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                    $("#video_block .vjs-control-bar").show();
                }
            }


            function show_warning_video() {
                alert('Seems like you have already watched 3 movies this month. Please come back next month to watch again.');
            }
            function play_video(video_link, movie_id) {
                var url = "<?php echo Yii::app()->baseUrl; ?>/video/add_log";
                var episode_id = "<?php echo isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0 ?>";
                var movie_id = "<?php echo $_REQUEST['movie'] ?>";
                var episode_number = "<?php echo $episode_number ?>";
                var content_type = "<?php echo $content_type ?>";
                var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
                var wiki_data = "<?php echo $wiki_data ?>";
                var is_mobile =  "<?php echo Yii::app()->common->isMobile(); ?>";
                    
                //video_link='http://www.w3schools.com/html/mov_bbb.mp4';
                $("#vid_more_info").hide();
                $("#episode_block").html("");
                $("#episode_block").hide();
                var main_video = videojs("video_block");
                
                
                
                //url : "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=1552hCkaKYjg"
                var multipleVideoResolution = new Array();
                multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;                
                var ad_url = '';
                <?php
                if($mvstream->enable_ad == 1)
                {
                ?>                 
                <?php
                    if($studio_ad->ad_network_id == 1)
                    {
                        ?>
                        ad_url = "https://search.spotxchange.com/vast/2.00/<?php echo $studio_ad->channel_id?>?VPAID=0&content_page_url="+encodeURIComponent(window.location.href)+"&cb=" + Math.random() + "&player_width=640&player_height=400";
                        <?php
                    }
                    if($studio_ad->ad_network_id == 2)
                    {
                        ?>
                        ad_url = "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=<?php echo $studio_ad->channel_id?>"
                        <?php
                    }
                    if($mvstream->rolltype == 1)
                    {
                    ?>
                    <?php
                    }
                    else 
                    {
                        $roll_after = 0;
                        $roll_parts = explode(':', $mvstream->roll_after);
                        if($roll_parts[0] > 0)
                            $roll_after += 3600*$roll_parts[0];
                        if($roll_parts[1] > 0)
                            $roll_after += 60*$roll_parts[1];
                        if($roll_parts[2] > 0)
                            $roll_after += $roll_parts[2];                        
                    ?>                                        
   var state = {};
    var midrollPoint = <?php echo $roll_after?>;
    
    
    main_video.on('timeupdate', function(event) {

      if (state.midrollPlayed) {
        return;
      }

      var currentTime = main_video.currentTime(), opportunity;

      if ('lastTime' in state) {
        opportunity = currentTime > midrollPoint && state.lastTime < midrollPoint;
      }

      state.lastTime = currentTime;
      if (opportunity) {
        state.midrollPlayed = true;

            main_video.ads();
            main_video.vast({
                skip: 5,
                url : ad_url
            });   
            
            main_video.one('adended', function() {
              // play your linear ad content, then when it's finished ...
              main_video.ads.endLinearAdMode();
              state.adPlaying = false;
            });

      }

    });      
    
                    main_video.one("readyforpreroll", function () {  
                         main_video.one('adend', function() {
                             console.log('ad ended');
                             main_video.pause(); 
                             $(".vjs-loading-spinner").show();
                             if (typeof main_video.getCurrentRes === "function") { 
                                 var currentVideoResolution = main_video.getCurrentRes();
                             } else{
                                 var currentVideoResolution =144;
                             }
                             var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                             $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data}, function (res) {
                                 main_video.src(res).play();
                                 main_video.on("loadeddata", function () {
                                     $(".vjs-loading-spinner").hide();
                                 });
                             });                        
                             main_video.ads.endLinearAdMode();
                         });      
                    });       
                    <?php
                    }
                }
                ?>                    
                
                
                
                if(is_mobile != 0){
                        main_video.pause(); 
                        $.post(createSignedUrl, {video_url: video_link, wiki_data: wiki_data}, function (res) {
                            main_video.src(res).play();
                        });
                }else{
                    //main_video.src(video_link);
                    if (video_link.indexOf('http://youtu') > -1) {
                        main_video.src({type: "video/youtube", src: "http://youtu.be/32xWXN6Zuio"});
                    } else {
                        main_video.src({type: "video/mp4", src: video_link});
                    }
                    //main_video.src({type: "video/mp4", src: video_link });
                    main_video.play();
                    main_video.on("loadeddata", function () {
                        $(".vjs-loading-spinner").hide();
                    });
                }
                main_video.on("play", function () {
                    $("#episode_block").hide();
                });
                var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";
                if (is_studio_admin != "true") {
                    main_video.on("loadstart", function () {
                        $.post(url, {movie_id: movie_id, episode_id: episode_id, status: "start"}, function (res) {
                        });
                    });
                    main_video.on("ended", function () {
                        $.post(url, {movie_id: movie_id, episode_id: episode_id, status: "complete"}, function (res) {
                        });
                        /*
                        if (content_type != 'movie') {
                            var next_epiurl = "<?php echo Yii::app()->baseUrl; ?>/video/get_next_episode";
                            $.post(next_epiurl, {movie_id: movie_id, episode_id: episode_id, episode_number: episode_number}, function (epi_res) {
                                $("#episode_block").html(epi_res);
                                $("#episode_block").show();
                            });
                        }*/
                    });
                }
            }

            function get_infoimg_height(percent) {
                var info_height = $("#vid_more_info").css("height");
                var img_px = parseInt(info_height) * (percent / 100);
                return img_px + "px";
            }

            function get_infoimg_width(percent) {
                var info_width = $("#vid_more_info").css("width");
                var img_px = parseInt(info_width) * (percent / 100);
                return img_px + "px";
            }
            function backto_info() {
                var trailer = videojs("trailer_block");
                trailer.pause();
                $("#trailer_div").hide();
                $("#vid_more_info").show();
            }

            function close_info() {
                $("#vid_more_info").hide();
                $("#wannasee_block").hide();
                //$(".random_msg").hide();
            }
            $('.vjs-loading-spinner').ready(function () {
            });
        </script>
    </body>
</html>
