<div class="row">
    <div class="col-xs-12">
        <a class="btn btn-primary btn-default m-t-10" data-toggle="modal" data-target="#NewKeyword">Add Keyword</a>&nbsp;
        <a class="btn btn-primary btn-default m-t-10 importlang" data-target="#langFileModal" data-toggle="modal">Import</a>&nbsp;
        <a class="btn btn-primary btn-default m-t-10 exportlang">Export</a>
    </div> 
</div>
<div class="row m-t-40 m-b-40">
    <div class="col-sm-11">
        <div class="message red">* Please do not change system variables such as $action, $registered, $movie_name, $date.</div>
        <form class="form-horizontal" method="post" name="translate" id="translate">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Language Key</th>
                        <th width="35%">Original (English)&nbsp;&nbsp <button class="btn btn-default-with-bg" type="button" onclick="Copytoall();">
                                <i class="fa fa-copy"></i>&nbsp;&nbsp;
                                Copy all&nbsp;&nbsp;
                                <i class="fa fa-long-arrow-right"></i>
                            </button></th>
                        <th width="45%">Translate to <?php echo $select_lang; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($trans_key as $trans_keys) { ?>
                        <tr class="trans_msg">
                            <td><?php echo $trans_keys->trans_key; ?></td>
                            <td class="original_word"><?php echo $trans_keys->trans_value; ?></td>
                            <td>
                                <?php
                                $translated_value = "";
                                foreach ($trans_val as $encode => $translated) {
                                    if ($trans_keys->trans_key == $encode) {
                                        $translated_value = $translated;
                                    }
                                }
                                if ($trans_keys->trans_type == '0') {
                                    $keyname = "static_message_key[]";
                                    $valuename = "static_message_value[]";
                                } elseif ($trans_keys->trans_type == '1') {
                                    $keyname = "js_message_key[]";
                                    $valuename = "js_message_value[]";
                                } elseif ($trans_keys->trans_type == '2') {
                                    $keyname = "server_message_key[]";
                                    $valuename = "server_message_value[]";
                                }
                                ?>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="fg-line">
                                            <input type="hidden" name="<?php echo $keyname; ?>" value="<?php echo $trans_keys->trans_key; ?>">
                                            <input type="text" name="<?php echo $valuename; ?>" value="<?php echo $translated_value; ?>" class="form-control input-sm tranlate_input">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <input type="button" class="btn btn-primary" id="save_translated_words" name="save_translated_words" value="Save">
        </form>
    </div>
</div>
<div class="modal fade" id="NewKeyword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h4 class="modal-title" id="myModalLabel">Add New Keyword</h4> 
            </div>
            <form name="new_keyword" id="new_keyword" class="form-horizontal" method="post" action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Language Key:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" name="translate_key" id="translate_key" class="form-control" placeholder="Language Key"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Original(English):</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" name="translate_word" id="translate_word" class="form-control" placeholder="Original(English)"/>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary add_new_word_to_translate">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="langFileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content relative">

            <div class="centerLoader-InDiv" id="loader" style="display:none;"> 
                <div class="preloader pls-blue">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                    </svg>
                </div>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Translation File</h4>
            </div>
            <div class="modal-body">
                <p>Note: Please make sure worksheet name is unchanged</p>
                <form name="lang_upload" id="lang_upload" method="post">  
                    <div id="message"></div>
                    <button type="button" class="btn btn-default-with-bg" onclick="click_browse('upload_lang_file')">Browse</button>
                    <span id="filename"> No file selected</span>
                    <input type="file" name="upload_lang_file" id="upload_lang_file" style="display:none;" />   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  
<style type="text/css">
    #save_translated_words{
        position:fixed;
        bottom:35px;
        right:10px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#save_translated_words').click(function () {
            swal({
                title: "Save translated Content",
                text: "Are you sure to save the translated content?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    var translate_all = checkAllField();
                    if (translate_all) {
                        $(".alert-dismissable").remove();
                        $('.tranlate_input').prop('type', 'text');
                        $('.trans_msg').removeAttr("style");
                        $.post(HTTP_ROOT + '/language/makeTranslation', $('#translate').serialize(), function (result) {
                        var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Language Content Updated Successfully.</div>'
                        $('.pace').prepend(sucmsg);
                        $('html, body').animate({scrollTop : 0},800);
                        return false; 
                        });
                    } else {
                        $('.tranlate_input').each(function () {
                            var translate_input = $(this).val();
                            translate_input = translate_input.trim();
                            if (translate_input != '') {
                                $(this).prop('type', 'hidden');
                                $(this).parent().parent().parent().parent().parent().hide();
                            }
                        });
                        $(".alert-dismissable").remove();
                        var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Please translate below messages and try submitting again.</div>'
                        $('.pace').prepend(sucmsg);
                    }
                } else {
                    return false;
                }
            });
        });
        $("#upload_lang_file").change(function(e) {
            e.preventDefault();
            $("#message").empty(); // To remove the previous error message
            var file = this.files[0];
            var filename = file.name;
            $("#filename").html(filename);
            var imagefile = file.type;
            var match = ["application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
            var aftermatch = inArray(imagefile, match);
            aftermatch = hasExtension(filename, ['.xls','.xlsx']);
            if (aftermatch === false)
            {
                $("#message").html("<span class='error red'>Please select a valid xls file.</p>");
                return false;
            }
            else
            {
                $('#loader').show();
                var form_data = new FormData();
                form_data.append('lang_file', file);
                $.ajax({
                    url: '<?php echo $this->createUrl('language/uploadLangfile/'); ?>',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(res) {
                        $('#loader').hide();
                        if (res.action == 'success') {
                            $('#langFileModal').modal('hide');
                            $(".alert-dismissable").remove();
                            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;'+res.message+'</div>'
                            $('.pace').prepend(sucmsg);
                            window.location.reload();
                        } else {
                            $('#message').html("<span class='error red'>" + res.message + "</span>");
                            $("#upload_lang_file").val("");
                        }
                    }
                });
                e.stopImmediatePropagation();
                return false;
            }
        });
    });
    function checkAllField() {
        var total_words_to_translate = <?php echo $total_words_encode; ?>;
        var translated_total = 0;
        $('.tranlate_input').each(function () {
            var translate_input = $(this).val();
            translate_input = translate_input.trim();
            if (translate_input != '') {
                translated_total++;
            }
        });
        if (translated_total >= total_words_to_translate) {
            return true;
        } else {
            return false;
        }
    }
    function Copytoall() {
        swal({
            title: "Copy All",
            text: "Are you sure to copy all the content of Original(English) to <?php echo $select_lang; ?> ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true
        },
        function () {
            $('.original_word').each(function () {
                var original_word = $(this).text();
                $(this).next().find('.tranlate_input').val(original_word);
                $('.trans_msg').removeAttr('style');
                $('.tranlate_input').prop('type', 'text');
            });
        });

    }
    $(document).ready(function(){
        $("#new_keyword").validate({ 
            rules: {    
                translate_key: {
                    required: true,
                },
                translate_word: {
                    required: true,
                }
            },errorPlacement: function(label, element) {
                label.addClass('red');
                label.insertAfter(element.parent());
            },submitHandler: function (form) {
                $.post(HTTP_ROOT+"/language/checkUniqueKey", {key: $('#translate_key').val()}, function(result){
                    if(result.trim() == "unique"){
                        $.post(HTTP_ROOT+"/language/newKeyword", $('#new_keyword').serialize(), function(result){
                            if(result.trim() == "success"){
                                $('#NewKeyword').modal('hide');
                                var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;New message added successfully.</div>'
                                $('.pace').prepend(sucmsg);
                                window.location.href=window.location.href;
                            }
                        }); 
                    }else{
                        $('<label class="error red">Please enter unique key</label>').insertAfter($('#translate_key').parent());
                        return false;
                    }
                }); 
            }         
        });
        $('a.exportlang').on('click',function(){
            window.location = HTTP_ROOT+"/language/getLanguageList";
        });
        $('a.importlang').on('click',function(){
            $("#message").empty();
            $("#filename").empty();
        });
    });
    function click_browse(modal_file) {
        $('#' + modal_file).click();
    }
    function inArray(needle, haystack) {
        var length = haystack.length;
        for (var i = 0; i < length; i++) {
            if (haystack[i] == needle)
                return true;
        }
        return false;
    }
    function hasExtension(fileName, exts) {
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }
</script>
