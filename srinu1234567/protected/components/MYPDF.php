<?php

ini_set("display_errors", 1);

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf_include.php');

// Extend the TCPDF class to create custom Header and Footer
//class MYPDF extends TCPDF {
class MYPDF extends CApplicationComponent {

    //Page header
    /* public function Header() {
      // Logo
      $image_file = 'muvi_studio_logo.png'; // *** Very IMP: make sure this image is available on given path on your server
      $this->Image($image_file,15,6,30);
      // Set font
      $this->SetFont('helvetica', 'C', 12);

      // Line break
      $this->Ln();
      $this->Cell(294, 15, 'Muvi Invoice', 0, false, 'C', 0, '', 0, false, 'M', 'M');
      // We need to adjust the x and y positions of this text ... first two parameters

      } */

    // Page footer
    public function Footer() {
        // Position at 25 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        $this->SetTextColor(39, 99, 135);
        $this->Cell(0, 0, 'Thank you for your business!', 0, 0, 'C');
        //$this->Ln();
        //$this->Cell(0,0,'www.muvi.com', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        // Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    public function getCommonPdf($data = array()) {
        $studio = $data['studio'];
        $isRenew = '';
        if (isset($data['isRenew']) && intval($data['isRenew'])) {
            $billing_period = $data['billing_period'];
            $isRenew = '<tr>
            <td width="500" align="left" colspan="2">' . $billing_period . '</td>
         </tr>
         <tr>
            <td width="500" align="left" colspan="2">&nbsp;</td>
         </tr>';
        }
        
        $customer_name = '';
        if (isset($data['user']) && !empty($data['user'])) {
            $user = $data['user'];
            $customer_name = (isset($user->last_name) && trim($user->last_name)) ? ucfirst($user->first_name).' '.ucfirst($user->last_name) : ucfirst($user->first_name);
            if (isset($user->address_1) && trim($user->address_1)) {
                $customer_name.='<br/>'.nl2br($user->address_1);
            }
        }
        
        if (isset($data['isUser']) && intval($data['isUser'])) {
            $studio_name = ucfirst($studio->name);
            $accountname = ucfirst($studio->name);
            $accountnumber = '';
        } else {
            $studio_name = 'Muvi';
            $accountname = ucfirst($studio->name);
            $accountnumber = $studio->id;
        }

        $billing_info_id = $data['billing_info_id'];
        $invoice_date = date('F d, Y');
        $amount_due = '$' . $data['billing_amount'];

        $html = <<<EOF
            <style>
                h1 {
                    color: #2B6E96;
                    font-family: helvetica;
                    font-size: 18pt;
                    text-align: center;
                }
                p.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                table.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second td {
                    color: #283240;
                    border-top: 1px solid #283240;
                    border-right: 1px solid #283240;
                }
                table.second td:first {
                    border-top: 0px solid #283240;
                }
                div.test {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                .strongbold{font-weight:bold;}
            </style>
            <h1 class="title">{$studio_name} Invoice</h1>
            <p class="first"><i>Greetings from {$studio_name}. We're writing to provide you with an electronic invoice for your use of {$studio_name}
                services. Additional information regarding your bill and your account 
                history are available on the Transaction interface. Email or call your {$studio_name} Account 
                Manager in case of any questions your account or bill.
            </i></p>

            <br />
            <table class="first" cellpadding="2">
            {$isRenew}
             <tr>
                <td width="120" align="left" valign="top">Customer:</td>
                <td width="380" align="left">{$customer_name}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Name:</td>
                <td width="380" align="left">{$accountname}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Number:</td>
                <td width="380" align="left">{$accountnumber}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Number:</td>
                <td width="380" align="left">{$billing_info_id}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Date:</td>
                <td width="380" align="left">{$invoice_date}</td>
             </tr>
             <tr>
                <td width="120" align="left"><b>Total Amount:</b></td>
                <td width="380" align="left"><b>{$amount_due}</b></td>
             </tr>
            </table>
            <br />
            <br /> 
            <br /> 
            <br />
EOF;
        return $html;
    }

    public function generatePdf($data = array()) {
        ob_clean();
        
        $account_name = '';
        if (isset($data['isReseller']) && intval($data['isReseller'])) {
            $account_name = $data['account_name'];
        } else {
        $studio = $data['studio'];
            $account_name = (isset($data['isUser']) && intval($data['isUser'])) ? ucfirst($studio->name) : 'Muvi';
        }
        
        $html = $data['html'];
        $isEmail = (isset($data['isEmail'])) ? $data['isEmail'] : 1;
        $billing_date = (isset($data['billing_date'])) ? $data['billing_date'] : date('F_d_Y');
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle($account_name . ' Invoice');
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80, 80, 80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();

        $pdf->writeHTML($html, true, false, true, false, '');

        $file_name = 'Invoice_details_' . str_replace(" ", '', ucwords(strtolower($account_name))) . '_' . $billing_date . '.pdf';
        ob_flush();

        if (intval($isEmail)) {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
        } else {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'I');
        }

        return $file_name;
    }

    public function getCommonFooter($html) {
        $html = <<<EOF
            {$html}
            <p class="first">
                Your Admin interface: <a href="https://www.muvi.com" target="_blank" style="text-decoration: none;">www.muvi.com</a>
            </p>
                <p class="first">Address:<br/>
                 6H Legacy Ln<br/>
                 Halfmoon,
                 NY 12065<br/>Phone: 860-468-9832<br/>Fax: (267)430-2476<br/>Email: <a href="mailto:billing@muvi.com" target="_blank" style="text-decoration: none;">billing@muvi.com</a>                   
               </p>
EOF;
        return $html;
    }

    public function adminRaiseInvoiceDetial($data = array()) {
        $html = $this->getCommonPdf($data);
        $billing_dtails = BillingDetails::model()->findByAttributes(array('billing_info_id' => $data['billing_info_id']));
        
        $bandwidth_detail = $billing_dtails->description;
        if (isset($data['bandwidth']) && !empty($data['bandwidth'])) {
            $bandwidth_detail = $this->getBandwidthDetail($data['bandwidth'], $data['bandwidth_type']);
        }
        
        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="center"><b>Title</b></td>
                <td width="250" align="center"><b>Description</b></td>
                <td width="100" align="center"><b>Amount</b></td>
             </tr>
            <tr>
                <td width="150" align="left">' . $billing_dtails->title . '</td>
                <td width="250" align="left">' . $bandwidth_detail . '</td>
                <td width="100" align="right">$' . $billing_dtails->amount . '</td>
            </tr>
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $billing_dtails->amount . '</b></td>
             </tr>
        </table>';

        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }
    
    public function adminRaiseInvoiceDetialOfBandwidthStorage($data = array(),$invoice = array()) {
        $html = $this->getCommonPdf($data);
        //$billing_dtails = BillingDetails::model()->findByAttributes(array('billing_info_id' => $data['billing_info_id']));
        
        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="center"><b>Title</b></td>
                <td width="250" align="center"><b>Description</b></td>
                <td width="100" align="center"><b>Amount</b></td>
             </tr>';
        
        if (isset($invoice['bandwidth']['bandwidth']) && !empty($invoice['bandwidth']['bandwidth'])) {
            $bandwidth_detail = $this->getBandwidthDetail($invoice['bandwidth']['bandwidth'], $invoice['bandwidth']['bandwidth_type']);
            $html.='<tr>
                <td width="150" align="left">' . $invoice['bandwidth']['title'] . '</td>
                <td width="250" align="left">' . $bandwidth_detail . '</td>
                <td width="100" align="right">$' . $invoice['bandwidth']['bandwidth_amount'] . '</td>
            </tr>';
        }
        
        if(isset($invoice['storage']) && !empty($invoice['storage'])){
            $storage_detail = $this->getStorageDetail($invoice['storage']['storage']);
            $html.='<tr>
                <td width="150" align="left">' . $invoice['storage']['title'] . '</td>
                <td width="250" align="left">' . $storage_detail . '</td>
                <td width="100" align="right">$' . $invoice['storage']['storage_amount'] . '</td>
            </tr>';
        }
        $html.='<tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $data['billing_amount'] . '</b></td>
             </tr>
             </table>';
        
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }

    public function adminSubscriptionReactivationInvoiceDetial($data = array()) {
        $html = $this->getCommonPdf($data);
        
        $app_price = Yii::app()->general->getAppPrice($data['package_code']);
        $billing_amount = $data['billing_amount'];
        $package = $data['package_name'];
        if (trim($data['applications'])) {
            $package.=' with ' . $data['applications'];
        }

        $package.='<br/>Plan: ' . $data['plan'].'ly';
        $package.='<br/>' . $data['from_to_date'];
        $base_price = $data['base_price'];
        $no_of_applications = trim($data['no_of_applications']) ? $data['no_of_applications'] : 0;
        $discount = '';

        if ($data['plan'] == 'Year') {
            $yearly_discount = $data['yearly_discount'];
            if (intval($no_of_applications)) {
                $totalprice = number_format((float) ($base_price + (($no_of_applications - 1) * $app_price)) * 12, 2, '.', '');
            } else {
                $totalprice = number_format((float) ($base_price) * 12, 2, '.', '');
            }
            
            $discount_price = number_format((float) ($totalprice * $yearly_discount) / 100, 2, '.', '');
            
            if (intval($no_of_applications)) {
                $price = "(" . $base_price . " + (".($no_of_applications - 1)." * " . $app_price . ") * 12) = $" . $totalprice;
            } else {
                $price = $base_price . " * 12 = $" . $totalprice;
            }
            
            $discount = '<tr>
                <td width="150" align="left">Discount on platform</td>
                <td width="250" align="left">' . $yearly_discount . '%</td>
                <td width="100" align="right">$' . $discount_price . '</td>
             </tr>';
        } else {
            if (intval($no_of_applications)) {
                $totalprice = number_format((float) $base_price + (($no_of_applications - 1) * $app_price), 2, '.', '');
                $price = $base_price . " + (".($no_of_applications -1)." * " . $app_price . ") = $" . $totalprice;
            } else {
                $totalprice = number_format((float) $base_price, 2, '.', '');
                $price = "$" . $totalprice;
            }
        }

        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="left">Platform</td>
                <td width="250" align="left">' . $package . '</td>
                <td width="100" align="right">' . $price . '</td>
             </tr>
             ' . $discount . '
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $billing_amount . '</b></td>
             </tr>
        </table>';
        
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }
    
     public function adminPurchaseDevhourInvoiceDetail($data = array()) {
        $html = $this->getCommonPdf($data);
        
        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="left">DevHours Purchase Fee</td>
                <td width="250" align="left">$'.$data['billing_amount'].' for '.$data['hours_purchased'].' hours</td>
                <td width="100" align="right">$'.$data['billing_amount'].'</td>
             </tr>
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$'.$data['billing_amount'].'</b></td>
             </tr>
        </table>';
        
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }
    
    public function getBandwidthDetail($data = array(), $isBollean = 0, $bandwidth_period = '') {
        $html = '';
        if (isset($data) && !empty($data)) {
            $html = '<table>';
            if (intval($isBollean)) {
                $total_bandwidth = @$data['total_bandwidth_consumed'];
                    $html.='<tr>
                           <td>Total bandwidth consumed: '.$total_bandwidth.' TB</td>
                        </tr>';
                $html.='<tr>
                           <td>&nbsp;</td>
                        </tr>';

                $free_usage = number_format((float) (1), 3, '.', '');

                    $html.='<tr>
                           <td>Total free usage: '.$free_usage.' TB</td>
                        </tr>';
                $html.='<tr>
                           <td>&nbsp;</td>
                        </tr>';

                $billable = number_format((float) ($total_bandwidth - $free_usage), 3, '.', '');

                $html.='<tr>
                           <td>Billable usage: '.$billable.' TB</td>
                        </tr>';
                $html.='<tr>
                           <td>&nbsp;</td>
                        </tr>';

                $grand_total = number_format((float) $data['price'], 2, '.', '');

                    $html.='<tr>
                           <td><b>Bandwidth cost at the rate of $'.BANDWIDTH_CHARGE.' per GB: $'.$grand_total.'</b></td>
                        </tr>';

                        $html.='<tr>
                           <td>Bandwidth Period '.$bandwidth_period.'</td>
                        </tr>';
                
            } else {
                $price = number_format((float) $data['price'], 2, '.', '');
                
                $html.='<tr>
                           <td>Total hour consumed: '.$data['hour'].' hr</td>
                        </tr>';
                $html.='<tr>
                           <td>$0.01 per hour on '.$data['hour'].' hr: <b>$'.$price.'</b></td>
                        </tr>';
                $html.='<tr>
                           <td>Total Hour Period '.$bandwidth_period.'</td>
                        </tr>';
            }
            
            $html.='</table>';
        }
        
        return $html;
    }
    
    public function getStorageDetail($data = array()) {
        $html = '';
        
        if (isset($data['cost']) && !empty($data['cost']) && (abs($data['cost']) > 0.01)) {
            $total_storage = $data['total_storage'];
            
            $bandwidth = '';
            if (isset($data['bandwidth']) && abs($data['bandwidth']) > 0) {
                $free_storage = $data['bandwidth'];
                $bandwidth = " Bandwidth";
            } else {
                $free_storage = $data['free_storage'];
            }
            
            $billable_storage = number_format((float) ($total_storage - $free_storage), 3, '.', '');
            
            $html = '<table>';
                $html.='<tr>
                           <td>Total storage used: '.$data['total_storage'].' TB</td>
                        </tr>';
                $html.='<tr>
                           <td>Free storage: '.$free_storage.' TB '.$bandwidth.'</td>
                        </tr>';
                $html.='<tr>
                           <td>Chargeable storage = '.$billable_storage.' TB ('.$total_storage.' TB - '.$free_storage.' TB '.$bandwidth.')</td>
                        </tr>';
                $html.='<tr>
                           <td>&nbsp;</td>
                        </tr>';
                $html.='<tr>
                           <td><b>Storage cost at the rate of $'.STORAGE_CHARGE.' per GB: $'.$data['cost'].'</b></td>
                        </tr>';
                
            $html.='</table>';
        }
        
        return $html;
    }
    
    public function adminMonthlyYearlyRenewInvoiceDetial($data = array()) {
        if (isset($data['isReseller']) && intval($data['isReseller'])) {
            $html = '';
        } else {
            $html = self::getCommonPdf($data);
        }
        
        $app_price = Yii::app()->general->getAppPrice($data['package_code']);
        $billing_amount = $data['billing_amount'];
        $package = $data['package_name'];
        if (trim($data['applications'])) {
            $package.=' with ' . $data['applications'];
        }

        $package.='<br/>Plan: ' . $data['plan'].'ly';
        $package.='<br/>' . $data['billing_period'];
        $base_price = $data['base_price'];
        $no_of_applications = $data['no_of_applications'];
        $discount = '';

        $discount_price = 0;

        //Calculate total price and yearly discount
        if ($data['plan'] == 'Year') {
            $yearly_discount = $data['yearly_discount'];
            if (intval($no_of_applications) > 0) {
                $totalprice = number_format((float) ($base_price + (($no_of_applications - 1) * $app_price)) * 12, 2, '.', '');
                $discount_price = number_format((float) ($totalprice * $yearly_discount) / 100, 2, '.', '');
                $price = "(" . $base_price . " + (".($no_of_applications - 1)." * " . $app_price . ") * 12) = $" . $totalprice;
            } else {
                $totalprice = number_format((float) ($base_price) * 12, 2, '.', '');
                $discount_price = number_format((float) ($totalprice * $yearly_discount) / 100, 2, '.', '');
                $price = "(" . $base_price . " * 12) = $" . $totalprice;
            }
            
            if (isset($data['is_bandwidth_storage_only']) && intval($data['is_bandwidth_storage_only'])) {
                
            } else {
                $discount = '<tr>
                    <td width="150" align="left">Discount on platform</td>
                    <td width="250" align="left">' . $yearly_discount . '%</td>
                    <td width="100" align="right">(-) $' . $discount_price . '</td>
                 </tr>';
            }
        } else {
            if (intval($no_of_applications) > 0) {
                $totalprice = number_format((float) $base_price + (($no_of_applications - 1) * $app_price), 2, '.', '');
                $price = $base_price . " + (".($no_of_applications - 1)." * " . $app_price . ") = $" . $totalprice;
            } else {
                $totalprice = number_format((float) $base_price, 2, '.', '');
                $price = "$" . $totalprice;
            }
        }

        //Calculate bandwidth charge if exists
        $isBandwidth = $data['isBandwidth'];
        $bandwidth = '';
        if (intval($isBandwidth)) {
            $bandwidth_charge = $data['bandwidth_charge'];
            if (abs($bandwidth_charge) > 0.00001) {
                $bandwidth_detail = $this->getBandwidthDetail($data['bandwidth'], $data['bandwidth_type'], $data['bandwidth_period']);
                
                if (isset($data['package_code']) && $data['package_code'] == 'muvi_studio_on_premise') {
                    $cdn = ' using your CDN';
                } else {
                    $cdn = ' using Muvi CDN';
                }
                
                $bandwidth = '<tr>
                    <td width="150" align="left">Bandwidth Charge'.$cdn.'</td>
                    <td width="250" align="left">'.$bandwidth_detail.'</td>
                    <td width="100" align="right">$' . $bandwidth_charge . '</td>
                 </tr>';
            }
        }
        
        //Calculate storage charge if exists
        $isStorage = $data['isStorage'];
        $storage = '';
        if (intval($isStorage)) {
            $storage_charge = $data['storage_charge'];
            if (abs($storage_charge) > 0.00001) {
                $storage_detail = $this->getStorageDetail($data['storage']);
                if (isset($data['package_code']) && $data['package_code'] == 'muvi_studio_on_premise') {
                    $cdn = ' using your CDN';
                } else {
                    $cdn = ' using Muvi CDN';
                }
                
                $storage = '<tr>
                    <td width="150" align="left">Storage Charge'.$cdn.'</td>
                    <td width="250" align="left">'.$storage_detail.'</td>
                    <td width="100" align="right">$' . $storage_charge . '</td>
                 </tr>';
            }
        }
        
        //Calculate Drm price if enabled
        $isDrm = @$data['isDrm'];
        $drm = '';
        if (intval($isDrm) && (abs($data['web_view_price'])> 0.00 || abs($data['android_view_price'])> 0.00 || abs($data['ios_view_price'])> 0.00)) {
            $drm_detail = '<table>';             
                if($data['no_of_web_view'] != '' && $data['no_of_web_view'] > 0){
                    $drm_detail.='<tr>
                               <td>DRM cost at the rate of $'.$data['drm_price_per_web_view'].' per web view(s) ('.$data['no_of_web_view'].'): $'.$data['web_view_price'].'</td>
                            </tr>'; 
                }
                if($data['no_of_android_view'] != '' && $data['no_of_android_view'] > 0){
                    $drm_detail.='<tr>
                               <td>DRM cost at the rate of $'.$data['drm_price_per_android_view'].' per andriod view(s) ('.$data['no_of_android_view'].'): $'.$data['android_view_price'].'</td>
                            </tr>';
                }
                if($data['no_of_ios_view'] != '' && $data['no_of_ios_view'] > 0){
                    $drm_detail.='<tr>
                               <td>DRM cost at the rate of $'.$data['drm_price_per_ios_view'].' per ios view(s) ('.$data['no_of_ios_view'].'): $'.$data['ios_view_price'].'</td>
                            </tr>'; 
                }
                if($data['no_of_raku_view'] != '' && $data['no_of_raku_view'] > 0){
                    $drm_detail.='<tr>
                               <td>DRM cost at the rate of $'.$data['drm_price_per_raku_view'].' per ios view(s) ('.$data['no_of_raku_view'].'): $'.$data['raku_view_price'].'</td>
                            </tr>'; 
                }                
                 $drm_detail.='<tr>
                           <td>&nbsp;</td>
                        </tr>';                
                $drm_detail.='<tr>
                           <td><b>Total DRM cost : $'.$data['drm_price'].'</b></td>
                        </tr>';     
            $drm_detail.='</table>';
            
            $drm = '<tr>
                <td width="150" align="left">Studio-approved DRM</td>
                <td width="250" align="left">'.$drm_detail.'</td>
                <td width="100" align="right">$' . $data['drm_price'] . '</td>
             </tr>';
        }
        
        //Calculate Muvi Kart price if enabled
        $isMuviKart = @$data['isMuviKart'];
        $muvi_kart = '';
        if (intval($isMuviKart)) {
            $trnsstr = 'Transaction amount:';
            if (isset($data['amount_in_currency']) && !empty($data['amount_in_currency']) && count($data['amount_in_currency']) > 1) {
                $trnsstr = 'Transaction amount in multiple currencies:';
            }
            
            $muvi_kart_detail = '<table>';
            $muvi_kart_detail.='<tr>
                           <td>'.$trnsstr.'</td>
                        </tr>';
            
                foreach ($data['amount_in_currency'] as $key => $value) {
                    $strtd = '<td>'.$value['code'].': '.number_format((float)$value['amount'], 2, '.', '').'($'.$value['usd_amount'].')</td>';
                    if ($value['code'] == 'USD') {
                        $strtd = '<td>USD: $'.number_format((float)$value['amount'], 2, '.', '').'</td>';
                    }
                    $muvi_kart_detail.='<tr>
                               '.$strtd.'
                            </tr>';
                }
                $muvi_kart_detail.='<tr>
                           <td>Total transaction amount in USD: $'.$data['muvi_kart_total_transactions'].'</td>
                        </tr>';
                $muvi_kart_detail.='<tr>
                           <td>&nbsp;</td>
                        </tr>';
                $muvi_kart_detail.='<tr>
                           <td><b>Muvi Kart cost at '.$data['muvi_kart_price_of_transaction'].'% of total transaction amount $'.$data['muvi_kart_total_transactions'].'= $'.$data['muvi_kart_price'].'</b></td>
                        </tr>';
                
            $muvi_kart_detail.='</table>';
            
            $muvi_kart = '<tr>
                <td width="150" align="left">Muvi Kart</td>
                <td width="250" align="left">'.$muvi_kart_detail.'</td>
                <td width="100" align="right">$' . $data['muvi_kart_price'] . '</td>
             </tr>';
        }
        
        
        //Calculate paused applications with package
        $paused_billing = '';
        if (@$data['is_paused']) {
            $paused_billing = '<tr>
                <td width="150" align="left">Paused Billing</td>
                <td width="250" align="left">'.$data['paused_package_string'].'</td>
                <td width="100" align="right">(-) $' . $data['paused_package_amount'] . '</td>
             </tr>';
        }
        if (isset($data['is_bandwidth_storage_only']) && intval($data['is_bandwidth_storage_only'])) {
            $platform = '';
        } else {
            $platform = '<tr>
                <td width="150" align="left">Platform</td>
                <td width="250" align="left">' . $package . '</td>
                <td width="100" align="right">' . $price . '</td>
             </tr>';
        }
        $html.='<table class="second" cellpadding="4" border="1">
             ' . $platform . '
             ' . $discount . '
             ' . $paused_billing . '
             ' . $bandwidth . '
             ' . $storage . '
             ' . $drm . '
             ' . $muvi_kart . '
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $billing_amount . '</b></td>
             </tr>
        </table>';

        if (isset($data['isReseller']) && intval($data['isReseller'])) {
            return $html;
        } else {
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }
    }

    public function adminUpdatePackageWithoutPlanChangeInvoiceDetial($data = array()) {
        $html = $this->getCommonPdf($data);
        
        $package = $data['package_name'];
        $package.='<br/>Plan: ' . $data['plan'].'ly';
        if (trim($data['applications'])) {
            $package.='<br/>New Applications: ' . $data['applications'];
        }
        
        $package.='<br/>' . $data['from_to_date'];
        
        $discount = '';
        if ($data['plan'] == 'Year') {
            if (trim($data['applications'])) {
                $discount = '<tr>
                    <td width="150" align="left">Yearly discount on applications</td>
                    <td width="250" align="left">' . $data['yearly_discount'] . '%</td>
                    <td width="100" align="right">$' . $data['discount'] . '</td>
                 </tr>';
            }
        }
        
        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="left">'.$data['plan'].'ly prorated amount of new applications</td>
                <td width="250" align="left">' . $package . '</td>
                <td width="100" align="right">$' . $data['pro_amount'] . '</td>
             </tr>
             ' . $discount . '
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $data['billing_amount'] . '</b></td>
             </tr>
        </table>';
        
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }
    
    public function adminUpdatePackageWithPlanChangeInvoiceDetial($data = array()) {
        $html = $this->getCommonPdf($data);
        
        $package = $data['package_name'];
        $package.='<br/>Plan: ' . $data['plan'].'ly';
        if (trim($data['applications'])) {
            $package.='<br/>Applications: ' . $data['applications'];
        }
        
        $package.='<br/>' . $data['from_to_date'];
        
        $discount = '';
        if ($data['plan'] == 'Year') {
            $discount = '<tr>
                <td width="150" align="left">Yearly discount on platform</td>
                <td width="250" align="left">' . $data['yearly_discount'] . '%</td>
                <td width="100" align="right">$' . $data['discount'] . '</td>
             </tr>';
        }
        
        $proamountofapp = '';
        if (isset($data['newapplications']) && trim($data['newapplications'])) {
            $proamountofapp = '<tr>
                <td width="150" align="left">Monthly prorated charge for New applications</td>
                <td width="250" align="left">' . $data['newapplications'] .'<br/>' . $data['pro_from_to_date']. '</td>
                <td width="100" align="right">$' . $data['pro_amount'] . '</td>
             </tr>';
        }
        
        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="left">Platform</td>
                <td width="250" align="left">' . $package . '</td>
                <td width="100" align="right">$' . $data['platform_amount'] . '</td>
             </tr>
             ' . $discount . '
             ' . $proamountofapp . '
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $data['billing_amount'] . '</b></td>
             </tr>
        </table>';
        
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }

    public function getResellerCommonPdf($data = array()) {
        $studio_name = 'Muvi';
        $reseller_name = $data['rseller_name'];
        $account_name = $data['industry'];
        $account_number = $data['account_number'];
        $invoice_number = $data['invoice_number'];
        $invoice_date = date('F d, Y');
        $billing_amount = '$'.$data['billing_amount'];
        
        $isRenew = '';
        if (isset($data['isRenew']) && intval($data['isRenew'])) {
            $billing_period = $data['billing_period'];
            $isRenew = '<tr>
            <td width="500" align="left" colspan="2">' . $billing_period . '</td>
         </tr>
         <tr>
            <td width="500" align="left" colspan="2">&nbsp;</td>
         </tr>';
        }
        
        $html = <<<EOF
            <style>
                h1 {
                    color: #2B6E96;
                    font-family: helvetica;
                    font-size: 18pt;
                    text-align: center;
                }
                p.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                table.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second td {
                    color: #283240;
                    border-top: 1px solid #283240;
                    border-right: 1px solid #283240;
                }
                table.second td:first {
                    border-top: 0px solid #283240;
                }
				div.pagebreak { page-break-before: always; }
            </style>
            <h1 class="title">{$studio_name} Invoice</h1>
            <p class="first"><i>Greetings from {$studio_name}. We're writing to provide you with an electronic invoice for your use of {$studio_name}
                services. Additional information regarding your bill and your account 
                history are available on the Transaction interface. Email or call your {$studio_name} Account 
                Manager in case of any questions your account or bill.
            </i></p>

            <br />
            <table class="first" cellpadding="2">
            {$isRenew}
             <tr>
                <td width="120" align="left" valign="top">Reseller:</td>
                <td width="380" align="left">{$reseller_name}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Name:</td>
                <td width="380" align="left">{$account_name}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Number:</td>
                <td width="380" align="left">{$account_number}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Number:</td>
                <td width="380" align="left">{$invoice_number}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Date:</td>
                <td width="380" align="left">{$invoice_date}</td>
             </tr>
             <tr>
                <td width="120" align="left"><b>Total Amount:</b></td>
                <td width="380" align="left"><b>{$billing_amount}</b></td>
             </tr>
            </table>
            <br />
            <br />
EOF;
        
        return $html;
    }
    
    public function resellerMonthlyYearlyRenewInvoiceDetial($data = array()) {
        $html = self::getResellerCommonPdf($data);
        
        $billing_amount = '$'.$data['billing_amount'];
        $reseller_level_name = $data['reseller_level_name'];
        $reseller_bill_amount = $data['reseller_bill_amount'];
        
        $customer_total_price = '';
        $customer = '';
        
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                if (isset($value['customer_name']) && trim($value['customer_name'])) {
                    $customer_total_price.= '<tr>
                    <td width="250" align="left">'.$value['customer_name'].'</td>
                    <td width="250" align="right">$'.$value['customer_total_bill_amount'].'</td>
                    </tr>';
                    
                    $customer.='<div class="pagebreak"></div>';
                    $customer.='<table class="first" cellpadding="2">
                        <tr>
                           <td align="left" valign="top"><b><u>'.$value['customer_name'].'</u></b></td>
                        </tr>
                     </table><br />';
                    $customer.= $value['customer_html'];
                }
            }
        }
        
        $html.='<table class="first" cellpadding="2">
                <tr>
                   <td align="left" valign="top"><b><u>Summary</u></b></td>
                </tr>
             </table>';
        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="250" align="left">Reseller Fee - '.$reseller_level_name.'</td>
                <td width="250" align="right">$'.$reseller_bill_amount.'</td>
             </tr>
            ' . $customer_total_price . '
            <tr>
                <td width="250" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="250" align="left"><b>Total</b></td>
                <td width="250" align="right"><b>' . $billing_amount . '</b></td>
             </tr>
        </table><br /><br />';
        
        $html.=$customer;
        
        $res['html'] = $data['html'] = self::getCommonFooter($html);
        $res['pdf'] = self::generatePdf($data);

        return $res;
    }
    
    /**
     * @method public downloadUserinvoice It will generate PDF for transactions
     * @param type $studio
     * @param type $transaction_id
     * @return string@
     */
    public function downloadUserinvoice($studio_id, $transaction_id = '', $user_id = '',$subscription_plan_id='', $isEmail = '',$device_types='') {
        try {
            $tran_info = Transaction::model()->findByAttributes(array('id' => $transaction_id, 'studio_id' => $studio_id, 'user_id' => $user_id));
            
            if (!$tran_info) {
                return false;
            }
            //$c_name = Studios::model()->findByPk($studio_id)->name;
            $studio_details = Yii::app()->db->createCommand("SELECT name FROM studios WHERE id=".$studio_id)->queryROW();
            $c_name=$studio_details['name'];
            $Merchant_email=$studio_details['email'];
            $ppv_sub = PpvSubscription::model()->findByAttributes(array('id'=>$tran_info->ppv_subscription_id));
            $user_MerchantAddress = User::model()->findByAttributes(array('studio_id'=>$studio_id,'role_id'=>1));
            $gw_name_sql = "SELECT name FROM payment_gateways WHERE short_code='".$tran_info->payment_method."' LIMIT 1";
            $data_gt_name = Yii::app()->db->createCommand($gw_name_sql)->queryRow();
            $amount = Yii::app()->common->formatPrice($tran_info->amount, $tran_info->currency_id,1);
            if($subscription_plan_id){
            $purchased = SubscriptionPlans::model()->findByAttributes(array('id'=>$subscription_plan_id))->name; 
            }
            $coupon_code = "N/A";
            if($ppv_sub->coupon_code != ""){
                $coupon_code = $ppv_sub->coupon_code;
                $price_info = PpvPricing::model()->findByAttributes(array('ppv_plan_id'=>$ppv_sub->ppv_plan_id));
                $discount = "";
                $coupon_data = Coupon::model()->findByAttributes(array('coupon_code'=>$ppv_sub->coupon_code));
                if($coupon_data->discount_type == 1){
                    $discount = $coupon_data->discount_amount."%";
                    
                    if($ppv_sub->season_id != 0 && $ppv_sub->episode_id != 0){
                        $price = $price_info->episode_subscribed;
                    }else if($ppv_sub->season_id != 0 && $ppv_sub->episode_id == 0){
                        $price = $price_info->season_subscribed;
                    }else{
                        if($price_info->show_subscribed != 0){
                            $price = $price_info->show_subscribed;
                        }else{
                            $price = $price_info->price_for_subscribed;
                        }
                    } 
                    $price_amount = Yii::app()->common->formatPrice($price, $tran_info->currency_id,1);
                }else{
                    $coupon_cur = CouponCurrency::model()->findByAttributes(array('coupon_id'=>$coupon_data->id));
                    if($ppv_sub->season_id != 0 && $ppv_sub->episode_id != 0){
                        $price = $price_info->episode_subscribed;
                    }else if($ppv_sub->season_id != 0 && $ppv_sub->episode_id == 0){
                        $price = $price_info->season_subscribed;
                    }else{
                        if($price_info->show_subscribed != 0){
                            $price = $price_info->show_subscribed;
                        }else{
                             $price = $price_info->price_for_subscribed;
                        }
                    }
                    $discount_price = $coupon_cur->discount_amount;
                    if($coupon_cur->discount_amount > $price){
                        $discount_price = $price;
                    }
                    $discount = Yii::app()->common->formatPrice($discount_price, $coupon_cur->currency_id,1);
                    $price_amount = Yii::app()->common->formatPrice($price, $coupon_cur->currency_id,1);
                }
            }else{
                $price_amount=$amount;
            }
            
            
            if ($tran_info->subscription_id) {
                $data = Yii::app()->db->createCommand()
                        ->SELECT('us.*,p.name,p.recurrence,p.frequency,sp.price,p.trial_recurrence,p.trial_period')
                        ->from('user_subscriptions us ,subscription_plans p,subscription_pricing sp')
                        ->where('us.plan_id = p.id AND p.id = sp.subscription_plan_id AND sp.currency_id ='.$tran_info->currency_id.'  AND us.id=' . $tran_info->subscription_id)
                        ->queryAll();
                $coupon_code = 'N/A';
                
                if(isset($data[0]['coupon_code']) && $data[0]['coupon_code'] != ""){
                    $coupon_code = $data[0]['coupon_code'];
                    $discount = "";
                    $coupon_data = CouponSubscription::model()->findByAttributes(array('coupon_code'=>$coupon_code));
                    if($coupon_data->discount_type == 1){
                        if($data[0]['count_discount']>1){
                            $discount='';
                            $coupon_code ='N/A';
                        }else{
                            $discount = $coupon_data->discount_amount."%";
                        }
                        $price = $data[0]['price'];
                        $price_amount = Yii::app()->common->formatPrice($price, $tran_info->currency_id,1);
                        if($tran_info->amount <= 0){
                            $price_amount = Yii::app()->common->formatPrice($data[0]['price'], $tran_info->currency_id,1);
                        }
                    }else{
                        $coupon_cur = CouponCurrencySubscription::model()->findByAttributes(array('coupon_id'=>$coupon_data->id,'currency_id'=>$tran_info->currency_id));
                    if($data[0]['count_discount']>1){
                            $coupon_code ='N/A';
                            $discount='';
                        }else{
                            $discount = Yii::app()->common->formatPrice($coupon_cur->discount_amount, $coupon_cur->currency_id,1);
                        }
                        $price = $data[0]['price'];
                        $price_amount = Yii::app()->common->formatPrice($price, $coupon_cur->currency_id,1);
                        if($tran_info->amount <= 0){
                            $discount = $price_amount = Yii::app()->common->formatPrice($data[0]['price'], $tran_info->currency_id,1);
                        }
                    }
                }
                
                $recurrence_frequency = $data[0]['frequency'].' '.strtolower($data[0]['recurrence'])."s";
                $start_date = date("F d, Y", strtotime($tran_info->created_date));
                $end_date = Date('F d, Y', strtotime($start_date."+{$recurrence_frequency}-1days"));
                 
                $purchased = " " . $data[0]['name'] . " Subscription <span style='font-size:12px;'>(" . $start_date . " To " . $end_date . ')</span> ';
            } elseif ($tran_info->ppv_subscription_id) {
                if ($tran_info->transaction_type == 5) {
                    $planModel = new PpvPlans();
                    $purchased = $planModel->findByPk($tran_info->plan_id)->title;
                } else {
                    $ppvSubscription = PpvSubscription::model()->findByPk($tran_info->ppv_subscription_id);
                    $pdo_cond = ' f.id=:movie_id';
                    $pdo_cond_arr[':movie_id'] = $ppvSubscription->movie_id;
					
                    $season = '';
                    if ($ppvSubscription->season_id) {
                        $season = "Season#: " . $ppvSubscription->season_id;
                    }

                    if ($ppvSubscription->episode_id) {
                        $pdo_cond .= ' AND ms.id=:stream_id';
                        $pdo_cond_arr[':stream_id'] = $ppvSubscription->episode_id;
                    }

                    $data = Yii::app()->db->createCommand()
                            ->SELECT('f.id,f.name,f.permalink,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id')
                            ->from('films f ,movie_streams ms ')
                            ->where('f.id = ms.movie_id AND ' . $pdo_cond, $pdo_cond_arr)
                            ->queryAll();
                    $purchased = ucfirst($data[0]['name']);
                    $name = ucfirst($data[0]['name']);

                    $detail_name = "";
                    $detail_episode = "";
                    if($data[0]['episode_title']!=""){
                        $detail_episode = ' - ' . $data[0]['episode_title'];
                    }
                    if ($ppvSubscription->season_id || $ppvSubscription->episode_id) {
                        $detail_name = $season.$detail_episode;
                }
            }
            }
            
            $userInfo = Yii::app()->db->createCommand()
                    ->SELECT('s.name, s.logo_file, s.domain, s.theme, s.subdomain, s.default_color,u.*')
                    ->from('studios s,sdk_users u')
                    ->WHERE('s.id = u.studio_id AND u.id = ' . $user_id)
                    ->queryAll();
            $timestamp = strtotime($tran_info->created_date);
            $trns_dt = date("d F, Y h:i:s A", $timestamp);
                $logo_path = Yii::app()->common->getLogoFavPath($studio_id,'logo',$device_types,1);
                $real_logo = Yii::app()->getbaseUrl() ."/images/no-logo.png";
                if (strpos($logo_path, "logos")!==false || strpos($logo_path, "invoice_logo")!==false){
                    $real_logo = $logo_path;
                }
                
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);

        $pdf->SetTitle(@$userInfo[0]['name'] . ' Invoice');
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80, 80, 80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();

        $dfy = date('d F, Y',$timestamp);
        $vname= nl2br($user_MerchantAddress->address_1);
        $Vphoneno= nl2br($user_MerchantAddress->phone_no);
        $vemail=$user_MerchantAddress->email;
        if(Yii::app()->controller->IS_PAYPAL_EXPRESS['paypalpro']==1){
          $gatway='Paypal Express';  
        }else{
           $gatway=$data_gt_name['name']; 
        }
        
        $html = <<<EOF
                <table width="100%" border="0">
		 <tr>
    <td width="50%" height="45" rowspan="2"><img src="{$real_logo}" /></td>
    <td width="50%" align="right"><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size:11px;">{$c_name}</span></strong></td>
		 </tr>	
		<tr>
    <td align="right">{$vname}<br>{$Vphoneno}<br>{$vemail}</td>
  </tr>
  <tr>
    <td height="34">
    <p><strong>CUSTOMER DETAIL</strong></p>
    <p>{$userInfo[0]['display_name']}<br /><span style="color:#4150f4; text-decoration: underline;">{$userInfo[0]['email']}</span></p>
    </td>
    <td align="right"><p><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size:18px;">INVOICE#{$transaction_id}</span></strong></p>
      <p><strong>DATE</strong><br />{$dfy}</p>
    </td>
		 </tr>	
		 <tr>
    <td height="22">&nbsp;</td>
    <td>&nbsp;</td>
		 </tr>
		 <tr>
    <td colspan="2"><table width="100%" border="1">
      <tr>
        <td width="26%" align="center"><strong>ITEM</strong></td>
        <td width="19%" align="center"><strong>DETAILS</strong></td>
        <td width="20%" align="center"><strong>PRICE</strong></td>
        <td width="20%" align="center"><strong>DISCOUNT</strong></td>
        <td width="15%" align="center"><strong>SUBTOTAL</strong></td>
		 </tr>
		 <tr>
        <td>{$purchased}</td>
        <td>{$detail_name}</td>
        <td align="center">{$price_amount}</td>
        <td align="center">{$discount}</td>
        <td align="center">{$amount}</td>
		 </tr>
		 <tr>
        <td colspan="4" align="right"><strong>TOTAL(INCLUSIVE OF VAT)</strong></td>
        <td align="center"><strong>{$amount}</strong></td>
		 </tr>
    </table></td>
  </tr>
  <tr>
    <td height="174" rowspan="15">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><strong>PAYMENT DETAILS</strong></td>
  </tr>
  <tr>
    <td align="right"><strong>Gate Way</strong></td>
  </tr>
  <tr>
    <td align="right">{$gatway}</td>
  </tr>
  <tr>
    <td align="right"><strong>Gateway Order#</strong></td>
  </tr>
  <tr>
    <td align="right">{$tran_info->order_number}</td>
  </tr>
  <tr>
    <td align="right"><strong>Gateway Transaction#</strong></td>
  </tr>
  <tr>
    <td align="right">{$tran_info->invoice_id}</td>
  </tr>
  <tr>
    <td align="right"><strong>Transaction Date-Time</strong></td>
  </tr>
  <tr>
    <td align="right">{$trns_dt}</td>
  </tr>
  <tr>
    <td align="right" style="color:#42b6f4"><strong>Coupon Code</strong></td>
  </tr>
  <tr>
    <td align="right" style="color:#42b6f4">{$coupon_code}</td>
  </tr>
  
	 </table>
                    
EOF;
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $file_name = 'Invoice_details_' . str_replace(" ", '', ucwords(strtolower($userInfo[0]['subdomain']))) . '_' . date('F_d_Y') . '.pdf';
        if (intval($isEmail)) {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
        }else if($device_types=='app'){
            $file_name=time().$file_name;
            $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
            $file_name;
        }else{
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'I');
        }

        return $file_name;
    }
    
    //voucherInvoice Generate
     public function getFilmName($movie_id){
        $movie_name = Yii::app()->db->createCommand("SELECT name FROM films WHERE id=".$movie_id)->queryROW();
        return $movie_name['name'];
    }
     public function getEpisodeName($episode_id){
        $movie_name = Yii::app()->db->createCommand("SELECT episode_title FROM movie_streams WHERE id=".$episode_id)->queryROW();
        return $movie_name['episode_title'];
    }
    public function downloadVoucherinvoice($studio_id, $transaction_id = '', $user_id = '', $isEmail = '',$device_types='') {
        try {
            $voucher_info = PpvSubscription::model()->findByAttributes(array('id' => $transaction_id, 'studio_id' => $studio_id, 'user_id' => $user_id));
           //$voucherdetails = Yii::app()->db->createCommand("select vs.*,v.voucher_code from voucher_subscription as vs LEFT JOIN  voucher as v on vs.voucher_id=v.id WHERE vs.id=".$transaction_id."vs.studio_id=".$studio_id."user_id=")->queryRow();
            if (!$voucher_info) {
                return false;
            }
            if ($voucher_info->movie_id) {
              $studio_details = Yii::app()->db->createCommand("SELECT name FROM studios WHERE id=".$studio_id)->queryROW();
              $c_name=$studio_details['name'];
              $Merchant_email=$studio_details['email'];
              //$c_name = Studios::model()->findByPk($studio_id)->name;
              $trns_ifo = Transaction::model()->findByAttributes(array('ppv_subscription_id' => $transaction_id, 'studio_id' => $studio_id, 'user_id' => $user_id));
              $voucher_code = CouponOnly::model()->findByAttributes(array('id'=>$voucher_info->ppv_plan_id));
              $user_MerchantAddress = User::model()->findByAttributes(array('studio_id'=>$studio_id,'role_id'=>1));
              $movie_id=$voucher_info->movie_id;
              $permalink=Film::model()->findByAttributes(array('id'=>$movie_id))->permalink ;
              $movie_name = isset($movie_id)?$this->getFilmName($movie_id):"";
              $season_id =$voucher_info->season_id;
              $episode_id=$voucher_info->episode_id;
              $episode_name = isset($episode_id)?$this->getEpisodeName($episode_id):"";
              $vmovie_names = "";
              if($season_id != 0){
                  $vmovie_names .= "#Season-".$season_id;
              }
              if($episode_id != 0){
                  $vmovie_names .= "->".$episode_name;
              }
              $timestamp = strtotime($trns_ifo->created_date);
              $trns_dt = date("d F, Y h:i:s A", $timestamp);      
            }
            
            $userInfo = Yii::app()->db->createCommand()
                    ->SELECT('s.name, s.logo_file, s.domain, s.theme, s.subdomain, s.default_color,u.*')
                    ->from('studios s,sdk_users u')
                    ->WHERE('s.id = u.studio_id AND u.id = ' . $user_id)
                    ->queryAll();
            //SdkUser::model()->findByPk($ppvSubscription->user_id);
                $logo_path = Yii::app()->common->getLogoFavPath($studio_id,'logo','',1);
                
                $real_logo = Yii::app()->getbaseUrl() ."/images/no-logo.png";
                if (strpos($logo_path, "logos")!==false || strpos($logo_path, "invoice_logo")!==false){
                    $real_logo = $logo_path;
                }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);

        $pdf->SetTitle(@$userInfo[0]['name'] . ' Invoice');
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80, 80, 80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();
        
        $dfy = date('d F, Y',$timestamp);   
        $vname= nl2br($user_MerchantAddress->address_1);
        $Vphoneno= nl2br($user_MerchantAddress->phone_no);
        $vemail=$user_MerchantAddress->email;
        $html = <<<EOF
		<table width="100%" border="0">
  <tr>
    <td width="50%" height="45" rowspan="2"><img src="{$real_logo}" /></td>
    <td width="50%" align="right"><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size:11px;">{$c_name}</span></strong></td>
  </tr>
  <tr>
    <td align="right">{$vname}<br>{$Vphoneno}<br>{$vemail}</td>
  </tr>
  <tr>
    <td height="34">
    <p><strong>CUSTOMER DETAIL</strong></p>
    <p>{$userInfo[0]['display_name']}<br /><span style="color:#4150f4; text-decoration: underline;">{$userInfo[0]['email']}</span></p>    </td>
    <td align="right"><p><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size:18px;">INVOICE#{$transaction_id}</span></strong></p>
      <p><strong>DATE</strong><br />{$dfy}</p>    </td>
  </tr>
  <tr>
    <td height="22">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="1">
      <tr>
        <td width="26%" align="center"><strong>ITEM</strong></td>
        <td width="19%" align="center"><strong>DETAILS</strong></td>
        <td width="20%" align="center"><strong>PRICE</strong></td>
        <td width="20%" align="center"><strong>DISCOUNT</strong></td>
        <td width="15%" align="center"><strong>SUBTOTAL</strong></td>
      </tr>
      <tr>
        <td>{$movie_name}</td>
        <td>{$vmovie_names}</td>
        <td align="center">0</td>
        <td align="center">0</td>
        <td align="center">0</td>
      </tr>
      <tr>
        <td colspan="4" align="right"><strong>TOTAL(INCLUSIVE OF VAT)</strong></td>
        <td align="center"><strong>0</strong></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="174" rowspan="15">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><strong>PAYMENT DETAILS</strong></td>
  </tr>
  
  <tr>
    <td align="right"><strong>Order Id#</strong></td>
  </tr>
  <tr>
    <td align="right">{$trns_ifo->order_number}</td>
  </tr>
  <tr>
    <td align="right"><strong>Transaction Id#</strong></td>
  </tr>
  <tr>
    <td align="right">{$trns_ifo->invoice_id}</td>
  </tr>
  <tr>
    <td align="right"><strong>Transaction Date-Time</strong></td>
  </tr>
  <tr>
    <td align="right">{$trns_dt}</td>
  </tr>
  <tr>
    <td align="right" style="color:#42b6f4"><strong>Voucher Code</strong></td>
  </tr>
  <tr>
    <td align="right" style="color:#42b6f4">{$voucher_code->voucher_code}</td>
  </tr>
  
</table>

EOF;
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $file_name = 'Invoice_details_' . str_replace(" ", '', ucwords(strtolower($userInfo[0]['subdomain']))) . '_' . date('F_d_Y') . '.pdf';
        if (intval($isEmail)) {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
        }else if($device_types=='app'){
            $file_name=time().$file_name;
            $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
            $file_name;
        }else{
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'I');
        }

        return $file_name;
    }
    
      public function physicalUserinvoice($studio_id, $transaction_id = '', $user_id = '') {
        try {
            $tran_info = Transaction::model()->findByAttributes(array('id' => $transaction_id, 'studio_id' => $studio_id, 'user_id' => $user_id));
    
            if (!$tran_info) {
                return false;
            }
            $currency = Currency::model()->findByAttributes(array('id' => $tran_info->currency_id));
            $id = $_GET['transaction_id'];
            $userInfo = Yii::app()->db->createCommand()
                    ->SELECT('psa.first_name,psa.last_name,psa.address,psa.city,psa.country,psa.zip,psa.phone_number,pgo.created_date,pgo.total,pgo.shipping_address,s.display_name,s.email,pgo.id,pgo.order_number,pgo.grand_total,pgo.shipping_cost,pgo.discount,pgo.discount_type,pgo.coupon_code,tr.currency_id,tr.amount,tr.payment_method')
                    ->from('pg_order pgo,transactions tr,sdk_users s,pg_shipping_address psa')
                    ->WHERE('pgo.id=psa.order_id AND tr.id = pgo.transactions_id AND s.id=pgo.buyer_id AND tr.id = ' . $id)
                    ->queryAll();
            $studio_name = Yii::app()->db->createCommand("SELECT name FROM studios WHERE id=".$studio_id)->queryAll();
            foreach ($userInfo as $key => $details) {
                $orderquery = "SELECT d.id,d.name,d.price,d.quantity,o.order_number,d.sub_total FROM pg_order_details d, pg_order o WHERE d.order_id=o.id AND d.order_id=".$details['id']."";
                $order = Yii::app()->db->createCommand($orderquery)->queryAll();
                $userInfo[$key]['details'] = $order;
                $country = Countries::model()->findByAttributes(array('code' => $userInfo[0]['country']));
                $created_date = date('jS M, Y',strtotime($userInfo[0]['created_date']));
            }            
            array('order' => $order);
            //$logo_path = Yii::app()->common->getLogoFavPath($studio_id);
            $logo_path = Yii::app()->common->getLogoFavPath($studio_id,'logo',$device_types,1);
            $real_logo = Yii::app()->getbaseUrl() ."/images/no-logo.png";
            if (strpos($logo_path, "logos")!==false || strpos($logo_path, "invoice_logo")!==false){
                $real_logo = $logo_path;
            }
            $img_path = PGProduct::getpgImage($userInfo[0]['id'], 'thumb');
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
        $order_total = Yii::app()->common->formatPrice($userInfo[0]['grand_total'],$tran_info['currency_id'],1);
        $sub_total = Yii::app()->common->formatPrice($userInfo[0]['total'],$tran_info['currency_id'],1);
        $shipment_handling = Yii::app()->common->formatPrice($userInfo[0]['shipping_cost'],$tran_info['currency_id'],1);
        $tax = Yii::app()->common->formatPrice(0,$tran_info['currency_id']);
        $total_shipment = $userInfo[0]['grand_total'];
        if($userInfo[0]['discount']!='0.00'){
            if($userInfo[0]['discount_type']){
                $discount = $userInfo[0]['discount']."%";
            }else{
                $discount = Yii::app()->common->formatPrice($userInfo[0]['discount'],$tran_info['currency_id'],1);
            }
        }else{
            $discount = Yii::app()->common->formatPrice($userInfo[0]['discount'],$tran_info['currency_id'],1);
        }        
        $total_shipment = Yii::app()->common->formatPrice($total_shipment,$tran_info['currency_id'],1);
        $method = ($tran_info['payment_method']=='paypalpro')?'Paypal':ucwords($tran_info['payment_method']);
        $amount_charged = Yii::app()->common->formatPrice($tran_info['amount'],$tran_info['currency_id'],1);
        $address = nl2br($userInfo[0]['address']);
        //get studio details        
        $user = User::model()->find(array('select'=>'phone_no,email,address_1','condition' => 'studio_id=:studio_id and role_id=:role_id','params' => array(':studio_id' =>$studio_id,':role_id'=>1)));
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Invoice of Order Number- '.$userInfo[0]['order_number']);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80, 80, 80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();
         $dfy = date('d F, Y',strtotime($tran_info['created_date']));
        $trns_dt = date("d F, Y h:i:s A", strtotime($tran_info['created_date']));

	

$html = <<<EOF

            
	<table width="100%" border="0">
	<tr>
              <td width="50%" height="45"><img src="{$real_logo}" /></td>
              <td width="50%" align="right"><strong>{$studio_name[0]['name']}</strong></td>
	</tr>
            <tr>
              <td height="34">
              <p><strong>CUSTOMER DETAIL</strong>
              <br />{$userInfo[0]['display_name']}<br /><span style="color:#4150f4; text-decoration: underline;">{$userInfo[0]['email']}</span><br />Tel - {$userInfo[0]['phone_number']}</p>
                    </td>
              <td align="right"><p><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size:18px;">Invoice# {$transaction_id}<br />Order# {$userInfo[0]['order_number']}</span></strong></p>
                <p><strong>DATE</strong><br />{$dfy}</p>
                    </td>
            </tr>
            <tr>
              <td height="22">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>  
              <td colspan="2"><table width="100%" border="1" cellpadding="2" cellspacing="0" align="center">
                <tr>
                  <td width="40%" align="center"><strong>ITEM</strong></td>
                  <td width="20%" align="center"><strong>PRICE</strong></td>
                  <td width="20%" align="center"><strong>QTY</strong></td>
                  <td width="20%" align="center"><strong>SUBTOTAL</strong></td>
            </tr>
EOF;
                
        foreach ($order as $key => $value) {
            $price = ($value['price']!='0.00')?Yii::app()->common->formatPrice($value['price'],$tran_info['currency_id'],1):'Free';
            $sub_total = ($value['price']!='0.00')?Yii::app()->common->formatPrice(($value['price']*$value['quantity']),$tran_info['currency_id'],1):'';
            $html.="<tr>
                        <td>{$value['name']}</td>
			<td style='color:#42b6f4'>{$price}</td>
			<td>{$value['quantity']}</td>
                        <td>{$sub_total}</td>
		</tr>";
        }
                
                
$html.= <<<EOF
                <tr>
                  <td colspan="3" align="right"><strong>Discount</strong></td>
                  <td align="center"><strong>{$discount}</strong></td>
		</tr>
                <tr>
                  <td colspan="3" align="right"><strong>Shipping Cost</strong></td>
                  <td align="center"><strong>{$shipment_handling}</strong></td>
                </tr>
                <tr>
                  <td colspan="3" align="right"><strong>Total</strong></td>
                  <td align="center"><strong>{$total_shipment}</strong></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2"><table width="100%" border="0">
                    <tr>
                        <td width="34%"><table border="0">
                                <tr>
                                  <td><strong>Shipping Address</strong></td>
                                </tr>
                                <tr>
                                  <td>{$userInfo[0]['first_name']}&nbsp;{$userInfo[0]['last_name']}</td>
                                </tr>
                                <tr>  
                                  <td>{$address}</td>
                                </tr>
                                <tr> 
                                  <td>{$userInfo[0]['city']}</td>
                                </tr> 
                                <tr> 
                                  <td>{$country->country}</td>
                                </tr>
                                <tr> 
                                  <td>{$userInfo[0]['zip']}</td>
                                </tr>                    
        </table>
                        </td>
                        <td width="33%"><table border="0">
            <tr>
                                  <td><strong>Billing Address</strong></td>
                                </tr>
                                <tr>
                                  <td>{$userInfo[0]['first_name']}&nbsp;{$userInfo[0]['last_name']}</td>
                                </tr>
                                <tr>  
                                  <td>{$address}</td>
                                </tr>
                                <tr> 
                                  <td>{$userInfo[0]['city']}</td>
                                </tr> 
                                <tr> 
                                  <td>{$country->country}</td>
                                </tr>
                                <tr> 
                                  <td>{$userInfo[0]['zip']}</td>
                                </tr> 
                            </table>
                        </td>
                        <td width="33%"><table width="100%" border="0">
                                <tr>
                                  <td><strong>Payment Gateway</strong></td>
                                </tr>
                                <tr>
                <td>{$method}</td>
            </tr>
                                <tr>
                                  <td><strong>Gateway Order #</strong></td>
                                </tr>
                                <tr>
                                  <td>{$tran_info['order_number']}</td>
                                </tr>
                                <tr>
                                  <td><strong>Gateway Transaction #</strong></td>
                                </tr>
                                <tr>
                                  <td>{$tran_info['invoice_id']}</td>
                                </tr>
                                <tr>
                                  <td><strong>Transaction Date-Time</strong></td>
                                </tr>
                                <tr>
                                  <td>{$trns_dt}</td>
                                </tr>
                                <tr>
                                  <td style="color:#42b6f4"><strong>Coupon Code</strong></td>
                                </tr>
                                <tr>
                                  <td style="color:#42b6f4">{$userInfo[0]['coupon_code']}</td>
                                </tr>                        
        </table>
                        </td>
                    </tr>   
                </table>
              </td>
            </tr>            
	
        </table>	
  
  
EOF;
        
        $pdf->writeHTML($html, true, false, true, false, '');

        $file_name = 'Invoice_details_' . str_replace(" ", '', ucwords(strtolower($userInfo[0]['subdomain']))) . '_' . date('F_d_Y') . '.pdf';
        if (intval($isEmail)) {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
        } else {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'I');
        }

        return $file_name;
    }
    
    
    public function invoiceDetial($studio, $user, $invoice_number) {
        $studio_name = ucfirst($studio->name);
        $billing_amount = $user['amount'];
        $invoice_date = date('F d, Y');
        $amount_due = $billing_amount;

        $billing_detail ='<tr>
        <td width="350" align="left">Subscription fee for '.$studio_name.'</td>
        <td width="150" align="right">'.$billing_amount.'</td>
        </tr>';

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);

        $pdf->SetTitle($studio_name.' Invoice'); 
        $pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80,80,80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();

        $html = <<<EOF
        <style>
            h1 {
                color: #2B6E96;
                font-family: helvetica;
                font-size: 18pt;
                text-align: center;
            }
            p.first {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
                line-height:15pt;
            }
            table.first {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
            }
            table.second {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
            }
            table.second td {
                color: #283240;
                border-top: 1px solid #283240;
                border-right: 1px solid #283240;
            }
            table.second td:first {
                border-top: 0px solid #283240;
            }
            div.test {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
                line-height:15pt;
            }
            .strongbold{font-weight:bold;}
        </style>
        <h1 class="title">{$studio_name} Invoice</h1>
        <p class="first"><i>Greetings from {$studio_name}. We're writing to provide you with an electronic invoice for your use of {$studio_name}
            services. Additional information regarding your bill and your account 
            history are available on the Transaction interface. Email or call your {$studio_name} Account 
            Manager in case of any questions your account or bill.
        </i></p>

        <br />
        <table class="first" cellpadding="2">
         <tr>
            <td width="120" align="left">Account Name:</td>
            <td width="380" align="left">{$user['card_holder_name']}</td>
         </tr>
         <tr>
            <td width="120" align="left">Account Number:</td>
            <td width="380" align="left">{$user['card_last_fourdigit']}</td>
         </tr>
         <tr>
            <td width="120" align="left">Invoice Number:</td>
            <td width="380" align="left">{$invoice_number}</td>
         </tr>
         <tr>
            <td width="120" align="left">Invoice Date:</td>
            <td width="380" align="left">{$invoice_date}</td>
         </tr>
         <tr>
            <td width="120" align="left"><b>Total Amount:</b></td>
            <td width="380" align="left"><b>{$amount_due}</b></td>
         </tr>
        </table>
        <br />
        <br /> 
        <br /> 
        <br /> 
        <table class="second" cellpadding="4" border="1">
         <tr>
            <td width="350" align="center"><b>Description</b></td>
            <td width="150" align="center"><b>Amount</b></td>
         </tr>
         {$billing_detail}
         <tr>
            <td width="350" align="left">&nbsp;</td>
            <td width="150" align="right">&nbsp;</td>
         </tr>
         <tr>
            <td width="350" align="left">&nbsp;</td>
            <td width="150" align="right">&nbsp;</td>
         </tr>
         <tr>
            <td width="350" align="left">&nbsp;</td>
            <td width="150" align="right">&nbsp;</td>
         </tr>
         <tr>
            <td width="350" align="left"><b>Total</b></td>
            <td width="150" align="right"><b>{$amount_due}</b></td>
         </tr>
        </table>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');

        $file_name = 'Invoice_details_'.str_replace(" ", '', ucwords(strtolower($studio_name))).'_'.date('F_d_Y').'.pdf';
        echo $pdf->Output(ROOT_DIR.'docs/'.$file_name, 'F');

        return $file_name;
    }
public function resellerSubscriptionReactivationInvoiceDetial($data = array()){
            $html = $this->getCommonResellerPdf($data);
            $billing_amount = $data['billing_amount'];
            $package = $data['package_name'];   
            $package.='<br/>Plan: ' . $data['plan'].'ly';
            $package.='<br/>' . $data['from_to_date'];
            $base_price = $data['base_price'];   
            $discount = '';
            if ($data['plan'] == 'Year') {
                $totalprice = number_format((float) ($base_price) * 12, 2, '.', '');
                $price = $base_price . " * 12 = $" . $totalprice;
            }else{
                $totalprice = number_format((float) $base_price, 2, '.', '');
                $price = "$" . $totalprice;
            }
         $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="left">Platform</td>
                <td width="250" align="left">' . $package . '</td>
                <td width="100" align="right">' . $price . '</td>
             </tr>
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $billing_amount . '</b></td>
             </tr>
        </table>';   
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generateresellerPdf($data);

        return $res;        
    }
    public function getCommonResellerPdf($data = array()) {
        $isRenew = '';
        if (isset($data['isRenew']) && intval($data['isRenew'])) {
            $billing_period = $data['billing_period'];
            $isRenew = '<tr>
            <td width="500" align="left" colspan="2">' . $billing_period . '</td>
         </tr>
         <tr>
            <td width="500" align="left" colspan="2">&nbsp;</td>
         </tr>';
        }
        
        $customer_name = '';
        if (isset($data['user']) && !empty($data['user'])) {
            $user = $data['user'];
            $customer_name = (isset($user->name) && trim($user->name)) ? ucfirst($user->name):'';
            if (isset($user->address_1) && trim($user->address_1)) {
                $customer_name.='<br/>'.nl2br($user->address_1);
            }
        }
        //print_r($user);exit;
        $payment_method = $data['payment_method'];
        $studio_name = 'Muvi';
        $accountname = ucfirst($user->name);
        $accountnumber = $user->id;
        

        $billing_info_id = $data['billing_info_id'];
        $invoice_date = date('F d, Y');
        $amount_due = '$' . $data['billing_amount'];

        $html = <<<EOF
            <style>
                h1 {
                    color: #2B6E96;
                    font-family: helvetica;
                    font-size: 18pt;
                    text-align: center;
                }
                p.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                table.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second td {
                    color: #283240;
                    border-top: 1px solid #283240;
                    border-right: 1px solid #283240;
                }
                table.second td:first {
                    border-top: 0px solid #283240;
                }
                div.test {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                .strongbold{font-weight:bold;}
            </style>
            <h1 class="title">{$studio_name} Invoice</h1>
            <p class="first"><i>Greetings from {$studio_name}. We're writing to provide you with an electronic invoice for your use of {$studio_name}
                services. Additional information regarding your bill and your account 
                history are available on the Transaction interface. Email or call your {$studio_name} Account 
                Manager in case of any questions your account or bill.
            </i></p>

            <br />
            <table class="first" cellpadding="2">
            {$isRenew}
             <tr>
                <td width="120" align="left" valign="top">Customer:</td>
                <td width="380" align="left">{$customer_name}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Name:</td>
                <td width="380" align="left">{$accountname}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Number:</td>
                <td width="380" align="left">{$accountnumber}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Number:</td>
                <td width="380" align="left">{$billing_info_id}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Date:</td>
                <td width="380" align="left">{$invoice_date}</td>
             </tr>
             <tr>
                <td width="120" align="left"><b>Total Amount:</b></td>
                <td width="380" align="left"><b>{$amount_due}</b></td>
             </tr>
              <tr>
                <td width="120" align="left"><b>Payment Method:</b></td>
                <td width="380" align="left"><b>{$payment_method}</b></td>
             </tr>               
            </table>
            <br />
            <br /> 
            <br /> 
            <br />
EOF;
        return $html;      
    }
    public function generateresellerPdf($data = array()) {
        ob_clean();
        $user_id = $data['user']->id;
        $studio_name = (isset($data['isUser']) && intval($data['isUser'])) ? ucfirst($studio->name) : 'Muvi';
        $html = $data['html'];
        $isEmail = (isset($data['isEmail'])) ? $data['isEmail'] : 1;
        $billing_date = (isset($data['billing_date'])) ? $data['billing_date'] : date('F_d_Y');
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle($studio_name . ' Invoice');
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80, 80, 80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();

        $pdf->writeHTML($html, true, false, true, false, '');

        $file_name = 'Invoice_details_' . str_replace(" ", '', ucwords(strtolower('reseller'))) .$user_id. '_' . $billing_date . '.pdf';
        ob_flush();

        if (intval($isEmail)) {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'F');
        } else {
            echo $pdf->Output(ROOT_DIR . 'docs/' . $file_name, 'I');
        }

        return $file_name;
    }
     public function invoiceSubscriptionBundleDetial($studio, $user, $invoice_number) {
        $studio_name = ucfirst($studio->name);
        $billing_amount = $user['amount'];
        $invoice_date = date('F d, Y');
        $amount_due = $billing_amount;

        $billing_detail ='<tr>
        <td width="350" align="left">Subscription Bundles fee for '.$studio_name.'</td>
        <td width="150" align="right">'.$billing_amount.'</td>
        </tr>';
        
        $coupon = "";
        $full_amount = "";
        $discount_amount = "";
        if(isset($user['coupon']) && $user['coupon'] != ''){
            $coupon = '<tr>
            <td width="120" align="left">Coupon Code:</td>
            <td width="380" align="left">'.$user['coupon'].'</td>
            </tr>';
            
            $full_amount = '<tr>
            <td width="120" align="left">Full Amount:</td>
            <td width="380" align="left">'.$user['full_amount'].'</td>
            </tr>';
            
            $discount_amount = '<tr>
            <td width="120" align="left">Discount Amount:</td>
            <td width="380" align="left">'.$user['discount_amount'].'</td>
            </tr>';
}

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);

        $pdf->SetTitle($studio_name.' Invoice'); 
        $pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetTextColor(80,80,80);
        $pdf->xfootertext = 'Copyright ';
        $pdf->AddPage();

        $html = <<<EOF
        <style>
            h1 {
                color: #2B6E96;
                font-family: helvetica;
                font-size: 18pt;
                text-align: center;
            }
            p.first {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
                line-height:15pt;
            }
            table.first {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
            }
            table.second {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
            }
            table.second td {
                color: #283240;
                border-top: 1px solid #283240;
                border-right: 1px solid #283240;
            }
            table.second td:first {
                border-top: 0px solid #283240;
            }
            div.test {
                color: #283240;
                font-family: helvetica;
                font-size: 10pt;
                line-height:15pt;
            }
            .strongbold{font-weight:bold;}
        </style>
        <h1 class="title">{$studio_name} Invoice</h1>
        <p class="first"><i>Greetings from {$studio_name}. We're writing to provide you with an electronic invoice for your use of {$studio_name}
            services. Additional information regarding your bill and your account 
            history are available on the Transaction interface. Email or call your {$studio_name} Account 
            Manager in case of any questions your account or bill.
        </i></p>

        <br />
        <table class="first" cellpadding="2">
         <tr>
            <td width="120" align="left">Account Name:</td>
            <td width="380" align="left">{$user['card_holder_name']}</td>
         </tr>
         <tr>
            <td width="120" align="left">Account Number:</td>
            <td width="380" align="left">{$user['card_last_fourdigit']}</td>
         </tr>
         <tr>
            <td width="120" align="left">Invoice Number:</td>
            <td width="380" align="left">{$invoice_number}</td>
         </tr>
         <tr>
            <td width="120" align="left">Invoice Date:</td>
            <td width="380" align="left">{$invoice_date}</td>
         </tr>
          {$coupon}
          {$full_amount}
          {$discount_amount}
         <tr>
            <td width="120" align="left"><b>Total Amount:</b></td>
            <td width="380" align="left"><b>{$amount_due}</b></td>
         </tr>
        </table>
        <br />
        <br /> 
        <br /> 
        <br /> 
        <table class="second" cellpadding="4" border="1">
         <tr>
            <td width="350" align="center"><b>Description</b></td>
            <td width="150" align="center"><b>Amount</b></td>
         </tr>
         {$billing_detail}
         <tr>
            <td width="350" align="left">&nbsp;</td>
            <td width="150" align="right">&nbsp;</td>
         </tr>
         <tr>
            <td width="350" align="left">&nbsp;</td>
            <td width="150" align="right">&nbsp;</td>
         </tr>
         <tr>
            <td width="350" align="left">&nbsp;</td>
            <td width="150" align="right">&nbsp;</td>
         </tr>
         <tr>
            <td width="350" align="left"><b>Total</b></td>
            <td width="150" align="right"><b>{$amount_due}</b></td>
         </tr>
        </table>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');

        $file_name = 'Invoice_details_'.str_replace(" ", '', ucwords(strtolower($studio_name))).'_'.date('F_d_Y').'.pdf';
        echo $pdf->Output(ROOT_DIR.'docs/'.$file_name, 'F');

        return $file_name;
    }
    public function resellerCustomerSubscriptionReactivationInvoiceDetial($data = array()) {
        $html = $this->getAddresellerCustomerCommonPdf($data);
        $app_price = Yii::app()->general->getAppPrice($data['package_code']);
        $billing_amount = $data['billing_amount'];
        $package = $data['package_name'];
        if (trim($data['applications'])) {
            $package.=' with ' . $data['applications'];
        }

        $package.='<br/>Plan: ' . $data['plan'].'ly';
        $package.='<br/>' . $data['from_to_date'];
        $base_price = $data['base_price'];
        $no_of_applications = trim($data['no_of_applications']) ? $data['no_of_applications'] : 0;
        $discount = '';

        if ($data['plan'] == 'Year') {
            $yearly_discount = $data['yearly_discount'];
            if (intval($no_of_applications)) {
                $totalprice = number_format((float) ($base_price + (($no_of_applications - 1) * $app_price)) * 12, 2, '.', '');
            } else {
                $totalprice = number_format((float) ($base_price) * 12, 2, '.', '');
}
            
            $discount_price = number_format((float) ($totalprice * $yearly_discount) / 100, 2, '.', '');
            
            if (intval($no_of_applications)) {
                $price = "(" . $base_price . " + (".($no_of_applications - 1)." * " . $app_price . ") * 12) = $" . $totalprice;
            } else {
                $price = $base_price . " * 12 = $" . $totalprice;
            }
            
            $discount = '<tr>
                <td width="150" align="left">Discount on platform</td>
                <td width="250" align="left">' . $yearly_discount . '%</td>
                <td width="100" align="right">$' . $discount_price . '</td>
             </tr>';
        } else {
            if (intval($no_of_applications)) {
                $totalprice = number_format((float) $base_price + (($no_of_applications - 1) * $app_price), 2, '.', '');
                $price = $base_price . " + (".($no_of_applications -1)." * " . $app_price . ") = $" . $totalprice;
            } else {
                $totalprice = number_format((float) $base_price, 2, '.', '');
                $price = "$" . $totalprice;
            }
        }

        $html.='<table class="second" cellpadding="4" border="1">
            <tr>
                <td width="150" align="left">Platform</td>
                <td width="250" align="left">' . $package .'<br>'.$price .'</td>
                <td width="100" align="right">$0</td>
             </tr>
             ' . $discount . '
            <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left">&nbsp;</td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right">&nbsp;</td>
             </tr>
             <tr>
                <td width="150" align="left"><b>Pro-Rated Amount</b></td>
                <td width="250" align="right">' . $data['prorated_calculation'] . '</td>
                <td width="100" align="right"><b>$' . $billing_amount . '</b></td>
             </tr>             
             <tr>
                <td width="150" align="left"><b>Total</b></td>
                <td width="250" align="right">&nbsp;</td>
                <td width="100" align="right"><b>$' . $billing_amount . '</b></td>
             </tr>
        </table>';
        
        $res['html'] = $data['html'] = $this->getCommonFooter($html);
        $res['pdf'] = $this->generatePdf($data);

        return $res;
    }    
   public function getAddresellerCustomerCommonPdf($data = array()) {
        $studio = $data['studio'];
        $isRenew = '';
        if (isset($data['isRenew']) && intval($data['isRenew'])) {
            $billing_period = $data['billing_period'];
            $isRenew = '<tr>
            <td width="500" align="left" colspan="2">' . $billing_period . '</td>
         </tr>
         <tr>
            <td width="500" align="left" colspan="2">&nbsp;</td>
         </tr>';
        }
        
        $customer_name = '';
        if (isset($data['portal_user']) && !empty($data['portal_user'])) {
            $user = $data['portal_user'];
            $customer_name = $user->name;
            $studio_name = 'Muvi';
            $accountname = ucfirst($user->industry);
            $accountnumber = $user->id;
        }

        $billing_info_id = $data['billing_info_id'];
        $invoice_date = date('F d, Y');
        $amount_due = '$' . $data['billing_amount'];

        $html = <<<EOF
            <style>
                h1 {
                    color: #2B6E96;
                    font-family: helvetica;
                    font-size: 18pt;
                    text-align: center;
                }
                p.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                table.first {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                }
                table.second td {
                    color: #283240;
                    border-top: 1px solid #283240;
                    border-right: 1px solid #283240;
                }
                table.second td:first {
                    border-top: 0px solid #283240;
                }
                div.test {
                    color: #283240;
                    font-family: helvetica;
                    font-size: 10pt;
                    line-height:15pt;
                }
                .strongbold{font-weight:bold;}
            </style>
            <h1 class="title">{$studio_name} Invoice</h1>
            <p class="first"><i>Greetings from {$studio_name}. We're writing to provide you with an electronic invoice for your use of {$studio_name}
                services. Additional information regarding your bill and your account 
                history are available on the Transaction interface. Email or call your {$studio_name} Account 
                Manager in case of any questions your account or bill.
            </i></p>

            <br />
            <table class="first" cellpadding="2">
            {$isRenew}
             <tr>
                <td width="120" align="left" valign="top">Customer:</td>
                <td width="380" align="left">{$customer_name}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Name:</td>
                <td width="380" align="left">{$accountname}</td>
             </tr>
             <tr>
                <td width="120" align="left">Account Number:</td>
                <td width="380" align="left">{$accountnumber}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Number:</td>
                <td width="380" align="left">{$billing_info_id}</td>
             </tr>
             <tr>
                <td width="120" align="left">Invoice Date:</td>
                <td width="380" align="left">{$invoice_date}</td>
             </tr>
             <tr>
                <td width="120" align="left"><b>Total Amount:</b></td>
                <td width="380" align="left"><b>{$amount_due}</b></td>
             </tr>
            </table>
            <br />
            <br /> 
            <br /> 
            <br />
EOF;
        return $html;
    }
}
