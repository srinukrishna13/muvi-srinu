<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginHistroy
 *
 * @author SKM<support@muvi.com>
 */
class LoginHistory extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'login_history';
    }
    
    public function relations(){
        return array();
    }
    
    public function getLoginHistory($dt = '',$studio_id,$offset,$page_size,$searchKey) {
        if($dt == ''){
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $sDate = $dt->start;
            $eDate = $dt->end;
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%' OR ".$this->tableName().".ip LIKE '%".$searchKey."%')";
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS ".$this->tableName().".*,u.id AS user_id,u.display_name,u.email FROM ".$this->tableName()." LEFT JOIN sdk_users AS u ON ".$this->tableName().".user_id = u.id WHERE ".$this->tableName().".studio_id=".$studio_id." AND (DATE_FORMAT(login_at, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY ".$this->tableName().".id DESC LIMIT ".$offset.",".$page_size;
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    
    public function getSearchedLoginHistory($studio_id,$key) {
        $sql = "SELECT ".$this->tableName().".*,u.id AS user_id,u.display_name,u.email FROM ".$this->tableName()." LEFT JOIN sdk_users AS u ON ".$this->tableName().".user_id = u.id WHERE ".$this->tableName().".studio_id=".$studio_id." AND (u.email LIKE '%".$key."%' OR u.display_name LIKE '%".$key."%')";
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    public function getLoginHistoryReport($dt = '',$studio_id,$searchKey) {
       
        if(empty($dt)){
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
           // $dt = stripcslashes($dt);
            //$dt = json_decode($dt);
             $sDate = $dt->start;
            $eDate = $dt->end;
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%' OR ".$this->tableName().".ip LIKE '%".$searchKey."%')";
        }
       $sql = "SELECT ".$this->tableName().".*,u.id AS user_id,u.display_name,u.email FROM ".$this->tableName()." LEFT JOIN sdk_users AS u ON ".$this->tableName().".user_id = u.id WHERE ".$this->tableName().".studio_id=".$studio_id." AND (DATE_FORMAT(login_at, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr. " ORDER BY u.display_name";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
    public function getAllLoginHistory($studio_id) {
        $sql = "SELECT * FROM ".$this->tableName()." WHERE studio_id=".$studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    public function getAllCurrentLogin($studio_id, $user_id, $device_type) {
        $login_history_data = array();
        $sql = "SELECT * FROM `login_history` WHERE `studio_id` = {$studio_id} AND `user_id` = {$user_id} AND  (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
        $login_data = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($login_data)) {
            $session_time = $this->getSesssionTime($studio_id);
            foreach ($login_data as $data) {
                $last_login_check = @$data['login_at'];
                if ($device_type == 0) {
                    if (@$data['remember_me'] == '0') {
                        $last_login_check = @$data['last_login_check'];
                        $seconds_to_add = $session_time;
                        $time = new DateTime($last_login_check);
                        $time->add(new DateInterval('PT' . $seconds_to_add . 'S'));
                        $expiry_time = $time->format('Y-m-d H:i:s');
                    } else {
                        $seconds_to_add = 3600 * 24 * 30;
                        $time = new DateTime($last_login_check);
                        $time->add(new DateInterval('PT' . $seconds_to_add . 'S'));
                        $expiry_time = $time->format('Y-m-d H:i:s');
                    }
                    if (strtotime($expiry_time) > strtotime("now")) {
                        $login_history_data[$data['id']] = $expiry_time;
                    } else {
                        $login_history = LoginHistory::model()->findByPk(@$data['id']);
                        $login_history->logout_at = $expiry_time;
                        $login_history->save();
                    }
                } else {
                    $login_history_data[$data['id']] = $last_login_check;
                }
            }
        }
        return $login_history_data;
    }
    public function checkUserLoginTime($user_id,$studio_id,$login_history_id){
        $session_time = $this->getSesssionTime($studio_id);
        $login_history = $this->findByPk($login_history_id);
        $login_history_cookie_name = "login_history_id_".$studio_id."_".$user_id;
        if($login_history->logout_at != '0000-00-00 00:00:00'){
            $expiry_time = $login_history->logout_at;
        }else{
            if($login_history['remember_me'] == '0'){
                $last_login_check = $login_history['last_login_check'];
                $seconds_to_add = $session_time;
                $time = new DateTime($last_login_check);
                $time->add(new DateInterval('PT' . $seconds_to_add . 'S'));
                $expiry_time = $time->format('Y-m-d H:i:s');
            }else{
                $last_login_check = $login_history['login_at'];
                $seconds_to_add = 3600*24*30;
                $time = new DateTime($last_login_check);
                $time->add(new DateInterval('PT' . $seconds_to_add . 'S'));
                $expiry_time = $time->format('Y-m-d H:i:s');
            }
        }
        if(strtotime($expiry_time) <= strtotime("now")){
            $login_history->logout_at = $expiry_time;
            $login_history->save();
            Yii::app()->user->logout(); 
            Yii::app()->session->clear();
            Yii::app()->session->destroy();
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//", $domain);
            $domain = $domain[1];
            if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                setcookie($login_history_cookie_name, null, time()-3600, '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
            }
        }
        return true;
    }
    public function getSesssionTime($studio_id){
        $session_time = Yii::app()->session->timeout;
        $config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'session_timeout');
        if(!empty($config)){
            $session_time = $config['config_value'];
        }else{
            $config_all = StudioConfig::model()->getconfigvalueForStudio(0,'session_timeout');
            if(!empty($config_all)){
                $session_time = $config_all['config_value'];
        }
        }
        return $session_time;
    }
    public function logoutUser($studio_id,$user_id,$login_history_id=false){
        $history = new LoginHistory;
        $params= array(':studio_id'=>$studio_id,':user_id'=>$user_id,':logout_at'=>'0000-00-00 00:00:00');
        $condition = "studio_id=:studio_id AND user_id =:user_id AND (logout_at=:logout_at OR logout_at IS NULL)";
        if($login_history_id){
            $condition.=" AND id !=".$login_history_id;
        }
        $attr = array('logout_at'=>date('Y-m-d H:i:s'));
        $history = $history->updateAll($attr,$condition,$params);
    }
}
