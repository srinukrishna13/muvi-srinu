<style type="text/css">
    .m-t-15{margin-top: 15px;margin-left: 5px;}
</style>
<?php


    $selcontents = '';
    if (@$content) {
        $selcontents = json_encode($content);
    }
?>
<script>
    sel_contents = '<?php echo $selcontents;?>';
</script>
<?php 
$disable = "";

if (!empty($res)) {
        if ($language_id == $res[0]['language_id']) {
            if ($res[0]['parent_id'] > 0) {
                $disable = "disabled";
            }
        } else {
            $disable = "disabled";
        }
    }

?>
<div class="modal-dialog">
    <form action="javascript:void(0);" class="form-horizontal" method="post" name="subscription_form" id="subscription_form" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php if (isset($res[0]['plan_id']) && !empty($res[0]['plan_id'])){echo 'Edit';} else {echo 'Add';} ?> Subscription Bundles Plan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="errorTxt"></div>
                    </div>
                </div>
                <div class="form-group">
                   
                    <label class="col-sm-4 control-label" for="plan_name">Plan Name:</label>
                   
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <?php $plan_name =  (isset($res[0]['name']) && !empty($res[0]['name'])) ? $res[0]['name'] : ""; ?>
                            
                            <input type="text" class="form-control input-sm" placeholder="Plan Name" id="plan_name" name="data[name]" value="<?php echo $plan_name; ?>" maxlength="50" required="required">
                        </div>
                        <input type="hidden" value="<?php if (isset($res[0]['plan_id']) && !empty($res[0]['plan_id'])){echo $res[0]['plan_id'];} ?>" name="data[id]"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="subscription_period">Duration:</label>
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <div class="select">
                            <select class="form-control input-sm rmvdisable" id="recurrence" name="data[recurrence]" <?php echo $disable; ?>>
                                <option value="1 Month" <?php if (isset($res[0]['recurrence']) && !empty($res[0]['recurrence']) && ($res[0]['recurrence'] == 'Month') && (isset($res[0]['frequency']) && !empty($res[0]['frequency']) && $res[0]['frequency'] == 1)){?>selected="selected"<?php } ?>>Monthly</option>
                                <option value="3 Month" <?php if (isset($res[0]['recurrence']) && !empty($res[0]['recurrence']) && ($res[0]['recurrence'] == 'Month') && (isset($res[0]['frequency']) && !empty($res[0]['frequency']) && $res[0]['frequency'] == 3)){?>selected="selected"<?php } ?>>Quarterly</option>
                                <option value="6 Month" <?php if (isset($res[0]['recurrence']) && !empty($res[0]['recurrence']) && ($res[0]['recurrence'] == 'Month') && (isset($res[0]['frequency']) && !empty($res[0]['frequency']) && $res[0]['frequency'] == 6)){?>selected="selected"<?php } ?>>Semi-Annual</option>
                                <option value="1 Year" <?php if (isset($res[0]['recurrence']) && !empty($res[0]['recurrence']) && ($res[0]['recurrence'] == 'Year') && (isset($res[0]['frequency']) && !empty($res[0]['frequency']) && $res[0]['frequency'] == 1)){?>selected="selected"<?php } ?>>Yearly</option>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="sub_price_box">
                <?php 
                if (isset($res) && !empty($res)) {//Update case
                    $cnt = 0;
                    foreach ($res as $key => $value) {
                        $pricing_id = $value['pricing_id'];
                        $price = $value['price'];
                        $price_status = $value['price_status'];

                        $currency_id = $value['currency_id'];
                        $symbol = $value['symbol'];
                        $code = $value['code'];
                       ?>
                    <div class="form-group">
                        <?php if ($cnt == 0) { ?>
                        <label class="col-sm-4 control-label" for="price">Subscription Bundle Fee:</label>
                        <?php } else{ ?>
                        <label class="col-sm-4 control-label">&nbsp;</label>
                        <?php }
                        $cnt++;
                        ?>
                        <div class="col-sm-4 row">
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="statuschk" name="data[status][]" <?php echo $disable; ?> value="<?php echo $currency_id;?>" <?php if (isset($price_status) && intval($price_status)) { ?>checked="checked" <?php } ?> <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true" checked="checked"<?php }?> /><i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8 p-l-0">
                               <input type="hidden" name="data[pricing_id][]" value="<?php echo $pricing_id;?>" />
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm currency" name="data[currency_id][]" <?php echo $disable; ?> <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                        <?php foreach ($currency as $key1 => $value1) { ?>
                                        <option value="<?php echo $value1['id'];?>" <?php if ($currency_id == $value1['id']) {?>selected="selected"<?php }?>><?php echo $value1['code'];?>(<?php echo $value1['symbol'];?>)</option>
                                        <?php }?>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 row">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm rmvdisable subscription_fee" placeholder="Subscription Fee" name="data[price][]" value="<?php echo $price; ?>" required="required" <?php echo $disable; ?>>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                <?php } else {//Add case ?>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="price">Subscription Fee:</label>
                        <div class="col-sm-4 row">
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="statuschk" name="data[status][]" checked="checked" disabled="disabled" value="<?php echo $studio->default_currency_id;?>" /><i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8 p-l-0">
                               <input type="hidden" name="data[pricing_id][]" value="" />
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm currency" name="data[currency_id][]" disabled="disabled">
                                            <?php foreach ($currency as $key => $value) { ?>
                                                <option value="<?php echo $value['id']; ?>" <?php if ($studio->default_currency_id == $value['id']) {?>selected="selected"<?php } ?>><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 row">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm rmvdisable subscription_fee" placeholder="Subscription Fee" name="data[price][]" value="" required="required">
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
                <label id="data[currency_id][]-error" class="error red curncycls" for="data[currency_id][]" style="display: none;"></label>
                <label style="float:right;display: none;" for="data[price][]" class="error red" id="data[price][]-error"></label>
                <div class='clearfix'></div>
                <?php /* ?><div class="form-group">
                    <div class="pull-right">
                        <?php if($disable ==""): ?> 
                        <a href="javascript:void(0);" onclick="addMorePrice();" class="text-gray" >
                            <em class="icon-plus"></em>
                            Add more price for specific country
                        </a>
                        <?php endif; ?>
                    </div>
                    <div class="clearfix"></div>
                </div><?php */ ?>
                
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="checkbox">
                        <label>
                            <input type="checkbox" id="free_trial_period" class="rmvdisable" name="free_trial_period" value="1" <?php if (isset($res[0]['trial_period']) && intval($res[0]['trial_period'])){ ?>checked="checked"<?php } ?> <?php echo $disable; ?>/>
                            <i class="input-helper"></i> Free Trial Period
                        </label>
                        </div>
                    </div>

                    <div class="col-sm-8" id="trial_period_div" style="<?php if (isset($res[0]['trial_period']) && intval($res[0]['trial_period'])){ } else {?>display: none;<?php } ?>">
                    <div class="fg-line">
                        <div class="select">
                        <?php
                        $trial_period = '7 Day';
                        if (isset($res[0]['trial_period']) && !empty($res[0]['trial_period']) && isset($res[0]['trial_recurrence']) && !empty($res[0]['trial_recurrence'])) {
                            $trial_period = $res[0]['trial_period']." ".$res[0]['trial_recurrence'];
                        }
                        ?>
                        <select class="form-control input-sm rmvdisable" id="trial_recurrence" name="data[trial_recurrence]" <?php echo $disable; ?>>
                            <option value="1 Day" <?php if ($trial_period == '1 Day'){?>selected="selected"<?php } ?>>1 Day</option>
                            <option value="7 Day" <?php if ($trial_period == '7 Day'){?>selected="selected"<?php } ?>>7 Days</option>
                            <option value="14 Day" <?php if ($trial_period == '14 Day'){?>selected="selected"<?php } ?>>14 Days</option>
                            <option value="1 Month" <?php if ($trial_period == '1 Month'){?>selected="selected"<?php } ?>>1 Month</option>
                        </select>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="form-group">
                                    <label class="col-sm-4" for="content">Content: </label>
                                    <div class="col-sm-8">

                                        <div class="fg-line">
                                            <select class="input-sm" data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content" multiple>
                                            </select>
                                        </div>
                                        <small class="help-block">
                                            <label id="data[content]-error" class="error red" for="data[content][]" style="display: none"></label>
                                        </small>
                                    </div>
                                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="data[pricestatus]" value=""  id="status_id" />
                <button type="button" class="btn btn-primary btn-sm" onclick="return validateSubscriptionForm();" id="sub-btn"><?php if (isset($res[0]['plan_id']) && !empty($res[0]['plan_id'])){echo 'Update';} else {echo 'Submit';} ?></button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>

<div id="price-div" style="display: none;">
    <div class="form-group">
        <label class="col-sm-4 control-label">&nbsp;</label>
        <div class="col-sm-4 row">
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="statuschk" name="data[status][]" value="<?php echo $currency[0]->id;?>" /><i class="input-helper"></i>
                    </label>
                </div>
            </div>
            <div class="col-md-8 p-l-0">
               <input type="hidden" name="data[pricing_id][]" value="" />
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm currency" name="data[currency_id][]">
                            <?php foreach ($currency as $key => $value) { ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 row">
            <div class="fg-line">
                <input type="text" class="form-control input-sm rmvdisable subscription_fee" placeholder="Subscription Fee" name="data[price][]" value="" required="required">
            </div>
        </div>
        <div class="col-sm-2 row m-t-15 text-right">
            <a href="javascript:void(0);" onclick="removeBox(this);" class="text-black">
                <em class="icon-trash"></em>&nbsp; Delete
            </a>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    Array.prototype.getUnique = function(){
       var u = {}, a = [];
       for(var i = 0, l = this.length; i < l; ++i){
          if(u.hasOwnProperty(this[i])) {
             continue;
          }
          a.push(this[i]);
          u[this[i]] = 1;
       }
       return a;
    }
    
    $(function() {
        $('.subscription_period').on("keypress",function(event) {
            return numbersonly(event);
        });
        
        $( "#free_trial_period" ).change(function() {
            $( "#trial_period_div" ).toggle( "slow", function() {
            });
            if ($("#free_trial_period").is(':checked')) {
                $("#free_trial_period").val(1);
            } else {
                $("#free_trial_period").val(0);
            }
        });
        
        setStatus();
        restrictPrice();
    });
    
    function setStatus() {
        $('.currency').on("change",function(event) {
            $(this).parent().parent().parent().prev('div').find('.statuschk').val($(this).val());
        });
    }
    
    function restrictPrice() {
        $('.subscription_fee').on("keypress",function(event) {
            return decimalsonly(event);
        });
        
        $('.subscription_fee').on("contextmenu",function(event) {
            return false;
        });
    }
    /* function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
           if((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) )
           {
            return true;
          }
           else{
              return false; 
           }
        }
     function restrictPlanName() {
        $('#plan_name').on("keypress",function(event) {
            return IsAlphaNumeric(event);
        });
       
    }*/
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
    
    function addMorePrice() {
        var isTrue = 0;
        $("#sub_price_box").find(".subscription_fee").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        
        if (parseInt(isTrue)) {
            var str = $("#price-div").html();
            $("#sub_price_box").append(str);
            setStatus();
            restrictPrice();
        }
    }
    
    function removeBox(obj) {
        $(obj).parent().parent().remove();
    }
    
    function validateSubscriptionForm() {
        var validate = $("#subscription_form").validate({
            rules: {
                'data[name]': {
                    required: true
                },
                'data[recurrence]': {
                    required: true
                },
                'data[price][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                }
            },
            messages: {
                'data[name]': {
                    required: 'Please enter plan name'
                },
                'data[recurrence]': {
                    required: 'Please enter subscription period'
                },
                'data[price][]': {
                    required: 'Please enter subscription fee',
                    allrequired: 'Please enter subscription fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                }
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();
        if (x) {
             if (!$.trim($("#content").val())) {
                swal("Oops! The given content doesn't exists in your studio");
                return false;
            }
            var currency = new Array();
            $("#sub_price_box").find(".statuschk").each(function(){
                if ($(this).is(':checked')) {
                    currency.push($(this).parent().parent().parent().next('div').find('select').val());
                }
            });
            
            $("#status_id").val(currency.toString());
            $(".statuschk").each(function(){
                $(this).prop("disabled", false);
            });

            $(".currency").each(function(){
                $(this).prop("disabled", false);
            });
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/AddEditSubscriptionbundles";
            $('.rmvdisable').removeAttr('disabled');
            document.subscription_form.action = url;
            document.subscription_form.submit();
        }
    }
        
jQuery.validator.addMethod("allrequired", function (value, element) {
    var isTrue = 1;
    $("#sub_price_box").find(".statuschk").each(function(){
        if ($(this).is(':checked')) {
            var price = $(this).parent().parent().parent().parent().next('div').find('.subscription_fee').val();
            if (price === '') {
                isTrue = 0;
                return false;
            }
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Please enter subscription fee");

jQuery.validator.addMethod("price", function (value, element) {
    var isTrue = 1;
    $("#sub_price_box").find(".statuschk").each(function(){
        if ($(this).is(':checked')) {
            var price = $(this).parent().parent().parent().parent().next('div').find('.subscription_fee').val();
            
            price = $.trim(price);
            price = Math.floor(price * 100) / 100;
            price = Math.round(price);
            if (price <= 0) {
                isTrue = 0;
                return false;
            }
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Minimum price should be 50 cent");

jQuery.validator.addMethod("uniquecurrency", function (value, element) {
    var currency = new Array();
    
    $("#sub_price_box").find(".statuschk").each(function(){
        if ($(this).is(':checked')) {
            currency.push($(this).parent().parent().parent().next('div').find('select').val());
        }
    });
    
    var cur = currency.getUnique();
    if (cur.length === currency.length) {
        return true;
    } else {
        return false;
    }
}, "Currency should be unique");
  
</script>


 