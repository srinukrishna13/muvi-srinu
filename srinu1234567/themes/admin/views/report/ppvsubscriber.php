<input type="hidden" class="data-count" value="<?php echo $ppv_subscription_count;?>" />
<thead>
    <th>User</th>
    <th>GATEWAY REF</th>
    <th>Amount</th>
    <th data-hide="phone">Date & Time</th>
    <th data-hide="phone">Content</th>
</thead>
<tbody class="list">
    <?php
    
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
        if(isset($ppv_subscription) && count($ppv_subscription)){
          
            foreach ($ppv_subscription as $key=>$value){
                 
                $date = new DateTime($value['start_date']);
                $date->setTimezone(new DateTimeZone('GMT'));
                $Moviename="";
                $fnameSeason="";
                $episodeName="";
                if(isset($value['name']) && !empty($value['name'])){
                   $Moviename= $value['name'];
                }
                if((isset($value['season_id']) && !empty($value['season_id']))){
                   $fnameSeason= "->Season #".$value['season_id']; 
                }
                if((isset($value['episode_id']) && !empty($value['episode_id']))){
                  
                   $episode_title=Yii::app()->pdf->getEpisodeName($value['episode_id']);
                   $episodeName= "<br>->".$episode_title;
                }
                $fname=$Moviename.$fnameSeason.$episodeName;
                
    ?>
    <tr>
        <td class="email_ppv">
            <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)){
                $totalrevenue=$value['amount']*($percentageshare/100);
                
                ?>
                <?php echo ($value['email']);?>
            <?php } else { 
                $totalrevenue=$value['amount'];
                
                ?>
            <a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email']?$value['email']:$value['email'];?></a>
            <?php } ?>
            
        </td>
        <td class="gatewayref"><?php  if($value['payment_method'] != ''){ echo (($value['payment_method'])?$value['payment_method']:'').' : '.(($value['order_number'])?$value['order_number']:''); }else { echo 'N/A'; }?></td>
        <td class="amount"><?php echo $totalrevenue?Yii::app()->common->formatPrice($totalrevenue,$value['currency_id']):Yii::app()->common->formatPrice(0,$value['currency_id']);?></td>
        <td class="date_time"><?php echo $date->format('F j, Y, g:ia'). ' GMT';?></td>
        <td class="content"><?php echo $fname;?></td>
    </tr>
    <?php }}else{?>
    <tr>
        <td colspan="4">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>