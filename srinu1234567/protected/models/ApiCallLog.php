<?php

class ApiCallLog extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'api_call_log';
    }

    function saveApiLog($studio_id, $unique_id, $call_for, $params) {
        $apilog = new ApiCallLog;
        $apilog->studio_id = $studio_id;
        $apilog->unique_id = $unique_id;
        $apilog->call_for = $call_for;
        $apilog->params = $params;

        $ret = $apilog->save();

        $apilog_id = $apilog->id;
        $arr['apilog_id'] = $apilog_id;
    }

}
