<?php
require 's3bucket/aws-autoloader.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';

use Aws\S3\S3Client;

class TemplateController extends Controller {

    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = 'admin';
    public $draft = 0;
    public $image;
    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        } else {
            $this->checkPermission();
        }
        Yii::app()->theme = 'admin';
        //$title_page = Yii::app()->controller->action->id;
        // $title_page = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_page);
        //echo $Word; exit;
        $this->pageTitle = Yii::app()->name . ' | Manage Home';
        return true;
    }

    /**
     * @method public home() Default action
     * @author GDR<support@muvi.in>
     * @return HTML 
     */
    public function actionHome() {
        $this->breadcrumbs = array('Website','Templates','Manage Template');
        $this->headerinfo = "Manage Template";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Manage Template';
        $studio_id = Yii::app()->common->getStudioId();
        $content_types = StudioContent::model()->getContentSettings($studio_id);
        $std = $this->studio;
        $studio_theme = $std->parent_theme;
        $active_template = Template::model()->with(array('template_colors' => array('alias' => 'tc')))->findAll(array('condition' => 't.code = "'.$studio_theme.'"'));
        $condition = $cond = "";
        if(isset($content_types) && (($content_types['content_count'] & 1) || ($content_types['content_count'] & 2))){
            $cond .= " t.content_type = '0'";
        }
        if(Yii::app()->general->getStoreLink()){
            if($cond != ""){
              $cond .= " OR ";  
            }
            $cond .= " t.content_type = '1'";
        }
        if(isset($content_types) && (($content_types['content_count'] & 4))){
            if($cond != ""){
              $cond .= " OR ";  
            }
            $cond .= " t.content_type = '3'";
        }
        if($cond != ""){
            $condition = " AND ( ".$cond." ) ";
        }
        $templates = Template::model()->with(array('template_colors' => array('alias' => 'tc')))->findAll(array('condition' => 't.status=1 AND t.code != "'.$studio_theme.'"'.$condition, 'order' => 'tc.id ASC'));
        $this->render('template', array('templates' => $templates, 'studio' => $std,'active_template'=>$active_template));
    }

    function actionSavetemplate() {
        $studio_id = Yii::app()->common->getStudioId();
        if (isset($_REQUEST) && isset($_REQUEST['template_name']) && $_REQUEST['template_name'] != '' && isset($_REQUEST['template_color']) && $_REQUEST['template_color'] != '') {
            $template_name = $_REQUEST['template_name'];
            $template_color = $_REQUEST['template_color'];
            $in_preview_theme = $_REQUEST['in_preview_theme'];
            $ip_address = Yii::app()->getRequest()->getUserHostAddress();
            $current_template = Template::model()->findByAttributes(array('code' => $template_name), array('select' => 'id'));
            $current_template_id = $current_template->id;
           	$data = StudioBannerStyle::model()->findByAttributes(array('studio_id' => $studio_id),array('select' => 'banner_style_code'));
			if ($studio_id > 0) {
                $std = new Studio();
                $std = $std->findByPk($studio_id);
                $old_parent_theme = $std->parent_theme;
                $old_theme = $std->theme;
                if ($in_preview_theme == 1) {
                    $theme = $old_theme . '_preview';
                    $sourcePath = 'themes/' . $theme . '/';
                    if (@file_exists(ROOT_DIR . '/' . $sourcePath) && $theme)
                        Yii::app()->common->rrmdir(ROOT_DIR . '/' . $sourcePath);

                    @mkdir($sourcePath, 0777, true);
                    chmod($sourcePath, 0777);

                    $source = ROOT_DIR . '/sdkThemes/' . $template_name . '-preview/';
                    $ret = Yii::app()->common->recurse_copy($source, $sourcePath);

                    $cookie = new CHttpCookie('in_preview_theme', 1);
                    $cookie->expire = time() + 60 * 5;
                    Yii::app()->request->cookies['in_preview_theme'] = $cookie; // will send the cookie                    

                    $std->in_preview_theme = 1;
                    $std->preview_theme = $theme;
                    $std->preview_parent_theme = $template_name;
                    $std->preview_theme_color = $template_color;
                    $std->preview_ip = $ip_address;
                    $std->save();

                    $url = $this->createUrl('/template?preview=1');
                    $this->redirect($url);
                    exit();
                }
                else {

                    $template_id = 0;
                    $temps = new Template;
                    $temps = $temps->findAllByAttributes(array('code' => $old_parent_theme), array('order' => 'id ASC'));
                    if (count($temps) > 0)
                        $template_id = $temps[0]->id;
                    if ($template_name != $old_parent_theme) {
                        if (Yii::app()->user->id != SAMPLE_DATA_STUDIO) {
                            //Deleting the banners

                            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                            $bucket = $bucketInfo['bucket_name'];
                            $s3dir = $unsignedFolderPathForVideo . "public/system/studio_banner/" . $studio_id;
							if(empty($data)){
                                $bn = new StudioBanner;
                                $banners = $bn->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
                                if (count($banners) > 0) {
                                    foreach ($banners as $banner) {
                                        $banner_id = $banner->id;
                                        $result = $s3->deleteObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => $s3dir . "/original/" . $banner->image_name
                                        ));

                                        $result = $s3->deleteObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => $s3dir . "/studio_thumb/" . $banner->image_name
                                        ));

                                        $res = $banner->delete();
                                    }
                                }
                                //Deleting banner texts
                                $bnr = new BannerText();
                                $banner_texts = $bnr->findAllByAttributes(array('studio_id' => $studio_id, 'mode' => $this->draft));
                                if (count($banner_texts) > 0) {
                                    foreach ($banner_texts as $banner_text) {
                                        $banner_text->delete();
                                    }
                                }
							}
                            //Updating template id of featured sections
                            $feat = new FeaturedSection;
                            $attr = array('template_id' => $current_template_id);
                            $condition = "studio_id=:studio_id";
                            $params = array(':studio_id' => $studio_id);
                            $feat = $feat->updateAll($attr, $condition, $params);

                            //Deleting featured sections
                            /* $section = new FeaturedSection;
                              $sections = $section->findAllByAttributes(array('studio_id' => $studio_id,'mode' => $this->draft), array('order' => 'id_seq ASC'));
                              if (count($sections) > 0) {
                              foreach ($sections as $section) {
                              $featureds = Yii::app()->common->getFeaturedContents($section->id);
                              if (count($featureds) > 0) {
                              foreach ($featureds as $featured) {
                              $featured->delete();
                              }
                              }
                              $section->delete();
                              }
                              } */
                        }

                        //Creating the themebackup folder if not exists
                        $bakup_path = ROOT_DIR . '/themebackups';
                        if (!file_exists($bakup_path)) {
                            $res = @mkdir($bakup_path, 0777, true);
                            chmod($bakup_path, 0777);
                        }

                        //Collecting the source theme folder
                        $sourcePath = 'themes/' . $old_theme . '/';
                        if ($old_theme && file_exists($sourcePath)) {
                            //Setting the target ZIP file
                            $bkp_file = $old_theme . '_' . $old_parent_theme . '_' . date("Y-m-d-H-i-s") . '.zip';
                            $destPath = 'themebackups/' . $bkp_file;

                            //Creating ZIP
                            $zip = Yii::app()->zip;
                            $res = $zip->zipDir($sourcePath, $destPath);

                            $sourcePath = ROOT_DIR . '/themes/' . $old_theme . '/';
                            if ($old_theme) {
                                Yii::app()->common->rrmdir($sourcePath);
                            }
                        }
                        //Creating the theme folder to ignore duplicacies
                        @mkdir($sourcePath, 0777, true);
                        chmod($sourcePath, 0777);
                        $source = ROOT_DIR . '/sdkThemes/' . $template_name . '/';
                        $ret = Yii::app()->common->recurse_copy($source, $sourcePath);
                    }

                    $std->in_preview_theme = 0;
                    $std->parent_theme = $template_name;
                    $std->default_color = $template_color;
                    $std->save();

                    $log = new TemplateLog();
                    $log->studio_id = $studio_id;
                    $log->template_code = $old_parent_theme . ' to ' . $template_name;
                    $log->backup_file = @$bkp_file;
                    $log->action_at = new CDbExpression("NOW()");
                    $log->action = 'change';
                    $log->ip_address = CHttpRequest::getUserHostAddress();
                    $log->save();

                    //To disable preview when applied
                    $preview_template = new Previewtemplatehistory();
                    $previews = $preview_template->findAllByAttributes(array('studio_id' => $studio_id, 'preview_ip' => $ip_address), array('order' => 'id DESC'));
                    foreach ($previews as $preview) {
                        $preview->is_preview = 0;
                        $preview->save();
                    }

                    unset(Yii::app()->request->cookies['in_preview_theme']);

			if(!empty($data)){
			   $temps = new Template;
			   $template = $temps->findAllByAttributes(array('code' => $template_name), array('order' => 'id ASC'));
			   $template_id = $template[0]->id;
			   $sc = new BannerSection;
			   $sections = $sc->findAllByAttributes(array('template_id' => $template_id, 'studio_id' => 0,'is_new' => 0));
               if ($template_name == "traditional-byod") {
                 $sections = $sc->findAllByAttributes(array('template_id' => $template_id, 'studio_id' => 0, 'is_new' => 1));
                }
               $bn = new StudioBanner;
			   $banners = $bn->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
			   $section_id=$sections[0]->id;
			   foreach ($banners as $banner){
					$banner->banner_section = $section_id;
					$banner->save();
			   }
			   $theme = $this->studio->theme;
			   $file = $data['banner_style_code']."/banner.html";
			   $root = ROOT_DIR . 'bannerStyle/'.$file;
			   chmod(ROOT_DIR . 'bannerStyle/', 0777);
			   chmod($root, 0777);
			   $path = ROOT_DIR . 'themes/' . $theme . '/views/layouts/banner.html';
			   if (@file_exists($path) && @file_exists($root)){
				   copy($root, $path);
			   }
			}
                    Yii::app()->user->setFlash('success', 'Template updated.');
                }
            }
        }
        $url = $this->createUrl('/template');
        $this->redirect($url);
        exit;
    }

    //To upload video banner
    public function actionVideobanner() {
        $this->breadcrumbs = array('Video Banner',);
        $this->pageTitle = Yii::app()->name . ' | ' . 'Upload Video Banner';
        $studio = $this->studio;
        $studio_id = $studio->id;
        $theme = $this->studio->parent_theme;

        $template_id = 0;
        $temps = new Template;
        $temps = $temps->findAllByAttributes(array('code' => $theme), array('order' => 'id ASC'));
        if (count($temps) > 0)
            $template_id = $temps[0]->id;

        $sc = new BannerSection;
        $section = $sc->findByAttributes(array('template_id' => $template_id));

        $ban = new StudioBanner;
        $data = $ban->findAllByAttributes(array('studio_id' => $studio_id, 'banner_type' => '0', 'banner_section' => $section->id));

        $this->render('videobanner', array('studio' => $studio, 'studio_id' => $studio_id, 'banners' => $data, 'section' => $section));
    }

    /* public function actionLogo() {
      $this->breadcrumbs = array('Website','Templates','Logo');
      $this->headerinfo = "Logo";
      $this->pageTitle = Yii::app()->name . ' | ' . Logo;
      $studio_id = Yii::app()->common->getStudioId();
      $std = new Studio();
      $studio = $std->findByPk($studio_id);
      $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
      $logo_dimension = Yii::app()->general->getLogoDimension($studio_id);
      $this->render('logo', array('studio' => $studio, 'logo_dimension' => $logo_dimension, 'all_images' => $all_images));
      } */

    /**
     * @method Public saveFavicon()
     * @author GDR<support@muvi.com>
     * @return HTML 
     */
    public function actionSaveFavicon() {
        //echo "<pre>";print_r($_REQUEST);print_r($_FILES);exit;
        $studio_id = Yii::app()->user->studio_id;
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        $user_id = Yii::app()->user->id;
        $std = $this->studio;
        $theme_folder = $std->theme;
        $width = $height = 16;
        $cropDimension = array('original' => $width . 'x' . $height);
        if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
            $return = $this->processLogoImage($theme_folder, $cropDimension, 'favicon');
            $std->favicon_name = $_FILES['Filedata']['name'];
            $std->save();

            $url = $this->createUrl('/template/homepage');
            $this->redirect($url);
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Please choose a icon to upload.');
            $this->redirect($this->createUrl('template/homepage'));
            exit;
        }
    }

    public function actionSavelogo() {
        $studio_id = Yii::app()->user->studio_id;
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        $user_id = Yii::app()->user->id;
        $std = $this->studio;
        $theme_folder = $std->theme;
        $parent_theme = $std->parent_theme;
        $logo_dimension = Yii::app()->general->getLogoDimension($studio_id);
        $width = $logo_dimension['logo_width'];
        $height = $logo_dimension['logo_height'];
        $cropDimension = array('original' => $width . 'x' . $height);
        if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
            $return = $this->processLogoImage($theme_folder, $cropDimension, 'logos');
            $imgName = $_FILES['Filedata']['name'];
            $std->show_sample_data = 0;
            $std->logo_file = $imgName;
            $std->save();
            $this->redirect($this->createUrl('template/homepage'));
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Please choose image to upload.');
            $url = $this->createUrl('/template/homepage');
            $this->redirect($url);
            exit;
        }
    }

    /**
     * @method public processlogoImage() It will crop and upload the image into the respective bucket with mentioned size
     * @author Gayadhar<support@muvi.com>
     * @return array Image path array
     */
    function processLogoImage($theme_folder = '', $cropDimension = '', $type = 'logos') {
        $studio_id = Yii::app()->common->getStudiosId();
        $url = $this->createUrl('/template/homepage');
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $theme_folder;
        if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
            $file_info = pathinfo($_FILES['Filedata']['name']);
            $extension = array('PNG', 'png', 'JPG', 'jpeg', 'JPEG', 'jpg');
            if (!in_array($file_info['extension'], $extension)) {
                Yii::app()->user->setFlash('error', 'Please upload valid files formats(png,jpg,jpeg).');
                $url = $this->createUrl('/template/homepage');
                $this->redirect($url);
                exit;
            }

            if ($_REQUEST['fileimage']['w'] == '' && $_REQUEST['fileimage']['h'] == '') {
                Yii::app()->user->setFlash('error', 'Please crop image to upload.');
                $this->redirect($url);
                exit;
            }
            $cdimension = array('thumb' => "64x64");
            $ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);

            $path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
            if (@$path) {
                $ret = $this->uploadLogo($_FILES['Filedata'], $cropDimension, $theme_folder, $path, $type);
                Yii::app()->common->rrmdir($dir . '/jcrop');
            } else {
                Yii::app()->user->setFlash('error', 'Error in uploading image,Please try another image');
                $this->redirect($url);
                exit;
            }
            return $ret;
        } else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name']);
            $_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $jcrop_allimage = $_REQUEST['jcrop_allimage'];
            $image_name = $_REQUEST['g_image_file_name'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];

            $path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
            if (@$path) {
                $fileinfo['name'] = $_REQUEST['g_image_file_name'];
                $fileinfo['error'] = 0;
                $_FILES['Filedata']['name'] = $_REQUEST['g_image_file_name'];
                $ret = $this->uploadLogo($fileinfo, $cropDimension, $theme_folder, $path, $type);
            } else {
                Yii::app()->user->setFlash('error', 'Error in uploading image,Please try another image');
                $this->redirect($url);
                exit;
            }
            Yii::app()->common->rrmdir($dir . '/jcrop');
            return $ret;
        } else {
            return false;
        }
    }

    /* public function actionBanner() {
      $this->breadcrumbs = array('Website','Templates','Banner List');
      $this->headerinfo = "Banner List";
      $this->pageTitle = Yii::app()->name . ' | ' . 'Banner List';
      $studio_id = Yii::app()->common->getStudioId();
      $theme = $this->studio->parent_theme;

      $template_id = 0;
      $temps = new Template;
      $temps = $temps->findAllByAttributes(array('code' => $theme), array('order' => 'id ASC'));
      if (count($temps) > 0)
      $template_id = $temps[0]->id;

      $sc = new BannerSection;
      $sections = $sc->findAllByAttributes(array('template_id' => $template_id));
      $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);

      if (@IS_LANGUAGE == 1) {
      $sql = "SELECT * FROM banner_text WHERE studio_id={$studio_id} AND (language_id={$this->language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM banner_text WHERE studio_id={$studio_id} AND language_id={$this->language_id})) ORDER BY id DESC LIMIT 0, 1";
      } else {
      $sql = "SELECT * FROM banner_text WHERE studio_id={$studio_id} ORDER BY id DESC LIMIT 0, 1";
      }
      $banner_text = Yii::app()->db->createCommand($sql)->queryRow();
      //print '<pre>';print_r($banner_text);exit;

      $this->render('banners', array('sections' => $sections, 'banner_text' => $banner_text, 'all_images' => $all_images));
      } */

    //Saving the banner text
    function actionSaveBannerText() {
        $studio_id = Yii::app()->common->getStudioId();
        $theme = $this->studio->parent_theme;
        if (isset($_REQUEST) && $_REQUEST['update_text'] > 0) {
            $join_btn_txt = $_REQUEST['join_btn_txt'];
            $show_join_btn = $_REQUEST['show_join_btn'];

            if ($theme == 'traditional-byod') {
                $text_banner = isset($_REQUEST['bnr_txt']) ? $_REQUEST['bnr_txt'] : "";
            } else {
                $banner_text = isset($_REQUEST['bnr_txt']) ? htmlspecialchars($_REQUEST['bnr_txt']) : "";
            }
            $url_text = isset($_REQUEST['url_text']) ? $_REQUEST['url_text'] : "";
            $banner_id = isset($_REQUEST['banner_id']) ? $_REQUEST['banner_id'] : "";

            $bnr = new BannerText();
            $language_id = $this->language_id;
            $banner = $bnr->findByAttributes(array('studio_id' => $studio_id, 'parent_id' => 0), array('order' => 'id DESC'), array('limit' => 1));

            if (!empty($banner)) {
                $baner = $bnr->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('order' => 'id DESC'), array('limit' => 1));

                if (!empty($baner)) {//Update for both parent and child
                    $banr = $baner;
                } else {//Add new child with parent
                    $banr = new BannerText();
                    $banr->language_id = $language_id;
                    $banr->parent_id = $banner->id;
                }
            } else {//Add new parent
                $banr = new BannerText();
                $banr->language_id = $language_id;
                $banr->parent_id = 0;
            }


            $banr->bannercontent = $banner_text;
            $banr->show_join_btn = $show_join_btn;
            $banr->studio_id = $studio_id;
            $banr->join_btn_txt = $join_btn_txt;
            $banr->save();
            if (isset($_REQUEST['banner_id']) && $_REQUEST['banner_id'] != "") {
                for ($i = 0; $i < count($banner_id); $i++) {
                    $studio_banner = new StudioBanner;
                    $studio_banner = $studio_banner->findByPk($banner_id[$i]);
                    $studio_banner->url_text = $url_text[$i];
                    if ($theme == 'traditional-byod') {
                        $studio_banner->banner_text = isset($text_banner[$i]) ? $text_banner[$i] : "";
                    }
                    $studio_banner->save();
                }
            }
            Yii::app()->user->setFlash('success', 'Banner text updated.');
        }
        $url = $this->createUrl('/template/homepage');
        $this->redirect($url);
        exit;
    }

    /**
     * @method public addBannerImage() Adding top banner for SDK users only..
     * @author GDR<support@muvi.in>
     * @return boolen True/false
     */
    public function actionAddBannerImage() {
        $studio_id = $this->studio->id;
        $upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . "images/public/system/studio_banner/" . $studio_id . "/";
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
            chmod($upload_dir, 0777);
        }
        $upload_dir1 = $upload_dir . "original";
        if (!is_dir($upload_dir1)) {
            mkdir($upload_dir1, 0777, true);
            chmod($upload_dir1, 0777);
        }
        $bannUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        if(isset($_REQUEST) && count($_REQUEST) > 0){
            $app_image  = (isset($_REQUEST['app_image']) && $_REQUEST['app_image'] != "") ? $_REQUEST['app_image'] : 0;
            $s3         = Yii::app()->common->connectToAwsS3($studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucket     = $bucketInfo['bucket_name'];            
            if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                $title        = isset($_REQUEST['title']) ? $_REQUEST['title'] : '';
                $theme_folder = "";
                $format       = new Format();
                if($app_image){
                    $resp         = AppBannerSection::model()->getBannerDimension($studio_id);
                    $width        = $resp['banner_width'];
                    $height       = $resp['banner_height'];
                    $thumb        = AppBannerSection::model()->getAppThumbBannerDimension($studio_id);
                    $thumb_width  = $thumb['width'];
                    $thumb_height = $thumb['height'];
                }else{
                    $section      = new BannerSection;
                    $section      = $section->findByPk($_REQUEST['section_id1']);
                    $resp         = Yii::app()->common->getSectionBannerDimension($section->id);
                    $width        = $resp['width'];
                    $height       = $resp['height'];
                    $thumb        = Yii::app()->common->getThumbBannerDimension($section->id);
                    $thumb_width  = $thumb['width'];
                    $thumb_height = $thumb['height'];
                }
                //$cropDimension = array('original' => $width . 'x' . $height);
                $cropDimension = array('original' => $width . 'x' . $height,'studio_thumb' => $thumb_width. 'x' .$thumb_height, 'mobile_view' => '800x300','thumb' => '64x64', 'banner_original' => $width . 'x' . $height );  
                $file_path     = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, "system/studio_banner/" . $studio_id . "/original", $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'],false,'banner');
                $file_path     = $file_path['original'];
                if($file_path !=''){
                    $file_name = explode('original/',$file_path);
                    $bnrname =  $file_name[1];
                    $ip_address = CHttpRequest::getUserHostAddress();
                    if($app_image){
                        $id_seq        = AppBanners::model()->getMaxOrd()+1;
                        $studio_banner = new AppBanners;
                        $studio_banner->image_name   = $bnrname;
                        $studio_banner->studio_id    = $studio_id;
                        $studio_banner->id_seq       = $id_seq;
                        $studio_banner->created_date = new CDbExpression("NOW()");
                        $studio_banner->created_by   = Yii::app()->user->id;
                        $studio_banner->ip           = $ip_address;
                        $studio_banner->save();
                        $redirect_url = Yii::app()->getBaseUrl(true) . '/apps/homepage';
                    }else{
                        //Adding the sort order
                        $studiobnr = new StudioBanner;
                        $id_seq = $studiobnr->getMaxOrd()+1;
                        $studio_banner = new StudioBanner;
                        $studio_banner->title = htmlspecialchars($title);
                        $studio_banner->image_name = $bnrname;
                        $studio_banner->is_published = 1;
                        $studio_banner->banner_type = 1;
                        $studio_banner->studio_id = $studio_id;
                        $studio_banner->id_seq = $id_seq;
                        $studio_banner->created_date = new CDbExpression("NOW()");
                        $studio_banner->banner_section = $_REQUEST['section_id1'];
                        $studio_banner->ip = $ip_address;
                        $studio_banner->save();
                        $banner_id = $studio_banner->id;

                        $studio = $this->studio;
                        $studio->show_sample_data = 0;
                        $studio->save();
                        $redirect_url = Yii::app()->getBaseUrl(true) . '/template/homepage';
                    }
                    $msg = 'Banner added successfully.';
                    Yii::app()->user->setFlash('success', $msg);
                }else{
                     $msg = 'Error in processing data.';             
                     Yii::app()->user->setFlash('error', $msg);  
                }
            } 
        }else{
            $msg = 'Error in processing data.';             
            Yii::app()->user->setFlash('error', $msg);            
        }  
        $this->redirect($redirect_url);
        exit();
    }

    public function actionSectiondimention() {
        $section_id = $_REQUEST['section_id'];
        $resp = Yii::app()->common->getSectionBannerDimension($section_id);
        echo json_encode($resp);
    }

    public function actionSortBanner() {
        $success = 0;
        $msg = 'Error in processing data';

        if (isset($_REQUEST) && count($_REQUEST) > 0) {
            $studio_id = Yii::app()->common->getStudiosId();
            $section_id = $_REQUEST['section_id'];
            $banner_id = $_REQUEST['banner_id'];
            $direction = $_REQUEST['direction'];
            $seq_ary = $_REQUEST['seq_ary'];
            $seq_ary = explode(',', $seq_ary);
            $seq_of_cur = array_search($banner_id, $seq_ary);
            $temp = $seq_ary[$seq_of_cur];
            if ($direction == 'next') {
                $next_of_cur = Yii::app()->general->getNextItem($seq_ary, $seq_of_cur);
                $order_of_rest = $next_of_cur['key'];
            } elseif ($direction == 'previous') {
                $prev_of_cur = Yii::app()->general->getPrevItem($seq_ary, $seq_of_cur);
                $order_of_rest = $prev_of_cur['key'];
            }
            $seq_ary[$seq_of_cur] = $seq_ary[$order_of_rest];
            $seq_ary[$order_of_rest] = $temp;
            foreach ($seq_ary as $key => $val) {
                $studio_banner = StudioBanner::model()->findByPk($val);
                $studio_banner->id_seq = $key;
                $studio_banner->save();
            }
            $msg = "Banner ordered successfully.";
        }
        $response = array(
            'message' => $msg,
            'success' => $success
        );
        echo json_encode($response);
    }

    /**
     * @method public removeBanner($banner_id) It is used to remove a banner image.
     * @return bool 
     * @author GDR<support@muvi.in>
     */
    function actionRemoveBanner() {
        if (isset($_REQUEST['banner_id']) && $_REQUEST['banner_id']) {
            $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $s3dir = $unsignedFolderPath . "public/system/studio_banner/" . Yii::app()->common->getStudioId();
            $banner = StudioBanner::model()->findByPk($_REQUEST['banner_id']);
            if ($banner) {
                if ($banner->image_name) {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir . "/original/" . $banner->image_name
                    ));

                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir . "/studio_thumb/" . $banner->image_name
                    ));

                    $res = $banner->delete();
                    if ($res) {
                        $this->clearUsersiteCache();
                        Yii::app()->user->setFlash('success', 'Banner removed successfully');
                    }
                } else {
                    Yii::app()->user->setFlash('error', 'Oops! error in banner deletion');
                }
            } else {
                Yii::app()->user->setFlash('error', 'Oops! You do not haave access to this');
            }
        } else {
            Yii::app()->user->setFlash('error', 'You do not haave access to this');
        }
        $url = $this->createUrl('/template/homepage');
        $this->redirect($url);
        exit;
    }

    /**
     * @method public reorderBanner($banner_id) It is used to remove a banner image.
     * @return bool 
     * @author GDR<support@muvi.in>
     */
    function actionReorderBanner() {
        $orders = explode('&', $_REQUEST['orders']);
        foreach ($orders as $key => $item) {
            $item = explode('=', $item);
            $array[] = $item[1];
            $studio_banner = StudioBanner::model()->findByPk($item[1]);
            $studio_banner->id_seq = $key + 1;
            $studio_banner->save();
            $this->clearUsersiteCache();
        }
        echo "success";
        exit;
    }

    public function actionDomainname() {
        $this->breadcrumbs = array('Website', 'Domain Name');
        $this->headerinfo = "Domain Name";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Domain Name';
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = Studios::model()->findByPk($studio_id);
        $enable_comingsoon = false;
        $coming_option = 0;
        $logo_path = '';

        $settings = ComingsoonSetting::getComingSoonSetting($studio_id);
        if (isset($settings) && count($enable_comingsoon > 0)) {
            $enable_comingsoon = $settings->status;
            $coming_option = $settings->option_enabled;
            $logo_path = $this->getComingSoonPath($studio_id, $settings->logo_file);
            $background_path = $this->getComingSoonPath($studio_id, $settings->background_file);
        }
        $logo_width = 166;
        $logo_height = 72;
        $bg_width = 1000;
        $bg_height = 800;
        $this->render('domainname', array('studio' => $studio, 'enable_comingsoon' => $enable_comingsoon, 'coming_option' => $coming_option, 'settings' => $settings, 'logo_path' => $logo_path, 'background_path' => $background_path, 'logo_width' => $logo_width, 'logo_height' => $logo_height, 'bg_width' => $bg_width, 'bg_height' => $bg_height));
    }

    public function actionsaveComingSoon() {
        $studio_id = $this->studio->id;
        if (isset($_REQUEST) && count($_REQUEST) > 0) {
            $setting = ComingsoonSetting::getComingSoonSetting($studio_id);
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucket = $bucketInfo['bucket_name'];
            if ($setting) {
                $setting->studio_id = $studio_id;
            } else {
                $setting = new ComingsoonSetting;
                $setting->studio_id = $studio_id;
            }

            $setting->status = $_REQUEST['enable_comingsoon'];
            $setting->option_enabled = $_REQUEST['coming_option'];
            $setting->comingsoon_text = $_REQUEST['soon_message'];
            $setting->passcode = $_REQUEST['passcode'];
            if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                $theme_folder = $this->studio->theme;
                $width = $_REQUEST['img_width'];
                $height = $_REQUEST['img_height'];
                $cropDimension = array('original' => $width . 'x' . $height);
                $return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, 'comingsoon', $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage']);
                if ($return != '') {
                    if (isset($setting) && $setting->id > 0 && $setting->logo_file != '') {
                        $logo_path = $this->getComingSoonPath($studio_id, $setting->logo_file);
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $logo_path
                        ));
                    }
                    $setting->logo_file = basename($return);
                }
            }
            if ((isset($_FILES["bg_Filedata"]["name"]) && $_FILES["bg_Filedata"]["name"] != '') || (isset($_REQUEST['bg_g_image_file_name']) && $_REQUEST['bg_g_image_file_name'] != '')) {
                $theme_folder = $this->studio->theme;
                $width = $_REQUEST['bg_img_width'];
                $height = $_REQUEST['bg_img_height'];
                $cropDimension = array('original' => $width . 'x' . $height);
                $return = Yii::app()->custom->processUploadImage($_FILES['bg_Filedata'], $_REQUEST['bg_fileimage'], $theme_folder, $cropDimension, 'comingsoon', $_REQUEST['bg_g_image_file_name'], $_REQUEST['bg_g_original_image'], $_REQUEST['bg_jcrop_allimage']);
                if ($return != '') {
                    if (isset($setting) && $setting->id > 0 && $setting->background_file != '') {
                        $bg_path = $this->getComingSoonPath($studio_id, $setting->background_file);
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $bg_path
                        ));
                    }
                    $setting->background_file = basename($return);
                }
            }
            $setting->save();
            $msg = 'Settings saved successfully.';
            Yii::app()->user->setFlash('success', $msg);
        } else {
            $msg = 'Error in processing data.';
            Yii::app()->user->setFlash('error', $msg);
        }
        $this->redirect(Yii::app()->getBaseUrl(true) . '/template/domainname');
        exit();
    }

    public function actionSavedomain() {
        $domain = $_REQUEST['domain'];
        $err = 1;
        $msg = 'Please enter a valid domain name';

        if (isset($domain) && trim($domain) != '') {
            $domain = str_replace('http://', '', $domain);
            $domain = str_replace('www.', '', $domain);
            if (!$domain) {
                $err = 1;
                $msg = 'Please enter a valid domain name';
            } else {
                $studio_id = Yii::app()->common->getStudiosId();
                $std = new Studios;
                $studios = $std->findAllByAttributes(array(), 'id <> :id AND domain = :domain', array(':id' => $studio_id, 'domain' => $domain));

                if (count($studios) > 0) {
                    $err = 1;
                    $msg = 'This domain name is not available. Please try a new domain.';
                } else {
                    if(strstr($domain,"muvi.com") !== FALSE){
                        $is_live = 1;
                        $emails = array('ssl@muvi.com');
                        $adminSubject = "Studio going Live";
                        $template_name = 'ssl_email';
                    } else {
                        $is_live = 0;
                        $emails = array('accounts@muvi.com');
                        $adminSubject= "New website is Live";
                        $template_name = 'website_live';
                    }
                    $studio = $this->studio;
                    //if (!$is_live) {
                        $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
                        $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
                        $params = array(
                            'name' => 'All',
                            'studio_name' => $studio->subdomain,
                            'domain_name' => $_REQUEST['domain']
                        );

                        $to = $emails;
                        $from = $fromEmail;
                        $subject = $adminSubject;
                        Yii::app()->theme = 'bootstrap';
                        if($is_live == 0){
                            $thtml = Yii::app()->controller->renderPartial('//email/website_live', array('params' => $params), true);
                        } else{
                            $thtml = Yii::app()->controller->renderPartial('//email/ssl_email', array('params' => $params), true);
                        }

                        $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', $fromName);
                        //  $this->sendMandrilEmail($template_name, $params, $mailAddress);
                    //}
                    $studio->domain = $domain;
                    $studio->is_live = $is_live;
                    $studio->save();
                    $err = 0;
                    $msg = 'Your domain is updated.';
                    Yii::app()->user->setFlash('success', 'Your website domain updated successfully.');
                }
            }
        } else {
            $err = 1;
            $msg = 'Please enter your domain name';
        }

        $arr = array('err' => $err, 'msg' => $msg);
        echo json_encode($arr);
    }

    /* Manage Featured Sections for Home page */

    /* public function actionFeaturedsections() {
      $this->breadcrumbs = array('Website','Templates','Featured Content on Homepage');
      $this->headerinfo = "Featured Content on Homepage";
      $this->pageTitle = Yii::app()->name . ' | ' . 'Featured Content on Homepage';
      $studio_id = Yii::app()->common->getStudioId();
      $theme = Yii::app()->common->getTemplateDetails($this->studio->parent_theme);
      $dbcon = Yii::app()->db;
      $language_id = '';

      if (@IS_LANGUAGE == 1) {
      $language_id = $this->language_id;
      if ($theme->max_featured_sections > 0) {
      $cc_cmd = $dbcon->createCommand("SELECT * FROM featured_section WHERE studio_id={$studio_id} AND template_id={$theme->id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM featured_section WHERE studio_id={$studio_id} AND template_id={$theme->id} AND language_id={$language_id})) LIMIT {$theme->max_featured_sections} ORDER BY id_seq ASC");
      } else {
      $cc_cmd = $dbcon->createCommand("SELECT * FROM featured_section WHERE studio_id={$studio_id} AND template_id={$theme->id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM featured_section WHERE studio_id={$studio_id} AND template_id={$theme->id} AND language_id={$language_id})) ORDER BY id_seq ASC");
      }
      } else {
      if ($theme->max_featured_sections > 0) {
      $cc_cmd = $dbcon->createCommand("SELECT * FROM featured_section WHERE studio_id={$studio_id} AND template_id={$theme->id}  ORDER BY id_seq ASC LIMIT {$theme->max_featured_sections}");
      } else {
      $cc_cmd = $dbcon->createCommand("SELECT * FROM featured_section WHERE studio_id={$studio_id} AND template_id={$theme->id} ORDER BY id_seq ASC");
      }
      }

      $section = $cc_cmd->queryAll();
      $this->render('featuredsections', array('data' => $section, 'language_id' => $language_id));
      } */

    public function actionGetfeaturedsection() {
        $studio_id = Yii::app()->common->getStudioId();
        $section_id = isset($_REQUEST['section']) ? $_REQUEST['section'] : 0;

        $section = new FeaturedSection;
        $section = $section->findByPk($section_id);

        $section_detail = array(
            'title' => html_entity_decode($section->title),
            'id' => $section->id,
        );

        echo json_encode($section_detail);
    }

    public function actionAddHomepageSection() {
        $studio_id = Yii::app()->common->getStudioId();
        $arr['succ'] = 0;
        $arr['msg'] = 'Error while saving the section.';
        if (isset($_POST['section_name']) && $_POST['section_name'] != '') {
            $section_name = $_POST['section_name'];
            $studio = $this->studio;
            $current_template = $studio->parent_theme;
            $content_type = @$_POST['content_type'];

            if ($_POST['section_id'] > 0) {
                $section = new FeaturedSection;
                $section = $section->findByPk($_POST['section_id']);
                $section->title = htmlspecialchars($section_name);
                $section->save();

                $arr['succ'] = 1;
                $arr['msg'] = 'Homepage section is updated successfully.';
            } else {
                $temp = new Template;
                $template = $temp->findByAttributes(array('code' => $current_template));

                $template_id = $template->id;
                $section = new FeaturedSection;
                $section->studio_id = $studio_id;
                $section->content_type = $content_type;
                $section->template_id = $template_id;
                $section->title = htmlspecialchars($section_name);
                $section->is_active = 1;
                $section->save();

                $arr['succ'] = 1;
                $arr['msg'] = 'Homepage section is added successfully.';

                Yii::app()->user->setFlash('success', 'Homepage section added successfully.');
            }

            $this->redirect(Yii::app()->getBaseUrl(true) . '/template/homepage');
            exit();
        }
        echo json_encode($arr);
        exit;
    }

    function actionAutocomplete() {
        $studio_id = Yii::app()->user->studio_id;
        $this->layout = false;
        $arr = array();

        $content_type = @$_REQUEST['content_type'];
        $apphome      = @$_REQUEST['apphome'];
        //$dbcon = Yii::app()->db2;
        $dbcon = Yii::app()->db;
        $sq = $_REQUEST['term'];
        $featured_section = $_REQUEST['featured_section'];
        if($apphome){
            $feat = new AppFeaturedContent;
        }else{
            $feat = new FeaturedContent;
        }
        $feats = $feat->findAllByAttributes(array('studio_id' => $studio_id, 'section_id' => $featured_section));        

        //Searching Physical contents
        $items = array();
        foreach ($feats as $feat) {
            $items[] = array($feat['movie_id'], $feat['is_episode']);
        }
        if (Yii::app()->general->getStoreLink() && $content_type == 1) {
            $is_episode = 2; //For Physical Content
            $sql = "SELECT id, name FROM pg_product WHERE studio_id=" . $studio_id . " AND is_deleted = '0' AND if(is_preorder = 1, 1, ((publish_date IS NULL) OR  (UNIX_TIMESTAMP(publish_date) = 0) OR (publish_date <=NOW()))) AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ORDER BY name ASC";
            $command = $dbcon->createCommand($sql);
            $list = $command->queryAll();
            if ($list) {
                foreach ($list AS $key => $val) {
                    $find = array($val['id'], $is_episode);
                    if (!in_array($find, $items)) {
                        $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => $is_episode);
                    }
                }
            }
        }
		if($content_type == 2){
			//Searching Audio contents
			$sql = "SELECT distinct F.id , F.name, M.id as stream_id, M.episode_title, M.series_number, M.is_episode FROM films F, movie_streams M WHERE F.id=M.movie_id AND F.parent_id=0 AND M.episode_parent_id=0 AND F.studio_id=" . $studio_id . " AND (F.content_types_id=5 OR F.content_types_id=6) AND (LOWER(name) LIKE '%" . strtolower($sq) . "%' OR LOWER(episode_title) LIKE '%" . strtolower($sq) . "%') ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
			$command = $dbcon->createCommand($sql);
			$list = $command->queryAll();
			if ($list) {
				foreach ($list AS $key => $val) {
					if (isset($_REQUEST['movie_name'])) {

					if ($val['is_episode'] == 1)
					$find = array($val['stream_id'], $val['is_episode']);
					else
					$find = array($val['id'], $val['is_episode']);

					if (!in_array($find, $items)) {
					if ($val['is_episode'] == 1) {
					$tlt = ($val['episode_title'] != '') ? $val['episode_title'] : " SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
					$tlt = 'Episode-' . $tlt;
					$arr['movies'][] = array('movie_name' => $val['name'] . ' ' . $tlt, 'movie_id' => $val['stream_id'], 'is_episode' => 1);
					} else
					$arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => 0);
					}
					} else {
					$arr[] = array('category' => 'Movie', 'label' => $val['name']);
					}
				}
			}
		
		}

        if($content_type == 0 || $content_type == 3){
            //Searching Digital contents
            //$sql = "SELECT distinct F.id , F.name, M.id as stream_id, M.episode_title, M.series_number, M.is_episode FROM films F, movie_streams M WHERE F.id=M.movie_id AND F.parent_id=0 AND M.episode_parent_id=0 AND F.studio_id=" . $studio_id . " AND F.content_types_id!=4 AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
            $sql = "SELECT distinct F.id , F.name, M.id as stream_id, M.episode_title, M.series_number, M.is_episode FROM films F, movie_streams M WHERE F.id=M.movie_id AND F.parent_id=0 AND M.episode_parent_id=0 AND F.studio_id=" . $studio_id . " AND F.content_types_id!=4 AND (LOWER(name) LIKE '%" . strtolower($sq) . "%' OR LOWER(episode_title) LIKE '%" . strtolower($sq) . "%') ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
            $command = $dbcon->createCommand($sql);
            $list = $command->queryAll();
            if ($list) {
                foreach ($list AS $key => $val) {
                    if (isset($_REQUEST['movie_name'])) {

                        if ($val['is_episode'] == 1)
                            $find = array($val['stream_id'], $val['is_episode']);
                        else
                            $find = array($val['id'], $val['is_episode']);

                        if (!in_array($find, $items)) {
                            if ($val['is_episode'] == 1) {
                                $tlt = ($val['episode_title'] != '') ? $val['episode_title'] : " SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                                $tlt = 'Episode-' . $tlt;
                                $arr['movies'][] = array('movie_name' => $val['name'] . ' ' . $tlt, 'movie_id' => $val['stream_id'], 'is_episode' => 1);
                            } else
                                $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => 0);
                        }
                    } else {
                        $arr[] = array('category' => 'Movie', 'label' => $val['name']);
                    }
                }
            }

            //Searching Live streaming contents
            $sql = "SELECT distinct F.id , F.name, L.id as stream_id FROM films F, livestream L WHERE F.id=L.movie_id AND F.parent_id=0 AND F.studio_id=" . $studio_id . " AND F.content_types_id = 4 AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ORDER BY char_length(name) ASC, F.id DESC";
            $command = $dbcon->createCommand($sql);
            $list = $command->queryAll();
            if ($list) {
                foreach ($list AS $key => $val) {
                    if (isset($_REQUEST['movie_name'])) {
                        $find = array($val['id'], $val['is_episode']);
                        if (!in_array($find, $items)) {
                            $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => 3);
                        }
                    } else {
                        $arr[] = array('category' => 'Movie', 'label' => $val['name']);
                    }
                }
            }
        }
		//Searching playlist Contents
		$sql2 = "SELECT distinct id, playlist_name FROM user_playlist_name WHERE studio_id=" . $studio_id . " AND user_id=0 AND LOWER(playlist_name) LIKE '%" . strtolower($sq) . "%' ORDER BY id ASC";
		$command2 = $dbcon->createCommand($sql2);
		$playitem = $command2->queryAll(); 
		$is_episode = 4 ; //for Playlist content
		if ($playitem) {
			foreach ($playitem AS $key => $val) {
				$find = array($val['id'],$is_episode); 
				if (!in_array($find, $items)) {
					$arr['movies'][] = array('movie_name' => $val['playlist_name'], 'movie_id' => $val['id'],'is_episode'=>$is_episode);
				}                    
			}
		}                    

        echo json_encode($arr);
        exit;
    }

    public function actionSortfeatured() {
        $apphome = @$_REQUEST['apphome'];
        foreach ($_REQUEST as $key => $value) {
            $sec_parts = explode('_', $key);
            $parts = explode('=', $_REQUEST['order']);

            if (count($sec_parts) > 0 && $sec_parts[1] > 0 && count($parts) > 0) {
                $feat_id = (int) $parts[1];
                if($apphome){
                    $feat = new AppFeaturedContent;
                }else{
                    $feat = new FeaturedContent;
                }
                $feat = $feat->findByPk($feat_id);
                $feat->id_seq = $pos;
                $feat->save();

                $section_id = $sec_parts[1];
                foreach ($_REQUEST['sort_' . $section_id] as $position => $item) {
                    $pos = $position + 1;
                    if($apphome){
                        $feat = new AppFeaturedContent;
                    }else{
                        $feat = new FeaturedContent;
                    }
                    $feat = $feat->findByPk($item);
                    $feat->id_seq = $pos;
                    $feat->save();
                }
            }
        }
        echo "success";
        exit;
    }

    public function actionSortfeaturedsection() {
        $parts   = explode('=', $_REQUEST['order']);
        $feat_id = (int) $parts[1];
        $apphome = @$_REQUEST['apphome'];
        $pos = 0;
        $attr= array('id_seq'=>$pos);
        $condition = "id=:id OR parent_id =:id";
        if($apphome){
            $feats_id = AppFeaturedSections::model()->getAppFeaturesParentId($feat_id);
            $feat     = new AppFeaturedSections;
            $params   = array(':id' => $feats_id);
            $feat     = $feat->updateAll($attr,$condition,$params);
            foreach ($_REQUEST['sort'] as $position => $feat_id) {
                $feats_id  = AppFeaturedSections::model()->getAppFeaturesParentId($feat_id);
                $feat      = new AppFeaturedSections;
                $pos       = $position + 1;
                $attr      = array('id_seq'=>$pos);
                $params    = array(':id'=>$feats_id);
                $feat      = $feat->updateAll($attr,$condition,$params);
            }            
        }else{
            $feat     = new FeaturedSection;
            $feats_id = $this->getParentFeaturedId($feat_id);
            $params   = array(':id'=>$feats_id);
            $feat     = $feat->updateAll($attr,$condition,$params);
            foreach ($_REQUEST['sort'] as $position => $feat_id) {
                $feat = new FeaturedSection;
                $pos = $position + 1;
                $attr= array('id_seq'=>$pos);
                $condition = "id=:id OR parent_id =:id";
                $feats_id = $this->getParentFeaturedId($feat_id);
                $params = array(':id'=>$feats_id);
                $feat = $feat->updateAll($attr,$condition,$params);
            }
        }
        echo "success";
        exit;
    }
    public function getParentFeaturedId($feat_id){
        $feats = new FeaturedSection;
        $feats = $feats->findByPk($feat_id);
        if ($feats->parent_id == 0):
            $feats_id = $feat_id;
        else:
            $feats_id = $feats->parent_id;
        endif;
        return $feats_id;
    }
    public function actionReorderfeaturedsections() {

        $orders = explode('&', $_REQUEST['orders']);
        foreach ($orders as $key => $item) {
            $item = explode('=', $item);
            $section_id = $item[1];

            $section = new FeaturedSection;
            $section = $section->findByPk($section_id);

            $section->id_seq = $key + 1;
            $section->save();
        }
        echo "success";
        exit;
    }

    public function actionRemovefeatured() {
        $apphome     = @$_REQUEST['apphome'];
        $studio_id   = Yii::app()->common->getStudioId();
        $featured_id = $_REQUEST['featured_id'];
        $section_id  = $_REQUEST['section'];
        if ($featured_id > 0) {
            if($apphome){
                $featured = new AppFeaturedContent;
            }else{
                $featured = new FeaturedContent;
            }
            $featured = $featured->findByPk($featured_id);
            $featured->delete();

            $fcontent['status'] = "success";
            $featureds = Yii::app()->common->getFeaturedAllContents($section_id, $apphome);
            $fcontent['data'] = $this->renderPartial('featuredcontent', array('featureds' => $featureds, 'language_id' => $this->language_id,'section_id'=>$section_id),true); 
            $fcontent['message'] = "Featured content is removed";
        } else {
            $fcontent['status'] = 'failure';
            $fcontent['message'] = 'Error in deleting featured content';
        }
        echo json_encode($fcontent);exit;
    }

    public function actionRemovefeaturedsection() {
        $apphome    = @$_REQUEST['apphome'];
        $err        = 0;
        $studio_id  = Yii::app()->common->getStudioId();
        $section_id = $_REQUEST['section'];
        if ($section_id > 0) {
            if($apphome){
                $section  = new AppFeaturedSections;
                $featured = new AppFeaturedContent;
                $websec   = AppFeaturedSections::model()->findByPk($section_id, array('select' => 'web_featured_id'))->web_featured_id;
                if($websec > 0){
                    $fsection   = new FeaturedSection;
                    $attr       = array('sync_with_app' => 0);
                    $fcondition = "id =:id  OR parent_id = :id";
                    $param      = array(":id" => $websec);
                    $fsection   = $fsection->updateAll($attr, $fcondition, $param);
                }
            }else{
                $section  = new FeaturedSection;
                $featured = new FeaturedContent;
            }
            $section->deleteAll('id = :id OR parent_id = :parent_id', array(
                ':id' => $section_id,
                ':parent_id' => $section_id
            ));
            $featureds = $featured->findAllByAttributes(array('studio_id' => $studio_id, 'section_id' => $section_id));
            foreach ($featureds as $feat) {
                if($apphome){
                    $cont = new AppFeaturedContent;
                }else{
                    $cont = new FeaturedContent; 
                }
                $cont = $cont->findByPk($feat->id);
                $cont->delete();
            }

            Yii::app()->user->setFlash('success', 'Featured section is removed.');
        } else {
            $err = 1;
        }

        echo json_encode(array('err' => $err));
    }

    public function actionUpdatesection() {
        $studio_id = Yii::app()->common->getStudioId();
        $section_id = $_REQUEST['section'];
        $section_name = $_REQUEST['sectionname'];
        $apphome      = @$_REQUEST['apphome'];
        $language_id  = $this->language_id;
        $err = 1;
        if ($section_id > 0 && $section_name != '') {
            if($apphome){
                $section = new AppFeaturedSections;
            }else{
                $section = new FeaturedSection;
            }
            if($language_id != 20){
            $section_old = $section->findByAttributes(array('studio_id' => $studio_id, 'parent_id' => $section_id, 'language_id' => $this->language_id));
                //Update section name
                if (!empty($section_old)) {
                    $section_old->title = htmlspecialchars($section_name);
                    $section_old->save();
                } else {//Add new section name
                    $section_parent = $section->findByAttributes(array('studio_id' => $studio_id, 'id' => $section_id, 'parent_id' => 0));

                    $section->studio_id = $studio_id;
                    $section->language_id = $this->language_id;
                    $section->parent_id = $section_parent->id;
                    $section->content_type = $section_parent->content_type;
                    if(!$apphome){
                        $section->template_id = $section_parent->template_id;
                    }
                    $section->title = htmlspecialchars($section_name);
                    $section->id_seq = $section_parent->id_seq;
                    $section->is_active = 1;
                    $section->save();
                }
            } else {
                $section_old = $section->findByAttributes(array('studio_id' => $studio_id, 'id' => $section_id));
                $section_old->title = htmlspecialchars($section_name);
                $section_old->save();
            }

            $err = 0;
            Yii::app()->user->setFlash('success', 'Featured section updated.');
        }
        echo json_encode(array('err' => $err));
    }

    /**
     * @method public addPopularMovie(type $paramName) Description
     * @return json movie name and movie_id
     * @author GDR<support@muvi.com>
     */
   function actionAddPopularMovie() {
        $apphome = @$_REQUEST['apphome'];
        if (isset($_REQUEST['movie_id']) && isset($_REQUEST['featured_section'])) {
            if ($_REQUEST['movie_id']) {
                $studio_id = Yii::app()->common->getStudioId();
                $movie_id = $_REQUEST['movie_id'];
                $is_episode = $_REQUEST['is_episode'];
                $section_id = $_REQUEST['featured_section'];
                $content_type = $_REQUEST['fs_content_type'];
                if($apphome){
                    $featured = new AppFeaturedContent;
                }else{
                    $featured = new FeaturedContent;
                }
                $featured->studio_id  = $studio_id;
                $featured->is_episode = $is_episode;
                $featured->movie_id   = $movie_id;
                $featured->section_id = $section_id;
                $featured->id_seq     = $featured->getMaxOrd($studio_id, $section_id) + 1;
                $featured->save();
                if($content_type == 0){
                    if ($is_episode == 1) {
                        $movie_stream = movieStreams::model()->findByPk($movie_id,array('select'=>'movie_id,episode_title,series_number,episode_number'));
                        $movie_id = @$movie_stream->movie_id;

                        $film = Film::model()->findByPk($movie_id, array('select'=>'name'));
                        $cont_name = $film->name;

                        $cont_name .= ' ' . (@$movie_stream->episode_title != '') ? @$movie_stream->episode_title : "SEASON " . @$movie_stream->series_number . ", EPISODE " . @$movie_stream->episode_number;
                    }elseif ($is_episode == 4){
                        $playlist = UserPlaylistName::model()->findByPk($movie_id,array('select'=>'playlist_name'));
                        $cont_name = $playlist->playlist_name;
                   }else{
                        $film = Film::model()->findByPk($movie_id, array('select'=>'name'));
                        $cont_name = $film->name;
                    }
                }elseif($content_type == 2){
					if ($is_episode == 4) {
						$playlist = UserPlaylistName::model()->findByPk($movie_id, array('select' => 'playlist_name'));
						$cont_name = $playlist->playlist_name;
					}elseif($is_episode == 1){
						$movie_stream = movieStreams::model()->findByPk($movie_id,array('select'=>'movie_id,episode_title,series_number,episode_number'));
						$movie_id = @$movie_stream->movie_id;
						$film = Film::model()->findByPk($movie_id, array('select'=>'name'));
						$cont_name = $film->name;
						$cont_name .= ' ' . (@$movie_stream->episode_title != '') ? @$movie_stream->episode_title : "SEASON " . @$movie_stream->series_number . ", EPISODE " . @$movie_stream->episode_number;
                }else{
						$film = Film::model()->findByPk($movie_id, array('select'=>'name'));
                        $cont_name = $film->name;
					}
				}else{
                    $physical = PGProduct::model()->findByPk($movie_id, array('select'=>'name'));
                    $cont_name = $physical->name;
                }
                $fcontent['status'] = "success";
                $featureds = Yii::app()->common->getFeaturedAllContents($section_id, $apphome);
                $fcontent['data'] = $this->renderPartial('featuredcontent', array('featureds' => $featureds, 'language_id' => $this->language_id,'section_id'=>$section_id),true); 
                $fcontent['message'] = $cont_name. " is added as featured content.";
            } else {
                $fcontent['status'] = "error";
                $fcontent['message'] = 'Oops! Sorry Content don\'t have video or not exists in your studio';exit;
            }
            echo json_encode($fcontent);exit;
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You do not have access to this');
        }
        $url = $this->createUrl('template/homepage');
        $this->redirect($url);
        exit;
    }

    /**
     * @method public removePopularContent($banner_id) It is used to remove a banner image.
     * @return bool 
     * @author GDR<support@muvi.in>
     */
    function actionRemovePopularContent() {
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $movie_id = $_REQUEST['movie_id'];
            $content_type = $_REQUEST['contentType'];
            $studio_id = Yii::app()->common->getStudioId();

            $movie_stream = new movieStreams;
            $movie_stream = $movie_stream->findAllByAttributes(array('studio_id' => $studio_id, 'movie_id' => $movie_id));
            $movie_stream = $movie_stream[0];
            $movie_stream->is_popular = 0;
            $movie_stream->id_seq = 0;
            $movie_stream->save();

            $film = new Film;
            $cond = 'studio_id=' . $studio_id . ' AND id=' . $movie_id;
            $list = $film::model()->findAll(
                    array(
                        'condition' => $cond,
                        'order' => 'id ASC'
                    )
            );
            $cont_name = $list[0]->name;
            //$list_type = $list[0]->studio_content_type;
            //$cont_type = $list_type->display_name;

            Yii::app()->user->setFlash('success', $cont_name . ' is removed as featured ');
        } else {
            Yii::app()->user->setFlash('error', 'You do not haave access to this');
        }
        $url = $this->createUrl('template/popularContent', array('contentType' => $_REQUEST['contentType']));
        $this->redirect($url);
        exit;
    }

    /**
     * @method private clearusersiteCache() It will clear the cache of home page
     * @author GDR<support@muvi.com>
     * @return boolen
     */
    function clearUsersiteCache() {
        $url = Yii::app()->getBaseurl(TRUE) . "/site/FlushCache";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $cdata = curl_exec($ch);
        //echo $url."<pre>";print_r($cdata);exit;
        //$error = curl_error($ch);
        //$info = curl_getinfo($ch);
        curl_close($ch);
        return true;
    }

    //Footer Settings page
    public function actionFooterlinks() {
        $this->breadcrumbs = array('Website', 'Static Pages');
        $this->headerinfo = "Static Pages";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Static Pages';
        $studio_id = Yii::app()->common->getStudioId();
        //check get the native android and Ios apps for the studios from studio config tables
        $sql = "SELECT * FROM studio_config where studio_id=" . $studio_id . " and config_key in ('appstore_link','playstore_link')";

        $app_config = Yii::app()->db->createCommand($sql)->queryAll();

        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $language_id = $this->language_id;
        $sql = "SELECT * FROM pages WHERE status=1 AND studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM pages WHERE status=1 AND studio_id={$studio_id} AND language_id={$language_id})) ORDER BY id_seq ASC";
        $dbcon = Yii::app()->db;
        $pages_cmd = $dbcon->createCommand($sql);
        $pages = $pages_cmd->query();
        $this->render('footerlinks', array('studio' => $studio, 'pages' => $pages, 'language_id' => $language_id, 'app_config' => $app_config));
    }

    function actionReorderfooterlinks() {
        $pos = 0;
        $orders = explode('&', $_REQUEST['orders']);
        foreach ($orders as $key => $item) {
            $item = explode('=', $item);
            $array[] = $item[1];
            $page = Page::model()->findByPk($item[1]);
            $page_id = $page->id;
            if ($page->parent_id > 0) {
                $page_id = $page->parent_id;
            }
            $pos = $key + 1;
            $attr = array('id_seq' => $pos);
            $condition = "id=:id OR parent_id =:id";
            $params = array(':id' => $page_id);
            $feat = Page::model()->updateAll($attr, $condition, $params);
        }
        echo "success";
        exit;
    }

    //Footer Settings page
    public function actionCmspage() {
        $this->breadcrumbs = array('Website', 'Terms & Privacy Policy');
        $this->headerinfo = "Terms & Privacy Policy";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Terms & Privacy Policy';
        $studio_id = Yii::app()->common->getStudioId();

        $std = new Studio();
        $studio = $std->findByPk($studio_id);

        if (isset($_POST) && count($_POST) > 0) {
            $page = new Page;
            if (isset($_POST['page_id']) && $_POST['page_id'] > 0) {
                $page = $page->findByPk($_POST['page_id']);
            } else {
                $permalink = Yii::app()->common->formatPermalink($_POST['page_name']);
                $page->title = htmlspecialchars($_POST['page_name']);
                $page->studio_id = $studio_id;
                $page->permalink = $permalink;
                $page->status = 1;
                $page->created_date = new CDbExpression("NOW()");
                $page->created_by = Yii::app()->user->id;
            }
            $page->content = stripslashes(htmlspecialchars($_POST['page_content']));
            $page->save();

            Yii::app()->user->setFlash('success', 'Page updated successfully.');
            $url = Yii::app()->getBaseUrl(true) . '/template/cmspage';
            $this->redirect($url);
            exit;
        }

        if (@IS_LANGUAGE == 1) {
            $language_id = $this->language_id;
            $pages = Page::model()->find(array(
                'condition' => 'studio_id=:studio_id AND permalink = :permalink AND language_id = :language_id',
                'params' => array(':studio_id' => $studio_id, ':permalink' => 'terms-privacy-policy', ':language_id' => $language_id),
            ));
        } else {
            $pages = Page::model()->find(array(
                'condition' => 'studio_id=:studio_id AND permalink = :permalink',
                'params' => array(':studio_id' => $studio_id, ':permalink' => 'terms-privacy-policy'),
            ));
        }
        $this->render('cmspage', array('studio' => $studio, 'page' => $pages));
    }

    //Footer Settings page
    public function actionGetpagedetails() {
        $studio_id = Yii::app()->common->getStudioId();
        $page_id = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;

        $pages = new Page;
        $page = $pages->findByPk($page_id);

        if (count($page) > 0) {
            $pg = array(
                'title' => html_entity_decode($page->title),
                'content' => html_entity_decode($page->content)
            );
        } else {
            $pg = array(
                'title' => '',
                'content' => ''
            );
        }
        echo json_encode($pg);
    }

    //Saving CMS page contents
    public function actionAddpage() {
        $page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : 0;
        $language_id = isset($_REQUEST['language_id']) ? $_REQUEST['language_id'] : 0;
        $studio_id = Yii::app()->common->getStudioId();
        $page_content = (isset($_REQUEST['page_content']) && $_REQUEST['page_content'] != '') ? trim($_REQUEST['page_content']) : '';
        $external_url = (isset($_REQUEST['external_url']) && $_REQUEST['external_url'] != '') ? trim($_REQUEST['external_url']) : '';
        if (isset($_REQUEST['page_name']) && $_REQUEST['page_name'] != '' && ($page_content != '' || $external_url != '')) {
            $link_type = isset($_REQUEST['ltype']) ? $_REQUEST['ltype'] : 'internal';
            $page_name = $_REQUEST['page_name'];
            $ip_address = CHttpRequest::getUserHostAddress();

            //Remove sample data from website
            $studio = $this->studio;
            $studio->show_sample_data = 0;
            $studio->save();

            if (isset($_REQUEST['permalink']) && trim($_REQUEST['permalink'])) {
                $permalink = trim($_REQUEST['permalink']);
                $pg = new Page();
                $pg = $pg->findByAttributes(array('studio_id' => $studio_id, 'permalink' => $permalink));
                $parent_id = $pg->id;
                $id_seq = $pg->id_seq;
            } else {
                //$permalink = Yii::app()->common->formatPermalink($page_name, $page_id);
                $permalink = Yii::app()->general->generatePermalink($page_name);
                //Adding permalink to url routing 
                $urlRouts['permalink'] = $permalink;
                $urlRouts['mapped_url'] = '/page/show/permalink/' . $permalink;
                $urlRoutObj = new UrlRouting();
                $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
                $parent_id = 0;
            }
            if ($page_id <= 0 || $language_id == 0 || $language_id != $this->language_id) {
                $page = new Page();
                $page->title = Yii::app()->common->encode_to_html($page_name);
                $page->permalink = $permalink;
                $page->link_type = $link_type;
                if ($link_type == 'internal')
                    $page->content = Yii::app()->common->encode_to_html($page_content);
                else
                    $page->external_url = $external_url;

                $page->studio_id = $studio_id;
                if ($page_id != 0) {
                    $page->language_id = $this->language_id;
                    $page->parent_id = $parent_id;
                    $page->id_seq = $id_seq;
                }
                $page->created_date = new CDbExpression("NOW()");
                $page->created_by = Yii::app()->user->id;
                $page->ip = $ip_address;
                $page->status = 1;
                $page->save();
                Yii::app()->user->setFlash('success', 'Page added successfully.');
            } else {
                $page = new Page();
                $page = $page->findByPk($page_id);
                if ($page->studio_id == $studio_id) {
                    $page->title = Yii::app()->common->encode_to_html($page_name);

                    $page->link_type = $link_type;
                    if ($link_type == 'internal')
                        $page->content = Yii::app()->common->encode_to_html($page_content);
                    else
                        $page->external_url = $external_url;
                    $page->last_updated_date = new CDbExpression("NOW()");
                    $page->last_updated_by = Yii::app()->user->id;
                    $page->ip = $ip_address;
                    $page->save();
                }
                Yii::app()->user->setFlash('success', 'Page updated successfully.');
            }
        }

        $url = $this->createUrl('template/footerlinks');
        $this->redirect($url);
        exit;
    }

    public function actionEditpage() {
        $this->breadcrumbs = array('Update Page',);
        $this->pageTitle = Yii::app()->name . ' | ' . 'Update Page';
        $studio_id = Yii::app()->common->getStudioId();
        $page_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $language_id = $this->language_id;
        $permalink = trim($_REQUEST['page']);
        $sql = "SELECT * FROM pages WHERE status=1 AND studio_id={$studio_id} AND permalink='" . $permalink . "' AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM pages WHERE status=1 AND studio_id={$studio_id} AND permalink='" . $permalink . "' AND language_id={$language_id}))";
        $page = Yii::app()->db->createCommand($sql)->queryRow();
        $this->render('editpage', array('page' => $page, 'permalink' => $permalink));
    }

    //Saving CMS page contents
    public function actionDeletepage() {
        $permalink = (isset($_REQUEST['permalink']) && trim($_REQUEST['permalink'])) ? trim($_REQUEST['permalink']) : '';
        $studio_id = Yii::app()->common->getStudioId();
        if (trim($permalink) && $studio_id > 0) {
            $page = Page::model()->findByAttributes(array('studio_id' => $studio_id, 'permalink' => $permalink, 'language_id' => $this->language_id));
            if ($page->parent_id > 0) {
                $ret = $page->delete();
            } else {
                Page::model()->deleteAll('studio_id = :studio_id AND permalink = :permalink', array(
                    ':studio_id' => $studio_id,
                    ':permalink' => $permalink
                ));
            }
            Yii::app()->user->setFlash('success', 'Page deleted successfully.');
        }
        $url = $this->createUrl('template/footerlinks');
        $this->redirect($url);
        exit;
    }

    public function formatPermalink($str, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = $str;
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        return $clean;
    }

    //Saving CMS page contents
    public function actionSavesocial() {
        $studio_id = Yii::app()->common->getStudioId();

        $fb_link = isset($_REQUEST['fb_link']) ? $this->addHttp($_REQUEST['fb_link']) : '';
        $tw_link = isset($_REQUEST['tw_link']) ? $this->addHttp($_REQUEST['tw_link']) : '';
        $gp_link = isset($_REQUEST['gp_link']) ? $this->addHttp($_REQUEST['gp_link']) : '';
        $ig_link = isset($_REQUEST['ig_link']) ? $this->addHttp($_REQUEST['ig_link']) : '';
        $yb_link = isset($_REQUEST['yb_link']) ? $this->addHttp($_REQUEST['yb_link']) : '';
        $copyright = isset($_REQUEST['copyright']) ? $_REQUEST['copyright'] : '';


        //get the appstore & play  store links if given changes By <suraja@muvi.com>
        $appstore_link = isset($_REQUEST['apstr_link']) ? $this->addHttp($_REQUEST['apstr_link']) : '';
        $playstore_link = isset($_REQUEST['gglpl_link']) ? $this->addHttp($_REQUEST['gglpl_link']) : '';

        //check whether the record is present in the config table or not ??

        $storearray = array('appstore_link', 'playstore_link');

        foreach ($storearray as $k => $v) {

            $sql = "SELECT * FROM studio_config where studio_id=" . $studio_id . " and config_key in ('" . $v . "')";

            $app_config = Yii::app()->db->createCommand($sql)->queryAll();

            if (!empty($app_config)) {

                $update_config = "update studio_config set `config_value`= '" . $$v . "' where `studio_id`=" . $studio_id . " and `config_key`='" . $v . "'";
                Yii::app()->db->createCommand($update_config)->execute();
            } else {
                $insert_config = "INSERT INTO studio_config(`studio_id`, `config_key`, `config_value`) VALUES (" . $studio_id . ", '" . $v . "', '" . $$v . "')";
                Yii::app()->db->createCommand($insert_config)->execute();
            }
        }
        //End appstore & play  store links add changes changes By <suraja@muvi.com>
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        $std->fb_link = $fb_link;
        $std->tw_link = $tw_link;
        $std->gp_link = $gp_link;
        $std->ig_link = $ig_link;
        $std->yb_link = $yb_link;
        $std->copyright = htmlspecialchars($copyright);
        $std->save();

        Yii::app()->user->setFlash('success', 'Information updated successfully.');

        $url = $this->createUrl('template/footerlinks');
        $this->redirect($url);
        exit;
    }

    function addHttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            if ($url != "") {
                $url = "https://" . $url;
            } else {
                $url = "";
            }
        }
        return $url;
    }
    public function actionAndroidApp() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'Android App');
        $this->headerinfo = "Android App";
        $studio_id = Yii::app()->common->getStudioId();
        $this->pageTitle = Yii::app()->name . ' | ' . ' Android App';
        $studio = $this->studio;
        $mobileapp = new MobileAppInfo();
        if ($_REQUEST) {
            $app_type = 'android';
            $mobileapp->updateApp($studio_id, $app_type);
            $params = array(
                'name' => 'Admin',
                'studio_name' => $studio->subdomain
            );
            $useremail = 'accounts@muvi.com, sandeep@muvi.com';
            //$useremail = 'studio@muvi.com';
            $username = 'Muvi';
            $subject = 'New Android App Request';
            $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
            $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
            $message = array('subject' => $subject,
                'from_email' => $fromEmail,
                'from_name' => $fromName,
                'to' => array(
                    array(
                        'email' => $useremail,
                        'name' => $username,
                        'type' => 'to'
                    ),
                    array(
                        //'email' => 'help@muvi.com',
                        'email' => 'accounts@muvi.com',
                        'name' => $username,
                        'type' => 'cc'
                    )
                )
            );

            $to = array($useremail);
            $cc = array('satya@muvi.com','subhalaxmi@muvi.com');
            //$cc = array('rasmi@muvi.com');


            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/new_mobile_app_request', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $fromEmail, $cc,'','',$fromName);


            Yii::app()->user->setFlash('success', 'Information updated successfully.');

            $url = $this->createUrl('template/androidApp');
            $this->redirect($url);
            exit;
        } else {
            $appdata = $mobileapp->getAll($studio_id, 'android');
            $this->render('android', array('studio' => $studio, 'appdata' => $appdata));
        }
    }

    //NEW ANDROID TV APP CONTROLLER FUNCTION START
    public function actionAndroidTvApp() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'Android TV App');
        $this->headerinfo = "Android TV App";
        $studio_id = Yii::app()->common->getStudioId();
        $this->pageTitle = Yii::app()->name . ' | ' . ' Android TV App';
        $studio = $this->studio;
        $androidtv = new MobileAppInfo();
        if ($_REQUEST) {
            $app_type = 'androidtv';
            $androidtv->updateApp($studio_id, $app_type);
            $params = array(
                'name' => 'Admin',
                'studio_name' => $studio->subdomain
            );
            //$useremail = 'studio@muvi.com';
//            $useremail = 'accounts@muvi.com, sandeep@muvi.com';
            $useremail = 'subhalaxmi@muvi.com';
            $username = 'Muvi';
            $subject = 'New Android TV App Request';
            $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
            $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
            $message = array('subject' => $subject,
                'from_email' => $fromEmail,
                'from_name' => $fromName,
                'to' => array(
                    array(
                        'email' => $useremail,
                        'name' => $username,
                        'type' => 'to'
                    ),
                    array(
                        //'email' => 'help@muvi.com',
                        'email' => 'accounts@muvi.com',
                        'name' => $username,
                        'type' => 'cc'
                    )
                )
            );

            $to = array($useremail);
            $cc = array();

            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/new_mobile_app_request', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $fromEmail, $cc,'','',$fromName);

            Yii::app()->user->setFlash('success', 'Information updated successfully.');

            $url = $this->createUrl('template/androidTvApp');
            $this->redirect($url);
            exit;
        } else {
            $appdata = $androidtv->getAll($studio_id, 'androidtv');
            $this->render('androidTv', array('studio' => $studio, 'appdata' => $appdata));
        }
    }

    //NEW ANDROID TV APP CONTROLLER FUNCTION END
    
    //NEW FIRE TV APP CONTROLLER FUNCTION START
    public function actionFireTvApp() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'Fire TV App');
        $this->headerinfo = "Fire TV App";
        $studio_id = Yii::app()->common->getStudioId();
        $this->pageTitle = Yii::app()->name . ' | ' . ' Fire TV App';
        $studio = $this->studio;
        $fireapp = new FireTvApp();
        if ($_REQUEST) {
            $app_type = 'firetv';
            $fireapp->updateApp($studio_id, $app_type);
            $params = array(
                'name' => 'Admin',
                'studio_name' => $studio->subdomain
            );
            //$useremail = 'studio@muvi.com';
            $useremail = 'accounts@muvi.com, sandeep@muvi.com';
            $username = 'Muvi';
            $subject = 'New Fire TV App Request';
            $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
            $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
            $message = array('subject' => $subject,
                'from_email' => $fromEmail,
                'from_name' => $fromName,
                'to' => array(
                    array(
                        'email' => $useremail,
                        'name' => $username,
                        'type' => 'to'
                    ),
                    array(
                        //'email' => 'help@muvi.com',
                        'email' => 'accounts@muvi.com',
                        'name' => $username,
                        'type' => 'cc'
                    )
                )
            );

            $to = array($useremail);
            $cc = array('satya@muvi.com','subhalaxmi@muvi.com');

            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/new_mobile_app_request', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $fromEmail, $cc,'','',$fromName);


            Yii::app()->user->setFlash('success', 'Information updated successfully.');

            $url = $this->createUrl('template/fireTvApp');
            $this->redirect($url);
            exit;
        } else {
            $appdata = $fireapp->getAll($studio_id, 'firetv');
            $this->render('fireTv', array('studio' => $studio, 'appdata' => $appdata));
        }
    }
    //NEW FIRE TV APP CONTROLLER FUNCTION END
    //
    //NEW APPLE TV APP CONTROLLER FUNCTION START
    public function actionAppleTvApp() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'Apple TV');
        $this->headerinfo = "Apple TV App";
        $studio_id = Yii::app()->common->getStudioId();
        $studio = $this->studio;
        $appletv = new AppleTvApp();
        $this->pageTitle = Yii::app()->name . ' | ' . ' Apple TV App';
        if ($_REQUEST) {

            $app_type = 'appletv';
            $appletv->updateApp($studio_id, $app_type);
            $params = array(
                'name' => 'Admin',
                'studio_name' => $studio->subdomain
            );
            //$useremail = 'studio@muvi.com';
            $useremail = 'accounts@muvi.com, sandeep@muvi.com';
            $username = 'Muvi';
            $subject = 'New iOS App Request';
            $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
            $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
            $message = array('subject' => $subject,
                'from_email' => $fromEmail,
                'from_name' => $fromName,
                'to' => array(
                    array(
                        'email' => $useremail,
                        'name' => $username,
                        'type' => 'to'
                    ),
                    array(
                        //'email' => 'help@muvi.com',
                        'email' => 'accounts@muvi.com',
                        'name' => $username,
                        'type' => 'cc'
                    )
                )
            );
            $to = array($useremail);
            $cc = array('help@muvi.com','satya@muvi.com','subhalaxmi@muvi.com');

            $template_name = 'new_mobile_app_request';
            //$this->sendMandrilEmail($template_name, $params, $message); 
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/new_mobile_app_request', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $fromEmail, $cc,'','',$fromName);

            Yii::app()->user->setFlash('success', 'Information updated successfully.');

            $url = $this->createUrl('template/appleTvApp');
            $this->redirect($url);
            exit;
        } else {
            $appdata = $appletv->getAll($studio_id, 'appletv');
            $this->render('appleTv', array('studio' => $studio, 'appdata' => $appdata));
        }
    }
    //NEW APPLE TV APP CONTROLLER FNCTION END

    public function actionIOSApp() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'iOS App');
        $this->headerinfo = "iOS App";
        $studio_id = Yii::app()->common->getStudioId();
        $studio = $this->studio;
        $mobileapp = new MobileAppInfo();
        $this->pageTitle = Yii::app()->name . ' | ' . ' iOS App';
        if ($_REQUEST) {

            $app_type = 'ios';
            $mobileapp->updateApp($studio_id, $app_type);
            $params = array(
                'name' => 'Admin',
                'studio_name' => $studio->subdomain
            );
            //$useremail = 'studio@muvi.com';
            $useremail = 'accounts@muvi.com, sandeep@muvi.com';
            $username = 'Muvi';
            $subject = 'New iOS App Request';
            $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
            $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
            $message = array('subject' => $subject,
                'from_email' => $fromEmail,
                'from_name' => $fromName,
                'to' => array(
                    array(
                        //'email' => 'help@muvi.com',
                        'email' => $useremail,
                        'name' => $username,
                        'type' => 'to'
                    ),
                    array(
                        'email' => 'accounts@muvi.com',
                        'name' => $username,
                        'type' => 'cc'
                    )
                )
            );
            $to = array($useremail);
            $cc = array('satya@muvi.com','subhalaxmi@muvi.com');
           // $cc = array('rasmi@muvi.com');

            $template_name = 'new_mobile_app_request';
            //$this->sendMandrilEmail($template_name, $params, $message); 
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/new_mobile_app_request', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $fromEmail, $cc,'','',$fromName);

            Yii::app()->user->setFlash('success', 'Information updated successfully.');

            $url = $this->createUrl('template/iOSApp');
            $this->redirect($url);
            exit;
        } else {
            $appdata = $mobileapp->getAll($studio_id, 'ios');
            $this->render('iOS', array('studio' => $studio, 'appdata' => $appdata));
        }
    }

    public function actionGetAndroidData() {
        $studio_id = Yii::app()->common->getStudioId();
        if ($_REQUEST['is_ajax']) {
            $android_data = new MobileAppInfo();
            $data = $android_data->getAll($studio_id, 'android');
            echo json_encode($data);
        }
    }

    //FIRE TV APP ICON START
    public function actionAppIconFireTv() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $firetv = new FireTvApp();
        $app_type = $_REQUEST['app_type'];
        $image = 'appicon';
        if (!$_FILES['lunchicon']['error']) {
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/appiconfiretv';
            $cropDimension = array('thumb' => '512x512', 'standard' => '512x512');
            $lunch_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon'], $dir, $_REQUEST['jcrop_lunchicon']);
            $path = $this->uploadMobileImages($_FILES['lunchicon'], $studio_id, $app_type, $cropDimension, $lunch_icon_path, $app_type);
        }
        $is_new = $firetv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        if (count($is_new)) {
            $is_success = $firetv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $firetv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/fireTvApp');


        $this->redirect($url);
        exit;
    }
    //FIRE TV APP ICON END

    public function actionAppIcon() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
        $image = 'appicon';
        if (!$_FILES['lunchicon']['error']) {
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            $cropDimension = array('thumb' => '512x512', 'standard' => '512x512');
            $lunch_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon'], $dir, $_REQUEST['jcrop_lunchicon']);
            $path = $this->uploadMobileImages($_FILES['lunchicon'], $studio_id, $app_type, $cropDimension, $lunch_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        if ($app_type == 'android') {
            $url = $this->createUrl('template/androidApp');
        } else {
            $url = $this->createUrl('template/iOSApp');
        }

        $this->redirect($url);
        exit;
    }

    public function actionTransparentAppIcon() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
        $image = 'transparenticon';
        
        $dimension = array();
            $tutorial_screen_dimension = $_REQUEST['jcrop_lunchicon'];
            $dimension['x1'] = $tutorial_screen_dimension['x101'];
            $dimension['y1'] = $tutorial_screen_dimension['y101'];
            $dimension['x2'] = $tutorial_screen_dimension['x201'];
            $dimension['y2'] = $tutorial_screen_dimension['y201'];
            $dimension['w'] = $tutorial_screen_dimension['w5'];
            $dimension['h'] = $tutorial_screen_dimension['h5'];
        
        if (!$_FILES['lunchicon']['error']) {
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $cropDimension = array('thumb' => '512x512', 'standard' => '512x512');
            $lunch_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['lunchicon'], $studio_id, $app_type, $cropDimension, $lunch_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        if ($app_type == 'android') {
            $url = $this->createUrl('template/androidApp');
        } else {
            $url = $this->createUrl('template/iOSApp');
        }

        $this->redirect($url);
        exit;
    }
    
    
    
    //TUTORIAL SCREEN START
    public function actionTutorialScreen() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $screen = new TutorialScreen();
        $studio = $screen->findByPk($studio_id);
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['tutorial_screen']['error']) {
            $dimension = array();
            $tutorial_screen_dimension = $_REQUEST['jcrop_tutorial_screen'];
            $dimension['x1'] = $tutorial_screen_dimension['screen_x1'];
            $dimension['y1'] = $tutorial_screen_dimension['screen_y1'];
            $dimension['x2'] = $tutorial_screen_dimension['screen_x2'];
            $dimension['y2'] = $tutorial_screen_dimension['screen_y2'];
            $dimension['w'] = $tutorial_screen_dimension['screen_w2'];
            $dimension['h'] = $tutorial_screen_dimension['screen_h2'];
            
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/mobileapp/' . $studio_id;
           
        
            $cropDimension = array('thumb' => '1024x1024', 'standard' => '1024x1024');
            $tutorial_screen_path = Yii::app()->common->jcropImage($_FILES['tutorial_screen'], $dir, $dimension);
         
            $path = $this->uploadMobileImages($_FILES['tutorial_screen'], $studio_id, $app_type, $cropDimension, $tutorial_screen_path, $app_type);
        }
       Yii::app()->common->rrmdir($dir);
        
        $screen_data = $screen->getMaxSequence($studio_id, $app_type);
        if($screen_data['max_seq']>0 && isset($screen_data)){
            $sequence = $screen_data['max_seq']+1;
        }else{
            $sequence = 1;
        }
        
        $screen->studio_id = $studio_id;
        $screen->app_type = $app_type;
        $screen->screen_img = $path['original'];
        $screen->sequence = $sequence;
        $screen->created_date = new CDbExpression("NOW()");
        $screen->save();
        
        
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        if($app_type == 'android'){
            $url = $this->createUrl('template/androidApp');
        }else{
            $url = $this->createUrl('template/iOSApp');
        }
        $this->redirect($url);
        exit;
    }

    public function actionScreenSequence() {
        $studio_id = Yii::app()->common->getStudioId();
        $screen = new TutorialScreen();
        $data = $screen->updateSequence($studio_id,$_POST['type'],$_POST['list']);
        
    }

    public function actiondeleteScreen(){
        $studio_id = Yii::app()->common->getStudioId();
        $screen = new TutorialScreen();
        $img_id = $_POST['img_id'];
        $type = $_POST['type'];
        $del_data = $screen->deleteScreen($studio_id,$type,$img_id);
        return $res=1;
        
    }

    //TUTORIAL SCREEN END
    
    //FIIRE TV SPLASH SCREEN START
    public function actionSplashScreenFireTv() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $firetv = new FireTvApp();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon'];
            $dimension['x1'] = $splashicon_dimension['x11'];
            $dimension['y1'] = $splashicon_dimension['y11'];
            $dimension['x2'] = $splashicon_dimension['x21'];
            $dimension['y2'] = $splashicon_dimension['y21'];
            $dimension['w'] = $splashicon_dimension['w1'];
            $dimension['h'] = $splashicon_dimension['h1'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/splashscreenfiretv';
            $cropDimension = array('thumb' => '1920x1080', 'standard' => '1920x1080');
            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['splashicon'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
        }
        $is_new = $firetv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreen';
        if (count($is_new)) {
            $is_success = $firetv->updateImage($studio_id, $app_type, $image, $path['original'],$path_lscape['original']);
        } else {
            $is_success = $firetv->addImage($studio_id, $app_type, $image, $path['original'],$path_lscape['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/fireTvApp');
        $this->redirect($url);
        exit;
    }
    //FIRE TV SPLASH SCREEN END

    //APPLE TV FEATURE GRAPHIC START
    public function actionAppleFeatureGraphic() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new AppleTvApp();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['featuregraphic']['error']) {
            $dimension = array();
            $feature_dimension = $_REQUEST['jcrop_featuregraphic'];
            $dimension['x1'] = $feature_dimension['x5'];
            $dimension['y1'] = $feature_dimension['y5'];
            $dimension['x2'] = $feature_dimension['x25'];
            $dimension['y2'] = $feature_dimension['y25'];
            $dimension['w'] = $feature_dimension['w5'];
            $dimension['h'] = $feature_dimension['h5'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/applefeaturegraphic';
            $cropDimension = array('thumb' => '1920x720', 'standard' => '1920x720');
            $feature_icon_path = Yii::app()->common->jcropImage($_FILES['featuregraphic'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['featuregraphic'], $studio_id, $app_type, $cropDimension, $feature_icon_path, $app_type);
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'featurescreen';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/appleTvApp');
        $this->redirect($url);
        exit;
    }
    //APPLE TV FEATURE GRAPHIC END

    //APPLE TV SPLASH SCREEN START
    public function actionAppleTvSplashScreen() {

        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new AppleTvApp();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon'];
            $dimension['x1'] = $splashicon_dimension['x11'];
            $dimension['y1'] = $splashicon_dimension['y11'];
            $dimension['x2'] = $splashicon_dimension['x21'];
            $dimension['y2'] = $splashicon_dimension['y21'];
            $dimension['w'] = $splashicon_dimension['w1'];
            $dimension['h'] = $splashicon_dimension['h1'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/appletvsplashscreen';
            $cropDimension = array('thumb' => '1242x2208', 'standard' => '1242x2208');
            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['splashicon'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreen';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');


        $url = $this->createUrl('template/appleTvApp');
        $this->redirect($url);
        exit;
    }
    //APPLE TV SPLASH SCREEN END

    //APPLE TV APP ICON-FRONT START
    public function actionAppleTvAppIconFront() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new AppleTvApp();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['lunchicon-1']['error']) {
            $dimension = array();
            $appicon_dimension = $_REQUEST['jcrop_lunchicon_one'];
            $dimension['x1'] = $appicon_dimension['x101'];
            $dimension['y1'] = $appicon_dimension['y101'];
            $dimension['x2'] = $appicon_dimension['x201'];
            $dimension['y2'] = $appicon_dimension['y201'];
            $dimension['w'] = $appicon_dimension['w01'];
            $dimension['h'] = $appicon_dimension['h01'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/mobileapp/' . $studio_id.'/appletvfront';
            
            $cropDimension = array('thumb' => '1280x768', 'standard' => '1280x768');

            $app_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon-1'], $dir, $dimension);
            $path_one = $this->uploadMobileImages($_FILES['lunchicon-1'], $studio_id, $app_type, $cropDimension, $app_icon_path, $app_type);
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'appiconfront';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path_one['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path_one['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/appleTvApp');
        $this->redirect($url);
        exit;
    }
    //APPLE TV APP ICON-FRONT END

    //APPLE TV APP ICON-MIDDLE START
    public function actionAppleTvAppIconMiddle() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new AppleTvApp();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['lunchicon-2']['error']) {
            $dimension = array();
            $appicon_dimension_two = $_REQUEST['jcrop_lunchicon_two'];
            $dimension['x1'] = $appicon_dimension_two['x102'];
            $dimension['y1'] = $appicon_dimension_two['y102'];
            $dimension['x2'] = $appicon_dimension_two['x202'];
            $dimension['y2'] = $appicon_dimension_two['y202'];
            $dimension['w'] = $appicon_dimension_two['w02'];
            $dimension['h'] = $appicon_dimension_two['h02'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/appletvmiddle';
            
            $cropDimension = array('thumb' => '1280x768', 'standard' => '1280x768');
            $app_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon-2'], $dir, $dimension);
            $path_two = $this->uploadMobileImages($_FILES['lunchicon-2'], $studio_id, $app_type, $cropDimension, $app_icon_path, $app_type);
        }

        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'appiconmiddle';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image,$path_two['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image,$path_two['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/appleTvApp');
        $this->redirect($url);
        exit;
    }
    //APPLE TV APP ICON-MIDDLE END

    //APPLE TV APP ICON-BACK START
    public function actionAppleTvAppIconBack() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new AppleTvApp();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['lunchicon-3']['error']) {
            $dimension = array();
            $appicon_dimension = $_REQUEST['jcrop_lunchicon_three'];
            $dimension['x1'] = $appicon_dimension['x103'];
            $dimension['y1'] = $appicon_dimension['y103'];
            $dimension['x2'] = $appicon_dimension['x203'];
            $dimension['y2'] = $appicon_dimension['y203'];
            $dimension['w'] = $appicon_dimension['w03'];
            $dimension['h'] = $appicon_dimension['h03'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/appletvback';
            $cropDimension = array('thumb' => '1280x768', 'standard' => '1280x768');
            $app_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon-3'], $dir, $dimension);
            $path_three = $this->uploadMobileImages($_FILES['lunchicon-3'], $studio_id, $app_type, $cropDimension, $app_icon_path, $app_type);
        }

        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'appiconback';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image,$path_three['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image,$path_three['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/appleTvApp');
        $this->redirect($url);
        exit;
    }
    //APPLE TV APP ICON-BACK END

    //SPLASH SCREEN-PORTRAIT IOS START
    public function actionSplashScreenIosPortrait() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon'];
            $dimension['x1'] = $splashicon_dimension['x11'];
            $dimension['y1'] = $splashicon_dimension['y11'];
            $dimension['x2'] = $splashicon_dimension['x21'];
            $dimension['y2'] = $splashicon_dimension['y21'];
            $dimension['w'] = $splashicon_dimension['w1'];
            $dimension['h'] = $splashicon_dimension['h1'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/iosportrait';
            
            $cropDimension = array('thumb' => '2048x2732', 'standard' => '2048x2732');

            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['splashicon'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreenportrait';
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/iOSApp');
        $this->redirect($url);
        exit;
    }
    //SPLASH SCREEN-PORTRAIT IOS END

    //SPLASH SCREEN-LANDSCPAE IOS START
    public function actionSplashScreenIosLandscape() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon2']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon_lscape'];
            $dimension['x1'] = $splashicon_dimension['x112'];
            $dimension['y1'] = $splashicon_dimension['y112'];
            $dimension['x2'] = $splashicon_dimension['x212'];
            $dimension['y2'] = $splashicon_dimension['y212'];
            $dimension['w'] = $splashicon_dimension['w12'];
            $dimension['h'] = $splashicon_dimension['h12'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/ioslandscape';
            
            $cropDimension = array('thumb' => '2732x2048', 'standard' => '2732x2048');

            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['splashicon2'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreenlandscape';
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/iOSApp');
        $this->redirect($url);
        exit;
    }
    //SPLASH SCREEN-LANDSCAPE IOS END

    //SPLASH SCREEN-PORTARAIT ANDROID START
    public function actionSplashScreenAndroidPortrait() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon'];
            $dimension['x1'] = $splashicon_dimension['x11'];
            $dimension['y1'] = $splashicon_dimension['y11'];
            $dimension['x2'] = $splashicon_dimension['x21'];
            $dimension['y2'] = $splashicon_dimension['y21'];
            $dimension['w'] = $splashicon_dimension['w1'];
            $dimension['h'] = $splashicon_dimension['h1'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidportrait';
            
            $cropDimension = array('thumb' => '639x1136', 'standard' => '639x1136');

            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['splashicon'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreenportrait';
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/androidApp');
        $this->redirect($url);
        exit;
    }
    //SPLASH SCREEN-PORTARAIT ANDROID END

    
    //SPLASH SCREEN-LANDSCAPE ANDROID START
    public function actionSplashScreenAndroidLandscape() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon2']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon_lscape'];
            $dimension['x1'] = $splashicon_dimension['x112'];
            $dimension['y1'] = $splashicon_dimension['y112'];
            $dimension['x2'] = $splashicon_dimension['x212'];
            $dimension['y2'] = $splashicon_dimension['y212'];
            $dimension['w'] = $splashicon_dimension['w12'];
            $dimension['h'] = $splashicon_dimension['h12'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidlandscape';
            $cropDimension_lscape = array('thumb' => '1136x639', 'standard' => '1136x639');

            $splash_icon_path_lscape = Yii::app()->common->jcropImage($_FILES['splashicon2'], $dir, $dimension);
            $path_lscape = $this->uploadMobileImages($_FILES['splashicon2'], $studio_id, $app_type, $cropDimension_lscape, $splash_icon_path_lscape, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreenlandscape';
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path_lscape['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path_lscape['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/androidApp');
        $this->redirect($url);
        exit;
    }
    //SPLASH SCREEN-LANDSCAPE ANDROID END


    public function actionSplashScreen() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];

        if (!$_FILES['splashicon']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon'];
            $dimension['x1'] = $splashicon_dimension['x11'];
            $dimension['y1'] = $splashicon_dimension['y11'];
            $dimension['x2'] = $splashicon_dimension['x21'];
            $dimension['y2'] = $splashicon_dimension['y21'];
            $dimension['w'] = $splashicon_dimension['w1'];
            $dimension['h'] = $splashicon_dimension['h1'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            $cropDimension = array('thumb' => '1242x2208', 'standard' => '1242x2208');
            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['splashicon'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreen';
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        switch ($app_type){
            case 'android':
                $url = $this->createUrl('template/androidApp');
                break;
            case 'ios':
                $url = $this->createUrl('template/iOSApp');
                break;
        }
        
        $this->redirect($url);
        exit;
    }

    public function actionFeatureGraphic() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
        if (!$_FILES['feature_graphic']['error']) {
            $dimension = array();
            $jcrop_feature_graphic = $_REQUEST['jcrop_feature_graphic'];
            $dimension['x1'] = $jcrop_feature_graphic['x12'];
            $dimension['y1'] = $jcrop_feature_graphic['y12'];
            $dimension['x2'] = $jcrop_feature_graphic['x22'];
            $dimension['y2'] = $jcrop_feature_graphic['y22'];
            $dimension['w'] = $jcrop_feature_graphic['w2'];
            $dimension['h'] = $jcrop_feature_graphic['h2'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            $cropDimension = array('thumb' => '1024x500', 'standard' => '1024x500');
            $fg_icon_path = Yii::app()->common->jcropImage($_FILES['feature_graphic'], $dir, $dimension);
            $path = $this->uploadMobileImages($_FILES['feature_graphic'], $studio_id, $app_type, $cropDimension, $fg_icon_path, $app_type);
        }
        $is_new = $mobileapp->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'featuregraphic';
        if (count($is_new)) {
            $is_success = $mobileapp->updateImage($studio_id, $app_type, $image, $path['original']);
            $this->sendAppEmail($studio,$app_type);
        } else {
            $is_success = $mobileapp->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/androidApp');
        $this->redirect($url);
        exit;
    }

    //ANDROID TV APP UPDATE START
    public function actionUpdateAndroidTvApp() {

        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
//        $androidtv = new AndroidTvApp();
        $androidtv = new MobileAppInfo();
        $agent = Yii::app()->common->getAgent();

        $params = array();
        parse_str($_REQUEST['data_from'], $params);
        $app_type = $params['app_type'];
        $is_present = array();
        $is_present = $androidtv->checkImage($studio_id, $app_type);
        $data = array();
        $data['studio_id'] = $studio_id;
        $data['app_type'] = $params['app_type'];
        $data['app_name'] = $params['app_name'];
        $data['short_description'] = $params['short_description'];
        $data['description'] = $params['description'];
        $data['language'] = $params['language'];
        $data['website'] = $params['website'];
        $data['email'] = $params['email'];
        $data['phone'] = $params['phone'];
        $data['category'] = $params['category'];

        if (isset($params['developer_check']) && $params['developer_check'] == 1) {
            $data['developer_username'] = $params['developer_username'];
            $data['developer_password'] = $params['developer_password'];
        } else {
            $data['developer_username'] = '';
            $data['developer_password'] = '';
        }

        $string = '';
        for ($i = 0; $i < count($params['distribution_geography']); $i++) {
            $string .= $params['distribution_geography'][$i] . ',';
        }
        $data['distribution_geography'] = rtrim($string, ',');
        if ($params['android_check']) {
            $data['android_app_info'] = 1;
        }
        
        //$data['rating'] = $params['rating'];
        $data['rating'] = '';
        $rating = '';
        foreach ($params['rating'] AS $key => $value) {
            $rating .= $value . '-';
        }
        $data['rating'] = rtrim($rating, '-');
//        $data['device_model'] = $params['device_model'];
//        $data['device_number'] = $params['device_number'];
        $string = '';
        if (count($is_present)) {
            $androidtv->updateInfo($studio_id, $app_type, $data);
            
        } else {

            $androidtv->addInfo($data);

            /** code for creation of ticket By Suraja* */

            if (trim($params['device_model']) == '' || trim($params['device_number']) == '') {
                Yii::app()->user->setFlash('error', 'Device model and device number can not be empty');
                echo 0;
                exit;
            }
            $studio = $this->studio;
            $studio_id = $studio->id;
            $name = $studio->name;
            $creater_id = Yii::app()->user->id;
            $ticket_title = "Request for an Muvitest ticket Android TV App";
            $ticket_description = '';
            $ticket_description.= $name . " is Muvitest ticket requesting of an Android TV App. The details of the device are below.<br>";
            $ticket_description.= "Device Model:" . $params['device_model'] . "<br>";
            $ticket_description.= "Device Number:" . $params['device_number'] . "<br>";

            $status = 'New';
            $priority = 'medium';
            $type = 'New Feature';
            $creation_date = date("Y-m-d H:i:s");
            $last_updated = date("Y-m-d H:i:s");
            $insertticket = "insert into `ticket`(`description`, `status`, `priority`, `type`, `studio_id`, `creater_id`,  `creation_date`, `last_updated_date`, `title`) values ('" . $ticket_description . "','" . $status . "','" . $priority . "','" . $type . "',".$studio_id.",".$creater_id.",'". $creation_date . "','" . $last_updated . "','" . $ticket_title ."')";
            Yii::app()->db->createCommand($insertticket)->execute();
            $insert_id = Yii::app()->db->getLastInsertID();


            $subject = "Ticket" . $insert_id . "_" . $ticket_title . "_New Ticket Added";
            $url = "http://" . DOMAIN_COOKIE;
            $thtmltoCustomer = "<p>Hi " . ucfirst($studio->name) . ",<br /><br />
                                    Your support ticket is " . $rid . " added.<br/> ";
            $thtmltoCustomer.= " Ticket details:- <br /><br/><b>Priority</b>:" . $priority. '</p>';
            $thtmltoCustomer.= "<b>Description:</b>" . nl2br(stripslashes(htmlentities($ticket_description))) . "<br /><br/>";
            $thtmltoCustomer.="You will hear from Muvi Support team soon. Please find updates to your ticket <a target='_blank' href='" . $url . "'>here</a>.
                                    Add an update to the ticket by logging into <a target='_blank' href='" . $url . "'>Muvi</a>, or simply replying to this email.<br /><br />
                                </p>" . "<p><br/>Regards,<br /> Muvi</p>";
            //echo $thtmltoCustomer; exit;
            $htmltoAdmin = "<p>Hi Admin,<br /><br />
                               A new ticket is added by $studio->name .<br />";
            $htmltoAdmin.= " Ticket details:- <br /><br/><b>Priority</b>:" .$priority . "</p>";
            $htmltoAdmin.= "<b>Description:</b>" . nl2br(stripslashes(htmlentities($ticket_description))) . "<p><br/>Regards,<br /> Muvi</p>";
            //echo $htmltoAdmin;echo $thtmltoCustomer;exit;
            // $this->sendmailViaMandrill($htmltoAdmin, $subject, array(array("email" => 'suraja@muvi.com')));
            $to_admin = 'support@muvi.com';
            $from = 'support@muvi.com';
            $test_email_subject = "Muvitest ticket";

            if (strpos($ticket_description, $test_email_subject) !== false) {
                $from = 'testmuvi@gmail.com';
                $to_admin = 'testmuvi@gmail.com';
            }
            $ret_val1 = $this->sendAttchmentMailViaAmazonsdk($to_admin, $subject, $from, $htmltoAdmin);
            $to = Yii::app()->user->email;

            $admin_email = array();
            $all_admin_email = User::model()->findAll(array('condition' => 'studio_id =' . $studio_id . ' AND role_id in(1, 2) AND is_active=1'));

            foreach ($all_admin_email as $admin_key => $admin_val) {
                $admin_email[] = $admin_val['email'];
            }
            $to = $admin_email;

            $ret_val2 = $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtmltoCustomer);
            /** END code for creation of ticket By Suraja* */
            
            
        }
        echo 1;
    }
    //ANDROID TV APP UPDATE END

    //ANDROID TV APP ICON START
    public function actionAndroidTvAppIconFront() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $androidtv = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];



        if (!$_FILES['lunchicon-1']['error']) {
            $dimension = array();
            $appicon_dimension = $_REQUEST['jcrop_lunchicon_one'];
            $dimension['x1'] = $appicon_dimension['x1'];
            $dimension['y1'] = $appicon_dimension['y1'];
            $dimension['x2'] = $appicon_dimension['x2'];
            $dimension['y2'] = $appicon_dimension['y2'];
            $dimension['w'] = $appicon_dimension['w'];
            $dimension['h'] = $appicon_dimension['h'];
      
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;

            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/mobileapp/' . $studio_id . '/androidtvicon';

            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $cropDimension = array('thumb' => '512x512', 'standard' => '512x512');

            $app_icon_path = Yii::app()->common->jcropImage($_FILES['lunchicon-1'], $dir, $dimension);

            $path_one = $this->uploadMobileImages($_FILES['lunchicon-1'], $studio_id, $app_type, $cropDimension, $app_icon_path, $app_type);
        }
        $is_new = $androidtv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'appicon';
        if (count($is_new)) {
            $is_success = $androidtv->updateImage($studio_id, $app_type, $image, $path_one['original']);
        } else {
            $is_success = $androidtv->addImage($studio_id, $app_type, $image, $path_one['original']);
        }

        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('template/androidTvApp');
        $this->redirect($url);
        exit;
    }

    //ANDROID TV APP ICON-FRONT END
    
    //ANDROID TV SPALSH SCREEN START
    //     public function actionandroidTvSplashScreen() {
    //     $studio_id = Yii::app()->common->getStudioId();
    //     $agent = Yii::app()->common->getAgent();
    //     $std = new Studio();
    //     $studio = $std->findByPk($studio_id);
    //     $appletv = new MobileAppInfo();
    //     $app_type = $_REQUEST['app_type'];
       
    //     if (!$_FILES['splashicon']['error']) {
    //         $dimension = array();
    //         $splashicon_dimension = $_REQUEST['jcrop_splashicon'];
    //         $dimension['x1'] = $splashicon_dimension['x11'];
    //         $dimension['y1'] = $splashicon_dimension['y11'];
    //         $dimension['x2'] = $splashicon_dimension['x21'];
    //         $dimension['y2'] = $splashicon_dimension['y21'];
    //         $dimension['w'] = $splashicon_dimension['w1'];
    //         $dimension['h'] = $splashicon_dimension['h1'];
    //         $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
    //         if (!file_exists($dir)) {
    //             mkdir($dir, 0777);
    //         }
    //         $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidtvsplashscreen';
    //         $cropDimension = array('thumb' => '1242x2208', 'standard' => '1242x2208');
    //         $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon'], $dir, $dimension);
            
    //         $path = $this->uploadMobileImages($_FILES['splashicon'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
            
    //     }
    //     $is_new = $appletv->checkImage($studio_id, $app_type);
    //     Yii::app()->common->rrmdir($dir);
    //     $image = 'splashscreenportrait';
    //     if (count($is_new)) {
    //         $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
    //     } else {
    //         $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
    //     }
    //     Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        
        
    //     $url = $this->createUrl('template/androidTvApp');
    //     $this->redirect($url);
    //     exit;
    // }

    //ANDROID TV SPALSH SCREEN END

    //ANDROID TV LANDSCAPE SPALSH SCREEN START
        public function actionandroidTvLandscapeSplashScreen() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
       
        if (!$_FILES['splashicon2']['error']) {
            $dimension = array();
            $splashicon_dimension = $_REQUEST['jcrop_splashicon_lscape'];
            $dimension['x1'] = $splashicon_dimension['x112'];
            $dimension['y1'] = $splashicon_dimension['y112'];
            $dimension['x2'] = $splashicon_dimension['x212'];
            $dimension['y2'] = $splashicon_dimension['y212'];
            $dimension['w'] = $splashicon_dimension['w12'];
            $dimension['h'] = $splashicon_dimension['h12'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidtvlandscapesplashscreen';
            $cropDimension = array('thumb' => '1920x1080', 'standard' => '1920x1080');
            $splash_icon_path = Yii::app()->common->jcropImage($_FILES['splashicon2'], $dir, $dimension);
            
            $path = $this->uploadMobileImages($_FILES['splashicon2'], $studio_id, $app_type, $cropDimension, $splash_icon_path, $app_type);
            
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'splashscreenlandscape';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        
        
        $url = $this->createUrl('template/androidTvApp');
        $this->redirect($url);
        exit;
    }

    //ANDROID TV LANDSCAPE SPALSH SCREEN END
    
    //ANDROID TV BANNER SCREEN START
        public function actionandroidTvBannerScreen() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
//        print_r($_REQUEST);
//        exit;
        if (!$_FILES['banner_screen']['error']) {
            $dimension = array();
            $banner_screen_dimension = $_REQUEST['jcrop_banner_screen'];
            $dimension['x1'] = $banner_screen_dimension['screen_x1'];
            $dimension['y1'] = $banner_screen_dimension['screen_y1'];
            $dimension['x2'] = $banner_screen_dimension['screen_x2'];
            $dimension['y2'] = $banner_screen_dimension['screen_y2'];
            $dimension['w'] = $banner_screen_dimension['screen_w2'];
            $dimension['h'] = $banner_screen_dimension['screen_h2'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidbannerscreen';
            $cropDimension = array('thumb' => '1280x720', 'standard' => '1280x720');
            $banner_screen_path = Yii::app()->common->jcropImage($_FILES['banner_screen'], $dir, $dimension);
            
            $path = $this->uploadMobileImages($_FILES['banner_screen'], $studio_id, $app_type, $cropDimension, $banner_screen_path, $app_type);
            
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'bannerscreen';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        
        
        $url = $this->createUrl('template/androidTvApp');
        $this->redirect($url);
        exit;
    }

    //ANDROID TV BANNER SCREEN END
    
    //ANDROID TV Transparent SCREEN START
        public function actionandroidTvTransparentScreen() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
        if (!$_FILES['transparenticon']['error']) {
            $dimension = array();
            $transparenticon_dimension = $_REQUEST['jcrop_transparenticon'];
            $dimension['x1'] = $transparenticon_dimension['x101'];
            $dimension['y1'] = $transparenticon_dimension['y101'];
            $dimension['x2'] = $transparenticon_dimension['x201'];
            $dimension['y2'] = $transparenticon_dimension['y201'];
            $dimension['w'] = $transparenticon_dimension['w5'];
            $dimension['h'] = $transparenticon_dimension['h5'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidtvtransparentscreen';
            $cropDimension = array('thumb' => '512x512', 'standard' => '512x512');
            $transparent_icon_path = Yii::app()->common->jcropImage($_FILES['transparenticon'], $dir, $dimension);
            
            $path = $this->uploadMobileImages($_FILES['transparenticon'], $studio_id, $app_type, $cropDimension, $transparent_icon_path, $app_type);
            
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'transparenticon';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        
        
        $url = $this->createUrl('template/androidTvApp');
        $this->redirect($url);
        exit;
    }

    //ANDROID TV TRA SCREEN END
    
    //ANDROID TV FeatureGraphic SCREEN START
        public function actionandroidTvFeatureGraphic() {
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new MobileAppInfo();
        $app_type = $_REQUEST['app_type'];
       
        if (!$_FILES['featuregraphic']['error']) {
            $dimension = array();
            $featuregraphic_dimension = $_REQUEST['jcrop_featuregraphic'];
            $dimension['x1'] = $featuregraphic_dimension['x12'];
            $dimension['y1'] = $featuregraphic_dimension['y12'];
            $dimension['x2'] = $featuregraphic_dimension['x22'];
            $dimension['y2'] = $featuregraphic_dimension['y22'];
            $dimension['w'] = $featuregraphic_dimension['w2'];
            $dimension['h'] = $featuregraphic_dimension['h2'];
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            
            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id.'/androidtvfeaturegraphic';
            $cropDimension = array('thumb' => '1024x500', 'standard' => '1024x500');
            $featuregraphic_icon_path = Yii::app()->common->jcropImage($_FILES['featuregraphic'], $dir, $dimension);
            
            $path = $this->uploadMobileImages($_FILES['featuregraphic'], $studio_id, $app_type, $cropDimension, $featuregraphic_icon_path, $app_type);
//            print_r($path);exit;    
        }
        $is_new = $appletv->checkImage($studio_id, $app_type);
        Yii::app()->common->rrmdir($dir);
        $image = 'featuregraphic';
        if (count($is_new)) {
            $is_success = $appletv->updateImage($studio_id, $app_type, $image, $path['original']);
        } else {
            $is_success = $appletv->addImage($studio_id, $app_type, $image, $path['original']);
        }
        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');
        
        
        $url = $this->createUrl('template/androidTvApp');
        $this->redirect($url);
        exit;
    }

    //ANDROID TV TRA SCREEN END
    
    //FIRE TV APP INFO UPDATE START
    public function actionUpdateFireTvApp() {

        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $firetv = new FireTvApp();
        $agent = Yii::app()->common->getAgent();

        $params = array();
        parse_str($_REQUEST['data_from'], $params);
        $app_type = $params['app_type'];
        $is_present = array();
        $is_present = $firetv->checkImage($studio_id, $app_type);
        $data = array();
        $data['studio_id'] = $studio_id;
        $data['app_type'] = $params['app_type'];
        $data['app_name'] = $params['app_name'];
        $data['company_name'] = $params['company_name'];
        $data['short_description'] = $params['short_description'];
        $data['description'] = $params['description'];
        $data['language'] = $params['language'];
        $data['support_contact'] = json_encode($params['admin_contact']);

        $data['privacy_policy_url'] = $params['privacy_policy_url'];
        $data['third_party_content'] = $params['third_party_content'];
        $data['keywords'] = $params['keywords'];
        $data['product_feature_bullets'] = $params['pf_bullet'];
        if($params['app_charge']==1){
            $data['app_charge'] = $params['app_charge'];
            $data['app_charge_amount'] = $params['price'];
            $data['app_charge_currency'] = $params['currency'];
        }

        if (isset($params['developer_check']) && $params['developer_check'] == 1) {
            $data['developer_username'] = $params['developer_username'];
            $data['developer_password'] = $params['developer_password'];
        } else {
            $data['developer_username'] = '';
            $data['developer_password'] = '';
        }

        $string = '';
        for ($i = 0; $i < count($params['distribution_geography']); $i++) {
            $string .= $params['distribution_geography'][$i] . ',';
        }
        $data['distribution_geography'] = rtrim($string, ',');
        if ($params['developer_check']) {
            $data['firetv_app_info'] = 1;
        }


        if (count($is_present)) {
            $firetv->updateInfo($studio_id, $app_type, $data);
        } else {

            $firetv->addInfo($data);
        }
        echo 1;
    }
    //FIRE TV APP INFO UPDATE END

    //APPLE TV APP INFO UPDATE START
    public function actionUpdateAppInfoAppleTv() {

        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $appletv = new AppleTvApp();
        $agent = Yii::app()->common->getAgent();

        $params = array();
        parse_str($_REQUEST['data_from'], $params);
        $app_type = $params['app_type'];
        $is_present = array();
        $is_present = $appletv->checkImage($studio_id, $app_type);
        $data = array();
        $data['studio_id'] = $studio_id;
        $data['app_type'] = $params['app_type'];
        $data['app_name'] = $params['app_name'];

        $data['description'] = $params['description'];
        $data['language'] = $params['language'];
        $data['website'] = $params['website'];
        $data['email'] = $params['email'];
        $data['phone'] = $params['phone'];
        $data['category'] = $params['category'];


        $data['keywords'] = $params['keywords'];
        $data['copyright'] = $params['copyright'];
        if (isset($params['developer_check']) && $params['developer_check'] == 1) {
            $data['developer_username'] = $params['developer_username'];
            $data['developer_password'] = $params['developer_password'];
        } else {
            $data['developer_username'] = '';
            $data['developer_password'] = '';
        }

        $string = '';
        for ($i = 0; $i < count($params['distribution_geography']); $i++) {
            $string .= $params['distribution_geography'][$i] . ',';
        }
        $data['distribution_geography'] = rtrim($string, ',');
        if ($params['android_check']) {
            $data['android_app_info'] = 1;
        }

        //$data['rating'] = $params['rating'];
        $data['rating'] = '';
        $rating = '';
        foreach ($params['rating'] AS $key => $value) {
            $rating .= $value . '-';
        }
        $data['rating'] = rtrim($rating, '-');


        if (count($is_present)) {
            $appletv->updateInfo($studio_id, $app_type, $data);
        } else {

            $appletv->addInfo($data);
        }
        echo 1;
    }
    //APPLE TV APP INFO UPDATE END

    public function actionUpdateAppInfo() {
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $mobileapp = new MobileAppInfo();
        $agent = Yii::app()->common->getAgent();

        $params = array();
        parse_str($_REQUEST['data_from'], $params);
        $app_type = $params['app_type'];
        $is_present = array();
        $is_present = $mobileapp->checkImage($studio_id, $app_type);
        $data = array();
        $data['studio_id'] = $studio_id;
        $data['is_update'] = $_REQUEST['is_image'];
        $data['app_type'] = $params['app_type'];
        $data['app_name'] = $params['app_name'];
        $data['company_name'] = $params['company_name'];
        $data['short_description'] = $params['short_description'];
        $data['description'] = $params['description'];
        $data['language'] = $params['language'];
        $data['website'] = $params['website'];
        $data['email'] = $params['email'];
        $data['phone'] = $params['phone'];
        $data['category'] = $params['category'];

        $data['privacy_policy_url'] = $params['privacy_policy_url'];
        $data['third_party_content'] = $params['third_party_content'];
        $data['keywords'] = $params['keywords'];
        $data['copyright'] = $params['copyright'];
        if (isset($params['developer_check']) && $params['developer_check'] == 1) {
            $data['developer_username'] = $params['developer_username'];
            $data['developer_password'] = $params['developer_password'];
        } else {
            $data['developer_username'] = '';
            $data['developer_password'] = '';
        }

        if(isset($params['distribution_geography']) && !empty($params['distribution_geography'])){
        $string = '';
        for ($i = 0; $i < count($params['distribution_geography']); $i++) {
            $string .= $params['distribution_geography'][$i] . ',';
        }
        $data['distribution_geography'] = rtrim($string, ',');
        }
        
        if ($params['android_check']) {
            $data['android_app_info'] = 1;
        }
        if(isset($params['rating']) && !empty($params['rating'])){
        if ($params['app_type'] == 'ios') {
            $data['rating'] = '';
            $rating = '';
            foreach ($params['rating'] AS $key => $value) {
                $rating .= $value . '-';
            }
            $data['rating'] = rtrim($rating, '-');
        } else {
            //$data['rating'] = $params['rating'];
            $data['rating'] = '';
            $rating = '';
            foreach ($params['rating'] AS $key => $value) {
                $rating .= $value . '-';
            }
            $data['rating'] = rtrim($rating, '-');
        }
        }


        if (count($is_present)) {
            $mobileapp->updateInfo($studio_id, $app_type, $data);
        } else {
            $mobileapp->addInfo($data);
        }
        //print_r($data);
        echo 1;
    }

    public function actionRokuApp() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'Roku App');
        $this->headerinfo = "Roku App";
        $this->pageTitle = Yii::app()->name . ' | ' . ' Roku App';
        $studio = $this->studio;
        $agent = $studio->subdomain;
        if (isset($_REQUEST) && $_REQUEST) {
            if ($_REQUEST['app_id']) {
                $tv_app_info = TvAppInfo::model()->findByPk($_REQUEST['app_id']);
                $is_mail_sent = 0;
                $tv_app_info->last_updated_date = date('Y-m-d H:i:s');
            } else {
                $tv_app_info = new TvAppInfo();
                $tv_app_info->is_mail_sent = 1;
                $is_mail_sent = 1;
                $tv_app_info->created_date = date('Y-m-d H:i:s');
                $tv_app_info->last_updated_date = date('Y-m-d H:i:s');
            }
            $tv_app_info->studio_id = $studio->id;
            $tv_app_info->app_type = $_REQUEST['app_type'];
            $tv_app_info->channel_store = json_encode($_REQUEST['channel_store']);
            $tv_app_info->language = json_encode($_REQUEST['lanuages']);
            $tv_app_info->classification = $_REQUEST['classifications'];

            $tv_app_info->additional_requirments = $_REQUEST['additional_requirements'];
            $tv_app_info->parental_hint = $_REQUEST['parental_hint'];

            $tv_app_info->channel_name = $_REQUEST['channel_name'];
            $tv_app_info->channel_subtitle = $_REQUEST['channel_subtitle'];
            $tv_app_info->description = $_REQUEST['description'];
            $tv_app_info->category = $_REQUEST['category'];
            $tv_app_info->keywords = $_REQUEST['keywords'];
            $tv_app_info->web_description = $_REQUEST['web_description'];
            $tv_app_info->feature_info_url = $_REQUEST['feature_info_url'];
            $tv_app_info->support_contact = $_REQUEST['support_contact'];

            $tv_app_info->admin_contact = json_encode($_REQUEST['admin_contact']);
            $tv_app_info->tech_contact = json_encode($_REQUEST['tech_contact']);
            $tv_app_info->monetize_plan = json_encode($_REQUEST['monetize_plan']);
            $tv_app_info->roku_account = json_encode($_REQUEST['roku_account']);
            $tv_app_info->splash_screen = $_REQUEST['splash_poster'];
            $tv_app_info->fhd_poster = $_REQUEST['fhd_poster'];
            $tv_app_info->hd_poster = $_REQUEST['hd_poster'];
            $tv_app_info->sd_poster = $_REQUEST['sd_poster'];
            $tv_app_info->save();
            $ip_address = CHttpRequest::getUserHostAddress();
            if ($is_mail_sent) {
                $params = array(
                    'name' => 'Admin',
                    'studio_name' => $studio->subdomain
                );
                //$useremail = 'studio@muvi.com';
                $useremail = 'accounts@muvi.com, sandeep@muvi.com';
                $username = 'Muvi';
                $subject = 'New Roku App Request';
                $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
                $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
                $message = array(
                    'subject' => $subject,
                    'from_email' => $fromEmail,
                    'from_name' => $fromName,
                    'to' => array(
                        array(
                            'email' => $useremail,
                            'name' => $username,
                            'type' => 'to'
                        ),
                        array(
                            //'email' => 'help@muvi.com',
                            'email' => 'accounts@muvi.com',
                            'name' => $username,
                            'type' => 'cc'
                        )
                    )
                );
                $template_name = 'new_mobile_app_request';

                $to = array($useremail);
                $subject = $subject;
                $from = $fromEmail;
                $cc = array('help@muvi.com','satya@muvi.com','subhalaxmi@muvi.com');

                Yii::app()->theme = 'bootstrap';
                $html = Yii::app()->controller->renderPartial('//email/new_mobile_app_request', array('params' => $template_content), true);
                $returnVal = $this->sendmailViaAmazonsdk($html, $subject, $to, $from, $cc,'','',$fromName);
            }

            Yii::app()->user->setFlash('success', 'Information updated successfully.');

            $url = $this->createUrl('template/rokuApp');
            $this->redirect($url);
        } else {
            $app_info = TvAppInfo::model()->findByAttributes(array('studio_id' => $studio->id));
            $this->render('roku', array('app_info' => $app_info));
        }
    }

    public function actionRokuImageUpload() {
        $params = array();
        parse_str($_REQUEST['data'], $params);
        $studio = $this->studio;
        $studio_id = $studio->id;
        $app_type = 'roku';
        if (!$_FILES['file']['error']) {
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/mobileapp/' . $studio_id;
            switch ($params['img_type']) {
                case 'rokusplash':
                    $cropDimension = array('thumb' => '1920x1080', 'standard' => '1920x1080');
                    $dimension['x1'] = $params['jcrop_splash']['x14'];
                    $dimension['y1'] = $params['jcrop_splash']['y14'];
                    $dimension['x2'] = $params['jcrop_splash']['x24'];
                    $dimension['y2'] = $params['jcrop_splash']['y24'];
                    $dimension['w'] = $params['jcrop_splash']['w4'];
                    $dimension['h'] = $params['jcrop_splash']['h4'];
                    $path = Yii::app()->common->jcropImage($_FILES['file'], $dir, $dimension);
                    $path = $this->uploadMobileImages($_FILES['file'], $studio_id, $app_type, $cropDimension, $path, $app_type);
                    break;
                case 'fhd':
                    $cropDimension = array('thumb' => '540x405', 'standard' => '540x405');
                    $path = Yii::app()->common->jcropImage($_FILES['file'], $dir, $params['jcrop_fhd']);
                    $path = $this->uploadMobileImages($_FILES['file'], $studio_id, $app_type, $cropDimension, $path, $app_type);
                    break;
                case 'hd':
                    $cropDimension = array('thumb' => '290x218', 'standard' => '290x218');
                    $dimension['x1'] = $params['jcrop_hd']['x11'];
                    $dimension['y1'] = $params['jcrop_hd']['y11'];
                    $dimension['x2'] = $params['jcrop_hd']['x21'];
                    $dimension['y2'] = $params['jcrop_hd']['y21'];
                    $dimension['w'] = $params['jcrop_hd']['w1'];
                    $dimension['h'] = $params['jcrop_hd']['h1'];
                    $path = Yii::app()->common->jcropImage($_FILES['file'], $dir, $dimension);
                    $path = $this->uploadMobileImages($_FILES['file'], $studio_id, $app_type, $cropDimension, $path, $app_type);
                    break;
                case 'sd':
                    $cropDimension = array('thumb' => '214x144', 'standard' => '214x144');
                    $dimension['x1'] = $params['jcrop_sd']['x12'];
                    $dimension['y1'] = $params['jcrop_sd']['y12'];
                    $dimension['x2'] = $params['jcrop_sd']['x22'];
                    $dimension['y2'] = $params['jcrop_sd']['y22'];
                    $dimension['w'] = $params['jcrop_sd']['w2'];
                    $dimension['h'] = $params['jcrop_sd']['h2'];
                    $path = Yii::app()->common->jcropImage($_FILES['file'], $dir, $dimension);
                    $path = $this->uploadMobileImages($_FILES['file'], $studio_id, $app_type, $cropDimension, $path, $app_type);
                    break;
            }
        }
        echo $path['original'];
    }

    public function actionChangetemplate() {
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        $old_parent_theme = $std->parent_theme;

        $preview_template = new Previewtemplatehistory();
        $date = date('d-m-Y-h:s:i');

        $preview_template->studio_id = $_REQUEST['studio_id'];
        $preview_template->user_id = $_REQUEST['user_id'];
        $preview_template->preview_ip = $_REQUEST['ipaddress'];
        $preview_template->is_preview = 1;
        $preview_template->template_name = $_REQUEST['template_name'];
        $preview_template->template_color = $_REQUEST['template_color'];
        $preview_template->current_template = $old_parent_theme;
        $preview_template->admin_domain = $_REQUEST['admin_domain'];
        $preview_template->save();
    }

    /**
     * @function to download and upload template for byod
     * @author RK<support@muvi.in>
     * @return HTML 
     */
    public function actionDownloadtemplate() {
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        $template = (isset($_REQUEST['template']) && $_REQUEST['template'] != '') ? $_REQUEST['template'] : '';
        if ($template != '') {
            if ($std->parent_theme == $template) {
                $theme = $std->theme;
                $parent_theme = $template;
                $sourcePath = 'themes/' . $theme . '/';
            } else {
                $theme = $std->theme;
                $parent_theme = $template;
                $sourcePath = 'sdkThemes/' . $parent_theme . '/';
            }
        } else {
            $theme = $std->theme;
            $parent_theme = $std->parent_theme;
            $sourcePath = 'themes/' . $theme . '/';
        }
        $action = 'error';
        $message = 'Error in downloading.';

        //Collecting the source theme folder
        if (file_exists($sourcePath)) {
            //Setting the target ZIP file
            $backup = $theme . '_' . $parent_theme . '_' . date("Y-m-d-H-i-s") . '.zip';
            $destPath = 'themebackups/' . $backup;

            //Creating ZIP
            $zip = Yii::app()->zip;
            $zip->zipDir($sourcePath, $destPath);

            if (file_exists($destPath)) {
                //Deleting the previous backup files
                $log = new TemplateLog;
                $logs = $log->findAllByAttributes(array('action' => 'download', 'studio_id' => $studio_id), array('order' => 'id ASC'));
                foreach ($logs as $log) {
                    $old_backup = 'themebackups/' . $log->backup_file;
                    if (file_exists($old_backup)) {
                        @unlink($old_backup);
                    }
                }

                $log = new TemplateLog();
                $log->studio_id = $studio_id;
                $log->template_code = $parent_theme;
                $log->backup_file = $backup;
                $log->action_at = new CDbExpression("NOW()");
                $log->action = 'download';
                $log->ip_address = CHttpRequest::getUserHostAddress();
                $log->save();

                $action = 'success';
                $message = Yii::app()->getBaseUrl(true) . '/' . $destPath;
            }
        }

        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }

    public function actionUploadtemplate() {
        $this->layout = false;
        $action = 'error';
        $std = Yii::app()->common->getStudiosId(1);
        $studio_id = $std->id;
        $theme = $std->theme;
        $parent_theme = $std->parent_theme;
        $max_template_size = 20971520; //20MB=20971520B
        $restricted_template_files = array('mp4', 'mp3', 'sql', 'php', 'xml', 'doc', 'docx', 'xls', 'xlsx', 'pdf');
        $message = '<span class="error">Error in uploading. Please upload a valid zip file.</span>';
        if (count($_FILES) && $_FILES['temp_file']['size'] > 0 && !($_FILES['temp_file']['error'])) {
            if ($_FILES['temp_file']['size'] > $max_template_size) {
                $action = 'error';
                $message = '<span class="error">File size must be less than 20MB.</span>';
            } else {
                $zip = Yii::app()->zip;
                $source = $_FILES['temp_file']['tmp_name'];
                $res = $zip->infosZip($source);
                if (count($res) > 0) {
                    $x = 0;
                    $error = 0;
                    foreach ($res as $key => $value) {
                        $parts = explode('.', $key);
                        if (count($parts) > 1) {
                            $ext = end($parts);
                            if (in_array($ext, $restricted_template_files)) {
                                $action = 'error';
                                $error = 1;
                                $message = '<span class="error">' . $ext . ' files are not allowed.</span>';
                                break;
                            }
                        }
                    }

                    if ($error == 0) {
                        $dest = 'themes/';
                        foreach ($res as $key => $value) {
                            $k = explode('/', $key);
                            if ($k[0] == $theme && $x == 0) {
                                //Taking the back up of old template files
                                //Setting the target ZIP file
                                $sourcePath = 'themes/' . $theme . '/';
                                $backup = $theme . '_' . $parent_theme . '_' . date("Y-m-d-H-i-s") . '.zip';
                                $destPath = 'themebackups/' . $backup;

                                //Creating ZIP
                                $zip = Yii::app()->zip;
                                $zip->zipDir($sourcePath, $destPath);

                                //Extracting the new zip file uploaded
                                $ress = $zip->extractZip($source, $dest);

                                //Deleting the previous backup files
                                $log = new TemplateLog;
                                $logs = $log->findAllByAttributes(array('action' => 'upload', 'studio_id' => $studio_id), array('order' => 'id ASC'));
                                foreach ($logs as $log) {
                                    $old_backup = 'themebackups/' . $log->backup_file;
                                    if (file_exists($old_backup)) {
                                        @unlink($old_backup);
                                    }
                                }

                                //Saved template change log in DB
                                $log = new TemplateLog();
                                $log->studio_id = $studio_id;
                                $log->template_code = $parent_theme;
                                $log->backup_file = $backup;
                                $log->action_at = new CDbExpression("NOW()");
                                $log->action = 'upload';
                                $log->ip_address = CHttpRequest::getUserHostAddress();
                                $log->save();

                                Yii::app()->user->setFlash('success', 'Your template uploaded sucessfully.');
                                $action = 'success';
                                $message = '<span class="success">Your template uploaded sucessfully.</span>';
                                break;
                            } else {
                                $action = 'error';
                                $message = '<span class="error">Please correct the folder structure and then upload. Root folder name in your zip file must be <strong>' . $theme . '</strong>.</span>';
                                break;
                            }
                            $x++;
                        }
                    }
                }
            }
        }
        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }

    public function actionContactsupport() {
        $action = 'error';
        $message = '<span class="error">Error in downloading.</span>';
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        if (isset($_REQUEST['contact_message']) && $_REQUEST['contact_message'] != '') {
            $logo = 'https://www.muvi.com/themes/bootstrap/images/logo.png';
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
            $msg = '<p>' . Yii::app()->user->first_name . ' has requested for a custom design for ' . $std->name . '</p>';
            $msg.= '<p><strong>Website : </strong>' . $std->name . '</p>';
            $msg.= '<p><strong>Domain : </strong>' . $std->domain . '</p>';
            $msg.= '<p><strong>Email : </strong>' . Yii::app()->user->email . '</p>';
            $msg.= '<p><strong>Message:</strong></p>';
            $msg.= '<div>' . $_REQUEST['contact_message'] . '</div>';

            $params = array(
                array('name' => 'logo', 'content' => $logo),
                array('name' => 'msg', 'content' => $msg),
            );

            $subject = 'New custom template design request from Muvi';
            $adminGroupEmail = array(
                        //array('email' => 'studio@muvi.com', 'name' => 'Muvi Developer', 'type' => 'bcc'),
                        array('email' => 'accounts@muvi.com, sandeep@muvi.com', 'name' => 'Muvi Developer', 'type' => 'bcc'),
            );
            $mailAddress = array(
                'subject' => $subject,
                'from_email' => Yii::app()->user->email,
                'from_name' => Yii::app()->user->first_name,
                'to' => $adminGroupEmail
            );

            $template_name = 'studio_general_contact';
            $this->mandrilEmail($template_name, $params, $mailAddress);

            Yii::app()->user->setFlash('success', 'Your request has been sent successfully.');
            $action = 'success';
            $message = '<span class="success">Your request has been sent successfully.</span>';
        }
        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }

    public function actionDummydatastatus() {
        $studio_id = Yii::app()->common->getStudiosId();
        $status = Yii::app()->general->dummyDataCompleted($studio_id);
        echo $status;
    }

    /* [Biswajit]:Commenting this method as it moved to Manage Content -> Setting
     * 
     * public function actionSettings() {
      $this->breadcrumbs = array('Website','Templates','Advanced Settings');
      $this->headerinfo = "Advanced Settings";
      $this->pageTitle = Yii::app()->name . ' | ' . 'Advanced Settings';
      $studio_id = $this->studio->id;

      $poster_sizes = Yii::app()->general->getPosterSize($studio_id);

      $this->render('settings', array('data' => $poster_sizes));
      }

      public function actionSavesettings() {
      $studio_id = $this->studio->id;
      $action = 'error';
      $message = 'Error in saving data';
      if (isset($_REQUEST) && count($_REQUEST) > 0) {
      $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'h_poster_dimension');
      if ($getStudioConfig) {
      $getStudioConfig->config_value = $_REQUEST['h_poster_dimension'];
      $getStudioConfig->save();
      } else {
      $config = new StudioConfig();
      $config->studio_id = $studio_id;
      $config->config_key = 'h_poster_dimension';
      $config->config_value = $_REQUEST['h_poster_dimension'];
      $config->save();
      }

      $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'v_poster_dimension');
      if ($getStudioConfig) {
      $getStudioConfig->config_value = $_REQUEST['v_poster_dimension'];
      $getStudioConfig->save();
      } else {
      $config = new StudioConfig();
      $config->studio_id = $studio_id;
      $config->config_key = 'v_poster_dimension';
      $config->config_value = $_REQUEST['v_poster_dimension'];
      $config->save();
      }

      $action = 'success';
      $message = 'Saved successfully.';
      Yii::app()->user->setFlash('success', $message);
      }
      $ret = array('action' => $action, 'message' => $message);
      echo json_encode($ret);
      }
     */
    public function actionMenu() {
        $this->breadcrumbs = array('Website', 'Menu');
        $this->headerinfo = "Menu";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Menu';
        $studio_id = $this->studio->id;
        $menu_id = 0;
        $topmenu = array();
        $topmenuitems = array();
        $pages = array();
        $exts = array();
        $language_id = $this->language_id;
        $topmenu = Menu::model()->find('studio_id=:studio_id AND position=:position', array(':studio_id' => $studio_id, ':position' => 'top'));
        if (count($topmenu) == 0) {
            $menu = new Menu();
            $menu->studio_id = $studio_id;
            $menu->position = 'top';
            $menu->status = '1';
            $menu->save();
        }


        if (isset($_REQUEST) && count($_REQUEST)) {
            if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'cat_action') {
                $menu_id = $_REQUEST['menu_id'];
                $updmenu = Menu::model()->find('studio_id=:studio_id AND id=:id', array(':studio_id' => $studio_id, ':id' => $menu_id));
                if (count($updmenu) > 0) {
                    $act = (isset($_REQUEST['add_cat_auto']) && $_REQUEST['add_cat_auto'] == 'add_cat_auto') ? '1' : '0';
                    $updmenu->auto_categories = $act;
                    $updmenu->save();

                    $action = 'success';
                    $message = 'Changes are saved to menu successfully.';
                    Yii::app()->user->setFlash('success', $message);
                    $this->redirect($this->createUrl('template/menu'));
                    exit();
                }
            }
        }
        $dbcon = Yii::app()->db;
        $contentCategories = ContentCategories::model()->findAll('studio_id=:studio_id AND parent_id=0 ', array(':studio_id' => $studio_id));
        $topmenu = Menu::model()->find('studio_id=:studio_id AND position=:position', array(':studio_id' => $studio_id, ':position' => 'top'));
        if (count($topmenu) > 0) {
            $menu_id = $topmenu->id;
            $topmenuitems = MenuItem::model()->findAll(array(
                'condition' => 'studio_id=:studio_id AND menu_id=:menu_id AND parent_id=:parent_id AND (language_id=:language_id OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id=:studio_id AND menu_id=:menu_id AND parent_id=:parent_id AND language_id=:language_id))',
                'params' => array(':studio_id' => $studio_id, ':menu_id' => $menu_id, ':parent_id' => 0, ':language_id' => $this->language_id),
                'order' => 'id_seq ASC'
            ));
            $pages = $dbcon->createCommand("SELECT * FROM pages WHERE studio_id={$studio_id}  AND (link_type='internal' OR link_type='external') AND status=1  AND parent_id=0  ORDER BY id_seq ASC")->queryAll();
            $sql = "SELECT E.* FROM `studio_extensions` SE, `extensions` E WHERE SE.extension_id = E.id AND E.status = '1' AND SE.status = '1' AND SE.studio_id = " . $studio_id;
            $exts = $dbcon->createCommand($sql)->queryAll();
        }
        $this->render('menu', array('contentCategories' => $contentCategories, 'extensions' => $exts, 'pages' => $pages, 'menu_id' => $menu_id, 'topmenu' => $topmenu, 'topmenuitems' => $topmenuitems));
    }

    public function actionMenuitem() {
        $studio_id = $this->studio->id;
        $message = '';
        $action = 'success';
        if (isset($_REQUEST) && count($_REQUEST) > 0 && isset($_REQUEST['option']) && $_REQUEST['option'] != '') {
            $language_id = $_REQUEST['language_id'];
            $option = $_REQUEST['option'];
            $item_type = isset($_REQUEST['item_type']) ? $_REQUEST['item_type'] : '';
            if ($option == 'add_menu_item' && $item_type != '') {
                $menu_id = $_REQUEST['menu_id'];
                $menu_item_id = isset($_REQUEST['menu_item_id']) ? $_REQUEST['menu_item_id'] : 0;
                if ($menu_id == 0) {
                    $menu = new Menu();
                    $menu->studio_id = $studio_id;
                    $menu->position = 'top';
                    $menu->status = '1';
                    $menu->save();
                    $menu_id = $menu->id;
                }
                if ($item_type == 0) {
                    $cats = $_REQUEST['category-values'];
                    foreach ($cats as $value) {
                        $contentCategory = ContentCategories::model()->find('studio_id=:studio_id AND id=:id', array(':studio_id' => $studio_id, ':id' => $value));
                        $menu_title = $contentCategory->category_name;
                        $menu_permalink = Yii::app()->general->generatePermalink($menu_title);
                        $menuitem = new MenuItem();
                        $menuitem->studio_id = $studio_id;
                        $menuitem->menu_id = $menu_id;
                        $menuitem->link_type = $item_type;
                        $menuitem->parent_id = '0';
                        $menuitem->status = '1';
                        $menuitem->value = $value;
                        $menuitem->title = $menu_title;
                        $menuitem->permalink = $menu_permalink;
                        $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                        $menuitem->save();
                        $menu_item_id = $menuitem->id;
                        $message.= Yii::app()->general->menuItemForm($studio_id, $item_type, $menu_id, $menu_item_id, $value, $menu_title, $menu_permalink);

                        //Adding data to Url routing table
                        $urlRouting = new UrlRouting();
                        $urlRouting->permalink = $menu_permalink;
                        $urlRouting->mapped_url = '/media/list/menu_item_id/' . $menu_item_id;
                        $urlRouting->studio_id = Yii::app()->user->studio_id;
                        $urlRouting->created_date = new CDbExpression("NOW()");
                        $urlRouting->save();
                    }
                } else if ($item_type == 1) {
                    $pages = $_REQUEST['pages'];

                    foreach ($pages as $value) {
                        $pageobj = new Page();
                        $page = $pageobj->findByPk($value);

                        $menu_title = $page->title;
                        $menu_permalink = $page->permalink;

                        $menuitem = new MenuItem();
                        $menuitem->studio_id = $studio_id;
                        $menuitem->menu_id = $menu_id;
                        $menuitem->link_type = $item_type;
                        $menuitem->parent_id = '0';
                        $menuitem->status = '1';
                        $menuitem->value = $value;
                        $menuitem->title = $menu_title;
                        $menuitem->permalink = $menu_permalink;
                        $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                        $menuitem->save();
                        $menu_item_id = $menuitem->id;
                        $message.= Yii::app()->general->menuItemForm($studio_id, $item_type, $menu_id, $menu_item_id, $value, $menu_title, $menu_permalink);

                        $urlRouting = new UrlRouting();
                        //check the permalink is exists or not 
                        $urlData = $urlRouting->find('permalink=:permalink AND studio_id=:studio_id', array(':permalink' => $menu_permalink, ':studio_id' => Yii::app()->user->studio_id));
                        if (!$urlData) {
                            //Adding data to Url routing table
                            $urlRouting = new UrlRouting();
                            $urlRouting->permalink = $menu_permalink;
                            $urlRouting->mapped_url = '/page/show/permalink/' . $menu_permalink;
                            $urlRouting->studio_id = Yii::app()->user->studio_id;
                            $urlRouting->created_date = new CDbExpression("NOW()");
                            $urlRouting->save();
                        }
                    }
                } else if ($item_type == 2) {
                    $menu_title = $_REQUEST['menu_title'];
                    $menu_permalink = $_REQUEST['menu_permalink'];
                    $menuitem = new MenuItem();
                    $menuitem->studio_id = $studio_id;
                    $menuitem->menu_id = $menu_id;
                    $menuitem->link_type = $item_type;
                    $menuitem->parent_id = '0';
                    $menuitem->status = '1';
                    $menuitem->value = $menu_permalink;
                    $menuitem->title = $menu_title;
                    $menuitem->permalink = $menu_permalink;
                    $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                    $menuitem->save();
                    $menu_item_id = $menuitem->id;
                    $message.= Yii::app()->general->menuItemForm($studio_id, $item_type, $menu_id, $menu_item_id, $menu_permalink, $menu_title, $menu_permalink);
                } else if ($item_type == 3) {
                    $extensions = $_REQUEST['extensions'];
                    foreach ($extensions as $value) {
                        $extension = new Extension();
                        $extension = $extension->findByPk($value);

                        $menu_title = $extension->name;
                        $menu_permalink = Yii::app()->general->generatePermalink($extension->permalink);

                        $menuitem = new MenuItem();
                        $menuitem->studio_id = $studio_id;
                        $menuitem->menu_id = $menu_id;
                        $menuitem->link_type = $item_type;
                        $menuitem->parent_id = '0';
                        $menuitem->status = '1';
                        $menuitem->value = $value;
                        $menuitem->title = $menu_title;
                        $menuitem->permalink = $menu_permalink;
                        $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                        $menuitem->save();
                        $menu_item_id = $menuitem->id;
                        $message.= Yii::app()->general->menuItemForm($studio_id, $item_type, $menu_id, $menu_item_id, $value, $menu_title, $menu_permalink);
                    }
                }
            } else if ($option == 'delete_item') {
                $item_id = $_REQUEST['item_id'];
                $menu_item = new MenuItem();
				$menuitem = $menu_item->findByPk($item_id);
                $menu_item->deleteByPk($item_id);  
				$menu_item->deleteAll('parent_id =' . $item_id);
                UrlRouting::model()->deleteAll(array('condition'=>'studio_id=:studio_id AND permalink=:permalink AND mapped_url=:mapped_url', 'params'=>array(':studio_id'=>$menuitem->studio_id, ':permalink'=>$menuitem->permalink, ':mapped_url'=>'/media/list/menu_item_id/'.$item_id)));
                if ($menuitem->language_parent_id == 0) {
                    MenuItem::model()->deleteAll('language_parent_id =' . $item_id);
                }
                $action = 'success';
                $message = 'Deleted the menu item successfully.';
                Yii::app()->user->setFlash('success', $message);
            } else if ($option == 'save_item') {
                $language_id = $this->language_id;
                $menu_permalink = $_REQUEST['menu_permalink'];
                if ($item_type == 0)
                    $menu_permalink = Yii::app()->common->formatPermalink($menu_permalink);
                $menu_id = $_REQUEST['menu_id'];
                $item_id = $_REQUEST['menu_item_id'];
                $item_type = $_REQUEST['item_type'];
                $menu_item = new MenuItem();
                if($language_id == 20){
                    $menuitem  = $menu_item->findByPk($item_id);
                }else{
                     $menuitem = $menu_item->findByAttributes(array('language_parent_id'=>$item_id, 'language_id'=>$language_id));
                }
                if(!empty($menuitem)){
                    $menuitem->studio_id = $studio_id;
                    if ($menuitem->link_type != 0) {
                        $menuitem->value = $cat;
                    }
                    $menuitem->title = $_REQUEST['menu_title'];
                    if ($menuitem->link_type == 2) {
                        $menuitem->permalink = $menu_permalink;
                    }
                } else {
                    $menuitem  = MenuItem::model()->findByPk($item_id);
                    $menu_item->permalink = $menu_permalink;
                    $menu_item->link_type = $item_type;
                    $menu_item->title = $_REQUEST['menu_title'];
                    $menu_item->parent_id = $menuitem->parent_id;
                    $menu_item->language_id = $this->language_id;
                    $menu_item->language_parent_id = $item_id;
                    $menu_item->studio_id = $studio_id;
                    $menu_item->menu_id = $menu_id;
                    $menu_item->status = 1;
                    $menu_item->value = $menuitem->value;
                    $menu_item->id_seq = $menuitem->id_seq;
                    $menu_item->save();
                }
                $menuitem->save();
                $action = 'success';
                $message = 'Updated the menu item successfully.';
                Yii::app()->user->setFlash('success', $message);
            }
        }

        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }

    public function actionSortmenu() {
        $a = $_REQUEST['jsonString'];
        $jsonstring = stripslashes(str_replace('\"', '"', $a));
        $orders = json_decode($jsonstring);
        $pos = 1;
        foreach ($orders[0] as $order) {
            //print_r($order); 
            $items = new MenuItem();
            $items = $items->findByPk($order->id);
            $items->id_seq = $pos;
            $items->parent_id = 0;
            $items->save();
            $lang_items = new MenuItem();
            $attr = array('id_seq' => $pos, 'parent_id' => 0);
            $condition = "id=:id OR language_parent_id =:id";
            $params = array(':id' => $order->id);
            $lang_items = $lang_items->updateAll($attr, $condition, $params);
            $pos++;
            $childrens = $order->children;
            $childrens = $childrens[0];
            if (count($childrens) > 0) {
                $id_child_seq = 0;
                foreach ($childrens as $child) {
                    $items = new MenuItem();
                    $items = $items->findByPk($child->id);
                    $items->id_seq = $id_child_seq;
                    $items->parent_id = $order->id;
                    $items->save();
                    $lang_child_items = new MenuItem();
                    $attr = array('id_seq' => $id_child_seq, 'parent_id' => $order->id);
                    $condition = "id=:id OR language_parent_id =:id";
                    $params = array(':id' => $child->id);
                    $lang_child_items = $lang_child_items->updateAll($attr, $condition, $params);
                    $id_child_seq++;
                }
            }
        }
    }

    public function actionCmsblocks() {
        $this->breadcrumbs = array('Website', 'Widgets');
        $this->headerinfo = "Widgets";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Widgets';
        $studio_id = $this->studio->id;

        $blocks = CmsBlock::model()->findAll('studio_id=:studio_id AND (language_id=:language_id OR parent_id=0 AND id NOT IN (SELECT parent_id FROM cmsblocks WHERE studio_id=:studio_id AND language_id=:language_id)) ', array(':studio_id' => $studio_id,':language_id'=>$this->language_id));        
        $this->render('cmsblocks', array('blocks' => $blocks, 'language_id'=>$this->language_id));
    }

    public function actionCmsblock() {
        $this->breadcrumbs = array('Website', 'Widgets' => array('template/cmsblocks'), 'Add/Update Widget');
        $this->headerinfo = "Add/Update Widget";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Add/Update Widget';
        $studio_id = $this->studio->id;
        $block_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $block = array();
        if($block_id){
            $block = CmsBlock::model()->getWidget($block_id, $studio_id, $this->language_id);
        }        
        $option = (isset($_REQUEST['option']) && $_REQUEST['option'] != '') ? $_REQUEST['option'] : '';
        if ($option == 'delete') {
            $newblocks = CmsBlock::model()->findAll('studio_id=:studio_id AND code=:block_code', array(':studio_id' => $studio_id, ':block_code' => $block['code']));
            if (isset($newblocks) && count($newblocks) > 0) {
                foreach ($newblocks as $newblock) {
                    $newblock->delete();
                }
                $message = 'Block deleted successfully.';
                Yii::app()->user->setFlash('success', $message);
                $this->redirect(Yii::app()->getbaseUrl(true) . '/template/cmsblocks');
            } else {
                $message = 'Error in deleting the data.';
                Yii::app()->user->setFlash('error', $message);
                $this->redirect(Yii::app()->getbaseUrl(true) . '/template/cmsblocks');
            }
        }

        $this->render('cmsblock', array('block' => $block));
    }

    public function actionActcmsblock() {
        $this->layout = false;
        $error = 1;
        $message = 'Error in processing data';
        $studio_id = $this->studio->id;
        $block_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $block = CmsBlock::model()->find('studio_id=:studio_id AND id = :block_id', array(':studio_id' => $studio_id, ':block_id' => $block_id));
        $option = (isset($_REQUEST['option']) && $_REQUEST['option'] != '') ? $_REQUEST['option'] : '';
        if ($option == 'add') {
            if ($block_id > 0) {
                //Checking duplicate block code
                $chkblock = CmsBlock::model()->find('studio_id=:studio_id AND code=:block_code AND id !=:block_id AND parent_id=0', array(':studio_id' => $studio_id, ':block_code' => $_POST['block_code'], ':block_id' => $block_id));
                if (count($chkblock) == 0) {
                    if(($this->language_id != 20)){
                        $newblock = CmsBlock::model()->find('studio_id=:studio_id AND parent_id=:block_id AND language_id=:language_id', array(':studio_id' => $studio_id, ':block_id' => $block_id, 'language_id' => $this->language_id));
                    }else{
                    $newblock = CmsBlock::model()->find('studio_id=:studio_id AND id=:block_id', array(':studio_id' => $studio_id, ':block_id' => $block_id));
                    }
                    if (!empty($newblock)) {
                        $newblock->title = $_POST['block_name'];
                        $newblock->code = $_POST['block_code'];
                        $newblock->content = Yii::app()->common->encode_to_html($_POST['block_content']);
                        $newblock->save();

                        $error = 0;
                        $message = 'Content Block updated successfully.';
                        Yii::app()->user->setFlash('success', $message);
                    } else {
                        //Added by Utkarsh Sinha for add widgets in multiple languages
                        $widget = CmsBlock::model()->find('studio_id=:studio_id AND id=:block_id AND parent_id=0', array(':studio_id' => $studio_id, ':block_id' => $block_id));
                        $newblock = new CmsBlock();
                        $newblock->title = $_POST['block_name'];
                        $newblock->code = $widget->code;
                        $newblock->studio_id = $studio_id;
                        $newblock->created_date = new CDbExpression("NOW()");
                        $newblock->content = Yii::app()->common->encode_to_html($_POST['block_content']);
                        $newblock->language_id = $this->language_id;
                        $newblock->parent_id = $widget->id;
                        $newblock->save();
                        
                        $error = 0;
                        $message = 'Content Block updated successfully.';
                        Yii::app()->user->setFlash('success', $message);
                    }
                } else {
                    $error = 1;
                    $message = 'Block code must be unique!';
                }
            } else {
                $chkblock = CmsBlock::model()->find('studio_id=:studio_id AND code=:block_code', array(':studio_id' => $studio_id, ':block_code' => $_POST['block_code']));
                if (count($chkblock) == 0) {
                    $newblock = new CmsBlock();
                    $newblock->title = $_POST['block_name'];
                    $newblock->code = $_POST['block_code'];
                    $newblock->studio_id = $studio_id;
                    $newblock->created_date = new CDbExpression("NOW()");
                    $newblock->content = Yii::app()->common->encode_to_html($_POST['block_content']);
                    $newblock->save();

                    $error = 0;
                    $message = 'Content Block saved successfully.';
                    Yii::app()->user->setFlash('success', $message);
                } else {
                    $error = 1;
                    $message = 'Block code must be unique!';
                }
            }
        }
        $ret = array('error' => $error, 'message' => $message);
        echo json_encode($ret);
        exit();
    }

    public function actiontvGuide() {
        $studio_id = Yii::app()->common->getStudiosId();
        $has_tvguide = Yii::app()->common->IsTvguideAvailable($studio_id);
        if ($has_tvguide > 0) {
            $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
            $zone_name = $getStudioConfig->config_value;
            //show preview image, timezone
            $psql = "SELECT tv_guide_default_preview_img from studios where id=" . $studio_id;
            $command = Yii::app()->db->createCommand($psql);
            $p_image = $command->queryAll();
            $preview_image = $p_image[0]['tv_guide_default_preview_img'];

            if ($zone_name == '') {
                $zone_name = 'UTC';
            }

            //get the first channel_id      
            $fst_channel_sql = "SELECT * from tvguide_channel_master where studio_id=" . $studio_id . " order by id DESC Limit 0,1";
            $fstcommand = Yii::app()->db->createCommand($fst_channel_sql);
            $fst_channel = $fstcommand->queryAll();
            if (count($fst_channel) > 0) {
                $sql_allevent = "select b.id,a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and b.channel_id=" . $fst_channel[0]['id'] . " and a.studio_id=" . $studio_id;

                $eventcommand = Yii::app()->db->createCommand($sql_allevent);
                $allevent = $eventcommand->queryAll();
            } else {
                $allevent = array();
            }
            //convert to localtime
            if ($zone_name != 'UTC') {
                foreach ($allevent as $key => $val) {

                    $allevent[$key]['start_time'] = $this->convert_to_local($val['start_time'], $zone_name);
                    $allevent[$key]['end_time'] = $this->convert_to_local($val['end_time'], $zone_name);
                }
            }

            //convert to localtime
            $this->breadcrumbs = array('Playout');
            $this->pageTitle = "Muvi | Playout";
            $this->headerinfo = "Playout";
            
            //show contents in list page
            //$sql = "SELECT distinct F.id , F.name,F.content_types_id, M.id as stream_id, M.episode_title, M.series_number, M.is_episode,M.video_duration FROM films F, movie_streams M WHERE F.id=M.movie_id AND M.is_converted=1 AND M.is_episode=1 AND F.studio_id=" . $studio_id . " AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
            $sql = "SELECT distinct F.id , F.name,F.content_types_id, M.id as stream_id, M.episode_title, M.series_number, M.is_episode,M.video_duration FROM films F, movie_streams M WHERE F.id=M.movie_id AND M.is_converted=1 AND (F.content_types_id!=3 OR M.is_episode=1) AND F.studio_id=" . $studio_id . " ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
            $command = Yii::app()->db->createCommand($sql);
            $list = $command->queryAll();
            //show contents in list page
            if ($list) {
                $view = array();
                $cnt = 1;
                $movieids = '';
                $stream_ids = '';
                foreach ($list AS $k => $v) {
                    if ($v['is_episode']) {
                        $stream_ids .="'" . $v['stream_id'] . "',";
                    } else {
                        $movieids .="'" . $v['id'] . "',";
                    }
                }
                $viewcount = array();
                $movieids = rtrim($movieids, ',');
                $stream_ids = rtrim($stream_ids, ',');
                // Get the Video view Counts 
                if ($movieids) {
                    $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                    $videoLog = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($videoLog) {
                        foreach ($videoLog as $key => $val) {
                            $viewcount[$val['movie_id']] = $val['cnt'];
                        }
                    }
                }
                if ($stream_ids) {
                    $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                    $videoLog = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($videoLog) {
                        foreach ($videoLog as $key => $val) {
                            $episodeViewcount[$val['video_id']] = $val['cnt'];
                        }
                    }
                }

                //Get Posters for the Movies 
                if ($movieids) {
                    $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                    $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($posterData) {
                        foreach ($posterData AS $key => $val) {
                            $posters[$val['movie_id']] = $val;
                        }
                    }
                }
                //Get Posters for Episode
                if ($stream_ids) {
                    $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                    $sposter = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($sposter) {
                        foreach ($sposter AS $key => $val) {
                            $epsodePosters[$val['movie_id']] = $val;
                        }
                    }
                }
            }

            //get list of channels
            $channel_sql = "SELECT * from tvguide_channel_master where studio_id=" . $studio_id . " order by id DESC";
            $ccommand = Yii::app()->db->createCommand($channel_sql);
            $all_channel = $ccommand->queryAll();

            //get timezone list 
            $all_timezone = $this->generate_timezone_list();
            //get timezonelist
            $this->render("tvguide", array('time_zone' => $zone_name, 'all_event' => $allevent, 'all_list' => $list, 'preview_image' => $preview_image, 'all_channel' => $all_channel, 'first_channel' => $fst_channel[0]['id'], 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'timezone_list' => $all_timezone));
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }

    public function actioneditEvent() {
        $studio_id = Yii::app()->user->studio_id;
        $event_title = addslashes($_POST['event_title']);
        $event_start = $_POST['event_start'];
        $event_end = $_POST['event_end'];

        $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name = $getStudioConfig->config_value;

        if ($zone_name == '') {
            $zone_name = 'UTC';
        }

        $event_start = explode(" ", $event_start);
        $dateS = explode("-", $event_start[0]);
        $start_date = $dateS[2] . "-" . $dateS[0] . "-" . $dateS['1'];
        $start_time = $event_start[1];
        $start_day = $start_date . " " . $start_time;
        $utc_start_time = $this->convert_to_utc($start_day, $zone_name);

        $event_end = explode(" ", $event_end);
        $dateS = explode("-", $event_end[0]);
        $end_date = $dateS[2] . "-" . $dateS[0] . "-" . $dateS['1'];
        $end_time = $event_end[1];
        $end_day = $end_date . " " . $end_time;
        $utc_end_time = $this->convert_to_utc($end_day, $zone_name);

        $search_sql = "select a.id,b.stream_type,b.movie_id from stream_events a,ls_schedule b where a.id=b.event_id and (a.event_name='" . $event_title . "' and b.start_time='" . $utc_start_time . "' || b.end_time='" . $utc_end_time . "') and b.studio_id=" . $studio_id;

        $command = Yii::app()->db->createCommand($search_sql);
        $list = $command->queryAll();

        $event_id = $list[0]['id'];
        $stream_type = $list[0]['stream_type'];
        $movie_id = $list[0]['movie_id'];
        $arr = array('event_id' => $event_id, 'stream_type' => $stream_type, 'movie_id' => $movie_id);
        echo json_encode($arr);
    }

    public function actiondeleteChannel() {
        $studio_id = Yii::app()->user->studio_id;
        $channel_id = $_REQUEST['channel_id'];
        $check_active_channel = "select * from stream_events where channel_id=" . $channel_id;
        $command = Yii::app()->db->createCommand($check_active_channel);
        $list = $command->queryAll();
        if (count($list) == 0) {
            $deletesql = "delete from tvguide_channel_master where id=" . $channel_id;
            Yii::app()->db->createCommand($deletesql)->execute();

            echo "deleted";
        } else {
            echo "not";
        }
    }

    public function actionsaveEvent() {

        //echo "<pre>";print_r($_POST);exit;
        $studio_id = Yii::app()->user->studio_id;
        $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name = $getStudioConfig->config_value;

        if ($zone_name == '') {
            $zone_name = 'UTC';
        }
        $ls_schedule_id = $_POST['ls_schedule_id']; //event id
        $dateS = explode("-", $_POST['start_date']);
        $start_date = $dateS[2] . "-" . $dateS[0] . "-" . $dateS['1'];
        $start_time = $_POST['start_time'];
        $local_start_time = $start_date . " " . $start_time;

        $start_day = $start_day1 = $this->convert_to_utc($local_start_time, $zone_name);

        $utc = new DateTimeZone('UTC');
        $movie_id = $_POST['movie_id'];
        $repeat_weekend = $_POST['repeat_weekend'];
        $repeat_weekday = $_POST['repeat_weekday'];
        $repeat_hour = $_POST['repeat_hour'];
        $channel_id = $_POST['channel_id'];
        //Restrict user to enter events for past days
        $current_time = strtotime(gmdate("Y-m-d H:i:s"));

        if ($current_time > strtotime($start_day)) {
            // echo  Yii::app()->user->setFlash('error', 'Selected date should be greater than current date');
            echo "Timingerror";
            exit;
        }

        //get current Utc time
        $current_time = strtotime(gmdate("Y-m-d H:i:s"));
        if ($ls_schedule_id != '') {
            $checksql = "select start_time,end_time from ls_schedule where event_id=" . $ls_schedule_id;

            $checkcommand = Yii::app()->db->createCommand($checksql)->queryAll();

            $even_start_time = strtotime($checkcommand[0]['start_time']);
            $even_end_time = strtotime($checkcommand[0]['end_time']);

            if ($current_time >= $even_start_time && $current_time <= $even_end_time) {
                echo "Eventstarted";
                exit;
            }
        }
        //Restrict user to enter events if channel not available  
        if ($channel_id == 0) {
            //echo  Yii::app()->user->setFlash('error', 'Please select a channel.');
            echo "Nochannel";
            exit;
        }
        //Allow to create event whether condition available or not

        if ($start_day != '' && $movie_id != '') {

            //code to get video duration
            $ip = CHttpRequest::getUserHostAddress();
            $videofile_sql = "select full_movie from movie_streams where id=" . $movie_id;
            $video_filename = Yii::app()->db->createCommand($videofile_sql);
            $video_filename = $video_filename->queryAll();
            $video_filename = $video_filename[0]['full_movie'];
            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
            $bucketName = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $signedBucketPath = $folderPath['signedFolderPath'];
            $videoUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_id . '/' . $video_filename;
            //$duration = $this->getVideoDuration($videoUrl); // o/p: ("hh:mm:ss")
            //End code to get video duration
            // $duration="00:10:00";getVideoDuration
            $duration_sql = "select * from  movie_streams where id=" . $movie_id;
            $durationcommand = Yii::app()->db->createCommand($duration_sql)->queryAll();
            $duration = $durationcommand[0]['video_duration'];
            $parts = explode(':', $duration);
            $seconds = ($parts[0] * 60 * 60) + ($parts[1] * 60) + $parts[2];
            if ($duration == '' && $seconds <= 2) {
                echo "DurationError";
                exit;
            }
            //code to get the end time of the video
            $end_time = date("Y-m-d H:i:s", strtotime($start_day . '+' . $seconds . ' seconds'));
            //End code to get the end time of the video


            $studio_id = Yii::app()->user->studio_id;
            $name = addslashes($_POST['search_text']);
            $count_rec = 0;
            $count_rec1 = 0;

            //query to check whether the event exist in that perticular start and end time
            if ($repeat_hour != '') {

                $end_time_limit = date('Y-m-d H:i:s', strtotime($start_day . ' + ' . $repeat_hour . ' hours'));

                $sql_rec = "select  count(*) total_recr_recd from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  between '" . $start_day . "' and '" . $end_time_limit . "') or( b.end_time between '" . $start_day . "' and '" . $end_time_limit . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;

                $command_rec = Yii::app()->db->createCommand($sql_rec);
                $list_rec = $command_rec->queryAll();
                $count_rec = $list_rec[0]['total_recr_recd'];
                //query to check rec exist greater than start/end time range

                $sql_rec1 = "select  count(*) rec_count from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  <= '" . $start_day . "' and b.end_time >='" . $end_time_limit . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;

                $command_rec1 = Yii::app()->db->createCommand($sql_rec1);
                $list_rec1 = $command_rec1->queryAll();
                $count_rec1 = $list_rec1[0]['rec_count'];
                //query to check rec exist greater than start/end time range
            } else {
                $sql = "select  count(*) total_rec from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  between '" . $start_day . "' and '" . $end_time . "') or( b.end_time between '" . $start_day . "' and '" . $end_time . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;
                $command = Yii::app()->db->createCommand($sql);
                $list = $command->queryAll();
                $count_rec = $list_rec[0]['total_rec'];
                //query to check rec exist greater than start/end time range
                $sql_rec1 = "select  count(*) rec_count from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  <= '" . $start_day . "' and b.end_time >='" . $end_time . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;
                $command_rec1 = Yii::app()->db->createCommand($sql_rec1);
                $list_rec1 = $command_rec1->queryAll();
                $count_rec1 = $list_rec1[0]['rec_count'];
            }

            //End query to check whether the event exist in that perticular start and end time
            if ($count_rec >= 1 || $count_rec1 >= 1) {

                if ($ls_schedule_id == '') { //for creation of Event
                    //echo  Yii::app()->user->setFlash('error', 'Event already exists');
                    echo "Eventexists";
                    exit;
                } else {

                    $allevent = $this->commonTvguide($_POST, $name, $start_day, $duration, $studio_id, $ls_schedule_id, $channel_id);
                }
            } else {

                $allevent = $this->commonTvguide($_POST, $name, $start_day, $duration, $studio_id, $ls_schedule_id, $channel_id);
            }
            //get the day of week details
            $weekday = date("w", strtotime($start_day));
            //for weekend loop count
            $weekend_loop_count = 0;
            $add_date = 0;
            if ($weekday != 0 && $weekday != 6) {

                $weekend_loop_count = 2;
                $add_date = (6 - $weekday);
                // $start_day = strtotime($start_day . "+" . $add_date . " day");
                $startdate = new DateTime($start_day);
                $startdate->modify('+' . $add_date . ' day');
                $start_day = $startdate->format('Y-m-d H:i:s');
            } else if ($weekday == 6) {
                $weekend_loop_count = 1;
                $add_date = 1;
                $startdate = new DateTime($start_day);
                $startdate->modify('+' . $add_date . ' day');
                $start_day = $startdate->format('Y-m-d H:i:s');
            } else if ($weekday == 0) {
                $weekend_loop_count = 0;
                $add_date = 0;
                $startdate = new DateTime($start_day);
                $startdate->modify('+' . $add_date . ' day');
                $start_day = $startdate->format('Y-m-d H:i:s');
            } else {
                
            }
            //for weekend loop count


            if ($repeat_weekend == 1 && $weekend_loop_count > 0) {
                for ($i = 0; $i < $weekend_loop_count; $i++) {

                    $allevent = $this->commonTvguide($_POST, $name, $start_day, $duration, $studio_id, $ls_schedule_id, $channel_id);
                    $add_date = $add_date + 1;

                    $startdate = new DateTime($start_day1);
                    $startdate->modify('+' . $add_date . ' day');
                    $start_day = $startdate->format('Y-m-d H:i:s');
                }
            }
            //for weekday loop count
            $weekday_loop_count = 0;
            $start_day = $start_day1;
            if ($weekday > 0 && $weekday < 6) {
                $weekday_loop_count = (5 - $weekday);
            }

            //for weekday  loop count
            if ($repeat_weekday == 1) {
                for ($i = 0; $i < $weekday_loop_count; $i++) {

                    $startdate = new DateTime($start_day);
                    $startdate->modify('+1 day');
                    $start_day = $startdate->format('Y-m-d H:i:s');

                    $allevent = $this->commonTvguide($_POST, $name, $start_day, $duration, $studio_id, $ls_schedule_id, $channel_id);
                }
            }
            //for weekday loop count
            $end_time_limit = date('Y-m-d H:i:s', strtotime($start_day . ' + ' . $repeat_hour . ' hours'));

            if ($repeat_weekend != 1 && $repeat_weekday != 1 && $repeat_hour != '') {

                $get_loop_total = ($repeat_hour * 60 * 60) / $seconds;

                for ($i = 0; $i < $get_loop_total; $i++) {
                    $start_day = date("Y-m-d H:i:s", strtotime($start_day . '+' . ($seconds + 1) . ' seconds'));
                    $end_time = date("Y-m-d H:i:s", strtotime($start_day . '+' . $seconds . ' seconds'));

                    if (strtotime($end_time) <= strtotime($end_time_limit)) {

                        $allevent = $this->commonTvguide($_POST, $name, $start_day, $duration, $studio_id, $ls_schedule_id, $channel_id);
                    }
                }
            }
            //for weekday loop count
            $sql_allevent = "select b.id,a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and b.channel_id=" . $channel_id . " and a.studio_id=" . $studio_id;

            $eventcommand = Yii::app()->db->createCommand($sql_allevent);
            $allevent = $eventcommand->queryAll();
            if ($zone_name != 'UTC') {
                foreach ($allevent as $key => $val) {

                    $allevent[$key]['start_time'] = $this->convert_to_local($val['start_time'], $zone_name);
                    $allevent[$key]['end_time'] = $this->convert_to_local($val['end_time'], $zone_name);
                }
            }
            $this->layout = false;
            echo $this->render('ajaxeventcalender', array('all_event' => $allevent));
            exit;
        } else { //empty form submission
            //echo Yii::app()->user->setFlash('error', 'Please enter the event details');
            echo "NoDetail";
            exit;
        }
    }

    public function actioneventAutocomplete() {
        $studio_id = Yii::app()->user->studio_id;
        $sq = $_REQUEST['term'];
        //fetch records for those contents which are multiparts excluding the parent type.

        $sql = "SELECT distinct F.id , F.name,F.content_types_id, M.id as stream_id, M.episode_title, M.series_number, M.is_episode,M.video_duration FROM films F, movie_streams M WHERE F.id=M.movie_id AND M.is_converted=1 AND (F.content_types_id!=3 OR M.is_episode=1) AND F.studio_id=" . $studio_id . " AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
        //$sql = "SELECT distinct F.id , F.name,F.content_types_id, M.id as stream_id, M.episode_title, M.series_number, M.is_episode,M.video_duration FROM films F, movie_streams M WHERE F.id=M.movie_id AND M.is_converted=1 AND (F.content_types_id!=3 OR M.is_episode=1) AND F.studio_id=" . $studio_id . " AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
        $command = Yii::app()->db->createCommand($sql);
        $list = $command->queryAll();

        if ($list) {
            foreach ($list AS $key => $val) {
                if (isset($_REQUEST['movie_name'])) {

                    if ($val['is_episode'] == 1) {
                        $tlt = ($val['episode_title'] != '') ? $val['episode_title'] : " SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                        $tlt = 'Episode-' . $tlt;
                        $arr['movies'][] = array('movie_name' => $val['name'] . ' ' . $tlt, 'movie_id' => $val['stream_id'], 'stream_type' => $val['content_types_id'], 'is_episode' => 1, 'duration' => $val['video_duration']);
                    } else {
                        $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['stream_id'], 'stream_type' => $val['content_types_id'], 'is_episode' => 0, 'duration' => $val['video_duration']);
                    }
                } else {
                    $arr[] = array('category' => 'Movie', 'label' => $val['name'], 'stream_type' => $val['content_types_id']);
                }
            }
        }
        echo json_encode($arr);
        exit;
    }

    public function actionajaxSearchContent() {
        $sq = $_REQUEST['search_word'];
        $studio_id = Yii::app()->user->studio_id;
        if ($sq != '') {
            $sql = "SELECT distinct F.id , F.name,F.content_types_id, M.id as stream_id, M.episode_title, M.series_number, M.is_episode,M.video_duration FROM films F, movie_streams M WHERE F.id=M.movie_id AND M.is_converted=1 AND (F.content_types_id!=3 OR M.is_episode=1) AND F.studio_id=" . $studio_id . " AND LOWER(F.name) LIKE '%" . strtolower($sq) . "%' ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
        } else {
            $sql = "SELECT distinct F.id , F.name,F.content_types_id, M.id as stream_id, M.episode_title, M.series_number, M.is_episode,M.video_duration FROM films F, movie_streams M WHERE F.id=M.movie_id AND M.is_converted=1 AND (F.content_types_id!=3 OR M.is_episode=1) AND F.studio_id=" . $studio_id . "  ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC";
        }

        $command = Yii::app()->db->createCommand($sql);
        $list = $command->queryAll();
        if ($list) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            foreach ($list AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .="'" . $v['stream_id'] . "',";
                } else {
                    $movieids .="'" . $v['id'] . "',";
                }
            }
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = Yii::app()->db->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                $videoLog = Yii::app()->db->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['video_id']] = $val['cnt'];
                    }
                }
            }

            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = Yii::app()->db->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }

        $this->layout = false;
        echo $this->render('ajaxsearchcontent', array('all_list' => $list, 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount));
    }

    public function CommonTvguide($post, $name, $start_day, $duration, $studio_id, $ls_schedule_id, $channel_id) {
        //print_r($post);exit;
        // $duration="00:45:00";
        $repeat_weekend = $post['repeat_weekend'] != '' ? $post['repeat_weekend'] : 0;
        $repeat_weekday = $post['repeat_weekday'] != '' ? $post['repeat_weekday'] : 0;
        $repeat_hour = $post['repeat_hour'] != '' ? $post['repeat_hour'] : 0;

        $parts = explode(':', $duration);
        //$parts[0]=substr($parts[0],1);
        //$parts[2]=substr($parts[0],-1);
        $seconds = ($parts[0] * 60 * 60) + ($parts[1] * 60) + $parts[2];
        $end_time = date("Y-m-d H:i:s", strtotime($start_day . '+' . $seconds . ' seconds'));
        //Start Stream type values
        $movie_id = $post['movie_id'];
        $stream_type = ($post['content_types_id'] == 4) ? 1 : 2;

        if ($stream_type == 2) {
            $stream_id = $movie_id;
        } else {
            //enter lifestream table id
            $livesql = "select * from livestream where movie_id=" . $movie_id;
            $eventcommand = Yii::app()->db->createCommand($livesql);
            $allevent = $eventcommand->queryAll();

            $stream_id = $allevent[0]['id'];
        }

        // end Stream type values
        // count 1 of recs
        $sql = "select  * from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  between '" . $start_day . "' and '" . $end_time . "') or( b.end_time between '" . $start_day . "' and '" . $end_time . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;

        $command = Yii::app()->db->createCommand($sql);
        $list = $command->queryAll();
        $count = count($list);
        //count 2 of recs
        $sql_rec1 = "select  * from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  <= '" . $start_day . "' and b.end_time >='" . $end_time . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;

        $command_rec1 = Yii::app()->db->createCommand($sql_rec1);
        $list_rec1 = $command_rec1->queryAll();
        $count_rec1 = count($list_rec1);


        if ($count < 1 && $count_rec1 < 1) {

            if ($ls_schedule_id == '') {  // create event  
                $add_event = "insert into `stream_events`(`studio_id`,`channel_id`, `event_name`,`repeat_weekend`, `repeat_weekdays`, `repeat_hours`,`created_date`) values(" . $studio_id . "," . $post['channel_id'] . ",'" . $name . "'," . $repeat_weekend . "," . $repeat_weekday . "," . $repeat_hour . ",'" . date('Y-m-d H:i:s') . "')";
                Yii::app()->db->createCommand($add_event)->execute();
                $event_id = Yii::app()->db->getLastInsertID();
            } else if ($ls_schedule_id != '') { // Edit event        

                //get stream event_id from ls-schedule id                 

                $event_id = $stream_event_id = $ls_schedule_id;
            } else {
                $event_id = '';
            }

            if ($ls_schedule_id == '') { //create event
                $add_schedule = "insert into `ls_schedule`(`movie_id`, `event_id`, `studio_id`, `stream_type`, `stream_id`, `start_time`, `end_time`,`ip`,  `creation_date`) values(" . $movie_id . "," . $event_id . "," . $studio_id . "," . $stream_type . "," . $stream_id . ",'" . $start_day . "','" . $end_time . "','" . $ip . "','" . date('Y-m-d H:i:s') . "')";
            } else { //Edit event
                $update_event = "update stream_events set event_name='" . $name . "', repeat_weekend=" . $repeat_weekend . ",repeat_weekdays =" . $repeat_weekday . ",repeat_hours=" . $repeat_hour . " where id=" . $event_id;
                Yii::app()->db->createCommand($update_event)->execute();
                $add_schedule = "update `ls_schedule` set event_id=" . $event_id . ",start_time='" . $start_day . "',end_time='" . $end_time . "',stream_type=" . $stream_type . ",stream_id=" . $stream_id . " where id=" . $ls_schedule_id . " and studio_id=" . $studio_id;
            }
            Yii::app()->db->createCommand($add_schedule)->execute();

            //show all events after creating new event
            $sql_allevent = "select a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and a.studio_id=" . $studio_id;
            $eventcommand = Yii::app()->db->createCommand($sql_allevent);
            $allevent = $eventcommand->queryAll();
        } else if ($count == 1 && $count_rec1 == 1) {

            if ($ls_schedule_id == '') {  //create Event (Not possible)
                $allevent = array();
                //echo Yii::app()->user->setFlash('error', 'Event already exists');  
                echo "Eventexists";
                exit;
            } else { //Edit  possible  only without changing the event start and end time just change the content

                if ($list_rec1[0]['id'] == $ls_schedule_id || $list[0]['id'] == $ls_schedule_id) { //update same event

                    $event_id = $stream_event_id = $ls_schedule_id;

                    //update both tables
                    $update_event = "update stream_events set event_name='" . $name . "', repeat_weekend=" . $repeat_weekend . ",repeat_weekdays =" . $repeat_weekday . ",repeat_hours=" . $repeat_hour . " where id=" . $event_id;
                    Yii::app()->db->createCommand($update_event)->execute();
                    $add_schedule = "update `ls_schedule` set event_id=" . $event_id . ",start_time='" . $start_day . "',end_time='" . $end_time . "',stream_type=" . $stream_type . ",stream_id=" . $stream_id . " where id=" . $event_id . " and studio_id=" . $studio_id;
                    Yii::app()->db->createCommand($add_schedule)->execute();

                    //show all events after creating new event
                    $sql_allevent = "select a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and a.studio_id=" . $studio_id;
                    $eventcommand = Yii::app()->db->createCommand($sql_allevent);
                    $allevent = $eventcommand->queryAll();
                } else {
                    echo "Eventexists";

                    exit;
                }
            }
        } else if (($count == 1 && $count_rec1 < 1) || ($count < 1 && $count_rec1 == 1)) {
            if ($ls_schedule_id == '') {  //create Event (Not possible)
                $allevent = array();
                echo "Eventexists";
                exit;
            } else { //Edit  possible  only without changing the event start and end time just change the content
                if ($list_rec1[0]['id'] == $ls_schedule_id || $list[0]['id'] == $ls_schedule_id) {

                    $event_sql = "select event_id from ls_schedule where id=" . $ls_schedule_id;
                    $command = Yii::app()->db->createCommand($event_sql);
                    $list_event = $command->queryAll();
                    $event_id = $stream_event_id = $list_event[0]['event_id'];

                    //update both tables
                    $update_event = "update stream_events set event_name='" . $name . "', repeat_weekend=" . $repeat_weekend . ",repeat_weekdays =" . $repeat_weekday . ",repeat_hours=" . $repeat_hour . " where id=" . $event_id;
                    Yii::app()->db->createCommand($update_event)->execute();

                    $add_schedule = "update `ls_schedule` set event_id=" . $event_id . ",start_time='" . $start_day . "',end_time='" . $end_time . "',stream_type=" . $stream_type . ",stream_id=" . $stream_id . " where id=" . $event_id . " and studio_id=" . $studio_id;
                    Yii::app()->db->createCommand($add_schedule)->execute();

                    //show all events after creating new event
                    $sql_allevent = "select a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and a.studio_id=" . $studio_id;
                    $eventcommand = Yii::app()->db->createCommand($sql_allevent);
                    $allevent = $eventcommand->queryAll();
                    //  echo  Yii::app()->user->setFlash('success', 'Event updated successfully'); 
                } else {
                    echo "Eventexists";

                    exit;
                }
            }
        } else {

            $allevent = array();
            // echo Yii::app()->user->setFlash('error', 'Event already exists');
            echo "Eventexists";
            exit;
        }

        return $allevent;
    }

    public function actiongetEndTime() {
        $studio_id = Yii::app()->user->studio_id;
        $movie_id = $_REQUEST['movie_id'];
        $flag = 1;
        if (empty(trim($_REQUEST['start_value']))) {
            $flag = 0;
        }

        $dateT = explode(" ", $_REQUEST['start_value']);
        $stime = $dateT[1];
        $dateS = explode("-", $dateT['0']);
        $start_time = $dateS[2] . "-" . $dateS[0] . "-" . $dateS['1'] . " " . $stime;
        if ($movie_id != '' && $flag == 1) {
            $videofile_sql = "select full_movie from movie_streams where id=" . $movie_id;
            $video_filename = Yii::app()->db->createCommand($videofile_sql);
            $video_filename = $video_filename->queryAll();
            $video_filename = $video_filename[0]['full_movie'];

            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);

            $bucketName = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);

            $signedBucketPath = $folderPath['signedFolderPath'];
            $videoUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_id . '/' . $video_filename;
            //echo $videoUrl;
            //$videoUrl="http://stagingstudio.s3.amazonaws.com/462/EncodedVideo/uploads/movie_stream/full_movie/2986/small.mp4";
            //$duration = $this->getVideoDuration($videoUrl); // o/p: ("hh:mm:ss")
            //$duration="00:00:05";
            //echo $duration;
            $duration_sql = "select * from  movie_streams where id=" . $movie_id;
            $durationcommand = Yii::app()->db->createCommand($duration_sql)->queryAll();
            $duration = $durationcommand[0]['video_duration'];
            if ($duration) {
                $parts = explode(':', $duration);
                $parts[0] = substr($parts[0], 1);
                $parts[2] = substr($parts[0], -1);
                $seconds = ($parts[0] * 60 * 60) + ($parts[1] * 60) + $parts[2];
                $end_time = date("m-d-Y H:i:s", strtotime($start_time . '+' . $seconds . ' seconds'));
                echo $end_time;
                exit;
            }
        }
    }

    public function actionupdateDroppedImage() {

        $content_id = $_REQUEST['id'];
        $title = $_REQUEST['title'];
        $content_types_id = $_REQUEST['content_type'];

        $channel_id = $_REQUEST['channel_id'];
        $studio_id = Yii::app()->user->studio_id;
        $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name = $getStudioConfig->config_value;

        if ($zone_name == '') {
            $zone_name = 'UTC';
        }
        $movie_id = $content_id;
        $start_date1 = $_REQUEST['start_date'];
        $exp_start_date = explode(" ", $start_date1);
        $dateS = explode("-", $exp_start_date[0]);
        $local_start_time = $dateS[2] . "-" . $dateS[0] . "-" . $dateS['1'] . " " . $exp_start_date[1];
        $start_day = $start_day1 = $this->convert_to_utc($local_start_time, $zone_name);

        if ($movie_id != '' && (new DateTime() <= new DateTime($local_start_time))) {
            $videofile_sql = "select full_movie from movie_streams where id=" . $movie_id;
            $video_filename = Yii::app()->db->createCommand($videofile_sql);
            $video_filename = $video_filename->queryAll();
            $video_filename = $video_filename[0]['full_movie'];

            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);

            $bucketName = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);

            $signedBucketPath = $folderPath['signedFolderPath'];
            $videoUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_id . '/' . $video_filename;

            //$videoUrl="http://stagingstudio.s3.amazonaws.com/462/EncodedVideo/uploads/movie_stream/full_movie/2986/small.mp4";
            $duration = $this->getVideoDuration($videoUrl);
            //$duration="00:45:00";
            // echo $start_day;
            $parts = explode(':', $duration);
            $parts[0] = substr($parts[0], 1);
            $parts[2] = substr($parts[0], -1);
            $seconds = ($parts[0] * 60 * 60) + ($parts[1] * 60) + $parts[2];
            $end_time = date("Y-m-d H:i:s", strtotime($start_day . '+' . $seconds . ' seconds'));
            $sql = $sql = "select  count(*) total_rec from stream_events a,ls_schedule b where a.id=b.event_id and ((b.start_time  between '" . $start_day . "' and '" . $end_time . "') or( b.end_time between '" . $start_day . "' and '" . $end_time . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;
            $list = Yii::app()->db->createCommand($sql)->queryAll();

            $sql_rec1 = "select  count(*) total_rec1 from stream_events a,ls_schedule b where a.id=b.event_id  and ((b.start_time  <= '" . $start_day . "' and b.end_time >='" . $end_time . "')) and a.channel_id=" . $channel_id . " and b.studio_id=" . $studio_id;

            $command_rec1 = Yii::app()->db->createCommand($sql_rec1);
            $list_rec1 = $command_rec1->queryAll();

            if ($list[0]['total_rec'] < 1 && $list_rec1[0]['total_rec1'] < 1) {

                $add_event = "insert into `stream_events`(`studio_id`, `channel_id`,`event_name`,`created_date`) values(" . $studio_id . "," . $channel_id . ",'" . $title . "','" . date('Y-m-d H:i:s') . "')";

                Yii::app()->db->createCommand($add_event)->execute();
                $event_id = Yii::app()->db->getLastInsertID();

                $stream_type = ($content_types_id == 4) ? 1 : 2;

                if ($stream_type == 2) {
                    $stream_id = $movie_id;
                } else {
                    //enter lifestream table id
                    $livesql = "select * from livestream where movie_id=" . $movie_id;
                    $eventcommand = Yii::app()->db->createCommand($livesql);
                    $allevent = $eventcommand->queryAll();

                    $stream_id = $allevent[0]['id'];
                }

                $add_schedule = "insert into `ls_schedule`(`movie_id`, `event_id`, `studio_id`, `stream_type`, `stream_id`, `start_time`, `end_time`,`ip`,  `creation_date`) values(" . $movie_id . "," . $event_id . "," . $studio_id . "," . $stream_type . "," . $stream_id . ",'" . $start_day . "','" . $end_time . "','" . $ip . "','" . date('Y-m-d H:i:s') . "')";

                Yii::app()->db->createCommand($add_schedule)->execute();

                $schedule_id = Yii::app()->db->getLastInsertID();
                $event_array = array();
                $event_array['id'] = $schedule_id;
                $event_array['title'] = $title;
                $event_array['start'] = $start_time;
                $event_array['end'] = $end_time;
            } else {

                $event_array = array();
            }
        }

        $channel_id = $_REQUEST['channel_id'];

        $sql_allevent = "select b.id,a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and b.channel_id=" . $channel_id . " and a.studio_id=" . $studio_id;

        $eventcommand = Yii::app()->db->createCommand($sql_allevent);
        $allevent = $eventcommand->queryAll();
        if ($zone_name != 'UTC') {
            foreach ($allevent as $key => $val) {

                $allevent[$key]['start_time'] = $this->convert_to_local($val['start_time'], $zone_name);
                $allevent[$key]['end_time'] = $this->convert_to_local($val['end_time'], $zone_name);
            }
        }
        $this->layout = false;
        if (count($event_array) == 0) {
            echo "error";
            exit;
        } else {
            echo $this->render('ajaxeventcalender', array('all_event' => $allevent, 'event_array' => $event_array));
        }
    }

    public function actionuploadPreview() {

        $studio_id = Yii::app()->user->studio_id;
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        $tmp_name = $_FILES['file']['tmp_name'];

        if (!$_FILES['file']['error']) {

            $name = $_FILES["file"]["name"] = Yii::app()->common->fileName($_FILES["file"]["name"]);

            $studio_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/tv_guide/' . $studio_id;
            mkdir($studio_dir, 0777, true);
            chmod($studio_dir, 0777);
            $file_info = pathinfo($_FILES['file']['name']);
            $type = $file_info['extension'];

            if ($type == "jpg" || $type == "jpeg" || $type == "JPG" || $type == "JPEG" || $type == "PNG" || $type == "png" || $type == "gif") {
                if (file_exists($studio_dir)) {
                    unlink($studio_dir);
                }
                move_uploaded_file($_FILES['file']['tmp_name'], $studio_dir . '/' . $name);

                $img_path = $studio_dir . '/' . $name;
                //resize the image
                $maxWidth = 850;
                $maxHeight = 407;
                require_once "Image.class.php";
                require_once "Config.class.php";
                $config = Config::getInstance();
                $this->image = new Image();
                $this->image->load($img_path);

                $dimensions = array($maxWidth, $maxHeight);
                $this->image->resize($dimensions[0], $dimensions[1]);
                $this->image->save($img_path);

                $source_path = $img_path;
                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                $unsignedFolderPath = $folderPath['unsignedFolderPath'];
                $bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                $bucketName = $bucketInfoForLogo['bucket_name'];
                $client = Yii::app()->common->connectToAwsS3($studio_id);

                $dest_guide_path = $unsignedFolderPath . "tv_guide"; //newuser


                $source_guide_path = $source_path;
                $acl = 'public-read';
                $result_ = $client->putObject(array(
                    'Bucket' => $bucketName,
                    'Key' => $dest_guide_path . "/" . $name,
                    'SourceFile' => $source_guide_path,
                    'ACL' => $acl,
                ));
                unlink($studio_dir);

                $sql = "update studios set `tv_guide_default_preview_img`='" . $name . "' where id=" . $studio_id;

                Yii::app()->db->createCommand($sql)->execute();
                Yii::app()->user->setFlash('success', 'Image uploaded successfully');
                echo true;

                exit;
            } else {
                Yii::app()->user->setFlash('error', 'Error in image upload');
                echo false;
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', 'Error in Image upload');
            echo false;
            exit;
        }
    }

    public function actionsaveChannel() {
        // print_r($_FILES["channel_image"]);
        $studio_id = Yii::app()->user->studio_id;
        $channel_name = $_POST['channel_name'];
        $channel_id = $_POST['chnel_id'];
        //check whether the channel exists or not ??
        $ch_sql = "select * from tvguide_channel_master where studio_id=" . $studio_id . " and channel_name ='" . trim($channel_name) . "'";
        if ($channel_id > 0)
            $ch_sql.=' and id !=' . $channel_id;

        $channelcommand = Yii::app()->db->createCommand($ch_sql);
        $channel_count = $channelcommand->queryAll();

        if (count($channel_count) >= 1) {//create Channel
            Yii::app()->user->setFlash('error', 'Channel already exists');
            $url = $this->createUrl('/template/tvguide');
            $this->redirect($url);
            exit;
        }
        $channel_id = $_POST['chnel_id'];

        $user_id = Yii::app()->user->id;
        $cond = '';
        $name = '';
        if ($channel_name != '') {
            if ($_FILES["channel_image"]["name"] != '') {
                $name = $_FILES["channel_image"]["name"] = Yii::app()->common->fileName($_FILES["channel_image"]["name"]);
                $cond = ",channel_logo='" . $name . "'";
                $studio_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/tv_guide/channel_logo/' . $studio_id;
                mkdir($studio_dir, 0777, true);
                chmod($studio_dir, 0777);
                $file_info = pathinfo($_FILES['channel_image']['name']);
                $type = $file_info['extension'];

                if ($type == "jpg" || $type == "jpeg" || $type == "JPG" || $type == "JPEG" || $type == "PNG" || $type == "png" || $type == "gif") {
                    if (file_exists($studio_dir)) {
                        unlink($studio_dir);
                    }
                    move_uploaded_file($_FILES['channel_image']['tmp_name'], $studio_dir . '/' . $name);
                    $img_path = $studio_dir . '/' . $name;
                    //resize the image
                    $maxWidth = 186;
                    $maxHeight = 108;
                    require_once "Image.class.php";
                    require_once "Config.class.php";
                    $config = Config::getInstance();
                    $this->image = new Image();
                    $this->image->load($img_path);

                    $dimensions = array($maxWidth, $maxHeight);
                    $this->image->resize($dimensions[0], $dimensions[1]);
                    $this->image->save($img_path);

                    $source_path = $img_path;
                    $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                    $unsignedFolderPath = $folderPath['unsignedFolderPath'];
                    $bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                    $bucketName = $bucketInfoForLogo['bucket_name'];
                    $client = Yii::app()->common->connectToAwsS3($studio_id);


                    $dest_guide_path = $unsignedFolderPath . "tv_guide/channel_logo"; //newuser                    

                    $source_guide_path = $source_path;
                    $acl = 'public-read';
                    $result_ = $client->putObject(array(
                        'Bucket' => $bucketName,
                        'Key' => $dest_guide_path . "/" . $name,
                        'SourceFile' => $source_guide_path,
                        'ACL' => $acl,
                    ));
                    unlink($studio_dir);

                    //insert the record into chanel master
                } else {
                    Yii::app()->user->setFlash('error', 'Please upload image files');
                    $url = $this->createUrl('/template/tvguide');
                    $this->redirect($url);
                    exit;
                }
            }
            if ($channel_id == '') {
                $sql = "insert into `tvguide_channel_master`(`channel_name`, `channel_logo`, `studio_id`, `creater_id`, `creation_date`)values('" . $channel_name . "','" . $name . "'," . $studio_id . "," . $user_id . ",'" . date("Y-m-d H:i:s") . "')";
                Yii::app()->db->createCommand($sql)->execute();
                Yii::app()->user->setFlash('success', 'Channel added successfully');
            } else {
                $sql = "update `tvguide_channel_master` set channel_name='" . $channel_name . "' " . $cond . " where id=" . $channel_id;
                Yii::app()->db->createCommand($sql)->execute();
                Yii::app()->user->setFlash('success', 'Channel updated successfully');
            }
        } else {
            Yii::app()->user->setFlash('error', 'Error in channel creation');
        }

        $url = $this->createUrl('/template/tvguide');
        $this->redirect($url);
        exit;
    }

    public function actionshowCalender() {
        $studio_id = Yii::app()->user->studio_id;
        $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name = $getStudioConfig->config_value;

        if ($zone_name == '') {
            $zone_name = 'UTC';
        }
        $channel_id = $_REQUEST['channel_id'];

        $sql_allevent = "select b.id,a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and b.channel_id=" . $channel_id . " and a.studio_id=" . $studio_id;

        $eventcommand = Yii::app()->db->createCommand($sql_allevent);
        $allevent = $eventcommand->queryAll();
        if ($zone_name != 'UTC') {
            foreach ($allevent as $key => $val) {

                $allevent[$key]['start_time'] = $this->convert_to_local($val['start_time'], $zone_name);
                $allevent[$key]['end_time'] = $this->convert_to_local($val['end_time'], $zone_name);
            }
        }
        $this->layout = false;
        echo $this->render('ajaxeventcalender', array('all_event' => $allevent));
    }

    public function actionshowZone() {
        $studio_id = Yii::app()->user->studio_id;
        $channel_id = $_REQUEST['channel_id'];
        $zone_name = $_REQUEST['zone_name'];

        //check whether timezone exists for the studio or not

        $check_zone = "select * from studio_config where studio_id=" . $studio_id . " AND config_key='tvguide_timezone'";
        $studio_zone = Yii::app()->db->createCommand($check_zone)->queryAll();

        if (count($studio_zone) >= 1) {
            //update timezone for existing studio
            $updatezone = "update `studio_config` set config_value='" . $zone_name . "' where studio_id=" . $studio_id . " AND config_key='tvguide_timezone'";

            Yii::app()->db->createCommand($updatezone)->execute();
        } else {
            //save the timezone for the user
            $update_zone = "insert into `studio_config`(`studio_id`, `config_key`, `config_value`) values(" . $studio_id . ",'tvguide_timezone','" . $zone_name . "')";

            Yii::app()->db->createCommand($update_zone)->execute();
        }


        $sql_allevent = "select b.id,a.studio_id,a.stream_type,a.stream_id,a.start_time,a.end_time, b.event_name from ls_schedule a,stream_events b where a.event_id=b.id and b.channel_id=" . $channel_id . " and a.studio_id=" . $studio_id;
        $eventcommand = Yii::app()->db->createCommand($sql_allevent);
        $allevent = $eventcommand->queryAll();

        foreach ($allevent as $key => $val) {

            $allevent[$key]['start_time'] = $this->convert_to_local($val['start_time'], $zone_name);
            $allevent[$key]['end_time'] = $this->convert_to_local($val['end_time'], $zone_name);
        }
        $this->layout = false;
        echo $this->render('ajaxeventcalender', array('all_event' => $allevent));
    }

    public function actiondeleteEvent() {
        $studio_id = Yii::app()->user->studio_id;
        $event_id = $_REQUEST['event_id'];
        //current UTC/GMT time
        $current_time = strtotime(gmdate("Y-m-d H:i:s"));

        //check the event started or not ??
        $checksql = "select start_time,end_time from ls_schedule where event_id=" . $event_id;
        $checkcommand = Yii::app()->db->createCommand($checksql)->queryAll();

        $even_start_time = strtotime($checkcommand[0]['start_time']);
        $even_end_time = strtotime($checkcommand[0]['end_time']);

        if ($current_time >= $even_start_time && $current_time <= $even_end_time) {
            echo false;
        } else {
            $delete_eve = "delete from stream_events where id=" . $event_id;
            $delcommand = Yii::app()->db->createCommand($delete_eve)->execute();
            $delete_ls = "delete from ls_schedule where event_id=" . $event_id;
            $lsdelcommand = Yii::app()->db->createCommand($delete_ls)->execute();

            echo true;
        }
    }

    public function actionhomepage() {
        $this->breadcrumbs = array('Website', 'Templates', 'Home Page');
        $this->pageTitle = Yii::app()->name . ' | ' . 'Homepage';
        $this->headerinfo = "Homepage";
        $studio = $this->studio;
        $studio_id = $studio->id;
        $language_id = $this->language_id;
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $logo_dimension = Yii::app()->general->getLogoDimension($studio_id);
        $favicon_dimension = array("favicon_width" => 16, "favicon_height" => 16);
        $dimension['logo'] = $logo_dimension;
        $dimension['favicon'] = $favicon_dimension;

        $parent_theme = $this->studio->parent_theme;
        $template_id = 0;
        $temps = new Template;
        $temps = $temps->findAllByAttributes(array('code' => $parent_theme), array('order' => 'id ASC'));
        if (count($temps) > 0)
            $template_id = $temps[0]->id;
        $sc = new BannerSection;
        $sections = $sc->findAllByAttributes(array('template_id' => $template_id, 'studio_id' => 0,'is_new'=>0));
        $sql = "SELECT * FROM banner_text WHERE studio_id={$studio_id} AND (language_id={$this->language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM banner_text WHERE studio_id={$studio_id} AND language_id={$this->language_id})) ORDER BY id DESC LIMIT 0, 1";
        $banner_text = Yii::app()->db->createCommand($sql)->queryRow();
        $theme = Yii::app()->common->getTemplateDetails($this->studio->parent_theme);
		$content = Yii::app()->general->content_count($studio_id);
        $dbcon = Yii::app()->db;
		$style_code = $dbcon->createCommand()
			->select("banner_style_code,auto_scroll,scroll_interval,no_of_active_image")
			->from('studio_banner_style')
			->where('studio_id=:studio_id', array(':studio_id' => $studio_id))
			->queryRow();
        $stylecode = $style_code['banner_style_code'];
        $scroll= ($style_code['auto_scroll'] !="") ? $style_code['auto_scroll'] :0;
        $interval=($style_code['scroll_interval'] !="") ? $style_code['scroll_interval'] :3000;
        $visibleThumb=$style_code['no_of_active_image'];
		if ($style_code != '' && $parent_theme == "traditional-byod"){
			$sections = $sc->findAllByAttributes(array('template_id' => $template_id, 'studio_id' => 0,'is_new'=>1));
		}
        if ($theme->max_featured_sections > 0) {
            $cc_cmd = $dbcon->createCommand("SELECT * FROM featured_section WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM featured_section WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY id_seq ASC LIMIT {$theme->max_featured_sections}");
        } else {
            $cc_cmd = $dbcon->createCommand("SELECT * FROM featured_section WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM featured_section WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY id_seq ASC");
        }
        $section = $cc_cmd->queryAll();
        if (count($theme) > 0 && Yii::app()->custom->hasVideoBanner($theme) == 1) {
            if (!isset($video_section)) {
                $sc = new BannerSection;
                $video_section = $sc->findByAttributes(array('template_id' => $template_id, 'studio_id' => $studio_id));
            } else {
                $video_section = $sections;
            }

            $ban = new StudioBanner;
            $video_data = $ban->findAllByAttributes(array('studio_id' => $studio_id, 'banner_type' => '0', 'banner_section' => $video_section->id));
        }
        $home_layout  = Yii::app()->custom->HomePageLayout();
        $home_design  = Yii::app()->custom->HomePageLayoutType();
        $has_physical = Yii::app()->general->getStoreLink();        
        $this->render('homepage',array('studio' => $studio,'studio_id' => $studio_id,'video_data'=>$video_data,'video_section' => $video_section, 'dimension' => $dimension, 'all_images' => $all_images,'sections' => $sections, 'banner_text' => $banner_text,'data' => $section,'theme'=>$theme, 'language_id' => $language_id,'home_layout'=>$home_layout,'home_design'=>$home_design,'has_physical'=>$has_physical,'styleCode'=>$stylecode,'autoScroll'=>$scroll,'scroll_interval'=>$interval,'visible_thumb'=>$visibleThumb,'content_enable'=>$content));
    }

    public function actionS3mulatipartupload() {
        $actions = array('abortmultipartupload', 'completemultipartupload', 'signuploadpart', 'createmultipartupload');
        $command = strtolower($_REQUEST['command']);
        $studio = $this->studio;
        $studio_id = $studio->id;
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $dest_bucket = $bucketInfo['bucket_name'];
        $region = $bucketInfo['region_code'];
        $s3url = $bucketInfo['s3url'];
        //$dest_bucket = Yii::app()->params->video_bucketname;
        if (in_array($command, $actions)) {

            if (isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType'] == 'videobanner' && @$_REQUEST['otherInfo']['section_id'] != '') {
                $region = '';
                /* $banner_id = $this->addVideoBanner($_REQUEST['otherInfo']['section_id'],$_REQUEST['otherInfo']['movie_name']);
                  if ($banner_id !='') {
                  $_REQUEST['otherInfo']['banner_id'] = $banner_id;
                  } else {
                  $arr = array('error' => 'Error in uploading video');
                  header('Content-Type: application/json');
                  die(json_encode($arr));
                  } */
            }
            if ($command == 'completemultipartupload') {
                $id_banner = $this->addVideoBanner($_REQUEST['otherInfo']['section_id'], $_REQUEST['otherInfo']['movie_name']);
            }
            $s3multi = new S3multipart();
            $arr = $s3multi->$command($dest_bucket, $region);
            if (@$arr['success'] && $command == 'completemultipartupload') {
                $this->updateBannerInfo($id_banner);
            }
        } else {
            $arr = array('error' => 'Requested Command is Invalid');
        }
        header('Content-Type: application/json');
        die(json_encode($arr));
    }

    function updateBannerInfo($id_banner) {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $current_template = $studio->parent_theme;

        $arr['msg'] = "error";
        $bnr = StudioBanner::model()->findByPk($id_banner);
        if ($bnr) {
            $data = $bnr->attributes;
            $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
            $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
            $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
            $s3dir = $unsignedFolderPathForVideo . 'uploads/videobanner/' . $studio_id . "/";
            if ($bnr['image_name']) {
                $response = $s3->getListObjectsIterator(array(
                    'Bucket' => $bucket,
                    'Prefix' => $s3dir
                ));
                foreach ($response as $object) {
                    if ($object['Key'] != $s3dir . $_REQUEST['filename']) {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $object['Key']
                        ));
                    }
                }
            }
            $dt = new DateTime();
            $upload_end_time = $dt->format('Y-m-d H:i:s');
            $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];
            $movie_stream_id = $_REQUEST['movie_stream_id'];
            $section_id = $_REQUEST['section_id'];
            $path_parts = pathinfo($_REQUEST['filename']);
            $imgname = $path_parts['filename'] . '.jpg';
            $bnr->image_name = $imgname;
            $bnr->banner_section = $section_id;
            $bannerVideoUrl = $unsignedBucketPathForViewVideo . 'uploads/videobanner/' . $movie_stream_id . '/' . $_REQUEST['filename'];
            $bnr->video_remote_url = $bucketCloudfrontUrl . '/' . $bannerVideoUrl;
            $bnr->save();
            $arr['msg'] = "Banner uploaded successfully";
            $arr['banner_url'] = $bucketCloudfrontUrl . '/' . $bannerVideoUrl;
            $s3viewdir = $bucketCloudfrontUrl . '/' . $bannerVideoUrl;
            //Generate video thumbnail
            $ffmpeg = FFMPEG_PATH;

            $dir = $_SERVER['DOCUMENT_ROOT'] . "/images/videobanners/" . $studio_id . "/";

            if (!is_dir($dir)) {
                mkdir($dir);
                @chmod($dir, 0777);
            }
            $img_path = $dir . $imgname;
            $img_path_thumb  = $dir ."studio_thumb/". $imgname;

            $second = 1;
            $studio_id = Yii::app()->common->getStudiosId();
            $resp = Yii::app()->common->getSectionBannerDimension($section_id);
            $size=$resp['width'].'x'.$resp['height'];
            $scale = "scale=".$resp['width'].':'.$resp['height'];
            $thumb=Yii::app()->common->getThumbBannerDimension($section->id);
            $thumb_size=$thumb['width'].'x'.$thumb['height'];
            $scale_thumb = "scale=".$thumb['width'].':'.$thumb['height'];
            //screenshot size
            // get the screenshot
            //$cmd = "$ffmpeg -ss $second -i  $s3viewdir $img_path -r 1 -y -s $size -vframes 1 -an -vcodec mjpeg";
            if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf $scale $img_path -an -vcodec mjpeg 2>&1";
                $cmd_thumb = "$ffmpeg -ss $second -i  $s3viewdir -vf $scale_thumb $img_path_thumb -an -vcodec mjpeg 2>&1";
            } else {
                $cmd = "sudo $ffmpeg -y -i $s3viewdir -frames 1 -q:v 1 -vf $scale $img_path -an -vcodec mjpeg 2>&1";
                $cmd_thumb = "sudo $ffmpeg -y -i $s3viewdir -frames 1 -q:v 1 -vf $scale_thumb $img_path_thumb -an -vcodec mjpeg 2>&1";
            }

            exec($cmd, $output, $return);
            exec($cmd_thumb, $output_thumb, $return_thumb);           
            $dest_file = $unsignedBucketPath . 'uploads/' . $studio_id . '/' . $imgname;
            
//            if((HOST_IP == '52.211.41.107' || HOST_IP == '52.0.232.150') && ($studio_id == 3067 || $studio_id == 4748)){            
//                echo $cmd.' and '.$cmd_thumb;
//                exit;
//            }

            require_once "Image.class.php";
            require_once "Config.class.php";
            require_once "Uploader.class.php";
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once "amazon_sdk/sdk.class.php";
            spl_autoload_register(array('YiiBase', 'autoload'));

            define("BASEPATH", dirname(__FILE__) . "/..");
            $config = Config::getInstance();
            $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . "/images/videobanners"); //path for images uploaded$bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucketName = $bucketInfoForLogo['bucket_name'];
            $config->setBucketName($bucketName);
            $s3_config = Yii::app()->common->getS3Details($studio_id);
            $config->setS3Key($s3_config['key']);
            $config->setS3Secret($s3_config['secret']);
            $config->setAmount(250);  //maximum paralell uploads
            $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
            $config->setDimensions(array('studio_thumb' => $thumb_size, 'original' => $size));   //resize to these sizes

            if(file_exists($img_path)){
            $uploader = new Uploader($studio_id);
                $ret = $uploader->uploadFiletoS3($bucketName, $unsignedBucketPath . 'public/uploads/videobanner-image/', $imgname, $studio_id, $bnr->id, $img_path);
            
                if(is_array($ret) && $ret['original'] != '')
                {
                    $placeholder_path = $ret['original'];
                    $placeholder_thumb_path = $ret['studio_thumb'];
                }
                else
                {
            $bucketCloudfrontUrlForPoster = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                    $placeholder_thumb_path = $placeholder_path = $bucketCloudfrontUrlForPoster . '/uploads/videobanner-image/' . $studio_id . '/' . $bnr->id . '/' . $imgname;
                }

            //Saving video poster
                $bnr->video_placeholder_img = $placeholder_path;
                $bnr->video_placeholder_thumb_img = $placeholder_thumb_path;
                $bnr->save();
            }
            // $fullPath = $bucket . '/' . $s3dir . $_REQUEST['filename'];
            //Yii::app()->general->addvideoToVideoGallery($_REQUEST['filename'], $fullPath, $studio_id);
        } else {
            $arr['msg'] = "data not found";
        }

        echo json_encode($arr);
        exit;
    }

    function addVideoBanner($section_id) {
        $section_id = $_REQUEST['otherInfo']['section_id'];
        $studio_id = Yii::app()->common->getStudiosId();
        if ($studio_id > 0) {
            $bnr = StudioBanner::model()->findByAttributes(array('studio_id' => $studio_id, 'banner_section' => $section_id));
            if ($bnr) {
                $bnr = $bnr->attributes;
                return $bnr['id'];
            } else {
                $ip_address = CHttpRequest::getUserHostAddress();
                $studiobnr = new StudioBanner;
                $id_seq = $studiobnr->getMaxOrd() + 1;
                $studio_banner = new StudioBanner;
                $studio_banner->title = htmlspecialchars($title);
                $studio_banner->image_name = $_REQUEST['otherInfo']['movie_name'];
                $studio_banner->banner_type = 0;
                $studio_banner->id_seq = $id_seq;
                $studio_banner->banner_section = $section_id;
                $studio_banner->is_published = 1;
                $studio_banner->studio_id = $studio_id;
                $studio_banner->created_date = new CDbExpression("NOW()");
                $studio_banner->ip = $ip_address;
                $studio_banner->save();
                return $studio_banner->id;
            }
        } else {
            return false;
        }
    }

    public function actionPlayVideobanner() {
        $studio_id = Yii::app()->common->getStudiosId();
        $banner_id = $_REQUEST['banner_id'];
        $sql = "SELECT video_remote_url FROM studio_banner WHERE id={$banner_id} AND studio_id={$studio_id}";
        $banner_video_url = Yii::app()->db->createCommand($sql)->queryRow();
        $this->renderPartial('playvideobanner', array('video_url' => $banner_video_url));
    }

    /**
     * @method public addVideoFromVideoGallery() Adds the video from video gallery for the video banner
     * @return json
     * @author RKS<support@muvi.com>
     */
    public function actionaddVideoFromVideoGallery() {
        if (isset($_REQUEST['url']) && $_REQUEST['galleryId']) {
            $studioId = $studio_id = Yii::app()->user->studio_id;
            $url = @$_REQUEST['url'];
            $galleryId = @$_REQUEST['galleryId'];
            $section_id = @$_REQUEST['movie_id'];
            if ($url != '' && intval($galleryId)) {
                $url = str_replace(' ', '%20', $url);
                $_REQUEST['url'] = $url;
                $tModel = new ThirdpartyServerAccess();
                $arr = $this->is_url_exist($url);
                $path_parts = pathinfo($url);
                $ext = $path_parts['extension'];
                $video_name = $path_parts['basename'];
                $imgname = $path_parts['filename'] . '.jpg';

                if (@$arr['error'] != 1 && @$arr['is_video'] && strtolower($ext) == 'mp4') {
                    $dir = Yii::app()->custom->videoBannerImagePathFormat($studioId);

                    if (!is_dir($dir)) {
                        mkdir($dir);
                        @chmod($dir, 0777);
                    }
                    $img_path = $dir . '/' . $imgname;
                    $video_placeholder_img = $this->mapImageFromVideoGallery($url, $img_path, $section_id);
                    $s3 = Yii::app()->common->connectToAwsS3($studioId);
                    $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                    $bucket = $bucketInfo['bucket_name'];
                    $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                    $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                    $bannerVideoUrl = $unsignedFolderPathForVideo . Yii::app()->custom->videoBannerServerPathFormat() . $studio_id . "/" . $video_name;
                    $source_path = $unsignedFolderPathForVideo . 'videogallery/' . $video_name;
                    $response = $s3->copyObject(array(
                        'Bucket' => $bucket,
                        'Key' => $bannerVideoUrl,
                        'CopySource' => "{$bucket}/{$source_path}",
                        'ACL' => 'public-read',
                    ));

                    $bnr = StudioBanner::model()->findByAttributes(array('studio_id' => $studio_id, 'banner_section' => $section_id));
                    if ($bnr) {
                        $bucketCloudfrontUrl = CDN_HTTP . $bucketInfo['videoRemoteUrl'];
                        $bnr->image_name = $img_path;
                        $s3viewdir = $arr['banner_url'] = $bnr->video_remote_url = $bucketCloudfrontUrl . '/' . $bannerVideoUrl;
                        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                            $command = $ffmpeg . ' -i ' . $s3viewdir . ' -vstats 2>&1';
                        } else {
                            $command = 'sudo ' . $ffmpeg . ' -i ' . $s3viewdir . ' -vstats 2>&1';
                        }
                        $output = shell_exec($command);
                        $video_prop = Yii::app()->common->get_videoproperties($output, end(explode('.', $s3viewdir)));
                        $head = array_change_key_case(get_headers($s3viewdir, TRUE));
                        $filesize1 = $head['content-length'];
                        $filesize = Yii::app()->common->bytes2English($filesize1);
                        $video_prop['FileSize'] = $filesize;
                        $video_properties = json_encode($video_prop);
                        $bnr->video_properties = $video_properties;
                        $bnr->duration = $video_prop['duration'];
                        $bnr->file_size = $filesize;
                        $bnr->video_placeholder_img = $video_placeholder_img;
                        $bnr->save();
                        $arr['msg'] = "Banner uploaded successfully";
                        $arr['is_video'] = 'uploaded';
                        Yii::app()->user->setFlash('success', 'Banner uploaded successfully');
                    }
                }
            }
            echo json_encode($arr);
            exit;
        }
    }

    /**
     * @method public mapImageFromVideoGallery() Maps the Image from the video file
     * @return boolen true/false
     * @author RKS<support@muvi.com>
     */
    function mapImageFromVideoGallery($s3viewdir, $img_path, $item_id) {
        $studio_id = Yii::app()->user->studio_id;
        $ffmpeg = FFMPEG_PATH;
        $second = 1;
        $cloudfront_url = '';
        //screenshot size
        $size = '1600x600';
        $scale = 'scale=1600:600';

        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $cmd = "$ffmpeg -ss $second -i $s3viewdir -vf scale=1600:600 $img_path -an -vcodec mjpeg 2>&1";
        } else {
            $cmd = "sudo $ffmpeg -ss $second -i $s3viewdir -vf scale=1600:600 $img_path -an -vcodec mjpeg 2>&1";
        }
        exec($cmd, $output, $return);

        if ($return == 1) {
            $src_path = $img_path;
            $path_parts = pathinfo($img_path);
            $imgname = $path_parts['basename'];
            $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];

            require_once "Image.class.php";
            require_once "Config.class.php";
            require_once "Uploader.class.php";
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once "amazon_sdk/sdk.class.php";
            spl_autoload_register(array('YiiBase', 'autoload'));

            define("BASEPATH", dirname(__FILE__) . "/..");
            $config = Config::getInstance();
            $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . "/images/videobanners"); //path for images uploaded$bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucketName = $bucketInfoForLogo['bucket_name'];
            $config->setBucketName($bucketName);
            $s3_config = Yii::app()->common->getS3Details($studio_id);
            $config->setS3Key($s3_config['key']);
            $config->setS3Secret($s3_config['secret']);
            $config->setAmount(250);  //maximum paralell uploads
            $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
            $config->setDimensions(array('studio_thumb' => '1600x600'));   //resize to these sizes
            $uploader = new Uploader($studio_id);
            $ret = $uploader->uploadFiletoS3($bucketName, $unsignedBucketPath . Yii::app()->custom->videoBannerImageServerPathFormat(), $imgname, $studio_id, $item_id);
            if (count($ret) > 0) {
                foreach ($ret as $t) {
                    $cloudfront_url = $t;
                }
            }
        }
        return $cloudfront_url;
    }

    public function actiontinyGallery() {
        $studio_id = Yii::app()->user->studio_id;
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $this->renderPartial('image_gallery', array('all_images' => $all_images, 'base_cloud_url' => $base_cloud_url));
    }

    public function actionimageGallery() {
        $studio_id = Yii::app()->user->studio_id;
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $this->renderPartial('image_gallery_with_crop', array('all_images' => $all_images, 'base_cloud_url' => $base_cloud_url));
    }

    public function actionEditor() {
        $this->breadcrumbs = array('Website', 'Template', 'Template Editor');
        $this->pageTitle = Yii::app()->name . ' | ' . 'Template Editor';
        $this->headerinfo = "Template Editor";

        $studio_id = Yii::app()->user->studio_id;
        $themefolder = $this->studio->theme;
        $sourcePath = 'themes/' . $themefolder . '/';

        $view_folders = Yii::app()->general->dirToArray($sourcePath);
        $this->render('editor', array('view_folders' => $view_folders));
    }

    public function actionEditorcontent() {
        $error = 1;
        $message = 'Please select a correct path.';
        $themefolder = $this->studio->theme;
        $sourcePath = 'themes/' . $themefolder . '/';
        if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'original' && isset($_REQUEST['filepath']) && $_REQUEST['filepath'] != '') {
            $themefolder = $this->studio->parent_theme;
            $sourcePath = 'sdkThemes/' . $themefolder . '/';
            $filepath = $sourcePath . urldecode($_REQUEST['filepath']);
            $message = file_get_contents($filepath);
            $error = 0;
        } else if (isset($_REQUEST['filepath']) && $_REQUEST['filepath'] != '') {
            $filepath = $sourcePath . urldecode($_REQUEST['filepath']);
            $message = file_get_contents($filepath);
            $error = 0;
        }
        $ret = array('error' => $error, 'message' => $message);
        echo json_encode($ret);
        exit();
    }

    public function actionRemovefile() {
        $error = 1;
        $message = 'Please select a correct path.';
        $themefolder = $this->studio->theme;
        $sourcePath = 'themes/' . $themefolder . '/';
        if (isset($_REQUEST['filepath']) && $_REQUEST['filepath'] != '') {
            $filepath = $sourcePath . urldecode($_REQUEST['filepath']);
            $deleted = @unlink($filepath);
            if ($deleted) {
                $message = 'File deleted successfully.';
                Yii::app()->user->setFlash('success', $message);
                $error = 0;
            }
        }
        $ret = array('error' => $error, 'message' => $message);
        echo json_encode($ret);
        exit();
    }

    public function actionSaveeditorcontent() {
        $error = 1;
        $message = 'Please select a correct path.';
        $themefolder = $this->studio->theme;
        $sourcePath = 'themes/' . $themefolder . '/';
        if (isset($_REQUEST['filepath']) && $_REQUEST['filepath'] != '') {
            $filepath = $sourcePath . urldecode($_REQUEST['filepath']);
            $new_value = $_REQUEST['editor_content'];
            file_put_contents($filepath, $new_value);
            $message = 'Content updated successfully.';
            $error = 0;
        }
        $ret = array('error' => $error, 'message' => $message);
        echo json_encode($ret);
        exit();
    }

    public function generate_timezone_list() {

        $timezones = array();

        $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);


        $timezone_offsets = array();
        foreach ($timezones as $timezone) {
            $tz = new DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
        }

        // sort timezone by offset
        krsort($timezone_offsets); //descending order of keys

        $timezone_list = array();
        foreach ($timezone_offsets as $timezone => $offset) {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate('H:i', abs($offset));

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }

        return $timezone_list;
    }

    public function convert_to_utc($localtime, $local_zone) {
        $exist = new DateTimeZone($local_zone);
        $tz = new DateTimeZone('UTC'); // or whatever zone you're after
        $dt = new DateTime($localtime, $exist);
        $dt->setTimezone($tz);
        return $dt->format('Y-m-d H:i:s');
    }

    public function convert_to_local($utctime, $local_zone) {
        //$local_start_time='2016-07-19 15:00:00';
        $exist = new DateTimeZone('UTC');
        $tz = new DateTimeZone($local_zone); // or whatever zone you're after
        $dt = new DateTime($utctime, $exist);
        $dt->setTimezone($tz);
        return $dt->format('Y-m-d H:i:s');
    }

    public function actioncheckCurrentThemes() {
        if (@isset($_POST['theme_code'])) {
            $theme_code = $_POST['theme_code'];
            $studio_theme = $this->studio->parent_theme;
            $studio_draft_theme = $this->studio->preview_parent_theme;
            $msg = false;
            if ($theme_code == $studio_theme) {
                $msg = "You are currently using this template!";
            } elseif ($theme_code == $studio_draft_theme) {
                $msg = "You are currently using this template as draft!";
            }
            echo $msg;
            exit;
        } else {
            $url = $this->createUrl('/admin/dashboard');
            $this->redirect($url);
        }
    }
  public function actionBannerReorder() {
        $parts         = explode('=', $_REQUEST['order']);
        $banner_id     = (int) $parts[1];
        $device_banner = @$_REQUEST['apphome'];
        $pos           = 0;
        if($device_banner){
            $banner = new AppBanners;
            $banner = $banner->findByPk($banner_id);
            $banner->id_seq = $pos;
            $banner->save();
            foreach ($_REQUEST['sortable'] as $position => $bnr_id) {
                $pos  = $position + 1;
                $feat = new AppBanners;
                $feat = $feat->findByPk($bnr_id);
                $feat->id_seq = $pos;
                $feat->save();
            }    
        }else{
            $banner = new StudioBanner;
            $banner = $banner->findByPk($banner_id);
            $banner->id_seq = $pos;
            $banner->save();
            foreach ($_REQUEST['sortable'] as $position => $bnr_id) {
                $pos  = $position + 1;
                $feat = new StudioBanner;
                $feat = $feat->findByPk($bnr_id);
                $feat->id_seq = $pos;
                $feat->save();
            }  
        }
        echo "success";
        exit;
    }

    public function actionupdatehomepagelayout() {
        $this->layout = false;
        $studio_id = $this->studio->id;
        if (isset($_POST) && count($_POST) > 0 && isset($_POST['layout_type']) && $_POST['layout_type'] != '') {

            $getStudioConfig = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'homepage_layout'));
            if ($getStudioConfig) {
                $getStudioConfig->config_value = $_POST['layout_type'];
                $getStudioConfig->save();
            } else {
                $studioConfig = new StudioConfig();
                $studioConfig->studio_id = $studio_id;
                $studioConfig->config_key = 'homepage_layout';
                $studioConfig->config_value = $_POST['layout_type'];
                $studioConfig->save();
            }
            Yii::app()->user->setFlash('success', 'Settings saved successfully.');
        }
        $this->redirect($this->createUrl('/template/homepage'));
        exit();
    }

    function sendAppEmail($studio,$app_type) {
        $params = array(
                'name' => 'Admin',
                'app_type' => $app_type,
                'studio_name' => $studio->subdomain
            );
            $useremail = 'studio@muvi.com';
            $useremail = 'rasmi@muvi.com';
            $username = 'Muvi';
            $subject = $app_type.' app update request';
            $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
            $fromName = $studio->name ? $studio->name : Yii::app()->user->first_name . " " . Yii::app()->user->last_name;
            $message = array('subject' => $subject,
                'from_email' => $fromEmail,
                'from_name' => $fromName,
                'to' => array(
                    array(
                        'email' => $useremail,
                        'name' => $username,
                        'type' => 'to'
                    ),
                    array(
                        'email' => 'help@muvi.com',
                        'name' => $username,
                        'type' => 'cc'
                    )
                )
            );

            $to = array($useremail);
            $cc = array('help@muvi.com','satya@muvi.com','subhalaxmi@muvi.com');

            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/new_mobile_app_update', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $fromEmail, $cc,'','',$fromName);
            return $returnVal;
    }
	public function actionDefaultBannerStyle() {
		$this->layout = false;
		$studio_id = Yii::app()->common->getStudiosId();
		$parent_theme = $this->studio->parent_theme;
		$template_id = 0;
		$temps = new Template;
		$temps = $temps->findAllByAttributes(array('code' => $parent_theme), array('order' => 'id ASC'));
		if (count($temps) > 0)
			$template_id = $temps[0]->id;
		$sc = new BannerSection;
		$sections = $sc->findAllByAttributes(array('template_id' => $template_id, 'studio_id' => 0,'is_new'=>0), array('order' => 'id ASC', 'limit' => 1));
		$banner_code = isset($_REQUEST['banner_code']) ? $_REQUEST['banner_code'] : 'style_1';
		$section = Yii::app()->db->createCommand()
			->select("*")
			->from('studio_banner_style')
			->where('studio_id=:studio_id', array(':studio_id' => $studio_id))
			->queryRow();
        if ($section != '' && $parent_theme == "traditional-byod"){
            $sections = $sc->findAllByAttributes(array('template_id' => $template_id, 'studio_id' => 0,'is_new'=>1));
        }
		if (empty($section) || $_REQUEST['banner_code'] != '') {
			$data = Yii::app()->db->createCommand()
				->select("*")
				->from('banner_style')
				->where('banner_style_code=:banner_style_code', array(':banner_style_code' => $banner_code))
				->queryRow();
			$status = "change";
			$previous_data =$section;
		} else {
			$data = $section;
			$status = "update";
			$previous_data = '';
		}
		$this->render('bannerstyle', array('studio_id' => $studio_id, 'bannerstyle' => $data, 'sections' => $sections, 'status' => $status, 'existing_style' => $previous_data));
	}
	public function actionSaveBannerSetting(){
        $studio_id = Yii::app()->common->getStudioId();
        $studio = $this->studio;
        $theme = $studio->theme;
         if (isset($_REQUEST)) {
             $banner_code=$_REQUEST['banner_code'];
             $h_size=$_REQUEST['h_size'];
             $active_image=$_REQUEST['thumb_visible'];
             $v_size=$_REQUEST['v_size'];
             $auto_scroll=$_REQUEST['auto_scroll'];
             $scroll_interval=$_REQUEST['scroll_interval'];
			 $section = StudioBannerStyle::model()->findByAttributes(array('studio_id' => $studio_id));
             if (!empty($section)) {
                if (($section['h_banner_dimension'] != $h_size) || ($section['v_banner_dimension'] != $v_size)){
                    if (($section['h_banner_dimension'] != $h_size) || ($section['v_banner_dimension'] != $v_size)){
						$bn = new StudioBanner;
						$bn->deleteAll('studio_id = :studio_id', array(
							':studio_id' => $studio_id,
						));
						$bnr = new StudioBannerProperties();
						$bnr->deleteAll('studio_id = :studio_id', array(
							':studio_id' => $studio_id,
						));
					}
                }
                $section->banner_style_code	 = $banner_code;
                $section->h_banner_dimension = $h_size;
                $section->v_banner_dimension = $v_size;
                $section->auto_scroll        = $auto_scroll;
                $section->scroll_interval	 = $scroll_interval;
                $section->no_of_active_image = $active_image;
                $arr['succ'] = 1;
                $arr['msg'] = 'Banner Style is updated successfully.';
                Yii::app()->user->setFlash('success', 'Banner Style is updated successfully.');
            } else {
				$section = new StudioBannerStyle();
                $section->studio_id          = $studio_id;
                $section->banner_style_code  = $banner_code;
                $section->h_banner_dimension = $h_size;
                $section->v_banner_dimension = $v_size;
                $section->auto_scroll		 = $auto_scroll;
                $section->scroll_interval	 = $scroll_interval;
                $section->no_of_active_image = $active_image;
                Yii::app()->user->setFlash('success', 'Banner Style Saved.');
            }
		$section->save();
		$jsfile = $banner_code."/banner.html";
		$jsroot = ROOT_DIR . 'bannerStyle/'.$jsfile;
		$jspath = ROOT_DIR . 'themes/' . $theme . '/views/layouts/banner.html';
		copy($jsroot, $jspath);
        }
        $url = $this->createUrl('/template/homepage');
        $this->redirect($url);
        exit;
    }
    public function actionBannerProperties() {
		$this->layout = false;
		$studio_id = Yii::app()->common->getStudiosId();
		$banner_id = $_REQUEST['banner_id'];
		$section = StudioBannerProperties::model()->findByAttributes(array('studio_id' => $studio_id, 'banner_id' => $banner_id));
		if ($_REQUEST['save'] == "save") {
			if ($_REQUEST['is_advance'] == 0) {
				if (!empty($section)) {
					$section->banner_text_heading = $_REQUEST['text_heading'];
					$section->banner_text_description = $_REQUEST['text_description'];
					$section->banner_text_position = '';
					$section->banner_button_text = $_REQUEST['button_text'];
					$section->banner_button_type = $_REQUEST['button_link'];
					$section->banner_button_link = $_REQUEST['button_link_url'];
					$section->banner_url = $_REQUEST['url_text'];
					$section->is_advance = 0;
					$section->advance_content = '';
					Yii::app()->user->setFlash('success', 'Banner  properties updated successfully.');
				} else {
					$section = new StudioBannerProperties();
					$section->studio_id = $studio_id;
					$section->banner_id = $banner_id;
					$section->banner_text_heading = $_REQUEST['text_heading'];
					$section->banner_text_description = $_REQUEST['text_description'];
					$section->banner_text_position = '';
					$section->banner_button_text = $_REQUEST['button_text'];
					$section->banner_button_type = $_REQUEST['button_link'];
					$section->banner_button_link = $_REQUEST['button_link_url'];
					$section->banner_url = $_REQUEST['url_text'];
					$section->is_advance = 0;
					$section->advance_content = '';
					Yii::app()->user->setFlash('success', 'Banner properties Saved.');
				}
			} else {
				if (!empty($section)) {
					$section->banner_text_heading = '';
					$section->banner_text_description = '';
					$section->banner_text_position = '';
					$section->banner_button_text = '';
					$section->banner_button_type = '';
					$section->banner_button_link = '';
					$section->banner_url = '';
					$section->is_advance = 1;
					$section->advance_content = $_REQUEST['text_advance'];
					Yii::app()->user->setFlash('success', 'Banner  properties updated successfully.');
				} else {
					$section = new StudioBannerProperties();
					$section->studio_id = $studio_id;
					$section->banner_id = $banner_id;
					$section->banner_text_heading = $_REQUEST['text_heading'];
					$section->banner_text_description = $_REQUEST['text_description'];
					$section->banner_text_position = '';
					$section->banner_button_text = $_REQUEST['button_text'];
					$section->banner_button_type = $_REQUEST['button_link'];
					$section->banner_button_link = $_REQUEST['button_link_url'];
					$section->banner_url = $_REQUEST['url_text'];
					$section->is_advance = 1;
					$section->advance_content = $_REQUEST['text_advance'];
					Yii::app()->user->setFlash('success', 'Banner properties saved successfully.');
				}
			}
			$section->save();
			$url = $this->createUrl('/template/homepage');
			$this->redirect($url);
			exit;
		}
		$result = $section;
		$this->render('bannerproperties', array('studio_id' => $studio_id, 'banner' => $result, 'banner_id' => $banner_id));
	}

    public function actionNotificationCenter() {
        $this->breadcrumbs = array('Mobile & TV Apps', 'App Notification center');
        $this->headerinfo = "App Notification center";
        $studio_id = Yii::app()->common->getStudioId();
        $studio = $this->studio;
		$this->pageTitle = Yii::app()->name . ' | ' . 'App Notification center';
        $json = '';
        $response = '';
        $message_notification=$_REQUEST['message'];
        $limit = Yii::app()->custom->getConfigValueForStudio($studio_id);
        $createdDate = date('Y-m-d');
        $total_send = NotificationCenter::model()->findAllByAttributes(array('studio_id' => $studio_id, 'created_date' => $createdDate));
        $total = count($total_send) + 1;
        $action = $_REQUEST['action'];
        if ($action == "send") {
            if ($total <= $limit) {
                $push = new Push();
                $device_and = '1';
                $device_ios = '2';
                $sql = Yii::app()->db->createCommand()
                    ->select('fcm_token')
                    ->from('register_device r')
                    ->where('r.studio_id=' . $studio_id . ' AND device_type = ' . $device_and . '');
                $registration_ids = $sql->QueryAll();
                $sql2 = Yii::app()->db->createCommand()
                    ->select('fcm_token')
                    ->from('register_device r')
                    ->where('r.studio_id=' . $studio_id . ' AND device_type = ' . $device_ios . '');
                $registration_ids2 = $sql2->QueryAll();
                $reg_ids2 = array();
                $reg_ids = array();
                if (!empty($registration_ids)) {
                    foreach ($registration_ids as $ids) {
                        $reg_ids [] = $ids['fcm_token'];
                    }
                }
                if (!empty($registration_ids2)) {
                    foreach ($registration_ids2 as $ids2) {
                        $reg_ids2 [] = $ids2['fcm_token'];
                    }
                }
                $title = "Notification";
                $message = $message_notification;
                $push->setTitle($title);
                $push->setMessage($message);
                //json data android
                $jsondata = $push->getPushAndroid();
                //json data ios
                $jsonda = $push->getPushIos();
                if (!empty($registration_ids)) {
                    $response = $push->sendMultiple($reg_ids, $jsondata);
                }
                if (!empty($registration_ids2)) {
                    $response2 = $push->sendMultipleNotify($reg_ids2, $jsonda);
                }

                $messageAndroid = json_decode($response, true);
                $messageAnd = $messageAndroid['results'];
                $messageIos = json_decode($response2, true);
                $messageIo = $messageIos['results'];
                $notification = new NotificationCenter;
                $notification->studio_id = $studio_id;
                $notification->notification_message = $message_notification;
                $notification->status = '1';
                $notification->created_date = $createdDate;
                $notification->save();
                $command1 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('register_device r')
                    ->where('r.studio_id=' . $studio_id . ' AND device_type = ' . $device_and . '');
                $total_register_android = $command1->QueryAll();
                $command2 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('register_device r')
                    ->where('r.studio_id=' . $studio_id . ' AND device_type = ' . $device_ios . '');
                $total_register_ios = $command2->QueryAll();
                $i = 0;
                foreach ($total_register_android as $device) {
                    $pushlog = new PushNotificationLog;
                    $pushlog->studio_id = $studio_id;
                    $pushlog->device_id = $device['device_id'];
                    $pushlog->message = $message_notification;
                    $pushlog->device_type = $device['device_type'];
                    if ($messageAnd[$i]['message_id'] == '') {
                        $pushlog->status = '0';
                    } else {
                        $pushlog->status = '1';
                    }
                    $pushlog->message_unique_id = $messageAnd[$i]['message_id'];
                    $pushlog->created_date = $createdDate;
                    $pushlog->fcm_token = $device['fcm_token'];
                    $pushlog->save();
                    $i++;
                }
                $j=0;
                foreach ($total_register_ios as $device){
                   $pushlog= new PushNotificationLog;
                   $pushlog->studio_id = $studio_id;
                   $pushlog->device_id = $device['device_id'];
                   $pushlog->message = $message_notification;
                   $pushlog->device_type = $device['device_type'];
                   if($messageIo[$j]['message_id'] == ''){
                   $pushlog->status = '0';
                   }else{
                   $pushlog->status = '1';
                   }
                   $pushlog->message_unique_id = $messageIo[$j]['message_id'];
                   $pushlog->created_date = $createdDate;
                   $pushlog->fcm_token = $device['fcm_token'];
                   $pushlog->save();
                   $j++;
                }
                // echo $jsondata;echo $jsonda;  echo $response2;echo $response;exit;
                $msg = 'Notification Send Successfully';
                $data = array("status" => "success", "message" => $msg);
                $retrun = json_encode($data);
                echo $retrun;
                exit;
            } else {
                if ($limit > 1) {
                    $s = 's';
                }
                $msg = "You have exceeded the allowed limit.You can send {$limit} notification{$s} per day";
                $array = array("status" => "failure", "message" => $msg);
                $retrun = json_encode($array);
                echo $retrun;
                exit;
            }
        }
        $this->render('notificationcenter', array('studio_id' => $studio_id));
    }
    /*
     * @author: RK<developer@muvi.com>
     * @description: Copy all new files and folders from theme_updates to all templates
     */
    public function actionUpdateTemplate(){
        $this->layout = false;  
        $message = '';
        $criteria = new CDbCriteria;
        $criteria->addCondition("date_added > NOW() AND status = 0");
        $updates = TemplateUpdate::model()->findAll($criteria);            
        foreach($updates as $update){                
            $cond = 't.parent_theme = "'.$update->template_code.'" AND ';
            $cond .= "t.theme != '' AND t.is_deleted = 0 AND t.status = 1 AND t.is_default = 0 AND t.reseller_id = 0";
            if($update->studio_id > 0)
                $cond .= ' AND t.id = '.$update->studio_id;
            $criteria = new CDbCriteria;
            $criteria->select = 'parent_theme, theme, id, domain'; // select fields which you want in output
            $criteria->condition = $cond;
            $criteria->order = 'id asc';
            $studios = Studios::model()->findAll($criteria);       
            echo 'Total Updated:'.count($studios).'<br />';
            foreach($studios as $studio){
                echo 'Studio:'.$studio->name.', theme:'.$studio->theme.', parent:'.$studio->parent_theme.'<br />';
                $themes[] = $studio->theme;
                $source = ROOT_DIR . '/theme_updates/' . $studio->parent_theme . '/';
                $sourcePath = 'themes/' . $studio->theme . '/';
                $ret = Yii::app()->common->recurse_copy($source, $sourcePath);  
                $message.= 'Theme '.$studio->theme.' updated.<br />';
            }
            $update->status = 1;
            $update->release_id = $update->release_id+1;
            $update->date_executed = new CDbExpression("NOW()");
            $update->save();
        }
        echo $message;
        exit;
    }
}
