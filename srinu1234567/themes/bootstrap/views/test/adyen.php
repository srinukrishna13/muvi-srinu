<script   src="https://code.jquery.com/jquery-3.1.0.min.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/headers/header-default.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/animate.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/font-awesome/css/font-awesome.min.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/theme-colors/default.css" id="style_color">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/theme-skins/dark.css">

<style>
    .form-control {
        display: block;
        width: 100%;
        height: 34px !important;
        padding: 6px 12px !important;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        margin-bottom: 0 !important;
    }
</style>

<br />




<div class="panel panel-blue margin-bottom-40" style="width: 600px;margin: 10px auto;">
        <div class="panel-heading">
                <h3 class="panel-title">Lumiere: adyen</h3>
        </div>
        <div class="panel-body">
                <?php if(isset($_REQUEST['authResult']) && trim($_REQUEST['authResult'] == 'AUTHORISED')){?>
                        <div class="form-group">
                            <span id="success" style="color:greenyellow"><b>Transaction ID: <?php echo $_REQUEST['pspReference'];?></b></span>
                        </div>
                <?php }?>
                        <div class="form-group">
                                <label for="exampleInputEmail1" style="color: red">All fields are compulsory</label>
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="email" class="form-control" id="name" value="Test User">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id="email" value="test@test.com">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Amount ($)</label>
                                <input type="email" class="form-control" id="price" value="10">
                        </div>
                        
                        <button type="button" id="pay" class="btn-u btn-u-blue">Submit</button>
        </div>
</div>
<form action="https://test.adyen.com/hpp/pay.shtml"  name="payment_form" id='payment_form' method="post">
	
</form>

<script>
$("#pay").click(function(){
    
    var price = $('#price').val();
    if(price){
        $('#pay').html('wait...').attr('disabled', true);
        $.post("adyenPayment", {price:price}, function(data){
            res = JSON.parse(data);
            if (res) {
                for (var i in res) {
                    $("#payment_form").append("<input type='hidden' name='" + i + "' value='" + res[i] + "' />");
                }
            }
            
            setTimeout(function () {
                document.payment_form.action = "https://test.adyen.com/hpp/pay.shtml";
                document.payment_form.submit();
                return false;
            }, 5000);
            
        });
                
    }else{
        alert('Please enter a valid amount');
    }
});
</script>