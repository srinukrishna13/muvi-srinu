<div class="container">
    <div class="row contact_us">
        <div class="span12">
            <h2 class="btm-bdr">Examples</h2>

            <div class="row">
                <div class="span6">
                    <p><a href="https://itunes.apple.com/us/app/iskcon-now/id1031820581?ls=1&mt=8" target="_blank" ><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/iskcon-ios.jpg" alt="ISKCON iOS App" title="ISKCON iOS App" /></a></p>
                    <h4 class="center"><a href="https://itunes.apple.com/us/app/iskcon-now/id1031820581?ls=1&mt=8" target="_blank" >ISKCON iOS App</a></h4>
                    <p class="center">iOS app for ISKCON, a religious organization</p>
                    <br />
                </div>  
                <div class="span6">
                    <p><a href="#" ><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/iskcon-roku.jpg" alt="ISKCON Roku App" title="ISKCON Roku App" /></a></p>
                    <h4 class="center"><a href="#" >ISKCON Roku App</a></h4>
                    <p class="center">Roku private channel for ISKCON, a religious organization</p>
                    <br />
                </div>
            </div>  
            <p>&nbsp;</p>
            <div class="row">

                <div class="span6">
                    <p><a href="http://www.cinetropa.com" target="_blank" ><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/cinetropa.png" alt="Cinetropa: Movie VOD in Philippines" title="Cinetropa: Movie VOD in Philippines" /></a></p>
                    <h4 class="center"><a href="http://www.cinetropa.com" target="_blank" >Cinetropa: Movie VOD in Philippines</a></h4>
                    <p class="center">Premium pay-per-view movie VOD website in Philippines</p>
                    <br />
                </div>
                <div class="span6">
                    <p><a href="https://play.google.com/store/apps/details?id=com.muvi.muvistudio&hl=en" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/iskcon-androidapp.jpg" alt="ISKCON Android App" title="ISKCON Android App" /></a></p>
                    <h4 class="center"><a href="https://play.google.com/store/apps/details?id=com.muvi.muvistudio&hl=en" target="_blank">ISKCON Android App</a></h4>
                    <p class="center">Android app for ISKCON, a religious organization</p>
                    <br />
                </div>
            </div>  
            <p>&nbsp;</p>

            <div class="row">

                <div class="span6">
                    <p><a href="http://www.chitagade.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/ways-tv.jpg" alt="Chitagade" title="Chitagade" /></a></p>
                    <h4 class="center"><a href="http://www.chitagade.com" target="_blank">www.chitagade.com</a></h4>
                    <p class="center">WaysTV: Local TV channel in Florida, USA</p>
                    <br />
                </div>
                <div class="span6">
                    <p><a href="http://www.htvfun.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/htv-fun.jpg" alt="HTVFun.com" title="HTVFun.com" /></a></p>
                    <h4 class="center"><a href="http://www.htvfun.com" target="_blank">HTVFun.com</a></h4>
                    <p class="center">HTV: Leading media company in Philippines</p>
                    <br />
                </div>

            </div>             
            <p>&nbsp;</p>
            <div class="row">

                <div class="span6">
                    <p><a href="http://www.maaflix.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/maaflix.jpg" alt="Maaflix" /></a></p>
                    <h4 class="center"><a href="http://www.maaflix.com" target="_blank">www.maaflix.com</a></h4>
                    <p class="center">VOD platform of largest south Indian TV channel</p>
                    <br />
                </div>
                <div class="span6">
                    <p><a href="http://www.tornerannoiprati.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/i-prati.jpg" alt="Tornerannoiprati" title="Tornerannoiprati" /></a></p>
                    <h4 class="center"><a href="http://www.tornerannoiprati.com" target="_blank">www.tornerannoiprati.com</a></h4>
                    <p class="center">Single pay-per-view movie website</p>
                    <br />
                </div> 
            </div>              
            <p>&nbsp;</p>
            <div class="row">

                <div class="span6">
                    <p><a href="http://www.rispolicescientifique.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/ris.jpg" alt="Rispolicescientifique" title="Rispolicescientifique" /></a></p>
                    <h4 class="center"><a href="http://www.rispolicescientifique.com" target="_blank">www.rispolicescientifique.com</a></h4>
                    <p class="center">Single TV series website, PPV and subscription</p>
                    <br />
                </div> 
                <div class="span6">
                    <p><a href="http://www.iskcontelevisionindia.com" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/examples/iskcon.jpg" alt="Iskcontelevisionindia" title="Iskcontelevisionindia" /></a></p>
                    <h4 class="center"><a href="http://www.iskcontelevisionindia.com" target="_blank">www.iskcontelevisionindia.com</a></h4>
                    <p class="center">VOD and live streaming for large religious organization</p>
                    <br />
                </div>				
            </div>


        </div>
    </div>
</div>