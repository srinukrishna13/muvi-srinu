<style>
    /* css :It's Re defined' */
.ui-widget-content{
    z-index: 1050 !important;
    height: 200px !important;
    overflow-y: scroll !important;
}
.ui-widget-content li:hover{
    background-color: #0081c2;
    background-image: linear-gradient(to bottom, #0088cc, #0077b3);
    background-repeat: repeat-x;
    color:white;
    text-decoration: none;
}
.ui-menu .ui-menu-item{
    padding: 5px;
}
.drag-cursor {
    list-style: outside none none;
}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.showsectionname').click(function () {
            $(this).parent().find('.editsection').removeClass('hide');
            $(this).parent().find('.editsection').addClass('show');
            $(this).addClass('hide');
        });
    });
</script>
<?php $totalContent = count($data); ?>
<div class="row m-b-40">
        <div class="col-xs-12">
		 <?php
        if ($theme->max_featured_sections == 0 || $theme->max_featured_sections > $totalContent) {
            ?>
            <button class="btn btn-primary m-t-10 p-l-40" id="nextbtn" onclick="addHomepageSection(); return false;"> Add Featured Section</button>
		<?php } ?>
         </div> 
    </div>
<?php
$studio = $this->studio;
$theme = Yii::app()->common->getTemplateDetails($studio->preview_parent_theme);
$totalContent = 0;

        if ($data) {
            $totalContent = count($data);
            $x = 1;
            ?>
        <script type="text/javascript">
                $(function () {
                    $('#sort_section').sortable({
                        //handle : '.handle', 
                        update: function () {
                            var order = $('#sort_section').sortable('serialize');
                            console.log(order);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/drafttemplate/sortfeaturedsection",
                                data: "order=" + $('#sort_section').sortable('serialize'),
                                dataType: "json",
                                cache: false,
                                success: function (data)
                                {
                                    console.log(data);
                                }
                            });
                        }
                    });
                });

            </script>  
            
    

<form class="form-horizontal" method="post">
                <input type="hidden" name="sec_id" id="sec_id" />
                
				<div class="row m-b-40" id="sort_section">
                                    
                    <?php
                    $k=0;
                    foreach ($data AS $key => $details) {
                        $section_id = $details['id'];
                        if (intval($details['parent_id'])) {
                            $featureds = Yii::app()->common->getFeaturedAllContents($details['parent_id']);
                        } else {
                            $featureds = Yii::app()->common->getFeaturedAllContents($section_id);
                        }
                    ?>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#collap-<?php echo $section_id; ?>').click(function () {
                                    $('#section_data_<?php echo $section_id; ?>').toggle();
                                });
                            });
                        </script>                    
                       
                            
			
                <div class="col-xs-12" id="sort_<?php echo $section_id; ?>" >
				
                    <div class="Collapse-Block" >
					
                        <div class="Block-Header has-right-icon">
                            <div class="icon-OuterArea--rectangular">
                                <a href="javascript:void(0);" id="collap-<?php echo $section_id; ?>"><em class="icon-arrow-up icon left-icon"></em></a>
                            </div>
                            <?php if (($language_id == $details['language_id']) && ($details['parent_id'] == 0)) { ?>
                                <div class="icon-OuterArea--rectangular right">
                                    <a href="javascript:void(0);" onclick="deleteSection(<?php echo $section_id; ?>)"><em class="icon-trash icon" title="Remove the Section"></em></a>
                                </div>
                            <?php } ?>
                           
                            
                            <div class="section_settings form-horizontal">
                                <div class="editsection hide form-group">
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <input type="text" class="sectionname form-control" value="<?php echo $details['title']; ?>" id="sectionname_<?php echo $section_id; ?>" name="sectionname_<?php echo $section_id; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="javascript:void(0);"  onclick="updateSection(<?php echo $section_id; ?>);return false;" title="Update"><i class="h3 green icon-check"></i></a>
                                    </div>
                                </div> 
                            </div>
                            <h4 class="showsectionname"><?php echo $details['title']; ?> </h4>
                            
                        </div>

                        <div class="Collapse-Content" id="section_data_<?php echo $section_id; ?>">
                                   <!--Add New Content-->
                                   
                                   
                                    <a href="#" class="btn btn-primary m-t-10 p-l-40" onclick="addPopularMovie(<?php echo $section_id ?>); return false;">
                                        Add Content
                                    </a>
                                  <?php if(!empty($featureds)){  ?>
                                    <div class="m-t-40 m-b-40"></div>
                                  <?php } ?>
                           
							
                            <ul class="row featured_contents" id="sortable_<?php echo $section_id; ?>" >
							
                                
									 <?php
                                                        foreach ($featureds as $featured) {
                                                            $is_episode = $featured->is_episode;
                                                            if ($is_episode == 1) {
                                                                $stream_id = $featured->movie_id;
                                                                $movie_stream = new movieStreams;
                                                                $movie_stream = $movie_stream->findByPk($stream_id);
                                                                $movie_id = $movie_stream->movie_id;

                                                                $film = new Film();
                                                                $film = $film->findByPk($movie_id);
                                                                $cont_name = $film->name . ' ';

                                                                $cont_name .= ($movie_stream->episode_title != '') ? $movie_stream->episode_title : "SEASON " . $movie_stream->series_number . ", EPISODE " . $movie_stream->episode_number;
                                                                $poster = $this->getPoster($stream_id, 'moviestream', 'episode');
                                                            } else {
                                                                $movie_id = $featured->movie_id;
                                                                $film = new Film();
                                                                $film = $film->findByPk($movie_id);
                                                                $cont_name = $film->name;
																$ctype = $film->content_types_id;
                                                                if ($ctype == 2)
                                                                    $poster = $this->getPoster($film->id, 'films', 'episode');
                                                                else
                                                                    $poster = $this->getPoster($film->id, 'films', 'standard');
                                                            }
                                                            if (false === file_get_contents($poster)) {
                                                                $poster = "/img/No-Image-Horizontal.png";
                                                            }
                                                            ?>
                                <li class="col-sm-3 drag-cursor" id="sort_<?php echo $section_id; ?>_<?php echo $featured->id ?>" >
                                    <div class="Preview-Block"  >		
                                        <div class="thumbnail m-b-0"  >
                                            <div class="relative m-b-10 drag-cursor" <?php if ((@IS_LANGUAGE == 1) && ($details['parent_id'] != 0)) { ?>style="cursor: default"<?php } ?>>
                                                <img src="<?php echo $poster ?>" alt="">
                                                <div class="overlay-bottom overlay-white text-right">
                                                    <div class="overlay-Text">
                                                        <?php if (($language_id == $details['language_id']) && ($details['parent_id'] == 0)) { ?>
                                                                <div>
                                                                    <a href="#"  onclick="deleteFeatured(<?php echo $section_id; ?>,<?php echo $featured->id ?>);" class="btn btn-danger icon-with-fixed-width">
                                                                        <em class="icon-trash"></em>
                                                                    </a>
                                                                </div>
                                                        <?php }  ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5><?php echo $cont_name; ?></h5>
                                        </div>
                                    </div>
                                </li>
                                       <?php } ?>
                              
                                    </ul>
                                      
							 
                            
                           
                        </div>

                   
                     </div>
                     </div>
               
                    <?php 
                    $is_sort = 1;
                    if (intval($is_sort)) { ?>
                        <script type="text/javascript">
                            $('#sortable_<?php echo $section_id; ?>').sortable({
                                //handle : '.handle', 
                                update: function () {
                                    var order = $('#sortable_<?php echo $section_id; ?>').sortable('serialize');
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/drafttemplate/sortfeatured",
                                        data: "order=" + $('#sortable_<?php echo $section_id; ?>').sortable('serialize'),
                                        dataType: "json",
                                        cache: false,
                                        success: function (data)
                                        {
                                        }
                                    });
                                }
                            });
                            $("#sortable_<?php echo $section_id; ?>").disableSelection();
                        </script> 
                        <?php } ?>
                        
                        
                        <!--style type="text/css">
                            #sortable_<?php echo $section_id; ?> { list-style-type: none; margin: 0; padding: 0; width:100%;  }
                            #sortable_<?php echo $section_id; ?> li { margin: 3px 3px 3px 0; padding: 1px; text-align: center; background:none; display:inline-block; width:300px;}
                            #sortable_<?php echo $section_id; ?> li .thumbnail{display:inline-block;}
                        </style-->
            <?php $k++; } ?>
                        
                        
               </div>
            </form>
			<?php } ?>
       
   
<div id="addHomepageSection" class="modal fade">
    <div class="modal-dialog">
        <form action="<?php echo $this->createUrl('drafttemplate/addHomepageSection'); ?>" class="form-horizontal" method="post" id="add_homepage_section">
            <input type="hidden" name="section_id" id="section_id" value="0" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Home Page Section</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        
                            <label class="control-label col-sm-3">Section Name:</label> 
                            <div class="col-sm-9">
                                <div class="fg-line">
                                     <input required type="text" id="section_name" class="form-control input-sm" name="section_name" placeholder="Enter Section Name" value="<?php echo $page->title ?>" />
                                </div>
                            </div>
                                   
                    </div>                        

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    
                </div>
            </div>
        </form>	
        <script type="text/javascript">
            function addSection()
            {
                var data = $("#add_homepage_section").serialize();
                var url = "<?php echo $this->createUrl('drafttemplate/addHomepageSection'); ?>";
                $.post(url + "?" + data, function (res) {
                    if (res.succ == 0) {
                        $(".err_msg").html(res.msg);
                        $(".err_msg").show();
                    }
                });
            }
        </script>

    </div>
</div>
<!-- Add Popular Section Popup End -->



<!-- Add Popular Movie Popup Start -->
<div id="addPopularcontnet" class="modal fade">
    <div class="modal-dialog">
        <form action="<?php echo $this->createUrl('drafttemplate/addPopularMovie'); ?>" method="post" id="add_content" class="form-horizontal">
            <input type="hidden" id="featured_section" name="featured_section" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Popular Content</h4>
                </div>
                <div class="modal-body">                        
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name:</label>
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <input type="text" id="title" class="form-control input-sm" name="title" placeholder="Start Typing movie name..." required>
                            </div>
                            <span class="help-block" id="preview_poster"></span>
                        </div>
                            
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <input type="hidden" value="" id="movie_id" name="movie_id" />
            <input type="hidden" value="" id="is_episode" name="is_episode" />
        </form>	
    </div>
</div>
<!-- Add Popular Movie Popup End -->
