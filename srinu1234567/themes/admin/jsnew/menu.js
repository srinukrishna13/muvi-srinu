var appmenu = 0;
var control = "template";
$(document).ready(function () { 
    if($("#appmenu").length > 0){
        appmenu = 1;
        control = "apps";
    }
    $("body").on('click','.menu-item-save', function () {  
        var frm = $(this).closest("form");
        $(frm).validate({
            rules: {
                menu_title: {
                    required: true,
                    minlength: 4
                },
                menu_permalink: {
                    required: true,
                    external_url: true
                }
            },
            messages: {
                menu_title: {
                    required: "Please enter menu text",
                    minlength: "Menu text should be more than 3"
                },
                menu_permalink: {
                    required: "Please enter a valid url",
                    external_url: "Please enter a valid url"
                }
            },
            submitHandler: function (form) {
                var url = HTTP_ROOT + "/"+ control +"/menuitem";
                $.ajax({
                    url: url,
                    data: frm.serialize(),
                    dataType: "json",
                    method: "post",
                    success: function (result) {
                        if (result.action == "success") {
                            window.location.reload();
                        } else {
                            $("#err_message").html(result.message)
                        }
                    }
                });
            }
        });            
    });
    $("body").on('click','.menu-item-remove', function (){ 
        var item_id = $(this).attr("data-id");
        swal({
            title: "Delete Menu Item?",
            text: "Do you really want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: HTTP_ROOT + "/"+ control +"/menuitem",
                    data: {"item_id" : item_id, "option" : "delete_item", appmenu : appmenu},
                    dataType: "json",
                    method: "post",
                    success: function (result) {
                        if (result.action == "success") {
                            window.location.reload();
                        } else {
                            $("#err_message").html(result.message)
                        }
                    }
                });                 
            }
        });
    });
    $('#add-category').click(function () {
        $.ajax({
            url: HTTP_ROOT + "/"+control+"/menuitem",
            data: $('#FRM_MN').serialize(),
            dataType: 'json',
            method: 'post',
            success: function (result) {
                if (result.action == 'success') {
                    window.location.reload();
                } else {
                    $('#err_message').html(result.message)
                }
            }
        });
    });
    var group = $("ol.serialization").sortable({
        group: 'serialization',
        delay: 500,
        isValidTarget: function ($item, container) {
            var depth = 1, // Start with a depth of one (the element itself)
            maxDepth = 2,
            children = $item.find('ol').first().find('li');

            // Add the amount of parents to the depth
            depth += container.el.parents('ol').length;

            // Increment the depth for each time a child
            while (children.length) {
                depth++;
                children = children.find('ol').first().find('li');
            }

            return depth <= maxDepth;
        },
        onDrop: function($item, container, _super) {
            var data = group.sortable("serialize").get();

            var jsonString = JSON.stringify(data, null, ' ');
            _super($item, container);
            var data = {jsonString : jsonString, appmenu : appmenu};
             $.ajax({
                url: HTTP_ROOT + "/"+control+"/sortmenu/",
                data: data,
                dataType: 'json',
                async: false,
                method: 'post',
                success: function (result) {
                    console.log(result);
                }
            });          
        }
    });
});