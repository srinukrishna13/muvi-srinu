<?php
class films extends CActiveRecord{
	private static $db2 = null;
 
    protected static function getNewDbConnection()
    {
        if (self::$db2 !== null)
            return self::$db2;
        else
        {
            self::$db2 = Yii::app()->db2;
            if (self::$db2 instanceof CDbConnection)
            {
                self::$db2->setActive(true);
                return self::$db2;
            }
            else
                throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
        }
    }   
    
    public function getDbConnection()
    {
        return self::getNewDbConnection();
    }    
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'films';
    }
}