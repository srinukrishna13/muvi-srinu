<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/css/cropper.min.css" type="text/css" >
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/css/crop-avatar.css" type="text/css" >
<style type="text/css">
	.movie_save_btn{
		padding: 5px 20px !important;
		font-weight: 700;
	}
	.addmovie-footer{
		padding: 5px 25px !important; 
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function(res){
		$("[data-mask]").inputmask();
		$( "#episode_date" ).datepicker();
	});
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<div id="crop-avatar">
<div class="box-body table-responsive no-padding">
	<div class="col-lg-12" >
		<?php if(isset($editflag) && $editflag){?>
		<div class="col-lg-3 box-body">
<!--			<div class="avatar-view" title="Change the avatar">
				<img src="<?php //echo @$episodeImg;?>" alt="Avatar">
			  </div>-->
			<div class="poster-cls thumbnail avatar-view" >
				<img src="<?php echo @$episodeImg;?>" rel="tooltip" title="<?php echo $movieDetails[0]['name'];?>" />
				<div class="edit-poster ">Change Poster</div>
			</div>
		</div>
		<?php }?>
		<div class="col-lg-9">
			<div class="box box-primary">
				<div class="box-header">
					<?php if(isset($editflag) && $editflag){?>
						<h3 class="box-title">Edit Episode for - <?php echo $movieDetails[0]['name'];?></h3>
				<?php }else{?>
						<h3 class="box-title">Add Episode for - <?php echo $movieDetails[0]['name'];?></h3>
				<?php }?>
					
				</div>
				<!-- form start -->
				<form role="form" method="post" action="<?php echo $this->createUrl('admin/addEpisode');?>" enctype="multipart/form-data" data-toggle="validator" name="episode">
					<div class="box-body">
						<div class="form-group">
							<label for="movie_name">Content Name</label>
							<input type="text" placeholder="Enter content name..." id="movie_name" name='episode[movie_name]' readonly="true" class="form-control" value="<?php echo isset($movieDetails[0]['name']) ? $movieDetails[0]['name']:'';?>" required>
						</div>
						<div class="form-group ">
							<label for="series#">Season#</label>
							<!--<input type="number" placeholder="Enter series number..." id="series_number" name='episode[series_number]' class="form-control" value="" required>-->
							<select class="form-control"  id="sereies_number" name='episode[series_number]'  <?php if(@$editflag){?>disabled="true"<?php }?> >
								<option value="">--Select--</option>
								<?php for($i=1;$i<100;$i++){
									$selc ='';
									if(isset($movieDetails[0]['series_number']) && $movieDetails[0]['series_number'] && ($movieDetails[0]['series_number']==$i)){
										$selc = "selected='selected'";
									}
									echo '<option value="'.$i.'" '.$selc.' >'.$i.'</option>';
								}?>
							</select>
						</div>
						<div class="form-group ">
							<label for="episode#">Episode#</label>
							<input type="number" placeholder="Enter episode number..." id="episode_number" name='episode[episode_number]' class="form-control" value="<?php echo @$movieDetails[0]['episode_number'];?>" onkeypress="return isNumber(event);" >
						</div>
						<div class="form-group">
							<label for="title">Title<span class="text-red"><b>*</b></span></label>
							<input type="text" placeholder="Enter episode title..." id="title" name='episode[title]' class="form-control" value="<?php echo @$movieDetails[0]['episode_title'];?>" required="" >
						</div>
						<div class="form-group ">
							<label for="story">Story</label>
							<textarea placeholder="Enter episode story..." rows="3" class="form-control" name="episode[story]" maxlength="200" id="story"><?php echo @$movieDetails[0]['episode_story'];?></textarea>
							<span class="countdown"></span>
						</div>
						<div class="form-group ">
							<label for="date">Episode Date</label>
							<input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="episode_date" name="episode[episode_date]" value="<?php echo isset($movieDetails[0]['episode_date'])?date('m/d/Y',strtotime($movieDetails[0]['episode_date'])):'';?>" class="form-control">
						</div>
						<div class="form-group" id="preview_poster"></div>
						<input type="hidden" value="<?php echo $movieDetails[0]['movie_id'];?>" name="movie_id" id="movie_id"  />
						<input type="hidden" value="<?php echo $movieDetails[0]['uniq_id'];?>" name="uniq_id" id="uniq_id"  />
						<input type="hidden" value="<?php echo @$movieDetails[0]['movie_stream_id'];?>" name="movie_stream_id" id="movie_stream_id"/>
						<input type="hidden" value="<?php echo @$editflag;?>" name="editflag" id="editflag"/>
					</div>
					<div class="form-group">
						<label for="genre">Content Category: </label>
						<select  name="movie[content_category_value][]" id="content_category_value" multiple required="true" class="form-control">
							<?php 
								foreach($contentCategories AS $k =>$v){
									echo "<option value='".$v."'>".$v."</option>";
							}?>
						</select>
					</div>
					<div class="box-footer addmovie-footer">
						<button class="btn btn-primary movie_save_btn" type="submit" value="1" name="submit_btn"><?php if(isset($editflag) && $editflag){?>Update<?php }else{?>Save<?php }?></button>&nbsp; or &nbsp;<a href="<?php echo $this->createUrl('admin/dashboard');?>">Cancel</a>
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>
<?php
	$movie_id = @$movieDetails[0]['movie_id'];
	$movieName = @$movieDetails[0]['name'];
	$banner_image = @$episodeImg;
	$movie_stream_id = @$movieDetails[0]['id'];
	//require_once 'change_video_thumbnail.php';
?>	
<!-- Cropping modal -->
<div class="modal fade" id="avatar-modal" tabindex="-1" role="dialog" aria-labelledby="avatar-modal-label" aria-hidden="true">
      <div class="modal-dialog modal-lg" style="width: 600px;">
        <div class="modal-content">
          <form class="avatar-form" method="post" action="<?php echo $this->createUrl('admin/cropVideoThumbnail',array('movie_id'=>$movie_id,'movie_stream_id'=> @$movieDetails[0]['movie_stream_id'],'movie_name'=>$movieName));?>" enctype="multipart/form-data">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">Change Video Thumbnail</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Upload image and data -->
                <div class="avatar-upload">
                  <input class="avatar-src" name="avatar_src" type="hidden">
                  <input class="avatar-data" name="avatar_data" type="hidden">
                  <input class="" name="fileinfo" id="fileinfo" type="hidden">
                  <label for="avatarInput">Upload Poster</label>
                  <input class="avatar-input" id="avatarInput" name="Filedata" type="file">
				  <p class="help-block">Upload image size of 560 x 312</p>
                </div>
                <!-- Crop and preview -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="avatar-wrapper"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-primary avatar-save" type="submit">Save</button>
              <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- /.modal -->
</div>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/cropper.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/crop-avatar.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript">
$('.poster-cls').hover(function(res){
	$('.edit-poster').show();
},function(){
	$('.edit-poster').hide();
});	
$(document).ready(function(){
	/* Counter for Story */
	updateCountdown();
	$('#story').change(updateCountdown);
	$('#story').keyup(updateCountdown);
});
	function updateCountdown() {
		// 140 is the max message length
		if($.trim($('#story').val()).length>200){
			var story = $.trim($('#story').val());
			$('#story').val(story.substring(0,200));
		}
		var remaining = 200 - $.trim($('#story').val()).length;
		$('.countdown').text(remaining + ' characters remaining.');
	}
	function globalPopup(popupid){
		$('#'+popupid).modal('show');
	}
</script>