<?php $is_hosted=$this->IS_PCI_COMPLIANCE[$gateway_code];?>
<!-- Payment form-->
   <div id="card_detail" style="position: relative;display: none;">
      <form id="membership_form" name="membership_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false" >
            <div id="other-payment-gateway">
               <div id="card-info-error" class="error" style="display: block;margin: 0 0 15px;"></div>
               <input type="hidden" id="email" name="data[email]" value="<?php if (isset(Yii::app()->user->email) && trim(Yii::app()->user->email)) { echo Yii::app()->user->email;} ?>" />
               <input type="hidden" id="payment_gateway" value="<?php echo $gateways[0]->short_code; ?>" />
               <input type="hidden" name="data[coupon]" id="coupon_code" value="<?php if (isset($data['coupon_code'])) { echo $data['coupon_code'];} ?>" />
               <input type="hidden" name="data[plan_id]" id="plandetail_id" value="<?php echo $plan->id; ?>" />
               <input type="hidden" name="data[plandetailbundles_id]" id="plandetailbundles_id" value="" />
               <input type="hidden" name="data[timeframe_id]" value="" id="timeframe_id" />
               <input type="hidden" name="data[is_bundle]" value="" id="is_bundle" />
               <input type="hidden" name="data[is_subscriptionbundle]" value="" id="is_subscriptionbundle" />
               <input type="hidden" name="data[movie_id]" id="ppvmovie_id" value="<?php if (isset($data['movie_id']) && trim($data['movie_id'])) { echo $data['movie_id'];} ?>" />
               <input type="hidden" name="data[season_id]" id="ppvseason_id" value="<?php if (isset($data['season_id']) && trim($data['season_id'])) { echo $data['season_id'];} else { echo 0;} ?>" />
               <input type="hidden" name="data[episode_id]" id="ppvepisode_id" value="<?php if (isset($data['episode_id']) && trim($data['episode_id'])) { echo $data['episode_id'];} else { echo 0;} ?>" />
               <input type="hidden" name="data[contentTypePermalink]" value="<?php echo $films['content_permalink'];?>" />
               <input type="hidden" id="permalink" name="data[permalink]" value="<?php echo $films['permalink'];?>" />
               <input type="hidden" name="data[ppv_plan]" id="ppv_plan" value="<?php echo (isset($this->PAYMENT_GATEWAY[$gateway_code]) && ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' || $this->PAYMENT_GATEWAY[$gateway_code] == 'paygate')) ? 1 : 0;?>" />
               <input type="hidden" name="data[payment_method]" id="payment_method" value="<?php echo $gateway_code;?>" />
               <input type="hidden" name="data[is_advance_purchase]" id="is_adv_plan" value="<?php echo intval(@$data['isadv']);?>" />
               
               
                <?php echo $this->Language['you_are_purchasing']; ?> <span id="bundle-hdr"><?php echo ucfirst($films['name']);?> </span>
                
                <?php if(isset($data['isadv']) && intval($data['isadv'])) {?>
                <div style="color: #5b5654;margin-top: 10px;">
                    <?php echo $this->Language['content_available_from']; ?> <?php echo date('M d, Y',strtotime($plan->expiry_date . "+1 Days"));?>
                </div>
                <?php if (isset($plan->description) && trim($plan->description)) { ?><div style="color:  #5b5654;margin-top: 10px;"><?php echo $plan->description;?></div><?php } ?>
                <?php } ?>
                <div id="title-hdrdiv"></div>
                
             <?php
             $count=count($this->PAYMENT_GATEWAY);
              if($this->IS_PAYPAL_EXPRESS['paypalpro']==0 && isset($is_hosted) && !$is_hosted) {?>
               <div class="form-group">
                   <div class="col-md-12" id="credit_card_detail_info">
                     <h4>
                        <?php echo $this->Language['credit_card_detail']; ?>
                     </h4>
                  </div>
               </div>
               <?php } ?>
               <?php if($this->IS_PAYPAL_EXPRESS['paypalpro']==1) {?>           
                <input id="payment_gateway" value="paypalpro" autocomplete="false" type="hidden">
                 <input id="payment_gateway_str" value="paypalpro" autocomplete="false" type="hidden">
                  <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY['paypalpro'].'.js';?>"></script>
                <?php } ?>
               <div class="form-group">
                  <div class="col-sm-6 pull-left">
                     <label style="font-weight: normal">
                     <?php if($this->IS_PAYPAL_EXPRESS['paypalpro']==0) {?>                   
                       <?php echo $this->Language['card_will_charge']; ?>: 
                     <?php } ?>
                       <?php if($this->IS_PAYPAL_EXPRESS['paypalpro']==1) {?>                   
                     <?php echo $this->Language['account_will_charge']; ?>: 
                     <?php } ?>
                     <span id="price_amount" data-amount="" data-currency="<?php echo $symbol;?>" style="font-weight: bold;font-size: 18px;"></span>
                     </label>
                  </div>
                  <?php if (isset($cards) && !empty($cards) && intval($can_save_card)) { ?>
                  <div class="col-sm-6 pull-left">
                     <select class="form-control" name="data[creditcard]" id="creditcard" onchange="getCardDetail();">
                        <option value=""><?php echo $this->Language['use_new_card']; ?></option>
                        <?php foreach ($cards as $key => $value) { ?>
                        <option value="<?php echo $value['id']; ?>" ><?php echo trim($value['card_name']) ? $value['card_name']."-".$value['card_last_fourdigit'] . " " . $value['card_type'] : $value['card_last_fourdigit'] . " " . $value['card_type']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
                  <?php } ?>
                  <div class="clearfix"></div>
               </div>
               <?php 
               if(isset($is_hosted) && !$is_hosted){
               if($this->IS_PAYPAL_EXPRESS['paypalpro']==0) {?>           
                 <div id="card_detail_div"></div>
               <?php } }?>
               <input type="hidden" name="data[currency_id]" value="<?php if (isset($currency->id)) { echo $currency->id; } else { echo 0; }?>" id="currency_id" />
               <?php if (intval($can_save_card)) { ?>
               <div class="form-group" id="save_card_option">
                  <?php if (intval($can_save_card)) { ?>
                  <div class="col-sm-6 pull-left" id="for_last_checkout">
                     <div class="checkbox">
                        <label for="save_card">
                        <input type="checkbox" checked="checked" id="for_last_checkout_chk" name="data[save_this_card]" value="1" onclick="saveThisCard(this);"/> <?php echo $this->Language['save_this_card']; ?>
                        </label>
                     </div>
                  </div>
                  <?php } ?>
               </div>
               <?php }?>
               <div class="clear"></div>
               <div id="card_div"></div>
               <?php if(isset($this->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($this->API_ADDONS_OTHERGATEWAY[$gateway_code])){ ?>
               <div class="form-group" id="other-gateway">
                  <div class="loader" id="othergateway_loading" style="z-index:10;"></div>
                  <div class="col-md-12">
                      <div id="paypal-container"></div>
                  </div>
              </div>
              <div class="clearfix"></div>
              <?php }?>
              <div class="form-group">
                      <div class="row" style="padding-left: 16px;">
                          <?php if (isset($this->PAYMENT_GATEWAY) && array_key_exists('paypalpro', $this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY['paypalpro'] == 'paypalpro') {
                              if(isset($is_hosted) && intval($is_hosted)){}else{?>
                          <div 
                            <?php if($this->IS_PAYPAL_EXPRESS['paypalpro']==0) {?> class="col-md-6" <?php } ?>          
                            <?php if($this->IS_PAYPAL_EXPRESS['paypalpro']==1) {?> class="col-md-12" align="right" <?php } ?>
                              >
                              <input type="checkbox" style="display:none" id="have_paypal_pro" name="havePaypal" value="1" class="process-check" />
                              <input type="image" src="<?php echo Yii::app()->baseUrl; ?>/images/paypal.png" id="paypal" onclick="return savePayPalCardForm();" style="margin-top: -15px;">
                          </div>
                          <?php }}?>
                          
                          
                          <?php if($this->IS_PAYPAL_EXPRESS['paypalpro']==0) {?> 
                          <div class="col-md-6"><button id="paynowbtn" class="btn btn-primary" onclick="return validateCardForm();"><?php echo $this->Language['btn_paynow']; ?></button>
                           <?php } ?>
                          </div>

                      <div class="clear"></div>
                      </div>
                      <div class="clearfix"></div>
                  </div>

            </div>
         </form>
    <?php
    if(isset($is_hosted) && intval($is_hosted) && ($gateway_code == 'paypalpro' || $gateway_code == 'ippayment')){
        include($gateway_code.'_hosted_detail.php');   
    }
    ?>
       <div class="clearfix"></div>
   </div>

<script type="text/javascript">
    var is_hosted = '<?php echo $is_hosted;?>';
    function validateCardForm() {
        if($.trim($("#payment_gateway").val()) === 'paypalpro' && !parseInt(is_hosted)){
            document.getElementById('paypal').setAttribute('disabled', 'disabled');
        }
        if (parseInt($("#creditcard option:selected").val())) {
            if (confirm(JSLANGUAGE.confirm_existing_card)) {
                submitForm();
            } else {
                $("#paypal").removeAttr("disabled");
                $("#paynowbtn").removeAttr("disabled");
                return false;
            }
        } else {
            $("#membership_form").validate({
                rules: {
                    "data[card_name]": {
                        required: true
                    },
                    "data[card_number]": {
                        required: true,
                        number: true
                    },
                    "data[exp_month]": {
                        required: true
                    },
                    "data[exp_year]": {
                        required: true
                    },
                    "data[security_code]": {
                        required: true
                    }
                },
                messages: {
                    "data[card_name]": {
                        required: JSLANGUAGE.card_name_required,
                    },
                    "data[card_number]": {
                        required: JSLANGUAGE.card_number_required,
                        number: JSLANGUAGE.card_number_required
                    },
                    "data[exp_month]": {
                        required: JSLANGUAGE.expiry_month_required
                    },
                    "data[exp_year]": {
                        required: JSLANGUAGE.expiry_year_required
                    },
                    "data[security_code]": {
                        required: JSLANGUAGE.security_code_required
                    }
                },
                submitHandler: function (form) {
                    if ($("#payment_gateway").length) {
                        if ($("#payment_gateway").val() !== 'manual') {
                            var payment_gateway = $("#payment_gateway").val();
                            var have_paypal_pro = 0;
                            
                            if($('#have_paypal_pro').is(":checked")){
                                have_paypal_pro = 1;
                            }
                            
                            var payment_gateway_str = $('#payment_gateway_str').val();
                            if(!have_paypal_pro && payment_gateway !== 'paypalpro'){
                                 payment_gateway_str = payment_gateway_str.replace('paypalpro','');
                            }else if(have_paypal_pro){
                                if(payment_gateway === 'paypalpro'){
                                    payment_gateway_str = payment_gateway;
                                }else{
                                     payment_gateway_str = payment_gateway_str.replace(payment_gateway,'');
                                 }
                             }else{
                                 payment_gateway_str = payment_gateway;
                             }
                            payment_gateway_str = payment_gateway_str.replace(/(^,)|(,$)/g, "");
                            if($.trim(payment_gateway_str) == 'paypalpro' && parseInt(is_hosted)){
                                PaymentAction('digital_payment');
                            }else{
                                var class_name = payment_gateway_str+'()';
                                eval ("var obj = new "+class_name);
                                obj.processCard();
                            }
                        }
                    } else {
                        document.getElementById('paypal').setAttribute('disabled', 'disabled');
                        submitForm();
                    }             
                }
            });
        }
    }
    
    function getCardDetail() {
        if (parseInt($("#creditcard option:selected").val())) {
            $("#card_detail_div").html('');
            $("#for_last_checkout_chk").val(0);
            $("#for_last_checkout_chk").prop("checked", false);
            $("#for_last_checkout").hide();
            $("#other-gateway").hide();
            $("#iframeContainer_ipp").hide();
            if($("#paypal").length){
                $("#paypal").attr("disabled",true);
            }
        } else {
            $("#card_detail_div").html($("#new_card_div").html());
            $("#for_last_checkout").show();
            $("#other-gateway").show();
            $("#iframeContainer_ipp").show();
            $("#for_last_checkout_chk").prop("checked", true);
            $("#for_last_checkout_chk").val(1);
            if($("#paypal").length){
                $("#paypal").removeAttr("disabled");
            }
        }
    }
    
    function saveThisCard(obj) {
        if ($(obj).is(':checked')) {
            $(obj).val(1);
        } else {
            $(obj).val(0);
        }
    }
    
    function savePayPalCardForm(){
        if (parseInt($("#creditcard option:selected").val())) {
            if (confirm(JSLANGUAGE.confirm_existing_card)) {
                submitForm();
            } else {
                return false;
            }
        } else {
            $("#have_paypal_pro").prop('checked', true);
                if ($("#payment_gateway").length) {
                if ($("#payment_gateway").val() !== 'manual') {
                    var payment_gateway = $("#payment_gateway").val();
                    var have_paypal_pro = 1;
                    var payment_gateway_str = $('#payment_gateway_str').val();
                    if(!have_paypal_pro && payment_gateway != 'paypalpro'){
                         payment_gateway_str = payment_gateway_str.replace('paypalpro','');
                    }else if(have_paypal_pro){
                        if(payment_gateway = 'paypalpro'){
                            payment_gateway_str = payment_gateway;
                        }else{
                             payment_gateway_str = payment_gateway_str.replace(payment_gateway,'');
                         }
                     }else{
                         payment_gateway_str = payment_gateway;
                     }
                     payment_gateway_str = payment_gateway_str.replace(/(^,)|(,$)/g, "");
                     
                      var class_name = payment_gateway_str+'()';
                      eval ("var obj = new "+class_name);
                      $("#ppvModalMain").hide();
                    obj.processCard();
                    $("#card_detail").hide();
                    $("#card_name").removeAttr('required');
                    $("#card_number").removeAttr('required');
                    $("#exp_month").removeAttr('required');
                    $("#exp_year").removeAttr('required');
                    $("#security_code").removeAttr('required');
                }
            } else {
                $("#ppvModalMain").hide();
                submitForm();
                
            }
        }
        
    }
    
    $('#have_paypal_pro').click(function () {
        if ($(this).is(":checked")) {
            $('#card_detail_div').hide(1500);
            $('#creditcard').hide(1500);
            <?php if (intval($can_save_card)) { ?>
            $('#for_last_checkout').hide(1500);
            <?php }?>
        } else {
            $('#card_detail_div').show(1500);
            $('#creditcard').show(1500);
            <?php if (intval($can_save_card)) { ?>
            $('#for_last_checkout').show(1500);
            <?php }?>
        }
    });
    
    function PaymentAction(goods_type) {
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        var class_name = '<?php echo $gateway_code;?>' + '()';
        eval ("var obj = new "+class_name);
        obj.pciPayment(goods_type);
    }
</script>