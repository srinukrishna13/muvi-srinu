// How this CSS File Organised , The commented code sectins in this file
/*
***** 1. Import all SCSS file into "_all.scss" and Included it to this file. "variables"                    declaration is done in "modules > _variables.scss"
***** 2. Used Font Section
***** 3. All Mixins and they are Life saving !
***** 4. General CSS
***** 5. Utility Classes for Spaces
***** 6. Table Style
***** 7. Tabs
***** 8. Form element
***** 9. Pre loader
***** 10.Sweet Alert
***** 11.Modal
***** 12.Progress Bar
***** 13.General Page Structure
         
            >>Header
            >>Sidebar Element
            >>Content Section: Display of Contents
            >>Resposnive Utilities
            >> One media query exists below "450px"
*/