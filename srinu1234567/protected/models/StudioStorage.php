<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StudioStorage
 *
 * @author Sanjeev<support@muvi.com>
 */
class StudioStorage extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'studio_storage_log';
    }
    
    public function relations(){
        return array();
    }
    
    public function addLog($log_data = array())
    {
        $data = Yii::app()->db->createCommand()
                ->insert($this->tableName(),$log_data);
        return $data;
    }
    
    public function updateLog($id,$log_data = array())
    {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(),$log_data,'id=:id',array(':id' => $id));
        return $data;
    }
}