<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
    <tbody>
        <tr>

            <td align="left" style="padding-left:20px;"><img src="<?php echo EMAIL_LOGO; ?>"/></td>
        </tr>
        <tr>
            <td>
                <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                    <tbody>
                        <tr style="background-color:#f3f3f3">
                            <td style="text-align:left;padding-top:10px">
                                <div mc:edit="logo"><?php echo $params['logo']; ?></div>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                        Congratulations!</p>
                                    <p style="display:block;margin:0 0 17px">
                                        <span mc:edit="user_name"><?php echo $params['user_name']; ?></span>  has made an pre-order of the content <span mc:edit="content_name"><?php echo $params['content_name']; ?></span>. Below are the transaction details</p>
                                    <p style="display:block;margin:0 0 17px">
                                        Name : <span mc:edit="user_name"><?php echo $params['user_name']; ?></span>
                                    </p>
                                    <p style="display:block;margin:0 0 17px">
                                        Email : <span mc:edit="user_email"><?php echo $params['user_email']; ?></span>
                                    </p>
                                    <p style="display:block;margin:-17px 0 17px">
                                        Content : <span mc:edit="content_name"><?php echo $params['content_name']; ?></span>
                                    </p>
                                    <p style="display:block;margin:-17px 0 17px">
                                        Payment Amount : <span mc:edit="payment_amount"><?php echo $params['payment_amount']; ?></span>
                                    </p>
                                    <?php if (isset($params['coupon_code']) && trim($params['coupon_code'])) { ?>
                                    <p style="display:block;margin:-17px 0 17px">
                                        Coupon Code : <span mc:edit="coupon_code"><?php echo $params['coupon_code']; ?></span>
                                    </p>
                                    <p style="display:block;margin:-17px 0 17px">
                                        Coupon Amount : <span mc:edit="coupon_amount"><?php echo $params['coupon_amount']; ?></span>
                                    </p>
                                    <?php } ?>
                                    <p style="display:block;margin:0 0 17px">
                                        Regards,
                                    </p>
                                    <p style="display:block;margin:-17px 0 17px">
                                        Muvi Studio
                                    </p>

                                </div>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>