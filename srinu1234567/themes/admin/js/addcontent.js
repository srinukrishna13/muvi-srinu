/* It will contain all the Javascript related add Contents */
var isFormChanged = 0;
$('.checkInput').on('change', function () {
    if ($(this).val() && !isFormChanged) {
        isFormChanged = 1;
    }
});

(function () {
    $('#sub-btn').click(function () {
        var movie_id = $('#movie_id').val();
        removeTopBanner(movie_id);
    });
})();

function showhinttext(){
$.ajax({
   url: "/wpstudio/wp-content/themes/muvi/news-content-search-data.php",
   dataType: "text",
   data: {
       q: $('#hint_text').html()
   },
   success: function (data) {
       $('.postdetails').html(data); 
   }
});
$(".default_cont").hide();
    $('.info-Action').click();
    $("#aq").val($('#hint_text').html());

        }
function removeTopBanner() {
    var movie_id = $('#movie_id').val();
    swal({
        title: "Remove Banner?",
        text: "Are you sure to remove this banner?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        if (movie_id) {
            var url = HTTP_ROOT + "/admin/removeTopBanner";
            $('#remove-top-banner-text').hide();
            $.post(url, {'movie_id': movie_id, 'is_ajax': 1}, function (res) {
                if (res.err) {
                    $('#success_msg').html('<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting top banner.</div>');
                    $('#remove-top-banner-text').show();
                } else {
                    $('#success_msg').html('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Top Banner removed successfully.</div>');
                    $('#remove-top-banner-text').hide();
                    $('#topbanner-preview-img').children('img').remove();
                    $('#topbanner-preview-img').html('<img src="' + defaultbanner + '" rel="tooltip" title="Upload your poster" />');
                    $('#add_change-top-banner-text').html("Upload Banner ");
                }
            }, 'json');
        } else {
            return false;
        }
    });
}

function removeposter(movie_id, obj_type, movie_stream_id) {
    swal({
            title: "Remove Poster?",
            text: "Are you sure to remove this poster?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
        if (movie_id) {
            var url = HTTP_ROOT + "/admin/removeposter";
            $('#remove-poster-text').text('Removing...');
            $('#remove-poster-text').attr('disabled', 'disabled');
            $.post(url, {'movie_id': movie_id, 'obj_type': obj_type, 'movie_stream_id': movie_stream_id, 'is_ajax': 1}, function (res) {
                if (res.err) {
                    $('#remove-poster-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting poster.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Poster removed successfully.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('');
                    $('#avatar_preview_div').children('img').remove();
                    window.location.reload(1);
                }
            }, 'json');
        } else {
            return false;
        }
    });
}
/* Select the Form based on the content type */
function getFormContent(formType,ctypeid,custom_form_id) {
    isFormChanged = 0;
    $('#reqwidth').val(VER_POSTER_WIDTH);
    $('#reqheight').val(VER_POSTER_HEIGHT);
    $('.loading_div').show();
    $('#formContents').css({'opacity': '0.3'});
    var baseUrl = HTTP_ROOT;
    if (formType == 'episode') {
        $.post(baseUrl + "/customForm/getEpisodeForm",{'contentTypesId':ctypeid,'custom_form_id':custom_form_id},function (data) {
            $('#reqwidth').val(HOR_POSTER_WIDTH);
            $('#reqheight').val(HOR_POSTER_HEIGHT);
            $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
            $('#upload-sizes').html($('#reqimgsize').html());
            $('#contentForm').attr('action', 'javascript:void(0);');
            $("#avatar_preview_div").html('');
            Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text='+HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
            Holder.run();             
            $('#contentForm').removeAttr('onsubmit');
            $('#contentForm').attr('onsubmit', 'return submitEpisodeForm()');
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            $("[data-mask]").inputmask();
            $("#episode_date").datepicker();
        });
    } else if (formType == 'live') {
        $.post(baseUrl + "/customForm/getLiveStreamForm",{'contentTypesId':ctypeid,'custom_form_id':custom_form_id}, function (data) {
            $('#reqwidth').val(HOR_POSTER_WIDTH);
            $('#reqheight').val(HOR_POSTER_HEIGHT);
            $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
            $('#upload-sizes').html($('#reqimgsize').html());
            $('#contentForm').attr('action', HTTP_ROOT + '/admin/addChannel');
            $("#avatar_preview_div").html('');
            Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text='+HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
            Holder.run();             
            $('#contentForm').removeAttr('onsubmit');
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            $("[data-mask]").inputmask();
            $("#release_date").datepicker();
            intializeTagsInput();
//            $('#is_recording').click(function () {
//                if (this.checked == false) {
//                    $('#recordingDiv_date_div').hide();
//                } else {
//                    $('#recordingDiv_date_div').show();
//                }
//            });
            });
    } else if (formType == 'physical') {
        $.post(baseUrl + "/customForm/getPhysicalForm",{'contentTypesId':ctypeid,'custom_form_id':custom_form_id}, function (data) {
            $('#reqwidth').val(HOR_POSTER_WIDTH);
            $('#reqheight').val(HOR_POSTER_HEIGHT);
            $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
            $('#upload-sizes').html($('#reqimgsize').html());
            $('#contentForm').attr('action','javascript:void(0);');
            $("#avatar_preview_div").html('');
            Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text='+HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
            Holder.run();             
            $('#contentForm').removeAttr('onsubmit');
            $('#contentForm').attr('onsubmit', 'return submitPhysicalForm()');
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            $("[data-mask]").inputmask();
            $("#release_date").datepicker();
            intializeTagsInput();
        });
    } else {
        var content_types_id = 2;
        if (formType == 'basic') {
            content_types_id = 1;
        }
        $.post(baseUrl + "/customForm/getContentForm", {'content_types_id': content_types_id,'contentTypesId':ctypeid,'custom_form_id':custom_form_id}, function (data) {
            if (content_types_id == 2) {
                $('#contentForm').attr('action', HTTP_ROOT + '/admin/addContents');
                $('#reqwidth').val(HOR_POSTER_WIDTH);
                $('#reqheight').val(HOR_POSTER_HEIGHT);
                $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
                $('#upload-sizes').html($('#reqimgsize').html());
                $("#avatar_preview_div").html('');
                Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text='+ HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
                Holder.run();                 
                $('#contentForm').removeAttr('onsubmit');
                $('#contentForm').prop('onsubmit', false);
            } else {
                $('#contentForm').removeAttr('onsubmit');
                $('#contentForm').prop('onsubmit', false);
                $('#contentForm').removeAttr('action');
                $('#contentForm').attr('action', 'javascript:void(0);');
                $('#contentForm').prop('onsubmit', true);
                $('#contentForm').attr('onsubmit', 'return submitContentForm()');
                $("#avatar_preview_div").html('');
                $('#reqimgsize').html(VER_POSTER_WIDTH + 'X' + VER_POSTER_HEIGHT);
                $('#upload-sizes').html($('#reqimgsize').html());
                Holder.addImage('holder.js/' + VER_POSTER_WIDTH + 'x' + VER_POSTER_HEIGHT + '?text='+ VER_POSTER_WIDTH + 'x' + VER_POSTER_HEIGHT, "#avatar_preview_div");
                Holder.run(); 

            }
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            intializeTagsInput();
            $("[data-mask]").inputmask();
            $("#release_date").datepicker({
                changeMonth: true,
                changeYear: true
            });
            $("#publish_date").datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)
            });
            $("#publish_time").timepicker({
                showInputs: false
            });
            $('#content_publish_date').click(function () {
                if (this.checked == false) {
                    $('#content_publish_date_div').hide();
                } else {
                    $('#content_publish_date_div').show();
                }
            });
        });
    }
    $("#avatar_preview_div").show();
    $("#previewcanvas").hide();
}

/* Hide so live streaming infos based on the radio values*/
function checkFeedType() {
    var val = $('input[name=method_type]:checked').val();
    $('.info_text').hide();
    $('.info_text').removeClass('hidden');
    $('#method_' + val).show();
    if (val == 'push') {
        $('#pull_feeds').hide();
        $('#pull_feedss').hide();
        $('#feed_url').hide();
        $('#showPushHints').show();
        $('#feed_url').removeAttr( "required" );
        $('#feed_url').prop( "value",'rtmp://52.20.205.94/live/testing' );
        $('#feed_url').prop( "type",'hidden' );
        $('#recordingDiv').show();
        checkIsDeleteContent();
    } else {
        $('#feed_url').show();
        $('#feed_url').prop('placeholder', 'rtmp://muvi.com/LiveStream/53WDJMVT');
        $('#feed_url').prop( "value",'' );
        $('#feed_url').prop( "type",'text' );
        $('#feed_url').prop('required', 'required');
        $('#pull_feedss').show();
        $('#pull_feeds').show();
        $('#showPushHints').hide();
        $('#recordingDiv').hide();
        $('#delete_no').removeAttr( "required" );
        $("#delete_no").val('');
    }
}

function checkIsDeleteContent(){
    var delete_content = $("input[name='delete_content']:checked").val();
    if(delete_content == 1){
        $("#delete_no").prop('required', 'required');
    } else{
        $('#delete_no').removeAttr( "required" );
        $("#delete_no").val('');
    }
}

function checkUrlpartern(type) {
    var val = $('input[name=method_type]:checked').val();
    if (val != 'push') {
	$('#feed_url').val('');
		if (type == 2) {
			$('#feed_url').attr('pattern', 'rtmp?://.+');
			$("#feed_url").attr("placeholder", "rtmp://muvi.com/LiveStream/53WDJMVT").placeholder();
		} else {
			$('#feed_url').attr('pattern', 'https?://.+');
			$("#feed_url").attr("placeholder", "http://muvi.com/manifests/vM7nH0Kl.m3u8").placeholder();        
		}
	}
}

function globalPopup(popupid) {
    $('#' + popupid).modal('show');
    if (popupid == 'topbanner') {
        $('#content_id').val($('#movie_id').val());
    }
}

function addCastCrewpopup() {
    $('#add_cast_crew').toggle('slow');
    settout = setTimeout('scrollwindow()', '1000');
}
function scrollwindow() {
    window.scrollTo(0, document.body.scrollHeight);
    clearTimeout(settout);
}
function addCast(){
	 if(celebxhr && celebxhr.readyState != 4){
		celebxhr.abort();
	 }
    if($("#castname").val()!='' && $("input:radio[name='cast_type']").is(":checked")){
        $('#add_btn').html('Please wait');
        $('#add_btn').attr('disabled','disabled');
        }
    var e=$("#cast_id").val(),t=$("#castname").val(),a=$("#castchar").val(),o=$("input[name='cast_type']:checked").val(),i=$("#movie_id").val(),r=HTTP_ROOT+"/admin/addCastCrew",n="";
    $.post(r,{castid:e,castname:t,castchar:a,casttype:o,movie_id:i},function(a){
        if(a.success){
            $('#add_btn').html('Add');
            $('#add_btn').removeAttr('disabled');
            n=a.img;
            m=a.castid;
            var i="<tr><td>"+t+'</td><td><div class="Box-Container m-b-10"><div class="thumbnail thumbnail-small"><img src="'+n+'" alt="'+t+'"></div></div></td><td>'+o+'</td><td><h5><a href="javascript:void(0)" onclick="removeCast('+m+', this);"><em class="icon-trash"></em>&nbsp;&nbsp;Remove</a></h5></td></tr>';
            $("#castcrew_body").append(i),$("#cast_id").val(""),$("#castname").val(""),$("#castchar").val(""),$("#add_cast_crew").toggle("slow"),$("#nocast_crew").hide()
        }else
            a.error&&swal(a.msg)},"json")
    }


function removeCast(cast_id, obj) {
    swal({
        title: "Remove cast?",
        text: "Are you sure you want to remove this cast from the movie?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        var url = HTTP_ROOT + "/admin/removeCastCrew";
        var movie_id = $('#movie_id').val();
        $.post(url, {'castid': cast_id, 'movie_id': movie_id}, function (res) {
            $(obj).parents('tr').remove();
            console.log(obj);
        });
    });

}

function deleteVideo(movie_id) {
    swal({
        title: "Remove video?",
        text: "Are you sure you want to remove this video from the movie? \n This video will parmanetly deleted from the movie.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        $.post(HTTP_ROOT + "/admin/removeVideo", {'is_ajax': 1, 'movie_id': movie_id}, function (res) {
            if (res.error == 1) {
                swal('Error in removing movie');
            } else {
                $('#fmovie_name').html("<small><em>Not Available</em></small>");
                swal('Your movie video removed successfully');
            }
        }, 'json');
    });
}

function click_browse(file_name) {
    $("#" + file_name).click();
}

function openUploadpopup() {
    $('input[type=file]').val('');
    $("#addvideo_popup").modal('show');
    searchvideo();
}
function checkfileSize() {
    var movie_name = $('#mname').val();
    var stream_id = $('#movie_stream_id').val();
    var movie_st_id = $('#movie_id').val();
    var filename = $('#videofile').val().split('\\').pop();
    var extension = filename.replace(/^.*\./, '');
    if (extension == filename) {
        extension = '';
    } else {
        extension = extension.toLowerCase();
    }
    switch (extension) {
        case 'mp4':
            break;
        case 'mov':
            break;
        case 'mkv':
            break;
        case 'flv':
            break;
        case 'vob':
            break;
        case 'm4v':
            break;
        case 'avi':
            break;
        case '3gp':
            break;
        case 'mpg':
            break;
        case 'wmv':
            break;
        default:
            swal('Sorry! This video format is not supported. \n MP4/MOV/MKV/FLV/VOB/M4V/AVI/3GP/MPG/WMV format video are allowed to upload');
            return false;
            break;
    }
    swal({
            title: "Upload File?",
            text: "Are you sure to upload " + filename + " file as preview for content " + movie_name + " ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#10CFBD",
            customClass: "cancelButtonColor",
            confirmButtonText: "Upload",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true,
            html:true
          },
          function(isConfirm){
            if (isConfirm) {
                $("#addvideo_popup").modal('hide');
                if (!$('#dprogress_bar').is(":visible")) {
                    $('#dprogress_bar').show();
                }
                $('#trailer_btn').attr('disabled', 'disabled');
                upload('user', 'pass', stream_id, movie_name, movie_st_id);
                return true;
            }else{
                return false;
            }
        });
}
function posterpreview(obj) {
    $("#previewcanvas").show();
    var canvaswidth = $("#reqwidth").val();
    var canvasheight = $("#reqheight").val();
    if($('#g_image_file_name').val()==''){
        var x1 = $('#x1').val();
        var y1 = $('#y1').val();
        var width = $('#w').val();
        var height = $('#h').val();
    }else{
        var x1 = $('#x13').val();
        var y1 = $('#y13').val();
        var width = $('#w3').val();
        var height = $('#h3').val();
    }
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function () {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
    };
    $("#avatar_preview_div").hide();
    if($('#g_image_file_name').val()==''){
        img.src = $('#preview').attr("src");
    }else{
        img.src = $('#glry_preview').attr("src");
    }    
    $('#myLargeModalLabel').modal('hide');
    $(obj).html("Next");
    
        }
function hide_file() {
    $('#preview').css("display", "none");
}
function hide_gallery() {
    $('#glry_preview').css("display", "none");
}
function seepreview(obj) {
    
    if ($("#x13").val() != "") {
        $(obj).html("Please Wait");
        $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
        posterpreview(obj);
    } else {
        if ($("#celeb_preview").hasClass("hide")) {
            $('#myLargeModalLabel').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
            if ($("#x13").val() != "") {
                posterpreview(obj);
            } else if ($('#x1').val() != "") {
                posterpreview(obj);
            } else {
                $('#myLargeModalLabel').modal('hide');
                $(obj).html("Next");
            }
        }
    }
}
function addvideoFromVideoGallery(videoUrl, galleryId) {
    showLoader();
    var action_url = HTTP_ROOT + '/admin/addVideoFromVideoGalleryToTrailer';
    $('#server_url_error').html('');
    var movie_id = $('#movie_id').val();
    $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id}, function (res) {
        showLoader(1);
        if (res.error) {
            $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
        } else {
            if (res.is_video === 'uploaded') {
                window.location.href = window.location.href;
            } else if (res.msg === 'Trailer updted successfully') {
                var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Wow! Preview for the content uploaded successfully. It will be available after completion of encoding.</div>'
                $("#addvideo_popup").modal('hide');
                $('.pace').prepend(sucmsg);
                $('#trailer_btn').remove();
                $('#videoCorrupt').hide();
                $('#trailerbtn ').html('<a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>');
            } else {
                $('#trailerLable').html('There seems to be something wrong with the video');
            }
        }
    }, 'json');
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
        $('#profile button').attr('disabled', 'disabled');

    } else {
        $('.loaderDiv').hide();
        $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
        $('#profile button').removeAttr('disabled');
    }
}

function searchvideo(type) {   
    showLoader();
    var key_word = $("#search_video").val();
    var key_word = key_word.trim();
    $.ajax({
        url: HTTP_ROOT + "/admin/ajaxSearchVideo",
        data: {'search_word': key_word,'type':type},
        contentType: false,
        cache: false,
        success: function (result)
        {
            showLoader(1);
            $("#video_content_div").html(result);
        }
    });
}
function fileSelectHandler() {
    $('#celeb_preview').css("display", "block");
    $("#g_image_file_name").val('');
    $("#g_original_image").val('');
    $('#glry_preview').css("display", "none");
    clearInfo();
    $(".jcrop-keymgr").css("display", "none");
    $("#editceleb_preview").hide();
    $("#celeb_preview").removeClass("hide");
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    var oFile = $('#celeb_pic')[0].files[0];
    var ext = oFile.name.split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        swal('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    if (oFile.name.match(/['|"|-|,]/)) {
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        swal('File names with symbols such as \' , - are not supported');
        return;
    }
     var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
             $("#celeb_preview").addClass("hide");
             document.getElementById("celeb_pic").value = "";
             
             swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
       
        if (width < reqwidth || height < reqheight) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);
            
            return;
        }
        var oImage = document.getElementById('preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                    $('#glry_preview').width("450");
                    $('#glry_preview').height("250");
                }
                $('#preview').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    }
}
function toggle_preview(id, img_src, name_of_image) {
    
    $('#gallery_preview').css("display", "block");
    clearInfo();
    $('#gallery_preview').removeClass("hide");
    document.getElementById("celeb_pic").value = "";
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    showLoader();
    var image_file_name = name_of_image;
    var image_src = img_src;
    $("#g_image_file_name").val(image_file_name);
    $("#g_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < reqwidth || height < reqheight) {
            showLoader(1);
            $("#gallery_preview").addClass("hide");
            $("#g_image_file_name").val("");
            $("#g_original_image").val("");
            swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' X ' + reqheight);
            return;
        }
        var oImage = document.getElementById('glry_preview');
        showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#glry_preview').width(oImage.naturalWidth);
                $('#glry_preview').height(oImage.naturalHeight);
                $('#preview').width("800");
                $('#preview').height("300");
            }
            $('#glry_preview').Jcrop({
                minSize: [reqwidth, reqheight],
                aspectRatio: aspectRatio,
                boxWidth: 400,
                boxHeight: 150,
                bgFade: true,
                bgOpacity: .3,
                onChange: updateInfoallImage,
                onSelect: updateInfoallImage,
                onRelease: clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
            });
        };
    };
}
function removeTrailer(movie_id) {
    swal({
            title: "Remove Trailer?",
            text: "Are you sure you want to remove this trailer?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
        if (movie_id) {
            var url = HTTP_ROOT + "/admin/removeContentTrailer";
            $('#remove-trailer-text').attr('disabled', 'disabled');
            $.post(url, {'movie_id': movie_id, 'is_ajax': 1}, function (res) {
                if (res.err) {
                    $('#remove-trailer-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting preview.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-trailer-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Preview removed successfully.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#trailerplayer').remove();
                    $('#remove-trailer-text').remove();
                    $('#trailerbtn').html('<a href="javascript:void(0)" onclick="openUploadpopup(\'addvideo_popup\');" class="btn btn-default-with-bg btn-sm" id="trailer_btn"><i class="icon-upload"></i> Upload Preview </a>');
                }
            }, 'json');
        } else {
            return false;
        }
    }); 
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function intializeTagsInput() {
    if($('#is_check_custom').val()==1){
        return false;
    }
    var genre = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/getTags?taggable_type=1',
            filter: function (list) {
                return $.map(list, function (genre) {
                    return {name: genre};
                });
            }
        }
    });
    genre.clearPrefetchCache();
    genre.initialize();

    $('#genre').tagsinput({
        typeaheadjs: {
            name: 'genre',
            displayKey: 'name',
            valueKey: 'name',
            source: genre.ttAdapter()
        }
    });
    var language = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/getTags?taggable_type=3',
            filter: function (list) {
                return $.map(list, function (language) {
                    return {name: language};
                });
            }
        }
    });
    language.clearPrefetchCache();
    language.initialize();

    $('#language').tagsinput({
        typeaheadjs: {
            name: 'language',
            displayKey: 'name',
            valueKey: 'name',
            source: language.ttAdapter()
        }
    });

    var censor_rating = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/getTags?taggable_type=2',
            filter: function (list) {
                return $.map(list, function (censor_rating) {
                    return {name: censor_rating};
                });
            }
        }
    });
    censor_rating.clearPrefetchCache();
    censor_rating.initialize();

    $('#censor_rating').tagsinput({
        typeaheadjs: {
            name: 'censor_rating',
            displayKey: 'name',
            valueKey: 'name',
            source: censor_rating.ttAdapter()
        }
    });
}
function submitContentForm() {
    var formobj = document.getElementById('contentForm');
    $('#save-btn').text('Wait!...');
    $('#save-btn').attr('disabled', 'disabled');

    $.ajax({
        url: HTTP_ROOT + "/admin/ajaxSubmitForm",
        type: "POST",
        data: new FormData(formobj),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        success: function (data) {
            clearInfo();
            $('#save-btn').removeAttr('disabled');
            $('#contentForm').removeAttr('onsubmit');
            $('#contentForm').attr('action', HTTP_ROOT + '/admin/updateMovieDetails');
            $('#save-btn').text('Update Content');
            $('#movie_id').val(data.movie_id);
            $('#uniq_id').val(data.uniq_id);
            $('#movie_stream_id').val(data.movie_stream_id);
            if (data.poster) {
				$("#avatar_preview_div").html('');
                $("#avatar_preview_div").html('<img id="preview_content_img" src="' + data.poster + '"/>');
            }
            if(($('#child_content_type').val()==5) || ($('#child_content_type').val()==6)){
                $('.hideforaudio').hide();
            }
            $('.addmore-content').show();
            $('#avatarInput').val('');
            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i>&nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Content was added successfully</div>'
            $('.Current_PageHeader').append(sucmsg);
            $('#celeb_pic').val('');
            $('#parent_content_type').attr({disabled: 'disabled', readonly: 'readonly'});
            $('#child_content_type').attr({disabled: 'disabled', readonly: 'readonly'});
            $('#multi_content_child').attr({disabled: 'disabled', readonly: 'readonly'});
            $('#multi_content_parent').attr({disabled: 'disabled', readonly: 'readonly'});
            window.scrollTo(0, 0);
        }
    });
}
function submitEpisodeForm() {
    var formobj = document.getElementById('contentForm');
    $('#save-btn').text('Wait!...');
    $('#save-btn').attr('disabled', 'disabled');

    $.ajax({
        url: HTTP_ROOT + "/admin/ajaxAddEpisode",
        type: "POST",
        data: new FormData(formobj),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        success: function (data) {
            if (data.err) {
                var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + data.msg + '</div>'
                $('.pace').prepend(sucmsg);
                $('#save-btn').removeAttr('disabled');
                $('#save-btn').text('Save');
                window.scrollTo(0, 0);
            } else {
                window.location.href = HTTP_ROOT + '/admin/manageContent';
            }
        }
    });
}
function timeCal(seconds){
    var hours   = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var sec = seconds - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (sec < 10) {sec = "0"+sec;}
    var  timeLeft = hours+':'+minutes+':'+sec;
    return timeLeft;
}
function upload(user, pass, stream_id, movie_name, movie_st_id) {
	var size = '';
	var sizeleft = 0;
	var sizeName = '';
    var filename = $('#videofile').val().split('\\').pop();
    if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
        swal("Sorry! You are using an older or unsupported browser. Please update your browser");
        return;
    }
	var timeLeft = '00:00:00';
	var sizeKb;
	var sizeLeftKb;
    var file = $('#videofile')[0].files[0];
	size = Math.round(file.size/1000);
	sizeKb=size;
	timeLeft = ((sizeKb/1000)/userinternetSpeed).toFixed(0);
	if(timeLeft < 4){
		timeLeft = 4;
	}
	if((size/1000)<6){
		speed=userinternetSpeed+"mbps";
	}
	timeLeft = timeCal(timeLeft);
	sizeName = 'Kb';
	if(size > 1000){
		size = Math.round(size/1000);
		var sizeName = 'MB';
	}
	if(size > 1000){
		size = size/1000;
		size = size.toFixed(2);
		var sizeName = 'GB';
	}
	var speed="0 kbps";
	var speedMbps;
	var startTime = (new Date()).getTime();
    s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'movie_id': movie_st_id, 'uploadType': 'trailer'});
    s3upload.onServerError = function (command, jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 403) {
            swal("Sorry you are not allowed to upload");
        } else {
            console.log("Our server is not responding correctly");
        }
    };
    s3upload.onS3UploadError = function (xhr) {
        s3upload.waitRetry();
        console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
    };
    s3upload.onProgressChanged = function (progPercent) {
		var endTime = (new Date()).getTime();
		var duration = (endTime-startTime)/1000;
        if (progPercent == 100) {
            $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
			$('#upload_'+stream_id+' .uploadFileSizeProgress').html(size);
            $('#upload_'+stream_id+' .timeRemaining').html("00:00:00");
        } else {
            var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
            $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

            $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
                
			if(sizeName == 'GB'){
				sizeleft = (progper * size)/100;
				sizeleft = sizeleft.toFixed(2);
			}else{
				sizeleft = Math.round((progper * size)/100);
			}
			if(sizeName == 'GB'){
				 speed=((sizeleft*1000*1000)/duration).toFixed(2) ;
				 sizeLeftKb = sizeleft*1000*1000;
			}
			if(sizeName == 'MB'){
				speed=((sizeleft*1000)/duration).toFixed(2) ; 
				sizeLeftKb = sizeleft*1000;
			}
			if(sizeName == 'KB'){
				 speed=((sizeleft)/duration).toFixed(2);
				 sizeLeftKb=sizeleft;
			}
			speedMbps=(speed/1000).toFixed(2);
			seconds = ((sizeKb-sizeLeftKb)/speed).toFixed(0);
			timeLeft = timeCal(seconds);
			if(speed > 999){
				speed=(speed/1000).toFixed(2)+"mbps";
			}
			else{
				speed=speed + "kbps";
			}
			$('#upload_'+stream_id+' .uploadFileSizeProgress').html(sizeleft);
			$('#upload_'+stream_id+' .uploadSpeed').html(speed);
			$('#upload_'+stream_id+' .timeRemaining').html(timeLeft);
        }
    };
    s3upload.onUploadCompleted = function (data) {
        $.post(HTTP_ROOT+"/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {});
        var obj = jQuery.parseJSON(data);
        $('#upload_' + stream_id).remove();
        $('#trailer_btn').removeAttr('disabled');
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }
        var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg" style="display: block;"><i class="icon-check"></i>&nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Wow! Preview for the content uploaded successfully. It will be available after completion of encoding.</div>'
        $('.pace').prepend(sucmsg);
        $('#trailer_btn').remove();
        $('#videoCorrupt').hide();
        $('#trailerbtn').html('<a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>');
        console.log("Congratz, upload is complete now");
    };
    s3upload.onUploadCancel = function () {
        $('#upload_' + stream_id).remove();
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }

        $('#trailer_btn').removeAttr('disabled');
        console.log("Upload Cancelled..");
    };
    s3upload.start();
    s3obj[stream_id] = s3upload;
    var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">'+sizeleft+'</span>/'+size+' '+sizeName+') (<span class="uploadSpeed">'+speed+'</span>)<span class="timeRemaining" style="float:right">'+timeLeft+'</span></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
    $('#all_progress_bar').append(progressbar);
    $('#cancel_' + stream_id).on('click', function () {
        aid = this.id;
        sid = aid.split('_');
        swal({
            title: "Cancel Upload?",
            text: "Are you sure you want to cancel this upload?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            s3obj[sid[1]].cancel();
            document.getElementById("videofile").value = "";
        });

    });	
}
/* Get Subcategory list*/
function getSubCategoryList(){
	$('.loading_div_subcategory').show();
	$('#content_subcategory_value').hide();
	var categoryValue = $('#content_category_value').val();
	var baseUrl = HTTP_ROOT;
    $.post(baseUrl + "/customForm/getSubCategory",{'category_value':categoryValue},function (data) {
		$('#content_subcategory').html(data);
		$('.loading_div_subcategory').hide();
		$('#content_subcategory_value').show();
	});
}
function getCustomForm(obj){
    var metadata_form_id = $(obj).find('option:selected').attr('data-value');
    var custom_form_id = $(obj).val();
    $.post(HTTP_ROOT + "/customForm/GetPosterDimention",{'custom_form_id':custom_form_id},function (data) {
        VER_POSTER_WIDTH = data.vwidth;
        VER_POSTER_HEIGHT = data.vheight;
        if((metadata_form_id == 6) && (data.pwidth && data.pheight)){
            HOR_POSTER_WIDTH = data.pwidth;
            HOR_POSTER_HEIGHT = data.pheight;
        }else{
        HOR_POSTER_WIDTH = data.hwidth;
        HOR_POSTER_HEIGHT = data.hheight;
        }        
        if((metadata_form_id==4) || (metadata_form_id==9)){
            $('#firetvimagediv').show();
            getFormContent('episode',metadata_form_id,custom_form_id);
        }else if((metadata_form_id==5) || (metadata_form_id==10)){
            $('#firetvimagediv').show();
            getFormContent('live',metadata_form_id,custom_form_id);
        }else if(metadata_form_id==6){
            $('#firetvimagediv').hide();
            getFormContent('physical',metadata_form_id,custom_form_id);
        }else{
            $('#firetvimagediv').show();
            getFormContent('basic',metadata_form_id,custom_form_id);
        }
    }, 'json');
}
function submitPhysicalForm() {
    var formobj = document.getElementById('contentForm');    
    $('#save-btn').attr('disabled', 'disabled');

    if($("input[name='pg[product_type]']:checked"). val()==1){
        if($('#movie_ids').val()==''){
            $('#error_movie_id').html('Enter content name');
            return false;
        }else{
            $('#error_movie_id').html('');
        }                
    }else{
        $('#error_movie_id').html('');
    }        
    var isprice = 1;
    $(".moreCurrencyDiv").find(".cost").each(function () {
        $(this).parent().find('label').remove();
        if($(this).val()==''){
            $(this).parent().after("<label class='error red multierrorprice'>This field is required.</label>");
            isprice = 0;
        }
    });
    if(isprice==0){
        return false;
    }
    var currency = new Array();
    $(".moreCurrencyDiv").find(".select").each(function () {
        currency.push($(this).find('select').val());
    });
    var x = currency.getUnique();
    if (x.length === currency.length) {
        
    } else {
        swal("Currency should be unique");
        return false;
    }
    var url = HTTP_ROOT + "/store/checkSkuNumber";         
    $('#save-btn').attr('disabled','disabled');
    var productid = $('#movie_id').val();
    var check;
    if(productid){check = 'FALSE';}else{check = 'TRUE'}
    $.post(url, {'skuno': $('#skuno').val(),'check':check,'id':productid}, function (res)
    {
        if (res.succ)
        {
            $('#save-btn').text('Wait!...');
            $('#email-error').hide();
            $('#email-error').html('');
            $.ajax({
                url: HTTP_ROOT + "/admin/ajaxSubmitPhysicalForm",
                type: "POST",
                data: new FormData(formobj),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    clearInfo();
                    $('#save-btn').removeAttr('disabled');
                    $('#contentForm').removeAttr('onsubmit');
                    $('#contentForm').attr('action', HTTP_ROOT + '/admin/updatePhysicalDetails');
                    $('#save-btn').text('Update Content');
                    $('#movie_id').val(data.movie_id);
                    $('#uniq_id').val(data.uniq_id);
                    $('#movie_stream_id').val(data.movie_stream_id);
                    if (data.poster) {
                    $("#avatar_preview_div").html('');
                        $("#avatar_preview_div").html('<img id="preview_content_img" src="' + data.poster + '"/>');
                    }
                    if(($('#child_content_type').val()==5) || ($('#child_content_type').val()==6)){
                        $('.hideforaudio').hide();
                    }
                    $('#avatarInput').val('');
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i>&nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Content was added successfully</div>'
                    $('.Current_PageHeader').append(sucmsg);
                    $('#celeb_pic').val('');
                    $('#parent_content_type').attr({disabled: 'disabled', readonly: 'readonly'});
                    $('#child_content_type').attr({disabled: 'disabled', readonly: 'readonly'});
                    $('#multi_content_child').attr({disabled: 'disabled', readonly: 'readonly'});
                    $('#multi_content_parent').attr({disabled: 'disabled', readonly: 'readonly'});
                    window.scrollTo(0, 0);
                }
            });
        } else {
            $('#save-btn').removeAttr('disabled');
            $('#email-error').show();
            $('#email-error').html('This sku number has been used.');
        }
    }, 'json');    
}
