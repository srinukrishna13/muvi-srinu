<?php

//error_reporting(E_ALL);
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
// This is the main Web application configuration. Any writable
if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
    define('HTTP', 'https://');
} else {
    define('HTTP', 'http://');
}
// CWebApplication properties can be configured here.
$hs = explode(".", $_SERVER['HTTP_HOST']);
if (isset($hs[0]) && $hs[0] == 'www' && isset($hs[1]))
    $ar = $hs[1];
else
    $ar = $hs[0];

$subdomain = '';
if (isset($ar))
    $subdomain = $ar;
defined('SUB_DOMAIN') ? '' : define('SUB_DOMAIN', $subdomain);
if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
    define('HTTP', 'https://');
} else {
    define('HTTP', 'http://');
}
define('CDN_HTTP', 'https://');

$baseurl = HTTP . $_SERVER['SERVER_NAME'];
$hosturl = HTTP . $_SERVER['HTTP_HOST'];
defined('HOST_URL') ? '' : define('HOST_URL', $hosturl);

//Constant define for FirstData (Live and Demo)
define('IS_LIVE_FIRSTDATA', 0);

define('SAMPLE_DATA_STUDIO', 54);
define('HOR_POSTER_STUDIO', 413);

define('CLASSIC_PREVIEW_STUDIO', 425);
define('TRADITIONAL_PREVIEW_STUDIO', 428);
define('MODERN_PREVIEW_STUDIO', 426);

define('DUMMY_DATA_STUDIO', 436);
define('SENDGRID_APIKEY', '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');
define('SENDGRID_APIID', 'v_b3cyHrTAGqyw0bxORsEQ');
define('IS_LANGUAGE', 2);
define('CLASSIC_STYLE', 2);
define('MODERN_STYLE', 3);
define('TRADITIONAL_STYLE', 4);
if (IS_LIVE_FIRSTDATA) {
    define('FIRSTDATA_API_LOGIN', 'C97062-02');
    define('FIRSTDATA_API_KEY', 'c858kd7n90k2m8g50js5xnv7g6up3p56');
} else {
    define('FIRSTDATA_API_LOGIN', 'AI1578-05');
    define('FIRSTDATA_API_KEY', '4cfbpr5x3k9yux227g5a612p800xyae8');
}
//FFMPEG PATH
if (HOST_IP == '127.0.0.1') {
    define('FFMPEG_PATH', 'ffmpeg');
} else if (HOST_IP == '52.0.64.95') {
    define('FFMPEG_PATH', '/home/bitnami/bin/ffmpeg');
}else if (HOST_IP == '52.211.41.107') {
    define('FFMPEG_PATH', '/root/bin/ffmpeg');
} else {
    define('FFMPEG_PATH', '/home/exelanz/bin/ffmpeg');
}

define('DEMO_FIRSTDATA_API_LOGIN', 'AI1578-05');
define('DEMO_FIRSTDATA_API_KEY', '4cfbpr5x3k9yux227g5a612p800xyae8');
//Development server database
if (strpos($_SERVER['HTTP_HOST'], 'muvi.in')) {
    $mysql_db = 'studio';
    $mysql_user = 'studio';
    $mysql_pwd = '#%!!studio%&!%##';
    $db_muvi = 'muvi_data';
    $db_wiki = 'wiki_data';
    $hostip = '54.84.118.3';
    define('DOMAIN_COOKIE', 'studio.muvi.in');
    define("live_url", HTTP . 'muvi.com');
    define("website_url", HTTP . 'muvi.com');    
} 
//Staging server database
else if(DOMAIN == 'idogic'){
    $mysql_db = 'studiodev';
    $mysql_user = 'studiodev';
    $mysql_pwd = '#$1234stddev15$';   
    
    define('DOMAIN_COOKIE', 'studio.idogic.com');
    define("live_url", HTTP . 'idogic.com');
    define("website_url", HTTP . 'idogic.com');    
}
//Local database
else {
    $mysql_db = 'muviapp';
    $mysql_user = 'root';
    $mysql_pwd = '';
    $db_muvi = 'muvi_data';
    $db_wiki = 'wiki_data';
    $hostip = '54.84.118.3';
    define('DOMAIN_COOKIE', 'devstudio.gaya.com');
    define("live_url", HTTP . 'muvi.com');
    define("website_url", HTTP . 'muvi.com');
}
define('REPORT_INTERVAL',300);
define('APP_PRICE', 29);
$s3_ticket_attachments = 'stagingstudio';
define('CLOUDFRONT_URL', HTTP . 'd2gx0xinochgze.cloudfront.net/');
define('DEFAULT_STUDIO_THEME', '../../../bootstrap/views/');
define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/');
define('GA_ACCOUNT', 'developer@muvi.com');
define('GA_PASS', 'dev@muvi#');
define('RELEASE', 1);
defined('POSTER_HOST') ? '' : define('POSTER_HOST', "d2gx0xinochgze.cloudfront.net");
defined('POSTER_URL') ? '' : define('POSTER_URL', CDN_HTTP . POSTER_HOST . "/public");
defined('ASSETS_URL') ? '' : define('ASSETS_URL', CDN_HTTP . "d1yjifjuhwl7lc.cloudfront.net/public/assets/");
defined('OLD_POSTER_URL') ? '' : define('OLD_POSTER_URL', CDN_HTTP . "dfquahprf1i3f.cloudfront.net/public");
defined('TRAILER_URL') ? '' : define('TRAILER_URL', CDN_HTTP . 'd2gx0xinochgze.cloudfront.net');
defined('VIDEO_URL') ? '' : define('VIDEO_URL', CDN_HTTP . 'stagingstudio.s3.amazonaws.com');
defined('S3_TRAILER_BUCKET') ? '' : define('S3_TRAILER_BUCKET', 'stagingstudio');
define('internetSpeedSet1', 3);
define('internetSpeedSet2', 2);
define('internetSpeedSet3', 1.5);
define('internetSpeedSet4', 1);
define('internetSpeedSet5', 0.5);
define('internetSpeedSet6', 5);
define('internetSpeedSet7', 8);
define('EMAIL_LOGO', 'http://' . $_SERVER['SERVER_NAME'] . '/themes/signup/images/muvi_studio_logo_1.png');

define('RTMP_URL', 'rtmp://52.70.64.85/live/');
define('HLS_URL', 'https://video.muvi.com/hls/');
define('HLS_PATH', '/var/www/vhosts/video/hls/');
define('HUBSPOT_API_KEY', '79788211-3eb2-485d-ab5e-9fe523b7c1d3');//key of manas@muvi.com
define('MADMIMI_KEY', '00c5886c43742434d21572104450fb04');//key of manas@muvi.com
define('MADMIMI_EMAIL', 'manas@muvi.com');//madmimi email
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Muvi',
    'defaultController' => 'site',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.vendor.*',
        'ext.MyLinkPager'
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'sitemap' => array(
            'class' => 'ext.sitemap.SitemapModule', //or whatever the correct path is
            'absoluteUrls' => true,
            'protectedControllers' => array('admin', 'cron', 'report', 'login', 'payment', 'monetization', 'item', 'video', 'star', 'test', 'muvisdk', 'affiliate', 'notifypaypal', 'signup', 'rest', 'embed', 'adminceleb', 'template', 'mergedata', 'conversion', 'user','awss3','mrss'), //optional
            'protectedActions' => array('site/error', 'sdk/downloadsubmit', 'sdk/demoreques', '/user/testing', 'user/paynow', 'user/paydueamount', 'user/statistics', 'site/reload', 'site/muvilogin', 'user/paypalreturn', 'user/cancel', 'user/upload_image', 'site/pageList', '', 'user/index', 'movie/get_episodes', 'media/test', 'search/add', 'movie/comment_add', 'movie/filter_episode', 'media/getphpInfo', 'media/checkspeed', 'media/index', 'user/cancelsubscription', 'user/pay', 'user/success', 'user/transactions', 'user/transaction', 'site/register', 'site/login', 'site/logout', 'site/logmeout', 'site/authorized', 'site/details', 'site/change_css', 'site/addmoviedetails', 'user/forgotpassword', 'user/process', 'user/test', 'user/profileupdate'), //optional
            'priority' => '0.5', //optional
            'changefreq' => 'always', //optional
            'cacheId' => '', //optional
            'cachingDuration' => 3600,
        ),
        'comment' => array(
            'class' => 'ext.comment-module.CommentModule',
            'commentableModels' => array(
                // define commentable Models here (key is an alias that must be lower case, value is the model class name)
                'movie' => 'Movie'
            ),
            // set this to the class name of the model that represents your users
            'userModelClass' => 'User',
            // set this to the username attribute of User model class
            'userNameAttribute' => 'username',
            // set this to the email attribute of User model class
            'userEmailAttribute' => 'email',
        // you can set controller filters that will be added to the comment controller {@see CController::filters()}
        //'controllerFilters'=>array(),
        // you can set accessRules that will be added to the comment controller {@see CController::accessRules()}
        //'controllerAccessRules'=>array(),
        // you can extend comment class and use your extended one, set path alias here
        //'commentModelClass'=>'comment.models.Comment',
        ),
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'pass',
            'generatorPaths' => array(
                'bootstrap.gii',
            ),
        ),
    ),
    // application components
    'components' => array(
        'viewRenderer'=>array(
            'class'=>'application.vendor.Smarty.ESmartyViewRenderer',
            'fileExtension' => '.html',
        ),         
        'request' => array(
            'hostInfo' => $baseurl,
        ),
        'common' => array('class' => 'UserFunction'),
        'billing' => array('class' => 'Billing'),
        'Helper' => array('class' => 'Helper'),
        'aws'=>array('class'=>'AWS'),
        'pdf'=>array('class'=>'MYPDF'),
        'email'=>array('class'=>'MyEmail'),        
        'general' => array('class' => 'GeneralFunction'),
                'muvipayment' => array('class' => 'MuviPayment'),
        'custom' => array('class' => 'CustomFunction'),
        'mvsecurity' => array('class' => 'Mvsecurity'),
        'zip' => array(
            'class' => 'application.extensions.zip.EZip',
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        /* 'session' => array(
          'class' => 'CDbHttpSession',
          'timeout' => 24*3600,
          ), */
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'class' => 'MyUrlManager',
            'showScriptName' => false,
            'urlFormat' => 'path',
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=' . $mysql_db,
            'username' => $mysql_user,
            'password' => $mysql_pwd,
            'charset' => 'UTF8',
            'autoConnect' => true,
        ),
        'cache' => array(
            'class' => 'system.caching.CDbCache',
            'connectionID' => 'db'
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'pg_port' => ($_SERVER['HTTP_HOST'] != 'localhost') ? '5432' : '5432',
        's3_key' => 'AKIAJ4HTSCRXGHFTKRBQ',
        's3_secret' => '7qsZSeYDFueNwFA5m07T/5Oil1WHlzW/Yj7S16aq',
        's3_bucketname' => 'stagingstudio',
        'video_bucketname' => 'stagingstudio',
        's3_ticket_attachments' => $s3_ticket_attachments,
        'celeb_img_thumb' => HTTP . 'dfquahprf1i3f.cloudfront.net/public/system/profile_pictures/'
    ),
);
