<?php

require_once 'Paypal/PaypalRecurringPaymentProfile.php';

class ApipaypalController extends Controller {
    
    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }
    
    /**
     *
     * Initialize the api
     * @return payPalExpress Object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() 
    {    
        $payPalExpress = new PaypalExpress($this->PAYMENT_GATEWAY_API_USER,$this->PAYMENT_GATEWAY_API_PASSWORD,$this->PAYMENT_GATEWAY_API_SIGNATURE,75.0,$this->IS_LIVE_API_PAYMENT_GATEWAY);
        return $payPalExpress;
    }
    
    /**
     *
     * Take Customer to PayPal for Payment
     * @return redirect to PayPal for Account Information
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCard($arg = array())
    {
        $payPalExpress = self::initializePaymentGateway();
        $resArray = $payPalExpress->CallShortcutExpressCheckout ($arg['paymentAmount'], $arg['email'], $arg['currencyCodeType'], $arg['paymentType'], $arg['paymentDesc'], $arg['returnURL'], $arg['cancelURL'], $arg['have_paypal'], $arg['address']);
        if($resArray['TOKEN']){
           $payPalExpress->RedirectToPayPal($resArray["TOKEN"] );
        }
    }
    
    /**
     *
     * Get Customer's Profile Details
     * @return payPalExpress Object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function paymentTransaction($token)
    {
        $payPalExpress = self::initializePaymentGateway();
        $resArray = $payPalExpress->GetShippingDetails( $token );
        return $resArray;
    }
    
    /**
     *
     * Create Customer's Recurring Profile
     * @return payPalExpress Object (Create a Recurring Payment Profile)
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function paymentTransactions($user = array())
    {
        $payPalExpress = self::initializePaymentGateway();
        $resArray = $payPalExpress->CreateRecurringPaymentsProfile($user['token'], $user['email'], $user['plan_desc'], $user['recurrence'], $user['frequency'], $user['charged_amount'], $user['currency'], $user['trialperiod'], $user['trialfrequency'], $user['trialtotalcysles'], $user['trial_amount']);
        return $resArray;
    }
      
    /**
     * 
     * Initialize the api
     * @return PaypalRecurringPaymentProfile Object (Profile Details of a Recurring Payment Profile)
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
   /* function recurringPayment($profileid)
    {
        $payPal = new PaypalRecurringPaymentProfile($this->PAYMENT_GATEWAY_API_USER,$this->PAYMENT_GATEWAY_API_PASSWORD,$this->PAYMENT_GATEWAY_API_SIGNATURE,75.0,$this->IS_LIVE_API_PAYMENT_GATEWAY);
        $result = $payPal->getProfileDetails($profileid);
        echo '<pre>';
        print_r($result);exit;
    }*/
    
    /**
     * 
     * Cancel Customer's Recurring PayPal Account
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null) {
        $user_profile_id = $usersub->profile_id;
        $payPalExpress = self::initializePaymentGateway();
        $data = '';
        if (isset($user_profile_id) && $user_profile_id != '') {
            try {
                $data = $payPalExpress->manageProfileStatus($user_profile_id, 'Cancel', 'Profile was Cancelled!'); 
            } catch (Exception $e) {
                
            }
        }        
        return $data;
    }
    
    /**
     * 
     * Signle payment with express checkout (generally for ppv plans)
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processTransactions($paymentInfo = array())
    {
        $payPalExpress = self::initializePaymentGateway();
        $resArray = $payPalExpress->SetExpressCheckout($paymentInfo);
        if($resArray['TOKEN']){
           $payPalExpress->RedirectToPayPal($resArray["TOKEN"] );
        }
    }
    
    /**
     * 
     * Get Details of transaction
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCheckoutDetails($token)
    {
        $payPalExpress = self::initializePaymentGateway();
        $resArray = $payPalExpress->GetExpressCheckoutDetails($token);
        return $resArray;
    }
    
    /**
     * 
     * Do payment
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processDoPayment($paymentInfo = array())
    {
        $payPalExpress = self::initializePaymentGateway();
        $resArray = $payPalExpress->DoExpressCheckoutPayment($paymentInfo);
        return $resArray;
    }
       
}