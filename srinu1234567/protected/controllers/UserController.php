<?php

require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Aws\Ses\SesClient;

class UserController extends Controller {

    public function init() {
        require_once(dirname(__FILE__) . '/lib/Twocheckout.php');
        require_once(dirname(__FILE__) . '/ec/ecfunctions.php');
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        if (isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        parent::init();
    }

    public function actionIndex() {
        $this->render('index');
    }

    /*
      public function actionAccount() {
      if(isset(Yii::app()->user->id) && Yii::app()->user->id > 0)
      {
      //$this->redirect(Yii::app()->user->returnUrl);
      $this->redirect(Yii::app()->baseUrl.'/user/process');
      }
      else
      {
      $this->render('account');
      }
      }
     * 
     */

    function actionLogout() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $login_history_cookie_name = "login_history_id_" . $studio_id . "_" . $user_id;
        if (isset(Yii::app()->user->login_id) && Yii::app()->user->login_id != '') {
            $login = StudioLoginHistory::model()->findByPk(Yii::app()->user->login_id);
            $login->logout_at = new CDbExpression("NOW()");
            $login->save();
        }

        if (isset(Yii::app()->session['login_history_id']) && Yii::app()->session['login_history_id'] != '') {
            $login_history = LoginHistory::model()->findByPk(Yii::app()->session['login_history_id']);
            $login_history->logout_at = date('Y-m-d H:i:s');
            $login_history->save();
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//", $domain);
            $domain = $domain[1];
            if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                setcookie($login_history_cookie_name, null, time() - 3600, '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
            }
        }
        if (Yii::app()->general->getStoreLink()) {
            PGCart::model()->deleteAll("studio_id =:studio_id AND user_id=:user_id", array(':studio_id' => $this->studio->id, ':user_id' => Yii::app()->user->id));
            if (!empty($_SESSION["cart_item"])) {
                self::saveCartToDB($_SESSION["cart_item"]);
            }
        }
        $this->clearFacebookCookie();
        setcookie('videoUrl', '', time() - 36000);
        unset($_SESSION['internetSpeed']);

        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();
        // Wpuser Logout
        if(WP_USE_THEMES == 1) {
        wp_logout();
        }
        if (@$has_passcode == 1) {
            session_start();
            $_SESSION['has_valid_passcode'] = 1;
        }
        //Yii::app()->request->cookies->clear();
        $this->redirect(Yii::app()->getBaseUrl(true));
    }

    public function actionLogin() {

        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->getbaseUrl(true));
        } else {
            if (@$studio->is_csrf_enabled) {
                if (@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']) {
                    unset($_SESSION['csrfToken']);
                } else {
                    $url = Yii::app()->createAbsoluteUrl();
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: ' . $url);
                    exit();
                }
            }
            //set cookie for zopim testing
            setcookie('studiouserloggedin', '', time() + 36000);
            $studio_id = Yii::app()->common->getStudiosId();
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            $meta = Yii::app()->common->pagemeta('login');
            if ($meta['title'] != '') {
                $title = $meta['title'];
            } else {
                /* $temp = Yii::app()->request->url;
                  $temp = explode('/', $temp);
                  $permalink = $temp[count($temp)-1];
                  $type = explode('-', $permalink);
                  $default_title = implode(' ', $type);
                  $default_title = ucwords($default_title);
                  $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);

                  // echo $Words;exit;
                  $title = $Words. ' | '. $studio->name; */
                $title = 'Login | ' . $studio->name;
            }

            if ($meta['description'] != '') {
                $description = $meta['description'];
            } else {
                $description = '';
            }
            if ($meta['keywords'] != '') {
                $keywords = $meta['keywords'];
            } else {
                $keywords = '';
            }
            eval("\$title = \"$title\";");
            eval("\$description = \"$description\";");
            eval("\$keywords = \"$keywords\";");
            $this->pageTitle = $title;
            $this->pageDescription = $description;
            $this->pageKeywords = $keywords;
            
            $getLoginwith = Yii::app()->db->createCommand()
                    ->select('id, studio_id, field_type')
                    ->from('unique_access_fields')
                    ->where('studio_id=:studio_id', array(':studio_id' => $studio_id))
                    ->queryRow();

            $this->render('login', array('user_id' => $user_id, 'agent' => $agent, 'chkLogin' => $getLoginwith['field_type']));
        }
    }

    public function actionChecklogin() {
        $this->layout = false;
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) {
            echo true;
        } else {
            echo false;
        }
    }

    public function actionDologin() {
        /*         * LOC for restricted user acces by IP and user name by Suraja** */
        $restricted_user = new Restriction();
        $restricted_userModel = Restriction::model()->findAll();

        foreach ($restricted_userModel as $restricted_userModel) {
            $ip_list = $restricted_userModel->ip;
            $user_list = $restricted_userModel->email;
        }
        $explode_userlist = explode(',', $user_list);
        $explode_iplist = explode(',', $ip_list);
        $ip = gethostbyname($_SERVER['REMOTE_ADDR']);

        if (in_array($_REQUEST['LoginForm']['email'], $explode_userlist) || in_array($ip, $explode_iplist)) {

            Yii::app()->session['block_ga'] = 0;
        } else {

            Yii::app()->session['block_ga'] = 1;
        }

        // echo Yii::app()->session['block_ga'];
        // exit;

        /*         * LOC for restricted user acces by IP and user name by Suraja** */
        $model = new LoginForm;
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];

            if ($model->loginuser()) {
                $studio_id = $std_id = Yii::app()->common->getStudiosId();

                //Added By SNL: Check plan and payment gateway are exists or not
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($std_id);

                $activate = 0; //No plan and payment gateway set by studio
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $activate = 1;
                }//End of Payment Gateway

                $usr = Yii::app()->common->getSDKUserInfo(Yii::app()->user->id);
                $subscription = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                if ($usr->add_video_log) {
                    $ip_address = CHttpRequest::getUserHostAddress();
                    $user_id = Yii::app()->user->id;
                    $login_at = date('Y-m-d H:i:s');
                    $login_history = new LoginHistory();
                    $login_history->studio_id = $std_id;
                    $login_history->user_id = $user_id;
                    $login_history->login_at = $login_at;
                    $login_history->logout_at = '0000-00-00 00:00:00';
                    $login_history->ip = $ip_address;
                    $login_history->last_login_check = $login_at;
                    $login_history->save();
                    Yii::app()->session['login_history_id'] = $login_history->id;
                    $this->setLoginHistoryCookie($login_history->id);
                    /* if(Yii::app()->custom->hasOtherLogins() == true){
                      Yii::app()->user->setFlash('success', 'You were logged in from another browser. Logging out from there.');
                      } */
                }

                $action = 'home';
                if (isset($usr) && !empty($usr) && intval($usr->is_deleted) && intval($activate)) {
                    if ($subscription == '') {
                        $action = 'reactivate';
                    }
                } else if (intval($activate)) {
                    if ($subscription == '') {
                        //$action = 'activate';
                    }
                }

                $array = array("login" => "success", "action" => $action);
                //Yii::app()->user->setFlash("success", "Successfully logged in.");
                $json = json_encode($array);
                echo $json;
                Yii::app()->end();
            } else {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }
    }

    public function actionRegister() {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        if (isset($_REQUEST['error'])) {
            Yii::app()->user->setFlash('error', $_REQUEST['error']);
        }

        $getStudioApi = Yii::app()->db->createCommand()
                ->select('e.id,e.studio_id,s.id,s.studio_id')
                ->from('external_api_keys e')
                ->join('studio_api_details s', 'e.id=s.api_type_id')
                ->where('s.studio_id=:id AND s.module=:module', array(':id' => $studio_id, ':module' => 'register'))
                ->queryRow();


        if (Yii::app()->user->id) {
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                if (Yii::app()->common->isSubscribed(Yii::app()->user->id))
                    $this->redirect(Yii::app()->user->returnUrl);
                else
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/activate');
            } else
                $this->redirect(Yii::app()->user->returnUrl);
            //$this->redirect(Yii::app()->request->urlReferrer);
        } else {
            $meta = Yii::app()->common->pagemeta('register');
            if ($meta['title'] != '') {
                $title = $meta['title'];
            } else {
                /* $temp = Yii::app()->request->url;
                  $temp = explode('/', $temp);
                  $permalink = $temp[count($temp)-1];
                  $type = explode('-', $permalink);
                  $default_title = implode(' ', $type);
                  $default_title = ucwords($default_title);

                  $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                  // echo $Words;exit;
                  $title = $Words. ' | '. $studio->name; */
                $title = 'Register | ' . $studio->name;
            }

            if ($meta['description'] != '') {
                $description = $meta['description'];
            } else {
                $description = '';
            }
            if ($meta['keywords'] != '') {
                $keywords = $meta['keywords'];
            } else {
                $keywords = '';
            }
            eval("\$title = \"$title\";");
            eval("\$description = \"$description\";");
            eval("\$keywords = \"$keywords\";");
            $this->pageTitle = $title;
            $this->pageDescription = $description;
            $this->pageKeywords = $keywords;

            //$free_month_end = date('d/m/Y',strtotime(date('m/d/Y') . "+1 month"));
            //Plans
            $isCouponExists = 0;
            $gateways = array();
            $plans = array();
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $gateways = $plan_payment_gateway['gateways'];
                $plans = $plan_payment_gateway['plans'];

                if (!empty($plans)) {
                    $default_plan_id = @$plans[0]->id;
                    foreach ($plans as $key => $value) {
                        if ($value->is_default) {
                            $default_plan_id = $value->id;
                        }
                    }
                }

                $isCouponExists = Yii::app()->billing->isCouponExistsForSubscription($studio_id);
            }

            $custom_fields = Yii::app()->general->getCustomFields($studio_id, 1);
            $hide_terms = StudioConfig::model()->getconfigvalue('hide_terms');
            if ($hide_terms['config_value'] == 1)
                $hide_terms = 1;
            else
                $hide_terms = 0;

            $signup_steps = Yii::app()->general->signupSteps();
            $payment_form = Yii::app()->general->payment_form($gateways, $plans);
            if($gateways[0]->short_code == 'ippayment'){
                $payment_register_btn = Yii::app()->general->payment_next_btn($signup_steps);
            }else{
            $payment_register_btn = Yii::app()->general->payment_register($gateways, $plans);
            }
            if (!empty($getStudioApi)) {
                $this->render('register_step', array('gateways' => $gateways, 'plans' => $plans, 'default_plan_id' => $default_plan_id, 'studio_id' => $studio_id, 'payment_form' => $payment_form, 'payment_register_btn' => $payment_register_btn, 'custom_fields' => $custom_fields, 'hide_terms' => $hide_terms, 'isCouponExists' => $isCouponExists, 'is_api' => 1));
            } else if ($signup_steps == 1 && count($gateways) > 0 && count($plans) > 0) {
                $this->render('register', array('gateways' => $gateways, 'plans' => $plans, 'default_plan_id' => @$default_plan_id, 'studio_id' => $studio_id, 'payment_form' => $payment_form, 'payment_register_btn' => $payment_register_btn, 'custom_fields' => $custom_fields, 'hide_terms' => $hide_terms, 'isCouponExists' => $isCouponExists));
            } else {
                $this->render('register_step', array('gateways' => $gateways, 'plans' => $plans, 'default_plan_id' => $default_plan_id, 'studio_id' => $studio_id, 'payment_form' => $payment_form, 'payment_register_btn' => $payment_register_btn, 'custom_fields' => $custom_fields, 'hide_terms' => $hide_terms, 'isCouponExists' => $isCouponExists));
            }
        }
    }

    public function actionCheckemail() {
        $isExists = 0;
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_REQUEST) && isset($_REQUEST['email']) && filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL) != false) {
            $email = filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL);
            $user = new SdkUser;
            $users = $user->findAllByAttributes(array('email' => $email, 'studio_id' => $studio_id));
            if (isset($users) && !empty($users)) {
                $isExists = 1;
            }
        }

        $res = array('isExists' => $isExists);
        echo json_encode($res);
    }

    public function actionCheckmobile() {
        $isExists = 0;
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_REQUEST) && isset($_REQUEST['mobile_number'])) {
            $mobile_number = $_REQUEST['mobile_number'];
            $user = new SdkUser;
            $users = $user->findAllByAttributes(array('mobile_number' => $mobile_number, 'studio_id' => $studio_id));
            $checkTempMobile = TempSdkUser::model()->findByAttributes(array('mobile_number' => $mobile_number, 'studio_id' => $studio_id));
            if ((isset($users) && !empty($users)) ||  isset($checkTempMobile['mobile_number'])) {
                $isExists = 1;
            }
        }
        echo $isExists;
        exit;
    }

    public function actionProcessCard() {
        $data = '';
        if (isset($_POST) && !empty($_POST)) {
            $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
            $_POST['gateway_code'] = $gateway_code;
            
            if(isset($this->API_ADDONS_REDIRECT[$gateway_code]['redirect']) && intval($this->API_ADDONS_REDIRECT[$gateway_code]['redirect'])){
                $form_data = array();
                parse_str($_POST['form_data'], $form_data);
                $_POST = array();
                $_POST = $form_data['data'];
                Yii::app()->session['redirect_form_data'] = $_POST;
            }
            
            $studio_id = Yii::app()->common->getStudiosId();

            if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
                $user = SdkUser::model()->findByPk(Yii::app()->user->id);
                $_POST['email'] = $user->email;
            }
            $_POST['user_id'] = Yii::app()->user->id ? Yii::app()->user->id : $studio_id;
            $plan_details = '';
            $price = 0;
            $currency_id = 0;
            if (isset($_POST['plan_id']) && intval($_POST['plan_id'])) {
                 $country = (isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION[$studio_id]['country'] : '';
                $plan_details = SubscriptionPlans::model()->getPlanDetails($_POST['plan_id'], $studio_id, '', $country);
                $price = $plan_details['price'];
                if ($_POST['discount_amount'] != '') {
                    $price = $_POST['discount_amount'];
                }
                $trail_period = $plan_details['trial_period'];
                if ($_POST['coupon'] != '') {
                    $trail_period = 0;
                    if ($_POST['new_free_trail'] != '') {
                        $trail_period = $_POST['new_free_trail'];
                    }
                }
                $currency_id = $plan_details['currency_id'];
                if (trim($currency_id)) {
                    $currency = Currency::model()->findByPk($currency_id);
                } else {
                    $currency = Currency::model()->findByPk($_POST['currency_id']);
                }
            }
            if (isset($_POST['subscriptionbundles_plan_id']) && intval($_POST['subscriptionbundles_plan_id'])) {
                //$plan_details = SubscriptionPlans::model()->getPlanDetails($_POST['subscriptionbundles_plan_id'], $studio_id);
                $planDetails = Yii::app()->common->getAllSubscriptionBundlesPrices($_POST['subscriptionbundles_plan_id'], $default_currency_id, $studio_id);
                $currency_id = (isset($planDetails[0]['currency_id']) && trim($planDetails[0]['currency_id'])) ? $planDetails[0]['currency_id'] : $default_currency_id;
                $currency = Currency::model()->findByPk($currency_id);
                $price = $planDetails[0]['price'];
		 if(isset($_POST["coupon"]) && trim($_POST["coupon"])){
                    $data = array();
                    $data['studio_id'] = $studio_id;
                    $data['coupon'] = $_POST["coupon"];
                    $data['plan'] = $_POST["subscriptionbundles_plan_id"];
                    $data['currency'] = $_POST["currency_id"];
                    $user_id = Yii::app()->user->id;
                    $data['user_id'] = $user_id;
                    //getting the subscription bundles plan details
                    $planDetails['currency_id']= $data['currency'] = $currency_id;
                    $planDetails['couponCode']=$_POST["coupon"];
                    $planDetails['physical']=$_POST["physical"];
                    $couponDetails = Yii::app()->billing->isValidCouponForSubscription($data);
                    $res = Yii::app()->billing->CouponSubscriptionBundlesCalculation($couponDetails,$planDetails);
                    $price = $res['discount_amount']; 
                    $data['extend_free_trail'] =$res['extend_free_trail'];
                } 
			
                if(trim($currency_id)){
                    $currency = Currency::model()->findByPk($currency_id);
                } else {
                    $currency = Currency::model()->findByPk($_POST['currency_id']);
                }
            }
            
            $_POST['currency_code'] = $currency->code;
            $_POST['currency_id'] = $_POST['currency_id'];
            
            if (isset($this->PAYMENT_GATEWAY) && array_key_exists('paypalpro', $this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY['paypalpro'] == 'paypalpro') {
                $_POST['billingPeriod'] = $plan_details['recurrence'];
                $_POST['billingFreq'] = $plan_details['frequency'];
                $_POST['amount'] = $price;
                
                $_POST['trialBillingPeriod'] = $plan_details['trial_recurrence'];
                $_POST['trialBillingFreq'] = $plan_details['trial_period'];
                $_POST['trialAmount'] = 0.00;
                $_POST['trialTotalBillingCycles'] = 1;
                if (isset($_POST['havePaypal']) && $_POST['havePaypal']) {
                    $_POST['isSuccess'] = 1;
                    $_POST['payment_method'] = 'paypalpro';
                    echo json_encode($_POST);
                    exit;
                }
            }
            if (isset($this->PAYMENT_GATEWAY) && array_key_exists('paygate', $this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY['paygate'] == 'paygate') {
                $_POST['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/payGateReturn";
                $_POST['cancelURL'] = Yii::app()->getBaseUrl(true) . "/user/cancel";
                $_POST['plan_desc'] = $plan_details['name'] . $price;
                $_POST['isSuccess'] = 1;
                $_POST['payment_method'] = 'paygate';
                echo json_encode($_POST);
                exit;
            }
            
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            if ($this->IS_PAYPAL_EXPRESS['paypalpro'] == 1) {
            $payment_gateway_controller = 'ApipaypalproController';    
            }
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::processCard($_POST);
            //If subscription free trial is zero day, then charge instantly
            $res = json_decode($data, true);
            if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                if (isset($plan_details) && !empty(array_filter($plan_details))) {
                    if(isset($this->API_ADDONS_REDIRECT[$gateway_code]['redirect']) && intval($this->API_ADDONS_REDIRECT[$gateway_code]['redirect'])){
                        echo json_encode($res);
                         exit;
                    }else{
                    if (intval($trail_period) == 0) {
                        if ($price <= 0 && isset($_POST['plan_id']) && intval($_POST['plan_id'])) {
                            //$trans_data = Yii::app()->billing->setPpvTransaction($plan,$data,'',$trans_data, '');
                            $trans_data['transaction_status'] = 'succeeded';
                            $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                            $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                            $trans_data['dollar_amount'] = 0;
                            $trans_data['amount'] = 0;
                            $trans_data['response_text'] = '';
                            
                            $data = json_decode($data, true);
                            $data['transaction_data'] = $trans_data;
                            $data = json_encode($data);
                        }else if($price <= 0 && intval($_POST["subscriptionbundles_plan_id"])){
                            //$trans_data = Yii::app()->billing->setPpvTransaction($plan,$data,'',$trans_data, '');
                            $trans_data['transaction_status'] = 'succeeded';
                            $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                            $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                            $trans_data['dollar_amount'] = 0;
                            $trans_data['amount'] = 0;
                            $trans_data['response_text'] = '';
                            $data = json_decode($data, true);
                            $data['transaction_data'] = $trans_data;
                            $data = json_encode($data);
                        }else{
                        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypalpro') {
                            $user = array();
                            //Prepare an array to charge from given card
                            $user['currency_id'] = $currency->id;
                            $user['currency_code'] = $currency->code;
                            $user['currency_symbol'] = $currency->symbol;
                            $user['amount'] = $price;
                            $user['token'] = @$res['card']['token'];
                            $user['profile_id'] = @$res['card']['profile_id'];
                            $user['card_holder_name'] = $_POST['card_name'];
                            $user['email'] = $_POST['email'];
                            $user['user_id'] = Yii::app()->user->id;
                            $user['card_type'] = $res['card']['card_type'];
                            $user['exp_month'] = $_POST['exp_month'];
                            $user['exp_year'] = $_POST['exp_year'];
                            $user['gateway_code'] = $gateway_code;
                                $user['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                            $trans_data = $payment_gateway::processTransactions($user);

                            if (intval($trans_data['is_success'])) {
                                $data = json_decode($data, true);
                                $data['transaction_data'] = $trans_data;
                                $data = json_encode($data);
                            } else {
                                $data = json_encode($trans_data);
                            }
                        }
                    }
                }
                }
            }
        }
        }
        
        echo $data;
        exit;
    }

    public function actionSaveuser() {
        if (@$studio->is_csrf_enabled) {
            if (@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']) {
                unset($_SESSION['csrfToken']);
            } else {
                if (!Yii::app()->request->isAjaxRequest) {
                    $url = Yii::app()->createAbsoluteUrl('user/register');
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: ' . $url);
                    exit();
                } else {
                    echo '';
                    exit;
                }
            }
        }
        $lang_code = $this->language_code;
        //if (isset($_REQUEST['data']) && !empty($_REQUEST['data']) && isset($_REQUEST['data']['email']) && filter_var($_REQUEST['data']['email'], FILTER_VALIDATE_EMAIL) != false) {
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data']) && (isset($_REQUEST['data']['email']) && filter_var($_REQUEST['data']['email'], FILTER_VALIDATE_EMAIL) != false) || !isset($_REQUEST['data']['email'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $studio_name = $this->studio->name;
            $name = trim($_REQUEST['data']['name']);
            $email = filter_var($_REQUEST['data']['email'], FILTER_VALIDATE_EMAIL);
            $password = $_REQUEST['data']['password'];
            $gateway_code = $_REQUEST['data']['payment_method'];
            $signup_steps = Yii::app()->general->signupSteps();

            if ($_REQUEST['data']['is_api'] && !empty($_REQUEST['data']['is_api'])) {
                require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
                $mobile_number = $_REQUEST['data']['mobile_number'];
                $chk = new clickHereApi();
                $uniqueID = $chk->getUniqueID();
                $_REQUEST['data']['unique_id'] = $uniqueID;
                $message = $this->Language['register_api_body_msg'];

                /* save tempsdkuser */
                $ret = TempSdkUser::model()->saveTempSdkUser($_REQUEST['data'], $studio_id);

                $getApiDetails = Yii::app()->db->createCommand()
                        ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                        ->from('external_api_keys e')
                        ->join('studio_api_details s', 'e.id=s.api_type_id')
                        ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module' => 'register'))
                        ->queryRow();



                $chk = $chk->doRegistration($mobile_number, $message, $uniqueID, $getApiDetails);

                if ($chk['code'] === 202) {
                    Yii::app()->user->setFlash('success', $this->Language['api_register_success']);
                } else {
                    Yii::app()->user->setFlash('error', $this->Language['api_subscribe_failed']);
                }

                $this->redirect(array('/user/register'));
                exit;
            }



            if ($signup_steps == 1) {
                $_REQUEST['PAYMENT_GATEWAY'] = $this->PAYMENT_GATEWAY[$gateway_code];
                $_REQUEST['GATEWAY_ID'] = $this->GATEWAY_ID[$gateway_code];
                $_REQUEST['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
            }

            $is_paypal = 0;
            if ((isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'paypal') || (isset($_REQUEST['data']['havePaypal']) && $_REQUEST['data']['havePaypal'])) {
                $is_paypal = 1;
            }

            if (isset(Yii::app()->request->cookies['SITE_REFERRER']) && trim(Yii::app()->request->cookies['SITE_REFERRER'])) {
                $source = Yii::app()->request->cookies['SITE_REFERRER'];
            }

            $_REQUEST['source'] = $source;

            if (isset($_REQUEST['data']['havePaypal']) && $_REQUEST['data']['havePaypal']) {
                if (isset($_REQUEST['data']['plan_id']) && $_REQUEST['data']['plan_id']) {
                    Yii::app()->session['from_data'] = $_REQUEST;
                    $this->actionPaynow($_REQUEST);
                    exit;
                }
            }

            if (isset($_REQUEST['data']['payment_method']) && $_REQUEST['data']['payment_method'] == 'paygate') {
                if (isset($_REQUEST['data']['plan_id']) && $_REQUEST['data']['plan_id']) {
                    Yii::app()->session['from_data'] = $_REQUEST;
                    $this->actionPaynowPayGate($_REQUEST);
                    exit;
                }
            }
            $price_list = Yii::app()->common->getUserSubscriptionPrice($_REQUEST['data']['plan_id'], $_REQUEST['data']['currency_id']);
            $plan_price = $price_list['price'];
            $ret = SdkUser::model()->saveSdkUser($_REQUEST, $studio_id, $is_paypal);

            if ($ret) {
                if (@$ret['error']) {
                    Yii::app()->user->setFlash('error', $ret['error']);
                    $this->redirect($this->createUrl('register'));
                    exit;
                }
                if (!$is_paypal) {
                    // Send welcome email to user
                    if (@$ret['is_subscribed']) {

                        $file_name = '';
                        //Generate pdf for instant payment when zero free trial
                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id']);
                            if (isset($_REQUEST['data']['coupon_code']) && $_REQUEST['data']['coupon_code'] != "") {
                                $discount_price = $plan_price - $_REQUEST['data']['transaction_data']['amount'];
                                $user['discount_amount'] = Yii::app()->common->formatPrice($discount_price, $_REQUEST['data']['currency_id']);
                                $user['full_amount'] = Yii::app()->common->formatPrice($plan_price, $_REQUEST['data']['currency_id']);
                                $user['coupon'] = $_REQUEST['data']['coupon_code'];
                            }
                            $user['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }

                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name, '', '', '', '', '', $lang_code, $studio_name);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email_with_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $ret['user_id'], '', 0);
                        }
                    } else {
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email', '', '', '', '', '', '', $lang_code, $studio_name);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email', $studio_id);
                        if ($isEmailToStudio) {
                            $admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'admin_new_user_registration', $ret['user_id'], '', 0);
                        }
                    }
                }
            }

            //Validate user identity
            //$identity = new UserIdentity($email, $password);
            //$identity->validate();
            //Yii::app()->user->login($identity);
            //$user_id = Yii::app()->user->id;
            $model = new LoginForm;
            $model->attributes = $_POST['data'];
            if ($model->loginuser()) {
                if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
                    $user_id = Yii::app()->user->id;
                    $usr = Yii::app()->common->getSDKUserInfo($user_id);
                    if ($usr->add_video_log) {
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $login_at = date('Y-m-d H:i:s');
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = $login_at;
                        $login_history->logout_at = '0000-00-00 00:00:00';
                        $login_history->ip = $ip_address;
                        $login_history->last_login_check = $login_at;
                        $login_history->save();
                        Yii::app()->session['login_history_id'] = $login_history->id;
                        $this->setLoginHistoryCookie($login_history->id);
                    }

                    if ($signup_steps == 2 && isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != '') {
                        //$this->actionActivate();                
                        $this->redirect(Yii::app()->getbaseUrl(true) . '/user/activate');
                        exit();
                    } else if ($signup_steps == 2 && !empty(Yii::app()->common->isPaymentGatwayExists($studio_id))) {
                        $this->redirect(Yii::app()->getbaseUrl(true) . '/user/activate');
                        exit();
                    } else {
                        Yii::app()->user->setFlash('success', $this->ServerMessage['thanks_for_register']);
                        $this->redirect(Yii::app()->getbaseUrl(true));
                        exit;
                    }
                }
            } else {
                Yii::app()->user->setFlash('success', $this->ServerMessage['thanks_for_register']);
                $this->redirect(Yii::app()->getbaseUrl(true));
                exit;
            }
        } else {
            $err_msg = $this->ServerMessage['error_process_payment'];
            $this->redirect(Yii::app()->getbaseUrl(true) . '/user/register?error=' . urlencode($err_msg));
        }
    }

    public function actionActivate() {
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        $lang_code = $this->language_code;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $user_id = Yii::app()->user->id;
            $data = $_REQUEST['data'];

            Yii::app()->session['from_data'] = $_REQUEST;
            $gateway_code = $_REQUEST['data']['payment_method'];
            //Check studio has plan or not
            $start_date = $end_date = Null;
            $plan_id = $plan_price = $payment_gateway_id = 0;
            $currency_id = (isset($_REQUEST['data']['currency_id']) && intval($_REQUEST['data']['currency_id'])) ? $_REQUEST['data']['currency_id'] : $this->studio->default_currency_id;

            if ((isset($_REQUEST['data']['plan_id']) && intval($_REQUEST['data']['plan_id']))) {
                $plan_id = $_REQUEST['data']['plan_id'];
                //Get plan detail which user selected at the time of registration
                $planModel = new SubscriptionPlans();
                $plan = $planModel->findByPk($plan_id);
                $price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $plan_price = $price_list['price'];


                $trail_period = $plan->trial_period;
                $couponCode = "";
                if ($_REQUEST['data']['coupon_code'] != '') {
                    $couponCode = $_REQUEST['data']['coupon_code'];
                    $trail_period = $plan->trial_period;
                    if ($_REQUEST['data']['free_trail_charged'] != '') {
                        $trail_period = $_REQUEST['data']['free_trail_charged'];
                    }
                    if(intval($trail_period) > 0){
                        $use_discount = 1;
                    }
                }
                $discount_price = '';
                if ($trail_period > 0) {
                    if ($_REQUEST['data']['discount_amount'] != '') {
                        $discount_price = $_REQUEST['data']['discount_amount'];
                    }
                }

                $is_transaction = 0;
                if (isset($trail_period) && (intval($trail_period) == 0) && isset($_REQUEST['data']['transaction_data']) && !empty($_REQUEST['data']['transaction_data'])) {
                    $is_transaction = 1;
                    $use_discount = 0;
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                } else {
                    if ($_REQUEST['data']['free_trail_charged'] != '') {
                    $trail_recurrence = 'day';
                     }else if($plan->trial_recurrence != ''){
                        $trail_recurrence = $plan->trial_recurrence;
                    }
                    $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
                $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);
            }

            //Getting Ip address
            $ip_address = Yii::app()->request->getUserHostAddress();

            //Save card information if payment gateway is not manual
            $card_id = 0;
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paygate' || $this->PAYMENT_GATEWAY[$gateway_code] == 'paypal' || ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && $_REQUEST['data']['havePaypal'])) {
                    $connection = Yii::app()->db;
                    $agent = Yii::app()->common->getAgent();
                    $plan_id = isset($_REQUEST['data']['plan_id']) ? $_REQUEST['data']['plan_id'] : 0;
                    $company_id = STUDIO_USER_ID;
                    $ip_address = Yii::app()->common->getIP();
                    $payment_method = isset($_REQUEST['data']['payment_method']) ? $_REQUEST['data']['payment_method'] : '';
                    $payment_details = Yii::app()->common->getPaymentDetails($agent);

                    $msg = "Some issue in processing the payment. Please try later.";
                    $transaction_id = 0;

                    $data['email'] = isset($_REQUEST['data']['email']) ? $_REQUEST['data']['email'] : '';

                    $price = '0.00';
                    $startup_discount = '0.00';
                    $plan_name = '';
                    $duration = 'Never';
                    $recurrence = '';

                    $currency = Currency::model()->findByPk($currency_id);

                    $data['currencyCodeType'] = $currency->code;
                    $data['currency_code'] = $currency->code;
                    $data['currency_symbol'] = $currency->symbol;

                    $qry = "SELECT *,sp.id as sub_plan_id FROM subscription_plans sp  LEFT JOIN subscription_pricing sup ON (sp.id = sup.subscription_plan_id) WHERE sp.id = '" . $plan_id . "' AND sp.studio_id = '" . $studio_id . "' AND sp.status = '1'";
                    $command = $connection->createCommand($qry);
                    $rowCount = $command->execute(); // execute the non-query SQL
                    $plans = $command->queryAll(); // execute a query SQL
                    //print '<pre>';print_r($plans);exit;
                    if (count($plans)) {
                        foreach ($plans as $plan) {
                            if ($plan_id == $plan['sub_plan_id'] && $plan['currency_id'] == $currency_id) {
                                $price = $plan['price'];
                                $startup_discount = $plan['startup_discount'];

                                $duration = $plan['duration'];
                                $recurrence = $plan['recurrence'];
                                $frequency = $plan['frequency'];
                                $plan_name = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;
                            }
                        }

                        Yii::app()->session['save_user'] = 0;
                        Yii::app()->session['activate'] = 1;
                        $data['have_paypal'] = isset($_REQUEST['data']['havePaypal']) ? 1 : 0;
                        $data['paymentType'] = "Sale";
                        if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paygate') {
                            $data['cvv'] = $_REQUEST['data']['security_code'];
                            $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/payGateReturn";
                        } else {
                            $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/confirm?plan_id=" . $plan_id . "&reactivate=0";
                        }

                        $data['cancelURL'] = Yii::app()->getBaseUrl(true) . "/user/cancel";
                        $data['paymentAmount'] = '0.00';
                        $data['paymentDesc'] = $plan_name;
                        $data['gateway_code'] = $gateway_code;
                        $data['address'] = array();
                        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $data = $payment_gateway::processCard($data);

                            if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paygate') {
                                if (isset($data) && $data['isSuccess'] == 0) {
                                    Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/activate');
                                }
                            }
                        }
                    }
                } else {
                    if (isset($_REQUEST['data']['card_last_fourdigit']) && trim($_REQUEST['data']['card_last_fourdigit'])) {

                        //Update card to inactive mode
                        $sql = "UPDATE sdk_card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                        $con = Yii::app()->db;
                        $ciData = $con->createCommand($sql)->execute();

                        $sciModel = New SdkCardInfos;
                        $sciModel->studio_id = $studio_id;
                        $sciModel->user_id = $user_id;
                        $sciModel->gateway_id = $this->GATEWAY_ID[$gateway_code];
                        $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                        $sciModel->card_holder_name = $_REQUEST['data']['card_name'];
                        $sciModel->exp_month = $_REQUEST['data']['exp_month'];
                        $sciModel->exp_year = $_REQUEST['data']['exp_year'];
                        $sciModel->card_last_fourdigit = $_REQUEST['data']['card_last_fourdigit'];
                        $sciModel->auth_num = $_REQUEST['data']['auth_num'];
                        $sciModel->token = @$_REQUEST['data']['token'];
                        $sciModel->profile_id = @$_REQUEST['data']['profile_id'];
                        $sciModel->card_type = $_REQUEST['data']['card_type'];
                        $sciModel->reference_no = $_REQUEST['data']['reference_no'];
                        $sciModel->response_text = $_REQUEST['data']['response_text'];
                        $sciModel->status = $_REQUEST['data']['status'];
                        $sciModel->is_cancelled = 0;
                        $sciModel->ip = $ip_address;
                        $sciModel->created = new CDbExpression("NOW()");

                        $sciModel->save();
                        $card_id = $sciModel->id;
                    }
                    $payment_gateway_id = $this->PAYMENT_GATEWAY_ID[$gateway_code];

                    if (intval($plan_id) && intval($payment_gateway_id)) {
                        //Create user subscription for making transaction
                        $usModel = new UserSubscription;
                        $usModel->studio_id = $studio_id;
                        $usModel->user_id = $user_id;
                        $usModel->plan_id = $plan_id;
                        $usModel->card_id = $card_id;
                        $usModel->studio_payment_gateway_id = $payment_gateway_id;
                        $usModel->currency_id = $currency_id;
                        $usModel->profile_id = $_REQUEST['data']['profile_id'];
                        $usModel->amount = $plan_price;
                        $usModel->discount_amount = @$discount_price;
                        $usModel->coupon_code = @$couponCode;
                        $usModel->use_discount = $use_discount;
                        $usModel->start_date = $start_date;
                        $usModel->end_date = $end_date;
                        $usModel->status = 1;
                        $usModel->ip = $ip_address;
                        $usModel->created_by = $user_id;
                        $usModel->is_subscription_bundle = $_REQUEST['data']['is_subscription_bundles'];
                        $usModel->created_date = new CDbExpression("NOW()");
                        $usModel->save();

                        $user_subscription_id = $usModel->id;

                        $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
                        if (isset($couponDetails) && !empty($couponDetails)) {
                            $command = Yii::app()->db->createCommand();
                            if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                                $qry = $command->update('coupon_subscription', array('used_by' => $couponDetails->used_by . "," . $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                            } else {
                                $qry = $command->update('coupon_subscription', array('used_by' => $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                    }
                        }
                    }

                    //Save transaction data
                    if (intval($is_transaction)) {
                        $transaction = new Transaction;
                        $transaction->user_id = $user_id;
                        $transaction->studio_id = $studio_id;
                        $transaction->plan_id = $plan_id;
                        $transaction->currency_id = $currency_id;
                        $transaction->transaction_date = new CDbExpression("NOW()");
                        $transaction->payment_method = $_REQUEST['data']['payment_method'];
                        $transaction->transaction_status = $_REQUEST['data']['transaction_data']['transaction_status'];
                        $transaction->invoice_id = $_REQUEST['data']['transaction_data']['invoice_id'];
                        $transaction->order_number = $_REQUEST['data']['transaction_data']['order_number'];
                        $transaction->dollar_amount = $_REQUEST['data']['transaction_data']['dollar_amount'];
                        $transaction->amount = $_REQUEST['data']['transaction_data']['amount'];
                        $transaction->response_text = $_REQUEST['data']['transaction_data']['response_text'];
                        $transaction->subscription_id = $user_subscription_id;
                        $transaction->ip = $ip_address;
                        $transaction->created_date = new CDbExpression("NOW()");
                        $transaction->save();

                        $file_name = '';
                        //Generate pdf for instant payment when zero free trial
                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $arg['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            if (isset($_REQUEST['data']['coupon_code']) && $_REQUEST['data']['coupon_code'] != "") {
                                $discount_price = $price_list['price'] - $_REQUEST['data']['transaction_data']['amount'];
                                $arg['discount_amount'] = Yii::app()->common->formatPrice($discount_price, $_REQUEST['data']['currency_id']);
                                $arg['full_amount'] = Yii::app()->common->formatPrice($price_list['price'], $_REQUEST['data']['currency_id']);
                                $arg['coupon'] = $_REQUEST['data']['coupon_code'];
                            }
                            $arg['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $arg['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $arg, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }

                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', $file_name, '', '', '', '', '', $lang_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                        }
                        //$admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'welcome_email_with_subscription', $user_id, '', 0);
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', '', '', '', '', '', '', $lang_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                        }
                        //$admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'welcome_email_with_subscription', $user_id, '', 0);
                    }

                    Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
                    $this->redirect(Yii::app()->getbaseUrl(true));
                    exit;
                }
            }
        } else {
            $isCouponExists = 0;
            $gateways = array();
            $plans = array();
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $gateways = $plan_payment_gateway['gateways'];
                $plans = $plan_payment_gateway['plans'];

                if (!empty($plans)) {
                    $default_plan_id = @$plans[0]->id;
                    foreach ($plans as $key => $value) {
                        if ($value->is_default) {
                            $default_plan_id = $value->id;
                        }
                    }
                }
                $isCouponExists = Yii::app()->billing->isCouponExistsForSubscription($studio_id);
                    }
            $payment_form = Yii::app()->general->payment_form($gateways, $plans);
            if($gateways[0]->short_code == 'ippayment'){
                $activate_btn_form = Yii::app()->general->payment_next_btn(2);
            }else{
            $activate_btn_form = Yii::app()->general->activate_btn();
            }
            $custom_fields = Yii::app()->general->getCustomFields($studio_id, 1);
            $this->render('activate', array('gateways' => $gateways, 'plans' => $plans, 'default_plan_id' => @$default_plan_id, 'studio_id' => $studio_id, 'payment_form' => $payment_form, 'activate_btn_form' => $activate_btn_form, 'custom_fields' => $custom_fields, 'isCouponExists' => $isCouponExists, 'activate' => 1));
        }
    }

    public function actionSubscriptionPaymentProcess(){
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $data = $_REQUEST['data'];
            $data['studio_id'] = $studio_id;
            $data['user_id'] = $user_id = Yii::app()->user->id;
			
            Yii::app()->session['from_data'] = $_REQUEST;
            $gateway_code = $_REQUEST['data']['payment_method'];
            //Check studio has plan or not
            $start_date = $end_date = Null;
            $plan_id = $plan_price = 0;
            $data['currency_id'] = $currency_id = (isset($_REQUEST['data']['currency_id']) && intval($_REQUEST['data']['currency_id'])) ? $_REQUEST['data']['currency_id'] : $this->studio->default_currency_id;

            if ((isset($_REQUEST['data']['plan_id']) && intval($_REQUEST['data']['plan_id']))) {
                $data['plan_id'] = $plan_id = $_REQUEST['data']['plan_id'];
                //Get plan detail which user selected at the time of registration
                $planModel = new SubscriptionPlans();
                $plan = $planModel->findByPk($plan_id);
                $price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $data['plan_price'] = $plan_price = $price_list['price'];
                
                $trail_period = $plan->trial_period;
                $couponCode = "";
                if ($_REQUEST['data']['coupon_code'] != '') {
                    $couponCode = $_REQUEST['data']['coupon_code'];
                    $trail_period = $plan->trial_period;
                    if ($_REQUEST['data']['free_trail_charged'] != '') {
                        $trail_period = $_REQUEST['data']['free_trail_charged'];
                    }
                    if(intval($trail_period) > 0){
                        $use_discount = 1;
                    }
                }
                $discount_price = '';
                if ($trail_period > 0) {
                    if ($_REQUEST['data']['discount_amount'] != '') {
                        $discount_price = $_REQUEST['data']['discount_amount'];
                    }
                }
                $is_transaction = 0;
                
                if (isset($trail_period) && (intval($trail_period) == 0)) {
                    $use_discount = 0;
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                } else {
                    $trail_recurrence = 'day';
                    if($plan->trial_recurrence != ''){
                        $trail_recurrence = $plan->trial_recurrence;
                    }
                    $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
                $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);
            }

            //Getting Ip address
            $ip_address = Yii::app()->request->getUserHostAddress();
            $signup_step = Yii::app()->general->signupSteps();
            
            $data['name'] = @$_REQUEST['data']['name'];
            $data['email'] = @$_REQUEST['data']['email'];
            $data['password'] = @$_REQUEST['data']['password'];
            $data['is_subscription_bundles'] = @$_REQUEST['data']['is_subscription_bundles'];
            
            if($signup_step == 1 && $data['user_id'] == ""){
                
                    $name = trim($data['name']);
                    $email = trim($data['email']);
                    $password = @$data['password'];
                    $is_subscription_bundles=@$data['is_subscription_bundles'];
                    $geo_data = new EGeoIP();
                    $geo_data->locate($ip_address);

                    $pass = '';

                    $enc = new bCrypt();
                    if ($password) {
                        $pass = $enc->hash($password);
                    }
                    $confirm_token = $enc->hash($email);
                    if (strpos($confirm_token, "/") !== FALSE) {
                        $confirm_token = str_replace('/', '', $confirm_token);
                    }
                
                    //Save SDK user detail
                    $user = new SdkUser;
                    $user->email = $email;
                    $user->studio_id = $studio_id;
                    $user->signup_ip = $ip_address;
                    $user->display_name = $name;
                    $user->nick_name = @$_REQUEST['nick_name']?@$_REQUEST['nick_name']:$name;
                    $user->mobile_number = @$_REQUEST['mobile_number'];
                            if(@$_REQUEST['livestream'] &&  $_REQUEST['livestream']== 1){
                                    $user->is_broadcaster = 1;
                            }
                    $user->encrypted_password = $pass;
                    $user->source = $source;
                    $user->status = 1;
                    $user->confirmation_token = $confirm_token;
                    $user->signup_location = serialize($geo_data);
                    $user->created_date = new CDbExpression("NOW()");
                    $user->user_language =  "";
                    if (@$data['data']['fb_access_token']) {
                        $user->fb_access_token = $data['data']['fb_access_token'];
                    }
                    if (@$data['data']['fb_userid']) {
                        $user->fb_userid = $data['data']['fb_userid'];
                    }
                    if (@$data['data']['gplus_userid']) {
                        $user->gplus_userid = $data['data']['gplus_userid'];
                    }
                    $return = $user->save();
                    $user_id = $user->id;
                    $data['user_id'] = $user_id;
                    $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);
                    
                    
                    $model = new LoginForm;
                    $model->attributes = $data;
                    if ($model->loginuser()) {
                        if (isset($user_id) && intval($user_id)) {
                            $usr = Yii::app()->common->getSDKUserInfo($user_id);
                            if ($usr->add_video_log) {
                                $login_at = date('Y-m-d H:i:s');
                                $login_history = new LoginHistory();
                                $login_history->studio_id = $studio_id;
                                $login_history->user_id = $user_id;
                                $login_history->login_at = $login_at;
                                $login_history->logout_at = '0000-00-00 00:00:00';
                                $login_history->ip = $ip_address;
                                $login_history->last_login_check = $login_at;
                                $login_history->save();
                                Yii::app()->session['login_history_id'] = $login_history->id;
                                $this->setLoginHistoryCookie($login_history->id);
                            }
                        }
                    }
                    
                } 
            //Save card information if payment gateway is not manual
            
            $card_id = 0;
            $data['signup_steps'] = $signup_step;
            $data['couponCode'] = $couponCode;
            $data['trail_period'] = $trail_period;
            $data['trail_recurrence'] = $trail_recurrence;
            $data['recurrence_frequency'] = $recurrence_frequency;
            $data['use_discount'] = $use_discount;
            $data['discount_price'] = $discount_price;
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['ip'] = $ip_address;
            $data['gateway_id'] = $this->GATEWAY_ID['ippayment'];
            
            $data['amount'] = 0;
            $data['session_id'] = Yii::app()->common->generateUniqNumber();
            $data['session_key'] = Yii::app()->common->generateUniqNumber();
            $data['customer_ref'] = Yii::app()->common->generateUniqNumber();
            $data['ServerURL'] = base64_encode(Yii::app()->getbaseUrl(true).'/userPayment/ServerURLIPPaymentSubscription');
            $data['UserURL'] = base64_encode(Yii::app()->getbaseUrl(true).'/userPayment/UserURLIPPaymentSubscription');

            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY['ippayment'] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data['ipp_info'] = $payment_gateway::generateSST($data);
            $log = new PciLog();
            $log->unique_id = $data['session_id'];
            $log->log_data = json_encode($data);
            $log->save();
            $src = "https://demo.ippayments.com.au/Access/index.aspx?SessionID=".$data['ipp_info']['session_id']."&SST=".$data['ipp_info']['sst'];
            
            }
        $this->render('ippayment_form', array('src' => $src));
    }

    public function actionProcesssignup() {
        $studio_id = Yii::app()->common->getStudiosId();
        $payment_method = isset($_REQUEST['payment_method']) ? $_REQUEST['payment_method'] : '';
        $plan_id = isset($_REQUEST['plan_id']) ? $_REQUEST['plan_id'] : 0;
        $user_id = Yii::app()->user->id;

        if ($payment_method != '' && $plan_id > 0) {
            $ip_address = CHttpRequest::getUserHostAddress();

            $gateways = new StudioPaymentGateways;
            $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1 AND short_code = "' . $payment_method . '"';
            $gateways = $gateways::model()->with()->findAll(
                    array(
                        'condition' => $cond,
                        'order' => 't.id DESC',
                        'limit' => '1'
                    )
            );

            $msg = $this->ServerMessage['err_payment'];
            $transaction_id = 0;

            if ($payment_method == 'paypal') {
                $paypal_api_username = $gateways[0]->api_username;
                $paypal_api_password = $gateways[0]->api_password;
                $paypal_api_signature = $gateways[0]->api_signature;
                $paypal_api_version = $gateways[0]->api_version;
                $paypal_api_mode = $gateways[0]->api_mode;

                $have_paypal = isset($_REQUEST['have_paypal']) ? 1 : 0;
                $email = Yii::app()->session['logged_email'];

                $price = '0.00';
                $startup_discount = '0.00';
                $plan_name = '';
                $duration = 'Never';
                $recurrence = '';

                $plan = new SubscriptionPlans;
                $plan = $plan->findByPk($plan_id);

                $price = $plan->price;
                $startup_discount = $plan->startup_discount;

                $duration = $plan->duration;
                $recurrence = $plan->recurrence;
                $frequency = $plan->frequency;
                $free_month = $plan->free_month;
                $plan_name = $plan->name . ', ' . Yii::app()->common->formatPrice($price) . '/' . $recurrence;

                Yii::app()->session['plan_name'] = $plan_name;
                Yii::app()->session['period'] = $recurrence;
                Yii::app()->session['free_month'] = $free_month;
                Yii::app()->session['frequency'] = $frequency;
                Yii::app()->session['amount'] = $price;
                Yii::app()->session['plan_id'] = $plan_id;

                $currencyCodeType = "USD";
                $paymentType = "Sale";
                $returnURL = Yii::app()->getBaseUrl(true) . "/user/paymentreview";
                $cancelURL = Yii::app()->getBaseUrl(true) . "/user/process/action/cancel";
                $paymentAmount = '0.00';
                $paymentDesc = $plan_name;

                $address = array();
                $pp_ec = new PaypalExpress($paypal_api_username, $paypal_api_password, $paypal_api_signature, $paypal_api_version, $paypal_api_mode);
                $resArray = $pp_ec->CallShortcutExpressCheckout($paymentAmount, $email, $currencyCodeType, $paymentType, $paymentDesc, $returnURL, $cancelURL, $have_paypal, $address);
                //print_r($resArray);exit();

                $ack = strtoupper($resArray["ACK"]);

                if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                    $token = $resArray["TOKEN"];
                    $pay_log = new UserPaymentLog;
                    $pay_log->user_id = $user_id;
                    $pay_log->studio_id = $studio_id;
                    $pay_log->plan_id = $plan_id;
                    $pay_log->ip_address = $ip_address;
                    $pay_log->payment_method = $payment_method;
                    $pay_log->payment_tried = 't';
                    $pay_log->payment_tried_at = new CDbExpression("NOW()");
                    $pay_log->payment_done = 'f';
                    $pay_log->payment_token = $token;
                    $pay_log->save();
                    $payment_log_id = $pay_log->id;
                    Yii::app()->session['payment_log_id'] = $payment_log_id;

                    $pp_ec->RedirectToPayPal($resArray["TOKEN"]);
                } else {
                    $user = SdkUser::model()->findByPk($user_id); // assuming there is a post whose ID is 10
                    $user->delete(); // delete the row from the database table                    
                    Yii::app()->user->logout();
                    $err_msg = $this->ServerMessage['error_process_payment'];
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/register?error=' . urlencode($err_msg));
                }
            } else {
                Yii::app()->user->setFlash('error', $this->ServerMessage['err_payment']);
                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
            }
        } else {
            $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
            exit();
        }

        /*
          if(Yii::app()->user->id > 0)
          {
          //Added for PPV
          if(isset($_POST['movie_id']) && $_POST['movie_id'])
          {
          $connection = Yii::app()->db1;
          $agent = Yii::app()->common->getAgent();
          $company_id = STUDIO_USER_ID;
          $user_id = Yii::app()->user->id;
          $movie_id = isset($_REQUEST['movie_id'])?$_REQUEST['movie_id']:0;
          $movie_name = isset($_REQUEST['movie_name'])?$_REQUEST['movie_name']:'';
          Yii::app()->session['paid_movie_name'] = $movie_name;
          $ip_address = Yii::app()->common->getIP();
          $payment_method = isset($_REQUEST['payment_method'])?$_REQUEST['payment_method']:'';
          $payment_details = Yii::app()->common->getPaymentDetails($agent);
          $paypal_api_username = isset($payment_details['paypal_api_username'])?$payment_details['paypal_api_username']:'';
          $paypal_api_password = isset($payment_details['paypal_api_password'])?$payment_details['paypal_api_password']:'';
          $paypal_api_signature = isset($payment_details['paypal_api_signature'])?$payment_details['paypal_api_signature']:'';
          $paypal_api_version = isset($payment_details['paypal_api_version'])?$payment_details['paypal_api_version']:'64';
          $paypal_api_mode = isset($payment_details['paypal_api_mode'])?$payment_details['paypal_api_mode']:'sandbox';
          $amount = isset($_REQUEST['amount'])?$_REQUEST['amount']:'0';
          $link = isset($_REQUEST['perlink'])?$_REQUEST['perlink']:'';


          $returnURL = Yii::app()->getBaseUrl(true)."/user/paypalreturn";
          $movie_url = Yii::app()->getBaseUrl(true).'/movie/'.$link;
          $cancelURL = $movie_url."?move=cancel";

          $pp_ec = new PaypalExpress($paypal_api_username, $paypal_api_password, $paypal_api_signature, $paypal_api_version, $paypal_api_mode);

          $paymentInfo['Order']['theTotal'] = $amount;
          $paymentInfo['Order']['description'] = isset($_REQUEST['desc'])?$_REQUEST['desc']:'';
          $paymentInfo['Order']['quantity'] = '1';
          $paymentInfo['Order']['currency'] = 'USD';
          $paymentInfo['Order']['retuenUrl'] = $returnURL;
          $paymentInfo['Order']['cancelUrl'] = $cancelURL;
          $paymentInfo['Order']['landing'] = isset($_REQUEST['have_paypal'])?$_REQUEST['have_paypal']:0;

          // call paypal
          $result = $pp_ec->SetExpressCheckout($paymentInfo);
          //Detect Errors
          if(!$pp_ec->isCallSucceeded($result)){
          if($paypal_api_mode == 'life'){
          //Live mode basic error message
          $error = 'We were unable to process your request. Please try again later';
          }else{
          //Sandbox output the actual error message to dive in.
          $error = $result['L_LONGMESSAGE0'];
          }
          echo $error;
          Yii::app()->end();

          }else {
          // send user to paypal
          $token = urldecode($result["TOKEN"]);

          $qry = "INSERT INTO user_payment_logs (user_id, company_id, movie_id, ip_address, payment_method, payment_tried, payment_tried_at, payment_done, payment_token) VALUES";
          $qry.= " ('".$user_id."', '".$company_id."', '".$movie_id."', '".$ip_address."', '".$payment_method."', 't', NOW(), 'f', '".$token."');";
          $command = $connection->createCommand($qry);
          $command -> execute();
          $payment_log_id = $connection->getLastInsertID('user_payment_logs_id_seq');
          Yii::app()->session['payment_log_id'] = $payment_log_id;
          Yii::app()->session['movie_url'] = $movie_url;
          Yii::app()->session['movie_id'] = $movie_id;
          $pp_ec->RedirectToPayPal ( $token);
          }
          }
          else if(isset($_REQUEST['plan_id']) && isset($_REQUEST['plan_id']) > 0){
          $connection = Yii::app()->db;
          $plan_id = isset($_REQUEST['plan_id'])?$_REQUEST['plan_id']:0;
          $ip_address = CHttpRequest::getUserHostAddress();

          $payment_method = isset($_REQUEST['payment_method'])?$_REQUEST['payment_method']:'';

          $gateways = new StudioPaymentGateways;
          $cond = 't.studio_id='.$studio_id.' AND t.status = 1 AND short_code = "'.$payment_method.'"';
          $gateways = $gateways::model()->with()->findAll(
          array(
          'condition'=> $cond,
          'order' => 't.id DESC',
          'limit' => '1'
          )
          );


          $msg = "Some issue in processing the payment. Please try later.";
          $transaction_id = 0;

          if($payment_method == 'paypal')
          {
          $paypal_api_username = $gateways[0]->api_username;
          $paypal_api_password = $gateways[0]->api_password;
          $paypal_api_signature = $gateways[0]->api_signature;
          $paypal_api_version = $gateways[0]->api_version;
          $paypal_api_mode = $gateways[0]->api_mode;

          $have_paypal = isset($_REQUEST['have_paypal'])?1:0;
          $email = Yii::app()->session['logged_email'];

          $price = '0.00';
          $startup_discount = '0.00';
          $plan_name = '';
          $duration = 'Never';
          $recurrence = '';

          $plan = new SubscriptionPlans;
          $plan = $plan->findByPk($plan_id);

          $price = $plan->price;
          $startup_discount = $plan->startup_discount;

          $duration = $plan->duration;
          $recurrence = $plan->recurrence;
          $frequency = $plan->frequency;
          $free_month = $plan->free_month;
          $plan_name = $plan->name.', '.Yii::app()->common->formatPrice($price).'/'.$recurrence;

          Yii::app()->session['plan_name'] = $plan_name;
          Yii::app()->session['period'] = $recurrence;
          Yii::app()->session['free_month'] = $free_month;
          Yii::app()->session['frequency'] = $frequency;
          Yii::app()->session['amount'] = $price;
          Yii::app()->session['plan_id'] = $plan_id;

          $currencyCodeType = "USD";
          $paymentType = "Sale";
          $returnURL = Yii::app()->getBaseUrl(true)."/user/paymentreview";
          $cancelURL = Yii::app()->getBaseUrl(true)."/user/process/action/cancel";
          $paymentAmount = '0.00';
          $paymentDesc = $plan_name;

          $address = array();
          $pp_ec = new PaypalExpress($paypal_api_username, $paypal_api_password, $paypal_api_signature, $paypal_api_version, $paypal_api_mode);
          $resArray = $pp_ec->CallShortcutExpressCheckout ($paymentAmount, $email, $currencyCodeType, $paymentType, $paymentDesc, $returnURL, $cancelURL, $have_paypal, $address);

          $ack = strtoupper($resArray["ACK"]);

          if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING")
          {
          $token = $resArray["TOKEN"];
          $pay_log = new UserPaymentLog;
          $pay_log->user_id = $user_id;
          $pay_log->studio_id = $studio_id;
          $pay_log->plan_id = $plan_id;
          $pay_log->ip_address = $ip_address;
          $pay_log->payment_method = $payment_method;
          $pay_log->payment_tried = 't';
          $pay_log->payment_tried_at = new CDbExpression("NOW()");
          $pay_log->payment_done = 'f';
          $pay_log->payment_token = $token;
          $pay_log->save();
          $payment_log_id = $pay_log->id;
          Yii::app()->session['payment_log_id'] = $payment_log_id;

          $pp_ec->RedirectToPayPal ( $resArray["TOKEN"] );
          }

          }
          }
          }
          else {
          $this->redirect(Yii::app()->getbaseUrl(true).'/user/login');
          }
         * 
         */
    }

    //Successful  Order Placed
    public function actionSuccess() {
        if (isset(Yii::app()->session['transaction_id']) && Yii::app()->session['transaction_id'] > 0)
            $this->render('success');
        else
            $this->redirect(Yii::app()->user->returnUrl);
    }

    public function actionForgot() {
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) {
            $this->redirect(Yii::app()->user->returnUrl);
            //$this->redirect(Yii::app()->request->urlReferrer);
        } else {
            $meta = Yii::app()->common->pagemeta('forgot');
            $this->pageTitle = ($meta['title'] == '') ? 'Forgot' : $meta['title'];
            $this->pageDescription = $meta['description'];
            $this->pageKeywords = $meta['keywords'];
            $this->render('forgot', array('studio' => $studio_id));
        }
    }

    public function actionProfile() {
        if (Yii::app()->user->id > 0) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();

            //$subscribed = Yii::app()->common->isSubscribed($user_id);
            $cancel_reasons = Yii::app()->common->cancelReasons();
            $next_pt = Yii::app()->common->nextPayment();

            $usr = new SdkUser();
            $user = $usr->findByPk($user_id);
            $user_picture = $this->getProfilePicture($user->id, 'profilepicture', 'thumb');
            $custom_fields = Yii::app()->general->getCustomFields($studio_id, 1);
            $this->render('profile', array('user' => $user, 'studio_id' => $studio_id, 'cancel_reasons' => $cancel_reasons, 'next_payment' => $next_pt, 'user_picture' => $user_picture, 'custom_fields' => $custom_fields));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

    public function actionUpdate_user_profile() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();

        $err = 0;
        if ($user_id > 0 && $studio_id > 0) {
            if (isset($_REQUEST['name']) && trim($_REQUEST['name'])) {
                $usr = new SdkUser();
                $user = $usr->findByPk($user_id);
                $login_history_id = Yii::app()->session['login_history_id'];
                if (isset($_REQUEST['new_password']) && $_REQUEST['new_password'] != '' && isset($_REQUEST['confirm_password']) && $_REQUEST['confirm_password'] != '' && ($_REQUEST['new_password'] == $_REQUEST['confirm_password'])) {
                    $password = $_REQUEST['new_password'];
                    $enc = new bCrypt();
                    $pass = $enc->hash($password);
                    $user->encrypted_password = $pass;
                    LoginHistory::model()->logoutUser($studio_id, $user_id, $login_history_id);
                } else if (isset($_REQUEST['new_password']) && $_REQUEST['new_password'] != '' && isset($_REQUEST['confirm_password']) && $_REQUEST['confirm_password'] != '' && ($_REQUEST['new_password'] != $_REQUEST['confirm_password'])) {
                    $err = 1;
                }

                $user->display_name = trim($_REQUEST['name']);
                
                if($_REQUEST['subscribe_newsletter'] == '1'){
                    $user->announcement_subscribe = trim($_REQUEST['subscribe_newsletter']);
                }else{
                    $user->announcement_subscribe = '0';
                }
                $user->save();
                foreach ($_REQUEST['data'] as $key => $value) {
                    $parts = explode('custom_', $key);
                    if (@$parts[1] != '') {
                        $field = CustomField::model()->findFieldByName($studio_id, $parts[1]);
                        $cval = new CustomFieldValue();
                        $criteria = array('field_id' => $field->id, 'studio_id' => $studio_id, 'user_id' => $user_id);
                        $cvals = $cval->findAllByAttributes($criteria);
                        foreach ($cvals as $val) {
                            $val->delete();
                        }
                        if (is_array($value)) {
                            $c = 0;
                            foreach ($value as $val) {
                                $field_value = new CustomFieldValue;
                                $field_value->studio_id = $studio_id;
                                $field_value->field_id = $field->id;
                                $field_value->user_id = $user_id;
                                $field_value->value = $val;
                                $field_value->id_seq = $c;
                                $field_value->save();
                                $c++;
                            }
                        } else {
                            $field_value = new CustomFieldValue;
                            $field_value->studio_id = $studio_id;
                            $field_value->field_id = $field->id;
                            $field_value->user_id = $user_id;
                            $field_value->value = $value;
                            $field_value->save();
                        }
                    }
                }

                Yii::app()->user->display_name = trim($_REQUEST['name']);
                $err = 0;
            } else {
                $err = 2;
            }
        } else {
            $err = 2;
        }

        echo json_encode(array('err' => $err));
    }

    //Update Address
    public function actionUpdate_user_address() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();

        $err = 0;

        if ($user_id > 0 && $studio_id > 0 && isset($_REQUEST['name']) && $_REQUEST['name'] != '') {
            $display_name = $_POST['name'];
            $address1 = $_POST['address1'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $country = $_POST['country'];
            $zip = $_POST['zip'];
            $phone = $_POST['phone'];

            $address_id = $_POST['address_id'];
            if ($address_id > 0) {
                $address = new UserAddress;
                $addr = $address->findByPk($address_id);
                $addr->address1 = htmlspecialchars($address1);
                $addr->city = htmlspecialchars($city);
                $addr->state = htmlspecialchars($state);
                $addr->country = $country;
                $addr->zip = $zip;
                $addr->phone = $phone;
                $addr->last_updated_date = new CDbExpression("NOW()");
                $addr->save();
            } else {
                $addr = new UserAddress;
                $addr->address1 = htmlspecialchars($address1);
                $addr->city = htmlspecialchars($city);
                $addr->state = htmlspecialchars($state);
                $addr->country = $country;
                $addr->zip = $zip;
                $addr->phone = $phone;
                $addr->created_date = new CDbExpression("NOW()");
                $addr->save();
            }

            $user = new SdkUser();
            $user = $user->findByPk($user_id);
            $user->display_name = $display_name;
            $user->save();
            Yii::app()->user->display_name = $display_name;
            $err = 0;
        } else {
            $err = 1;
        }
        echo json_encode(array('err' => $err));
    }

    public function actionResetpassword() {
        $token = Yii::app()->request->getParam('auth');
        $email = Yii::app()->request->getParam('email');
        $agent = Yii::app()->common->getAgent();
        $user_id = Yii::app()->request->getParam('user_id');


        $this->render('reset', array('token' => $token, 'email' => $email, 'user_id' => $user_id));
    }

    public function actionReset_user_password() {
        $err = 0;
        $msg = $this->Language['link_expired'];
        $studio_id = Yii::app()->common->getStudiosId();

        if (isset($_REQUEST['password']) && $_REQUEST['password'] != '' && isset($_REQUEST['reset_password_token']) && $_REQUEST['reset_password_token'] != '') {

            $enc = new bCrypt();
            $pass = $enc->hash($_REQUEST['password']);

            $user = new SdkUser();
            $user = $user->findByAttributes(array('studio_id' => $studio_id, 'reset_password_token' => $_REQUEST['reset_password_token']));
            if ($user) {
                $email = $user->email;
                $user->reset_password_token = '';
                $user->encrypted_password = $pass;
                $user->save();

                $usr = new User();
                $usr = $usr->findByAttributes(array('studio_id' => $studio_id, 'email' => $email));
                if ($usr) {
                    $usr->encrypted_password = $pass;
                    $usr->save();
                }
                $err = 0;
                $msg = $this->ServerMessage['password_changed'];
                LoginHistory::model()->logoutUser($studio_id, $user->id);
            } else {
                $err = 1;
            }
        } else {
            $err = 1;
        }
        echo json_encode(array('err' => $err, 'msg' => $msg));
    }

    public function actionForgotpassword() {
        if (@$studio->is_csrf_enabled) {
            if (@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']) {
                unset($_SESSION['csrfToken']);
            } else {
                if (!Yii::app()->request->isAjaxRequest) {
                    $url = Yii::app()->createAbsoluteUrl('user/forgot');
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: ' . $url);
                    exit();
                } else {
                    $ret = array('status' => 'error', 'message' => 'Invalid Request');
                    echo json_encode($ret);
                    exit;
                }
            }
        }
        $response = 'error';
        $response_message = $this->ServerMessage['email_does_not_exist'];
        $studio_id = Yii::app()->common->getStudiosId();

        if (isset($_POST) && count($_POST) > 0 && isset($_POST['email']) && $_POST['email'] != '' && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) != false) {
            $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);

            $user = new SdkUser();
            $user = $user->findByAttributes(array('studio_id' => $studio_id, 'email' => $email));
            if ($user->id > 0) {
                $user_id = $user->id;
                $user_lang_code = SdkUser::model()->getUserLanguage($studio_id, $user_id);
                $lang_details = Language::model()->findByAttributes(array('code' => $user_lang_code));
                if (!empty($lang_details)) {
                    $language_id = $lang_details->id;
                } else {
                    $language_id = 20;
                }
                $to = array($email);

                $enc = new bCrypt();
                $reset_tok = $enc->hash($email);
                $user->reset_password_token = $reset_tok;
                $user->save();

                $user_name = $user->display_name;
                $to_name = $user_name;
                $from = $this->studio->contact_us_email;
                $from_name = $this->studio->name;
                if (isset($_POST['admin']) && $_POST['admin'] == 1) {
                    if (strpos($_SERVER['HTTP_HOST'], 'muvi.in')) {
                        $site_url = 'http://' . $this->studio->domain;
                    } else {
                        if ($this->studio->status == 1) {
                            $site_url = 'https://' . $this->studio->domain;
                        } else {
                            $site_url = 'http://' . $this->studio->domain;
                        }
                    }
                } else {
                    $site_url = Yii::app()->getBaseUrl(true);
                }


                $logo = '<a href="' . $site_url . '"><img src="' . $this->siteLogo . '" /></a>';

                $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';

                if ($studio->fb_link != '')
                    $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                else
                    $fb_link = '';
                if ($studio->tw_link != '')
                    $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                else
                    $twitter_link = '';
                if ($studio->gp_link != '')
                    $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
                else
                    $gplus_link = '';

                $reset_link = $site_url . '/user/resetpassword?user_id=' . $user_id . '&auth=' . $reset_tok . '&email=' . $email;
                $rlink = '<a href="' . $reset_link . '">' . $reset_link . '</a>';

                $studio_email = Yii::app()->common->getStudioEmail();
                $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
                if ($linked_emails->notification_from_email) {
                    $studio_email = $linked_emails->notification_from_email;
                }

                $subject = 'Reset password for your ' . $this->studio->name . ' account';
                //6122: Email Triggers: Add trigger for 'Forgot Password'
                //ajit@muvi.com
                //Apply dyanamic template
                //get the studio id from requested email id.               
                $command = Yii::app()->db->createCommand()
                        ->select('display_name,studio_id')
                        ->from('sdk_users u')
                        ->where(' u.status = 1 AND u.email = "' . $email . '" AND studio_id=' . $studio_id);
                $dataem = $command->queryAll();
                $FirstName = $dataem[0]['display_name'];
                $link = $rlink;
                $StudioName = $this->studio->name;
                $EmailAddress = $studio_email;
                $email_type = 'forgot_password';
                $content = "SELECT * FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND (language_id = " . $language_id . " OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND language_id = " . $language_id . "))";
                $data = Yii::app()->db->createCommand($content)->queryAll();
                if (count($data) > 0) {
                    
                } else {
                    $temp = New NotificationTemplates;
                    $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
                }

                $temps = $data[0];
                //Subject
                $subject = $temps['subject'];
                //$subject = htmlentities($subject);
                eval("\$subject = \"$subject\";");
                // $subject = htmlspecialchars_decode($subject);
                //Content
                $content = (string) $temps["content"];
                $breaks = array("<br />", "<br>", "<br/>");
                $content = str_ireplace($breaks, "\r\n", $content);
                $content = htmlentities($content);
                eval("\$content = \"$content\";");
                $content = htmlspecialchars_decode($content);
                //6122- END
                $params = array(
                    'website_name' => $this->studio->name,
                    'logo' => $logo,
                    'site_link' => $site_link,
                    'reset_link' => $rlink,
                    'username' => $user_name,
                    'fb_link' => @$fb_link,
                    'twitter_link' => @$twitter_link,
                    'gplus_link' => @$gplus_link,
                    'supportemail' => $studio_email,
                    'website_address' => $studio->address,
                    'content' => $content
                );
                $message = array('subject' => $subject,
                    'from_email' => $studio_email,
                    'from_name' => $this->studio->name,
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $to_name,
                            'type' => 'to'
                        )
                    )
                );
                $from = $studio_email;
                $template_name = 'sdk_reset_password_user';
                // $return_param = $this->mandrilEmail($template_name, $params, $message);   
                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/sdk_reset_password_user', array('params' => $params), true);
                $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, "", "", "", $StudioName);


                $response = 'success';
                $response_message = $this->ServerMessage['password_reset_link'];
                Yii::app()->user->setFlash('success', $response_message);
            }
        }
        $ret = array('status' => $response, 'message' => $response_message);
        echo json_encode($ret);
    }

    public function actionPaydueamount() {
        $agent = Yii::app()->common->getAgent();
        $payment_details = Yii::app()->common->getPaymentDetails($agent);
        $paypal_api_username = isset($payment_details['paypal_api_username']) ? $payment_details['paypal_api_username'] : '';
        $paypal_api_password = isset($payment_details['paypal_api_password']) ? $payment_details['paypal_api_password'] : '';
        $paypal_api_signature = isset($payment_details['paypal_api_signature']) ? $payment_details['paypal_api_signature'] : '';

        $connection = Yii::app()->db1;

        $api_env = PaypalRecurringPaymentProfile::env_type_sandbox;
        $api_version = '75.0';
        $pp_profile = new PaypalRecurringPaymentProfile($paypal_api_username, $paypal_api_password, $paypal_api_signature, $api_version, $api_env);


        $qry = "SELECT T.order_number FROM transactions T, user_subscriptions US WHERE T.id = US.transaction_id AND US.status = 't' AND T.payment_method = 'paypal'";
        $command = $connection->createCommand($qry);
        $trans = $command->queryAll(); // execute a query SQL               
        if (count($trans) > 0) {
            foreach ($trans as $tran) {
                //$sale_id = $tran['order_number'];
                $sale_id = 'I-RXFKK9L7L7TH';
                $pp_profile_details = $pp_profile->billOutstandingAmount($sale_id);
                echo "<br />Collected Due Amount : subscription profile - " . $sale_id;
            }
        }
    }

    public function actionDeleteUserByAdmin() {
        $res = array();
        if (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
            $studio_admin_user_id = Yii::app()->user->id;

            $studio_id = $this->studio->id;
            $user_id = $_REQUEST['user_id'];

            //Delete all Email Notification User
            EmailNotificationUser::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                ':studio_id' => $studio_id,
                ':user_id' => $user_id
            ));

            //Delete all Transaction Errors
            TransactionErrors::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                ':studio_id' => $studio_id,
                ':user_id' => $user_id
            ));

            //Delete all card infos of users
            SdkCardInfos::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                ':studio_id' => $studio_id,
                ':user_id' => $user_id
            ));

            //Delete all User Subscription
            UserSubscription::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id', array(
                ':studio_id' => $studio_id,
                ':user_id' => $user_id
            ));

            //Deactivate to user
            $usr = SdkUser::model()->findByPk($user_id);
            $usr->email = 'deleted_' . $usr->email;
            $usr->status = 0;
            $usr->is_deleted = 1;
            $usr->deleted_by = $studio_admin_user_id;
            $usr->deleted_at = gmdate('Y-m-d H:i:s');
            $usr->save();

            $res['responce'] = 1;
            $res['message'] = "User has been deleted successfully.";
        } else {
            $res['responce'] = 0;
            $res['message'] = "Error in deletion of user!";
        }
        echo json_encode($res);
        exit;
    }

    public function actionDeleteFreeByAdmin() {
        $res = array();
        if (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
            $studio_admin_user_id = Yii::app()->user->id;
            $studio_id = $this->studio->id;
            $user_id = $_REQUEST['user_id'];
            //Deactivate Free Account
            $usr = SdkUser::model()->findByPk($user_id);
            $usr->is_free = 0;
            $usr->save();
            $res['responce'] = 1;
            $res['message'] = "Free Service Removed From User";
        } else {
            $res['responce'] = 0;
            $res['message'] = "Error in Removing Free Service!";
        }
        echo json_encode($res);
        exit;
    }

    public function actionCancelsubscription() {
        $res = array();
        $card = array();
       
       
        if (isset($_REQUEST['cancel_note']) && trim($_REQUEST['cancel_note'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = $_REQUEST['user_id'];
            } else {
                $user_id = Yii::app()->user->id;
            }
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/paymentgateway.log', "a+");
            //Getting user subscription detail to find out plan, card detail
            $usersub = new UserSubscription;
             if(isset($_REQUEST['is_admin']) && $_REQUEST['is_admin']==1){
            $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
        }else{
             $user_subscriptionplan_id=$_REQUEST['user_subscription_plan'];
            $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1,'plan_id'=>$user_subscriptionplan_id));
        }
            $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
            $payment_gateway_type = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
            
            
            //Getting plan detail
            $plan = new SubscriptionPlans;
            $plan = $plan->findByPk($usersub->plan_id);
            //Getting card detail
            $card = new SdkCardInfos;
            $card = $card->findByPk($usersub->card_id);

            //Deduct outstanding due if plan is post paid and sent invoice detail to user
            $is_paid = 1;
            $file_name = '';
            if (isset($plan) && intval($plan->is_post_paid)) {
                //If trial period expires
                $start_date = date("Y-m-d", strtotime($usersub->start_date));
                $today_date = gmdate('Y-m-d');
                if (strtotime($start_date) < strtotime($today_date)) {
                    $trans_data = $this->processTransaction($card, $usersub);
                    $is_paid = $trans_data['is_success'];

                    //Save a transaction detail
                    if (intval($is_paid)) {
                        //Getting Ip address
                        $ip_address = Yii::app()->request->getUserHostAddress();

                        $transaction = new Transaction;
                        $transaction->user_id = $user_id;
                        $transaction->studio_id = $studio_id;
                        $transaction->plan_id = $usersub->plan_id;
                        $transaction->transaction_date = new CDbExpression("NOW()");
                        $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                        $transaction->transaction_status = $trans_data['transaction_status'];
                        $transaction->invoice_id = $trans_data['invoice_id'];
                        $transaction->order_number = $trans_data['order_number'];
                        $transaction->amount = $trans_data['amount'];
                        $transaction->response_text = $trans_data['response_text'];
                        $transaction->subscription_id = $usersub->id;
                        $transaction->ip = $ip_address;
                        $transaction->created_by = $user_id;
                        $transaction->created_date = new CDbExpression("NOW()");
                        $transaction->save();
                    }
                }
            }

            if (intval($is_paid)) {
                $success = 1;

                //Cancel customer's account from payment gateway
                $cancel_acnt = $this->cancelCustomerAccount($usersub, $card);
                //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.print_r($cancel_acnt, true));
                if (isset($payment_gateway_type) && ($payment_gateway_type == 'paypal' || $payment_gateway_type == 'paypalpro') && ($usersub->card_id == '' || !$usersub->card_id)) {
                    if (isset($cancel_acnt) && $cancel_acnt['ACK'] == 'Success') {
                        $success = 1;
                    } else {
                        $success = 0;
                    }
                } else {
                    //Delete card detail of user
                    if (trim($usersub->profile_id)) {
                        SdkCardInfos::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id AND profile_id = :profile_id', array(
                            ':studio_id' => $studio_id,
                            ':user_id' => $user_id,
                            ':profile_id' => $usersub->profile_id
                        ));
                    }
                }

                if ($success) {
                    //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.print_r('sunccess', true));
                    //Inactivate user subscription
                    $reason_id = isset($_REQUEST['cancelreason']) ? $_REQUEST['cancelreason'] : '';
                    $cancel_note = isset($_REQUEST['cancel_note']) ? $_REQUEST['cancel_note'] : '';

                    $usersub->status = 0;
                    $usersub->cancel_date = new CDbExpression("NOW()");
                    $usersub->cancel_reason_id = $reason_id;
                    $usersub->cancel_note = htmlspecialchars($cancel_note);
                    $usersub->payment_status = 0;
                    $usersub->partial_failed_date = '';
                    $usersub->partial_failed_due = 0;
                    $usersub->canceled_by = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? 0 : $user_id;
                    $usersub->save();

                    //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
                    if($usersub->is_subscription_bundle==0){
                    $usr = new SdkUser;
                    $usr = $usr->findByPk($user_id);
                    $usr->is_deleted = 1;
                    $usr->deleted_at = new CDbExpression("NOW()");
                    $usr->will_cancel_on = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? '' : $usersub->start_date;
                    $usr->save();
                    }
                    //Send an email to user
                    if (isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin'])) {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation', $file_name, '', '', '', '', '', $lang_code);
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation_user', $file_name, '', '', '', '', '', $lang_code);
                    }

                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_cancellation', $studio_id);
                    if ($isEmailToStudio) {
                        $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_subscription_cancelled', $user_id, '', 0);
                    }
                    $res['responce'] = 1;
                    $res['message'] = $this->Language['subscription_canceled'];
                } else {
                    $res['responce'] = 0;
                    $res['message'] = $this->Language['transc_cant_process'];
                }
            } else {
                $res['responce'] = 0;
                $res['message'] = $this->Language['transc_cant_process'];
            }
        } else {
            $res['responce'] = 0;
            $res['message'] = $this->Language['add_your_comment'];
        }

        echo json_encode($res);
        exit;
    }

    function cancelCustomerAccount($usersub = Null, $card = Null) {
        $data = '';
        $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
        if (isset($this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::cancelCustomerAccount($usersub, $card);
        }

        return $data;
    }

    function processTransaction($card_info, $usersub) {
        $data = '';
        $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
        if (isset($this->PAYMENT_GATEWAY[$gateway_info->short_code]) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::processTransaction($card_info, $usersub);
        }

        return $data;
    }

    public function actionReactivate() {
         $this->redirect(Yii::app()->getbaseUrl(true));
        exit;
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        $lang_code = $this->language_code;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        if (@$studio->is_csrf_enabled) {
            if (@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']) {
                unset($_SESSION['csrfToken']);
            } else {
                if (!Yii::app()->request->isAjaxRequest) {
                    $url = Yii::app()->createAbsoluteUrl();
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: ' . $url);
                    exit();
                } else {
                    echo '';
                    exit;
                }
            }
        }
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $data = $_REQUEST['data'];
            $user_id = Yii::app()->user->id;
            $gateway_code = $_REQUEST['data']['payment_method'];
            Yii::app()->session['from_data'] = $_REQUEST;
            //Getting Ip address
            $ip_address = Yii::app()->request->getUserHostAddress();

            //Get plan detail which user selected at the time of registration
            //Check studio has plan or not
            $start_date = $end_date = Null;
            $plan_id = $plan_price = $payment_gateway_id = 0;
            $currency_id = (isset($_REQUEST['data']['currency_id']) && intval($_REQUEST['data']['currency_id'])) ? $_REQUEST['data']['currency_id'] : $this->studio->default_currency_id;

            if ((isset($_REQUEST['data']['plan_id']) && intval($_REQUEST['data']['plan_id']))) {
                $plan_id = $_REQUEST['data']['plan_id'];
                //Get plan detail which user selected at the time of registration
                $planModel = new SubscriptionPlans();
                $plan = $planModel->findByPk($plan_id);

                $price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $plan_price = $price_list['price'];

                //$trail_period = $plan->trial_period;
                $trail_period = 0;
                //print_r($trail_period); exit;
                $couponCode = "";
                if ($_REQUEST['data']['coupon_code'] != '') {
                    $couponCode = $_REQUEST['data']['coupon_code'];
                    //$trail_period = $plan->trial_period;
                    $trail_period = 0;
                    if ($_REQUEST['data']['free_trail_charged'] != '') {
                        $trail_period = $_REQUEST['data']['free_trail_charged'];
                    }
                }
                $discount_price = '';
                if ($trail_period > 0) {
                    if ($_REQUEST['data']['discount_amount'] != '') {
                        $discount_price = $_REQUEST['data']['discount_amount'];
                    }
                }

                $is_transaction = 0;
                if (isset($trail_period) && (intval($trail_period) == 0) && isset($_REQUEST['data']['transaction_data']) && !empty($_REQUEST['data']['transaction_data'])) {
                    $is_transaction = 1;
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                } else {
                    $trail_recurrence = 'day';
                    $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
            }

            //Save SDK user detail
            $user = new SdkUser;
            $user = $user->findByPk($user_id);
            $user->is_deleted = 0;
            $user->last_updated_by = $user_id;
            $user->last_updated_date = new CDbExpression("NOW()");
            $user->save();

            //Save card information if payment gateway is not manual
            $card_id = 0;
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);
                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paygate' || $this->PAYMENT_GATEWAY[$gateway_code] == 'paypal' || ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && $_REQUEST['data']['havePaypal'])) {

                    $connection = Yii::app()->db;
                    $agent = Yii::app()->common->getAgent();
                    $plan_id = isset($_REQUEST['data']['plan_id']) ? $_REQUEST['data']['plan_id'] : 0;
                    $company_id = STUDIO_USER_ID;
                    $ip_address = Yii::app()->common->getIP();
                    $payment_method = isset($_REQUEST['data']['payment_method']) ? $_REQUEST['data']['payment_method'] : '';
                    $payment_details = Yii::app()->common->getPaymentDetails($agent);

                    $msg = $this->ServerMessage['err_payment'];
                    $transaction_id = 0;

                    $data['email'] = isset($_REQUEST['data']['email']) ? $_REQUEST['data']['email'] : '';

                    $price = '0.00';
                    $startup_discount = '0.00';
                    $plan_name = '';
                    $duration = 'Never';
                    $recurrence = '';

                    $currency = Currency::model()->findByPk($currency_id);

                    $data['currencyCodeType'] = $currency->code;
                    $data['currency_code'] = $currency->code;
                    $data['currency_symbol'] = $currency->symbol;

                    $qry = "SELECT *,sp.id as sub_plan_id FROM subscription_plans sp  LEFT JOIN subscription_pricing sup ON (sp.id = sup.subscription_plan_id) WHERE sp.id = '" . $plan_id . "' AND sp.studio_id = '" . $studio_id . "' AND sp.status = '1'";
                    $command = $connection->createCommand($qry);
                    $rowCount = $command->execute(); // execute the non-query SQL
                    $plans = $command->queryAll(); // execute a query SQL
                    if (count($plans)) {
                        foreach ($plans as $plan) {
                            if ($plan_id == $plan['sub_plan_id'] && $plan['currency_id'] == $currency_id) {
                                $price = $plan['price'];
                                $startup_discount = $plan['startup_discount'];

                                $duration = $plan['duration'];
                                $recurrence = $plan['recurrence'];
                                $frequency = $plan['frequency'];
                                $plan_name = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;
                            }
                        }

                        Yii::app()->session['save_user'] = 0;
                        Yii::app()->session['reactivate'] = 1;
                        $data['have_paypal'] = isset($_REQUEST['data']['havePaypal']) ? 1 : 0;
                        $data['paymentType'] = "Sale";
                        if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paygate') {
                            $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/payGateReturn";
                        } else {
                            $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/confirm?plan_id=" . $plan_id . "&reactivate=1";
                        }

                        $data['cancelURL'] = Yii::app()->getBaseUrl(true) . "/user/cancel";
                        $data['paymentAmount'] = '0.00';
                        $data['paymentDesc'] = $plan_name;
                        $data['address'] = array();
                        $data['gateway_code'] = $gateway_code;
                        $data['cvv'] = $_REQUEST['data']['security_code'];
                        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $data = $payment_gateway::processCard($data);
                            if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paygate') {
                                if (isset($data) && $data['isSuccess'] == 0) {
                                    Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/reactivate');
                                }
                            }
                        }
                    }
                } else {
                    if (isset($data['data']['card_last_fourdigit']) && trim($data['data']['card_last_fourdigit'])) {
                        //Update card to inactive mode
                        $sql = "UPDATE sdk_card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                        $con = Yii::app()->db;
                        $ciData = $con->createCommand($sql)->execute();

                        $sciModel = New SdkCardInfos;
                        $sciModel->studio_id = $studio_id;
                        $sciModel->user_id = $user_id;
                        $sciModel->gateway_id = $this->GATEWAY_ID[$gateway_code];
                        $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                        $sciModel->card_holder_name = $_REQUEST['data']['card_name'];
                        $sciModel->exp_month = $_REQUEST['data']['exp_month'];
                        $sciModel->exp_year = $_REQUEST['data']['exp_year'];
                        $sciModel->card_last_fourdigit = $_REQUEST['data']['card_last_fourdigit'];
                        $sciModel->auth_num = $_REQUEST['data']['auth_num'];
                        $sciModel->token = @$_REQUEST['data']['token'];
                        $sciModel->profile_id = @$_REQUEST['data']['profile_id'];
                        $sciModel->card_type = $_REQUEST['data']['card_type'];
                        $sciModel->reference_no = $_REQUEST['data']['reference_no'];
                        $sciModel->response_text = $_REQUEST['data']['response_text'];
                        $sciModel->status = $_REQUEST['data']['status'];
                        $sciModel->is_cancelled = 0;
                        $sciModel->ip = $ip_address;
                        $sciModel->created = new CDbExpression("NOW()");

                        $sciModel->save();
                        $card_id = $sciModel->id;
                    }
                    $payment_gateway_id = $this->PAYMENT_GATEWAY_ID[$gateway_code];

                    if (intval($plan_id) && intval($payment_gateway_id)) {
                        //Create user subscription for making transaction
                        $command = Yii::app()->db->createCommand()
                                ->select('MAX(end_date) as end_dt')
                                ->from('user_subscriptions')
                                ->where('user_id=:user_id AND studio_id=:studio_id AND status=:status AND end_date>NOW()', array(':user_id' => $user_id, ':studio_id' => $studio_id, ':status' => 0))
                                ->queryRow();

                        if (isset($command) && !empty($command)) {
                            $date_use = $command['end_dt'];
                            if ($_REQUEST['data']['free_trail_charged'] != '') {
                                $trail_recurrence = 'day';
                                $trial_period = $_REQUEST['data']['free_trail_charged'] . ' ' . strtolower($trail_recurrence) . "s";
                                $date_use = Date('Y-m-d H:i:s', strtotime($command['end_dt'] . "+{$trial_period}"));
                            }
                            $start_date = $date_use;
                            $en_time = strtotime($start_date);
                            $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $en_time));
                        }
                        $usModel = new UserSubscription;
                        $usModel->studio_id = $studio_id;
                        $usModel->user_id = $user_id;
                        $usModel->plan_id = $plan_id;
                        $usModel->card_id = $card_id;
                        $usModel->studio_payment_gateway_id = $payment_gateway_id;
                        $usModel->currency_id = $currency_id;
                        $usModel->profile_id = $_REQUEST['data']['profile_id'];
                        $usModel->amount = $plan_price;
                        $usModel->discount_amount = @$discount_price;
                        $usModel->coupon_code = @$couponCode;
                        $usModel->use_discount = 0;
                        $usModel->start_date = $start_date;
                        $usModel->end_date = $end_date;
                        $usModel->status = 1;
                        $usModel->ip = $ip_address;
                        $usModel->created_by = $user_id;
                        $usModel->created_date = new CDbExpression("NOW()");
                        $usModel->save();

                        $user_subscription_id = $usModel->id;

                        $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
                        if (isset($couponDetails) && !empty($couponDetails)) {
                            $command = Yii::app()->db->createCommand();
                            if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                                $qry = $command->update('coupon_subscription', array('used_by' => $couponDetails->used_by . "," . $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                            } else {
                                $qry = $command->update('coupon_subscription', array('used_by' => $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                    }
                        }
                    }

                    //Save transaction data
                    if (intval($is_transaction)) {
                        $transaction = new Transaction;
                        $transaction->user_id = $user_id;
                        $transaction->studio_id = $studio_id;
                        $transaction->plan_id = $plan_id;
                        $transaction->currency_id = $currency_id;
                        $transaction->transaction_date = new CDbExpression("NOW()");
                        $transaction->payment_method = $_REQUEST['data']['payment_method'];
                        $transaction->transaction_status = $_REQUEST['data']['transaction_data']['transaction_status'];
                        $transaction->invoice_id = $_REQUEST['data']['transaction_data']['invoice_id'];
                        $transaction->order_number = $_REQUEST['data']['transaction_data']['order_number'];
                        $transaction->dollar_amount = $_REQUEST['data']['transaction_data']['dollar_amount'];
                        $transaction->amount = $_REQUEST['data']['transaction_data']['amount'];
                        $transaction->response_text = $_REQUEST['data']['transaction_data']['response_text'];
                        $transaction->subscription_id = $user_subscription_id;
                        $transaction->ip = $ip_address;
                        $transaction->created_date = new CDbExpression("NOW()");
                        $transaction->save();

                        $file_name = '';
                        //Generate pdf for instant payment when zero free trial
                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $arg['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            if (isset($_REQUEST['data']['coupon_code']) && $_REQUEST['data']['coupon_code'] != "") {
                                $discount_price = $price_list['price'] - $_REQUEST['data']['transaction_data']['amount'];
                                $arg['discount_amount'] = Yii::app()->common->formatPrice($discount_price, $_REQUEST['data']['currency_id']);
                                $arg['full_amount'] = Yii::app()->common->formatPrice($price_list['price'], $_REQUEST['data']['currency_id']);
                                $arg['coupon'] = $_REQUEST['data']['coupon_code'];
                            }
                            $arg['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $arg['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $arg, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }

                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_reactivate', $file_name, '', '', '', '', '', $lang_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_reactivate', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_user_reactivate', $user_id, '', 0);
                        }
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_reactivate', '', '', '', '', '', '', $lang_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_reactivate', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_user_reactivate', $user_id, '', 0);
                        }
                    }

                    Yii::app()->user->setFlash('success', $this->ServerMessage['suc_subscription_reactivated']);
                    $this->redirect(Yii::app()->getbaseUrl(true));
                    exit;
                }
            }
        } else {
            $isCouponExists = 0;
            $gateways = array();
            $plans = array();
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $gateways = $plan_payment_gateway['gateways'];
                $plans = $plan_payment_gateway['plans'];

                if (!empty($plans)) {
                    $default_plan_id = @$plans[0]->id;
                    foreach ($plans as $key => $value) {
                        if ($value->is_default) {
                            $default_plan_id = $value->id;
                        }
                    }
                }
                $isCouponExists = Yii::app()->billing->isCouponExistsForSubscription($studio_id);

                $user_id = Yii::app()->user->id;
                if (intval($user_id)) {
                    $subscribed = Yii::app()->common->isSubscribedNotCancelled($user_id);
                    if (intval($subscribed)) {
                        $redirect_url = Yii::app()->getBaseUrl(true);
                        $this->redirect($redirect_url);
                    }
                }
            }

            $payment_form = Yii::app()->general->payment_form($gateways, $plans);
            if($gateways[0]->short_code == 'ippayment'){
                $activate_btn_form = Yii::app()->general->payment_next_btn();
            }else{
            $activate_btn_form = Yii::app()->general->activate_btn();
            }
            $custom_fields = Yii::app()->general->getCustomFields($studio_id, 1);
            $this->render('reactivate', array('gateways' => $gateways, 'plans' => $plans, 'default_plan_id' => @$default_plan_id, 'studio_id' => $studio_id, 'payment_form' => $payment_form, 'activate_btn_form' => $activate_btn_form, 'custom_fields' => $custom_fields, 'isCouponExists' => $isCouponExists, 'reactivate' => 1));
        }
    }

    /*
     * @author Sanjeev
     * For paypal
     */

    public function actionPaynow($rest_arg = array()) {
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        $arg = $rest_arg['data'];
        $studio_id = Yii::app()->common->getStudiosId();
        if (!empty($arg)) {
            $plan_id = $arg['plan_id'];
            $payment_method = $arg['payment_method'];
            $email = isset($arg['email']) ? $arg['email'] : '';

            if (isset($plan_id) && $plan_id > 0) {
                $connection = Yii::app()->db;
                $agent = Yii::app()->common->getAgent();
                $plan_id = isset($plan_id) ? $plan_id : 0;
                $company_id = STUDIO_USER_ID;
                $ip_address = Yii::app()->common->getIP();
                $payment_details = Yii::app()->common->getPaymentDetails($agent);
                $msg = $this->ServerMessage['err_payment'];
                $transaction_id = 0;
                $data['email'] = $email;
                $price = '0.00';
                $startup_discount = '0.00';
                $plan_name = '';
                $duration = 'Never';
                $recurrence = '';
                $qry = "SELECT *,sp.id as sub_plan_id FROM subscription_plans sp  LEFT JOIN subscription_pricing sup ON (sp.id = sup.subscription_plan_id) WHERE sp.id = '" . $plan_id . "' AND sp.studio_id = '" . $studio_id . "' AND sp.status = '1'";
                $command = $connection->createCommand($qry);
                $rowCount = $command->execute(); // execute the non-query SQL
                $plans = $command->queryAll(); // execute a query SQL
                $currency_id = $arg['currency_id'] ? $arg['currency_id'] : $this->studio->default_currency_id;
                $currency = Currency::model()->findByPk($currency_id);
                if (count($plans)) {
                    foreach ($plans as $plan) {
                        if ($plan_id == $plan['sub_plan_id'] && $plan['currency_id'] == $arg['currency_id']) {
                            $price = $plan['price'];
                            $startup_discount = $plan['startup_discount'];

                            $duration = $plan['duration'];
                            $recurrence = $plan['recurrence'];
                            $frequency = $plan['frequency'];
                            $plan_name = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;
                        }
                    }
                    $data['have_paypal'] = 1;
                    Yii::app()->session['save_user'] = 1;
                    $data['currencyCodeType'] = $currency->code;
                    $data['currency_code'] = $currency->code;
                    $data['currency_symbol'] = $currency->symbol;
                    $data['paymentType'] = "Sale";
                    $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/confirm?plan_id=" . $plan_id . "&reactivate=0";
                    $data['cancelURL'] = Yii::app()->getBaseUrl(true) . "/user/cancel";
                    $data['paymentAmount'] = '0.00';
                    $data['paymentDesc'] = $plan_name;
                    $data['address'] = array();
                    $gateway_code = $arg['payment_method'];
                    $data['gateway_code'] = $gateway_code;

                    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                        $payment_gateway = new $payment_gateway_controller();
                        $data = $payment_gateway::processCard($data);
                    }
                }
            }
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

    public function actionProcess() {
        $connection = Yii::app()->db;
        if (Yii::app()->request->getParam('action') == 'cancel') {
            $user_id = Yii::app()->user->id;

            $payment_log_id = isset(Yii::app()->session['payment_log_id']) ? Yii::app()->session['payment_log_id'] : 0;

            if ($payment_log_id > 0) {
                $params = serialize($resArray);
                $qry = "UPDATE user_payment_logs SET payment_cancelled = 't', payment_cancelled_at = NOW()";
                $qry.= " WHERE id = '" . $payment_log_id . "'";
                $command = $connection->createCommand($qry);
                $command->execute();
            }
            $user = SdkUser::model()->findByPk($user_id); // assuming there is a post whose ID is 10
            $user->delete(); // delete the row from the database table                    
            Yii::app()->user->logout();

            $msg = $this->Language['sorry_cancel_payment'];
            $this->redirect(Yii::app()->baseUrl . '/user/register?error=' . urlencode($msg));
            exit();
        }


        $selected_plan = 0;
        $isPlans = Yii::app()->common->checkIfPlan();
        //print_r($isPlans);exit();
        if (!$isPlans) {
            $this->redirect(Yii::app()->getBaseurl('true') . '/');
            exit;
        }

        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) {
            $user_id = Yii::app()->user->id;
            $subscribed = Yii::app()->common->isSubscribed($user_id);
            $studio_id = Yii::app()->common->getStudiosId();

            $free_month_end = date('d/m/Y', strtotime(date('m/d/Y') . "+1 month"));
            if ($subscribed > 0) {
                $this->render('alreadysubscribed');
            } else {
                $free_month_end = date('d/m/Y', strtotime(date('m/d/Y') . "+1 month"));

                //Plans
                $gateways = new StudioPaymentGateways;
                $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
                $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 't.id DESC'
                        )
                );

                $plans = new SubscriptionPlans;
                $cond = 'studio_id=' . $studio_id . ' AND status = 1';
                $plans = $plans::model()->with()->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 'id DESC'
                        )
                );

                $this->render('process', array('gateways' => $gateways, 'plans' => $plans, 'free_month_end' => $free_month_end, 'studio_id' => $studio_id));
            }
        } else {
            Yii::app()->session['previous_url'] = Yii::app()->baseUrl . '/user/login';
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

    public function actionCancel() {
        $user_id = Yii::app()->user->id;
        $usersub = new UserSubscription();
        $deleteSub = $usersub->deleteUserSubscription($user_id);
        if (isset($_GET['token']) && trim($_GET['token'])) {
            Yii::app()->user->setFlash('error', $this->ServerMessage['canceled_transaction']);
            $this->redirect(Yii::app()->session['backbtnlink']);
            exit;
        }
        //$this->render('cancel');
    }

    //Paypal return for PPV Payment
    public function actionPaypalreturn() {

        $token = "";
        $msg = '';
        $class = 'error';
        $connection = Yii::app()->db1;
        $agent = Yii::app()->common->getAgent();
        $payment_details = Yii::app()->common->getPaymentDetails($agent);
        $paypal_api_username = isset($payment_details['paypal_api_username']) ? $payment_details['paypal_api_username'] : '';
        $paypal_api_password = isset($payment_details['paypal_api_password']) ? $payment_details['paypal_api_password'] : '';
        $paypal_api_signature = isset($payment_details['paypal_api_signature']) ? $payment_details['paypal_api_signature'] : '';
        $paypal_api_version = isset($payment_details['paypal_api_version']) ? $payment_details['paypal_api_version'] : '64';
        $paypal_api_mode = isset($payment_details['paypal_api_mode']) ? $payment_details['paypal_api_mode'] : 'sandbox';

        if (isset($_REQUEST['token']) && $_REQUEST['token'] != '') {
            $token = $_REQUEST['token'];
            $pp_ec = new PaypalExpress($paypal_api_username, $paypal_api_password, $paypal_api_signature, $paypal_api_version, $paypal_api_mode);
            $resArray = $pp_ec->GetExpressCheckoutDetails($token);

            $payment_log_id = 0;
            $payment_log_id = Yii::app()->session['payment_log_id'];

            if ($payment_log_id > 0) {
                $params = serialize($resArray);
                $qry = "UPDATE user_payment_logs SET return_parameters = '" . $params . "'";
                $qry.= " WHERE id = '" . $payment_log_id . "'";
                $command = $connection->createCommand($qry);
                $command->execute();
            }

            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCESSWITHWARNING") {
                $payer_id = $resArray['PAYERID'];
                $payer_email = $resArray['EMAIL'];
                $first_name = $resArray['SHIPTONAME'];
                $address1 = $resArray['SHIPTOSTREET'];
                $city = $resArray['SHIPTOCITY'];
                $state = $resArray['SHIPTOSTATE'];
                $country = $resArray['SHIPTOCOUNTRYCODE'];
                $zip = $resArray['SHIPTOZIP'];

                $currency = 'USD';
                $resArray['TOKEN'] = $token;
                $resArray['CURRENCY'] = $currency;

                $user_id = Yii::app()->user->id;
                $charged_amount = $resArray['AMT'];
                $movie_id = Yii::app()->session['movie_id'];

                $res = $pp_ec->DoExpressCheckoutPayment($resArray);
                $ack = strtoupper($res["ACK"]);

                if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                    $status = $res['PAYMENTSTATUS'];

                    if ($status = 'Completed') {
                        $order_number = $res['TRANSACTIONID'];
                        $invoice_id = $res['CORRELATIONID'];

                        //Removing the express checkout test
                        //$pp_ec->ConfirmPayment( $charged_amount, $token, 'Sale', $currency, $payerID );
                        //print_r($ret);

                        $qry = "INSERT INTO transactions (user_id, agent, movie_id, transaction_date, payment_method, transaction_status, amount, order_number, invoice_id) VALUES";
                        $qry.= " ('" . $user_id . "', '" . $agent . "', '" . $movie_id . "', NOW(), 'paypal', '" . $status . "', '" . $charged_amount . "', '" . $order_number . "', '" . $invoice_id . "');";

                        $command = $connection->createCommand($qry);
                        $command->execute();
                        $transaction_id = $connection->getLastInsertID('transactions_id_seq');
                        $intv = '48 hours';

                        //Updating the Billing Address
                        $qry_trn = "UPDATE transactions SET";
                        $qry_trn.= " fullname = '" . pg_escape_string($first_name) . "'";
                        $qry_trn.= ", address1 = '" . pg_escape_string($address1) . "'";
                        $qry_trn.= ", city = '" . pg_escape_string($city) . "'";
                        $qry_trn.= ", state = '" . pg_escape_string($state) . "'";
                        $qry_trn.= ", country = '" . $country . "'";
                        $qry_trn.= ", zip = '" . $zip . "'";
                        $qry_trn.= ", payer_id = '" . $payer_id . "'";
                        $qry_trn.= ", expiry_date = NOW()+interval '" . $intv . "'";
                        $qry_trn.= " WHERE id = '" . $transaction_id . "'";
                        //file_put_contents($logfile, $qry_trn, FILE_APPEND | LOCK_EX);
                        $command = $connection->createCommand($qry_trn);
                        $command->execute();
                        //Updating the user address    

                        $qry_addr = "SELECT id FROM user_addresses WHERE user_id = '" . $user_id . "' ORDER BY id DESC LIMIT 1 OFFSET 0";
                        $command = $connection->createCommand($qry_addr);
                        $addrs = $command->queryAll(); // execute a query SQL            


                        if (count($addrs) > 0) {
                            foreach ($addrs as $addr) {
                                $qry_upd = "UPDATE user_addresses SET";
                                $qry_upd.= " address1 = '" . pg_escape_string($address1) . "'";
                                $qry_upd.= ", city = '" . pg_escape_string($city) . "'";
                                $qry_upd.= ", state = '" . pg_escape_string($state) . "'";
                                $qry_upd.= ", country = '" . $country . "'";
                                $qry_upd.= ", zip = '" . $zip . "'";
                                $qry_upd.= ", last_updated = NOW()";
                                echo $qry_upd.= " WHERE id = '" . $addr['id'] . "'";
                                $command = $connection->createCommand($qry_upd);
                                $command->execute();
                            }
                        } else {
                            $qry = "INSERT INTO user_addresses (user_id, address1, city, state, country, zip, last_updated) VALUES";
                            echo $qry.= " ('" . $user_id . "', '" . $address1 . "', '" . $city . "', '" . $state . "', '" . $country . "', '" . $zip . "', NOW());";
                            $command = $connection->createCommand($qry);
                            $command->execute();
                        }

                        if ($payment_log_id > 0) {
                            $qry = "UPDATE user_payment_logs SET payment_done = 't', payment_done_at = NOW()";
                            echo $qry.= " WHERE id = '" . $payment_log_id . "'";
                            $command = $connection->createCommand($qry);
                            $command->execute();
                        }

                        $start_time = '';
                        $end_time = '';
                        $qry_dt = "SELECT transaction_date, expiry_date FROM transactions WHERE id = '" . $transaction_id . "' ORDER BY id DESC LIMIT 1 OFFSET 0";
                        $command = $connection->createCommand($qry_dt);
                        $dts = $command->queryAll(); // execute a query SQL               
                        if (count($dts) > 0) {
                            foreach ($dts as $dt) {
                                $start_time = Yii::app()->common->phpDate($dt['transaction_date']);
                                $exp_date = Yii::app()->common->phpDate($dt['expiry_date']);
                            }
                        }


                        $movie_name = isset(Yii::app()->session['paid_movie_name']) ? Yii::app()->session['paid_movie_name'] : '';
                        $movie_url = isset(Yii::app()->session['movie_url']) ? Yii::app()->session['movie_url'] : '';

                        // Subscribtion welcome email 
                        $store_details = Yii::app()->common->storeDetails($agent);
                        @$display_name = Yii::app()->session['display_name'];
                        $useremail = Yii::app()->session['logged_email'];
                        $username = @$display_name ? ((strstr(trim(@$display_name), ' ')) ? strstr(trim(@$display_name), ' ', TRUE) : trim(@$display_name)) : strstr($useremail, '@', TRUE);

                        $site_url = Yii::app()->getBaseUrl(true);
                        $logo = $site_url . '/themes/' . $agent . '/images/logo.png';
                        $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="' . $agent . '" /></a>';

                        $siteName = @$store_details['siteName'];
                        $agent_address = @$store_details['agent_address'];
                        $supportemail = @$store_details['supportemail'];
                        $fb_link = '<a href="' . @$store_details['fb_link'] . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                        $twitter_link = '<a href="' . @$store_details['twitter_link'] . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                        $gplus_link = '<a href="' . @$store_details['gplus_link'] . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';

                        $available = 'You can watch <a href="' . $movie_url . '">' . $movie_name . '</a> at anytime from now within next 48 hours.';
                        $movie_link = '<a href="' . $movie_url . '">' . $movie_name . '</a>';

                        $faq_link = '<a href="' . $this->createAbsoluteUrl('site/faq') . '" >FAQs</a>';
                        $trans_link = '<a href="' . $this->createAbsoluteUrl('user/transactions') . '" >click here</a>';

                        $paid_amount = Yii::app()->common->formatPrice($charged_amount);
                        $qry_subs = "SELECT id FROM user_subscriptions WHERE user_id = '" . $user_id . "' and agent = '" . $agent . "' ORDER BY id DESC LIMIT 1 OFFSET 0";
                        $command = $connection->createCommand($qry_subs);
                        $subs = $command->queryAll(); // execute a query SQL    
                        if ($subs) {
                            $user_type = "subscriber";
                        } else {
                            $user_type = "non-subscriber";
                        }
                        $params = array(
                            'website_name' => $agent,
                            'logo' => $logo,
                            'username' => ucfirst(@$username),
                            'start_date' => @$start_time,
                            'exp_date' => @$exp_date,
                            'fb_link' => @$fb_link,
                            'twitter_link' => @$twitter_link,
                            'gplus_link' => @$gplus_link,
                            'faq_link' => @$faq_link,
                            'trans_link' => @$trans_link,
                            'agent_address' => @$agent_address,
                            'supportemail' => @$supportemail,
                            'available' => @$available,
                            'paid_amount' => @$paid_amount,
                            'movie_link' => @$movie_link,
                            'user_type' => @$user_type
                        );

                        $subject = 'Thank you for your payment in ' . $store_details['siteName'];
                        $message = array('subject' => $subject,
                            'from_email' => $store_details['adminEmail'],
                            'from_name' => $store_details['name'],
                            'to' => array(
                                array(
                                    'email' => $useremail,
                                    'name' => $username,
                                    'type' => 'to'
                                )
                            )
                        );
                        $template_name = 'sdk_ppv_paid_user';
                        // $this->sendMandrilEmail($template_name, $params, $message);  
                        $user_to = $useremail;
                        $user_subject = $subject;
                        $user_from = $store_details['adminEmail'];


                        Yii::app()->theme = 'bootstrap';
                        $html = Yii::app()->controller->renderPartial('//email/sdk_ppv_paid_user', array('params' => $params), true);
                        $returnVal_user = $this->sendmailViaAmazonsdk($html, $user_subject, $user_to, $user_from);

                        //Sending Email to Admins
                        $adminGroupEmail = @$store_details['adminGroup'];
                        //$adminSubject = 'Congratulations! '.$display_name.' has paid to watch movie '.$movie_name.' in '.$store_details['siteName'].'!';
                        $adminSubject = $store_details['siteName'] . " PPV Content Purchased!";
                        $mailAddress = array(
                            'subject' => $adminSubject,
                            'from_email' => $store_details['adminEmail'],
                            'from_name' => $store_details['name'],
                            'to' => $adminGroupEmail
                        );
                        $template_name = 'sdk_ppv_paid_admin';
                        //$returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);       
                        Yii::app()->theme = 'bootstrap';
                        $html = Yii::app()->controller->renderPartial('//email/sdk_ppv_paid_admin', array('params' => $params), true);
                        $returnVal_admin = $this->sendmailViaAmazonsdk($html, $adminSubject, $store_details[0]['email'], $store_details['adminEmail']);


                        $succ_msg = $this->ServerMessage['suc_ppv_msg'];
                        $succ_msg = htmlentities($succ_msg);
                        eval("\$succ_msg = \"$succ_msg\";");
                        $succ_msg = htmlspecialchars_decode($succ_msg);
                        Yii::app()->user->setFlash('success', $succ_msg);
                        if (isset(Yii::app()->session['movie_url']) && Yii::app()->session['movie_url'] != '')
                            $this->redirect(Yii::app()->session['movie_url']);
                        else {
                            $this->redirect(Yii::app()->common->userHome());
                        }
                    }
                } else {
                    //Display a user friendly Error on the page using any of the following error information returned by PayPal
                    $ErrorCode = urldecode($res["L_ERRORCODE0"]);
                    $ErrorShortMsg = urldecode($res["L_SHORTMESSAGE0"]);
                    $ErrorLongMsg = urldecode($res["L_LONGMESSAGE0"]);
                    $ErrorSeverityCode = urldecode($res["L_SEVERITYCODE0"]);

                    $msg = $this->ServerMessage['payment_failed'];
                    $msg.= $this->ServerMessage['detailed_err_msg'] . ": " . $ErrorLongMsg;

                    Yii::app()->user->setFlash('error', $msg);
                    if (isset(Yii::app()->session['movie_url']) && Yii::app()->session['movie_url'] != '')
                        $this->redirect(Yii::app()->session['movie_url']);
                    else {
                        $this->redirect(Yii::app()->common->userHome());
                    }
                }
            } else {
                //Display a user friendly Error on the page using any of the following error information returned by PayPal
                $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
                $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
                $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
                $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

                $msg = $this->ServerMessage['payment_failed'];
                $msg.= $this->ServerMessage['detailed_err_msg'] . ": " . $ErrorLongMsg;

                Yii::app()->user->setFlash('error', $msg);
                if (isset(Yii::app()->session['movie_url']) && Yii::app()->session['movie_url'] != '')
                    $this->redirect(Yii::app()->session['movie_url']);
                else {
                    $this->redirect(Yii::app()->common->userHome());
                }
            }
        }
        $this->render('paymentreview', array('msg' => $msg, 'class' => $class));
    }

    public function actionTransactions() {
        $user_id = Yii::app()->user->id;
        if ($user_id > 0) {
            $studio_id = Yii::app()->user->studio_id;
            $subscribed_user_data = Yii::app()->common->isSubscribed($user_id, $studio_id, 1);

            $dbcon = Yii::app()->db;

            //Pagination codes
            $items_per_page = 10;
            $page_size = $limit = $items_per_page;
            $offset = 0;
            if (isset($_REQUEST['p'])) {
                $offset = ($_REQUEST['p'] - 1) * $limit;
                $page_number = $_REQUEST['p'];
            } else {
                $page_number = 1;
            }

            $sql_data = "SELECT SQL_CALC_FOUND_ROWS T.id FROM transactions T WHERE studio_id = " . $studio_id . " AND user_id = " . $user_id . " LIMIT " . $limit . " OFFSET " . $offset;
            $transactions = $dbcon->createCommand($sql_data)->queryAll();
            $item_count = $dbcon->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();
            $data = array();
            foreach ($transactions as $transaction) {
                $data[] = Yii::app()->general->getTransactionDetails($transaction['id']);
            }
            if ($studio_id == 1955) { //for forest Tv only
                foreach ($data as $key => $val) {
                    $date_splitted = explode(" ", $val['transaction_date']);
                    $dateS = explode("-", $date_splitted[0]);

                    $start_date = $dateS[1] . "-" . $dateS[2] . "-" . $dateS['0'];
                    $start_time = $date_splitted[1];
                    $data[$key]['transaction_date'] = $start_date . " " . $start_time;
                }
            }
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $page_url = Yii::app()->general->full_url($url, '?p=');
            $page_url = Yii::app()->general->full_url($page_url, '&p=');
            $pg = new bootPagination();
            $pg->pagenumber = $page_number;
            $pg->pagesize = $page_size;
            $pg->totalrecords = $item_count;
            $pg->showfirst = true;
            $pg->showlast = true;
            $pg->paginationcss = "pagination-normal";
            $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
            $pg->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1)
                $pg->paginationUrl = $page_url . "&p=[p]";
            else {
                $pg->paginationUrl = $page_url . "?p=[p]";
            }
            $pagination = $pg->process();
            // print_r($data);
            $this->render('transactions', array('transactions' => json_encode($data), 'pagination' => $pagination));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

//transaction details page by suniln
    public function actionTransaction() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        if ($user_id > 0 && $studio_id > 0) {
            $voucherid = Yii::app()->request->getParam('v');
            $id = Yii::app()->request->getParam('id');
            if ($id != '') {
                $transaction = Yii::app()->general->getTransactionDetails($id);
                if ($studio_id == 1955) {
                    $date_splitted = explode(" ", $transaction[0]['transaction_date']);
                    $dateS = explode("-", $date_splitted[0]);

                    $start_date = $dateS[1] . "-" . $dateS[2] . "-" . $dateS['0'];
                    $start_time = $date_splitted[1];
                    $transaction[0]['transaction_date'] = $start_date . " " . $start_time;
                }
                $this->render('transaction', array('transaction' => json_encode($transaction)));
            }
            if ($voucherid != '') {
                $voucherdetails = Yii::app()->db->createCommand("select ppv_subscription_id,transaction_type,transaction_date,invoice_id from transactions WHERE id=" . $voucherid)->queryRow();
                $ppv_subscription_id = $voucherdetails['ppv_subscription_id'];
                $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=" . $ppv_subscription_id)->queryRow();
                $end_date = $PpvSubscription['end_date'];
                $start_date = $PpvSubscription['start_date'];
                $season_id = $PpvSubscription['season_id'];
                $episode_id = $PpvSubscription['episode_id'];
                $movie_id = $PpvSubscription['movie_id'];
                $ppv_plan_id = $PpvSubscription['ppv_plan_id'];
                $unique_id = $voucherdetails['invoice_id'];
                $voucher_code = CouponOnly::model()->findByAttributes(array('id' => $ppv_plan_id))->voucher_code;
                $vcreated_date = gmdate("F d, Y", strtotime($start_date));
                $permalink = Film::model()->findByAttributes(array('id' => $movie_id))->permalink;
                $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                $vmovie_names = $movie_name;
                if ($season_id != 0 && $episode_id != 0) {
                    $vmovie_names = $movie_name . " -> Season #" . $season_id . " ->" . $episode_name;
                }
                if ($season_id == 0 && $episode_id != 0) {
                    $vmovie_names = $movie_name . " -> " . $episode_name;
                }
                if ($season_id != 0 && $episode_id == 0) {
                    $vmovie_names = $movie_name . " -> Season #" . $season_id;
                }
                $this->render('transaction', array('vcreated_date' => $vcreated_date, 'voucher_code' => $voucher_code, 'vmovie_names' => $vmovie_names, 'unique_id' => $unique_id, 'is_voucher' => 1));
            }
            if($subscription_bundlesid!=''){
              $subscription_bundles_details = Yii::app()->db->createCommand()
                ->select('subscription_id,transaction_type,transaction_date,invoice_id')
                ->from('transactions')
                ->where('id=:id', array(':id'=>$subscription_bundlesid))
                ->queryRow();
              $subscription_bundles_id=$subscription_bundles_details['subscription_id'];
              $UserSubscriptionBundle = Yii::app()->db->createCommand()
                ->select('end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id')
                ->from('user_subscriptions')
                ->where('id=:id', array(':id'=>$subscription_bundles_id))
                ->queryRow();
                $end_date = $UserSubscriptionBundle['end_date'];
                $start_date = $UserSubscriptionBundle['start_date'];
                $plan_id = $UserSubscriptionBundle['plan_id'];
                $unique_id = $subscription_bundles_details['invoice_id'];
                $this->render('transaction', array('vcreated_date' => $vcreated_date, 'voucher_code' => $voucher_code, 'vmovie_names' => $vmovie_names, 'unique_id' => $unique_id));
            }
        } else {
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }

    public function actionAjaxlogin() {
        $studio = $this->studio;
        if (@$studio->is_csrf_enabled) {
            if (@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken']) {
                unset($_SESSION['csrfToken']);
                $this->generateCsrfToken();
            } else {
                if (!Yii::app()->request->isAjaxRequest) {
                    $url = Yii::app()->createAbsoluteUrl();
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: ' . $url);
                    exit();
                } else {
                    echo '';
                    exit;
                }
            }
        }
        $model = new LoginForm;
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $studio_id = $studio->id;
            if ($model->loginuser($_POST['LoginForm'])) {
                $user_id = Yii::app()->user->id;
                $get_allcontents = Yii::app()->general->gethomepageList($studio_id, $user_id);
                if ($get_allcontents['url'] != '') {
                    $action = $get_allcontents['url'];
                } else {
                    $action = '';
                }
                if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
                    $usr = Yii::app()->common->getSDKUserInfo(Yii::app()->user->id);
                    if ($usr->add_video_log) {
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = Yii::app()->user->id;
                        $login_at = date('Y-m-d H:i:s');
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = $login_at;
                        $login_history->logout_at = '0000-00-00 00:00:00';
                        $login_history->ip = $ip_address;
                        $login_history->last_login_check = $login_at;
                        $login_history->remember_me = isset($_POST['LoginForm']['rememberMe']) ? '1' : '0';
                        $login_history->save();
                        Yii::app()->session['login_history_id'] = $login_history->id;
                        $this->setLoginHistoryCookie($login_history->id);
                    }
                    if (isset($this->studio->parent_theme) && trim($this->studio->parent_theme) == "singleshow-a") {
                        $command = Yii::app()->db->createCommand()
                                ->select('confirmation_token')
                                ->from('sdk_users')
                                ->where('id = "' . Yii::app()->user->id . '" AND studio_id=' . $studio_id . ' AND fb_userid IS NULL');
                        $users = $command->queryAll();

                        if (isset($users[0]['confirmation_token']) && trim($users[0]['confirmation_token'])) {
                            $array = array("resend" => "success", "action" => $action);
                        } else {
                            $array = array("login" => "success", "action" => $action);
                        }
                    } else {
                        $array = array("login" => "success", "action" => $action);
                    }
                    if (Yii::app()->general->getStoreLink()) {
                        self::readCartFromDB($studio_id, Yii::app()->user->id);
                    }
                }
                $array['csrfToken'] = @$_SESSION['csrfToken'];
                $json = json_encode($array);
                echo $json;
                Yii::app()->end();
            } else {
                $array = json_decode(CActiveForm::validate($model), true);
                $array['csrfToken'] = @$_SESSION['csrfToken'];
                $json = json_encode($array);
                echo $json;
                Yii::app()->end();
            }
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                echo '';
                exit;
            } else {
                Yii::app()->user->setFlash('error', "Oops! Sorry trying to access a wrong url");
                $this->redirect(Yii::app()->getBaseUrl(true));
                exit;
            }
        }
    }

    public function setLoginHistoryCookie($history_id) {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $domain = Yii::app()->getBaseUrl(true);
        $domain = explode("//", $domain);
        $domain = $domain[1];
        $login_history_cookie_name = "login_history_id_" . $studio_id . "_" . $user_id;
        if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
            setcookie($login_history_cookie_name, $history_id, time() + (60 * 60 * 24 * 30), '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
        }
    }

    public function actionAjaxregister() {
        $lang_code = $this->language_code;
        $studio = $this->studio;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data']) && ((isset($_REQUEST['data']['email']) && filter_var($_REQUEST['data']['email'], FILTER_VALIDATE_EMAIL) != false) || !isset($_REQUEST['data']['email']))) {
            $studio_id = Yii::app()->common->getStudiosId();
            $name = trim($_REQUEST['data']['name']);
            $subscribe_newsletter = $_REQUEST['data']['subscribe_newsletter'];
            $email = filter_var($_REQUEST['data']['email'], FILTER_VALIDATE_EMAIL);
            $get_allcontents = Yii::app()->general->gethomepageList($studio_id, 0);
            $password = $_REQUEST['data']['password'];
            if ($get_allcontents['url'] != '') {
                $action = $get_allcontents['url'];
            } else {
                $action = 'Profile';
            }

            //Getting Ip address
            $ip_address = Yii::app()->request->getUserHostAddress();

            if ($_REQUEST['data']['email']) {
                $checkEmailExist = SdkUser::model()->findByAttributes(array('email' => $email, 'studio_id' => $studio_id));
                if (isset($checkEmailExist['email'])) {
                    $array = array("login" => "error", "message" => "Email id exist.");
                    $json = json_encode($array);
                    echo $json;
                    Yii::app()->end();
                }
            }
                if (@$studio->is_csrf_enabled) {
                    if ((@$_REQUEST['csrfToken'] === @$_SESSION['csrfToken'])) {
                        if (!Yii::app()->request->isAjaxRequest) {
                            unset($_SESSION['csrfToken']);
                        }
                    } else {
                        if (!Yii::app()->request->isAjaxRequest) {
                            $url = Yii::app()->createAbsoluteUrl();
                            header("HTTP/1.1 301 Moved Permanently");
                            header('Location: ' . $url);
                            exit();
                        } else {
                            echo '';
                            exit;
                        }
                    }
                }
                $geo_data = new EGeoIP();
                $geo_data->locate($ip_address);
                //Get referrence link from where user come this site.
                $source = isset(Yii::app()->request->cookies['source']) ? Yii::app()->request->cookies['source'] : '';

                //Make encrypted password and token
                $enc = new bCrypt();
                $pass = $enc->hash($password);
                $confirm_token = $enc->hash($email);
                if (strpos($confirm_token, "/") !== FALSE) {
                    $confirm_token = str_replace('/', '', $confirm_token);
                }

                if (isset($_POST['chk_register']) && !empty($_POST['chk_register'])) {
                    $mobile_number = $_POST['data']['mobile_number'];
                    $checkMobileExist = SdkUser::model()->findByAttributes(array('mobile_number' => $mobile_number, 'studio_id' => $studio_id));
                    $checkTempMobile = TempSdkUser::model()->findByAttributes(array('mobile_number' => $mobile_number, 'studio_id' => $studio_id));
                    if (isset($checkMobileExist['mobile_number']) || isset($checkTempMobile['mobile_number'])) {
                        $array = array("register" => "error", "message" => $this->Language['mobile_number_exists_us']);
                        $json = json_encode($array);
                        echo $json;
                        Yii::app()->end();
                    }
                    require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
                    $chk = new clickHereApi();
                    $uniqueID = $chk->getUniqueID();
                    $_REQUEST['data']['unique_id'] = $uniqueID;
                    $message = $this->Language['register_api_body_msg'];

                    /* save tempsdkuser */
                    $ret = TempSdkUser::model()->saveTempSdkUser($_REQUEST['data'], $studio_id);

                    $getApiDetails = Yii::app()->db->createCommand()
                            ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                            ->from('external_api_keys e')
                            ->join('studio_api_details s', 'e.id=s.api_type_id')
                            ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module' => 'register'))
                            ->queryRow();



                    $chk = $chk->doRegistration($mobile_number, $message, $uniqueID, $getApiDetails);

                    if ($chk['code'] === 202) {
                    Yii::app()->user->setFlash('success', $this->Language['api_register_success']);
                    } else {
                    Yii::app()->user->setFlash('error', $this->Language['api_subscribe_failed']);
                    }

                    $array = array("register" => "success", 'code' => $chk['code']);
                    $json = json_encode($array);
                    echo $json;
                    exit;
                }


                //Save SDK user detail
                $user = new SdkUser;
                $user->email = $email;
                $user->studio_id = $studio_id;
                $user->signup_ip = $ip_address;
                $user->display_name = $name;
                $user->encrypted_password = $pass;
                $user->source = $source;
                $user->status = 1;
                if($subscribe_newsletter == '1'){
                    $user->announcement_subscribe = $subscribe_newsletter;
                }else{
                    $user->announcement_subscribe = '0';
                }
                $user->confirmation_token = $confirm_token;
                $user->signup_location = serialize($geo_data);
                $user->created_date = new CDbExpression("NOW()");
                $user->user_language = $this->language_code;
                $user->save();
                $user_id = $user->id;

                $is_subscribed = 0;

                // Send welcome email to user
                $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'welcome_email', '', '', '', '', '', '', $lang_code);
                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email', $studio_id);
                if ($isEmailToStudio) {
                    $admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'admin_new_user_registration', $user_id, '', 0);
                }

                //Validate user identity
                $identity = new UserIdentity($email, $password);
                $identity->validate();
                Yii::app()->user->login($identity);

                if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
                    $studio_id = Yii::app()->common->getStudiosId();
                    $usr = Yii::app()->common->getSDKUserInfo(Yii::app()->user->id);
                    if ($usr->add_video_log) {
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = Yii::app()->user->id;
                        $login_at = date('Y-m-d H:i:s');
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = $login_at;
                        $login_history->logout_at = '0000-00-00 00:00:00';
                        $login_history->ip = $ip_address;
                        $login_history->last_login_check = $login_at;
                        $login_history->save();
                        Yii::app()->session['login_history_id'] = $login_history->id;
                        $this->setLoginHistoryCookie($login_history->id);
                    }
                }

                if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id) && isset($this->studio->parent_theme) && trim($this->studio->parent_theme) == "singleshow-a") {

                    $command = Yii::app()->db->createCommand()
                            ->select('confirmation_token')
                            ->from('sdk_users')
                            ->where('id = "' . Yii::app()->user->id . '" AND studio_id=' . $studio_id . ' AND fb_userid IS NULL');
                    $users = $command->queryAll();

                    if (isset($users[0]['confirmation_token']) && trim($users[0]['confirmation_token'])) {
                        $array = array("resend" => "success", "action" => $action);
                    } else {
                        $array = array("login" => "success", "action" => $action);
                    }
                } else {
                    $array = array("login" => "success", "action" => $action);
                }

                //Yii::app()->user->setFlash("success", "Proceed for Payment.");
                $json = json_encode($array);
                echo $json;
                Yii::app()->end();
            }

        $array = array("login" => "error", "action" => 'pay');
        Yii::app()->user->setFlash("success", $this->ServerMessage['error_saving_data']);
        $json = json_encode($array);
        echo $json;
        Yii::app()->end();
    }

    function actionConfirmation() {
        $studio_id = Yii::app()->common->getStudiosId();

        if (isset($_REQUEST['token']) && trim($_REQUEST['token'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('confirmation_token')
                    ->from('sdk_users')
                    ->where('confirmation_token = "' . trim($_REQUEST['token']) . '" AND studio_id=' . $studio_id . ' AND fb_userid IS NULL');
            $users = $command->queryAll();
            if (isset($users) && !empty($users)) {
                $qry = "UPDATE sdk_users SET confirmation_token='', confirmed_at = NOW() WHERE confirmation_token = '" . trim($_REQUEST['token']) . "' AND studio_id=" . $studio_id;
                Yii::app()->db->createCommand($qry)->execute();

                Yii::app()->user->setFlash('success', $this->ServerMessage['account_confirmed_successfully']);
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
        } else if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
            $command = Yii::app()->db->createCommand()
                    ->select('email, confirmation_token')
                    ->from('sdk_users')
                    ->where('confirmation_token != "" AND id= ' . Yii::app()->user->id . ' AND studio_id=' . $studio_id . ' AND fb_userid IS NULL');
            $users = $command->queryAll();

            if (isset($users) && !empty($users)) {
                $this->render('confirmation', array('email' => $users[0]['email'], 'studio_id' => $studio_id));
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
        } else {
            Yii::app()->user->setFlash('error', $this->ServerMessage['invalid_token_for_confirm']);
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
    }

    function actionResendVerification() {
        if (isset($_REQUEST['email']) && !empty($_REQUEST['email']) && isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = Yii::app()->user->id;

            $resend_confirmation = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'resend_confirmation', '', '', '', $_REQUEST['email'], '', '', $this->language_code);

            $array = array("resend" => "success");
            Yii::app()->user->setFlash('success', $this->ServerMessage['verification_link_sent']);
            $json = json_encode($array);
            echo $json;
            Yii::app()->end();
        } else {
            Yii::app()->user->setFlash('error', $this->ServerMessage['oops_invalid_email']);
        }

        Yii::app()->getBaseUrl(true) . "/user/confirmation";
    }

    function isAjaxRequest() {
        if (!Yii::app()->request->isAjaxRequest) {
            $url = Yii::app()->createAbsoluteUrl();
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: ' . $url);
            exit();
        }
    }

    function actionGetppvPlans() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;

        //For login or registered user
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;
            $data = array();
            $data['stream_id'] = $stream_code = isset($_POST['stream_id']) ? $_POST['stream_id'] : '0';
            $data['multipart_season'] = $season = isset($_POST['season']) ? $_POST['season'] : 0;
            $data['purchase_type'] = @$_POST['purchase_type'];
            $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
            $cond = '';
            $condArr = array(':uniq_id' => $movie_code, ':studio_id' => $studio_id);
            if ($stream_code) {
                $cond = " AND m.embed_id=:embed_id";
                $condArr[":embed_id"] = $stream_code;
            }
            if ($season) {
                $cond .= " AND m.series_number=:series_number";
                $condArr[":series_number"] = $season;
            }

            $command = Yii::app()->db->createCommand()
                    ->select('f.id,m.id as stream_id, f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f , movie_streams m')
                    ->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $stream_id = @$films['stream_id'];

            $can_see_data = array();
            $can_see_data['movie_id'] = $movie_id;
            $can_see_data['stream_id'] = @$stream_id;
            $can_see_data['season'] = @$season;
            $can_see_data['purchase_type'] = @$_POST['purchase_type'];
            $can_see_data['studio_id'] = $studio_id;
            $can_see_data['user_id'] = $user_id;
            $can_see_data['default_currency_id'] = $this->studio->default_currency_id;
            $can_see_data['is_monetizations_menu'] = 1;

            $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);

            $error = array();
            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $error['msg'] = $this->ServerMessage['activate_subscription_watch_video'];
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $error['msg'] = $this->ServerMessage['reactivate_subscription_watch_video'];
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $error['msg'] = $this->ServerMessage['video_restiction_in_your_country'];
            } else if ($mov_can_see == 'advancedpurchased') {
                $error['msg'] = $this->ServerMessage['already_purchase_this_content'];
            } else if ($mov_can_see == 'allowed') {
                echo 1;
                exit;
            } else {
                if (array_key_exists('playble_error_msg', $mov_can_see)) {
                    if ($mov_can_see['playble_error_msg'] == 'access_period') {
                        $mov_can_see['playble_error_msg'] = $this->ServerMessage['access_period_expired'];
                    } else if ($mov_can_see['playble_error_msg'] == 'watch_period') {
                        $mov_can_see['playble_error_msg'] = $this->ServerMessage['watch_period_expired'];
                    } else if ($mov_can_see['playble_error_msg'] == 'maximum') {
                        $mov_can_see['playble_error_msg'] = $this->ServerMessage['crossed_max_limit_of_watching'];
                    } else if ($mov_can_see['playble_error_msg'] == 'already_purchased') {
                        $error['msg'] = $this->ServerMessage['already_purchase_this_content'];
                        unset($mov_can_see['playble_error_msg']);
                    }
                }

                $monetization = $mov_can_see;
                $content_types_id = $films['content_types_id'];

                $data['isadv'] = $isadv = isset($_POST['isadv']) ? $_POST['isadv'] : 0;
                $data['is_ppv_bundle'] = $is_ppv_bundle = isset($_POST['is_ppv_bundle']) ? $_POST['is_ppv_bundle'] : 0;

                if (intval($content_types_id)) {
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                }

                $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                    $gateways = $gateway['gateways'];
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
                }
            }
            $data['download_type'] = @$_POST['download_type'];
            $is_preorder_content = @$_POST['isadv'];
            $not_refresh_on_close = (isset($_POST['onCloseNotRefresh']) && $_POST['onCloseNotRefresh'] == 1 ) ? true : false;
            Yii::app()->theme = 'bootstrap';
            $this->render('monetizationplans', array('gateways' => @$gateways, 'gateway_code' => @$gateway_code, 'films' => $films, 'data' => @$data, 'monetization' => $monetization, 'error' => @$error, 'is_preorder_content' => @$is_preorder_content, 'not_refresh_on_close' => $not_refresh_on_close));
        } else {
            Yii::app()->user->setFlash('error', $this->ServerMessage['oops_invalid_video']);
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit();
        }
    }

    function actionGetppvPlan() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $data = array();
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;

            $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
            $data['stream_id'] = $stream_code = isset($_POST['stream_id']) ? $_POST['stream_id'] : '0';
            $season = isset($_POST['season']) ? $_POST['season'] : 0;

            $cond = '';
            $condArr = array(':uniq_id' => $movie_code, ':studio_id' => $studio_id);
            if ($stream_code) {
                $cond = " AND m.embed_id=:embed_id";
                $condArr[":embed_id"] = $stream_code;
            }
            if ($season) {
                $cond .= " AND m.series_number=:series_number";
                $condArr[":series_number"] = $season;
            }

            $command = Yii::app()->db->createCommand()
                    ->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f, movie_streams m')
                    ->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $stream_id = @$films['stream_id'];
            $content_types_id = $data['content_types_id'] = $films['content_types_id'];

            if (intval($content_types_id) == 3) {
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                $EpDetails = $this->getEpisodeToPlay($movie_id);

                if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                    $series = explode(',', $EpDetails->series_number);
                    sort($series);

                    $data['max_season'] = $series[count($series) - 1];
                    $data['min_season'] = $series[0];

                    $data['series_number'] = intval($season) ? $season : $series[0];

                    $condEpi = array(':studio_id' => $studio_id, ':movie_id' => $movie_id, ':series_number' => $data['series_number']);
                    $episql = Yii::app()->db->createCommand()
                            ->select('m.*')
                            ->from('movie_streams m ')
                            ->where('m.studio_id=:studio_id AND m.is_episode=1 AND m.movie_id=:movie_id AND m.series_number=:series_number AND m.episode_parent_id=0', $condEpi, array('order' => 'episode_number ASC'));
                    $episodes = $episql->queryAll();

                    if (isset($episodes) && !empty($episodes)) {
                        $data['episodes'] = $episodes;
                    }
                }
            }

            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            $connection = Yii::app()->db;

            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();

                $is_subscribed = Yii::app()->common->isSubscribed($user_id);
                if ($is_subscribed) {
                    $data['member_subscribed'] = 1;
                }

                $condcard = array(':studio_id' => $studio_id, ':user_id' => $user_id);
                $cardsql = Yii::app()->db->createCommand()
                        ->select('c.id, c.gateway_id, c.card_name, c.card_holder_name, c.card_last_fourdigit, c.card_type, c.exp_month, c.exp_year')
                        ->from('sdk_card_infos c')
                        ->where('c.studio_id=:studio_id AND c.user_id=:user_id AND c.gateway_id !=1 AND c.gateway_id !=4', $condcard, array('order' => 'is_cancelled ASC, id DESC'), array('group' => 'exp_month, exp_year, card_holder_name, card_type, card_last_fourdigit'));
                $cards = $cardsql->queryAll();

                //If it has been on advance purchase
                $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);

                if (empty($plan)) {
                    //Check ppv plan set or not by studio admin
                    $plan = Yii::app()->common->getContentPaymentType($content_types_id, $films['ppv_plan_id']);
                }
                $ppv_id = $plan->id;

                $default_currency_id = $this->studio->default_currency_id;
                $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
                $currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;

                if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                    $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                    $season_price_data['ppv_pricing_id'] = $price['id'];
                    $season_price_data['studio_id'] = $studio_id;
                    $season_price_data['currency_id'] = $price['currency_id'];
                    $season_price_data['season'] = (intval($season)) ? $season : 1;
                    $season_price_data['is_subscribed'] = $is_subscribed;

                    $default_season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                    if ($default_season_price != -1) {
                        $price['default_season_price'] = $default_season_price;
                    }
                }

                $currency = Currency::model()->findByPk($currency_id);
                $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $data['is_coupon_exists'] = self::isCouponExists();
            }

            Yii::app()->theme = 'bootstrap';
            $this->render('ppvplans', array('gateways' => @$gateways, 'gateway_code' => @$gateway_code, 'plan' => @$plan, 'data' => @$data, 'season' => @$season, 'price' => @$price, 'currency' => @$currency, 'validity' => @$validity, 'films' => @$films, 'cards' => @$cards, 'ppv_buy' => @$ppv_buy));
        }
    }

    function actionGetPPVSeasonPrice() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $arg = array();

        $arg['ppv_plan_id'] = isset($_POST['ppv_plan_id']) ? $_POST['ppv_plan_id'] : 0;
        $arg['ppv_pricing_id'] = isset($_POST['ppv_pricing_id']) ? $_POST['ppv_pricing_id'] : 0;
        $arg['studio_id'] = $studio_id = $this->studio->id;
        $arg['currency_id'] = isset($_POST['currency_id']) ? $_POST['currency_id'] : ((isset($this->studio->default_currency_id) && intval($this->studio->default_currency_id)) ? $this->studio->default_currency_id : Studio::model()->findByPk($studio_id)->default_currency_id);
        $arg['is_subscribed'] = isset($_POST['isSubscribed']) ? $_POST['isSubscribed'] : 0;
        $arg['season'] = isset($_POST['season']) ? $_POST['season'] : 0;

        $season_price = Yii::app()->common->getPPVSeasonPrice($arg);
        print $season_price;
        exit;
    }

    public function actionGetSeasonPrice() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $default_season_price = 0.00;
        $studio_id = $this->studio->id;
        $user_id = (isset($_POST['user_id']) && $_POST['user_id'] != "") ? $_POST['user_id'] : 0;
        $addBefore = (isset($_POST['addBefore']) && $_POST['addBefore'] != "") ? $_POST['addBefore'] : 0;
        $ppv_id = isset($_POST['ppv_plan_id']) ? $_POST['ppv_plan_id'] : 0;
        $default_currency_id = $this->studio->default_currency_id;
        $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
        $isSubscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);

        $default_season_price = (intval($isSubscribed)) ? $price['season_subscribed'] : $price['season_unsubscribed'];

        if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
            $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
            $season_price_data['ppv_pricing_id'] = $price['id'];
            $season_price_data['studio_id'] = $studio_id;
            $season_price_data['currency_id'] = $price['currency_id'];
            $season_price_data['season'] = isset($_POST['season']) ? $_POST['season'] : 1;
            $season_price_data['is_subscribed'] = $isSubscribed;

            $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
            if ($season_price != -1) {
                $default_season_price = $season_price;
            }
        }
        $ppv_price = Yii::app()->common->formatPrice($default_season_price, $price['currency_id']);
        if (abs($default_season_price) > 0.00001) {
            if ($addBefore == 1) {
                $ppv_price = $this->Language['for'] . " " . $ppv_price;
            }
        } else {
            $ppv_price = $default_season_price;
        }
        echo $ppv_price;
        exit;
    }

    function actionGetPreOrderPlan() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $plan = '';
        $data = array();

        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;

            $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $data['content_types_id'] = $films['content_types_id'];

            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);

            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();

                $is_subscribed = Yii::app()->common->isSubscribed($user_id);
                if ($is_subscribed) {
                    $data['member_subscribed'] = 1;
                }

                $condcard = array(':studio_id' => $studio_id, ':user_id' => $user_id);
                $cardsql = Yii::app()->db->createCommand()
                        ->select('c.id, c.gateway_id, c.card_name, c.card_holder_name, c.card_last_fourdigit, c.card_type, c.exp_month, c.exp_year')
                        ->from('sdk_card_infos c')
                        ->where('c.studio_id=:studio_id AND c.user_id=:user_id AND c.gateway_id !=1 AND c.gateway_id !=4', $condcard, array('order' => 'is_cancelled ASC, id DESC'), array('group' => 'exp_month, exp_year, card_holder_name, card_type, card_last_fourdigit'));
                $cards = $cardsql->queryAll();

                //If it has been on advance purchase
                $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                $ppv_id = $plan->id;
                $data['isadv'] = 1;

                $default_currency_id = $this->studio->default_currency_id;
                $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
                $currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;

                $currency = Currency::model()->findByPk($currency_id);

                $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');

                $data['is_coupon_exists'] = self::isCouponExists();
            }

            Yii::app()->theme = 'bootstrap';
            $this->render('preorderplans', array('gateways' => @$gateways, 'gateway_code' => @$gateway_code, 'plan' => @$plan, 'data' => @$data, 'price' => @$price, 'currency' => @$currency, 'validity' => @$validity, 'films' => @$films, 'cards' => @$cards));
        }
    }

    function actionGetPpvBundlePlan() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $plan = '';
        $data = array();

        //For login or registered user
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;

            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();

                $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
                $command = Yii::app()->db->createCommand()
                        ->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                        ->from('films f ')
                        ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
                $films = $command->queryRow();
                $movie_id = $films['id'];
                $content_types_id = $data['content_types_id'] = $films['content_types_id'];

                $data['is_ppv_bundle'] = $is_ppv_bundle = 1;
                $default_currency_id = $currency_id = $this->studio->default_currency_id;

                $plan = Yii::app()->common->getAllPPVBundle($movie_id, $content_types_id, $films['ppv_plan_id']);

                $ppv_id = $plan[0]->id;
                if (isset($plan[0]->is_timeframe) && intval($plan[0]->is_timeframe)) {
                    $timeframe_prices = Yii::app()->common->getAllTimeFramePrices($ppv_id, $default_currency_id, $studio_id);
                    $currency_id = (isset($timeframe_prices[0]['currency_id']) && trim($timeframe_prices[0]['currency_id'])) ? $timeframe_prices[0]['currency_id'] : $default_currency_id;
                }

                $currency = Currency::model()->findByPk($currency_id);
                $data['is_coupon_exists'] = self::isCouponExists();

                $condcard = array(':studio_id' => $studio_id, ':user_id' => $user_id);
                $cardsql = Yii::app()->db->createCommand()
                        ->select('c.id, c.gateway_id, c.card_name, c.card_holder_name, c.card_last_fourdigit, c.card_type, c.exp_month, c.exp_year')
                        ->from('sdk_card_infos c')
                        ->where('c.studio_id=:studio_id AND c.user_id=:user_id AND c.gateway_id !=1 AND c.gateway_id !=4', $condcard, array('order' => 'is_cancelled ASC, id DESC'), array('group' => 'exp_month, exp_year, card_holder_name, card_type, card_last_fourdigit'));
                $cards = $cardsql->queryAll();

                Yii::app()->theme = 'bootstrap';
                $this->render('ppvbundleplans', array('gateways' => @$gateways, 'gateway_code' => @$gateway_code, 'plan' => @$plan, 'currency' => @$currency, 'data' => @$data, 'films' => @$films, 'cards' => @$cards, 'timeframe_prices' => @$timeframe_prices, 'is_ppv_bundle' => $is_ppv_bundle));
            }
        }
    }

    public function actionSaveGeneralInfoUser(){
        $lang_code = $this->language_code;
        $studio = $this->studio;
        $guest_id = 0;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            
            $studio_id = Yii::app()->common->getStudiosId();
            $name = trim($_REQUEST['data']['name']);
            $email = filter_var($_REQUEST['data']['email'], FILTER_VALIDATE_EMAIL);
            $get_allcontents = Yii::app()->general->gethomepageList($studio_id, 0);
            if ($get_allcontents['url'] != '') {
                $action = $get_allcontents['url'];
            } else {
                $action = 'Profile';
            }
            
            $guestUser = new GuestUser();
            $guestUser->studio_id = $studio_id;
            $guestUser->name = $name;
            $guestUser->email = $email;
            $guestUser->created_date = date("Y-m-d H:i:s");
            $guestUser->ip = CHttpRequest::getUserHostAddress();
            $guestUser->save();
            $guest_id = $guestUser->id;
        }
        $array = array("login" => "success", "action" => $action, "guest_id" => $guest_id);
        $json = json_encode($array);
        echo $json;
        Yii::app()->end();
    }
    
    function actionGetVoucherDownloadContent(){
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $plan = '';
        $data = array();

        if (isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
            $data['guest_id'] = $guest_id = $_POST['guest_id'];
            $cond = '';
            $condArr = array(':uniq_id' => $movie_code, ':studio_id' => $studio_id);
            if ($stream_code) {
                $cond = " AND m.embed_id=:embed_id";
                $condArr[":embed_id"] = $stream_code;
            }
            if ($season) {
                $cond .= " AND m.series_number=:series_number";
                $condArr[":series_number"] = $season;
            }

            $command = Yii::app()->db->createCommand()
                    ->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f, movie_streams m')
                    ->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $stream_id = @$films['stream_id'];
            $content_types_id = $data['content_types_id'] = $films['content_types_id'];

            //check free content
            $free_data = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id, movie_id')->from('free_contents')
                    ->where('studio_id=:studio_id AND movie_id=:movie_id', array(':studio_id' => $studio_id, ':movie_id' => $movie_id))
                    ->queryAll();
            $new_array_show = array();
            $new_array_season = array();
            $new_array_episode = array();
            if (isset($free_data) && !empty($free_data)) {
                for ($i = 0; $i < count($free_data); $i++) {
                    if ($free_data[$i]['season_id'] == 0 && $free_data[$i]['episode_id'] == 0) {
                        $new_array_show = $free_data[$i]['movie_id'];
                    } else if ($free_data[$i]['season_id'] != 0 && $free_data[$i]['episode_id'] == 0) {
                        $new_array_season[] = $free_data[$i]['season_id'];
                    } else if ($free_data[$i]['season_id'] != 0 && $free_data[$i]['episode_id'] != 0) {
                        $new_array_episode[] = $free_data[$i]['episode_id'];
                    }
                }
            }

            if (intval($stream_id)) {

                if (intval($content_types_id) == 3) {
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    $EpDetails = $this->getEpisodeToPlay($movie_id);

                    if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                        $series = explode(',', $EpDetails->series_number);
                        sort($series);

                        $data['max_season'] = $series[count($series) - 1];
                        $data['min_season'] = $series[0];

                        $data['series_number'] = intval($season) ? $season : $series[0];

                        $condEpi = array(':studio_id' => $studio_id, ':movie_id' => $movie_id, ':series_number' => $data['series_number']);
                        $episql = Yii::app()->db->createCommand()
                                ->select('m.*')
                                ->from('movie_streams m ')
                                ->where('m.studio_id=:studio_id AND m.is_episode=1 AND m.movie_id=:movie_id AND m.series_number=:series_number', $condEpi, array('order' => 'episode_number ASC'));
                        $episodes = $episql->queryAll();

                        if (isset($episodes) && !empty($episodes)) {
                            $data['episodes'] = $episodes;
                        }
                    }
                }

                $sql_mstream = Yii::app()->db->createCommand()
                        ->select('ms.id,ms.movie_id,ms.episode_number,ms.series_number,ms.episode_title')
                        ->from('movie_streams ms ')
                        ->where(' ms.id=:ms_id AND ms.movie_id=:movie_id', array(':ms_id' => $stream_id, ':movie_id' => $movie_id));
                $stream_data = $sql_mstream->queryAll();

                $content_name = "";
                $search_str = "";
                $con_type = "";
                if ($stream_data[0]['movie_id'] != "") {
                    $search_str .= $stream_data[0]['movie_id'];
                    $content_name .= $films['name'];
                }if ($stream_data[0]['series_number'] != 0 && intval($content_types_id) == 3) {
                    $search_str .= ":" . $stream_data[0]['series_number'];
                    $content_name .= "-Season-" . $stream_data[0]['series_number'];
                }if ($stream_data[0]['episode_number'] != "" && intval($content_types_id) == 3) {
                    if ($stream_data[0]['series_number'] == 0) {
                        $search_str .= ":0:" . $stream_data[0]['id'];
                    } else {
                        $search_str .= ":" . $stream_data[0]['id'];
                        $content_name .= "-Season-" . $stream_data[0]['episode_title'];
                    }
                }

                $content = "'" . $search_str . "'";
                $voucher_exist = Yii::app()->common->isVoucherExists($studio_id, $content);
            }
            if(intval($voucher_exist) == 1){
                Yii::app()->theme = 'bootstrap';
                $this->render('voucherplan_download', array('studio_id' => $studio_id, 'free_data_show' => $new_array_show, 'free_data_season' => $new_array_season, 'free_data_episode' => $new_array_episode, 'content' => $search_str, 'cnt_name' => $content_name, 'films' => @$films, 'data' => @$data, 'season' => @$season));
            }else{
                echo 1;exit;
            }
        }
    }
    
    function actionGetVoucherPlan() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $plan = '';
        $data = array();

        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;
            $data['download_type'] = @$_POST['download_type'];
            $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
            $data['stream_id'] = $stream_code = isset($_POST['stream_id']) ? $_POST['stream_id'] : '0';
            $season = isset($_POST['season']) ? $_POST['season'] : 0;

            $cond = '';
            $condArr = array(':uniq_id' => $movie_code, ':studio_id' => $studio_id);
            if ($stream_code) {
                $cond = " AND m.embed_id=:embed_id";
                $condArr[":embed_id"] = $stream_code;
            }
            if ($season) {
                $cond .= " AND m.series_number=:series_number";
                $condArr[":series_number"] = $season;
            }

            $command = Yii::app()->db->createCommand()
                    ->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f, movie_streams m')
                    ->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $stream_id = @$films['stream_id'];
            $content_types_id = $data['content_types_id'] = $films['content_types_id'];

            //check free content
            $free_data = Yii::app()->db->createCommand()
                        ->select('season_id, episode_id, movie_id')->from('free_contents')
                        ->where('studio_id=:studio_id AND movie_id=:movie_id', array(':studio_id'=>$studio_id,':movie_id'=>$movie_id))
                        ->queryAll();
            $new_array_show = array();
            $new_array_season = array();
            $new_array_episode = array();
            if(isset($free_data) && !empty($free_data)){
                for($i = 0; $i < count($free_data);$i++){
                    if($free_data[$i]['season_id'] == 0 && $free_data[$i]['episode_id'] == 0){
                        $new_array_show = $free_data[$i]['movie_id'];
                    }else if($free_data[$i]['season_id'] != 0 && $free_data[$i]['episode_id'] == 0){
                        $new_array_season[] = $free_data[$i]['season_id'];
                    }else if($free_data[$i]['season_id'] != 0 && $free_data[$i]['episode_id'] != 0){
                       $new_array_episode[] = $free_data[$i]['episode_id']; 
                    }
                }
            }
            
            if (intval($stream_id)) {

                if (intval($content_types_id) == 3) {
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    $EpDetails = $this->getEpisodeToPlay($movie_id);

                    if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                        $series = explode(',', $EpDetails->series_number);
                        sort($series);

                        $data['max_season'] = $series[count($series) - 1];
                        $data['min_season'] = $series[0];

                        $data['series_number'] = intval($season) ? $season : $series[0];

                        $condEpi = array(':studio_id' => $studio_id, ':movie_id' => $movie_id, ':series_number' => $data['series_number']);
                        $episql = Yii::app()->db->createCommand()
                                ->select('m.*')
                                ->from('movie_streams m ')
                                ->where('m.studio_id=:studio_id AND m.is_episode=1 AND m.movie_id=:movie_id AND m.series_number=:series_number', $condEpi, array('order' => 'episode_number ASC'));
                        $episodes = $episql->queryAll();

                        if (isset($episodes) && !empty($episodes)) {
                            $data['episodes'] = $episodes;
                        }
                    }
                }

                $sql_mstream = Yii::app()->db->createCommand()
                        ->select('ms.id,ms.movie_id,ms.episode_number,ms.series_number,ms.episode_title')
                        ->from('movie_streams ms ')
                        ->where(' ms.id=:ms_id AND ms.movie_id=:movie_id', array(':ms_id' => $stream_id, ':movie_id' => $movie_id));
                $stream_data = $sql_mstream->queryAll();

                $content_name = "";
                $search_str = "";
                $con_type = "";
                if ($stream_data[0]['movie_id'] != "") {
                    $search_str .= $stream_data[0]['movie_id'];
                    $content_name .= $films['name'];
                }if ($stream_data[0]['series_number'] != 0 && intval($content_types_id) == 3) {
                    $search_str .= ":" . $stream_data[0]['series_number'];
                    $content_name .= "-Season-" . $stream_data[0]['series_number'];
                }if ($stream_data[0]['episode_number'] != "" && intval($content_types_id) == 3) {
                    if ($stream_data[0]['series_number'] == 0) {
                        $search_str .= ":0:" . $stream_data[0]['id'];
                    } else {
                        $search_str .= ":" . $stream_data[0]['id'];
                        $content_name .= "-Season-" . $stream_data[0]['episode_title'];
                    }
                }

                $content = "'" . $search_str . "'";
                $voucher_exist = Yii::app()->common->isVoucherExists($studio_id, $content);
            }

            Yii::app()->theme = 'bootstrap';
            $this->render('voucherplan', array('studio_id' => $studio_id,'free_data_show'=>$new_array_show, 'free_data_season'=>$new_array_season, 'free_data_episode'=>$new_array_episode,'user_id' => $user_id, 'content' => $search_str, 'cnt_name' => $content_name, 'films' => @$films, 'data' => @$data, 'season' => @$season));
        }
    }

    function actionGetBundledContents() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $contents = '';
        if (isset($_POST['plan']) && intval($_POST['plan'])) {
            $studio_id = $this->studio->id;
            $sql = "SELECT f.name FROM films f, ppv_advance_content pac WHERE pac.ppv_plan_id=" . $_POST['plan'] . " AND f.studio_id=" . $studio_id . " AND f.id=pac.content_id";
            $data = Yii::app()->db->createCommand($sql)->queryAll();

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $contents.=ucfirst($value['name']) . "<br/>";
                }
            }
        }
        print $contents;
        exit;
    }

    function actionGetBundlePrice() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $studio_id = $this->studio->id;
        $user_id = Yii::app()->user->id;
        $default_currency_id = $this->studio->default_currency_id;
        $plan_id = @$_POST['plan_id'];
        $is_subscribed = Yii::app()->common->isSubscribed($user_id);

        $planModel = new PpvPlans();
        $plan = $planModel->findByPk($plan_id);

        $data = array();

        if ($plan->is_advance_purchase == 2) {


            $tmprice = 0;
            $data['is_timeframe'] = 0;
            if (isset($plan->is_timeframe) && intval($plan->is_timeframe)) {
                $timeframe_prices = Yii::app()->common->getAllTimeFramePrices($plan_id, $default_currency_id, $studio_id);

                if (!empty($timeframe_prices)) {
                    $data['is_timeframe'] = 1;
                    $cnt = 1;
                    foreach ($timeframe_prices as $key => $value) {
                        $data['timeframeprice'][$key]['id'] = $value['id'];
                        $data['timeframeprice'][$key]['title'] = $value['title'];

                        if ($is_subscribed) {
                            if ($cnt == 1) {
                                $tmprice = $value['price_for_subscribed'];
                            }

                            $data['timeframeprice'][$key]['price'] = $value['price_for_subscribed'];
                        } else {
                            if ($cnt == 1) {
                                $tmprice = $value['price_for_unsubscribed'];
                            }

                            $data['timeframeprice'][$key]['price'] = $value['price_for_unsubscribed'];
                        }

                        $cnt++;
                    }
                }
            }

            $data['price'] = $tmprice;
        } else if ($plan->is_advance_purchase == 0) {
            $price = Yii::app()->common->getPPVPrices($plan_id, $default_currency_id);

            if ($plan->content_types_id != 3) {
                if ($is_subscribed) {
                    $data['price'] = $price['price_for_subscribed'];
                } else {
                    $data['price'] = $price['price_for_unsubscribed'];
                }
            } else {
                if ($is_subscribed) {
                    $data['price'] = $price['show_subscribed'];
                } else {
                    $data['price'] = $price['show_unsubscribed'];
                }
            }
        }

        print json_encode($data);
        exit;
    }

    function isCouponExists() {
        $studio_id = $this->studio->id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);

        $isCouponExists = 0;
        if (isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 32)) {
            //$default_currency_id = $this->studio->default_currency_id;
            //$sql = "SELECT c.id FROM coupon c, coupon_currency cc WHERE c.studio_id = {$studio_id} AND c.status=1 AND c.id=cc.coupon_id AND cc.currency_id={$default_currency_id}";
            //$sql = "SELECT c.id FROM coupon c WHERE c.studio_id = {$studio_id} AND c.status=1";
            //$coupon = Yii::app()->db->createCommand($sql)->queryAll();

            $coupon = Coupon::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1));

            if (isset($coupon) && !empty($coupon)) {
                $isCouponExists = 1;
            }
        }

        return $isCouponExists;
    }

    function actionGetEpisodes() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);
        $data = array();
        if (isset($_POST['movie_id']) && trim($_POST['movie_id']) && isset($_POST['season']) && intval($_POST['season'])) {
            $studio_id = $this->studio->id;

            $films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $_POST['movie_id']));
            $movie_id = $films->id;

            $connection = Yii::app()->db;
            $episql = "SELECT embed_id, episode_title,id FROM movie_streams WHERE studio_id=" . $studio_id . " AND is_episode=1 AND is_converted =1 AND full_movie !=''  AND movie_id=" . $movie_id . " AND series_number=" . $_POST['season'] . " ORDER BY episode_number ASC";
            $episodes = $connection->createCommand($episql)->queryAll();

            $free_episode_array = $_REQUEST['free_episode_array'];
            if (isset($episodes) && !empty($episodes)) {
                foreach ($episodes as $key => $value) {
                    $data[$key]['embed_id'] = $value['embed_id'];
                    $data[$key]['episode_title'] = $value['episode_title'];
                    $data[$key]['id'] = $value['id'];
                    if(in_array($value['id'],$free_episode_array)){
                        $data[$key]['free_episode'] = 1;
                    }else{
                        $data[$key]['free_episode'] = 0;
                }
            }
        }
        }

        print json_encode($data);
        exit;
    }

    function actionIsPPVSubscribed() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $studio_id = $this->studio->id;
        $user_id = Yii::app()->user->id;
        $data = array();
        $data['coupon_code'] = @$_POST['coupon_code'];
        $data['coupon_currency_id'] = @$_POST['coupon_currency_id'];

        $data['movie_id'] = @$_POST['movie_id'];
        $data['season_id'] = @$_POST['season_id'];
        $data['episode_id'] = @$_POST['episode_id'];
        $data['purchase_type'] = @$_POST['purchase_type'];
        $data['isadv'] = $isadv = @$_POST['isadv'];
        $data['is_advance_purchase'] = @$_POST['isadv'];


        //Used for PPV Bundle
        $data['plan'] = @$_POST['plan'];
        $timeframe_id = $data['timeframe'] = @$_POST['timeframe'];
        $is_ppv_bundle = $data['is_ppv_bundle'] = @$_POST['is_ppv_bundle'];
        $is_subscription_bundle = $data['is_subscription_bundle'] = @$_POST['is_subscription_bundle'];

        $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
        $command = Yii::app()->db->createCommand()
                ->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                ->from('films f ')
                ->where('f.uniq_id=:uniq_id AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
        $movie = $command->queryAll();
        $films = $movie[0];
        $movie_id = $films['id'];
        $content_types_id = $films['content_types_id'];

        $stream_code = isset($_POST['episode_id']) ? $_POST['episode_id'] : '0';
        $season = isset($_POST['season_id']) ? $_POST['season_id'] : 0;

        //Get stream Id
        $stream_id = 0;
        if (trim($movie_code) != '0' && trim($stream_code) != '0') {
            $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code);
        } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
            $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season);
        } else {
            $stream_id = Yii::app()->common->getStreamId($movie_id);
        }

        $default_currency_id = $this->studio->default_currency_id;
        $currency = Currency::model()->findByPk($default_currency_id);
        $data['currency_id'] = $currency->id;
        $data['currency_code'] = $currency->code;
        $can_see_data['movie_id'] = $movie_id;
        $can_see_data['stream_id'] = $stream_id;
        $can_see_data['purchase_type'] = $data['purchase_type'];
        $can_see_data['studio_id'] = $studio_id;
        $can_see_data['user_id'] = $user_id;
        $can_see_data['default_currency_id'] = $default_currency_id;

        //For PPV Bundle
        $can_see_data['ppv_id'] = $data['plan'];
        $can_see_data['timeframe_id'] = $data['timeframe'];
        $can_see_data['is_ppv_bundle'] = $data['is_ppv_bundle'];

        $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);

        if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
            $data['msg'] = $this->ServerMessage['activate_subscription_watch_video'];
        } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
            $data['msg'] = $this->ServerMessage['reactivate_subscription_watch_video'];
        } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
            $data['msg'] = $this->ServerMessage['video_restiction_in_your_country'];
        } else if ($mov_can_see == 'advancedpurchased') {
            $data['msg'] = $this->ServerMessage['already_purchase_this_content'];
        } else if ($mov_can_see == 'allowed') {
            echo 1;
            exit;
        } else if ($mov_can_see == 'already_purchased') {
            $data['msg'] = $this->ServerMessage['already_purchase_this_content'];
        } else if ($mov_can_see == 'unpaid' || $mov_can_see == 'access_period' || $mov_can_see == 'watch_period' || $mov_can_see == 'maximum') {
            $data['playble_error_msg'] = '';

            if (intval($is_ppv_bundle)) {
                $data['purchase_type'] = 'bundle';
                $plan = Yii::app()->common->getPPVBundleDetail($data['plan']);

                $data['bundle_title'] = $plan->title;
                $ppv_id = $plan->id;

                $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);

                if ($timeframe_id) {
                    $price = Yii::app()->common->getTimeFramePrice($ppv_id, $timeframe_id, $studio_id);
                    if (intval($is_subscribed_user)) {
                        $amount = $price->price_for_subscribed;
                    } else {
                        $amount = $price->price_for_unsubscribed;
                    }
                } else {
                    //Getting pricing according to currency
                    $price = Yii::app()->common->getPPVPrices($ppv_id, $this->studio->default_currency_id);
                    $data['currency_id'] = $price['currency_id'];
                    $data['currency_code'] = $price['currency_code'];
                    if (intval($is_subscribed_user)) {
                        if ($content_types_id == 3) {
                            $amount = $price['show_subscribed'];
                        } else {
                            $amount = $price['price_for_subscribed'];
                        }
                    } else {
                        if ($content_types_id == 3) {
                            $amount = $price['show_unsubscribed'];
                        } else {
                            $amount = $price['price_for_unsubscribed'];
                        }
                    }
                }
            } else if ($is_subscription_bundle) {
                $subscription_plan_id = @$_POST['plan'];
                //$planModel = new SubscriptionBundlesPlan();
                //$plan = $planModel->findByPk($subscription_plan_id);
                //$data['bundle_title'] = $plan->name;
                //$pricew = Yii::app()->common->getSubscriptionBundlesPrice($subscription_plan_id, $this->studio->default_currency_id, $studio_id);
                //echo $amount = $pricew['price']; 
                     $data = array();
                     $data['studio_id'] = $studio_id;
                     $data['coupon'] = $_POST["coupon_code"];
                     $data['plan'] = $_POST["plan"];

                     $data['user_id'] = $user_id;
                $planDetails = Yii::app()->common->getAllSubscriptionBundlesPrices($subscription_plan_id, $default_currency_id, $studio_id);
                $currency_id = (isset($planDetails[0]['currency_id']) && trim($planDetails[0]['currency_id'])) ? $planDetails[0]['currency_id'] : $default_currency_id;
                $currency = Currency::model()->findByPk($currency_id);
                $data['currency'] = $currency_id;
                //$planDetails = SubscriptionPlans::model()->getPlanDetails($subscription_plan_id,$studio_id);
               $amount = $planDetails[0]['price'];

                if (!empty($_POST["coupon_code"]) && isset($_POST["coupon_code"])) {
                    $planDetails['currency_id'] = $currency_id;
                    $planDetails['couponCode'] = $_POST["coupon_code"];
                    $planDetails['physical'] = $_POST["physical"];
                     $couponDetails = Yii::app()->billing->isValidCouponForSubscription($data);
                    $res = Yii::app()->billing->CouponSubscriptionBundlesCalculation($couponDetails, $planDetails);
                    $amount = $res['discount_amount'];
                    $data['extend_free_trail'] = $res['extend_free_trail']; 
                      }
            } else {
                //Check ppv plan set or not by studio admin
                if (intval($isadv)) {
                    $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id);
                    $amount = self::ppvAmount($plan, $data);
                } else {

                    if ($mov_can_see == 'access_period') {
                        $data['playble_error_msg'] = $this->ServerMessage['access_period_expired'];
                    } else if ($mov_can_see == 'watch_period') {
                        $data['playble_error_msg'] = $this->ServerMessage['watch_period_expired'];
                    } else if ($mov_can_see == 'maximum') {
                        $data['playble_error_msg'] = $this->ServerMessage['crossed_max_limit_of_watching'];
                    }

                    $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                    if (empty($plan)) {
                        $plan = Yii::app()->common->getContentPaymentType($films['content_types_id'], $films['ppv_plan_id']);
                    }

                    $amount = self::ppvAmount($plan, $data);

                    if (abs($amount) < 0.00001) {
                        echo 1;
                        exit;
                    }
                }
            }

            $data['amount'] = $amount;
            $data['gateway_code'] = isset($_POST['payment_method']) && trim($_POST['payment_method']) ? $_POST['payment_method'] : '';
            //Calculate coupon if exists
            if((isset($data['coupon_code']) && $data['coupon_code'] != '') && $is_subscription_bundle!=1){
                $coupon = $data['coupon_code'];
                $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['coupon_currency_id']);
                $data['amount'] = $amount = $getCoup["amount"];
                $data['coupon'] = $getCoup["couponCode"];
                $data['currency_id'] = $data['coupon_currency_id'];

                $data['studio_id'] = $studio_id;
                $data['user_id'] = $user_id;
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code);
                $data['episode_id'] = $stream_id;
                if ($amount == 0) {
                    $res = self::setPPVAmounttoZero($studio_id, $plan, $price, $data, $isadv, $is_ppv_bundle);

                    if (intval($isadv)) {
                        $data['msg'] = $res['meg'];
                        $data['isAdvanceAmtZero'] = 1;
                    } else {
                        echo 1;
                        exit;
                    }
                }
            }
        }

        $data['plan_id'] = $plan->id;
        $data['is_pci_compliance'] = $this->IS_PCI_COMPLIANCE[$_POST['payment_method']];
        $data['is_hosted_ppv'] = 0;
        if (isset($_POST['good_type']) && trim($_POST['good_type']) == 'digital_payment') {
            if (trim($data['gateway_code']) == 'paypalpro' && $data['is_pci_compliance']) {
                $data['is_hosted_ppv'] = 1;
            }
        }
        echo json_encode($data);
        exit;
    }

    function actionVoucherSubscription() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        if ((isset($_POST['voucher']) && trim($_POST['voucher']))) {
            $data = array();

            $voucher = trim($_POST['voucher']);
            $content_id = $_POST['content_id'];
            $user_id = isset($_POST['user_id'])?$_POST['user_id']:0;
            $download_voucher = (isset($_POST['voucher_download'])) ? intval($_POST['voucher_download']) : 0;
            $isadv = (isset($_POST['is_preorder_content'])) ? intval($_POST['is_preorder_content']) : 0;
            $studio_id = Yii::app()->common->getStudiosId();

            $getVoucher = Yii::app()->common->voucherCodeValid($voucher, $user_id, $content_id, $studio_id);
            if ($getVoucher == 2) {
                $res['isError'] = 2;
            } else if ($getVoucher == 3) {
                $res['isError'] = 3;
            } else {
                $sql = "SELECT id FROM voucher WHERE studio_id={$studio_id} AND voucher_code='" . $voucher . "'";
                $voucher_id = Yii::app()->db->createCommand($sql)->queryRow();

                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';

                if (!empty($voucher_id)) {
                    if (intval($isadv) == 0 && intval($download_voucher) == 0) {
                        $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'limit_video');
                        $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                        $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'watch_period');
                        $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                        $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'access_period');
                        $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                        $time = strtotime($start_date);
                        if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                        }

                        $data['view_restriction'] = $limit_video;
                        $data['watch_period'] = $watch_period;
                        $data['access_period'] = $access_period;
                    }

                    $data['voucher_id'] = $voucher_id['id'];
                    $data['content_id'] = $content_id;
                    $data['user_id'] = $user_id;
                    $data['guestId'] = $_REQUEST['guestId'];
                    $data['studio_id'] = $studio_id;
                    $data['isadv'] = $isadv;
                    $data['start_date'] = @$start_date;
                    $data['end_date'] = @$end_date;

                    $res = Yii::app()->billing->setVoucherSubscription($data);
                    if ($isadv) {
                        $movie_name = CouponOnly::model()->getContentInfo($content_id);
                        $msgs = $this->ServerMessage['purchased'];
                        $msgs = htmlentities($msgs);
                        eval("\$msgs = \"$msgs\";");
                        $msgs = htmlspecialchars_decode($msgs);
                        Yii::app()->user->setFlash("success", $msgs);
                    }
                } else {
                    $res['isError'] = 2;
                }
            }
        } else {
            $res['isError'] = 2;
        }
        echo json_encode($res);
        exit;
    }

    function setPPVAmounttoZero($studio_id = 0, $plan, $price, $data, $isadv = 0, $is_bundle = 0) {
        //Calculate ppv expiry time only for all types of content type
        $start_date = Date('Y-m-d H:i:s');
        $end_date = '';

        if (intval($is_bundle)) {
            $timeframe_id = $price->ppv_timeframelable_id;
            $timeframe_days = PpvTimeframeLable::model()->findByPk($timeframe_id)->days;

            $time = strtotime($start_date);
            $expiry = $timeframe_days . ' ' . "days";
            $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
            $trans_data['transaction_type'] = 5;
            $data['timeframe_id'] = $data['timeframe'];
            $data['is_bundle'] = 1;
        } else {
            if (intval($isadv) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
                $trans_data['transaction_type'] = 2;
            }
        }

        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
        $video_id = $Films->id;
        $gateway_code = $data['gateway_code'];

        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);

        $trans_data['order_number'] = uniqid();
        $trans_data['invoice_id'] = uniqid() . time();
        $trans_data['transaction_status'] = 'Success';
        $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $set_ppv_subscription, $gateway_code, $isadv);

        if (intval($is_bundle)) {
            $VideoName = $plan->title;
        } else {
            $VideoName = Yii::app()->common->getVideoname($video_id);
            $VideoName = trim($VideoName);
        }

        $VideoDetails = $VideoName;
        $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $isadv, $VideoName, $VideoDetails);

        $res = '';
        if (intval(@$isadv)) {
            $res['msg'] = $this->ServerMessage['purchased'] . " '" . $VideoName . "' " . $this->ServerMessage['successfully'];
            $res['isAdvanceAmtZero'] = 1;
        } else {
            $res = 1;
        }

        return $res;
    }

    public function actionPpvpayment() {
        if (isset($this->PAYMENT_GATEWAY) && trim($this->PAYMENT_GATEWAY['instafeez']) == 'instafeez') {
            $data = array();
            parse_str($_REQUEST['data'], $data);
            $_REQUEST = '';
            $_REQUEST = $data;
        }

        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $gateway_code = isset($_REQUEST['data']['payment_method']) && trim($_REQUEST['data']['payment_method']) ? $_REQUEST['data']['payment_method'] : '';
            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = Yii::app()->user->id;
            $VideoDetails = '';
            $data = $_REQUEST['data'];
            $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
            $video_id = $Films->id;
            $plan_id = 0;

            //Check studio has plan or not
            $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);

            if ((isset($data['plan_id']) && intval($data['plan_id']))) {
                $plan_id = $data['plan_id'];
                $is_bundle = @$data['is_bundle'];
                $timeframe_id = @$data['timeframe_id'];
                $isadv = @$data['is_advance_purchase'];
                $timeframe_days = 0;
                $end_date = '0000-00-00 00:00:00';

                //Get plan detail which user selected at the time of registration
                //$plan = PpvPlans::model()->with('ppvadvancecontent')->find(array("condition" => "studio_id = " . $studio_id . " AND t.id=".$plan_id));
                $plan = PpvPlans::model()->findByPk($plan_id);

                $data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;

                if (intval($is_bundle) && intval($timeframe_id)) {
                    (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $data['timeframe_id'], $studio_id);
                    $price = $price->attributes;

                    $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
                } else {
                    $price = Yii::app()->common->getPPVPrices($plan->id, $this->studio->default_currency_id);
                }

                $currency = Currency::model()->findByPk($price['currency_id']);
                $data['currency_id'] = $currency->id;
                $data['currency_code'] = $currency->code;
                $data['currency_symbol'] = $currency->symbol;

                //Calculate ppv expiry time only for single part or episode only
                $start_date = Date('Y-m-d H:i:s');

                if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
                    $time = strtotime($start_date);
                    $expiry = $timeframe_days . ' ' . "days";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
                } else {
                    if (intval($isadv) == 0) {
                        $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                        $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                        $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                        $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                        $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                        $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                        $time = strtotime($start_date);
                        if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                        }

                        $data['view_restriction'] = $limit_video;
                        $data['watch_period'] = $watch_period;
                        $data['access_period'] = $access_period;
                    }
                }

                if ($is_bundle) {//For PPV bundle
                    $data['season_id'] = 0;
                    $data['episode_id'] = 0;

                    if (intval($is_subscribed_user)) {
                        if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                            $data['amount'] = $amount = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['price_for_subscribed'];
                        }
                    } else {
                        if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                            $data['amount'] = $amount = $price['show_unsubscribed'];
                        } else {
                            $data['amount'] = $amount = $price['price_for_unsubscribed'];
                        }
                    }
                } else {
                    //Set different prices according to schemes
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                        //Check which part wants to sell by studio admin
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                        $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                        if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                            if (intval($is_episode)) {
                                $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                                $data['episode_id'] = $streams->id;
                                $data['episode_uniq_id'] = $streams->embed_id;

                                if (intval($is_subscribed_user)) {
                                    $data['amount'] = $amount = $price['episode_subscribed'];
                                } else {
                                    $data['amount'] = $amount = $price['episode_unsubscribed'];
                                }
                            }
                        } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                            if (intval($is_season)) {
                                $data['episode_id'] = 0;

                                if (intval($is_subscribed_user)) {
                                    $data['amount'] = $amount = $price['season_subscribed'];
                                } else {
                                    $data['amount'] = $amount = $price['season_unsubscribed'];
                                }

                                if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status']) && intval($isadv) == 0) {
                                    $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                                    $season_price_data['ppv_pricing_id'] = $price['id'];
                                    $season_price_data['studio_id'] = $studio_id;
                                    $season_price_data['currency_id'] = $price['currency_id'];
                                    $season_price_data['season'] = $data['season_id'];
                                    $season_price_data['is_subscribed'] = $is_subscribed_user;

                                    $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                                    if ($season_price != -1) {
                                        $data['amount'] = $amount = $season_price;
                                    }
                                }

                                //$end_date = '';//Make unlimited time limit for season
                            }
                        } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                            if (intval($is_show)) {
                                $data['season_id'] = 0;
                                $data['episode_id'] = 0;
                                if (intval($is_subscribed_user)) {
                                    $data['amount'] = $amount = $price['show_subscribed'];
                                } else {
                                    $data['amount'] = $amount = $price['show_unsubscribed'];
                                }

                                //$end_date = '';//Make unlimited time limit for show
                            }
                        }
                    } else {//Single part videos
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['price_for_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['price_for_unsubscribed'];
                        }
                    }
                }

                $couponCode = '';
                //Calculate coupon if exists
                if ((isset($data['coupon']) && $data['coupon'] != '') || (isset($data['coupon_instafeez']) && $data['coupon_instafeez'] != '')) {
                    $coupon = (isset($data['coupon']) && $data['coupon'] != '') ? $data['coupon'] : $data['coupon_instafeez'];
                    $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
                    $data['amount'] = $amount = $getCoup["amount"];
                    $couponCode = $getCoup["couponCode"];
                }
                $data['card_holder_name'] = @$data['card_name'];

                if (isset($data['creditcard']) && trim($data['creditcard'])) {
                    $card = SdkCardInfos::model()->findByAttributes(array('id' => $data['creditcard'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                    $data['card_id'] = @$card->id;
                    $data['token'] = @$card->token;
                    $data['profile_id'] = @$card->profile_id;
                    $data['card_holder_name'] = @$card->card_holder_name;
                    $data['card_type'] = @$card->card_type;
                    $data['exp_month'] = @$card->exp_month;
                    $data['exp_year'] = @$card->exp_year;

                    $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                    $gateway_code = $gateway_info->short_code;
                }

                if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                    if (intval($is_bundle)) {
                        $VideoName = $plan->title;
                    } else {
                        $VideoName = Yii::app()->common->getVideoname($video_id);
                        $VideoName = trim($VideoName);
                    }

                    $VideoDetails = $VideoName;

                    if (!$is_bundle && isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                        if ($data['season_id'] == 0) {
                            $VideoDetails .= ', All Seasons';
                        } else {
                            $VideoDetails .= ', Season ' . $data['season_id'];
                            if ($data['episode_id'] == 0) {
                                $VideoDetails .= ', All Episodes';
                            } else {
                                $episodeName = Yii::app()->common->getEpisodename($streams->id);
                                $episodeName = trim($episodeName);
                                $VideoDetails .= ', ' . $episodeName;
                            }
                        }
                    }

                    if (abs($data['amount']) < 0.01) {

                        if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                            $data['gateway_code'] = $gateway_code;
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }

                        $data['amount'] = 0;
                        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);

                        /* if ($is_bundle) {
                          $set_ppv_content = Yii::app()->billing->setPpvSubscriptionContents($plan, $set_ppv_subscription);
                          } */
                        $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $isadv, $VideoName, $VideoDetails);
                        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                            echo json_encode($data);
                            exit;
                        }
                        self::setRedirectPpv($Films, $plan, $data, $isadv);
                    } else {
                        $data['studio_id'] = $studio_id;
                        $data['gateway_code'] = $gateway_code;

                        $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];

                        if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])) {
                            $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/user/ppvConfirm?plan_id=" . $plan_id;
                            $data['cancelURL'] = Yii::app()->getBaseUrl(true) . "/user/cancel";
                            $session_data = array(
                                'gateway_code' => $gateway_code,
                                'uniq_id' => $data['movie_id'],
                                'episode_id' => $data['episode_id'],
                                'episode_uniq_id' => $data['episode_uniq_id'],
                                'season_id' => $data['season_id'],
                                'permalink' => $data['permalink'],
                                'currency_id' => $data['currency_id'],
                                'amount' => $data['amount'],
                                'couponCode' => $data['coupon'],
                                'is_bundle' => @$data['is_bundle'],
                                'timeframe_id' => @$data['timeframe_id'],
                                'isadv' => $isadv
                            );
                            Yii::app()->session['session_data'] = $session_data;
                        }
                        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                            echo json_encode($data);
                            exit;
                        }
                        $data['user_id'] = Yii::app()->user->id;
                        $data['studio_name'] = Studio::model()->findByPk($studio_id)->name;
                        //print "<pre>";print_r($data);exit;
                        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                        $payment_gateway = new $payment_gateway_controller();
                        $trans_data = $payment_gateway::processTransactions($data);
                        $payment_error_msg = '';
                        $logfile = dirname(__FILE__) . '/ppvpayment.txt';
                        $payment_error_msg .= "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
                        $payment_error_msg .= "\n-----Studio ID: " . $studio_id . "-----\n";
                        $payment_error_msg .= "\n-----User ID: " . $user_id . "-----\n";
                        $payment_error_msg.= json_encode($trans_data) . "\n";
                        //Write into the log file
                        file_put_contents($logfile, json_encode($trans_data), FILE_APPEND | LOCK_EX);

                        $is_paid = $trans_data['is_success'];
                        if (intval($is_paid)) {
                            $data['amount'] = $trans_data['amount'];
                            if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                                $crd = Yii::app()->billing->setCardInfo($data);
                                $data['card_id'] = $crd;
                            }

                            $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
                            $ppv_subscription_id = $set_ppv_subscription;

                            /* if ($is_bundle) {
                              $set_ppv_content = Yii::app()->billing->setPpvSubscriptionContents($plan, $set_ppv_subscription);
                              } */

                            $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                            $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);

                            self::setRedirectPpv($Films, $plan, $data, $isadv);
                        } else {
                            $logfile = dirname(__FILE__) . '/ppvpayment.txt';
                            $payment_error_msg .= "\n----------Transaction Error----------\n";
                            $payment_error_msg .= "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
                            $payment_error_msg .= "\n-----Studio ID: " . $studio_id . "-----\n";
                            $payment_error_msg .= "\n-----User ID: " . $user_id . "-----\n";
                            $payment_error_msg.= json_encode($trans_data) . "\n";
                            //Write into the log file
                            file_put_contents($logfile, json_encode($trans_data), FILE_APPEND | LOCK_EX);

                            $teModel = New TransactionErrors;
                            $teModel->studio_id = $studio_id;
                            $teModel->user_id = $user_id;
                            $teModel->plan_id = $plan_id;
                            $teModel->payment_method = $this->PAYMENT_GATEWAY[$gateway_code];
                            $teModel->response_text = $trans_data['response_text'];
                            $teModel->created_date = new CDbExpression("NOW()");
                            $teModel->save();
                            
                            $lang_code = $this->language_code;
                            //$faliure_reason = json_decode($trans_data['response_text'],true);
                            $ppv_faliure_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'onetime_payment_failed', '', '', '', '', '', '', $lang_code, '', $studio_name, $data['amount'], $monetization_name, $faliure_reason);
                            Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                            $this->redirect($_SERVER['HTTP_REFERER']);
                            exit;
                        }
                    }
                } else {
                    
                }
            }
        }
    }

    /*
     * By Sanjeev
     * When comes from Paypal
     */

    public function actionConfirm() {
        $connection = Yii::app()->db;
        $studio_id = Yii::app()->common->getStudiosId();
        $reactivate = isset($_REQUEST['reactivate']) ? $_REQUEST['reactivate'] : 0;
        $save_user = isset(Yii::app()->session['save_user']) ? Yii::app()->session['save_user'] : 0;
        $ip_address = CHttpRequest::getUserHostAddress();
        $user['token'] = $_REQUEST['token'];

        $from_data = Yii::app()->session['from_data'];
        $gateway_code = $from_data['data']['payment_method'];
        $user['gateway_code'] = $gateway_code;
        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::paymentTransaction($user);
        }
        if (isset($from_data) && !empty($from_data) && intval($save_user)) {
            $user_details = SdkUser::model()->saveSdkUser($from_data, $studio_id, 1);
            $user_id = $user_details['user_id'];
        } else {
            $user_id = Yii::app()->user->id;
        }

        if ($resArray) {
            $payer_id = $resArray['PAYERID'];
            $payer_email = $resArray['EMAIL'];
            $first_name = $resArray['SHIPTONAME'];
            $address1 = $resArray['SHIPTOSTREET'];
            $city = $resArray['SHIPTOCITY'];
            $state = $resArray['SHIPTOSTATE'];
            $country = $resArray['SHIPTOCOUNTRYCODE'];
            $zip = $resArray['SHIPTOZIP'];
            $invoice_id = $resArray['CORRELATIONID'];
            $full_name = $resArray['FIRSTNAME'] . ' ' . $resArray['LASTNAME'];
            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCESSWITHWARNING") {
                $plan_id = isset($_REQUEST['plan_id']) ? $_REQUEST['plan_id'] : 0;

                $return_parameters = json_encode($resArray);
                $query = "INSERT INTO user_payment_logs (user_id,plan_id,payment_tried,payment_tried_at,payment_done,studio_id,ip_address,payment_method,payment_token,return_parameters) VALUES";
                $query .= " ('" . $user_id . "', '" . $plan_id . "', 't', 'NOW()', 'f', " . $studio_id . ", '" . $ip_address . "', 'paypal', '" . $user['token'] . "', '" . $return_parameters . "')";
                $command = $connection->createCommand($query);
                $command->execute();

                $agent = Yii::app()->common->getAgent();
                $user['email'] = $resArray["EMAIL"]; // ' Email address of payer.  
                $user['payerid'] = $payerID = $resArray['PAYERID'];
                $qry = "SELECT *,sp.id as sub_plan_id FROM subscription_plans sp  LEFT JOIN subscription_pricing sup ON (sp.id = sup.subscription_plan_id) WHERE sp.id = '" . $plan_id . "' AND sp.studio_id = '" . $studio_id . "' AND sp.status = '1'";
                $command = $connection->createCommand($qry);
                $rowCount = $command->execute(); // execute the non-query SQL
                $plans = $command->queryAll(); // execute a query SQL
                if (count($plans)) {
                    foreach ($plans as $plan) {
                        if ($plan_id == $plan['sub_plan_id'] && $plan['currency_id'] == $from_data['data']['currency_id']) {
                            $price = $plan['price'];
                            $startup_discount = $plan['startup_discount'];
                            $currency_id = $plan['currency_id'];
                            $duration = $plan['duration'];
                            $recurrence = $plan['recurrence'];
                            $frequency = $plan['frequency'];
                            $plan_name = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;

                            $user['plan_desc'] = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;
                            $user['recurrence'] = $plan['recurrence'];
                            $user['frequency'] = $plan['frequency'];
                            $user['charged_amount'] = $price;
                            $plan_id = $plan_id;
                            $used_trial = Yii::app()->common->usedTrial();
                        }
                    }
                }

                $currency = Currency::model()->findByPk($currency_id);
                $user['currency_id'] = $currency->id;
                $user['currency_code'] = $currency->code;
                $user['currency_symbol'] = $currency->symbol;

                $user['currency'] = $currency->code;
                $user['trialperiod'] = '';
                $user['trialfrequency'] = '';
                $user['trialtotalcysles'] = '';
                $user['trial_amount'] = '';


                if ($used_trial == 0) {
                    if ($recurrence == 'Month') {
                        $user['trialperiod'] = $plan['trial_recurrence'];
                        $user['trialfrequency'] = $plan['trial_period'];
                        $user['trialtotalcysles'] = '1';
                        $user['trial_amount'] = '0.00';
                        $validity = $plan['frequency'] . ' ' . strtolower($plan['recurrence']) . "s";
                        $trial_period = $plan['trial_period'] . ' ' . strtolower($plan['trial_recurrence']) . "s";
                        $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                        $time = strtotime($start_date);
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$validity}", $time));
                    }
                    if ($recurrence == 'Year') {
                        $user['trialperiod'] = $plan['trial_recurrence'];
                        $user['trialfrequency'] = $plan['trial_period'];
                        $user['trialtotalcysles'] = '1';
                        $user['trial_amount'] = '0.00';
                        $validity = $plan['frequency'] . ' ' . strtolower($plan['recurrence']) . "s";
                        $trial_period = $plan['trial_period'] . ' ' . strtolower($plan['trial_recurrence']) . "s";
                        $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                        $time = strtotime($start_date);
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$validity}", $time));
                    }
                }
                $user['gateway_code'] = $gateway_code;
                $resArr = $payment_gateway::paymentTransactions($user);
                $ack = strtoupper($resArr["ACK"]);
                if ($ack == "SUCCESS" || $ack == "SUCESSWITHWARNING") {

                    $select_query = "SELECT * FROM user_subscriptions WHERE user_id = " . $user_id . " AND status = 1";
                    $command = $connection->createCommand($select_query);
                    $data = $command->execute();
                    if (count($data) && $data) {
                        $query = "UPDATE user_subscriptions SET profile_id = '" . $resArr['PROFILEID'] . "', is_success  = 1";
                        $query .= " WHERE user_id = " . $user_id . " AND status = 1";
                        $command = $connection->createCommand($query);
                        $command->execute();
                    } else {
                        $created_date = Date('Y-m-d H:i:s');
                        $query = "INSERT INTO user_subscriptions (user_id,studio_id,plan_id,studio_payment_gateway_id,amount,profile_id,ip,created_by,start_date,end_date,created_date,is_success) VALUES";
                        $query .= " ('" . $user_id . "','" . $studio_id . "', '" . $plan_id . "','" . $this->PAYMENT_GATEWAY_ID[$gateway_code] . "', '" . $price . "','" . $resArr['PROFILEID'] . "','" . Yii::app()->request->getUserHostAddress() . "','" . $user_id . "','" . $start_date . "','" . $end_date . "','" . $created_date . "','1')";
                        $command = $connection->createCommand($query);
                        $command->execute();
                    }

                    /* $transaction = new Transaction();
                      $transaction->user_id = $user_id;
                      $transaction->studio_id = $studio_id;
                      $transaction->plan_id = $plan_id;
                      $transaction->payment_method = 'paypal';
                      $transaction->transaction_status = 'Active';
                      $transaction->invoice_id = $invoice_id;
                      $transaction->order_number = $resArray['PROFILEID'];
                      $transaction->amount =  $price;
                      $transaction->fullname = $full_name;
                      $transaction->address1 = $address1;
                      $transaction->city = $city;
                      $transaction->state = $state;
                      $transaction->country = $country;
                      $transaction->zip = $zip;
                      $transaction->payer_id = $payerID;
                      $transaction->ip = $ip_address;
                      $transaction->save(); */

                    if ($reactivate) {
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_reactivate', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_user_reactivate', $user_id, '', 0);
                        }
                    } else {
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'welcome_email_with_subscription', '', '', '', '', '', '', $this->language_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email_with_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                        }
                    }

                    if (isset($from_data) && !empty($from_data)) {
                        $email = $from_data['data']['email'];
                        $password = $from_data['data']['password'];
                        $identity = new UserIdentity($email, $password);
                        $identity->validate();
                        Yii::app()->user->login($identity);
                        unset(Yii::app()->session['back_btn']);
                    }

                    Yii::app()->user->setFlash("success", $this->ServerMessage['subscribed_success']);
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                } else {
                    Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                }
            }
        }
    }

    /*
     * By Sanjeev
     * When comes from Paypal
     */

    public function actionPpvConfirm() {
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $session_data = Yii::app()->session['session_data'];
        $gateway_code = $session_data["gateway_code"];
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $trans_data = $payment_gateway::processCheckoutDetails($_REQUEST['token']);

        $userdata['TOKEN'] = $trans_data['TOKEN'];
        $userdata['PAYERID'] = $trans_data['PAYERID'];
        $userdata['AMT'] = $trans_data['AMT'];
        $userdata['CURRENCYCODE'] = $trans_data['CURRENCYCODE'];
        $resArray = $payment_gateway::processDoPayment($userdata);
        $plan_id = $_REQUEST['plan_id'];

        $ip_address = CHttpRequest::getUserHostAddress();

        $planModel = new PpvPlans();
        $plan = $planModel->findByPk($plan_id);
        $start_date = Date('Y-m-d H:i:s');

        $end_date = '';
        $is_bundle = $session_data['is_bundle'];
        $timeframe_id = $session_data['timeframe_id'];

        if (intval($is_bundle) && intval($timeframe_id)) {
            (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $timeframe_id, $studio_id);
            $price = $price->attributes;
            $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
            if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
                $time = strtotime($start_date);
                $expiry = $timeframe_days . ' ' . "days";
                $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
            }
        } else if (intval($isadv) == 0) {
            $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
            $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

            $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
            $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

            $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
            $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

            $time = strtotime($start_date);
            if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
            }

            $data['view_restriction'] = $limit_video;
            $data['watch_period'] = $watch_period;
            $data['access_period'] = $access_period;
        }


        $plan = PpvPlans::model()->findByPk($plan_id);
        $data = $session_data;
        $isadv = $data['isadv'];
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['uniq_id']));
        $video_id = $Films->id;
        $VideoName = Yii::app()->common->getVideoname($video_id);
        $VideoName = trim($VideoName);
        $VideoDetails = $VideoName;
        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
        if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
            if ($data['season_id'] == 0) {
                $VideoDetails .= ', All Seasons';
            } else {
                $VideoDetails .= ', Season ' . $data['season_id'];
                if ($data['episode_id'] == 0) {
                    $VideoDetails .= ', All Episodes';
                } else {
                    $episodeName = Yii::app()->common->getEpisodename($streams->id);
                    $episodeName = trim($episodeName);
                    $VideoDetails .= ', ' . $episodeName;
                }
            }
        }
        $data['amount'] = $trans_data['AMT'];
        $ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['couponCode'], $isadv, $gateway_code);
        $trans_data['transaction_status'] = $resArray['PAYMENTSTATUS'];
        $trans_data['invoice_id'] = $resArray['CORRELATIONID'];
        $trans_data['order_number'] = $resArray['TRANSACTIONID'];
        $trans_data['amount'] = $trans_data['AMT'];
        $trans_data['dollar_amount'] = $trans_data['AMT'];
        $trans_data['response_text'] = json_encode($trans_data);
        $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
        $trans_data['amount'] = $resArray['AMT'];
        $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
        self::setRedirectPpv($Films, $plan, $data, $isadv);
    }

    public function actionvalidateCoupon() {
        $res = array();
        $res['isError'] = 0;
        $studio_id = Yii::app()->common->getStudiosId();
        $_SESSION['couponCode'] = '';

        $movie_unq_id = $_REQUEST["movie_unq_id"];
        $season_id = $_REQUEST["season_id"];
        $episode_id = $_REQUEST["episode_id"];
        $film = Film::model()->findByAttributes(array('uniq_id' => $movie_unq_id));
        $freearg['studio_id'] = $studio_id;
        $freearg['movie_id'] = $film->id;
        $freearg['season_id'] = $season_id;
        $freearg['episode_id'] = $episode_id;
        $freearg['content_types_id'] = $film->content_types_id;
        $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
        $isFreeContent = 0;
        if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 8)) {
            $isFreeContent = Yii::app()->common->isFreeContent($freearg);
        }
        if ($isFreeContent == 0) {
            if (isset($_REQUEST["couponCode"]) && trim($_REQUEST["couponCode"])) {
                $couponeDetails = Yii::app()->common->couponCodeIsValid($_REQUEST["couponCode"], Yii::app()->user->id, '', $_REQUEST["currency_id"]);
                if ($couponeDetails != 0) {
                    if ($couponeDetails == 1) {
                        $res['isError'] = 2;
                    } else {
                        if ($couponeDetails["discount_type"] == 1) {
                            $res['discount_amount'] = $couponeDetails['discount_amount'];
                            $res['is_cash'] = 0;
                        } else {
                            if ($_REQUEST["currency_id"] != 0) {
                                $currencyDetails = Currency::model()->findByPk($_REQUEST["currency_id"]);
                                $res['discount_amount'] = $couponeDetails['discount_amount'];
                                $res['is_cash'] = 1;
                            } else {
                                $res['discount_amount'] = $couponeDetails['discount_amount'];
                                $res['is_cash'] = 1;
                            }
                        }
                        if (isset($_REQUEST['physical']) && $_REQUEST['physical']) {
                            $_SESSION['couponCode'] = $_REQUEST["couponCode"];
                        }
                    }
                } else {
                    $res['isError'] = 1;
                }
            } else {
                $res['isError'] = 1;
            }
        } else {
            $res['isError'] = 3;
        }
        echo json_encode($res);
        exit;
    }

    public function actionvalidateVoucher() {
        $res = array();
        $res['isError'] = 1;

        if (isset($_POST["voucherCode"]) && trim($_POST["voucherCode"]) && isset($_POST["contentId"])) {
			$user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
            $voucherDetails = Yii::app()->common->voucherCodeValid($_POST["voucherCode"], $user_id, $_POST["contentId"], '');
            if (!empty($voucherDetails) && $voucherDetails != 1) {
                $res['isError'] = $voucherDetails;
            } else {
                $res = $voucherDetails;
            }
        }

        echo json_encode($res);
        exit;
    }

    function ppvAmount($plan, $data) {
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $amount = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);

        //Getting pricing according to currency
        $price = Yii::app()->common->getPPVPrices($plan->id, $this->studio->default_currency_id);

        //Set different prices according to schemes
        if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
            //Multi part videos
            //Check which part wants to sell by studio admin
            $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
            $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
            $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
            $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

            if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                if (intval($is_episode)) {

                    if (intval($is_subscribed_user)) {
                        $amount = $price['episode_subscribed'];
                    } else {
                        $amount = $price['episode_unsubscribed'];
                    }
                }
            } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                if (intval($is_season)) {
                    if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                        $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                        $season_price_data['ppv_pricing_id'] = $price['id'];
                        $season_price_data['studio_id'] = $studio_id;
                        $season_price_data['currency_id'] = $price['currency_id'];
                        $season_price_data['season'] = $data['season_id'];
                        $season_price_data['is_subscribed'] = $is_subscribed_user;

                        $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                        if ($season_price != -1) {
                            $amount = $season_price;
                        } else {
                            if (intval($is_subscribed_user)) {
                                $amount = $price['season_subscribed'];
                            } else {
                                $amount = $price['season_unsubscribed'];
                            }
                        }
                    } else {
                        if (intval($is_subscribed_user)) {
                            $amount = $price['season_subscribed'];
                        } else {
                            $amount = $price['season_unsubscribed'];
                        }
                    }
                }
            } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                if (intval($is_show)) {
                    if (intval($is_subscribed_user)) {
                        $amount = $price['show_subscribed'];
                    } else {
                        $amount = $price['show_unsubscribed'];
                    }
                }
            }
        } else {//Single part videos
            if (intval($is_subscribed_user)) {
                $amount = $price['price_for_subscribed'];
            } else {
                $amount = $price['price_for_unsubscribed'];
            }
        }

        return $amount;
    }

    function setRedirectPpv($Films, $plan, $data, $isadv = 0, $isinstafeez = 0) {
        if (!intval($isadv)) {
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {

                if (intval($data['season_id']) && $data['episode_id'] == 0) {
                    $time = time() . uniqid();
                    $_SESSION['showconfirmuniqueid'] = $time;
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/' . $data['permalink'] . '?unique_id=' . $time . '&season_id=' . $data['season_id']);
                    exit;
                } elseif ($data['season_id'] == 0 && $data['episode_id'] == 0) {
                    $timeunique = time() . uniqid() . uniqid();
                    $_SESSION['showconfirmuniqueid'] = $timeunique;
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/' . $data['permalink'] . '?unique_id=' . $timeunique);
                    exit;
                }
            }
        }
        if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
            if (trim($data['episode_id']) && trim($data['episode_id']) != '0') {
                $str = '&stream=' . $data['episode_uniq_id'];
                $playerParam = '/stream/' . $data['episode_uniq_id'];
            }

            if (intval($data['season_id']) && $data['episode_id'] == 0) {
                $str = '&season=' . $data['season_id'];
                $playerParam .='/season/' . $data['season_id'];
            }
        }
        if (intval($isadv)) {
            $movie_name = $Films->name;
            $msgs = $this->ServerMessage['purchased'];
            $msgs = htmlentities($msgs);
            eval("\$msgs = \"$msgs\";");
            $msgs = htmlspecialchars_decode($msgs);
            Yii::app()->user->setFlash("success", $msgs);
            if ($isinstafeez) {
                return $_SERVER['HTTP_REFERER'];
            } else {
                if (isset($data['permalink'])) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/' . $data['permalink']);
                    exit;
                } elseif (isset($_SERVER['HTTP_REFERER']) && trim($_SERVER['HTTP_REFERER'])) {
                    $this->redirect($_SERVER['HTTP_REFERER']);
                    exit;
                } else {
                    $this->redirect(Yii::app()->session['backbtnlink']);
                    exit;
                }
            }
        } else {
            if ($isinstafeez) {
                return Yii::app()->getbaseUrl(true) . '/player/' . $data['permalink'] . $playerParam;
                exit;
            } else {
                $this->redirect(Yii::app()->getbaseUrl(true) . '/player/' . $data['permalink'] . $playerParam);
            }
        }
    }

    public function actionUpload_image() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        $theme = $this->studio->parent_theme;
        $avatarFile = $fileinfo = $_FILES['Filedata'];
        $fileinfo['name'] = strtolower($fileinfo['name']);
        $format = new Format();
        if (!$_REQUEST['avatar_src']) {
            $checkImg = $format->checkValidImages($fileinfo, '', 150, 150);
        } else {
            $_FILES = json_decode($_REQUEST['fileinfo'], TRUE);
            $_FILES['Filedata']['error'] = 4;
            $fileinfo = $_FILES['Filedata'];
            $_REQUEST['avatar_src'] = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['avatar_src'];
        }
        if (@$checkImg[0] == 'valid' || $_REQUEST['avatar_src']) {
            if ($_REQUEST['avatar_data']) {
                //Checking for existing object
                $old_poster = new Poster;
                $old_posters = $old_poster->findAllByAttributes(array('object_id' => $user_id, 'object_type' => 'profilepicture'));
                if (count($old_posters) > 0) {
                    foreach ($old_posters as $oldposter) {
                        $oldposter->delete();
                    }
                }
                $ip_address = CHttpRequest::getUserHostAddress();
                $poster = new Poster();
                $poster->object_id = $user_id;
                $poster->object_type = 'profilepicture';
                $poster->poster_file_name = $avatarFile['name'];
                $poster->ip = $ip_address;
                $poster->created_by = $user_id;
                $poster->created_date = new CDbExpression("NOW()");
                $poster->save();
                $poster_id = $poster->id;

                $crop = new cropProfileAvatar($_REQUEST['avatar_src'], $_REQUEST['avatar_data'], $avatarFile, $poster_id);
                if ($_REQUEST['avatar_src']) {
                    $tpath = substr($_REQUEST['avatar_src'], 0, strrpos($_REQUEST['avatar_src'], '/'));
                    $format->recursiveRemoveDirectory($tpath);
                }

                //$uid = $_REQUEST['movie_id'];
                require_once "Image.class.php";
                require_once "Config.class.php";
                require_once "ProfileUploader.class.php";
                spl_autoload_unregister(array('YiiBase', 'autoload'));
                require_once "amazon_sdk/sdk.class.php";
                spl_autoload_register(array('YiiBase', 'autoload'));

                define("BASEPATH", dirname(__FILE__) . "/..");
                $config = Config::getInstance();
                $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/profile_images'); //path for images uploaded
                $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                $config->setBucketName($bucketInfo['bucket_name']);
                $s3_config = Yii::app()->common->getS3Details($studio_id);
                $config->setS3Key($s3_config['key']);
                $config->setS3Secret($s3_config['secret']);
                $config->setAmount(250);  //maximum paralell uploads
                $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
                if ($theme == 'traditional-byod') {
                    $config->setDimensions(array('small' => "250x250", 'thumb' => "200x200"));   //resize to these sizes
                } else if ($theme == 'audio-streaming') {
                    $config->setDimensions(array('small' => "400x400", 'thumb' => "400x400"));   //resize to these sizes
                } else {
                    $config->setDimensions(array('small' => "150x150", 'thumb' => "100x100"));   //resize to these sizes
                }
                //usage of uploader class - this simple :)
                $uploader = new ProfileUploader($poster_id);
                $folderPath = Yii::app()->common->getFolderPath("", Yii::app()->common->getStudioId());
                $unsignedBucketPath = $folderPath['unsignedFolderPath'];
                $ret = $uploader->uploadprofileThumb($unsignedBucketPath . "public/system/profile_images/");
                $poster = array($ret);

                $response = array(
                    'state' => 200,
                    'message' => $crop->getMsg(),
                    'result' => $ret['original']
                );
                if ($_SERVER['HTTP_X_PJAX'] == true) {
                    $user_picture = $this->getProfilePicture($user_id, 'profilepicture', 'thumb');
                    $response['profile_image'] = $user_picture;
                }
                Yii::app()->session['profile_image'] = "/system/profile_images/" . $uid . "/small/" . $avatarFile['name'];
                if ($_SERVER['HTTP_X_PJAX'] == false) {
                    Yii::app()->user->setFlash('success', $this->ServerMessage['profile_picture_updated']);
                }
                echo json_encode($response);
                exit;
            }
        }
    }

    public function actiongetNewSignedUrlForTrailer() {
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudiosId();
        $cloudFront = Yii::app()->common->connectToAwsCloudFront($studio_id);
        if (Yii::app()->common->isMobile()) {
            $expires = time() + 50;
        } else if ($_REQUEST['is_ajax'] == 1) {
            $expires = time() + 50;
        } else {
            $expires = time() + 50;
        }

        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $_REQUEST['video_url'],
            'expires' => $expires,
        ));
        echo $signedUrlCannedPolicy;
        exit;
    }

    public function actiongetNewSignedUrlForInternetSpeedEmb() {
        $this->layout = 'false';
        $_SESSION['internetSpeed'] = $_REQUEST['user_internet_speed'];
        $res = $this->getVideoToBePlayed($_REQUEST['multiple_video_url'], "", $_REQUEST['user_internet_speed']);
        $res = explode(',', $res);
        echo $res[1];
        exit;
    }

    public function actiongetNewSignedUrlForEmbeded() {

        if ($_REQUEST['wiki_data'] == "") {

            $studio_id = 0;
            if (isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] != 0) {
                $studio_id = $_REQUEST['studio_id'];
            } else {
                $studio_id = Yii::app()->common->getStudiosId();
            }
            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $_REQUEST['video_url']));
            $isAjax = 1;
            if (isset($_REQUEST['isAjax']) && $_REQUEST['isAjax'] == 2) {
                $isAjax = 2;
            }
            echo $video_url_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, $isAjax, "", 0, $studio_id);
            exit;
        } else {
            $rel_path = str_replace(" ", "%20", str_replace("https://muviassetsdev.s3.amazonaws.com/", "", $_REQUEST['video_url']));
            echo $video_url_path = $this->getsecure_url($rel_path);
            exit;
        }
    }

    /**
     * @method public getInvoidPDF() It will return users transaction invoice as PDF
     * @author GAYADHAR<support@muvi.com>
     * @return PDF A pdf file with invoice details
     */
    function actionGetInvoicePDF() {
        if (Yii::app()->user->id) {
            $subscription_bundles_plan_id = '';
            if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id']) {
                $subscription_bundles_plan_id = $_REQUEST['subscriptionbundles_id'];
                $studio_id = Yii::app()->common->getStudiosId();
                $name = Yii::app()->pdf->downloadUserinvoice($studio_id, $_REQUEST['transaction_id'], Yii::app()->user->id, $subscription_bundles_plan_id);
                echo $name;
                exit;
            } else {
                $this->redirect($this->createUrl('user/login'));
                exit;
            }
        } else {
            $this->redirect($this->createUrl('user/login'));
            exit;
        }
    }

    //voucher invoice pdf
    function actionGetVoucherInvoicePDF() {
        if (Yii::app()->user->id) {
            if (isset($_REQUEST['vouchersubscriptio_id']) && $_REQUEST['vouchersubscriptio_id']) {
                $studio_id = Yii::app()->common->getStudiosId();
                $name = Yii::app()->pdf->downloadVoucherinvoice($studio_id, $_REQUEST['vouchersubscriptio_id'], Yii::app()->user->id);
                echo $name;
                exit;
            } else {
                $this->redirect($this->createUrl('user/login'));
                exit;
            }
        } else {
            $this->redirect($this->createUrl('user/login'));
            exit;
        }
    }

    function actionViewInvoicePDF() {
        if (Yii::app()->user->id) {
            if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id']) {
                $studio_id = Yii::app()->common->getStudiosId();
                $name = Yii::app()->pdf->physicalUserinvoice($studio_id, $_REQUEST['transaction_id'], Yii::app()->user->id);
                echo $name;
                exit;
            } else {
                $this->redirect($this->createUrl('user/login'));
                exit;
            }
        } else {
            $this->redirect($this->createUrl('user/login'));
            exit;
        }
    }

    public function actionConversionPixel() {
        $this->render('conversionPixel');
    }

    public function actionCardInformation() {
        $user_id = Yii::app()->user->id;
        if ($user_id > 0) {
            $studio_id = Yii::app()->common->getStudiosId();
            $user = SdkUser::model()->findByPk($user_id);
            $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));

            // if (isset($usersub) && !empty($usersub)) {
            $gateways = array();

            $payment_gateway = StudioPaymentGateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->find(
                    array(
                        'condition' => ' t.studio_id=' . $studio_id . ' AND t.status = 1'
                    )
            );
            if (isset($payment_gateway) && !empty($payment_gateway)) {
                $gateways = $payment_gateway;

                $cards = SdkCardInfos::model()->findAllByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id), array('order' => 'is_cancelled ASC'));
            }
            //}

            $date = date('F d, Y', strtotime('+5 days', strtotime($usersub->partial_failed_date)));
            $last_charge_failed = $this->Language['last_charge_failed'];
            $last_charge_failed = htmlentities($last_charge_failed);
            eval("\$last_charge_failed = \"$last_charge_failed\";");
            $last_charge_failed = htmlspecialchars_decode($last_charge_failed);
            if (empty($cards)) {
                $redirect_url = Yii::app()->getBaseUrl(true);
                Yii::app()->user->setFlash('error', $this->ServerMessage['no_cards_added_yet']);
                $this->redirect($redirect_url);
            }
            $this->render('cardinformation', array('user' => $user, 'cards' => $cards, 'usersub' => $usersub, 'gateways' => $gateways, 'last_charge_failed' => $last_charge_failed));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

    public function actionSaveCard() {
        $card_data = $_REQUEST['data'];
        $gateway_code = $card_data['payment_gateway'];
        $card_data['cvv'] = @$card_data['security_code'];
        $card_data['gateway_code'] = $gateway_code;
        if(isset($this->API_ADDONS_REDIRECT[$gateway_code]['redirect']) && $this->API_ADDONS_REDIRECT[$gateway_code]['redirect']){
            $usersub = array();
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::saveCard($usersub, $card_data);
            $this->redirect($resArray['redirect_url']);
            exit;
        }else{
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $user_id = Yii::app()->user->id;
                $studio_id = Yii::app()->common->getStudiosId();

                $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));

                $currency = Currency::model()->findByPk($usersub->currency_id);
                $card_data['currency_id'] = $currency->id;
                $card_data['currency_code'] = $currency->code;
                $card_data['currency_symbol'] = $currency->symbol;
                $card_data['email'] = Yii::app()->user->email;

                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $resArray = $payment_gateway::saveCard($usersub, $card_data);
                
                $data = $resArray['card'];
                if (isset($resArray['isSuccess']) && $resArray['isSuccess']) {
                    $cards = SdkCardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'is_cancelled' => 0));
                    $is_primary = 0;
                    if (isset($cards) && !empty($cards)) {
                        $is_primary = 1;
                    }
                    $ip_address = Yii::app()->request->getUserHostAddress();

                    $sciModel = New SdkCardInfos;
                    $sciModel->studio_id = $studio_id;
                    $sciModel->user_id = $user_id;
                    $sciModel->gateway_id = $this->GATEWAY_ID[$gateway_code];
                    $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                    $sciModel->card_holder_name = $data['card_name'];
                    $sciModel->exp_month = $data['exp_month'];
                    $sciModel->exp_year = $data['exp_year'];
                    $sciModel->card_last_fourdigit = $data['card_last_fourdigit'];
                    $sciModel->auth_num = @$data['auth_num'];
                    $sciModel->token = @$data['token'];
                    $sciModel->profile_id = $usersub->profile_id;
                    $sciModel->card_type = $data['card_type'];
                    $sciModel->reference_no = @$data['reference_no'];
                    $sciModel->response_text = $data['response_text'];
                    $sciModel->status = $data['status'];
                    $sciModel->is_cancelled = $is_primary;
                    $sciModel->ip = $ip_address;
                    $sciModel->created = new CDbExpression("NOW()");
                    $sciModel->save();
                    $sdk_card_info_id = $sciModel->id;

                    //Check if partial or failed payment record exists for user only
                    $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
                    if (isset($usersub->status) && ($usersub->status == 1) && isset($usersub->payment_status) && ($usersub->payment_status != 0)) {
                        if (abs($usersub->partial_failed_due) >= 0.01) {
                            //Prepare an array to charge from given card
                            $arg = array();
                            $currency = Currency::model()->findByPk($usersub->currency_id);
                            $arg['currency_id'] = $currency->id;
                            $arg['currency_code'] = $currency->code;
                            $arg['currency_symbol'] = $currency->symbol;

                            $arg['amount'] = $usersub->partial_failed_due;
                            $arg['token'] = $sciModel->token;
                            $arg['profile_id'] = $usersub->profile_id;
                            $arg['card_holder_name'] = $sciModel->card_holder_name;
                            $arg['card_last_fourdigit'] = $sciModel->card_last_fourdigit;
                            $arg['card_type'] = $sciModel->card_type;
                            $arg['exp_month'] = $sciModel->exp_month;
                            $arg['exp_year'] = $sciModel->exp_year;
                            $arg['email'] = Yii::app()->user->email;
                            $arg['user_id'] = Yii::app()->user->id;
                            $res = $this->chargeDueOnPartialOrFailedTransaction($usersub, $arg, $gateway_code);
                        }
                    }
                    if ($sdk_card_info_id > 0) {
                        Yii::app()->user->setFlash('success', $this->ServerMessage["credit_card_saved_success"]);
                        $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
                    } else {
                        Yii::app()->user->setFlash('error', $this->Language['unable_to_process_try_another_card']);
                        $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
                    }
                } else {
                    Yii::app()->user->setFlash('error', $this->Language['unable_to_process_try_another_card']);
                    $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
                }
            }
        }
    }

    function actionMakePrimaryCard() {
        $gateway_code = $_REQUEST['gateway_code'];
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();

            $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
            $card = SdkCardInfos::model()->findByAttributes(array('id' => $_REQUEST['id_payment'], 'studio_id' => $studio_id, 'user_id' => $user_id));
            if (isset($usersub) && !empty($usersub) && isset($usersub) && !empty($usersub)) {
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $data = $payment_gateway::defaultCard($usersub, $card, $gateway_code);

                $res = json_decode($data, true);

                if (intval($res['isSuccess'])) {
                    //Update card to inactive mode
                    $sql = "UPDATE sdk_card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                    $con = Yii::app()->db;
                    $ciData = $con->createCommand($sql)->execute();

                    $card->is_cancelled = 0;
                    $card->save();
                    $usersub->card_id = $card->id;
                    $usersub->save();
                    //Check if partial or failed payment record exists for user only
                    if (isset($usersub->status) && ($usersub->status == 1) && isset($usersub->payment_status) && ($usersub->payment_status != 0)) {
                        if (abs($usersub->partial_failed_due) >= 0.01) {
                            //Prepare an array to charge from given card
                            $arg = array();

                            $currency = Currency::model()->findByPk($usersub->currency_id);
                            $arg['currency_id'] = $currency->id;
                            $arg['currency_code'] = $currency->code;
                            $arg['currency_symbol'] = $currency->symbol;

                            $arg['amount'] = $usersub->partial_failed_due;
                            $arg['token'] = $card->token;
                            $arg['profile_id'] = $usersub->profile_id;
                            $arg['card_holder_name'] = $card->card_holder_name;
                            $arg['card_last_fourdigit'] = $card->card_last_fourdigit;
                            $arg['card_type'] = $card->card_type;
                            $arg['exp_month'] = $card->exp_month;
                            $arg['exp_year'] = $card->exp_year;
                            $arg['email'] = Yii::app()->user->email;
                            $arg['user_id'] = Yii::app()->user->id;
                            $res = $this->chargeDueOnPartialOrFailedTransaction($usersub, $arg, $gateway_code);
                        }
                    }

                    Yii::app()->user->setFlash('success', $this->ServerMessage['credit_card_set_primary']);
                } else {
                    Yii::app()->user->setFlash('error', $this->ServerMessage['credit_card_not_set_primary']);
                }
            } else {
                Yii::app()->user->setFlash('error', $this->ServerMessage['credit_card_not_set_primary']);
            }
        } else {
            Yii::app()->user->setFlash('error', $this->ServerMessage['credit_card_not_set_primary']);
        }
        $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
    }

    function actionPayFromPrimaryCard() {
        $gateway_code = $_REQUEST['gateway_code'];
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();

        $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));

        if (isset($usersub->payment_status) && intval($usersub->payment_status) != 0) {
            $card = SdkCardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'is_cancelled' => 0), array('order' => 'is_cancelled ASC'));
            if (!empty($card)) {
                //Prepare an array to charge from given card
                $arg = array();

                $currency = Currency::model()->findByPk($usersub->currency_id);
                $arg['currency_id'] = $currency->id;
                $arg['currency_code'] = $currency->code;
                $arg['currency_symbol'] = $currency->symbol;

                $arg['amount'] = $usersub->partial_failed_due;
                $arg['token'] = $card->token;
                $arg['profile_id'] = $usersub->profile_id;
                $arg['card_holder_name'] = $card->card_holder_name;
                $arg['card_last_fourdigit'] = $card->card_last_fourdigit;
                $arg['card_type'] = $card->card_type;
                $arg['exp_month'] = $card->exp_month;
                $arg['exp_year'] = $card->exp_year;
                $arg['email'] = Yii::app()->user->email;
                $arg['user_id'] = Yii::app()->user->id;
                $res = $this->chargeDueOnPartialOrFailedTransaction($usersub, $arg, $gateway_code);

                if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                    $url = $this->createUrl('user/transactions');
                    Yii::app()->user->setFlash('success', $this->ServerMessage['payement_process_success']);
                } else {
                    $url = $this->createUrl('user/cardInformation');
                    Yii::app()->user->setFlash('error', $this->ServerMessage['payement_process_failed']);
                }
            } else {
                $url = $this->createUrl('user/cardInformation');
                Yii::app()->user->setFlash('error', $this->ServerMessage['no_primarycard_found']);
            }
        } else {
            $url = $this->createUrl('user/cardInformation');
            Yii::app()->user->setFlash('error', $this->ServerMessage['no_past_due']);
        }

        $this->redirect($url);
    }

    function chargeDueOnPartialOrFailedTransaction($usersub = Null, $data = array(), $gateway_code) {
        $res = array();
        $res['isSuccess'] = 0;
        $studio_id = $usersub->studio_id;
        $user_id = $usersub->user_id;
        $plan_id = $usersub->plan_id;
        $currency_id = $usersub->currency_id;
        $user_subscription_id = $usersub->id;
        $bill_amount = $data['amount'];
        $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $trans_data = $payment_gateway::processTransactions($data);

        if (intval($trans_data['is_success'])) {
            //Getting Ip address
            $ip_address = Yii::app()->request->getUserHostAddress();

            //Save a transaction detail
            $transaction = new Transaction;
            $transaction->user_id = $user_id;
            $transaction->studio_id = $studio_id;
            $transaction->plan_id = $plan_id;
            $transaction->currency_id = $currency_id;
            $transaction->transaction_date = new CDbExpression("NOW()");
            $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_code];
            $transaction->transaction_status = $trans_data['transaction_status'];
            $transaction->invoice_id = $trans_data['invoice_id'];
            $transaction->order_number = $trans_data['order_number'];
            $transaction->dollar_amount = $trans_data['dollar_amount'];
            $transaction->amount = $trans_data['paid_amount'];
            $transaction->response_text = $trans_data['response_text'];
            $transaction->subscription_id = $user_subscription_id;
            $transaction->ip = $ip_address;
            $transaction->created_date = new CDbExpression("NOW()");
            $transaction->save();

            $paid_amount = $trans_data['paid_amount'];
            //Check billing amount and paid amount is same or not, otherwise charge again.
            if (abs(($bill_amount - $paid_amount) / $paid_amount) < 0.00001) {
                $billing_date = gmdate('Y-m-d', strtotime($usersub->start_date));
                if ((isset($usersub->payment_status) && $usersub->payment_status == 2) || (strtotime(gmdate('Y-m-d')) > strtotime($billing_date))) {
                    $plan = SubscriptionPlans::model()->getPlanDetails($plan_id, $studio_id);

                    $recurrence_frequency = $plan['frequency'] . ' ' . strtolower($plan['recurrence']) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime($usersub->start_date . "+{$recurrence_frequency}"));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));

                    $usersub->start_date = $start_date;
                    $usersub->end_date = $end_date;
                }

                $is_paid = 1;

                $usersub->payment_status = 0;
                $usersub->partial_failed_date = '';
                $usersub->partial_failed_due = 0;
            } else {
                $is_paid = 0;
                $usersub->partial_failed_due = $paid_amount;
            }
            $usersub->save();

            if (intval($is_paid)) {
                $res['isSuccess'] = 1;
                $studio = Studios::model()->findByPk($studio_id);

                $user = array();
                $user['amount'] = Yii::app()->common->formatPrice($data['amount'], $currency_id, 1);
                $user['card_holder_name'] = $data['card_holder_name'];
                $user['card_last_fourdigit'] = $data['card_last_fourdigit'];

                //Get invoice detail
                $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $trans_data['invoice_id']);
                //Send an email with invoice detail
                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name, '', '', '', '', '', $this->language_code);
                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_renew', $studio_id);
                if ($isEmailToStudio) {
                    $trans_amt = Yii::app()->common->formatPrice($trans_data['paid_amount'], $currency_id, 1);
                    $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_card_updated_and_charged', $user_id, '', $trans_amt);
                }
            } else {
                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed', '', '', '', '', '', '', $this->language_code);
                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                if ($isEmailToStudio) {
                    $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                }
            }
        } else {
            $teModel = New TransactionErrors;
            $teModel->studio_id = $studio_id;
            $teModel->user_id = $user_id;
            $teModel->plan_id = $plan_id;
            $teModel->subscription_id = $user_subscription_id;
            $teModel->payment_method = $this->PAYMENT_GATEWAY[$gateway_code];
            $teModel->response_text = $trans_data['response_text'];
            $teModel->created_date = new CDbExpression("NOW()");
            $teModel->save();

            $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed', '', '', '', '', '', '', $this->language_code);
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
            if ($isEmailToStudio) {
                $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
            }
        }

        return $res;
    }

    function actionDeleteCard() {
        $gateway_code = $_REQUEST['gateway_code'];
        if (isset($_REQUEST['id_payment']) && !empty($_REQUEST['id_payment'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
            $card = SdkCardInfos::model()->findByAttributes(array('id' => $_REQUEST['id_payment'], 'studio_id' => $studio_id, 'user_id' => $user_id));
            if (isset($usersub) && !empty($usersub) && isset($usersub) && !empty($usersub)) {
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $data = $payment_gateway::deleteCard($usersub, $card, $gateway_code);
                $res = json_decode($data, true);

                if (intval($res['isSuccess'])) {
                    SdkCardInfos::model()->deleteByPk($_REQUEST['id_payment'], 'user_id=:user_id AND studio_id=:studio_id', array(':user_id' => $user_id, ':studio_id' => $studio_id));

                    Yii::app()->user->setFlash('success', $this->ServerMessage['credit_card_delete_success']);
                } else {
                    Yii::app()->user->setFlash('error', $this->ServerMessage['credit_card_not_delete']);
                }
            } else {
                Yii::app()->user->setFlash('error', $this->ServerMessage['credit_card_not_delete']);
            }
        } else {
            Yii::app()->user->setFlash('error', $this->ServerMessage['credit_card_not_delete']);
        }

        $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
    }

    /*
     * @author Sanjeev
     * For paypal
     */

    public function actionUpdateCard() {
        $res = array();
        $gateway_code = $_REQUEST['gateway_code'];
        $studio = $this->studio;
        $user_id = Yii::app()->user->id;
        $studio_id = $studio->id;
        $currency = Currency::model()->findByPk($this->studio->default_currency_id);
        $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
        if (isset($_REQUEST) && !empty($_REQUEST)) {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::updateCard($_REQUEST, $usersub);
            if ($data['ACK'] == 'Success' || $data['ACK'] == 'SUCCESS') {
                $card_last_fourdigit = str_replace(substr($_REQUEST['card_number'], 0, strlen($_REQUEST['card_number']) - 4), str_repeat("#", strlen($_REQUEST['card_number']) - 4), $_REQUEST['card_number']);
                $card = SdkCardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
                $card->card_holder_name = $_REQUEST['card_name'];
                $card->card_last_fourdigit = $card_last_fourdigit;
                $card->exp_month = $_REQUEST['exp_month'];
                $card->exp_year = $_REQUEST['exp_year'];
                $card->card_type = $data['card']['card_type'];
                $card->save();
                if (isset($usersub->payment_status) && $usersub->payment_status != 0) {
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $profile_data = $payment_gateway::profileDetails($usersub->profile_id);
                    $amount = $profile_data['OUTSTANDINGBALANCE'];
                    if ($amount > 0.00001) {
                        $arg['amount'] = $amount;
                        $arg['currency_id'] = $currency->id;
                        $arg['currency_code'] = $currency->code;
                        $arg['currency_symbol'] = $currency->symbol;
                        $arg['exp_month'] = $_REQUEST['exp_month'];
                        $arg['exp_year'] = $_REQUEST['exp_year'];
                        $arg['card_number'] = $_REQUEST['card_number'];
                        $arg['cvv'] = $_REQUEST['cvv'];
                        $arg['email'] = Yii::app()->user->email;
                        $arg['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                        $payment_gateway = new $payment_gateway_controller();
                        $transaction_res = $payment_gateway::processTransactions($arg);
                        if (isset($transaction_res['is_success']) && $transaction_res['is_success']) {
                            $ip_address = Yii::app()->request->getUserHostAddress();
                            $transaction = new Transaction;
                            $transaction->user_id = $user_id;
                            $transaction->studio_id = $studio_id;
                            $transaction->plan_id = $usersub->plan_id;
                            $transaction->currency_id = $currency->id;
                            $transaction->transaction_date = new CDbExpression("NOW()");
                            $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_code];
                            $transaction->transaction_status = $transaction_res['transaction_status'];
                            $transaction->invoice_id = $transaction_res['invoice_id'];
                            $transaction->order_number = $usersub->profile_id;
                            $transaction->dollar_amount = $amount;
                            $transaction->amount = $amount;
                            $transaction->response_text = json_encode($transaction_res);
                            $transaction->subscription_id = $usersub->id;
                            $transaction->ip = $ip_address;
                            $transaction->created_by = $user_id;
                            $transaction->created_date = new CDbExpression("NOW()");
                            $transaction->save();

                            $plan = SubscriptionPlans::model()->getPlanDetails($usersub->plan_id, $studio_id);
                            $recurrence_frequency = $plan['frequency'] . ' ' . strtolower($plan['recurrence']) . "s";
                            $start_date = date("Y-m-d H:i:s", strtotime($usersub->start_date . "+{$recurrence_frequency}"));
                            $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                            $usersub->is_success = 1;
                            $usersub->payment_status = 0;
                            $usersub->partial_failed_date = '';
                            $usersub->partial_failed_due = 0;
                            $usersub->start_date = $start_date;
                            $usersub->end_date = $end_date;
                            $usersub->save();

                            $user['amount'] = Yii::app()->common->formatPrice($amount, $currency->id, 1);
                            $user['card_holder_name'] = $_REQUEST['card_name'];
                            $user['card_last_fourdigit'] = $card_last_fourdigit;
                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $transaction_res['invoice_id']);
                            $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name, '', '', '', '', '', $this->language_code);
                            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_renew', $studio_id);
                            if ($isEmailToStudio) {
                                $trans_amt = Yii::app()->common->formatPrice($amount, $currency->id, 1);
                                $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_monthly_payment', $user_id, '', $trans_amt);
                            }
                            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $update_data = $payment_gateway::updateOutstandingBillAmount($usersub->profile_id);
                            $res['isSuccess'] = 1;
                            $res['message'] = $update_data['ACK'];
                        }
                    }
                } else {
                    $res['isSuccess'] = 1;
                    $res['message'] = $data['ACK'];
                }
            } else {
                $res['isSuccess'] = 0;
                $res['message'] = $data['L_LONGMESSAGE0'];
            }
            echo json_encode($res);
            exit;
        }
    }

    public function actionUpdateUserEmail() {
        $old_email = $_REQUEST['data']['Admin']['old_email'];
        $new_email = $_REQUEST['data']['Admin']['new_email'];
        $studio_id = Yii::app()->common->getStudiosId();
        $user = SdkUser::model()->findByAttributes(array('email' => $old_email, 'studio_id' => $studio_id));
        if (isset($user) && !empty($user)) {
            $usersub = UserSubscription::model()->findByAttributes(array('user_id' => $user->id, 'status' => 1));
            $enc = new bCrypt();
            $confirm_token = $enc->hash($new_email);
            if (strpos($confirm_token, "/") !== FALSE) {
                $confirm_token = str_replace('/', '', $confirm_token);
            }
            if (isset($usersub) && !empty($usersub)) {

                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);

                $activate = 0; //No plan and payment gateway set by studio
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $studio_payment_gateway = $plan_payment_gateway['gateways'];
                    $this->setPaymentGatwayVariable($studio_payment_gateway);
                }
                $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $res = $payment_gateway::updatePaymentProfile($user, $usersub, $new_email);
                if (isset($res['isSuccess']) && $res['isSuccess']) {
                    $user->email = $new_email;
                    $user->confirmation_token = $confirm_token;
                    $user->save();
                    Yii::app()->user->setFlash('success', 'Email updated successfully.');
                } else {
                    Yii::app()->user->setFlash('error', "Oops! Sorry, Email can't be updated!");
                }
            } else {
                $user->email = $new_email;
                $user->confirmation_token = $confirm_token;
                $user->save();
                Yii::app()->user->setFlash('success', 'Email updated successfully.');
            }
        } else {
            Yii::app()->user->setFlash('error', "Oops! Sorry, User doesn't exist!");
        }
        $this->redirect($this->createUrl('monetization/endusersupport'));
        exit;
    }

    public function actionOrderDetails() {
        $user_id = Yii::app()->user->id;
        if ($user_id > 0) {
            $studio_id = Yii::app()->common->getStudiosId();
            $order_id = $_REQUEST['id'];
            $con = Yii::app()->db;
            $order = $con->createCommand('SELECT * FROM pg_order WHERE transactions_id=' . $order_id . ' AND studio_id=' . $studio_id . ' AND buyer_id=' . $user_id)->queryROW();
            if (!empty($order)) {
                $orderdetails = $con->createCommand('SELECT d.*,p.currency_id FROM pg_order_details d,pg_product p WHERE d.product_id=p.id AND d.order_id=' . $order['id'])->queryAll();
                $shiping = $con->createCommand('SELECT * FROM pg_shipping_address WHERE order_id = ' . $order['id'])->queryROW();
                $countryname = Countries::model()->findByAttributes(array('code' => $shiping['country']));
                $shiping['country'] = isset($countryname->country) ? $countryname->country : $shiping['country'];
                $theme = $this->site_parent_theme;
                $template = Yii::app()->common->getTemplateDetails($theme);

                foreach ($order as $key => $value) {
                    $order['created_date'] = date('F d, Y h:i a', strtotime($order['created_date']));
                    $order['discount_type'] = 0;
                }
                foreach ($orderdetails as $kdetail => $vdetail) {
                    if ($vdetail['currency_id'] == 0) {
                        $ord = $con->createCommand("SELECT currency_id FROM transactions WHERE id=(SELECT transactions_id FROM pg_order WHERE id='" . $vdetail['order_id'] . "')")->queryROW();
                        $orderdetails[$kdetail]['currency_id'] = $ord['currency_id'];
                    }
                }
                $userinfo = $con->createCommand('SELECT display_name,email FROM sdk_users WHERE id=' . $order['buyer_id'])->queryROW();
                $status = PGOrder::getOrderStatus();
                $this->render('orderdetails', array('order' => $order, 'order_details' => $orderdetails, 'userinfo' => $userinfo, 'status' => $status, 'addr' => $shiping));
            }
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

//purchase history by suniln
    public function actionPurchaseHistory() {
        $user_id = Yii::app()->user->id;
        if ($user_id > 0) {
            if ($user_id == 1038) {
                $studio_id = Yii::app()->common->getStudiosId();
            } else {
                $studio_id = Yii::app()->user->studio_id;
            }
            $subscribed_user_data = Yii::app()->common->isSubscribed($user_id, $studio_id, 1);

            $userdate['expire_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date . '-1 Days'));
            $userdate['nextbilling_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date));
            //Pagination codes
            $items_per_page = 5;
            $page_size = $limit = $items_per_page;
            $offset = 0;
            $offsetq = 0;
            $tab_active = 0;
            if (isset($_REQUEST['p'])) {
                $offset = ($_REQUEST['p'] - 1) * $limit;
                $page_number = $_REQUEST['p'];
                $tab_active = 1;
            } else {
                $page_number = 1;
            }
            $sql = "select SQL_CALC_FOUND_ROWS(0),id,amount,user_id,studio_id,plan_id,currency_id,transaction_date,payer_id,invoice_id,subscription_id,ppv_subscription_id,subscriptionbundles_id,movie_id,order_number,transaction_type,transaction_status from transactions WHERE studio_id=" . $studio_id . " AND user_id=" . $user_id . " ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;
            $transactions = Yii::app()->db->createCommand($sql)->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();
            if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
                $http = 'https://';
            } else {
                $http = 'http://';
            }
            $url = $http . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            if ($item_count > 0) {
                $page_url = Yii::app()->general->full_url($url, '?p=');
                $page_url = Yii::app()->general->full_url($page_url, '&p=');
                $pg = new bootPagination();
                $pg->pagenumber = $page_number;
                $pg->pagesize = $page_size;
                $pg->totalrecords = $item_count;
                $pg->showfirst = true;
                $pg->showlast = true;
                $pg->paginationcss = "pagination-normal";
                $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
                $pg->defaultUrl = $page_url;
                if (strpos($page_url, '?') > -1)
                    $pg->paginationUrl = $page_url . "&p=[p]";
                else {
                    $pg->paginationUrl = $page_url . "?p=[p]";
                }
                $pagination = $pg->process();
            }
            $planModel = new PpvPlans();
            foreach ($transactions as $key => $details) {
                $transactions[$key]['invoice_id'] = $details['invoice_id'];
                $transactions[$key]['amount'] = $details['amount'];
                $transactions[$key]['currency_symbol'] = Currency::model()->findByPk($details['currency_id'])->symbol;
                $ppv_subscription_id = $details['ppv_subscription_id'];
                $movie_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->movie_id;
                $permalink = Film::model()->findByAttributes(array('id' => $movie_id))->permalink;
                $transactions[$key]['transaction_date'] = date('M d, Y', strtotime($details['transaction_date']));
                if ($details['transaction_type'] == 1) {
                    $subscription_id = $details['subscription_id'];
                    $startdate = UserSubscription::model()->findByAttributes(array('id' => $subscription_id))->start_date;
                    $plan_id = UserSubscription::model()->findByAttributes(array('id' => $subscription_id))->plan_id;
                    $plan_name = SubscriptionPlans::model()->findByAttributes(array('id' => $plan_id))->name;
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($startdate);
                    $resultDate = date('M y', strtotime($startdate . " - 1 month"));
                    if ($currdatetime <= $expirytime && strtotime($startdate) != '') {
                        $statusppv = $this->Language['active'];
                    } else {
                        $statusppv = 'N/A';
                    }
                    $transactions[$key]['statusppv'] = $statusppv;
                    $transactions[$key]['Content_type'] = 'digital';
                    $transactions[$key]['order_type'] = $this->Language['subscription_charges'];
                    $trans_type = $plan_name . ' - ' . $resultDate;
                } elseif ($details['transaction_type'] == 2) {
                    $end_date = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->end_date;
                    $season_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->season_id;
                    $episode_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->episode_id;
                    $movie_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->movie_id;
                    $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                    $content_types_id = Film::model()->findByAttributes(array('id' => $movie_id))->content_types_id;
                    $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                    $movie_names = $movie_name;
                    if ($season_id != 0 && $episode_id != 0) {
                        $movie_names = $movie_name . " -> " . $this->Language['season'] . " " . $season_id . " ->" . $episode_name;
                    }
                    if ($season_id == 0 && $episode_id != 0) {
                        $movie_names = $movie_name . " -> " . $episode_name;
                    }
                    if ($season_id != 0 && $episode_id == 0) {
                        $movie_names = $movie_name . " -> " . $this->Language['season'] . " " . $season_id;
                    }
                    $transactions[$key]['movie_name'] = $movie_names;
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($end_date);
                    $statusppv = 'N/A';
                    if ($content_types_id == 1) {
                        if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
                            $statusppv =$this->Language['active'];
                        }
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink;
                    }
                    if ($content_types_id == 4) {
                        if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
                            $statusppv = $this->Language['active'];
                        }
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink;
                    }
                    if ($content_types_id == 3) {
                        $datamovie_stream = Yii::app()->db->createCommand()
                                ->SELECT('f.id,f.name,f.permalink,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id')
                                ->from('films f ,movie_streams ms ')
                                ->where('f.id = ms.movie_id AND ms.id=' . $episode_id)
                                ->queryRow();
                        $embed_id = $datamovie_stream['embed_id'];
                        if ($currdatetime <= $expirytime && strtotime($end_date) != '0000-00-00 00:00:00') {
                            $statusppv = $this->Language['active'];
                        }
                        if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                            $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink;
                        }
                        if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                            $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink . '?season=' . $season_id;
                        }
                        if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                            $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/player/' . $permalink . '/stream/' . $embed_id;
                        }
                    }
                    $transactions[$key]['statusppv'] = $statusppv;
                    if (strtotime($end_date) != '' && $end_date != '0000-00-00 00:00:00') {
                        $expirydateppv = date('M d, Y', strtotime($end_date));
                        $transactions[$key]['expiry_dateppv'] =  $this->Language['selct_exp_date'] . $expirydateppv;
                    }
                    $trans_type = $movie_names;
                    $transactions[$key]['Content_type'] = 'digital';
                    $transactions[$key]['order_type'] = $this->Language['Pay_Per_View'];
                } elseif ($details['transaction_type'] == 3) {
                    $movie_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->movie_id;
                    $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                    //$title = $planModel->findByPk($details['plan_id'])->title;
                    $trans_type = $movie_name;
                    $transactions[$key]['Content_type'] = 'digital';
                    $transactions[$key]['order_type'] = $this->Language['pre_order'];
                    $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink;
                } elseif ($details['transaction_type'] == 4) {
                    $orderquery = "SELECT d.id,d.name,d.price,d.quantity,d.product_id,d.item_status,d.cds_tracking,d.tracking_url,o.order_number,o.cds_order_status FROM pg_order_details d, pg_order o  WHERE d.order_id=o.id AND o.transactions_id=" . $details['id'] . " ORDER BY id DESC";
                    $order = Yii::app()->db->createCommand($orderquery)->queryAll();
                    foreach ($order as $key1 => $val) {
                        $order[$key1]['permalink'] = PGProduct::model()->findByAttributes(array('id' => $val['product_id']))->permalink;
                        $order[$key1]['currency_id'] = $details['currency_id'];
                    }
                    $transactions[$key]['details'] = $order;
                    $transactions[$key]['order_item_id'] = $order[0]['id'];
                    $transactions[$key]['order_number'] = $order[0]['order_number'];
                    $transactions[$key]['cds_order_status'] = $order[0]['cds_order_status'];
                    $transactions[$key]['cds_tracking'] = $order[0]['cds_tracking'];
                    $transactions[$key]['tracking_url'] = $order[0]['tracking_url'];
                    $transactions[$key]['Content_type'] = 'Physical';
                    $transactions[$key]['order_type'] = $this->Language['cart_checkout'];
                    $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true);
                } elseif ($details['transaction_type'] == 5) {
                    $title = $planModel->findByPk($details['plan_id'])->title;
                    $ppv_subscription_id = $details['ppv_subscription_id'];
                    $ppvplanid = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->timeframe_id;
                    $ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id' => $ppvplanid))->validity_days;
                    $ppvbundlesmovieid = Yii::app()->db->createCommand("SELECT content_id FROM ppv_advance_content as pac left join ppv_timeframe as pt on pt.ppv_plan_id=pac.ppv_plan_id WHERE pt.id=" . $ppvplanid)->queryAll();
                    foreach ($ppvbundlesmovieid as $keyb => $contentid) {
                        $movie_id = $contentid['content_id'];
                        $ppvbundlesmovieid[$keyb]['permalink'] = Yii::app()->getBaseUrl(true) . '/' . Film::model()->findByAttributes(array('id' => $movie_id))->permalink;
                        $ppvbundlesmovieid[$keyb]['movie_name'] = Film::model()->findByAttributes(array('id' => $movie_id))->name;
                    }
                    $transactions[$key]['ppv_bundles_movie_name'] = $ppvbundlesmovieid;
                    $transactions[$key]['movie_name'] = 'digital';
                    $transaction_dates = $details['transaction_date'];
                    $expirydate = date('M d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
                    if ($currdatetime <= $expirytime) {
                        $statusbundles = $this->Language['active'];
                    } else {
                        $statusbundles = 'N/A';
                    }

                    if ($expirydate == "") {
                        $expirydate = "";
                    } else {
                        $expirydates = $this->Language['selct_exp_date'] . ':' . $expirydate;
                    }
                    $transactions[$key]['expiry_date'] = $expirydates;
                    $transactions[$key]['movie_name'] = $title;
                    $trans_type = $title;
                    $transactions[$key]['Content_type'] = 'digital';
                    $transactions[$key]['order_type'] = $this->Language['Pay_Per_View_Bundle'];
                } elseif ($details['transaction_type'] == 6) {
                    $ppv_subscription_id = $details['ppv_subscription_id'];
                    $transactions[$key]['order_type'] = $this->Language['voucher_redemption'];
                    $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=" . $ppv_subscription_id)->queryRow();
                    $end_date = $PpvSubscription['end_date'];
                    $start_date = $PpvSubscription['start_date'];
                    $season_id = $PpvSubscription['season_id'];
                    $episode_id = $PpvSubscription['episode_id'];
                    $movie_id = $PpvSubscription['movie_id'];
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($end_date);
                    $permalink = Film::model()->findByAttributes(array('id' => $movie_id))->permalink;
                    $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                    $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                    if ($end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
                        $expirydatevoucher = date('M d, Y', strtotime($end_date));
                        $transactions[$key]['expiry_date'] = $this->Language['selct_exp_date'] . ':' . $expirydatevoucher;
                    } else {
                        $expirydatevoucher = "N/A";
                        $transactions[$key]['expiry_date'] = '';
                    }
                    if ($currdatetime <= $expirytime && $end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
                        $statusvoucher = $this->Language['active'];
                    } else {
                        $statusvoucher = 'N/A';
                    }

                    $transactions[$key]['ppv_subscription_id'] = $ppv_subscription_id;
                    $datamovie_stream = Yii::app()->db->createCommand()
                            ->SELECT('f.id,f.name,f.permalink,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id')
                            ->from('films f ,movie_streams ms ')
                            ->where('f.id = ms.movie_id AND ms.id=' . $episode_id)
                            ->queryRow();
                    $embed_id = $datamovie_stream['embed_id'];
                    $vmovie_names = $movie_name;
                    if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                        $vmovie_names = $movie_name;
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink;
                    }
                    if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                        $vmovie_names = $movie_name . " ->  " . $this->Language['season'] . " " . $season_id . " ->" . $episode_name;
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/player/' . $permalink . '/stream/' . $embed_id;
                    }
                    if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                        $vmovie_names = $movie_name . " ->  " . $this->Language['season'] . " " . $season_id;
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink . '?season=' . $season_id;
                    }
                    $trans_type = $vmovie_names;
                } elseif ($details['transaction_type'] == 7) {
                    $subscriptionBundlesplanModel = new SubscriptionPlans();
                    $title = $subscriptionBundlesplanModel->findByPk($details['plan_id'])->name;
                     $subscriptionbundles_id = $details['subscription_id'];
                    $subscriptionbundleplan_details = UserSubscription::model()->findByAttributes(array('id' => $subscriptionbundles_id));
                    $subscriptionplanidd = "";
                    $subscriptionplanidd = $subscriptionbundleplan_details->plan_id;
                    $subscriptionbundlesmovieid = Yii::app()->db->createCommand("SELECT content_id FROM subscriptionbundles_content WHERE subscriptionbundles_plan_id=" . $subscriptionplanidd)->queryAll();
                    foreach ($subscriptionbundlesmovieid as $keyb => $contentid) {
                        $movie_id = $contentid['content_id'];
                        $subscriptionbundlesmovieid[$keyb]['permalink'] = Yii::app()->getBaseUrl(true) . '/' . Film::model()->findByAttributes(array('id' => $movie_id))->permalink;
                        $subscriptionbundlesmovieid[$keyb]['movie_name'] = Film::model()->findByAttributes(array('id' => $movie_id))->name;
                    }
                    $transactions[$key]['ppv_bundles_movie_name'] = $subscriptionbundlesmovieid;
                    $transactions[$key]['movie_name'] = 'digital';
                    $transaction_dates = $details['transaction_date'];
                    $transactions[$key]['subscriptionbundles_id'] = $details['plan_id'];
                    $expirydate = date('M d, Y', strtotime($subscriptionbundleplan_details->start_date));
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($subscriptionbundleplan_details->start_date);
                    if ($currdatetime <= $expirytime) {
                        $statussubscriptionbundles = $this->Language['active'];
                    } else {
                        $statussubscriptionbundles = 'N/A';
                    }
                    if ($expirydate == "") {
                        $expirydate = "";
                    } else {
                        $expirydates = $this->Language['selct_exp_date'] . ':' . $expirydate;
                    }
                    $transactions[$key]['expiry_date'] = $expirydates;
                    $transactions[$key]['movie_name'] = $title;
                    $trans_type = $title;
                    $transactions[$key]['Content_type'] = 'digital';
                    $transactions[$key]['order_type'] = $this->Language['subscription_charges'];
                } else {
                    $trans_type = '';
                }
                $transactions[$key]['transaction_for'] = $trans_type;
            }
            $status = PGOrder::getOrderStatus();
            $this->render('purchasehistory', array('purchasehistory' => $transactions, 'item_count' => $item_count, 'subscribed_user_data' => $subscribed_user_data, 'status' => $status, 'userdate' => @$userdate, 'statusbundles' => $statusbundles, 'statusppv' => $statusppv, 'statussubscriptionbundles' => $statussubscriptionbundles, 'statusvoucher' => $statusvoucher, 'pagination' => @$pagination));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

    public function getFilmName($movie_id) {
        $movie_name = Yii::app()->db->createCommand("SELECT name FROM films WHERE id=" . $movie_id)->queryROW();
        return $movie_name['name'];
    }

    public function getEpisodeName($episode_id) {
        $movie_name = Yii::app()->db->createCommand("SELECT episode_title FROM movie_streams WHERE id=" . $episode_id)->queryROW();
        return $movie_name['episode_title'];
    }

    public function actionCanLogin() {
        $this->layout = false;
        $ret = 0;
        if (isset(Yii::app()->user->id)) {
            $ret = Yii::app()->custom->canLogin();
        }
        echo $ret;
    }

    public function actionPaynowPayGate() {

        if (isset($_REQUEST) && !empty($_REQUEST)) {
            $data = $_REQUEST['data'];
            Yii::app()->session['save_user'] = 1;
            Yii::app()->session['reactivate'] = 0;
            $gateway_code = $data['payment_method'];
            $data['gateway_code'] = $gateway_code;
            $data['cvv'] = $data['security_code'];
            $currency = Currency::model()->findByPk($data['currency_id']);
            $data['currency_code'] = $currency->code;
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $data = $payment_gateway::processCard($data);
                if (isset($data) && $data['isSuccess'] == 0) {
                    Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/register');
                }
            }
        }
    }

    public function actionPayGateReturn() {
        $resArray = array();
        $studio_id = $this->studio->id;
        $connection = Yii::app()->db;
        $session_data = Yii::app()->session['from_data'];
        $save_user = Yii::app()->session['save_user'];
        $reactivate = Yii::app()->session['reactivate'] ? Yii::app()->session['reactivate'] : 0;
        $activate = Yii::app()->session['activate'] ? Yii::app()->session['activate'] : 0;
        $ip_address = CHttpRequest::getUserHostAddress();
        if (isset($_REQUEST) && $_REQUEST['TRANSACTION_STATUS'] == 1) {
            $PAY_REQUEST_ID = $_REQUEST['PAY_REQUEST_ID'];
            $data['PAY_REQUEST_ID'] = $PAY_REQUEST_ID;
            $data['gateway_code'] = 'paygate';
            $gateway_code = 'paygate';
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $resArray = $payment_gateway::getVaultData($data);
            }
            $plan_details = SubscriptionPlans::model()->getPlanDetails($session_data['data']['plan_id'], $studio_id);
            $price = $plan_details['price'];
            $currency_id = $plan_details['currency_id'];
            $currency = Currency::model()->findByPk($currency_id);
            $user['card_holder_name'] = $session_data['data']['card_name'];
            $user['email'] = $session_data['data']['email'];
            $user['exp_month'] = $session_data['data']['exp_month'];
            $user['exp_year'] = $session_data['data']['exp_year'];
            $card_last_fourdigit = str_replace(substr($session_data['data']['card_number'], 0, strlen($session_data['data']['card_number']) - 4), str_repeat("#", strlen($session_data['data']['card_number']) - 4), $session_data['data']['card_number']);

            $session_data['data']['token'] = $resArray['response']->QueryResponse->Status->VaultId;
            $session_data['data']['profile_id'] = $resArray['response']->QueryResponse->Status->VaultId;
            $session_data['data']['card_last_fourdigit'] = $card_last_fourdigit;
            $session_data['data']['card_type'] = $resArray['response']->QueryResponse->Status->PaymentType->Detail;
            $session_data['data']['response_text'] = json_encode($resArray['response']);
            if (isset($session_data) && !empty($session_data) && intval($save_user)) {
                $user_details = SdkUser::model()->saveSdkUser($session_data, $studio_id, 1);
                $user_id = $user_details['user_id'];
            } else {
                $user_id = Yii::app()->user->id;
            }

            if (isset($session_data) && !empty($session_data) && intval($save_user)) {
                $email = $session_data['data']['email'];
                $password = $session_data['data']['password'];
                $identity = new UserIdentity($email, $password);
                $identity->validate();
                Yii::app()->user->login($identity);
                unset(Yii::app()->session['back_btn']);
            }

            $plan_id = $session_data['data']['plan_id'];
            if (isset($resArray) && !empty($resArray['response'])) {
                if ($resArray['response']->QueryResponse->Status->TransactionStatusDescription == 'Approved') {
                    if (isset($plan_details) && !empty($plan_details)) {

                        $qry = "SELECT *,sp.id as sub_plan_id FROM subscription_plans sp  LEFT JOIN subscription_pricing sup ON (sp.id = sup.subscription_plan_id) WHERE sp.id = '" . $plan_id . "' AND sp.studio_id = '" . $studio_id . "' AND sp.status = '1'";
                        $command = Yii::app()->db->createCommand($qry);
                        $rowCount = $command->execute(); // execute the non-query SQL
                        $plans = $command->queryAll(); // execute a query SQL
                        if (count($plans)) {
                            foreach ($plans as $plan) {
                                if ($plan_id == $plan['sub_plan_id'] && $plan['currency_id'] == $session_data['data']['currency_id']) {
                                    $price = $plan['price'];
                                    $startup_discount = $plan['startup_discount'];
                                    $currency_id = $plan['currency_id'];
                                    $duration = $plan['duration'];
                                    $recurrence = $plan['recurrence'];
                                    $frequency = $plan['frequency'];
                                    $plan_name = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;

                                    $user['plan_desc'] = $plan['name'] . ', ' . Yii::app()->common->formatPrice($price, $currency_id) . '/' . $recurrence;
                                    $user['recurrence'] = $plan['recurrence'];
                                    $user['frequency'] = $plan['frequency'];
                                    $user['charged_amount'] = $price;
                                    $plan_id = $plan_id;
                                    $used_trial = Yii::app()->common->usedTrial();
                                }
                            }
                        }
                        $user['currency'] = $currency->code;
                        $user['trialperiod'] = '';
                        $user['trialfrequency'] = '';
                        $user['trialtotalcysles'] = '';
                        $user['trial_amount'] = '';

                        if (intval($plan_details['trial_period']) == 0 || $used_trial = 0) {
                            $start_date = Date('Y-m-d H:i:s');
                            $time = strtotime($start_date);
                            $recurrence_frequency = $plan_details['frequency'] . ' ' . strtolower($plan_details['recurrence']) . "s";
                            $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                            $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                        } else {
                            $trial_period = $plan_details['trial_period'] . ' ' . strtolower($plan_details['trial_recurrence']) . "s";
                            $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                            $time = strtotime($start_date);
                            $recurrence_frequency = $plan_details['frequency'] . ' ' . strtolower($plan_details['recurrence']) . "s";
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                        }

                        if ($activate || $reactivate) {
                            $card_data['gateway_code'] = 'paygate';
                            $card_data['card_holder_name'] = $session_data['data']['card_name'];
                            $card_data['exp_month'] = $session_data['data']['exp_month'];
                            $card_data['exp_year'] = $session_data['data']['exp_year'];
                            $card_data['card_last_fourdigit'] = $card_last_fourdigit;
                            $card_data['auth_num'] = '';
                            $card_data['token'] = $resArray['response']->QueryResponse->Status->VaultId;
                            $card_data['profile_id'] = $resArray['response']->QueryResponse->Status->VaultId;
                            $card_data['card_type'] = $resArray['response']->QueryResponse->Status->PaymentType->Detail;
                            $card_data['response_text'] = json_encode($resArray['response']);
                            $card_data['status'] = '';
                            $card_id = Yii::app()->billing->setCardInfo($card_data);
                            $cards = SdkCardInfos::model()->findByPk($card_id);
                            $cards->is_cancelled = 0;
                            $cards->save();
                            $usModel = new UserSubscription;
                            $usModel->studio_id = $studio_id;
                            $usModel->user_id = $user_id;
                            $usModel->plan_id = $plan_id;
                            $usModel->card_id = $card_id;
                            $usModel->studio_payment_gateway_id = $this->PAYMENT_GATEWAY_ID[$gateway_code];
                            $usModel->currency_id = $currency_id;
                            $usModel->profile_id = $resArray['response']->QueryResponse->Status->VaultId;
                            $usModel->amount = $price;
                            $usModel->start_date = $start_date;
                            $usModel->end_date = $end_date;
                            $usModel->status = 1;
                            $usModel->is_success = 1;
                            $usModel->ip = $ip_address;
                            $usModel->created_by = $user_id;
                            $usModel->created_date = new CDbExpression("NOW()");
                            $usModel->save();
                        }
                        if (intval($plan_details['trial_period']) == 0) {
                            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypalpro') {
                                //Prepare an array to charge from given card
                                $user['currency_id'] = $currency->id;
                                $user['currency_code'] = $currency->code;
                                $user['currency_symbol'] = $currency->symbol;
                                $user['amount'] = $price;
                                $user['token'] = $resArray['response']->QueryResponse->Status->VaultId;
                                $user['plan_desc'] = $resArray['response']->QueryResponse->Status->Reference;
                                $user['gateway_code'] = $gateway_code;
                                $user['studio_name'] = $this->studio->name;
                                $trans_data = $payment_gateway::processTransactions($user);

                                if ($trans_data['response']->CardPaymentResponse->Status->TransactionStatusDescription == 'Approved') {

                                    if ($trans_data['isSuccess'] == 1) {
                                        if ($trans_data['isSuccess'] == 1) {
                                            $plan_id = $session_data['data']['plan_id'];
                                            $return_parameters = json_encode($trans_data['response']);
                                            $query = "INSERT INTO user_payment_logs (user_id,plan_id,payment_tried,payment_tried_at,payment_done,studio_id,ip_address,payment_method,payment_token,return_parameters) VALUES";
                                            $query .= " ('" . $user_id . "', '" . $plan_id . "', 't', 'NOW()', 'f', " . $studio_id . ", '" . $ip_address . "', 'paypal', '" . $user['token'] . "', '" . $return_parameters . "')";
                                            $command = Yii::app()->db->createCommand($query);
                                            $command->execute();

                                            $user['email'] = $session_data['data']['email'];

                                            if ($trans_data['isSuccess'] == 1) {
                                                if (!intval($reactivate)) {
                                                    $select_query = "SELECT * FROM user_subscriptions WHERE user_id = " . $user_id . " AND status = 1";
                                                    $command = $connection->createCommand($select_query);
                                                    $data = $command->execute();
                                                    if (count($data) && $data) {
                                                        $query = "UPDATE user_subscriptions SET profile_id = '" . $resArray['response']->QueryResponse->Status->VaultId . "', is_success  = 1";
                                                        $query .= " WHERE user_id = " . $user_id . " AND status = 1";
                                                        $command = $connection->createCommand($query);
                                                        $command->execute();
                                                    } else {
                                                        $created_date = Date('Y-m-d H:i:s');
                                                        $query = "INSERT INTO user_subscriptions (user_id,studio_id,plan_id,studio_payment_gateway_id,amount,profile_id,ip,created_by,start_date,end_date,created_date,is_success) VALUES";
                                                        $query .= " ('" . $user_id . "','" . $studio_id . "', '" . $plan_id . "','" . $this->PAYMENT_GATEWAY_ID[$gateway_code] . "', '" . $price . "','" . $resArray['response']->QueryResponse->Status->VaultId . "','" . Yii::app()->request->getUserHostAddress() . "','" . $user_id . "','" . $start_date . "','" . $end_date . "','" . $created_date . "','1')";
                                                        $command = $connection->createCommand($query);
                                                        $command->execute();
                                                    }
                                                }
                                                $transaction = new Transaction();
                                                $transaction->user_id = $user_id;
                                                $transaction->studio_id = $studio_id;
                                                $transaction->plan_id = $plan_id;
                                                $transaction->currency_id = $currency->id;
                                                $transaction->payment_method = 'paygate';
                                                $transaction->transaction_status = $trans_data['response']->CardPaymentResponse->Status->StatusName;
                                                $transaction->invoice_id = $trans_data['response']->CardPaymentResponse->Status->TransactionId;
                                                $transaction->order_number = $resArray['response']->QueryResponse->Status->VaultId;
                                                $transaction->amount = $price;
                                                $transaction->payer_id = $trans_data['response']->CardPaymentResponse->Status->PayRequestId;
                                                $transaction->ip = $ip_address;
                                                $transaction->save();

                                                if (intval($reactivate)) {
                                                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_reactivate', $studio_id);
                                                    if ($isEmailToStudio) {
                                                        $this->sendStudioAdminEmails($studio_id, 'admin_user_reactivate', $user_id, '', 0);
                                                    }
                                                } else if (intval($activate)) {
                                                    $studio = Studios::model()->findByPk($studio_id);
                                                    $arg['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                                                    $arg['card_holder_name'] = $_REQUEST['data']['card_name'];
                                                    $arg['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

                                                    $file_name = Yii::app()->pdf->invoiceDetial($studio, $arg, $_REQUEST['data']['transaction_data']['invoice_id']);

                                                    $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', $file_name, '', '', '', '', '', $this->language_code);
                                                } else {
                                                    $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'welcome_email_with_subscription', '', '', '', '', '', '', $this->language_code);
                                                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email_with_subscription', $studio_id);
                                                    if ($isEmailToStudio) {
                                                        $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                                                    }
                                                }

                                                Yii::app()->user->setFlash("success", $this->ServerMessage['subscribed_success']);
                                                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                                            } else {
                                                Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                                                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                                            }
                                        }
                                    }
                                } else {
                                    Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                                }
                            }
                        } else {

                            if (intval($reactivate)) {
                                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_reactivate', $studio_id);
                                if ($isEmailToStudio) {
                                    $this->sendStudioAdminEmails($studio_id, 'admin_user_reactivate', $user_id, '', 0);
                                }
                            } else if (intval($activate)) {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', '', '', '', '', '', '', $this->language_code);
                            } else {
                                $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'welcome_email', '', '', '', '', '', '', $this->language_code);
                                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email', $studio_id);
                                if ($isEmailToStudio) {
                                    $admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'admin_new_user_registration', $user_id, '', 0);
                                }
                            }




                            Yii::app()->user->setFlash("success", $this->ServerMessage['subscribed_success']);
                            $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                        }
                    }
                } else {
                    Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                }
            } else {
                Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
            }
        }
    }

    function returnUrl() {
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        return Yii::app()->session['backbtnlink'] = $path;
    }

    public function actionPaymentUserDeatils() {
        $res = array();
        $gateway_code = $_REQUEST['gateway_code'];
        $res['user_id'] = $user_id = Yii::app()->user->id;
        $res['studio_id'] = $studio_id = Yii::app()->user->studio_id;
        $user = SdkUser::model()->findByPk($user_id);
        $res['email'] = $user->email;
        $res['user_name'] = $user->display_name;
        $res['contact'] = isset($user->phone) ? $user->phone : '';
        $res['api_user'] = $this->PAYMENT_GATEWAY_API_USER[$gateway_code];
        $res['api_password'] = $this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code];
        $res['api_signature'] = $this->PAYMENT_GATEWAY_API_SIGNATURE[$gateway_code];
        $res['studio_name'] = $this->studio->name;
        $res['studio_logo'] = $this->siteLogo;
        echo json_encode($res);
    }

    public function actionInstafeezPpvTransaction() {
        $data = $_REQUEST['user_data'];
        $transaction_data = $_REQUEST['transaction_data'];
        $trans_data['transaction_status'] = $transaction_data['status'];
        $trans_data['invoice_id'] = $transaction_data['_id'];
        $trans_data['order_number'] = $transaction_data['payment_id'];
        $trans_data['amount'] = $transaction_data['amount_details']['amount'];
        $trans_data['currency_code'] = $transaction_data['amount_details']['currency'];
        $trans_data['response_text'] = json_encode($transaction_data);
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
        $gateway_code = 'instafeez';

        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
        $video_id = $Films->id;
        $plan_id = 0;
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        if ((isset($data['plan_id']) && intval($data['plan_id']))) {
            $plan_id = $data['plan_id'];
            $timeframe_id = @$data['timeframe_id'];
            $isadv = @$data['is_advance_purchase'];
            $timeframe_days = 0;
            $end_date = '';
            //Get plan detail which user selected at the time of registration
            $planModel = new PpvPlans();
            $plan = $planModel->findByPk($plan_id);

            $data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;

            if (intval($is_bundle) && intval($timeframe_id)) {
                (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $data['timeframe_id'], $studio_id);
                $price = $price->attributes;

                $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
            } else {
                $price = Yii::app()->common->getPPVPrices($plan->id, $this->studio->default_currency_id);
            }

            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_id'] = $currency->id;
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;

            $isadv = @$data['is_advance_purchase'];
            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
                $time = strtotime($start_date);
                $expiry = $timeframe_days . ' ' . "days";
                $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
            } else {
                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                    }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
                }
            }

            if ($is_bundle) {//For PPV bundle
                if (intval($is_subscribed_user)) {
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                        $data['amount'] = $amount = $price['show_subscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_subscribed'];
                    }
                } else {
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                        $data['amount'] = $amount = $price['show_unsubscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_unsubscribed'];
                    }
                }
            } else {
                //Set different prices according to schemes
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                    //Check which part wants to sell by studio admin
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                        if (intval($is_episode)) {
                            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_uniq_id']));
                            $data['episode_id'] = $streams->id;
                            $data['episode_uniq_id'] = $streams->embed_id;
                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['episode_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['episode_unsubscribed'];
                            }
                        }
                    } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                        if (intval($is_season)) {
                            $data['episode_id'] = 0;

                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['season_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['season_unsubscribed'];
                            }

                            //$end_date = '';//Make unlimited time limit for season
                        }
                    } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                        if (intval($is_show)) {
                            $data['season_id'] = 0;
                            $data['episode_id'] = 0;
                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['show_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['show_unsubscribed'];
                            }

                            //$end_date = '';//Make unlimited time limit for show
                        }
                    }
                } else {//Single part videos
                    $data['season_id'] = 0;
                    $data['episode_id'] = 0;

                    if (intval($is_subscribed_user)) {
                        $data['amount'] = $amount = $price['price_for_subscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_unsubscribed'];
                    }
                }
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon']) && $data['coupon'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon'], $amount, $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $amount = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }

            if (intval($is_bundle)) {
                $VideoName = $plan->title;
            } else {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
            }

            $VideoDetails = $VideoName;

            if (!$is_bundle && isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if ($data['season_id'] == 0) {
                    $VideoDetails .= ', All Seasons';
                } else {
                    $VideoDetails .= ', Season ' . $data['season_id'];
                    if ($data['episode_id'] == 0) {
                        $VideoDetails .= ', All Episodes';
                    } else {
                        $episodeName = Yii::app()->common->getEpisodename($streams->id);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', ' . $episodeName;
                    }
                }
            }

            $data['amount'] = $trans_data['amount'];
            if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                $crd = Yii::app()->billing->setCardInfo($data);
                $data['card_id'] = $crd;
            }
            $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, @$data['coupon'], $isadv, $gateway_code);
            $ppv_subscription_id = $set_ppv_subscription;

            $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
            $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
            $url = self::setRedirectPpv($Films, $plan, $data, $isadv, 1);
            echo $url;
            exit;
        }
    }

    public function actionsetUserInterNetSpeed() {
        $this->layout = false;
        if (filter_var($_REQUEST['speedMbps'], FILTER_VALIDATE_FLOAT)) {
            $_SESSION['internetSpeed'] = $_REQUEST['speedMbps'];
            echo $_REQUEST['speedMbps'];
            exit;
        }
    }

    /*
     * Below Function are for storing session cart to database
     * @author manas@muvi.com
     */

    function saveCartToDB($cart_item) {
        $pgcart = new PGCart();
        $ip = CHttpRequest::getUserHostAddress();
        $pgcart->insertCart($this->studio->id, Yii::app()->user->id, $cart_item, $ip);
    }

    /*
     * Below Function are for Reading session cart from database
     * @author manas@muvi.com
     */

    function readCartFromDB($studio_id, $userid) {
        $pgcart = new PGCart();
        $cart_arr = $pgcart->getCart($studio_id, $userid);
        if (!empty($cart_arr)) {
            $_SESSION['cart_item'] = json_decode($cart_arr['cart_item'], true);
            $_SESSION['totalqnt'] = 0;
            $item_total = 0;
            foreach ($_SESSION["cart_item"] as $k => $v) {
                $_SESSION['totalqnt'] = $_SESSION['totalqnt'] + $_SESSION["cart_item"][$k]["quantity"];
                $item_total += ($v["price"] * $v["quantity"]);
                $currency_id = $v['currency_id'];
            }
            $_SESSION['item_total'] = Yii::app()->common->formatPrice($item_total, $currency_id);
        }
    }

    /**
     *
     * Handles payment (payment return from hosted pages) 
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function actionPciPayment() {
        
        /* Write all the response from Adyen to a log fle*/
            $logfile = dirname(__FILE__).'/adyen.txt';
            $msg = "\n----------Log File: UserController: actionPciPayment()----------\n";
            $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
            $data_log = serialize($_REQUEST);
            $msg.= $data_log."\n";
            file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        /*end*/
        
        $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['merchantReference']));
        $data = array();
        if(isset($log_data['log_data']) && trim($log_data['log_data'])){
             $data = json_decode($log_data['log_data'], true);
        }
       
        $is_success = 0;
        if(!empty($data)){
            if (isset($_REQUEST['authResult']) && (trim(strtoupper($_REQUEST['authResult'])) == 'AUTHORISED')) {
                $is_success = 1;
            } else if (isset($_REQUEST['authResult']) && (trim(strtoupper($_REQUEST['authResult'])) == 'PENDING') && isset($_REQUEST['merchantSig']) && trim($_REQUEST['merchantSig']) == trim($data["merchantSig"])) {
                $is_success = 1;
            }
        }
        
        if ($is_success) {

            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = Yii::app()->user->id;
            $gateway_code = $data["payment_method"];
            $plan_id = $data['plan_id'];
            $ip_address = CHttpRequest::getUserHostAddress();
            $planModel = new PpvPlans();
            $plan = $planModel->findByPk($plan_id);

            if (isset($data['is_advance_purchase']) && intval($data['is_advance_purchase'])) {
                $isadv = $data['is_advance_purchase'];
            } else {
                $isadv = $data['isadv'];
            }
            $Films = Film::model()->findByAttributes(array('studio_id' => $data['studio_id'], 'uniq_id' => $data['movie_id']));
            $video_id = $Films->id;


            $VideoName = Yii::app()->common->getVideoname($video_id);
            $VideoName = trim($VideoName);
            $VideoDetails = $VideoName;
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if ($data['season_id'] == 0) {
                    $VideoDetails .= ', All Seasons';
                } else {
                    $VideoDetails .= ', Season ' . $data['season_id'];
                    if ($data['episode_id'] == 0) {
                        $VideoDetails .= ', All Episodes';
                    } else {
                        $episodeName = Yii::app()->common->getEpisodename($streams->id);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', ' . $episodeName;
                    }
                }
            }
            //Save ppv subscription detail
            $ppvsubscription = new PpvSubscription;
            $ppvsubscription->user_id = $user_id;
            $ppvsubscription->studio_id = $studio_id;
            $ppvsubscription->ppv_plan_id = $plan_id;
            $ppvsubscription->studio_payment_gateway_id = $this->PAYMENT_GATEWAY_ID[$gateway_code];
            $ppvsubscription->token = Yii::app()->common->generateUniqNumber();
            $ppvsubscription->amount = $data['amount'];
            if (trim($data['start_date'])) {
            $ppvsubscription->start_date = $data['start_date'];
		}
            if (trim($data['end_date'])) {
            $ppvsubscription->end_date = $data['end_date'];
		}
            $ppvsubscription->status = 1;
            $ppvsubscription->movie_id = $Films->id;
            $ppvsubscription->season_id = $data['season_id'];
            $ppvsubscription->episode_id = $data['episode_id'];
            $ppvsubscription->is_advance_purchase = $isadv;
            $ppvsubscription->is_ppv_bundle = $data['is_bundle'];
            $ppvsubscription->timeframe_id = $data['timeframe_id'];
            $ppvsubscription->currency_id = $data['currency_id'];
            $ppvsubscription->coupon_code = $data['coupon'];
            $ppvsubscription->ip = $ip_address;
            $ppvsubscription->created_by = $user_id;
            $ppvsubscription->created_date = new CDbExpression("NOW()");
            $ppvsubscription->view_restriction = @$data['view_restriction'];
            $ppvsubscription->watch_period = @$data['watch_period'];
            $ppvsubscription->access_period = @$data['access_period'];
            $ppvsubscription->save();
            $ppv_subscription_id = $ppvsubscription->id;

            //Save a transaction detail
            $transaction = new Transaction;
            $transaction->user_id = $user_id;
            $transaction->studio_id = $studio_id;
            $transaction->plan_id = $plan_id;
            $transaction->currency_id = $data['currency_id'];
            $transaction->transaction_date = new CDbExpression("NOW()");
            $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_code];
            $transaction->transaction_status = $_REQUEST['authResult'];
            $transaction->invoice_id = $_REQUEST['pspReference'];
            $transaction->order_number = $_REQUEST['pspReference'];
            $transaction->amount = $data['amount'];
            $transaction->dollar_amount = $data['amount'];
            $transaction->user_reference = $data['shopper_reference'];
            $transaction->payer_id = $_REQUEST['merchantSig'];
            $transaction->ppv_subscription_id = $ppv_subscription_id;
            $transaction->movie_id = $Films->id;
            $transaction->ip = $ip_address;
            $transaction->created_date = new CDbExpression("NOW()");
            $tType = 2;
            if (isset($isadv) && intval($isadv)) {
                $tType = 3;
            } elseif (isset($data['is_bundle']) && intval($data['is_bundle'])) {
                $tType = 5;
            }

            $transaction->transaction_type = $tType;
            $transaction->save();
            $transaction_id = $transaction->id;
            
            $couponDetails = Coupon::model()->findByAttributes(array('coupon_code' => @$data['coupon']));
            if (isset($couponDetails) && !empty($couponDetails)) {
                if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                    $qry = "UPDATE coupon SET used_by='" . $couponDetails->used_by . "," . $user_id . "',used_date = NOW() WHERE coupon_code='" . $data['coupon'] . "'";
                } else {
                    $qry = "UPDATE coupon SET used_by=" . $user_id . ",used_date = NOW() WHERE coupon_code='" . $data['coupon'] . "'";
                }
                Yii::app()->db->createCommand($qry)->execute();
            }
            
            PciLog::model()->deleteByPk($log_data['id']);
            $trans_data['amount'] = $data['amount'];
            $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
            self::setRedirectPpv($Films, $plan, $data, $isadv);
            exit;
        } else {
            PciLog::model()->deleteByPk($log_data['id']);
            $this->redirect(Yii::app()->session['backbtnlink']);
        }
    }

    public function actionPciData() {
        $data = $_POST;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = $data['session_url'];
        Yii::app()->session['backbtnlink'] = $path;

        $gateway_code = $_POST['payment_method'];
        $data['studio_id'] = $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $plan_id = $data['plan_id'];
        $is_bundle = @$data['is_bundle'];
        $timeframe_id = @$data['timeframe_id'];
        if (isset($data['is_advance_purchase']) && intval($data['is_advance_purchase'])) {
            $isadv = $data['is_advance_purchase'];
        } else {
            $isadv = $data['isadv'];
        }

        $timeframe_days = 0;
        $end_date = '0000-00-00 00:00:00';
        $data['coupon'] = $_POST['coupon'];
        $plan = PpvPlans::model()->findByPk($plan_id);

        $data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        if (intval($is_bundle) && intval($timeframe_id)) {
            (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $data['timeframe_id'], $studio_id);
            $price = $price->attributes;

            $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
        } else {
            $price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id']);
        }
        $currency = Currency::model()->findByPk($price['currency_id']);
        $data['currency_id'] = $currency->id;
        $data['currency_code'] = $currency->code;
        $data['currency_symbol'] = $currency->symbol;

        //Calculate ppv expiry time only for single part or episode only
        $start_date = Date('Y-m-d H:i:s');
        $data['start_date'] = $start_date;
        if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
            $time = strtotime($start_date);
            $expiry = $timeframe_days . ' ' . "days";
            $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
        } else {
            if (intval($isadv) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }
        }

        if ($is_bundle) {//For PPV bundle
            $data['season_id'] = 0;
            $data['episode_id'] = 0;

            if (intval($is_subscribed_user)) {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                }
            } else {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_unsubscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        } else {
            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $data['episode_uniq_id'] = $data['episode_id'];
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;
                        $data['episode_uniq_id'] = $streams->embed_id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['season_unsubscribed'];
                        }

                        if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status']) && intval($isadv) == 0) {
                            $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                            $season_price_data['ppv_pricing_id'] = $price['id'];
                            $season_price_data['studio_id'] = $studio_id;
                            $season_price_data['currency_id'] = $price['currency_id'];
                            $season_price_data['season'] = $data['season_id'];
                            $season_price_data['is_subscribed'] = $is_subscribed_user;

                            $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                            if ($season_price != -1) {
                                $data['amount'] = $amount = $season_price;
                            }
                        }

                        //$end_date = '';//Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['show_unsubscribed'];
                        }

                        //$end_date = '';//Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        }
        $data['end_date'] = $end_date;
        $couponCode = '';
        //Calculate coupon if exists
        if ((isset($data['coupon']) && $data['coupon'] != '') || (isset($data['coupon_instafeez']) && $data['coupon_instafeez'] != '')) {
            $coupon = (isset($data['coupon']) && $data['coupon'] != '') ? $data['coupon'] : $data['coupon_instafeez'];
            $getCoup = Yii::app()->common->getCouponDiscount(trim($coupon), $amount, $studio_id, $user_id, $data['currency_id']);
            $data['amount'] = $amount = $getCoup["amount"];
            $couponCode = $getCoup["couponCode"];
        }
        $timeparts = explode(" ", microtime());
        $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
        $reference_number = explode('.', $currenttime);
        $data['reference_number'] = $reference_number[0];
        $data['email'] = Yii::app()->user->email;
        $data['shopper_reference'] = Yii::app()->user->id . $reference_number[0];
        $data['shopper_interaction'] = "ContAuth";
        $sql = "SELECT user_reference FROM transactions WHERE user_id = {$user_id} ORDER BY id DESC LIMIT 1";
        $trans_data = Yii::app()->db->createCommand($sql)->queryRow();

        if (isset($trans_data) && trim($trans_data['user_reference'])) {
            $data['shopper_reference'] = $trans_data['user_reference'];
            $data['shopper_interaction'] = "Ecommerce";
        }
        $shopper_country = $_SESSION['country_name'];
        $lang_code = isset($this->language_code) && trim($this->language_code) ? $this->language_code : Yii::app()->common->getLanguageCode($shopper_country);
        $data['shopper_locale'] = trim($lang_code) == 'en' ? 'en_GB' : $lang_code;
        $data['resURL'] = Yii::app()->getBaseUrl(true) . "/user/pciPayment";
        $data['user_id'] = $user_id;
        $data['studio_payment_gateway_id'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $pci_data = $payment_gateway::processCard($data);
        $data['merchantSig'] = $pci_data['response']['merchantSig'];
        $log = new PciLog();
        $log->unique_id = $reference_number[0];
        $log->log_data = json_encode($data);
        $log->save();
        
        /* Write all the response from Adyen to a log fle*/
            $logfile = dirname(__FILE__).'/adyen_request.txt';
            $msg = "\n----------Log File: UserController: actionPciData()----------\n";
            $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
            $data_log = serialize($data);
            $msg.= $data_log."\n";
            file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        /*end*/
        
        echo json_encode($pci_data);
        exit;
    }

    //sisp payment gateway by sunil N
    public function actionSispPayment() {
        //print_r($_REQUEST);
        //exit;
        $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['merchantRespMerchantRef']));
        if (isset($_REQUEST['messageType']) && trim(strtoupper($_REQUEST['messageType'])) == 8) {
            $data = json_decode($log_data['log_data'], true);
            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = Yii::app()->user->id;
            $gateway_code = $data["payment_method"];
            $plan_id = $data['plan_id'];
            $ip_address = CHttpRequest::getUserHostAddress();
            $planModel = new PpvPlans();
            $plan = $planModel->findByPk($plan_id);
            $isadv = @$data['isadv'];
            $Films = Film::model()->findByAttributes(array('studio_id' => $data['studio_id'], 'uniq_id' => $data['movie_id']));
            $video_id = $Films->id;
            $VideoName = Yii::app()->common->getVideoname($video_id);
            $VideoName = trim($VideoName);
            $VideoDetails = $VideoName;
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if ($data['season_id'] == 0) {
                    $VideoDetails .= ', All Seasons';
                } else {
                    $VideoDetails .= ', Season ' . $data['season_id'];
                    if ($data['episode_id'] == 0) {
                        $VideoDetails .= ', All Episodes';
                    } else {
                        $episodeName = Yii::app()->common->getEpisodename($streams->id);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', ' . $episodeName;
                    }
                }
            }
            $start_date = $data['start_date'];
            $end_date = $data['end_date'];
            //Save ppv subscription detail
            $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
            $ppv_subscription_id = $set_ppv_subscription;

            $trans_data['amount'] = $data['amount'];
            $trans_data['order_number'] = $_REQUEST['merchantRespMerchantSession'];
            $trans_data['invoice_id'] = $_REQUEST['merchantRespMessageID'];
            $trans_data['transaction_status'] = 'Success';
            $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);

            $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
            PciLog::model()->deleteByPk($log_data['id']);
            self::setRedirectPpv($Films, $plan, $data, $isadv);
            exit;
        } else {
            PciLog::model()->deleteByPk($log_data['id']);
            Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
            $this->redirect(Yii::app()->session['backbtnlink']);
        }
    }

    public function actionSispProcessCard() {
        $data = $_POST;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = $data['session_url'];
        Yii::app()->session['backbtnlink'] = $path;
        $gateway_code = $_POST['payment_method'];
        $data['studio_id'] = $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $plan_id = $data['plan_id'];
        //carddetails
        $card_name = @$data['card_name'];
        $card_number = @$data['card_number'];
        $exp_month = @$data['exp_month'];
        $exp_year = @$data['exp_year'];
        $security_code = @$data['security_code'];
        //end
        $is_bundle = @$data['is_bundle'];
        $timeframe_id = @$data['timeframe_id'];
        $isadv = @$data['isadv'];
        $timeframe_days = 0;
        $end_date = '';
        $data['coupon'] = $_POST['coupon'];
        $plan = PpvPlans::model()->findByPk($plan_id);
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        $data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;
        if (intval($is_bundle) && intval($timeframe_id)) {
            (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $data['timeframe_id'], $studio_id);
            $price = $price->attributes;
            $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
        } else {
            $price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id']);
        }
        $currency = Currency::model()->findByPk($price['currency_id']);
        $data['currency_id'] = $currency->id;
        $data['currency_code'] = $currency->code;
        $data['currency_symbol'] = $currency->symbol;
        $data['iso_num'] = $currency->iso_num;

        //Calculate ppv expiry time only for single part or episode only
        $start_date = Date('Y-m-d H:i:s');
        $data['start_date'] = $start_date;
        if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
            $time = strtotime($start_date);
            $expiry = $timeframe_days . ' ' . "days";
            $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
        } else {
            if (intval($isadv) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }
        }
        if ($is_bundle) {//For PPV bundle
            $data['season_id'] = 0;
            $data['episode_id'] = 0;

            if (intval($is_subscribed_user)) {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                }
            } else {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_unsubscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        } else {
            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $data['episode_uniq_id'] = $data['episode_id'];
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;
                        $data['episode_uniq_id'] = $streams->embed_id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['season_unsubscribed'];
                        }

                        //$end_date = '';//Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['show_unsubscribed'];
                        }

                        //$end_date = '';//Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        }
        $data['end_date'] = $end_date;
        $couponCode = '';
        //Calculate coupon if exists
        if ((isset($data['coupon']) && $data['coupon'] != '') || (isset($data['coupon_instafeez']) && $data['coupon_instafeez'] != '')) {
            $coupon = (isset($data['coupon']) && $data['coupon'] != '') ? $data['coupon'] : $data['coupon_instafeez'];
            $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
            $data['amount'] = $amount = $getCoup["amount"];
            $couponCode = $getCoup["couponCode"];
        }
        $timeparts = explode(" ", microtime());
        $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
        $reference_number = explode('.', $currenttime);
        $data['merchantRef'] = $reference_number[0];
        $data['merchantSession'] = Yii::app()->common->generateUniqNumber();
        $log = new PciLog();
        $log->unique_id = $reference_number[0];
        $log->log_data = json_encode($data);
        $log->save();
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $sisp_data = $payment_gateway::processCard($data);
        echo json_encode($sisp_data);
        exit;
    }

    public function actionclientToken() {
        require_once 'Braintree/lib/Braintree.php';
        $gateway_code = 'braintreepaypal';
        if ($this->IS_LIVE_API_PAYMENT_GATEWAY[$gateway_code] != 'sandbox') {
            \Braintree\Configuration::environment('production');
        } else {
            \Braintree\Configuration::environment('sandbox');
        }
        \Braintree\Configuration::merchantId($this->PAYMENT_GATEWAY_API_USER[$gateway_code]);
        \Braintree\Configuration::publicKey($this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code]);
        \Braintree\Configuration::privateKey($this->PAYMENT_GATEWAY_API_SIGNATURE[$gateway_code]);

        $tk = new Braintree\ClientToken();
        $token = $tk->generate();
        echo $token;
    }

    public function actionAddToFavList() {

        if (isset($_POST['content_id']) && isset($_POST['content_type'])) {
            $content_id = $_POST['content_id'];
            $content_type = $_POST['content_type'];
            $studio_id = $this->studio->id;
            $user_id = Yii::app()->user->id;
            //If action=1, it will add new and if 0 then it will update
            $action = isset($_POST['action']) ? $_POST['action'] : 1;
            if ($action == 0) {
                $this->actionDeleteFromFavList($_POST);
            } else if ($action == 1) {
                $user_fav_stat = UserFavouriteList::model()->findByAttributes(array('content_id' => $content_id, 'content_type' => $content_type, 'studio_id' => $studio_id, 'user_id' => $user_id));
                if (empty($user_fav_stat)) {
                    $usr_fav = new UserFavouriteList;
                    $usr_fav->status = '1';
                    $usr_fav->content_id = $content_id;
                    $usr_fav->content_type = $content_type;
                    $usr_fav->studio_id = $studio_id;
                    $usr_fav->user_id = $user_id;
                    $usr_fav->date_added = date('Y-m-d H:i:s');
                    $usr_fav->last_updated_at = date('Y-m-d H:i:s');
                } else {
                    $usr_fav = UserFavouriteList::model()->findByPk($user_fav_stat->id);
                    if ($user_fav_stat->status == '0') {
                        $usr_fav->status = '1';
                        $usr_fav->last_updated_at = date('Y-m-d H:i:s');
                    }
                }
            }
            $usr_fav->save();
            echo "success";
            exit;
        }
    }

    public function actionDeleteFromFavList() {
        if (isset($_POST['content_id']) && isset($_POST['content_type'])) {
            $content_id = $_POST['content_id'];
            $content_type = $_POST['content_type'];
            $studio_id = $this->studio->id;
            $user_id = Yii::app()->user->id;
            $params = array(':content_id' => $content_id, ':content_type' => $content_type, ':studio_id' => $studio_id, ':user_id' => $user_id);
            $user_fav_stat = UserFavouriteList::model()->deleteAll('content_id = :content_id AND content_type = :content_type AND studio_id = :studio_id AND user_id = :user_id', $params);
            echo "success";
            exit;
        }
    }

    public function actionFavourites() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        $language_id = $this->language_id;
        $page_size = $limit = Yii::app()->general->itemsPerPage();
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $limit;
        } else if (isset($_REQUEST['p'])) {
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
        } else {
            $page_number = 1;
        }
        if (($this->add_to_favourite) && ($user_id > 0)) {
            $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($user_id, $studio_id, $page_size, $offset);
            $item_count = $favlist['total'];
            $list = $favlist['list'];
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $page_url = Yii::app()->general->full_url($url, '?p=');
            $page_url = Yii::app()->general->full_url($page_url, '&p=');
            $pg = new bootPagination();
            $pg->pagenumber = $page_number;
            $pg->pagesize = $page_size;
            $pg->totalrecords = $item_count;
            $pg->showfirst = true;
            $pg->showlast = true;
            $pg->paginationcss = "pagination-normal";
            $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
            $pg->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1)
                $pg->paginationUrl = $page_url . "&p=[p]";
            else
                $pg->paginationUrl = $page_url . "?p=[p]";
            $pagination = $pg->process();
            $this->render('favlist', array('pagination' => $pagination, 'contents' => json_encode($list), 'orderby' => @$order, 'item_count' => @$item_count, 'page_size' => @$page_size, 'pages' => @$pages));
        }else {
            $redirect_url = Yii::app()->getBaseUrl(true);
            Yii::app()->user->setFlash('error', "You do not have access to this page.");
            $this->redirect($redirect_url);
            exit;
        }
    }

    //To unsubscribe the email from announcements
    //@author: B Parida
    public function actionUnsubscribeFromNotificationEmail() {
        $user_id = $_GET['user'];
        $user_id = base64_decode($user_id);
        $user = SdkUser::model()->findByPk($user_id);
        if (!empty($user)) {
            if ($user->announcement_subscribe == '1') {
                $user->announcement_subscribe = '0';
                $user->announcement_subscribe_time = date('Y-m-d H:i:s');
                $user->save();
            }
            $redirect_url = Yii::app()->getBaseUrl(true);
            Yii::app()->user->setFlash('success', $this->Language['unsuscribe_from_announcement_mail']);
            $this->redirect($redirect_url);
            exit;
        } else {
            $redirect_url = Yii::app()->getBaseUrl(true);
            Yii::app()->user->setFlash('error', "You do not have access to this page.");
            $this->redirect($redirect_url);
            exit;
        }
    }

    public function actionmyLibrary() {
        $user_id = Yii::app()->user->id;
        if ($user_id > 0) {
            if ($user_id == 1038) {
                $studio_id = Yii::app()->common->getStudiosId();
            } else {
                $studio_id = Yii::app()->user->studio_id;
            }
            $transactions = Yii::app()->db->createCommand()
                    ->select('plan_id,ppv_subscription_id,transaction_type')
                    ->from('transactions')
                    ->where('studio_id = ' . $studio_id . ' AND user_id=' . $user_id . ' AND transaction_type IN (2,3,5,6)')
                    ->order('id DESC')
                    ->queryAll();
            $planModel = new PpvPlans();
            foreach ($transactions as $key => $details) {
                $ppv_subscription = PpvSubscription::model()->find('id=:id', array(':id' => $details['ppv_subscription_id']));
				$ppv_subscription->end_date = $ppv_subscription->end_date?$ppv_subscription->end_date=='1970-01-01 00:00:00'?'0000-00-00 00:00:00':$ppv_subscription->end_date:'0000-00-00 00:00:00';
                $film = Film::model()->find('id=:id', array(':id' => $ppv_subscription->movie_id));
                $currdatetime = strtotime(date('y-m-d'));
                $expirytime = strtotime($ppv_subscription->end_date);
                if ($details['transaction_type'] == 2) {//PPV
                    $statusppv = 'N';
                    if ($film->content_types_id == 1 || $film->content_types_id == 4) {
                        if ($currdatetime <= $expirytime && (strtotime($ppv_subscription->end_date) != '' || $ppv_subscription->end_date != '0000-00-00 00:00:00')) {
                            $statusppv = 'A';
                        } else if ($ppv_subscription->end_date == '0000-00-00 00:00:00') {
                            $statusppv = 'A';
                        }
                        $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                    }
                    if ($film->content_types_id == 3) {

                        $datamovie_stream = Yii::app()->db->createCommand()
                                ->SELECT('ms.embed_id')
                                ->from('movie_streams ms ')
                                ->where('ms.id=' . $ppv_subscription->episode_id)
                                ->queryRow();
                        $embed_id = $datamovie_stream['embed_id'];
                        if ($currdatetime <= $expirytime && strtotime($ppv_subscription->end_date) != '0000-00-00 00:00:00') {
                            $statusppv = 'A';
                        } else if ($ppv_subscription->season_id != 0 && $ppv_subscription->episode_id == 0) {
                            $statusppv = 'A';
                        } else if ($ppv_subscription->end_date == '0000-00-00 00:00:00') {
                            $statusppv = 'A';
                        }
                        if ($statusppv == 'A') {
                            if ($ppv_subscription->movie_id != 0 && $ppv_subscription->season_id == 0 && $ppv_subscription->episode_id == 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            }
                            if ($ppv_subscription->movie_id != 0 && $ppv_subscription->season_id != 0 && $ppv_subscription->episode_id == 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $ppv_subscription->season_id;
                            }
                            if ($ppv_subscription->movie_id != 0 && $ppv_subscription->season_id != 0 && $ppv_subscription->episode_id != 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/player/' . $film->permalink . '/stream/' . $embed_id;
                            }
                        }
                    }
                    if ($statusppv == 'A') {
                        $episode_name = isset($ppv_subscription->episode_id) ? $this->getEpisodeName($ppv_subscription->episode_id) : "";
                        $movie_names = $film->name;
                        if ($ppv_subscription->season_id != 0 && $ppv_subscription->episode_id != 0) {
                            $movie_names = $film->name . " -> Season " . $ppv_subscription->season_id . " ->" . $episode_name;
                        }
                        if ($ppv_subscription->season_id == 0 && $ppv_subscription->episode_id != 0) {
                            $movie_names = $film->name . " -> " . $episode_name;
                        }
                        if ($ppv_subscription->season_id != 0 && $ppv_subscription->episode_id == 0) {
                            $movie_names = $film->name . " -> " . $this->Language['season'] . " " . $ppv_subscription->season_id;
                        }
                        if ($film->content_types_id == 2) {
                            $poster = $this->getPoster($ppv_subscription->movie_id, 'films', 'episode', $studio_id);
                        } else {
                            $poster = $this->getPoster($ppv_subscription->movie_id, 'films', 'standard', $studio_id);
                        }
                        $library[$key]['movie_name'] = $movie_names;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster'] = $poster;
                        $library[$key]['genres'] = json_decode($film->genre);
                    }
                }
                if ($details['transaction_type'] == 3) {//Pre Order                   
                    $datamovie_stream1 = Yii::app()->db->createCommand()
                            ->SELECT('ms.is_converted')
                            ->from('movie_streams ms ')
                            ->where('ms.is_episode=0 and ms.movie_id=' . $ppv_subscription->movie_id)
                            ->queryRow();
                    $is_converted = $datamovie_stream1['is_converted'];
                    if ($is_converted == 1) {
                        $library[$key]['movie_name'] = isset($ppv_subscription->movie_id) ? $this->getFilmName($ppv_subscription->movie_id) : "";
                        $library[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                        $library[$key]['poster'] = $this->getPoster($ppv_subscription->movie_id, 'films', 'episode', $studio_id);
                        $library[$key]['genres'] = json_decode($film->genre);
                    }
                }
                if ($details['transaction_type'] == 5) {//PPV Bundle
                    $ppvplanid = PpvSubscription::model()->findByAttributes(array('id' => $details['ppv_subscription_id']))->timeframe_id;
                    $ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id' => $ppvplanid))->validity_days;
                    $ppvbundlesmovieid = Yii::app()->db->createCommand("SELECT content_id FROM ppv_advance_content WHERE ppv_plan_id=" . $details['plan_id'])->queryAll();
                    ;

                    $expirydate = date('M d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
                    if ($currdatetime <= $expirytime) {
                        $statusbundles = 'Active';
                        foreach ($ppvbundlesmovieid as $keyb => $contentid) {
                            $movie_id = $contentid['content_id'];
                            $film = Film::model()->findByAttributes(array('id' => $movie_id));
                            if ($film->content_types_id == 2) {
                                $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            } else {
                                $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                            }

                            $pplibrary[$keyb]['movie_name'] = $film->name;
                            $pplibrary[$keyb]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            $pplibrary[$keyb]['poster'] = $poster;
                            $pplibrary[$keyb]['genres'] = json_decode($film->genre);
                        }
                    }
                }
                if ($details['transaction_type'] == 6) {//Voucher
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($ppv_subscription->end_date);
                    $movie_name = isset($ppv_subscription->movie_id) ? $this->getFilmName($ppv_subscription->movie_id) : "";
                    $episode_name = isset($ppv_subscription->episode_id) ? $this->getEpisodeName($ppv_subscription->episode_id) : "";
                    $statusvoucher = 'N';
                    if ($currdatetime <= $expirytime && $ppv_subscription->end_date != '0000-00-00 00:00:00' && strtotime($ppv_subscription->end_date) != "") {
                        $statusvoucher = 'A';
                    } else if ($ppv_subscription->end_date != '0000-00-00 00:00:00' && strtotime($ppv_subscription->end_date) != "") {
                        $statusvoucher = 'A';
                    } else if ($ppv_subscription->end_date == '0000-00-00 00:00:00') {
                        $statusvoucher = 'A';
                    }
                    if ($statusvoucher == 'A') {
                        $datamovie_stream = Yii::app()->db->createCommand()
                                ->SELECT('ms.embed_id')
                                ->from('movie_streams ms ')
                                ->where('ms.id=' . $ppv_subscription->episode_id)
                                ->queryRow();
                        $embed_id = $datamovie_stream['embed_id'];
                        $vmovie_names = $movie_name;

                        if ($ppv_subscription->movie_id != 0 && $ppv_subscription->season_id == 0 && $ppv_subscription->episode_id == 0) {
                            $vmovie_names = $movie_name;
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                        }
                        if ($ppv_subscription->movie_id != 0 && $ppv_subscription->season_id != 0 && $ppv_subscription->episode_id != 0) {
                            $vmovie_names = $movie_name . " -> " . $this->Language['season'] . " " . $ppv_subscription->season_id . " ->" . $episode_name;
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '/stream/' . $embed_id;
                        }
                        if ($ppv_subscription->movie_id != 0 && $ppv_subscription->season_id != 0 && $ppv_subscription->episode_id == 0) {
                            $vmovie_names = $movie_name . " -> " . $this->Language['season'] . " " . $ppv_subscription->season_id;
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $ppv_subscription->season_id;
                        }
                        if ($film->content_types_id == 2) {
                            $poster = $this->getPoster($ppv_subscription->movie_id, 'films', 'episode', $studio_id);
                        } else {
                            $poster = $this->getPoster($ppv_subscription->movie_id, 'films', 'standard', $studio_id);
                        }
                        $library[$key]['movie_name'] = $vmovie_names;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster'] = $poster;
                        $library[$key]['genres'] = json_decode($film->genre);
                    }
                }
            }
            /*
              //add the free contents to be list
              $free_contents = FreeContent::model()->findAllByAttributes(array('studio_id' => $studio_id));
              if($free_contents){
              foreach($free_contents as $key=>$frc){
              $movie_name = isset($frc['movie_id'])?$this->getFilmName($frc['movie_id']):"";
              $episode_name = isset($frc['episode_id'])?$this->getEpisodeName($frc['episode_id']):"";
              $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$frc['movie_id'])));
              $datamovie_stream = Yii::app()->db->createCommand()
              ->SELECT('ms.embed_id')
              ->from('movie_streams ms ')
              ->where('ms.id='.$frc['episode_id'])
              ->queryRow();
              $embed_id=$datamovie_stream['embed_id'];
              $vmovie_names=$movie_name;
              if($frc['movie_id']!=0 && $frc['season_id']==0 && $frc['episode_id']==0 ){
              $vmovie_names=$movie_name;
              $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
              }
              if($frc['movie_id']!=0 && $frc['season_id']!=0 && $frc['episode_id']!=0){
              $vmovie_names=$movie_name." -> Season #".$frc['season_id']." ->".$episode_name;
              $baseurl=Yii::app()->getBaseUrl(true).'/player/'.$film->permalink.'/stream/'.$embed_id;
              }
              if($frc['movie_id']!=0 && $frc['season_id']!=0 && $frc['episode_id']==0){
              $vmovie_names=$movie_name." -> Season #".$frc['season_id'];
              $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'?season='.$frc['season_id'];
              }

              if($frc['content_types_id'] == 2){
              $poster = $this->getPoster($frc['movie_id'], 'films', 'episode', $studio_id);
              }else{
              $poster = $this->getPoster($frc['movie_id'], 'films', 'standard', $studio_id);
              }
              $freecontent[$key]['movie_name'] = $vmovie_names;
              $freecontent[$key]['baseurl'] = $baseurl;
              $freecontent[$key]['poster'] = $poster;
              $freecontent[$key]['genres'] = json_decode($film->genre);
              }
              }
             */
            $this->render('mylibrary', array('library' => $library, 'pplibrary' => $pplibrary));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }

    public function actionSeasonShowConfirm() {
        $_SESSION['showconfirmuniqueid'] = '';
        echo 1;
    }

    public function actionLogoutall() {
        if ($_POST) {
            $studio_id = $_POST['studio_id'];
            $user_id = $_POST['user_id'];
            LoginHistory::model()->logoutUser($studio_id, $user_id);
            echo "success";
            exit;
        }
    }

    public function actionwatchHistory() {
		if (!Yii::app()->user->id) {
			Yii::app()->user->setFlash('error', "Please login to access this page.");
           $this->redirect(Yii::app()->getbaseUrl(true));exit;
		} 
		
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        $language_id = $this->language_id;
        $page_size = $limit = Yii::app()->general->itemsPerPage();
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $limit;
        } else if (isset($_REQUEST['p'])) {
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
        } else {
            $page_number = 1;
        }
		
        if (($this->watch_history != 0) && ($user_id > 0)) {
            $watchlist = VideoLogs::model()->getWatchHistoryList($user_id, $studio_id, $page_size, $offset, $this->watch_history);
            $item_count = $watchlist['total'];
            $list = $watchlist['list'];
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $page_url = Yii::app()->general->full_url($url, '?p=');
            $page_url = Yii::app()->general->full_url($page_url, '&p=');
            $pg = new bootPagination();
            $pg->pagenumber = $page_number;
            $pg->pagesize = $page_size;
            $pg->totalrecords = $item_count;
            $pg->showfirst = true;
            $pg->showlast = true;
            $pg->paginationcss = "pagination-normal";
            $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
            $pg->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1)
                $pg->paginationUrl = $page_url . "&p=[p]";
            else
                $pg->paginationUrl = $page_url . "?p=[p]";
            $pagination = $pg->process();
            $this->render('watchhistory', array('pagination' => $pagination, 'contents' => json_encode($list), 'orderby' => @$order, 'item_count' => @$item_count, 'page_size' => @$page_size, 'pages' => @$pages));
        }else {
            $redirect_url = Yii::app()->getBaseUrl(true);
            Yii::app()->user->setFlash('error', "You do not have access to this page.");
            $this->redirect($redirect_url);
            exit;
        }
    }

    public function actionmanageDevice() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        $devicelist = new Device();
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('device_management')
                ->where('studio_id =:studio_id AND user_id=:user_id AND flag!=9', array(':studio_id' => $studio_id, ':user_id' => $user_id))
                ->order('id DESC');
        $devicearray = $command->queryAll();
        $this->render('device', array('devicedata' => $devicearray));
    }

    public function actionDeleteDevice() {
        $id = (isset($_POST['id']) && $_POST['id'] > 0) ? $_POST['id'] : 0;
        $studio_id = Yii::app()->common->getStudioId();
        if ($id) {
            $response = array();
            $sql = "UPDATE `device_management` SET flag=1, deleted_date = '" . date('Y-m-d H:i:s') . "' WHERE id = :id";
            if (Yii::app()->db->createCommand($sql)->execute(array(':id' => $id))) {
                //Mail section to admin			
                /*if (NotificationSetting::model()->isEmailNotificationRemoveDevice($studio_id, 'device_management')) {
                    $user_data = Yii::app()->db->createCommand()
                            ->select('email,device,device_info')
                            ->from('sdk_users u')
                            ->join('device_management d', 'u.id=d.user_id')
                            ->where('d.id=:id', array(':id' => $id))
                            ->queryRow();
                    $mailcontent = array('studio_id' => $studio_id, 'device' => $user_data['device'], 'device_info' => $user_data['device_info'], 'email' => $user_data['email']);
                    Yii::app()->email->mailRemoveDevice($mailcontent);
                }*/
                }
            echo 1;
            exit;
        }
    }

    //get subscription bundles plan By sunilN
    function actionGetSubscriptionBundlesPlan() {
        $this->layout = false;
        $plan = '';
        $data = array();
        //For login or registered user
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
                $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
                $command = Yii::app()->db->createCommand()
                        ->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                        ->from('films f ')
                        ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
                $films = $command->queryRow();
                $movie_id = $films['id'];
                $content_types_id = $data['content_types_id'] = $films['content_types_id'];
                $data['is_ppv_bundle'] = $is_ppv_bundle = 1;
                $default_currency_id = $currency_id = $this->studio->default_currency_id;
                $plan = Yii::app()->common->getAllSubscriptionsBundle($movie_id, $content_types_id, $films['ppv_plan_id']);
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                $plans = '';
                if ($is_subscribed == '') {
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id, '', '', '', 1);
                        if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                            $gateways = $plan_payment_gateway['gateways'];
                            $plans = $plan_payment_gateway['plans'];
                        }
                } 
                $isCouponExists = Yii::app()->billing->isCouponExistsForSubscription($studio_id);
                $subscriptionbundles_id = $plan[0]->id;
                if (isset($plan[0]->id) && intval($plan[0]->id)) {
                    $timeframe_prices = Yii::app()->common->getAllSubscriptionBundlesPrices($subscriptionbundles_id, $default_currency_id, $studio_id);
                    $currency_id = (isset($timeframe_prices[0]['currency_id']) && trim($timeframe_prices[0]['currency_id'])) ? $timeframe_prices[0]['currency_id'] : $default_currency_id;
                    $currency = Currency::model()->findByPk($currency_id);
                }
                if (!intval($plan[0]->id) && !empty($plans)) {
                    $currency_id = (isset($plans[0]->currency_id) && trim($plans[0]->currency_id)) ? $plans[0]->currency_id : $default_currency_id;
                    $currency = Currency::model()->findByPk($currency_id);
                }
                $data['is_coupon_exists'] = self::isCouponExists();
                $condcard = array(':studio_id' => $studio_id, ':user_id' => $user_id);
                $cardsql = Yii::app()->db->createCommand()
                        ->select('c.id, c.gateway_id, c.card_name, c.card_holder_name, c.card_last_fourdigit, c.card_type, c.exp_month, c.exp_year')
                        ->from('sdk_card_infos c')
                        ->where('c.studio_id=:studio_id AND c.user_id=:user_id AND c.gateway_id !=1 AND c.gateway_id !=4', $condcard, array('order' => 'is_cancelled ASC, id DESC'), array('group' => 'exp_month, exp_year, card_holder_name, card_type, card_last_fourdigit'));
                $cards = $cardsql->queryAll();
                Yii::app()->theme = 'bootstrap';
                $this->render('subscriptionbundleplans', array('gateways' => @$gateways, 'gateway_code' => @$gateway_code, 'plan' => @$plan, 'plans' => @$plans, 'currency' => @$currency, 'data' => @$data, 'films' => @$films, 'cards' => @$cards, 'timeframe_prices' => @$timeframe_prices, 'is_ppv_bundle' => $is_ppv_bundle));
            }
        }
    }
    //show user subscription bundles listing  by sunilN     
     public function actionMyplans() {
        $lang_code = $this->language_code;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true).Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        if ($user_id > 0 && $studio_id > 0) {
            $isCouponExists = 0;
            $gateways = array();
            $plans = array();
                $user_id = Yii::app()->user->id;
            $default_currency_id = $this->studio->default_currency_id;
            $bundleplan_payment_gateway = Yii::app()->common->isPaymentGatwayAndBundlesPlanExists($studio_id,$user_id,$default_currency_id);
            if (isset($bundleplan_payment_gateway) && !empty($bundleplan_payment_gateway)) {
                            $gateways = $bundleplan_payment_gateway['gateways'];
                            $bundlesplans = $bundleplan_payment_gateway['plans'];
            }
            $bundleplan_payment_gateway_cancelled = Yii::app()->common->isPaymentGatwayAndBundlesPlanExistscancelled($studio_id,$user_id,$default_currency_id);
            if (isset($bundleplan_payment_gateway_cancelled) && !empty($bundleplan_payment_gateway_cancelled)) {
                            $gateways = $bundleplan_payment_gateway_cancelled['gateways'];
                            $bundlesplans_cancelled = $bundleplan_payment_gateway_cancelled['plans'];
                    }
            if(empty($bundleplan_payment_gateway) && empty($bundleplan_payment_gateway_cancelled) && !empty($plan_payment_gateway) ){
            $default_plan_id= $plans[0]['id'];
                }
            $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id); 
            $payment_form = Yii::app()->general->payment_form($gateways, $plans);
            $activate_btn_form = Yii::app()->general->activate_btn();
            $custom_fields = Yii::app()->general->getCustomFields($studio_id, 1);
        $cancel_btn_form = Yii::app()->general->cancel_btn();
            $this->render('planlists', array('gateways' => $gateways, 'plans' => $plans ,'bundlesplans'=> $bundlesplans,'bundlesplans_cancelled'=>$bundlesplans_cancelled ,'studio_id' => $studio_id, 'payment_form' => $payment_form,'activate_btn_form'=>$activate_btn_form,'custom_fields' => $custom_fields,'isCouponExists' => $isCouponExists, 'activate' => 1,'cancel_btn_form'=>$cancel_btn_form,'is_subscribed'=>$is_subscribed,'default_plan_id'=>$default_plan_id));
                    } else {
            $this->redirect(Yii::app()->user->returnUrl);
                    }
                    }
    
    //User playlist BY BD<biswajitdas@muvi.com>
   public function actionmyPlaylist() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        if (($user_id > 0)) {
			$playlist_name = Yii::app()->common->getAllPlaylistName($studio_id,$user_id,0);
			$total_playlist = count($playlist_name);
			$this->render('myplaylist', array('playlist_count'=>$total_playlist));
        } else {
            $redirect_url = Yii::app()->getBaseUrl(true);
            Yii::app()->user->setFlash('error', "You do not have access to this page.");
            $this->redirect($redirect_url);
            exit;
        }
    }

    public function actionallPlaylist() {
        $studio_id = Yii::app()->common->getStudioId();
        $language_id = $this->language_id;
        $page_size = $limit = Yii::app()->general->itemsPerPage();
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $limit;
        } else if (isset($_REQUEST['p'])) {
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
        } else {
            $page_number = 1;
        }
        if ((isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
            $user_id = (isset($_POST['user_id']) && $_POST['user_id'] != "") ? $_POST['user_id'] : 0;
			$offset = (isset($_POST['offset']) && $_POST['offset'] != "") ? $_POST['offset'] : 0;
			$limit = (isset($_POST['limit']) && $_POST['limit'] != "") ? $_POST['limit'] : 10;
            $playlist = Yii::app()->common->getAllplaylist($studio_id,$user_id,0,$offset,$limit);
			$prev_offset = $offset;
			$offset_ret =$offset+$limit;
            $this->renderpartial('lists', array('contents' => json_encode($playlist), 'offset' => $offset_ret, 'count' => $count,'prev_offset'=>$prev_offset));
        }
    }
    public function actionsetWatchPeriod(){
        $user_id = Yii::app()->user->id;
        if(isset($user_id) && intval($user_id)){
            $chek_ppv_voideo = PpvSubscription::model()->findByAttributes(array('user_id' =>$user_id,'studio_id'=>$_REQUEST['studio_id'],'movie_id'=>$_REQUEST['movie_id']));
           $end = strtotime($chek_ppv_voideo->end_date);
           if(($chek_ppv_voideo->end_date == '0000-00-00 00:00:00' || $chek_ppv_voideo->end_date == 'NULL' || $chek_ppv_voideo->end_date == '')){
                if(count($chek_ppv_voideo) > 0){
                    if($chek_ppv_voideo->access_period == '' && isset($chek_ppv_voideo->watch_period)){
                        $today = Date('Y-m-d H:i:s');
                        $time = strtotime($today);
                        $end_date = date("Y-m-d H:i:s", strtotime("+$chek_ppv_voideo->watch_period", $time));
                        $chek_ppv_voideo->end_date = $end_date;
                        $chek_ppv_voideo->update();
                    }
                }
           }
          exit;
        }
    }
    
    public function actionchkPemission() {
        if (isset($_POST['movie_id']) && $_POST['movie_id']) {
            $can_see_data['movie_id'] = $_POST['movie_id'];

            $can_see_data['stream_id'] = $_POST['stream_id'];
            $can_see_data['purchase_type'] = $_POST['purchase_type'];
            $can_see_data['studio_id'] = Yii::app()->common->getStudiosId();
            $can_see_data['user_id'] = Yii::app()->user->id;
            $can_see_data['api_perimission'] = 1;
            $can_see_data['title'] = $_POST['content_title'];
            $can_see_data['content_type'] = $_POST['content_type'];

            $getMovieID = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('films')
                    ->where('uniq_id=:uniq_id', array(':uniq_id' => $_POST['movie_id']))
                    ->queryRow();

            $can_see_data['movie_id'] = $getMovieID['id'];
            if ($_POST['stream_id']) {
                $getStreamID = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from('movie_streams')
                        ->where('embed_id=:embed_id', array(':embed_id' => $_POST['stream_id']))
                        ->queryRow();
                $can_see_data['movie_id'] = $getStreamID['id'];
            }
            $ret = $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);

            echo $ret;
            exit;
        }
    }

    public function actionSubscribe() {
        $mobile_number = (isset(Yii::app()->user->mobile_number) && Yii::app()->user->id > 0) ? Yii::app()->user->mobile_number : 0;
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        if ($mobile_number) {
            require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
            $chk = new clickHereApi();
            $uniqueID = $chk->getUniqueID();
            //$_REQUEST['data']['unique_id'] = $uniqueID;
            $message = $this->Language['subscribe_api_body_msg'];
            $getApiDetails = Yii::app()->db->createCommand()
                    ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                    ->from('external_api_keys e')
                    ->join('studio_api_details s', 'e.id=s.api_type_id')
                    ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module' => 'subscribe'))
                    ->queryRow();
            $chk = $chk->doPayment($mobile_number, $message, $uniqueID, $getApiDetails);
            if ($chk['code'] === 202) {
                SdkUser::model()->updateApiUniqueId($mobile_number, $studio_id, $user_id, $uniqueID);
            }
            echo json_encode($chk);
            exit;
        }
    }

    public function actionApicancelSubscription() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        $mobile_number = (isset(Yii::app()->user->mobile_number) && Yii::app()->user->id > 0) ? Yii::app()->user->mobile_number : 0;
        if ($user_id && $studio_id && $mobile_number) {
            require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
            $chk = new clickHereApi();
            $uniqueID = $chk->getUniqueID();
            $message = $this->Language['deactivate_api_body_msg'];
            $getApiDetails = Yii::app()->db->createCommand()
                    ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                    ->from('external_api_keys e')
                    ->join('studio_api_details s', 'e.id=s.api_type_id')
                    ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module' => 'deActivate'))
                    ->queryRow();
            if (!empty($getApiDetails)) {
                $chk = $chk->doDeactivate($mobile_number, $message, $uniqueID, $getApiDetails);
            }
            if ($chk['code'] === 202) {
                SdkUser::model()->updateApiUniqueId($mobile_number, $studio_id, $user_id, $uniqueID);
                Yii::app()->user->setFlash('success', $this->Language['account_deactivate']);
            } else {
                Yii::app()->user->setFlash('error', $this->Language['api_subscribe_failed']);
            }
        }
        $this->redirect(Yii::app()->getBaseUrl(true));

        exit;
    }    

    public function actionDownloadContent() {
        if (isset($_REQUEST['vlink']) && $_REQUEST['vlink']) {
            $permaLink = $_REQUEST['vlink'];
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You are trying to access an invalid content');
            $this->redirect(Yii::app()->getBaseUrl(TRUE));
            exit;
        }
        if ($permaLink) {
            $studio_id = $this->studio->id;
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.content_types_id,f.mapped_id')
                    ->from('films f ')
                    ->where('f.permalink=:permalink AND f.studio_id=:studio_id', array(':permalink' => $permaLink, ':studio_id' => $studio_id));
            $movie = $command->queryRow();
            if ($movie) {
                $stream_id = Yii::app()->common->getStreamId($movie['id']);
                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $fullmovie_path = $this->getFullVideoPath($stream_id, 0, $studio_id);
                $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 0, "", 36000, $studio_id);
                //$fullmovie_path = substr($fullmovie_path, 0, strpos($fullmovie_path, "?"));
                Yii::app()->usercontent->downloadVideo($fullmovie_path, $movie['name']);
            } else {
                $this->redirect(Yii::app()->baseUrl . '/user/login');
            }
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }
    
    public function actiongeneratOtp() {
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudioId();
        if (isset($_POST['LoginForm']) && $_POST['LoginForm']) {
            $mobile_number = $_POST['LoginForm']['mobile_number'];
            $email = $_POST['LoginForm']['email'];
            $password = $_POST['LoginForm']['password'];
            $otpEnable = $_POST['LoginForm']['optenable'];
            $otp = rand(1111, 9999);
            if ($otpEnable == 1) { 
                if ($_POST['LoginForm']['mobile_number']) {
                    $is_register = SdkUser::model()->findByAttributes(array('mobile_number' => $mobile_number, 'studio_id' => $studio_id), array('select' => 'id')); 
                    if (!empty($is_register)) {
                        $array = $this->sendOtpToMobile($otp, $mobile_number);
                    } else {
                        $array = array("result" => "failed", "msg" => $this->Language['mobilenumber_not_registered']);
                    }
                } else if ($_POST['LoginForm']['email']) {
                    $is_register = SdkUser::model()->findByAttributes(array('email' => $email, 'studio_id' => $studio_id), array('select' => 'id,display_name'));
                    if (!empty($is_register)) {
                        $array = $this->sendOtpToEmail($otp, $email, $is_register['display_name']);
                    } else {
                        $array = array("result" => "failed", "msg" => $this->Language['email_not_registered']);
                    }
                }
            } else if ($otpEnable == 2) {
                $enc = new bCrypt();
                if ($email && $password) {
                    $record = SdkUser::model()->findByAttributes(array('email' => $email, 'studio_id' => $studio_id), array('select' => 'id,display_name'));
                    if (!empty($record)) {
                        if (!$enc->verify($password, $record['encrypted_password'])) {
                            $array = array("result" => "failed", "msg" => $this->Language['incorrect_password']);
                        } else {
                            $array = $this->sendOtpToEmail($otp, $email, $record['display_name']);
                        }
                    } else {
                        $array = array("result" => "failed", "msg" => $this->Language['email_not_registered']);
                    }
                } else if ($mobile_number && $password) {
                    $record = SdkUser::model()->findByAttributes(array('mobile_number' => $mobile_number, 'studio_id' => $studio_id));
                    if (!empty($record)) {
                        if (!$enc->verify($password, $record['encrypted_password'])) {
                            $array = array("result" => "failed", "msg" => $this->Language['incorrect_password']);
                        } else {
                            $array = $this->sendOtpToMobile($otp, $mobile_number);
                        }
                    } else {
                        $array = array("result" => "failed", "msg" => $this->Language['mobilenumber_not_registered']);
                    }
                } 
            }
        }
        echo json_encode($array);
        exit;
    }

    private function sendOtpToMobile($otp, $mobile_number) {
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudioId();
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
        $chk = new clickHereApi();
        $uniqueID = $chk->getUniqueID();
        $message = $this->Language['otp_api_body_msg1'].' '.$otp.'. '.$this->Language['otp_api_body_msg2'];
        $getApiDetails = Yii::app()->db->createCommand()
                ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                ->from('external_api_keys e')
                ->join('studio_api_details s', 'e.id=s.api_type_id')
                ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module' => 'verify'))
                ->queryRow();
        if (!empty($getApiDetails)) {
            $chk = $chk->doVerify($mobile_number, $message, $uniqueID, $getApiDetails);
            if ($chk['code'] === 202) {
                SdkUser::model()->saveOtp($otp, $studio_id, $mobile_number, 1);
                $array = array("otp" => "success", "msg" => $this->Language['otp_success_mobile']);
            } else {
                $array = array("otp" => "failed", "msg" => $this->Language['otp_failed']);
            }
        }
        return $array;
        exit;
    }

    private function sendOtpToEmail($otp, $email,$name) {
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudioId();
        SdkUser::model()->saveOtp($otp, $studio_id, $email, 2);
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $studio_name = strtoupper($studio->name);
        $valid = Yii::app()->general->getDefaultExpiryTime();
        $params = array(
            'name' => $name,
            'otp' => $otp,
            'validUntil' => $valid,
            'studio_name' => $studio_name
        );
        $fromEmail = $studio->contact_us_email ? $studio->contact_us_email : Yii::app()->user->email;
        $to = $email;
        $from = $fromEmail;
        $subject = "OTP for Login";
        $fromName = $studio_name;
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/otp', array('params' => $params), true);
        $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', $fromName);
        $array = array("otp" => "success", "msg" => $this->Language['otp_success_email']);
        return $array;
        exit;
    }    
}
