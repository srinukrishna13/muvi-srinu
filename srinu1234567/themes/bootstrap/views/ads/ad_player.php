<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ad Player | <?php echo $this->studio->name; ?></title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />          

        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>               
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">        
        
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ads.js"></script>
        
        <?php if($ad_network_id == 1 || $ad_network_id == 2){?>
        <!-- Video Ad -->
        <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/es5-shim.js"></script>
        <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/ie8fix.js"></script>
        <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/swfobject.js"></script>
        <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/videojs-vast-vpaid.js"></script>
        <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/adplayer.js"></script>        

        <link rel="stylesheet" type="text/css" href="<?php echo $this->siteurl; ?>/muviplayer/styles/videojs.vast.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->siteurl; ?>/muviplayer/styles/videojs.vpaid.css"/>   
        <?php
        }
        else if($ad_network_id == 3){
        ?>
        <script type="text/javascript" src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ima.js"></script> 
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/contrib/videojs.ads.css" rel="stylesheet" type="text/css">        
        <?php }?>

    <body>
        <div class="wrapper">
            <div class="videocontent">
                <video id="video_block" class="video-js moo-css vjs-default-skin" controls autoplay preload="auto" autobuffer poster="">
                    <?php
                        echo '<source src="' . $fullmovie_path . '" type="video/mp4" />';
                    ?>
                </video>
            </div> 
        </div>

        <script type="text/javascript">
            var playerHeight = window.innerHeight-5;
            var playerWidth = window.innerWidth-5;    
            
            <?php if($ad_network_id == 1 || $ad_network_id == 2){?>
            <?php if($ad_network_id == 1){?>
                var ad_src = 'https://search.spotxchange.com/vast/2.00/<?php echo $channel_id;?>?VPAID=1&content_page_url=' + encodeURIComponent(window.location.href) + '&cb=' + Math.random() + '&player_width=' + playerWidth + '&player_height=' + playerHeight;
            <?php } else if($ad_network_id == 2){?>                
                <?php if($ad_type == 'vpaid'){?>
                ad_src = "http://shadow01.yumenetworks.com/yvp/21/xml/<?php echo $channel_id;?>/vpaid_html5.xml";
                <?php }else{?>
                ad_src = "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=<?php echo $channel_id;?>";
                <?php }?>
            <?php }?>            
            var playerPlugins = {
                "plugins": {
                    "ads-setup": {
                        "adCancelTimeout": 200000, // Wait for ten seconds before canceling the ad.
                        "adsEnabled": true,
                        adTagUrl: ad_src,
                        responseTimeout: 500000
                    }
                },
                responsive: true, 
                width: playerWidth, 
                height: playerHeight
            } 
            videojs('#video_block', playerPlugins, function () {        
            <?php }else if($ad_network_id == 3){?>    
            videojs('#video_block', { responsive: true, width: playerWidth, height: playerHeight }, function(){
            <?php }?>
                var player = this;
                <?php if($ad_network_id == 3){
                    $ad_url = urldecode("https://googleads.g.doubleclick.net/pagead/ads?sdkv=h.3.124.0&sdki=5&video_product_type=0&correlator=1093401004392578&client=ca-video-pub-4968145218643279&url=http%3A%2F%2Fdemo.muuvi.com%2Fads%2Fplay&adk=1537899732&num_ads=3&channel&output=xml_vast3&adtest=on&flash=21.0.0&sz=1361x318&adsafe=high&hl=en&ea=0&image_size=450x50%2C468x60%2C480x70%2C728x90&ad_type=video_image_text_flash&eid=265944522&u_tz=330&u_his=1&u_java=true&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_nplug=9&u_nmime=101&dt=1458539241928&unviewed_position_start=1&videoad_start_delay=0&osd=2&frm=0&sdr=1&video_format=43&t_pyv=allow&min_ad_duration=0&max_ad_duration=44000&ca_type=flash&description_url=http%3A%2F%2Fwww.youtube.com&mpt=videojs-ima&mpv=0.2.0&ref=http%3A%2F%2Fstudio.muuvi.com%2Fadmin%2Fvideoad&ged=ve4_pt1.u.1_td1_tt0_pd0_bs10_la4000_er0.0.154.300_vi0.0.323.1366_vp100_eb24171");
                    ?>
                var options = {
                    id: 'video_block',                
                    adTagUrl: "<?php echo $ad_url;?>"
                    //adTagUrl: "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator="
                    //adTagUrl: "https://googleads.g.doubleclick.net/pagead/ads?client=<?php echo $channel_id;?>&w="+playerWidth+"&h="+playerHeight+"&description_url="+encodeURIComponent(window.location.href)
                };            
                player.ima(options);
                player.ima.requestAds();  
                <?php }?>
            });          
        </script>
        <style type="text/css">
            .vjs-hidden{display: none !important;}
        </style>
    </body>
</html>