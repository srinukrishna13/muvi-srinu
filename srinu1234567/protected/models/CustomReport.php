<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SeoInfo
 *
 * @author SKM<support@muvi.com>
 */
class CustomReport extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'custom_report';
    }
    
    public function relations(){
        return array();
    }
     public function  getAvailableReport(){
        $studio_id = Yii::app()->common->getStudiosId();
        $r_sql = "SELECT * FROM `custom_report` where studio_id=$studio_id ORDER BY id DESC";
       (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
         return $result;
    }
   public function  getReportDetails($id){
        $studio_id = Yii::app()->common->getStudiosId();
        $r_sql = "SELECT cr.report_title,cr.report_type,cl.label_name,cl.label_code FROM `custom_report` as cr  left join custom_report_column as crl on crl.custom_report_id=cr.id LEFT JOIN report_template as r on r.id=crl.report_template_id LEFT JOIN custom_lable_column as cl on cl.id=r.custom_label_id where cr.id=$id and cr.studio_id=$studio_id  
ORDER BY `crl`.`position` ASC";
       (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
         return $result;
    }
    public function  getPartnerName($useridd){
        $studio_id = Yii::app()->common->getStudiosId();
         $r_sql = "SELECT first_name,id FROM `user` where id=$useridd";
       (array) $result = Yii::app()->db->createCommand($r_sql)->queryRow();
         return $result;
    }
    public function getReportDetailsPartner($userid){
    $sql="SELECT count(id) as countid FROM custom_report WHERE FIND_IN_SET($userid,partner_id) AND report_type=1 ";
    $partnersdata = Yii::app()->db->createCommand($sql)->queryAll(); 
    return $partnersdata;
    }
    public function getReportDetailsPartnercontent($userid){
    $sql="SELECT count(id) as countid FROM custom_report WHERE FIND_IN_SET($userid,partner_id) AND report_type=2 ";
    $partnersdata = Yii::app()->db->createCommand($sql)->queryAll(); 
    return $partnersdata;
    }
    
}
