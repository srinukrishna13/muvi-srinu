<?php
//echo '<pre>';
// print_r($packages);exit;
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
?>
<style>
.no-padding{padding:0px;}
</style>
<div class="row ">
    <div class="modal-header">
       <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close">&times;</button>-->
        <h4 id="myModalLabel" class="modal-title"> Purchase Subscription </h4>
    </div>   
    <div class="col-sm-12" id="packagediv">
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="javascript:void(0);">
                    <div class="col-sm-12 col-lg-12 col-xs-12">   
                        <div class="form-group clearfix">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Reseller level</label>
                            <div class="col-sm-5">
                                <div class="fg-line">
                                    <div class="select col-sm-12 no-padding">
                                         <select id="packages" class="form-control input-sm" onchange="getplan_price(this);">
                                             <?php
                                                foreach($packages as $key => $val){
                                                   echo '<option value="'.$val['id'].'">'.$val['name'].'</option>';
                                                 }
                                             ?>
                                         </select>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-sm-7">
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                         <div class="form-group clearfix">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Projected Pricing</label>
                            <input type="hidden" name="plane_price" id="plane_price" value="<?php echo $packages[0]['base_price'];?>">
                            <div class="col-sm-12" id="">
                                <div class="col-sm-12" id="plane_price_text">
                                    Charging to your card now: <b><?php echo Yii::app()->common->formatPrice($packages[0]['base_price'],$curency_id) ;?></b>
                                </div>
                              <!--  <div class="col-sm-12" id="monthly_pay_next">
                                    Next Billing date: <?php echo gmdate('F d, Y'); ?>
                                </div>-->
                            </div> 
                        </div>   
                          <div class="form-group clearfix">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Plan</label>
                            <div class="col-sm-4" id="plane_price_text">
                                <div class="col-sm-12"> 
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="plane" id="plane" value="Month" checked><i class="input-helper"></i>Monthly
                                            </label>
                                        </div>
                                </div>
                            </div> 
                            <div class="col-sm-8 grey">
                                    Pay month by month, cancel anytime
                             </div>
                        </div>   
                        
                           <div class="form-group clearfix">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Payment Method</label>
                            <div class="col-sm-4" id="plane_price_text">
                                <div class="col-sm-12"> 
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="payment_method" id="pay_by_creaditcard" value="creaditcard" checked><i class="input-helper"></i>Credit card
                                            </label>
                                        </div>
                                       <!-- <div class="radio">
                                            <label>
                                                <input type="radio" name="payment_method" id="pay_by_wire_transfer" value="wiretransfer" ><i class="input-helper"></i>Wire Transfer
                                            </label>
                                        </div> -->
                                        <div class="radio" style="display:none;">
                                            <label>
                                                <input type="radio" name="payment_method" id="pay_by_paypal" value="paypal" ><i class="input-helper"></i>PayPal
                                            </label>
                                        </div>                                     
                                </div>
                            </div> 
                            <div class="col-sm-8 grey">
                                    Select one of your preferred payment method, we will proceed with that
                             </div>
                        </div>             
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="paymentMethod" class="form-horizontal" method="post" name="paymentMethod"  action="javascript:void(0);" autocomplete="false">
        <div class="col-sm-12" id="payment_div" style="display:none;">
            <div class="row">
                <div class="col-sm-12">  
                    <div class="charging_details">
                            <label class="col-sm-12">Projected Pricing</label>
                            <div class="col-sm-12">
                                Charging to your card now:
                                <label class="control-label"> $ <span class="charge_now"></span> </label>
                            </div>
                             <div class="col-sm-12">
                                Monthly payment: 
                                <label class="control-label"> $ <span class="charge_now"></span> </label>
                            </div>  
                              <div class="col-sm-12">
                                Next billing date:
                                <label class="control-label"><span class="next_billing_date"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></span> </label>
                            </div>                            
                    </div>
                    <div class="clearfix"></div>
                    <!-- card info div start --->
                            <div id="new_card_info" class="  m-b-10">
                                <div id="card-info-error" class="error red" style="display: none;"></div>
                                <div class="col-sm-6">

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Name on Card:</label>                    
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" autocomplete="off"/>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Card Number:</label>                    
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Expiry Date:</label>                    
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select name="exp_month" id="exp_month" class="form-control input-sm">
                                                    <option value="">Expiry Month</option>	
                                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select name="exp_year" id="exp_year" class="form-control input-sm">
                                                    <option value="">Expiry Year</option>
                                                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-sm-4">Security Code:</label>                    
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="password" id="" name="" style="display: none;" autocomplete="off"/>
                                            <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Billing Address:</label>                    
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>                    
                        <div id="terms-div" class="m-t-10" style="">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                                <input name="terms_of_use" id="terms_of_use" autocomplete="false" aria-required="true" class="error" type="checkbox"><i class="input-helper"></i>                                  
                                                I agree with the <a href="https://www.muvi.com/partneragreement/reseller" target="_blank">terms of use</a> of Muvi
                                            </label>
                                            <div><label id="terms_of_use-error" class="error red" for="terms_of_use" style=""></label></div>
                                        </div>
                                    </div>                     
                                </div>
                            </div>
                    <!-- card info div end -->
                </div>
            </div>
        </div>
        <input type="hidden" name="payment_price" id="payment_price">
        <input type="hidden" name="payment_plan" id="payment_plan">
        <input type="hidden" name="payment_packeg" id="payment_packeg">
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="nextbutn" onclick="showCards();">Continue to Authorize Payment</button>
            <button type="submit" class="btn btn-primary" id="nextbtn" style="display:none;" onclick="return validateForm();">Purchase Subscription</button>
            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
    </form>
    <!-- loading popup -->
    <div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header" style="border: none;">
                   <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                   <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
               </div>
           </div>
       </div>
   </div> 
   
      <!-- loading wire popup -->
    <div id="loadingPopup_wire" class="modal fade" data-backdrop="static" data-keyboard="false">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header" style="border: none;">
                   <div class="modal-title auth-msg">Just a moment... we're Processing your transaction.<br/>Please don't refresh or close this page.</div>
                   <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
               </div>
           </div>
       </div>
   </div>   
    <!-- success Popup-- -->
    <div id="successPopup" class="modal fade in" data-backdrop="static" data-keyboard="false" style="display: none;">
       <div class="modal-dialog">
           <div class="modal-content" style="overflow: hidden;">
               <div class="modal-header">
                   <h4 class="modal-title" style="color: #42B970">Thank you, Your subscription has been activated successfully.<br>Please wait we are redirecting you...</h4>
               </div>
           </div>
       </div>
   </div>   
</div>

<script type="text/javascript">
  function getplan_price(val){
        var id = val.value;
        $.post('/PartnerPayment/GetPlanPrice',{'id':id},function(res){ 
            var data = $.parseJSON(res);
            $("#plane_price_text").html('Charging to your card now: <b>'+data.price_format+'</b>');
            $("#plane_price").val(data.price);
        });
  } 
  function showCards(){
      var payment_method = $('input[name="payment_method"]:checked').val();
      var packegs = $("#packages").val();
      var price = $("#plane_price").val();
      var plane = $("#plane").val();
      if(payment_method == 'creaditcard'){
            $("#packagediv").hide('fast');
            $("#nextbutn").hide('fast');
            $("#nextbtn").fadeIn('slow');
            $("#payment_div").fadeIn('slow');
            $("#payment_price").val(price);
            $("#payment_plan").val(plane);
            $("#payment_packeg").val(packegs);
            $(".charge_now").html(price);
        }
      if(payment_method == 'wiretransfer'){
          var confm = confirm("Are you sure you want to pay by wire transfer ?");
          if(confm){
                $("#loadingPopup_wire").modal('show');
               var url = "<?php echo Yii::app()->baseUrl; ?>/partnerPayment/doWireTransfer"; 
               $.post(url, {'packages': packegs, 'pricing': price, 'plan': plane}, function (data) {
                   //alert(data);
                   if(data == 1){
                  $("#loadingPopup_wire").modal('hide');     
                  $("#successPopup").modal('show');
                  window.location="<?php echo Yii::app()->baseUrl; ?>/partnerPayment/paymentHistory";
                   }
               });
           }
      }
  }
</script>
<script type="text/javascript">
 function validateForm() {
    //$('#card-info-error').hide();

    //form validation rules
    var validate = $("#paymentMethod").validate({
        rules: {
            payment_info: "required",
            card_name: "required",
            exp_month: "required",
            exp_year: "required",
            security_code: "required",
            address1: "required",
            city: "required",
            state: "required",
            zipcode: "required",
            terms_of_use: "required",
            card_number: {
                required: true,
                number: true
            }
        },
        messages: {
            payment_info: "Please select payment information",
            card_name: "Please enter a valid name",
            exp_month: "Please select the expiry month",
            exp_year: "Please select the expiry year",
            security_code: "Please enter your security code",
            address1: "Please enter your address",
            city: "Please enter your city",
            state: "Please enter your State",
            zipcode: "Please enter your Zipcode",
            terms_of_use: "Please select terms of use",
            card_number: {
                required: "Please enter a valid card number",
                number: "Please enter a valid card number"
            }
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            switch (element.attr("name")) {
                case 'exp_month':
                    error.insertAfter(element.parent().parent());
                    break;
                case 'exp_year':
                    error.insertAfter(element.parent().parent());
                    break;
                default:
                    error.insertAfter(element.parent());
            }
        }
    });  
    var is_valid = validate.form();
    if(is_valid){
         $('#nextbtn').html('wait!...');  
         $('#nextbtn').attr('disabled', 'disabled');
         $("#loadingPopup").modal('show');
         var url = "<?php echo Yii::app()->baseUrl; ?>/partnerPayment/authenticateCard";
         var card_name = $('#card_name').val();
         var card_number = $('#card_number').val();
         var exp_month = $('#exp_month').val();
         var exp_year = $('#exp_year').val();
         var cvv = $('#security_code').val();
         var address1 = $('#address1').val();
         var address2 = $('#address2').val();
         var city = $('#city').val();
         var state = $('#state').val();
         var zip = $('#zipcode').val();
         var pricing = $("#payment_price").val();
         var packages = $("#payment_packeg").val(); 
         var plan = $("#payment_plan").val(); 
         $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'packages': packages, 'pricing': pricing, 'plan': plan}, function (data) {
                $("#loadingPopup").modal('hide');
                if (parseInt(data.isSuccess) === 1) {
                    $("#successPopup").modal('show');
                    setTimeout(function () {
                        $('#is_submit').val(1);
                         window.location="<?php echo Yii::app()->baseUrl; ?>/partnerPayment/paymentHistory";
                        return false;
                    }, 5000);
                } else {
                    $('.close').show();
                    $('#nextbtn').html('Purchase Subscription');
                    $('#nextbtn').removeAttr('disabled');
                    $('#is_submit').val('');
                    if ($.trim(data.Message)) {
                        $('#card-info-error').show().html(data.Message);
                    } else {
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
                }
            }, 'json');
     }
 }
</script>