<style>
  .layout-Centered{
    margin: 125px auto;
    width: 400px;
    border: 1px solid #e4e2e2;
    padding: 15px;
    border-radius: 4px;
  }
  .navbar-brand{
      padding: 10px 17px !important;
  }
</style>

    <!--Main Container-->
    <div class="container-fluid">
        <!--Content Section-->    
        <div class="layout-Centered">
            <h4 class="text-capitalize f-300">Select Store to Login</h4>
            <hr>
            <form class="form-horizontal">
                <div class="form-group" style="margin-bottom:5px;">
                    <label for="inputEmail3" class="col-sm-2 control-label">Store</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                          <div class="select">
                            <select class="form-control" name="loginstudio" id="loginstudio">
                     <?php
                     foreach($data as $val){
                     ?>
                                <option value="<?php echo $val['studio_id']; ?>"><?php echo $val['studio_name']; ?></option>
                     <?php
                     }
                     ?>
                            </select>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-2 text-right">
                        <button type="button" class="btn btn-primary btn-default m-t-10" onclick="javascript:studioLogin();">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>





<div id="studio_login" style="display: none;">
    <form id="studioLoginForm" name="studioLoginForm" method="post" action="<?php echo Yii::app()->baseUrl; ?>/login/studioLogin" target="StudioWindow">
        <input type="hidden" id="studio_id" name="studio_id" value="" />
        <input type="hidden" id="studio_name" name="studio_name" value="" />
        <input type="hidden" id="studio_email" name="studio_email" value="" />
    </form>
</div>

<script type="text/javascript">
    function studioLogin() { 
        var studio_id = $('#loginstudio').val();        
        var studio_email = '<?php echo $_SESSION['login_attempt_email_id'];?>';
        var url = "<?php echo Yii::app()->baseUrl; ?>/multistore/getData";
        $.post(url, {'studio_id': studio_id}, function (res) {
            if(res.studio_name){
                var chkmaster = "";
                var studio_name = res.studio_name;
                //if(chkmaster==1){
                    var confmsg = "Are you sure you want to login to studio '" + studio_name + "'?";
                /*}else{
                    var confmsg = "Are you sure you want to login to studio '" + studio_name + "'?\nYou will be logout from partner portal.";
                }*/
                if (confirm(confmsg)) {            
                    $("#studio_login").show();
                    $("#studio_id").val(studio_id);
                    $("#studio_name").val(studio_name);
                    $("#studio_email").val(studio_email);
                    $("#multistore_email").val('<?php echo $_SESSION['login_attempt_email_id'];?>');
                    
                    window.open('', 'StudioWindow');
                    document.getElementById('studioLoginForm').submit();
                    $("#studio_login").hide();
                }
            }
            
        }, 'json');
        

    }
</script>
