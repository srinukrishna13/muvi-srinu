/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $("#review").validate({ 
        rules: {                
            rate: {
                required: true,
            },            
        },         
        submitHandler: function(form) {   
            if($('#rating_value').val() > 0){
                $.ajax({
                    url: HTTP_ROOT+"/content/savereview",
                    data: $("#review").serialize(),
                    dataType: 'json',
                    method: 'post',
                    success: function (result) {
                        if(result.status == 'success')
                        {
                            window.location.reload();
                        }
                        else
                        {
                            $('#review_error').html(result.message);
                            return false;                    
                        }
                    }
                });   
            }else{
                $('#review_error').html('Please rate the content.');
                return false;   
            }
        }                
    });
    
    $('.rating').rating();
    $('.rating').on('change', function () {
        $('#review_error').html('');
        $(this).next('.label').text($(this).val());
    });  
});    

