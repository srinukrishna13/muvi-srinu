<?php
class paypal_pro
{
    public $API_USERNAME;
    public $API_PASSWORD;
    public $API_SIGNATURE;
    public $API_ENDPOINT;
    public $USE_PROXY;
    public $PROXY_HOST;
    public $PROXY_PORT;
    public $PAYPAL_URL;
    public $VERSION;
    public $NVP_HEADER;

    function __construct($API_USERNAME, $API_PASSWORD, $API_SIGNATURE, $PROXY_HOST, $PROXY_PORT, $IS_ONLINE = TRUE, $USE_PROXY = FALSE, $VERSION = '57.0')
    {
        $this->API_USERNAME = $API_USERNAME;
        $this->API_PASSWORD = $API_PASSWORD;
        $this->API_SIGNATURE = $API_SIGNATURE;
        $this->USE_PROXY = $USE_PROXY;
        $this->API_ENDPOINT = 'https://api-3t.paypal.com/nvp';
        if($this->USE_PROXY == true)
        {
            $this->PROXY_HOST = $PROXY_HOST;
            $this->PROXY_PORT = $PROXY_PORT;
        }
        else
        {
            $this->PROXY_HOST = '127.0.0.1';
            $this->PROXY_PORT = '808';
        }
        if($IS_ONLINE == FALSE)
        {
            $this->API_ENDPOINT = 'https://api-3t.sandbox.paypal.com/nvp';
            $this->PAYPAL_URL = 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=';
        }
        else
        {
            $this->API_ENDPOINT = 'https://api-3t.paypal.com/nvp';
            $this->PAYPAL_URL = 'https://www.paypal.com/webscr?cmd=_express-checkout&useraction=commit&token=';
        }
        $this->VERSION = $VERSION;
    }

    function hash_call($methodName,$nvpStr)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->API_ENDPOINT);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        if($this->USE_PROXY)
        {
            curl_setopt ($ch, CURLOPT_PROXY, $this->PROXY_HOST.":".$this->PROXY_PORT); 
        }
        $nvpreq="METHOD=".urlencode($methodName)."&VERSION=".urlencode($this->VERSION)."&PWD=".urlencode($this->API_PASSWORD)."&USER=".urlencode($this->API_USERNAME)."&SIGNATURE=".urlencode($this->API_SIGNATURE).$nvpStr;
        curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);
        $response = curl_exec($ch);
        $nvpResArray=$this->deformatNVP($response);
        $nvpReqArray=$this->deformatNVP($nvpreq);
        $_SESSION['nvpReqArray']=$nvpReqArray;
        if (curl_errno($ch))
        {
            die("CURL send a error during perform operation: ".curl_errno($ch));
        } 
        else 
        {
            curl_close($ch);
        }

        return $nvpResArray;
    }

    function deformatNVP($nvpstr)
    {

        $intial=0;
        $nvpArray = array();
        while(strlen($nvpstr))
        {
            $keypos= strpos($nvpstr,'='); 
            $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr); 
            $keyval=substr($nvpstr,$intial,$keypos);
            $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
            $nvpArray[urldecode($keyval)] =urldecode( $valval);
            $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
         }
        return $nvpArray;
    }

    function __destruct() 
    {

    }
    
    function authorizeCard($cardDetails)
    {
        $nvpstr = '';
        $methodToCall = 'doDirectPayment';
        $nvpstr .= '&PAYMENTACTION='.$cardDetails['paymentAction'];
        $nvpstr .= '&AMT='.$cardDetails['amount'];
        $nvpstr .= '&CURRENCYCODE='.$cardDetails['currency_code'];
        $nvpstr .= '&ACCT='.$cardDetails['creditCardNumber'];
        $nvpstr .= '&EXPDATE='.$cardDetails['expDate'];
        $nvpstr .= '&CVV2='.$cardDetails['cvv'];
        $nvpstr .= '&TRXTYPE='.$cardDetails['trxType'];
        $nvpstr .= '&FIRSTNAME='.$cardDetails['cardName'];
        $nvpstr .= '&EMAIL='.$cardDetails['cardEmail'];
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function doPayment($cardDetails)
    {
        $nvpstr = '';
        $methodToCall = 'CreateRecurringPaymentsProfile';
        $trialStr = '&TRIALBILLINGPERIOD='.$cardDetails["trialBillingPeriod"].'&TRIALBILLINGFREQUENCY='.$cardDetails["trialBillingFreq"].'&TRIALAMT='.$cardDetails["trialAmount"].'&TRIALTOTALBILLINGCYCLES='.$cardDetails["trialTotalBillingCycles"];
        $nvpRecurring ='&BILLINGPERIOD='.$cardDetails["billingPeriod"].'&BILLINGFREQUENCY='.$cardDetails["billingFreq"].'&PROFILESTARTDATE='.$cardDetails["profileStartDate"].'&INITAMT='.$cardDetails["initAmt"].'&FAILEDINITAMTACTION='.$cardDetails["failedInitAmtAction"].'&DESC='.$cardDetails["desc"].'&AUTOBILLAMT='.$cardDetails["autoBillAmt"].'&PROFILEREFERENCE='.$cardDetails["profileReference"].$trialStr;
        $nvpstr .= '&PAYMENTACTION='.$cardDetails['paymentAction'];
        $nvpstr .= '&AMT='.$cardDetails['amount'];
        $nvpstr .= '&CURRENCYCODE='.$cardDetails['currency_code'];
        $nvpstr .= '&ACCT='.$cardDetails['creditCardNumber'];
        $nvpstr .= '&EXPDATE='.$cardDetails['expDate'];
        $nvpstr .= '&CVV2='.$cardDetails['cvv'];
        $nvpstr .= '&FIRSTNAME='.$cardDetails['cardName'];
        $nvpstr .= '&EMAIL='.$cardDetails['cardEmail'];
        $nvpstr .= $nvpRecurring;
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function paymentsProfileDetails($profile_id)
    {
        $nvpstr = '';
        $methodToCall = 'GetRecurringPaymentsProfileDetails';
        $nvpstr .= '&PAYMENTACTION=Sale';
        $nvpstr .= '&PROFILEID='.$profile_id;
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function manageProfileStatus($profile_id,$action,$note='')
    {
        $nvpstr = '';
        $methodToCall = 'ManageRecurringPaymentsProfileStatus';
        $nvpstr .= '&PAYMENTACTION=Sale';
        $nvpstr .= '&PROFILEID='.$profile_id;
        $nvpstr .= '&ACTION='.$action;
        $nvpstr .= '&NOTE='.$note;
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function doSinglePayment($data)
    {
        $nvpstr = '';
        $methodToCall = 'doDirectPayment';
        $nvpstr .= '&PAYMENTACTION='.$data['paymentAction'];
        $nvpstr .= '&AMT='.$data['amount'];
        $nvpstr .= '&ACCT='.$data['creditCardNumber'];
        $nvpstr .= '&EXPDATE='.$data['expDate'];
        $nvpstr .= '&CVV2='.$data['cvv'];
        $nvpstr .= '&COUNTRYCODE=US&CURRENCYCODE='.$data['currencyCode'];
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function CallShortcutExpressCheckout($arg = array()) 
    {
        $landing = (isset($arg['landing']) && !intval($arg['landing']))?$arg['landing']:1;
        $shipping = (isset($arg['shipping']) && !intval($arg['shipping']))?$arg['shipping']:1;
        $nvpstr = '';
        $nvpstr = $nvpstr . "&AMT=". $arg['paymentAmount'];
        $nvpstr = $nvpstr . "&NOSHIPPING=" . $shipping;
        $nvpstr = $nvpstr . "&CURRENCYCODE=".$arg['currency_code'];
        $nvpstr = $nvpstr . "&PAYMENTACTION=" . $arg['paymentType'];
        $nvpstr = $nvpstr . "&BILLINGAGREEMENTDESCRIPTION=".urlencode($arg['paymentDesc']);
        $nvpstr = $nvpstr . "&BILLINGTYPE=".urlencode($arg['billingType']);
        $nvpstr = $nvpstr . "&RETURNURL=" . $arg['returnURL'];
        $nvpstr = $nvpstr . "&CANCELURL=" . $arg['cancelURL'];
        $nvpstr = $nvpstr . "&EMAIL=" . $arg['email'];
        if($landing == 0){
            $nvpstr = $nvpstr . "&LANDINGPAGE=Billing";
            $nvpstr = $nvpstr . "&SOLUTIONTYPE=Sole";
            $address = $arg['address'];        
            $name = isset($address['name'])?$address['name']:'';
            $street = isset($address['street'])?$address['street']:'';
            $city = isset($address['city'])?$address['city']:'';
            $state = isset($address['state'])?$address['state']:'';
            $country = isset($address['country'])?$address['country']:'';
            $zip = isset($address['zip'])?$address['zip']:'';
            $nvpstr = $nvpstr . "&SHIPTONAME=".  urldecode($name);
            $nvpstr = $nvpstr . "&SHIPTOSTREET=".  urldecode($street);
            $nvpstr = $nvpstr . "&SHIPTOCITY=".  urldecode($city);
            $nvpstr = $nvpstr . "&SHIPTOSTATE=".  urldecode($state);
            $nvpstr = $nvpstr . "&SHIPTOCOUNTRY=".  urldecode($country);
            $nvpstr = $nvpstr . "&SHIPTOZIP=".  urldecode($zip);
        }else{
            $nvpstr = $nvpstr . "&LANDINGPAGE=Login";
        }
        $nvpstr .= "&CUSTOM=21";
        $_SESSION["currencyCodeType"] = $arg['currencyCodeType'];	  
        $_SESSION["PaymentType"] = $arg['paymentType'];
        //echo $nvpstr;exit;
        $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING"){
            $token = urldecode($resArray["TOKEN"]);
            $_SESSION['TOKEN']=$token;
        }
        return $resArray;
    } 
    function CallShortcutExpressCheckoutPCI($arg = array()) 
    {
        $landing =0;// (isset($arg['landing']) && !intval($arg['landing']))?$arg['landing']:1;
        //$shipping = (isset($arg['shipping']) && !intval($arg['shipping']))?$arg['shipping']:1;
        $nvpstr = '';
        $nvpstr = $nvpstr . "&AMT=". $arg['paymentAmount'];
        $nvpstr = $nvpstr . "&NOSHIPPING=1";
        $nvpstr = $nvpstr . "&LOCALCODE=US";
        $nvpstr = $nvpstr . "&CURRENCYCODE=".$arg['currency_code'];
        $nvpstr = $nvpstr . "&PAYMENTACTION=" . $arg['paymentType'];
        $nvpstr = $nvpstr . "&BILLINGAGREEMENTDESCRIPTION=".urlencode($arg['paymentDesc']);
        $nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_DESC0=" . $arg['paymentDesc'];
        $nvpstr = $nvpstr . "&BILLINGTYPE=".urlencode($arg['billingType']);
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=Sale";
        $nvpstr = $nvpstr . "&RETURNURL=" . $arg['returnURL'];
        $nvpstr = $nvpstr . "&CANCELURL=" . $arg['cancelURL'];
        $nvpstr = $nvpstr . "&EMAIL=" . $arg['email'];
        if($landing == 0){
            $nvpstr = $nvpstr . "&LANDINGPAGE=Billing";
            $nvpstr = $nvpstr . "&SOLUTIONTYPE=Sole";
            $address = $arg['address'];        
            $name = isset($address['name'])?$address['name']:'';
            $street = isset($address['street'])?$address['street']:'';
            $city = isset($address['city'])?$address['city']:'';
            $state = isset($address['state'])?$address['state']:'';
            $country = isset($address['country'])?$address['country']:'';
            $zip = isset($address['zip'])?$address['zip']:'';
            /*$nvpstr = $nvpstr . "&SHIPTONAME=".  urldecode($name);
            $nvpstr = $nvpstr . "&SHIPTOSTREET=".  urldecode($street);
            $nvpstr = $nvpstr . "&SHIPTOCITY=".  urldecode($city);
            $nvpstr = $nvpstr . "&SHIPTOSTATE=".  urldecode($state);
            $nvpstr = $nvpstr . "&SHIPTOCOUNTRY=".  urldecode($country);
            $nvpstr = $nvpstr . "&SHIPTOZIP=".  urldecode($zip);*/
        }else{
            $nvpstr = $nvpstr . "&LANDINGPAGE=Login";
        }
        $nvpstr .= "&CUSTOM=21";
        $_SESSION["currencyCodeType"] = $arg['currencyCodeType'];	  
        $_SESSION["PaymentType"] = $arg['paymentType'];
        //echo $nvpstr;exit;*/
        
        $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING"){
            $token = urldecode($resArray["TOKEN"]);
            $_SESSION['TOKEN']=$token;
        }
        return $resArray;
    } 
    
    function RedirectToPayPal ($token)
    {	
        $PAYPAL_URL = $this->PAYPAL_URL;
        $payPalURL = $PAYPAL_URL . $token;
        header("Location: ".$payPalURL);exit;
    }
    
    function GetShippingDetails( $token )
    {
        $nvpstr="&TOKEN=" . $token;
        $resArray = $this->hash_call("GetExpressCheckoutDetails", $nvpstr);
        return $resArray;
    }
    
    function CreateRecurringPaymentsProfile($token,$payerID ,$email, $plan_desc, $period, $frequency, $amount, $currency, $trialperiod = '', $trialfrequency = '', $trialtotalcysles = '', $trial_amount = '')
    {
        $nvpstr="&TOKEN=".$token;
        $nvpstr.="&PAYERID=".$payerID;
        $nvpstr.="&EMAIL=".$email;
        $nvpstr.="&PROFILESTARTDATE=".gmdate('Y-m-d\TH:i:s\Z');
        $nvpstr.="&DESC=".urlencode($plan_desc);
        $nvpstr.="&BILLINGPERIOD=".$period;
        $nvpstr.="&BILLINGFREQUENCY=".$frequency;
        $nvpstr.="&AUTOBILLOUTAMT=AddToNextBilling";
        $nvpstr.="&AMT=".$amount;
        if($trialperiod != '')
        {
            $nvpstr.="&TRIALBILLINGPERIOD=".$trialperiod;
            $nvpstr.="&TRIALBILLINGFREQUENCY=".$trialfrequency;
            $nvpstr.="&TRIALTOTALBILLINGCYCLES=".$trialtotalcysles;
            $nvpstr.="&TRIALAMT=".$trial_amount;  
        }
        $nvpstr.="&CURRENCYCODE=".$currency;
        $nvpstr.="&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];
        $resArray = $this->hash_call("CreateRecurringPaymentsProfile", $nvpstr);
        return $resArray;
    }
    
    function UpdateCardDetails($cardDetails = array())
    {
        $nvpstr = '';
        $methodToCall = 'UpdateRecurringPaymentsProfile';
        $nvpstr .= '&PAYMENTACTION='.$cardDetails['paymentAction'];
        $nvpstr .= '&FIRSTNAME='.$cardDetails['cardName'];
        $nvpstr .= '&FIRSTNAME=""';
        $nvpstr .= '&PROFILEID='.$cardDetails['profileID'];
        $nvpstr .= '&ACCT='.$cardDetails['creditCardNumber'];
        $nvpstr .= '&CVV2='.$cardDetails['cvv'];
        $nvpstr .= '&EXPDATE='.$cardDetails['expDate'];
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function billOutstandingAmount($profile_id)
    {
        $nvpstr = '';
        $methodToCall = 'BillOutstandingAmount';
        $nvpstr .= '&PROFILEID='.$profile_id;
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function updateOutstandingBillAmount($profile_id,$amt)
    {
        $nvpstr = '';
        $methodToCall = 'UpdateRecurringPaymentsProfile';
        $nvpstr .= '&PROFILEID='.$profile_id;
        $nvpstr .= '&OUTSTANDINGAMT='.$amt;
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function updateAutoBillOutAmt($profile_id)
    {
        $nvpstr = '';
        $methodToCall = 'UpdateRecurringPaymentsProfile';
        $nvpstr .= '&PROFILEID='.$profile_id;
        $nvpstr .= '&AUTOBILLOUTAMT=NoAutoBill';
        $resArray = $this->hash_call($methodToCall,$nvpstr);
        return $resArray;
    }
    
    function GetExpressCheckoutDetails($token){ 
        $nvpstr='&TOKEN='.$token; 
        $resArray=$this->hash_call("GetExpressCheckoutDetails",$nvpstr); 
        return $resArray; 
    }
    
    function DoExpressCheckoutPayment($paymentInfo=array()){ 
        
        $paymentType = 'SALE'; 
        $serverName = $_SERVER['SERVER_NAME']; 
        $nvpstr='&TOKEN='.$paymentInfo['TOKEN'].'&PAYERID='.$paymentInfo['PAYERID'].'&PAYMENTACTION='.$paymentType.'&AMT='.$paymentInfo['AMT'].'&CURRENCYCODE='.$paymentInfo['CURRENCYCODE'].'&IPADDRESS='.$serverName; 
        $resArray=$this->hash_call("DoExpressCheckoutPayment",$nvpstr);
        return $resArray; 
    }
    
    function getPCICardTransactionDetails($txn_id) {
        $nvpstr = '';
        $nvpstr .= '&METHOD=GetTransactionDetails';
        $nvpstr .= '&TRANSACTIONID='.$txn_id;
        $resArray=$this->hash_call("GetTransactionDetails",$nvpstr); 
        return $resArray;
    }
    
    
    function CallShortcutExpressCheckoutRefTransaction($arg = array()) 
    {
        $shipping = (isset($arg['shipping']) && !intval($arg['shipping']))?$arg['shipping']:1;
        $nvpstr = '';
        $nvpstr = $nvpstr . "&AMT=". $arg['paymentAmount'];
        $nvpstr = $nvpstr . "&NOSHIPPING=" . $shipping;
        $nvpstr = $nvpstr . "&CURRENCYCODE=".$arg['currency_code'];
        $nvpstr = $nvpstr . "&PAYMENTACTION=" . $arg['paymentType'];
        $nvpstr = $nvpstr . "&BILLINGAGREEMENTDESCRIPTION=".urlencode($arg['paymentDesc']);
        $nvpstr = $nvpstr . "&BILLINGTYPE=".urlencode($arg['billingType']);
        $nvpstr = $nvpstr . "&RETURNURL=" . $arg['returnURL'];
        $nvpstr = $nvpstr . "&CANCELURL=" . $arg['cancelURL'];
        $nvpstr = $nvpstr . "&EMAIL=" . $arg['email'];
        $nvpstr = $nvpstr . "&LANDINGPAGE=Login";
        $nvpstr .= "&CUSTOM=21";
        $_SESSION["currencyCodeType"] = $arg['currencyCodeType'];	  
        $_SESSION["PaymentType"] = $arg['paymentType'];
        $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING"){
            $token = urldecode($resArray["TOKEN"]);
            $_SESSION['TOKEN']=$token;
}
        return $resArray;
    }
    
 //-------------  pritam started   ----------------------------------------------------------  
    function CreateBillingAgreement( $data )
    {
        $nvpstr  ="&TOKEN=" . $data['token'];
        $resArray = $this->hash_call("CreateBillingAgreement", $nvpstr);
        return $resArray;
    }
    
    function CreateReferenceTransaction($paymentInfo, $amount)
    {    
        $paymentType = 'SALE'; 
        $nvpstr= '&REFERENCEID='.$paymentInfo->customer_id.'&PAYMENTACTION='.$paymentType.'&AMT='.$amount.'&CURRENCYCODE=USD'; 
        $resArray=$this->hash_call("DoReferenceTransaction",$nvpstr);
        return $resArray;     
        
    }
 //-----------------  ended  -----------------------------------------------------------------
    
}