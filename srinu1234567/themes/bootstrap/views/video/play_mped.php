<?php 
$v = RELEASE;
$x = RELEASE;
$play_length = 0;
$play_percent = 0;
if(isset($durationPlayed['played_length']) && isset($durationPlayed['played_percent'])) {
    $play_length = $durationPlayed['played_length'];
    $play_percent = $durationPlayed['played_percent'];    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!DOCTYPE html> 
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.1.0.min.js"   integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
        <script>
            var user_id="<?php echo Yii::app()->user->id; ?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js"></script>
        <script type='text/javascript'>
        var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
        var totalDRMBandWidth=0;
        var getDRMBandWidthForVideo=0;
        var getDRMBandWidthForAudio =0;
        var DRMresolution=0;
        var DRMbufferenEndd = 0;
        var DRMbufferenStart =0;
        var DRMbuff_log_id = 0;
        var DRMu_id =0;
        var requestVar = 'mped_dash';
        // video js error message
            var you_aborted_the_media_playback ="<?php echo $this->Language["you_aborted_the_media_playback"]; ?>";
            var a_network_error_caused_the ="<?php echo $this->Language["a_network_error_caused_the"]; ?>";
            var the_media_playback_was_aborted ="<?php echo $this->Language["the_media_playback_was_aborted"]; ?>";
            var the_media_could_not_be_loaded ="<?php echo $this->Language["the_media_could_not_be_loaded"]; ?>";
            var the_media_is_encrypted ="<?php echo $this->Language["the_media_is_encrypted"]; ?>";
            var no_compatible_source_was_found ="<?php echo $this->Language["no_compatible_source_was_found"];?>";
            var only_watchable_on_pc_and_laptops ="<?php echo $this->Language["player_watchable_message"];?>";
            
            var off ="<?php echo $this->Language["off"];?>";
            var muviWaterMark = "";
                <?php  if (isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != '') { ?>
                            var muviWaterMark = "<?php echo $_REQUEST['waterMarkOnPlayer'] ?>";
                <?php }  else if ($waterMarkOnPlayer != '') { ?>
                            var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
                <?php } ?>
        </script>

        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/css/bootstrap.min.css" />
	<script src="https://vjs.zencdn.net/5.10.8/video.js"></script>
	<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
	<link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
	
		<!-- If you'd like to support IE8 -->
	<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
	<!--videojserror-->
    <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs_error_css.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
    <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs_error_js.js?v=<?php echo $v ?>"></script>
    <!--videojserror-->
	<link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />    
        <link href="https://vjs.zencdn.net/5.10.8/video-js.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/newdrm.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
		
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
		<style type="text/css">
            .video-js{height:50%;padding-top:48%}.vjs-fullscreen{padding-top:0}.RDVideoHelper{display:none}video::-webkit-media-controls{display:none!important}video::-webkit-media-controls-panel{display:none!important}#backButton,.customized-res{display:none;position:absolute}body{background:#000}#backButton{left:100px;top:70px;z-index:1}.ytp-svg-shadow{stroke:#000;stroke-opacity:.15;stroke-width:2px;fill:none}.ytp-svg-fill{fill:#ccc}.customized-res{z-index:2000;right:10%;padding:0 5px;cursor:pointer;visibility:visible;opacity:.1%;line-height:23px;-webkit-transition:visibility .1s,opacity .1s;-moz-transition:visibility .1s,opacity .1s;-o-transition:visibility .1s,opacity .1s;transition:visibility .1s,opacity .1s;background-color:#07141e;background-color:rgba(7,20,30,.7);border-radius:3px}#video-res-sec ul,#video-res-sec ul li{list-style:none;margin:0;padding:0}.textlftAlg{text-align:left}.textRgtAlg{text-align:right;float:right}.vjs-big-play-button{display: none !important;visibility: hidden;}
        #play{
            display: none;
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            -webkit-animation: fadeinout .5s linear forwards;
            animation: fadeinout .5s linear forwards;
        }
        @-webkit-keyframes fadeinout {
            0%,100% { opacity: 0; }
            50% { opacity: 1; }
            from { transform: 5%; }
            50% { transform: scale(1.4); }
        }

        @keyframes fadeinout {
            0%,100% { opacity: 0; }
            50% { opacity: 1; }
            from { transform:  5%; }
            50% { transform: scale(1.4); }
        }
        #pause{
            display: none;
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            -webkit-animation: fadeinout .5s linear forwards;
            animation: fadeinout .5s linear forwards;
        }
        @-webkit-keyframes fadeinout {
            0%,100% { opacity: 0; }
            50% { opacity: 1; }
            from { transform: 5%; }
            50% { transform: scale(1.4); }
        }

        @keyframes fadeinout {
            0%,100% { opacity: 0; }
            50% { opacity: 1; }
            from { transform:  5%; }
            50% { transform: scale(1.4); }
        }

        </style>
		<?php 
			if(isset($block_ga)){ 
				$block_ga = Yii::app()->session['block_ga'];
			if ($this->studio->google_analytics != '' && $block_ga!=0 ) { ?>
            <script>
				(function (i, s, o, g, r, a, m) {
					i['GoogleAnalyticsObject'] = r;
					i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
					a = s.createElement(o),
							m = s.getElementsByTagName(o)[0];
					a.async = 1;
					a.src = g;
					m.parentNode.insertBefore(a, m)
				})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

				ga('create', '<?php echo html_entity_decode($this->studio->google_analytics); ?>', 'auto');
				ga('send', 'pageview');
            </script>
        <?php
        }}?>   
</head>
<body>
    <input type="hidden" id="full_video_duration" value="" />
    <input type="hidden" id="full_video_resolution" value="" />
    <input type="hidden" id="u_id" value="0" />
    <input type="hidden" id="buff_log_id" value="0" />
    <input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
	<div class="wrapper">
		<div class="videocontent" style="overflow:hidden;">
			<video id="video_block" class="video-js moo-css vjs-default-skin" controls <?php if($play_percent != 0 && $play_length != 0){ ?> autoplay="false" preload="none" autobuffer="false" <?php } else{ ?> autoplay preload="auto" autobuffer <?php } ?> style="width: 100%;height: 100%;"  webkit-playsinline crossorigin="anonymous" data-setup='{"plugins": {"errors": {}}}'>
			<!-- Subtitle For DRM Player -->
                            <?php
                                foreach ($subtitleFiles as $subtitleFilesKey => $subtitleFilesval) {
                                   if ($subtitleFilesKey == 1) {
                                       echo '<track kind="subtitles" src="' . $subtitleFilesval['url'] . '" srclang="' . $subtitleFilesval['code'] . '" label="' . $subtitleFilesval['language'] . '" default="true" ></track>';
                                    } else {
                                       echo '<track kind="subtitles" src="' . $subtitleFilesval['url'] . '" srclang="' . $subtitleFilesval['code'] . '" label="' . $subtitleFilesval['language'] . '" ></track>';
                                    }
                                }
                            ?>
			</video>

		</div> 
	</div>
	  <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl();?>/common/js/dash.all.debug.js?x=<?php echo $x ?>"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-dash/2.4.0/videojs-dash.js"></script>
  <script src="<?php echo Yii::app()->getbaseUrl() ?>/js/videojs.fairplay.min.js"></script>
<?php if($play_percent != 0 && $play_length != 0) { ?>
            <div class="modal fade" id="play_confirm" tabindex="-1" role="dialog" >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"> <?php echo $this->Language['resume_watching']; ?></h4>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-danger" id="confirm_yes" type="button" onclick="FromDurationPlayed()"><?php echo $this->Language['yes']; ?></button>
                            <button data-dismiss="modal" class="btn btn-default-with-bg" type="button" onclick="FromBeginning()"><?php echo $this->Language['btn_cancel']; ?></button>
                        </div>  
                    </div>
                </div>
            </div>
    <?php } ?>
  <script> 
    var url = "<?php echo Yii::app()->baseUrl; ?>/video/add_log";
    var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";
//console.log("is_studio_admin ::: "+ is_studio_admin);
    var movie_id = "<?php echo $movie_id ?>";
  //console.log("$movie_id ::: "+ movie_id);
    var stream_id = "<?php echo isset($stream_id) ? $stream_id : 0 ?>";
 //console.log("$stream_id ::: "+ stream_id);
    var percen = 5;
    var play_length = 0;
    var t = 0;
    var adavail = 0;    
    var play_status = 0;
	        
                
    var error_count= 1;   
    var player = videojs('video_block', {
      html5: {
        dash: {
          limitBitrateByPortal: true
        },
        nativeTextTracks: false
      }
    });
    var nAgt = navigator.userAgent;
    var browserName = navigator.appName;
    var verOffset;

    // In Opera 15+, the true version is after "OPR/" 
    if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
        browserName = "Opera";
    }
    // In older Opera, the true version is after "Opera" or after "Version"
    else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        browserName = "Opera";
    }
    // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        browserName = "Microsoft Internet Explorer";
    }
    // In Chrome, the true version is after "Chrome" 
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        browserName = "Chrome";
    }
    // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        browserName = "Safari";
    }
    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        browserName = "Firefox";
    }
    player.ready(function() {
        if(is_mobile===0){
            if(browserName == 'Safari'){
                player.src({
                    src: '<?= $fullmovie_path_hls;?>',
                    type: 'application/x-mpegURL',

                    protection: {
                    keySystem: 'com.apple.fps.1_0',

                    certificateUrl: '/fairplay.cer',
                    licenseUrl: '<?= $Authtoken['fairPlayToken'];?>',
                    },
                });
            } else{
                player.src({
                    src: '<?= $fullmovie_path;?>',
                    type: 'application/dash+xml',
                    keySystemOptions: [
                        {
                          name: 'com.widevine.alpha',
                          options: {
                            licenseUrl:"<?= $Authtoken['wvToken'];?>"
                          }
                        },
                        {
                          name: 'com.microsoft.playready',
                          options: {
                            'serverURL': "<?= $Authtoken['playReadyToken'];?>"
                          }
                        }
                      ]
                });
            }
        }else{
            //Environment for mobile platform & DRM activated content 
            player.src({
                src: '',
                type: 'application/x-mpegURL',

                protection: {
                keySystem: 'com.apple.fps.1_0',

                certificateUrl: '/fairplay.cer',
                licenseUrl: '',
                },
            });
            //$('.vjs-modal-dialog-content').html('arvind');
            if($(document).find('role').val('document')){
                $('.vjs-modal-dialog-content').attr('style','display:none');
                $('.vjs-error-display').attr('style','border:1px solid red;display:none;');
            }
        }
	$('#video_block').append('<img src="/images/back-button.png" width="40" height="40" id="backButton" style="cursor:pointer" data-toggle="tooltip" title="Back to Browse" allowFullScreen = true/>'); 
	$('#backButton').click(function(){
		if($('#backbtnlink').val() !='')
                    window.location.href = $('#backbtnlink').val();
                else
                   parent.history.back();
                return false;
	});
        // Back button hide in fullscreen
        setInterval(function() {
            var user_active = player.userActive();
            if(user_active === true){
                $('#backButton').show();
            } else {
                $('#backButton').hide();
            }
        }, 1000);
            $('#video_block').append('<img src="/images/pause.png" id="pause"/>');
            $('#video_block').append('<img src="/images/play-button.png" id="play"/>');
            $('#video_block_html5_api').on('click', function (e) {
                e.preventDefault();
                if (player.paused()) {
                    $('#pause').hide();
                    $('#play').show();

                } else {
                    $('#play').hide();
                    $('#pause').show();
                }
            });
            // use of spacebar for pause the video during play.                    
        if ((browserName === 'Safari') || (browserName === 'Firefox')) {
                $('.vjs-control.vjs-button').click(function (e) {
                    $(this).blur();
                    e.preventDefault();
                }); 
            $(document).bind('keydown', function(e) {
            if (e.keyCode === 32) { 
                if(player.paused()){
                    player.play();
                } else{
                     player.pause();
                }
            }
          });
        } else if(browserName === 'Chrome'){
                $('.vjs-control.vjs-button').click(function (e) {
                    $(this).blur();
                    e.preventDefault();
                }); 
            $(document).bind('keypress', function(e) {
                //$(document).focus();
            if (e.keyCode === 32) {
                    if(player.paused()){
                        player.play();
                    } else {
                        player.pause();
                    }
                }
             });
        } else if(browserName === "Microsoft Internet Explorer"){
            $('.vjs-control.vjs-button').click(function (e) {
                $(this).blur();
                e.preventDefault();
            });
            $('#video_block_html5_api').on('click',function(e){ 
                $(this).blur();
                e.preventDefault(); 
            });
            $(document).bind('keyup', function(e) {
             if (e.keyCode === 32) { 
                    if(player.paused()){
                        player.play();
                    } else {
                        player.pause();
                    }
                }
            });
        }
<?php if ($v_logo != '') { ?>
                        player.watermark({
			file: "<?php echo $v_logo; ?>",
			xrepeat: 0,
			opacity: 0.75
		});
		$(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;cursor:pointer;");
		$(".vjs-watermark").click(function () {
			window.history.back();
		});
	<?php } ?>
	  player.play();
          
          //Implementng Viudeo Logs
          if (is_studio_admin !== "true") {
            <?php if (Yii::app()->aws->isIpAllowed() && isset(Yii::app()->user->add_video_log) && (Yii::app()->user->add_video_log == 1)) { ?>
                        var started = 0;
                        var ended = 0;
                        var logged = 0;
                        var log_id = 0;
                        
                        player.on('loadedmetadata', function() {
                            
                            /*
                             * Ajax 18/11/2016 @avi (aravind@muvi.com)
                             */
                            $.ajax({
                                    type: 'GET',
                                    url: '<?php echo Yii::app()->baseUrl; ?>/report/addNewBufferLog',
                                    async: false,
                                    data :{
                                                movie_id: "<?php echo $movie_id ?>",
                                                video_id: "<?php echo isset($stream_id) ? $stream_id : 0 ?>", 
                                                start_time: DRMbufferenStart, 
                                                end_time: DRMbufferenEndd, 
                                                resolution: DRMresolution,
                                                request_type : requestVar,
                                                totDRMBandwidth: parseInt(totalDRMBandWidth/1024)
                                                },
                                    success: function(data)
                                    {
                                        var obj = JSON.parse(data);
                                        DRMbuff_log_id = obj.id;
                                        DRMu_id = obj.u_id;
                                    },
                                    error: function(e)
                                    {
                                       alert("Exception ::: " + e.message);
                                    }
                            });
                        });
//Player ended...
                        player.on("ended", function() {
                            /*
                             * Ajax call for Buffer & Bandwidth updation...
                             */
                            setDRMNewBufferLogInfo(player,Math.round(player.currentTime()),DRMbuff_log_id,DRMu_id)
                        });

 //Interval Call...
                        setInterval(function(){
                            /*
                             * Ajax call for Buffer & Bandwidth updation...
                             */
                            setDRMNewBufferLogInfo(player,Math.round(player.currentTime()),DRMbuff_log_id,DRMu_id);},60000);
                        
<?php } ?>
                }
                //End video Log
                
               //  player.on("loadedmetadata", function() {
                    <?php if($play_percent != 0 && $play_length != 0) { ?>
                        player.pause();
                        $('#play_confirm').modal('show');
                        $(document).bind('keydown', function(e) {
                            if (e.keyCode === 13) {
                                 $('#confirm_yes').trigger('click');      
                            }
                        });
                        player.on('loadstart', function() {
                            player.pause();
                        });
                    <?php } ?>
                //});
                player.on('error', function () {
                        var errordetails = this.player().error();
                        if (is_mobile === 1){
                            $(".vjs-error-display").html("<div>"+only_watchable_on_pc_and_laptops+"</div>");
                        }
                        else if (errordetails.code !=="" && is_mobile === 0){ 
                            if(document.getElementsByTagName("video")[0].error != null){
                                var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                                if (videoErrorCode === 2) {
                                    $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                                } else if (videoErrorCode === 3) {
                                    $(".vjs-error-display").html("<div>"+the_media_playback_was_aborted+"</div>");
                                } else if (videoErrorCode === 1) {
                                     $(".vjs-error-display").html("<div>"+you_aborted_the_media_playback+"</div>");
                                } else if (videoErrorCode === 4) {
                                      $(".vjs-error-display").html("<div>"+the_media_could_not_be_loaded+"</div>");
                                } else if (videoErrorCode === 5) {
                                    $(".vjs-error-display").html("<div>"+the_media_is_encrypted+"</div>");
                                }
                            }
                        }
                    });
                if($("div.vjs-subtitles-button").length) {
                    var divtagg = $(".vjs-subtitles-button .vjs-menu .vjs-menu-content li:first-child");
                    var divtaggVal = divtagg.text();
                    if(divtaggVal.trim() == 'subtitles off'){
                        divtagg.text("subtitles " +off);
                    }
                }
            });
          
    videojs.Html5DashJS.beforeInitialize = function(player, mediaPlayer) {
    /* Log MediaPlayer messages through video.js*/
    if (videojs && videojs.log) {
      mediaPlayer.getDebug().setLogToBrowserConsole(false);
      mediaPlayer.on('log', function(event) {
      /*console.log(event);*/
      });
      mediaPlayer.on('error', function(event) {
        console.log(event);
		if(event.type=='error' && event.error=='key_session' && error_count ==1){
			
		}
      });
    }
  };
 	$('.videocontent').mouseover(function() {
		$('#backButton').show();
	});
	$('.videocontent').mouseout(function() {
		$('#backButton').hide();
	});

  /*player.on('error', function () {alert('hi');
	if(document.getElementsByTagName("video")[0].error != null){
		var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
		if (videoErrorCode === 2) {
			if(seekStart === null){
				seekStart = 123;
				createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime,browserName);
			}
		} else if (videoErrorCode === 3) {
			$(".vjs-error-display").html("<div>Network not available.</div>");
		} else if (videoErrorCode === 1) {
			$(".vjs-error-display").hide();
		}
	}
});*/

        function FromDurationPlayed() {  
            percen = <?php echo $play_percent;?> ;
            play_length = <?php echo $play_length;?> ; 
            player.currentTime(<?php echo $play_length;?>);
            player.on('loadedmetadata', function() {
                player.currentTime(<?php echo $play_length;?>);
            });
            player.play();
            play_status = 0;
            $('.modal').hide();
        }
        
        function FromBeginning() {
            percen = 5 ;
            play_length = 0 ;
            player.play();
            play_status = 0;
            $('.modal').hide();     
        }
        
//DRM Buffer & Bandwidth calculation...
        function setDRMNewBufferLogInfo(player, player_curTime, DRMbuff_log_id,DRMu_id)
        {
            //return false;
            if (parseFloat(DRMbuff_log_id)) 
            {
                DRMbufferenEndd = player_curTime;
                $.ajax({
                        url: '<?php echo Yii::app()->baseUrl; ?>/report/addBufferLog',
                        type: 'post',
                        data: {
                                    movie_id: "<?php echo $movie_id ?>", 
                                    video_id: "<?php echo isset($stream_id) ? $stream_id : 0 ?>", 
                                    start_time: DRMbufferenStart, 
                                    end_time: DRMbufferenEndd, 
                                    resolution: DRMresolution, 
                                    buff_log_id: DRMbuff_log_id, 
                                    u_id: DRMu_id,
                                    request_type : requestVar,
                                    totDRMBandwidth: parseInt(totalDRMBandWidth/1024)
                                   },
                          success: function(res) {
                                var obj = JSON.parse(res);
                                buff_log_id = obj.id;
                                u_id = obj.u_id;
                                document.getElementById('u_id').value = u_id;
                            if (buff_log_id) {
                                document.getElementById('buff_log_id').value = buff_log_id;
                            }
                        },
                        error: function(jqXhr, textStatus, errorThrown){
                            console.log("Error :: " +  errorThrown );
                        }
                    });
            }
        }
  </script>
  <?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
    <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => $stream_id, 'movie_id' => $movie_id)); ?>
</body>
</html>
