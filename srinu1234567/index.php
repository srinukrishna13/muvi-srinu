<?php
ini_set('session.gc_maxlifetime', 21600);
error_reporting(1);
ini_set("upload_max_filesize", "4M");

// change the following paths if necessary
$yii = dirname(__FILE__) . '/../yii/framework/yii.php';

$enterprisesIps = array('52.211.41.107','52.77.47.122','52.70.64.85','52.65.244.101');
$ip = gethostbyname($_SERVER['HTTP_HOST']);

@defined('HOST_IP') or @ define('HOST_IP', $ip);
$hs = explode(".", $_SERVER['HTTP_HOST']);
$tp = count($hs);
$di = $tp - 2;
$domain = $hs[$di];
//getting the domain name
defined('DOMAIN') ? '' : define('DOMAIN', $domain);
$host_ip = array('52.0.232.150','52.211.41.107','52.77.47.122','52.70.64.85','52.65.244.101');
$subdomain = '';
if (isset($hs[0]) && $hs[0] == 'www' && isset($hs[1]))
    $subdomain = $hs[1];
else
    $subdomain = $hs[0];

//@Rati Added for moving muvi studio to muvi only
if (HOST_IP == '127.0.0.1')
    define('PRIMARY_DOMAIN', 'muuvi');
else if(HOST_IP == '52.211.41.107')
	define('PRIMARY_DOMAIN', 'sonydadc');
else if(HOST_IP == '52.77.47.122')
	define('PRIMARY_DOMAIN', 'adminbollywoodbubble');
else if(HOST_IP == '52.70.64.85')
	define('PRIMARY_DOMAIN', 'adminkingjesus');
else if(HOST_IP == '52.65.244.101')
	define('PRIMARY_DOMAIN', 'adminquickflix');
else if(DOMAIN == 'edocent'){
	define('PRIMARY_DOMAIN', 'edocent');
}else{
    define('PRIMARY_DOMAIN', 'muvi');
}
if($subdomain == PRIMARY_DOMAIN)
    $subdomain = 'studio';

//Getting the subdomain name
defined('SUB_DOMAIN') ? '' : define('SUB_DOMAIN', $subdomain);

if (HOST_IP == '127.0.0.1' || DOMAIN == 'idogic') {
    $config = dirname(__FILE__) . '/protected/config/main_staging.php';
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG', TRUE);
}  else if(HOST_IP == '52.211.41.107'){
	$config = dirname(__FILE__) . '/protected/config/sony_config.php';
    defined('YII_DEBUG') or define('YII_DEBUG', FALSE);
}else if(in_array (HOST_IP, $enterprisesIps)){
	$config = dirname(__FILE__) . '/protected/config/enterprises.php';
    defined('YII_DEBUG') or define('YII_DEBUG', FALSE);
}else if (DOMAIN == 'edocent') {
    $config = dirname(__FILE__) . '/protected/config/edo_config.php';
    defined('YII_DEBUG') or define('YII_DEBUG', TRUE);
}  else {
    $config = dirname(__FILE__) . '/protected/config/main.php';
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG', FALSE);
}

// specify how many levels of call stack should be shown in each log message
if (HOST_IP != '127.0.0.1') {
    defined('SUB_FOLDER') or define('SUB_FOLDER', '');
} else {
    defined('SUB_FOLDER') or define('SUB_FOLDER', '');
}

defined('BUCKET_NAME') or define('BUCKET_NAME', 'muvistudio');
defined('ACCESS_KEY') or define('ACCESS_KEY', 'AKIAJ4HTSCRXGHFTKRBQ');
defined('SECRET_KEY') or define('SECRET_KEY', '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');
defined('MAX_FILE_SIZE') or define('MAX_FILE_SIZE', 100000000000);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

 if ((HOST_IP != '127.0.0.1' && !(in_array(HOST_IP,$enterprisesIps)) && DOMAIN != 'edocent') && (SUB_DOMAIN == 'studio' || SUB_DOMAIN == 'devstudio') && (DOMAIN != 'idogic')) {   
    define('WP_USE_THEMES', true);
    $wp_did_header = true;
    require_once(dirname(__FILE__) . '/wpstudio/wp-load.php');

    require_once(dirname(__FILE__) . '/protected/components/ExceptionHandler.php');
    $router = new ExceptionHandler();
}

require_once($yii);

/* if(strstr(strtolower($_SERVER["REQUEST_URI"]),'page/index')){
  header("HTTP/1.1 301 Moved Permanently");
  $url ='http://'.$_SERVER['HTTP_HOST'].'/'.SUB_FOLDER;
  header('Location: '.$url);exit;
  } */
if (strstr(strtolower($_SERVER["REQUEST_URI"]), 'dam/index')) {
    header("HTTP/1.1 301 Moved Permanently");
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . SUB_FOLDER . 'dam';
    header('Location: ' . $url);
    exit;
}
if (strstr(strtolower($_SERVER["REQUEST_URI"]), 'muvi/index')) {
    header("HTTP/1.1 301 Moved Permanently");
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . SUB_FOLDER . "muvi";
    header('Location: ' . $url);
    exit;
}
if (strstr(strtolower($_SERVER["REQUEST_URI"]), 'contact/index')) {
    header("HTTP/1.1 301 Moved Permanently");
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . SUB_FOLDER . "contact";
    header('Location: ' . $url);
    exit;
}
if (strstr(strtolower($_SERVER["REQUEST_URI"]), 'category/general')) {
    header("HTTP/1.1 301 Moved Permanently");
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . SUB_FOLDER . 'blogs';
    header('Location: ' . $url);
    exit;
}
$_GET    = remove_magic_quotes( $_GET    );
$_POST   = remove_magic_quotes( $_POST   );
$_COOKIE = remove_magic_quotes( $_COOKIE );
$_SERVER = remove_magic_quotes( $_SERVER );
$_REQUEST = remove_magic_quotes( $_REQUEST );
Yii::createWebApplication($config)->run();

 function remove_magic_quotes( $array ) {
	foreach ( (array) $array as $k => $v ) {
		if ( is_array( $v ) ) {
			$array[$k] = remove_magic_quotes( $v );
		} else {
			$array[$k] = stripslashes( $v );
		}
	}
	return $array;
}
	