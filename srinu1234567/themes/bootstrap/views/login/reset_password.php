<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">Reset Password</h2>
            <div class="row" style="padding-left:30%;">
                <form method="post" id="reset_pass" novalidate="novalidate">
                    <div>
                        <div class="pull-left" style="width:20%"> New password </div> 
                        <div class="pull-left"><input type="password" name="new_password" id="new_password"/></div>
                    </div>
                    <div class="clear" style="height:10px;"></div>
                    <div>
                        <div class="pull-left" style="width:20%"> Confirm password </div> 
                        <div class="pull-left"><input type="password" name="confirm_password"/></div>
                    </div>
                    <div class="clear" style="height:10px;"></div>
                    <div style="padding-left:20%">
                        <input type="hidden" name="token" value="<?php echo $_REQUEST['user'] ?>" />
                        <input name="submit" type="submit" value="Save Password" class="btn btn-primary" />
                    </div>

                </form>
                <div id="success" class="alert alert-success hide"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        jQuery.validator.addMethod("alphaNumeric", function(value, element) {
            return this.optional(element) || /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i.test(value);
        }, "password must contain atleast one number and one character");
        $("#reset_pass").validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 8,
                    alphaNumeric: true
                },
                confirm_password: {
                    minlength: 8,
                    equalTo: "#new_password"
                }
            },
            messages: {
                new_password: {
                    required: "Please enter your password",
                    minlength: "Password must contain atleast 8 characters",
                    alphaNumeric: "password must contain atleast one number and one character"
                },
                confirm_password: {
                    minlength: "Password must contain atleast 8 characters",
                    equalTo: "Please enter the same value again"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/login/set_password/",
                    data: $('#reset_pass').serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function() {
                        //$('#reset-loading').show();
                    },
                    success: function(data) {
                        if (data.status == "success") {
                            //window.location = ;
                            $("#success").html(data.message).fadeIn("slow");
                            setTimeout(function() {
                                location.href = "<?php echo Yii::app()->getBaseUrl(true) ?>";
                            }, 5000);

                        } else {
                            bootbox.alert(data.message, function() {
                            });
                        }
                    }
                });
            }
        });
    });
</script>