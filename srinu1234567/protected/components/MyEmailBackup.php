<?php

class MyEmailBackup extends CApplicationComponent {

    function sendMandrilEmail($template_name, $template_content, $message) {

        require_once 'mandrill-api-php/src/Mandrill.php';
        $mandrill = new Mandrill('Xb3a0BtRxPQIJTbJwn1Vlg');
        $async = false;
        try {
            $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);
            return $result;
        } catch (Exception $e) {
            $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
            fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage() . "\n Template name-" . $template_name . '\n Template Content:-' . print_r($template_content, TRUE));
            fclose($fp);
            return false;
        }
    }

    function getCommonParams($arg = 0) {
        $location = '';
        if (intval($arg)) {
            $ip_address = CHttpRequest::getUserHostAddress();
            $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
            $country = $visitor_loc['country_name'];
            $region = $visitor_loc['region'];
            $location = (trim($region)) ? $country . ', ' . $region : $country;
        }

        //Sending Email to Sales Team
        $site_url = Yii::app()->getBaseUrl(true);
        $store_details = Yii::app()->common->storeDetails('studio');

        $logo = $site_url . '/themes/signup/images/muvi_studio_logo.png';
        $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
        $fb_link = '<a href="' . @$store_details['fb_link'] . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        $twitter_link = '<a href="' . @$store_details['twitter_link'] . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        $gplus_link = '<a href="' . @$store_details['gplus_link'] . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';


        $params = array(
            array('name' => 'website_name', 'content' => 'Muvi'),
            array('name' => 'logo', 'content' => @$logo),
            array('name' => 'location', 'content' => @$location),
            array('name' => 'fb_link', 'content' => @$fb_link),
            array('name' => 'twitter_link', 'content' => @$twitter_link),
            array('name' => 'gplus_link', 'content' => @$gplus_link)
        );

        return $params;
    }

    public function emailToSales($req = array()) {
        $cmnparams = $this->getCommonParams(1);

        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => @$req['email']),
            array('name' => 'phone', 'content' => @$req['phone']),
            array('name' => 'company', 'content' => @$req['companyname'])
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('autosignup_recipients');
        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');

        $adminSubject = "New Free Trial";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'signup_sales_welcome';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function emailToUser($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('trial_period'), 'value');

        $cmnparams = $this->getCommonParams();

        $params = array(
            array('name' => 'fname', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'domain', 'content' => $req['domain']),
            array('name' => 'trial_period', 'content' => $config[0]['value']),
            array('name' => 'expiry_date', 'content' => date('d F, Y', strtotime("+{$config[0]['value']} days"))),
        );
        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = "Getting started with Muvi";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );
        $template_name = 'studio_signup_touser';

        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function subscriptionPurchaseMailToSales($req = array()) {
        $cmnparams = $this->getCommonParams(1);
        
        $cloud_hosting ='';
        if (isset($req['is_cloud_hosting']) && intval($req['is_cloud_hosting'])) {
            $cloud_hosting = '<strong>Add Cloud hosting for your application (powered by Amazon Web Services)</strong><br/>';
        }
        
        $params = array(
            array('name' => 'package_name', 'content' => ucwords(@$req['package_name'])),
            array('name' => 'applications', 'content' => ucwords(@$req['applications'])),
            array('name' => 'plan', 'content' => ucwords(@$req['plan'])),
            array('name' => 'amount_charged', 'content' => '$' . @$req['amount_charged']),
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => @$req['email']),
            array('name' => 'phone', 'content' => @$req['phone']),
            array('name' => 'cloud_hosting', 'content' => $cloud_hosting),
            array('name' => 'company', 'content' => @$req['companyname']),
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('subscription_recipients');
        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');

        $adminSubject = "New paying customer!";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'subscription_sales';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function subscriptionPurchaseMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParams();

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = ucwords($req['package_name']) . " subscription activated!";

        if (trim($invoice_file)) {
            $attachment = file_get_contents(ROOT_DIR . 'docs/' . $invoice_file);
            $attachment_encoded = base64_encode($attachment);
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail,
                'attachments' => array(array('content' => $attachment_encoded,
                        'type' => "application/pdf",
                        'name' => $invoice_file))
            );
            $attached_file = "Please find the invoice attached here.";
        } else {
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail
            );
            $attached_file = "";
        }

        $params = array(
            array('name' => 'fname', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'domain', 'content' => $req['domain']),
            array('name' => 'attached_file', 'content' => $attached_file),
        );

        $params = array_merge($cmnparams, $params);

        $template_name = 'subscription_activation';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function raiseInvoicePaidMailToSales($req = array()) {
        $cmnparams = $this->getCommonParams();

        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'customer', 'content' => ucfirst(@$req['companyname'])),
            array('name' => 'invoice_title', 'content' => $req['invoice_title']),
            array('name' => 'invoice_amount', 'content' => $req['invoice_amount'])
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('invoice_paid');
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);

        $adminSubject = "Invoice paid!";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $emails_from['email'],
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'invoice_paid_sales';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function raiseInvoicePaidMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParams();

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = "Payment successful";

        if (trim($invoice_file)) {
            $attachment = file_get_contents(ROOT_DIR . 'docs/' . $invoice_file);
            $attachment_encoded = base64_encode($attachment);
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail,
                'attachments' => array(array('content' => $attachment_encoded,
                        'type' => "application/pdf",
                        'name' => $invoice_file))
            );
            $attached_file = "Please find the attached receipt.";
        } else {
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail
            );
            $attached_file = "";
        }

        $params = array(
            array('name' => 'name', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'invoice_title', 'content' => $req['invoice_title']),
            array('name' => 'invoice_amount', 'content' => $req['invoice_amount']),
            array('name' => 'attached_file', 'content' => $attached_file)
        );

        $params = array_merge($cmnparams, $params);

        $template_name = 'invoice_paid';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function errorMessageMailToSales($req = array()) {
        $cmnparams = $this->getCommonParams(1);

        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => @$req['email']),
            array('name' => 'company', 'content' => @$req['companyname']),
            array('name' => 'card_last_fourdigit', 'content' => @$req['card_last_fourdigit']),
            array('name' => 'error_message', 'content' => @$req['ErrorMessage'])
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('credit_card_failure');
        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');

        $adminSubject = "Credit card failure of " . $req['name'];
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'credit_card_failure';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function mailToStudioForPaymentGatewayAPI($req = array()) {
        $cmnparams = $this->getCommonParams();

        $params = array(
            array('name' => 'company', 'content' => @$req['companyname']),
            array('name' => 'payment_gateway', 'content' => @$req['payment_gateway']),
            array('name' => 'api_username', 'content' => @$req['api_username']),
            array('name' => 'api_password', 'content' => @$req['api_password']),
            array('name' => 'api_signature', 'content' => @$req['api_signature']),
            array('name' => 'api_mode', 'content' => @$req['api_mode'])
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('payment_gateway');
        $autosignup_from = array('email' => $req['email'], 'name' => $req['companyname']);

        $adminSubject = "Integration of '" . $req['payment_gateway'] . "' payment gateway";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'payment_gateway_api';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function monthlyYearlyRenewMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParams();

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $plan = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'Bandwidth' : @$req['plan'].'ly';
        $adminSubject = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'Bandwidth payment receipt' : ucwords($plan)." payment receipt";

        if (trim($invoice_file)) {
            $attachment = file_get_contents(ROOT_DIR . 'docs/' . $invoice_file);
            $attachment_encoded = base64_encode($attachment);
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail,
                'attachments' => array(array('content' => $attachment_encoded,
                        'type' => "application/pdf",
                        'name' => $invoice_file))
            );
            $attached_file = "Please find the receipt attached here.";
        } else {
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail
            );
            $attached_file = "";
        }

        $params = array(
            array('name' => 'name', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'plan', 'content' => $plan),
            array('name' => 'package_name', 'content' => $req['package_name']),
            array('name' => 'billing_period', 'content' => $req['billing_period']),
            array('name' => 'attached_file', 'content' => $attached_file)
        );

        $params = array_merge($cmnparams, $params);

        $template_name = 'monthly_yearly_renew';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function monthlyYearlyRenewMailToSales($req = array()) {
        $plan = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'Bandwidth' : @$req['plan'].'ly';
        
        $cmnparams = $this->getCommonParams();
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'customer', 'content' => ucfirst(@$req['companyname'])),
            array('name' => 'plan', 'content' => $plan),
            array('name' => 'billing_amount', 'content' => '$' . @$req['billing_amount'])
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('invoice_paid');
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);
        
        $adminSubject = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? "Bandwidth payment charged" : ucwords($plan)." payment charged";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $emails_from['email'],
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'monthly_yearly_renew_sales';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function cancelSubscriptionMailToSales($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');

        $cmnparams = $this->getCommonParams(1);
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => @$req['email']),
            array('name' => 'phone', 'content' => @$req['phone']),
            array('name' => 'company', 'content' => @$req['companyname']),
            array('name' => 'cancel_reason', 'content' => @$req['cancel_reason']),
            array('name' => 'custom_notes', 'content' => @$req['custom_notes']),
            array('name' => 'expiry_date', 'content' => date('F d, Y', strtotime("+{$config[0]['value']} days")))
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('cancel_recipients');
        $autosignup_from = array('email' => $req['email'], 'name' => $req['companyname']);

        $adminSubject = "Subscription cancelled";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'cancel_subscription_sales';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function cancelSubscriptionMailToUser($req = array(), $invoice_file) {
        $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');
        $cmnparams = $this->getCommonParams();

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = "Muvi subscription cancelled";

        if (trim($invoice_file)) {
            $attachment = file_get_contents(ROOT_DIR . 'docs/' . $invoice_file);
            $attachment_encoded = base64_encode($attachment);

            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail,
                'attachments' => array(array('content' => $attachment_encoded,
                        'type' => "application/pdf",
                        'name' => $invoice_file))
            );

            $attached_file = "Please find the invoice attached here.";
        } else {
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail
            );
            $attached_file = "";
        }

        $params = array(
            array('name' => 'fname', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'domain', 'content' => $req['domain']),
            array('name' => 'onhold_period', 'content' => $config[0]['value']),
            array('name' => 'attached_file', 'content' => $attached_file)
        );

        $params = array_merge($cmnparams, $params);

        $template_name = 'cancel_subscription';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);

        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function freeTrialExpiringEmailToUser($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('monthly_charge'), 'value');

        $cmnparams = $this->getCommonParams();

        $params = array(
            array('name' => 'fname', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'domain', 'content' => $req['domain']),
            array('name' => 'monthly_charge', 'content' => $config[0]['value'])
        );
        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = "Your free trial is expiring in 2 days";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );
        $template_name = 'free_trial_expire';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function freeTrialExpiredEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParams();

        $params = array(
            array('name' => 'fname', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'domain', 'content' => $req['domain']),
            array('name' => 'expired_date', 'content' => $req['expired_date'])
        );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = "Your free trial has expired";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );
        $template_name = 'free_trial_expired';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function billingCycleMailToSale($req) {
        $cmnparams = $this->getCommonParams(1);

        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => @$req['email']),
            array('name' => 'company', 'content' => @$req['companyname']),
            array('name' => 'to_date', 'content' => date('F d, Y'))
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = array(array('email' => 'support@muvi.com', 'name' => 'Muvi', 'type' => 'to'));
        $billing_from = array('email' => $req['email'], 'name' => ucfirst(@$req['name']));

        $adminSubject = "Billing cycle started!";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $billing_from['email'],
            'from_name' => $billing_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'billing_cycle';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function yearlyPlanRenewalReminderEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParams();

        if ($req['days'] == 30) {
            $adminSubject = "Muvi yearly plan renewal";
            $cntnt_msg = "";
        } else if ($req['days'] == 14) {
            $adminSubject = "Muvi yearly plan renewal - second reminder";
            $cntnt_msg = "second and final";
        }

        $params = array(
            array('name' => 'cntnt_msg', 'content' => $cntnt_msg),
            array('name' => 'fname', 'content' => ucfirst($req['name'])),
            array('name' => 'package_name', 'content' => $req['package_name']),
            array('name' => 'next_billing_date', 'content' => $req['next_billing_date']),
            array('name' => 'total_amount_to_be_charged', 'content' => $req['total_amount_to_be_charged']),
        );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));

        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );
        $template_name = 'yearly_plan_renewal';

        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function cardFailureMailToUser($req = array()) {
        $cmnparams = $this->getCommonParams();

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        $adminSubject = "Payment failed!";

        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );

        $params = array(
            array('name' => 'name', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'card_last_fourdigit', 'content' => $req['card_last_fourdigit']),
        );

        $params = array_merge($cmnparams, $params);

        $template_name = 'user_payment_failed';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function cardFailureMailToAdmin($req = array()) {
        $cmnparams = $this->getCommonParams();

        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => ucfirst(@$req['email'])),
            array('name' => 'company', 'content' => ucfirst(@$req['companyname'])),
            array('name' => 'plan', 'content' => @$req['plan']),
            array('name' => 'card_last_fourdigit', 'content' => @$req['card_last_fourdigit']),
            array('name' => 'error_message', 'content' => @$req['ErrorMessage']),
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('payment_failed');
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);

        $adminSubject = ucfirst(@$req['name']) . " payment failed";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $emails_from['email'],
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'admin_payment_failed';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function paymentFailedReminderEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParams();
        $adminSubject = "Reminder - payment failed!";
        
        $params = array(
            array('name' => 'name', 'content' => ucfirst($req['name'])),
            array('name' => 'plan', 'content' => strtolower(@$req['plan'].'ly')),
            array('name' => 'card_last_fourdigit', 'content' => $req['card_last_fourdigit'])
        );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));

        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );
        $template_name = 'payment_failed_reminder';

        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function paymentFailedFinalReminderEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParams();
        $adminSubject = "Final Reminder - payment failed!";
        
        $params = array(
            array('name' => 'name', 'content' => ucfirst($req['name'])),
            array('name' => 'plan', 'content' => strtolower(@$req['plan'].'ly')),
            array('name' => 'card_last_fourdigit', 'content' => $req['card_last_fourdigit']),
            array('name' => 'cancelled_date', 'content' => $req['cancelled_date'])
        );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));

        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $toEmail
        );
        $template_name = 'payment_failed_final_reminder';

        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }

    public function subscriptionUpdatedMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParams();

        $autosignup_from = Yii::app()->common->MuviAdminEmails('autosignup_from');
        $toEmail = array(array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to'));
        
        $adminSubject = "Muvi subscription updated";

        if (trim($invoice_file)) {
            $attachment = file_get_contents(ROOT_DIR . 'docs/' . $invoice_file);
            $attachment_encoded = base64_encode($attachment);
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail,
                'attachments' => array(array('content' => $attachment_encoded,
                        'type' => "application/pdf",
                        'name' => $invoice_file))
            );
        } else {
            $mailAddress = array(
                'subject' => $adminSubject,
                'from_email' => $autosignup_from['email'],
                'from_name' => $autosignup_from['name'],
                'to' => $toEmail
            );
        }

        $amount_charged = '';

        if (isset($req['amount_charged']) && trim($req['amount_charged'])) {
            $amount_charged = "Amount charged: <strong>$" . $req['amount_charged'] . "</strong>";
        }
        
        $params = array(
            array('name' => 'name', 'content' => ucfirst($req['name'])),
            array('name' => 'email', 'content' => $req['email']),
            array('name' => 'package_name', 'content' => ucwords(@$req['package_name'])),
            array('name' => 'applications', 'content' => ucwords(@$req['applications'])),
            array('name' => 'plan', 'content' => ucwords(@$req['plan'].'ly')),
            array('name' => 'amount_charged', 'content' => $amount_charged)
        );

        $params = array_merge($cmnparams, $params);
        
        $template_name = 'subscription_updated';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }
    
    public function subscriptionUpdatedMailToSales($req = array()) {
        $cmnparams = $this->getCommonParams();
        
        $amount_charged = '';

        if (isset($req['amount_charged']) && trim($req['amount_charged'])) {
            $amount_charged = "Amount charged: $" . $req['amount_charged'];
        }
        
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'customer', 'content' => ucfirst(@$req['companyname'])),
            array('name' => 'package_name', 'content' => ucwords(@$req['package_name'])),
            array('name' => 'applications', 'content' => ucwords(@$req['applications'])),
            array('name' => 'plan', 'content' => ucwords(@$req['plan'].'ly')),
            array('name' => 'amount_charged', 'content' => $amount_charged)
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmails('invoice_paid');
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);

        $adminSubject = "Subscription updated";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $emails_from['email'],
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'subscription_updated_sales';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function RaiseNewticket_frommailtoadmin($req = array()){
      $cmnparams = $this->getCommonParams();
        $priority = 'medium';
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => (@$req['email'])),
            array('name' => 'description', 'content' => (@$req['description'])),
            array('name' => 'url1', 'content' => (@$req['url1'])),
            array('name' => 'url2', 'content' => (@$req['url2'])),
            array('name' => 'priority', 'content' => $priority),
            array('name' => 'subject', 'content' => (@$req['subject']))
        );
        $params = array_merge($cmnparams, $params);
       // print_r($params);exit;
        $adminGroupEmail = array(
                    array('email' => 'support@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);
//        $mailAddress = array(
//            'subject' => @$req['subject'],
//            'from_email' => 'support@muvi.com',
//            'from_name' => $emails_from['name'],
//            'to' => $adminGroupEmail
//        );
        
      $mailAddress = array(
            'subject' => @$req['subject'],
            'from_email' => 'testmuvi12@gmail.com',
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );  
        
        //print '<pre>';print_r($mailAddress);//exit;
        $template_name = 'addticket_mail_admin';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function RaiseNewticket_frommailtouser($req = array()){
       $cmnparams = $this->getCommonParams();
        $priority = 'medium';
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => (@$req['email'])),
            array('name' => 'description', 'content' => (@$req['description'])),
            array('name' => 'url1', 'content' => (@$req['url1'])),
            array('name' => 'url2', 'content' => (@$req['url2'])),
            array('name' => 'priority', 'content' => $priority),
            array('name' => 'subject', 'content' => (@$req['subject']))
        );
        
        $params = array_merge($cmnparams, $params);
        echo "<pre>";
        //print_r($params);exit;
        $adminGroupEmail = array(
                    array('email' => $req['email'], 'name' => 'Muvi', 'type' => 'to')
                );
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);
       // print_r($emails_from);exit;
        //$adminSubject = "$subject";
//        $mailAddress = array(
//            'subject' => trim(@$req['subject']),
//            'from_email' => 'support@muvi.com',
//            'from_name' => 'Muvi',
//            'to' => $adminGroupEmail
//        );
//        
        
         $mailAddress = array(
            'subject' => trim(@$req['subject']),
            'from_email' => 'testmuvi12@gmail.com',
            'from_name' => 'Muvi',
            'to' => $adminGroupEmail
        );
       //print_r($mailAddress);exit;
        $template_name = 'addticket_mail_user';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        
        return $returnVal;
    }
    
    
    public function addnote_frommailtoadmin($req = array()){
      $cmnparams = $this->getCommonParams();
        $priority = 'medium';
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => (@$req['email'])),
            array('name' => 'description', 'content' => (@$req['description'])),
            array('name' => 'url1', 'content' => (@$req['url1'])),
            array('name' => 'url2', 'content' => (@$req['url2'])),
            array('name' => 'priority', 'content' => $priority),
            array('name' => 'subject', 'content' => (@$req['subject']))
        );
        $params = array_merge($cmnparams, $params);
       // print_r($params);exit;
        $adminGroupEmail = array(
                    array('email' => 'ticket@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);
//        $mailAddress = array(
//            'subject' => @$req['subject'],
//            'from_email' => 'support@muvi.com',
//            'from_name' => $emails_from['name'],
//            'to' => $adminGroupEmail
//        );
        
      $mailAddress = array(
            'subject' => @$req['subject'],
            'from_email' => 'testmuvi12@gmail.com',
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );  
        
        //print '<pre>';print_r($mailAddress);//exit;
        $template_name = 'addnote_mail_admin';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function addnote_frommailtouser($req = array()){
       $cmnparams = $this->getCommonParams();
        $priority = 'medium';
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => (@$req['email'])),
            array('name' => 'description', 'content' => (@$req['description'])),
            array('name' => 'url1', 'content' => (@$req['url1'])),
            array('name' => 'url2', 'content' => (@$req['url2'])),
            array('name' => 'priority', 'content' => $priority),
            array('name' => 'subject', 'content' => (@$req['subject']))
        );
        
        $params = array_merge($cmnparams, $params);
      
        //print_r($params);exit;
        $adminGroupEmail = array(
                    array('email' => $req['email'], 'name' => 'Muvi', 'type' => 'to')
                );
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);
       // print_r($emails_from);exit;
        //$adminSubject = "$subject";
//        $mailAddress = array(
//            'subject' => trim(@$req['subject']),
//            'from_email' => 'support@muvi.com',
//            'from_name' => 'Muvi',
//            'to' => $adminGroupEmail
//        );
//        
        
         $mailAddress = array(
            'subject' => trim(@$req['subject']),
            'from_email' => 'testmuvi12@gmail.com',
            'from_name' => 'Muvi',
            'to' => $adminGroupEmail
        );
       //print_r($mailAddress);exit;
        $template_name = 'addnote_mail_user';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        
        return $returnVal;
    }
    
    
    public function UpdateNote_admin($req = array()){
        //echo 1111111111111;exit;
       $cmnparams = $this->getCommonParams();
        $priority = 'medium';
        $params = array(
            array('name' => 'name', 'content' => ucfirst(@$req['name'])),
            array('name' => 'email', 'content' => (@$req['email'])),
            array('name' => 'description', 'content' => (@$req['description'])),
            array('name' => 'url1', 'content' => (@$req['url1'])),
            array('name' => 'url2', 'content' => (@$req['url2'])),
            array('name' => 'priority', 'content' => $priority),
            array('name' => 'subject', 'content' => (@$req['subject']))
        );
        //print_r($params);exit;
        $params = array_merge($cmnparams, $params);
        $adminGroupEmail = array(
                    array('email' => 'ticket@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        $emails_from = array('email' => $req['email'], 'name' => $req['name']);

        //$adminSubject = "$subject";
        $mailAddress = array(
            'subject' => (@$req['subject']),
            'from_email' => 'testmuvi12@gmail.com',
            'from_name' => 'Muvi',
            'to' => (@$req['email'])
        );
        //print_r($mailAddress);exit;
        $template_name = 'UpdateticketNote_admin';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function videoEncoding($mailcontent = '') {
        $cmnparams = $this->getCommonParams();
        if(HOST_IP == '52.0.232.150'){
            $adminGroupEmail = array(
                array('email' => 'srutikant@muvi.com', 'name' => 'Sruti Kanta', 'type' => 'to'),
                array('email' => 'rasmi@muvi.com', 'name' => 'Rasmi', 'type' => 'bcc'),
                array('email' => 'mohan@muvi.com', 'name' => 'Mohan', 'type' => 'bcc'),
            );
        } else{
            $adminGroupEmail = array(
                array('email' => 'srutikant@muvi.com', 'name' => 'Sruti Kanta', 'type' => 'to'),
            );
        }
        $adminSubject = "Video Encoding status";
        $params = array(
                array('name' => 'mailcontent', 'content' => $mailcontent)
            );
        $params = array_merge($cmnparams, $params);
        $adminSubject = "Video Encoding status";
        $fromMail = array('email' => 'studio@muvi.com', 'name' => 'Muvi');
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $fromMail['email'],
            'from_name' => $fromMail['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'video_conversion';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    
    public function weeklyPaymentStatus($str) {
        $cmnparams = $this->getCommonParams();
        
        $params = array(
            array('name' => 'payment_data', 'content' => $str)
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = array(
            array('email' => 'sunil@muvi.com', 'name' => 'Sunil Kund', 'type' => 'to'),
            array('email' => 'rasmi@muvi.com', 'name' => 'Rasmi', 'type' => 'cc')
        );
        $emails_from = array('email' => 'studio@muvi.com', 'name' => 'Muvi');

        $adminSubject = "Weekly studio's payment status!";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $emails_from['email'],
            'from_name' => $emails_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'weekly_payment_status';
        
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    public function customerByReseller($req = array(),$resellername,$studio) {
        //$StudioName = $studio['name'];
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = 'https://www.muvi.com/themes/signup/images/muvi_studio_logo.png';
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left"><p>Hi,</p>
                    <p>A New customer from reseller '.$resellername.' has registered to ' . $studio['domain'] . ' Below are the details of the user</p>
                    <p style="display:block;margin:0 0 17px">
                        Name: <strong><span mc:edit="name">' . ucfirst(@$req['customer_name']) . '</span></strong><br/>
                        Email: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            array( 'name' => 'website_name', 'content' => $site_url),
            array( 'name' => 'logo', 'content' => $logo),
            array( 'name' => 'mailcontent', 'content' => $admincontent),
        );
        if (HOST_IP != '52.0.232.150') {
            $adminGroupEmail = array(
                    array('email' => 'manas@muvi.com', 'name' => 'All', 'type' => 'to')
                );
        }else{
            $adminGroupEmail = array(
                    array('email' => 'all@muvi.com', 'name' => 'All', 'type' => 'to')
                );
        }        
        $autosignup_from = array('email' => 'studio@muvi.com', 'name' => 'Muvi');
        
        $adminSubject = "Reseller purchased new customer!";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'sdk_user_welcome_new';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
    public function SendApplicationMail($req){
        $applicationtype = implode(",",$req['application_type']);
        $applicationtype = str_replace(1, "referral", $applicationtype);
        $applicationtype = str_replace(2, "reseller", $applicationtype);
        
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = 'https://www.muvi.com/themes/signup/images/muvi_studio_logo.png';
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left"><p>Hi,</p>
                    <p><strong>'.$req['name'].'</strong> has submitted the following application</p>
                    <p style="display:block;margin:0 0 17px">
                        Application Type: <strong><span mc:edit="applicationtype">' . $applicationtype . '</span></strong><br/>
                        Company Name: <strong><span mc:edit="name">' . @$req['name'] . '</span></strong><br/>
                        Industry: <strong><span mc:edit="industry">' . @$req['industry'] . '</span></strong><br/>
                        Primary Contact: <strong><span mc:edit="email">' . @$req['company_name'] . '</span></strong><br/>
                        phone: <strong><span mc:edit="phone">' . @$req['phone'] . '</span></strong><br/>
                        No of Customers: <strong><span mc:edit="email">' . @$req['numberofcustomer'] . '</span></strong><br/>
                        Reason: <strong><span mc:edit="reason">' . @$req['reason'] . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            array( 'name' => 'website_name', 'content' => $site_url),
            array( 'name' => 'logo', 'content' => $logo),
            array( 'name' => 'mailcontent', 'content' => $admincontent),
        );
        if (HOST_IP != '52.0.232.150') {
            $adminGroupEmail = array(
                    array('email' => 'manas@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        }else{
            $adminGroupEmail = array(
                    array('email' => 'studio@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        }
        $autosignup_from = array('email' => 'studio@muvi.com', 'name' => 'Muvi');
        
        $adminSubject = "New reseller/referral application";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'sdk_user_welcome_new';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
    }
    public function SendMailtoApplier($req){
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = 'https://www.muvi.com/themes/signup/images/muvi_studio_logo.png';
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                    <p>Dear <strong>'.$req['name'].'</strong>,</p>
                    <p style="display:block;margin:0 0 17px">
                        Thank you for your partnership application with Muvi.<br> 
                        We are reviewing your application, you will hear from us within 48 hours.
                    </p>
                    <p>&nbsp;</p>
                    <p>Regards,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            array( 'name' => 'website_name', 'content' => $site_url),
            array( 'name' => 'logo', 'content' => $logo),
            array( 'name' => 'mailcontent', 'content' => $admincontent),
        );
        $adminGroupEmail = array(
                    array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to')
                );
        $autosignup_from = array('email' => 'partners@muvi.com', 'name' => 'Muvi');
        
        $adminSubject = "Thank you for partner application";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'sdk_user_welcome_new';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
    }
    public function addReferrermail($req = array(),$resellername,$studio){
        $StudioName = $studio->name;
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = 'https://www.muvi.com/themes/signup/images/muvi_studio_logo.png';
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left"><p>Hi,</p>
                    <p><strong>'.$resellername.'</strong> has added the following referral</p>
                    <p style="display:block;margin:0 0 17px">
                        Company Name: <strong><span mc:edit="name">' . ucfirst(@$req['company_name']) . '</span></strong><br/>
                        Primary Contact: <strong><span mc:edit="email">' . @$req['primary_contact'] . '</span></strong><br/>
                        Email Address: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                        Phone Number: <strong><span mc:edit="email">' . @$req['phone'] . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            array( 'name' => 'website_name', 'content' => $site_url),
            array( 'name' => 'logo', 'content' => $logo),
            array( 'name' => 'mailcontent', 'content' => $admincontent),
        );
        if (HOST_IP != '52.0.232.150') {
            $adminGroupEmail = array(
                    array('email' => 'manas@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        }else{
            $adminGroupEmail = array(
                    array('email' => 'studio@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
        }
        $autosignup_from = array('email' => 'studio@muvi.com', 'name' => 'Muvi');
        
        $adminSubject = "New referral";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'sdk_user_welcome_new';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }    
    public function mailToResellerscustomer($req = array(),$studio=''){
        $site_url = Yii::app()->getBaseUrl(true);
        if($studio){
            if($studio['id']==1){//for sony
                $StudioName = 'Sony';
                $logo = 'https://www.muvi.com/images/Sony_Logo.png';
            }else{
                $StudioName = $studio['name'];
                $logo = Yii::app()->getBaseUrl(true) . Yii::app()->common->getLogoFavPath($studio['id']);
            }
            $domain = $studio['domain'];
        }else{
            $StudioName = 'Muvi';
            $logo = 'https://www.muvi.com/themes/signup/images/muvi_studio_logo.png';
            $domain = 'studio.muvi.com';
        }
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="'.$StudioName.'" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                    <p>Dear '.@$req['name'].',</p>
                    <p>Congratulations on your first step towards launching your own branded VOD platform!</p>
                    <p style="display:block;margin:0 0 17px">
                        Your VOD platform\'s Admin Panel:: <strong><span mc:edit="website">http://'.$domain.'</span></strong><br/>
                        Login: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                        Password: <strong><span mc:edit="email">' . @$req['password'] . '</span></strong><br/>
                    </p>
                    <p>You can change your password after you login<br>
Contact us anytime you have any questions, we will reply ASAP.
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team '.$StudioName.'</p></div>';
        $params = array(
            array( 'name' => 'website_name', 'content' => $site_url),
            array( 'name' => 'logo', 'content' => $logo),
            array( 'name' => 'mailcontent', 'content' => $admincontent),
        );

        $adminGroupEmail = array(
                    array('email' => $req['email'], 'name' => $req['name'], 'type' => 'to')
                );
        $autosignup_from = array('email' => 'studio@muvi.com', 'name' => 'Muvi');
        
        $adminSubject = "Getting started with $StudioName";
        $mailAddress = array(
            'subject' => $adminSubject,
            'from_email' => $autosignup_from['email'],
            'from_name' => $autosignup_from['name'],
            'to' => $adminGroupEmail
        );
        $template_name = 'sdk_user_welcome_new';
        $returnVal = $this->sendMandrilEmail($template_name, $params, $mailAddress);
        return $returnVal;
    }
}