<?php
class AppBanners extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'app_banners';
    }
    public function getAppBanners($studio_id){
        $banners = $this->findAllByAttributes(array('studio_id' => $studio_id),array('select'=>	'id, image_name, id_seq, banner_text, banner_url', 'order'=>'id_seq ASC')); 
        return $banners;
    }  
    public function getMaxOrd($studio_id = false){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $bnr = $this->find(array(
            'select' => 'id_seq',
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id_seq DESC',
            'limit' => '1'
        ));   
        if(isset($bnr) && count($bnr)){
            return (int) $bnr->id_seq;
        }
        else{
            return 0;
        }      
    }
}
