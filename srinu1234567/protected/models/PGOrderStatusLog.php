<?php
class PGOrderStatusLog extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'pg_order_status_log';
    }
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function insertStatusLog($studio_id,$data,$ip){
        $this->studio_id = $studio_id;
        $this->from_status = $data['from_status'];
        $this->to_status = $data['to_status'];
        $this->comments = addslashes($data['comments']);
        $this->created_date = gmdate('Y-m-d H:i:s');//new CDbExpression("NOW()");
        $this->orderdetailsid = $data['orderdetailsid'];
        $this->ip = $ip;
        if($this->save()){
           $last_inserted_id=$this->id;
           //check copytoall option
           if($data['copytoall']==1){
                //get the product id
                $ord = "select order_id from pg_order_details WHERE id=" .$data['orderdetailsid'];
                $order = Yii::app()->db->createCommand($ord)->queryAll();
                //get all the id of this order
                $allid = "select id from pg_order_details WHERE order_id=" .$order[0]['order_id'];
                $allidarr = Yii::app()->db->createCommand($allid)->queryAll();
                foreach($allidarr as $val){
                    $copyids .="'" . $val['id'] . "',";
                }
                $copyids = rtrim($copyids, ',');
                $qry = "UPDATE pg_order_details SET item_status=".$data['to_status'].",cds_tracking='".$data['cds_tracking']."',tracking_url='".$data['tracking_url']."' WHERE id IN (".$copyids.")";
           }else{
                $qry = "UPDATE pg_order_details SET item_status=".$data['to_status'].",cds_tracking='".$data['cds_tracking']."',tracking_url='".$data['tracking_url']."' WHERE id='".$data['orderdetailsid']."'";
           }
           Yii::app()->db->createCommand($qry)->execute();
        }
        return $last_inserted_id;
    }
    public function getOrderStatusLog($id){
        $status_arr = Yii::app()->db->createCommand("SELECT * FROM pg_order_status_log WHERE orderdetailsid = {$id} ORDER BY id ASC")->queryAll();
        return $status_arr;
    }
}
