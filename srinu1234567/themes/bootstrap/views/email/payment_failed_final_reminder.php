<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"><?php echo $param['logo']; ?></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
							<p style="display:block;margin:0 0 17px">
								Dear <span mc:edit="name"><?php echo $param['name']; ?></span>,
							</p>              
							<p style="display:block;margin:0 0 17px">
								This is a  second and final reminder for the failure of <span mc:edit="plan"><?php echo $param['plan']; ?></span> payment. 
							</p>
							<p style="display:block;margin:0 0 17px">
								We have not been able to charge your card <span mc:edit="card_last_fourdigit"><?php echo $param['card_last_fourdigit']; ?></span> for the <span mc:edit="plan"><?php echo $param['plan']; ?></span> payment. Please update your card details by <span mc:edit="cancelled_date"><?php echo $param['cancelled_date']; ?></span>, failing to do so will cancel your Muvi subscription. All of your data are removed from our servers after your account is cancelled. 
							</p>
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%;font-family:helvetica,Arial;">
                            <tbody>
                                <tr>
                                    <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left;width:70%;">
                                        Regards,
                                        <p style="font-size:14px;margin:2px 0px">
                                            Team Muvi
                                        </p>
                                    </td>
                             		<td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"><?php echo $param['fb_link']; ?></span>&nbsp;<span mc:edit="gplus_link"><?php echo $param['gplus_link']; ?></span>&nbsp;<span mc:edit="twitter_link"><?php echo $param['twitter_link']; ?></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>