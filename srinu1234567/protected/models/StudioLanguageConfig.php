<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class StudioLanguageConfig extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studio_language_config';
    }
    
    public function getconfigvalue($key) {
        $studio_id = Yii::app()->common->getStudiosId();
        $data = Yii::app()->db->createCommand()
                ->select('config_value')
                ->from($this->tableName())
                ->where('studio_id = :studio_id AND config_key = :config_key', array(':studio_id' => $studio_id, ':config_key' => $key))
                ->queryRow();
        return $data;
    }
    public function getconfigvalueForStudio($studio_id,$key) {
        $data = Yii::app()->db->createCommand()
                ->select('config_value')
                ->from($this->tableName())
                ->where('studio_id = :studio_id AND config_key = :config_key', array(':studio_id' => $studio_id, ':config_key' => $key))
                ->queryRow();
        return $data;
    }
    
    public function getConfig($studio_id, $key){
        $config = $this->find('studio_id = :studio_id AND config_key=:config_key',array(':studio_id'=>$studio_id,":config_key"=>trim($key)));
        return $config;
    }    
}
?>