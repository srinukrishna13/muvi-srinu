<div class="wrapper">
    <div class="container home-page-studio">
        <h2 class="btm-bdr">Muvi</h2>
        <h3>Enables content owners <span class="blue">maximize revenue</span> from their content by offering several tools <br />
            that help find the right audience and increase the reach of their content.</h3>
    </div>
</div>

<div class="wrapper grybg">
    <div class="container home-page-customers">
        <h2 class="btm-bdr">Our Partners</h2>
        <ul>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/maa-tv.png" alt="" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/iskon.png" alt="Iskon" /></li>
			<li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/funkara_logo.png" alt="funkara-films" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/fountain-films.png" alt="" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/sepl.png" alt="" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/movie-tee-vee.png" alt="" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/pocket-films.png" alt="" /></li>                        
        </ul>
    </div>
</div> 

<div class="container" id="page">
    <h2 class="btm-bdr">Our Solutions</h2>
    <div class="sols">
        <div class="span4">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/streaming-app.png" alt="streaming-app" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/sdk">Streaming Website/App</a></h3>
            <p>Muvi SDK offers a streaming website and mobile app on your name (www.yourname.com). Get it up and running in minutes, at ZERO cost. 
                Muvi  takes care of everything including hosting, streaming, DRM, payment gateway and maintenance.</p>
        </div>
        <div class="span4">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/monetize.png" alt="monetize" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/muvi">Individual Content</a></h3>
            <p><a href="http://www.muvi.com" class="blue" target="_blank">Muvi.com</a> is a social content discovery platform for movies and TV 
                shows. Content owners upload their content to directly sell to the audience who pays to watch it. Unlike YouTube and other 
                streaming platforms, Muvi.com helps you find the exact audience whose taste matches with your content.</p>
        </div>         
        <div class="span4">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/digital-asset.png" alt="digital-asset" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/dam">Digital Asset Management</a></h3>
            <p>Store all your digital assets in the cloud. Muvi DAM (Digital Asset Management) provides content creators and aggregates a secure, 
                cost-effective cloud-based solution for storage, conversion and distribution of their media content.</p>
        </div>            

    </div>
</div>   
<div class="container blogsection">
    <div class="span6">
        <h2 class="btm-bdr">Latest Blogs</h2>
        <?php
            if(HOST_IP!='127.0.0.1' && HOST_IP!='52.0.64.95'){
                query_posts('posts_per_page=1');
                while ( have_posts() ) {
                    the_post();
                    ?>

                    <article>
                        <header>
                            <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="blue"><?php the_title(); ?></a></h3>
                        </header><!-- .entry-header -->

                        <div>
                            <?php the_excerpt() ?>
                        </div><!-- .entry-content -->
                        <a href="<?php echo home_url(); ?>" title="Goto Blogs"><?php /*the_title();*/ echo 'Goto Blogs'?> &raquo;</a>
                    </article>

                <?php
                }
            }
            ?>
    </div>
    <div class="span6 pull-right">
        <h2 class="btm-bdr">Recent Tweets</h2>
            <a class="twitter-timeline"  href="https://twitter.com/muvistudiob2b" data-widget-id="538237511214956544">Tweets by @muvistudiob2b</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>          
    </div>
</div>

