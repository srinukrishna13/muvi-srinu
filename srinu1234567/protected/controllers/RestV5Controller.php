<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';
class RestV5Controller extends Controller {
    public $studio_id = '';
    public $items = array();
    public $code = '';
	public $msg_code = '';
	
    public function init() {
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
        $headerinfo = getallheaders();
        foreach ($headerinfo AS $key => $val) {
            $_REQUEST[$key] = $val;
        }
        return true;
    }
    
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $currentAction = strtolower($action->id);
        if($currentAction != 'getstudioauthkey'){
            $success = self::validateOauth();
            if (!$success)
                self::afterAction();
        }
        $redirect_url = Yii::app()->getBaseUrl(true);
        return true;
    }

    protected function afterAction($action) {
        ob_clean();
        ob_flush();

        $response = array();
        if (empty($this->msg_code)) {			
            $config_message = parse_ini_file($_SERVER["DOCUMENT_ROOT"] . "/protected/config/api_config_message.ini", true);			
            $config_code = $config_message['api_code'][$this->code];        //Use your language here			
        } else {			
            $config_code = $this->msg_code;
        }		
        $response['msg'] =  TranslateKeyword::model()->getKeyValue(@$_REQUEST['lang_code'], $config_code, $this->studio_id);;
        $response['code'] = $this->code;
        $response['status'] = (intval($this->code) == 200) ? 'SUCCESS' : 'FAILURE';
        $response['items'] = (array) $this->items;

        header('HTTP/1.1 ' . $this->code . ' ' . $response['msg']);
        header('Content-type: application/json; charset=utf-8');
        header('X-Powered-By: Muvi <support@muvi.com>');
        echo json_encode($response);
        exit;
    }

    function getTransData($lang_code='en', $msg_code){
        $studio = Studio::model()->findByPk($this->studio_id,array('select'=>'theme'));
        $theme = $studio->theme;
        if(file_exists(ROOT_DIR."languages/".$theme."/".trim($lang_code).".php")){
           $lang = include( ROOT_DIR."languages/".$theme."/".trim($lang_code).".php");
        }elseif(file_exists(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php')){
            $lang = include(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php');
        }else{
            $lang = include(ROOT_DIR . 'languages/studio/en.php');
        }
        $language = Yii::app()->controller->getAllTranslation($lang, $this->studio_id, $msg_code);
        return $language;
    }

    public function encrypt($value) {
        $enc = new bCrypt();
        return $enc->hash($value);
    }

    /**
     * @method private ValidateOauth() Validate the Oauth token if its registered to our database or not
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     */
    function validateOauth() {
        if (isset($_REQUEST) && (isset($_REQUEST['authToken']) || isset($_REQUEST['authtoken']) || isset($_REQUEST['muvi_token']))) {
            if (isset($_REQUEST['muvi_token']) && trim($_REQUEST['muvi_token'])) {
                $muvi_token = explode('-', $_REQUEST['muvi_token']);
                $authToken = $muvi_token[1];
            } else {
                $authToken = (isset($_REQUEST['authtoken'])) ? trim($_REQUEST['authtoken']) : $_REQUEST['authToken'];
            }
            $referer = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer = parse_url($_SERVER['HTTP_REFERER']);
                $referer = $referer['host'];
            }
            $data = Yii::app()->db->createCommand()
                    ->select(' * ')
                    ->from("oauth_registration")
                    ->where('oauth_token=:oauth_token', array(':oauth_token' => $authToken))
                    ->queryRow();
            if ($data) {
                $data = (object) $data;
                if ($data->request_domain && ($data->request_domain != $referer)) {
                    $this->code = 601;
                    return false;
                }
                if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
                    $this->code = 602;
                    return false;
                }
                $this->studio_id = $data->studio_id;
                return true;
            } else {
                $this->code = 601;
            }
        } else {
            $this->code = 600;
        }
        return false;
    }

	 /**
     * @method private login() Get login parameter from API request and Checks login and return response
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $email Registered Email
     * @param string $password Login password for the user
     */
    function actionLogin($req = array()) {
        if ($req) {
            $_REQUEST = $req;
        }
        $this->code = 406;
        if (isset($_REQUEST) && isset($_REQUEST['email']) && isset($_REQUEST['password'])) {
            $pwd = trim(@$_REQUEST['password']);$email = trim(@$_REQUEST['email']);$device_type = @$_REQUEST['device_type'];
            $userData = (object) Yii::app()->db->createCommand()->select('id, email, encrypted_password, display_name, nick_name, add_video_log')->from('sdk_users')->where("email = '" . $email . "' AND studio_id = '" . $this->studio_id . "' AND status = 1 AND is_deleted != 1")->order("id DESC")->queryRow();
            if (!empty($userData)) {
                $enc = new bCrypt();    //For encryption of password                                
                if ($enc->verify(@$pwd, $userData->encrypted_password)) {
                    $data = $this->getUserData($userData, 1, 1);
                    if ($userData->add_video_log) {
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                        $device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                        $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                        $data['login_history_id'] = (int) $this->save_login_history($this->studio_id, $userData->id, $google_id, $device_id, $device_type, $ip_address);
                    }
                    $this->code = 200; 
                    $this->items = $data; 
                    return true;
                }
            }
        }
        return true;
    }
    
    public function save_login_history($studio_id, $user_id, $google_id = '', $device_id = '', $device_type = '', $ip_address = ''){
        $login_history = new LoginHistory();
        $login_history->studio_id = $studio_id;
        $login_history->user_id = $user_id;
        $login_history->login_at = new CDbExpression("NOW()");
        $login_history->logout_at = "0000-00-00 00:00:00";
        $login_history->last_login_check = new CDbExpression("NOW()");
        $login_history->google_id = $google_id;
        $login_history->device_id = $device_id;
        $login_history->device_type = $device_type;
        $login_history->ip = $ip_address;
        $login_history->save(); 
        return $login_history->id;
    }
    
     /*By Biswajit das(biswajitdas@muvi.com)(for check whether user login or not)*/
    public function actionCheckIfUserLoggedIn(){
        $loginD = new LoginHistory();                
        $loginData = $loginD->findByAttributes(array('studio_id' => $this->studio_id, 'user_id' => $_REQUEST['user_id'], 'device_id' =>$_REQUEST['device_id'], 'device_type' =>$_REQUEST['device_type']));
        $this->code = 200;
        $this->items['is_login'] = $loginData ? '1' : '0';
    }
    
   public function actionLogoutAll(){
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND is_deleted !=1', array(':email' => $_REQUEST['email'], ':studio_id' => $this->studio_id, ':status' => 1));
            if ($userData) {
                $user_id = $userData->id;
                $sql = "SELECT device_type, google_id FROM login_history WHERE studio_id={$this->studio_id} and user_id={$user_id} AND (device_type='1' OR device_type='2') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
                $rids = Yii::app()->db->createCommand($sql)->queryAll();
                if (!empty($rids)) {
                    foreach ($rids as $ids) {
                        if ($ids['device_type'] == 1)
                            $reg_ids [] = $ids['google_id'];
                        else
                            $reg_ids2 [] = $ids['google_id'];
                    }
                }
                $translate = $this->getTransData(@$_REQUEST['lang_code'], $this->studio_id);
                $push = new Push();
                $push->setTitle($user_id);
                $push->setMessage($translate['logged_out_from_all_devices']);
                $push->sendMultiple($reg_ids, $push->getPushAndroid());
                $push->sendMultipleNotify($reg_ids2, $push->getPushIos());
                LoginHistory::model()->logoutUser($this->studio_id, $user_id);
                $this->code = 200;
                $this->msg_code = 'logged_out_from_all_devices';
            } else {
                $this->code = 752;
            }
        } else {
            $this->code = 757;
        }
    }  
   public function actionUpdateGoogleid(){
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['device_id'])) {
            $loginD = new LoginHistory();                
            $loginData = $loginD->findByAttributes(array('studio_id' => $this->studio_id, 'user_id' => $_REQUEST['user_id'], 'device_id' =>$_REQUEST['device_id'], 'logout_at'=>'0000-00-00 00:00:00'));
            if ($loginData){
                $loginData->google_id = $_REQUEST['google_id'];
                $loginData->save();
                $this->code = 200;
            } else {
                $this->code = 703;
            }
        } else {
            $this->code = 672;
        }   
    }
    /* By Biswajit Parida For updating login history when logout from mobile apps */
    public function actionLogout(){
        if(isset($_REQUEST['login_history_id']) && $_REQUEST['login_history_id']!=""){
            $login_history = LoginHistory::model()->findByPk($_REQUEST['login_history_id']);
            $login_history->logout_at = date('Y-m-d H:i:s');
            $login_history->save();
            $this->code = 200;
        }else{
            $this->code = 679;
        }
    }
    /**
     * @method private getContentList() Get the list of contents based on its content_type permalink
     * @author GDR<support@muvi.com>VideoLogs

     * @return json Returns the list of data in json format
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionGetContentList() {
        if ($_REQUEST['permalink']) {
			$language_id = 20;
			if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != 'en') {
				$language_id = Yii::app()->custom->getLanguage_id($_REQUEST['lang_code']);
			}
			$translate = $this->getTransData($lang_code, $this->studio_id);
			$menuItemInfo = MenuItem::model()->find('studio_id=:studio_id AND permalink=:permalink', array(':studio_id' => $this->studio_id, ':permalink' => $_REQUEST['permalink']));
            if ($menuItemInfo) {
                $studio_id = $this->studio_id;
                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
				$order = '';
				$cond = ' ';
                $offset = 0;
                if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }
				$pdo_cond_arr = array(':studio_id' => $studio_id, ':content_category_value' => $menuItemInfo->value);

				$_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                if (isset($_REQUEST['orderby']) && $_REQUEST['orderby']!='') {
                    $order = $_REQUEST['orderby'];
                    if ($_REQUEST['orderby'] == 'lastupload') {
                        $orderby = "  M.last_updated_date DESC";
                        $neworderby = " P.last_updated_date DESC ";
                    } else if ($_REQUEST['orderby'] == 'releasedate') {
						$orderby = $neworderby = " P.release_date DESC ";
                    } else if ($_REQUEST['orderby'] == 'sortasc') {
						$orderby = $neworderby = " P.name ASC ";
                    } else if ($_REQUEST['orderby'] == 'sortdesc') {
						$orderby = $neworderby = " P.name DESC ";
                    }
				} else {
					$orderby = "  M.last_updated_date DESC ";
					$neworderby = " P.last_updated_date DESC ";
					$orderData = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ', array(':studio_id' => $studio_id, ':category_value' => $menuItemInfo->value));
					if (@$orderData->method) {
                    if(@$orderData->orderby_flag==1){// most viewed
                        $mostviewed = 1;
                        $neworderby = " Q.cnt DESC";
                    }else if(@$orderData->orderby_flag==2){//Alphabetic A-Z
							$orderby = $neworderby = " P.name ASC ";
                    }else if(@$orderData->orderby_flag==3){//Alphabetic Z-A
							$orderby = $neworderby = " P.name DESC ";
                    }                    
                }else if($orderData  && $orderData->ordered_stream_ids){
						$neworderby = $orderby = " FIELD(movie_stream_id," . $orderData->ordered_stream_ids . ")";
                }
				}
                if (@$_REQUEST['genre']) {
					if(is_array($_REQUEST['genre'])){
						$cond .= " AND (";
						foreach ($_REQUEST['genre'] AS $gkey => $gval){
							if($gkey){
								$cond .= " OR "; 
							 }
							 $cond .= " (genre LIKE ('%" . $gval . "%'))";
						}
						$cond .= " ) ";
					}else{
						$cond .= " AND genre LIKE ('%" . $_REQUEST['genre'] . "%')";
					}
                }
				$cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                if($_REQUEST['prev_dev']){
                    $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id,P.uniq_id AS muvi_uniq_id,P.content_type_id, P.ppv_plan_id,P.permalink,P.name,P.content_type_id,M.full_movie,P.story,P.genre,P.release_date,P.content_types_id,M.is_converted, M.full_movie')
                        ->from('movie_streams M,films P')
                        ->where('M.movie_id = P.id AND M.studio_id=:studio_id AND (P.content_category_value & :content_category_value) AND is_episode=0 AND P.parent_id=0 AND M.episode_parent_id=0  ' . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                }else{
                    /* Add Geo block to listing by manas@muvi.com*/
                    if(isset($_REQUEST['country']) && $_REQUEST['country']){
                        $country = $_REQUEST['country'];
                    }else{
                        $visitor_loc = Yii::app()->common->getVisitorLocation();
                        $country = $visitor_loc['country'];
                    }
                    $sql_data1 = "SELECT M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id, M.is_episode,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted,M.last_updated_date"
                            . " FROM movie_streams M,films F WHERE "
                            . "M.movie_id = F.id AND M.studio_id=".$studio_id." AND (F.content_category_value & ".$menuItemInfo->value.") AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0 " . $cond;
                    $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = ".$studio_id." AND sc.country_code='{$country}'";
                    $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (".$sql_data1.") AS t LEFT JOIN (".$sql_geo.") AS g ON t.movie_id = g.movieid) AS P ";                     
                    if($mostviewed){
                        $sql_data.=" LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=".$studio_id." GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
                    }            
                    $sql_data.=" WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY " . $neworderby. " LIMIT " . $limit . " OFFSET " . $offset;
                    $command = Yii::app()->db->createCommand($sql_data);
                }                
                $list = $command->queryAll();
				$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				$is_payment_gateway_exist = 0;
				$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				if (isset($payment_gateway) && !empty($payment_gateway)) {
					$is_payment_gateway_exist = 1;
				}
				//Get Posters for the Movies 
				$movieids = '';
				$movieList = '';
				if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
					//Retrive the total counts based on deviceType 
					$countQuery  = Yii::app()->db->createCommand()
						->select('COUNT(DISTINCT F.id) AS cnt')
						->from('movie_streams M,films F')
						->where('M.movie_id = F.id AND M.studio_id=:studio_id AND (F.content_category_value & :content_category_value) AND (IF(F.content_types_id =3,is_episode=1, is_episode=0)) AND (F.content_types_id =4 OR (M.is_converted =1 AND M.full_movie !=\'\')) AND F.parent_id=0 AND M.episode_parent_id=0 ' . $cond, $pdo_cond_arr);
					$itemCount = $countQuery->queryAll();
					$item_count = @$itemCount[0]['cnt'];

					$newList = array();
					foreach ($list AS $k => $v) {
						if ($v['content_types_id'] == 3) {
							$epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 AND episode_parent_id=0 LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
                                                //Added by prakash on 1st feb 2017        
						} else if($v['content_types_id'] == 4){
							$epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
						} else if (($v['content_types_id']==1 || $v['content_types_id']==2 ) && ($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
							$newList[] = $list[$k];
						}
					}
					$list = array();
					$list = $newList;
				}
				$domainName= $this->getDomainName();
				$livestream_content_ids = '';
				foreach ($list AS $k => $v) {
					$movieids .="'" . $v['movie_id'] . "',";
					if($v['content_types_id'] == 4){
						$livestream_content_ids .="'" .$v['movie_id']. "',";
					}
					if ($v['content_types_id'] == 2 || $v['content_types_id'] == 4) {
						$defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
					} else {
						$defaultPoster = POSTER_URL . '/' . 'no-image-a.png';
					}
					$movie_id = $v['movie_id'];
					$v['poster_url'] = $defaultPoster;
					$list[$k]['poster_url'] = $defaultPoster;
                    $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0)?$v['is_episode']:0;
					$is_episode=0;
					$list[$k]['embeddedUrl'] = $domainName.'/embed/'.$v['movie_stream_uniq_id'];
					if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
						$director = '';
						$actor = '';
						$castsarr = $this->getCasts($v['movie_id'],'',$language_id,$this->studio_id);
						if ($castsarr) {
							$actor = $castsarr['actor'];
							$actor = trim($actor, ',');
							unset($castsarr['actor']);
							$director = $castsarr['director'];
							unset($castsarr['director']);
						}
						$list[$k]['actor'] = @$actor;
						$list[$k]['director'] = @$director;
						$list[$k]['cast_detail'] = @$castsarr;
					}
				}

				$movieids = rtrim($movieids, ',');
				$livestream_content_ids = rtrim($livestream_content_ids,',');
				$liveFeedUrls = array();
				if($livestream_content_ids){
					$StreamSql = " SELECT feed_url,movie_id,is_online FROM livestream where movie_id IN (".$livestream_content_ids .') AND studio_id=' . $this->studio_id . "  ";
					$streamData = Yii::app()->db->createCommand($StreamSql)->queryAll();
					foreach($streamData AS $skey=>$sval){
						$liveFeedUrls[$sval['movie_id']]['feed_url']= $sval['feed_url'];
						$liveFeedUrls[$sval['movie_id']]['is_online']= $sval['is_online'];
					}
				}
				if ($movieids) {
					$sql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id IN (".$movieids.") AND studio_id=".$studio_id." AND language_id=".$language_id;
					$otherlang1 = Yii::app()->db->createCommand($sql)->queryAll();
					foreach ($otherlang1 as $key => $val) {
						$otherlang[$val['parent_id']] = $val;
					}

					$psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movieids . ") AND (object_type='films' OR object_type='tvapp') ";
					$posterData = Yii::app()->db->createCommand($psql)->queryAll();
					if ($posterData) {
						$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
						foreach ($posterData AS $key => $val) {
							$posterUrl = '';
							if($val['object_type'] == 'films'){
								$posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/thumb/' . urlencode($val['poster_file_name']);
								$posters[$val['movie_id']] = $posterUrl;
								if(($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku'){
									$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
								}
							}else if(($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')){
								$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
							}
						}
					}
					//Get view count status
					$viewStatus = VideoLogs::model()->getViewStatus($movieids,$studio_id);
					if(@$viewStatus){
						$viewStatusArr = '';
						foreach ($viewStatus AS $key=>$val){
							$viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount']; 
							$viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount']; 
						}
					}
					foreach ($list as $key => $val) {
						if (isset($otherlang[$val['movie_id']])) {
							$list[$key]['name'] = $otherlang[$val['movie_id']]['name'];
							$list[$key]['story'] = $otherlang[$val['movie_id']]['story'];
							$list[$key]['genre'] = $otherlang[$val['movie_id']]['genre'];
							$list[$key]['censor_rating'] = $otherlang[$val['movie_id']]['censor_rating'];
						}
						if (isset($posters[$val['movie_id']])) {
							if(isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != ''){
								$list[$key]['poster_url_for_tv'] =$postersforTv[$val['movie_id']];
							}
							if (($val['content_types_id'] == 2) || ($val['content_types_id'] == 4)) {
									$posters[$val['movie_id']] = str_replace('/thumb/', '/episode/', $posters[$val['movie_id']]);
							}
							$list[$key]['poster_url'] = $posters[$val['movie_id']];
						}
						if(@$viewStatusArr[$val['movie_id']]){
							$list[$key]['viewStatus'] = $viewStatusArr[$val['movie_id']];
						}else{
							$list[$key]['viewStatus'] = array('viewcount'=>"0",'uniq_view_count'=>"0");
						}
						if($val['content_types_id']== 4){
							$list[$key]['feed_url'] = @$liveFeedUrls[$val['movie_id']]['feed_url'];
							$list[$key]['is_online'] = @$liveFeedUrls[$val['movie_id']]['is_online'];
						}
					}
				}
				$this->code = 200;
				$data['msg'] = $translate['btn_ok'];
				$data['movieList'] = @$list;
				if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
					$user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
					if ($user) {
						$favourite = Yii::app()->db->createCommand()
							->select('*')
							->from('user_favourite_list')
                                                     ->where('user_id=:user_id AND studio_id=:studio_id AND status=:status',array(':user_id' => $_REQUEST['user_id'], ':studio_id' => $this->studio_id, ':status' => 1))
							->queryAll();
						 $data['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
					 }
				 }                                 
				$data['orderby'] = @$order;
				$data['item_count'] = @$item_count;
				$data['limit'] = @$page_size;
				$data['Ads'] = $this->getStudioAds();
				$this->items = @$data;
            } else {
                $this->code = 605;
            }
        } else {
			$this->code = 604;
        }
    }

    /**
     * @method getstudioAds() It will return the add server details if enabled 
     * @author Gayadhar<support@muvi.com>
     * @return array array of details
     */
    function getStudioAds() {
        //Find the Ad Channel id if enabled for the studio
        $arr = array();
        $studioAds = StudioAds::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($studioAds) {
            if ($studioAds->ad_network_id == 1) {
                $data['Ads']['network'] = 'spotx';
            } elseif ($studioAds->ad_network_id == 2) {
                $arr['network'] = 'yume';
            }
            $arr['channelId'] = $studioAds->channel_id;
        }
        return $arr;
    }

  /**
     * @method private getepisodeDetails() Get the details of Episode
     * @author GDR<support@muvi.com>
     * @return json Returns the list of episodes
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
	 * @param int $has_video it will return those content which have video.
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionEpisodeDetails() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
			$language_id = 20;$ltFlag =0;
			if($lang_code && $lang_code!='en'){
				$language_id = Yii::app()->custom->getLanguage_id($lang_code);
			}
            $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.content_type_id,f.content_types_id,ms.id AS movie_stream_id,f.permalink')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND f.permalink=:permalink AND f.studio_id=:studio_id ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryRow();
		    if ($movie && (($movie['content_types_id'] == 3) || ($movie['content_types_id'] == 6))) {
                $movie_id = $movie['id'];
				if($language_id !=20){
                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id,0,$language_id,$this->studio_id);
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $movie['name']  = $langcontent['film'][$movie_id]->name;
                    $movie['story'] = $langcontent['film'][$movie_id]->story;
                    $movie['genre'] = $langcontent['film'][$movie_id]->genre;
                    $movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                }
				}	
                $cast = array();
                if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
                    $director = '';
                    $actor = '';
                    $castsarr = $this->getCasts($movie_id,'',$language_id,$this->studio_id);
                    if ($castsarr) {
                        $actor = $castsarr['actor'];
                        $actor = trim($actor, ',');
                        unset($castsarr['actor']);
                        $director = $castsarr['director'];
                        unset($castsarr['director']);
                    }
                    $cast['actor'] = @$actor;
                    $cast['director'] = @$director;
                    $cast['cast_detail'] = @$castsarr;
                }
                $pdo_cond_arr = array(':movie_id' => $movie_id, ":is_episode" => 1);
                if (isset($_REQUEST['series_number']) && $_REQUEST['series_number']) {
                    $cond = " AND series_number=:series_number";
                    $pdo_cond_arr[':series_number'] = $_REQUEST['series_number'];
                }
                $forRokuCond = '';
                if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
                    $forRokuCond = '  AND is_converted=:is_converted ';
                    $pdo_cond_arr[':is_converted'] = 1;
                }
				$content_order = StudioConfig::model()->getConfig($this->studio_id, 'multipart_content_setting')->config_value;            
				$order_type=($content_order==0)?'desc':'asc';
                $orderby = ' episode_number '.$order_type;
                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset'])) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }
                $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                $bucket = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
                $signedFolderPath = $folderPath['signedFolderPath'];
                $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";
                $list = array();
				$addQuery = '';
				$customFormObj = new CustomForms();
				$customData = $customFormObj->getFormCustomMetadat($this->studio_id, 4);
				if($customData){
					foreach ($customData AS $ckey => $cvalue){
						$addQuery = ", ".$cvalue['field_name'] ." AS ".addslashes($cvalue['f_id']);
						$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
					}
				}
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),id,embed_id AS movie_stream_uniq_id,full_movie,episode_number,video_resolution,episode_title,series_number,episode_date,episode_story,IF((full_movie != "" AND is_converted = 1),CONCAT(\'' . $video_url . '\',id,\'/\',full_movie),"") AS video_url,thirdparty_url,rolltype,roll_after,video_duration'.$addQuery)
                        ->from('movie_streams')
                        ->where('movie_id=:movie_id AND is_episode=:is_episode AND episode_parent_id = 0 ' . $forRokuCond . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                $list = $command->queryAll();

                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                if ($list) {
                    $domainName= $this->getDomainName();
                    $_REQUEST['is_mobile'] = 1;
					foreach($list AS $k => $v){
						$streamIds .= $v['id'].",";
					}
					$streamIds = trim($streamIds,',');
					// Get All Translated Content. 
					if($language_id != 20){
						$sql = "SELECT id,`movie_id`,`episode_title`,`episode_story`,episode_parent_id FROM movie_streams WHERE episode_parent_id IN(".$streamIds.") AND studio_id=".$studio_id." AND episode_language_id=".$language_id;
						$episodeData = Yii::app()->db->createCommand($sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
						if($episodeData){
							foreach ($episodeData as $ekey => $evalue) {
								$langcontent['episode'][$evalue->id] = $evalue;
								$ltFlag =1;
							}
						}
					}
					if($streamIds){
						$defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
						$postObj = new Poster();
						$pdata = $postObj->getPosterByids($streamIds,'moviestream',$this->studio_id,'episode');
					}
                    foreach ($list AS $k => $v) {
                        //$langcontent = Yii::app()->custom->getTranslatedContent($v['id'],1,$language_id,$this->studio_id);
                        if ($ltFlag && (array_key_exists($v['id'], $langcontent['episode']))) {
                            $list[$k]['episode_title']  = $langcontent['episode'][$v['id']]->episode_title;
                            $list[$k]['episode_story'] = $langcontent['episode'][$v['id']]->episode_story;
						}else{
							$list[$k]['episode_title']  = $v['episode_title'];
                            $list[$k]['episode_story'] = $v['episode_story'];
						}
                        $list[$k]['embeddedUrl'] = $domainName.'/embed/'.$v['movie_stream_uniq_id'];
                        $list[$k]['poster_url'] = $pdata[$v['id']]?$pdata[$v['id']]['poster_url']:$defaultPoster;
						
                        if(isset($_REQUEST['has_video']) && $_REQUEST['has_video'] == 1){
                            $posterUrlForTv = $this->getPoster($v['id'], 'moviestream', 'roku', $this->studio_id);
                            if($posterUrlForTv != ''){
                                $list[$k]['posterForTv'] = $posterUrlForTv;
                            }
                        }
                        $list[$k]['movieUrlForTv'] = '';
                        $list[$k]['video_url'] = '';
                        $list[$k]['resolution'] = array();
                        if($v['thirdparty_url'] != ""){
                            $info = pathinfo($v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8"){ 
                                $list[$k]['movieUrlForTv'] = $v['thirdparty_url'];
                                $list[$k]['video_url'] = $v['thirdparty_url'];
                            } 
                            $list[$k]['thirdparty_url']=$this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                        } else if ($v['video_url'] != '') {
                            $list[$k]['movieUrlForTv'] = $v['video_url'];
                            $multipleVideo = $this->getvideoResolution($v['video_resolution'], $v['video_url']);
                            $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $v['video_url']);
                            $videoToBeplayed12 = explode(",", $videoToBeplayed);
                            $list[$k]['video_url'] = $videoToBeplayed12[0];
                            $list[$k]['resolution'] = $multipleVideo;
                            //Ad Details
                            $list[$k]['adDetails'] = array();
                            if ($v['rolltype'] == 1) {
                                $list[$k]['adDetails'][] = 0;
                            } elseif ($v['rolltype'] == 2) {
                                $roleafterarr = explode(',', $v['roll_after']);
                                foreach ($roleafterarr AS $ktime => $vtime) {
                                    $list[$k]['adDetails'][] = $this->convertTimetoSec($vtime);
                                }
                            } elseif ($v['rolltype'] == 3) {
                                $list[$k]['adDetails'][] = $this->convertTimetoSec($v['video_duration']);
                            }
                        }
                    }
                }
                $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));

                $this->code = 200;
                $data['msg'] = $translate['btn_ok'];
                $data['name'] = $movie['name'];
                $data['muvi_uniq_id'] = $movie['muvi_uniq_id'];
				$data['custom_fields'] = $customArr;
                $data['is_ppv'] = $movie['is_ppv'];
                if (!empty($ppvarr)) {
                    $data['ppv_pricing'] = $ppvarr;
                    $data['currency'] = $currency->attributes;
                }
                $data['permalink'] = $movie['permalink'];
                $data['item_count'] = $item_count;
                $data['limit'] = $limit;
                $data['offset'] = $offset;
                $data['comments'] = $comments;
                $data['episode'] = $list;
                $data['cast'] = $cast;
				$this->items = $data;
            } else {
               $this->code = 608;
            }
        } else {
           $this->code = 607;
        }
    }

    /**
     * @method private getContentDetails() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns details of the content
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionGetContentDetails() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
			$language_id =20;
			if($lang_code && $lang_code !='en')
				$language_id = Yii::app()->custom->getLanguage_id($lang_code);
            
			$movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.content_types_id,ms.id AS movie_stream_id,ms.full_movie,ms.is_converted,ms.video_resolution,ms.embed_id AS movie_stream_uniq_id, f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.permalink,f.content_type_id,f.genre,f.release_date,f.censor_rating,f.story,ms.rolltype,ms.roll_after,ms.video_duration,ms.thirdparty_url,ms.is_episode')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND ((ms.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(ms.content_publish_date) = 0) OR (ms.content_publish_date <=NOW())) AND  f.permalink=:permalink AND  f.studio_id=:studio_id AND f.parent_id = 0 AND ms.episode_parent_id=0 ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryRow();            
        if(isset($_REQUEST['country']) && $_REQUEST['country']){
            $country = $_REQUEST['country'];
        }else{
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
        }
        if (Yii::app()->common->isGeoBlockContent(@$movie[0]['id'], @$movie[0]['movie_stream_id'],$this->studio_id,$country)) {
            if ($movie) {
                $thirdPartyUrl='';
                $domainName= $this->getDomainName();
                $movie['censor_rating'] = stripslashes($movie['censor_rating']);
                $movie_id = $movie['id'];
                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id,0,$language_id,$this->studio_id);
                $arg['studio_id'] = $this->studio_id;
                $arg['movie_id'] = $movie_id;
                $arg['content_types_id'] = $movie['content_types_id'];
                
                $isFreeContent = Yii::app()->common->isFreeContent($arg);
                $movie['isFreeContent'] = $isFreeContent;
                
                $trailerThirdpartyUrl='';$trailerUrl='';$embedTrailerUrl='';                                  
                $trailerData=movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id'=>$movie_id));
                if($trailerData['trailer_file_name']!=''){
                    $embedTrailerUrl=$domainName.'/embedTrailer/'.$movie['muvi_uniq_id'];
                } else if($trailerData['third_party_url']!=''){
					$trailerThirdpartyUrl=$this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
				}else if($trailerData['video_remote_url']!=''){
					$trailerUrl = $this->getTrailer($movie_id,"",$this->studio_id);
				}                  
                 
                $is_ppv = $is_advance = 0;
                if (intval($isFreeContent) == 0) {
                    $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    if (isset($payment_gateway) && !empty($payment_gateway)) {
                        if ($movie['is_converted'] == 1) {
                            $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
                            if (isset($ppv->id) && intval($ppv->id)) {
                                $is_ppv = 1;
                            } else {
                                $ppv = Yii::app()->common->getContentPaymentType($movie['content_types_id'], $movie['ppv_plan_id'], $this->studio_id);
                                $is_ppv = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
                            }
                        } else  {
                            $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id);
                            $is_advance = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
                        }
                    }
                }
                $ppvarr = array();
				$command = Yii::app()->db->createCommand()
						->select('default_currency_id,rating_activated')
						->from('studios')
						->where('id = ' . $this->studio_id);
				$studio = $command->queryAll();
                if (intval($is_ppv) || intval($is_advance)) {
                    $price = Yii::app()->common->getPPVPrices($ppv->id, $studio[0]['default_currency_id'], $_REQUEST['country']);
                    (array) $currency = Currency::model()->findByPk($price['currency_id']);

                    $ppvarr['id'] = $ppv->id;
                    $ppvarr['title'] = $ppv->title;
                    $ppvarr['pricing_id'] = $price['id'];
                    $ppvarr['price_for_unsubscribed'] = $price['price_for_unsubscribed'];
                    $ppvarr['price_for_subscribed'] = $price['price_for_subscribed'];
                    $ppvarr['show_unsubscribed'] = $price['show_unsubscribed'];
                    $ppvarr['season_unsubscribed'] = $price['season_unsubscribed'];
                    $ppvarr['episode_unsubscribed'] = $price['episode_unsubscribed'];
                    $ppvarr['show_subscribed'] = $price['show_subscribed'];
                    $ppvarr['season_subscribed'] = $price['season_subscribed'];
                    $ppvarr['episode_subscribed'] = $price['episode_subscribed'];

                    $validity = PpvValidity::model()->findByAttributes(array('studio_id' => $this->studio_id));

                    $ppvarr['validity_period'] = $validity->validity_period;
                    $ppvarr['validity_recurrence'] = $validity->validity_recurrence;

                    if (isset($ppv->content_types_id) && trim($ppv->content_types_id) == 3) {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
                        $ppvarr['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $ppvarr['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $ppvarr['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                    }

                    if (intval($is_ppv)) {
                        $movie['is_ppv'] = $is_ppv;
                    }
                    if (intval($is_advance)) {
                        $movie['is_advance'] = $is_advance;
                    }
                } else {
                    $movie['is_ppv'] = 0;
                    $movie['is_advance'] = 0;
                }

                $castsarr = $this->getCasts($movie_id,'',$language_id,$this->studio_id);
                if ($castsarr) {
                    $actor = $castsarr['actor'];
                    $actor = trim($actor, ',');
                    unset($castsarr['actor']);
                    $director = $castsarr['director'];
                    unset($castsarr['director']);
                }
                
                $cast_detail = $castsarr;
                $movie['actor'] = @$actor;
                $movie['director'] = @$director;
                $movie['cast_detail'] = @$castsarr;
                $movie['embeddedUrl'] = "";
                
                //Get view status 
                $viewStatus = VideoLogs::model()->getViewStatus($movie_id,$this->studio_id);
                $movie['viewStatus']['viewcount'] = @$viewStatus[0]['viewcount']?@$viewStatus[0]['viewcount']:"0";
                $movie['viewStatus']['uniq_view_count'] = @$viewStatus[0]['u_viewcount']?@$viewStatus[0]['u_viewcount']:"0";
                if($movie['thirdparty_url'] != ""){
                    $info = pathinfo($movie['thirdparty_url']);
                    if (@$info["extension"] == "m3u8"){ 
                        $movieUrlToBePlayed = $movie['thirdparty_url'];
                        $movieUrl = $movie['thirdparty_url'];
                    } 
                    $thirdPartyUrl=$this->getSrcFromThirdPartyUrl($movie['thirdparty_url']);
                    $movie['embeddedUrl'] = $domainName.'/embed/'.$movie['movie_stream_uniq_id'];
                }else if ($movie['full_movie'] != '' && $movie['is_converted'] == 1) {
                    $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                    $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
                    $signedFolderPath = $folderPath['signedFolderPath'];
                    $movieUrl = CDN_HTTP . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/" . $movie['movie_stream_id'] . "/" . urlencode($movie['full_movie']);
                    $movie['movieUrlForTv'] = $movieUrl;
                    $_REQUEST['is_mobile'] = 1;
                    $multipleVideo = $this->getvideoResolution($movie['video_resolution'], $movieUrl);
                    $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $movieUrl);
                    $videoToBeplayed12 = explode(",", $videoToBeplayed);
                    $movieUrlToBePlayed = $videoToBeplayed12[0];
                    $movie['embeddedUrl'] = $domainName.'/embed/'.$movie['movie_stream_uniq_id'];
                }                
                $movie['movieUrlForTv'] = @$movieUrl;
                $movie['movieUrl'] = @$movieUrlToBePlayed;        
                //Added thirdparty url
                $movie['thirdparty_url']=$thirdPartyUrl;               
                if ($movie['content_types_id'] == 4) {
                    $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id'=>$this->studio_id,':movie_id'=>$movie_id));
                    if($Ldata && $Ldata->feed_url){
                        $movie['embeddedUrl'] = $domainName.'/embed/livestream/'.$movie['muvi_uniq_id'];
                        $movie['feed_url'] = $Ldata->feed_url;
                    }					
				}
                $movie['resolution'] = @$multipleVideo;
                $MovieBanner = $this->getPoster($movie_id, 'topbanner', 'original', $this->studio_id);
                if ($movie['content_types_id'] == 2) {
                    $poster = $this->getPoster($movie_id, 'films', 'episode', $this->studio_id);
                } else {
                    $poster = $this->getPoster($movie_id, 'films', 'thumb', $this->studio_id);
                }
                if(isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku'){
                    $posterForTv = $this->getPoster($movie_id, 'films', 'roku', $this->studio_id);
                    if($posterForTv != ''){
                        $movie['posterForTv'] = $posterForTv;
                    }
                }
                $movie['banner'] = $MovieBanner;
                $movie['poster'] = @$poster;
                $movie_name = $movie['name'];
                if ($movie['content_types_id'] == 3) {
                    $EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);
                    $seasons = $this->getContentSeasons($movie_id, $this->studio_id);
                }
                $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));
                $movie['adDetails'] = array();
                if ($movie['rolltype'] == 1) {
                    $movie['adDetails'][] = 0;
                } elseif ($movie['rolltype'] == 2) {
                    $roleafterarr = explode(',', $movie['roll_after']);
                    foreach ($roleafterarr AS $tkey => $tval) {
                        $movie['adDetails'][] = $this->convertTimetoSec($tval);
                    }
                } elseif ($movie['rolltype'] == 3) {
                    $movie['adDetails'][] = $this->convertTimetoSec($movie['video_duration']);
                }
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $movie['name']  = $langcontent['film'][$movie_id]->name;
                    $movie['story'] = $langcontent['film'][$movie_id]->story;
                    $movie['genre'] = $langcontent['film'][$movie_id]->genre;
                    $movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                }
               
                $movie['trailerThirdpartyUrl']=$trailerThirdpartyUrl;
                $movie['trailerUrl']=$trailerUrl;
                $movie['embedTrailerUrl']=$embedTrailerUrl;
                $movie['is_episode'] = (isset($movie['is_episode']) && strlen($movie['is_episode']) > 0)?$movie['is_episode']:0;
               	if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
					$user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
					if ($user) {
						$favourite = Yii::app()->db->createCommand()
						->select('id')
						->from('user_favourite_list')
						->where('user_id=:user_id AND content_id=:content_id AND studio_id=:studio_id AND status=:status',array(':user_id' => $_REQUEST['user_id'],':content_id' => $movie['id'], ':studio_id' => $this->studio_id,':status'=>1))
						->queryAll();
						$movie['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
					}
				}
                $this->code = 200;
                $data['movie'] = $movie;
                if (!empty($ppvarr)) {
                    if (intval($is_ppv)) {
                        $data['ppv_pricing'] = $ppvarr;
                    }
                    if (intval($is_advance)) {
                        $data['adv_pricing'] = $ppvarr;
                    }
                    $data['currency'] = $currency->attributes;
                }                
                $data['comments'] = $comments;
                if($studio[0]['rating_activated'] == 1){
                    $data['rating'] = $this->actionRating($movie_id);
                }
                $data['epDetails'] = $EpDetails;
                if (@$seasons)
                    $data['seasons'] = $seasons;
            } else {
                $this->code = 605;
            }
			}else{
				$this->code = 770;
			}   
        } else {
            $this->code = 604;
        }
		$this->items = @$data;
    }

    protected function actionRating($content_id) {
        $rating_value = 0;
        $num_rating = 0;
        $rating = new ContentRating();
        $ratings = $rating->findAllByAttributes(array('studio_id' => $this->studio_id, 'content_id' => $content_id, 'status' => 1), array('order' => 'id desc'));
        foreach ($ratings as $rating) {
            $rating_value+= $rating->rating;
            $num_rating++;
        }
        $rat_val = 0;
        if ($num_rating > 0) {
            $rat_val = ($rating_value / $num_rating);
            $rat_val = round($rat_val, 1);
        }
        return $rat_val;
    }

    public function actionReviews() {
        if ($_REQUEST['content_id']) {
            $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 5;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $ratings = Yii::app()->db->createCommand()
                       ->select(' r.rating, r.review, r.created_date, r.user_id, u.display_name AS user_name ')
                       ->from("content_ratings r")
                       ->leftJoin('sdk_users u' , 'r.user_id=u.id')
                       ->where('r.studio_id = :studio_id AND r.content_id=:content_id AND r.status=1',array(':studio_id'=>$this->studio_id,':content_id'=>$_REQUEST['content_id']))
                       ->limit($limit)
                       ->offset($offset)
                       ->order('r.created_date DESC')
                       ->queryAll();
            foreach($ratings as $k => $rating){
                $ratings[$k]['profile_pic'] = $this->getProfilePicture($rating['user_id'], 'profilepicture');
                $ratings[$k]['content'] = substr($rating['review'], 0, 50);
                $ratings[$k]['date'] = Yii::app()->common->YYMMDD($rating['created_date']);
            }
            $this->code = 200;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = count($ratings);
            $data['review'] = $ratings;
            $this->items = $data;
        }else {
            $this->code = 607;
        }
    }

    /**
     * @method public Savereview()
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @return json message of success or failure 
     * @param int $content_id
     * @param int $rating
     * @param string $review_comment 
     * @param int $user_id
     */
    public function actionSavereview() {
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id'] > 0 && $_REQUEST['user_id'] > 0) {
            $rating = new ContentRating();
            $rate = $rating->countByAttributes(array('studio_id' => $this->studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $_REQUEST['user_id']));
            if ($rate > 0) {
                $this->code = 676;
            } else {
                if (isset($_REQUEST['rating']) && $_REQUEST['rating'] > 0) {
                    $rating->content_id = $_REQUEST['content_id'];
                    $rating->rating = round($_REQUEST['rating'], 1);
                    $rating->review = $_REQUEST['review_comment'];
                    $rating->created_date = new CDbExpression("NOW()");
                    $rating->studio_id = $this->studio_id;
                    $rating->user_ip = Yii::app()->getRequest()->getUserHostAddress();
                    $rating->user_id = $_REQUEST['user_id'];
                    $rating->status = 1;
                    $rating->save();
                    $this->code = 200;
                } else {
                    $this->code = 677;
                }
            }
        } else {
            $this->code = 678;
        }
    }

    /**
     * @method private searchData() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns details of the content
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionsearchData() {
        if (isset($_REQUEST['q']) && $_REQUEST['q']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate   = $this->getTransData($lang_code, $this->studio_id);
            $language_id = 20;
			if($lang_code && $lang_code!='en'){
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
			}
            if($language_id == 20){
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND M.episode_language_id = ' . $language_id . ') ';
            }else{
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND (IF (M.is_episode = 0, M.episode_language_id = 20 , M.episode_language_id=' . $language_id . '))) ';
            }
            $q = strtolower($_REQUEST['q']);
            $casts = Yii::app()->db->createCommand()
                    ->select('id,parent_id,language_id')
                    ->from('celebrities')
                    ->where('studio_id=:studio_id AND language_id=:language_id AND LOWER(name) LIKE  :search_string ', array(':studio_id' => $this->studio_id, ':search_string' => "%$q%", ':language_id' => $language_id))
                    ->queryAll();
            $cast_ids = array();
            $inCond = '';
            if (!empty($casts)) {
                foreach ($casts as $cast) {
                    if ($cast['parent_id'] > 0) {
                        $cast_ids[] = $cast['parent_id'];
                    } else {
                        $cast_ids[] = $cast['id'];
                    }
                }
            }
            if (!empty($cast_ids)) {
                $cast_ids = implode(',', $cast_ids);
                $movieIds = Yii::app()->db->createCommand()
                        ->select('GROUP_CONCAT(DISTINCT mc.movie_id) AS movie_ids')
                        ->from('movie_casts mc')
                        ->where('mc.celebrity_id IN (' . $cast_ids . ') ')
                        ->queryAll();
                if (@$movieIds[0]['movie_ids']) {
                    $movie_ids = $movieIds[0]['movie_ids'];
                    if ($language_id == 20) {
                        $film = Yii::app()->db->createCommand()
                                ->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
                                ->from('films f')
                                ->where('f.id IN (' . $movie_ids . ')')
                                ->queryAll();
                    } else {
                        $film = Yii::app()->db->createCommand()
                                ->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
                                ->from('films f')
                                ->where('f.parent_id IN (' . $movie_ids . ') AND language_id=' . $language_id)
                                ->queryAll();
                    }
                    if (!empty($film)) {
                        $inCond = " OR F.id IN(" . $film[0]['movie_ids'] . ") ";
                    }
                }
            }
            //Build the search query 
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $forRokuCond = '';
            if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
                $forRokuCond = " AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=''))) "; 
            }
            $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
            $signedFolderPath = $folderPath['signedFolderPath'];
            $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";

            $permalink = Yii::app()->getBaseUrl(TRUE) . '/';
            if ($_REQUEST['prev_dev']) {
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.search_parent_id '. $langcond .' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
                        ->order('F.name,M.episode_title')
                        ->limit($limit, $offset);
            } else {
                /* Add Geo block to listing by manas@muvi.com */
                if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                    $country = $_REQUEST['country'];
                } else {
                    $visitor_loc = Yii::app()->common->getVisitorLocation();
                    $country = $visitor_loc['country'];
                }
                $sql_data1 = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,F.parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
                        . ' FROM movie_streams M,films F WHERE '
                        . ' M.movie_id = F.search_parent_id  '. $langcond .' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . ' AND (F.name LIKE \'%' . $q . '%\' OR M.episode_title LIKE \'%' . $q . '%\'' . $inCond . ' ) ' . $forRokuCond;
                $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $this->studio_id . " AND sc.country_code='{$country}'";
                $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $limit . " OFFSET " . $offset;
                $command = Yii::app()->db->createCommand($sql_data);
                /* END */
            }
            $list = $command->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();

            if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
                //Retrive the total counts based on deviceType 
                $countQuery = Yii::app()->db->createCommand()
                        ->select('COUNT(*) as cnt')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.search_parent_id  '. $langcond .' AND M.studio_id=:studio_id AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=\'\'))) AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id));
                $itemCount = $countQuery->queryAll();
                $item_count = @$itemCount[0]['cnt'];
                $newList = array();
				
				foreach($list AS $ckey => $cval){
					if ($cval['content_types_id'] == 3 && $cval['is_episode'] == 0) {
						$mpp_ids .= $cval['movie_id'].",";
					}elseif($cval['content_types_id'] == 4){
						$ls_ids .= $cval['movie_id'].",";
					}	
				}
				$mpp_ids = trim($mpp_ids,',');
				$ls_ids = trim($ls_ids,',');
				if($mpp_ids){
					$epSql = " SELECT movie_id FROM movie_streams where movie_id IN (" . $mpp_ids . ') AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 GROUP BY movie_id";
                    $epData = Yii::app()->db->createCommand($epSql)->queryColumn();
				}
				if($ls_ids){
					$lsSql = " SELECT id FROM livestream where movie_id IN (" . $ls_ids . ')AND studio_id=' . $this->studio_id . "  LIMIT 1";
                    $lsData = Yii::app()->db->createCommand($lsSql)->queryColumn();
				}
                foreach ($list AS $k => $v) {
                    if ($v['content_types_id'] == 3 && $v['is_episode'] == 0) {
                        if (@$episodeData && (in_array($v['movie_id'],$episodeData))) {
                            $newList[] = $list[$k];
                        }
                    } else if ($v['content_types_id'] == 4) {
                        
                        if (@$lsData && (in_array($v['movie_id'],$lsData))) {
                            $newList[] = $list[$k];
                        }
                    } else if (($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
                        $newList[] = $list[$k];
                    }
                }
                $list = array();
                $list = $newList;
            }
            if ($list) {
                $view = array();$cnt = 1;$movieids = '';$stream_ids = '';
                $domainName = $this->getDomainName();
                foreach ($list AS $k => $v) {
                    $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0)?$v['is_episode']:0;
                    $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                    if($v['episode_parent_id'] > 0){
                        $v['movie_stream_id'] = $v['episode_parent_id'];
                        $list[$k]['movie_stream_id'] = $v['episode_parent_id'];
                    }
                    if ($v['is_episode']) {
                        $stream_ids .="'" . $v['movie_stream_id'] . "',";
                    } else {
                        $movieids .="'" . $v['movie_id'] . "',";
                        if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
                            $director = '';
                            $actor = '';
                            $castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
                            if ($castsarr) {
                                $actor = $castsarr['actor'];
                                $actor = trim($actor, ',');
                                unset($castsarr['actor']);
                                $director = $castsarr['director'];
                                unset($castsarr['director']);
                            }
                            $list[$k]['actor'] = @$actor;
                            $list[$k]['director'] = @$director;
                            $list[$k]['cast_detail'] = @$castsarr;
                        }
                    }
                    if ($v['content_types_id'] == 4) {
                        $liveTV[] = $v['movie_id'];
                    }
                    if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
                        if (@$v['thirdparty_url'] != "") {
                            $info = pathinfo(@$v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8") {
                                $list[$k]['movieUrlForTV'] = @$v['thirdparty_url'];
                            }
                        } else {
                            $list[$k]['movieUrlForTV'] = @$v['video_url'];
                        }
                    }
                    if ($v['thirdparty_url'] != "") {
                        $list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                    }
                }
                $viewcount = array();
                $movie_ids = rtrim($movieids, ',');
                $stream_ids = rtrim($stream_ids, ',');


                if ($movie_ids) {
                    $cast = MovieCast::model()->findAll(array("condition" => "movie_id  IN(" . $movie_ids . ')'));
                    foreach ($cast AS $key => $val) {
                        if (in_array('actor', json_decode($val['cast_type']))) {
                            $actor[$val['movie_id']] = @$actor[$val['movie_id']] . $val['celebrity']['name'] . ', ';
                        }
                    }
                    $psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movie_ids . ") AND (object_type='films' OR object_type='tvapp') ";
                    $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($posterData) {
                        $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
                        foreach ($posterData AS $key => $val) {
                            $size = 'thumb';
                            if ($val['object_type'] == 'films') {
                                $posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/' . $size . '/' . urlencode($val['poster_file_name']);
                                $posters[$val['movie_id']] = $posterUrl;
                                if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                                    $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
                                }
                            } else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
                                $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
                            }
                        }
                    }
                }
                $default_epp = POSTER_URL . '/' . 'no-image-h.png';
                $default_p = POSTER_URL . '/' . 'no-image-a.png';
                if (@$liveTV) {
                    $lcriteria = new CDbCriteria();
                    $lcriteria->select = "movie_id,IF(feed_type=1,'hls','rtmp') feed_type,feed_url,feed_method";
                    $lcriteria->addInCondition("movie_id", $liveTV);
                    $lcriteria->condition = "studio_id = $this->studio_id";
                    $lresult = Livestream::model()->findAll($lcriteria);
                    foreach ($lresult AS $key => $val) {
                        $liveTvs[$val->movie_id]['feed_url'] = $val->feed_url;
                        $liveTvs[$val->movie_id]['feed_type'] = $val->feed_type;
                        $liveTvs[$val->movie_id]['feed_method'] = $val->feed_method;
                    }
                }
                //Get view status for content other then episode
                $viewStatus = VideoLogs::model()->getViewStatus($movie_ids, $this->studio_id);

                if ($viewStatus) {
                    $viewStatusArr = '';
                    foreach ($viewStatus AS $key => $val) {
                        $viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
                        $viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                //Get view status for Episodes 
                $viewEpisodeStatus = VideoLogs::model()->getEpisodeViewStatus($stream_ids, $this->studio_id);
                if ($viewEpisodeStatus) {
                    $viewEpisodeStatusArr = '';
                    foreach ($viewEpisodeStatus AS $key => $val) {
                        $viewEpisodeStatusArr[$val['video_id']]['viewcount'] = $val['viewcount'];
                        $viewEpisodeStatusArr[$val['video_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                $default_status_view = array('viewcount' => "0", 'uniq_view_count' => "0");
                foreach ($list AS $key => $val) {
                    $casts = '';
                    if ($val['is_episode'] == 1) {
                        if ($language_id != 20 && $val['episode_parent_id'] > 0) {
                            $val['movie_stream_id'] = $val['episode_parent_id'];
                        }
                        //Get Posters for Episode
                        $posterUrlForTv = $this->getPoster($val['movie_stream_id'], 'moviestream', 'roku', $this->studio_id);
                        if ($posterUrlForTv != '') {
                            $list[$key]['posterForTv'] = $posterUrlForTv;
                        }
                        $poster_url = $this->getPoster($val['movie_stream_id'], 'moviestream', 'episode', $this->studio_id);
                        $list[$key]['viewStatus'] = @$viewEpisodeStatusArr[$val['movie_stream_id']] ? @$viewEpisodeStatusArr[$val['stream_id']] : $default_status_view;
                    } else {
                        if ($language_id != 20 && $val['parent_id'] > 0) {
                            $val['movie_id'] = $val['parent_id'];
                        }
                        $poster_url = $posters[$val['movie_id']] ? $posters[$val['movie_id']] : $default_p;
                        if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
                            $poster_url = str_replace('/thumb/', '/episode/', $poster_url);
                        }
                        $casts = $actor[$val['movie_id']] ? rtrim($actor[$val['movie_id']], ', ') : '';
                        $list[$key]['viewStatus'] = @$viewStatusArr[$val['movie_id']] ? @$viewStatusArr[$val['movie_id']] : $default_status_view;
                        if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
                            $list[$key]['posterForTv'] = $postersforTv[$val['movie_id']];
                        }
                    }
                    $list[$key]['poster_url'] = $poster_url;
                    if ($val['content_types_id'] == 4) {
                        $list[$key]['feed_type'] = $liveTvs[$val['movie_id']]['feed_type'];
                        $list[$key]['feed_url'] = $liveTvs[$val['movie_id']]['feed_url'];
                        $list[$key]['feed_method'] = $liveTvs[$val['movie_id']]['feed_method'];
                    }
                    $list[$key]['actor'] = $casts;
                            }
                $list[$key]['adDetails'] = array();
                if ($val['rolltype'] == 1) {
                    $list[$key]['adDetails'][] = 0;
                } elseif ($val['rolltype'] == 2) {
                    $roleafterarr = explode(',', $val['roll_after']);
                    foreach ($roleafterarr AS $tkey => $tval) {
                        $list[$key]['adDetails'][] = $this->convertTimetoSec($tval);
                    }
                } elseif ($val['rolltype'] == 3) {
                    $list[$key]['adDetails'][] = $this->convertTimetoSec($val['video_duration']);
                }
               
                  }


            $searchLogs = new SearchLog();
            $newlisting = array();
            $listings = $list;
            if (!empty($listings)) {
                foreach ($listings as $listnew) {
                    $searchLogs->setIsNewRecord(true);
                    $searchLogs->id = null;
                    $searchLogs->studio_id = $this->studio_id;
                    $searchLogs->search_string = $_REQUEST['q'];
                    $searchLogs->object_category = "content";
                    $searchLogs->object_id = $lists['movie_id'];
                    $searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                    $searchLogs->created_date = gmdate('Y-m-d H:i:s');
                    $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
                    $searchLogs->save();
                    $content_id = $list_id;
                    $is_episode = $listnew['is_episode'];
                    if ($is_episode == 1) {
                        $content_id = $stream_id;
                    }
                }
            } else {
                $searchLogs->studio_id = $this->studio_id;
                $searchLogs->search_string = $_REQUEST['q'];
                $searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                $searchLogs->created_date = gmdate('Y-m-d H:i:s');
                $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
                $searchLogs->save();
            }
            $this->code = 200;
            $data['msg'] = $translate['btn_ok'];
            $data['search'] = $list;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = $item_count;
            $data['Ads'] = $this->getStudioAds();
			$this->items = @$data;
        } else {
			$this->code = 609;
        }
    }

    /**
     * @method private forgotPassword() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns array of message and response code based on the action success/failure
     * @param string $email	Valid email of the user
     * @param string $oauthToken Auth token
     * 
     */
    public function actionforgotPassword() {
        $this->code = 610;
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $email = trim($_REQUEST['email']);
            $user = SdkUser::model()->find('studio_id = :studio_id AND email = :email', array(':studio_id' => $this->studio_id, ':email' => $email));
            $studio = Studio::model()->findByPk($this->studio_id);
            if ($user) {
                $enc = new bCrypt();
                $user->reset_password_token = $enc->hash($email);
                $user->save();
                $fb_link = $twitter_link = $gplus_link = '';
                $site_url = 'http://' . $studio->domain;
                $logo = '<a href="' . $site_url . '"><img src="' . Yii::app()->common->getLogoFavPath($this->studio_id) . '" /></a>';
                $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
                if ($studio->fb_link != '')
                    $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                if ($studio->tw_link != '')
                    $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                if ($studio->gp_link != '')
                    $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';

                $reset_link = $site_url . '/user/resetpassword?auth=' . $enc->hash($email) . '&email=' . $email;
                $studio_email = Yii::app()->common->getStudioEmail($this->studio_id);
                
                $template = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id' => $this->studio_id, ':type' => 'forgot_password'));
                if (!$template){
                    $template = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id' => 0, ':type' => 'forgot_password'));
                }
                //Subject
                $subject = $template['subject'];
                eval("\$subject = \"$subject\";");
                $content = (string) $template["content"];
                $breaks = array("<br />", "<br>", "<br/>");
                $content = htmlentities(str_ireplace($breaks, "\r\n", $content));
                eval("\$content = \"$content\";");
                $content = htmlspecialchars_decode($content);

                $params = array('website_name' => $studio->name,
                    'logo' => $logo,
                    'site_link' => $site_link,
                    'reset_link' => '<a href="' . $reset_link . '">' . $reset_link . '</a>',
                    'username' => $user->display_name,
                    'fb_link' => @$fb_link,
                    'twitter_link' => @$twitter_link,
                    'gplus_link' => @$gplus_link,
                    'supportemail' => $studio_email,
                    'website_address' => $studio->address,
                    'content' => $content
                );
                
                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/sdk_reset_password_user', array('params' => $params), true);
                $return_param = $this->sendmailViaAmazonsdk($thtml, $subject, $user->email, $studio_email, "", "", "", $studio->name);                //$return_param = $this->mandrilEmail($template_name, $params, $message);
                $this->code = 200;
                return true;
            } else {
                $this->code = 752;
            }
        }
        return true;
    }

    /**
     * @method public getSuscriptionPlans It will check if plan exists for a studio. If exists then return the plan lists
     * @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param String $country country code
	* @param String $lang_code language code
     */
	
    public function actionGetSuscriptionPlans() {
        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
        $language_id = Yii::app()->custom->getLanguage_id(@$_REQUEST['lang_code']);
        $data = array();
        
        $ret = Yii::app()->common->isPaymentGatwayAndPlanExists($this->studio_id, $default_currency_id, @$_REQUEST['country'], $language_id);
        if ($ret && !empty($ret)) {
            
            if (isset($ret['plans']) && !empty($ret['plans'])) {
                $plans = $ret['plans'];
                $default_plan_id = @$plans[0]->id;
                
                foreach ($plans AS $key => $val) {
                    if ($val->is_default) {
                        $default_plan_id = $val->id;
                    }
                        
                    $plan[$key] = $val->attributes;
                    $plan[$key]['price'] = $val->price;

                    (array) $currency = Currency::model()->findByPk($val->currency_id);
                    $plan[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
                }
				$this->code = 200;
                $data['plans'] = $plan;
                $data['default_plan'] = $default_plan_id;
            } else {
				$this->code = 648;
            }
        } else {
			$this->code = 612;
        }
		$this->items = $data;
    }

    /**
     * @method public authenticateCard It authenticate the credit card detial
     * @return json Return the json array of results
     * @param String authToken Auth token of the studio
     * @param string card_name Name as on Credit card
     * @param String card_number Card Number 
     * @param String cvv CVV as on card
     * @param String expiry_month Expiry month as on Card
     * @param String expiry_year Expiry Year as on Card
     * @param String email user email : Specifically require for stripe
     */
	public function actionAuthenticateCard() {
		$data = array();
		$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
		
		if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
			$gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
			$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
			
			if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
				$_REQUEST['exp_month'] = ltrim((string) @$_REQUEST['expiry_month'], '0');
				$_REQUEST['exp_year'] = @$_REQUEST['expiry_year'];

				if ($_REQUEST['card_name'] && $_REQUEST['card_number'] && $_REQUEST['exp_month'] && $_REQUEST['exp_year'] && $_REQUEST['cvv']) {
					$_REQUEST['isAPI'] = 1; //Only for stripe
					
					$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
					Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
					$payment_gateway = new $payment_gateway_controller();
					$res = $payment_gateway::processCard($_REQUEST);
					
					$data = json_decode($res, true);
					if (isset($data['isSuccess']) && intval($data['isSuccess'])) {
						$this->code = 200;
					} else {
						$this->code = 622;
					}
				} else {
					$this->code = 660;
				}
			} else {
				$this->code = 612;
			}
		} else {
			$this->code = 612;
		}
		
		$this->items = $data;
	}
	
    /**
     * @method public registerUser Register the user's details 
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param String $name Full Name of the User
     * @param String $email Email address of the User
     * @param String $password Login Password 
     * @param String $plan_id subscription plan id
	 * @param string card_name Name as on Credit card
     * @param String card_number Card Number 
     * @param String cvv CVV as on card
     * @param String expiry_month Expiry month as on Card
     * @param String expiry_year Expiry Year as on Card
     * @param String $card_last_fourdigit Last 4 digits of the card 
     * @param String $auth_num Authentication Number
     * @param String $token Payment Token
     * @param String $card_type Type of the card
     * @param String $reference_no Reference Number if any
     * @param String $response_text Response text as returned by Payment gateways
     * @param String $status Status of the payment gateways
     * @param String $profile_id User Profile id created in the payment gateways
     * @param Int $signup_step 1: Single, 2: 2-step, Default: single step
     * 
     */
    public function actionRegisterUser() {
		$data = array();
        if (isset($_REQUEST) && !empty($_REQUEST)) {
			if ($this->actionCheckEmailExistance($_REQUEST)) {
				$signup_step = (isset($_REQUEST['signup_step']) && intval($_REQUEST['signup_step'])) ? $_REQUEST['signup_step'] : 1;
        $_REQUEST['data'] = $_REQUEST;
				
				if ($signup_step == 1) {
				$is_register_success = 1;
				
				$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
					$gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
					$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
				}
				
				$_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
				$_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
				$_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
				
				if (isset($_REQUEST['plan_id']) && intval($_REQUEST['plan_id'])) {
					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$studio = Studios::model()->findByPk($this->studio_id);
						$default_currency_id = $studio->default_currency_id;
						$plan_details = SubscriptionPlans::model()->getPlanDetails($_REQUEST['plan_id'],$this->studio_id, $default_currency_id, $_REQUEST['country']);
                        if (isset($plan_details) && !empty($plan_details) && isset($plan_details['trial_period']) && intval($plan_details['trial_period']) == 0) {
                            $price = $plan_details['price'];
                            $currency_id = $plan_details['currency_id'];
                            $currency = Currency::model()->findByPk($currency_id);
							
							$card = array();
							$_REQUEST['data']['currency_id'] = $card['currency_id'] = $currency->id;
                            $card['currency_code'] = $currency->code;
                            $card['currency_symbol'] = $currency->symbol;
                            $card['amount'] = $price;
                            $card['token'] = @$_REQUEST['token'];
                            $card['profile_id'] = @$_REQUEST['profile_id'];
                            $card['card_holder_name'] = @$_REQUEST['card_name'];
                            $card['card_type'] = @$_REQUEST['card_type'];
                            $card['exp_month'] = $_REQUEST['exp_month'];
                            $card['exp_year'] = $_REQUEST['exp_year'];
                            $card['studio_name'] = $studio->name;
							
							$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
							Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
							$payment_gateway = new $payment_gateway_controller();
                            $trans_data = $payment_gateway::processTransactions($card);
							
							if (intval($trans_data['is_success'])) {
								$_REQUEST['data']['transaction_data']['transaction_status'] = $trans_data['transaction_status'];
								$_REQUEST['data']['transaction_data']['invoice_id'] = $trans_data['invoice_id'];
								$_REQUEST['data']['transaction_data']['order_number'] = $trans_data['order_number'];
								$_REQUEST['data']['transaction_data']['dollar_amount'] = $trans_data['dollar_amount'];
								$_REQUEST['data']['transaction_data']['amount'] = $trans_data['amount'];
								$_REQUEST['data']['transaction_data']['response_text'] = $trans_data['response_text'];
								$_REQUEST['data']['transaction_data']['is_success'] = $trans_data['is_success'];
                            } else {
								$is_register_success = 0;
                                $data = $trans_data;
									$this->code = 622;
                            }
						}
					} else {
						$is_register_success = 0;
						$this->code = 612;
					}
				}
				
				if (intval($is_register_success)) {
					$ret = SdkUser::model()->saveSdkUser($_REQUEST, $this->studio_id);
					if ($ret) {
						$is_login = 1;
						if (@$ret['is_subscribed']) {
							$file_name = '';

							if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
								$user = array();
								$user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
								$user['card_holder_name'] = $_REQUEST['data']['card_name'];
								$user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

								$file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
							}
							$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name);
							$admin_welcome_email = $this->sendStudioAdminEmails($this->studio_id, 'admin_new_paying_customer', $ret['user_id'], '', 0);
						} else {
							if (@$ret['error']) {
								$is_login = 0;
								$this->code = 661;
							} else {
								$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $ret['user_id'], 'welcome_email');
							}
						}
						
						if ($is_login) {
							$_REQUEST['sdk_user_id'] = $ret['user_id'];
								/*if ($_REQUEST['livestream'] == 1) {
									$this->registerStreamUser($_REQUEST);
								}*/
							$this->actionLogin($_REQUEST);
						}
					} else {
						$this->code = 661;
					}
				} else {
					$this->code = 661;
				}
				} elseif ($signup_step == 2) {
					$ret = SdkUser::model()->saveUserDetails($_REQUEST, $this->studio_id);
					
					if (@$ret['user_id']) {
						$_REQUEST['sdk_user_id'] = $ret['user_id'];
						$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $ret['user_id'], 'welcome_email');
						/*if ($_REQUEST['livestream'] == 1) {
							$this->registerStreamUser($_REQUEST);
						}*/
						$this->actionLogin($_REQUEST);
			} else {
				$this->code = 661;
			}
				}
		} else {
			$this->code = 661;
		}
		} else {
			$this->code = 661;
		}
		
		$this->items = $data;
	}
	
	/**
     * @method public purchaseSubscription Payment process for 2-step registration
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
	 * @param String $email Email address of the User
     * @param String $plan_id subscription plan id
	 * @param string card_name Name as on Credit card
     * @param String card_number Card Number 
     * @param String cvv CVV as on card
     * @param String expiry_month Expiry month as on Card
     * @param String expiry_year Expiry Year as on Card
     * @param String $card_last_fourdigit Last 4 digits of the card 
     * @param String $auth_num Authentication Number
     * @param String $token Payment Token
     * @param String $card_type Type of the card
     * @param String $reference_no Reference Number if any
     * @param String $response_text Response text as returned by Payment gateways
     * @param String $status Status of the payment gateways
     * @param String $profile_id User Profile id created in the payment gateways
     * 
     */
    function actionPurchaseSubscription() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$users = SdkUser::model()->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id, 'status' => 1));
			if (isset($users) && !empty($users)) {
        $_REQUEST['data'] = $_REQUEST;
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                $gateway_code = '';
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
					$gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
                    $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                }

                $_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                $_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                $_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
                
				if (isset($_REQUEST['plan_id']) && intval($_REQUEST['plan_id'])) {
					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$studio = Studios::model()->findByPk($this->studio_id);
						$default_currency_id = $studio->default_currency_id;
						$plan_details = SubscriptionPlans::model()->getPlanDetails($_REQUEST['plan_id'],$this->studio_id, $default_currency_id, $_REQUEST['country']);
						if (isset($plan_details) && !empty($plan_details)) {
							$is_register_success = 1;
							if (isset($plan_details['trial_period']) && intval($plan_details['trial_period']) == 0) {
								$price = $plan_details['price'];
								$currency_id = $plan_details['currency_id'];
								$currency = Currency::model()->findByPk($currency_id);
                
								$card = array();
								$_REQUEST['data']['currency_id'] = $card['currency_id'] = $currency->id;
								$card['currency_code'] = $currency->code;
								$card['currency_symbol'] = $currency->symbol;
								$card['amount'] = $price;
								$card['token'] = @$_REQUEST['token'];
								$card['profile_id'] = @$_REQUEST['profile_id'];
								$card['card_holder_name'] = @$_REQUEST['card_name'];
								$card['card_type'] = @$_REQUEST['card_type'];
								$card['exp_month'] = $_REQUEST['exp_month'];
								$card['exp_year'] = $_REQUEST['exp_year'];
								$card['studio_name'] = $studio->name;

								$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
								Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
								$payment_gateway = new $payment_gateway_controller();
								$trans_data = $payment_gateway::processTransactions($card);

								if (isset($trans_data['is_success']) && intval($trans_data['is_success'])) {
									$_REQUEST['data']['transaction_data']['transaction_status'] = $trans_data['transaction_status'];
									$_REQUEST['data']['transaction_data']['invoice_id'] = $trans_data['invoice_id'];
									$_REQUEST['data']['transaction_data']['order_number'] = $trans_data['order_number'];
									$_REQUEST['data']['transaction_data']['dollar_amount'] = $trans_data['dollar_amount'];
									$_REQUEST['data']['transaction_data']['amount'] = $trans_data['amount'];
									$_REQUEST['data']['transaction_data']['response_text'] = $trans_data['response_text'];
									$_REQUEST['data']['transaction_data']['is_success'] = $trans_data['is_success'];
								} else {
									$data = $trans_data;
									$is_register_success = 0;
									$this->code = 622;
                    }
							}
							
							if ($is_register_success) {
								$ret = SdkUser::model()->saveUserPayment($_REQUEST, $this->studio_id);
                    if (@$ret['is_subscribed']) {
                        $file_name = '';
									if (isset($trans_data['is_success']) && intval($trans_data['is_success'])) {
                            $user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            $user['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];
										$file_name = Yii::app()->pdf->invoiceDetial($this->studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
									}
                            
									$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $users->id, 'welcome_email_with_subscription', $file_name);
									$admin_welcome_email = $this->sendStudioAdminEmails($this->studio_id, 'admin_new_paying_customer', $users->id, '', 0);

									$this->code = 200;
								} else {
									$this->code = 661;
                        }
                    } else {
								$this->code = 661;
                    }
                } else {
							$this->code = 648;
                }
            } else {
						$this->code = 612;
            }
				} else {
					$this->code = 613;
        }
			} else {
				$this->code = 611;
    }
		} else {
			$this->code = 662;
                    }
    
		$this->items = $data;
                }

    /**
     * @method public registerstreamUser($request) It will register the stream user and create a channel id
     * @return json Return the json array of results
     * @author Gayadhar<support@muvi.com>
     */
    function registerStreamUser($request) {
        $lsuser = new LivestreamUsers;
        $lsuser->name = $request['name'];
        $lsuser->nick_name = @$request['nick_name']?@$request['nick_name']:$request['name'] ;
        $lsuser->email = $request['email'];
        $lsuser->sdk_user_id = $request['sdk_user_id'];
        $lsuser->studio_id = $this->studio_id;
        $lsuser->ip = Yii::app()->request->getUserHostAddress();
        $lsuser->status = 1;
        $lsuser->created_date = new CDbExpression("NOW()");
        $return = $lsuser->save();
        $lsuser_id = $lsuser->id;
        $push_url = RTMP_URL . $this->studio_id . '/' . $lsuser->channel_id;
        $hls_path = HLS_PATH . $this->studio_id;
        if (!is_dir($hls_path)) {
            mkdir($hls_path, 0777, TRUE);
            chmod($hls_path, 0777);
        }
        if (!is_dir($hls_path . '/' . $lsuser->channel_id)) {
            mkdir($hls_path . '/' . $lsuser->channel_id, 0777, TRUE);
            chmod($hls_path . '/' . $lsuser->channel_id, 0777);
        }
        $hls_path = $hls_path . '/' . $lsuser->channel_id;
        $hls_url = HLS_URL . $this->studio_id . '/' . $lsuser->channel_id . '/' . $lsuser->channel_id . '.m3u8';
        $lsuser->pull_url = $hls_url;
        $lsuser->push_url = $push_url;
        $lsuser->save();
        $_REQUEST['rtmp_path'] = $push_url;
        return true;
    }

    /**
     * @method public checkEmailExistance($email) It will check if an email exists with the studio or not
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param String $email Email to be validated
     */
    public function actionCheckEmailExistance($req = array()) {
        if ($req)
            $_REQUEST = $req;
        $code = 752;$email_status = 0;       
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $user = new SdkUser;
            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id));
            if (isset($users) && !empty($users)) {
                $code = 200;
                $email_status = 1;
            }
        }
        $this->code = $code;
        $this->items = array('isExists' => $email_status);
        return true;
    }

    /**
     * @method private isContentAuthorized Checks the user is authenticated to watch the content or not
     * @author SMK<support@muvi.com>
     * @param string $movie_id uniq id of film or muvi
     * @param int $season_id season (optional)
     * @param string $episode_id uniq id of stream (optional)
     * @param string $purchase_type show/episode for multipart only (optional)
     * @param string $oauthToken Auth token
	 * @return json Returns the list of data in json format
     * 
     */ 
    
    function actionIsContentAuthorized() {
        if ($_REQUEST['movie_id'] && $_REQUEST['user_id']) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];
            $lang_code   = @$_REQUEST['lang_code'];
            $translate   = $this->getTransData($lang_code,$studio_id);
            if (@$user_id) {
                $usercriteria = new CDbCriteria();
                $usercriteria->select = "is_developer";
                $usercriteria->condition = "id=$user_id";
                $userData = SdkUser::model()->find($usercriteria);
            }
            $data = array();

            $movie_code = @$_REQUEST['movie_id'];
            $movie_id = Yii::app()->common->getMovieId($movie_code);

            $stream_code = @$_REQUEST['episode_id'];
            $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }
            
            if (@$userData->is_developer) {
                $mov_can_see = 'allowed';
            } else {
                $can_see_data['movie_id'] = $movie_id;
                $can_see_data['season'] = @$season;
                $can_see_data['stream_id'] = $stream_id;
                $can_see_data['purchase_type'] = @$_REQUEST['purchase_type'];
                $can_see_data['studio_id'] = $studio_id;
                $can_see_data['user_id'] = $user_id;
                $can_see_data['country'] = @$_REQUEST['country'];
                
                $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            }

			$data['string_code'] = $mov_can_see;
			
            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
				$this->code = 652;
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
				$this->code = 653;
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
				$this->code = 654;
            } else if ($mov_can_see == 'advancedpurchased') {
				$this->code = 655;
            } else if ($mov_can_see == 'access_period') {
				$this->code = 656;
            } else if ($mov_can_see == 'watch_period') {
				$this->code = 657;
            } else if ($mov_can_see == 'maximum') {
				$this->code = 658;
            } else if ($mov_can_see == 'already_purchased') {
				$this->code = 655;
            } else if ($mov_can_see == 'unpaid') {
				$this->code = 659;
                $data['member_subscribed'] = 0;
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $data['member_subscribed'] = 1;
				}
            } else if ($mov_can_see == 'allowed') {
				$this->code = 200;
            }
        } else {
            $this->code = 680;
        }

        $this->items = $data;
    }

    /**
     * @method public validateCouponCode It will validate coupon code if it is enabled
     * @return json Return the json array of results
     * @param String $authToken*, Int user_id*, Int currencyId, String* couponCode, String lang_code
	 * @author Ashis<ashis@muvi.com>
     */
    public function actionvalidateCouponCode() {
        $data = array();
        $couponeDetails = Yii::app()->common->couponCodeIsValid($_REQUEST["couponCode"], @$_REQUEST['user_id'], $this->studio_id, @$_REQUEST["currencyId"]);
        if ($couponeDetails != 0) {
            if ($couponeDetails != 1) {
                $this->code = 200;
				
                $data['discount'] = $couponeDetails['discount_amount'];
                if ($couponeDetails["discount_type"] == 1) {
                    $data['discount_type'] = '%';
                } else {
                    if ($_REQUEST["currencyId"] != 0) {
                        $currencyDetails = Currency::model()->findByPk($_REQUEST["currencyId"]);
                        $data['discount_type'] = $currencyDetails['symbol'];
                    } else {
                        $data['discount_type'] = '$';
                    }
                }
            } else {
				$this->code = 651;
            }
        } else {
			$this->code = 650;
        }

        $this->items = $data;
    }

    public function actionPpvpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);

        $data = array();
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['card_holder_name'] = @$_REQUEST['card_name'];
        $data['card_number'] = @$_REQUEST['card_number'];
        $data['exp_month'] = @$_REQUEST['exp_month'];
        $data['exp_year'] = @$_REQUEST['exp_year'];
        $data['cvv'] = @$_REQUEST['cvv'];
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        
        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
        
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
        $video_id = $Films->id;
        $plan_id = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

        //Check ppv plan or advance plan set or not by studio admin
        $is_ppv = $is_advance = 0;
        if (intval($data['is_advance']) == 0) {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id, 0);
            if (isset($plan->id) && intval($plan->id)) {
                $is_ppv = 1;
            } else {
                $plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
                $is_ppv = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
            }
        } else  {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id);
            $is_advance = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
        }
        
        if (isset($plan) && !empty($plan)) {
            $plan_id = $plan->id;
            $price = Yii::app()->common->getPPVPrices($plan_id, $data['currency_id'], $_REQUEST['country']);
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;

            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            $end_date = '';
            if (intval($is_advance) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;
                        
                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
            }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }

            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $price['season_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $price['show_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $price['price_for_unsubscribed'];
                }
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }
                
            $gateway_code = '';
            
            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;

                $gateway_info = StudioPaymentGateways::model()->findAllByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                $gateway_code = $gateway_info[0]->short_code;
            } else {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $gateway_info = $plan_payment_gateway['gateways'];
                }
            }
            
            $this->setPaymentGatwayVariable($gateway_info);
            
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);

                $VideoDetails = $VideoName;
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }

                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypal') {
                    
                } else if (abs($data['amount']) < 0.01) {
                    
                    if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                        $data['gateway_code'] = $gateway_code;
                        $crd = Yii::app()->billing->setCardInfo($data);
                        $data['card_id'] = $crd;
                    }
                    
                    $data['amount'] = 0;
                    $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['coupon_code'], $is_advance, $gateway_code);
                    $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $is_advance, $VideoName, $VideoDetails);
                    
                    $res['code'] = 200;
                    $res['status'] = "OK";
                } else {
                    $data['gateway_code'] = $gateway_code;
                    $data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
                    
                    if($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])){
                        
                    }
                    
                    if(isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez'){
                        //echo json_encode($data);exit;
                    }
                    $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $trans_data = $payment_gateway::processTransactions($data);
                    
                    $is_paid = $trans_data['is_success'];
                    if (intval($is_paid)) {
                        $data['amount'] = $trans_data['amount'];
                        if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }
                        
                        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['coupon_code'], $is_advance, $gateway_code);
                        $ppv_subscription_id = $set_ppv_subscription;
                        
                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $is_advance);
                        $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $is_advance, $VideoName, $VideoDetails);
                        
                        $res['code'] = 200;
                        $res['status'] = "OK";
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = $translate['error_transc_process'];
                        $res['response_text'] = $trans_data['response_text'];
                    }
                }
            } else {
                $res['code'] = 411;
                $res['status'] = "Error";
                $res['msg'] = "Studio has no payment gateway";
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no PPV plan";
        }

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public GetBannerList() It will return the list of top banners for the studio
     * @param string authToken* ,string lang_code
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerList() {
		$banners = StudioBanner::model()->findAllByAttributes(array('studio_id' => $this->studio_id, 'banner_section' => 6, 'is_published' => 1), array('order' => 'id_seq ASC'));
		if ($banners) {
			$data = array();
			$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
			foreach ($banners AS $key => $banner) {
				$banner_src = $banner->image_name;
				$banner_title = $banner->title;
				$banner_id = $banner->id;
				$data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $this->studio_id . "/original/" . urlencode($banner_src);
			}
			$bnr = new BannerText();
			$banner_text = $bnr->findByAttributes(array('studio_id' => $this->studio_id), array('order' => 'id DESC'));
			$data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
			$data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
			$this->code = 200;
			$this->items = $data;
		} else
			$this->code = 625;
	}

	/**
     * @method public GetBannerSectionList() It will return the list of top banners for the studio
     * @param string $authToken Authentication Token
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerSectionList($nojson = 1) {
		$studio_id = $this->studio_id;
		$sql = "SELECT t.id FROM studios s,templates t WHERE s.parent_theme =t.code AND s.id =" . $studio_id;
		$res = Yii::app()->db->createCommand($sql)->queryRow();
		$template_id = $res['id'];
		$banners = BannerSection::model()->with(array('banners' => array('alias' => 'bn')))->findAll(array('condition' => 't.template_id=' . $template_id . ' and bn.studio_id = ' . $studio_id, 'order' => 'bn.id_seq ASC, bn.id DESC'));
		$banners = $banners[0]->banners;
		$data = array();
		if ($banners) {
			$bnr = new BannerText();
			$banner_text = $bnr->findByAttributes(array('studio_id' => $studio_id), array('order' => 'id DESC'));
			$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
			foreach ($banners AS $key => $banner) {
				$banner_src = urlencode(trim($banner->image_name));
				$data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
				$data['banners'][$key]['original'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
				$data['banners'][$key]['mobile_view'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/mobile_view/" . $banner_src;
			}
			$data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
			$data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
			$this->code = 200;
		} else {
			$this->code = 625;
		}
		if ($nojson = 1) {
			$this->items = $data;
		} else {
			return $data;
		}
	}

	/**
     * @method public socialAuth() It will authenticate the social Login 
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $fb_userId The user id return by the User
     * @param string $email Email return by the app
     * @param String $name Name as returned by the application
     * @return json Json string
     */
    function actionSocialAuth() {
        $req = $_REQUEST;
        $studio_id = $this->studio_id;
        if (@$req['fb_userid']) {
            if (@$req['email']) {
                $userData = SdkUser::model()->find('(email=:email OR fb_userid=:fb_userid) AND studio_id=:studio_id', array(':email' => $req['email'], ':fb_userid' => $req['fb_userid'], ':studio_id' => $studio_id));
                if ($userData && ($userData->is_deleted || !$userData->status)) {
                    $this->code = 753;
                    return true;
                } elseif ($userData) {
                    if ($userData['fb_userid'] != $req['fb_userid']) {
                        $userData->fb_userid = $req['fb_userid'];
                        $userData->save();
                    }  
                    $data = $this->getUserData($userData);
                    if ($userData->add_video_log) {
                        $google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                        $device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                        $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '';
                        $data['login_history_id'] = (int) $this->save_login_history($this->studio_id, $userData->id, $google_id, $device_id, $device_type, CHttpRequest::getUserHostAddress());
                    }
                    
                    $this->code = 200; 
                    $this->items = $data; 
                    return true;
                } else {
                    $email = $res['data']['email'] = @$req['email'];
                    $name = $res['data']['name'] = @$req['name'];
                    $res['data']['fb_userid'] = @$req['fb_userid'];
                    $res['data']['password'] = @$req['password'] ? $req['password'] : '';
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    $gateway_code = '';
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                        $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                    }
                    $res['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                    $res['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                    $res['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];

                    $ret = SdkUser::model()->saveSdkUser($res, $studio_id);
                    if ($ret) {
                        if (@$ret['error']) {
                            $this->code = 613;
                            return true;
                        }
                        // Send welcome email to user
                        Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], (@$ret['is_subscribed'])?'welcome_email_with_subscription':'welcome_email');
                        $data = $this->getUserData($userData);

                        if ($userData->add_video_log) {
                            $google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                            $device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                            $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '';
                            $data['login_history_id'] = (int) $this->save_login_history($this->studio_id, $userData->id, $google_id, $device_id, $device_type, CHttpRequest::getUserHostAddress());
                        }
                        $this->code = 200;
                        $this->items = $data;
                        return true;
                    } else {
                        $this->code = 661;
                        return true;
                    }
                }
            }
            $this->code = 610;
            return true;
        }        
        $this->code = 626;
        return true;
    }

    /**
     * @method public getPpvDetails() It will check and return PPV details for a content
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $content_id Content id 
     * @param string $stream_id Stream Id 
     * @return json Json string
     */
    public function actionGetPpvDetails() {
        $studio_id = $this->studio_id;
        $content_id = $_REQUEST['content_id'];
        $stream_id = $_REQUEST['stream_id'];
        if ($content_id && $stream_id) {
            $command = Yii::app()->db->createCommand()
                    ->select('f.ppv_plan_id,f.content_type_id,f.content_types_id')
                    ->from('films f ,movie_streams m')
                    ->where('f.id=m.movie_id AND f.id=:content_id AND f.studio_id=:studio_id AND m.id = :stream_id', array(':content_id' => $content_id, ':studio_id' => $studio_id, ":stream_id" => $stream_id));
            $movie = $command->queryAll();
            if ($movie) {
                if ($movie[0]['ppv_plan_id']) {
                    $ppvPlans = PpvPlans::model()->findByAttributes(array('id' => $movie[0]['ppv_plan_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));
                } else {
                    $ppvPlans = PpvPlans::model()->findByAttributes(array('studio_content_types_id' => $movie[0]['content_type_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 1, 'is_advance_purchase' => 0));
                }
                if (@$ppvPlans) {
                    $data['code'] = 200;
                    $data['status'] = 'OK';
                    $data['msg'] = "PPV Details";
                    if ($movie[0]['content_types_id'] != 3) {
                        $data['ppvPlans']['price_for_unsubscribed'] = $ppvPlans->price_for_unsubscribed;
                        $data['ppvPlans']['price_for_subscribed'] = $ppvPlans->price_for_subscribed;
                    } else {
                        $data['ppvPlans']['show_unsubscribed'] = $ppvPlans->show_unsubscribed;
                        $data['ppvPlans']['show_subscribed'] = $ppvPlans->show_subscribed;
                        $data['ppvPlans']['season_unsubscribed'] = $ppvPlans->season_unsubscribed;
                        $data['ppvPlans']['season_subscribed'] = $ppvPlans->season_subscribed;
                        $data['ppvPlans']['episode_subscribed'] = $ppvPlans->episode_subscribed;
                    }
                } else {
                    $data['code'] = 439;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'PPV not enabled for this content';
                }
            } else {
                $data['code'] = 438;
                $data['status'] = 'Failure';
                $data['msg'] = 'No content availbe for the information';
            }
        } else {
            $data['code'] = 437;
            $data['status'] = 'Failure';
            $data['msg'] = 'Content id and Stream id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getVideoDetails() It will give details of the video
     * @author Gayadhar<support@muvi.com>
     * @param string authToken* , string content_uniq_id* ,string stream_uniq_id*,int user_id,int internet_speed,string lang_code    
     * @return json object
     */
     function actionGetVideoDetails() {		                         
        if (isset($_REQUEST['content_uniq_id']) && $_REQUEST['content_uniq_id']!='' && isset($_REQUEST['stream_uniq_id']) && $_REQUEST['stream_uniq_id']!='') {	
			$internetSpeed = (isset($_REQUEST['internet_speed']) && $_REQUEST['internet_speed'] > 0)?$_REQUEST['internet_speed']:0;  
			$contentDetails = Yii::app()->db->createCommand()
							->select('F.id,F.name,F.content_types_id,S.id AS stream_id,S.rolltype AS rollType,S.roll_after AS rollAfter,S.video_resolution,S.enable_ad AS adActive,S.full_movie,S.embed_id,S.is_demo,S.is_converted,F.uniq_id,S.content_key,S.encryption_key,S.thirdparty_url,S.video_management_id,S.is_offline')
							->from('films F, movie_streams S')
							->where('F.id=S.movie_id AND F.uniq_id=:uniq_id AND S.embed_id=:stream_uniq_id',array(':uniq_id' => $_REQUEST['content_uniq_id'], ':stream_uniq_id' => $_REQUEST['stream_uniq_id']))
							->queryRow();						
            if ($contentDetails) {
					$lang_code = @$_REQUEST['lang_code'];
					$translate   = $this->getTransData($lang_code,$this->studio_id);
                    $language_nameArr = array();
                    $domainName = $this->getDomainName();                   
                    $user_id = (isset($_REQUEST['user_id'])&& $_REQUEST['user_id'] > 0)?$_REQUEST['user_id']:0;  					
					$bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
					$folderPath = Yii::app()->common->getFolderPath("",$this->studio_id);
                    $signedBucketPath = $folderPath['signedFolderPath'];
					$streamingRestriction = 0;
                    $getDeviceRestrictionSetByStudio = 0;
                    if($user_id>0){
                        $playLengthArr = Yii::app()->db->createCommand()
								->select('played_length') 
								->from('video_logs')
								->where('user_id=:user_id AND video_id=:stream_id' ,array(':user_id' => $user_id,':stream_id'=>$contentDetails['stream_id']))
								->order('id DESC')
								->limit(1)->queryRow();
						$getDeviceRestriction = StudioConfig::model()->getdeviceRestiction($this->studio_id, $user_id);
						$streamingRestriction = ($getDeviceRestriction > 0)? 1: 0;                       
                        if($getDeviceRestriction != ""){
                            $getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($this->studio_id, 'restrict_streaming_device');
                            if(@$getDeviceRestrictionSetByStudioData->config_value != ""){
                                $getDeviceRestrictionSetByStudio = $getDeviceRestrictionSetByStudioData->config_value;
                            } 
                        }
                    } 						
                    $played_length = !empty($playLengthArr['played_length']) ? $playLengthArr['played_length']: null;
                    //Get trailer info from movie_trailer table                                      
                    $trailerData = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id'=>$contentDetails['id']));
                    if($trailerData['trailer_file_name'] != ''){
                        $embedTrailerUrl = $domainName.'/embedTrailer/'.$contentDetails['uniq_id'];
                        if($trailerData['third_party_url'] != ''){
                            $trailerThirdpartyUrl = $this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
                        }else if($trailerData['video_remote_url'] != ''){
                            $trailerUrl = $this->getTrailer($contentDetails['id'],"",$this->studio_id);
                        }  
                    }                    
                    //End of trailer part
                   if($contentDetails['content_types_id'] == 4){
                        $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id'=>$this->studio_id,':movie_id'=>$contentDetails['id']));
                        if($Ldata && $Ldata->feed_url){
                            $embedUrl= $domainName.'/embed/livestream/'.$contentDetails['uniq_id'];
                            $fullmovie_path = $Ldata->feed_url;
                            if ($Ldata->feed_type == 2) {
                                if (strpos($Ldata->feed_url, 'rtmp://'.nginxserverip) !== false) {
                                    if (strpos($Ldata->feed_url, 'rtmp://'.nginxserverip.'/live') !== false) {
                                            $liveAppName = 'rtmp://'.nginxserverip.'/live';
                                    } else if (strpos($Ldata->feed_url, 'rtmp://'.nginxserverip.'/record') !== false) {
                                            $liveAppName = 'rtmp://'.nginxserverip.'/record';
                                    }
                                    $fullmovie_path = str_replace($liveAppName, nginxserverlivecloudfronturl, $Ldata->feed_url) . ".m3u8";
                                }
                            }
                        }
                    }else{
                        if($contentDetails['full_movie']!=''){
								$embedUrl = $domainName.'/embed/'.$contentDetails['embed_id'];
                           if($contentDetails['thirdparty_url'] != ''){
                                $thirdparty_url = $this->getSrcFromThirdPartyUrl($contentDetails['thirdparty_url']);
                           }else{                            
                              //Check thirdparty url is not present then pass video url with details                                                           
                                $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/movie_stream/full_movie/" . $contentDetails['stream_id'] . "/" . $contentDetails['full_movie'];
                                if($contentDetails['is_demo'] == 1){
                                    $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/small.mp4";
                                }                                                                
                                $multipleVideo = $this->getvideoResolution($contentDetails['video_resolution'], $fullmovie_path);
                                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path, $internetSpeed);
                                $videoToBeplayed12 = explode(",", $videoToBeplayed);
                                $fullmovie_path = $videoToBeplayed12[0];
								$this->items['video_resolution'] = $videoToBeplayed12[1];                                
                                $clfntUrl = 'https://' . $bucketInfo['cloudfront_url'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, "", "", 60000,$this->studio_id);
                                //added by prakash for signed url on 29th march 2017
                                $multipleVideoSignedArr=array();
                                foreach($multipleVideo as $resKey => $videoPath){
                                    $subArr = array();
                                    if($resKey){
                                        $subArr['resolution'] = $resKey;
                                        $subArr['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, "", "", 60000,$this->studio_id);
                                        $multipleVideoSignedArr[] = $subArr;
                                    }                                                               
                                }                                                                                               
								$this->items['video_details'] = $multipleVideoSignedArr;
								$this->items['new_video_url'] = $multipleVideoForUnsigned[144];
                                if($contentDetails['content_key'] && $contentDetails['encryption_key']){                                    
                                    $fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$contentDetails['stream_id'].'/stream.mpd';
                                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($this->studio_id,'drm_cloudfront_url');
                                    if($getStudioConfig){
                                        if(@$getStudioConfig['config_value'] != ''){
                                            $fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$contentDetails['stream_id'].'/stream.mpd';
                                        }
                                    }
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL,MS3_URL);
                                    curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&contentId=urn:marlin:kid:" . $contentDetails['encryption_key'] . "&contentKey=" . $contentDetails['content_key'] . "&ms3Scheme=true&contentURL=" . $fullmovie_path);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
                                    $server_output = curl_exec ($ch);
									$this->items['studio_approved_url'] = $server_output;
									$this->items['is_offline'] = $contentDetails['is_offline'];                                    
                                    curl_close ($ch);
									//get LicenceUrl									
									$licence_url = $this->generateExplayWideVineToken($contentDetails['content_key'],$contentDetails['encryption_key']);									
									$this->items['license_url'] = $licence_url? $licence_url: '';
                                }                            
                           }
                        }
                    }
               //Subtitle section start
                if($contentDetails['video_management_id'] > 0){						
                        $getVideoSubtitle = Yii::app()->db->createCommand()
											->select('vs.id AS subId,vs.filename,ls.name,ls.code,vs.video_gallery_id,') 
											->from('video_subtitle vs,language_subtitle ls')
											->where('vs.language_subtitle_id = ls.id AND vs.video_gallery_id=:video_management_id',array(':video_management_id' => $contentDetails['video_management_id']))
											->queryAll();												
                        if($getVideoSubtitle) {     								
                            $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];                                
                            foreach ($getVideoSubtitle as $subtitlekey => $getVideoSubtitlevalue) {                                
                                $filePth = $clfntUrl . "/" . $bucketInfo['signedFolderPath'] . "subtitle/" . $getVideoSubtitlevalue['subId'] . "/" . $getVideoSubtitlevalue['filename'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                                $language_nameArr[$subtitlekey]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1, "", 60000,$this->studio_id);
                                $language_nameArr[$subtitlekey]['code'] = $getVideoSubtitlevalue['code'];
								if (@$translate[$getVideoSubtitlevalue['name']]!='')
									$language_nameArr[$subtitlekey]['language'] = @$translate[$getVideoSubtitlevalue['name']];  
								else
									$language_nameArr[$subtitlekey]['language'] = $getVideoSubtitlevalue['name'];								                             
                            } 
                        }
                }
                 // End of subtitle section.
				//Ads section 
				$ads =array();
				$ads['ad_active'] = $contentDetails['adActive'];
				if($contentDetails['adActive']){
					$adsTime=array();
					$ads['ad_network'] = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($this->studio_id);										
					$rollType = $contentDetails['rollType'];
					$adsTime['start'] = ($rollType == '7' || $rollType == '5' || $rollType == '3' || $rollType == '1')?1:0;
					$adsTime['mid'] = ($rollType == '7' || $rollType == '3' || $rollType == '6' || $rollType == '2')?1:0;
					$adsTime['end'] = ($rollType == '7' || $rollType == '5' || $rollType == '6' || $rollType == '4')?1:0;					
					$roll_after = explode(',', $contentDetails['rollAfter']);
					if (in_array("end", $roll_after)) {
						array_pop($roll_after);
					}
					if (in_array("0", $roll_after)) {						
						array_shift($roll_after);						
					}
					//convert ads streaming time to seconds
					$time_seconds ="";
					foreach($roll_after as $k=>$time){
						if($time_seconds=="")
							$time_seconds = Yii::app()->general->getHHMMSSToseconds($time);
						else
							$time_seconds.=','.Yii::app()->general->getHHMMSSToseconds($time);
					}
					$adsTime['midroll_values'] = $time_seconds;
					if($ads['ad_network'][1]['ad_network_id']>0 && $ads['ad_network'][1]['ad_network_id']!=3){
						$ads['ads_time'] = $adsTime;
					}
				}
                $this->code = 200;                 
				$this->items['video_url'] = @$fullmovie_path;
				$this->items['thirdparty_url'] = @$thirdparty_url;
				$this->items['embed_url'] = @$embedUrl;
				$this->items['trailer_url'] = @$trailerUrl;
				$this->items['trailer_thirdparty_url'] = @$trailerThirdpartyUrl;
				$this->items['embed_trailer_url'] = @$embedTrailerUrl;
				$this->items['played_length'] = @$played_length;
				$this->items['subtitle'] = @$language_nameArr;
				$this->items['streaming_restriction'] = $streamingRestriction;
                $this->items['no_streaming_device'] = $getDeviceRestrictionSetByStudio;
				$this->items['restrict_streaming_msg'] = @$translate['restrict-streaming-device'];
				$this->items['ads_details'] = $ads;
            } else 
                $this->code = 629;                 
        } else 
			$this->code = 628;             
    }
	
    /**
     * @method public GetImageForDownload() It will give the s3 url image to check internet speed of user
	 * @param string authToken* ,string lang_code
     * @author Sruti kanta<support@muvi.com>
     * @return json 
     */
    function actionGetImageForDownload() {
        $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);		
        $image_url = CDN_HTTP . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/check-download-speed.jpg";
		$this->code = 200;
		$this->items['image_url']=$image_url;        
    }

    /**
     * @method public startUpstream() It will start the upstream automatically and will generate the HLS url 
     * @param string authToken* , int movie_id* ,string lang_code           
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionStartUpstream() {		
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']>0) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_start_time = $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');                
                $lsuser->save();
				$this->code = 200;				            
            } else {
				$this->code = 608;             
            }
        } else {
			$this->code = 644;          
        }      
    }    

    /**
     * @method public getliveUserlist() It will return the list of users who are currently live streaming 
     * @param string  authToken *,string lang_code
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetLiveUserlist() {   				
		$lsuser = Yii::app()->db->createCommand()
                            ->select('name,nick_name,channel_title as title,pull_url AS hls_path,push_url AS rtmp_path') 
                            ->from('livestream_users')
                            ->where('studio_id =:studio_id AND is_online=1' ,array(':studio_id' => $this->studio_id))
                            ->order('id ASC')
							->queryAll();		
		$this->code = 200;
		$this->items['users'] = $lsuser;				       				 
    }

    /**
     * @method public getChannelOnlineStatus() It will return the list of users who are currently live streaming 
     * @param string authToken * ,string permalink *,string lang_code
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actiongetChannelOnlineStatus() {
		$data=array();
		if (isset($_REQUEST['permalink']) && $_REQUEST['permalink']!='') {
            $movie = Yii::app()->db->createCommand()->select('f.content_types_id,ls.is_online')->from('films f ,livestream ls ')
                    ->where('ls.movie_id = f.id AND f.content_types_id=4 AND f.permalink=:permalink AND  f.studio_id=:studio_id', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryRow();			
			if($movie){
					$this->code = 200;                 	
                    $data['is_online'] = $movie["is_online"] ? (int)$movie["is_online"] : 0;
                    $data['msg'] = $movie["is_online"] ? "Online" : 'Offline';
					$this->items = $data;
            } else {
				$this->code = 605;                
			}
        } else {
			$this->code = 604;			
		}				     
    }

    /**
     * @method public stopUpstream() It will stop the upstream 
     * @param string authToken* , int movie_id* ,string lang_code
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionStopUpstream() {
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']>0) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 0;
                $lsuser->stream_start_time = $lsuser->stream_update_time= '';                
                $lsuser->save();
                $this->code = 200;               
            } else {
                $this->code = 680;                
            }
        } else {
            $this->code = 644;           
        }       
    }

    /**
     * @method public channelLiveStatus() It will stop the upstream 
     * @param string authToken* , int movie_id * ,string lang_code     
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionChannelStatus() {
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']>0) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');
                $lsuser->save();
                $this->code = 200;                                
            } else {
                $this->code = 680;                
            }
        } else {
            $this->code = 644;            
        }        
    }

    /**
     * @method public getPorfileDetails() It will fetch the profile details of the logged in user
     * @param string $authToken Authtoken
     * @param string $email
     * @param int $user_id
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetProfileDetails() {
        $code = 634;
        if (@$_REQUEST['email'] || @$_REQUEST['user_id']) {
            $userData = SdkUser::model()->find('studio_id =:studio_id AND status=:status AND (id=:user_id OR email=:email)', array(':studio_id' => $this->studio_id, ':status' => 1, ':user_id' => @$_REQUEST['user_id'], ':email' => @$_REQUEST['email']));
            if ($userData) {   
                $this->items = $this->getUserData($userData, true);
                $this->code = 200;
                return true;
            }
            $code = 633;
        }
        $this->code = $code;
        return true;
    }

    /**
     * @method getFbUserstatus() Returns the Facebook user status 
     * @param String $authToken A authToken
     * @param String $fbuser_id Facebook User id
     * @author Gayadhar<support@muvi.com>
     * @return Json Facebook user status
     */
    function actionGetFbUserStatus() {
        $userData = SdkUser::model()->find('(fb_userid=:fb_userid) AND studio_id=:studio_id', array(':fb_userid' => @$_REQUEST['fb_userid'], ':studio_id' => $this->studio_id));
        if ($userData && ($userData->is_deleted || !$userData->status)) {
            $this->code = 200;
            return true;
        } elseif ($userData) {            
            $this->items = $this->getUserData($userData);
            $this->code = 200;
            return true;            
        }
        $this->items = array('is_newuser' => 1);
        $this->code = 200;
        return true; 
    }

    /**
     * @method converttimetoSec(string $str) convert a string of HH:mm:ss to second
     * @author Gayadhar<support@muvi.com>
     * @return int 
     */
    function convertTimetoSec($str_time) {
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        return $time_seconds;
    }

    /**@method public VideoLogs() It will store video log data in video_logs
	 * @author prakash<support@muvi.com>
	 * @param string authToken*, string movie_id(movie_uniq_id)*, int user_id*, string ip_address*, int log_unique_id*, int log_id*, string episode_id(stream embed_id), int season_id, int device_type, int played_length*, string watch_status*, int start_time*, int end_time*,int content_type*, int video_length, int is_streaming_restriction,int restrict_stream_id,int is_active_stream_closed,string lang_code   
     * @return json object
	 */
    public function actionVideoLogs() {
		$movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
        if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed) {						
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$played_length = $_REQUEST['played_length'];
						$video_length = isset($_REQUEST['video_length']) ? $_REQUEST['video_length'] : 0;
						$watch_status = $_REQUEST['watch_status'];
						$device_type = $_REQUEST['device_type'];
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$restrictDeviceId = 0;
						if($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
							$restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
							if(@$_REQUEST['is_active_stream_closed'] == 1){
								RestrictDevice::model()->deleteData($restrictStreamId);
							} else {
								$postData = array();
								$postData['id'] = $restrictStreamId;
								$postData['studio_id'] = $studio_id;
								$postData['user_id'] = $user_id;
								$postData['movie_stream_id'] = $stream_id;
								$restrictDeviceId = RestrictDevice::model()->dataSave($postData);
							}
						}
						$video_log = new VideoLogs();
						$log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;

						$trailer_id='';
						if($content_type == 2){
							$trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							$trailer_id=$trailerData->id;
							$stream_id=0;
						}
						$video_log->trailer_id = $trailer_id;
						
						if ($log_id > 0) {
							$video_log = VideoLogs::model()->findByPk($log_id);
							$video_log->updated_date = new CDbExpression("NOW()");
							$video_log->played_length = $played_length;
						} else {
							$video_log->created_date = new CDbExpression("NOW()");
							$video_log->ip = $ip_address;
							$video_log->video_length = $video_length;
							$video_log->device_type = $device_type;
						}
						$video_log->movie_id = $movie_id;
						$video_log->video_id = $stream_id;
						$video_log->user_id = $user_id;
						$video_log->device_id = $device_id;
						$video_log->content_type = $content_type;
						$video_log->studio_id = $studio_id;
						$video_log->watch_status = $watch_status;
						if ($video_log->save()) {
							$this->code = 200;														
							$this->items['log_id'] = $video_log->id;
							$this->items['restrict_stream_id'] = $restrictDeviceId;
						} else 						
							$this->code = 500;						
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635; 	
		}else
			$this->code = 684; 		
    }
	/**@method public VideoLogNew() It will store video log data in video_logs tble and video_log_temp table
	 * @author prakash<support@muvi.com>
	 * @param string authToken*, string movie_id(movie_uniq_id)*, int user_id*, string ip_address*, int log_unique_id*, int log_id*, string episode_id(stream embed_id), int season_id, int device_type, int played_length*, string watch_status*, int start_time*, int end_time*,int content_type*, int video_length, int is_streaming_restriction,int restrict_stream_id,int is_active_stream_closed,string lang_code,int log_temp_id,float resume_time   
     * @return json object
	 */
	public function actionVideoLogNew() {    		
		$movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
        if($movie_code){
            $ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					if($user_id){
						$add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
					}else{
						$add_video_log = 1;
					}
					$isAllowed = $device_id = 0;					
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $_REQUEST['user_id'];
					}
					if ($add_video_log || $isAllowed) {						
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;												
						//Get stream Id
						$stream_id = 0;						
						if (trim($movie_code) != '0' && trim($stream_code) != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);							
						} else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);							
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);							
						}						
						$parameters = array();
						$parameters['movie_id'] = $movie_id;
						$parameters['stream_id'] = $stream_id;
						$parameters['studio_id'] = $studio_id;
						$activePlayCount = VideoLogs::model()->activePlayLog($parameters);

						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$restrictDeviceId = 0;
						if($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
							$restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
							if(@$_REQUEST['is_active_stream_closed'] == 1){
								RestrictDevice::model()->deleteData($restrictStreamId);
							} else {
								$postData = array();
								$postData['id'] = $restrictStreamId;
								$postData['studio_id'] = $studio_id;
								$postData['user_id'] = $user_id;
								$postData['movie_stream_id'] = $stream_id;
								$restrictDeviceId = RestrictDevice::model()->dataSave($postData);
							}
						}
						$dataForSave = array();
						$dataForSave = $_REQUEST;
						$dataForSave['movie_id'] = $movie_id;
						$dataForSave['stream_id'] = $stream_id;
						$dataForSave['studio_id'] = $studio_id;
						$dataForSave['user_id'] = $user_id;
						$dataForSave['ip_address'] = $ip_address;
						$dataForSave['trailer_id'] = "";
						$dataForSave['status'] = $_REQUEST['watch_status'];
						$dataForSave['content_type'] = $content_type;
						if($content_type == 2){
							$trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							$dataForSave['trailer_id']=$trailerData->id;
							$dataForSave['stream_id']="";
						}
						$video_log_id = VideoLogs::model()->dataSave($dataForSave);
						if ($video_log_id) {
							$this->code = 200;														
							$this->items['log_id'] = @$video_log_id[1];
							$this->items['log_temp_id'] = @$video_log_id[0];
							$this->items['restrict_stream_id'] = $restrictDeviceId;
							$this->items['no_of_views'] = @$activePlayCount;							
						} else 
							$this->code = 500;
					} else 
						$this->code = 637;
				} else 
					$this->code = 636;
			}else
				$this->code = 635;
        } else 
			$this->code = 684; 
    }
	/**@method public BufferLogs() It will store Buffer log data in bandwidth_log table
	 * @author prakash<support@muvi.com>
	 * @param string authToken*, string movie_id(movie_uniq_id)*, int user_id*, string ip_address*, int buffer_log_unique_id*, int buffer_log_id*, string episode_id(stream embed_id), int season_id, int device_type, int start_time*, int end_time*,int content_type*, int video_length, int is_streaming_restriction,int restrict_stream_id,int is_active_stream_closed,string lang_code,int total_bandwidth* for DRM  
     * @return json object
	 * 
	 */
    public function actionBufferLogs() {
        $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
		if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed) {
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$device_type = $_REQUEST['device_type'];
						$resolution = $_REQUEST['resolution'];
						$start_time = $_REQUEST['start_time'];
						$end_time = $_REQUEST['end_time'];
						$log_unique_id = (isset($_REQUEST['buffer_log_unique_id']) && trim(($_REQUEST['buffer_log_unique_id']))) ? $_REQUEST['buffer_log_unique_id'] : '';
						$log_id = (isset($_REQUEST['buffer_log_id']) && trim(($_REQUEST['buffer_log_id']))) ? $_REQUEST['buffer_log_id'] : '';
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$played_time = $end_time - $start_time;
						if(strtolower(@$_REQUEST['video_type']) == 'mped_dash'){
							//mped_dash
							$bandwidth_used = @$_REQUEST['total_bandwidth'];
						}else{
							if($content_type == 2){
								$movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							} else{
								$movie_stream_data = movieStreams::model()->findByPk($stream_id);
							}
							$res_size = json_decode($movie_stream_data->resolution_size, true);
							$video_duration = explode(':', $movie_stream_data->video_duration);
							$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
							$size = $res_size[$resolution];
							$bandwidth_used = ($size / $duration) * $played_time;
						}
						if (isset($log_id) && $log_id) {							
							$buff_log = BufferLogs::model()->findByPk($log_id);
							if (isset($buff_log) && !empty($buff_log)) {
								$buff_log->end_time = $end_time;
								$buff_log->buffer_size = $bandwidth_used;
								$buff_log->played_time = $end_time - $buff_log->start_time;
								$buff_log->save();
								$buff_log_id = $log_id;
								$unique_id = $log_unique_id;
							}
						} else {
							$new_buff_log = new BufferLogs();
							if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
								if ($device_type == 4) {
									$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id),array('order'=>'id DESC'));
								} else {
									$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id),array('order'=>'id DESC'));
								}								
								$new_buff_log->city = $buff_log->city;
								$new_buff_log->region = $buff_log->region;
								$new_buff_log->country = $buff_log->country;
								$new_buff_log->country_code = $buff_log->country_code;
								$new_buff_log->continent_code = $buff_log->continent_code;
								$new_buff_log->latitude = $buff_log->latitude;
								$new_buff_log->longitude = $buff_log->longitude;
							}else{
								$ip_address = Yii::app()->request->getUserHostAddress();
								$geo_data = new EGeoIP();
								$geo_data->locate($ip_address);								
								$new_buff_log->city = @$geo_data->getCity();
								$new_buff_log->region = @$geo_data->getRegion();
								$new_buff_log->country = @$geo_data->getCountryName();
								$new_buff_log->country_code = @$geo_data->getCountryCode();
								$new_buff_log->continent_code = @$geo_data->getContinentCode();
								$new_buff_log->latitude = @$geo_data->getLatitude();
								$new_buff_log->longitude = @$geo_data->getLongitude();
							}
							$unique_id = md5(uniqid(rand(), true));							
							$new_buff_log->studio_id = $studio_id;
							$new_buff_log->unique_id = $unique_id;
							$new_buff_log->user_id = $user_id;
							$new_buff_log->device_id = $device_id;
							$new_buff_log->movie_id = $movie_id;
							$new_buff_log->video_id = $stream_id;
							$new_buff_log->resolution = $resolution;
							$new_buff_log->start_time = $start_time;
							$new_buff_log->end_time = $end_time;
							$new_buff_log->played_time = $played_time;
							$new_buff_log->buffer_size = $bandwidth_used;							
							$new_buff_log->device_type = $device_type;
							$new_buff_log->content_type = $content_type;
							$new_buff_log->ip = $ip_address;
							$new_buff_log->created_date = date('Y-m-d H:i:s');
							$new_buff_log->save();
							$buff_log_id = $new_buff_log->id;
						}
						$this->code = 200;		
						$this->items['location'] = 1;
						$this->items['buffer_log_id'] = $buff_log_id;
						$this->items['buffer_log_unique_id'] = $unique_id;						
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635;
		}else
			$this->code = 684;
    }
	/**@method public UpdateBufferLogs() It will store UpdateBufferLogs data in bandwidth_log table
	 * This function will call only for NON-DRM video at the time of seek and change resolution
	 * @author prakash<support@muvi.com>
	 * @param string authToken*, string movie_id(movie_uniq_id)*, int user_id*, string ip_address*, int buffer_log_unique_id*, int buffer_log_id*, string episode_id(stream embed_id), int season_id, int device_type, int start_time*, int end_time*,int content_type*,string lang_code  
     * @return json object
	 * 
	 */
    public function actionUpdateBufferLogs() {
       $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
		if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed) {
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$device_type = $_REQUEST['device_type'];
						$resolution = $_REQUEST['resolution'];
						$start_time = $_REQUEST['start_time'];
						$end_time = $_REQUEST['end_time'];
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$movie_stream_data = movieStreams::model()->findByPk($stream_id);
						if($content_type == 2){
							$movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
						}
						$res_size = json_decode($movie_stream_data->resolution_size, true);
						$video_duration = explode(':', $movie_stream_data->video_duration);
						$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
						$size = $res_size[$resolution];
						$played_time = $end_time - $start_time;
						$bandwidth_used = ($size / $duration) * $played_time;
						$unique_id = md5(uniqid(rand(), true));
						$new_buff_log = new BufferLogs();
						if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
							if ($device_type == 4) {
								$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
							} else {
								$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
							}							
							$new_buff_log->city = $buff_log->city;
							$new_buff_log->region = $buff_log->region;
							$new_buff_log->country = $buff_log->country;
							$new_buff_log->country_code = $buff_log->country_code;
							$new_buff_log->continent_code = $buff_log->continent_code;
							$new_buff_log->latitude = $buff_log->latitude;
							$new_buff_log->longitude = $buff_log->longitude;
						} else {
							$ip_address = Yii::app()->request->getUserHostAddress();
							$geo_data = new EGeoIP();
							$geo_data->locate($ip_address);							
							$new_buff_log->city = @$geo_data->getCity();
							$new_buff_log->region = @$geo_data->getRegion();
							$new_buff_log->country = @$geo_data->getCountryName();
							$new_buff_log->country_code = @$geo_data->getCountryCode();
							$new_buff_log->continent_code = @$geo_data->getContinentCode();
							$new_buff_log->latitude = @$geo_data->getLatitude();
							$new_buff_log->longitude = @$geo_data->getLongitude();
						}
						$new_buff_log->studio_id = $studio_id;
						$new_buff_log->unique_id = $unique_id;
						$new_buff_log->user_id = $user_id;
						$new_buff_log->device_id = $device_id;
						$new_buff_log->movie_id = $movie_id;
						$new_buff_log->video_id = $stream_id;
						$new_buff_log->resolution = $resolution;
						$new_buff_log->start_time = $start_time;
						$new_buff_log->end_time = $end_time;
						$new_buff_log->played_time = $played_time;
						$new_buff_log->buffer_size = $bandwidth_used;							
						$new_buff_log->device_type = $device_type;
						$new_buff_log->content_type = $content_type;
						$new_buff_log->ip = $ip_address;						
						$new_buff_log->created_date = date('Y-m-d H:i:s');
						$new_buff_log->save();
						$buff_log_id = $new_buff_log->id;						
						$this->code = 200;								
						$this->items['buffer_log_id'] = $buff_log_id;
						$this->items['buffer_log_unique_id'] = $unique_id;	
						$this->items['location'] = 1;						
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635;
		}else
			$this->code = 684;
    }

    /**
* @method updateUserProfile() Returns the profile details along with profile image
* @param String $authToken A authToken
* @param array $data profile data in array format
* @param array $image optional image info 
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
    function actionUpdateUserProfile() {
        if (isset($_REQUEST['user_id'])) {
            $user_id = (int)@$_REQUEST['user_id'];
            if ($_FILES && ($_FILES['error'] == 0)) {
                $ret = $this->uploadProfilePics($user_id, $this->studio_id, $_FILES['file']);
            }
            $sdkUsers = SdkUser::model()->findByPk($user_id);
            if ($sdkUsers) {
                if (@$_REQUEST['nick_name'])
                    $sdkUsers->nick_name = $_REQUEST['nick_name'];
                if (@$_REQUEST['name']) 
                    $sdkUsers->display_name = $_REQUEST['name'];
                if (@$_REQUEST['password']) {
                    $enc = new bCrypt();
                    $pass = $enc->hash($_REQUEST['password']);
                    $sdkUsers->encrypted_password = $pass;
                }
                $sdkUsers->last_updated_date = new CDbExpression("NOW()");
                $sdkUsers->save();
                Yii::app()->custom->updateCustomFieldValue($user_id, $this->studio_id);
            }
            $this->code = 200; 
            $this->items = array('name' => $sdkUsers->display_name, 'email' => $sdkUsers->email, 'nick_name' => $sdkUsers->nick_name, 'profile_image' => $this->getProfilePicture($user_id, 'profilepicture', 'thumb', $this->studio_id)); 
            return true;            
        }
        $this->code = 639; 
        return true;
    }
	
	function uploadProfilePics($user_id,$studio_id,$fileinfo){
		if(!$studio_id){$studio_id = $this->studio_id;}

		//Checking for existing object
		$old_poster = new Poster;
		$old_posters = $old_poster->findAllByAttributes(array('object_id' => $user_id, 'object_type' => 'profilepicture'));
		if(count($old_posters) > 0){
			foreach ($old_posters as $oldposter) {
				$oldposter->delete();
			}
		}
		$ip_address = CHttpRequest::getUserHostAddress(); 
		$poster = new Poster();
		$poster->object_id = $user_id;
		$poster->object_type = 'profilepicture';
		$poster->poster_file_name = $fileinfo['name'];
		$poster->ip = $ip_address;
		$poster->created_by = $user_id;
		$poster->created_date = new CDbExpression("NOW()");
		$poster->save();
		$poster_id = $poster->id;                    
		
		$fileinfo['name'] = Yii::app()->common->fileName($fileinfo['name']);
		$_FILES['Filedata'] = $fileinfo;
		$dir = $_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'/images/public/system/profile_images/'.$poster_id;
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		$dir = $dir.'/original';
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		move_uploaded_file($fileinfo['tmp_name'], $dir.'/'.$fileinfo['name']);
		
		//$uid = $_REQUEST['movie_id'];
		require_once "Image.class.php";
		require_once "Config.class.php";
		require_once "ProfileUploader.class.php";
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		require_once "amazon_sdk/sdk.class.php";
		spl_autoload_register(array('YiiBase', 'autoload'));

		define( "BASEPATH",dirname(__FILE__) . "/.." );
		$config = Config::getInstance();
		$config->setUploadDir($_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'images/public/system/profile_images'); //path for images uploaded
		$bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
		$config->setBucketName($bucketInfo['bucket_name']);
		$s3_config = Yii::app()->common->getS3Details($studio_id);
		$config->setS3Key($s3_config['key']);
		$config->setS3Secret($s3_config['secret']);
		$config->setAmount( 250 );  //maximum paralell uploads
		$config->setMimeTypes( array( "jpg" , "gif" , "png",'jpeg' ) ); //allowed extensions
		$config->setDimensions( array('small'=>"150x150",'thumb'=>"100x100") );   //resize to these sizes
		//usage of uploader class - this simple :)
		$uploader = new ProfileUploader($poster_id);
		$folderPath = Yii::app()->common->getFolderPath("",$studio_id);
		$unsignedBucketPath = $folderPath['unsignedFolderPath'];
		$ret = $uploader->uploadprofileThumb($unsignedBucketPath."public/system/profile_images/");
		return true;
	}
/**
* @method GetStatByType() Return the 
* @param String $authToken A authToken
* @param array $filterType filter on specific type like genre, language,rating etc
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
    function actionGetStatByType() {
        if ($_REQUEST['filterType']) {
            if (strtolower(trim($_REQUEST['filterType'])) == 'genre') {
                $genreArr = array();
                $gsql = 'SELECT genre, COUNT(*) AS cnt FROM films WHERE studio_id =' . $this->studio_id . ' AND genre is not NULL AND genre !=\'\' AND genre !=\'null\' GROUP BY genre';
                $res = Yii::app()->db->createCommand($gsql)->queryAll();
                if ($res) {
                    foreach ($res As $key => $val) {
                        $expArr = json_decode($val['genre'], TRUE);
                        if (is_array($expArr)) {
                            foreach ($expArr AS $k => $v) {
                                $mygenre = strtolower(trim($v));
                                $data['content_statistics'][$mygenre] += $val['cnt'];
                            }
                        }
                    }
                    $this->code = 200; 
                    $this->items = @$data; 
                    return true;                    
                }
                $this->code = 642;
                return true;
            }            
        } 
        $this->code = 641;
        return true;        
    }
        
	/* Functions to set header with status code. eg: 200 OK ,400 Bad Request etc.. */

    private function setHeader($status) {
		ob_clean();
		ob_flush();
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Muvi <support@muvi.com>");
    }

    /**
	 * @method  actiongetEmbedUrl return embed url of stream
	 * @param string authToken* , int movie_stream_id * ,string lang_code
	 * @return json
	 */
    public function actionGetEmbedUrl() {
		if(isset($_REQUEST['movie_stream_id']) && $_REQUEST['movie_stream_id']>0){
			$movieStreamData = Yii::app()->db->createCommand()
							->select('s.is_embed_white_labled,s.domain,ms.embed_id')
							->from('studios s')
							->join('movie_streams ms', 's.id=ms.studio_id')
							->where('s.id=:studio_id AND ms.id=:stream_id', array(':studio_id'=>$this->studio_id,':stream_id'=>$_REQUEST['movie_stream_id']))
							->queryRow();
			if($movieStreamData){
				$domainName = Yii::app()->getBaseUrl(TRUE);
				if($movieStreamData['is_embed_white_labled'])
					$domainName = $movieStreamData['domain']? 'http://'.$movieStreamData['domain']:$domainName; 				
				$this->code = 200;
				$this->items['embed_url']=$domainName .'/embed/'.$movieStreamData['embed_id'];			
			}else
				$this->code = 682;			
		}else
			$this->code = 681;		
    }

    /**
     * @method public CheckGeoBlock() Get IP Address from API request and check if allowed in current country
     * @author RKS<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $ip_address IP Address of the users device
     */
    
    public function actionCheckGeoBlock() {
		$ip_address = isset($_REQUEST['ip']) ? $_REQUEST['ip'] : 'No IP';
		if (isset($_REQUEST) && count($_REQUEST) > 0 && isset($_REQUEST['ip'])) {
			$check_anonymous_ip = Yii::app()->mvsecurity->hasMaxmindSuspiciousIP($this->studio_id);
			if ($check_anonymous_ip == 1) {
				$is_anonymous = Yii::app()->mvsecurity->checkAnonymousIP($ip_address);
				if ($is_anonymous > 0) {
					$this->code = 200;
					$this->msg_code = 754;
					return;
				}
			}
			$visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
			$country = $visitor_loc['country'];
			$std_countr = StudioCountryRestriction::model();
			$studio_restr = $std_countr->findByAttributes(array('studio_id' => $this->studio_id, 'country_code' => $country));
			$this->code = (count($studio_restr) > 0) ? 755 : 200;
			$this->items = $country;
		} else {
			$this->code = 635;
		}
	}

    
	/**
     * @method public isRegistrationEnabled It will check wheather the registration is enabled or not and if yes, also return signup step type
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio ,String lang_code
	 * @author Ashis<ashis@muvi.com>
     */
    public function actionIsRegistrationEnabled() {
        $studioData = Studio::model()->findByPk($this->studio_id, array('select'=>'need_login, mylibrary_activated, rating_activated'));
        $is_registration = $studioData->need_login;
		$chromecast = $offline =0;       
        $app = Yii::app()->general->apps_count($this->studio_id);		
		if(empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))){
            $chromecast_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'chromecast');
            if ($chromecast_val['config_value'] == 1 || $chromecast_val == '') {
                $chromecast = 1;
            }
            $offline_val= StudioConfig::model()->getconfigvalueForStudio($this->studio_id,'offline_view');
            if($offline_val['config_value'] == 1 || $offline_val ==''){
                $offline=1;
            }
        }
		$data = array();
        if (intval($is_registration)) {
            $this->code = 200;
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
			$data['isRating '] = (int) $studioData->rating_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = self::CheckFavouriteEnable($this->studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
			$device_status = StudioConfig::model()->getConfig($this->studio_id,'restrict_no_devices');
            $data['isRestrictDevice'] = (isset($device_status['config_value']) && ($device_status['config_value'] != 0)) ? 1 : 0;
			$getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
            $getDeviceRestrictionSetByStudio = 0;
            if (@$getDeviceRestrictionSetByStudioData->config_value != "" && @$getDeviceRestrictionSetByStudioData->config_value > 0) {
                $getDeviceRestrictionSetByStudio = 1;
            }
            $data['is_streaming_restriction'] = $getDeviceRestrictionSetByStudio;
		} else {
            $this->code = 646;
            $data['is_login'] = 0;
        }
		$data['chromecast'] = $chromecast;
        $data['is_offline'] = $offline;
		$this->items = $data;
    }
    
    public function actiongetWebsiteSettings() {
        $studioData = Studio::model()->findByPk($this->studio_id);
        $is_registration = $studioData->need_login;
        if (intval($is_registration)) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = $this->CheckFavouriteEnable($studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
        } else {
            $data['code'] = 455;
            $data['status'] = "failure";
            $data['is_login'] = 0;
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }    

    function getDomainName(){
        $sql = "SELECT is_embed_white_labled, domain FROM studios WHERE id=".$this->studio_id;
        $stData = Yii::app()->db->createCommand($sql)->queryAll();
        $domainName = Yii::app()->getBaseUrl(TRUE);
        if(@$stData[0]['is_embed_white_labled']){
                 $domainName = @$stData[0]['domain']? 'http://'.@$stData[0]['domain']:$domainName; 
        }
        return $domainName;
    }
    
	/**
     * @method public getCardsList It will check if the card can be saved from store's payment gateway and if yes, return lists of card detail
     * @return json Return the json array of results
     * @param String $authToken* Auth token of the studio
     * @param Int $user_id* User id
	 * @author Ashis<ashis@muvi.com>
     */
    public function actionGetCardsList() {
		$studio_id = $this->studio_id;
		$user_id = @$_REQUEST['user_id'];
		$data = array();

		if ($user_id) {
			$gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
			if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
				$gateways = $gateway['gateways'];
				if (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) {
					$this->code = 200;
					$data['can_save_card'] = 1;

					$condcard = array(':studio_id' => $studio_id, ':user_id' => $user_id);
					$cardsql = Yii::app()->db->createCommand()
							->select('c.card_uniq_id AS card_id, c.gateway_id, c.card_name, c.card_holder_name, c.card_last_fourdigit, c.card_type, c.exp_month, c.exp_year')
							->from('sdk_card_infos c')
							->where('c.studio_id=:studio_id AND c.user_id=:user_id AND c.gateway_id !=1 AND c.gateway_id !=4', $condcard, array('order' => 'is_cancelled ASC, id DESC'), array('group' => 'exp_month, exp_year, card_holder_name, card_type, card_last_fourdigit'));
					$cards = $cardsql->queryAll();
					$data['cards'] = (!empty($cards)) ? $cards : array();
				} else {
					$this->code = 647;
					$data['can_save_card'] = 0;
				}
			} else {
				$this->code = 612;
			}
		} else {
			$this->code = 639;
		}
		
		$this->items = $data;
	}

    function actiongetCategoryList(){
        $studio_id = $this->studio_id;
        $cat = Yii::app()->db->createCommand("SELECT * FROM content_category WHERE studio_id={$studio_id} AND parent_id=0 ORDER BY id DESC")->queryAll();
        if(!empty($cat)){
            $cat_img_option = Yii::app()->custom->getCatImgOption($studio_id);
            $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
            foreach($cat as $k => $cat_data) {
                $list[$k]['category_id'] = $cat_data['id'];
                $list[$k]['category_name'] = $cat_data['category_name'];        
                $list[$k]['permalink'] = $cat_data['permalink'];
                $list[$k]['category_img_url'] = $this->getPoster($cat_data['id'], 'content_category','original',$studio_id);    
                $list[$k]['cat_img_size'] = @$cat_img_size;    
            }
            $this->code = 200;
        } else {
            $this->code = 670;
        }
        $this->items = @$list;
    }
    /**
     * @method public getGenreList() returns unique genre value either custom_metadata_field or movie_tags table
     * @author SKM<prakash@muvi.com>
	 * @param string authToken* string lang_code.
     * @return json Json data with parameters
    */
    function actiongetGenreList(){
		$lang_code = @$_REQUEST['lang_code'];		
		$customGenre = CustomMetadataField::model()->findByAttributes(array('studio_id' =>$this->studio_id,'f_id'=>'genre'),array('select' => 'f_value')); 			
			if($customGenre){
				$allgenre= json_decode($customGenre->f_value, TRUE);            
				if((isset($allgenre[$lang_code])) && is_array($allgenre[$lang_code])){
					$this->items = $allgenre[$lang_code]; 				
				}else if((isset($allgenre['en'])) && is_array($allgenre['en'])){
					$this->items = $allgenre['en'];    
				}else {
					$this->items = $allgenre ;
				}
			}else{ 
				$this->items = Yii::app()->db->createCommand()->select('name')->from('movie_tags')->where('taggable_type=1 AND studio_id=:studio_id',array(':studio_id'=>$this->studio_id))->group('name')->order('name')->queryColumn();
		    }		
        $this->code = $this->items ? 200 : 642;
    }
    
    /**
     * @method public CreatePpvPayPalPaymentToken() create a payment token for paypal express checkout
     * @author SKM<sanjeev@muvi.com>
	 * @param string authToken* , int plan_id*, int user_id*, int studio_id*,string movie_id*(uniq id), int is_bundle,int timeframe_id,int is_advance_purchase,int episode_id,string episode_uniq_id,int season_id,string permalink,string couponCode.
     * @return json Json data with parameters
    */
    public function actionCreatePpvPayPalPaymentToken() {
		$request_data = isset($_REQUEST) ? $_REQUEST : array();
		$plan_id = (isset($request_data['plan_id']) && $request_data['plan_id'] > 0) ? $request_data['plan_id'] : 0;
		$user_id = (isset($request_data['user_id']) && $request_data['user_id'] > 0) ? $request_data['user_id'] : 0;
		$studio_id = (isset($request_data['studio_id']) && $request_data['studio_id'] > 0) ? $request_data['studio_id'] : 0;
		$authToken = $request_data['authToken'];
		if ($user_id > 0) {
			if ($studio_id > 0) {
				$studio = Studio::model()->findByPk($studio_id, array('select' => 'domain'));
				if (isset($plan_id) && $plan_id > 0) {
					$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
					}
					$VideoDetails = '';
					$video_id = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $request_data['movie_id']), array('select' => 'id'))->id;
					//Check studio has plan or not
					$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

					$is_bundle = @$request_data['is_bundle'];
					$timeframe_id = @$request_data['timeframe_id'];
					$isadv = @$request_data['is_advance_purchase'];
					$timeframe_days = 0;
					$end_date = '';

					//Get plan detail which user selected at the time of registration
					$plan = PpvPlans::model()->findByPk($plan_id);
					$request_data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;

					if (intval($is_bundle) && intval($timeframe_id)) {
						(array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $request_data['timeframe_id'], $studio_id);
						$price = $price->attributes;
						$timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'], array('select' => 'days'))->days;
					} else {
						$plan_details = PpvPricing::model()->findByAttributes(array('ppv_plan_id' => $plan_id, 'status' => 1));
						$price = Yii::app()->common->getPPVPrices($plan->id, $plan_details->currency_id);
					}
					$currency = Currency::model()->findByPk($price['currency_id'], array('select' => 'id,code,symbol'));
					$data['currency_id'] = $currency->id;
					$data['currency_code'] = $currency->code;
					$data['currency_symbol'] = $currency->symbol;
					//Calculate ppv expiry time only for single part or episode only
					$start_date = Date('Y-m-d H:i:s');
					if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
						$time = strtotime($start_date);
						$expiry = $timeframe_days . ' ' . "days";
						$end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
					} else {
						if (intval($isadv) == 0) {
							$limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
							$limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

							$watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
							$watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

							$access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
							$access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

							$time = strtotime($start_date);
							if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
								$end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
							}
							$data['view_restriction'] = $limit_video;
							$data['watch_period'] = $watch_period;
							$data['access_period'] = $access_period;
						}
					}
					if ($is_bundle) {//For PPV bundle
						$data['season_id'] = 0;
						$data['episode_id'] = 0;
						if (intval($is_subscribed_user)) {
							if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
								$data['amount'] = $amount = $price['show_subscribed'];
							} else {
								$data['amount'] = $amount = $price['price_for_subscribed'];
							}
						} else {
							if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
								$data['amount'] = $amount = $price['show_unsubscribed'];
							} else {
								$data['amount'] = $amount = $price['price_for_unsubscribed'];
							}
						}
					} else {
						//Set different prices according to schemes
						if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
							//Check which part wants to sell by studio admin
							$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'is_show,is_season,is_episode'));
							$is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
							$is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
							$is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

							if (isset($request_data['movie_id']) && trim($request_data['movie_id']) && isset($request_data['season_id']) && intval($request_data['season_id']) && isset($request_data['episode_id']) && trim($request_data['episode_id']) != '0') { //Find episode amount
								if (intval($is_episode)) {
									$streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']), array('select' => 'id,embed_id'));
									$data['episode_id'] = $streams->id;
									$data['episode_uniq_id'] = $streams->embed_id;
									if (intval($is_subscribed_user)) {
										$data['amount'] = $amount = $price['episode_subscribed'];
									} else {
										$data['amount'] = $amount = $price['episode_unsubscribed'];
									}
								}
							} else if (isset($request_data['movie_id']) && trim($request_data['movie_id']) && isset($request_data['season_id']) && intval($request_data['season_id']) && isset($request_data['episode_id']) && trim($request_data['episode_id']) == 0) { //Find season amount
								if (intval($is_season)) {
									$data['episode_id'] = 0;
									if (intval($is_subscribed_user)) {
										$data['amount'] = $amount = $price['season_subscribed'];
									} else {
										$data['amount'] = $amount = $price['season_unsubscribed'];
									}
								}
							} else if (isset($request_data['movie_id']) && trim($request_data['movie_id']) && isset($request_data['season_id']) && trim($request_data['season_id']) == 0 && isset($request_data['episode_id']) && trim($request_data['episode_id']) == 0) { //Find show amount
								if (intval($is_show)) {
									$data['season_id'] = 0;
									$data['episode_id'] = 0;
									if (intval($is_subscribed_user)) {
										$data['amount'] = $amount = $price['show_subscribed'];
									} else {
										$data['amount'] = $amount = $price['show_unsubscribed'];
									}
								}
							}
						} else {//Single part videos
							$data['season_id'] = 0;
							$data['episode_id'] = 0;
							if (intval($is_subscribed_user)) {
								$data['amount'] = $amount = $price['price_for_subscribed'];
							} else {
								$data['amount'] = $amount = $price['price_for_unsubscribed'];
							}
						}
					}
					$couponCode = '';
					//Calculate coupon if exists
					if ((isset($request_data['coupon']) && $request_data['coupon'] != '') || (isset($request_data['coupon_instafeez']) && $request_data['coupon_instafeez'] != '')) {
						$coupon = (isset($request_data['coupon']) && $request_data['coupon'] != '') ? $request_data['coupon'] : $request_data['coupon_instafeez'];
						$getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
						$data['amount'] = $amount = $getCoup["amount"];
						$couponCode = $getCoup["couponCode"];
					}
					if (intval($is_bundle)) {
						$VideoName = $plan->title;
					} else {
						$VideoName = trim(Yii::app()->common->getVideoname($video_id));
					}
					$VideoDetails = $VideoName;

					if (!$is_bundle && isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
						if ($data['season_id'] == 0) {
							$VideoDetails .= ', All Seasons';
						} else {
							$VideoDetails .= ', Season ' . $data['season_id'];
							if ($data['episode_id'] == 0) {
								$VideoDetails .= ', All Episodes';
							} else {
								$episodeName = Yii::app()->common->getEpisodename($streams->id);
								$episodeName = trim($episodeName);
								$VideoDetails .= ', ' . $episodeName;
							}
						}
					}
					$data['studio_id'] = $studio_id;
					$data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];
					$log_data = array(
						'studio_id' => $studio_id,
						'user_id' => $user_id,
						'plan_id' => $plan_id,
						'uniq_id' => $request_data['movie_id'],
						'amount' => $data['amount'],
						'episode_id' => $request_data['episode_id'],
						'episode_uniq_id' => $request_data['episode_uniq_id'],
						'season_id' => $request_data['season_id'],
						'permalink' => $request_data['permalink'],
						'currency_id' => $data['currency_id'],
						'couponCode' => $request_data['coupon'],
						'is_bundle' => @$request_data['is_bundle'],
						'timeframe_id' => @$request_data['timeframe_id'],
						'isadv' => $isadv
					);
					$timeparts = explode(" ", microtime());
					$currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
					$reference_number = explode('.', $currenttime);
					$unique_id = $reference_number[0];
					$log = new PciLog();
					$log->unique_id = $unique_id;
					$log->log_data = json_encode($log_data);
					$log->save();
					if (strpos($studio->domain, 'idogic.com') !== false || strpos($studio->domain, 'muvi.com') !== false) {
						$pre = 'http://www.';
					} else {
						$pre = 'https://www.';
					}
					$muvi_token = $unique_id . "-" . $authToken;
					$data['returnURL'] = $pre . $studio->domain . "/rest/ppvSuccess?muvi_token=" . $muvi_token;
					$data['cancelURL'] = $pre . $studio->domain . "/rest/ppvPayPalCancel?muvi_token=" . $muvi_token;
					$data['user_id'] = $user_id;
					$payment_gateway_controller = 'ApipaypalproController';
					Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
					$payment_gateway = new $payment_gateway_controller();
					$resArr = $payment_gateway::createToken($data);
					if ($resArr['ACK'] == 'Success') {
						$this->code = 200;
						$this->items['url'] = $resArr['REDIRECTURL'];
						$this->items['token'] = $resArr['TOKEN'];
					} else
						$this->code = 691;
				} else
					$this->code = 690;
			} else
				$this->code = 692;
		} else
			$this->code = 639;
	}
	
	/**
     * @method public PpvSuccess() handles paypal retun token and do a expresscheckout
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
    */
    public function actionPpvSuccess() {
        $muvi_token = explode('-',$_REQUEST['muvi_token']);
        $unique_id = $muvi_token[0];
        if(isset($unique_id) && trim($unique_id)){
            $log_data = PciLog::model()->findByAttributes(array('unique_id'=>$unique_id));
            $data = json_decode($log_data['log_data'],true);
            $studio_id = $data['studio_id'];
            $user_id = $data['user_id'];
            
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
            }
            
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $trans_data = $payment_gateway::processCheckoutDetails($_REQUEST['token']);
            $userdata['TOKEN'] = $trans_data['TOKEN'];
            $userdata['PAYERID'] = $trans_data['PAYERID'];
            $userdata['AMT'] = $trans_data['AMT'];
            $userdata['CURRENCYCODE'] = $trans_data['CURRENCYCODE'];
            $resArray = $payment_gateway::processDoPayment($userdata);
            
            if(isset($resArray) && $resArray['ACK'] == 'Success') {
                $plan_id = $data['plan_id'];
                $ip_address = CHttpRequest::getUserHostAddress();
                $isadv = $data['isadv'];
                $planModel = new PpvPlans();
                $plan = $planModel->findByPk($plan_id);
                
                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';
                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
                }

                $Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'uniq_id'=>$data['uniq_id']));
                $video_id = $Films->id;
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
                $VideoDetails = $VideoName;
                $streams = movieStreams::model()->findByAttributes(array('studio_id'=>$studio_id, 'embed_id'=>$data['episode_id']));
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if($data['season_id'] == 0){
                        $VideoDetails .= ', All Seasons';
                    }else{
                        $VideoDetails .= ', Season '.$data['season_id'];
                        if($data['episode_id'] == 0){
                            $VideoDetails .= ', All Episodes';
                        }else{
                            $episodeName = Yii::app()->common->getEpisodename($streams->id);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', '.$episodeName;
                        }
                    }
                }
                $data['amount'] = $trans_data['AMT'];
                $ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['couponCode'], $isadv, $gateway_code);
                $trans_data['transaction_status'] = $resArray['PAYMENTSTATUS'];
                $trans_data['invoice_id'] = $resArray['CORRELATIONID'];
                $trans_data['order_number'] = $resArray['TRANSACTIONID'];
                $trans_data['amount'] = $trans_data['AMT'];
                $trans_data['dollar_amount'] = $trans_data['AMT'];
                $trans_data['response_text'] = json_encode($trans_data);
                $transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                $trans_data['amount'] = $trans_data['AMT'];
                $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
                PciLog::model()->deleteByPk($log_data['id']);
                $res['code'] = 200;
                $res['status'] = "OK";
            }else{
                $res['code'] = 458;
                $res['status'] = "Error";
                $res['msg'] = "Transaction error!";
            }
        }else{
            $res['code'] = 400;
            $res['status'] = "Error";
            $res['msg'] = "Bad Request";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);exit;
    }
    /**
     * @method public PpvPayPalCancel() handles transaction cancled by user
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
    */
    public function actionPpvPayPalCancel() {
        $muvi_token = explode('-',$_REQUEST['muvi_token']);
        $unique_id = $muvi_token[0];
        $log_data = AppPayPalLog::model()->findByAttributes(array('unique_id'=>$unique_id));
        AppPayPalLog::model()->deleteByPk($log_data['id']);
        $data['code'] = 460;
        $data['status'] = "Error";
        $data['msg'] = "Transaction canceled!";
        $this->setHeader($data['code']);
        echo json_encode($data);exit;
    }
    /**@method  return scr part from thirdparty url
     * @author SKM<prakash@muvi.com>
     * @return string
     */
    public function getSrcFromThirdPartyUrl($thirdPartyUrl=''){
        if($thirdPartyUrl!=''){
            preg_match('/src="([^"]+)"/',$thirdPartyUrl, $match);
            if(!empty($match[1])){
                return $match[1];
            }else{
                preg_match("/src='([^']+)'/",$thirdPartyUrl, $match);
                if(!empty($match[1])){
                   return $match[1]; 
                }else{
                   return $thirdPartyUrl; 
                }
            }          
        }else{
            return $thirdPartyUrl;
        }
    }  
    
    /**@method Get Marlin BB Offline content
     * @author SKP<srutikant@muvi.com>
     * @return Json data with parameters
     */
    public function actiongetMarlinBBOffline(){
        $studio_id = $this->studio_id;
        $stream_unique_id = @$_REQUEST['stream_unique_id'];
        if($stream_unique_id != ''){
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $stream_unique_id));
            if($streams){
                 if(@$streams->encryption_key != '' && @$streams->content_key != '' &&  @$streams->is_offline == 1){
                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id,'drm_cloudfront_url');
                    $fileName = substr($streams->full_movie, 0, -4).'.mlv';
                    if(@$getStudioConfig['config_value'] != ''){
                        $fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$streams->id.'/'.$fileName;
                    } else{
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
                        $signedBucketPath = $folderPath['signedFolderPath'];
                        $fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$streams->id.'/'.$fileName;
                    }
                    $token['file'] = $fullmovie_path;
                    $token['token'] = file_get_contents('https://bb-gen.test.expressplay.com/hms/bb/token?customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&actionTokenType=1&rightsType=BuyToOwn&outputControlOverride=urn:marlin:organization:intertrust:wudo,ImageConstraintLevel,0&cookie=MY_TEST01&contentId='.$streams->content_key.'&contentKey='.$streams->encryption_key);
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['data'] = $token;

                }else{
                    $data['code'] = 438;
                    $data['status'] = "Error";
                    $data['msg'] = "No such content found";

                }
            }else{
                $data['code'] = 438;
                $data['status'] = "Error";
                $data['msg'] = "No such content found";

            }
        }else{
            $data['code'] = 438;
            $data['status'] = "Error";
            $data['msg'] = "Stream unique id not found";

        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }  
    
   
    /**
	 * @method public Homepage() It will give section name list
	 * Param string authToken* ,string lang_code 
	 * @author Prakash<support@muvi.com>
	 * @return json object
    */
    public function actionHomepage(){
		$this->items['banner_section_list'] = $this->actiongetBannerSectionList(0);
		$this->items['section_name_list'] = $this->actiongetSectionName(0);
		$this->code = 200;        
    }
    /**
	 * @method public getSectionName() It will give section name list
	 * param string authToken* ,string lang_code 
	 * @author Prakash<support@muvi.com>
	 * @return json object
	 */
    public function actiongetSectionName($nojson=1) {
        $sections = FeaturedSection::model()->findAll('studio_id=:studio_id AND is_active=:is_active  ORDER BY id_seq', array(':studio_id' => $this->studio_id,':is_active' => 1));
		$section =array();
        if($sections){
            foreach($sections as $key=>$sec){
                $section[$key]['studio_id']=$sec->studio_id;
                $section[$key]['language_id']=$sec->language_id;
                $section[$key]['title']=$sec->title;
                $section[$key]['section_id']=$sec->id;
			}
			$this->code = 200;								        
        }else{
			$this->code = 688;			
        }
		if($nojson==1){
			$this->items['section'] = $section;
		}else{
			return $section;
		}
    }
	public function actiongetFeaturedContent() {
		$studio_id = $this->studio_id;
		$fecontents = Yii::app()->db->createCommand()->select('*')->from('homepage')->where('studio_id=:studio_id AND section_id=:section_id', array(':studio_id' => $this->studio_id, ':section_id' => $_REQUEST['section_id']))->queryAll();
		if ($fecontents) {
			$domainName = $this->getDomainName();
			$restriction = StudioContentRestriction::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
			foreach ($fecontents AS $key => $val) {
				$content[$keycon]['is_episode'] = $val['is_episode'];
				if ($val['is_episode'] == 2) {
					$standaloneproduct = Yii::app()->db->createCommand("SELECT * FROM pg_product WHERE id = '" . $val['movie_id'] . "'")->queryAll();
					if ($standaloneproduct) {
						foreach ($standaloneproduct AS $key => $val1) {
							if (Yii::app()->common->isGeoBlockPGContent($val1['id'])) {
								$cont_name = $val1['name'];
								$story = $val1['description'];
								$permalink = $val1['permalink'];
								$short_story = substr(Yii::app()->common->htmlchars_encode_to_html($story), 0, 200);
								$tempprice = Yii::app()->common->getPGPrices($val['id'], Yii::app()->controller->studio->default_currency_id);
								if (!empty($tempprice)) {
									$standaloneproduct[$key]['sale_price'] = $tempprice['price'];
									$standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
								}
								$poster = PGProduct::getpgImage($val1['id'], 'standard');
								$formatted_price = Yii::app()->common->formatPrice($standaloneproduct[$key]['sale_price'], $standaloneproduct[$key]['currency_id']);
								$final_content[$key] = array(
									'movie_id' => $movie_id,
									'title' => utf8_encode($cont_name),
									'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
									'poster' => $poster,
									'data_type' => 4,
									'uniq_id' => $movie_uniq_id,
									'short_story' => utf8_encode($short_story),
									'price' => $formatted_price,
									'status' => $val['status'],
									'is_episode' => 2
								);
							}
						}
					}
				} else {
					$arg['studio_id'] = $studio_id;
					$arg['movie_id'] = $val['movie_id'];
					$arg['season_id'] = 0;
					$arg['episode_id'] = 0;
					$arg['content_types_id'] = $val['content_types_id'];
					$isFreeContent = Yii::app()->common->isFreeContent($arg);
					$embededurl = $domainName . '/embed/' . $val['embed_id'];
					if ($val['is_episode'] == 1) {
						$cont_name = ($val['episode_title'] != '') ? $val['episode_title'] : "SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
						$story = $val['episode_story'];
						$release = $val['episode_date'];
						$poster = Yii::app()->controller->getPoster($val['stream_id'], 'moviestream', 'episode', $this->studio_id);
					} else {
						$cont_name = $val['name'];
						$story = $val['story'];
						$release = $val['release_date'];
						$stream = movieStreams::model()->findByPk($val['stream_id']);
						if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
							$poster = Yii::app()->controller->getPoster($val['movie_id'], 'films', 'episode', $this->studio_id);
						} else {
							$poster = Yii::app()->controller->getPoster($val['movie_id'], 'films', 'standard', $this->studio_id);
						}
					}
					$geo = GeoblockContent::model()->find('movie_id=:movie_id', array(':movie_id' => $val['movie_id']));
					//Get view count status
					$viewStatus = VideoLogs::model()->getViewStatus($val['movie_id'], $studio_id);
					if (@$viewStatus) {
						$viewStatusArr = '';
						foreach ($viewStatus AS $valarr) {
							$viewStatusArr['viewcount'] = $valarr['viewcount'];
							$viewStatusArr['uniq_view_count'] = $valarr['u_viewcount'];
						}
					}
					if (isset($stream['content_publish_date']) && @$stream['content_publish_date'] && $stream['content_publish_date'] > gmdate("Y-m-d H:i:s")) {
						
					} else {
						$final_content[$key]['is_episode'] = $val['is_episode'];
						$final_content[$key]['movie_stream_uniq_id'] = $val['embed_id'];
						$final_content[$key]['movie_id'] = $val['movie_id'];
						$final_content[$key]['movie_stream_id'] = $val['stream_id'];
						$final_content[$key]['muvi_uniq_id'] = $val['uniq_id'];
						$final_content[$key]['content_type_id'] = $val['content_type_id'];
						$final_content[$key]['ppv_plan_id'] = $val['ppv_plan_id'];
						$final_content[$key]['permalink'] = $val['fplink'];
						$final_content[$key]['name'] = utf8_encode($cont_name);
						$final_content[$key]['full_movie'] = $stream->full_movie;
						$final_content[$key]['story'] = $story;
						$final_content[$key]['genre'] = json_decode($val['genre']);
						$final_content[$key]['censor_rating'] = ($val['censor_rating'] != '') ? implode(',', json_decode($val['censor_rating'])) . '&nbsp;' : '';
						$final_content[$key]['release_date'] = $release;
						$final_content[$key]['content_types_id'] = $val['content_types_id'];
						$final_content[$key]['is_converted'] = $val['is_converted'];
						$final_content[$key]['last_updated_date'] = '';
						$final_content[$key]['movieid'] = $geo->movie_id;
						$final_content[$key]['geocategory_id'] = $geo->geocategory_id;
						$final_content[$key]['category_id'] = $restriction->category_id;
						$final_content[$key]['studio_id'] = $studio_id;
						$final_content[$key]['country_code'] = $restriction->country_code;
						$final_content[$key]['ip'] = $restriction->ip;
						$final_content[$key]['poster_url'] = $poster;
						$final_content[$key]['isFreeContent'] = $isFreeContent;
						$final_content[$key]['embeddedUrl'] = $embededurl;
						$final_content[$key]['viewStatus'] = $viewStatusArr;
					}
				}
			}
			$this->code = 200;
			$this->items = $final_content;
		} else {
			$this->code = 756;
		}
	}

    
	/*
     * END 6204
    */   
    
    /* API to initilise the sdk 
     * By <suraja@muvi.com>,ticket #6247
     */
     public function actioninitialiseSdk(){
        $studio_id = $this->studio_id;
        //check whether the hask key exists for the studio or not ?
       $apiKeyData = OuathRegistration::model()->find('studio_id=:studio_id AND status=:status',array(':studio_id'=>$studio_id,':status'=>'1'));
       if(@$apiKeyData){
       $hashkey=$apiKeyData->hask_key;
       $packg_name=$apiKeyData->package_name;
       if(empty(trim($hashkey))){           
           //generate the new hask key and send it.
           $key = md5(time());
           $hashkey=str_replace(" ","",$packg_name);
           //$hask_key=$hashkey.$key;          
           $apiKeyData->hask_key=$key;
           $apiKeyData->save();
           $data['code'] = 200;
           $data['status'] = "OK";
           $data['hashkey'] = $key;
           $data['pkgname'] = $packg_name;
           $data['msg'] = "Hashkey created successfully";     
        }else{
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['hashkey'] = $hashkey;
            $data['pkgname'] = $packg_name;
            $data['section'] = "Hashkey already exists";           
        }
       }
       
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    public function actionTextTranslation(){
        $studio_id = $this->studio_id;
        if(isset($_REQUEST['lang_code']) && $_REQUEST['lang_code']!=""){
            $lang_code = $_REQUEST['lang_code'];
            $studio = Studio::model()->findByPk($studio_id,array('select'=>'theme'));
            $theme = $studio->theme;
            if(file_exists(ROOT_DIR."languages/".$theme."/".trim($lang_code).".php")){
               $lang = include( ROOT_DIR."languages/".$theme."/".trim($lang_code).".php");
            }elseif(file_exists(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php')){
                $lang = include(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php');
            }else{
                $lang = include(ROOT_DIR . 'languages/studio/en.php');
            }
            $language = Yii::app()->controller->getAllTranslation($lang,$studio_id);
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['translation'] = $language;
        }else{
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Language code not found.";       
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    public function actionGetLanguageList(){
        $studio_id= $this->studio_id;
        $data['code'] = 200;
        $data['status'] = "OK";
        $con = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.translated_name,sl.status,sl.frontend_show_status FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE  l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        
        $studio_languages = $con->createCommand($sql)->queryAll();
        $lang_list= array();
        $i=0;
        foreach($studio_languages as $lang){
            if(($lang['frontend_show_status'] != "0") && ($lang['status'] == 1 || $lang['code'] == 'en')){
                if(trim($lang['translated_name']) !=""){
                    $lang['name'] = $lang['translated_name'];
                }
                $lang_list[$i]['code'] = $lang['code'];
                $lang_list[$i]['language'] = $lang['name'];
                $i++;
            }
        }
        $data['lang_list'] = $lang_list;
        $data['default_lang'] = $studio_languages[0]['default_language'];
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    /**
     * @method public PurchaseHistory() It will display list of purchase History data 
     * @param string authToken* , int user_id* ,string lang_code
     * @author Sunil Nayak<support@muvi.com>
     * @return Json string
     */
	public function actionPurchaseHistory() {
		$user_id = @$_REQUEST['user_id'];
		$page_number = @$_REQUEST['limit'];
		$studio_id = $this->studio_id;
		if ($user_id > 0) {
			$subscribed_user_data = Yii::app()->common->isSubscribed($user_id, $studio_id, 1);
			$userdate['expire_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date . '-1 Days'));
			$userdate['nextbilling_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date));
			//Pagination codes
			$items_per_page = 5;
			$page_size = $limit = $items_per_page;
			$offset = 0;
			if (isset($page_number)) {
				$offset = ($page_number - 1) * $limit;
			} else {
				$page_number = 1;
			}
			$transaction = Yii::app()->db->createCommand()
							->select('t.id, transaction_date, currency_id, amount, user_id, plan_id, payer_id, invoice_id ,subscription_id,ppv_subscription_id,movie_id,order_number,transaction_type,transaction_status,symbol AS currency_symbol,code AS currency_code')
							->from('transactions t')
							->join('currency c', 't.currency_id=c.id')
							->where('studio_id=:studio_id AND user_id=:user_id', array(':studio_id' => $studio_id, ':user_id' => $user_id))
							->order('t.id DESC')
							->limit($limit, $offset)->queryAll();
			$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();
			$planModel = new PpvPlans();
			$transactions = array();
			$currdatetime = strtotime(date('y-m-d'));
			foreach ($transaction as $key => $details) {
				$transactions[$key]['invoice_id'] = $details['invoice_id'];
				$transactions[$key]['transaction_date'] = date('F d, Y', strtotime($details['transaction_date']));
				$transactions[$key]['amount'] = $details['amount'];
				$transactions[$key]['transaction_status'] = $details['transaction_status'];
				$transactions[$key]['currency_symbol'] = $details['currency_symbol'];
				$transactions[$key]['currency_code'] = $details['currency_code'];
				$transactions[$key]['id'] = $this->getUniqueIdEncode($details['id']);
				if ($details['transaction_type'] == 1) {
					$trans_type = 'Monthly subscription';
					$subscription_id = $details['subscription_id'];
					$startdate = UserSubscription::model()->findByAttributes(array('id' => $subscription_id), array('select' => 'start_date'))->start_date;
					//Check current date less than start date
					if ($currdatetime <= strtotime($startdate)) {
						$transactions[$key]['statusppv'] = 'Active';
					} else {
						$transactions[$key]['statusppv'] = 'N/A';
					}
					$transactions[$key]['Content_type'] = 'digital';
				} elseif ($details['transaction_type'] == 2) {
					$ppv_subscription_id = $details['ppv_subscription_id'];
					$subscription_details = PpvSubscription::model()->findByPk($ppv_subscription_id, array('select' => 'movie_id,season_id,episode_id,end_date'));
					$end_date = @$subscription_details->end_date;
					$season_id = @$subscription_details->season_id;
					$episode_id = @$subscription_details->episode_id;
					$movie_id = @$subscription_details->movie_id;
					$film_data = Film::model()->findByAttributes(array('id' => $movie_id), array('select' => 'id,name,content_types_id'));
					$movie_name = @$film_data->name;
					$content_types_id = @$film_data->content_types_id;
					$episode_name = ($episode_id > 0) ? $this->getEpisodeName($episode_id) : "";
					if ($season_id != 0 && $episode_id != 0) {
						$transactions[$key]['movie_name'] = $movie_name . " -> Season #" . $season_id . " ->" . $episode_name;
					} else if ($season_id == 0 && $episode_id != 0) {
						$transactions[$key]['movie_name'] = $movie_name . " -> " . $episode_name;
					} else if ($season_id != 0 && $episode_id == 0) {
						$transactions[$key]['movie_name'] = $movie_name . " -> Season #" . $season_id;
					} else {
						$transactions[$key]['movie_name'] = $movie_name;
					}
					$statusppv = 'N/A';
					if ($content_types_id == 1) {
						if ($currdatetime <= strtotime($end_date) && ($end_date != '0000-00-00 00:00:00')) {
							$statusppv = 'Active';
						}
					} else if ($content_types_id == 3) {
						if ($currdatetime <= strtotime($end_date) && ($end_date != '0000-00-00 00:00:00')) {
							$statusppv = 'Active';
						} else if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
							$statusppv = 'Active';
						} else if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
							$statusppv = 'Active';
						}
					}
					$transactions[$key]['statusppv'] = $statusppv;
					if (strtotime($end_date) != '' && $end_date != '0000-00-00 00:00:00') {
						$transactions[$key]['expiry_dateppv'] = date('F d, Y', strtotime($end_date));
					} else {
						$transactions[$key]['expiry_dateppv'] = 'N/A';
					}
					$trans_type = 'Pay-per-view' . "-" . $transactions[$key]['movie_name'];
					$transactions[$key]['Content_type'] = 'digital';
				} else if ($details['transaction_type'] == 3) {
					$title = $planModel->findByPk($details['plan_id'], array('select' => 'title'))->title;
					$transactions[$key]['movie_name'] = $title;
					$trans_type = 'Pre-order' . "<br>" . $title;
					$transactions[$key]['Content_type'] = 'digital';
				} else if ($details['transaction_type'] == 4) {
					$trans_type = 'physical';
					$orderquery = "SELECT d.id,d.name,d.price,d.quantity,d.product_id,d.item_status,d.cds_tracking,d.tracking_url,o.order_number,o.cds_order_status FROM pg_order_details d, pg_order o WHERE d.order_id=o.id AND o.transactions_id=" . $details['id'] . " ORDER BY id DESC";
					$order = Yii::app()->db->createCommand($orderquery)->queryAll();
					$transactions[$key]['details'] = $order;
					$transactions[$key]['order_item_id'] = $order[0]['id'];
					$transactions[$key]['order_number'] = $order[0]['order_number'];
					$transactions[$key]['cds_order_status'] = $order[0]['cds_order_status'];
					$transactions[$key]['cds_tracking'] = $order[0]['cds_tracking'];
					$transactions[$key]['tracking_url'] = $order[0]['tracking_url'];
					$transactions[$key]['Content_type'] = 'physical';
				} else if ($details['transaction_type'] == 5) {
					$title = $planModel->findByPk($details['plan_id'], array('select' => 'title'))->title;
					$ppv_subscription_id = $details['ppv_subscription_id'];
					$ppvplanid = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id), array('select' => 'timeframe_id'))->timeframe_id;
					$ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id' => $ppvplanid), array('select' => 'validity_days'))->validity_days;
					$transaction_dates = $details['transaction_date'];
					$expirydate = date('F d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
					$expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
					if ($currdatetime <= $expirytime) {
						$statusbundles = 'Active';
					} else {
						$statusbundles = 'N/A';
					}
					$transactions[$key]['expiry_date'] = ($expirydate == "") ? $expirydate : "N/A";
					$transactions[$key]['movie_name'] = $title;
					$transactions[$key]['Content_type'] = 'digital';
					$trans_type = 'Pay-per-view Bundle' . "-" . $title;
					$transactions[$key]['statusppv'] = $statusbundles;
				} else {
					$trans_type = '';
				}
				$transactions[$key]['transaction_for'] = $trans_type;
			}
			$this->code = 200;
			$this->items['section'] = $transactions;
		} else
			$this->code = 663;
	}

	//get movie name by suniln
     public function getFilmName($movie_id){
        $movie_name = Yii::app()->db->createCommand("SELECT name FROM films WHERE id=".$movie_id)->queryROW();
        return $movie_name['name'];
    }
    //get episode name by suniln
     public function getEpisodeName($episode_id){
        $movie_name = Yii::app()->db->createCommand("SELECT episode_title FROM movie_streams WHERE id=".$episode_id)->queryROW();
        return $movie_name['episode_title'];
    }
    function getUniqueIdEncode($str){
       for($i=0; $i<5;$i++){
            //apply base64 first and then reverse the string
            $str=strrev(base64_encode($str));
        }
            return $str;
    }
    function getUniqueIdDecode($str){
         for($i=0; $i<5;$i++){
            //apply reverse the string first and then base64
            $str=base64_decode(strrev($str));
        }
            return $str;
    }
	/**
     * @method public Transaction() It will give details of the transaction
     * @author suniln<support@muvi.com>
     * @param string authToken* ,int user_id*,string encoded_id*,string lang_code    
     * @return json object
     */
	public function actionTransaction() {
		$user_id = @$_REQUEST['user_id'];
		if ($user_id > 0) {
			$encoded_id = (isset($_REQUEST['encoded_id']) && $_REQUEST['encoded_id'] != '') ? $_REQUEST['encoded_id'] : '';
			$id = $this->getUniqueIdDecode($encoded_id);
			if ($id > 0) {
				$transaction = Yii::app()->general->getTransactionDetails($id, $user_id, 1);
				$transaction_details['payment_method'] = $transaction['payment_method'];
				$transaction_details['transaction_status'] = $transaction['transaction_status'];
				$transaction_details['transaction_date'] = $transaction['transaction_date'];
				$transaction_details['amount'] = $transaction['amount'];
				$transaction_details['currency_symbol'] = $transaction['currency_symbol'];
				$transaction_details['currency_code'] = $transaction['currency_code'];
				$transaction_details['invoice_id'] = $transaction['invoice_id'];
				$transaction_details['order_number'] = $transaction['order_number'];
				$transaction_details['plan_name'] = $transaction['plan_name'];
				$transaction_details['plan_recurrence'] = $transaction['plan_recurrence'];
				$this->code = 663;
				$this->items['section'] = $transaction_details;
			} else
				$this->code = 689;
		} else
			$this->code = 663;
	}

	/**
	* @method public getPurchaseInvoice It returns invoice path
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int $user_id*
	* @param String device_type* app
	* @param Int $transaction_id* 
	*/
    function actionGetPurchaseInvoice() {
		$data = array();
		if ($_REQUEST['user_id'] && $_REQUEST['device_type'] && $_REQUEST['transaction_id']) {
			$transaction_id = $this->getUniqueIdDecode(@$_REQUEST['transaction_id']);
			$name = Yii::app()->pdf->downloadUserinvoice($this->studio_id, $transaction_id, @$_REQUEST['user_id'], '', '', @$_REQUEST['device_type']);
			if (trim($name)) {
				$this->code = 200;
				$data['invoice'] = $name;
			} else {
				$this->code = 663;
}
		} else {
			$this->code = 662;
		}
        
		$this->items = $data;
        }

	/**
	* @method public deletePurchaseInvoice It deletes the purchase invoice
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param String $invoice*
	*/
	function actionDeletePurchaseInvoice() {
		if ($_REQUEST['invoice']) {
			$invoice_path = ROOT_DIR . 'docs/' . $_REQUEST['invoice'];
			if (file_exists($invoice_path)) {
				unlink($invoice_path);
				$this->code = 200;
			} else {
				$this->code = 664;
        }
		} else {
			$this->code = 662;
        }
    }

/**
 * @purpose get the cast and crew
 * @param movie_id Mandatory
 * @Note Applicable only for single part, multipart content. Not for live streams and episodes
 * @Author <ajit@muvi.com>
 */
    function actiongetCelibrity(){
		$movie_uniqueid = @$_REQUEST['movie_id'];
		if ($movie_uniqueid) {
			$studio_id = $this->studio_id;
			$Films = Film::model()->find(array('select' => 'id', 'condition' => 'studio_id=:studio_id and uniq_id=:uniq_id', 'params' => array(':studio_id' => $studio_id, ':uniq_id' => $movie_uniqueid)));
			$movie_id = $Films->id;
			if ($movie_id) {
				$sql = "SELECT C.id,C.name,C.birthdate,C.birthplace,C.summary,C.twitterid,C.permalink,C.alias_name,C.meta_title,C.meta_description,C.meta_keywords FROM celebrities C WHERE C.id IN(SELECT celebrity_id FROM movie_casts MC WHERE MC.movie_id = " . $movie_id . ") AND C.studio_id = " . $studio_id;
				$command = Yii::app()->db->createCommand($sql);
				$list = $command->queryAll();
				if ($list) {
					foreach ($list as $key => $val) {
						//get celebrity type
						$cast_name = MovieCast::model()->find(array('select' => 'cast_type', 'condition' => 'celebrity_id=:celebrity_id', 'params' => array(':celebrity_id' => $val['id'])));
						$celebrity[$key]['name'] = $val['name'];
						$celebrity[$key]['permalink'] = $val['permalink'];
						$celebrity[$key]['summary'] = $val['summary'];
						$celebrity[$key]['cast_type'] = $cast_name->cast_type;
						$celebrity[$key]['celebrity_image'] = $this->getPoster($val['id'], 'celebrity', 'medium', $studio_id);
					}
					$data['code'] = 200;
					$data['status'] = "OK";
					$data['celibrity'] = $celebrity;
				} else {
					$data['code'] = 448;
					$data['status'] = "Failure";
					$data['msg'] = "Celebrity not found.";
				}
			} else {
				$data['code'] = 448;
				$data['status'] = "Failure";
				$data['msg'] = "Records for this id not found";
			}
		} else {
			$data['code'] = 448;
			$data['status'] = "Failure";
			$data['msg'] = "Required parameters not found";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}

	function actiongetEmojis(){
        $emo = Emojis::model()->findAll();        
            if($emo){
            foreach($emo as $key => $val){                
                $set[$key]['code'] = $val['code'];
                $set[$key]['name'] = $val['emoji_name'];
                $set[$key]['url'] = $val['emoji_url'];
            }
                $data['code'] = 200;
                $data['status'] = "OK";
            $data['emojis'] = $set;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Emoji not found.";            
            }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
/**
 * @method public assignBroadCastToContent() It will create content against broadcaster under a category. 
 * @author Gayadhar<support@muvi.com>
 * @param string $content_name Name of the BroadCast
 * @param string $category_id Id of the content category
 * @param int $user_id Logged in user id
 * @param text $description Description of the content
 * @return HTML 
 */	
	function actionAssignBroadCastToContent(){
		$required_params = array('category_id'=>1,'content_name'=>1,'user_id'=>1);
		if(@$_REQUEST['category_id']){
			unset($required_params['category_id']);
		}
		if(@$_REQUEST['content_name']){
			unset($required_params['content_name']);
		}
		if(@$_REQUEST['user_id']){
			unset($required_params['user_id']);
		}
		if(empty($required_params)){
			$studio_id = $this->studio_id;
			$arr['succ']= 0;
			$Films = new Film();
			$data['name'] = $_REQUEST['content_name'];
			$data['story'] = @$_REQUEST['description'];
			$catData = Yii::app()->db->CreateCommand("SELECT binary_value,category_name,permalink FROM content_category WHERE id={$_REQUEST['category_id']} AND studio_id={$this->studio_id}")->queryRow();
			$data['content_category_value'] = array($catData['binary_value']);
			$data['parent_content_type'] = 2;
			$data['content_types_id'] = 4;
			$movie = $Films->addContent($data,$this->studio_id);
			$movie_id = $movie['id'];
			$arr['uniq_id'] = $movie['uniq_id'];
			
            //Live stream set up
            $streamUrl = $movie['permalink'].'-'.strtotime(date("Y-m-d H:i:s"));
            $length = 8;
            $userName = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $enc = new bCrypt();
            $passwordEncrypt = $enc->hash($password);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=save&is_active=1&stream_name=".$streamUrl."&user_name=".$userName."&password=".$passwordEncrypt."&serverUrl=".Yii::app()->getBaseUrl(TRUE)."/conversion/stopLiveStreaming?movie_id".$movie_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = trim(curl_exec($ch));
            curl_close($ch);
            if($server_output != 'success'){
                Film::model()->deleteByPk($movie_id);
                $data['code'] = 458;
                $data['status'] = "Failure";
                $data['msg'] = "Due to some reason live stream content was unable to create. Please try again"; 
                $this->setHeader($data['code']);
                echo json_encode($data);exit;
            }

			//Adding permalink to url routing 
			$urlRouts['permalink'] = $movie['permalink'];
			$urlRouts['mapped_url'] = '/movie/show/content_id/'.$movie_id;
			$urlRoutObj = new UrlRouting();
			$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);
			
			//Insert Into Movie streams table
			$MovieStreams = new movieStreams();
			$MovieStreams->studio_id = $studio_id;
			$MovieStreams->movie_id = $movie_id;
			$MovieStreams->is_episode = 0;
			$MovieStreams->created_date = gmdate('Y-m-d H:i:s');
			$MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
			$embed_uniqid = Yii::app()->common->generateUniqNumber();
			$MovieStreams->embed_id = $embed_uniqid; 
			$publish_date = NULL;
			$MovieStreams->content_publish_date = $publish_date;
			$MovieStreams->save();

			//Upload Poster
			//$this->processContentImage(4,$movie_id,$MovieStreams->id);
			
			//Adding the feed infos into live Stream table
            $feedData['feed_url'] = 'rtmp://'.nginxserverip.'/live/'.$streamUrl;
            $feedData['stream_url'] = 'rtmp://'.nginxserverip.'/live';
            $feedData['stream_key'] = $streamUrl."?user=".$userName."&pass=".$password;
			$feedData['feed_type'] = 2;
            $feedData['is_recording'] = 0;
			$feedData['method_type'] = 'push';
            $feedData['start_streaming'] = 1;
			
			$livestream = new Livestream();
			$livestream->saveFeeds($feedData,$studio_id,$movie_id,$_REQUEST['user_id']);
			if(HOST_IP !='127.0.0.1'){
				$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
				$solrArr['content_id'] = $movie_id;
				$solrArr['stream_id'] = $MovieStreams->id;
				$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
				$solrArr['is_episode'] = 0;
				$solrArr['name'] = $movie['name'];
				$solrArr['permalink'] = $movie['permalink'];
				$solrArr['studio_id'] =  $studio_id;
				$solrArr['display_name'] = 'livestream';
				$solrArr['content_permalink'] =  $movie['permalink'];
				$solrObj = new SolrFunctions();
				$ret = $solrObj->addSolrData($solrArr);
			}  
			$data = array();
			$data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = 'Broadcast Content created successfully.';
            $data['feed_url'] = $feedData['stream_url']."/".$feedData['stream_key'];
            $data['stream_url'] = $feedData['stream_url'];
            $data['stream_key'] = $feedData['stream_key'];
            $data['movie_id'] = $movie_id;
			$data['uniq_id'] = $movie['uniq_id'];
		}else{
			$data['code'] = 458;
            $data['status'] = "Failure";
            $data['msg'] = implode(',',$required_params).": required param(s) missing";    
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
        
    /**
     * @purpose get the total no of videos and total duration of videos
     * @param authToken Mandatory
     * @Author Biswajit Parida<biswajit@muvi.com>
     */
    public function actionGetTotalNoOfVideo(){
        $mapped_videos = 0;
        $mapped_duration = $total_duration =  0;
        $studio_id = $this->studio_id;
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*,ms.id as movie_stream_id,ms.is_converted as movieConverted,mt.is_converted as trailerConverted FROM video_management vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) LEFT JOIN movie_trailer mt ON (vm.id=mt.video_management_id) WHERE  vm.studio_id='.$studio_id.' GROUP BY vm.id  order by vm.video_name ASC';
        $allvideo = Yii::app()->db->createCommand($sql)->queryAll();
        $total_videos = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        if(!empty($allvideo)){
            foreach($allvideo as $videos){
                $duration = Yii::app()->custom->convertTimeToMiliseconds($videos['duration']);
                if(trim($duration) == ""){
                    $duration = 0;
                }
                $total_duration += $duration;
                if(($videos['movieConverted'] == 1)){
                    $mapped_videos++;
                    $mapped_duration += $duration;
                }elseif(($videos['trailerConverted'] == 1)){
                    $mapped_videos++;
                    $mapped_duration += $duration;
                }
            }
        }
        //$total_duration  = Yii::app()->custom->formatMilliseconds($total_duration);
        //$mapped_duration = Yii::app()->custom->formatMilliseconds($mapped_duration);
        $data = array(
            'total_videos'    => $total_videos,
            'total_duration'  => $total_duration,
            'mapped_videos'   => $mapped_videos,
            'mapped_duration' => $mapped_duration
        );
       echo json_encode($data);exit;
    }
    /**
     * @purpose get the categories of a content
     * @param authToken,content_category_value Mandatory, lang_code Optional
     * @Author Biswajit Parida<biswajit@muvi.com>
     */
    public function actionGetCategoriesOfContent(){
        $lang_code   = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : 'en';
        $language_id = Yii::app()->custom->getLanguage_id($lang_code); 
        $studio_id   = $this->studio_id;
        $sql = "SELECT id,binary_value,category_name,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'binary_value', 'category_name');
        $content_category_value = $_REQUEST['content_category_value']; 
        $categories = Yii::app()->Helper->categoryNameFromBvalue($content_category_value,$contentCategories);
        echo $categories;exit;
    }
/**
 * @method public GetStudioAuthKey() It will validate the login credential & return the authToken for that studio. 
 * @author Gayadhar<support@muvi.com>
 * @param string $email Loggin email address
 * @param string $password studio login password
 * @return JSON will return the authToken for the respective studio
 */	
	function actionGetStudioAuthkey(){
		if($_REQUEST['email'] && $_REQUEST['password']){
			$userData = User::model()->find('email=:email AND role_id =:role_id',array(':email'=>$_REQUEST['email'],'role_id'=>1));
			if($userData){
				$enc = new bCrypt();
				if($enc->verify($_REQUEST['password'], $userData->encrypted_password)){
					$sql = "SELECT * FROM oauth_registration WHERE studio_id={$userData->studio_id} and status=1";
					$oauthData = Yii::app()->db->createCommand($sql)->queryRow();
					if ($oauthData){
						$referer = '';
						if (isset($_SERVER['HTTP_REFERER'])) {
							$referer = parse_url($_SERVER['HTTP_REFERER']);
							$referer = $referer['host'];
						}
						$oauthData = (object) $oauthData;
						if ($oauthData->request_domain && ($oauthData->request_domain != $referer)) {
							$data['code'] = 409;
							$data['status'] = "failure";
							$data['msg'] = "Domain not registered with Muvi for API calls!";
							echo json_encode($data);exit;
						}
						if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
							$data['code'] = 410;
							$data['status'] = "failure";
							$data['msg'] = "Oauth Token expired!";
							echo json_encode($data);exit;
						}
						$data['code'] = 200;
						$data['authToken'] = $oauthData->oauth_token;
						$data['status'] = "OK";
						$data['msg'] = "Success";
					}else{
						$data['code'] = 462;
						$data['status'] = "Failure";
						$data['msg'] = "AuthToken not available, Contact admin to generate the token";   
					}
				}else{
					$data['code'] = 461;
					$data['status'] = "Failure";
					$data['msg'] = "Invalid login credential";   
				}
			}else{
				$data['code'] = 460;
				$data['status'] = "Failure";
				$data['msg'] = "No studio available with the given credential!"; 
			}
		}else{
			$data['code'] = 459;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid Login credential";    
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
/**
 * @purpose get the categories of a content
 * @param authToken,user_id Mandatory
 * @<ajit@muvi.com>
 */
	public function actionMyLibrary(){
        $user_id   = $_REQUEST['user_id'];
        $studio_id   = $this->studio_id;
		$lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
		$translate = $this->getTransData($lang_code, $studio_id);
        if(isset($user_id) && intval($user_id) && $user_id !=0 ){            
            $transactions = Yii::app()->db->createCommand()
                ->select('plan_id,subscription_id,ppv_subscription_id,transaction_type')
                ->from('transactions')
                ->where('studio_id = '.$studio_id.' AND user_id='.$user_id.' AND transaction_type IN (2,5,6)')
                ->order('id DESC')    
                ->queryAll();
            
            $planModel = new PpvPlans();
			$library = array();$newarr= array();$free=array();
            foreach ($transactions as $key => $details) {
                $ppv_subscription_id=$details['ppv_subscription_id'];
                $ppv_subscription = PpvSubscription::model()->findByAttributes(array('id'=>$ppv_subscription_id));
                if($details['transaction_type']==2){                    
                    $film = Film::model()->findByAttributes(array('id'=>$ppv_subscription->movie_id));
                    $movie_id = $ppv_subscription->movie_id;
                    $end_date = $ppv_subscription->end_date ? $ppv_subscription->end_date=='1970-01-01 00:00:00'?'0000-00-00 00:00:00':$ppv_subscription->end_date:'0000-00-00 00:00:00';                                       
                    $season_id = $ppv_subscription->season_id ;
                    $episode_id = $ppv_subscription->episode_id ; 
                    $movie_name = isset($movie_id)?$this->getFilmName($movie_id):"";                    
                    $content_types_id = $film->content_types_id;
                    $episode_name = isset($episode_id)?$this->getEpisodeName($episode_id):"";
                    $movie_names=$movie_name;
                    if($season_id!=0 && $episode_id!=0){
                      $movie_names=$movie_name." -> ".$translate['season']." ".$season_id." ->".$episode_name; 
                    }
                    if($season_id==0 && $episode_id!=0){
                     $movie_names=$movie_name." -> ".$episode_name;  
                    }
                    if($season_id!=0 && $episode_id==0){
                      $movie_names=$movie_name." -> ".$translate['season']." ".$season_id;  
                    }                    
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($end_date);
                    $statusppv='N/A';
                    if($content_types_id==1){
                        if($currdatetime<=$expirytime && (strtotime($end_date)!='' || $end_date!='0000-00-00 00:00:00')){
                           $statusppv='Active';  
                        }
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                        if($end_date=='0000-00-00 00:00:00'){
                          $statusppv='Active';  
                    }
                    }
                    if($content_types_id==4){
                        if($currdatetime<=$expirytime && (strtotime($end_date)!='' || $end_date!='0000-00-00 00:00:00')){
                            $statusppv='Active'; 
                        }
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                        if($end_date=='0000-00-00 00:00:00'){
                          $statusppv='Active';  
                    } 
                     }
                    if($content_types_id==3){
                        $datamovie_stream = Yii::app()->db->createCommand()
                            ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                            ->from('movie_streams ms ')
                            ->where('ms.id='.$episode_id)
                            ->queryRow();  
                        $embed_id=$datamovie_stream['embed_id'];
                        if($currdatetime<=$expirytime && strtotime($end_date)!='0000-00-00 00:00:00'){
                           $statusppv='Active';  
                        }
                    if($season_id!=0 && $episode_id==0){
                        $statusppv='Active'; 
                    } 
                    if($end_date=='0000-00-00 00:00:00'){
                      $statusppv='Active';  
                    }
                        if($movie_id!=0 && $season_id==0 && $episode_id==0 ){
                         $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                        }
                        if($movie_id!=0 && $season_id!=0 && $episode_id==0){
                          $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'?season='.$season_id;
                        }
                        if($movie_id!=0 && $season_id!=0 && $episode_id!=0){
                          $baseurl=Yii::app()->getBaseUrl(true).'/player/'.$film->permalink.'/stream/'.$embed_id;
                        }
                    } 
                    if($statusppv=='Active'){
                        if($content_types_id == 2){
                           $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                        }else{
                           $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                    }
                       if($content_types_id==3){
							if($episode_id!=0){
						 $stm = movieStreams::model()->findByAttributes(array('id'=>$episode_id));
							}else if($season_id!=0 && $episode_id==0){
						 $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id,'is_episode'=>0));
							}else{
						 $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
							}
                        }else{
                            $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
                        }
                        $library[$key]['name'] = $movie_names;
                        $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                        $library[$key]['movie_id'] = $movie_id;
                        $library[$key]['movie_stream_id'] = $stm->id;
                        $library[$key]['is_episode'] = ($episode_id!=0)?'1':isset($stm->is_episode)?$stm->is_episode:0;
                        $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                        $library[$key]['content_types_id'] = $film->content_types_id;
                        $library[$key]['content_type_id'] = $film->content_type_id;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster_url'] = $poster;
                        $library[$key]['video_duration'] = $stm->video_duration;   
                        $library[$key]['genres'] = json_decode($film->genre);
                        $library[$key]['permalink'] = $film->permalink;
                        $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                        $library[$key]['full_movie'] = $stm->full_movie;
                        $library[$key]['story'] = $film->story;;
                        $library[$key]['release_date'] = $film->release_date;
                        $library[$key]['is_converted'] = $stm->is_converted;
                        $library[$key]['isFreeContent'] = 0;
                        $library[$key]['season_id'] = $season_id;
                    }
                }elseif($details['transaction_type']==5){                    
                    $ppvplanid = PpvSubscription::model()->find(array('select'=>'timeframe_id','condition' => 'id=:id','params' => array(':id' =>$ppv_subscription_id)))->timeframe_id;                    
                    $ppvtimeframes = Ppvtimeframes::model()->find(array('select'=>'validity_days','condition' => 'id=:id','params' => array(':id' =>$ppvplanid)))->validity_days;
                    $ppvbundlesmovieid = Yii::app()->db->createCommand()
                        ->SELECT('content_id')
                        ->from('ppv_advance_content')
                        ->where('ppv_plan_id='.$details['plan_id'])
                        ->queryAll();
                    $expirydate= date('M d, Y', strtotime($transaction_dates. ' + '.$ppvtimeframes.' days'));
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($transaction_dates. ' + '.$ppvtimeframes.' days');
                    if($currdatetime<=$expirytime){
                        foreach($ppvbundlesmovieid as $keyb=>$contentid){
                            $movie_id=$contentid['content_id']; 
                            $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$movie_id)));
                            if($film->content_types_id == 2){
                               $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            }else{
                               $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                        }                      
                        $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
                        
                        $library[$key]['name'] = $film->name;
                        $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                        $library[$key]['movie_id'] = $movie_id;
                        $library[$key]['movie_stream_id'] = $stm->id;
                        $library[$key]['is_episode'] = isset($stm->is_episode)?$stm->is_episode:0;
                        $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                        $library[$key]['content_types_id'] = $film->content_types_id;
                        $library[$key]['content_type_id'] = $film->content_type_id;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster_url'] = $poster;
                        $library[$key]['video_duration'] = $stm->video_duration;   
                        $library[$key]['genres'] = json_decode($film->genre);
                        $library[$key]['permalink'] = $film->permalink;
                        $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                        $library[$key]['full_movie'] = $stm->full_movie;
                        $library[$key]['story'] = $film->story;;
                        $library[$key]['release_date'] = $film->release_date;
                        $library[$key]['is_converted'] = $stm->is_converted;
                        $library[$key]['isFreeContent'] = 0;
                        $library[$key]['season_id'] = $stm->series_number;
                    }                    
                    }                    
                }elseif($details['transaction_type']==6){ 

                    $ppv_subscription_id=$details['ppv_subscription_id'];
                    $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=".$ppv_subscription_id)->queryRow();
                    $PpvSubscription = Yii::app()->db->createCommand()
                        ->SELECT('end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id')
                        ->from('ppv_subscriptions')
                        ->where('id='.$ppv_subscription_id)
                        ->queryRow();
                    $end_date = $PpvSubscription['end_date']?$PpvSubscription['end_date']=='1970-01-01 00:00:00'?'0000-00-00 00:00:00':$PpvSubscription['end_date']:'0000-00-00 00:00:00' ;
                    $start_date = $PpvSubscription['start_date'] ;
                    $season_id = $PpvSubscription['season_id'] ;
                    $episode_id = $PpvSubscription['episode_id'] ;
                    $movie_id = $PpvSubscription['movie_id'] ;
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($end_date);
                    $movie_name = isset($movie_id)?$this->getFilmName($movie_id):"";
                    $episode_name = isset($episode_id)?$this->getEpisodeName($episode_id):"";
                    $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$movie_id)));
                    $statusvoucher='N/A';
                    if($currdatetime<=$expirytime && $end_date!='0000-00-00 00:00:00' && strtotime($end_date)!=""){
                        $statusvoucher='Active';  
                    }
                    if($end_date!='0000-00-00 00:00:00' && strtotime($end_date)!=""){
                       $statusvoucher='Active'; 
                    }
                    if($end_date=='0000-00-00 00:00:00'){
                        $statusvoucher='Active';  
                    }  
                    if($statusvoucher=='Active'){
                        $datamovie_stream = Yii::app()->db->createCommand()
                            ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                            ->from('movie_streams ms ')
                            ->where('ms.id='.$episode_id)
                            ->queryRow();  
                        $embed_id=$datamovie_stream['embed_id'];
                        $vmovie_names=$movie_name;

                        if($movie_id!=0 && $season_id==0 && $episode_id==0 ){
                            $vmovie_names=$movie_name;
                            $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                    }
                        if($movie_id!=0 && $season_id!=0 && $episode_id!=0){
                            $vmovie_names=$movie_name." -> ".$translate['season']." ".$season_id." ->".$episode_name; 
                            $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'/stream/'.$embed_id;
                }
                    if($movie_id!=0 && $season_id!=0 && $episode_id==0){
                        $vmovie_names=$movie_name." -> ".$translate['season']." ".$season_id;
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'?season='.$season_id; 
                    }
                    if($film->content_types_id == 2){
                        $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                        }else{
                        $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                    }
                        
                        if($film->content_types_id==3){
                                if($episode_id!=0){
							$stm = movieStreams::model()->findByAttributes(array('id'=>$episode_id));
							   }else if($season_id!=0 && $episode_id==0){
							$stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id,'is_episode'=>0));
							   }else{
							$stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
							   }
                        }else{
                        $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
                        }

                        $library[$key]['name'] = $vmovie_names;
                        $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                        $library[$key]['movie_id'] = $movie_id;
                        $library[$key]['movie_stream_id'] = $stm->id;
                        $library[$key]['is_episode'] = isset($datamovie_stream['is_episode'])?$datamovie_stream['is_episode']:0;
                        $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                        $library[$key]['content_types_id'] = $film->content_types_id;
                        $library[$key]['content_type_id'] = $film->content_type_id;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster_url'] = $poster;
                        $library[$key]['video_duration'] = $stm->video_duration;   
                        $library[$key]['genres'] = json_decode($film->genre);
                        $library[$key]['permalink'] = $film->permalink;
                        $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                        $library[$key]['full_movie'] = $stm->full_movie;
                        $library[$key]['story'] = $film->story;;
                        $library[$key]['release_date'] = $film->release_date;
                        $library[$key]['is_converted'] = $stm->is_converted;
                        $library[$key]['isFreeContent'] = 0;
                        $library[$key]['season_id'] = $season_id;
                    }
                    }
                            }
            /*
            $free_contents = FreeContent::model()->findAllByAttributes(array('studio_id' => $studio_id));
            if($free_contents){
                foreach($free_contents as $key=>$frc){
                    $movie_name = isset($frc['movie_id'])?$this->getFilmName($frc['movie_id']):"";
                    $episode_name = isset($frc['episode_id'])?$this->getEpisodeName($frc['episode_id']):"";
                    $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$frc['movie_id'])));
                    $datamovie_stream = Yii::app()->db->createCommand()
                        ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode')
                        ->from('movie_streams ms ')
                        ->where('ms.id='.$frc['episode_id'])
                        ->queryRow();
                    $embed_id=$datamovie_stream['embed_id'];
                    $vmovie_names=$movie_name;
                    if($frc['movie_id']!=0 && $frc['season_id']==0 && $frc['episode_id']==0 ){
                        $vmovie_names=$movie_name;
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                    }
                    if($frc['movie_id']!=0 && $frc['season_id']!=0 && $frc['episode_id']!=0){
                        $vmovie_names=$movie_name." -> Season #".$frc['season_id']." ->".$episode_name; 
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'/stream/'.$embed_id;
                    }
                    if($frc['movie_id']!=0 && $frc['season_id']!=0 && $frc['episode_id']==0){
                        $vmovie_names=$movie_name." -> Season #".$frc['season_id'];
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'?season='.$frc['season_id']; 
                    } 
        
                    if($frc['content_types_id'] == 2){
                       $poster = $this->getPoster($frc['movie_id'], 'films', 'episode', $studio_id);
                        }else{
                       $poster = $this->getPoster($frc['movie_id'], 'films', 'standard', $studio_id);
                        }
                    
                    if($frc['content_types_id']==3){
                       if($frc['episode_id']!=0){
							$stm = movieStreams::model()->findByAttributes(array('id'=>$frc['episode_id']));
					}else if($frc['season_id']!=0 && $frc['episode_id']==0){
						$stm = movieStreams::model()->findByAttributes(array('movie_id'=>$frc['movie_id'],'is_episode'=>0));
					}else{
						$stm = movieStreams::model()->findByAttributes(array('movie_id'=>$frc['movie_id']));
					}
                    }else{
                        $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$frc['movie_id']));
                    }
                    $free[$key]['name'] = $vmovie_names;
                    $free[$key]['movie_stream_uniq_id'] = ($embed_id!='')?$embed_id:$stm->embed_id;
                    $free[$key]['movie_id'] = $frc['movie_id'];
                    $free[$key]['movie_stream_id'] = $stm->id;
                    $free[$key]['is_episode'] =  ($frc['episode_id']!=0)?'1':isset($stm->is_episode)?$stm->is_episode:0;
                    $free[$key]['muvi_uniq_id'] = $film->uniq_id;
                    $free[$key]['content_types_id'] = $film->content_types_id;
                    $free[$key]['content_type_id'] = $film->content_type_id;
                    $free[$key]['baseurl'] = Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                    $free[$key]['poster_url'] = $poster;
                    $free[$key]['video_duration'] = $stm->video_duration;   
                    $free[$key]['genres'] = json_decode($film->genre);
                    $free[$key]['permalink'] = $film->permalink;
                    $free[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                    $free[$key]['full_movie'] = $stm->full_movie;
                    $free[$key]['story'] = $film->story;;
                    $free[$key]['release_date'] = $film->release_date;
                    $free[$key]['is_converted'] = $stm->is_converted;
                    $free[$key]['isFreeContent'] = 1;
                    $free[$key]['season_id'] = $frc['season_id'];
                    }
                }
                */
            $newarr = array_merge($library,$free);
            if($newarr){
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['mylibrary'] = $newarr;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Content not found.";            
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id not found.";           
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    /**
     * @method GetMenus() Returns the list of menus in tree structure
     * @param String $authToken A authToken
     * @author Ratikanta<support@muvi.com>
     * @return Returns the list of menus in tree structure
     */
    function actionGetMenus() {
		$studio_id = $this->studio_id;
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $studio_id);
		$language_id = Yii::app()->custom->getLanguage_id($lang_code);
		$mainmenu = Yii::app()->custom->getMainMenuStructure('array', $language_id, $studio_id);
		$user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
		$usermenu = Yii::app()->custom->getUserMenuStructure('array', $language_id, $studio_id, $user_id, $translate);
		$sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$studio_id} AND parent_id=0 AND status=1 AND permalink != 'terms-privacy-policy') AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
		$pages = Yii::app()->db->createCommand($sql)->queryAll();
		if ($language_id != 20) {
			$pages = Yii::app()->custom->getStaticPages($studio_id, $language_id, $pages);
		}
		$this->code = 200;
		$this->items = array('mainmenu' => $mainmenu, 'usermenu' => $usermenu, 'footer_menu' => $pages);
	}

	/**
	* @method public getMonetizationMethods It returns all monetization methods which is associated to a content
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int $user_id*
	* @param String $movie_id* movie unique code
	* @param String $stream_id stream unique code
	* @param Int $season
	* @param String $purchase_type
	*/
    public function actionGetMonetizationMethods() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
        $_REQUEST['data'] = $_REQUEST;
			$user_id = trim($_REQUEST['data']['user_id']);
			$movie_code = trim($_REQUEST['data']['movie_id']) ? trim($_REQUEST['data']['movie_id']) : 0;
			$stream_code = trim($_REQUEST['data']['stream_id']);
			$season = trim($_REQUEST['data']['season']) ? trim($_REQUEST['data']['season']) : 0;
        $purchase_type = trim($_REQUEST['data']['purchase_type']);
        
			if (isset($user_id) && $user_id > 0 && isset($movie_code) && !empty($movie_code)) {
				$cond = '';
				$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
				if ($stream_code) {
					$cond = " AND m.embed_id=:embed_id";
					$condArr[":embed_id"] = $stream_code;
				}
				if ($season) {
					$cond .= " AND m.series_number=:series_number";
					$condArr[":series_number"] = $season;
				}
                
            $command = Yii::app()->db->createCommand()
						->select('f.id,m.id as stream_id, m.series_number, f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
						->from('films f , movie_streams m')
						->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
            $films = $command->queryRow();
            $movie_id = $films['id'];
				$stream_id = @$films['stream_id'];
				$season_id = @$films['series_number'];
				$content_types_id = @$films['content_types_id'];
				if (intval($content_types_id) == 3) {
					$content = "'" . $movie_id . ":" . $season_id . ":" . $stream_id . "'";
            } else {
					$content = $movie_id;
            }
            
            $film = Film::model()->findByPk($movie_id);
            $monitization_data = array();
				$monitization_data['studio_id'] = $this->studio_id;
            $monitization_data['user_id'] = $user_id;
            $monitization_data['movie_id'] = $movie_id;
            $monitization_data['season'] = @$season_id;
            $monitization_data['stream_id'] = @$stream_id;
            $monitization_data['content'] = @$content;
            $monitization_data['film'] = $film;
            $monitization_data['purchase_type'] = $purchase_type;

            $monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);
				$df_plans = array('ppv' => 0, 'pre_order' => 0, 'voucher' => 0, 'ppv_bundle' => 0);
            
				if (isset($monetization['monetization']) && !empty($monetization['monetization'])) {
					foreach ($monetization['monetization'] as $key => $value) {
                if (array_key_exists($key, $df_plans)) {
                    $df_plans[$key] = 1;
                }
            }
					$data['monetizations'] = $monetization['monetization'];
            $data['monetization_plans'] = $df_plans;
				} else {
					$data['monetizations'] = array();
					$data['monetization_plans'] = $df_plans;
        }
				$this->code = 200;
			} else {
				$this->code = 662;
			}
		} else {
			$this->code = 662;
		}
        
		$this->items = $data;
    }
    
    /**
	* @method public getPpvPlan It returns plan detail about a content
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int $user_id*
	* @param String $movie_id* movie unique code
	* @param String $stream_id stream unique code
	* @param Int $season
	*/
    public function actionGetPpvPlan(){
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			$season_id = trim(@$_REQUEST['season']);
			$stream_code = trim(@$_REQUEST['stream_id']);
			if ($user_id && $movie_code) {
				$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				
				if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
					$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
					if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 2)) {
						$cond = '';
						$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
						if ($stream_code) {
							$cond = " AND m.embed_id=:embed_id";
							$condArr[":embed_id"] = $stream_code;
						}
						if ($season_id) {
							$cond .= " AND m.series_number=:series_number";
							$condArr[":series_number"] = $season_id;
						}

						$command = Yii::app()->db->createCommand()
								->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
								->from('films f, movie_streams m')
								->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
						$films = $command->queryRow();
						if (!empty($films)) {
							$movie_id = $films['id'];
							$content_types_id = $films['content_types_id'];
							
							//If it has been on advance purchase
							$plan = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
							if (empty($plan)) {
								//Check ppv plan set or not by studio admin
								$plan = Yii::app()->common->getContentPaymentType($content_types_id, $films['ppv_plan_id'],$this->studio_id);
							}
							if (!empty($plan)) {
								$this->code = 200;
								if (intval($content_types_id) == 3) {
									$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
									$data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
									$data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
									$data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

									$EpDetails = $this->getEpisodeToPlay($movie_id,$this->studio_id);

									if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
										$series = explode(',', $EpDetails->series_number);
										sort($series);

										$data['max_season'] = $series[count($series) - 1];
										$data['min_season'] = $series[0];

										$data['series_number'] = $series_number = intval($season_id) ? $season_id : $series[0];

										$episql = "SELECT * FROM movie_streams WHERE studio_id=" . $this->studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series_number . " AND episode_parent_id=0 ORDER BY episode_number ASC";
										$episodes = Yii::app()->db->createCommand($episql)->queryAll();
										$ep_data = array();
										if (isset($episodes) && !empty($episodes)) {
											for($i=0;$i<count($episodes);$i++){
												$ep_data[$i]['id'] = $episodes[$i]['id'];
												$ep_data[$i]['movie_id'] = $episodes[$i]['movie_id'];
												$ep_data[$i]['episode_title'] = $episodes[$i]['episode_title'];
												$ep_data[$i]['embed_id'] = $episodes[$i]['embed_id'];
											}
										}
										$data['episode'] = $ep_data;
									}
								}
								$is_subscribed = Yii::app()->common->isSubscribed($user_id,$this->studio_id);
								if ($is_subscribed) {
									$member_subscribed = 1;
								}

								$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
								$price = Yii::app()->common->getPPVPrices($plan->id, $default_currency_id);
								$currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;
								
								if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
									$season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
									$season_price_data['ppv_pricing_id'] = $price['id'];
									$season_price_data['studio_id'] = $this->studio_id;
									$season_price_data['currency_id'] = $price['currency_id'];
									$season_price_data['season'] = $season_id;
									$season_price_data['is_subscribed'] = $is_subscribed;
									$default_season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
									if ($default_season_price != -1) {
										$price['default_season_price'] = $default_season_price;
									}
								}
								
								$currency = Currency::model()->findByPk($currency_id);
								$validity = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'access_period');
							
								$data['is_coupon_exists'] = self::isCouponExists();
								$data['is_member_subscribed'] = @$member_subscribed;
								$data['price'] = $price;
								$data['currency_id'] = $currency->id;
								$data['currency_symbol'] = $currency->symbol;
								$data['ppv_validity'] = $validity;
								$data['can_save_card'] = @$gateway['gateways'][0]['paymentgt']['can_save_card'];
								if (intval(@$gateway['gateways'][0]['paymentgt']['can_save_card'])) {
									$card_sql = "SELECT tot.id, tot.gateway_id, tot.card_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$this->studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
									$card = Yii::app()->db->createCommand($card_sql)->queryAll();
									if (!empty($card)) {
										$data['card'] =  $card;
									}
								}
							} else {//Content is not in Pay-per-view plan
								$this->code = 666;
							}
						} else {//No content exists
							$this->code = 682;
						}
					} else {//Pay-per view is not enabled
						$this->code = 665;
					}
				} else {//No gateway found
					$this->code = 612;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
		
		$this->items = $data;
    }
    
	/**
	* @method public purchasePPV It will process the payment for PPV content
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int user_id*
	* @param String movie_id* movie unique code
	* @param int season_id
	* @param String episode_id
	* @param String existing_card_id*
	* @param string card_name* Name as on Credit card
	* @param String card_number* Card Number 
	* @param String cvv* CVV as on card
	* @param String expiry_month* Expiry month as on Card
	* @param String expiry_year* Expiry Year as on Card
	* @param String profile_id* User Profile id created in the payment gateways
	* @param String card_last_fourdigit Last 4 digits of the card 
	* @param int currency_id
	* @param String coupon_code
	* @param Boolean is_save_this_card 1: true, 0: false
	* @param String token Payment Token
	* @param String card_type Type of the card
	* @param String email User email
	* @param String country User location
	*/
	public function actionPurchasePPV() {
        $data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data['user_id'] = $user_id = @$_REQUEST['user_id'];
			$data['studio_id'] = $this->studio_id;
			if (intval($user_id) && isset($_REQUEST['movie_id']) && !empty($_REQUEST['movie_id'])) {
				$data['movie_id'] = @$_REQUEST['movie_id'];
				$data['season_id'] = @$_REQUEST['season_id'];
				$data['episode_id'] = @$_REQUEST['episode_id'];
				$data['currency_id'] = @$_REQUEST['currency_id'];
				$data['coupon_code'] = @$_REQUEST['coupon_code'];

				$is_valid_card_information = 0;
				if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
					$is_valid_card_information = 1;
				} else {
					$data['card_holder_name'] = @$_REQUEST['card_name'];
					$data['card_number'] = @$_REQUEST['card_number'];
					$data['exp_month'] = @$_REQUEST['exp_month'];
					$data['exp_year'] = @$_REQUEST['exp_year'];
					$data['cvv'] = @$_REQUEST['cvv'];
					$data['profile_id'] = @$_REQUEST['profile_id'];
					
					$data['token'] = @$_REQUEST['token'];
					$data['card_type'] = @$_REQUEST['card_type'];
					$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
					$data['email'] = @$_REQUEST['email'];
					$data['country'] = @$_REQUEST['contry'];
					$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
					
					if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
						$is_valid_card_information = 1;
					}
				}
				
				if ($is_valid_card_information) {
					$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
					if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
						$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
						if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 2)) {
							$Films = Film::model()->findByAttributes(array('studio_id' => $this->studio_id, 'uniq_id' => $data['movie_id']));
							if (!empty($Films)) {
								$plan = Yii::app()->common->checkAdvancePurchase($Films->id, $this->studio_id, 0);
								if (empty($plan)) {
									$plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
								}

								if (isset($plan) && !empty($plan)) {
									$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $this->studio_id);
									
									$price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id'], $data['country']);
									$currency = Currency::model()->findByPk($price['currency_id']);
									$data['currency_code'] = $currency->code;
									$data['currency_symbol'] = $currency->symbol;

									//Calculate ppv expiry time
									$start_date = Date('Y-m-d H:i:s');
									$end_date = '';

									$limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'limit_video');
									$limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

									$watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'watch_period');
									$watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

									$access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'access_period');
									$access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

									$time = strtotime($start_date);
									if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
										$end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
									} elseif((isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period))) {
										if (isset($plan->content_types_id) && intval($plan->content_types_id) != 3) {
											$end_date = date("Y-m-d H:i:s", strtotime("+{$watch_period}", $time));
										}
									}
									$data['view_restriction'] = $limit_video;
									$data['watch_period'] = $watch_period;
									$data['access_period'] = $access_period;
									
									if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
										//Check which part wants to sell by studio admin
										$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
										$is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
										$is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
										$is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
										
										if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
											if (intval($is_episode)) {
												$streams = movieStreams::model()->findByAttributes(array('studio_id' => $this->studio_id, 'embed_id' => $data['episode_id']));
												$data['episode_id'] = $streams->id;

												if (intval($is_subscribed_user)) {
													$data['amount'] = $price['episode_subscribed'];
												} else {
													$data['amount'] = $price['episode_unsubscribed'];
												}
											}
										} else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
											if (intval($is_season)) {
												$data['episode_id'] = 0;

												if (intval($is_subscribed_user)) {
													$data['amount'] = $price['season_subscribed'];
												} else {
													$data['amount'] = $price['season_unsubscribed'];
												}

												if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
													$season_price_data = array();
													$season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
													$season_price_data['ppv_pricing_id'] = $price['id'];
													$season_price_data['studio_id'] = $this->studio_id;
													$season_price_data['currency_id'] = $price['currency_id'];
													$season_price_data['season'] = $data['season_id'];
													$season_price_data['is_subscribed'] = $is_subscribed_user;

													$season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
													if ($season_price != -1) {
														$data['amount'] = $season_price;
													}
												}
											}
										} else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
											if (intval($is_show)) {
												$data['season_id'] = 0;
												$data['episode_id'] = 0;
												if (intval($is_subscribed_user)) {
													$data['amount'] = $price['show_subscribed'];
												} else {
													$data['amount'] = $price['show_unsubscribed'];
												}
											}
										}
									} else {//Single part videos
										$data['season_id'] = 0;
										$data['episode_id'] = 0;

										if (intval($is_subscribed_user)) {
											$data['amount'] = $price['price_for_subscribed'];
										} else {
											$data['amount'] = $price['price_for_unsubscribed'];
										}
									}
									
									$couponCode = '';
									//Calculate coupon if exists
									if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
										$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $this->studio_id, $user_id, $data['currency_id']);
										$data['amount'] = $getCoup["amount"];
										$couponCode = $getCoup["couponCode"];
									}
									
									$gateway_code = '';
            
									if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
										$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $this->studio_id, 'user_id' => $user_id));
										$data['card_id'] = @$card->id;
										$data['token'] = @$card->token;
										$data['profile_id'] = @$card->profile_id;
										$data['card_holder_name'] = @$card->card_holder_name;
										$data['card_type'] = @$card->card_type;
										$data['exp_month'] = @$card->exp_month;
										$data['exp_year'] = @$card->exp_year;

										$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
										$gateway_code = $gateway_info->short_code;
									} else {
										$gateway_info = $gateway['gateways'];
										$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
									}
									
									$this->setPaymentGatwayVariable($gateway_info);
									if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
										$VideoName = Yii::app()->common->getVideoname($Films->id);
										$VideoName = trim($VideoName);
										$VideoDetails = $VideoName;
										if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
											if ($data['season_id'] == 0) {
												$VideoDetails .= ', All Seasons';
											} else {
												$VideoDetails .= ', Season ' . $data['season_id'];
												if ($data['episode_id'] == 0) {
													$VideoDetails .= ', All Episodes';
												} else {
													$episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
													$episodeName = trim($episodeName);
													$VideoDetails .= ', ' . $episodeName;
												}
											}
										}
										
										if (abs($data['amount']) < 0.01) {
											if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
												$data['gateway_code'] = $gateway_code;
												$crd = Yii::app()->billing->setCardInfo($data);
												$data['card_id'] = $crd;
											}

											$data['amount'] = 0;
											$set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$Films->id,$start_date,$end_date,$data['coupon_code'], 0, $gateway_code);
											$ppv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, 0, $VideoName, $VideoDetails);

											$this->code = 200;
										} else {
											$data['gateway_code'] = $gateway_code;
											$data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
											$data['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
											$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
											Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
											$payment_gateway = new $payment_gateway_controller();
											$trans_data = $payment_gateway::processTransactions($data);

											$is_paid = $trans_data['is_success'];
											if (intval($is_paid)) {
												$data['amount'] = $trans_data['amount'];
												if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
													$crd = Yii::app()->billing->setCardInfo($data);
													$data['card_id'] = $crd;
												}

												$set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$Films->id,$start_date,$end_date,$data['coupon_code'], 0, $gateway_code);
												$ppv_subscription_id = $set_ppv_subscription;

												$transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$Films->id,$trans_data, $ppv_subscription_id, $gateway_code, 0);
												$ppv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, 0, $VideoName, $VideoDetails);

												$this->code = 200;
											} else {
												$this->code = 661;
											}
										}
									} else {
										$this->code = 737;
									}
								} else {//Content is not in Pay-per-view plan
									$this->code = 666;
								}
							} else {//No content exists
								$this->code = 682;
							}
						} else {//Pay-per view is not enabled
							$this->code = 665;
						}
					} else {//No gateway found
						$this->code = 612;
					}
				}else {
					$this->code = 660;
				}
			} else {
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
    }
	
	/**
	* @method public getPreOrderPlan It returns plan detail about a content
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int $user_id*
	* @param String $movie_id* movie unique code
	*/
    public function actionGetPreOrderPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			if ($user_id && $movie_code) {
				$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				
				if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
					$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
					if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 16)) {
						$command = Yii::app()->db->createCommand()
							->select('f.id,f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
							->from('films f ')
							->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id));
						$films = $command->queryRow();
						if (!empty($films)) {
							$movie_id = $films['id'];
							$plan = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
							if (isset($plan) && !empty($plan)) {
								$this->code = 200;
								
								$is_subscribed = Yii::app()->common->isSubscribed($user_id,$this->studio_id);
								if ($is_subscribed) {
									$member_subscribed = 1;
								}
								
								$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
								$price = Yii::app()->common->getPPVPrices($plan->id, $default_currency_id);
								$currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;
								$currency = Currency::model()->findByPk($currency_id);
								$validity = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'access_period');
								
								$data['isadv'] = 1;
								$data['is_coupon_exists'] = self::isCouponExists();
								$data['is_member_subscribed'] = @$member_subscribed;
								$data['price'] = $price;
								$data['currency_id'] = $currency->id;
								$data['currency_symbol'] = $currency->symbol;
								$data['preorder_validity'] = $validity;
								$data['can_save_card'] = @$gateway['gateways'][0]['paymentgt']['can_save_card'];
								if (intval(@$gateway['gateways'][0]['paymentgt']['can_save_card'])) {
									$card_sql = "SELECT tot.id, tot.gateway_id, tot.card_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$this->studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
									$card = Yii::app()->db->createCommand($card_sql)->queryAll();
									if (!empty($card)) {
										$data['card'] =  $card;
									}
								}
							} else {//Content is not in Pre order plan
								$this->code = 668;
							}
						} else {//No content exists
							$this->code = 682;
						}
					} else {//Pre order is not enabled
						$this->code = 667;
					}
				} else {//No gateway found
					$this->code = 612;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
		
		$this->items = $data;
    }
    
	/**
	* @method public purchasePreOrder It will process the payment for pre order content
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int user_id*
	* @param String movie_id* movie unique code
	* @param String existing_card_id*
	* @param string card_name* Name as on Credit card
	* @param String card_number* Card Number 
	* @param String cvv* CVV as on card
	* @param String expiry_month* Expiry month as on Card
	* @param String expiry_year* Expiry Year as on Card
	* @param String profile_id* User Profile id created in the payment gateways
	* @param String card_last_fourdigit Last 4 digits of the card 
	* @param int currency_id
	* @param String coupon_code
	* @param Boolean is_save_this_card 1: true, 0: false
	* @param String token Payment Token
	* @param String card_type Type of the card
	* @param String email User email
	* @param String country User location
	*/
	public function actionPurchasePreOrder() {
        $data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data['user_id'] = $user_id = @$_REQUEST['user_id'];
			$data['studio_id'] = $this->studio_id;
			if (intval($user_id) && isset($_REQUEST['movie_id']) && !empty($_REQUEST['movie_id'])) {
				$data['movie_id'] = @$_REQUEST['movie_id'];
				$data['currency_id'] = @$_REQUEST['currency_id'];
				$data['coupon_code'] = @$_REQUEST['coupon_code'];

				$is_valid_card_information = 0;
				if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
					$is_valid_card_information = 1;
				} else {
					$data['card_holder_name'] = @$_REQUEST['card_name'];
					$data['card_number'] = @$_REQUEST['card_number'];
					$data['exp_month'] = @$_REQUEST['exp_month'];
					$data['exp_year'] = @$_REQUEST['exp_year'];
					$data['cvv'] = @$_REQUEST['cvv'];
					$data['profile_id'] = @$_REQUEST['profile_id'];
					
					$data['token'] = @$_REQUEST['token'];
					$data['card_type'] = @$_REQUEST['card_type'];
					$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
					$data['email'] = @$_REQUEST['email'];
					$data['country'] = @$_REQUEST['contry'];
					$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
					
					if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
						$is_valid_card_information = 1;
					}
				}
				
				if ($is_valid_card_information) {
					$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
					if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
						$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
						if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 16)) {
							$Films = Film::model()->findByAttributes(array('studio_id' => $this->studio_id, 'uniq_id' => $data['movie_id']));
							if (!empty($Films)) {
								$plan = Yii::app()->common->checkAdvancePurchase($Films->id, $this->studio_id, 0);
								if (isset($plan) && !empty($plan)) {
									$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $this->studio_id);
									
									$price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id'], $data['country']);
									$currency = Currency::model()->findByPk($price['currency_id']);
									$data['currency_code'] = $currency->code;
									$data['currency_symbol'] = $currency->symbol;

									//Calculate ppv expiry time
									$start_date = Date('Y-m-d H:i:s');
									$end_date = '';
									$data['season_id'] = 0;
									$data['episode_id'] = 0;

									if (intval($is_subscribed_user)) {
										$data['amount'] = $price['price_for_subscribed'];
									} else {
										$data['amount'] = $price['price_for_unsubscribed'];
									}
									
									$couponCode = '';
									//Calculate coupon if exists
									if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
										$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $this->studio_id, $user_id, $data['currency_id']);
										$data['amount'] = $getCoup["amount"];
										$couponCode = $getCoup["couponCode"];
									}
									
									$gateway_code = '';
            
									if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
										$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $this->studio_id, 'user_id' => $user_id));
										$data['card_id'] = @$card->id;
										$data['token'] = @$card->token;
										$data['profile_id'] = @$card->profile_id;
										$data['card_holder_name'] = @$card->card_holder_name;
										$data['card_type'] = @$card->card_type;
										$data['exp_month'] = @$card->exp_month;
										$data['exp_year'] = @$card->exp_year;

										$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
										$gateway_code = $gateway_info->short_code;
									} else {
										$gateway_info = $gateway['gateways'];
										$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
									}
									
									$this->setPaymentGatwayVariable($gateway_info);
									if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
										$VideoName = Yii::app()->common->getVideoname($Films->id);
										$VideoName = trim($VideoName);
										$VideoDetails = $VideoName;
										
										if (abs($data['amount']) < 0.01) {
											if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
												$data['gateway_code'] = $gateway_code;
												$crd = Yii::app()->billing->setCardInfo($data);
												$data['card_id'] = $crd;
											}

											$data['amount'] = 0;
											$set_apv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$Films->id,$start_date,$end_date,$data['coupon_code'], 1, $gateway_code);
											$apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, 1, $VideoName, $VideoDetails);

											$this->code = 200;
										} else {
											$data['gateway_code'] = $gateway_code;
											$data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
											$data['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
											$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
											Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
											$payment_gateway = new $payment_gateway_controller();
											$trans_data = $payment_gateway::processTransactions($data);

											$is_paid = $trans_data['is_success'];
											if (intval($is_paid)) {
												$data['amount'] = $trans_data['amount'];
												if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
													$crd = Yii::app()->billing->setCardInfo($data);
													$data['card_id'] = $crd;
												}

												$set_apv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$Films->id,$start_date,$end_date,$data['coupon_code'], 1, $gateway_code);
												$ppv_subscription_id = $set_ppv_subscription;

												$transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$Films->id,$trans_data, $ppv_subscription_id, $gateway_code, 1);
												$apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, 1, $VideoName, $VideoDetails);

												$this->code = 200;
											} else {
												$this->code = 661;
											}
										}
									} else {
										$this->code = 737;
									}
								} else {//Content is not in Pre order plan
									$this->code = 668;
								}
							} else {//No content exists
								$this->code = 682;
							}
						} else {//Pay-per view is not enabled
							$this->code = 667;
						}
					} else {//No gateway found
						$this->code = 612;
					}
				}else {
					$this->code = 660;
				}
			} else {
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
    }
	
	/**
	 * @method public getPpvBundlePlan It returns plan detail about a content
	 * @return json Return the json array of results
	 * @param String $authToken* Auth token of the studio
	 * @param Int $user_id*
	 * @param String $movie_id* movie unique code
	 */
	public function actionGetPpvBundlePlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			if ($user_id && $movie_code) {
				$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

				if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
					$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
					if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 64)) {
						$command = Yii::app()->db->createCommand()
								->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
								->from('films f ')
								->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id));
						$films = $command->queryRow();
						if (!empty($films)) {
							$movie_id = $films['id'];
							$sql = "SELECT al.*, GROUP_CONCAT(f.name) AS content_name FROM (SELECT t.*, pc.content_id AS movie_id FROM (SELECT pp.id, pp.title, pp.is_timeframe, pac.content_id FROM ppv_plans pp, ppv_advance_content pac WHERE pp.studio_id={$this->studio_id} AND pp.status=1 AND pp.is_timeframe=1 AND pp.is_advance_purchase=2 AND pp.id=pac.ppv_plan_id AND pac.content_id={$movie_id}) AS t, ppv_advance_content pc WHERE t.id=pc.ppv_plan_id) al, films f WHERE al.movie_id=f.id GROUP BY al.id";
							$plan = Yii::app()->db->createCommand($sql)->queryAll();
							if (isset($plan) && !empty($plan)) {
								$this->code = 200;
								$is_subscribed = Yii::app()->common->isSubscribed($user_id, $this->studio_id);
								if ($is_subscribed) {
									$member_subscribed = 1;
								}
								$data['is_ppv_bundle'] = 1;
								$data['is_member_subscribed'] = @$member_subscribed;
								$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
								foreach ($plan as $key => $value) {
									$data['plan'][$key]['plan_id'] = $value['id'];
									$data['plan'][$key]['title'] = $value['title'];
									$data['plan'][$key]['timeframe_prices'] = Yii::app()->common->getAllTimeFramePrices($value['id'], $default_currency_id, $this->studio_id);
								}
								$data['is_coupon_exists'] = self::isCouponExists();
								$data['can_save_card'] = @$gateway['gateways'][0]['paymentgt']['can_save_card'];
								if (intval(@$gateway['gateways'][0]['paymentgt']['can_save_card'])) {
									$card_sql = "SELECT tot.id, tot.gateway_id, tot.card_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$this->studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
									$card = Yii::app()->db->createCommand($card_sql)->queryAll();
									if (!empty($card)) {
										$data['card'] = $card;
									}
								}
							} else {//Content is not in Pay-per-view bundle plan
								$this->code = 725;
							}
						} else {//No content exists
							$this->code = 682;
						}
					} else {//Pay-per-view bundle is not enabled
						$this->code = 669;
					}
				} else {//No gateway found
					$this->code = 612;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	* @method public purchasePPVBundle It will process the payment for PPV bundle content
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int user_id*
	* @param String movie_id* movie unique id
	* @param Int plan_id* ppv bundle plan id
	* @param Int timeframe_id* ppv bundle timeframe id
	* @param String existing_card_id*
	* @param string card_name* Name as on Credit card
	* @param String card_number* Card Number 
	* @param String cvv* CVV as on card
	* @param String expiry_month* Expiry month as on Card
	* @param String expiry_year* Expiry Year as on Card
	* @param String profile_id* User Profile id created in the payment gateways
	* @param String card_last_fourdigit Last 4 digits of the card 
	* @param int currency_id
	* @param String coupon_code
	* @param Boolean is_save_this_card 1: true, 0: false
	* @param String token Payment Token
	* @param String card_type Type of the card
	* @param String email User email
	* @param String country User location
	*/
	public function actionPurchasePPVBundle() {
        $data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data['user_id'] = $user_id = @$_REQUEST['user_id'];
			$data['studio_id'] = $this->studio_id;
			if (intval($user_id) && isset($_REQUEST['plan_id']) && !empty($_REQUEST['plan_id'])) {
				$data['plan_id'] = @$_REQUEST['plan_id'];
				$data['timeframe_id'] = @$_REQUEST['timeframe_id'];
				$data['currency_id'] = @$_REQUEST['currency_id'];
				$data['coupon_code'] = @$_REQUEST['coupon_code'];

				$is_valid_card_information = 0;
				if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
					$is_valid_card_information = 1;
				} else {
					$data['card_holder_name'] = @$_REQUEST['card_name'];
					$data['card_number'] = @$_REQUEST['card_number'];
					$data['exp_month'] = @$_REQUEST['exp_month'];
					$data['exp_year'] = @$_REQUEST['exp_year'];
					$data['cvv'] = @$_REQUEST['cvv'];
					$data['profile_id'] = @$_REQUEST['profile_id'];
					
					$data['token'] = @$_REQUEST['token'];
					$data['card_type'] = @$_REQUEST['card_type'];
					$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
					$data['email'] = @$_REQUEST['email'];
					$data['country'] = @$_REQUEST['contry'];
					
					if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
						$is_valid_card_information = 1;
					}
				}
				
				if ($is_valid_card_information) {
					$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
					if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
						$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
						if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 64)) {
							$Films = Film::model()->findByAttributes(array('studio_id' => $this->studio_id, 'uniq_id' => $data['movie_id']));
							if (!empty($Films)) {
								$plan = PpvPlans::model()->findByAttributes(array('id' => $data['plan_id'], 'studio_id' => $this->studio_id, 'is_advance_purchase' => 2));
								if (isset($plan) && !empty($plan)) {
									$data['is_bundle'] = 1;
									$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $this->studio_id);

									(array) $price = Yii::app()->common->getTimeFramePrice($data['plan_id'], $data['timeframe_id'], $this->studio_id);
									$price = $price->attributes;
									$timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;

									$currency = Currency::model()->findByPk($price['currency_id']);
									$data['currency_id'] = $currency->id;
									$data['currency_code'] = $currency->code;
									$data['currency_symbol'] = $currency->symbol;

									//Calculate ppv expiry time
									$start_date = Date('Y-m-d H:i:s');
									$time = strtotime($start_date);
									$expiry = $timeframe_days . ' ' . "days";
									$end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));

									$data['movie_id'] = 0;
									$data['season_id'] = 0;
									$data['episode_id'] = 0;

									if (intval($is_subscribed_user)) {
										if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
											$data['amount'] = $price['show_subscribed'];
										} else {
											$data['amount'] = $price['price_for_subscribed'];
										}
									} else {
										if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
											$data['amount'] = $price['show_unsubscribed'];
										} else {
											$data['amount'] = $price['price_for_unsubscribed'];
										}
									}

									$couponCode = '';
									//Calculate coupon if exists
									if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
										$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $this->studio_id, $user_id, $data['currency_id']);
										$data['amount'] = $getCoup["amount"];
										$couponCode = $getCoup["couponCode"];
									}

									$gateway_code = '';

									if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
										$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $this->studio_id, 'user_id' => $user_id));
										$data['card_id'] = @$card->id;
										$data['token'] = @$card->token;
										$data['profile_id'] = @$card->profile_id;
										$data['card_holder_name'] = @$card->card_holder_name;
										$data['card_type'] = @$card->card_type;
										$data['exp_month'] = @$card->exp_month;
										$data['exp_year'] = @$card->exp_year;

										$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
										$gateway_code = $gateway_info->short_code;
									} else {
										$gateway_info = $gateway['gateways'];
										$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
									}

									$this->setPaymentGatwayVariable($gateway_info);
									if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
										$VideoName = $plan->title;
										$VideoDetails = $VideoName = trim($VideoName);

										if (abs($data['amount']) < 0.01) {
											if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
												$data['gateway_code'] = $gateway_code;
												$crd = Yii::app()->billing->setCardInfo($data);
												$data['card_id'] = $crd;
											}

											$data['amount'] = 0;
											$ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan,$data,$Films->id,$start_date,$end_date,$data['coupon_code'], 2, $gateway_code);
											$apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, 2, $VideoName, $VideoDetails);

											$this->code = 200;
										} else {
											$data['gateway_code'] = $gateway_code;
											$data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
											$data['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
											$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
											Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
											$payment_gateway = new $payment_gateway_controller();
											$trans_data = $payment_gateway::processTransactions($data);

											$is_paid = $trans_data['is_success'];
											if (intval($is_paid)) {
												$data['amount'] = $trans_data['amount'];
												if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
													$crd = Yii::app()->billing->setCardInfo($data);
													$data['card_id'] = $crd;
												}

												$ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan,$data,$Films->id,$start_date,$end_date,$data['coupon_code'], 0, $gateway_code);

												$transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$Films->id,$trans_data, $ppv_subscription_id, $gateway_code, 0);
												$apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, 0, $VideoName, $VideoDetails);

												$this->code = 200;
											} else {
												$this->code = 661;
											}
										}
									} else {
										$this->code = 737;
									}
								} else {//Content is not in Pay-per view bundle plan
									$this->code = 725;
								}
							} else {//No content exists
								$this->code = 682;
							}
						} else {//Pay-per view bundle is not enabled
							$this->code = 669;
						}
					} else {//No gateway found
						$this->code = 612;
					}
				}else {
					$this->code = 660;
				}
			} else {
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
    }
	
	function isCouponExists() {
		$data = Yii::app()->general->monetizationMenuSetting($this->studio_id);
		$isCouponExists = 0;
		if (isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 32)) {
			$sql = "SELECT c.id FROM coupon c WHERE c.studio_id = {$this->studio_id} AND c.status=1";
			$coupon = Yii::app()->db->createCommand($sql)->queryAll();
			if (isset($coupon) && !empty($coupon)) {
				$isCouponExists = 1;
			}
		}
		return $isCouponExists;
	}

	/**
	 * @method public getVoucherPlan It returns plan detail about a content
	 * @return json Return the json array of results
	 * @param String authToken* Auth token of the studio
	 * @param Int user_id*
	 * @param String movie_id* movie unique code
	 * @param String stream_id stream unique code
	 * @param Int season
	 */
	public function actionGetVoucherPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			if ($user_id && $movie_code) {
				$season_id = trim(@$_REQUEST['season']);
				$stream_code = trim(@$_REQUEST['stream_id']);
				$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
				if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
					$cond = '';
					$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
					if ($stream_code) {
						$cond = " AND m.embed_id=:embed_id";
						$condArr[":embed_id"] = $stream_code;
					}
					if ($season_id) {
						$cond .= " AND m.series_number=:series_number";
						$condArr[":series_number"] = $season_id;
					}
					$command = Yii::app()->db->createCommand()
							->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
							->from('films f, movie_streams m')
							->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
					$films = $command->queryRow();
					if (!empty($films)) {
						$search_str = $movie_id = $films['id'];
						$content_types_id = $films['content_types_id'];

						if (intval($content_types_id) == 3) {
							$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
							$data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
							$data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
							$data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

							$EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);

							if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
								$series = explode(',', $EpDetails->series_number);
								sort($series);
								$data['max_season'] = $series[count($series) - 1];
								$data['min_season'] = $series[0];
								$data['series_number'] = $series_number = intval($season_id) ? $season_id : $series[0];
								$episql = "SELECT * FROM movie_streams WHERE studio_id=" . $this->studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series_number . " AND episode_parent_id=0 ORDER BY episode_number ASC";
								$episodes = Yii::app()->db->createCommand($episql)->queryAll();
								$ep_data = array();
								if (isset($episodes) && !empty($episodes)) {
									for ($i = 0; $i < count($episodes); $i++) {
										$ep_data[$i]['id'] = $episodes[$i]['id'];
										$ep_data[$i]['movie_id'] = $episodes[$i]['movie_id'];
										$ep_data[$i]['episode_title'] = $episodes[$i]['episode_title'];
										$ep_data[$i]['embed_id'] = $episodes[$i]['embed_id'];
									}
								}
								$data['episode'] = $ep_data;
							}

							$search_str .= (intval($series_number)) ? ":" . $series_number : ":0";
							$search_str .= (intval($films['stream_id'])) ? ":" . $films['stream_id'] : ":0";
						}

						$content = "'" . $search_str . "'";
						$voucher_exist = Yii::app()->common->isVoucherExists($this->studio_id, $content);
						if ($voucher_exist) {
							$this->code = 200;
						} else {
							$data = array();
							$this->code = 727; //Voucher doesn't exist for this content
						}
					} else {//No content exists
						$this->code = 682;
					}
				} else {//Voucher is not enabled
					$this->code = 726;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public validateVoucher It validates the voucher code
	 * @return json Return the json array of results
	 * @param String authToken* Auth token of the studio
	 * @param Int user_id*
	 * @param String movie_id* movie unique code
	 * @param String voucher_code*
	 * @param String stream_id stream unique code
	 * @param Int season
	 * @param String purchase_type
	 * @param String lang_code
	 */
	public function actionValidateVoucher() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			$voucher_code = trim(@$_REQUEST['voucher_code']);
			if ($user_id && $movie_code && $voucher_code) {
				$season_id = trim(@$_REQUEST['season']);
				$stream_code = trim(@$_REQUEST['stream_id']);
				$purchase_type = trim(@$_REQUEST['purchase_type']);
				$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
				if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
					$cond = '';
					$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
					if ($stream_code) {
						$cond = " AND m.embed_id=:embed_id";
						$condArr[":embed_id"] = $stream_code;
					}
					if ($season_id) {
						$cond .= " AND m.series_number=:series_number";
						$condArr[":series_number"] = $season_id;
					}
					$command = Yii::app()->db->createCommand()
							->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
							->from('films f, movie_streams m')
							->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
					$films = $command->queryRow();
					if (!empty($films)) {
						$content_id = $films['id'];
						$content_types_id = $films['content_types_id'];
						if ($content_types_id == 3) {
							if ($purchase_type == 'season') {
								$content_id .= ":" . $season_id;
							} else if ($purchase_type == 'episode') {
								$content_id .= (intval($season_id)) ? ":" . $season_id : ":0";
								$content_id .= ":" . $films['stream_id'];
							}
						}

						$voucherDetails = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $this->studio_id);
						switch ($voucherDetails) {
							case 1:
								$this->code = 200;
							case 2:
								$this->code = 729;
								$this->msg_code = 'invalid_voucher';
							case 3:
								$this->code = 730;
								$this->msg_code = 'voucher_already_used';
							case 4:
								$this->code = 200;
								$this->msg_code = 'free_content';
							case 5:
								$this->code = 200;
								$this->msg_code = 'already_purchase_this_content';
							case 6:
								$this->code = 656;
								$this->msg_code = 'access_period_expired';
							case 7:
								$this->code = 657;
								$this->msg_code = 'watch_period_expired';
							case 8:
								$this->code = 658;
								$this->msg_code = 'crossed_max_limit_of_watching';
						}
					} else {//No content exists
						$this->code = 682;
					}
				} else {//Voucher is not enabled
					$this->code = 726;
				}
			} else {//Either user id or movie id or voucher code is missing
				$this->code = 728;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	public function actionVoucherSubscription() {
        
        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        $voucher_code = @trim($_REQUEST['data']['voucher_code']);
        $purchase_type = @trim($_REQUEST['data']['purchase_type']);
        $is_preorder = @trim($_REQUEST['data']['is_preorder']);
        $lang_code   = @$_REQUEST['lang_code'];
        $data = array();
        
        
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $data = array();
            $studio_id = $this->studio_id;
            $translate   = $this->getTransData($lang_code,$studio_id);
            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;
            
            
            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season,'',$studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
            
            $content_id = "";
            if($content_types_id == 3 && $stream_id!="" && $purchase_type == 'episode'){
                if($movie_id != ""){
                    $content_id .= $movie_id;
                }if($season_id != ""){
                    $content_id .= ":".$season;
                }if($stream_id != ""){
                    $content_id .= ":".$stream_id;
                }
            }else if($content_types_id == 3 && $purchase_type == 'season'){
                if($movie_id != ""){
                    $content_id .= $movie_id;
                }if($season_id != ""){
                    $content_id .= ":".$season_id;
                }
            }else{
                $content_id .= $movie_id;
            }
            $isadv = (isset($is_preorder)) ? intval($is_preorder) : 0;
            
            $getVoucher = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $studio_id);
            if ($getVoucher == 2) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['invalid_voucher'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 3) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['voucher_already_used'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            }else if ($getVoucher == 4) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['free_content'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 5) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['already_purchased_content'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else {
                $sql = "SELECT id FROM voucher WHERE studio_id={$studio_id} AND voucher_code='" . $voucher_code . "'";
                $voucher_id = Yii::app()->db->createCommand($sql)->queryRow();
                
                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';
                if (!empty($voucher_id)) {
                    if (intval($isadv) == 0) {
                        $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'limit_video');
                        $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;
                        
                        $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'watch_period');
                        $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                        $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'access_period');
                        $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';
                        
                        $time = strtotime($start_date);
                        if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                        }
                        
                        $data['view_restriction'] = $limit_video;
                        $data['watch_period'] = $watch_period;
                        $data['access_period'] = $access_period;
                    }
                    $data['movie_id'] = $movie_code;
                    $data['content_types_id'] = $content_types_id;
                    $data['voucher_id'] = $voucher_id['id'];
                    $data['content_id'] = $content_id;
                    $data['user_id'] = $user_id;
                    $data['studio_id'] = $studio_id;
                    $data['isadv'] = $isadv;
                    $data['start_date'] = @$start_date;
                    $data['end_date'] = @$end_date;
                    
                    $VideoName = CouponOnly::model()->getContentInfo($content_id,$isadv);
                    $res = Yii::app()->billing->setVoucherSubscription($data);
                    
                    $ppv_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'voucher', '', $VideoName, '', '', $voucher_code);
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('voucher', $studio_id);
                    if ($isEmailToStudio) {
                        $this->sendStudioAdminEmails($studio_id, 'admin_voucher_content', $user_id, $VideoName,'','','',$voucher_code);
                    }
                    
                    if ($isadv) {
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                    }else{
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                    }
                } else {
                    $data['isError'] = $getVoucher;
                    $data['msg'] = $translate['invalid_voucher'];
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                }
            }
        } else {
            $data['isError'] = $getVoucher;
            $data['msg'] = $translate['invalid_voucher'];
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    public function actionGetStaticPagedetails(){
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        if(isset($_REQUEST['permalink']) &&  $_REQUEST['permalink'] != ""){
            $permalink  = $_REQUEST['permalink'];
            $sql   = "SELECT * FROM pages where studio_id=".$studio_id." AND permalink ='".$permalink."' AND status=1 AND parent_id=0";
            $page  = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($page)){
                if ($language_id != 20) {
                    $page = Yii::app()->custom->getStaticPageDetails($studio_id, $language_id, $page);
                }
                $page['content'] = html_entity_decode($page['content']);
                $data['status'] = 'OK';
                $data['code'] = 200;
                $data['msg'] = 'Success';
                $data['page_details'] = $page;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Content not found.";          
            }
        }else{
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Required parameters not found";            
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }   
    public function actionContactUs(){
        $studio_id   = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $studio_id);        
        if(isset($_REQUEST['email']) && trim($_REQUEST['email'])!=""){
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $studio      = Studio::model()->findByPk($studio_id, array('select'=>'name,contact_us_email,domain'));
            $subject     = $studio->name . ' :: New Contact Us';
            $site_url    = "https://".$studio->domain;
            $siteLogo    = Yii::app()->common->getLogoFavPath($studio_id);
            $logo        = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
            $site_link   = '<a href="' . $site_url . '">' . $studio->name . '</a>';
            $msg         = htmlspecialchars(@$_REQUEST['message']);
            $useremail   = @$_REQUEST['email'];
            $username = '';
            if (isset($_REQUEST['name']) && strlen(trim($_REQUEST['name'])) > 0){
                $username = htmlspecialchars($_REQUEST['name']);
            }
            $template_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                "message" => $msg
            );
            if ($studio->contact_us_email) {
                $admin_to [] = $studio_email = $studio->contact_us_email;
                if (strstr($studio_email, ',')) {
                    $admin_to     = explode(',', $studio_email);
                    $studio_email = $admin_to[0];
                }
            } else {
                $studio_user   = User::model()->findByAttributes(array('studio_id' => $studio_id));
                $studio_email  = $studio_user->email;                
                $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
                if ($linked_emails->notification_from_email) {
                    $studio_email = $linked_emails->notification_from_email;
                }
                $admin_to[] = $studio_email;
            }
            $studio_name   = $StudioName = $studio->name;
            $EndUserName   = $username;
            $admin_subject = $subject;
            $admin_from    = $useremail;
            $email_type    = 'contact_us';
            $admin_cc      = Yii::app()->common->emailNotificationLinks($studio_id, $email_type); 
            if(empty($admin_cc) || $admin_cc !=""){
                $admin_cc  = array();
            }
            $user_to       = array($useremail);
            $user_from     = $studio_email;
            $content  = "SELECT * FROM `notification_templates` WHERE type= '".$email_type."' AND studio_id=".$studio_id." AND (language_id = ".$language_id." OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '".$email_type."' AND studio_id=" . $studio_id." AND language_id = ".$language_id."))";
            $data     = Yii::app()->db->createCommand($content)->queryAll();
            if(empty($data)) {                
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }
            $user_subject = $data[0]['subject'];
            eval("\$user_subject = \"$user_subject\";");
            $user_content = $data[0]['content'];
            eval("\$user_content = \"$user_content\";");
            $template_user_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                'content'=>$user_content
            );
            Yii::app()->theme = 'bootstrap';
            $admin_html = Yii::app()->controller->renderPartial('//email/studio_contactus', array('params' => $template_content), true);
            $user_html = Yii::app()->controller->renderPartial('//email/studio_contactus_user', array('params' => $template_user_content), true);
            $returnVal_admin = $this->sendmailViaAmazonsdk($admin_html, $admin_subject, $admin_to, $admin_from,$admin_cc,'','', $username);
            $returnVal_user = $this->sendmailViaAmazonsdk($user_html, $user_subject, $user_to, $user_from,'','','', $studio_name);
            $contact['status']      = 'OK';
            $contact['code']        = 200;
            $contact['msg']         = 'Success';
            $contact['success_msg'] = $translate['thanks_for_contact'];
        }else{
            $contact['code'] = 448;
            $contact['status'] = "Failure";
            $contact['msg'] = "Failure";
            $contact['error_msg'] = $translate['oops_invalid_email'];
        }
        $this->setHeader($contact['code']);
        echo json_encode($contact);
        exit;
    }    
    
    /*APIs for managing favourite contents*/
    function CheckFavouriteEnable($studio_id){
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        if(!empty($fav_status)){
            $result = $fav_status['config_value'];
        }
        else{
            $result = 0;
        }
        return $result;
    }
    
    public function actionGetFilteredContent(){
        if(isset($_REQUEST)){
            $list = array();
            $cat  = array();
            $studio_id   = $this->studio_id;
            $lang_code   = @$_REQUEST['lang_code'];
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !="") ? $_REQUEST['user_id'] : 0;
            $is_episode = (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] !="") ? $_REQUEST['is_episode'] : "";
            $translate   = $this->getTransData($lang_code, $studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $content_category_value = 0;
            $condition = "";
            $sort_by   = 'F.name';
            $order_by  = 'ASC';
            $cond      = '';
            $group_by  = (isset($_REQUEST['group_by']) && $_REQUEST['group_by'] != '') ? $_REQUEST['group_by']  : '';
            if(isset($_REQUEST['category']) && $_REQUEST['category'] !=""){
                $categories = $_REQUEST['category'];
                if(!is_array($categories)){
                    $categories = explode("," , $categories);
                }
                $permalink = "";
                if(!empty($categories)){
                    $cond = " AND permalink IN ";
                    $total = count($categories);
                    for($i = 0; $i <$total; $i++){
                        $comma = " ";
                        if($total != ($i+1)){
                            $comma = ",";
                        }
                        $permalink .= "'".$categories[$i]."'";
                        $permalink .= $comma;
                    }
                    $cond .= " (".$permalink.") ";
                }
                $sql = "SELECT SUM(binary_value) as content_category_value FROM content_category WHERE parent_id =0 AND studio_id=".$studio_id." ".$cond;
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                if(!empty($res)){
                    $content_category_value = $res['content_category_value'];
                }
            }
            if($group_by == "category"){
                $sql = "SELECT id,category_name,permalink,binary_value FROM content_category WHERE parent_id =0 AND studio_id=".$studio_id."  ".$cond." ORDER BY category_name ASC";
                $cat = Yii::app()->db->createCommand($sql)->queryAll();
            }
            if($content_category_value > 0){
                $condition = " AND (F.content_category_value & ".$content_category_value.") ";
            }
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            if(isset($_REQUEST['sort'])){
                $sort_by = $_REQUEST['sort'];
            }
            if(isset($_REQUEST['order'])){
                $order_by = $_REQUEST['order'];
            }
            $filter_cond = "";
            $request_array = array('authToken','lang_code','category','sort','order','limit','offset','Host','User-Agent','Accept','Accept-Encoding','Cookie','Content-Type','Content-Length');
            $film_array  = array('name','language','genre','rating','country','censor_rating','release_date');
            $movie_stream_array  = array('episode_title','episode_story','episode_date');
            foreach($_REQUEST as $key=>$value){
                if(!in_array($key,$request_array)){
                    if(in_array($key, $film_array)){
                        $get_filter_details = array(
                            'table' => 'films',
                            'field' => $key
                        );
                    }elseif(in_array($key, $movie_stream_array)){
                        $get_filter_details = array(
                            'table' => 'movie_streams',
                            'field' => $key
                        );
                    }else{
                        $get_filter_details = Yii::app()->custom->getFilterTableData($key, $this->studio_id);
                    }
                    if(!empty($get_filter_details)){
                        if(trim($value) != ""){
                            $ts = ($get_filter_details['table'] == "movie_streams") ? "M" : "F" ;
                            $field_name = $ts.".".$get_filter_details['field']; 
                            $fdata = explode(',' , trim($value));
                            $total = count($fdata);
                            for($i = 0; $i < $total; $i++){
                                $andor = ($i == 0) ? ' AND (' : ' OR';
                                $filter_cond .= $andor . ' '.$field_name. ' LIKE \'%' . $fdata[$i] . "%' ";
                            }
                            $filter_cond .= ") ";
                        }
                    }
                }
            }
            $episode_cond = "";
            if($is_episode !=""){
                $episode_cond = " AND M.is_episode=".$is_episode;
            }
            if(!empty($cat)){
                foreach ($cat as $category) {
                    $category_id = $category['id'];
                    $poster_category = $this->getPoster($category_id, 'content_category');
                    $category_name = $category['category_name'];
                    $category_permalink = $this->siteurl . '/' . $category['permalink'];
                    $binary_value = $category['binary_value'];
                    $sql = "SELECT id,name,story,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND content_category_value & " . $binary_value . " AND parent_id=0 ORDER BY name,parent_id ASC";
                    $films = Yii::app()->db->createCommand($sql)->queryAll();
                    $content_details = array();
                    $poster_film = array();
                    if (!empty($films)) {
                        foreach ($films as $film) {
                            $id = $film['id'];
                            $name = $film['name'];
                            $story = $film['story'];
                            $langcontent = Yii::app()->custom->getTranslatedContent($id, 0, $language_id, $studio_id);
                            if (array_key_exists($id, $langcontent['film'])) {
                                $name = $langcontent['film'][$id]->name;
                                $story = $langcontent['film'][$id]->story;
                            }
                            $fav_status = 1;
                            $login_status = 0;
                            if ($user_id) {
                                $fav_status = UserFavouriteList::model()->getFavouriteContentStatus($id, $studio_id, $user_id);
                                $fav_status = ($fav_status == 1) ? 0 : 1;
                                $login_status = 1;
                            }
                            $poster = $this->getPoster($id, 'films');
                            $permalink = $this->siteurl . '/' . $film['permalink'];
                            $content_details[] = array(
                                'content_id' => $id,
                                'name' => $name,
                                'story' => $story,
                                'poster' => $poster,
                                'content_permalink' => $permalink,
                                'is_episode' => 0,
                                'fav_status' => $fav_status,
                                'login_status' => $login_status
                            );
                        }
                    }

                    $channels[] = array(
                        'category_name' => $category_name,
                        'permalink' => $category_permalink,
                        'link' => $category['permalink'],
                        'category_poster' => $poster_category,
                        'content_list' => $content_details
                    );
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $channels;
                $this->setHeader($data['code']);
                echo json_encode($data);exit;
            }else{
                $command = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.language,F.content_types_id,F.language_id,M.episode_language_id')
                    ->from('movie_streams M,films F')
                    ->where('M.movie_id = F.id AND F.studio_id ='.$this->studio_id.' AND F.parent_id = 0 AND M.episode_parent_id =0 '.$condition.' '.$filter_cond.''.$episode_cond)
                    ->order($sort_by,$order_by)
                    ->limit($limit,$offset);
                $contents = $command->QueryAll();
                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                $customData = array();
                $list       = array();
                if(!empty($contents)){
                    foreach($contents as $content){
                        $is_episode = $content['is_episode'];
                        $content_id = $content['movie_id'];
                        if($is_episode == 1){
                            $content_id = $content['movie_stream_id'];
                        }
                        $list[] = Yii::app()->general->getContentData($content_id, $is_episode, $customData, $language_id, $studio_id, $user_id, $translate);
                    }
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $list;
                if(!empty($list)){
                    $data['total_content'] = $item_count;
                }else{
                    $data['msg'] = "No content found";    
                }
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid data sent.";    
        }
        $this->setHeader($data['code']);
        echo json_encode($data);exit;
    }    
    public function actionCheckFavourite(){
        $studio_id=$this->studio_id;
        $favenable=$this->CheckFavouriteEnable($studio_id);
        $data['code'] = 200;
        $data['status'] = "Success";
        $data['msg'] = $favenable;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }   
    public function actionViewFavourite() {
        $fav_status = $this->CheckFavouriteEnable($this->studio_id);
        if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !='') {
            if ($fav_status != 0) {
                $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset']) && $_REQUEST['offset'])
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                $langid = Yii::app()->custom->getLanguage_id(@$_REQUEST['lang_code']);
                $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($_REQUEST['user_id'], $this->studio_id, $limit, $offset, $langid);
                $this->items = @$favlist['list'];
                $$this->items['limit'] = $limit;
                $this->code = 200;
            } else {
                $this->code = 702;
            }
        } else {
            $this->code = 633;
        }
    }
	
    public function actionAddtoFavlist() {
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !='')) {
            $data = Yii::app()->db->createCommand()->select('id')->from("films")->where('uniq_id =:uniq_id',array(':uniq_id' => $_REQUEST['movie_uniq_id']))->queryRow();
            $content_type = isset($_REQUEST['content_type'])?@$_REQUEST['content_type']:'0';
            $user_fav_stat = UserFavouriteList::model()->findByAttributes(array('content_id' => $data['id'], 'content_type' => $content_type, 'studio_id' => $this->studio_id, 'user_id' => $_REQUEST['user_id']));
            if (empty($user_fav_stat)) {
                $usr_fav = new UserFavouriteList;
                $usr_fav->status = '1';
                $usr_fav->content_id = $data['id'];
                $usr_fav->content_type = $content_type;
                $usr_fav->studio_id = $this->studio_id;
                $usr_fav->user_id = $_REQUEST['user_id'];
                $usr_fav->last_updated_at = $usr_fav->date_added = date('Y-m-d H:i:s');
                $usr_fav->save();
                $this->code = 200;
            } else {
                $this->code = 701;
            }
        } else {
            $this->code = 700;
        }
    }
	
    public function actionDeleteFavList() {
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !='')) {
            $data = Yii::app()->db->createCommand()->select('id')->from("films")->where('uniq_id =:uniq_id',array(':uniq_id' => $_REQUEST['movie_uniq_id']))->queryRow();
            $content_type = isset($_REQUEST['content_type'])?@$_REQUEST['content_type']:'0';
            $params = array(':content_id' => $data['id'], ':content_type' => $content_type, ':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']);
            $user_fav_stat = UserFavouriteList::model()->deleteAll('content_id = :content_id AND content_type = :content_type AND studio_id = :studio_id AND user_id = :user_id', $params);
            $this->code = 200;
        } else {
            $this->code = 700;
        }
    } 
    
    /**@method public RegisterMyDevice() registering device for sending push notification
	 * @author Biswajit Das<support@muvi.com>
	 * @param string authToken* , string device_id* ,string fcm_token*,int device_type,string lang_code 
	 */
    public function actionRegisterMyDevice(){		
		$device_id = (isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '')?$_REQUEST['device_id']:'';
		$fcm_token = (isset($_REQUEST['fcm_token']) && $_REQUEST['fcm_token']!='')?@$_REQUEST['fcm_token']:'';
		if($device_id && $fcm_token){
			$notification = new RegisterDevice;
            $registerdevice = $notification->findByAttributes(array('studio_id' => $this->studio_id, 'device_id' => $device_id));
			if($registerdevice){
				$registerdevice->fcm_token = $fcm_token;
                $registerdevice->save();
			}else{
				$register_device = new RegisterDevice;
                $register_device->studio_id = $this->studio_id;
                $register_device->device_type = isset($_REQUEST['device_type'])?@$_REQUEST['device_type']:'1';
                $register_device->device_id = $device_id;
                $register_device->fcm_token = $fcm_token;
                $register_device->save();				
			}
			$this->code = 200;
			$this->msg_code = 'device_registerd'; 
		}else
			$this->code = 685;		  
    }
	/**@method public GetNotificationList()() It will list push notification list log device wise 
     * @param string authToken* , string device_id* ,int before_days,string lang_code
     * @author Prakash<support@muvi.com>
     * @return Json object
	 */
    public function actionGetNotificationList() {
		$device_id = (isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '') ? $_REQUEST['device_id'] : '';
		if ($device_id) {
			$diff_days = (isset($_REQUEST['before_days']) && $_REQUEST['before_days'] > 0 ) ? $_REQUEST['before_days'] : 7;
			$all_notification = Yii::app()->db->createCommand()
							->select('*')
							->from('push_notification_log')
							->where("`studio_id`=:studio_id AND status > '0' AND `device_id`=:device_id AND `created_date` >= DATE_SUB(CURDATE(), INTERVAL :diff_days DAY)", array(':studio_id' => $this->studio_id, ':device_id' => $device_id, ':diff_days' => $diff_days))->queryAll();
			if ($all_notification) {
				$unread_cnt = 0;
				foreach ($all_notification as $values) {
					if ($values['status'] == 1)
						$unread_cnt++;
				}
				$this->code = 200;
				$this->items['count'] = count($all_notification);
				$this->items['count_unread'] = $unread_cnt;
				$this->items['notify_list'] = $all_notification;
			}else {
				$this->code = 200;
				$this->msg_code = 'no_notification';
			}
		} else
			$this->code = 687;
	}
	public function actionReadNotification(){
        if((isset($_REQUEST['device_id']) && $_REQUEST['device_id'] !='') && (isset($_REQUEST['message_unique_id']) && $_REQUEST['message_unique_id'] !='') ){
            $notification = new PushNotificationLog;
            $notificationstatus = $notification->findByAttributes(array('studio_id' => $this->studio_id, 'device_id' => $_REQUEST['device_id'], 'message_unique_id'=>$_REQUEST['message_unique_id'], 'status' => '1'));
            if ($notificationstatus) {
                $notificationstatus->status = '2';
                $notificationstatus->save();
                $this->msg_code = 'notification_read';
                $this->code = 200;
            } else {
                $this->code = 705;
            }
        } else {
            $this->code = 671;
        }
    }
    public function actionGetNoOfUnreadNotification(){
        $notification = new PushNotificationLog;
        $notificationstatus = $notification->findAllByAttributes(array('studio_id' => $this->studio_id, 'device_id' => $_REQUEST['device_id'], 'status'=>'1'));
        $this->items['count'] = count($notificationstatus);
        $this->code = 200;
    }  
    public function actionReadAllNotification() {
        if ((isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '')) {
            $notification = new PushNotificationLog;
            $params = array(':studio_id' => $this->studio_id, ':device_id' => $_REQUEST['device_id'], ':status' => '1');
            $notificationstatus = $notification->updateAll(array('status' => '2'), "studio_id=:studio_id AND device_id =:device_id AND status =:status", $params);
            $this->code = 200;
        } else {
            $this->code = 671;
        }
    }
    /**
     * API for Device Management Listing
    */
    public function actionManageDevices() {
       if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
            $this->items = Yii::app()->db->createCommand()->select(' * ')   ->from("device_management")->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1',array(':studio_id' => $this->studio_id,':user_id'=>$_REQUEST['user_id']))->order ('id ASC')->queryAll();
            $this->code = $this->items ? 200 : 674;
        } else {
            $this->code = 672;
        }
    }
    /**
     * This function is used to request studio admin to remove device
     */
    public function actionRemoveDevice() {
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                $studio_id = $this->studio_id;
                $user_id = $_REQUEST['user_id'];
                $device = $_REQUEST['device'];
                $device_details =Device::model()->findBySql('SELECT * FROM `device_management` WHERE `studio_id`=:studio_id AND `user_id`=:user_id AND `device`=:device AND flag=0 LIMIT 1',array(':studio_id'=>$studio_id,':user_id'=>$user_id,':device'=>$device));				
                if (!empty($device_details)) {
                    Device::model()->updateByPk($device_details['id'], array('flag' => 1, 'deleted_date'=>date('Y-m-d H:i:s')));					
                    if (NotificationSetting::model()->isEmailNotificationRemoveDevice($studio_id, 'device_management')) {
                        $user_data = SdkUser::model()->findByPk($user_id, array('select' => 'email'));							
                        $mailcontent = array('studio_id' => $studio_id,'device' => $device, 'device_info'=>$device_details['device_info'], 'email'=>$user_data['email']);						
                        Yii::app()->email->mailRemoveDevice($mailcontent);
                    }
                    $this->code = 200;
                } else {
                    $this->code = 673;
                }
            } else {
                $this->code = 672;
            }
        } else {
            $this->code = 671;
        }
    }
    /**
     * This function is used to check a device will add or not
     */
    public function actionCheckDevice() {
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = @$_REQUEST['user_id'];
                $device = @$_REQUEST['device'];
                $google_id = @$_REQUEST['google_id'];
                $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                if (Yii::app()->custom->restrictDevices($this->studio_id, 'restrict_no_devices')) {
                    $chkDeviceLimit = Yii::app()->custom->restrictDevices($this->studio_id, 'limit_devices');
                    $all_devices = Yii::app()->db->createCommand()->select(' count(*) AS cnt, SUM(IF(device=:device, 1, 0)) AS exist ')->from("device_management")
                            ->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1', array(':studio_id' => $this->studio_id, ':user_id' => $user_id, ':device' => $device))->queryrow();
                    if ($all_devices['exist'] == 0) {
                        if ($all_devices['cnt'] < $chkDeviceLimit) {
                            $this->actionAddDevice();
                        } else {
                            $this->code = 675;
                        }
                    } else {
                        $sql = "UPDATE device_management SET google_id = :google_id WHERE studio_id =:studio_id AND user_id=:user_id AND device=:device AND flag=0";
                        Yii::app()->db->createCommand($sql)->execute(array(':google_id' => $google_id, ':studio_id' => $this->studio_id, ':user_id' => $user_id, ':device' => $device));
                        $this->code = 200;
                    }
                } else {
                    $this->code = 674;
                }
            } else {
                $this->code = 672;
            }
        } else {
            $this->code = 671;
        }
    } 
	/**
	 * This function is used to add new devices
	 */
    public function actionAddDevice() {
        $devicelist = new Device();
        $devicelist->user_id = @$_REQUEST['user_id'];
        $devicelist->studio_id = $this->studio_id;
        $devicelist->device = @$_REQUEST['device'];
        $devicelist->device_info = @$_REQUEST['device_info'];				
        $devicelist->device_type = isset($_REQUEST['device_type'])?@$_REQUEST['device_type']:'1';
        $devicelist->google_id=isset($_REQUEST['google_id'])?@$_REQUEST['google_id']:'';
        $devicelist->created_by = @$_REQUEST['user_id'];
        $devicelist->created_date=date('Y-m-d H:i:s');
        $devicelist->save();
        $this->code = 200;
    }
    
    /*
     * @author RK<developer@muvi.com>
     * To load the homepage contents
     */
    public function actionloadFeaturedSections(){
        $offset = isset($_REQUEST['dataoffset'])?$_REQUEST['dataoffset']:0;
        $limit = isset($_REQUEST['viewlimit'])?$_REQUEST['viewlimit']:2;
        $studio_id = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
        $translate = $this->getTransData($lang_code, $studio_id);        
        $user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0?$_REQUEST['user_id']:0;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.studio_id = ' . $studio_id.' AND language_id='.$language_id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $criteria->limit = $limit;
        $criteria->offset = $offset;     
        $sections = FeaturedSection::model()->findAll($criteria);
        $return = array();
        foreach ($sections as $section) {
            $final_content = array();
            $total_contents = 0;                                 
            if ($section->content_type == 1) {
                $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id);
            }else{            
                foreach ($section->contents as $featured) {                                       
                    $final_content[] = Yii::app()->general->getContentData($featured->movie_id, $featured->is_episode, array(), $language_id, $studio_id, $user_id, $translate);
                }
            }
            $return[] = array(
                'id' => $section->id,
                'title' => utf8_encode($section->title),
                'content_type' => $section['content_type'],
                'total' => count($final_content),
                'contents' => $final_content
            );
        }     
        
        $res['code'] = 200;
        $res['status'] = "OK";
        $res['section'] = $return;  
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;        
    }     
    /*@purpose Generate Widevine Licence Url
	 * @param string $conentKey , string $encryptionKey
     * @return  string 
     * @author Prakash<support@muvi.com>
    */
	private function generateExplayWideVineToken($conentKey = '', $encryptionKey = '') {
        if ($conentKey && $encryptionKey) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, WV_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&kid=" . $encryptionKey . "&contentKey=" . $conentKey . "&securityLevel=1&hdcpOutputControl=0&expirationTime=%2B120");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = curl_exec($ch);            
            curl_close($ch);
            return ($server_output)?$server_output:false;
        } else {
            return false;
        }
    }
    /*
     * @purpose for filter custom manage metadata 
     * @author Sweta<sweta@muvi.com>
    */
    public function actionGetCustomFilter(){
        $lists = array();
        $studio_id = $this->studio_id;
        $command = Yii::app()->db->createCommand()
            ->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value as options')
            ->from('custom_metadata_field f')
            ->where('f.f_type IN(2,3) AND f.studio_id='.$studio_id.' ORDER BY id ASC');
        $filterdata = $command->queryAll();
        if(!empty($filterdata)){
           foreach($filterdata as $listdata){
                $options = json_decode($listdata['options']);
                if(!is_array($options) && $listdata['options']){
                    if(!in_array(trim($listdata['options']),array('null','NULL','[','""'))){
                        $options = explode(",", $listdata['options']);
                    }
                } 
              $listdata['options'] =  $options;     
              $lists[] = $listdata; 
           }
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['filter_list'] = $lists;
        }else{
            $data['code'] = 452;
            $data['status'] = 'Failure';
            $data['msg'] = 'No data found';
        }
        echo json_encode($data);
        exit;
    }    
    public function actionTestTrans(){
        $trans  = TranslateKeyword::model()->getKeyValue('en', 'btn_login', $this->studio_id);
        echo $trans;exit;
    }
    
    public function getUserData($userData = array(), $custom_fields = false, $send_lang_list = ''){
        $data['id'] = $userData->id;
        $data['email'] = $userData->email;
        $data['display_name'] = $userData->display_name;
        $data['nick_name'] = $userData->nick_name;        
        $data['isFree'] = (int) @$userData->is_free;
        $data['is_newuser'] = 0;
        $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb');            
        $data['isSubscribed'] = (int) Yii::app()->common->isSubscribed($userData->id, $this->studio_id) ? 1 : 0;  
        if(isset($userData->is_broadcaster))
            $data['is_broadcaster'] = (int) $userData->is_broadcaster;
        if(isset($send_lang_list))
            $data['language_list'] = Yii::app()->custom->getactiveLanguageList($this->studio_id);        
        if($custom_fields){
            $custom_field_details = Yii::app()->custom->getCustomFieldsDetail($this->studio_id, $userData->id);
            if(!empty($custom_field_details)){
                foreach($custom_field_details as $key=>$val){
                    if($key == "languages")
                        $data["custom_".$key] = $val;
                    else
                        $data["custom_".$key] = $val[0];
                }
            }            
        }
        return $data;
    }
	
	/**
	* @method setSubscriptionForOtherDevices Set the user subscription of User when purchase subscription from other device like Roku, Apple etc
	* @return json Returns the response in json format
	* @param string $email* Email of the user
	* @param string $plan_id* Unique Plan id of the Subscription Plan
	* @param int $device_type* Device type For Roku
	* @param string $out_source_user_id Roku user id
	* @author Ashis<ashis@muvi.com>
	*/
   public function actionSetSubscriptionForOtherDevices() {
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$email = trim(@$_REQUEST['email']);
			$plan_id = trim(@$_REQUEST['plan_id']);
			$device_type = trim(@$_REQUEST['device_type']);
			$out_source_user_id = trim(@$_REQUEST['out_source_user_id']);

			if (!empty($email) && !empty($plan_id) && !empty($device_type)) {
				$user = SdkUser::model()->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id, 'status' => 1));
				if (isset($user) && !empty($user)) {
					$planData = SubscriptionPlans::model()->findByAttributes(array('unique_id' => $plan_id, 'studio_id' => $this->studio_id, 'status' => 1));
					if (isset($planData) && !empty($planData)) {
						$usModel = new UserSubscription;
						$subscriptionData = $usModel->findByAttributes(array('studio_id' => $this->studio_id, 'plan_id' => $planData->id, 'user_id' => $user->id, 'device_type' => $device_type, 'status' => 1));
						if (isset($subscriptionData) && !empty($subscriptionData)) {//User has already purchased the subscription
							$this->code = 736;
						} else {
							$usModel->studio_id = $this->studio_id;
							$usModel->user_id = $user->id;
							$usModel->plan_id = $planData->id;
							$usModel->device_type = @$device_type;
							$usModel->status = 1;
							$usModel->save();

							if (trim($out_source_user_id)) {
								$users = SdkUser::model()->findByPk($user->id);
								$users->out_source_user_id = $out_source_user_id;
								$users->save();
}
							$this->code = 200;
						}
					} else {//Subscription plans are not found
						$this->code = 648;
					}
				} else {
					$this->code = 633; //User doesn't exists
				}
			} else {//Either email or plan id or device type is missing
				$this->code = 735;
			}
		} else {
			$this->code = 662;
		}
	}

	/**
	* @method validateCouponForSubscriptionPlan Validate the coupon code for subscription plans
	* @return json Returns the response in json format
	* @param int $user_id* user id
	* @param string $coupon* Coupon code
	* @param string $plan_id* subcription plan id
	* @param int $currency currency id
	* @author Ashis<ashis@muvi.com>
	*/
   public function actionValidateCouponForSubscriptionPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$arg = array();
			$arg['user_id'] = $user_id = trim(@$_REQUEST['user_id']);
			$arg['coupon'] = $coupon = trim(@$_REQUEST['coupon']);
			if ($user_id && $plan_id && $coupon) {
				$currency_id = (isset($_REQUEST['currency']) && intval($_REQUEST['currency'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($this->studio_id)->default_currency_id;
				$arg['studio_id'] = $this->studio_id;
				$arg['currency'] = $currency_id;
				$couponDetails = Yii::app()->billing->isValidCouponForSubscription($arg);
				
				if ($couponDetails != 0) {
					if ($couponDetails != 1) {
						$this->code = 200;
						
						$planDetails = SubscriptionPlans::model()->getPlanDetails($plan_id, $this->studio_id, $currency_id, '', 1);
						$data['discount'] = $couponDetails['discount_amount'];
						if ($couponDetails["discount_type"] == 1) {
							$final_price = $planDetails['price'] - ($planDetails['price'] * $couponDetails['discount_amount'] / 100);
							$data['full_amount'] = $planDetails['price'];
							$data['discount_amount'] = $final_price;
							$data['extend_free_trail'] = $couponDetails['extend_free_trail'];
							$data['discount_type'] = '%';
						} else {
							if (isset($_REQUEST['currency']) && intval($_REQUEST['currency'])) {
								$currencyDetails = Currency::model()->findByPk($_REQUEST['currency']);
								$final_price = $planDetails['price'] - $couponDetails['discount_amount'];
								if (abs($final_price) < 0.01) {
									$final_price = 0;
								}
								$new_final_price = number_format($final_price, 2, '.', '');
								$data['full_amount'] = $planDetails['price'];
								$data['discount_amount'] = $new_final_price;
								$data['extend_free_trail'] = $couponDetails['extend_free_trail'];
								$data['discount_type'] = $currencyDetails['symbol'];
							} else {
								$data['discount_type'] = '$';
							}
						}
					} else {
						$this->code = 651;
					}
				} else {
					$this->code = 650;
				}
			} else {//Either email or plan id or coupon code is missing
				$this->code = 0;
			}
		} else {
			$this->code = 662;
		}
		$this->items = $data;
	}
	
	/**
	* @method public purchaseMuvikart It will process the payment for physical goods
	* @return json Return the json array of results
	* @param String $authToken* Auth token of the studio
	* @param Int user_id*
	* @param String existing_card_id*
	* @param String profile_id* User Profile id created in the payment gateways
	* @param Float amount*
	* @param Json string cart_item*
	* @param Json string ship*
	* @param Json string card*
	* @param String card_last_fourdigit Last 4 digits of the card 
	* @param Boolean is_save_this_card 1: true, 0: false
	* @param String token Payment Token
	* @param String card_type Type of the card
	* @param String email User email
	* @param int currency_id
	* @param String coupon_code
	* @author Sanjeev
	*/
	public function actionPurchaseMuvikart() {
        if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data = array();
			$data['profile_id'] = @$_REQUEST['profile_id'];
			$data['card_type'] = @$_REQUEST['card_type'];
			$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
			$data['token'] = @$_REQUEST['token'];
			$data['email'] = @$_REQUEST['email'];
			$data['currency_id'] = @$_REQUEST['currency_id'];
			$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
			$data['existing_card_id'] = @$_REQUEST['existing_card_id'];
			$data['coupon_code'] = @$_REQUEST['coupon_code'];
			$data['amount'] = @$_REQUEST['amount'];
			$data['dollar_amount'] = @$_REQUEST['amount'];
			$data['studio_id'] = $studio_id = $this->studio_id;
			$data['user_id'] = $user_id = $_REQUEST['user_id'];
			$_REQUEST["cart_item"] = json_decode($_REQUEST["cart_item"], true);
			$_REQUEST["ship"] = json_decode($_REQUEST["ship"], true);
			$data['cart_item'] = $_REQUEST["cart_item"];
			$data['ship'] = $_REQUEST["ship"];
			$gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
			if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
				 $this->setPaymentGatwayVariable($gateway_info);

				if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
					$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
					$data['card_id'] = @$card->id;
					$data['token'] = @$card->token;
					$data['profile_id'] = @$card->profile_id;
					$data['card_holder_name'] = @$card->card_holder_name;
					$data['card_type'] = @$card->card_type;
					$data['exp_month'] = @$card->exp_month;
					$data['exp_year'] = @$card->exp_year;
}

				$couponCode = '';
				//Calculate coupon if exists
				if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
					$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
					$data['amount'] = $getCoup["amount"];
					$couponCode = $getCoup["couponCode"];
					$data['coupon_code'] = $couponCode;
					$data['discount_type'] = 0; //$getCoup["discount_type"];
					$data['discount'] = $getCoup["coupon_amount"];
				}


				if ($data['amount'] > 0) {
					$currency = Currency::model()->findByPk($data['currency_id']);
					$data['currency_id'] = $currency->id;
					$data['currency_code'] = $currency->code;
					$data['currency_symbol'] = $currency->symbol;
					$timeparts = explode(" ", microtime());
					$currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
					$reference_number = explode('.', $currenttime);
					$data['order_reference_number'] = $reference_number[0];
					$payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
					Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
					$payment_gateway = new $payment_gateway_controller();
					$data['gateway_code'] = $gateway_info[0]->short_code;
					$data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
					$trans_data = $payment_gateway::processTransactions($data); //charge the card
					if (intval($trans_data['is_success'])) {
						if (intval($data['is_save_this_card'] == 1)) {
							$sciModel = New SdkCardInfos;
							$card = $data;
							$card['card_last_fourdigit'] = $res['card']['card_last_fourdigit'];
							$card['token'] = $res['card']['token'];
							$card['card_type'] = $res['card']['card_type'];
							$card['auth_num'] = $res['card']['auth_num'];
							$card['profile_id'] = $res['card']['profile_id'];
							$card['reference_no'] = $res['card']['reference_no'];
							$card['response_text'] = $res['card']['response_text'];
							$card['status'] = $res['card']['status'];
							$sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
						}
						//Save a transaction detail
						$transaction = new Transaction;
						$transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
						$data['transactions_id'] = $transaction_id;
						/* Save to order table */
						$data['hear_source'] = $_REQUEST['hear_source'];
						$pgorder = new PGOrder();
						$orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
						/* Save to shipping address */
						$pgshippingaddr = new PGShippingAddress();
						$pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
						/* Save to Order Details */
						$pgorderdetails = new PGOrderDetails();
						$pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);


						//send email
						$req['orderid'] = $orderid;
						$req['emailtype'] = 'orderconfirm';
						$req['studio_id'] = $studio_id;
						$req['studio_name'] = @$data['studio_name'];
						$req['currency_id'] = $data['currency_id'];
						// Send email to user
						Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
						$isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
						if ($isEmailToStudio) {
							Yii::app()->email->pgEmailTriggers($req);
						}
						//create order in cds
						$webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
						if ($webservice) {
							if ($webservice->inventory_type == 'CDS') {
								$pgorder = new PGOrder();
								$pgorder->CreateCDSOrder($studio_id, $orderid);
								//PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
							}
						}
						$this->code = 200;
					}else{
						$this->code = 614;
						$this->msg_code = 'error_transc_process';
					}
				}else {
					$isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
					if($isData){
						$this->code = 200;
					}
				}
			}else{
				$this->code = 612;
			}
		}else {
			$this->code = 662;
		}
    }
    
    public function physicalDataInsert($datap, $pay, $trans_data, $ip, $short_code) {
        //Save a transaction detail
        $datap['transactions_id'] = $transaction_id = Transaction::model()->insertTrasactions($this->studio_id, $pay['user_id'], $datap['currency_id'], $trans_data, 4, $ip, $short_code);
        /* Save to order table */
        $datap['hear_source'] = $pay['hear_source'];
        $pgorder = new PGOrder();
        $orderid = $pgorder->insertOrder($this->studio_id, $pay['user_id'], $datap, $datap["cart_item"], $datap['ship'], $ip);
        /* Save to shipping address */
        $pgshippingaddr = new PGShippingAddress();
        $pgshippingaddr->insertAddress($this->studio_id, $pay['user_id'], $orderid, $datap['ship'], $ip);
        /* Save to Order Details */
        $pgorderdetails = new PGOrderDetails();
        $pgorderdetails->insertOrderDetails($orderid, $datap["cart_item"]);
        if ($pay['card_options'] != '') {
            $data = array('isSuccess' => 1);
            $data = json_encode($data);
        }
        //send email
        $req['orderid'] = $orderid;
        $req['emailtype'] = 'orderconfirm';
        $req['studio_id'] = $this->studio_id;
        $req['studio_name'] = $datap['studio_name'];
        $req['currency_id'] = $datap['currency_id'];
        // Send email to user
        Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio_id);
        if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
        }
        if($transaction_id){
            $command = Yii::app()->db->createCommand();
            $command->delete('pg_cart', 'user_id=:userid AND studio_id:studio_id', array(':userid' => $pay['user_id'],':studio_id' => $this->studio_id)); 
        }   
        //create order in cds
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($webservice) {
            if ($webservice->inventory_type == 'CDS') {
                $pgorder->CreateCDSOrder($this->studio_id, $orderid);
                //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
            }
        }
		
        return true;
    }
	/**
     * @method private GetAppFeaturedContent() Get featured contents of a particular section for  Mobile & TV Apps 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of featured contents in json format
     * @param string authToken*,string section_id*, string lang_code.
     * 
     */
    public function actionGetAppFeaturedContent(){
        $lang_code   = @$_REQUEST['lang_code'];       
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate   = $this->getTransData($lang_code, $this->studio_id);
        if(isset($_REQUEST['section_id']) && $_REQUEST['section_id'] !=""){
            $domainName= $this->getDomainName();
            $contents = AppFeaturedContent::model()->getAppFeatured($_REQUEST['section_id'], $this->studio_id, $language_id,0,$domainName);			
            if(!empty($contents)){
                $this->code = 200;
                $this->items = $contents;
            }else
				$this->code = 693;
        }else
			$this->code = 688;            
	}
}