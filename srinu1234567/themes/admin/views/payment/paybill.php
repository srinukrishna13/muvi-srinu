<?php
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
$config = Yii::app()->common->getConfigValue(array('monthly_charge'), 'value');
?>
<style type="text/css">
    .form-horizontal .control-label {
        text-align: left;
    }
    h3, .h3 {
        font-size: 22px;
        text-align: center;

    }
    .btn-left    {
        margin-left: -15px;
    }
    .control-label-font
    {
        font-size: 16px;
    }
    .checkbox-left{
        margin-left: 18px;
    }
    .error{font-weight: normal;}
</style>
<div class="col-lg-12 box box-primary">
    <div class="col-lg-12">
        <form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
            <div class="row form-group">
                <div class="col-lg-11">
                    <?php if (isset($billing_infos) && !empty($billing_infos)) { ?>
                    <h3 class="pull-left"><?php echo $billing_details->title;?></h3>
                    <?php } ?>
                </div>              
            </div>
            <?php if (isset($billing_infos) && !empty($billing_infos)) { ?>
            <div class="row form-group">
                <div class="col-lg-11">
                    <div>
                        <label class="control-label-font">Date:</label> <?php echo date('F d, Y'); ?>
                    </div>
                    <div>
                        <label class="control-label-font">Amount:</label> <?php echo '$' . $billing_infos->billing_amount; ?>
                    </div>
                    <br/>
                    <div><label class="control-label-font">Description:</label></div>
                    <div><?php echo nl2br($billing_details->description); ?></div>
                    <br/>
                </div>
                <input type="hidden" id="billing_uniqid" name="billing_uniqid" value="0" />
            </div>
            <?php } ?>
            
            <?php if (isset($card) && !empty($card)) { ?>
            <div class="row form-group">
                <div class="col-lg-11">
                    <div>
                        <label class="control-label-font">Pay using:</label>
                    </div>
                    <div>
                        <div class="pull-left">
                            <label class="control-label-font" style="font-weight: normal;">
                                <input type="radio" name="payment_info" id="used_card" onclick="paymentInfo(this);" value="saved" />&nbsp;&nbsp;
                            </label>
                        </div>
                        <div class="pull-left">
                            <select name="creditcard" id="creditcard" class="form-control" style="width: auto;margin-left: 5%;">
                                <?php foreach ($card as $key => $value) { ?>
                                    <option value="<?php echo $value->id; ?>" <?php if (isset($value->is_cancelled) && $value->is_cancelled == 0) { ?>selected="selected"<?php } ?>><?php echo $value->card_last_fourdigit . " " . $value->card_type; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <label class="control-label-font" style="font-weight: normal;">
                            <input type="radio" name="payment_info" id="new_card" onclick="paymentInfo(this);" value="new" />&nbsp;&nbsp;Or, add a new card
                        </label>
                    </div>
                    <div>
                        <label id="payment_info-error" class="error" for="payment_info" style="display: none;font-weight: normal;"></label>
                    </div>
                </div>             
            </div>
            <?php } ?>
            
            <div id="new_card_dv" style="display: none;"></div>

            <div class="form-group">
                <div class="col-lg-9">
                    <label class="control-label col-lg-3">&nbsp;</label>
                    <div class="col-lg-5">&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-blue btn-left" id="nextbtn">Make Payment</button></div>
                </div>
            </div>

            <br /><br />
        </form>        
    </div>
</div>

<div id="new_card_info" style="display: none;">
    <div id="card-info-error" class="error" style="display: none;margin: 0 0 15px 170px;"></div>
    <div class="row form-group">
        <div class="col-lg-9">
            <label class="control-label col-lg-3">Name on Card</label>                    
            <div class="col-lg-9">
                <input type="text" class="form-control" id="card_name" name="card_name" placeholder="Enter Name" />
            </div>
        </div>                     
    </div>
    <div class="row form-group">
        <div class="col-lg-9">
            <label class="control-label col-lg-3">Card Number</label>                    
            <div class="col-lg-9">
                <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Enter Card Number" />
            </div>
        </div>                     
    </div>
    <div class="row form-group">
        <div class="col-lg-9">
            <label class="control-label col-lg-3">Expiry Date</label>                    
            <div class="col-lg-9">
            <div style="float: left;width: 49%;margin-right: 2%;">
                <select name="exp_month" id="exp_month" class="form-control">
                    <option value="">Expiry Month</option>	
                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div style="float: left;width: 49%;">
                <select name="exp_year" id="exp_year" class="form-control" onchange="getMonthList();">
                    <option value="">Expiry Year</option>
                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </div>
                <div style="clear: both;"></div>
            </div>
        </div>                     
    </div>
    <div class="row form-group">
        <div class="col-lg-9">
            <label class="control-label col-lg-3">Security Code</label>                    
            <div class="col-lg-9">
                <input type="password" id="" name="" style="display: none;" />
                <input type="password" class="form-control" id="security_code" name="security_code" placeholder="Enter security code" />
            </div>
        </div>                     
    </div>        
    <div class="row form-group">
        <div class="col-lg-9">
            <label class="control-label col-lg-3">Billing Address</label>                    
            <div class="col-lg-9">
                <input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" ><br/>
                <input type="text" class="form-control" id="address2" name="address2" placeholder="Address 2" ><br/>
                <input type="text" class="form-control" id="city" name="city" placeholder="City"><br/>
                <div style="float: left;width: 49%;margin-right: 2%;">
                    <input type="text" class="form-control" id="state" name="state" placeholder="State" />
                </div>
                <div style="float: left;width: 49%;">
                    <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" />
                </div>
                <div style="clear: both"></div>
            </div>
        </div>                     
    </div>
</div>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Thank you, Your payment has been processed successfully.<br/>Please wait we are redirecting you...</h4>
            </div>
        </div>
    </div>
</div>

<?php if (isset($card) && empty($card)) { ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#new_card_dv" ).html($("#new_card_info").html()).show();
    });
</script>
<?php } ?>

<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    });
    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;
        
        if (curyr === selyr) {
            startindex = curmonth;
        }
        
        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" '+selected+'>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    
    function paymentInfo(obj) {
        $('#paymentMethod')[0].reset();
        $(obj).prop('checked', true);
        
        if ($("#new_card:checked").length) {
            $( "#new_card_dv" ).html($("#new_card_info").html());
            $( "#new_card_dv" ).slideDown( "slow");
        } else {
            $( "#new_card_dv" ).html('');
            $( "#new_card_dv" ).slideUp( "slow");
        }
    }
    
    function validateForm() {
        $('#card-info-error').hide();
        
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                payment_info: "required",
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                payment_info: "Please select payment information",
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            }
        });

        var x = validate.form();
        
        if (x) {
            var is_used_card = 0;
            if ($("#used_card:checked").length) {
                is_used_card = 1;
            }
            
            var billing_uniqid = '<?php echo $billing_infos->uniqid;?>';
            $("#billing_uniqid").val(billing_uniqid); 
            if (parseInt(is_used_card)) {
                $('#nextbtn').html('wait!...');
                $('#nextbtn').attr('disabled','disabled');
                var url ="<?php echo Yii::app()->baseUrl; ?>/payment/makePayment";
                document.paymentMethod.action = url;
                document.paymentMethod.submit();return false;
            } else {
                $('#nextbtn').html('wait!...');
                $('#nextbtn').attr('disabled','disabled');
                $("#loadingPopup").modal('show');
                var url ="<?php echo Yii::app()->baseUrl; ?>/payment/authenticateAndMakePayment";
                var card_name = $('#card_name').val();
                var card_number = $('#card_number').val();
                var exp_month = $('#exp_month').val();
                var exp_year = $('#exp_year').val();
                var cvv = $('#security_code').val();
                var address1 = $('#address1').val();
                var address2 = $('#address2').val();
                var city = $('#city').val();
                var state = $('#state').val();
                var zip = $('#zipcode').val();
                
                $.post(url,{'card_name':card_name, 'card_number':card_number, 'exp_month':exp_month, 'exp_year':exp_year, 'cvv':cvv, 'address1':address1, 'address2':address2, 'city':city, 'state':state, 'zip':zip, 'billing_uniqid': billing_uniqid},function(data){
                    $("#loadingPopup").modal('hide');
                    if (parseInt(data.isSuccess) === 1) {
                        $("#successPopup").modal('show');
                        setTimeout(function() {
                            $('#is_submit').val(1);
                            window.location = '<?php echo Yii::app()->baseUrl; ?>/payment/paymentHistory';
                        }, 5000);
                   } else {
                        $('#nextbtn').html('Make Payment');
                        $('#nextbtn').removeAttr('disabled');
                        $('#is_submit').val('');
                        if ($.trim(data.Message)) {
                            $('#card-info-error').show().html(data.Message);
                        } else {
                            $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                        }
                   }
                }, 'json');
            }
        }
    }
</script>