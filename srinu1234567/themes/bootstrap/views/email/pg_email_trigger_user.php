    <table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
    <tbody>
    	<tr>
    		<td>
    			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
    				<tbody>
    					<tr style="background-color:#f3f3f3">
    						<td style="text-align:left;padding-top:10px">
                                <div mc:edit="logo"><?php echo $params['studiologo']; ?></div>
    						</td>
    						<td></td>
    					</tr>
    				</tbody>
    			</table>
    			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
    			<tbody>
    				<tr>
    					<td>
    						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                <p style="display:block;margin:0 0 17px">
                                	Dear <span mc:edit="name"><?php echo $params['name']; ?>
                                </p>              
    			    <p style="display:block;margin:0 0 17px">
                                     <?php echo $params['firstline']; ?>     
                                </p>
    			    <p style="display:block;margin:0 0 17px">
                                     Order Number :  <?php echo $params['ordernumber']; ?>   
                                </p> 
                                <p>
                                    <table width="100%" cellspacing="1" cellpadding="2" bgcolor="#CCCCCC">
                                        <tr bgcolor="#17A6B2">
                                            <th align="center">Image</th>
                                            <th align="center">Product</th>
                                            <th align="center">Quantity</th>
                                            <th align="center">Unit Price</th>
                                            <th align="center">Subtotal</th>                                        
                                        </tr>
                                    
                                    <?php
                                    foreach ($params['items'] as $item) {
                                        $itemtot = 0;
                                        $img_path = PGProduct::getpgImage($item['product_id'],'thumb');
                                        //get the currency id
                                        $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $item['product_id']));
                                        $itemtot = $item['price'] * $item['quantity'];
                                        
                                        $total = $total + $itemtot;
                                    ?>
                                        <tr bgcolor="#FFFFFF">
                                            <td align="center"><img src="<?php echo $img_path; ?>" alt="<?php echo $value['name']; ?>" ></td>
                                            <td align="center"><?php echo $item['name'];?></td>
                                            <td align="center"><?php echo $item['quantity'];?></td>
                                            <td align="center"><?php echo ($item['price']!='0.00')?Yii::app()->common->formatPrice($item['price'],$pgproduct['currency_id']):'Free';?></td>
                                            <td align="center"><?php echo ($item['price']!='0.00')?Yii::app()->common->formatPrice($itemtot,$pgproduct['currency_id']):'Free';?></td>                                            
                                        </tr>
                                    <?php                                
                                    }
                                    $shipping_cost = $params['shipping_cost'];
                                    $discount = $params['discount'];
                                    $grand_total = $params['grand_total'];                                    
                                    ?>
                                        <tr bgcolor="#FFFFFF">
                                            <th colspan="4" align="right">Subtotal:</th>
                                            <th align="center"><?php echo Yii::app()->common->formatPrice($total,$pgproduct['currency_id']);?></th>
                                        </tr>
                                        <?php if($discount){?>
                                        <tr bgcolor="#FFFFFF">
                                            <th colspan="4" align="right">Discount:</th>
                                            <th align="center"><?php 
                                            if($params['discount_type']){
                                                echo $discount.'%';
                                            }else{
                                                echo Yii::app()->common->formatPrice($discount,$pgproduct['currency_id']);
                                            }                                            
                                            ?></th>
                                        </tr>
                                        <?php }?>
                                        <tr bgcolor="#FFFFFF">
                                            <th colspan="4" align="right">Shipping Cost:</th>
                                            <th align="center"><?php echo Yii::app()->common->formatPrice($shipping_cost,$pgproduct['currency_id']);?></th>
                                        </tr>
                                        <tr bgcolor="#FFFFFF">
                                            <th colspan="4" align="right">Total:</th>
                                            <th align="center"><?php echo Yii::app()->common->formatPrice($grand_total,$pgproduct['currency_id']);?></th>
                                        </tr>
                                    </table>
                                </p>
                                <?php
                                if($params['emailtype']=='ordershipped'){
                                ?>
                                <p>
                                  <h3>Shipping Details</h3>
                                  Shipping Method - <?php echo $params['cds_shipping_method']; ?><br />
                                  Tracking - <?php echo $params['cds_tracking']; ?><br />
                                  Consignment No - <?php echo $params['cds_consignment_no']; ?><br />
                                  Despatch No - <?php echo $params['cds_despatch_no']; ?><br />
                                </p>
                                <?php
                                }
                                ?>
                                <p>
                                <h3>Delivery Address</h3>
                                <?php echo $params['first_name']; ?><br />
                                <?php echo $params['address']; ?>,<?php echo $params['address2']; ?><br />
                                <?php echo $params['city']; ?><br />
                                <?php echo $params['state']; ?><br />
                                <?php echo $params['country']; ?><br />
                                <?php echo $params['zip']; ?><br />
                                <?php echo $params['phone_number']; ?>
                                </p>
    
                                
     
    						</div>
    					</td>
    				</tr>
                     <tr>
                        <td>
                            <table style="width:100%;font-family:helvetica,Arial;width:70%;">
                                <tbody>
                                    <tr>
                                        <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                            Regards,
                                            <p style="font-size:14px;margin:2px 0px">
                                                <?php echo $params['studio_name']; ?>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
    			</tbody>
    			</table>
          </td>
        </tr>
      </tbody>
</table>