<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />   
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/icon.png" type="image/png" rel="icon" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="canonical" href="https://www.muvi.com/signup" />
        <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>" />
        <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Launch your Audio / Video Streaming Platform Instantly, signup for 14 days free trial - Muvi" />
        <meta property="og:description" content="Launch your Audio/Video On-demand & Live Streaming Platform instantly in 1-Click at Zero upfront Investment. Signup now for our 14 days FREE Trial, no credit card required." />
        <meta property="og:url" content="https://www.muvi.com/signup" />
        <meta property="og:site_name" content="Muvi" />
        <meta property="og:image" content="https://www.muvi.com/wpstudio/wp-content/uploads/2016/11/muvi-og-image.jpg" />

        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' type='text/css' media='all' />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo yii::app()->theme->baseurl; ?>/css/owl.carousel.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo yii::app()->theme->baseurl; ?>/css/owl.theme.default.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/font-awesome/css/font-awesome.min.css" />
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/wpstudio/wp-content/themes/muvi/video/video-js.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/css/videojs.watermark.css" rel="stylesheet" type="text/css" /> 

        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/common/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/bs/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true); ?>/wpstudio/wp-content/themes/muvi/video/video.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/js/videojs.watermark.js" ></script>
        <style>
            .error {
                color: red;
                font-size: 12px;
                font-weight: 600;
                margin-bottom: 0px;
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod("mail", function(value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, "Please enter a correct email address");
                jQuery.validator.addMethod("phone", function(value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, "Please enter correct phone number");
            });
        </script>
        <style>.async-hide { opacity: 0 !important} </style>
        <script>
            (function(a, s, y, n, c, h, i, d, e) {
                s.className += ' ' + y;
                h.start = 1 * new Date;
                h.end = i = function() {
                    s.className = s.className.replace(RegExp(' ?' + y), '')
                };
                (a[n] = a[n] || []).hide = h;
                setTimeout(function() {
                    i();
                    h.end = null
                }, c);
                h.timeout = c;
            })(window, document.documentElement, 'async-hide', 'dataLayer', 4000, {'GTM-T85M3G4': true});
        </script>   
<?php
if (strpos($_SERVER['HTTP_HOST'], 'muvi.in')) {
    ?>
            <script>
                (function(i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function() {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-60904088-1', 'auto');
                ga('send', 'pageview');

            </script>    
            <?php
        } else if ($_SERVER['HTTP_HOST'] != 'localhost') {
            ?>
            <script>
                (function(i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function() {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-57762605-1', 'auto');
                ga('require', 'GTM-T85M3G4');
                ga('send', 'pageview');

            </script>
<?php }
if (Yii::app()->controller->action->id == 'TypeofContent') {
    ?>
            <img height="1" width="1" style="display:none" src="https://www.quora.com/_/ad/e93e17883f52473fb84c931eaa8a1329/pixel" />
            <?php } ?>     
        <script type='text/javascript'>
            window.__lo_site_id = 78655;
            (function() {
                var wa = document.createElement('script');
                wa.type = 'text/javascript';
                wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wa, s);
            })();
        </script>  
        <!-- Google Tag Manager -->
        <script>(function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MG9TSS7');</script>
        <!-- End Google Tag Manager -->     
    </head>
        <?php $cur_action = Yii::app()->controller->action->id; ?>    
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG9TSS7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->           
        <div class="freetrial">
<?php require_once 'header.php'; ?>
            <div class="container freetrail-container">
                <div class="row frt_content_box">

                    <?php echo $content; ?>
                    <div class="col-md-6 col-sm-6 frt_lft_box">
                        <div class="frt_hdng1"> Muvi Includes</div>
                        <div class="frt_list_box">
                            <div class="frt_list">Instant Setup &amp; Launch</div>
                            <div class="frt_list">Video, Audio or Physical</div>
                            <div class="frt_list">Fully Managed, End-to-End Platform</div>
                            <div class="frt_list">Website, Mobile* &amp; TV Apps* </div>
                            <div class="frt_list">One CMS controls everything</div>
                            <div class="frt_list">Multiple Monetizations - Subscription, Pay Per View, Ad Supported</div>
                            <div class="frt_list">Cloud Hosting, Servers, CDN, DRM*, Online Video Player</div> 
                            <div class="frt_list">Maintenance, Updates &amp; Support </div>       
                        </div>
                        <div class="frt_hdng2"> <a href="#" data-target="#myModal" data-toggle="modal" id="how-it-works" > <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/how-it-works-bullet.png" alt=""/>&nbsp;&nbsp; How it Works </a></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <p class="frt_additional_txt">*Additional charges apply</p>
                    </div> 
                </div>
            </div>

        </div>
<?php require_once 'footer.php'; ?>

<?php if (WP_USE_THEMES == 1) { ?>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body custom-video-container">
                            <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span class="videoclosebtn" aria-hidden="true">&times;</span></button>
                            <!--Start Video player Modal-->
    <?php
    if (is_active_sidebar('home-page-hoow-it-works-video')) {
        dynamic_sidebar('home-page-hoow-it-works-video');
    }
    ?>
                            <!--End Video player modal-->
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
        <script type="text/javascript" src="<?php echo yii::app()->theme->baseurl; ?>/js/owl.carousel.js"></script>

        <script>
            $(document).ready(function() {
                $(window).resize(function() {
                    console.log(window.innerWidth);
                    if (window.innerWidth <= 768) {
                        $('#menu1').html('Video');
                        $('#menu2').html('Live');
                        $('#menu3').html('Audio');
                        $('#menu4').html('Playout');
                        $('#menu5').html('Physical');
                    } else if (window.innerWidth > 768 && window.innerWidth <= 991) {
                        $('#menu1').html('Video On Demand');
                        $('#menu2').html('Live Streaming');
                        $('#menu3').html('Audio Streaming');
                        $('#menu4').html('Playout');
                        $('#menu5').html('Physical Goods');
                        $('.navbar-inverse .navbar-nav > li > a').css('padding-left', '10px');
                    }

                });

                if ($(window).width() <= 768)
                {
                    $('#menu1').html('Video');
                    $('#menu2').html('Live');
                    $('#menu3').html('Audio');
                    $('#menu4').html('Playout');
                    $('#menu5').html('Physical');
                }
                //Add blue animated border and remove with condition when focus and blur
                if ($('.fg-line')[0]) {
                    $('body').on('focus', '.fg-line .form-control', function() {
                        $(this).closest('.fg-line').addClass('fg-toggled');
                    })

                    $('.fg-input').each(function(){
                        if($(this).val()!='')
                        {
                           $(this).closest('.fg-line').addClass('fg-toggled');
                        }
                    });
                                    
                   $('body').on('blur', '.form-control', function () {
                        var p = $(this).closest('.form-group, .input-group');
                        var i = p.find('.form-control').val();

                        if (p.hasClass('fg-float')) {
                            if (i.length == 0) {
                                $(this).closest('.fg-line').removeClass('fg-toggled');
                            }
                        } else {
                            $(this).closest('.fg-line').removeClass('fg-toggled');
                        }
                    });
                }
                $('.who_is_useing_it').owlCarousel({
                    items: 4,
                    loop: true,
                    margin: 10,
                    dots: false,
                    autoplay: true
                });

                $('.frt_testimonial').owlCarousel({
                    items: 1,
                    loop: true,
                    nav: true,
                    navText: ["<img src='<?php echo yii::app()->theme->baseurl; ?>/images/Arrow_left.png'>", "<img src='<?php echo yii::app()->theme->baseurl; ?>/images/Arrow_right.png'>"],
                    margin: 10,
                    autoplay: true
                });


                $(".platform").on('click', function()
                {
                    if (!$(this).hasClass('platform_selected'))
                    {
                        $(this).addClass("platform_selected");
                        $(this).find('img').attr('src', $(this).find('img').attr('changeimg'));
                        $(this).find('input').prop("checked", true);
                    } else
                    {
                        $(this).removeClass("platform_selected");
                        $(this).find('img').attr('src', $(this).find('img').attr('defaultimg'));
                        $(this).find('input').prop("checked", false);
                    }
                });


                $("#how-it-works").click(function() {
                    $('video').attr("autoplay", "");
                    $("video").each(function() {
                        this.play()
                    });
                });
                $(".close").click(function() {
                    $("video").each(function() {
                        this.pause()
                    });
                });

                $('#myModal').on('hidden.bs.modal', function() {
                    $("video").each(function() {
                        this.pause()
                    });
                });
                videojs('example_video_1').ready(function() {
                    player = this;
                    player.watermark({
                        file: "https://www.muvi.com/wpstudio/wp-content/uploads/2016/11/logoicon.png",
                        xrepeat: 0,
                        opacity: 0.75
                    });
                    $(".vjs-watermark").attr("style", "bottom:50px;right:0px;width:7%;");
                });

            });
        </script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3Ck44EbYwaLXpRDAitMdvTyD4LH7YeoO";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<script type="text/javascript">

    var intervalVariable;
intervalVariable = setInterval(zopim30sec, 10000);
$zopim(function () { 
    
    $zopim.livechat.setOnConnected(function () {
       
        $zopim.livechat.departments.clearVisitorDepartment();
        var zemail = $zopim.livechat.getEmail();
        var zname = $zopim.livechat.getName();
		//alert(CHECKSTUDIOLOGIN);
	<?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
		<?php } else { ?>
        
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
		<?php } ?>
        
        //zopim redirection to sales and support department as per the login details
        if (zemail != '') {
            if (getCookie("zopimemail") != zemail) {
                $.post("/contact/SendFromZopim", {"email": zemail, "name": zname}, function () {
                    document.cookie = "zopimemail=" + zemail;
                });
            }
        } else {
            //intervalVariable = setInterval(zopim30sec, 20000);
        }
    });
    
     
   });  
$zopim(function() {
$zopim.livechat.setOnChatEnd(end);
});
function end() {
$zopim.livechat.clearAll();
}
function zopim30sec() {
    
    $zopim.livechat.departments.clearVisitorDepartment();
    
    var zemail1 = $zopim.livechat.getEmail();
    var zname1 = $zopim.livechat.getName();
    //zopim redirection to sales and support department as per the login details
     //alert(CHECKSTUDIOLOGIN);
    <?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
        <?php } else { ?>
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
       <?php } ?>
	
        
    //zopim redirection to sales and support department as per the login details
    if (zemail1 != '') {
        if (getCookie("zopimemail") != zemail1) {
            $.post("/contact/SendFromZopim", {"email": zemail1, "name": zname1}, function () {
                document.cookie = "zopimemail=" + zemail1;
                //clearInterval(intervalVariable);
            });
        }
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
</script>
    </body>
</html>
