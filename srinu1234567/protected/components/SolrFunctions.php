<?php
require_once( $_SERVER["DOCUMENT_ROOT"].'/SolrPhpClient/Apache/Solr/Service.php' );
class SolrFunctions extends CApplicationComponent {
	
/**
 * @method public deletesolrQuery() It will delete a query from solr
 * @param string $cond Condition of deletion
 * @author Gayadhar<support@muvi.com>
 * @return boolen True/false
 */
	function deleteSolrQuery($cond=''){
		if($cond){
			$solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );
			$solr->deleteByQuery($cond);
			$solr->commit(); //commit to see the deletes and the document
			$solr->optimize();
			return TRUE;
		}else{
			return false;
		}
	}
/**
 * @method type addsolrData(type $paramName) It will add data into solr
 * @author Gayadhar<support@muvi.com>
 * @param array $data Data to be added to solr
 * @return bool true/false
 */	
	function  addSolrData($data){
            
		if($data){
			$solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );                                             
			$part = new Apache_Solr_Document();
			$part -> id = $data['uniq_id'];
			$part -> content_id = $data['content_id'];
			$part -> stream_id = $data['stream_id'];
			$part -> stream_uniq_id = $data['stream_uniq_id'];
			$part -> is_episode = $data['is_episode'];
			$part -> name = $data['name'];
			$part -> title = $data['permalink'];
			if($data['is_episode'] ==1){
				$part -> cat = strtolower(trim($data['display_name']))." - Episode";
			}else{
				$part -> cat = strtolower(trim($data['display_name']));
			}
			$part -> sku = $data['studio_id'];
			$part -> features = @$data['content_permalink'];
			$part -> genre = @$data['genre_val'];
			$part -> format = @$data['product_format'];
			$part -> product_sku = @$data['product_sku'];
			if(@$data['publish_date'])
				$part -> publish_date =date('Y-m-d',strtotime(@$data['publish_date'])).'T'. date('H:i:s',strtotime(@$data['publish_date'])).'Z';                      
			$return = $solr->addDocument( $part );
			$ret1 = $solr->commit();
			$ret2= $solr->optimize();
			return TRUE;
		}else{
			return FALSE;
		}
	}
}