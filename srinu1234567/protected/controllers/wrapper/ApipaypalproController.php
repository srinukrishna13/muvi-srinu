<?php

require_once 'Paypal/paypal_pro.inc.php';

class ApipaypalproController extends Controller{
    
    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }
    
    /**
     *
     * Initialize the api
     * @return payPalPro Object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() 
    {   
        $gateway_code = 'paypalpro';
        if($this->IS_LIVE_API_PAYMENT_GATEWAY[$gateway_code] != 'sandbox'){
            $payPalPro = new paypal_pro(trim($this->PAYMENT_GATEWAY_API_USER[$gateway_code]),trim($this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code]),trim($this->PAYMENT_GATEWAY_API_SIGNATURE[$gateway_code]),'','',TRUE,FALSE,'57.0');
        }else{
            $payPalPro = new paypal_pro(trim($this->PAYMENT_GATEWAY_API_USER[$gateway_code]),trim($this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code]),trim($this->PAYMENT_GATEWAY_API_SIGNATURE[$gateway_code]),'','',FALSE,FALSE,'57.0');
        }
        
        return $payPalPro;
    }
    
    /**
     * Validate the card and make a zero transaction
     * @return redirect to PayPal for Account Information
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCard($arg = array())
    {
        $payPalPro = self::initializePaymentGateway();
        $arg['currency_code'] = strtoupper($arg['currency_code']);
        if(isset($arg['have_paypal']) && $arg['have_paypal']){
            $amount = $arg['paymentAmount'];
            $currency = strtoupper($arg['currency_code']);
            
            if (intval($this->IS_CURRENCY_CONVERSION['paypalpro']) && isset($currency) && trim($currency) != 'USD') {
                $amount = $arg['dollar_amount'] = Yii::app()->billing->currencyConversion($currency, 'USD', $amount);
                $currency = 'USD';
            }
            $arg['paymentAmount']=$amount; 
            $arg['currencyCodeType']=$currency; 
            $arg['currency_code']=$currency; 
            $arg['billingType'] = isset($arg['billingType']) && trim($arg['billingType'])?$arg['billingType']:'RecurringPayments';
            $resArray = $payPalPro->CallShortcutExpressCheckout ($arg);
            if($resArray['TOKEN']){
               $payPalPro->RedirectToPayPal($resArray["TOKEN"] );
            }
        }else{
            if (isset($arg['card_number']) && !empty($arg['card_number'])) {
                $cardDetails = array();
                $cardDetails['paymentAction'] = 'Authorization';
                $cardDetails['trxType'] = 'A';
                $cardDetails['amount'] = 0.00;
                $cardDetails['currency_code'] = $arg['currency_code'];
                
                $amount = $cardDetails['amount'];
                $currency = strtoupper($cardDetails['currency_code']);
            
                if (intval($this->IS_CURRENCY_CONVERSION['paypalpro']) && isset($currency) && trim($currency) != 'USD') {
                    $amount = $arg['dollar_amount'] = Yii::app()->billing->currencyConversion($currency, 'USD', $amount);
                    $currency = 'USD';
                    $cardDetails['currency_id'] = 153;
                }
                $cardDetails['paymentAmount']=$amount; 
                $cardDetails['currencyCodeType']=$currency; 
                $cardDetails['currency_code']=$currency; 
                $cardDetails['creditCardNumber'] = $arg['card_number'];
                $padDateMonth = str_pad($arg['exp_month'], 2, '0', STR_PAD_LEFT);
                $expDateYear =urlencode( $arg['exp_year']);      
                $cardDetails['expDate'] = $padDateMonth.$expDateYear;
                $cardDetails['cvv'] = $arg['cvv'];
                $cardDetails['cardName'] = urlencode($arg['card_name']);
                $cardDetails['cardEmail'] = urlencode($arg['email']);
                $resArray = $payPalPro->authorizeCard($cardDetails);
                $ack = strtoupper($resArray['ACK']);
                if($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING'){
                    $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);
                    if(isset($arg['ppv']) && $arg['ppv'] >0){
                        $res['isSuccess'] = 1;
                        $res['card']['status'] = 'approved';
                        $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                    }else{
                        $resArr = self::processTransaction($cardDetails,$arg);
                        $ack = strtoupper($resArr['ACK']);
                        if($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING'){
                            $res['isSuccess'] = 1;
                            $res['card']['status'] = $resArr['PROFILESTATUS'];
                            $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                            $res['card']['invoice_id'] = $resArr['CORRELATIONID'];
                            $res['card']['profile_id'] = $resArr['PROFILEID'];
                            $profileDetails = self::profileDetails($resArr['PROFILEID']);
                            $res['card']['card_type'] = $profileDetails['CREDITCARDTYPE'];
                        }else{
                            $res['isSuccess'] = 0;
                            $res['processtransaction'] = 1;
                            $res['response_text'] = $resArr['L_LONGMESSAGE0'];
                        }
                    }
                }else{
                    $res['isSuccess'] = 0;
                    $res['cardauthorization'] = 1;
                    $res['response_text'] = $resArray['L_LONGMESSAGE0'];
                }
            }else {
                $res['isSuccess'] = 0;
                $res['Code'] = 55;
                $res['Message'] = 'We are not able to process your credit card. Please try another card.';
            }
            return json_encode($res);
        }
    }
    
    /**
     * Process the payment
     * @return transaction details
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processTransaction($arg = array(),$reqst = array())
    {
        $payPalPro = self::initializePaymentGateway();
        $arg['paymentAction'] = 'Sale';
        $arg['profileStartDate'] = urlencode(date('Y-m-d h:i:s'));
	$arg['billingPeriod'] = urlencode($reqst['billingPeriod']);
	$arg['billingFreq'] = urlencode($reqst['billingFreq']);
	$arg['amount'] = $reqst['amount'];
        $arg['currency_code'] = $reqst['currency_code'];
        $arg['initAmt'] = 0.00;
        
	$arg['cardName'] = urlencode($reqst['card_name']);
	$arg['cardEmail'] = urlencode($reqst['email']);
        
        $arg['trialBillingPeriod'] = urlencode($reqst['trialBillingPeriod']);
        $arg['trialBillingFreq'] = urlencode($reqst['trialBillingFreq']);
        $arg['trialAmount'] = urlencode($reqst['trialAmount']);
        $arg['trialTotalBillingCycles'] = urlencode($reqst['trialTotalBillingCycles']);
        
	$arg['failedInitAmtAction'] = urlencode("ContinueOnFailure");
	$arg['desc'] = urlencode("Recurring ".$reqst['currency_symbol'].$reqst['amount']);
	$arg['autoBillAmt'] = urlencode("AddToNextBilling");
	$arg['profileReference'] = urlencode("Muvi");
        $resArray = $payPalPro->doPayment($arg);
        return $resArray;
    }
    
    /**
     * Get recurring profile details
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function profileDetails($profile_id)
    {
        $payPalPro = self::initializePaymentGateway();
        $resArray = $payPalPro->paymentsProfileDetails($profile_id);
        return $resArray;
    }
    
    /**
     * 
     * Cancel Customer's Recurring PayPal Pro Account
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null) {
        $user_profile_id = $usersub->profile_id;
        $payPalPro = self::initializePaymentGateway();
        $data = '';
        if (isset($user_profile_id) && $user_profile_id != '') {
            try {
                $data = $payPalPro->manageProfileStatus($user_profile_id, 'Cancel', 'Profile was Cancelled!'); 
            } catch (Exception $e) {
                
            }
        }        
        return $data;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processTransactions($arg = array())
    {
        $payPalPro = self::initializePaymentGateway();
        $data = array();
        $res = array();
        
        if(isset($arg['havePaypal']) && $arg['havePaypal']){
            $arg['billingType'] = 'MerchantInitiatedBilling ';
            $arg['paymentAmount'] = $arg['amount'];
            $resArray = $payPalPro->CallShortcutExpressCheckout($arg);
            if($resArray['TOKEN']){
               $payPalPro->RedirectToPayPal($resArray["TOKEN"]);
            }
        }else{
            $amount = $arg['amount'];
            $currency = strtoupper($arg['currency_code']);
            
            if (intval($this->IS_CURRENCY_CONVERSION['paypalpro']) && isset($currency) && trim($currency) != 'USD') {
                $amount = $arg['dollar_amount'] = Yii::app()->billing->currencyConversion($currency, 'USD', $amount);
                $currency = 'USD';
                $data['currency_id'] = 153;
            }
            
            $padDateMonth = str_pad($arg['exp_month'], 2, '0', STR_PAD_LEFT);
            $expDateYear =urlencode( $arg['exp_year']);
            $data['paymentAction'] = urlencode("Sale");
            $data['amount'] = urlencode($amount);
            $data['creditCardNumber'] = urlencode($arg['card_number']);
            $data['expDate'] = urlencode($padDateMonth.$expDateYear);
            $data['cvv'] = urlencode($arg['cvv']);
            $data['currencyCode'] = urlencode($currency);
            $resArray = $payPalPro->doSinglePayment($data);
            $ack = strtoupper($resArray['ACK']);
            if($ack == 'SUCCESS'){
                $res['is_success'] = 1;
                $res['transaction_status'] = $resArray['ACK'];
                $res['invoice_id'] = $resArray['CORRELATIONID'];
                $res['order_number'] = $resArray['TRANSACTIONID'];
                $res['amount'] = $resArray['AMT'];
                $res['dollar_amount'] = @$arg['dollar_amount'];
                $res['response_text'] = json_encode($resArray);
            }else {
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = json_encode($resArray);
            }
            return $res;
        }
    }
    
    /**
     *
     * Get Customer's Profile Details
     * @return payPalPro Object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function paymentTransaction($user)
    {
        $payPalPro = self::initializePaymentGateway();
        $resArray = $payPalPro->GetShippingDetails( $user['token'] );
        return $resArray;
    }
    
    /**
     *
     * Create Customer's Recurring Profile
     * @return payPalPro Object (Create a Recurring Payment Profile)
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function paymentTransactions($user = array())
    {
        $payPalPro = self::initializePaymentGateway();
        
        $amount = $user['charged_amount'];
        $trial_amount = $user['trial_amount'];
        $currency = strtoupper($user['currency_code']);

        if (intval($this->IS_CURRENCY_CONVERSION['paypalpro']) && isset($currency) && trim($currency) != 'USD') {
            $amount = Yii::app()->billing->currencyConversion($currency, 'USD', $amount);
            $trial_amount = Yii::app()->billing->currencyConversion($currency, 'USD', $trial_amount);
            $currency = 'USD';
            $user['currency_id'] = 153;
            $user['currency_symbol'] = "$";
        }
        $user['charged_amount'] = $amount;
        $user['trial_amount'] = $trial_amount;
        $user['currency_code'] = $currency;
        $user['currency'] = $currency;
        //print "<pre>";print_r($user);exit;
        $resArray = $payPalPro->CreateRecurringPaymentsProfile($user['token'],$user['payerid'], $user['email'], $user['plan_desc'], $user['recurrence'], $user['frequency'], $user['charged_amount'], $user['currency'], $user['trialperiod'], $user['trialfrequency'], $user['trialtotalcysles'], $user['trial_amount']);
        return $resArray;
    }
    
    /**
     *
     * Update Customer's Recurring Profile card details
     * @return payPalPro Object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function updateCard($arg = array(),$user = array())
    {
        $payPalPro = self::initializePaymentGateway();
        $cardDetails = array();
        $cardDetails['paymentAction'] = 'Authorization';
        $cardDetails['creditCardNumber'] = $arg['card_number'];
        $padDateMonth = str_pad($arg['exp_month'], 2, '0', STR_PAD_LEFT);
        $expDateYear =urlencode( $arg['exp_year']);      
        $cardDetails['expDate'] = $padDateMonth.$expDateYear;
        $cardDetails['cvv'] = $arg['cvv'];
        $cardDetails['cardName'] = urlencode($arg['card_name']);
        $cardDetails['profileID'] = $user->profile_id;
        $resArray = $payPalPro->UpdateCardDetails($cardDetails);
        $profileDetails = self::profileDetails($user->profile_id);
        $resArray['card']['card_type'] = $profileDetails['CREDITCARDTYPE'];
        return $resArray;
    }
    
    function sampleIntegrationTransaction($user = array())
    {
        $payPalPro = self::initializePaymentGateway();
        if (isset($user['card_number']) && !empty($user['card_number'])) {
            if(isset($user['dprp_status']) && !$user['dprp_status']){
                $tranData = self::processTransactions($user);
                if($tranData['isSuccess']){
                    $res['isSuccess'] = 1;
                }else{
                    $res['isSuccess'] = 0;
                    $res['error_message'] = (isset($tranData['L_LONGMESSAGE0']) && trim($tranData['L_LONGMESSAGE0'])) ? trim($tranData['L_LONGMESSAGE0']) : "Some internal error found";
                    $res['response'] = json_encode($tranData);
                }
            }else{
                $cardDetails = array();
                $cardDetails['paymentAction'] = 'Authorization';
                $cardDetails['trxType'] = 'A';
                $cardDetails['amount'] = $user['amount'];
                $cardDetails['creditCardNumber'] = $user['card_number'];
                $padDateMonth = str_pad($user['exp_month'], 2, '0', STR_PAD_LEFT);
                $expDateYear =urlencode( $user['exp_year']);      
                $cardDetails['expDate'] = $padDateMonth.$expDateYear;
                $cardDetails['cvv'] = $user['cvv'];
                $cardDetails['cardName'] = urlencode($user['card_name']);
                $cardDetails['cardEmail'] = urlencode($user['email']);
                $user['billingPeriod'] = urlencode('Month');
                $user['billingFreq'] = urlencode('1');
                $user['trialBillingPeriod'] = urlencode('Month');
                $user['trialBillingFreq'] = urlencode('1');
                $user['trialAmount'] = urlencode('0.00');
                $user['trialTotalBillingCycles'] = urlencode('1');
                $resArray = $payPalPro->authorizeCard($cardDetails);
                $ack = strtoupper($resArray['ACK']);
                if($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING'){
                    $resData = self::processTransaction($cardDetails,$user);
                    
                    $ack = strtoupper($resData['ACK']);
                    if($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING'){
                        $profile['profile_id'] = $resData['PROFILEID'];
                        $profile = (object)$profile;
                        $cancelData = self::cancelCustomerAccount($profile);
                        if($cancelData['ACK'] == 'Success'){
                            $res['isSuccess'] = 1;
                        }
                        $res['isSuccess'] = 1;
                    }else{
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $resData['L_LONGMESSAGE0'];
                        $res['response'] = json_encode($resData);
                    }
                }else{
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $resArray['L_LONGMESSAGE0'];
                        $res['response'] = json_encode($resArray);
                }
            }
        }else {
            $res['isSuccess'] = 0;
            $res['error_message'] = 'We are not able to process your credit card. Please try another card.';
        }
        
        return $res;
    }
    
    function billOutstandingAmount($user_profile_id)
    {
        $payPalPro = self::initializePaymentGateway();
        $data = '';
        if (isset($user_profile_id) && $user_profile_id != '') {
            try {
                $data = $payPalPro->billOutstandingAmount($user_profile_id); 
            } catch (Exception $e) {
                return $e;
            }
        }        
        return $data;
    }
    
    function updateOutstandingBillAmount($user_profile_id)
    {
        $data = array();
        $amt = 0.00;
        $payPalPro = self::initializePaymentGateway();
        $data = $payPalPro->updateOutstandingBillAmount($user_profile_id,$amt);
        return $data;
    }
    
    function updateAutoBillOutAmt($user_profile_id)
    {
        $data = array();
        $payPalPro = self::initializePaymentGateway();
        $data = $payPalPro->updateAutoBillOutAmt($user_profile_id); 
        return $data;
    }
    
    function processTransactionPpv($arg = array()) 
    {
        $payPalPro = self::initializePaymentGateway();
        if(isset($arg['Order']['have_paypal']) && $arg['Order']['have_paypal']){
            $resArray = $payPalPro->CallShortcutExpressCheckout ($arg['Order']['theTotal'], $arg['Order']['email'], $arg['Order']['currency'], $arg['Order']['paymentType'], $arg['Order']['description'], $arg['Order']['returnURL'], $arg['Order']['cancelURL'], $arg['Order']['have_paypal'], $arg['Order']['address']);
            if($resArray['TOKEN']){
               $payPalPro->RedirectToPayPal($resArray["TOKEN"] );
            }
        }
    }
    
    /**
     * 
     * Get Details of transaction
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCheckoutDetails($token)
    {
        $payPalPro = self::initializePaymentGateway();
        $resArray = $payPalPro->GetExpressCheckoutDetails($token);
        return $resArray;
    }
    
    /**
     * 
     * Do payment
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processDoPayment($paymentInfo = array())
    {
        $payPalPro = self::initializePaymentGateway();
        $resArray = $payPalPro->DoExpressCheckoutPayment($paymentInfo);
        return $resArray;
    }
    
    /**
     * 
     * Update user profile
     * @return object
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function updatePaymentProfile($user = array(),$sub = array(),$email)
    {
        $payPalPro = self::initializePaymentGateway();
        $resArray = $payPalPro->updateUserDetails($sub->profile_id,$email);
        if(isset($resArray) && strtoupper($resArray['ACK']) == 'SUCCESS'){
            $resArray['isSuccess'] = 1;
        }else{
            $resArray['isSuccess'] = 0;
        }
        return $resArray;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processTransactionsPCI($arg = array())
    {
        $payPalPro = self::initializePaymentGateway();
        $data = array();
        $res = array();
        
        if(isset($arg['havePaypal']) && $arg['havePaypal']){
            $arg['landing'] = 1;
        }else{
            $arg['landing'] = 0;
        }
        $arg['billingType'] = 'MerchantInitiatedBilling';
        $arg['paymentAmount'] = $arg['amount'];
        $resArray = $payPalPro->CallShortcutExpressCheckoutPCI($arg);
        return $resArray;
    }
    /**
     * 
     * Get tranasction details
     * @param array transaction id as $txn_id
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function getPCICardTransactionDetails($txn_id) {
        $payPalPro = self::initializePaymentGateway();
        $resArray = $payPalPro->getPCICardTransactionDetails($txn_id);
        return $resArray;
    }
    /**
     * 
     * Create token for Pay using paypal (Apps)
     * @param array $user_data
     * @return array 
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function createToken($user_data) {
        $payPalPro = self::initializePaymentGateway();
        $user_data['currency_code'] = strtoupper($user_data['currency_code']);
        $amount = $user_data['amount'];
        $currency = strtoupper($user_data['currency_code']);

        $user_data['paymentAmount']=$amount; 
        $user_data['currencyCodeType']=$currency; 
        $user_data['currency_code']=$currency; 
        $user_data['billingType'] = 'MerchantInitiatedBilling';
        
        $resArray = $payPalPro->CallShortcutExpressCheckout ($user_data);
        if($this->IS_LIVE_API_PAYMENT_GATEWAY['paypalpro'] != 'sandbox'){
            $PAYPAL_URL = 'https://www.paypal.com/webscr&cmd=_express-checkout&token=';
        }else{
            $PAYPAL_URL = 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=';
        }
        
        $res = array();
        if(isset($resArray) && (strtoupper($resArray['ACK']) == "SUCCESS" || strtoupper($resArray['ACK']) == 'SUCCESSWITHWARNING')){
            $res['ACK'] = 'Success';
            $res['TOKEN'] = $resArray['TOKEN'];
            $res['CORRELATIONID'] = $resArray['CORRELATIONID'];
            $res['REDIRECTURL'] = isset($PAYPAL_URL)&&trim($PAYPAL_URL)?$PAYPAL_URL.$resArray['TOKEN']:'';
        }else{
            $res['isSuccess'] = 0;
            $res['error_message'] = $resArray['L_LONGMESSAGE0'];
            $res['response'] = json_encode($resArray);
        }
        return $res;
    }
}