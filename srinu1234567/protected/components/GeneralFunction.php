<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class GeneralFunction extends AppComponent {

    var $image;

    public function MainBanners($template_id) {
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $theme = $controller->studio->parent_theme;
        $is_new = ' AND t.is_new = 0';
        $bannerStyle = Yii::app()->db->createCommand()
            ->select("*")
            ->from('studio_banner_style')
            ->where('studio_id=:studio_id', array(':studio_id' => $studio_id))
            ->queryRow();
        if ($theme == 'traditional-byod') {
            if (isset($bannerStyle) && !empty($bannerStyle)) {
                $is_new = ' AND t.is_new = 1';
            }
        }
        // query criteria
        $criteria = new CDbCriteria();
        // join Post model (one for fetching data, the other for filtering)
        $criteria->with = array(
            'banners' => array(// this is for fetching data
                'together' => false,
                'condition' => 'banners.is_published = :is_published AND banners.studio_id = :studio_id',
                'params' => array(
                    ':is_published' => 1,
                    ':studio_id' => $studio_id,
                ),
                'order' => 'id_seq ASC'
            ),
        );
        // compare title
        if (Yii::app()->custom->hasVideoBanner($template_id) == 1) {
            $cond = ' AND t.studio_id = ' . $studio_id;
        } else {
            $cond = ' AND t.studio_id = 0';
        }
        $criteria->condition = 't.template_id = ' . $template_id . $cond . $is_new;
        $criteria->order = 't.id ASC';
        $banner_sections = BannerSection::model()->findAll($criteria);
        $unsigned_path = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        $language_id = $controller->language_id;
        //Banner Text
        $bantxt = $this->getBannerText($studio_id, $language_id);
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        foreach ($banner_sections as $section) {
            $thumb = Yii::app()->common->getThumbBannerDimension($section->id);
            $thumb_width = $thumb['width'];
            $thumb_height = $thumb['height'];

            $bans = array();
            $join_btn = '';
            $join_btn_text = '';
            if ($bantxt->show_join_btn > 0 && ($user_id < 1 && !$controller->is_subscribed)) {
                $join_btn_text = (isset($bantxt->join_btn_txt) && strlen($bantxt->join_btn_txt) > 1) ? $bantxt->join_btn_txt : 'JOIN NOW';
                $join_btn = '<a href="' . Yii::app()->getBaseUrl(true) . '/user/register" class="joinnow">' . $join_btn_text . '</a>';
            }
            if (empty($bannerStyle)) {
                if ($theme == "traditional-byod") {
                    $banner_full_path = Yii::app()->getBaseUrl(true) . '/images/dummy_banner_traditional.jpg';
                    $bannercontent = '';
                } else {
                    $banner_full_path = Yii::app()->getBaseUrl(true) . '/images/dummy_banner.jpg';
                    $bannercontent = html_entity_decode($bantxt->bannercontent);
                }
            } else {
                $banner_full_path = '';
            }

            if (count($section->banners) > 0) {
                foreach ($section->banners as $banner) {
                    if ($theme == "traditional-byod") {
                        if (isset($banner->banner_text)) {
                            $bannercontent = $banner->banner_text;
                        } else {
                            $bannercontent = '';
                        }
                    }
                    $banner_id = $banner->id;
                    $bannerproperties = Yii::app()->db->createCommand()
                        ->select("*")
                        ->from('studio_banner_properties')
                        ->where('studio_id=:studio_id and banner_id=:banner_id', array(':studio_id' => $studio_id, ':banner_id' => $banner_id))
                        ->QueryAll();
                    if ($banner->banner_type == '0')
                        $full_path = $banner->video_remote_url;
                    else
                        $full_path = $unsigned_path . "/system/studio_banner/" . $studio_id . "/original/" . $banner->image_name;
                    $full_path_thumb = $unsigned_path . "/system/studio_banner/" . $studio_id . "/studio_thumb/" . $banner->image_name;
                    $bans[] = array(
                        'id' => $banner->id,
                        'title' => $banner->title,
                        'image_name' => $banner->image_name,
                        'banner_text' => $bannercontent,
                        'join_btn' => $join_btn,
                        'join_btn_text' => $join_btn_text,
                        'url_text' => $banner->url_text,
                        'is_published' => $banner->is_published,
                        'banner_section' => $banner->banner_section,
                        'banner_type' => $banner->banner_type,
                        'banner_full_path' => $full_path,
                        'banner_thumb_full_path' => $full_path_thumb,
                        'video_placeholder' => $banner->video_placeholder_img,
                        'video_placeholder_thumb' => $banner->video_placeholder_thumb_img,
                        'video_url' => $banner->video_remote_url,
                        'bannerProperties' => $bannerproperties,
                        'banner_style' => $bannerStyle,
                        'thumb_width' => $thumb_width,
                        'thumb_height' => $thumb_height
                    );
                }
            }else {
                $bans[] = array(
                    'id' => '',
                    'title' => '',
                    'image_name' => $banner->image_name,
                    'banner_text' => $bannercontent,
                    'join_btn' => $join_btn,
                    'join_btn_text' => $join_btn_text,
                    'url_text' => '',
                    'is_published' => 1,
                    'banner_section' => $banner->banner_section,
                    'banner_type' => 1,
                    'banner_full_path' => $banner_full_path,
                    'banner_thumb_full_path' => '',
                    'video_placeholder' => '',
                    'video_placeholde_thumb' => '',
                    'video_url' => '',
                    'bannerProperties' => '',
                    'banner_style' => $bannerStyle,
                    'thumb_width' => $thumb_width,
                    'thumb_height' => $thumb_height
                );
            }
            $return[] = array(
                'section_id' => $section->id,
                'banners' => $bans,
                'join_btn' => $join_btn,
                'banner_text' => $bannercontent,
            );
        }

        return $return;
    }

    public function getBannerText($studio_id, $language_id) {
        $bantxt = Yii::app()->db->createCommand()
                ->select('*')
                ->from('banner_text')
                ->where("studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM banner_text WHERE studio_id={$studio_id} AND language_id={$language_id}))")
                ->setFetchMode(PDO::FETCH_OBJ)->queryRow();
        return $bantxt;
    }

    public function getUserData($user_id) {
        $user = SdkUser::model()->findByPk($user_id);
        $controller = Yii::app()->controller;
        $user_picture = $controller->getProfilePicture($user->id, 'profilepicture', 'thumb');
        $data = array(
            'email' => $user->email,
            'display_name' => $user->display_name,
            'user_picture' => $user_picture,
            'created_date' => Yii::app()->common->phpDate($user->created_date),
            'facebook_id' => $user->facebook_id,
            'twitter_id' => $user->twitter_id,
        );
        return $data;
    }

    public function getPlanDetails($plan_id) {
        $plan = SubscriptionPlans::model()->findByPk($plan_id);
        $data = array(
            'plan_name' => $plan->name,
            'price' => $plan->price,
            'status' => $plan->status,
        );
        return $data;
    }

    public function getTransactionDetails($transaction_id, $user_id = 0, $is_app = 0) {
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : $user_id;
        $transaction = Transaction::model()->findByPk($transaction_id);

        if (intval($transaction->ppv_subscription_id)) {
            $plan = Yii::app()->common->getPPVPlanDetails($transaction->plan_id);
            $price = Yii::app()->common->getPPVPrices($transaction->plan_id, $transaction->currency_id);
            $plan_price = Yii::app()->common->formatPrice($transaction->amount, $transaction->currency_id);
            $plan_name = '';

            if ($transaction['transaction_type'] == 2) {
                $plan_name = 'Pay-per-view: ' . $plan['title'];

                $content_access_period = '';

                $access_period = @$plan['access_period'];
                if (trim($access_period)) {
                    $content_access_period = $access_period;
                }

                if (trim($content_access_period)) {
                    $plan_name = $plan_name . ' (' . $plan_price . '/' . $content_access_period . ')';
                } else {
                    $plan_name = $plan_name;
                }
                if ($is_app == 1) {
                    $plan_name = 'Pay-per-view: ' . $plan['title'];
                    $recurrence = $validity['validity_period'];
                }
            } else if ($transaction['transaction_type'] == 3) {
                $plan_name = 'Pre-order: ' . $plan['title'];
            } else if ($transaction['transaction_type'] == 5) {
                $plantimeframe = Yii::app()->common->getPPVPlantimeframeDetails($transaction->ppv_subscription_id);
                $plan_name = 'Pay-per-view Bundle: ' . $plan['title'];
                $validitydays = $plantimeframe['title'];
                $plan_name = $plan_name . ' (' . $validitydays . ')';
            }
        } else if (intval($transaction->subscriptionbundles_id)) {
            if ($transaction['transaction_type'] == 7) {
                $subscriptionBundlesplanModel = new SubscriptionBundlesPlan();
                $subscription_bundles_plan_name = $subscriptionBundlesplanModel->findByPk($transaction->plan_id);
                $plan_name = $subscription_bundles_plan_name->name;
                $plan_recurrence = $subscription_bundles_plan_name->recurrence;
                $plan_pricing = SubscriptionBundlesPricing::model()->findByAttributes(array('subscription_plan_id' => $transaction->plan_id, 'currency_id' => $transaction->currency_id));
                $plan_price = Yii::app()->common->formatPrice($plan_pricing->price, $transaction->currency_id);
                $plan_name = $plan_name . ' (' . $plan_price . '/' . $plan_recurrence . ')';
            }
        } else {
            $plan = Yii::app()->common->getPlanDetails($transaction->plan_id);
            $plan_name = $plan['name'];
            $plan_pricing = SubscriptionPricing::model()->findByAttributes(array('subscription_plan_id' => $transaction->plan_id, 'currency_id' => $transaction->currency_id));
            $plan_price = Yii::app()->common->formatPrice($plan_pricing->price, $transaction->currency_id);
            $plan_name = $plan_name . ' (' . $plan_price . '/' . $plan['recurrence'] . ')';
            if ($is_app == 1) {
                $plan_name = $plan['name'];
                $recurrence = $plan['recurrence'];
            }
        }

        if ($is_app == 0) {
            $data = array(
                'id' => $transaction->id,
                'payment_method' => $transaction->payment_method,
                'transaction_status' => $transaction->transaction_status,
                'transaction_date' => Yii::app()->common->MonthTextDate($transaction->transaction_date),
                'amount' => $plan_price,
                'invoice_id' => $transaction->invoice_id,
                'order_number' => $transaction->order_number,
                'fullname' => $transaction->fullname,
                'address1' => $transaction->address1,
                'city' => $transaction->city,
                'phone' => $transaction->phone,
                'zip' => $transaction->zip,
                'country' => Yii::app()->common->getCountry($transaction->country),
                'state' => Yii::app()->common->getState($transaction->state, $transaction->country),
                'permalink' => Yii::app()->getBaseUrl(true) . '/user/transaction/' . $transaction_id,
                'user' => self::getUserData($user_id),
                'plan' => $plan,
                'plan_name' => $plan_name,
            );
        }
        if ($is_app == 1) {
            $currency = Currency::model()->findByPk($transaction->currency_id);
            $data = array(
                'id' => $transaction->id,
                'payment_method' => $transaction->payment_method,
                'transaction_status' => $transaction->transaction_status,
                'transaction_date' => Yii::app()->common->MonthTextDate($transaction->transaction_date),
                'amount' => $transaction->amount,
                'currency_symbol' => $currency->symbol,
                'currency_code' => $currency->code,
                'invoice_id' => $transaction->invoice_id,
                'order_number' => $transaction->order_number,
                'fullname' => $transaction->fullname,
                'address1' => $transaction->address1,
                'city' => $transaction->city,
                'phone' => $transaction->phone,
                'zip' => $transaction->zip,
                'country' => Yii::app()->common->getCountry($transaction->country),
                'state' => Yii::app()->common->getState($transaction->state, $transaction->country),
                'permalink' => Yii::app()->getBaseUrl(true) . '/user/transaction/' . $transaction_id,
                'user' => self::getUserData($user_id),
                'plan' => $plan,
                'plan_name' => $plan_name,
                'plan_recurrence' => $recurrence
            );
        }
        return $data;
    }

    public function FeaturedBlockContents($template_id, $section_id = 0) {
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;

        $cond = 't.is_active = 1 AND t.studio_id = ' . $studio_id;
        if ($section_id > 0)
            $cond .= ' AND t.id = ' . $section_id;
        else
            $cond .= ' AND template_id = ' . $template_id;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $sections = FeaturedSection::model()->findAll($criteria);
        $return = array();
        foreach ($sections as $section) {
            $final_content = array();
            $total_contents = 0;
            foreach ($section->contents as $featured) {
                $total_contents++;
                $final_content[] = self::getContentData($featured->movie_id, $featured->is_episode);
            }
            $return[] = array(
                'id' => $section->id,
                'title' => utf8_encode($section->title),
                'total' => $total_contents,
                'contents' => $final_content
            );
        }
        /*
          if($studio_id == 1600){
          print_r($return);
          }
         * 
         */
        return $return;
    }

    public function FeaturedBlockContentsNew($template_id, $sections) {
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        if (!$sections) {
            $limit = '';
            $sections = FeaturedSection::model()->getFeaturedSections($studio_id, $controller->language_id);  
        }
        if ($sections) {
            $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
            $postCldFontPath = Yii::app()->common->getPosterCloudFrontPath($studio_id);
            $cloudFront = Yii::app()->common->connectToAwsCloudFront($studio_id);
            $return = array();
            $is_payment_gateway_exist = 0;
            $payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($payment_gateway) && !empty($payment_gateway)) {
                $is_payment_gateway_exist = 1;
            }
            foreach ($sections as $section) {
                if ($section['content_type'] == 1) {
                    $finalContents = Yii::app()->custom->SectionProducts($section['id'], $studio_id);
                } else {
                    if (Yii::app()->custom->LimitedContentHomePageData() == 1) {
                        $finalContents = self::mandatoryData($section['id'], $studio_id, $is_payment_gateway_exist, $postCldFontPath, $cloudFront);
                    } else {
                        $finalContents = self::FormatContentData($section['id'], $studio_id, $is_payment_gateway_exist, $postCldFontPath, $cloudFront);
                    }
                }
                $return[] = array(
                    'title' => utf8_encode($section['title']),
                    'total' => count($finalContents),
                    'content_type' => $section['content_type'],
                    'contents' => $finalContents
                );
            }
            return $return;
        } else {
            return false;
        }
    }

    public function formatReleaseDate($release) {
        $release_date = '';
        if ($release != '' && $release != NULL) {
            if ($release == '1970-01-01')
                $release_date = date('Y');
            else
                $release_date = date('Y', strtotime($release));
        }
        return $release_date;
    }

    public function getStoreLink($studio_id = 0, $is_api = 0) {
        /* $config = new StudioConfig();
          $pgconfig = $config->getconfigvalue('pg_enable');
          if ($pgconfig['config_value'] == 1) {return true;}else{return false;} */
        if (isset($_SESSION['storelink']) && !intval($is_api)) {
            return $_SESSION['storelink'];
        } else {
            $studio_id = (intval($studio_id)) ? $studio_id : Yii::app()->common->getStudiosId();
            $exts = Extension::model()->find(array(
                'condition' => 'name=:name and status=:status',
                'params' => array(':name' => 'Muvi Kart', ':status' => 1),
                'order' => 't.id ASC'
            ));

            $ext = StudioExtension::model()->findByAttributes(array('studio_id' => $studio_id, 'extension_id' => $exts['id']));
            $_SESSION['storelink'] = $ext->status;
            return $ext->status;
        }
    }

    function getTotalTransactionsOfStudioForMuviKart($studio_id) {
        $start_date = Date('Y-m-d', strtotime("-1 Months +1 Days"));
        $end_date = Date('Y-m-d');

        $sql = "SELECT t.currency_id, SUM(t.amount) AS amount, c.code FROM transactions t LEFT JOIN currency c ON (t.currency_id=c.id) WHERE t.studio_id = {$studio_id} AND t.transaction_type=4 AND (DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY t.currency_id ORDER BY FIND_IN_SET(c.code,'USD') DESC, t.currency_id ASC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        $price = array();
        $toal_amount_in_usd = 0.00;
        if (isset($data) && !empty($data)) {

            foreach ($data as $key => $value) {
                $amount = $value['amount'] = number_format((float) ($value['amount']), 2, '.', '');
                $price['amount_in_currency'][$key] = $value;

                if ($value['code'] == 'USD') {
                    $price['amount_in_currency'][$key]['usd_amount'] = $amount;
                } else if (trim($value['code'])) {
                    $usd_amount = Yii::app()->billing->currencyConversion($value['code'], 'USD', $amount);
                    $amount = number_format((float) ($usd_amount), 2, '.', '');
                    $price['amount_in_currency'][$key]['usd_amount'] = $amount;
                }

                $toal_amount_in_usd = number_format((float) ($toal_amount_in_usd + $amount), 2, '.', '');
            }

            $price['total_amount'] = $toal_amount_in_usd;
        }

        return $price;
    }

    function getMuviKartCostForStudio($studio_id = 0, $total_transactions = array()) {
        $sql = "SELECT price FROM muvi_kart_pricing WHERE studio_id={$studio_id} OR studio_id=0 ORDER BY studio_id DESC LIMIT 0, 1";
        $res = Yii::app()->db->createCommand($sql)->queryRow();

        $price = (isset($res['price']) && trim($res['price'])) ? number_format((float) ($res['price']), 2, '.', '') : number_format((float) (0), 2, '.', '');

        $data = array();
        if (abs($price) >= 0.01) {
            $data['muvi_kart_price_of_transaction'] = $price;
            $data['muvi_kart_total_transactions'] = $total_transactions['total_amount'];
            $data['muvi_kart_price'] = number_format((float) ($total_transactions['total_amount'] * $price / 100), 2, '.', '');
        }

        return $data;
    }

    public function getFeaturedProduct() {
        $studio_id = Yii::app()->common->getStudiosId();
        $cond = " is_deleted=0 AND status != 2 AND studio_id={$studio_id} AND feature=1";
        $productlist = Yii::app()->db->createCommand("SELECT * FROM pg_product WHERE " . $cond)->queryAll();
        if (!empty($productlist)) {
            return $productlist;
        } else {
            return array();
        }
    }

    public function getMainMenu() {
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $controller = Yii::app()->controller;
        /* To check if subcatery to show in menu */
        $config = new StudioConfig();
        $subcat_config = $config->getconfigvalue('subcategory_enabled');

        //Get the current menu Items
        //$current = end(explode('/', Yii::app()->urlManager->parseUrl(Yii::app()->request)));   
        $current = explode('/', Yii::app()->request->url);
        $current = @$current[1];

        $mainmenu = Menu::model()->find('studio_id=:studio_id AND position=:position', array(':studio_id' => $studio_id, ':position' => 'top'));
        $menu = '';
        //if (count($mainmenu) > 0) {
        $menu_id = @$mainmenu->id;
        $language_id = $controller->language_id;
        $sql = "SELECT * FROM menu_items WHERE studio_id={$studio_id} AND (menu_id=" . $menu_id . ") AND parent_id=0 AND is_hidden=0 AND (language_id={$language_id} OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id={$studio_id}  AND is_hidden=0 AND (menu_id=" . $menu_id . " OR menu_id != '') AND parent_id=0 AND language_id={$language_id})) ORDER BY id_seq ASC";
        $topmenuitems = Yii::app()->db->createCommand($sql)->queryAll();
        if (count($topmenuitems) > 0) {
            $menu.= '<ul class="nav navbar-nav" id="main-top-menu">';
            foreach ($topmenuitems as $topmenuitem) {
                $parent_menu_item_id = $topmenuitem['id'];
                if ($topmenuitem['language_parent_id'] > 0) {
                    $parent_menu_item_id = $topmenuitem['language_parent_id'];
                }
                $sql = "SELECT * FROM menu_items WHERE studio_id={$studio_id} AND menu_id=" . $menu_id . " AND parent_id=" . $parent_menu_item_id . " AND (language_id={$language_id} OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id={$studio_id} AND menu_id=" . $menu_id . " AND parent_id=" . $parent_menu_item_id . " AND language_id={$language_id})) ORDER BY id_seq ASC";
                $topmenus = Yii::app()->db->createCommand($sql)->queryAll();

                $allmenus = array();
                $allmenus[] = $topmenuitem['permalink'];
                $tplink = explode('//', $topmenuitem['permalink']);
                $tplink1 = explode('/', @$tplink[1]);
                $tplink = str_replace(@$tplink1[0], '', @$tplink[1]);

                if (count($topmenus) > 0) {
                    //Code for active class
                    foreach ($topmenus as $topmenu) {
                        $allmenus[] = $topmenu['permalink'];
                    }
                }

                //Adding subcategories to Menu
                $subcats = array();
                if (@$subcat_config['config_value'] == 1 && $topmenuitem['link_type'] == 0) {
                    $cat_qry = "SELECT id,binary_value FROM content_category WHERE permalink = '" . $topmenuitem['permalink'] . "' AND studio_id=" . $studio_id;
                    $cat = Yii::app()->db->createCommand($cat_qry)->queryAll();
                    foreach ($cat as $ct) {
                        $binary_value = $ct['binary_value'];
                        $subcat_qry = "SELECT * FROM content_subcategory WHERE studio_id=" . $studio_id . "  AND FIND_IN_SET(" . $ct['id'] . ",category_value) ORDER BY subcat_name ASC";
                        $subcats = Yii::app()->db->createCommand($subcat_qry)->queryAll();
                    }
                }

                if (in_array($current, $allmenus) || (Yii::app()->request->url == $tplink) && strpos($topmenuitem['permalink'], Yii::app()->controller->studio->domain) > -1)
                    $active = 'active';
                else
                    $active = '';
                if (count($topmenus) > 0 || count($subcats) > 0)
                    $menu.= '<li class="dropdown ' . $active . '">';
                else
                    $menu.= '<li class="' . $active . '">';

                $menu_link = $controller->siteurl . '/' . $topmenuitem['permalink'];
                $menu_cls = ' class="dropdown-toggle js-activated disabled"';
                if ($topmenuitem['link_type'] == 2 && $topmenuitem['permalink'] != '#' && strpos($topmenuitem['permalink'], $controller->siteurl) > -1)
                    $menu.= '<a href="' . $topmenuitem['permalink'] . '" ' . $menu_cls . '>' . $topmenuitem['title'];
                else if ($topmenuitem['link_type'] == 2 && $topmenuitem['permalink'] != '#')
                    $menu.= '<a href="' . $topmenuitem['permalink'] . '" target="_blank" ' . $menu_cls . '>' . $topmenuitem['title'];
                else
                    $menu.= '<a href="' . $menu_link . '" ' . $menu_cls . '>' . $topmenuitem['title'];
                if (count($topmenus) > 0 || count($subcats) > 0)
                    $menu.= '<span class="caret"></span>';
                $menu.= '</a>';

                //Adding subcategories to Menu
                if (@$subcat_config['config_value'] == 1 && count($subcats) > 0) {
                    $menu.= '<ul class="dropdown-menu">';
                    foreach ($subcats as $subcat) {
                        $menu_link = $controller->siteurl . '/' . $topmenuitem['permalink'] . '/' . $subcat['permalink'];
                        $menu.= '<li><a href="' . $menu_link . '">' . $subcat['subcat_name'] . '</a></li>';
                    }
                    $menu.= '</ul>';
                }

                if (count($topmenus) > 0) {
                    $menu.= '<ul class="dropdown-menu">';
                    foreach ($topmenus as $topmenu) {
                        $menu_link = $controller->siteurl . '/' . $topmenu['permalink'];
                        $menu.= '<li>';
                        if ($topmenu['link_type'] == 2 && $topmenu['permalink'] != '#' && strpos($topmenu['permalink'], $controller->siteurl) > -1)
                            $menu.= '<a href="' . $topmenu['permalink'] . '">' . $topmenu['title'] . '</a>';
                        else if ($topmenu['link_type'] == 2 && $topmenu['permalink'] != '#')
                            $menu.= '<a href="' . $topmenu['permalink'] . '" target="_blank">' . $topmenu['title'] . '</a>';
                        else
                            $menu.= '<a href="' . $menu_link . '">' . $topmenu['title'] . '</a>';
                        $menu.= '</li>';
                    }
                    $menu.= '</ul>';
                }

                $menu.= '</li>';
            }
            $menu.= '</ul>';
        }
        //}
        return $menu;
    }

    public function getUserMenu() {
        $controller = Yii::app()->controller;
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $hide_signup = Yii::app()->custom->hideSignup($studio_id);
        $theme = $controller->studio->parent_theme;
        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
        $monetization_menu = self::monetizationMenuSetting($studio_id);
        if ($controller->studio->need_login == 1) {
            $menu = '<ul id="user_menu">';
            if ($user_id > 0) {
                $is_subscribed = Yii::app()->common->isSubscribed($user_id);
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
                $user = Yii::app()->common->getSDKUserInfo($user_id);
                $profile_image = $controller->getProfilePicture($user->id, 'profilepicture', 'thumb');
                $display_name = isset(Yii::app()->user->display_name) ? Yii::app()->user->display_name : '';
                $cards = SdkCardInfos::model()->findAllByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id), array('order' => 'is_cancelled ASC'));
                $menu.= '<li class="dropdown">';
                $menu.= '<a data-toggle="dropdown" class="dropdown-toggle" href="#"><img class="img-circle user-icon" alt="" title="' . $display_name . '" src="' . $profile_image . '" /> ' . $display_name . ' <b class="caret"></b></a>';
                $menu.= '<ul class="dropdown-menu">';
                $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/profile">' . $controller->Language['profile'] . '</a></li>';
                if ($controller->add_to_favourite) {
                    $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/favourites">' . $controller->Language['my_favourite'] . '</a></li>';
                }
                if ($controller->watch_history != '0' && ($theme == 'traditional-byod' || $theme == 'modern-byod' || $theme == 'classic-byod')) {
                    $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/watchHistory">' . $controller->Language['watch_history'] . '</a></li>';
                }
                $plans = UserSubscription::model()->UserSubscriptionBundlesPlanExist($studio_id, $user_id);
                $Countplans = count($plans);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    if ($user->is_deleted > 0) {
                        //$menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/reactivate">' . $controller->Language['reactivate'] . '</a></li>';
                    } else {
                        if ($is_subscribed > 0) {
                            //$menu.= '<li><a href="#" data-target="#cancelsubscription" data-toggle="modal">' . $controller->Language['cancel_subscription'] . '</a></li>';
                        } else if ($Countplans == 0) {
                           // $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/activate">' . $controller->Language['activate'] . '</a></li>';
                        }
                    }
                }
                if (($monetization_menu['menu'] & 1) && ($theme !='physical') ) {
                    $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/myplans">' . $controller->Language['my_plan'] . '</a></li>';
                }
                if (!empty($cards)) {
                    if (isset($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($controller->API_ADDONS_OTHERGATEWAY[$gateway_code])) {
                        $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/cardinformation">' . $controller->Language['card_info_other'] . '</a></li>';
                    } else {
                        $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/cardinformation">' . $controller->Language['card_info'] . '</a></li>';
                    }
                }
                if ((isset($plan_payment_gateway) && !empty($plan_payment_gateway))  || ($monetization_menu['menu'] & 128) || ($theme =='physical')) {
                $menu .= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/PurchaseHistory">' . $controller->Language['purchase_history'] . '</a></li>';
                }
                if ($theme == 'traditional-byod' && $controller->studio->mylibrary_activated == 1) {
                    $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/myLibrary">' . $controller->Language['my_library'] . '</a></li>';
                }
                if ($controller->manage_device != '0') {
                    $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/manageDevice">' . $controller->Language['manage_device'] . '</a></li>';
                }
                $menu.= '<li><a href="' . Yii::app()->getBaseUrl(true) . '/user/logout">' . $controller->Language['logout'] . '</a></li>';
                $menu.= '</ul>';
                $menu.= '<li>';
            } else {
                if ($hide_signup == 0) {
                    $menu.= '<li class="notlogin"><a href="' . Yii::app()->getBaseUrl(true) . '/user/register"><i class="fa fa-user"></i> ' . $controller->Language['register'] . '</a></li>';
                }
                $menu.= '<li class="notlogin"><a href="' . Yii::app()->getBaseUrl(true) . '/user/login"><i class="fa fa-lock"></i> ' . $controller->Language['login'] . '</a></li>';
            }
            $menu.= '</ul>';
        } else {
            $menu = '';
        }
        $send_nojson = Yii::app()->custom->sendNoJSONData();
        if ($send_nojson == 1)
            return $menu;
        else
            return json_encode($menu);
    }

    public function getcancelPopUp() {
        $controller = Yii::app()->controller;
        $cancel_reasons = Yii::app()->common->cancelReasons();
        $choose_reason = Yii::app()->controller->Language['choose_reason'];

        $data .= '<div id="showseasonconfirmpopup"  class="modal fade login-popu" data-backdrop="static" data-keyboard="false">';
        $data .= '<div class="modal-dialog">';
        $data .= '<div class="modal-content">';
        $data .= '<div class="modal-header">';
        $data .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $data .= '<h4 class="modal-title">&nbsp;</h4>';
        $data .= '</div>';
        $data .= '<div class="modal-body"><div class="loader loader_confirm" id="loader" style="display: none;"></div>';
        $data .= '<div align="center" style="font-size:36px; color:#009933">' . Yii::app()->controller->Language['thank_you'] . '</div>';
        $data .= '<div align="center" style="font-size:16px;">' . Yii::app()->controller->Language['your_purchase_for'] . '<span id="modalmsg"  style="font-weight:600"></span> ' . Yii::app()->controller->Language['processed_successfully'] . '</div>';
        $data .= '<br><br><div align="center"><button type="button" class="btn btn-primary bold_class" id="dismisShowseasonconfirm">' . Yii::app()->controller->Language['dismiss'] . '</button>&nbsp;&nbsp;<button class="btn btn-primary bold_class" type="button" id="watchnowShowseasonconfirm">' . Yii::app()->controller->Language['watch_now'] . '</button></div><br><br>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';

        $data .= '<div id="cancelsubscription" class="modal fade login-popu">';
        $data .= '<div class="modal-dialog">';
        $data .= '<div class="modal-content">';
        $data .= '<div class="modal-header">';
        $data .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $data .= '<h4 class="modal-title">' . Yii::app()->controller->Language['sorry_to_let_go'] . '</h4>';
        $data .= '</div>';
        $data .= '<div class="modal-body">';
        $data .= '<div class="row-fluid">';
        $data .= '<div class="col-md-12">';
        $data .= '<div id="cancel-loading" class="loader"></div>';
        $data .= '<form id="cancel_form"  class="form-horizontal">';
        $data .= '<input type="hidden" name="user_subscription_plan" id="user_subscription_plan" value="" />';
        $data .= '<div class="form-group">';
        $data .= '<p>' . Yii::app()->controller->Language['tell_reason_for_improve'] . '</p><br />';
        $data .= '</div>';
        foreach ($cancel_reasons as $reason) {
            $data .= '<div class="form-group">';
            $data .= '<label>';
            $data .= '<input type="radio" name="cancelreason" id="reason' . $reason->id . '" value="' . $reason->id . '" class="pull-left" />&nbsp;&nbsp;';
            if (trim($reason->reason) == trim($controller->Original['very_expensive'])) {
                $reason = $controller->Language['very_expensive'];
            } elseif (trim($reason->reason) == trim($controller->Original['other'])) {
                $reason = $controller->Language['other'];
            } else {
                $reason = $reason->reason;
            }
            $data .= $reason;
            $data .= '</label>';
            $data .= '</div>';
            $data .= '<div class="clearfix"></div>';
        }
        $data .= '<div class="form-group"><textarea name="cancel_note" class="form-control" placeholder="' . Yii::app()->controller->Language['text_leavenote_placeholder'] . '"></textarea></div>';
        $data .= '<p class="error" id="cancel_error"></p>';
        $data .= '<div class="form-group ">';
        $data .= '<div class="controls">';
        $data .= '<button type="button" aria-hidden="true" class="btn btn-primary pull-right bold_class" id="cancel_now">' . Yii::app()->controller->Language['cancel_subscription'] . '</button>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '<div class="clearfix"></div>';
        $data .= '</form>';
        $data .= '</div>';
        $data .= '<div class="clearfix"></div>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';

        $data .= '<div id="cancelsuccessPopup" class="modal fade login-popu">';
        $data .= '<div class="modal-dialog">';
        $data .= '<div class="modal-content">';
        $data .= '<div class="modal-header">';
        $data .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $data .= '<h4 class="modal-title">&nbsp;</h4>';
        $data .= '</div>';
        $data .= '<div class="modal-body">';
        $data .= '<p>' . Yii::app()->controller->Language['subscription_canceled_success_redirect_you'] . '</p>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';
        $data .= '</div>';


        $data .= "<script type='text/javascript'>
        $(document).ready(function(){
            //$('#cancel-loading').hide();
            //$('#cancel_error').hide();
            $('#cancel_now').click(function(){
                $('#cancel-loading').show(); 
                $.ajax({
                    url: '" . Yii::app()->getbaseUrl(true) . "/user/cancelsubscription',
                    type:'POST',                    
                    dataType: 'json',
                    data: $('#cancel_form').serialize(),
                    beforeSend:function(){
                        if($('input:radio').length) {
                            if($('input:radio:checked').length < 1) {
                                $('#cancel_error').text('" . $choose_reason . "');
                                $('#cancel_error').show();
                                $('#cancel-loading').hide();
                                return false;
                            }
                        }
                    },
                    complete:function(){
                        $('#cancel-loading').hide();
                    },
                    success: function(data){
                        if(parseInt(data.responce) === 1) {
                            //$('#confirmModal').modal('hide');
                            //$('#finalModal').modal('show');
                            $('#cancelsuccessPopup').modal('show');
                            setTimeout(function () {
                                window.location = '" . Yii::app()->getbaseUrl(true) . "/user/myplans';
                            }, 5000);
                        } else {
                            $('#cancel_error').text(data.message);
                            $('#cancel_error').show();
                        }
                    }
                });
            });
            

        });


function cancelsubscription(plan_id) {
    $('#user_subscription_plan').val(plan_id); 
    $('#cancelsubscription').modal('show'); 
}
    </script>";
        return json_encode($data);
    }

    public function powered_by_text() {
        $studio_id = Yii::app()->common->getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        $return = '';
        if ($studio->hide_powered_by == 0) {
            $return.= '<div class="footer-poweredby">';
            $return.= Yii::app()->controller->Language['powered_by'] . ':<br /><a href="http://www.muvi.com/sdk/?utm_source=' . $studio->permalink . '" target="_blank" title="Muvi"><img src="' . Yii::app()->getbaseUrl(true) . '/common/images/powered-by.png" alt="Muvi" title="Muvi" /></a>';
            $return.= '</div>';
        }
        return $return;
    }

    public function copyright_text($text = '') {
        if ($text) {
            return html_entity_decode($text);
        } else {
            $studio_id = Yii::app()->common->getStudiosId();
            if (!$this->studioData) {
                $studio = new Studio();
                $studio = $studio->findByPk($studio_id);
            } else {
                $studio = $this->studioData;
            }
            return html_entity_decode($studio->copyright);
        }
    }

    public function formatFullReleaseDate($release) {
        $release_date = '';
        if ($release != '' && $release != NULL) {
            if ($release == '1970-01-01')
                $release_date = date('d F Y');
            else
                $release_date = date('d F Y', strtotime($release));
        }
        return $release_date;
    }

    public function getContentData($content_id, $is_episode = 0, $customData = array(), $language_id = false, $studio_id = false, $user_id = false, $translate = array(), $permalinkType = '') {
        $controller = Yii::app()->controller;
        $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');

        if ($user_id == false) {
            $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        }
        if ($studio_id == false) {
            $studio_id = Yii::app()->common->getStudiosId();
            $reminder = $controller->reminder;
        } else {
            $reminder = 0;
            $reminder_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'reminder');
            if (!empty($reminder_status)) {
                $reminder = $reminder_status['config_value'];
            }
        }
        $translate = $controller->Language;       
        if (!$language_id) {
            $language_id = $controller->language_id;
        }
        
        /* checking registration and login in popup if register by API  Sanjib */
        $api_available = $controller->login_with;
        $getStudioApi = ExternalApiKey::model()->checkRegisterApi();
        if (!empty($getStudioApi)) {
            $chk_registeration = 1;
        }
        $getPerimissionApi = ExternalApiKey::model()->chkPermissionApi();
        $langcontent = Yii::app()->custom->getTranslatedContent($content_id, $is_episode, $language_id, $studio_id);
        $censor_rating = '';
        $review_form_only = '';
        $reviewsummary = '';
        $reviews = array();
        $defaultResolution = '';
        $custom_vals = $multipleVideo = array();
        $is_ppv_bundle = 0;
        $is_live = 0;
        $content_title = '';
        $buy_btn = '';
        $content_category_value = 0;
        $episode_number = 0;
        $season_number = 0;
        $parent_content_title = "";
        
        if ($is_episode == 1) {
            $stream_id = $content_id;
            $stream = movieStreams::model()->findByPk($stream_id);
            $movie_id = $stream->movie_id;
            $content = Film::model()->findByPk($movie_id);
            if (array_key_exists($movie_id, $langcontent['film'])) {
                $content = Yii::app()->Helper->getLanuageCustomValue($content,@$langcontent['film'][$movie_id]);
                $content->name = $langcontent['film'][$movie_id]->name;
                $content->story = $langcontent['film'][$movie_id]->story;
                $content->genre = $langcontent['film'][$movie_id]->genre;
                $content->censor_rating = $langcontent['film'][$movie_id]->censor_rating;
                $content->language = $langcontent['film'][$movie_id]->language;
            }
            if (array_key_exists($stream_id, $langcontent['episode'])) {
                $stream = Yii::app()->Helper->getEpisodeLanguageCustomValue($stream,@$langcontent['episode'][$stream_id]);
                $stream->episode_title = $langcontent['episode'][$stream_id]->episode_title;
                $stream->episode_story = $langcontent['episode'][$stream_id]->episode_story;
            }
            $content_name = $content->name . ' ';
            $content_type = StudioContentType::model()->findByPk($content->content_type_id);
            $content_title = ($stream->episode_title != '') ? $stream->episode_title : "SEASON " . $stream->series_number . ", EPISODE " . $stream->episode_number;
            $content_name .= ($stream->episode_title != '') ? $stream->episode_title : "SEASON " . $stream->series_number . ", EPISODE " . $stream->episode_number;
            $parent_content_title = $content->name;
            if ($stream->mapped_stream_id) {
                $poster = $controller->getPoster($stream->mapped_stream_id, 'moviestream', 'episode', $studio_id);
            } else {
                $poster = $controller->getPoster($stream->id, 'moviestream', 'episode', $studio_id);
            }
            $story = $stream->episode_story;
            $release = $stream->episode_date;
            $ppv_plan = Yii::app()->common->getPPVBundle($movie_id, $studio_id);
            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                $is_ppv_bundle = 1;
                $payment_type = $ppv_plan->id;
            } else {
                $ppv = Yii::app()->common->getContentPaymentType($content->content_types_id, $content->ppv_plan_id, $studio_id);
                $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
            }

            $cast_detail = self::getCasts($content_id, 1, $language_id, $studio_id, $translate);
            $casts = $cast_detail['casts'];
            $casting = $cast_detail['casting'];
            $genre = json_decode($content->genre);
            $movie_uniq_id = $content->uniq_id;
            $content_category_value = $content->content_category_value;
            $stream_uniq_id = $stream->embed_id;
            $video_duration = $stream->video_duration;
            $episode_number = $stream->episode_number;
            $season_number = $stream->series_number;
            $permalink = Yii::app()->getbaseUrl(true) . '/' . $content->permalink;
            $release_date = self::formatReleaseDate($release);
            $full_release_date = self::formatFullReleaseDate($release);
            $is_landscape = 1;
            $ppv_id = $ppv->id;
            $is_converted = $stream->is_converted;
            $stream_id = $stream->id;
            $content_unique_id = $content->uniq_id;
            $content_type_id = $content->content_type_id;
            $content_types_id = $content->content_types_id;
            $ppv_plan_id = $content->ppv_plan_id;
            $full_movie = $stream->full_movie;
            $content_type_name = $content_type->display_name;
            $content_type_permalink = $content_type->permalink;
            $fpermalink = $content->permalink;
            $trailer_url = '';
            $is_trailer_converted = '';
            $trailer_player = '';
            $content_banner = '';
            $start_time = $content->start_time;
            $duration = $content->duration;
	    $content_language = $content->language;
            $movie_content = "'" . $movie_id . ":" . $stream->series_number . ":" . $stream_id . "'";
            
            $arg['studio_id'] = $studio_id;
            $arg['movie_id'] = $movie_id;
            $arg['season_id'] = 0;
            $arg['episode_id'] = 0;
            $arg['content_types_id'] = $content_types_id;

            $isFreeContent = Yii::app()->common->isFreeContent($arg);
            $vexist = Yii::app()->common->isVoucherExists($studio_id, $movie_content);
            if ($is_converted > 0) {
                if ($payment_type > 0 || $vexist != 0) {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="episodegetPpvPlans(this);" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                    }  else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                        $play_btn = '<a href="javascript:void(0);" onclick="episodeplayMovie(this);" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';                      
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                    }
                } else {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="episodeplayMovie(this);" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                    } else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                        $play_btn = '<a href="javascript:void(0);" onclick="episodeplayMovie(this);" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';                      
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="allplay playbtn" data-ctype="' . $content_types_id . '">';
                    }
                }
                $play_btn .= $translate['play_now'] . '</a>';
            } else {
                $play_btn = '';
            }

            /* sanjib */
            if (!empty($getPerimissionApi) && $user_id > 0) {
                $play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-stream_id="' . $stream_uniq_id . '" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn"  data-ctype="' . $content_types_id . '">' . $translate['play_now'] . '</a>';
            }
            
            $data_type = 2;
            $fcm = new MovieStreamsCustomMetadata;
            $fcms = $fcm->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id desc'));
            if (count($fcms) > 0) {
                foreach ($fcms as $fcm) {
                    $fld = CustomMetadataField::model()->findByPk($fcm->custom_field_id);
                    if ($fld->f_id != '' && strlen($fld->f_id) > 0) {
                        $arr[$fcm->field_name] = $fld->f_id;
                        $arr1[] = array(
                            'f_id' => trim($fld->f_id),
                            'field_name' => trim($fcm->field_name),
                            'field_display_name' => trim($fld->f_display_name),
                        );
                    }
                }
            }
            if (isset($arr1) && count($arr1) > 0) {
                foreach ($arr1 as $ar) {
                    $ar_k = trim($ar['f_id']);
                    $ar_k1 = trim($ar['field_name']);
                    $k = (int) str_replace('custom', '', $ar_k1);
                    if ($k > 5) {
                        if (@$stream->custom6) {
                            $x = json_decode($stream->custom6, true);
                            foreach ($x as $key => $value) {
                                foreach ($value as $key1 => $value1) {
                                    $custom_vals[$ar_k] = array(
                                        'field_display_name' => trim($ar['field_display_name']),
                                        'field_value' => trim($value1)
                                    );
                                }
                            }
                        }
                    } else {
                        $custom_vals[$ar_k] = array(
                            'field_display_name' => trim($ar['field_display_name']),
                            'field_value' => trim(is_array(json_decode($stream->$ar_k1)) ? implode(', ', json_decode($stream->$ar_k1)) : $stream->$ar_k1)
                        );
                    }
                }
            }
        } elseif ($is_episode == 4) {
            $playlistId = $content_id;
            $command1 = Yii::app()->db->createCommand()
                    ->select('id,playlist_name,playlist_permalink')
                    ->from('user_playlist_name u')
                    ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id=' . $playlistId);
            $data = $command1->QueryRow();
            if ($data) {
                $playlist_id = $data['id'];
                $playlist_name = $data['playlist_name'];
                $permalink = $data['playlist_permalink'];
                $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                $final_content[] = array(
                    'movie_id' => $playlist_id,
                    'title' => utf8_encode($playlist_name),
                    'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                    'poster' => $poster,
                    'is_episode' => $is_episode
                );
            }
        } else {
            $movie_id = $content_id;
            // query criteria
            $criteria = new CDbCriteria();
            // join Post model (one for fetching data, the other for filtering)
            $criteria->with = array(
                'movie_streams' => array(// this is for fetching data
                    'together' => false,
                    'condition' => 'movie_streams.is_episode = 0 AND movie_streams.studio_id = ' . $studio_id,
                ),
                'live_stream' => array(
                    'select' => 'feed_url',
                    'condition' => 'live_stream.studio_id = ' . $studio_id,
                )
            );
            $criteria->condition = 't.id = ' . $content_id;
            // order by author name
            $criteria->order = 't.id ASC';
            $criteria->limit = '1';
            $films = Film::model()->findAll($criteria);
            $content = $films[0];
            if ($content->mapped_id) {
                $stream = movieStreams::model()->find(array('condition' => 'movie_id =' . $content->mapped_id));
            } else {
                $stream = $content->movie_streams[0];
            }
            $live_stream = $content->live_stream[0];
            if ($live_stream->feed_url != '')
                $is_live = 1;
            if (array_key_exists($movie_id, $langcontent['film'])) {
                $content = Yii::app()->Helper->getLanuageCustomValue($content,@$langcontent['film'][$movie_id]);
                $content->name = $langcontent['film'][$movie_id]->name;
                $content->story = $langcontent['film'][$movie_id]->story;
                $content->genre = $langcontent['film'][$movie_id]->genre;
                $content->censor_rating = $langcontent['film'][$movie_id]->censor_rating;
            }
            $content_type = StudioContentType::model()->findByPk($content->content_type_id);
            $map_movieid = ($content->mapped_id) ? $content->mapped_id : $content->id;
            if ($content->content_types_id == 2 || $content->content_types_id == 4)
                $poster = $controller->getPoster($map_movieid, 'films', 'episode', $studio_id, @$content->mapped_id);
            else
                $poster = $controller->getPoster($map_movieid, 'films', 'standard', $studio_id, @$content->mapped_id);

            $story = $content->story;
            $release = $content->release_date;
            $content_category_value = $content->content_category_value;
            $movie_uniq_id = $content->uniq_id;
            $stream_uniq_id = 0;

            $ppv = Yii::app()->common->getContentPaymentType($content->content_types_id, $content->ppv_plan_id, $studio_id);
            $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
            $is_landscape = ($content_type == 2) ? 1 : 0;
            $data_type = ($content->content_types_id == 3) ? 1 : 0;
            $permalink = Yii::app()->getbaseUrl(true) . '/' . $content->permalink;
            $release_date = self::formatReleaseDate($release);
            $full_release_date = self::formatFullReleaseDate($release);
            $content_name = $content->name;
            $content_title = $content_name;
            $fpermalink = $content->permalink;
            $censor_rating = (!is_array(json_decode($content->censor_rating))) ? @$content->censor_rating : implode(',', json_decode($content->censor_rating)) . '&nbsp;';
            $ppv_id = $ppv->id;
            $is_converted = $stream->is_converted;
            $stream_id = $stream->id;
            $content_unique_id = $content->uniq_id;
            $content_type_id = $content->content_type_id;
            $content_types_id = $content->content_types_id;
            $ppv_plan_id = $content->ppv_plan_id;
            $full_movie = $stream->full_movie;
            $video_duration = $stream->video_duration;
            $content_type_name = $content_type->display_name;
            $content_type_permalink = $content_type->permalink;
            $trailer_url = $controller->getTrailer($content_id, '', $studio_id);
            $is_trailer_converted = $controller->getTrailerIsConverted($content_id);
            $start_time = $content->start_time;
            $duration = $content->duration;
            $content_language = $content->language;
            $MovieBanner = $controller->getPoster($movie_id, 'topbanner', 'original', $studio_id);
            $content_banner = '';
            if ($MovieBanner && @file_get_contents($MovieBanner)) {
                $content_banner = $MovieBanner;
            }
            $review_form_only = self::reviewformonly($content_id, $studio_id, $user_id, $translate);
            $reviewsummary = self::review($content_id, $studio_id, $user_id);
            $reviews = self::reviews($content_id, $studio_id, $user_id);
            $trailerDetArr = movieTrailer::model()->find(array("condition" => "movie_id = " . $content_id . " AND is_converted= 1 AND trailer_file_name !=''"));
            $defaultResolution = 144;
            $ad_source = '';
            if ($trailerDetArr) {
                $multipleVideo = $controller->getvideoResolution($trailerDetArr['video_resolution'], $trailerDetArr['video_remote_url']);
                $videoToBeplayed = $controller->getVideoToBePlayed($multipleVideo, $trailerDetArr['video_remote_url']);
                $videoToBeplayed12 = explode(",", $videoToBeplayed);
                $defaultResolution = $videoToBeplayed12[1];
                foreach ($multipleVideo as $key => $val) {
                    $ad_source .= '<source src="' . $val . '" data-res="' . $key . '" type="video/mp4" />';
                }
            }
            $payment_type = $adv_payment = $is_ppv_bundle = 0;
            $arg['studio_id'] = $studio_id;
            $arg['movie_id'] = $movie_id;
            $arg['season_id'] = 0;
            $arg['episode_id'] = 0;
            $arg['content_types_id'] = $content_types_id;

            $isFreeContent = Yii::app()->common->isFreeContent($arg);
            $ses = array();
            if ($content_types_id == 3) {
                $content_details = Yii::app()->controller->getContentSeasonDetails($movie_id, $studio_id);
                $EpDetails = Yii::app()->controller->getEpisodeToPlay($movie_id, $studio_id);
                if ($EpDetails) {
                    $is_converted = 1;
                }
                $seasons = $this->getSeasonsToPlay($movie_id);
                foreach ($seasons as $season) {
                    if (isset($season->series_number) && intval($season->series_number)) {
                        $ses[] = array('series_number' => $season->series_number);
                    }
                }
            }
            if (intval($isFreeContent) == 0) {
                $is_payment_gateway_exist = 0;
                $payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                if (isset($payment_gateway) && !empty($payment_gateway)) {
                    $is_payment_gateway_exist = 1;
                }
                if ($is_payment_gateway_exist) {
                    if ($is_converted == 1 || $content_types_id == 4) {
                        $ppv_plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                        if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                            $payment_type = $ppv_plan->id;
                        } else {
                            $ppv_plan = Yii::app()->common->getPPVBundle($movie_id, $studio_id);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $is_ppv_bundle = 1;
                                $payment_type = $ppv_plan->id;
                            } else {
                                $ppv_plan = Yii::app()->common->getContentPaymentType($content_types_id, $content->ppv_plan_id, $studio_id);
                                $payment_type = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? $ppv_plan->id : 0;
                            }
                        }
                        if ($payment_type == 0) {
                            $subscription_plan = Yii::app()->common->getSubscriptionBundle($movie_id, $studio_id);
                            if (isset($subscription_plan['id']) && intval($subscription_plan['id'])) {
                                $is_subscription_bundle = 1;
                                $payment_type = $subscription_plan['id'];
                            }
                        }
              $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);   
              $is_subscribed_user = Yii::app()->common->isSubscribed($user_id,$studio_id); 
         if(isset($monetizations['menu']) && ($monetizations['menu'] & 1) && !($monetizations['menu'] & 2) && !($monetizations['menu'] & 16) && !($monetizations['menu'] & 64) && !($monetizations['menu'] & 128) && !($monetizations['menu'] & 4) && !($monetizations['menu'] & 8) && ($is_subscribed_user=='') && ($payment_type==0)){
                        $payment_type=1; 
         }elseif(isset($monetizations['menu']) && ($monetizations['menu'] & 1) && !($monetizations['menu'] & 2) && !($monetizations['menu'] & 16) && !($monetizations['menu'] & 64) && !($monetizations['menu'] & 128) && ($monetizations['menu'] & 4) && !($monetizations['menu'] & 8) && ($is_subscribed_user=='') && ($payment_type==0)){
              $payment_type=1; 
         }elseif(isset($monetizations['menu']) && ($monetizations['menu'] & 1) && !($monetizations['menu'] & 2) && !($monetizations['menu'] & 16) && !($monetizations['menu'] & 64) && !($monetizations['menu'] & 128) && !($monetizations['menu'] & 4) && ($monetizations['menu'] & 8) && ($is_subscribed_user=='') && ($payment_type==0)){
              $payment_type=1; 
         }elseif(isset($monetizations['menu']) && ($monetizations['menu'] & 1) && !($monetizations['menu'] & 2) && !($monetizations['menu'] & 16) && !($monetizations['menu'] & 64) && !($monetizations['menu'] & 128) && ($monetizations['menu'] & 4) && ($monetizations['menu'] & 8) && ($is_subscribed_user=='') && ($payment_type==0)){
              $payment_type=1; 
         }   
         
                    } else {
                        $adv_plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id);
                        $adv_payment = (isset($adv_plan->id) && intval($adv_plan->id)) ? $adv_plan->id : 0;
                    }
                }
            }
            $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
            $vexist = Yii::app()->common->isVoucherExists($studio_id, $movie_id);
            if ($is_converted > 0 || $is_live == 1) {
                if ($payment_type > 0 || $vexist != 0) {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
                    }   else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                        $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
                    }   else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-content_title="' . $content_title . '" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-target="#loginModal" data-backdrop="static" data-purchase_type=""  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
                    }
                    if ($content_types_id == 3) {
                        if ($user_id > 0) {
                            $play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="season" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
                        } else {
                            $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-content_title="' . $content_title . '" data-backdrop="static" data-purchase_type="season" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
                        }
                    }
                } else {
                    if ($user_id > 0) {
                            $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
                    } else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                            $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-content_title="' . $content_title . '" data-backdrop="static" data-purchase_type=""  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn" data-ctype="' . $content_types_id . '">';
                    }
                    if ($content_types_id == 3) {
                        if ($user_id > 0) {
                            $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="season" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
                        } else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                            $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
                        } else {
                            $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-content_title="' . $content_title . '" data-backdrop="static" data-purchase_type="season" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
                        }
                    }
                }
                if ($content_types_id == 3) {
                    $buy_btn = $play_btn . $translate['buy_now'] . ' <span id="season_price"></span></a>';
                    $buy_btn .= '<input type="hidden" name="permalink" id="permalink" value="' . $fpermalink . '" />';
                    $buy_btn .= '<input type="hidden" name="content_name" id="content_name" value="' . $content_name . '" />';
                }
                $play_btn .= $translate['play_now'] . '</a>';
            } else {
                if (intval($adv_payment)) {
                    $price = Yii::app()->common->getPPVPrices($adv_payment, $controller->studio->default_currency_id);
                    if (isset(Yii::app()->user->id) && (Yii::app()->user->id > 0)) {
                        $play_btn .= '<a href="javascript:void(0);" onclick="showPpvPlans(this,1);" data-content-permalink="' . $permalink . '" data-purchase_type=""  data-movie_id="' . $content->uniq_id . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" class="playbtn"  data-ctype="' . $content_types_id . '">' . $translate['advance_purchase'] . '</a>';
                    } else {
                        $play_btn.= '<a href="javascript:void(0);" data-toggle="modal" data-content-permalink="' . $permalink . '" data-target="#loginModal" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-content_title="' . $content_title . '" data-purchase_type=""  data-backdrop="static" data-movie_id="' . $content->uniq_id . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $translate['advance_purchase'] . '</a>';
                    }
                }
            }
            /* sanjib */
            if (!empty($getPerimissionApi) && $user_id > 0) {
                //$play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn" data-stream_id="' . $stream_id . '" data-ctype="' . $content_types_id . '">' . $translate['play_now'] . '</a>';
                $play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn" data-stream_id="" data-ctype="' . $content_types_id . '">' . $translate['play_now'] . '</a>';
            }
            if ($play_btn != "") {
                $play_btn .= '<input type="hidden" name="permalink" id="permalink" value="' . $fpermalink . '" />';
                $play_btn .= '<input type="hidden" name="content_name" id="content_name" value="' . $content_name . '" />';
            }
            if ($content_types_id == 3 && count($ses) < 1) {
                $buy_btn = "";
            }
            $cast_detail = self::getCasts($content_id, 1, $language_id, $studio_id, $translate);
            $casts = $cast_detail['casts'];
            $casting = $cast_detail['casting'];
            $genre = json_decode($content->genre);
            if (!is_array($genre) && $content->genre) {
                if (!in_array(trim($content->genre), array('null', 'NULL', '[', '""'))) {
                    $genre = explode(",", $content->genre);
                }
            }
            if ($content['custom_metadata_form_id']) {
                $sql_cus = "SELECT p.*, cm.field_name FROM (SELECT cf.id,cf.f_display_name,cf.f_id FROM custom_metadata_form f,custom_metadata_field cf, custom_metadata_form_field cff WHERE f.id=cff.custom_form_id AND cf.id = cff.custom_field_id AND f.id = {$content['custom_metadata_form_id']} AND f.studio_id ={$studio_id})  AS p, film_custom_metadata cm WHERE p.id = cm.custom_field_id AND cm.custom_metadata_form_id={$content['custom_metadata_form_id']}";
            } else {
                $sql_cus = "SELECT p.*, cm.field_name FROM (SELECT cf.id,cf.f_display_name,cf.f_id FROM custom_metadata_form f,custom_metadata_field cf, custom_metadata_form_field cff WHERE f.id=cff.custom_form_id AND cf.id = cff.custom_field_id AND f.studio_id ={$studio_id}) AS p, film_custom_metadata cm WHERE p.id = cm.custom_field_id GROUP BY p.id";
            }
            $command_custom = Yii::app()->db->createCommand($sql_cus)->QueryAll();
            if (!empty($command_custom)) {
                $x = (@$content->custom10) ? json_decode($content->custom10, true) : 0;
                foreach ($command_custom as $key => $ar) {
                    $ar_k = trim($ar['f_id']);
                    $ar_k1 = trim($ar['field_name']);
                    $k = (int) str_replace('custom', '', $ar_k1);
                    if ($k > 9) {
                        if ($x) {
                            foreach ($x as $ckey => $value) {
                                foreach ($value as $key1 => $value1) {
                                    if ($key1 == $ar['field_name']) {
                                        $custom_vals[$ar_k] = array(
                                            'field_display_name' => trim($ar['f_display_name']),
                                            'field_value' => trim($value1)
                                        );
                                    }
                                }
                            }
                        }
                    } else {
                        $custom_vals[$ar_k] = array(
                            'field_display_name' => trim($ar['f_display_name']),
                            'field_value' => trim(is_array(json_decode($content->$ar_k1)) ? implode(', ', json_decode($content->$ar_k1)) : $content->$ar_k1)
                        );
                    }
                }
            }
        }
		if ($stream->is_downloadable && ($stream->thirdparty_url=='')) {
			if ($is_converted > 0) {
				if ($payment_type > 0 || $vexist != 0) {
					if ($user_id > 0) {
						$download_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" data-download="' . $stream->is_downloadable . '" class="playbtn" >';
					} else if ($std_config['config_value'] == 0 && $isFreeContent == 1) {
						$download_btn = '<a href="javascript:void(0);" onclick="download_content(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-purchase_type=""  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn" data-stream_id="' . $stream_id . '" data-ctype="' . $content_types_id . '">';
					} else {
						$download_btn = '<a href="javascript:void(0);" data-toggle="modal" data-download="' . $stream->is_downloadable . '" data-target="#generalInfoModal" data-backdrop="static" data-purchase_type=""  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
					}
				}else{
					if ($user_id > 0) {
						$download_btn = '<a href="javascript:void(0);" onclick="download_content(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
					} else if ($std_config['config_value'] == 0 && $isFreeContent == 1) {
						$download_btn = '<a href="javascript:void(0);" onclick="download_content(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
					} else {
						$download_btn = '<a href="javascript:void(0);" data-toggle="modal" data-download="' . $stream->is_downloadable . '" data-target="#generalInfoModal" data-backdrop="static" data-purchase_type=""  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn"  data-ctype="' . $content_types_id . '">';
					}
				}
				$download_btn .= $translate['download'] . '</a>';
			}
		}
        $short_story = Yii::app()->common->htmlchars_encode_to_html(self::formattedWords($story, 200));
        $is_episode = (($is_episode) && strlen($is_episode) > 0) ? $is_episode : '0';
        $final_content = array(
            'movie_id' => $movie_id,
            'content_category_value' => $content_category_value,
            'is_episode' => $is_episode,
            'episode_number' => $episode_number,
            'season_number' => $season_number,
            'title' => utf8_encode($content_name),
            'parent_content_title' => utf8_encode($parent_content_title),
            'content_details' => $content_details,
            'content_title' => utf8_encode($content_title),
            'play_btn' => $play_btn,
            'buy_btn' => $buy_btn,
            'permalink' => $permalinkType ? $fpermalink : $permalink,
            'poster' => $poster,
            'data_type' => $data_type,
            'is_landscape' => $is_landscape,
            'release_date' => $release_date,
            'full_release_date' => $full_release_date,
            'censor_rating' => $censor_rating,
            'movie_uniq_id' => $movie_uniq_id,
            'stream_uniq_id' => $stream_uniq_id,
            'video_duration' => $video_duration,
            'video_duration_text' => self::videoDurationText($video_duration),
            'ppv' => $ppv_id,
            'payment_type' => $payment_type,
            'is_converted' => $is_converted,
            'movie_stream_id' => $stream_id,
            'uniq_id' => $content_unique_id,
            'content_type_id' => $content_type_id,
            'content_types_id' => $content_types_id,
            'ppv_plan_id' => $ppv_plan_id,
            'full_movie' => $full_movie,
            'story' => utf8_encode(Yii::app()->general->showHtmlContents($story)),
            'short_story' => utf8_encode($short_story),
            'genres' => $genre,
            'display_name' => utf8_encode($content_type_name),
            'content_permalink' => $content_type_permalink,
            'trailer_url' => $trailer_url,
            'trailer_is_converted' => $is_trailer_converted,
            'trailer_player' => $trailer_player,
            'casts' => @$casts,
            'casting' => @$casting,
            'content_banner' => $content_banner,
            'reviewformonly' => $review_form_only,
            'reviewsummary' => $reviewsummary,
            'reviews' => json_encode($reviews),
            'myreview' => $this->getMyreview($content_id, $studio_id, $user_id),
            'defaultResolution' => $defaultResolution,
            'multipleVideo' => $multipleVideo,
            'start_time' => $start_time,
            'duration' => $duration,
            'custom' => @$custom_vals,
			'is_downloadable' => @$stream->is_downloadable,
			'download_btn' => @$download_btn,
            'buy_btn' => @$buy_btn,
	    'content_language'=>@$content_language
        );
        $final_content['booking_status'] = 0;
        $final_content['show_booking_button'] = 0;
        $final_content['booking_time'] = "";
        if ($reminder) {
            $final_content['show_booking_button'] = 1;
            if (($user_id > 0)) {
                $book_event = BookedContent::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'content_type' => $is_episode, 'content_id' => $content_id));
                if (!empty($book_event)) {
                    if (strtotime($book_event->booked_time) > strtotime(gmdate('Y-m-d H:i:s'))) {
                        $final_content['booking_status'] = 1;
                        $final_content['booking_time'] = $book_event->booked_time;
                    }
                }
            }
            if (($start_time != "0000-00-00 00:00:00") && (strtotime($start_time) < strtotime(gmdate('Y-m-d H:i:s')))) {
                $final_content['show_booking_button'] = 0;
            }
        }
        if (count($custom_vals) > 0)
            $final_content = array_merge($final_content, @$custom_vals);
        return $final_content;
    }

    /*
     * author: RK
     * To get limited content for home page
     */

    public function getLimitedContentData($content_id, $is_episode = 0, $customData = array()) {
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $censor_rating = '';
        $review_form_only = '';
        $reviewsummary = '';
        $reviews = array();
        $defaultResolution = '';
        $multipleVideo = array();
        $is_ppv_bundle = 0;
        $is_live = 0;
        $content_title = '';
        if ($is_episode == 1) {
            $stream_id = $content_id;
            $stream = movieStreams::model()->findByPk($stream_id);
            $movie_id = $stream->movie_id;

            $content = Film::model()->findByPk($movie_id);
            $content_name = $content->name . ' ';
            $content_title = ($stream->episode_title != '') ? $stream->episode_title : "SEASON " . $stream->series_number . ", EPISODE " . $stream->episode_number;
            $content_name.= ($stream->episode_title != '') ? $stream->episode_title : "SEASON " . $stream->series_number . ", EPISODE " . $stream->episode_number;
            $poster = $controller->getPoster($stream_id, 'moviestream', 'episode', $studio_id);
            $story = $stream->episode_story;
            $release = $stream->episode_date;

            $ppv_plan = Yii::app()->common->getPPVBundle($movie_id);
            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                $is_ppv_bundle = 1;
                $payment_type = $ppv_plan->id;
            } else {
                $ppv = Yii::app()->common->getContentPaymentType($content->content_types_id, $content->ppv_plan_id);
                $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
            }

            $cast_detail = self::getCasts($movie_id);
            $genre = json_decode($content->genre);
            $movie_uniq_id = $content->uniq_id;
            $stream_uniq_id = $stream->embed_id;
            $video_duration = $stream->video_duration;
            $permalink = Yii::app()->getbaseUrl(true) . '/' . $content->permalink;
            $release_date = self::formatReleaseDate($release);
            $full_release_date = self::formatFullReleaseDate($release);
            $is_landscape = 1;
            $ppv_id = $ppv->id;
            $is_converted = $stream->is_converted;
            $stream_id = $stream->id;
            $content_unique_id = $content->uniq_id;
            $content_type_id = $content->content_type_id;
            $content_types_id = $content->content_types_id;
            $ppv_plan_id = $content->ppv_plan_id;
            $full_movie = $stream->full_movie;
            $fpermalink = $content->permalink;
            $trailer_url = '';
            $is_trailer_converted = '';
            $trailer_player = '';
            $content_banner = '';

            if ($is_converted > 0) {
                if ($payment_type > 0) {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="episodegetPpvPlans(this);" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                    }
                } else {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="episodeplayMovie(this);" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="allplay playbtn" data-ctype="' . $content_types_id . '">';
                    }
                }
                $play_btn.= $controller->ServerMessage['play_now'] . '</a>';
            } else {
                $play_btn = '';
            }
            $data_type = 2;
        } else {
            $movie_id = $content_id;
            // query criteria
            $criteria = new CDbCriteria();
            // join Post model (one for fetching data, the other for filtering)
            $criteria->with = array(
                'movie_streams' => array(// this is for fetching data
                    'together' => false,
                    'condition' => 'movie_streams.is_episode = 0 AND movie_streams.studio_id = ' . $studio_id,
                ),
                'live_stream' => array(
                    'select' => 'feed_url',
                    'condition' => 'live_stream.studio_id = ' . $studio_id,
                )
            );
            $criteria->condition = 't.id = ' . $content_id;
            // order by author name
            $criteria->order = 't.id ASC';
            $criteria->limit = '1';
            $films = Film::model()->findAll($criteria);
            $content = $films[0];
            $stream = $content->movie_streams[0];
            $live_stream = $content->live_stream[0];
            if ($live_stream->feed_url != '')
                $is_live = 1;
            $content_type = StudioContentType::model()->findByPk($content->content_type_id);
            if ($content->content_types_id == 2 || $content->content_types_id == 4)
                $poster = $controller->getPoster($content->id, 'films', 'episode', $studio_id);
            else
                $poster = $controller->getPoster($content->id, 'films', 'standard', $studio_id);

            $release = $content->release_date;
            $movie_uniq_id = $content->uniq_id;
            $stream_uniq_id = 0;
            $is_landscape = ($content_type == 2) ? 1 : 0;
            $data_type = ($content->content_types_id == 3) ? 1 : 0;
            $permalink = Yii::app()->getbaseUrl(true) . '/' . $content->permalink;
            $release_date = self::formatReleaseDate($release);
            $full_release_date = self::formatFullReleaseDate($release);
            $content_name = $content->name;
            $content_title = $content_name;
            $fpermalink = $content->permalink;
            $censor_rating = ($content->censor_rating != '') ? implode(',', json_decode($content->censor_rating)) . '&nbsp;' : '';
            $is_converted = $stream->is_converted;
            $stream_id = $stream->id;
            $content_unique_id = $content->uniq_id;
            $content_type_id = $content->content_type_id;
            $content_types_id = $content->content_types_id;
            $story = $content->story;
            $payment_type = $adv_payment = $is_ppv_bundle = 0;
            $arg['studio_id'] = $studio_id;
            $arg['movie_id'] = $movie_id;
            $arg['season_id'] = 0;
            $arg['episode_id'] = 0;
            $arg['content_types_id'] = $content_types_id;

            $isFreeContent = Yii::app()->common->isFreeContent($arg);
            if (intval($isFreeContent) == 0) {
                $is_payment_gateway_exist = 0;
                $payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                if (isset($payment_gateway) && !empty($payment_gateway)) {
                    $is_payment_gateway_exist = 1;
                }
                if ($is_payment_gateway_exist) {
                    if ($is_converted == 1) {
                        $ppv_plan = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);
                        if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                            $payment_type = $ppv_plan->id;
                        } else {
                            $ppv_plan = Yii::app()->common->getPPVBundle($movie_id);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $is_ppv_bundle = 1;
                                $payment_type = $ppv_plan->id;
                            } else {
                                $ppv_plan = Yii::app()->common->getContentPaymentType($content_types_id, $content->ppv_plan_id);
                                $payment_type = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? $ppv_plan->id : 0;
                            }
                        }
                    } else {
                        $adv_plan = Yii::app()->common->checkAdvancePurchase($movie_id);
                        $adv_payment = (isset($adv_plan->id) && intval($adv_plan->id)) ? $adv_plan->id : 0;
                    }
                }
            }

            if ($is_converted > 0 || $is_live == 1) {
                if ($payment_type > 0) {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
                    }
                } else {
                    if ($user_id > 0) {
                        $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                    } else {
                        $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn" data-ctype="' . $content_types_id . '">';
                    }
                }
                $play_btn.= $controller->ServerMessage['play_now'] . '</a>';
            } else {
                if (intval($adv_payment)) {
                    $price = Yii::app()->common->getPPVPrices($adv_payment, $controller->studio->default_currency_id);
                    if (isset(Yii::app()->user->id) && (Yii::app()->user->id > 0)) {
                        $play_btn.= '<a href="javascript:void(0);" onclick="showPpvPlans(this,1);" data-content-permalink="' . $permalink . '" data-movie_id="' . $content->uniq_id . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $controller->Language['advance_purchase'] . '</a>';
                    } else {
                        $play_btn.= '<a href="javascript:void(0);" data-toggle="modal" data-content-permalink="' . $permalink . '" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $content->uniq_id . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $controller->Language['advance_purchase'] . '</a>';
                    }
                }
            }
        }
        $short_story = Yii::app()->common->htmlchars_encode_to_html(self::formattedWords($story, 200));
        $final_content = array(
            'movie_id' => $movie_id,
            'title' => utf8_encode($content_title),
            'play_btn' => $play_btn,
            'permalink' => $permalink,
            'poster' => $poster,
            'data_type' => $data_type,
            'release_date' => $release_date,
            'full_release_date' => $full_release_date,
            'is_converted' => @$is_converted,
            'movie_stream_id' => $stream_id,
            'uniq_id' => $movie_uniq_id,
            'content_type_id' => $content_type_id,
            'content_types_id' => $content_types_id,
            'short_story' => utf8_encode($short_story)
        );
        return $final_content;
    }

    /* Added by Ratikanta for loading less content in homepage */

    public function mandatoryData($section_id, $studio_id, $is_payment_gateway_exist, $postCldFontPath, $cloudFront, $langcontent = array()) {
        $contentData = Yii::app()->common->getFeaturedContentsNew($section_id);
        $controller = Yii::app()->controller;
        $movie_product_ids = array();
        $movie_content_ids = array();
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $has_physical = Yii::app()->general->getStoreLink();
        $con = Yii::app()->db;
        foreach ($contentData['contents'] AS $fkey => $featured) {
            $movie_id = $featured['movie_id'];
            $is_episode = $featured['is_episode'];
            $play_btn = '';
            $arg['studio_id'] = $studio_id;
            $arg['movie_id'] = $movie_id;
            $arg['season_id'] = 0;
            $arg['episode_id'] = 0;
            $arg['content_types_id'] = $content_types_id = $featured['content_types_id'];
            $is_converted = $featured['is_converted'];
            $payment_type = $adv_payment = $is_ppv_bundle = 0;
            if (array_key_exists($movie_id, $langcontent['film'])) {
                $featured = Yii::app()->Helper->getLanuageCustomValue($featured, @$langcontent['film'][$movie_id]);
                $featured['name'] = $langcontent['film'][$movie_id]->name;
                $featured['story'] = $langcontent['film'][$movie_id]->story;
                $featured['genre'] = @$langcontent['film'][$movie_id]->genre;
                $featured['censor_rating'] = @$langcontent['film'][$movie_id]->censor_rating;
                $featured['language'] = @$langcontent['film'][$movie_id]->language;
            }
            //For Physical content
            if ($is_episode == 2) {
                if ($has_physical > 0) {
                    $sql = "SELECT P.* FROM pg_product P WHERE P.id = '" . $movie_id . "'";
                    $standaloneproduct = $con->createCommand($sql)->queryAll();
                    if ($standaloneproduct) {
                        foreach ($standaloneproduct AS $key => $val) {
                            if (Yii::app()->common->isGeoBlockPGContent($val['id'])) {
                                $cont_name = $val['name'];
                                $story = $val['description'];
                                $permalink = $val['permalink'];
                                $short_story = Yii::app()->common->htmlchars_encode_to_html(self::formattedWords($story, 200));
                                $tempprice = Yii::app()->common->getPGPrices($val['id'], Yii::app()->controller->studio->default_currency_id);
                                if (!empty($tempprice)) {
                                    $standaloneproduct[$key]['sale_price'] = $tempprice['price'];
                                    $standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
                                }
                                $poster = PGProduct::getpgImage($val['id'], 'standard');
                                $formatted_price = Yii::app()->common->formatPrice($standaloneproduct[$key]['sale_price'], $standaloneproduct[$key]['currency_id']);

                                $final_content[] = array(
                                    'movie_id' => $movie_id,
                                    'title' => utf8_encode($cont_name),
                                    'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                    'poster' => $poster,
                                    'data_type' => 4,
                                    'uniq_id' => $movie_uniq_id,
                                    'short_story' => utf8_encode($short_story),
                                    'price' => $formatted_price,
                                    'status' => $val['status'],
                                    'is_episode' => 2
                                );
                            }
                        }
                    }
                }
            } elseif ($is_episode == 4) {
                $command1 = Yii::app()->db->createCommand()
                    ->select('id,playlist_name,playlist_permalink')
                    ->from('user_playlist_name u')
                    ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id=' . $movie_id);
                $data = $command1->QueryRow();
                if ($data) {
                    $playlist_id = $data['id'];
                    $playlist_name = $data['playlist_name'];
                    $permalink = $data['playlist_permalink'];
                    $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                    $final_content[] = array(
                        'movie_id' => $playlist_id,
                        'title' => utf8_encode($playlist_name),
                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                        'poster' => $poster,
                        'is_episode' => $is_episode
                    );
                }
            } else {
                $isFreeContent = Yii::app()->common->isFreeContent($arg);
                if (intval($isFreeContent) == 0) {
                    $is_payment_gateway_exist = 0;
                    $payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                    if (isset($payment_gateway) && !empty($payment_gateway)) {
                        $is_payment_gateway_exist = 1;
                    }
                    if ($is_payment_gateway_exist) {
                        if ($is_converted == 1) {
                            $ppv_plan = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $payment_type = $ppv_plan->id;
                            } else {
                                $ppv_plan = Yii::app()->common->getPPVBundle($movie_id);
                                if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                    $is_ppv_bundle = 1;
                                    $payment_type = $ppv_plan->id;
                                } else {
                                    $ppv_plan = Yii::app()->common->getContentPaymentType($featured['content_types_id'], $featured['ppv_plan_id']);
                                    $payment_type = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? $ppv_plan->id : 0;
                                }
                            }
                        } else {
                            $adv_plan = Yii::app()->common->checkAdvancePurchase($movie_id);
                            $adv_payment = (isset($adv_plan->id) && intval($adv_plan->id)) ? $adv_plan->id : 0;
                        }
                    }
                }
                if ($is_episode == 1) {
                    $data_type = 2;
                    $is_landscape = 1;
                    $stream_id = $featured['stream_id'];
                    if (array_key_exists($stream_id, $langcontent['episode'])) {
                        $featured = Yii::app()->Helper->getEpisodeLanguageCustomValue($featured, @$langcontent['episode'][$stream_id]);
                        $featured['episode_title'] = $langcontent['episode'][$stream_id]->episode_title;
                        $featured['episode_story'] = $langcontent['episode'][$stream_id]->episode_story;
                    }
                    $cont_name = ($featured['episode_title'] != '') ? $featured['episode_title'] : "SEASON " . $featured['series_number'] . ", EPISODE " . $featured['episode_number'];
                    if ($featured['mapped_stream_id']) {
                        $poster = $controller->getPoster($featured['mapped_stream_id'], 'moviestream', 'episode');
                    } else {
                        $poster = $controller->getPoster($stream_id, 'moviestream', 'episode');
                    }

                    $story = $featured['episode_story'];
                    $release = $featured['episode_date'];
                    $movie_uniq_id = $featured['uniq_id'];
                    $stream_uniq_id = $featured['embed_id'];
                    if ($featured['is_converted'] == 1) {
                        if ($payment_type > 0) {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" onclick="episodegetPpvPlans(this,1);" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                            }
                        } else {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" onclick="episodeplayMovie(this);" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="allplay playbtn" data-ctype="' . $content_types_id . '">';
                            }
                        }
                        $play_btn .= $controller->ServerMessage['play_now'] . '</a>';
                    } else {
                        if ($adv_payment > 0) {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" onclick="episodegetPpvPlans(this,1);" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isadv="1" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isadv="1" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                            }
                            $play_btn .= $controller->ServerMessage['play_now'] . '</a>';
                        }
                    }
                } else {
                    $data_type = ($featured['content_types_id'] == 3) ? 1 : 0;
                    $is_landscape = ($featured['content_types_id'] == 2) ? 1 : 0;
                    $cont_name = $featured['name'];
                    $stream_id = $featured['stream_id'];
                    $is_converted = $featured['is_converted'];
                    if ($featured['mapped_id']) {
                        $map_movieid = $featured['mapped_id'];
                    } else {
                        $map_movieid = $movie_id;
                    }
                    if ($featured['content_types_id'] == 2 || $featured['content_types_id'] == 4) {
                        $poster = $controller->getPoster($map_movieid, 'films', 'episode');
                    } else {
                        $poster = $controller->getPoster($map_movieid, 'films', 'standard');
                    }
                    $story = $featured['story'];
                    $release = $featured['release_date'];
                    $movie_uniq_id = $featured['uniq_id'];
                    $stream_uniq_id = 0;
                    $fpermalink = $featured['fplink'];

                    if ($is_converted > 0) {
                        if ($payment_type > 0) {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                            }
                        } else {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn" data-ctype="' . $content_types_id . '">';
                            }
                        }
                        $play_btn.= $controller->ServerMessage['play_now'] . '</a>';
                    } else {
                        if (intval($adv_payment)) {
                            if ($user_id > 0) {
                                $play_btn.= '<a href="javascript:void(0);" onclick="showPpvPlans(this,1);" data-content-permalink="' . $permalink . '" data-movie_id="' . $featured['uniq_id'] . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $controller->Language['advance_purchase'] . '</a>';
                            } else {
                                $play_btn.= '<a href="javascript:void(0);" data-toggle="modal" data-content-permalink="' . $permalink . '" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $featured['uniq_id'] . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $controller->Language['advance_purchase'] . '</a>';
                            }
                        }
                    }
                }
                $release_date = self::formatReleaseDate($release);
                $full_release_date = self::formatFullReleaseDate($release);
                $short_story = Yii::app()->common->htmlchars_encode_to_html(self::formattedWords($story, 200));

                $final_content[] = array(
                    'movie_id' => $movie_id,
                    'title' => utf8_encode($cont_name),
                    'play_btn' => $play_btn,
                    'permalink' => Yii::app()->getbaseUrl(true) . '/' . $featured['fplink'],
                    'poster' => $poster,
                    'data_type' => $data_type,
                    'release_date' => $release_date,
                    'full_release_date' => $full_release_date,
                    'is_converted' => @$is_converted,
                    'movie_stream_id' => $stream_id,
                    'uniq_id' => $movie_uniq_id,
                    'content_type_id' => @$featured['content_type_id'],
                    'content_types_id' => @$featured['content_types_id'],
                    'short_story' => utf8_encode($short_story),
                    'is_episode' => $is_episode,
                    'genres' => json_decode($featured['genre']),
                );
            }
        }
        return $final_content;
    }

    /**
     * @method public getTrailerInfo($movie_id,) Returns the trailer url for the Moive
     * @param int $movie_id Id of the movie to get the Trailer
     * @param int $return_type If its mentioned then it will return the complete information of trailer else just the URL.
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    function getTrailerInfo($movie_id = '', $return_type = 0, $cloudFront) {
        if ($movie_id) {
            $data = movieTrailer::model()->findAllByAttributes(array('movie_id' => $movie_id));
            if ($data) {
                if ($data[0]->video_remote_url != '') {
                    if (HOST_IP == '127.0.0.1') {
                        return $data[0]->video_remote_url;
                    } else {
                        $videoUrl = $data[0]->video_remote_url;
                        $studio_id = Yii::app()->common->getStudiosId();
                        $expires = time() + 10000;
                        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
                            'url' => $videoUrl,
                            'expires' => $expires,
                            'is_converted' => $data[0]->is_converted,
                            'video_resolution' => $data[0]->video_resolution,
                            'video_remote_url' => $data[0]->video_remote_url
                        ));
                        return $signedUrlCannedPolicy;
                    }
                } else {
                    return '';
                }
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function formatContentData($section_id, $studio_id, $is_payment_gateway_exist, $postCldFontPath, $cloudFront, $langcontent = array()) {
        $contentData = Yii::app()->common->getFeaturedContentsNew($section_id);
        $controller = Yii::app()->controller;
        $con = Yii::app()->db;
        $language_id = $controller->language_id;
        $theme_name = Studio::model()->findByPk($studio_id, array('select' => 'parent_theme'))->parent_theme;
        $content = Template::model()->findByAttributes(array('code' => $theme_name), array('select' => 'content_type'));
        $content_type = $content->content_type;
        $movie_product_ids = array();
        $movie_content_ids = array();
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $has_physical = Yii::app()->general->getStoreLink();
        foreach ($contentData['contents'] AS $fkey => $featured) {
            $episode_number = 0;
            $season_number = 0;
            $parent_content_title = "";
            $content_details = array();
            $payment_type = $adv_payment = 0;
            $movie_id = $featured['movie_id'];
            $is_episode = $featured['is_episode'];
            if ($content_type == 3) {
                if ($is_episode == 1) {
                    $stream_id = $movie_id;
                    $movie = MovieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $stream_id), array('select' => 'movie_id'));
                    $movie_id = $movie->movie_id;
                    $final_content[] = self::getContentData($stream_id, $is_episode);
                } elseif ($is_episode == 4) {
                    $command1 = Yii::app()->db->createCommand()
                        ->select('id,playlist_name,playlist_permalink')
                        ->from('user_playlist_name u')
                        ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id=' . $movie_id);
                    $data = $command1->QueryRow();
                    if ($data) {
                        $playlist_id = $data['id'];
                        $playlist_name = $data['playlist_name'];
                        $permalink = $data['playlist_permalink'];
                        $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                        $final_content[] = array(
                            'movie_id' => $playlist_id,
                            'title' => utf8_encode($playlist_name),
                            'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                            'poster' => $poster,
                            'is_episode' => $is_episode
                        );
                    }
                } else {
                    $stream_id = $movie_id;
                    $final_content[] = self::getContentData($movie_id, $is_episode);
                }
            } else {
            $stream_id = $featured['stream_id'];
            if ($is_episode == 1) {
                $content_id = $stream_id;
            } else {
                $content_id = $movie_id;
            }
            $langcontent = Yii::app()->custom->getTranslatedContent($content_id, $is_episode, $language_id);
            $btn_link = '';
            $play_btn = '';
            $trailer_player = '';
            $arg['studio_id'] = $studio_id;
            $arg['movie_id'] = $movie_id;
            $arg['season_id'] = 0;
            $arg['episode_id'] = 0;
            $arg['content_types_id'] = $content_types_id = $featured['content_types_id'];
            $is_converted = $featured['is_converted'];
            $payment_type = $adv_payment = $is_ppv_bundle = 0;
            if (array_key_exists($movie_id, $langcontent['film'])) {
                    $featured = Yii::app()->Helper->getLanuageCustomValue($featured, @$langcontent['film'][$movie_id]);
                    $featured['name'] = $langcontent['film'][$movie_id]->name;
                    $featured['story'] = $langcontent['film'][$movie_id]->story;
                    $featured['genre'] = @$langcontent['film'][$movie_id]->genre;
                    $featured['censor_rating'] = @$langcontent['film'][$movie_id]->censor_rating;
                    $featured['language'] = @$langcontent['film'][$movie_id]->language;
                }
            if ($is_episode == 2) {
                if ($has_physical > 0) {
                    $sql = "SELECT P.* FROM pg_product P WHERE P.id = '" . $movie_id . "'";
                    $standaloneproduct = $con->createCommand($sql)->queryAll();
                    if ($standaloneproduct) {
                        foreach ($standaloneproduct AS $key => $val) {
                            if (Yii::app()->common->isGeoBlockPGContent($val['id'])) {
                                $cont_name = $val['name'];
                                $story = $val['description'];
                                $permalink = $val['permalink'];
                                $short_story = Yii::app()->common->htmlchars_encode_to_html(self::formattedWords($story, 80));
                                $tempprice = Yii::app()->common->getPGPrices($val['id'], Yii::app()->controller->studio->default_currency_id);
                                if (!empty($tempprice)) {
                                    $standaloneproduct[$key]['sale_price'] = $tempprice['price'];
                                    $standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
                                }
                                $poster = PGProduct::getpgImage($val['id'], 'standard');
                                $formatted_price = Yii::app()->common->formatPrice($standaloneproduct[$key]['sale_price'], $standaloneproduct[$key]['currency_id']);

                                $final_content[] = array(
                                    'movie_id' => $movie_id,
                                    'title' => utf8_encode($cont_name),
                                    'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                    'poster' => $poster,
                                    'data_type' => 4,
                                    'uniq_id' => $movie_uniq_id,
                                    'short_story' => utf8_encode($short_story),
                                    'price' => $formatted_price,
                                    'status' => $val['status'],
                                    'is_episode' => 2
                                );
                            }
                        }
                    }
                }
            } else {
                $isFreeContent = Yii::app()->common->isFreeContent($arg);
                if (intval($isFreeContent) == 0) {
                    $is_payment_gateway_exist = 0;
                    $payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                    if (isset($payment_gateway) && !empty($payment_gateway)) {
                        $is_payment_gateway_exist = 1;
                    }
                    if ($is_payment_gateway_exist) {
                        if ($is_converted == 1) {
                            $ppv_plan = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $payment_type = $ppv_plan->id;
                            } else {
                                $ppv_plan = Yii::app()->common->getPPVBundle($movie_id);
                                if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                    $is_ppv_bundle = 1;
                                    $payment_type = $ppv_plan->id;
                                } else {
                                    $ppv_plan = Yii::app()->common->getContentPaymentType($featured['content_types_id'], $featured['ppv_plan_id']);
                                    $payment_type = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? $ppv_plan->id : 0;
                                }
                            }
                        } else {
                            $adv_plan = Yii::app()->common->checkAdvancePurchase($movie_id);
                            $adv_payment = (isset($adv_plan->id) && intval($adv_plan->id)) ? $adv_plan->id : 0;
                        }
                    }
                }
                $stream_id = $featured['stream_id'];
                $stream = movieStreams::model()->findByPk($stream_id);
                if ($is_episode == 1) {
                    $episode_number = $stream->episode_number;
                    $season_number = $stream->series_number;
                    $parent_content_title = $featured['name'];
                    $data_type = 2;
                    $is_landscape = 1;
                    if (array_key_exists($stream_id, $langcontent['episode'])) {
                        $featured['episode_title'] = $langcontent['episode'][$stream_id]->episode_title;
                        $featured['episode_story'] = $langcontent['episode'][$stream_id]->episode_story;
                    }
                    if (isset($stream) && isset($stream->video_duration))
                        $video_duration = $stream->video_duration;
                    $cont_name = ($featured['episode_title'] != '') ? $featured['episode_title'] : "SEASON " . $featured['series_number'] . ", EPISODE " . $featured['episode_number'];
                    $mappedstreamid = ($featured['mapped_stream_id']) ? $featured['mapped_stream_id'] : $featured['stream_id'];
                    $poster = $controller->getPoster($mappedstreamid, 'moviestream', 'episode');

                    $story = $featured['episode_story'];
                    $release = $featured['episode_date'];

                    $movie_uniq_id = $featured['uniq_id'];
                    $stream_uniq_id = $featured['embed_id'];
                    if ($featured['is_converted'] == 1) {
                        if ($payment_type > 0) {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" onclick="episodegetPpvPlans(this);" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                            }
                        } else {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" onclick="episodeplayMovie(this);" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isppv="0" class="allplay playbtn" data-ctype="' . $content_types_id . '">';
                            }
                        }
                        $play_btn .= $controller->ServerMessage['play_now'] . '</a>';
                    } else {
                        if ($adv_payment > 0) {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" onclick="episodegetPpvPlans(this);" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isadv="1" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);"  data-content-permalink="' . $featured['fplink'] . '" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $movie_uniq_id . '" data-stream_id="' . $stream_uniq_id . '" data-isadv="1" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                            }
                            $play_btn .= $controller->ServerMessage['play_now'] . '</a>';
                        }
                    }
                } else {
                    $is_live  = 0;
                    $feed_url = "";
                    if($is_episode == 3){
                       $feed_url = Livestream :: model()->findByAttributes(array("studio_id"=>$studio_id, "movie_id"=> $movie_id), array('select' => 'feed_url'))->feed_url; 
                    }
                    if($feed_url !=""){
                        $is_live = 1;
                    }
                    if($featured['content_types_id'] == 3){
                        $content_details = Yii::app()->controller->getContentSeasonDetails($movie_id, $studio_id);
                    }
                    $data_type = ($featured['content_types_id'] == 3) ? 1 : 0;
                    $is_landscape = ($featured['content_types_id'] == 2) ? 1 : 0;
                    $cont_name = $featured['name'];
                    $is_converted = $featured['is_converted'];
                    if (isset($stream) && isset($stream->video_duration))
                        $video_duration = $stream->video_duration;
                    $is_converted = $featured['is_converted'];
                    $map_movieid = ($featured['mapped_id']) ? $featured['mapped_id'] : $movie_id;
                    if ($featured['content_types_id'] == 2 || $featured['content_types_id'] == 4) {
                        $poster = $controller->getPoster($map_movieid, 'films', 'episode', 0, @$featured['mapped_id']);
                    } else {
                        $poster = $controller->getPoster($map_movieid, 'films', 'standard', 0, @$featured['mapped_id']);
                    }
                    $story = $featured['story'];
                    $release = $featured['release_date'];
                    $movie_uniq_id = $featured['uniq_id'];
                    $stream_uniq_id = 0;
                    $fpermalink = $featured['fplink'];

                    $movie_content = "'" . $movie_id . ":" . $stream->series_number . ":" . $stream_id . "'";
                    $vexist = Yii::app()->common->isVoucherExists($studio_id, $movie_content);

                    if ($is_converted > 0 || $is_live == 1) {
                        if ($payment_type > 0 || $vexist != 0) {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this);" data-purchase_type="" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-purchase_type="" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">';
                            }
                        } else {
                            if ($user_id > 0) {
                                $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-purchase_type="" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
                            } else {
                                $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-purchase_type="" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $featured['uniq_id'] . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn" data-ctype="' . $content_types_id . '">';
                            }
                        }
                        $play_btn .= $controller->ServerMessage['play_now'] . '</a>';
                    } else {
                        if (intval($adv_payment)) {
                            if ($user_id > 0) {
                                $play_btn .= '<a href="javascript:void(0);" onclick="showPpvPlans(this);" data-purchase_type="" data-content-permalink="' . $permalink . '" data-movie_id="' . $featured['uniq_id'] . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $controller->Language['advance_purchase'] . '</a>';
                            } else {
                                $play_btn .= '<a href="javascript:void(0);" data-toggle="modal" data-purchase_type="" data-content-permalink="' . $permalink . '" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $featured['uniq_id'] . '" data-stream_id="0" data-isadv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $cont_name . '" class="playbtn" data-ctype="' . $content_types_id . '">' . $controller->Language['advance_purchase'] . '</a>';
                            }
                        }
                    }
                    $review_form_only = Yii::app()->general->reviewformonly($movie_id);
                    $reviewsummary = Yii::app()->general->review($movie_id);
                    $reviews = Yii::app()->general->reviews($movie_id);
                    $trailer_url = $controller->getTrailer($movie_id);
                    $trailerDetArr = movieTrailer::model()->find(array("condition" => "movie_id = " . $movie_id . " AND is_converted= 1 AND trailer_file_name !=''"));
                    $is_trailer_converted = $controller->getTrailerIsConverted($movie_id);
                    $defaultResolution = 144;
                    $ad_source = '';
                    if ($trailerDetArr) {
                        $multipleVideo = $controller->getvideoResolution($trailerDetArr['video_resolution'], $trailerDetArr['video_remote_url']);
                        $videoToBeplayed = $controller->getVideoToBePlayed($multipleVideo, $trailerDetArr['video_remote_url']);
                        $videoToBeplayed12 = explode(",", $videoToBeplayed);
                        $defaultResolution = $videoToBeplayed12[1];
                        foreach ($multipleVideo as $key => $val) {
                            $ad_source .= '<source src="' . $val . '" data-res="' . $key . '" type="video/mp4" />';
                        }
                    }
                    $MovieBanner = $controller->getPoster($movie_id, 'topbanner', 'original');
                    $content_banner = '';
                    if ($MovieBanner) {
                        $content_banner = $MovieBanner;
                    }
                }
                $release_date = self::formatReleaseDate($release);
                $full_release_date = self::formatFullReleaseDate($release);
                $short_story = Yii::app()->common->htmlchars_encode_to_html(self::formattedWords($story, 80));
                //$duration ="select video_duration,content_publish_date from movie_streams where studio_id=".$studio_id." and movie_id=".$movie_id."";
                //$stream = Yii::app()->db->createCommand($duration)->queryRow();
                if ($controller->studio->parent_theme != 'traditional-byod') {
                    $cast_detail = self::getCasts($movie_id);
                    $casts = $cast_detail['casts'];
                    $casting = $cast_detail['casting'];
                }
                if (isset($stream->content_publish_date) && @$stream->content_publish_date && $stream->content_publish_date > gmdate("Y-m-d H:i:s")) {
                    
                } else {
                    $final_content[] = array(
                        'movie_id' => $movie_id,
                        'is_episode' => $is_episode,
                        'title' => utf8_encode($cont_name),
                        'episode_number' => @$episode_number,
                        'season_number' => @$season_number,
                        'parent_content_title' => utf8_encode(@$parent_content_title),
                        'content_details' => $content_details,
                        'play_btn' => $play_btn,
                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $featured['fplink'],
                        'poster' => $poster,
                        'data_type' => $data_type,
                        'is_landscape' => $is_landscape,
                        'release_date' => $release_date,
                        'full_release_date' => $full_release_date,
                        'censor_rating' => ($featured['censor_rating'] != '') ? implode(',', json_decode($featured['censor_rating'])) . '&nbsp;' : '',
                        'movie_uniq_id' => $movie_uniq_id,
                        'stream_uniq_id' => @$featured['embed_id'],
                        'video_duration' => @$stream['video_duration'],
                        'video_duration_text' => self::videoDurationText(@$video_duration),
                        'ppv' => @$ppv->id,
                        'payment_type' => @$payment_type,
                        'is_converted' => @$is_converted,
                        'movie_stream_id' => $stream_id,
                        'uniq_id' => $movie_uniq_id,
                        'content_type_id' => @$featured['content_type_id'],
                        'content_types_id' => @$featured['content_types_id'],
                        'ppv_plan_id' => @$featured['ppv_plan_id'],
                        'full_movie' => @$full_movie,
                        'story' => utf8_encode(Yii::app()->general->showHtmlContents($story)),
                        'short_story' => utf8_encode($short_story),
                        'genres' => json_decode($featured['genre']),
                        'display_name' => '',
                        'content_permalink' => '',
                        'trailer_url' => @$trailer_url,
                        'trailer_is_converted' => @$is_trailer_converted,
                        'trailer_player' => $trailer_player,
                        'casts' => $casts,
                        'casting' => $casting,
                        'content_banner' => @$content_banner,
                        'reviewformonly' => @$review_form_only,
                        'reviewsummary' => @$reviewsummary,
                        'reviews' => json_encode(@$reviews),
                        'defaultResolution' => @$defaultResolution,
                        'multipleVideo' => @$multipleVideo,
                    );
                }
            }
            }
        }
        return $final_content;
    }

    function videoDurationText($duration) {
        $parts = explode(':', $duration);
        $res = '';
        if (count($parts) > 0) {
            if ($parts[0] != '00') {
                $dt = ltrim($parts[0], "0");
                if ($dt > 0)
                    $res.= $dt . 'hr';
            }
            if ($parts[1] != '00') {
                $dt = ltrim($parts[1], "0");
                if ($dt > 0)
                    $res.= $dt . 'm';
            }
            if ($parts[2] != '00') {
                $dt = ltrim($parts[2], "0");
                if ($dt > 0)
                    $res.= $dt . 's';
            }
        }
        return $res;
    }

    function full_url($s, $trim = '') {
        if ($trim != '') {
            $parts = explode($trim, $s);
            $s = $parts[0];
        }
        return $s;
    }

    function getCasts($movie_id = '', $actdirc = 1, $language_id = false, $studio_id = false, $translate = array()) {
        $controller = Yii::app()->controller;
        if (!$language_id) {
            $language_id = $controller->language_id;
        }
        $default_lang_id = 20;
        if ($studio_id == false) {
            $studio_id = Yii::app()->common->getStudiosId();
            $translate = $controller->Language;
        }
        $std = new Studio();
        $studio = $std->findByPk($studio_id, array('select' => 'parent_theme'));
        $template_name = $studio->parent_theme;
        if ($movie_id) {
            // $cast = MovieCast::model()->findAll(array("condition" => "movie_id = " . $movie_id));
            $sql = "SELECT mc.*,c.name,c.permalink,c.summary FROM movie_casts mc, celebrities c WHERE c.parent_id=0 AND c.studio_id = " . $studio_id . " AND mc.celebrity_id = c.id AND mc.movie_id=" . $movie_id;
            $con = Yii::app()->db;
            $cast = $con->createCommand($sql)->queryAll();
            if ($cast) {
                $i = 0;
                $casting = array();
                $cast_type = array();
                foreach ($cast AS $key => $val) {
                    $celeb_name = Yii::app()->custom->getTranslatedCastName($val['celebrity_id'], $val['name'], $language_id, $studio_id);
                    if ($actdirc) {
                        $cast_type = (json_decode(strtolower($val['cast_type'])));
                        $key = $cast_type[0];
                        $casting[$key][] = $cast_data = array(
                            'cast_type' => $key,
                            'celeb_name' => $celeb_name,
                            'celeb_image' => $controller->getPoster($val['celebrity_id'], 'celebrity', 'medium'),
                            'celeb_id' => $val['celebrity_id'],
                            'permalink' => $val['permalink'],
                            'full_permalink' => Yii::app()->getBaseUrl(true) . '/star/' . $val['permalink'],
                            'celeb_summary' => $val['summary']
                        );

                        if ($template_name == 'traditional-byod') {
                            if ($cast_type[0] == 'actor') {
                                $key = $translate['as_actor'];
                            }
                            if ($cast_type[0] == 'director') {
                                $key = 'directors';
                            }
                        } elseif ($studio_id == 1600) {
                            if ($cast_type[0] == 'actor') {
                                $key = 'actors';
                            }
                            if ($cast_type[0] == 'director') {
                                $key = 'directors';
                            }
                        } else {
                            if ($cast_type[0] == 'actor') {
                                $key = $translate['actors'];
                            }
                            if ($cast_type[0] == 'director') {
                                $key = $translate['directors'];
                            }
                        }
                        $casts[$key][] = $cast_data;
                    }
                }
                $all_casts['casts'] = $casts;
                $all_casts['casting'] = $casting;
                return $all_casts;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getCelebrityData($id) {
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $celeb = new Celebrity;
        $celeb = $celeb->findByAttributes(array('studio_id' => $studio_id, 'id' => $id));
        $casts = MovieCast::model()->findAll(array("condition" => "celebrity_id = " . $id));
        $contents = array();
        foreach ($casts as $cast) {
            $content = self::getContentData($cast->movie_id);
            $contents[] = array(
                'content_name' => $content['title'],
                'permalink' => $content['permalink'],
            );
        }

        $data = array(
            'name' => $celeb->name,
            'celebrity_type' => $celeb->celebrity_type,
            'celebrity_image' => $controller->getPoster($id, 'celebrity', 'medium'),
            'birthdate' => $celeb->birthdate,
            'birthplace' => $celeb->birthplace,
            'summary' => $celeb->summary,
            'twitterid' => $celeb->twitterid,
            'permalink' => $celeb->permalink,
            'alias_name' => $celeb->alias_name,
            'language' => $celeb->language,
            'meta_title' => $celeb->meta_title,
            'meta_description' => $celeb->meta_description,
            'meta_keywords' => $celeb->meta_keywords,
            'contents' => $contents
        );

        return $data;
    }

    function getSeasonsToPlay($movie_id) {
        $studio_id = Yii::app()->common->getStudiosId();
        $multipart_cntsetting = StudioConfig::model()->getConfig($studio_id, 'multipart_content_setting');
        $order = ($multipart_cntsetting->config_value == 1) ? 'series_number ASC' : 'series_number DESC';

        if ($movie_id) {
            $data = movieStreams::model()->findAll(array('condition' => 'movie_id =' . $movie_id . ' AND is_episode = 1 AND is_converted = 1 AND studio_id = ' . $studio_id . ' AND full_movie !=\'\' ', 'order' => $order, 'group' => 'series_number', 'select' => 'DISTINCT(series_number)'));
            return @$data;
        } else {
            return FALSE;
        }
    }

    public function StudioPaymentForm($short_code) {
        $controller = Yii::app()->controller;
        $form = '<h3>' . $controller->Language['credit_card_detail'] . '</h3>';
        $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
        $form.= '<div id="card-info-error" class="error"></div>';
        $form.= '<div class="form-group">';
        $form.= '<label for="card_name" class="col-sm-2 control-label">' . $controller->Language['text_card_name'] . '</label>';
        $form.= '<div class="col-sm-4">';
        $form.= '<input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_name'] . '" id="card_name" name="data[card_name]" required />';
        $form.= '</div>';
        $form.= '<label for="card_number" class="col-sm-2 control-label">' . $controller->Language['text_card_number'] . '</label>';
        $form.= '<div class="col-sm-4">';
        $form.= '<input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_number'] . '" id="card_number" name="data[card_number]" required />';
        $form.= '</div>';
        $form.= '</div>';
        $form.= '<div class="form-group">';
        $form.= '<label for="exp_month" class="col-sm-2 control-label">' . $controller->Language['selct_exp_date'] . '</label>';
        $form.= '<div class="col-sm-2">';
        $form.= '<select name="data[exp_month]" id="exp_month" class="form-control" required="true">';
        $form.= '<option value="">' . $controller->Language['select_month'] . '</option>';
        for ($i = 1; $i <= 12; $i++) {
            $form.= '<option value="' . $i . '">' . $months[$i - 1] . '</option>';
        }
        $form.= '</select>';
        $form.= '<div class="col-lg-12 sbox-err-lbl">';
        $form.= '<label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>';
        $form.= '</div>';
        $form.= '</div>';
        $form.= '<div class="col-sm-2">';
        $form.= '<select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">';
        $form.= '<option value="">' . $controller->Language['select_year'] . '</option>';
        for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
            $form.= '<option value="' . $i . '">' . $i . '</option>';
        }
        $form.= '</select>';
        $form.= '<div class="col-lg-12 sbox-err-lbl">';
        $form.= '<label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>';
        $form.= '</div>';
        $form.= '</div>';
        $form.= '<label for="security_code" class="col-sm-2 control-label">' . $controller->Language['text_security_code'] . '</label>';
        $form.= '<div class="col-sm-4">';
        $form.= '<input type="password" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_security_code'] . '" id="security_code" name="data[security_code]" required />';
        $form.= '</div>';
        $form.= '</div>';
        $form.= '<input type="hidden" id="payment_gateway" value="' . $short_code . '" />';
        if (isset($short_code) && $short_code != 'manual') {
            if ($short_code == 'stripe') {
                $form.= '<script type="text/javascript" src="https://js.stripe.com/v2/"></script>';
                $form.= '<script type="text/javascript">Stripe.setPublishableKey("' . $controller->PAYMENT_GATEWAY_API_PASSWORD . '");</script>';
            }
            $form.= '<script type="text/javascript" src="' . Yii::app()->getbaseUrl(true) . '/common/js/' . $short_code . '.js"></script>';
        }
        return $form;
    }

    public function review($content_id, $studio_id = false, $user_id = false) {
        $controller = Yii::app()->controller;
        if ($user_id == false) {
            $user_id = self::getUserId();
        }
        if ($studio_id == false) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $ret = '';
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        if ($studio->rating_activated == 1) {
            $rating_content = '';
            $rating_value = 0;
            $num_rating = 0;
            $rating = new ContentRating();
            $ratings = $rating->findAllByAttributes(array('studio_id' => $studio_id, 'content_id' => $content_id, 'status' => 1), array('order' => 'id desc'));
            foreach ($ratings as $rating) {
                $rating_value+= $rating->rating;
                $num_rating++;
            }
            $rat_val = 0;
            if ($num_rating > 0) {
                $rat_val = ($rating_value / $num_rating);
                $rat_val = round($rat_val, 1);
            }
            $ret.= '<div id="review_summary"><input type="hidden" class="rating" data-readonly value="' . $rat_val . '" /></div>';
        }
        return $ret;
    }

    public function reviewform($content_id, $studio_id = false, $user_id = false, $translate = array()) {
        $comment_size = 1000;
        $controller = Yii::app()->controller;
        $form = '';
        if ($user_id == false) {
            $user_id = self::getUserId();
        }
        $form = '';
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
            $translate = $controller->Language;
            $comment_size = $controller->review_comment_size > 0 ? $controller->review_comment_size : $comment_size;
        }else{
            $review_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'max_review_comment_size');
            if(empty($review_config)){
                $review_config = StudioConfig::model()->getconfigvalueForStudio(0,'max_review_comment_size');
            }
            $comment_size = $review_config['config_value'] > 0 ? $review_config['config_value'] : $comment_size;
        }
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        if ($studio->rating_activated == 1) {
            $rating_content = '';
            $all_rating = '';
            $rating = new ContentRating();
            $ratings = $rating->findAllByAttributes(array('studio_id' => $studio_id, 'content_id' => $content_id, 'status' => 1));
            $num_rating = count($ratings);
            $c = 0;
            foreach ($ratings as $rating) {
                $c++;
                if ($c < 5) {
                    $usr = new SdkUser;
                    $user = $usr->findByPk($rating->user_id);
                    $all_rating.= '<div class="rating-content">';
                    $all_rating.= '<input type="hidden" class="rating" data-readonly value="' . $rating->rating . '" />';
                    $all_rating.= '<div class="review-text">' . substr($rating->review, 0, $comment_size) . '</div>';
                    $all_rating.= '<div class="review-user">' . $user->display_name . ' at ' . Yii::app()->common->YYMMDD($rating->created_date) . '</div>';
                    $all_rating.= '</div><hr />';
                }
            }
            $form = '<h2>' . $translate['reviews'] . '</h2>';
            if ($num_rating > 0) {
                $form.= $all_rating;
            }
            if ($user_id > 0) {
                $rate = new ContentRating();
                $rate = $rate->findAllByAttributes(array('studio_id' => $studio_id, 'content_id' => $content_id, 'user_id' => $user_id));
                if (count($rate) > 0) {
                    //$form.= '<div class="submit-review">';
                    //$form.= '<p>You have already added your review for this content.</p>';
                    //$form.= '</div>';
                } else {
                    $enter_review = str_replace("50", $comment_size, $translate['enter_review_here']);
                    $form.= '<div class="submit-review">';
                    $form.= '<div class="error" id="review_error"></div>';
                    $form.= '<div class="success" id="review_message"></div>';
                    $form.= '<form name="review" id="review" method="post">';
                    $form.= '<input type="hidden" name="content_id" value="' . $content_id . '" />';
                    $form.= '<div class="form-group"><input type="hidden" class="rating" id="rating_value" name="rating" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="2" /></div>';
                    $form.= '<div class="form-group"><textarea class="form-control" name="review_comment" rows="4" id="review_comment" placeholder="' . $enter_review . '" maxlength="' . $comment_size . '"></textarea></div>';
                    $form.= '<p><input type="submit" class="btn btn-default" value="' . $translate['btn_post_review'] . '" /></p>';
                    $form.= '</form>';
                    $form.= '</div>';
                }
            } else {
                $form.= '<div class="submit-review">';
                $form.= '<p>' . $translate['need_login_to_review'] . ' <a href="#" data-toggle="modal" data-target="#loginModal">' . $translate['click_here'] . '</a> ' . $translate['to_login'] . '</p>';
                $form.= '</div>';
            }
        }
        return $form;
    }

    public function reviewformonly($content_id, $studio_id = false, $user_id = false, $translate = array()) {
        $comment_size = 1000;
        $controller = Yii::app()->controller;
        if ($user_id == false) {
            $user_id = self::getUserId();
        }
        $form = '';
        if ($studio_id == false) {
            $studio_id = Yii::app()->common->getStudiosId();
            $translate = $controller->Language;
            $comment_size = $controller->review_comment_size > 0 ? $controller->review_comment_size : $comment_size;
        }else{
            $review_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'max_review_comment_size');
            if(empty($review_config)){
                $review_config = StudioConfig::model()->getconfigvalueForStudio(0,'max_review_comment_size');
            }
            $comment_size = $review_config['config_value'] > 0 ? $review_config['config_value'] : $comment_size;
        }
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        if ($studio->rating_activated == 1) {
            if ($user_id > 0) {
                $rate = new ContentRating();
                $rate = $rate->findAllByAttributes(array('studio_id' => $studio_id, 'content_id' => $content_id, 'user_id' => $user_id));
                if (count($rate) > 0) {
                    $form.= '<div class="submit-review">';
                    $form.= '<p>' . $translate['already_added_your_review'] . '</p>';
                    $form.= '</div>';
                } else {
                    $enter_review = str_replace("50", $comment_size, $translate['enter_review_here']);
                    $form.= '<div class="submit-review">';
                    $form.= '<div class="error" id="review_error"></div>';
                    $form.= '<div class="success" id="review_message"></div>';
                    $form.= '<form name="review" id="review" method="post">';
                    $form.= '<input type="hidden" name="content_id" value="' . $content_id . '" />';
                    $form.= '<div class="form-group"><input type="hidden" class="rating" id="rating_value" name="rating" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="2" /></div>';
                    $form.= '<div class="form-group"><textarea class="form-control" name="review_comment" rows="4" id="review_comment" placeholder="' . $translate['enter_review_here'] . '" maxlength="' . $comment_size . '"></textarea></div>';
                    $form.= '<p><input type="submit" class="btn btn-default" value="Submit" /></p>';
                    $form.= '</form>';
                    $form.= '</div>';
                }
            } else {
                $form.= '<div class="submit-review">';
                $form.= '<p>' . $translate['need_login_to_review'] . ' <a href="#" data-toggle="modal" data-target="#loginModal">' . $translate['click_here'] . '</a> ' . $translate['to_login'] . '</p>';
                $form.= '</div>';
            }
        }
        return $form;
    }

    public function reviews($content_id, $studio_id = false, $user_id = false) {
        $controller = Yii::app()->controller;
        if ($user_id == false) {
            $user_id = self::getUserId();
        }
        if ($studio_id == false) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $res = array();

        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        if ($studio->rating_activated == 1) {
            $rating_content = '';
            $all_rating = '';
            $rating = new ContentRating();
            $ratings = $rating->findAllByAttributes(array('studio_id' => $studio_id, 'content_id' => $content_id, 'status' => 1));
            $num_rating = count($ratings);
            $c = 0;
            if ($num_rating > 0) {
                foreach ($ratings as $rating) {
                    $c++;
                    $usr = new SdkUser;
                    $user = $usr->findByPk($rating->user_id);
                    $profile_image = $controller->getProfilePicture($rating->user_id, 'profilepicture', 'thumb', $studio_id);
                    $res[] = array(
                        'rating_value' => $rating->rating,
                        'review' => $rating->review,
                        'username' => $user->display_name,
                        'timestamp' => Yii::app()->common->YYMMDD($rating->created_date),
                        'profilepicture' => $profile_image,
                        'rating_picture' => '<div class="rating-content"><input type="hidden" class="rating" data-readonly value="' . $rating->rating . '" /></div>'
                    );
                }
            }
        }
        return $res;
    }

    public function commentform($content_id) {
        $controller = Yii::app()->controller;
        $studio_id = Yii::app()->common->getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        $form = '';
        if ($studio->comment_activated == 1) {
            $form.= '<div class="row">';
            $form.= '<div class="comment-icon heading_2" style="margin:auto">' . $controller->Language['btn_postcomment'] . '</div><br />';
            $form.= '<div class="row-fluid">';
            $form.= '<textarea id="comment_box" placeholder="Write something here" rows="4" class="form-control input-xxxlarge"></textarea>';
            $form.= '<button type="button" class="btn btn-primary post-comment" id="comment-btn" onclick="post_comment(' . $content_id . ');">' . $controller->Language['btn_postcomment'] . '</button>';
            $form.= '</div>';
            $form.= '<div class="clearfix"></div>';
            $form.= '</div>';
        }
        return $form;
    }

    /* returns all the comments for a content */

    public function comments($movie_id) {
        $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0) ORDER BY id desc', array(':movie_id' => $movie_id));
        $return = array();
        $controller = Yii::app()->controller;
        $comments_cnt = count($comments);
        if ($comments_cnt > 0) {
            foreach ($comments as $k => $v) {
                $replies = Comment::model()->findAll('parent_id=:parent_id ORDER BY id desc', array(':parent_id' => $v['id']));

                $all_replies = array();
                foreach ($replies as $rk => $rv) {
                    $repl_user = $controller->getUserDetail($rv['user_id']);
                    $profile_image = $controller->getProfilePicture($rv['user_id'], 'profilepicture', 'thumb');
                    $all_replies[] = array(
                        'reply_id' => $rv['id'],
                        'username' => $repl_user["display_name"],
                        'profile_picture' => $profile_image,
                        'reply_since' => $controller->facebook_style($rv['created_at'], date('Y-m-d H:i:s')),
                        'message' => $rv['message']
                    );
                }
                $user_profile = $controller->getUserDetail($v['user_id']);
                $profile_image = $controller->getProfilePicture($v['user_id'], 'profilepicture', 'thumb');
                $return[] = array(
                    'comment_id' => $v['id'],
                    'username' => $user_profile["display_name"],
                    'profile_picture' => $profile_image,
                    'comment' => $v["message"],
                    'comment_since' => $controller->facebook_style($v['created_at'], date('Y-m-d H:i:s')),
                    'content_id' => $v['movie_id'],
                    'replies' => $all_replies
                );
            }
        }
        return $return;
    }

    public function newsletterform() {
        $controller = Yii::app()->controller;
        $studio_id = Yii::app()->common->getStudiosId();
        $config = new StudioConfig();
        $config = $config->getconfigvalue('newsletter');
        $form = '';
        if ($config['config_value'] == 1) {
            $form.= '<a class="newsletter-icon" onclick="javascript: toggleNewsletter();"></a>';
            $form.= '<div><div id="newsletter-container">';
            $form.= '<div class="loader" id="newsletter-loader"></div>';
            $form.= '<h2 class="newsletter-title">' . $controller->Language['subscribe_to_newsletter'] . '</h2>';
            //$form.= '<p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>';
            $form.= '<div class="subscription-form">';
            $form.= '<div id="newsletter-message"></div>';
            $form.= '<form action="#" name="newsletter-form" id="newsletter-form" role="form">';
            $form.= '<div class="form-group"><input type="email" name="newsletter-email" id="newsletter-email" class="form-control" placeholder="' . $controller->Language['text_email_placeholder'] . '" /></div>';
            $form.= '<div class="form-group"><input type="submit" id="newsletter-btn" class="btn btn-primary" value="' . $controller->Language['subscribe'] . '" /></div>';
            $form.= '</form>';
            $form.= '</div></div>';
            $form.= '</div></li>';
        }
        return $form;
    }

    public function newsletterformonly() {
        $controller = Yii::app()->controller;
        $studio_id = Yii::app()->common->getStudiosId();
        $config = new StudioConfig();
        $config = $config->getconfigvalue('newsletter');
        $form = '';
        if ($config['config_value'] == 1) {
            $form.= '<div id="newsletter-container">';
            $form.= '<div class="loader" id="newsletter-loader"></div>';
            $form.= '<div id="newsletter-message" class="error"></div>';
            $form.= '<form class="form-inline" name="newsletter-form" id="newsletter-form" method="post">';
            $form.= '<input type="email" name="newsletter-email" id="newsletter-email" placeholder="' . $controller->Language['text_email_placeholder'] . '" />';
            $form.= '<button type="submit" class="btn" id="newsletter-btn">' . $controller->Language['subscribe'] . '</button>';
            $form.= '</form>';
            $form.= '</div>';
        }
        return $form;
    }

    public function showHtmlContents($content) {
        return htmlspecialchars_decode(stripslashes($content));
    }

    public function getUserId() {
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0)
            return Yii::app()->user->id;
        else
            return 0;
    }

    public function getCSV($headArr, $sheet, $sheetName, $data, $filename = 'report', $type = 'xls') {
        require 'PHPExcel/PHPExcel.php';
        require 'PHPExcel/PHPExcel/IOFactory.php';

        $objPHPExcel = new PHPExcel();

        for ($i = 0; $i < $sheet; $i++) {
            $letters = range('A', 'Z');
            $count = 0;
            $cell_name = "";
            $objWorkSheet = $objPHPExcel->createSheet($i);
            foreach ($headArr[$i] as $tittle) {
                $cell_name = $letters[$count] . "1";
                $count++;
                $value = $tittle;
                $objWorkSheet->SetCellValue($cell_name, $value);
                $objWorkSheet->getStyle($cell_name)->getFont()->setBold(true);
            }

            $objWorkSheet->fromArray($data[$i], NULL, 'A2');
            // Rename worksheet
            $objWorkSheet->setTitle($sheetName[$i]);
        }

        $objPHPExcel->removeSheetByIndex($sheet);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        switch ($type) {
            case 'xls':
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');
                // If you're serving to IE over SSL, then the following may be needed
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
                header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header('Pragma: public'); // HTTP/1.0
                ob_clean();
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
                $rendererLibrary = 'dompdf';
                $rendererLibraryPath = dirname(__FILE__) . '/../../../' . $rendererLibrary;
                PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath);
                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
                $objWriter->save('php://output');
                break;
        }
    }

    public function csvImportWithMultiSheet($file_path) {
        require 'PHPExcel/PHPExcel.php';
        require 'PHPExcel/PHPExcel/IOFactory.php';
		$inputFileType = PHPExcel_IOFactory::identify($file_path);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($file_path);
        //$objPHPExcel = PHPExcel_IOFactory::load($file_path);
        $list = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheetTitle = $worksheet->getTitle();
            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $nrColumns = ord($highestColumn) - 64;
            $i = 0;
            for ($row = 2; $row <= $highestRow; ++$row) {
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();
                    $list[$worksheetTitle][$i][$col] = $val;
                }
                $i++;
            }
        }
        return $list;
    }

    public function updateVideoInfo($filename, $studio_id, $forVideoSync = 0, $userid = 0) {
        $arr['msg'] = "error";
        $video_gallery = new VideoManagement();
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];
        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);

        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
        $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "videogallery/";

        $dt = new DateTime();
        $upload_end_time = $dt->format('Y-m-d H:i:s');
        $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];
        $extsn = explode('.', $filename);
        $ext = $extsn[1];
        $video_gallery->video_name = $filename;
        $imgname = $extsn[0] . '.jpg';
        if ($this->studioData) {
            $studio = $this->studioData->attributes;
        } else {
            $studio = Studio::model()->getStudioBucketData($studio_id);
        }

        $newUser = $studio['new_cdn_users'];
        if ($newUser) {
            $galleryVideoUrl = $unsignedBucketPathForViewVideo . 'videogallery/' . $filename;
            $dest_file = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $imgname;
        } else {
            $galleryVideoUrl = $unsignedBucketPathForViewVideo . 'videogallery/' . $studio_id . '/' . $filename;
            $dest_file = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/videogallery-image/' . $imgname;
        }
        $video_gallery->studio_id = $studio_id;
        $video_gallery->user_id = $userid;
        $video_gallery->creation_date = new CDbExpression("NOW()");
        $video_gallery->save();
        $arr['msg'] = "Video uploaded successfully";
        $arr['video_url'] = $bucketCloudfrontUrl . '/' . $galleryVideoUrl;
        $s3viewdir = $bucketCloudfrontUrl . '/' . $galleryVideoUrl;

        //Generate video thumbnail
        $ffmpeg = FFMPEG_PATH;
        $dir2 = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/videogallery';
        if (!is_dir($dir2)) {
            mkdir($dir2);
            @chmod($dir2, 0777);
        }
        $dir1 = $dir2 . '/' . $studio_id;
        if (!is_dir($dir1)) {
            mkdir($dir1);
            @chmod($dir1, 0777);
        }
        $dir = $dir1 . '/images';
        if (!is_dir($dir)) {
            mkdir($dir);
            @chmod($dir, 0777);
        }
        $img_path = $dir . '/' . $imgname;
        $thumbImageFolder = $dir . '/thumbImage/';
        if (!is_dir($thumbImageFolder)) {
            mkdir($thumbImageFolder);
            @chmod($thumbImageFolder, 0777);
        }
        $second = 1;

        // get the duration and a random place within that
        $cmd = "$ffmpeg -i $s3viewdir 2>&1";
        if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)) {
            $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
            $second = rand(1, ($total - 1));
            $countFrame = self::ImageDuration($total);
        }

        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $command = $ffmpeg . ' -i ' . $s3viewdir . ' -vstats 2>&1';
        } else {
            $command = 'sudo ' . $ffmpeg . ' -i ' . $s3viewdir . ' -vstats 2>&1';
        }
        $output = shell_exec($command);

        $video_prop = Yii::app()->common->get_videoproperties($output, $ext);
        $resolution = $video_prop['resolution'];
        $ex_resolution = explode('x', $resolution);
        $width = $ex_resolution[0];
        $height = $ex_resolution[1];
        $maxWidth = 280;
        $maxHeight = 160;

        if ($maxWidth > $height) {
            $as_height = 160;
            $as_width = 280;
        } else {
            $as_width = floor(($width / $height) * $maxHeight);
            $as_height = $maxHeight;
        }
        $size = '560x312';
        $scale = "scale=560:312";
        /* if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
          $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf $scale $img_path -an -vcodec mjpeg 2>&1";
          } else {
          $cmd = "sudo $ffmpeg -ss $second -i $s3viewdir -frames 1 -q:v 1 -vf $scale $img_path -an -vcodec mjpeg 2>&1";
          } */

        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
        } else {
            $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
        }

        exec($cmd, $output12, $return);
        $dataForFunction['studio_id'] = $studio_id;
        $dataForFunction['video_management_id'] = $video_gallery->id;
        $dataForFunction['video_url'] = $s3viewdir;
        $dataForFunction['countFrame'] = @$countFrame;
        $generateThumbSlider = Yii::app()->general->videoGalleryThumbSlider($dataForFunction);
        $head = array_change_key_case(get_headers($s3viewdir, TRUE));
        $filesize1 = $head['content-length'];
        $filesize = Yii::app()->common->bytes2English($filesize1);
        $video_prop['FileSize'] = $filesize;
        $video_properties = json_encode($video_prop);
        require_once "Image.class.php";
        require_once "Config.class.php";
        $this->image = new Image();
        $this->image->load($img_path);
        $dimensions = array($maxWidth, $maxHeight);
        $this->image->resize($dimensions[0], $dimensions[1]);
        $this->image->save($img_path);

        $src_path = $img_path;

        $acl = 'public-read';
        $result = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key' => $dest_file,
            'SourceFile' => $src_path,
            'ACL' => $acl,
        ));
        unlink($src_path);
        $video_gallery->thumb_image_name = $imgname;
        $video_gallery->video_properties = @$video_properties;
        $video_gallery->duration = @$video_prop['duration'];
        if($generateThumbSlider == 1){
            $video_gallery-> thumbnail_status =1;  
        }
        $video_gallery->file_size = @$filesize;
        $video_gallery->save();

        if ($forVideoSync == 0) {
            echo json_encode($arr);
            exit;
        } else {
            return $video_gallery->id;
        }
    }

    public function ImageDuration($videoframeduration) {
        if ($videoframeduration < 300) {
            $total = '3/'. ($videoframeduration);
        } else {
            if ($videoframeduration < 1200) {
                $total = '8/'.($videoframeduration);
            } else {
                $total = '18/'.($videoframeduration);
            }
        }
        return $total;
    }

    public function addvideoToVideoGallery($fileName, $fullPath, $studioId) {
        $s3 = Yii::app()->common->connectToAwsS3($studioId);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studioId);
        $folderPath = Yii::app()->common->getFolderPath("", $studioId);
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $bucket = $bucketInfo['bucket_name'];
        $videoManagement = VideoManagement::model()->findByAttributes(array('studio_id' => $studioId, 'video_name' => $fileName));
        if ($videoManagement) {
            $file_name_upload = $fileName;
            $file_name_list = explode('.', $file_name_upload);
            $fileName = $file_name_list[0] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_name_list[1];
        }
        $key = $unsignedFolderPathForVideo . 'videogallery/' . $fileName;
        if ($this->studioData) {
            $studio = $this->studioData->attributes;
        } else {
            $studio = Studio::model()->getStudioBucketData($studioId);
        }
        $newUser = $studio['new_cdn_users'];
        if ($newUser == 0) {//existing user
            $key = $unsignedFolderPathForVideo . 'videogallery/' . $studioId . '/' . $fileName;
        }
        $response = $s3->copyObject(array(
            'Bucket' => $bucket,
            'Key' => $key,
            'CopySource' => $fullPath,
            'ACL' => 'public-read',
        ));
        if ($response->get('ETag') != '') {
            return Yii::app()->general->updateVideoInfo($fileName, $studioId, 1);
        }
    }

    /*
     * modified :   Arvind 
     * email    :   aravind@muvi.com
     * reason   :   Audio Gallery :  
     * functionality : addaudioToAudioGallery
     * date     :   24-1-2017
     */

    public function addaudioToAudioGallery($paraDetails) {
        $s3 = Yii::app()->common->connectToAwsS3($paraDetails['studio_id']);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $paraDetails['studio_id']);
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath("", $paraDetails['studio_id']);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $bucket = $bucketInfo['bucket_name'];
        /* $AudioManagement = AudioManagement::model()->findByAttributes(array('studio_id'=> $studioId,'audio_name' => $fileName));
          if($AudioManagement){
          $file_name_upload =$fileName;
          $file_name_list=explode('.',$file_name_upload);
          $fileName = $file_name_list[0]."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_name_list[1];
          } */
        $key = $unsignedFolderPathForVideo . 'audiogallery/' . $paraDetails['file_name'];
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        if ($new_cdn_user == 0) {//existing user
            $key = $unsignedFolderPathForVideo . 'audiogallery/' . $paraDetails['studio_id'] . '/' . $paraDetails['file_name'];
        }
        $studio = Studio::model()->getStudioBucketData($paraDetails['studio_id']);
        $newUser = $studio['new_cdn_users'];
        if ($newUser == 0) {//existing user
            $key = $unsignedFolderPathForVideo . 'audiogallery/' . $paraDetails['studio_id'] . '/' . $paraDetails['file_name'];
        }
        $fullpath = $paraDetails['file_path'] . "" . $paraDetails['file_name'];

        $response = $s3->copyObject(array(
            'Bucket' => $bucket,
            'Key' => $key,
            'CopySource' => $fullpath,
            'ACL' => 'public-read',
        ));
        if ($response->get('ETag') != '') {
            return Yii::app()->general->updateAudioInfo($paraDetails['file_path'], $paraDetails['file_name'], $paraDetails['studio_id'], $paraDetails['studio_id']);
        }
    }

    /*
     * modified :   Arvind 
     * email    :   aravind@muvi.com
     * reason   :   Audio Gallery :  
     * functionality : updateAudioInfo
     * date     :   24-1-2017
     */

    public function updateAudioInfo($filePath, $filename, $studio_id, $forAudioSync = 0, $userid = 0) {
        $arr['msg'] = "error";
        $audio_prop = Array();
        $audio_gallery = new AudioManagement();
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];
        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);

        $s3dir = $unsignedFolderPathForVideo . "audiogallery/";

        $dt = new DateTime();
        $upload_end_time = $dt->format('Y-m-d H:i:s');
        $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];

        //Arvind (aravind@muvi.com) - check audio file exist or not
        $audioManagement = AudioManagement::model()->get_audiodetails_by_studio_id($studio_id, 0, 0);
        if ($audioManagement->audio_name != null) {
            //if exist then
            $dt = new DateTime();
            $upload_end_time = $dt->format('Y-m-d H:i:s');
            $audio_gallery->audio_name = $filename . "_" . $upload_end_time;
        } else {
            //if not exist then 
            $audio_gallery->audio_name = $filename;
        }
        $studio = Studio::model()->getStudioBucketData($studio_id);
        $newUser = $studio['new_cdn_users'];

        $audio_gallery->studio_id = $studio_id;
        $audio_gallery->user_id = $userid;
        $audio_gallery->creation_date = new CDbExpression("NOW()");
        $audio_gallery->save();

        $arr['msg'] = "Audio uploaded successfully";
        $galleryAudioUrl = $unsignedBucketPathForViewVideo . 'audiogallery/' . $studio_id . '/' . $filename;
        $arr['video_url'] = $bucketCloudfrontUrl . '/' . $galleryAudioUrl;
        $s3viewdir = $bucketCloudfrontUrl . '/' . $galleryAudioUrl;
        $_fullName = $filePath . $filename;
        $duration = Yii::app()->general->getAudioDuration($_fullName);
        $head = array_change_key_case(get_headers($s3viewdir, TRUE));
        $filesize1 = $head['content-length'];
        $filesize = Yii::app()->common->bytes2English($filesize1);
        $audio_prop['FileSize'] = $filesize;
        $audio_properties = json_encode($audio_prop);
        $audio_gallery->thumb_image_name = null;
        $audio_gallery->audio_properties = null;
        $audio_gallery->duration = $duration;
        $audio_gallery->file_size = $filesize;
        $audio_gallery->save();

        if ($forAudioSync == 0) {
            echo json_encode($arr);
            exit;
        } else {
            return $audio_gallery->id;
        }
    }

    //Audio Duration 
    public function getAudioDuration($filename) {
        try {
            $ffmpeg = FFMPEG_PATH;
            if (HOST_IP != '127.0.0.1' && HOST_IP != '52.0.64.95') {
                $ffmpeg = 'sudo ' . FFMPEG_PATH;
            }
            $command = $ffmpeg . " -i " . $filename . " 2>&1 | grep Duration | awk '{print $2}' | tr -d ,";
            $output = shell_exec($command);
            $output12 = explode(".", $output);
            $duration = $output12[0] ? $output12[0] : null;
            $duration = explode("\"", $duration);
            return $duration[0];
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function signupSteps($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }

        $isSubscriptionEnabled = self::monetizationMenuSetting($studio_id);
        $return = 0;

        if (isset($isSubscriptionEnabled['menu']) && !empty($isSubscriptionEnabled['menu']) && ($isSubscriptionEnabled['menu'] & 1)) {
            $sql = "SELECT COUNT(*) AS is_subscription_plan_enabled FROM subscription_plans where studio_id={$studio_id} AND status=1";
            $dbcon = Yii::app()->db;
            $is_subscription_plan_enabled = $dbcon->createCommand($sql)->queryRow();

            if (isset($is_subscription_plan_enabled['is_subscription_plan_enabled']) && intval($is_subscription_plan_enabled['is_subscription_plan_enabled']) > 0) {
                $studioconfig = new StudioConfig;
                $config = $studioconfig->findByAttributes(array('config_key' => 'signup_steps', 'studio_id' => $studio_id));
                if (count($config) > 0 && $config->config_value == 2) {
                    $return = 2;
                } else {
                    $return = 1;
                }
            }
        }

        return $return;
    }

    public function getPartnersContentIds() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $sql = "SELECT GROUP_CONCAT(movie_id) AS movie_id FROM partners_contents WHERE studio_id = " . $studio_id . " AND user_id=" . $user_id . " GROUP BY user_id";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }

    //Added by RK for dummy data
    public function setBanners($studio_id, $source_studio_id) {
        $ip_address = CHttpRequest::getUserHostAddress();

        $studio_banner_data = new StudioBanner;
        $studio_banners = $studio_banner_data->findAllByAttributes(array('studio_id' => $source_studio_id), array('order' => 'id ASC'));
        foreach ($studio_banners as $banner) {

            $studio_banner = new StudioBanner;
            $studio_banner->title = $banner->title;
            $studio_banner->image_name = $banner->image_name;
            $studio_banner->banner_type = $banner->banner_type;
            $studio_banner->is_published = $banner->is_published;
            $studio_banner->studio_id = $studio_id;
            $studio_banner->created_date = new CDbExpression("NOW()");
            $studio_banner->banner_section = $banner->banner_section;
            $studio_banner->banner_text = $banner->banner_text;
            $studio_banner->ip = $ip_address;
            $studio_banner->save();
        }
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $target_bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
        $source_bucketInfo = $target_bucketInfo;
        $folderPath = Yii::app()->common->getFolderPath(1, $studio_id);
        $unsignedFolderPath = $folderPath['unsignedFolderPath'];
        $sourceBucket = $source_bucketInfo['bucket_name'];
        $targetBucket = $target_bucketInfo['bucket_name'];
        $s3dir = $source_studio_id . '/public/public/system/studio_banner/' . $source_studio_id . '/';
        $response = $s3->getListObjectsIterator(array(
            'Bucket' => $sourceBucket,
            'Prefix' => $s3dir
        ));
        foreach ($response as $object) {
            $key = $object['Key'];
            $key_arr = explode('/', $key);
            $batch = array();
            if ($key_arr['6'] != '') {
                $keypath = $key_arr['6'] . '/' . $key_arr['7'];
                $sourceKeyname = $source_studio_id . '/public/public/system/studio_banner/' . $source_studio_id . '/' . $keypath;
                $targetKeyname = $unsignedFolderPath . '/public/system/studio_banner/' . $studio_id . '/' . $keypath;
                $batch[] = $s3->getCommand('CopyObject', array(
                    'Bucket' => $targetBucket,
                    'Key' => $targetKeyname,
                    'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                    'ACL' => 'public-read',
                ));
            }

            $s3->execute($batch);
        }
    }

    public function setCelebrity($studio_id, $source_studio_id) {
        $studio_celebrity_data = Celebrity::model()->findAllByAttributes(array('studio_id' => $source_studio_id), array('order' => 'id ASC'));
        foreach ($studio_celebrity_data as $studio_celebrity) {
            $Celebrity = new Celebrity();
            $Celebrity->name = $studio_celebrity->name;
            $Celebrity->summary = $studio_celebrity->summary;
            $Celebrity->permalink = $studio_celebrity->permalink;
            $Celebrity->created_date = gmdate('Y-m-d H:i:s');
            $Celebrity->studio_id = $studio_id;
            $Celebrity->save();
        }
    }

    function playerWatermark($studio_id = 0, $waterMarkParam = '') {
        $waterMarkOnPlayer = '';

        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'embed_watermark');
        if (isset($getStudioConfig) && $getStudioConfig['config_value'] == 1) {
            $getWatermark = WatermarkPlayer::model()->getStudioId($studio_id);
            if ($getWatermark) {
                if ($waterMarkParam != '') {
                    $waterMarkOnPlayer.= $waterMarkParam . ':';
                }
                if ($getWatermark['email'] == 1) {
                    if (isset(Yii::app()->user->email)) {
                        $waterMarkOnPlayer.= Yii::app()->user->email . ':';
                    }
                }

                if ($getWatermark['ip'] == 1) {
                    $waterMarkOnPlayer.= $_SERVER['REMOTE_ADDR'] . ':';
                }

                if ($getWatermark['date'] == 1) {
                    $waterMarkOnPlayer.= date("F j, Y");
                }
            }
        }
        return $waterMarkOnPlayer;
    }

    function setDummymovie($studio_id, $source_studio_id, $content_type_id) {
        $ip_address = CHttpRequest::getUserHostAddress();
        $video_uploaded = 0;
        $video_file = '';
        if (isset($studio_id) && intval($studio_id)) {

            $s3 = Yii::app()->common->connectToAwsS3($studio_id);

            $target_bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $source_bucketInfo = $target_bucketInfo;

            $folderPath = Yii::app()->common->getFolderPath(1, $studio_id);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $sourceBucket = $source_bucketInfo['bucket_name'];
            $targetBucket = $target_bucketInfo['bucket_name'];

            $Films_data = new Film();
            $studio_films = $Films_data->findAllByAttributes(array('studio_id' => $source_studio_id, 'content_types_id' => $content_type_id), array('order' => 'id ASC', 'limit' => '5'));
			$arr = ContentCategories::model()->findAll(array(
						'select' => 'id',
						'condition' => 'studio_id=:studio_id AND category_name = :category_name',
						'params' => array(':studio_id' => $studio_id, ':category_name' => 'Movie'),						
						'limit' => '1'
					));
            foreach ($studio_films as $films) {
                $Films = new Film();
                $Films->studio_id = $studio_id;
                $Films->name = $films->name;
                $uniqid = Yii::app()->common->generateUniqNumber();
                $Films->uniq_id = $uniqid;
                $Films->language = $films->language;
                $Films->rating = $films->rating;
                $Films->country = $films->country;
                $Films->genre = $films->genre;
                $Films->permalink = $films->permalink;
                $Films->alias_name = $films->alias_name;
                $Films->story = $films->story;
                $Films->meta_title = $films->meta_title;
                $Films->meta_description = $films->meta_description;
                $Films->meta_keywords = $films->meta_keywords;
                $Films->content_types_id = $films->content_types_id;
                $Films->parent_content_type_id = $films->parent_content_type_id;
                $Films->content_category_value = (@$arr[0]['id'])?$arr[0]['id']:$films->content_category_value;
                $Films->censor_rating = $films->censor_rating;
                $Films->release_date = $films->release_date;
                $Films->tags = $films->tags;
                $Films->movie_payment_type = $films->movie_payment_type;
                $Films->is_verified = $films->is_verified;
                $Films->is_merged = $films->is_merged;
                $Films->ip = $ip_address;
                $Films->created_by = $films->created_by;
                $Films->created_date = gmdate('Y-m-d H:i:s');
                $Films->last_updated_by = $films->last_updated_by;
                $Films->last_updated_date = $films->last_updated_date;
                $Films->status = $films->status;
                $Films->ppv_plan_id = $films->ppv_plan_id;
                $Films->save();
                $movie_id = $Films->id;
                $dummydata_movie_id = $films->id;

                $urlRouts['permalink'] = $films->permalink;
                $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
                $urlRoutObj = new UrlRouting();
                $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);

                // upload poster
                $posters_data = Poster::model()->findAllByAttributes(array('object_id' => $dummydata_movie_id), array('order' => 'id ASC',));

                foreach ($posters_data as $poster) {
                    $posters = new Poster();
                    $posters->object_id = $movie_id;
                    $posters->object_type = $poster->object_type;
                    $posters->poster_file_name = $poster->poster_file_name;
                    $posters->ip = $ip_address;
                    $posters->created_by = Yii::app()->user->id;
                    $posters->last_updated_by = Yii::app()->user->id;
                    $posters->created_date = gmdate('Y-m-d H:i:s');
                    $posters->last_updated_date = gmdate('Y-m-d H:i:s');
                    $posters->is_poster = $poster->is_poster;
                    $posters->save();
                    $poster_id = $posters->id;

                    $s3dir = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/';
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $sourceBucket,
                        'Prefix' => $s3dir
                    ));
                    $batch = array();
                    foreach ($response as $object) {
                        $key = $object['Key'];
                        $key_arr = explode('/', $key);
                        $keypath = $key_arr['6'] . '/' . $key_arr['7'];
                        if (@$key_arr['6'] != '' && @$key_arr['7'] != '') {
                            $sourceKeyname = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/' . $keypath;
                            $targetKeyname = $unsignedFolderPath . '/public/system/posters/' . $poster_id . '/' . $keypath;
                            $batch[] = $s3->getCommand('CopyObject', array(
                                'Bucket' => $targetBucket,
                                'Key' => $targetKeyname,
                                'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                                'ACL' => 'public-read',
                            ));
                        }
                    }
                    $s3->execute($batch);
                }

                $trailer = movieTrailer::model()->findByAttributes(array('movie_id' => $dummydata_movie_id), array('order' => 'id ASC'));
                $movieTrailer = new movieTrailer();
                $movieTrailer->trailer_file_name = $trailer->trailer_file_name;
                $movieTrailer->movie_id = $movie_id;
                $movieTrailer->video_remote_url = $video_remote_url;
                $movieTrailer->created_date = gmdate('Y-m-d H:i:s');
                $movieTrailer->created_by = Yii::app()->user->id;
                $movieTrailer->last_updated_by = Yii::app()->user->id;
                $movieTrailer->last_updated_date = gmdate('Y-m-d H:i:s');
                $movieTrailer->is_converted = 1;
                $movieTrailer->has_sh = $trailer->has_sh;
                $movieTrailer->video_resolution = $trailer->video_resolution;
                $movieTrailer->resolution_size = $trailer->resolution_size;
                $movieTrailer->encoding_start_time = gmdate('Y-m-d H:i:s');
                $movieTrailer->encoding_end_time = gmdate('Y-m-d H:i:s');
                $movieTrailer->video_duration = $trailer->video_duration;
                $movieTrailer->upload_start_time = $trailer->upload_start_time;
                $movieTrailer->upload_end_time = $trailer->upload_end_time;
                $movieTrailer->mail_sent_for_trailer = 1;
                $movieTrailer->save();
                $trailer_id = $movieTrailer->id;
                if (isset($trailer_id) && $video_uploaded == 0) {
                    $s3dir = $source_studio_id . '/EncodedVideo/uploads/';
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $sourceBucket,
                        'Prefix' => $s3dir
                    ));
                    $batch = array();
                    foreach ($response as $object) {

                        $key = $object['Key'];
                        $key_arr = explode('/', $key);
                        $video_file = $keypath = $key_arr['3'];
                        if (@$key_arr['3'] != '') {
                            $sourceKeyname = $key;
                            $targetKeyname = $studio_id . '/EncodedVideo/uploads/trailers/' . $keypath;
                            $batch[] = $s3->getCommand('CopyObject', array(
                                'Bucket' => $targetBucket,
                                'Key' => $targetKeyname,
                                'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                                'ACL' => 'public-read',
                            ));
                        }
                        $s3->execute($batch);
                    }
                    $video_uploaded++;
                }


                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $bucketCloudfrontUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                $signedBucketPath = $bucketInfo['signedFolderPath'];
                $trailer_s3url = '/' . $signedBucketPath . 'uploads/trailers/' . $trailer->trailer_file_name;
                $trailer_url = $bucketCloudfrontUrl . $trailer_s3url;

                $qry = "UPDATE movie_trailer SET video_remote_url='" . $trailer_url . "' WHERE id='" . $trailer_id . "'";
                Yii::app()->db->createCommand($qry)->execute();

                $Celebrity = Celebrity::model()->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
                foreach ($Celebrity as $Celebrity_data) {
                    $MovieCast = new MovieCast();
                    $MovieCast->movie_id = $movie_id;
                    $MovieCast->celebrity_id = $Celebrity_data->id;
                    $MovieCast->cast_type = '["actor"]';
                    $MovieCast->created_date = gmdate('Y-m-d H:i:s');
                    $MovieCast->save();
                }

                $MovieStreams_data = new movieStreams();
                $studio_movie_data = $MovieStreams_data->findAllByAttributes(array('studio_id' => $source_studio_id, 'movie_id' => $dummydata_movie_id), array('order' => 'id ASC'));
                foreach ($studio_movie_data as $studio_movie) {

                    $MovieStreams = new movieStreams();
                    $MovieStreams->studio_id = $studio_id;
                    $MovieStreams->movie_id = $movie_id;
                    $MovieStreams->video_quality = $studio_movie->video_quality;
                    $MovieStreams->full_movie = $studio_movie->full_movie;
                    $MovieStreams->full_movie_url = $studio_movie->full_movie_url;
                    $MovieStreams->episode_number = $studio_movie->episode_number;
                    $MovieStreams->episode_title = $studio_movie->episode_title;
                    $MovieStreams->episode_story = $studio_movie->episode_story;
                    $MovieStreams->episode_date = $studio_movie->episode_date;
                    $MovieStreams->is_episode = $studio_movie->is_episode;
                    $MovieStreams->series_number = $studio_movie->series_number;
                    $MovieStreams->is_popular = $studio_movie->is_popular;
                    $MovieStreams->id_seq = $studio_movie->id_seq;
                    $MovieStreams->is_poster = $studio_movie->is_poster;
                    $MovieStreams->is_active = $studio_movie->is_active;
                    $MovieStreams->ip = $ip_address;
                    $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
                    $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
                    $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
                    $MovieStreams->is_converted = 1;
                    $MovieStreams->is_demo = '1';
                    $MovieStreams->mail_sent_for_video = 1;
                    $MovieStreams->save();
                    $stream_id = $MovieStreams->id;
                    if (HOST_IP != '127.0.0.1') {
                        $movie = Film::model()->findByPk($movie_id);
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $movie_id;
                        $solrArr['stream_id'] = $stream_id;
                        $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        $solrArr['name'] = $movie->name;
                        $solrArr['permalink'] = $movie->permalink;
                        $solrArr['studio_id'] = $studio_id;
                        $solrArr['display_name'] = 'Content';
                        $solrArr['content_permalink'] = $movie->permalink;
                        $solrObj = new SolrFunctions();
                        $ret = $solrObj->addSolrData($solrArr);
                    }
                }
            }
            self::setFeaturedSections($studio_id, $content_type_id);
        }
        return $video_uploaded;
    }

    public function setFeaturedSections($studio_id, $content_types_id) {
        // start feature section
        $FeaturedSection = new FeaturedSection();
        $FeaturedSection->studio_id = $studio_id;
        $FeaturedSection->title = 'Feature Content';
        $FeaturedSection->template_id = '1';
        $FeaturedSection->save();
        $section_id = $FeaturedSection->id;
        $Featured_movie = Film::model()->findAllByAttributes(array('studio_id' => $studio_id, 'content_types_id' => $content_types_id), array('order' => 'id ASC'));
        foreach ($Featured_movie as $dummycontent) {
            $FeaturedContentSave = new FeaturedContent();
            $FeaturedContentSave->studio_id = $studio_id;
            $FeaturedContentSave->movie_id = $dummycontent->id;
            $FeaturedContentSave->section_id = $section_id;
            $FeaturedContentSave->save();
        }
    }

    function setDummytv($studio_id, $source_studio_id, $content_type_id) {
        $ip_address = CHttpRequest::getUserHostAddress();
        $video_uploaded = 0;
        if (isset($studio_id) && intval($studio_id)) {

            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $target_bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $source_bucketInfo = $target_bucketInfo;
            $folderPath = Yii::app()->common->getFolderPath(1, $studio_id);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $sourceBucket = $source_bucketInfo['bucket_name'];
            $targetBucket = $target_bucketInfo['bucket_name'];

            $Films_data = new Film();
            $studio_films = $Films_data->findAllByAttributes(array('studio_id' => $source_studio_id, 'content_types_id' => $content_type_id), array('order' => 'id ASC'));
            $arr = ContentCategories::model()->findAll(array(
						'select' => 'id',
						'condition' => 'studio_id=:studio_id AND category_name = :category_name',
						'params' => array(':studio_id' => $studio_id, ':category_name' => 'TV'),						
						'limit' => '1'
					));
            foreach ($studio_films as $films) {
                $Films = new Film();
                $Films->studio_id = $studio_id;
                $Films->name = $films->name;
                $uniqid = Yii::app()->common->generateUniqNumber();
                $Films->uniq_id = $uniqid;
                $Films->language = $films->language;
                $Films->rating = $films->rating;
                $Films->country = $films->country;
                $Films->genre = $films->genre;
                $Films->permalink = $films->permalink;
                $Films->alias_name = $films->alias_name;
                $Films->story = $films->story;
                $Films->meta_title = $films->meta_title;
                $Films->meta_description = $films->meta_description;
                $Films->meta_keywords = $films->meta_keywords;
                $Films->content_types_id = $films->content_types_id;
                $Films->parent_content_type_id = $films->parent_content_type_id;
                $Films->content_category_value = (@$arr[0]['id'])?$arr[0]['id']:$films->content_category_value;
                $Films->censor_rating = $films->censor_rating;
                $Films->release_date = $films->release_date;
                $Films->tags = $films->tags;
                $Films->movie_payment_type = $films->movie_payment_type;
                $Films->is_verified = $films->is_verified;
                $Films->is_merged = $films->is_merged;
                $Films->ip = $ip_address;
                $Films->created_by = $films->created_by;
                $Films->created_date = gmdate('Y-m-d H:i:s');
                $Films->last_updated_by = $films->last_updated_by;
                $Films->last_updated_date = $films->last_updated_date;
                $Films->status = $films->status;
                $Films->ppv_plan_id = $films->ppv_plan_id;
                $Films->save();
                $movie_id = $Films->id;
                $dummydata_movie_id = $films->id;

                $urlRouts['permalink'] = $films->permalink;
                $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
                $urlRoutObj = new UrlRouting();
                $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);

                $posters_data = Poster::model()->findAllByAttributes(array('object_id' => $dummydata_movie_id));
                foreach ($posters_data as $poster) {
                    $posters = new Poster();
                    $posters->object_id = $movie_id;
                    $posters->object_type = $poster->object_type;
                    $posters->poster_file_name = $poster->poster_file_name;
                    $posters->ip = $ip_address;
                    $posters->created_by = Yii::app()->user->id;
                    $posters->last_updated_by = Yii::app()->user->id;
                    $posters->created_date = gmdate('Y-m-d H:i:s');
                    $posters->last_updated_date = gmdate('Y-m-d H:i:s');
                    $posters->wiki_data = $poster->wiki_data;
                    $posters->is_poster = $poster->is_poster;
                    $posters->save();
                    $poster_id = $posters->id;

                    $s3dir = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/';
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $sourceBucket,
                        'Prefix' => $s3dir
                    ));
                    $batch = array();
                    foreach ($response as $object) {
                        $key = $object['Key'];
                        $key_arr = explode('/', $key);
                        $keypath = $key_arr['6'] . '/' . $key_arr['7'];
                        if (@$key_arr['6'] != '' && @$key_arr['7'] != '') {
                            $sourceKeyname = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/' . $keypath;
                            $targetKeyname = $unsignedFolderPath . '/public/system/posters/' . $poster_id . '/' . $keypath;
                            $batch[] = $s3->getCommand('CopyObject', array(
                                'Bucket' => $targetBucket,
                                'Key' => $targetKeyname,
                                'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                                'ACL' => 'public-read',
                            ));
                        }
                    }
                    $s3->execute($batch);
                }

                $Celebrity = Celebrity::model()->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
                foreach ($Celebrity as $Celebrity_data) {
                    $MovieCast = new MovieCast();
                    $MovieCast->movie_id = $movie_id;
                    $MovieCast->celebrity_id = $Celebrity_data->id;
                    $MovieCast->cast_type = '["actor"]';
                    $MovieCast->created_date = gmdate('Y-m-d H:i:s');
                    $MovieCast->save();
                }

                $MovieStreams_data = new movieStreams();
                $studio_movie_data = $MovieStreams_data->findAllByAttributes(array('studio_id' => $source_studio_id, 'movie_id' => $dummydata_movie_id), array('order' => 'id ASC'));
                foreach ($studio_movie_data as $studio_movie) {
                    $MovieStreams = new movieStreams();
                    $MovieStreams->studio_id = $studio_id;
                    $MovieStreams->movie_id = $movie_id;
                    $MovieStreams->video_quality = $studio_movie->video_quality;
                    $MovieStreams->full_movie = $studio_movie->full_movie;
                    $MovieStreams->full_movie_url = $studio_movie->full_movie_url;
                    $MovieStreams->episode_number = $studio_movie->episode_number;
                    $MovieStreams->episode_title = $studio_movie->episode_title;
                    $MovieStreams->episode_story = $studio_movie->episode_story;
                    $MovieStreams->episode_date = $studio_movie->episode_date;
                    $MovieStreams->is_episode = $studio_movie->is_episode;
                    $MovieStreams->series_number = $studio_movie->series_number;
                    $MovieStreams->is_popular = $studio_movie->is_popular;
                    $MovieStreams->id_seq = $studio_movie->id_seq;
                    $MovieStreams->is_poster = $studio_movie->is_poster;
                    $MovieStreams->is_active = $studio_movie->is_active;
                    $MovieStreams->ip = $ip_address;
                    $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
                    $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
                    $MovieStreams->is_converted = 1;
                    $MovieStreams->is_demo = '1';
                    $MovieStreams->mail_sent_for_video = 1;
                    $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
                    $MovieStreams->save();
                    $stream_id = $MovieStreams->id;
                    $dummydata_stream_id = $studio_movie->id;
                    if (HOST_IP != '127.0.0.1') {
                        //$movie = Film::model()->findByPk($movie_id);
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $movie_id;
                        $solrArr['stream_id'] = $stream_id;
                        $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        if ($MovieStreams->is_episode) {
                            $contentName = $MovieStreams->episode_title ? $MovieStreams->episode_title : 'Episode ' . $MovieStreams->episode_number;
                            $contentTypeName = 'content - Episode';
                        } else {
                            $contentName = $Films->name;
                            $contentTypeName = 'content';
                        }
                        $solrArr['name'] = $contentName;
                        $solrArr['permalink'] = $Films->permalink;
                        $solrArr['studio_id'] = $studio_id;
                        $solrArr['display_name'] = $contentTypeName;
                        $solrArr['content_permalink'] = $Films->permalink;
                        $solrObj = new SolrFunctions();
                        $ret = $solrObj->addSolrData($solrArr);
                    }

                    if (isset($stream_id) && $video_uploaded == 0) {
                        $s3dir = $source_studio_id . '/EncodedVideo/uploads/';
                        $response = $s3->getListObjectsIterator(array(
                            'Bucket' => $sourceBucket,
                            'Prefix' => $s3dir
                        ));
                        $batch = array();
                        foreach ($response as $object) {

                            $key = $object['Key'];
                            $key_arr = explode('/', $key);
                            $keypath = $key_arr['3'];
                            if (@$key_arr['3'] != '') {
                                $sourceKeyname = $key;
                                $targetKeyname = $studio_id . '/EncodedVideo/uploads/' . $keypath;
                                $batch[] = $s3->getCommand('CopyObject', array(
                                    'Bucket' => $targetBucket,
                                    'Key' => $targetKeyname,
                                    'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                                    'ACL' => 'public-read',
                                ));
                            }
                            $s3->execute($batch);
                        }
                        $video_uploaded = 1;
                    }

                    $posters_data = Poster::model()->findAllByAttributes(array('object_id' => $dummydata_stream_id), array('order' => 'id ASC'));
                    foreach ($posters_data as $poster) {
                        $posters = new Poster();
                        $posters->object_id = $stream_id;
                        $posters->object_type = $poster->object_type;
                        $posters->poster_file_name = $poster->poster_file_name;
                        $posters->ip = $ip_address;
                        $posters->created_by = Yii::app()->user->id;
                        $posters->last_updated_by = Yii::app()->user->id;
                        $posters->created_date = gmdate('Y-m-d H:i:s');
                        $posters->last_updated_date = gmdate('Y-m-d H:i:s');
                        $posters->wiki_data = $poster->wiki_data;
                        $posters->is_poster = $poster->is_poster;
                        $posters->save();
                        $poster_id = $posters->id;

                        $s3dir = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/';
                        $response = $s3->getListObjectsIterator(array(
                            'Bucket' => $sourceBucket,
                            'Prefix' => $s3dir
                        ));
                        $batch = array();
                        foreach ($response as $object) {
                            $key = $object['Key'];
                            $key_arr = explode('/', $key);
                            $keypath = $key_arr['6'] . '/' . $key_arr['7'];
                            if (@$key_arr['6'] != '' && @$key_arr['7'] != '') {
                                $sourceKeyname = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/' . $keypath;
                                $targetKeyname = $unsignedFolderPath . '/public/system/posters/' . $poster_id . '/' . $keypath;
                                $batch[] = $s3->getCommand('CopyObject', array(
                                    'Bucket' => $targetBucket,
                                    'Key' => $targetKeyname,
                                    'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                                    'ACL' => 'public-read',
                                ));
                            }
                        }
                        $s3->execute($batch);
                    }
                }
            }
            self::setFeaturedSectionEpisodes($studio_id);
        }
        return $video_uploaded;
    }

    public function setFeaturedSectionEpisodes($studio_id, $studio_content_type_id) {
        // start feature section
        $FeaturedSection = new FeaturedSection();
        $FeaturedSection->studio_id = $studio_id;
        $FeaturedSection->title = 'Feature Content';
        $FeaturedSection->template_id = '1';
        $FeaturedSection->save();
        $section_id = $FeaturedSection->id;
        $Featured_movie = movieStreams::model()->findAllByAttributes(array('studio_id' => $studio_id, 'is_episode' => 1), array('order' => 'id ASC'));
        foreach ($Featured_movie as $dummycontent) {
            $FeaturedContentSave = new FeaturedContent();
            $FeaturedContentSave->studio_id = $studio_id;
            $FeaturedContentSave->movie_id = $dummycontent->id;
            $FeaturedContentSave->section_id = $section_id;
            $FeaturedContentSave->is_episode = 1;
            $FeaturedContentSave->save();
        }
    }

    public function setVideoGallery($studio_id, $source_studio_id) {
        $conn = Yii::app()->db;
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);

        $target_bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
        $source_bucketInfo = $target_bucketInfo;
        $folderPath = Yii::app()->common->getFolderPath(1, $studio_id);

        $sourceBucket = $source_bucketInfo['bucket_name'];
        $targetBucket = $target_bucketInfo['bucket_name'];

        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "videogallery/";

        $batch = array();
        $gals = VideoManagement::model()->findAllByAttributes(array('studio_id' => $source_studio_id));
        foreach ($gals as $gal) {
            $sql = 'INSERT INTO video_management (video_name, video_remote_url, thumb_image_name, video_properties, flag_uploaded, creation_date, updation_date, No_of_Times_Used) SELECT video_name, video_remote_url, thumb_image_name, video_properties, flag_uploaded, creation_date, updation_date, No_of_Times_Used FROM video_management WHERE studio_id = ' . $source_studio_id . ';';
            $conn->createCommand($sql)->execute();
            $video_id = $conn->getLastInsertId();
            $vid = new VideoManagement();
            VideoManagement::model()->updateByPk($video_id, array('studio_id' => $studio_id));

            if ($gal->video_name != '') {
                $video_name = $gal->video_name;
                $galleryVideoUrl = $s3dir . $video_name;
                $sourceKeyname = $source_studio_id . '/RawVideo/videogallery/' . $video_name;
                $batch[] = $s3->getCommand('CopyObject', array(
                    'Bucket' => $targetBucket,
                    'Key' => $galleryVideoUrl,
                    'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                    'ACL' => 'public-read',
                ));
            }
            if ($gal->thumb_image_name != '') {
                $imgname = $gal->thumb_image_name;
                $dest_file = $s3dir . 'videogallery-image/' . $imgname;
                $sourceKeyname = $source_studio_id . '/RawVideo/videogallery/videogallery-image/' . $imgname;
                $batch[] = $s3->getCommand('CopyObject', array(
                    'Bucket' => $targetBucket,
                    'Key' => $dest_file,
                    'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                    'ACL' => 'public-read',
                ));
            }
        }
        $s3->execute($batch);
    }

    //Checking if dummy data is added
    public function dummyDataCompleted($studio_id) {

        $criteria = new CDbCriteria;
        $criteria->condition = "(status = :status OR status = :nstatus) AND studio_id = :studio_id";
        $criteria->params = array(
            ':status' => '0',
            ':nstatus' => '1',
            ':studio_id' => $studio_id,
        );
        $recs = DummyData::model()->findAll($criteria);
        if (count($recs) > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getLogoDimension($studio_id) {
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $template = new Template;
        $template = $template->getTemplate($studio->parent_theme);
        $logo_dmn = StudioConfig::model()->getConfig($studio_id, 'logo_dimension');
        if (isset($logo_dmn)) {
            $logo_dimension = $logo_dmn->config_value;
            $parts = explode('x', $logo_dimension);
            $logo_width = $parts[0];
            $logo_height = $parts[1];
        } else {
            $logo_width = $template->logo_width;
            $logo_height = $template->logo_height;
        }

        $logos_dimension = array('logo_width' => $logo_width, 'logo_height' => $logo_height);
        return $logos_dimension;
    }
	public function getInvoiceLogoDimension($studio_id) {
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $template = new Template;
        $template = $template->getTemplate($studio->parent_theme);
        $logo_dmn = StudioConfig::model()->getConfig($studio_id, 'invoice_logo_dimension');
        if (isset($logo_dmn)) {
            $logo_dimension = $logo_dmn->config_value;
            $parts = explode('x', $logo_dimension);
            $logo_width = $parts[0];
            $logo_height = $parts[1];
        } else {
            $logo_width = $template->logo_width;
            $logo_height = $template->logo_height;
        }

        $logos_dimension = array('logo_width' => $logo_width, 'logo_height' => $logo_height);
        return $logos_dimension;
    }
    public function getPosterSize($studio_id) {
        /* if(isset($_SESSION['PosterSize'][$studio_id])){
          return $_SESSION['PosterSize'][$studio_id];
          } */
        $controller = Yii::app()->controller;
        $template = new Template;
        $template = $template->getTemplate($controller->studio->parent_theme);

        $getStudioConfig_h = StudioConfig::model()->getConfig($studio_id, 'h_poster_dimension');
        $getStudioConfig_v = StudioConfig::model()->getConfig($studio_id, 'v_poster_dimension');

        if (isset($getStudioConfig_h) && isset($getStudioConfig_v)) {
            $h_poster_dimension = $getStudioConfig_h->config_value;
            $v_poster_dimension = $getStudioConfig_v->config_value;
        } else if (count($template) > 0) {
            $h_poster_dimension = $template->h_poster_dimension;
            $v_poster_dimension = $template->v_poster_dimension;
        } else {
            $h_poster_dimension = 0;
            $v_poster_dimension = 0;
        }
        $data = array('h_poster_dimension' => $h_poster_dimension, 'v_poster_dimension' => $v_poster_dimension);
        $_SESSION['PosterSize'][$studio_id] = $data;
        return $data;
    }

    public function getCelebPosterSize($studio_id) {
        $celeb_dmn = StudioConfig::model()->getConfig($studio_id, 'celeb_poster_dimension');
        if (isset($celeb_dmn)) {
            $celeb_dimension = $celeb_dmn->config_value;
            $parts = explode('x', $celeb_dimension);
            $celeb_width = $parts[0];
            $celeb_height = $parts[1];
        } else {
            $celeb_width = 400;
            $celeb_height = 400;
        }

        $celebs_dimension = array('poster_width' => $celeb_width, 'poster_height' => $celeb_height);
        return $celebs_dimension;
    }

    public function getContentBannerSize($studio_id) {
        $content_dmn = StudioConfig::model()->getConfig($studio_id, 'content_banner_dimension');
        $std = new Studio();
        $studio = $std->findByPk($studio_id, array('select' => 'parent_theme'));
        $template_name = $studio->parent_theme;
        if (isset($content_dmn)) {
            $content_dimension = $content_dmn->config_value;
            $parts = explode('x', $content_dimension);
            $content_width = $parts[0];
            $content_height = $parts[1];
        } else {
            /* for traditional byod content banner */
            if ($template_name == "traditional-byod") {
                $content_width = 1506;
                $content_height = 550;
            } else {
                $content_width = 1200;
                $content_height = 350;
            }
        }

        $contents_dimension = array('banner_width' => $content_width, 'banner_height' => $content_height);
        return $contents_dimension;
    }

    public function content_count($studio_id) {
        $data = StudioContent::model()->getContentSettings($studio_id);
        return $data;
    }

    public function content_count_child($studio_id) {
        $data = StudioContent::model()->getChildContentSettings($studio_id);
        return $data;
    }

    public function apps_count($studio_id) {
        $data = StudioAppMenu::model()->getAppSettings($studio_id);
        return $data;
    }

    public function policy_count($studio_id) {
        $data = StudioConfig::model()->getConfig($studio_id, 'enable_policy');
        return $data;
    }

    public function childmenu($studio_id, $language_id, $parent_menu_item_id, $menu_id) {
        $sql = "SELECT * FROM menu_items WHERE studio_id={$studio_id} AND parent_id = {$parent_menu_item_id} AND menu_id={$menu_id} AND (language_id={$language_id} OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id={$studio_id} AND menu_id={$menu_id} AND parent_id={$parent_menu_item_id})) ORDER BY id_seq ASC ";
        $dbcon = Yii::app()->db;
        $topmenus = $dbcon->createCommand($sql)->queryAll();
        return $topmenus;
    }

    public function menuItemForm($studio_id, $item_type, $menu_id = 0, $menu_item_id = 0, $value = 0, $menu_title = '', $menu_permalink = '', $menu_parent_language = 0, $menu_language = 20, $language_id = 20) {
        if ($language_id == $menu_language && $menu_parent_language == 0) {
            $readonly = '';
            $disable = '';
        } else {
            $readonly = 'readonly';
            $disable = 'disabled';
        }
        if ($item_type != 2) {
            $readonly = 'readonly';
        }
        $message.= '<li id="list_' . $menu_item_id . '" data-id="' . $menu_item_id . '" data-name="' . $menu_title . '"><div class="panel panel-default" id="sort_' . $menu_item_id . '">
                    <div class="panel-heading" role="tab" id="heading' . $menu_item_id . '">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-accordion" href="#collapse' . $menu_item_id . '" aria-expanded="false" aria-controls="collapse' . $menu_item_id . '">' . $menu_title . '</a>
                    <a class="collapsed pull-right" role="button" data-toggle="collapse" data-parent="#res-accordion" href="#collapse' . $menu_item_id . '" aria-expanded="false" aria-controls="collapse' . $menu_item_id . '">Edit</a>
                    </h4>
                    </div>
                    <div id="collapse' . $menu_item_id . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $menu_item_id . '">
                    <div class="panel-body">';
        if ($menu_parent_language > 0) {
            $menu_item_id = $menu_parent_language;
        }
        $message.= '<form name="frmMN" class="frmMN" method="post">';
        $message.= '<input type="hidden" name="menu_id" value="' . $menu_id . '" />';
        $message.= '<input type="hidden" name="menu_item_id" value="' . $menu_item_id . '" />';
        $message.= '<input type="hidden" name="option" value="save_item" />';
        $message.= '<input type="hidden" name="item_type" value="' . $item_type . '" />';
        $message.= '<input type="hidden" name="language_id" value="' . $menu_language . '" />';
        $message.= '<div class="form-group">';
        $message.= '<label class="control-label">Title</label>';
        $message.= '<div class="fg-line"><input type="text" name="menu_title" value="' . $menu_title . '" class="form-control input-sm" /></div>';
        $message.= '</div>';
        $message.= '<div class="form-group">';
        $message.= '<label class="control-label">Permalink</label>';
        $message.= '<div class="fg-line"><input type="text" name="menu_permalink"  value="' . $menu_permalink . '" class="form-control input-sm menu_permalink" ' . $readonly . '/></div>';
        $message.= '</div><div class="form-group">';
        $message.= '<input type="submit" class="btn btn-primary btn-sm menu-item-save" data-id="' . $menu_item_id . '" value="Save" />&nbsp;';
        $message.= '<input type="button" class="btn btn-danger btn-sm menu-item-remove" data-id="' . $menu_item_id . '"  value="Remove" ' . $disable . ' /></div>';
        $message.= '</form>';

        $message.= '
                            </div>
                        </div>
                    </div>';



        return $message;
    }

    public function findMaxmenuorder($app_menu = false) {
        $table = 'menu_items';
        if($app_menu){
            $table = 'app_menu_items';
        }
        $studio_id = Yii::app()->common->getStudiosId();
        $max_menu = Yii::app()->db->createCommand()
                ->select('max(id_seq) as max_order')
                ->from($table)
                ->where('studio_id=:id', array(':id' => $studio_id))
                ->queryRow();
        $ord = $max_menu['max_order'] + 1;
        return $ord;
    }

    public function format_uri($string, $separator = '-') {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array('&' => 'and', "'" => '');
        $string = mb_strtolower(trim($string), 'UTF-8');
        $string = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }

    public function generatePermalink($title, $id = 0) {
        $studio_id = Yii::app()->common->getStudiosId();
        $delimiter = '-';
        $resevredPermalink = array('about', 'insertapplication', 'application', 'partners', 'byod', 'contactus', 'video-streaming-platform', 'live-streaming', 'admin', 'site', 'user', 'adminceleb', 'ads', 'affiliate', 'apidocs', 'awss3', 'blogs', 'category', 'contact', 'content', 'conversion', 'cron', 'dam', 'dashboard', 'embed', 'language', 'livestream', 'login', 'management', 'media', 'mergedata', 'monetization', 'movie', 'mrss', 'muvi', 'notifypaypal', 'page', 'partner', 'partners', 'payment', 'permission', 'player', 'pricing', 'report', 'rest', 'sdk', 'search', 'signup', 'site', 'star', 'template', 'test', 'ticket', 'userfeature', 'video', 'wp');

        $permalink = strtolower(trim($title, $delimiter));
        $permalink = preg_replace('@[\s!:;_\?=\\\+\*/%&#]+@', '-', $permalink);
        $permalink = trim($this->format_uri($permalink));
        $permalink = trim($permalink, $delimiter);
        if (!$permalink) {
            $permalink = 'vod-';
        }
        if (in_array($permalink, $resevredPermalink)) {
            $permalink .="-1";
        }

        $routing = New UrlRouting;
        $res = $routing->findAllByAttributes(array('studio_id' => $studio_id, 'permalink' => $permalink));
        if (count($res) > 0) {

            $format = addcslashes($permalink, '%_');
            $q = new CDbCriteria(array(
                'condition' => "permalink LIKE :match AND studio_id = :studio_id",
                'params' => array(':match' => "$format%", ':studio_id' => $studio_id),
                'order' => 'permalink DESC'
            ));

            $plinks = UrlRouting::model()->findAll($q);
            foreach ($plinks as $key => $value) {
                $pernamlinkarray[] = $value['permalink'];
            }
            $total_matched = count($plinks);
            if ($plinks[0]->id != $id) {
                $perm = $plinks[0]['permalink'];
                $format_perm = preg_replace("/\d+$/", "", $perm);
                $parts = explode($format_perm, $perm);
                $end = 1;
                if (count($parts) > 0) {
                    $end = (int) $parts[1];
                    $end++;
                }
                $permalink.= $end;
            }
            $permalink = self::recursiveSearch($pernamlinkarray, $permalink);
        }
        return $permalink;
    }

    function recursiveSearch($pernamlinkarray, $x) {
        static $permanlinkindex = 1;
        if (in_array($x, $pernamlinkarray)) {
            $x = $x . $permanlinkindex++;
            return self::recursiveSearch($pernamlinkarray, $x);
        } else {
            return $x;
        }
    }

    public function monetizationMenuSetting($studio_id) {
        $data = MonetizationMenuSettings::model()->getStudioSettings($studio_id);
        return $data;
    }

    public function PartnerCustomReport($user_id) {
        $customReportdetail_data = CustomReport::model()->getReportDetailsPartner($user_id);
        return $customReportdetail_data;
    }

    public function PartnerCustomReportcontent($user_id) {
        $customReportdetail_data = CustomReport::model()->getReportDetailsPartnercontent($user_id);
        return $customReportdetail_data;
    }

    public function cmsblocks() {
        $studio_id   = Yii::app()->common->getStudiosId();
        $returns = array();
        $blocks = CmsBlock::model()->findAll('studio_id=:studio_id AND (language_id=:language_id OR parent_id=0 AND id NOT IN (SELECT parent_id FROM cmsblocks WHERE studio_id=:studio_id AND language_id=:language_id)) ', array(':studio_id' => $studio_id,':language_id'=>Yii::app()->controller->language_id));        
        if (count($blocks) > 0) {
            foreach ($blocks as $block) {
                $code = $block->code;
                $returns["$code"] = array(
                    'content' => Yii::app()->common->htmlchars_encode_to_html($block->content),
                    'title' => $block->title
                );
            }
        }
        return $returns;
    }

    public function formattedWords($longtext, $len) {
        $result = explode(chr(0), wordwrap($longtext, $len, chr(0)));
        return @$result[0];
    }

    public function getBannerDetail() {
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $language_id = $controller->language_id;
        $sql = "SELECT * FROM banner_text WHERE studio_id={$studio_id}  AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM banner_text WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY id DESC LIMIT 0, 1";


        $banner = Yii::app()->db->createCommand($sql)->queryRow();

        return $banner;
    }

    public function getAppPrice($package) {
        $pricing = Package::model()->findByAttributes(array('code' => $package));

        return $pricing->price;
    }

    public function payment_form($gateways = array(), $plans = array()) {
        $studio_id = Yii::app()->common->getStudiosId();
        if (!isset($gateways) || !isset($plans)) {
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $gateways = $plan_payment_gateway['gateways'];
            }
        }
        $studio_config = StudioConfig::getconfigvalueForStudio($studio_id, 'third_party_coupon');
        $is_3rd_party = 0;
        if (isset($studio_config) && !empty($studio_config)) {
            $is_3rd_party = $studio_config['config_value'];
        }
        $total_gateways = count($gateways);
        $controller = Yii::app()->controller;
        $form = '';
        if (isset($gateways) && count($gateways) > 1) {
            $payment_gateway_str = implode(',', $controller->PAYMENT_GATEWAY);
            $form.='<input type="hidden" id="payment_gateway_str" value="' . $payment_gateway_str . '">';
            $PAYMENT_GATEWAY = $controller->PAYMENT_GATEWAY;
            foreach ($gateways as $gateway) {
                if ($total_gateways > 0 && $gateway->is_primary) {

                    //Start the payment form
                    if ($gateway->short_code != 'paypal') {
                        $form.='<div class="top20"></div>';

                        if (isset($gateway->short_code) && $gateway->short_code != 'manual') {
                            $form.='<input type="hidden" value="0" id="is_sepa" />';
                            if ($gateway->short_code == 'paypalpro') {
                                $form.='<h3>' . $controller->Language['credit_debit_card_detail'] . '</h3>';
                            } else {
                                $form.='<h3>' . $controller->Language['credit_card_detail'] . '</h3>';
                            }
                            $form.='<div id="card-section" style="position: relative;">';

                            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
                            $form.='<div id="card-info-error" class="error"></div>';

                            if (isset($plans) && count($plans) > 0) {
                                $form.='<div class="form-group">
                                <label for="card_name" class="col-sm-2 control-label">' . $controller->Language['text_card_name'] . '</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_name'] . '" id="card_name" name="data[card_name]" required />
                                </div>

                                <label for="card_number" class="col-sm-2 control-label">' . $controller->Language['text_card_number'] . '</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_number'] . '" id="card_number" name="data[card_number]" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exp_month" class="col-sm-2 control-label">' . $controller->Language['selct_exp_date'] . '</label>
                                <div class="col-sm-2">
                                    <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                        <option value="">' . $controller->Language['select_month'] . '</option>';
                                for ($i = 1; $i <= 12; $i++) {
                                    $form.='<option value="' . $i . '">' . $months[$i - 1] . '</option>';
                                }
                                $form.='        </select>
                                    <div class="col-lg-12 sbox-err-lbl">
                                        <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
                                        <option value="">' . $controller->Language['select_year'] . '</option>';
                                for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
                                    $form.='            <option value="' . $i . '">' . $i . '</option>';
                                }
                                $form.='        </select>';
                                $form.='        <div class="col-lg-12 sbox-err-lbl">';
                                $form.='            <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>';
                                $form.='        </div>';
                                $form.='    </div>';

                                $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['text_security_code'] . '</label>';
                                $form.='    <div class="col-sm-4">';
                                $form.='        <input type="password" id="" name="" style="display: none;" />';
                                $form.='        <input type="password" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_security_code'] . '" id="security_code" name="data[security_code]" required />';
                                $form.='    </div>';
                            } else {
                                $form.= '<div class="form-group">
                            <div class="col-sm-6 pull-left">
                                <label for="card_name">' . $controller->Language['text_card_name'] . '</label>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <label for="card_number">' . $controller->Language['text_card_number'] . '</label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6 pull-left">
                                <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_name'] . '" id="card_name" name="data[card_name]" required />
                            </div>
                            <div class="col-sm-6 pull-left">
                                <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_number'] . '" id="card_number" name="data[card_number]" required />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 pull-left">
                                <label for="exp_month">' . $controller->Language['selct_exp_date'] . '</label>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <label for="security_code">' . $controller->Language['text_security_code'] . '</label>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6 pull-left">                                
                                <div class="col-sm-6" style="padding: 0">
                                    <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                        <option value="">' . $controller->Language['select_month'] . '</option>';
                                for ($i = 1; $i <= 12; $i++) {
                                    $form.= '           <option value="' . $i . '">' . $months[$i - 1] . '</option>';
                                }
                                $form.= '       </select>
                                    <div class="sbox-err-lbl">
                                        <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
                                    </div>                        
                                </div>
                                <div class="col-sm-6" style="padding-left: 5px;padding-right: 0;">
                                    <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
                                        <option value="">' . $controller->Language['select_year'] . '</option>';
                                for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
                                    $form.= '           <option value="' . $i . '">' . $i . '</option>';
                                }
                                $form.= '           </select>
                                    <div class="sbox-err-lbl">
                                        <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>
                                    </div>                          
                                </div>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <input type="password" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_security_code'] . '" id="security_code" name="data[security_code]" required />
                            </div>
                            <div class="clearfix"></div>
                        </div>';
                            }

                            //End card section
                            $form.='</div>';
                            $form.='</div>';
                        }
                    } else {
                        $form.='<div class="clearfix"></div>';
                        $form.='<div class="form-group">';
                        $form.='    <div class="col-sm-4">';
                        $form.='        <div class="checkbox">';
                        $form.='            <label>';
                        $form.='                <input type="checkbox" id="have_paypal" name="data[have_paypal]" value="1" class="process-check" />';
                        $form.='                <label for="have_paypal" class="caps">' . $controller->Language['chk_have_paypal_ac'] . '</label>';
                        $form.='            </label>';
                        $form.='        </div>';
                        $form.='    </div>';
                        $form.='</div>';
                    }
                    $email = isset(Yii::app()->user->email) ? Yii::app()->user->email : '';
                    $form.='<input type="hidden" id="payment_gateway" value="' . $gateway->short_code . '" />';

                    if (Yii::app()->controller->action->id != 'register') {
                        $form.='<input type="hidden" id="email" name="data[email]" value="' . $email . '" />';
                    }
                    $form.='<div class="clearfix"></div>';
                    $form.='<span class="error" id="plan_error"></span><div id="card_div"></div>';



                    if (isset($gateway->short_code) && $gateway->short_code != 'manual') {
                        $form.='<script type="text/javascript" src="' . Yii::app()->getbaseUrl(true) . '/common/js/' . $gateway->short_code . '.js"></script>';
                    }
                }
            }
            if (array_key_exists('paypalpro', $PAYMENT_GATEWAY)) {
                $form.='<div class="form-group">';
                $form.='    <div class="col-sm-4">';
                $form.='        <div class="checkbox"><label>';
                $form.='            <input type="checkbox" id="have_paypal_pro" name="have_paypal" />';
                $form.='            ' . $controller->Language['chk_have_paypal_ac'];
                $form.='        </label></div>';
                $form.='    </div>';
                $form.='</div>';
                $form.='<script type="text/javascript" src="' . Yii::app()->getbaseUrl(true) . '/common/js/' . $PAYMENT_GATEWAY['paypalpro'] . '.js"></script>';
            }
        } else {
            $payment_gateway_str = implode(',', $controller->PAYMENT_GATEWAY);
            $form.='<input type="hidden" id="payment_gateway_str" value="' . $payment_gateway_str . '">';
            //Start the payment form
            $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
            if (isset($gateway_code) && trim($gateway_code)) {
                
            } else {
                $gateway_code = $gateways[0]->short_code;
            }
            //if((isset($controller->PAYMENT_GATEWAY) && !array_key_exists('paypalpro', $controller->PAYMENT_GATEWAY) && $count==1) || (isset($controller->IS_PRIMARY) && array_key_exists('paypalpro', $controller->IS_PRIMARY) && $controller->IS_PRIMARY['paypalpro'] == 1) || (isset($controller->PAYMENT_GATEWAY) && array_key_exists('paypalpro', $controller->PAYMENT_GATEWAY) && $controller->IS_PRIMARY['paypalpro'] == 0 &&  $count==2)){
            if ($controller->IS_PAYPAL_EXPRESS[$gateway_code] == 0) {
                if ($controller->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
                    $form.='<div class="top20"></div>';

                    //if (isset($gateway_code) && $gateway_code == 'stripe') {
                    if ((isset($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && $controller->API_ADDONS_OTHERGATEWAY[$gateway_code]['sepa']) || $is_3rd_party) {
                        $form.='<ul class="nav nav-tabs" role="tablist">';
                        $form.='<li role="presentation" class="active" id="credit_card_control">';
                        $form.='<a href="#credit_card" aria-controls="credit_card" role="tab" data-toggle="tab" aria-expanded="true">' . $controller->Language['credit_cards'] . '</a>';
                        $form.='</li>';
                        if ($is_3rd_party) {
                            $form.='<li role="presentation" class="" id="third_party_coupon_control">';
                            $form.='<a href="#third_party_coupon" aria-controls="third_party_coupon" role="tab" data-toggle="tab" aria-expanded="false">' . $controller->Language['external_coupon'] . '</a>';
                            $form.='</li>';
                        } else {
                            $form.='<li role="presentation" class="" id="sepa_control">';
                            $form.='<a href="#sepa" aria-controls="sepa" role="tab" data-toggle="tab" aria-expanded="false">SEPA</a>';
                            $form.='</li>';
                        }
                        $form.='</ul>';
                    }
                    // }
                    if (isset($gateways[0]->short_code) && $gateways[0]->short_code != 'manual') {

                        if (isset($controller->API_ADDONS_REDIRECT[$gateway_code]['redirect']) && intval($controller->API_ADDONS_REDIRECT[$gateway_code]['redirect'])) {
                            
                        } else {

                            if ((isset($gateway_code) && $gateway_code == 'stripe' && isset($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && $controller->API_ADDONS_OTHERGATEWAY[$gateway_code]['sepa']) || $is_3rd_party) {
                                
                            } else {
                                if ($controller->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro') {
                                    $form.='<h3>' . $controller->Language['credit_debit_card_detail'] . '</h3>';
                                } else {
                                    $form.='<h3>' . $controller->Language['credit_card_detail'] . '</h3>';
                                }
                            }

                            if (isset($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($controller->API_ADDONS_OTHERGATEWAY[$gateway_code])) {
                                $form.='<div id="card-section" style="position: relative;">';
                                $form.='<div class="loader" id="othergateway_loading" style="z-index:10;"></div>';
                            } else {
                                $form.='<div id="card-section">';
                            }
                            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
                            $form.='<div id="card-info-error" class="error"></div>';
                            $country = "<select id='country' name='data[country]' class='form-control' required='true'><option value=''>Select</option><option value='AF'>Afghanistan</option><option value='AX'>Åland Islands</option><option value='AL'>Albania</option><option value='DZ'>Algeria</option><option value='AS'>American Samoa</option><option value='AD'>Andorra</option><option value='AO'>Angola</option><option value='AI'>Anguilla</option><option value='AQ'>Antarctica</option><option value='AG'>Antigua and Barbuda</option><option value='AR'>Argentina</option><option value='AM'>Armenia</option><option value='AW'>Aruba</option><option value='AU'>Australia</option><option value='AT'>Austria</option><option value='AZ'>Azerbaijan</option><option value='BS'>Bahamas</option><option value='BH'>Bahrain</option><option value='BD'>Bangladesh</option><option value='BB'>Barbados</option><option value='BY'>Belarus</option><option value='BE'>Belgium</option><option value='BZ'>Belize</option><option value='BJ'>Benin</option><option value='BM'>Bermuda</option><option value='BT'>Bhutan</option><option value='BO'>Bolivia</option><option value='BQ'>Bonaire, Sint Eustatius and Saba</option><option value='BA'>Bosnia and Herzegovina</option><option value='BW'>Botswana</option><option value='BV'>Bouvet Island</option><option value='BR'>Brazil</option><option value='IO'>British Indian Ocean Territory</option><option value='BN'>Brunei Darussalam</option><option value='BG'>Bulgaria</option><option value='BF'>Burkina Faso</option><option value='BI'>Burundi</option><option value='KH'>Cambodia</option><option value='CM'>Cameroon</option><option value='CA'>Canada</option><option value='CV'>Cape Verde</option><option value='KY'>Cayman Islands</option><option value='CF'>Central African Republic</option><option value='TD'>Chad</option><option value='CL'>Chile</option><option value='CN'>China</option><option value='CX'>Christmas Island</option><option value='CC'>Cocos (Keeling Islands)</option><option value='CO'>Colombia</option><option value='KM'>Comoros</option><option value='CG'>Congo</option><option value='CK'>Cook Islands</option><option value='CR'>Costa Rica</option><option value='CI'>Cote D'Ivoire (Ivory Coast)</option><option value='HR'>Croatia (Hrvatska)</option><option value='CU'>Cuba</option><option value='CW'>Curaçao</option><option value='CY'>Cyprus</option><option value='CZ'>Czech Republic</option><option value='CD'>Democratic Republic of the Congo</option><option value='DK'>Denmark</option><option value='DJ'>Djibouti</option><option value='DM'>Dominica</option><option value='DO'>Dominican Republic</option><option value='TP'>East Timor</option><option value='EC'>Ecuador</option><option value='EG'>Egypt</option><option value='SV'>El Salvador</option><option value='GQ'>Equatorial Guinea</option><option value='ER'>Eritrea</option><option value='EE'>Estonia</option><option value='ET'>Ethiopia</option><option value='FK'>Falkland Islands (Malvinas)</option><option value='FO'>Faroe Islands</option><option value='FJ'>Fiji</option><option value='FI'>Finland</option><option value='FR'>France</option><option value='GF'>French Guiana</option><option value='PF'>French Polynesia</option><option value='TF'>French Southern Territories</option><option value='GA'>Gabon</option><option value='GM'>Gambia</option><option value='GE'>Georgia</option><option value='DE'>Germany</option><option value='GH'>Ghana</option><option value='GI'>Gibraltar</option><option value='GR'>Greece</option><option value='GL'>Greenland</option><option value='GD'>Grenada</option><option value='GP'>Guadeloupe</option><option value='GU'>Guam</option><option value='GT'>Guatemala</option><option value='GG'>Guernsey</option><option value='GN'>Guinea</option><option value='GW'>Guinea-Bissau</option><option value='GY'>Guyana</option><option value='HT'>Haiti</option><option value='HM'>Heard and McDonald Islands</option><option value='HN'>Honduras</option><option value='HK'>Hong Kong</option><option value='HU'>Hungary</option><option value='IS'>Iceland</option><option value='IN'>India</option><option value='ID'>Indonesia</option><option value='IR'>Iran, Islamic Republic of</option><option value='IQ'>Iraq</option><option value='IE'>Ireland</option><option value='IM'>Isle of Man</option><option value='IL'>Israel</option><option value='IT'>Italy</option><option value='JM'>Jamaica</option><option value='JP'>Japan</option><option value='JE'>Jersey</option><option value='JO'>Jordan</option><option value='KZ'>Kazakhstan</option><option value='KE'>Kenya</option><option value='KI'>Kiribati</option><option value='KP'>Korea (North) (Democratic People's Republic)</option><option value='KR'>Korea (South) (Republic)</option><option value='KW'>Kuwait</option><option value='KG'>Kyrgyzstan (Kyrgyz Republic)</option><option value='LA'>Laos (Lao People's Democratic Republic)</option><option value='LV'>Latvia</option><option value='LB'>Lebanon</option><option value='LS'>Lesotho</option><option value='LR'>Liberia</option><option value='LY'>Libya</option><option value='LI'>Liechtenstein</option><option value='LT'>Lithuania</option><option value='LU'>Luxembourg</option><option value='MO'>Macau (Macao)</option><option value='MK'>Macedonia, the Former Yugoslav Republic of</option><option value='MG'>Madagascar</option><option value='MW'>Malawi</option><option value='MY'>Malaysia</option><option value='MV'>Maldives</option><option value='ML'>Mali</option><option value='MT'>Malta</option><option value='MH'>Marshall Islands</option><option value='MQ'>Martinique</option><option value='MR'>Mauritania</option><option value='MU'>Mauritius</option><option value='YT'>Mayotte</option><option value='MX'>Mexico</option><option value='FM'>Micronesia, Federated States of</option><option value='MD'>Moldova, Republic of</option><option value='MC'>Monaco</option><option value='MN'>Mongolia</option><option value='ME'>Montenegro</option><option value='MS'>Montserrat</option><option value='MA'>Morocco</option><option value='MZ'>Mozambique</option><option value='MM'>Myanmar</option><option value='NA'>Namibia</option><option value='NR'>Nauru</option><option value='NP'>Nepal</option><option value='NL'>Netherlands</option><option value='NC'>New Caledonia</option><option value='NZ'>New Zealand</option><option value='NI'>Nicaragua</option><option value='NE'>Niger</option><option value='NG'>Nigeria</option><option value='NU'>Niue</option><option value='NF'>Norfolk Island</option><option value='MP'>Northern Mariana Islands</option><option value='NO'>Norway</option><option value='OM'>Oman</option><option value='PK'>Pakistan</option><option value='PW'>Palau</option><option value='PS'>Palestine, State of</option><option value='PA'>Panama</option><option value='PG'>Papua New Guinea</option><option value='PY'>Paraguay</option><option value='PE'>Peru</option><option value='PH'>Philippines</option><option value='PN'>Pitcairn</option><option value='PL'>Poland</option><option value='PT'>Portugal</option><option value='PR'>Puerto Rico</option><option value='QA'>Qatar</option><option value='RE'>Reunion</option><option value='RO'>Romania</option><option value='RU'>Russian Federation</option><option value='RW'>Rwanda</option><option value='BL'>Saint Barthelemy</option><option value='SH'>Saint Helena, Ascension and Tristan Da Cunha</option><option value='KN'>Saint Kitts and Nevis</option><option value='LC'>Saint Lucia</option><option value='MF'>Saint Martin (French part)</option><option value='PM'>Saint Pierre and Miquelon</option><option value='VC'>Saint Vincent and The Grenadines</option><option value='WS'>Samoa</option><option value='SM'>San Marino</option><option value='ST'>Sao Tome and Principe</option><option value='SA'>Saudi Arabia</option><option value='SN'>Senegal</option><option value='RS'>Serbia</option><option value='SC'>Seychelles</option><option value='SL'>Sierra Leone</option><option value='SG'>Singapore</option><option value='SX'>Sint Maarten (Dutch Part)</option><option value='SK'>Slovakia (Slovak Republic)</option><option value='SI'>Slovenia</option><option value='SB'>Solomon Islands</option><option value='SO'>Somalia</option><option value='ZA'>South Africa</option><option value='GS'>South Georgia and South Sandwich Islands</option><option value='SS'>South Sudan</option><option value='SU'>Soviet Union (former)</option><option value='ES'>Spain</option><option value='LK'>Sri Lanka</option><option value='SD'>Sudan</option><option value='SR'>Suriname</option><option value='SJ'>Svalbard and Jan Mayen Islands</option><option value='SZ'>Swaziland</option><option value='SE'>Sweden</option><option value='CH'>Switzerland</option><option value='SY'>Syrian Arab Republic</option><option value='TW'>Taiwan, Province of China</option><option value='TJ'>Tajikistan</option><option value='TZ'>Tanzania, United Republic of</option><option value='TH'>Thailand</option><option value='TL'>Timor-Leste</option><option value='TG'>Togo</option><option value='TK'>Tokelau</option><option value='TO'>Tonga</option><option value='TT'>Trinidad and Tobago</option><option value='TN'>Tunisia</option><option value='TR'>Turkey</option><option value='TM'>Turkmenistan</option><option value='TC'>Turks and Caicos Islands</option><option value='TV'>Tuvalu</option><option value='UG'>Uganda</option><option value='UA'>Ukraine</option><option value='AE'>United Arab Emirates</option><option value='GB'>United Kingdom (Great Britain)</option><option value='US'>United States</option><option value='UM'>United States Minor Outlying Islands</option><option value='UY'>Uruguay</option><option value='UZ'>Uzbekistan</option><option value='VU'>Vanuatu</option><option value='VA'>Vatican City State (Holy See)</option><option value='VE'>Venezuela</option><option value='VN'>Viet Nam</option><option value='VG'>Virgin Islands (British)</option><option value='VI'>Virgin Islands (US)</option><option value='WF'>Wallis and Futuna Islands</option><option value='EH'>Western Sahara</option><option value='YE'>Yemen</option><option value='ZM'>Zambia</option><option value='ZW'>Zimbabwe</option></select>";
                            if (isset($plans) && count($plans) > 0) {

                                if ((isset($gateway_code) && $gateway_code == 'stripe' && isset($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && $controller->API_ADDONS_OTHERGATEWAY[$gateway_code]['sepa']) || $is_3rd_party) {
                                    $form.='<div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="credit_card">
                                        <h3>' . $controller->Language['credit_card_detail'] . '</h3>
                                          <div class="m-t-20">';
                                    $form.='<div class="form-group">
                                    <label for="card_name" class="col-sm-2 control-label">' . $controller->Language['text_card_name'] . '</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_name'] . '" id="card_name" name="data[card_name]" required />
                                    </div>

                                    <label for="card_number" class="col-sm-2 control-label">' . $controller->Language['text_card_number'] . '</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_number'] . '" id="card_number" name="data[card_number]" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exp_month" class="col-sm-2 control-label">' . $controller->Language['selct_exp_date'] . '</label>
                                    <div class="col-sm-2">
                                        <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                            <option value="">' . $controller->Language['select_month'] . '</option>';
                                    for ($i = 1; $i <= 12; $i++) {
                                        $form.='<option value="' . $i . '">' . $months[$i - 1] . '</option>';
                                    }
                                    $form.='        </select>
                                            <div class="col-lg-12 sbox-err-lbl">
                                                <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
                                                <option value="">' . $controller->Language['select_year'] . '</option>';
                                    for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
                                        $form.='            <option value="' . $i . '">' . $i . '</option>';
                                    }
                                    $form.='        </select>';
                                    $form.='        <div class="col-lg-12 sbox-err-lbl">';
                                    $form.='            <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>';
                                    $form.='        </div>';
                                    $form.='    </div>';

                                    $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['text_security_code'] . '</label>';
                                    $form.='    <div class="col-sm-4">';
                                    $form.='        <input type="password" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_security_code'] . '" id="security_code" name="data[security_code]" required />';
                                    $form.='    </div>';
                                    $form.='  </div>';
                                    if (isset($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($controller->API_ADDONS_OTHERGATEWAY[$gateway_code]) && $controller->API_ADDONS_OTHERGATEWAY[$gateway_code]['paypal'] == 1) {
                                        $form.='<div class="form-group" id="other-gateway">';
                                        $form.='<div class="col-sm-12 pull-left">';
                                        $form.='<div id="paypal-container"></div>';
                                        $form.='</div></div>';
                                    }
                                    $form.='  </div></div>';

                                    if ($is_3rd_party) {

                                        $form.='  <div role="tabpanel" class="tab-pane" id="third_party_coupon">
                                            <h3>' . $controller->Language['external_coupon_header'] . '</h3>
                                              <div class="m-t-20 m-b-20">';

                                        $form.='    <div class="form-group">';
                                        $form.='    <label for="ext_coupon" class="col-sm-2 control-label col-sm-3">' . $controller->Language['external_coupon_code'] . '</label>';
                                        $form.='    <div class="col-sm-4 col-sm-8">';
                                        $form.='        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['external_coupon_code'] . '" id="ext_coupon" name="data[ext_coupon]" required />';
                                        $form.='    </div>';


                                        $form.='  </div>
                                        </div></div>';
                                    } else {

                                        $form.='  <div role="tabpanel" class="tab-pane" id="sepa">
                                            <h3>' . $controller->Language['international_bank_account_details'] . '</h3>
                                              <div class="m-t-20">';
                                        $form.='<div class="form-group">
                                        <label for="card_name" class="col-sm-2 control-label">' . $controller->Language['name_on_bank_account'] . '</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['name_on_bank_account'] . '" id="account_name" name="data[card_name]" required />
                                        </div>

                                        <label for="card_number" class="col-sm-2 control-label">' . $controller->Language['international_bank_account_number'] . '</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['international_bank_account_number'] . '" id="iban" name="data[iban]" required />
                                        </div>
                                    </div>';

                                        $form.='    <div class="form-group">';
                                        $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['street_address'] . '</label>';
                                        $form.='    <div class="col-sm-4">';
                                        $form.='        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['street_address'] . '" id="street" name="data[street]" required />';
                                        $form.='    </div>';

                                        $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['city_address'] . '</label>';
                                        $form.='    <div class="col-sm-4">';
                                        $form.='        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['city_address'] . '" id="city" name="data[city]" required />';
                                        $form.='    </div> </div>';

                                        $form.='    <div class="form-group">';
                                        $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['postal_code_address'] . '</label>';
                                        $form.='    <div class="col-sm-4">';
                                        $form.='        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['postal_code_address'] . '" id="postalcode" name="data[postalcode]" required />';
                                        $form.='    </div>';

                                        $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['country_address'] . '</label>';
                                        $form.='    <div class="col-sm-4">';
                                        $form.= $country;
                                        $form.='    </div>';
                                        $form.='  </div>
                                        </div></div>';
                                    }
                                } else {
                                    $form.='<div class="form-group">
                                    <label for="card_name" class="col-sm-2 control-label">' . $controller->Language['text_card_name'] . '</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_name'] . '" id="card_name" name="data[card_name]" required />
                                    </div>

                                    <label for="card_number" class="col-sm-2 control-label">' . $controller->Language['text_card_number'] . '</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_number'] . '" id="card_number" name="data[card_number]" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exp_month" class="col-sm-2 control-label">' . $controller->Language['selct_exp_date'] . '</label>
                                    <div class="col-sm-2">
                                        <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                            <option value="">' . $controller->Language['select_month'] . '</option>';
                                    for ($i = 1; $i <= 12; $i++) {
                                        $form.='<option value="' . $i . '">' . $months[$i - 1] . '</option>';
                                    }
                                    $form.='        </select>
                                            <div class="col-lg-12 sbox-err-lbl">
                                                <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
                                                <option value="">' . $controller->Language['select_year'] . '</option>';
                                    for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
                                        $form.='            <option value="' . $i . '">' . $i . '</option>';
                                    }
                                    $form.='        </select>';
                                    $form.='        <div class="col-lg-12 sbox-err-lbl">';
                                    $form.='            <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>';
                                    $form.='        </div>';
                                    $form.='    </div>';

                                    $form.='    <label for="security_code" class="col-sm-2 control-label">' . $controller->Language['text_security_code'] . '</label>';
                                    $form.='    <div class="col-sm-4">';
                                    $form.='        <input type="password" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_security_code'] . '" id="security_code" name="data[security_code]" required />';
                                    $form.='    </div>';
                                }
                            } else {
                                $form.= '<div class="form-group">
                                <div class="col-sm-6 pull-left">
                                    <label for="card_name">' . $controller->Language['text_card_name'] . '</label>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label for="card_number">' . $controller->Language['text_card_number'] . '</label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6 pull-left">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_name'] . '" id="card_name" name="data[card_name]" required />
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_card_number'] . '" id="card_number" name="data[card_number]" required />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 pull-left">
                                    <label for="exp_month">' . $controller->Language['selct_exp_date'] . '</label>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label for="security_code">' . $controller->Language['text_security_code'] . '</label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6 pull-left">                                
                                    <div class="col-sm-6" style="padding: 0">
                                        <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                            <option value="">' . $controller->Language['select_month'] . '</option>';
                                for ($i = 1; $i <= 12; $i++) {
                                    $form.= '           <option value="' . $i . '">' . $months[$i - 1] . '</option>';
                                }
                                $form.= '       </select>
                                        <div class="sbox-err-lbl">
                                            <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
                                        </div>                        
                                    </div>
                                    <div class="col-sm-6" style="padding-left: 5px;padding-right: 0;">
                                        <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
                                            <option value="">' . $controller->Language['select_year'] . '</option>';
                                for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
                                    $form.= '           <option value="' . $i . '">' . $i . '</option>';
                                }
                                $form.= '           </select>
                                        <div class="sbox-err-lbl">
                                            <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>
                                        </div>                          
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <input type="password" class="form-control" autocomplete="off" placeholder="' . $controller->Language['text_security_code'] . '" id="security_code" name="data[security_code]" required />
                                </div>
                                <div class="clearfix"></div>
                            </div>';
                            }
                            //End card section
                            $form.='</div></div>';
                        }
                    }
                } else {
                    $form.='<div class="clearfix"></div>';
                    $form.='<div class="form-group">';
                    $form.='    <div class="col-sm-4">';
                    $form.='        <div class="checkbox">';
                    $form.='            <label>';
                    $form.='                <input type="checkbox" id="have_paypal" name="data[have_paypal]" value="1" class="process-check" />';
                    $form.='                <label for="have_paypal" class="caps">' . $controller->Language['chk_have_paypal_ac'] . '</label>';
                    $form.='            </label>';
                    $form.='        </div>';
                    $form.='    </div>';
                    $form.='</div>';
                }
            } else {
                $form = ' <input id="payment_gateway" value="paypalpro" autocomplete="false" type="hidden">
                 <input id="payment_gateway_str" value="paypalpro" autocomplete="false" type="hidden">';
                $form.='<script type="text/javascript" src="' . Yii::app()->getbaseUrl(true) . '/common/js/' . $controller->PAYMENT_GATEWAY['paypalpro'] . '.js"></script>';
                $form.='<input type="hidden" id="have_paypal" name="data[have_paypal]" value="1" class="process-check" />';
                $form.='<input type="checkbox" id="have_paypal_pro" name="have_paypal" value="1" checked style="display:none" />';
            }
            $email = isset(Yii::app()->user->email) ? Yii::app()->user->email : '';
            $form.='<input type="hidden" id="payment_gateway" value="' . $gateways[0]->short_code . '" />';

            if (Yii::app()->controller->action->id != 'register') {
                $form.='<input type="hidden" id="email" name="data[email]" value="' . $email . '" />';
            }
            $form.='<div class="clearfix"></div>';
            $form.='<span class="error" id="plan_error"></span><div id="card_div"></div>';

            if ($controller->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && $controller->IS_PAYPAL_EXPRESS[$gateway_code] == 0) {
                $form.='<div class="form-group">';
                $form.='    <div class="col-sm-4">';
                $form.='        <div class="checkbox"><label>';
                $form.='            <input type="checkbox" id="have_paypal_pro" name="have_paypal" />';
                $form.='            ' . $controller->Language['chk_have_paypal_ac'];
                $form.='        </label></div>';
                $form.='    </div>';
                $form.='</div>';
            }
            if (isset($controller->PAYMENT_GATEWAY[$gateway_code]) && $controller->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $form.='<script type="text/javascript" src="' . Yii::app()->getbaseUrl(true) . '/common/js/' . $controller->PAYMENT_GATEWAY[$gateway_code] . '.js"></script>';
            }
        }
        $form.='<input type="hidden" value="0" id="is_sepa" />';
        $form.= '<div id="viewBundleContent"  class="modal fade login-popu" data-backdrop="static" data-keyboard="false">';
        $form.= '<div class="modal-dialog">';
        $form.= '<div class="modal-content">';
        $form.= '<div class="modal-header">';
        $form.= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $form.= '<h4 class="modal-title">&nbsp;</h4>';
        $form.= '</div>';
        $form.= '<div class="modal-body" id="viewbundlescontent"><div class="loader loader_confirm" id="loader" style="display: none;"></div>';
        $form.= '<br><br><div align="center"><button type="button" class="btn btn-primary bold_class" id="dismisShowseasonconfirm">' . rtyhrt . '</button>&nbsp;&nbsp;<button class="btn btn-primary bold_class" type="button" id="watchnowShowseasonconfirm">' . fgthygfh . '</button></div><br><br>';
        $form.= '</div>';
        $form.= '</div>';
        $form.= '</div>';
        $form.= '</div>';
        //End the payment form

        return $form;
    }
    public function payment_next_btn($signup_step){
        $controller = Yii::app()->controller;
        if($signup_step == 1){
            $btn = '<input type="hidden" id="payment_gateway" value="ippayment" />';
            $btn .= '<div class="clear"></div><br><div class="form-group ">
                <div class="controls">
                    <div class="col-md-12">
                        <button id="register_membership" class="btn btn-primary" onclick="validateUserSignupForm();">' . $controller->Language['btn_payment_next'] . '</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>';
        }else{
        $btn = '<div class="clear"></div><br><div class="form-group">
                <div class="controls">
                    <div class="col-md-12">
                        <button id="register_membership" class="btn btn-primary" onclick="generateSSTSubscription();">' . $controller->Language['btn_payment_next'] . '</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>';
        }
        return $btn;
    }
    public function activate_btn() {
        $controller = Yii::app()->controller;
        if ($controller->IS_PAYPAL_EXPRESS['paypalpro'] == 0) {
            $btn = '<div class="form-group">
                <div class="col-sm-4">
                    <div class="">
                        <label>
                             ' . $controller->Language['agree_terms'] . ' <a class="link linkcol" href="' . $controller->siteurl . '/page/terms-privacy-policy" target="_blank">' . $controller->Language['terms'] . '</a>
                        </label>
                        <label id="data[over_18]-error" class="error" for="data[over_18]" style="display: none;"></label>
                    </div>
                </div>
            </div>            
            
            <div class="clear"></div><br>
            <div class="form-group ">
                <div class="controls">
                    <div class="pull-left">
                        <button id="register_membership" class="btn btn-primary" onclick="validatePaymentForm();">' . $controller->Language['btn_activate'] . '</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>';
        } else {
            $btn = '<div class="clear"></div><br><div class="form-group top50"><div class="col-sm-6" align="left"><input type="image" src="' . Yii::app()->baseUrl . '/images/paypal.png" id="paypal" onclick="return validatePaymentForm();">
                       </div></div>';
        }
        return $btn;
    }

    public function cancel_btn() {
        $controller = Yii::app()->controller;
        $btn = $controller->Language['btn_cancel'];
        return $btn;
    }

    public function payment_register() {
        $controller = Yii::app()->controller;
        if ($controller->IS_PAYPAL_EXPRESS['paypalpro'] == 0) {
            $btn = '<div class="form-group">
                <div class="col-sm-4">
                    <div class="">
                        <label>
                          ' . $controller->Language['chk_over_18'] . ' <a class="link linkcol" href="' . $controller->siteurl . '/page/terms-privacy-policy" target="_blank">' . $controller->Language['terms'] . '</a>
                        </label>
                        <label id="data[over_18]-error" class="error" for="data[over_18]" style="display: none;"></label>
                    </div>
                </div>
            </div>            
            <div class="clear"></div>
            <div class="form-group col-md-12">
                <div class="controls">
                    <div class="pull-left">
                        <button id="register_membership" class="btn btn-primary" onclick="validateUserSignupForm();">' . $controller->Language['btn_register'] . '</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>     ';
        } else {
            $btn = '<div class="clear"></div><br><div class="form-group top50"><div class="col-sm-6 top20" align="left"><input type="image" src="' . Yii::app()->baseUrl . '/images/paypal.png" id="paypal" onclick="return validateUserSignupForm();">
                       </div></div>';
        }
        return $btn;
    }

    public function getPlayerLogo($studio_id) {
        $playerLogoRestrictionConfig = StudioConfig::model()->getconfigvalue('player_logo_restriction');
        if ($playerLogoRestrictionConfig['config_value'] == 1) {
            $playerLogoPath = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'player_logo');
            if (@$playerLogoPath['config_value'] != "") {
                $playerLogoCDnUlr = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                return $playerLogoCDnUlr . '/' . $studio_id . '/playerlogos/' . $playerLogoPath['config_value'];
            } else {
                return "";
            }
        }
    }

    public function hideChatAdmin($studio_id) {
        $ret_val = 0;
        if (isset(Yii::app()->user->id) && Yii::app()->user->id && $studio_id > 0) {
            $hide_chat = StudioConfig::model()->getconfigvalueForStudio(0, 'hidestudio_chat');
            if (isset($hide_chat) && $hide_chat['config_value'] != '') {
                $carr = UserRelation::model()->findByAttributes(array('studio_id' => $studio_id, 'refer_id' => $hide_chat['config_value']));
                if (!empty($carr)) {
                    $ret_val = 1;
                }
            }
        }
        return $ret_val;
    }

    public function alljs($studio_id) {
        $controller = Yii::app()->controller;
        $studio = Yii::app()->common->getStudiosId(1);
        $authToken   = OuathRegistration::model()->findByAttributes(array('studio_id' => $studio_id), array('select'=>'oauth_token'))->oauth_token;
        $alljs = '';
        if ($studio->additional_meta_contents != '' && $controller->ishome == true) {
            $alljs.= '<meta name="google-site-verification" content="' . $studio->additional_meta_contents . '" />';
        }
        if (isset($controller->PAYMENT_GATEWAY) && trim($controller->PAYMENT_GATEWAY['instafeez']) == 'instafeez') {
            $alljs.= '<script type="text/javascript" src="' . $controller->siteurl . '/common/js/instafeez.js"></script>';
        }
        $alljs.= '<script type="text/javascript">';
        $alljs.= 'var HTTP_ROOT ="' . Yii::app()->getbaseUrl(true) . '";';
        $alljs.= 'var THEME_URL ="' . Yii::app()->getbaseUrl(true) . '/themes/' . $controller->site_theme . '";';
        $alljs.= 'var website_url ="' . Yii::app()->getbaseUrl(true) . '";';
        $alljs.= 'var STORE_AUTH_TOKEN ="' . @$authToken . '";';
        $alljs.=" var json = '" . $controller->JsMessage . "';";
        $alljs.=" var JSLANGUAGE = JSON.parse(json);";
        if (isset($controller->PAYMENT_GATEWAY) && trim($controller->PAYMENT_GATEWAY['instafeez']) == 'instafeez') {
            $alljs.= 'var PAYMENT_GATEWAY = "instafeez";';
        } else {
            $alljs.= 'var PAYMENT_GATEWAY = "";';
        }
        $alljs.= 'var imageAddr = [], downloadSize = [], desc = [];';
        $alljs.= 'window.setTimeout(function() {
                        $(".alert").remove(); 
                        $(".navbar-fixed-top, .navbar-wrapper").css("top", "0px");
                    }, 5000);';
        $alljs.= '$(document).ready(function(){
                    if($(".alert").length) {
                        $(".navbar-fixed-top, .navbar-wrapper").css("top", "50px");
                        $(".alert .close").click(function(){
                            $(".navbar-fixed-top, .navbar-wrapper").css("top", "0px");
                        });
                    }
                    $(".search_field").bind("keypress keydown keyup", function(e) {
                        var search_val = $(".search_field").val();
                        if (e.keyCode == 13 && search_val == "") {
                            e.preventDefault();
                        }
                    });
                    $(".playbtn").click(function () {
                    $("#to_play").val("play");
                    $("#play_url").val($(this).prop("id"));
                });

                jQuery.validator.addMethod("mail", function (value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, JSLANGUAGE.valid_email);
                jQuery.validator.addMethod("email", function (value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, JSLANGUAGE.valid_email);
                jQuery.validator.addMethod("cc", function (value, element) {
                    return this.optional(element) || /^(?:3[47]\d|(?:4\d|5[1-5]|65)\d{2}|6011)\d{12}$/.test(value);
                }, JSLANGUAGE.card_number_required);
                $.validator.addMethod("textonly", function(value, element) {
                    return this.optional(element) || /[a-zA-Z]{1}/i.test(value);
                }, JSLANGUAGE.valid_text);
                jQuery.validator.addMethod("cvv", function (value, element) {
                    return this.optional(element) || /^[0-9]{3,4}$/.test(value);
                },JSLANGUAGE.enter_cvv);

                jQuery.validator.addMethod("phone", function (value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, JSLANGUAGE.enter_correct_phone);
                });
        ';
        if ((!isset($_SESSION['internetSpeed']) && !isset(Yii::app()->user->internetSpeed))) {
            $bucketInfoDetails = Yii::app()->common->getBucketInfo('', $studio_id);
            $internetSpeedImage = CDN_HTTP . $bucketInfoDetails['bucket_name'] . '.' . $bucketInfoDetails['s3url'] . '/check-download-speed.jpg';
            $alljs.= 'var imageAddr = [], downloadSize = [], desc = [];
            imageAddr[0] = "' . $internetSpeedImage . '";
            downloadSize[0] = 1036053;
            desc[0] = "Singapore S3 Bucket";
            var startTime, endTime;
            function showResults(index) {     
            console.log("Hello"+index);
                var duration = (endTime - startTime) / 1000;
                var bitsLoaded = downloadSize[index] * 8;
                var speedBps = (bitsLoaded / duration).toFixed(2);
                var speedKbps = (speedBps / 1024).toFixed(2);
                var speedMbps = (speedKbps / 1024).toFixed(2);
                console.log(speedMbps+" Log");
                $.post("' . Yii::app()->baseUrl . '/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {});
            }
            window.onload = function() {
                for (var i = 0; i < desc.length; i++) {
                    var download = new Image();
                    download.i = i;
                    download.onload = function() {
                        endTime = (new Date()).getTime();
                        showResults(this.i);
                    }
                    download.onerror = function(err, msg) {
                        console.log("Invalid image, or error downloading");
                    }
                    startTime = (new Date()).getTime();
                    download.src = imageAddr[i] + "?nnn=" + startTime;
                    console.log(download.src);
                }
            }';
        }
        if (isset($_SESSION['internetSpeed'])) {
            $alljs.= 'console.log(' . $_SESSION['internetSpeed'] . ');';
        }
        if (isset(Yii::app()->user->internetSpeed)) {
            $alljs.= 'console.log(' . Yii::app()->user->internetSpeed . ');';
        }
        $block_ga = Yii::app()->session['block_ga'];
        if ($studio->google_analytics != '' && isset($block_ga) && $block_ga != 0) {
            $alljs.= "(function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '" . html_entity_decode($studio->google_analytics) . "', 'auto');
            ga('send', 'pageview');";
        }
        $alljs.= '</script>';
        return $alljs;
    }

    function replacefromarrayvalue($arr, $search, $replace) {
        $ret = array();
        foreach ($arr as $ar) {
            $ret[] = str_replace($search, $replace, $ar);
        }
        return $ret;
    }

    function dirToArray($dir) {

        $result = array();

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                } else {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    function fileCanEdit($filename) {
        $allowed_types = array('html', 'css', 'js');
        $fileext = end(explode('.', $filename));
        return in_array($fileext, $allowed_types);
    }

    public function getContentDataPermalink($movie_id = 0, $start_time = "", $end_time = "") {
        $nowtime = gmdate('Y-m-d H:i:s');
        $permalink = '';
        $content_title = '';
        $content_description = '';
        $content_poster = '';
        $controller = Yii::app()->controller;
        $studio_id = Yii::app()->common->getStudiosId();
        if ($movie_id > 0) {
            $criteria = new CDbCriteria;
            $criteria->select = 't.movie_id, t.episode_story, episode_title,t.is_episode,t.embed_id'; // select fields which you want in output
            $criteria->condition = 't.id = ' . $movie_id . ' AND t.studio_id = ' . $studio_id;
            $film = movieStreams::model()->with('film')->find($criteria);
            $permalink = $film->film->permalink;
            $content_title = $film->film->name;
            $content_description = $film->film->story;
            if ($film->film->content_types_id == 2) {
                $content_poster = $controller->getPoster($film->film->id, 'films', 'episode', $studio_id);
            } else {
                $content_poster = $controller->getPoster($film->film->id, 'films', 'standard', $studio_id);
            }
            if ($film->is_episode == 1) {
                $permalink.= '/stream/' . $film->embed_id;
                $content_description = $film->episode_story;
                $content_title = $film->episode_title;
                $content_poster = $controller->getPoster($movie_id, 'moviestream', 'episode', $studio_id);
            }
        }

        if ($start_time != "" && $start_time <= $nowtime && $end_time > $nowtime) {
            $permalink = $permalink;
        } else {
            $permalink = "";
        }
        $data = array(
            'permalink' => $permalink,
            'content_title' => $content_title,
            'content_description' => $content_description,
            'content_short_description' => strlen($content_description) > 50 ? substr($content_description, 0, 50) . "..." : $content_description,
            'content_poster' => $content_poster,
            'start_time' => $start_time
        );
        return $data;
    }

    function getBannerSpeed() {
        $res = StudioConfig::model()->getconfigvalue('banner_speed');
        $speed = 6000;
        if (isset($res['config_value']) && $res['config_value'] > 0) {
            $speed = (int) $res['config_value'];
        }
        return $speed;
    }

    function getCustomFields($studio_id, $form_type) {
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $fields = CustomField::model()->findAllFields($studio_id, $form_type);
        $custom_fields = array();
        $i = 0;
        if (count($fields) > 0) {
            foreach ($fields as $field) {
                $req = '';
                if ($field->is_required)
                    $req = ' required="required"';
                if ($field->input_type == 1) {
                    $value = self::getCustomFieldsValue($studio_id, $form_type, $user_id, $field->id);
                    $custom_fields[] = array(
                        'field' => '<input' . $req . ' value="' . $value[0] . '" class="form-control" placeholder="' . $field->placeholder . '" type="text" name="data[custom_' . $field->field_name . ']" id="' . $field->field_id . '" />',
                        'label' => $field->field_label
                    );
                } else if ($field->input_type == 2) {
                    if ($field->autoselect_country == '1') {
                        $visitor_loc = Yii::app()->common->getVisitorLocation();
                        $country = $visitor_loc['country_name'];
                    }
                    $values = self::getCustomFieldsValue($studio_id, $form_type, $user_id, $field->id);
                    $fld_vals = json_decode($field->field_values);
                    $multiple = ($field->is_multi == '1') ? ' multiple="multiple"' : '';
                    $field_name = ($field->is_multi == '1') ? 'data[custom_' . $field->field_name . '][]' : 'data[custom_' . $field->field_name . ']';
                    $form_field = '<select' . $req . ' ' . $multiple . ' class="form-control" name="' . $field_name . '" id="' . $field->field_id . '">';
                    if ($field->placeholder != '')
                        $form_field.= '<option value="">' . $field->placeholder . '</option>';
                    foreach ($fld_vals as $key => $val) {
                        $selected = in_array($val, $values) ? ' selected="selected"' : '';
                        $selected2 = '';
                        if ((count($values) == 0) && $selected == '' && $field->autoselect_country == '1') {
                            $selected2 = (strtolower($val) == strtolower($country)) ? ' selected="selected"' : '';
                        }
                        $form_field.= '<option ' . $selected . ' ' . $selected2 . ' value="' . $val . '">' . $val . '</option>';
                    }
                    $form_field.= '</select>';
                    $custom_fields[] = array(
                        'field' => $form_field,
                        'label' => $field->field_label
                    );
                }
            }
        }
        return $custom_fields;
    }

    function getCustomFieldsValue($studio_id, $form_type, $user_id, $field_id) {
        $result = array();
        $cval = new CustomFieldValue();
        if ($user_id > 0)
            $criteria = array('field_id' => $field_id, 'user_id' => $user_id, 'studio_id' => $studio_id);
        else
            $criteria = array('field_id' => $field_id, 'user_id' => 0, 'studio_id' => $studio_id);
        $cvals = $cval->findAllByAttributes($criteria);
        foreach ($cvals as $val) {
            $result[] = $val->value;
        }
        return $result;
    }

    function getCustomFieldValue($studio_id, $form_type, $user_id = 0) {
        $fields = CustomField::model()->findAllFields($studio_id, $form_type);
        $i = 0;
        $result = array();
        if (count($fields) > 0) {
            foreach ($fields as $field) {
                $result[$i]['field_label'] = $field->field_label;
                $cval = new CustomFieldValue();
                if ($user_id > 0)
                    $criteria = array('field_id' => $field->id, 'user_id' => $user_id, 'studio_id' => $studio_id);
                else
                    $criteria = array('field_id' => $field->id, 'studio_id' => $studio_id);
                $cvals = $cval->findAllByAttributes($criteria);
                $res = array();
                foreach ($cvals as $val) {
                    $res[] = $val->value;
                }
                $result[$i]['field_value'] = $res;
                $i++;
            }
        }
        return $result;
    }

    public function isHomepage() {
        if (Yii::app()->request->url == '/' || Yii::app()->request->url == '/?preview=2') {
            return true;
        } else {
            return false;
        }
    }

    function checkSubPrice($parent_id, $default_currency = '') {
        $sql = "SELECT spr.id AS pricing_id, spr.status AS price_status, spr.price,spr.currency_id,spr.subscription_plan_id, cu.code, cu.symbol FROM subscription_pricing AS spr LEFT JOIN currency AS cu ON (spr.currency_id=cu.id) WHERE  spr.subscription_plan_id={$parent_id} AND spr.status=1 ";
        if ($default_currency != '') {
            $sql .= " ORDER BY FIND_IN_SET(spr.currency_id, {$default_currency}) DESC";
        }
        $dbcon = Yii::app()->db;
        $subpric = $dbcon->createCommand($sql)->queryAll();
        if (!@empty($subpric)) {
            return $subpric;
        } else {
            return false;
        }
    }

    function getAllChannel() {
        $studio_id = Yii::app()->common->getStudiosId();
        $allchannels = tvGuide::model()->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
        $channeldta = "";


        foreach ($allchannels as $channels) {
            $channel_guide = array();
            $channel_name = '';

            $channel_guide[$channels->channel_name . "*" . $channels->channel_logo] = tvGuide::model()->allguideschannelwise($channels->id, '', '');

            $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
            $base_cloud_url .= "tv_guide/channel_logo/";
            $guide = "";

            $name = @$channels->channel_name;
            $logo = @$channels->channel_logo;
            $id = @$channels->id;
            ;
            if ($logo != "") {
                $img_path = $base_cloud_url . $logo;
                $img = '<img src="' . $img_path . '" alt="' . $name . '">';
            } else {
                $img = $name;
            }
            $channeldta.='<div class="col-md-2 channel-logo" id="allchannel_' . $id . '" onclick="redirect_url(' . $id . ');" style="cursor:pointer;">' . $img . '</div>';
        }
        return $channeldta;
    }

    function getNextItem($array, $key) {
        $keys = array_keys($array);
        if ((false !== ($p = array_search($key, $keys))) && ($p < count($keys) - 1)) {
            return array('key' => $keys[++$p], 'value' => $array[$keys[$p]]);
        } else {
            return false;
        }
    }

    function getPrevItem($array, $key) {
        $keys = array_keys($array);
        $keyPos = array_flip($keys);
        $values = array_values($array);
        $search = $key;
        $arr['value'] = $values[$keyPos[$search] - 1];
        $arr['key'] = $keys[$keyPos[$search] - 1];
        return $arr;
    }

    /*
     * This function is used to generate random number
     */

    function moreRand($len) {
        $str = mt_rand(1, 9);
        for ($i = 0; $i < $len - 1; $i++) {
            $str .= mt_rand(0, 9);
        }
        return $str;
    }

    /*
     * issue no #4930
     * Author @manas@muvi.com */

    function CheckApp($studio_id) {
        $app = self::apps_count($studio_id);
        if (isset($app) && ($app['apps_menu'] & 16)) {
            return 1;
        } elseif (isset($app) && ($app['apps_menu'] & 32)) {
            return 2;
        }
        /* if(FireTvApp::model()->exists('studio_id='.$studio_id.' AND status=1')){
          return 1;
          }elseif(AppleTvApp::model()->exists('studio_id='.$studio_id.' AND status=1')){
          return 2;
          } */ else {
            return 0;
        }
    }

    /*
     * Check studio is referal of sony
     * Author Srutikant
     */

    function checkSonyReferal($studio_id) {
        if (HOST_IP != '127.0.0.1' && HOST_IP != '52.0.64.95') {
            $sonyReferalData = Yii::app()->db->createCommand()
                    ->select('studio_id')
                    ->from('user_relation')
                    ->where('refer_id = 7 and studio_id=' . $studio_id)
                    ->queryAll();
            if (@$sonyReferalData[0][studio_id]) {
                return 1;
            }
        }
        return 0;
    }

    public function itemsPerPage() {
        $config = new StudioConfig();
        $config = $config->getconfigvalue('items_per_page');
        if (isset($config) && $config['config_value'] > 0) {
            return $config['config_value'];
        } else {
            return Yii::app()->controller->template->items_per_page;
        }
    }

    function getGeoblockName($movie_id, $stream_id) {
        $sql = "SELECT category_name  FROM geo_block_category JOIN  geoblockcontent ON geoblockcontent.geocategory_id = geo_block_category.Id  where movie_id=" . $movie_id . " AND movie_stream_id=" . $stream_id;
        $country = Yii::app()->db->createCommand($sql)->queryRow();
        return $country;
    }

    function getGeoblockPGName($movie_id) {
        $sql = "SELECT category_name  FROM geo_block_category JOIN  geoblockpgcontent ON geoblockpgcontent.geocategory_id = geo_block_category.Id  where pg_product_id=" . $movie_id;
        $country = Yii::app()->db->createCommand($sql)->queryRow();
        return $country;
    }

    function getPgSettings() {
        $studio_id = Yii::app()->common->getStudiosId();
        $pgsettings = PGSettings::model()->find(array('condition' => 'studio_id=:studio_id', 'params' => array(':studio_id' => $studio_id)));
        return $pgsettings;
    }

    function getPgCustomerSource() {
        $studio_id = Yii::app()->common->getStudiosId();
        $pgcustomersource = Yii::app()->db->createCommand("SELECT * FROM pg_customer_source WHERE studio_id=" . $studio_id)->queryALL();
        return $pgcustomersource;
    }

    function showCategoriesInHomepage() {
        $studio_id = Yii::app()->common->getStudiosId();
        $config = new StudioConfig();
        $subcat_config = $config->getconfigvalue('subcategory_enabled');

        $config = new StudioConfig();
        $config = $config->getconfigvalue('show_category_homepage');
        $categories = array();
        $controller = Yii::app()->controller;

        if ($subcat_config['config_value'] == '1' && $config['config_value'] == '1') {
            $categories_res = ContentCategories::model()->findAll(array("condition" => "studio_id=" . $studio_id, "order" => "id_seq, category_name ASC"));
            foreach ($categories_res as $category) {
                $poster = $controller->getPoster($category->id, 'content_category', 'thumb');
                $categories[] = array(
                    'category_id' => $category->id,
                    'category_name' => $category->category_name,
                    'permalink' => $controller->siteurl . '/' . $category->permalink,
                    'category_poster' => $poster
                );
            }
        }
        return $categories;
    }

    public function gethomepagelist($studio_id, $user_id) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $userData = SdkUser::model()->findByPk($user_id);
        $subscription = Yii::app()->common->isSubscribed($user_id);
        $studioconfig = new StudioConfig;
        $config = $studioconfig->findByAttributes(array('config_key' => 'homepage_url_after_login', 'studio_id' => $studio_id));
        if (count($config) > 0 && $config->config_value != '') {
            $return = array(
                'url' => Yii::app()->getbaseUrl(true) . '/' . trim($config->config_value),
                'value' => 1,
            );
        } else {
            if (!empty(Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id))) {
                if (isset($userData) && !empty($userData) && intval($userData->is_deleted)) {
                    $return = array(
                        'url' => Yii::app()->getbaseUrl(true) . '/user/reactivate',
                        'value' => '0',
                    );
                } elseif ($subscription == '' && $userData->is_free=0) {
                    $return = array(
                        'url' => Yii::app()->getbaseUrl(true) . '/user/activate',
                        'value' => '0',
                    );
                } else {
                    $return = array(
                        'url' => Yii::app()->getbaseUrl(true),
                        'value' => 0,
                    );
                }
            } else {
                $return = array(
                    'url' => Yii::app()->getbaseUrl(true),
                    'value' => 0,
                );
            }
        }
        return $return;
    }

    public function getTrackingCode() {
        $data = array();
        $controller = Yii::app()->controller;
        $studio_id = Yii::app()->common->getStudiosId();
        $trackcode = Trackingcode::model()->findAllByAttributes(array('studio_id' => $studio_id));
        if (!empty($trackcode)) {
            foreach ($trackcode as $tracking_code) {
                $data[] = array(
                    'id' => $tracking_code->id,
                    'track_name' => $tracking_code->name,
                    'track_location' => $tracking_code->location,
                    'track_tag' => $tracking_code->tag,
                    'track_code' => html_entity_decode($tracking_code->code),
                );
            }
        }
        return $data;
    }

    /* Return parent_content_type by manas@muvi.com */

    public function parent_content_type() {
        $sql = "SELECT * FROM parent_content_type";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getStartContnetTypeID($studio_id) {
        $condition = 'studio_id=:studio_id order by id desc';
        $params = array(':studio_id' => $studio_id);
        $forms = CustomMetadataForm::model()->find($condition, $params);
        if (!empty($forms)) {
            if (($forms['parent_content_type_id'] == 5) && !Yii::app()->general->getStoreLink()) {
                $content['content_count'] = $content['content_count'] + 16; //16 is binary value of muvikart see "parent_content_type" table
            }
            $fid['parent_content_type_id'] = $forms['parent_content_type_id'];
            $fid['arg']['editid'] = $forms['id'];
            $fid['arg']['content_type'] = $forms['content_type'];
            $fid['arg']['is_child'] = $forms['is_child'];
            return $fid;
        } else {
        $studio_id = !empty($studio_id) ? $studio_id : $this->studio->id;
        $content = Yii::app()->general->content_count($studio_id);
        $content_child = Yii::app()->general->content_count_child($studio_id);
        if ($content['content_count'] & 1) {
            if ($content_child['content_count'] & 1) {
                return 1;
            } elseif ($content_child['content_count'] & 2) {
                return 2;
            } elseif ($content_child['content_count'] & 4) {
                return 3;
            } else {
                return 1;
            }
        } elseif ($content['content_count'] & 2) {
            return 5; //live streaming
		} elseif ($content['content_count'] & 8) {
			return 10; //Audio live streaming
        } elseif ($content['content_count'] & 4) {
            if ($content_child['content_count'] == 16) {
                return 8;
            } else {
                return 7;
            }
        } else {
            if ($content_child['content_count'] & 2) {
                return 2;
            } elseif ($content_child['content_count'] & 4) {
                return 3;
            } else {
                return 1;
            }
        }
    }
    }

    public function getDefaultHorizontalPoster($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if (isset($_SESSION[$studio_id]['horizontal_poster'])) {
            return $_SESSION[$studio_id]['horizontal_poster'];
        }
        $studioconfig = new StudioConfig;
        $config = $studioconfig->findByAttributes(array('config_key' => 'default_h_poster_content', 'studio_id' => $studio_id));
        $poster_path = trim($config->config_value);
        if (count($config) > 0 && $poster_path != '') {
            $_SESSION[$studio_id]['horizontal_poster'] = $poster_path;
            return $poster_path;
        } else {
            $_SESSION[$studio_id]['horizontal_poster'] = '';
            return '';
        }
    }

    public function getDefaultVerticalPoster($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if (isset($_SESSION[$studio_id]['vertical_poster'])) {
            return $_SESSION[$studio_id]['vertical_poster'];
        }
        $studioconfig = new StudioConfig;
        $config = $studioconfig->findByAttributes(array('config_key' => 'default_v_poster_content', 'studio_id' => $studio_id));
        $poster_path = trim($config->config_value);
        if (count($config) > 0 && $poster_path != '') {
            $_SESSION[$studio_id]['vertical_poster'] = $poster_path;
            return $poster_path;
        } else {
            $_SESSION[$studio_id]['vertical_poster'] = '';
            return '';
        }
    }

    public function getMuviKartPaymentForm($gateway_code, $methods) {
        Yii::app()->theme = 'bootstrap';
        $settings = self::getPgSettings();
        foreach ($_SESSION["cart_item"] as $item) {
            if ($item['price'] != '0.00') {
                $pertotal = $item["price"] * $item["quantity"];
                $item_total = $item_total + $pertotal;
            }
        }
        $currency_id = ($item['currency_id'] != 0) ? $item['currency_id'] : '153';
        $payment_form = Yii::app()->controller->renderPartial('//muvikart_paymentform/payment_form', array('settings' => $settings, 'gateway_code' => $gateway_code, 'item_total' => $item_total, 'currency_id' => $currency_id, 'methods' => $methods, 'single_method' => 'standard'), true);
        return $payment_form;
    }
	/**
     * @param custom_metadata_form_ids
     * @return ids of films or movie_streams table or pg_product
     */
    public function getIdsOfcontent($custom_metadata_form_ids) {
        $filmdata = Film::model()->findAll(array("select" => "id,custom_metadata_form_id", "condition" => 'custom_metadata_form_id IN (' . $custom_metadata_form_ids . ')'));
        $filmid = CHtml::listData($filmdata, 'id', 'custom_metadata_form_id');
        $movieStreamsdata = movieStreams::model()->findAll(array("select" => "id,custom_metadata_form_id", "condition" => 'custom_metadata_form_id IN (' . $custom_metadata_form_ids . ')'));
        $movieStreamsdataid = CHtml::listData($movieStreamsdata, 'id', 'custom_metadata_form_id');
        $physicaldata = PGProduct::model()->findAll(array("select" => "id,custom_metadata_form_id", "condition" => 'custom_metadata_form_id IN (' . $custom_metadata_form_ids . ')'));
        $physicaldataid = CHtml::listData($physicaldata, 'id', 'custom_metadata_form_id');
        $content_ids = array_merge($filmid, $movieStreamsdataid);
        $content_ids = array_merge($content_ids, $physicaldataid);
        return $content_ids;
    }

    public function getmetadata_form_type_id($arg) {
        if (($arg['parent_content_type_id'] == 1) && ($arg['content_type'] == 0)) {
            return 1;
        } else if (($arg['parent_content_type_id'] == 1) && ($arg['content_type'] == 1) && ($arg['is_child'] == 0)) {
            return 3;
        } else if (($arg['parent_content_type_id'] == 1) && ($arg['content_type'] == 1) && ($arg['is_child'] == 1)) {
            return 4;
        } else if (($arg['parent_content_type_id'] == 2)) {
            return 5;
        } else if (($arg['parent_content_type_id'] == 3) && ($arg['content_type'] == 0)) {
            return 7;
        } else if (($arg['parent_content_type_id'] == 3) && ($arg['content_type'] == 1) && ($arg['is_child'] == 0)) {
            return 8;
        } else if (($arg['parent_content_type_id'] == 3) && ($arg['content_type'] == 1) && ($arg['is_child'] == 1)) {
            return 9;
        } else if (($arg['parent_content_type_id'] == 5)) {
            return 6;
        } else if (($arg['parent_content_type_id'] == 4)) {
            return 10;
        }
    }

    function getArrayFrommetadata_form_type_id($id) {
        $arr[1] = array("parent_content_type_id" => 1, "arg" => array("content_type" => 0, "is_child" => 0));
        $arr[3] = array("parent_content_type_id" => 1, "arg" => array("content_type" => 1, "is_child" => 0));
        $arr[4] = array("parent_content_type_id" => 1, "arg" => array("content_type" => 1, "is_child" => 1));
        $arr[5] = array("parent_content_type_id" => 2, "arg" => array("content_type" => 0, "is_child" => 0));
        $arr[7] = array("parent_content_type_id" => 3, "arg" => array("content_type" => 0, "is_child" => 0));
        $arr[8] = array("parent_content_type_id" => 3, "arg" => array("content_type" => 1, "is_child" => 0));
        $arr[9] = array("parent_content_type_id" => 3, "arg" => array("content_type" => 1, "is_child" => 1));
        $arr[6] = array("parent_content_type_id" => 5, "arg" => array("content_type" => 0, "is_child" => 0));
		$arr[10] = array("parent_content_type_id" => 4, "arg" => array("content_type" => 0, "is_child" => 0));
        return $arr[$id];
    }

    function getcontents_types_id($arg) {
        if (($arg['parent_content_type_id'] == 1) && ($arg['content_type'] == 0)) {
            return 1;
        } else if (($arg['parent_content_type_id'] == 1) && ($arg['content_type'] == 1)) {
            return 3;
        } else if (($arg['parent_content_type_id'] == 2)) {
            return 4;
        } else if (($arg['parent_content_type_id'] == 3) && ($arg['content_type'] == 0)) {
            return 5;
        } else if (($arg['parent_content_type_id'] == 3) && ($arg['content_type'] == 1)) {
            return 6;
        } else if (($arg['parent_content_type_id'] == 5)) {
            return 7;
        } else if (($arg['parent_content_type_id'] == 4)) {
            return 8;
        }
    }

    function getFormName($v, $studio_id, $form, $list) {
        if ($v['is_episode'] == 1) {
            $cmf = $v['cmfid'];
        } else {
            $cmf = $v['custom_metadata_form_id'];
        }
        if ($cmf && ($list[$cmf] != '')) {
            return $list[$cmf];
        } else {
            if (in_array($v['content_types_id'], array(3, 6))) {
                $mflag = 1;
            } else {
                $mflag = 0;
            }
            $contentformdata = CustomMetadataForm::model()->getFromData(0);
            foreach ($contentformdata as $key => $value) {
                $contentformdataFinal[$value['parent_content_type_id'] . $value['content_type'] . $value['is_child']] = $value;
            }
            /* foreach ($form as $key => $value) {
              $contentformdataFinal[$value['parent_content_type_id'].$value['content_type'].$value['is_child']]=$value;
              } */
            $index = $v['parent_content_type_id'] . $mflag . $v['is_episode'];
            return $contentformdataFinal[$index]['name'];
        }
    }

    function formlist($studio_id) {
        $contentformdata = CustomMetadataForm::model()->getFromData(0);
        $contentformdataS = CustomMetadataForm::model()->getFromData($studio_id, '*', 'id DESC');
        foreach ($contentformdata as $key => $value) {
            $defaultkeys[] = $value['parent_content_type_id'] . $value['content_type'] . $value['is_child'];
            $contentformdataFinal[$value['parent_content_type_id'] . $value['content_type'] . $value['is_child']] = $value;
        }
        foreach ($contentformdataS as $key => $value) {
            $studiokeys[] = $value['parent_content_type_id'] . $value['content_type'] . $value['is_child'];
        }
        if (!empty($studiokeys)) {
            $diffarray = array_diff($defaultkeys, $studiokeys);
            foreach ($contentformdataFinal as $key => $value) {
                if (!in_array($key, $diffarray)) {
                    unset($contentformdataFinal[$key]);
                }
            }
        }
        $form = (!empty($contentformdataS)) ? array_merge($contentformdataS, $contentformdataFinal) : $contentformdataFinal;
        return CustomMetadataForm::model()->showFormBySetting($studio_id, $form);
    }

    function getCustomMetadataForm($studio_id, $content_types_id) {
        $fid = Yii::app()->general->getArrayFrommetadata_form_type_id($content_types_id);
        $customComp = new CustomForms();
        $customData[0] = $customComp->getCustomMetadata($studio_id, $fid['parent_content_type_id'], $fid['arg']);
        $customData[1] = $fid['arg']['is_child'];
        $customData[2] = $fid['parent_content_type_id'];
        return $customData;
    }
    public function checkFinalPGprice($studio_id, $coupon_code, $method) {
        foreach ($_SESSION["cart_item"] as $item) {
            if ($item['price'] != '0.00') {
                $pertotal = $item["price"] * $item["quantity"];
                $item_total = $item_total + $pertotal;
            }
        }
        $res['item_total'] = $item_total;
        $data['currency_id'] = ($item['currency_id'] != 0) ? $item['currency_id'] : '153';
        $res['currency_id'] = $data['currency_id'];
        $data['coupon'] = $coupon_code;
        if ((isset($data['coupon']) && $data['coupon'] != '') || (isset($data['coupon_instafeez']) && $data['coupon_instafeez'] != '')) {
            $coupon = (isset($data['coupon']) && $data['coupon'] != '') ? $data['coupon'] : $data['coupon_instafeez'];
            $getCoup = Yii::app()->common->getCouponDiscount($coupon, $item_total, $studio_id, Yii::app()->user->id, $data['currency_id']);
            $data['amount'] = $amount = $getCoup["amount"];
            $couponCode = $getCoup["couponCode"];
            $final_amount = $amount;
        } else {
            $final_amount = $item_total;
        }
        $res['discount'] = $getCoup["coupon_amount"];
        //calculate shipping cost
        $pgorder = new PGOrder();
        $rev_shipping_cost = $pgorder->calculateShippingCost($studio_id, $_SESSION['ship'], $_SESSION["cart_item"], $method, $data['currency_id']);
        //get Min Order amount for Free Shipping
        $minimum_shipping_cost = PGMinimumOrderFreeShipping::model()->find(array('select' => 'minimum_order_free_shipping', 'condition' => 'studio_id=:studio_id and currency_id=:currency_id', 'params' => array(':studio_id' => $studio_id, ':currency_id' => $data['currency_id'])))->minimum_order_free_shipping;

        if ($rev_shipping_cost['shipping_cost']) {
            $shipping_cost = $rev_shipping_cost['shipping_cost'];
        } else {
            $default_shipping_cost = PGDefaultShippingCost::model()->find(array('select' => 'default_shipping_cost', 'condition' => 'studio_id=:studio_id and currency_id=:currency_id', 'params' => array(':studio_id' => $studio_id, ':currency_id' => $data['currency_id'])));
            $shipping_cost = empty($default_shipping_cost) ? '0' : $default_shipping_cost->default_shipping_cost;
        }
        if ($minimum_shipping_cost) {
            if ($final_amount >= $minimum_shipping_cost) {
                $shipping_cost = 0;
            }
        }
        $res['shipping_cost'] = $shipping_cost;
        $res['total_amount'] = $final_amount + $shipping_cost;
        return $res;
    }

    public function checkProductizeExtension($studio_id) {
        $storelink = self::getStoreLink();
        if ($storelink) {
            if (isset($_SESSION[$studio_id]['productizeflag'])) {
                return $_SESSION[$studio_id]['productizeflag'];
            } else {
                $studio_id = (intval($studio_id)) ? $studio_id : Yii::app()->common->getStudiosId();
                $exts = Extension::model()->find(array(
                    'condition' => 'permalink=:permalink',
                    'params' => array(':permalink' => 'ProductPersonalization')
                ));
                $ext = StudioExtension::model()->findByAttributes(array('studio_id' => $studio_id, 'extension_id' => $exts['id']));
                $_SESSION[$studio_id]['productizeflag'] = $ext->status;
                return $ext->status;
            }
        } else {
            return 0;
        }
    }

    public function videoGalleryThumbSlider($data) {
        if(!isset($data['countFrame']) || $data['countFrame'] == ''){
            return "";
        }
        $condition = '';
        $videoFolder = 'upload_video';
        $shFolder = 'upload_progress';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        } else if (HOST_IP == '52.0.64.95') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        } 
        if (@$data['studio_id'] > 0 && @$data['video_management_id'] > 0 && @$data['video_url'] != '') {
            $studio_id = $data['studio_id'];
            $video_management_id = $data['video_management_id'];
            $videoUrl = $data['video_url'];
            $countFrame = $data['countFrame'];
            $thumbFolder = '/var/www/html/' . $videoFolder . '/' .'videoThumb_'.$video_management_id;
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            $bucketName = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $s3cfg = $bucketInfo['s3cmd_file_name'];
            $functionParams = array();
            $functionParams['field_name'] = 's3_upload_folder';
            if($bucketName == 'vimeoassets-singapore'){
                $functionParams['singapore_bucket'] = 1;
            }
            $shDataArray = Yii::app()->aws->getshUploadFolderPath($functionParams);
            $s3folderPath = $shDataArray['shfolder'];
            $threads = $shDataArray['threads'];
            $updateUrl = Yii::app()->getBaseUrl(true) . "/conversion/UpdateVideoManagementInfo?video_management_id=" . $video_management_id;
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $videoFolderpath = $folderPath['unsignedFolderPathForVideo'];
            $bucket_url = "s3://" . $bucketName . "/" . $videoFolderpath . "videogallery/videogallery-image/" . $video_management_id . "/";
            $file = 'video_management_thumb_' . $video_management_id .HOST_IP. '.sh';
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
            $cf = 'echo "${file%???}"';
            $file_data = "file=`echo $0`\n" .
                    "cf='" . $file . "'\n\n" .
                    "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                    "then\n\n" .
                    "echo \"$file is running\"\n\n" .
                    "else\n\n" .
                    "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                    "# create directory\n" .
                    "mkdir $thumbFolder\n" .
                    "chmod 0777 $thumbFolder\n" .
                    "#Go to Dirctory \n" .
                    "cd ". $thumbFolder . "\n" .
                    "# wget command\n" .
                    "/root/bin/ffmpeg -i  $videoUrl -vf fps=$countFrame,scale=560:312 -threads $threads $thumbFolder/$video_management_id%d.png -an -vcodec mjpeg 2>&1".
                    "\n" .
                    "\n\n\n# Check if thumb image are created\n" .
                    "if [ \"$(find $thumbFolder/ -maxdepth 1 -type f | wc -l)\" -gt 1 ] \n then \n" .
                    "\n# upload to s3\n" .
                    "/usr/local/bin/s3cmd put --recursive --config /usr/local/bin/.$s3cfg --acl-public $thumbFolder/ $bucket_url \n" .
                    "# update query\n" .
                    "curl $updateUrl\n" .
                    "# remove directory\n" .
                    "rm -rf $thumbFolder\n" .
                    "# remove the process copy file\n" .
                    "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                    "# remove the process file\n" .
                    "rm /var/www/html/$shFolder/$file\n" .
                    "\n\nelse\n\n" .
                    "# remove directory\n" .
                    "rm -rf $thumbFolder\n" .
                    "# remove the process copy file\n" .
                    "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                    "# remove the process file\n" .
                    "rm /var/www/html/$shFolder/$file\n" .
                    "fi\n\n" .
                    "fi";
            fwrite($handle, $file_data);
            fclose($handle);
            //Uploading conversion script from local to s3 
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file;
            $returnData = 0;
            if($s3->upload($conversionBucket, $s3folderPath.$file, fopen($filePath, 'rb'),  'public-read')){
                 $returnData = 1;
            }
            unlink($filePath);
            return $returnData;
        } 
    }
    
    public function getMyreview($content_id, $studio_id = false, $user_id = false){
        $ret = 0;
        if ($user_id == false) {
            $user_id = self::getUserId();
        }        
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }  
        if ($studio->rating_activated == 1) {
            $rate = new ContentRating();
            $rate = $rate->findByAttributes(array('studio_id' => $studio_id, 'content_id' => $content_id, 'user_id' => $user_id));        
            $ret = $rate->rating;
        }
        return $ret;
    }
	//@vi - get HH:MM:SS values to seconds || modified for Ads streaming 
    //Date : 07-2017-21
    public function getHHMMSSToseconds($time = ''){
        if($time!=''){        
             $timeExploded = explode(':', $time);
             return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
        }
    }
	public function IsDownloadable($studio_id) {
        $data = StudioConfig::model()->getConfig($studio_id, 'is_downloadable');
		if(isset($data) && $data['config_value'] == 1){
			return 1;
		}else{
			return 0;
		}
    }
    public function getDefaultExpiryTime(){
        return 4;
    }    
	public function updateFileInfo($filename, $studio_id, $forVideoSync = 0, $userid = 0) {
        $arr['msg'] = "error";
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];
        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);

        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];
		$unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $extsn = explode('.', $filename);
        $ext = $extsn[1];
        if ($this->studioData) {
            $studio = $this->studioData->attributes;
        } else {
            $studio = Studio::model()->getStudioBucketData($studio_id);
		}
        $s3viewdir = $bucketCloudfrontUrl . '/' . $unsignedBucketPathForViewVideo . 'filegallery/' . $filename;
        $head = array_change_key_case(get_headers($s3viewdir, TRUE));
        $filesize1 = $head['content-length'];
        $filesize = Yii::app()->common->bytes2English($filesize1);
		$file_type = $head['content-type'];
		
        $file_gallery = new FileManagement();
        $file_gallery->file_name = $filename;
        $file_gallery->studio_id = $studio_id;
        $file_gallery->user_id = $userid;
        $file_gallery->creation_date = new CDbExpression("NOW()");
        $file_gallery->file_size = @$filesize;
		$file_gallery->file_type = @$file_type;
        $file_gallery->save();
        $arr['msg'] = "File uploaded successfully";
		if ($forVideoSync == 0) {
			echo json_encode($arr);
			exit;
        } else {
            return $file_gallery->id;
		}
    }
	 public function addFileToFileGallery($paraDetails) {
        $s3 = Yii::app()->common->connectToAwsS3($paraDetails['studio_id']);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $paraDetails['studio_id']);
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath("", $paraDetails['studio_id']);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $bucket = $bucketInfo['bucket_name'];        
        $key = $unsignedFolderPathForVideo . 'filegallery/' . $paraDetails['file_name'];
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        if ($new_cdn_user == 0) {//existing user
            $key = $unsignedFolderPathForVideo . 'filegallery/' . $paraDetails['studio_id'] . '/' . $paraDetails['file_name'];
        }
        $studio = Studio::model()->getStudioBucketData($paraDetails['studio_id']);
        $newUser = $studio['new_cdn_users'];
        if ($newUser == 0) {//existing user
            $key = $unsignedFolderPathForVideo . 'filegallery/' . $paraDetails['studio_id'] . '/' . $paraDetails['file_name'];
        }
        $fullpath = $paraDetails['file_path'] . "" . $paraDetails['file_name'];

        $response = $s3->copyObject(array(
            'Bucket' => $bucket,
            'Key' => $key,
            'CopySource' => $fullpath,
            'ACL' => 'public-read',
        ));
        if ($response->get('ETag') != '') {
            return Yii::app()->general->updateFileInfo($paraDetails['file_name'], $paraDetails['studio_id'] , 1);
        }
    }
}
