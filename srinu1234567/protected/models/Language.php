<?php

class Language extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'languages';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function getFirstLanguageUser($lang_code){
        $sql = 'SELECT `studio_languages`.`studio_id` FROM `languages` JOIN `studio_languages` ON `studio_languages`.`language_id` = `languages`.`id` WHERE `languages`.`code`="'.$lang_code.'" AND `studio_languages`.`status`=1 ORDER BY `studio_languages`.`id` ASC';
        $studio = Yii::app()->db->createCommand($sql)->queryRow();
        return $studio['studio_id'];
    }
    public function getLanguageCode($langname){
        $data = Yii::app()->db->createCommand()
            ->select('code')
            ->from('languages')
            ->where("name=:langname", array(':langname'=>$langname))
            ->setFetchMode(PDO::FETCH_OBJ)->queryROW();
        if(!empty($data)){
            return $data->code;
        }else{
            return false;
        }
    }
}
