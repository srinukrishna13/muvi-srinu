
<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal"  id="update_apps" action="<?php echo $this->createUrl('/userfeature/saveapps')?>" method='post'>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" name="chromecast" value="1" <?php echo ( $chromecast== 1)?'checked="checked"':'';?> />
                        <i class="input-helper"></i>  Chromecast
                    </label>
                </div> 
            </div>
             <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" name="offline_view" value="1" <?php echo ($offline == 1)?'checked="checked"':''; ?> />
                        <i class="input-helper"></i>  Offline View
                    </label>
                </div>
            </div>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <input type="submit" class="btn btn-primary" name="queuebtn" id="queuebtn" value="Save" />
                </div>
            </div>
        </form>
    </div>        
</div>
