<?php

//class Policy extends CWidget {

class Policy extends CApplicationComponent {

    protected $response = array();
    protected $stream_id, $studio_id, $user_id, $cur_date, $enableQuery, $ppvSubscripton;

    public function init() {
        //$this->studio_id = Yii::app()->user->studio_id;
        $this->stream_id = '';
        $this->studio_id = Yii::app()->common->getStudiosId();
        $this->user_id = Yii::app()->user->id;
        $this->cur_date = gmdate("Y-m-d H:i:s");
        $this->enableQuery = true;
    }

    /* ############################# START CODING By STREAM ID ############################ */

    function resolutionByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                        return self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                    }
                    return;
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                }
                return;
            }
        }
        return;
    }

    function viewByStreamId($stream_id = null, $rules_id = null) {
        $no_of_views = '';
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    $no_of_views = self::checkPolicyMapping($rules['policy_rules_mapping'], 1);
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                $no_of_views = self::checkPolicyMapping($rules['policy_rules_mapping'], 1);
            }
        }
        $chkSubscription = self::checkUserSubscription();
        if (!empty($chkSubscription['views'])) {
            $no_of_views += $chkSubscription['views'];
        }
        return $no_of_views;
    }

    function watchDurationByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 3);
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                return self::checkPolicyMapping($rules['policy_rules_mapping'], 3);
            }
        }
    }

    function accessDurationByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 4);
                    exit;
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                return self::checkPolicyMapping($rules['policy_rules_mapping'], 4);
            }
            return;
        }
    }

    function userByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 5);
                    exit;
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                return self::checkPolicyMapping($rules['policy_rules_mapping'], 5);
                exit;
            }
            return;
        }
        return;
    }

    function checkMonetization($stream_id = null) {
        $is_ppv_bundle = 0;
        $response = array();
        $is_free = SdkUser::model()->findByPk($this->user_id)->is_free;
        if ((isset(Yii::app()->user->is_studio_admin) && Yii::app()->user->is_studio_admin) || (@Yii::app()->user->id == STUDIO_USER_ID || Yii::app()->user->id == 1038 || $is_free == 1)) {
            return 'free';
        }
        $movieStream = movieStreams::model()->findByPk($stream_id);
        $film = Film::model()->findByPk($movieStream->movie_id);

        $ppv_plan = Yii::app()->common->getPPVBundle($film->id, $this->studio_id);
        if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
            $is_ppv_bundle = 1;
            $payment_type = $ppv_plan->id;
        } else {
            $ppv = Yii::app()->common->getContentPaymentType($film->content_types_id, $film->ppv_plan_id, $this->studio_id);
            $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
        }

        /* if no ppv set */
        if ($payment_type == 0) {
            return;
        }
        /* if no ppv set */

        $purchase_type = '';
        if ($film->content_types_id == 3 && $movieStream->series_number > 0) {
            $purchase_type = 'episode';
        } else if ($film->content_types_id == 3) {
            $purchase_type = 'season';
        }


        $freearg['studio_id'] = $this->studio_id;
        $freearg['movie_id'] = $film->id;
        $freearg['season_id'] = $movieStream->series_number;
        $freearg['episode_id'] = (($purchase_type == 'show' || $purchase_type == 'season') && intval($film->content_types_id) == 3) ? 0 : $stream_id;
        $freearg['content_types_id'] = $film->content_types_id;
        $monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
        $isFreeContent = 0;
        if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 8)) {
            $isFreeContent = Yii::app()->common->isFreeContent($freearg);
        }
        if (intval($isFreeContent)) {
            $response['result'] = 'free';
            return $response;
        } else {
            $purchase_type = '';
            if (trim($film['permalink']) != '0' && trim($movieStream->embed_id) != '0') {
                $purchase_type = 'episode';
            } else if (trim($film->permalink) != '0' && trim($movieStream->embed_id) == '0' && intval($movieStream->series_number)) {
                $purchase_type = 'episode';
            }

            if (intval($film->content_types_id) == 3) {
                $content = "'" . $film->id . ":" . $movieStream->series_number . ":" . $stream_id . "'";
            } else {
                $content = $film->id;
            }

            $return = 'unpaid';
            $temp_return = 1;
            $expiredAccessability = '';

            $monitization_data = array();
            $monitization_data['studio_id'] = $studio_id = $this->studio_id;
            $monitization_data['user_id'] = $user_id = $this->user_id;
            $monitization_data['movie_id'] = $movie_id = $film->id;
            $monitization_data['season'] = $season_id = $movieStream->series_number;
            $monitization_data['stream_id'] = $stream_id;
            $monitization_data['content'] = $content;
            $monitization_data['film'] = $film;
            $monitization_data['purchase_type'] = $purchase_type;

            $monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);
            $ppv_subscription_data = array();
            /* if (isset($monetization['is_play']) && $monetization['is_play']) {
              return 'allowed';
              } */
            $is_pre_order_content = 0;
            /* if (isset($monetization['monetization']['pre_order']) && trim($monetization['monetization']['pre_order'])) {
              //Is content subscribed from pre-order
              $pre_order_plan = $monetization['monetization_plans']['pre_order'];
              $content_types_id = $pre_order_plan->content_types_id;
              $is_adv_subscribed = self::IsAdvPaid($movie_id, $stream_id, $season_id, $content_types_id, $purchase_type, $studio_id, $user_id);

              if (!empty($is_adv_subscribed)) {
              return $is_adv_subscribed;
              }
              } */
            /* if (isset($monetization['monetization']['subscription_bundles']) && trim($monetization['monetization']['subscription_bundles'])) {
              //Is content subscribed from subscription bundle
              $is_subscription_bundle_subscribed = self::IsBundledsubscriptionPaid($movie_id, $studio_id, $user_id);
              if (!empty($is_subscription_bundle_subscribed)) {
              return $is_subscription_bundle_subscribed;
              }
              } */
            if (isset($monetization['monetization']['voucher']) && trim($monetization['monetization']['voucher'])) {
                //Is content subscribed by implementing voucher
                $voucher = Yii::app()->common->isVoucherExists($studio_id, $content);
                if ($voucher != 0) {
                    $content_types_id = $film['content_types_id'];
                    $voucher_subscription_data = Yii::app()->common->IsVoucherSubscribed($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);

                    if (!empty($voucher_subscription_data)) {
                        $voucher_subscription_data['monetization'] = $monetization['monetization'];
                        return $voucher_subscription_data;
                    }
                }
            }

            if (isset($monetization['monetization']['ppv_bundle']) && trim($monetization['monetization']['ppv_bundle'])) {
                //Is content subscribed from ppv bundle
                $is_ppv_bundle_subscribed = self::IsBundledPpvPaid($movie_id, $studio_id, $user_id);
                if ($is_ppv_bundle_subscribed) {
                    $is_ppv_bundle_subscribed['monetization'] = $monetization['monetization'];;
                    return $is_ppv_bundle_subscribed;
                }
            }

            if (isset($monetization['monetization']['ppv']) && trim($monetization['monetization']['ppv'])) {
                //Is content subscribed from ppv
                $ppv_plan = $monetization['monetization_plans']['ppv'];

                $content_types_id = $ppv_plan->content_types_id;
                $ppv_subscription_data = Yii::app()->common->IsPpvPaid($movie_id, $stream_id, $season, $content_types_id, $purchase_type, $studio_id, $user_id);
                if (!empty($ppv_subscription_data)) {
                    $ppv_subscription_data['monetization'] = $monetization['monetization'];
                    return $ppv_subscription_data;
                }
            }


            if (empty($ppv_subscription_data)) {
                $response['result'] = 'unpaid';
                $response['monetization'] = $monetization['monetization'];
                return $response;
            }
        }
    }

    ############################ MAIN FUNCTION #################################

    function rules($stream_id = null, $user_id = null) {
        if ($user_id) {
            $this->user_id = $user_id;
        }
        $this->enableQuery = false;
        $this->stream_id = $stream_id;
        $isEnable = self::isEnabledPolicy($this->studio_id);
        if ($isEnable == 0) {
            return $this->response;
        } if ($stream_id) {
            $checkMonetization = self::checkMonetization($stream_id);
            $this->ppvSubscripton = $checkMonetization;
            if (!$checkMonetization) {
                return $this->response = self::checkUserSubscription();
            }
            if (!empty($checkMonetization['result']) && $checkMonetization['result'] == 'free') {
                return $this->response;
            } else if (!empty($checkMonetization['result']) && $checkMonetization['result'] == 'unpaid') {
                $response['error'] = true;
                $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                return $response;
            }
            $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
            if (empty($getPpvPlanDetail['rules_id'])) {
                return $this->response = self::checkUserSubscription();
            }
            $resolution = self::resolutionByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($resolution) {
                $this->response['resolution'] = $resolution;
            }
            $views = self::viewByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($views) {
                $this->response['views'] = $views;
            }
            $user = self::userByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($user) {
                $this->response['user'] = $user;
            }
            $watchDuration = self::watchDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($watchDuration) {
                $this->response['watch_duration'] = $watchDuration;
            }
            $accessDuration = self::accessDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($accessDuration) {
                $this->response['access_duration'] = $accessDuration;
            }
            return $this->response;
        }
        return;
    }

    function verifyRules($stream_id, $user_id = null) {
        if ($user_id) {
            $this->user_id = $user_id;
        }
        $countUser = Yii::app()->Controller->getUserDetail($this->user_id);
        if (!$countUser) {
            $response['user']['error'] = true;
            $response['user']['user_id'] = $this->user_id;
            $response['user']['message'] = Yii::app()->Controller->ServerMessage['user_information_not_found'];
            return $response;
        }
        $rulesArr = self::rules($stream_id);
        $response = array();
        if (!empty($rulesArr)) {
            if ($rulesArr['error'] == true) {
                $response = $rulesArr;
            } if (!empty($rulesArr['access_duration'])) {
                $response['access_duration'] = self::validateAccessDuration($rulesArr['access_duration']);
                //$response['access_duration'] = '';
            } if (!empty($rulesArr['watch_duration']) && empty($response['access_duration'])) {
                $response['watch_duration'] = self::validateWatchDuration($rulesArr['watch_duration']);
            } if (!empty($rulesArr['views']) && empty($response['access_duration'])) {
                $response['views'] = self::validateViews($rulesArr['views']);
            } if (!empty($rulesArr['resolution'])) {
                $response['resolution'] = self::validateResolution($rulesArr['resolution']);
            } if (!empty($rulesArr['user'])) {
                $response['user'] = self::validateUser($rulesArr['user']);
            }
        }
        return $response;
    }

    ############################# Rules Validation Start #############################

    function validateResolution($resolution) {
        return $resolution;
    }

    function validateViews($getView) {
        if (!$getView) {
            return;
        }
        $subscription = $this->ppvSubscripton;
        /* $ppvSubscription['view_restriction'] = $getView;
          $isView = Yii::app()->common->isNumberOfViewsOnContentExceeds($ppvSubscription, $this->stream_id);
          if ($isView == 0) {
          $response['error'] = true;
          $response['totalView'] = $getView;
          $response['message'] = Yii::app()->Controller->ServerMessage['crossed_max_limit_of_watching'];
          return $response;
          } */
        $getData = Yii::app()->db->createCommand()
                /* ->select('IF(watch_status="halfplay" OR watch_status="complete", COUNT(watch_status), 0) AS total_views') */
                ->select('COUNT(watch_status) AS total_views')
                ->from('video_logs a')
                /* ->where('a.video_id=:video_id AND a.created_date >= :created_date', array(':video_id' => $stream_id, ':created_date' => $subscription['created_date'])) */
                ->where('a.video_id=:video_id AND a.user_id=:user_id AND a.studio_id=:studio_id AND created_date >= :created_date', array(':video_id' => $this->stream_id, ':user_id' => $this->user_id, ':studio_id' => $this->studio_id, ':created_date' => $subscription['created_date']))
                ->queryRow();
        if ($getData) {
            if ($getView <= $getData['total_views']) {
                $response['error'] = true;
                $response['totalView'] = $getView;
                $response['myView'] = $getData['total_views'];
                $response['message'] = Yii::app()->Controller->ServerMessage['crossed_max_limit_of_watching'];
                return $response;
            }
            return;
        }
    }

    function validateUser($userType) {
        if (!$userType) {
            return;
        }
        /* will do later after discuss with gaya */
        //$countUser = SdkUser::model()->count('id=:id AND studio_id=:studio_id AND is_deleted=0');
        if (!$this->user_id) {
            $response['error'] = true;
            $response['userType'] = $userType;
            $response['message'] = 'This user neither registered nor subscribed';
            return $response;
        } if ($userType == 'subscribed') {
            $chkUser = Yii::app()->common->isSubscribed($this->user_id);
            if (!$chkUser) {
                $response['error'] = true;
                $response['userType'] = $userType;
                $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                return $response;
            }
        }
    }

    function validateWatchDuration($watchDuraion) {
        if (!$watchDuraion) {
            return;
        }
        $subscription = $this->ppvSubscripton;
        $criteria = new CDbCriteria;
        $criteria->select = 't.created_date';
        $criteria->condition = 't.video_id =:stream_id AND user_id=:user_id AND t.created_date >= :created_date';
        $criteria->params = (array(':stream_id' => $this->stream_id, ':user_id' => $this->user_id, ':created_date' => $subscription['created_date']));
        //$criteria->condition = 't.video_id = ' . $stream_id;
        $criteria->order = 't.id ASC';

        $getFirstRow = VideoLogs::model()->find($criteria);
        if (count($getFirstRow)) {
            $seconds = strtotime($getFirstRow['created_date']) + $watchDuraion;
            if ($seconds <= strtotime($this->cur_date)) {
                $response['error'] = true;
                $response['expiryDate'] = date('Y-m-d H:i:s', $seconds);
                $response['watchDuraion'] = $watchDuraion;
                $response['message'] = Yii::app()->Controller->ServerMessage['watch_period_expired'];
                return $response;
            }
            return;
        }
    }

    function validateAccessDuration($accessDuration) {
        if (!$accessDuration) {
            return;
        }
        $getData = $this->ppvSubscripton;
        $seconds = strtotime($getData['created_date']) + $accessDuration;
        if ($seconds <= $this->cur_date) {
            $response['error'] = true;
            $response['expiryDate'] = date('Y-m-d H:i:s', $seconds);
            $response['accessDuration'] = $accessDuration;
            $response['message'] = Yii::app()->Controller->ServerMessage['access_period_expired'];
            return $response;
        }
        return;
    }

    ###################### Subscription Start##########################

    function checkUserSubscription($user_id = null) {
        if (!$user_id) {
            $user_id = $this->user_id;
        }
        //$is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        $userPlan = UserSubscription::model()->with(array('subscription_plan' => array('select' => 'rules_id')))->find(array("select" => 't.id', 'condition' => 't.user_id=:user_id AND t.studio_id=:studio_id AND t.status=1', "params" => array(':user_id' => $user_id, ':studio_id' => $this->studio_id)));
        /* $userPlan = Yii::app()->db->createCommand()
          ->select('c.rules_id')
          ->from('user_subscriptions a')
          ->join('subscription_plans b', 'b.id=a.plan_id')
          ->join('policy_rules c', 'c.ppv_plan_id=b.ppv_plan_id AND c.user_id=' . $this->user_id . ' AND c.status=1')
          ->where('a.id=:id AND a.studio_id=:studio_id', array(':id' => $stream_id, ':studio_id' => $this->studio_id))
          ->queryRow(); */
        if (empty($userPlan)) {
            return;
        } else {
            if ($userPlan['subscription_plan']['rules_id'] == '' || $userPlan['subscription_plan']['rules_id'] == 0) {
                return;
            } else {
                if ($rules = self::getRuleDetails($userPlan['subscription_plan']['rules_id'])) {
                    if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                        $response = array();
                        $resolution = self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                        if ($resolution) {
                            $response['resolution'] = $resolution;
                        }
                        $view = self::checkPolicyMapping($rules['policy_rules_mapping'], 1);
                        if ($view) {
                            $response['views'] = $view;
                        }
                        $wd = self::checkPolicyMapping($rules['policy_rules_mapping'], 3);
                        if ($wd) {
                            $response['watch_duration'] = $wd;
                        }
                        $ad = self::checkPolicyMapping($rules['policy_rules_mapping'], 4);
                        if ($ad) {
                            $response['access_duration'] = $ad;
                        }
                        $user = self::checkPolicyMapping($rules['policy_rules_mapping'], 5);
                        if ($user) {
                            $response['user'] = $user;
                        }
                        return $response;
                    }
                    return;
                }
            }
        }
    }

    ###################### Subscription End ##########################

    /* ############################# common function start ####################### */

    function isEnabledPolicy($studio_id = null) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $studioConfig = StudioConfig::model()->count('studio_id=:studio_id AND config_key="enable_policy" AND config_value=1', array(':studio_id' => $studio_id));
        return $studioConfig;
    }

    function getRuleDetails($rule_id = null) {
        if (!$rule_id)
            return;
        $rule = PolicyRules::model()->with('policy_rules_mapping')->find('t.id=:id AND t.studio_id=:studio_id AND t.status=1', array(':id' => $rule_id, ':studio_id' => $this->studio_id));
        return $rule;
    }

    function checkPolicyMapping($mappingArr = array(), $master_id = 2) {
        foreach ($mappingArr as $val) {
            if (($master_id == 3 || $master_id == 4) && $val['policy_rules_master_id'] == $master_id) {
                /* 1=Hour, 2=Day, 3=Month, 4=Minute */
                $duration_type = 0;
                if ($val['duration_type'] == 1) {
                    $duration_type = 60 * 60; /* hour */
                } else if ($val['duration_type'] == 2) {
                    $duration_type = 60 * 60 * 24; /* day */
                } else if ($val['duration_type'] == 3) {
                    $duration_type = 60 * 60 * 24 * 30; /* day */
                } else if ($val['duration_type'] == 4) {
                    $duration_type = 60; /* minute */
                }
                //$duration_type = $val['duration_type'] == 1 ? 60 * 60 : ($val['duration_type'] == 2 ? 60 * 60 * 24 : 60 * 60 * 24 * 30); /* convert in second */
                return $val['policy_value'] * $duration_type;
                exit;
            }
            if ($val['policy_rules_master_id'] == $master_id) {
                return $val['policy_value'];
                exit;
            }
        }
        return;
    }

    public function IsBundledsubscriptionPaid($content_id, $studio_id, $user_id) {
        //Find out the content is in subscription Bundled 
        $bundled_content = Yii::app()->db->createCommand()
                ->select('GROUP_CONCAT(subscriptionbundles_plan_id) AS plan_ids')
                ->from("subscriptionbundles_content")
                ->where('content_id =:content_id', array(':content_id' => $content_id))
                ->queryRow();
        $now = Date('Y-m-d H:i:s');
        $command = array();
        if (isset($bundled_content['plan_ids']) && trim($bundled_content['plan_ids'])) {

            $pid = explode(',', $bundled_content['plan_ids']);
            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from("user_subscriptions")
                    ->where('user_id =:user_id AND studio_id=:studio_id AND status=1 AND is_subscription_bundle=1', array(':user_id' => $user_id, ':studio_id' => $studio_id))
                    ->andWhere(array('IN', 'plan_id', $pid))
                    ->andWhere('end_date > :end_date', array(':end_date' => $now));
            $command->queryRow();
        }
        return $command;
    }

    public function IsAdvPaid($movie_id = 0, $stream_id = 0, $season = 0, $content_types_id = 1, $purchase_type = Null, $studio_id, $user_id) {
        //Explicitily pass from Rest controller otherwise default
        if (!isset($studio_id)) {
            $studio_id = $this->studio_id;
        }

        if (!isset($user_id)) {
            $user_id = $this->user_id;
        }

        $isPlay = 0;
        $response = array();
        if (isset($content_types_id) && $content_types_id == 3) {
            $stream = new movieStreams();
            $series_number = $stream->findByPk($stream_id)->series_number;
            $season = $series_number ? $series_number : 0;

            $cond = '';
            $condArr = array();

            if (isset($purchase_type) && $purchase_type == "episode") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)";
                $condArr = array(':season' => $season, ':episode_id' => $stream_id);
            } else if (isset($purchase_type) && $purchase_type == "season") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=0)";
                $condArr = array(':season' => $season);
            } else {
                $cond = "(season_id=0) AND (episode_id=0)";
            }

            $conditions = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id);
            $allcondarr = array_merge($conditions, $condArr);

            $command = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_advance_purchase=1 AND ' . $cond, $allcondarr, array('order' => 'movie_id, season_id, episode_id'));
            $row = $command->queryRow();

            if (isset($row) && !empty($row)) {
                if ($row['season_id'] == 0 && $row['episode_id'] == 0) {//Purchase entire show before
                    $isPlay = 1;
                } else if ($row['season_id'] != 0 && $row['episode_id'] == 0) {//Purchase entire season before
                    $isPlay = 1;
                } else if ($row['season_id'] != 0 && $row['episode_id'] != 0) {//Purchase entire episode before
                    $isPlay = 1;
                }
            }
            if (isset($row) && !empty($row) && $isPlay == 1) {
                $response = $row;
            }
        } else {
            $command = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_advance_purchase=1 AND season_id=0 AND episode_id=0', array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id), array('order' => 'movie_id, season_id, episode_id'));
            $row = $command->queryRow();

            if (isset($row) && !empty($row) && $isPlay == 1) {
                $response = $row;
            }
        }

        return $row;
    }

    function IsBundledPpvPaid($content_id, $studio_id, $user_id) {
        //Find out the content is in Bundled PPV
        $bundled_content = Yii::app()->db->createCommand()
                ->select('GROUP_CONCAT(ppv_plan_id) AS plan_ids')
                ->from("ppv_advance_content")
                ->where('content_id =:content_id', array(':content_id' => $content_id))
                ->queryRow();

        $now = Date('Y-m-d H:i:s');
        $is_ppv_bundle_subscribed = array();
        if (isset($bundled_content['plan_ids']) && trim($bundled_content['plan_ids'])) {
            $is_ppv_bundle_subscribed = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from("ppv_subscriptions")
                    ->where('ppv_plan_id IN (:ppv_plan_id) AND user_id =:user_id AND studio_id=:studio_id AND status=1  AND end_date > :end_date', array(':ppv_plan_id' => $bundled_content['plan_ids'], ':user_id' => $user_id, ':studio_id' => $studio_id, ':end_date' => $now))
                    ->queryRow();
        }

        return $is_ppv_bundle_subscribed;
    }

    function getPpvSubscriptoinPlan($ppv_subscription = null) {
        $query = array();
        if ($ppv_subscription) {
            if ($ppv_subscription['is_voucher'] == 1) {
                $query = Yii::app()->db->createCommand()
                        ->select('b.rules_id')
                        ->from('ppv_subscriptions a, voucher b')
                        ->where('a.ppv_plan_id=b.id AND a.id=:id', array(':id' => $ppv_subscription['id']))
                        ->order('a.created_date DESC')
                        ->queryRow();
            } else {
                $query = Yii::app()->db->createCommand()
                        ->select('b.rules_id')
                        ->from('ppv_subscriptions a, ppv_plans b')
                        ->where('a.ppv_plan_id=b.id AND a.id=:id', array(':id' => $ppv_subscription['id']))
                        ->order('a.created_date DESC')
                        ->queryRow();
            }
        }
        return $query;
    }

    /* ##################### common function end ############################## */

    function getRulesAPI($stream_uniq_id, $user_id, $studio_id=null) {
        if($studio_id){
            $this->studio_id = $studio_id;
        }
        if ($user_id && $stream_uniq_id) {
            //$movie_stream = movieStreams::model()->find(array('select'=>'id', 'condition'=>'embed_id=:embed_id', 'params'=>array(':embed_id'=>$stream_uniq_id)));
            $movie_stream = Yii::app()->db->createCommand()
                    ->select('a.id AS stream_id, a.is_episode, a.episode_title,a.episode_number, a.series_number, b.name')
                    ->from('movie_streams a, films b')
                    ->where('a.movie_id=b.id AND a.embed_id=:embed_id', array(':embed_id' => $stream_uniq_id))
                    ->queryRow();
            if (empty($movie_stream)) {
                $res['code'] = 656;
                $res['message'] = 'Incorrect stream id';
                $res['status'] = 'FAILURE';
            } else {
                $stream_id = $movie_stream['stream_id'];
                $rules = self::verifyRules($stream_id, $user_id);
                if (!empty($rules['error']) && $rules['error'] == true) {
                    $rule = $rules;
                    $res['code'] = 657;
                } else if (!empty($rules['access_duration'])) {
                    $rule = $rules['access_duration'];
                    $res['code'] = 658;
                } else if (!empty($rules['watch_duration'])) {
                    $rule = $rules['watch_duration'];
                    $res['code'] = 659;
                } else if (!empty($rules['view'])) {
                    $rule = $rules['view'];
                    $res['code'] = 660;
                } else if (!empty($rules['user'])) {
                    $rule = $rules['user'];
                    $res['code'] = 661;
                } else {
                    $rule = "";
                    $res['code'] = 200;
                    $res['status'] = 'OK';
                }
                $res['name'] = $movie_stream['name'];
                if ($movie_stream['is_episode'] == 1) {
                    if ($movie_stream['episode_title']) {
                        $res['name'] .= ' - ' . $movie_stream['episode_title'];
                    }if ($movie_stream['episode_number']) {
                        $res['episode_number'] = $movie_stream['episode_number'];
                    }if ($movie_stream['episode_number']) {
                        $res['episode_number'] = $movie_stream['episode_number'];
                    }if ($movie_stream['series_number']) {
                        $res['season'] = $movie_stream['series_number'];
                    }
                }
                $res['monetization'] = $this->ppvSubscripton['monetization'];
                $res['rules'] = $rule;
                $res['message'] = "Policy Rules";
            }
        } else {
            $res['code'] = 662;
            $res['message'] = 'User id or Stream id is required';
            $res['status'] = 'FAILURE';
        }
        return $res;
    }

}
