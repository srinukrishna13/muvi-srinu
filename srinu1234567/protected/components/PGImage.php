<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PGImage
 *
 * @author manas@muvi.com
 */
class PGImage extends CApplicationComponent {

    function getProductImageSize($studio_id) {
        $pg_standard = StudioConfig::model()->getConfig($studio_id, 'pg_poster_dimension');
        if (isset($pg_standard)) {
            $pg_standard_dimension = $pg_standard->config_value;
        } else {
            $pg_standard_dimension = "220x260";
        }
            $pg_thumb_dimension = "100x100";
        $data = array('standard_dimension' => $pg_standard_dimension, 'thumb_dimension' => $pg_thumb_dimension);
        return $data;
    }

    function processProductImage($productid, $fileinfo,$req) {
        $_FILES['Filedata'] = $fileinfo;
        $_REQUEST = $req;
        $studio_id = Yii::app()->common->getStudiosId();
        $poster_sizes = self::getProductImageSize($studio_id);

        $standard_size = strtolower($poster_sizes['standard_dimension']);
        $thumb_size = strtolower($poster_sizes['thumb_dimension']);

        $cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/pgposters/' . $productid;

        if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
            $cdimension = array('thumb' => "64x64");
            $ret1 = Controller::uploadToImageGallery($_FILES['Filedata'], $cdimension);

            $path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
            $ret = self::uploadProductImage($_FILES['Filedata'], $productid, $cropDimension, $path);
            if ($productid) {
                Yii::app()->common->rrmdir($dir);
            }
            return $ret;
        } else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name']);
            $_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $jcrop_allimage = $_REQUEST['jcrop_allimage'];
            $image_name = $_REQUEST['g_image_file_name'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];

            $path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
            $fileinfo['name'] = $_REQUEST['g_image_file_name'];
            $fileinfo['error'] = 0;
            $ret = self::uploadProductImage($fileinfo, $productid, $cropDimension, $path);
            if ($productid) {
                Yii::app()->common->rrmdir($dir);
            }          
            return $ret;
        } else {
            return false;
        }
    }

    function uploadProductImage($fileinfo, $productid , $dimension , $jcropPath = '') {
        if ($fileinfo) {
            $posterData = PGProductImage::model()->find('product_id=:product_id', array('product_id' => $productid));
            $fileName = Yii::app()->common->fileName($fileinfo['name']);
            if ($posterData) {
                $oldposter = $posterData->attributes;
                //Remove old poster images
                //Update poster data 
                $posterData->name = $fileName;
                $posterData->product_id = $productid;
                $posterData->feature = 1;
                $posterData->save();
                $uid = $productid;
            } else {
                $poster = new PGProductImage();
                $poster->name = $fileName;
                $poster->product_id = $productid;
                $poster->feature = 1;
                $poster->save();
                $uid = $productid;
            }
        }
        $studio_id = Yii::app()->common->getStudioId();
        //$uid = $_REQUEST['movie_id'];
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/pgposters'); //path for images uploaded
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);

        $config->setBucketName($bucketInfo['bucket_name']);
        $s3_config = Yii::app()->common->getS3Details($studio_id);
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  //maximum paralell uploads
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
        $config->setDimensions($dimension);   //resize to these sizes
        //usage of uploader class - this simple :)
        $uploader = new Uploader($uid);
        $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $ret = $uploader->uploadPoster($fileinfo, $unsignedBucketPath . "public/system/pgposters/", $jcropPath);
        $poster = array($ret);
        return $ret;
    }

}
