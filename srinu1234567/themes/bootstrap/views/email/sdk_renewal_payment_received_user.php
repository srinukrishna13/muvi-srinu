<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <title>Payment receipt</title>
</head>

    <body>  
    <table>
        <tr>
            <td><div mc:edit="logo">logo</div></td>
        </tr>
    </table>
        <p>Thank you for your renewal payment in <span mc:edit="website_name">website_name</span>.</p>
    <p>Here is your payment details.</p>
    <table style="background: #333;" cellspacing="1" cellpadding="5">
        <tr style="background: #FFF;">
            <td width="10%">Name</td>
            <td width="25%"><div mc:edit="display_name">display_name</div></td>
            <td width="10%">Email</td>
            <td><div mc:edit="email">email</div></td>            
        </tr>
        <tr style="background: #FFF;">
            <td>Paid Amount</td>
            <td><div mc:edit="order_amount">order_amount</div></td>   
            <td>Paid For</td>
            <td><div mc:edit="payment_towards">payment_towards</div></td>                
        </tr> 
        <tr style="background: #FFF;">
            <td>Subscribed On</td>
            <td><div mc:edit="subscribed_date">subscribed_date</div></td>
            <td>Next Payment Date</td>
            <td><div mc:edit="next_pay_date">next_pay_date</div></td>            
        </tr> 
        <tr style="background: #FFF;">
            <td>Invoice ID</td>
            <td><div mc:edit="invoice_id">invoice_id</div></td>
            <td>Order Number</td>
            <td><div mc:edit="order_number">order_number</div></td>            
        </tr>  
        <tr style="background: #FFF;">
            <td>Phone</td>
            <td><div mc:edit="phone">phone</div></td>
            <td></td>
            <td></td>            
        </tr>          
    </table>
</body>
</html>