<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Aws\Ses\SesClient; 
require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
class AdminController extends Controller {
    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = '';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        Yii::app()->theme = 'admin';
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            Yii::app()->layout = 'partners';
        }else{
            Yii::app()->layout = 'admin';
        }
        if (!(Yii::app()->user->id)) {
            $this->redirect(Yii::app()->getBaseUrl(true));exit;
        } else {
            $this->checkPermission();
        }

        return true;
    }

    //Added for the Dashboard page by RK
    public function actionDashboard() {
        //$this->redirect('managecontent');
        $this->breadcrumbs = array('Dashboard');
        $this->layout = 'admin';
        $ordering_section = Dashboard::model()->vieworder($this->studio->id);
        $this->render('dashboard_new', array('ordering_section' => $ordering_section,'email_verified'=>1));
    }
    
    public function actionresendEmailVerificationLink()
    {
       $user_logged_email=Yii::app()->user->getState('email');
       
       $client = SesClient::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                                'region' => 'us-east-1',
                    ));
        
        echo true;
    }

    public function ActionGetdaytime() {
        $msg = 'Morning';
        $now = $_REQUEST['timenow'];
        if ($now >= 0 && $now <= 12.00)
            $msg = 'Morning';
        else if ($now > 12.00 && $now <= 17.00)
            $msg = 'Afternoon';
        else
            $msg = 'Evening';
        $ret = array('msg' => $msg);
        echo json_encode($ret);
    }

    /**
     * @method public manageContent() All the contnent will be managed over here 
     * @author GDR<support@muvi.com>
     * @return html List of Contents
     */
    public function actionManagecontent() {
        $this->pageTitle = "Muvi | Content List";
        $this->breadcrumbs = array('Manage Content',"Content Library");
        $this->headerinfo = "Content Library";
        if($_REQUEST['searchForm']['contenttype']==2){$_REQUEST['searchForm']['is_physical']=1;}
        if($_REQUEST['searchForm']['is_physical']){            
            self::managePhysicalContent($_REQUEST);
            exit;
        }
        $cond = " ";
        $contentType = '';
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
		$user_id    = 0;
        if (Yii::app()->common->allowedUploadContent($studio_id) > 0)
            $allow_new = 1;
        else
            $allow_new = 0;

        $params = array(':studio_id' => $studio_id);
        $pdo_cond_arr = array(":studio_id" => Yii::app()->user->studio_id);
        $pdo_cond = " f.studio_id=:studio_id ";
        $con = Yii::app()->db;
		$content_type =  Yii::app()->general->content_count($studio_id);
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {
            $search_form = $_REQUEST['searchForm'];
            if (isset($search_form) && $search_form['update_date'] == '' && $search_form['content_category_value'] == '' && $search_form['search_text'] == '' && $search_form['custom_metadata_form_id'] == '' && $search_form['sortby'] == '') {
                $url = $this->createUrl('admin/managecontent');
                $this->redirect($url);
            } else {
                $searchData = $_REQUEST['searchForm'];
                if ($searchData['update_date']) {
                    $sdate = json_decode(stripslashes($searchData['update_date']), TRUE);
                    $pdo_cond .= " AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') >= :start_dt AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') <= :end_dt ";
                    $pdo_cond_arr[':start_dt'] = $sdate['start'];
                    $pdo_cond_arr[':end_dt'] = $sdate['end'];
                }
                if ($searchData['search_text']) {
                    $pdo_cond .= " AND ((LOWER(f.name) LIKE :search_text) OR (LOWER(ms.episode_title) LIKE :search_text)) ";
                    $pdo_cond_arr[':search_text'] = "%". strtolower(trim($searchData['search_text'])) . "%";
                }
				if ($searchData['contenttype']==1) {//Audio
					$pdo_cond .= " AND f.content_types_id IN(5,6) ";
				}else{
					$pdo_cond .= " AND f.content_types_id IN(1,2,3,4) ";
				}
                if($searchData['filterby']==0){
					if ($searchData['content_category_value']) {
						foreach ($searchData['content_category_value'] as $key => $value) {
							$cat_cond .= "FIND_IN_SET($value,f.content_category_value) OR ";
						}
						$cat_cond = rtrim($cat_cond, 'OR ');
                        $pdo_cond .= " AND ($cat_cond) ";
                        /*$pdo_cond .= " AND (f.content_category_value & :content_category_value) ";
                        $pdo_cond_arr[':content_category_value'] = array_sum($searchData['content_category_value']);*/
                    }
                }elseif($searchData['filterby']==1){
                    if ($searchData['custom_metadata_form_id']) {
						if(count($searchData['custom_metadata_form_id'])==1){
							$get_is_child = Yii::app()->db->createCommand()
								->from('custom_metadata_form')
								->select('content_type,is_child')
								->where('id ='.$searchData['custom_metadata_form_id'][0])
								->queryROW();
						}
						if((@$get_is_child['content_type']==1) && (@$get_is_child['is_child']==0)){
							$andwherecondition = "`f`.`custom_metadata_form_id` IN (".implode(',', $searchData['custom_metadata_form_id']).") AND ms.is_episode=0";
						}else{
							$andwherecondition = "(`f`.`custom_metadata_form_id` IN (".implode(',', $searchData['custom_metadata_form_id']).")) OR (`ms`.`custom_metadata_form_id` IN (".implode(',', $searchData['custom_metadata_form_id'])."))";
						}
                        $custom_metadata_form_id_flag = 1;
					}
				}
            }
		}else{
			if((isset($content_type) && ($content_type['content_count'] == 4))){
				$pdo_cond .= " AND f.content_types_id IN(5,6,8) ";
			}else{
				$pdo_cond .= " AND f.content_types_id IN(1,2,3,4) ";
			}
        }
		//echo $pdo_cond."<pre>";print_r($searchData);print_r($pdo_cond_arr);exit;
        $search_text = '';
        $default_lang_id = 20;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();
        
        $contentTypes = CHtml::listData($contenttypeList, 'id', 'category_name');
        $order = ' ms.id DESC ';
        $orderType = 'desc';
        $sort = 'update_date';
        if (isset($_REQUEST['sort'])) {
            if ($_REQUEST['sort'] == 'update_date' && $_REQUEST['sort_type'] == 'asc') {
                $order = ' ms.last_update_date ASC ';
                $orderType = 'asc';
            }
        }

        if (isset($searchData['sortby'])) {
            if ($searchData['sortby'] == 1) {//most viewed
                $mostviewed = 1;
            }elseif ($searchData['sortby'] == 2) {//A-Z
                $order = ' f.name ASC ';
            }elseif ($searchData['sortby'] == 3) {//Z-A
                $order = ' f.name DESC ';
            }
        }       
        //Pagination Implimented ..
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }

        //PDO Query
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            $this->pageTitle = ucwords($this->studio->name)." | Content List";
            $pcontent = Yii::app()->general->getPartnersContentIds();
            $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_category_value,f.content_types_id,f.name,f.ppv_plan_id,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,ms.thirdparty_url,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,ms.is_download_progress,ms.video_management_id,f.language_id,f.parent_id,ms.episode_parent_id,ms.episode_language_id,ms.is_demo,f.parent_content_type_id,f.custom_metadata_form_id,ms.custom_metadata_form_id as cmfid,ms.is_downloadable')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND f.parent_id=0 AND ms.episode_parent_id=0  AND ' . $pdo_cond, $pdo_cond_arr)
                ->andwhere(array('in', 'f.id', explode(',',$pcontent['movie_id'])))
                ->order($order)
                ->limit($page_size, $offset);
                if($custom_metadata_form_id_flag){
                    $command->andwhere($andwherecondition);
                }
        }else{
            $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_category_value,f.content_types_id,f.name,f.ppv_plan_id,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,ms.thirdparty_url,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,ms.is_download_progress,ms.video_management_id,f.language_id,f.parent_id,ms.episode_parent_id,ms.episode_language_id,ms.is_demo,f.parent_content_type_id,f.custom_metadata_form_id,ms.custom_metadata_form_id as cmfid,ms.is_offline,ms.is_downloadable')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND f.parent_id=0 AND ms.episode_parent_id=0 AND ' . $pdo_cond, $pdo_cond_arr)                
                ->order($order)
                ->limit($page_size, $offset);
                if($custom_metadata_form_id_flag){
                    $command->andwhere($andwherecondition);
                }
        }
		$offline_val= StudioConfig::model()->getconfigvalueForStudio($studio_id,'offline_view');
        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $pages = new CPagination($count);

        $pages->setPageSize($page_size);

        // simulate the effect of LIMIT in a sql query
        $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
        $sample = range($pages->offset + 1, $end);
        $form = Yii::app()->general->formlist($studio_id);
		$form = self::unsetForms($form,@$_REQUEST['searchForm']['is_physical']);
        if ($data) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            $movieIdsForliveStream = '';
            $movieLiveStream = array();
            $videoGalleryIdsForvideoName = '';
            $audioGalleryIdsForaudioName = '';
            $movievideogalleryName = array();
            $movieaudiogalleryName = array();
            $videoEncodingLogarray = array();
            $list = CHtml::listData($form,'id', 'name');
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .="'" . $v['stream_id'] . "',";
                } else {
                    $movieids .="'" . $v['id'] . "',";
                }
                // For live sreaming
                if($v['content_types_id'] == 4) {
                    $movieIdsForliveStream .="'" . $v['id'] . "',";
                }
                //video name from video gallery
                if($v['video_management_id'] != 0) {
                    $videoGalleryIdsForvideoName .="'" . $v['stream_id'] . "',";
                }
                //audio name from video gallery
                if($v['video_management_id'] != 0 && ($v['content_types_id'] == 6 || $v['content_types_id'] == 5)) {
                    $audioGalleryIdsForaudioName .="'" . $v['stream_id'] . "',";
                }
                //video name from video gallery
                if($v['full_movie'] != '' && $v['is_converted'] == 0) {
                    $videoEncodingLogarray[] = $v['stream_id'];
                }
				if ($v['content_types_id']==5 || ( $v['content_types_id']==6 && $v['is_episode']==1 )){
					$playlist = Yii::app()->common->getAllPlaylistName($studio_id,$user_id,1);
				}
                $data[$k]['formname'] = Yii::app()->general->getFormName($v,$studio_id,$form,$list);
            }
            $videoEncodingLog= "";
            $videoEncodingLog = implode(",", $videoEncodingLogarray);
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
            $movieIdsForliveStream = rtrim($movieIdsForliveStream, ',');
            $videoGalleryIdsForvideoName = rtrim($videoGalleryIdsForvideoName, ',');
            $audioGalleryIdsForaudioName = rtrim($audioGalleryIdsForaudioName, ',');
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['video_id']] = $val['cnt'];
                    }
                }
            }
            //Live stream url
            if($movieIdsForliveStream){
                $sql = "SELECT movie_id,feed_method,feed_url,stream_url,stream_key,start_streaming FROM livestream WHERE movie_id IN(" . $movieIdsForliveStream . ") and studio_id = ".$studio_id;
                $liveSreamData = $con->createCommand($sql)->queryAll();
                if ($liveSreamData) {
                    foreach ($liveSreamData as $key => $val) {
                        if($val['feed_url'] != ""){
                            $movieLiveStream[$val['movie_id']] = 1;
                            if($val['feed_method'] == 'push'){
                                $movieLiveStream['feed_method'][$val['movie_id']] = $val['feed_method'];
                                $movieLiveStream['stream_url'][$val['movie_id']] = $val['stream_url'];
                                $movieLiveStream['stream_key'][$val['movie_id']] = $val['stream_key'];
                                $movieLiveStream['start_streaming'][$val['movie_id']] = $val['start_streaming'];
                            }
                        }
                    }
                }
            }
            //Video name from video gallery
            if($videoGalleryIdsForvideoName || $audioGalleryIdsForaudioName){
                $sql = "SELECT ms.id, vm.video_name FROM movie_streams ms, video_management vm WHERE ms.video_management_id=vm.id and ms.id IN(" . $videoGalleryIdsForvideoName . ") and ms.studio_id = ".$studio_id;
                $videoManagement = $con->createCommand($sql)->queryAll();
                if ($videoManagement) {
                    foreach ($videoManagement as $key => $val) {
                        if($val['video_name'] != ""){
                            $movievideogalleryName[$val['id']] = $val['video_name'];
                        }
                    }
                }
                //Get Audio Name from Audio Gallery
                if($audioGalleryIdsForaudioName){
                    $sql = "SELECT ms.id, vm.audio_name FROM movie_streams ms, audio_gallery vm WHERE ms.video_management_id=vm.id and ms.id IN(" . $audioGalleryIdsForaudioName . ") and ms.studio_id = ".$studio_id;
                    $audioManagement = $con->createCommand($sql)->queryAll();
					if ($audioManagement) {
						foreach ($audioManagement as $key => $val) {
							if($val['audio_name'] != ""){
								$movieaudiogalleryName[$val['id']] = $val['audio_name'];
							}
						}
					}
				}            
            }            
            
            //Get video encoding data
            $encodingData = array();
            if($videoEncodingLog){
                $encLogData = Encoding::model()->getEncodingData($videoEncodingLog);
                if ($encLogData) {
                    foreach ($encLogData AS $key => $val) {
                        $now = new DateTime();
                        $post = new DateTime($val['expected_end_time']);
                        $interval = $now->diff($post);
                        if($interval){
                            $encodingData[$val['movie_stream_id']] = $interval->h.':'.$interval->i.':'.$interval->s;
                        } else{
                            $encodingData[$val['movie_stream_id']] = '0:0:0';
                        }
                    }
                }
            }
            
            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }
		
        $studio = $this->studio;
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            $pcontent = Yii::app()->general->getPartnersContentIds();
            $contentid = $pcontent['movie_id'];
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id,"$contentid",Yii::app()->user->id);
        }else{
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id);
        }
        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id,'embed_watermark');
        $embedWaterMArkEnable = 0;
        if($getStudioConfig){
            if(@$getStudioConfig['config_value'] == 1){
                $embedWaterMArkEnable = 1;
            }
        }      
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            $notpartner = 0;
        }else{
            $notpartner = 1;
            $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = ' . $studio_id));
            $geoexist = StudioContentRestriction::model()->exists('studio_id=:studio_id',array(':studio_id' => $studio_id));
        }
        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $data_enable = Yii::app()->general->monetizationMenuSetting($studio_id);
        $srest = StudioCountryRestriction::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $studio_id));
		$poster_sizes = Yii::app()->common->getCropDimension();
        if($mostviewed){
            foreach ($data AS $k => $v) {
                if($v['is_episode']){
                    $cnt = $episodeViewcount[$v['stream_id']]?$episodeViewcount[$v['stream_id']]:0;
                }else{
                    $cnt = ($viewcount[$v['id']])?$viewcount[$v['id']]:0;
				}
                $data[$k]['cnt'] = $cnt;
                $num[] = $cnt;
            }            
            array_multisort($num, SORT_DESC, $data);            
        }
		if(empty($data) && Yii::app()->general->getStoreLink()){
			if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {}else{
                            self::managePhysicalContent($_REQUEST,1);
            exit;
		}
		}
        $this->render('managecontent', array('movievideogalleryName' => @$movievideogalleryName,'movieaudiogalleryName' => $movieaudiogalleryName,'movieLiveStream' => @$movieLiveStream,'embedWaterMArkEnable' => $embedWaterMArkEnable ,'data' => $data, 'studio' => $studio, 'content_category_value' => @$searchData['content_category_value'], 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sample' => @$sample, 'contentList' => @$contentTypes, 'sort' => @$sort, 'orderType' => @$orderType, 'studio_ads' => @$studio_ads, 'allow_new' => $allow_new, 'geoexist'=>$geoexist,'notpartner'=>$notpartner,'bucketInfo'=>$bucketInfo,'data_enable'=>$data_enable,'srest'=>$srest,'grestcat'=>$grestcat,'form'=>$form,'searchData'=>$searchData, 'encodingData'=> @$encodingData,'playlistName'=> $playlist,'poster_size'=> $poster_sizes, 'videoEncodingLog' => $videoEncodingLog,'offline_val'=>$offline_val));
    }

    /**
     * @method logout() User logout method
     * @return void 
     */
    function actionAutocomplete() {
        $this->layout = false;
        $sq = $_REQUEST['term'];
        $arr = array();
        $cond = "LOWER(name) LIKE '%" . strtolower($sq) . "%' AND studio_id = " . Yii::app()->common->getStudiosId();
        if (isset($_REQUEST['content_type']) && $_REQUEST['content_type'] != '') {
            $cond = " AND LOWER(content_type)= '" . strtolower($_REQUEST['content_type']) . "' ";
        }
        $list = Film::model()->findAll(array("condition" => $cond));
        if ($list) {
            foreach ($list AS $key => $val) {
                $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'uniq_id' => $val['uniq_id']);
            }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public SearchList() Method for searching admin dashboard records.
     * @return void 
     * @author GDR<support@muvi.com> 
     */
    function actionMovieDetails() {
        $this->pageTitle = "Muvi | Movie Details";
        $this->breadcrumbs = array('Movie List' => array('admin/managecontent'), 'Edit Movie Info',);
        $connection = Yii::app()->db;
        $dbcon = Yii::app()->db2;
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $sql = "SELECT * FROM films WHERE  id='" . $_REQUEST['movie_id'] . "' LIMIT 1";
            $list = $dbcon->createCommand($sql)->queryAll();
        }
        if ($list) {
            $sql_loc = "SELECT full_movie,full_movie_webm,id FROM movie_streams WHERE  movie_id=" . $list[0]['id'] . " AND studio_user_id=" . Yii::app()->common->getStudiosId();
            $stream_data = $dbcon->createCommand($sql_loc)->queryAll();
            $cdata = $this->getMovieDetails($list[0]['id']);
            $result = json_decode($cdata, true);
            $detailsData['data'] = $cdata;
            $this->headerinfo = 'Edit Movie Info - ' . $list[0]['name'];
            if ($stream_data) {
                $result[0]['full_movie'] = $detailsData['full_movie'] = $stream_data[0]['full_movie'];
                $result[0]['full_movie_webm'] = $detailsData['full_movie_webm'] = $stream_data[0]['full_movie_webm'];
                $result[0]['movie_stream_id'] = $detailsData['movie_stream_id'] = $stream_data[0]['id'];
                if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) {
                    $this->breadcrumbs = array('Movie List' => array('admin/managecontent'), 'Preview Movie',);
                    $this->headerinfo = 'Preview Movie Info - ' . $list[0]['name'];
                    $this->render('maaflix', array('item' => $result[0], 'movieData' => $list[0]));
                } else {
                    $this->render('movieDetails', array('data' => $detailsData, 'movieData' => $list[0]));
                }
            } else {
                $this->render('movieView', array('data' => $detailsData, 'movieData' => $list[0]));
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry,We are not able to fetch the movie details');
            $url = $this->createUrl('admin/managecontent');
            $this->redirect($url);
        }
    }

    /**
     * @method type updateMovieDetails(type $paramName) Description
     * @return type Description
     */
    function actionUpdateMovieDetails() {
        $studio_id = $this->studio->id;
        $language_id = $this->language_id;
			//echo "<pre>";print_r($_REQUEST);print_r($_FILES);exit;
        if ($_REQUEST['movie_id'] && $_REQUEST['uniq_id']) {
            $Films = Film::model()->findByAttributes(array('id' => $_REQUEST['movie_id'], 'uniq_id' => $_REQUEST['uniq_id']));
            $MovieStreams = movieStreams::model()->findByAttributes(array('id' => $_REQUEST['movie_stream_id'], 'movie_id' => $_REQUEST['movie_id'], 'is_episode' => 0));
            if ($Films){
                 //Update content publish date if any
                $publish_date = NULL;
                if (@$_REQUEST['content_publish_date']) {
                    if (@$_REQUEST['publish_date']) {
                        $pdate = explode('/', $_REQUEST['publish_date']);
                        $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                        if (@$_REQUEST['publish_time']) {
                            $publish_date .= ' ' . $_REQUEST['publish_time'];
                        }
                    }
                }
                $film_detail = $Films;
                $film_lang = Film::model()->findByAttributes(array('studio_id'=>$studio_id,'language_id'=>$language_id,'parent_id'=>$_REQUEST['movie_id']));
                $data = $_REQUEST['movie'];
                $data['custom10'] = self::createSerializeCustomDate($data);
                $movie_id = $_REQUEST['movie_id'];
                if(!empty($film_lang)){
                    $Films = Film::model()->findByAttributes(array('id' => $film_lang->id, 'uniq_id' => $_REQUEST['uniq_id']));
                }
                if($Films ->language_id == $this->language_id){
                    if($Films->parent_id == 0){
                $Films->name = stripslashes($data['name']);
                if (isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
                    $Films->release_date = date('Y-m-d', strtotime($data['release_date']));
                else
                    $Films->release_date = null;
						$Films->content_category_value = implode(',', $data['content_category_value']);
				if(@$data['content_subcategory_value'])
							$Films->content_subcategory_value = implode(',', $data['content_subcategory_value']);
		$data['language'] = is_array(@$data['language'])?array_map('ucwords', $data['language']):$data['language'];
                $Films->language = is_array(@$data['language'])?json_encode(array_values(array_unique($data['language']))):@$data['language'];
		$data['genre'] = is_array(@$data['genre'])?array_map('ucwords', $data['genre']):$data['genre'];
                $Films->genre = is_array(@$data['genre'])?json_encode(array_values(array_unique($data['genre'])), JSON_UNESCAPED_UNICODE):@$data['genre'];
		$data['censer_rating'] = is_array(@$data['censer_rating'])?array_map('ucwords', $data['censer_rating']):$data['censer_rating'];
                $Films->censor_rating = is_array(@$data['censer_rating'])?json_encode(array_values(array_unique($data['censer_rating']))):@$data['censer_rating'];
                $Films->story = stripslashes($data['story']);
                $Films->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                $Films->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                $Films->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                $Films->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                $Films->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                $Films->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
                $Films->custom7 = @$data['custom7']?(is_array(@$data['custom7'])?json_encode(@$data['custom7']):@$data['custom7']):'';
                $Films->custom8 = @$data['custom8']?(is_array(@$data['custom8'])?json_encode(@$data['custom8']):@$data['custom8']):'';
                $Films->custom9 = @$data['custom9']?(is_array(@$data['custom9'])?json_encode(@$data['custom9']):@$data['custom9']):'';
                $Films->custom10 = @$data['custom10']?(is_array(@$data['custom10'])?json_encode(@$data['custom10']):@$data['custom10']):'';
                $Films->save();
                        $filmChildData = new Film;
						$attr = array('content_category_value' => implode(',', $data['content_category_value']));
                        $condition = "parent_id =:id";
                        $params = array(':id'=>$movie_id);
                        $filmChildData = $filmChildData->updateAll($attr,$condition,$params);
                    }else{
                        $Films->name = stripslashes($data['name']);
                        $Films->language = json_encode($data['language']);
                        $Films->genre = json_encode(array_values(array_unique($data['genre'])));
                        $Films->censor_rating = is_array(@$data['censer_rating'])?json_encode(array_values(array_unique($data['censer_rating']))):@$data['censer_rating'];
                        $Films->story = stripslashes($data['story']);
						$Films->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
						$Films->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
						$Films->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
						$Films->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
						$Films->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
						$Films->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
						$Films->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
						$Films->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
						$Films->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
						$Films->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                        $Films->save();
                    }

                    if (HOST_IP != '127.0.0.1'){
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery("cat:content AND sku:" . $studio_id ." AND content_id:". $movie_id ." AND stream_id:". $MovieStreams->id);                                          
                        $solrArr=array();
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
			$solrArr['content_id'] = $movie_id;
			$solrArr['stream_id'] = $MovieStreams->id;
			$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
			$solrArr['is_episode'] = $MovieStreams->is_episode;
			$solrArr['name'] = $data['name'];
			$solrArr['permalink'] =$Films->permalink;
			$solrArr['studio_id'] =  Yii::app()->user->studio_id;
			$solrArr['display_name'] = 'content';
			$solrArr['content_permalink'] =$Films->permalink;
                        $solrArr['genre_val'] =(count($data['genre'])>1)?implode(',',$data['genre']):$data['genre'][0];
                       	$solrArr['product_format'] = '';
                        if($publish_date!=NULL)
			$solrArr['publish_date'] = $publish_date;
                        $solrobjnew = new SolrFunctions();
			$ret = $solrobjnew->addSolrData($solrArr);              
                     }
                    
                }else{
                    $Film = new Film;
                    $Film->name = stripslashes($data['name']);
                    $Film->content_category_value = $Films->content_category_value;
                    $Film->content_types_id = $Films->content_types_id;
                    $Film->uniq_id = $Films->uniq_id;
                    $Film->language = json_encode($data['language']);
                    $Film->genre = json_encode(array_values(array_unique($data['genre'])));
                    $Film->censor_rating = is_array(@$data['censer_rating'])?json_encode(array_values(array_unique($data['censer_rating']))):@$data['censer_rating'];
                    $Film->story = stripslashes($data['story']);
                    $Film->studio_id = $studio_id;
                    $Film->parent_id = $Films->id;
                    $Film->search_parent_id = $Films->id;
                    $Film->permalink = $Films->permalink;
                    $Film->language_id = $this->language_id;
					$Film->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
					$Film->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
					$Film->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
					$Film->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
					$Film->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
					$Film->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
					$Film->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
					$Film->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
					$Film->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
					$Film->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                    $Film->save();
                    if (HOST_IP != '127.0.0.1'){
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery("cat:content AND sku:" . $studio_id ." AND content_id:". $movie_id ." AND stream_id:". $MovieStreams->id);                                          
                        $solrArr=array();
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
			$solrArr['content_id'] = $movie_id;
			$solrArr['stream_id'] = $MovieStreams->id;
			$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
			$solrArr['is_episode'] = $MovieStreams->is_episode;
			$solrArr['name'] =$data['name'];
			$solrArr['permalink'] =$Films->permalink;
			$solrArr['studio_id'] =  Yii::app()->user->studio_id;
			$solrArr['display_name'] = 'content';
			$solrArr['content_permalink'] =$Films->permalink;
                        $solrArr['genre_val'] = (count($data['genre'])>1)?implode(',',$data['genre']):$data['genre'][0];
                       	$solrArr['product_format'] = '';
                        if($publish_date!=NULL)
			$solrArr['publish_date'] = $publish_date;
                        $solrobjnew = new SolrFunctions();
						$ret = $solrObjnew->addSolrData($solrArr);                  
                     }
                    Yii::app()->user->setFlash('success', 'Wow! Your content updated successfully');
                    $this->redirect($this->createUrl('admin/managecontent'));
                    exit;
                }
                
				$MovieStreams->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
                
                $MovieStreams->content_publish_date = $publish_date;
                $MovieStreams->save();

                //Check and Save new Tags into database
                $movieTags = new MovieTag();
                $movieTags->addTags($data);
                $movie_id = $Films->id;
                if (isset($_REQUEST['content_filter_type'])) {
                    $contentFilterCls = new ContentFilter();
                    $addContentFilter = $contentFilterCls->addContentFilter($_REQUEST, $Films->content_type_id);
                }
				$this->processContentImage($data['content_types_id'],$movie_id,$_REQUEST['movie_stream_id']);
                $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
                if($checkfireappletv){
                    $this->processAppTVImage($data['content_types_id'],$movie_id,$_REQUEST['movie_stream_id']);
                }
                    
                if (isset($_FILES['topbanner']) && !($_FILES['topbanner']['error'])) {
                    $this->uploadPoster($_FILES['topbanner'], $movie_id, 'topbanner');
                }
              
             
                Yii::app()->user->setFlash('success', 'Wow! Your content updated successfully');
                $this->redirect($this->createUrl('admin/managecontent'));
                exit;
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry error in updating your data');
                $this->redirect($this->createUrl('admin/managecontent'));
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry you are not authorised to access this data');
            $this->redirect($this->createUrl('admin/managecontent'));
            exit;
        }
    }

    function createSerializeCustomDate($data,$flag=9){
        $cus = '';
        foreach ($data as $key => $value) {
            $k = (int) str_replace('custom', '', $key);
            if($k>$flag){
                $cus[] = array($key=>$value);
            }
        }
        return $cus;
    }
    function getSerializeCustomDate($list){
        if(@$list[0]['custom10']){
            $x = json_decode($list[0]['custom10'],true);
            foreach ($x as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $list[0][$key1] = $value1;
                }
            }
        }
        if(@$list[0]['cs6']){
            $y = json_decode($list[0]['cs6'],true);            
            foreach ($y as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $k = str_replace('custom', 'cs', $key1);
                    $list[0][$k] = $value1;
                }
            }
        }
        return $list;
    }
    /**
     * @method public celebAutocomp() Add cast and crew to a specific moive
     * @return json 
     */
    function actionCelebAutocomp() {
        $studio_id = $this->studio->id;
        $language_id = $this->language_id;
        $dbcon = Yii::app()->db;
		$_REQUEST['term'] = trim(trim($_REQUEST['term'],'"'),"'");
		$inCond = '';
		if($language_id && $language_id!=20){
			$condData = $dbcon->createCommand()
					->SELECT("parent_id")
					->FROM('celebrities')
					->WHERE("studio_id={$studio_id} AND language_id={$language_id}",array(":name"=>$_REQUEST['term']))
					->AndWhere(array("like","LOWER(name)","%".$_REQUEST['term']."%"))		
					->queryAll();
					//queryAll("SELECT parent_id FROM  WHERE LOWER(name) LIKE '%" .addslashes($_REQUEST['term']). "%' AND studio_id={$studio_id} AND language_id={$language_id}");
			if($condData){
				foreach ($condData AS $k=>$v){
					$inArr[] = $v['parent_id'];
				}
				$inCond = "AND id NOT IN (". implode(',', $inArr).")";
			}
		}
		//"SELECT parent_id FROM celebrities WHERE LOWER(name) LIKE '%" .addslashes($_REQUEST['term']). "%' AND studio_id={$studio_id} AND language_id={$language_id})""
        $cc_cmd = $dbcon->createCommand()
				->SELECT("name,id,parent_id")
				->FROM ("celebrities")
				->WHERE("studio_id={$studio_id}  AND (language_id={$language_id} OR parent_id=0 ".$inCond.")")
				->AndWhere(array("like","LOWER(name)","%".$_REQUEST['term']."%"))		
				->ORDER("id DESC");
        $data = $cc_cmd->queryAll();
        $arr = array();
        if ($data) {
            foreach ($data AS $key => $val) {
                $celeb_id = $val['id'];
                if($val['parent_id'] > 0){
                    $celeb_id = $val['parent_id'];
            }
                $arr['celeb'][] = array('celeb_name' => $val['name'], 'celeb_id' => $celeb_id);
        }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public addCastCrew() Add cast and crew to a specific moive
     * @return json 
     */
    function actionAddCastCrew() {
        $castid = ($_REQUEST['castid']=='')?0:$_REQUEST['castid'];
        $castname = trim(trim($_REQUEST['castname'],'"'),"'");
        $castchar = $_REQUEST['castchar'];
        $casttypearr = $_REQUEST['casttype'];
        if($castid==0 && $casttypearr!=''){
           
			$studio_id = Yii::app()->user->studio_id;
            //$duplicate = Yii::app()->db->createCommand("SELECT COUNT(*) AS CNT FROM celebrities WHERE name='".$castname."' AND studio_id=".$studio_id)->queryROW();
			$celeb1 = new Celebrity();
            $duplicate = $celeb1->findAll('name=:name AND studio_id=:studio_id', array(':name' => $castname,':studio_id'=>$studio_id));
			if($duplicate && (count($duplicate)>0)){
				$castid = $duplicate[0]->id;
			}else{
				$celeb = new Celebrity();
				$celeb->name = $castname;
				$celeb->studio_id = $studio_id;
				$celeb->parent_id = 0;
				$celeb->created_by = Yii::app()->user->id;
				$celeb->ip = $_SERVER['REMOTE_ADDR'];
				$celeb->created_date = gmdate('Y-m-d H:i:s');
				$celeb->language_id = 20;
				$celeb->save();
				$castid = $celeb->id;
				if (HOST_IP != '127.0.0.1') {
					$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
					$solrArr['content_id'] = $castid;
					$solrArr['stream_id'] = '';
					$solrArr['stream_uniq_id'] = '';
					$solrArr['is_episode'] = 0;
					$solrArr['name'] = addslashes($castname);
					$solrArr['permalink'] = $celeb->permalink; 
					$solrArr['studio_id'] = Yii::app()->user->studio_id;
					$solrArr['display_name'] = 'star';
					$solrArr['content_permalink'] = 'star';
					$solrArr['genre_val'] = '';
					$solrArr['product_format'] = '';
					$solrObj = new SolrFunctions();
					$ret_solr = $solrObj->addSolrData($solrArr);
				}
			}
        }
        $arr = array();
        /* if(!$castid){
          echo json_encode(array('error'=>1,'msg'=>'Only Existing casts can be added to the movie.Please add this cast in Manage Cast/Crew.'));exit;
          } */
        $movie_id = $_REQUEST['movie_id'];
		if($castid!=0 && $casttypearr!=''){
			$celb_data = Celebrity :: model()->findByPk($castid);
			$extdata = MovieCast :: model()->findAll(array("select" => "id,cast_type,cast_name,celebrity_id", "condition" => " movie_id=" . $movie_id . " AND celebrity_id=" . $castid));
			/* if($extdata){
			  echo json_encode(array('error'=>1,'msg'=>'Cast is already added for the Movie!'));exit;
			  } */
			if (!empty($casttypearr)) {
				$movie_cast = new MovieCast();
				$movie_cast->movie_id = $movie_id;
				$movie_cast->celebrity_id = $castid;
				$movie_cast->cast_type = json_encode([$casttypearr]);
				$movie_cast->cast_name = $castname;
				$movie_cast->ip = $_SERVER["REMOTE_ADDR"];
				$movie_cast->created_by = Yii::app()->user->id;

				$movie_cast->created_date = date('Y-m-d H:i:s');
				$movie_cast->save(false);
				$celeb_poster = $this->getPoster($castid, 'celebrity', 'thumb');
				if (getimagesize($celeb_poster) == false) {
					$celeb_poster = str_replace('/thumb/', '/medium/', $celeb_poster);
				}
				$img = $celeb_poster;

				//Issue Id 5332 end

				$ret['success'] = 1;
				$ret['img'] = $img;
                $ret['castid'] = $castid;
				echo json_encode($ret);
				exit;
			}
		}
    }

    /**
     * @method public removeCastCrew() Add cast and crew to a specific moive
     * @return json 
     */
    function actionRemoveCastCrew() {
        $castid = $_REQUEST['castid'];
        $movie_id = $_REQUEST['movie_id'];
        if ($movie_id && $castid) {
            $movie_cast = MovieCast :: model()->find(array("condition" => " movie_id=" . $movie_id . " AND celebrity_id=" . $castid));
            $movie_cast->delete();
        }
        echo 1;
        exit;
    }

    function actionAddPoster() {
        $ret = $this->uploadPoster($_FILES['Filedata'], $_REQUEST['movie_id'], 'films');
        if (is_array($ret)) {
            Yii::app()->user->setFlash('success', 'Wow! Poster of the movie updated successfully.');
            header("Location:" . $_SERVER['HTTP_REFERER']);
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error while updating the poster.');
            header("Location:" . $_SERVER['HTTP_REFERER']);
            exit;
        }exit;
    }

    /**
     * @method public addBannerImage()
     * @return bool
     * @author GDR<info@muvi.in>
     */
    function actionAddBannerImage() {
        if (isset($_FILES) && isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $ret = $this->uploadPoster($_FILES['Filedata'], $_REQUEST['movie_id'], 'topbanner', array('thumb' => '200x100'));
            if (is_array($ret)) {
                Yii::app()->user->setFlash('success', 'Wow! Banner of the movie updated successfuly.');
                header("Location:" . $_SERVER['HTTP_REFERER']);
                exit;
            }
        }
        Yii::app()->user->setFlash('error', 'Oops! Error while updating the poster.');
        header("Location:" . $_SERVER['HTTP_REFERER']);
        exit;
    }

    /**
     * @method public profile() Logged in Users Details 
     * @return HTML 
     */
    function actionProfile() {
        //$this->breadcrumbs=array('Dashboard'=>array('admin/dashboard'), 'Profile Details',);
        $this->breadcrumbs = array('Dashboard' => array('admin/managecontent'), 'Profile Details',);
        (array) $user = User::model()->findByPk(Yii::app()->user->id);
        //echo "<pre>";print_r($user->attributes);exit;
        $this->render('profile', array('userdata' => $user->attributes));
    }

    /**
     * @method public editProfile() Update the profile details
     * @return HTML 
     */
    function actionEditProfile() {
        //echo "<pre>";print_r($_REQUEST['profile']);exit;
        if (isset($_REQUEST['profile']) && !empty($_REQUEST['profile'])) {
            if (User::model()->updateByPk(Yii::app()->user->id, $_REQUEST['profile'])) {
                Yii::app()->user->setState('first_name', $_REQUEST['profile']['first_name']);
                Yii::app()->user->setState('lastname', $_REQUEST['profile']['last_name']);
                Yii::app()->user->setFlash('success', 'Wow! Your profile information has been updated successful!');
                $url = $this->createUrl('admin/profile');
                $this->redirect($url);
                exit;
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Error in updating your profile information.');
                $url = $this->createUrl('admin/editProfile');
                $this->redirect($url);
                exit;
            }
        }
        $this->breadcrumbs = array('Profile Details' => array('admin/profile'), 'Edit Profile',);
        (array) $user = User::model()->findByPk(Yii::app()->user->id);
        //$countries = Countries::model()->findAllByAttributes(array(),array(),array('countryName'=>'countryName'));
        $sql = "SELECT countryName FROM countries ORDER BY countryName";
        $country = Yii::app()->db->createCommand($sql)->queryAll();
        $this->render('editprofile', array('userdata' => $user->attributes, 'country' => $country));
    }

    /**
     * @method public removevideo() Removing video from the movie 
     * @author GDR<support@muvi.com>
     * @return Json 
     */
    function actionRemoveVideo() {
        $movie_id = $_REQUEST['movie_id'];
        if ($_REQUEST['is_ajax']) {
            $connection = Yii::app()->db;
            $dbcon = Yii::app()->db2;
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            //$bucket = Yii::app()->params->s3_bucketname;
            $bucket = 'muviassetsdev';
            $getMovieStream = $dbcon->createCommand('SELECT full_movie,full_movie_webm,id FROM movie_streams WHERE movie_id=' . $_REQUEST['movie_id'] . ' AND studio_user_id=' . Yii::app()->common->getStudiosId())->queryAll();
            if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                $s3dir = 'movieVideo/' . $getMovieStream[0]['id'] . "/";
            } else {
                $s3dir = 'uploads/movie_stream/full_movie/' . $getMovieStream[0]['id'] . "/";
            }
            if ($getMovieStream[0]['full_movie']) {
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir . $getMovieStream[0]['full_movie']
                ));
            }
            if ($getMovieStream[0]['full_movie_webm']) {
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir . $getMovieStream[0]['full_movie_webm']
                ));
            }

            $sql_ext = "UPDATE movie_streams SET full_movie='',full_movie_webm='' WHERE movie_id=" . $_REQUEST['movie_id'] . " AND studio_user_id=" . Yii::app()->common->getStudiosId();
            //$dbcon->createCommand($sql_ext)->execute();
            //$sql_local = 'UPDATE movie_streams SET full_movie="" WHERE movie_id='.$_REQUEST['movie_id'].' AND studio_user_id='.Yii::app()->common->getStudiosId();
            //$connection->createCommand($sql_local)->execute();
            if ($dbcon->createCommand($sql_ext)->execute()) {
                $arr['error'] = 0;
            } else {
                $arr['error'] = 1;
            }

            echo json_encode($arr);
            exit;
        }
    }

    /**
     * @method public removeVideo() Removing video from the movie 
     * @author GDR<support@muvi.com>
     * @return Json 
     */
    function actionRemoveMovie() {
        $movie_id = $_REQUEST['movie_id'];
        $getMovieStream = movieStreams::model()->findAll('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id));
        if ($movie_id && $getMovieStream) {
            $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $sub_dir = '';
            foreach ($getMovieStream AS $key => $val) {
                $streams = $val->attributes;
                $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $streams['id'] . "/";
                if ($streams['full_movie']) {
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucket,
                        'Prefix' => $s3dir
                    ));
                    foreach ($response as $object) {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $object['Key']
                        ));
                    }
                }
                if ($streams['is_episode'] != 1) {
                    //Remove Trailer
                    $this->removeTrailer($movie_id);
                    //Remove Poster 
                    $this->removePosters($movie_id, 'films');
                    //Remove Top Banner
                    $this->removePosters($movie_id, 'topbanner');
                    //Remove Data from films table 
                    Film::model()->deleteByPk($movie_id);
                    //Remove All Child contents
                    $params = array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id);
                    Film::model()->deleteAll('parent_id=:movie_id AND studio_id=:studio_id',$params);
                    //Remove Featured Content
                    UserFavouriteList::model()->deleteAll('content_id=:movie_id AND studio_id=:studio_id AND content_type="0"',$params);
                    FeaturedContent::model()->deleteAll('movie_id =:movie_id AND studio_id =:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id));
                } else {
                    //Remove Poster
                    $this->removePosters($streams['id'], 'moviestream');
                }
                $this->removeTvGuideEvent($streams['id']);
                     
                movieStreams::model()->deleteByPk($streams['id']);
                Encoding::model()->deleteAll('movie_stream_id =:movie_stream_id AND studio_id =:studio_id', array(':movie_stream_id' => $streams['id'], ':studio_id' => Yii::app()->user->studio_id));
                if (HOST_IP != '127.0.0.1') {
                    $solrobj = new SolrFunctions();
                    $solrobj->deleteSolrQuery('content_id:' . $movie_id . " AND stream_id:" . $streams['id']);
                    /* $solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );
                      $solr->deleteByQuery('content_id:'.$movie_id." AND stream_id:".$streams['id']);
                      $solr->commit(); //commit to see the deletes and the document
                      $solr->optimize(); */
                }
            }

            //Delete live stream content
            $liveStreamData = Livestream::model()->findbyAttributes(array('studio_id' => Yii::app()->user->studio_id, 'movie_id' => $movie_id));
            if($liveStreamData){
                if(@$liveStreamData->feed_method == 'push' && @$liveStreamData->stream_key != ''){
                    $streamName = explode("?",@$liveStreamData->stream_key);
                    if(@$streamName[0] != ''){
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=delete&stream_name=".@$streamName[0]);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $server_output = trim(curl_exec($ch));
                        curl_close($ch);
                    }
                }
                Livestream::model()->deleteAll('movie_id =:movie_id AND studio_id =:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id));
            }
            Yii::app()->user->setFlash('success', "Content removed successfully.");
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            Yii::app()->user->setFlash('error', "Oops! Sorry error in deleting content.");
            $this->redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function actionHome() {
        if (Yii::app()->user->id) {
            $this->redirect('admin/managecontent');
            exit;
        }
        $this->render('home');
    }

    /**
     * @method public uploadMovie() Add and Upload movie to studio.
     * @author GDR<support@muvi.com>
     * @return HTML 
     */
    function actionAddVideo() {
        $dbcon = Yii::app()->db2;
        $this->breadcrumbs = array('Movie List' => array('admin/managecontent'), 'Upload Video',);
        $this->headerinfo = "Upload Video - " . $_REQUEST['name'];
        $sql_check = "SELECT movie_id FROM movie_streams WHERE movie_id=" . $_REQUEST['movie_id'] . " AND studio_user_id=" . Yii::app()->common->getStudiosId() . ' LIMIT 1';
        $rowcount = $dbcon->createCommand($sql_check)->execute();
        if (!$rowcount) {
            Yii::app()->user->setFlash('error', 'Oops! You are not authorised to upload video to this movie');
            $url = $this->createUrl('admin/addContent');
            $this->redirect($url);
            exit;
        }
        $this->render('video', array('movie_id' => $_REQUEST['movie_id'], 'name' => $_REQUEST['name']));
    }

    function actionGetPoster() {
        if (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] == 1) {
            echo $this->getPoster($_REQUEST['movie_id'], 'moviestream', 'episode');
        } elseif (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] == 2){
            echo PGProduct::getpgImage($_REQUEST['movie_id'], 'standard');
        }elseif (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] == 4){
			echo $this->getPoster($_REQUEST['movie_id'], 'playlist_poster');
        }else{
            echo $this->getPoster($_REQUEST['movie_id'], 'films');
        }
        exit;
    }

    /**
     * @method public forgotPassword"() Forgot Password
     * @return string Description
     */
    function actionforgotPassword() {
        require_once 'mandrill-api-php/src/Mandrill.php';
        $email = $_REQUEST['email'];
        $record = User::model()->find('email=:email', array('email' => $email));
        if ($record) {
            $userdata = $record->attributes;
            if (!empty($userdata)) {
                //Reset password token is Generated with the combination of email and id(Auto Incr Id)
                $token = md5($userdata['id'] . $userdata['email']);
                $resetUrl = $this->createUrl('admin/resetPassword', array('token' => $token));
                $to = $userdata['email'];
                $from = 'muvi.com<support@muvi.com>';
                $subject = "Muvi::Reset Password";
                //Mandril Implementation
                //$mandrill = new Mandrill('API_KEY');
                $mandrill = new Mandrill('8754658DSAFAS');
                $template_name = 'forgot-password';
                $template_content = array(
                    array(
                        'name' => 'name',
                        'content' => $userdata['first_name'] ? $userdata['first_name'] : strtoupper(strstr($userdata['email'], '@', TRUE)),
                    ),
                    array(
                        'name' => 'resetlink',
                        'content' => $resetUrl
                    )
                );
                $message = array(
                    'subject' => $subject,
                    'from_email' => 'support@muvi.com',
                    'from_name' => 'Muvi',
                    'to' => array(
                        array(
                            'email' => $userdata['email'],
                            'name' => $userdata['first_name'] . " " . $userdata['last_name'],
                            'type' => 'to'
                        )
                    ),
                    'headers' => array('Reply-To' => 'support@muvi.com'),
                    'important' => false,
                    'track_opens' => null,
                    'track_clicks' => null,
                    'auto_text' => null,
                    'auto_html' => null,
                    'inline_css' => null,
                    'url_strip_qs' => null,
                    'preserve_recipients' => null,
                    'view_content_link' => null,
                    //'bcc_address' => 'message.bcc_address@example.com',
                    'tracking_domain' => null,
                    'signing_domain' => null,
                    'return_path_domain' => null,
                    'merge' => true,
                    'global_merge_vars' => array(
                        array(
                            'name' => 'name',
                            'content' => $userdata['first_name'] ? $userdata['first_name'] : strtoupper(strstr($userdata['email'], '@', TRUE)),
                        ),
                        array(
                            'name' => 'resetlink',
                            'content' => $resetUrl
                        )
                    ),
                );
                $async = false;
                //$ip_pool = 'Main Pool';
                //$send_at = date('m-d-Y H:i:s');
                $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);
                if ($result['status'] == 'sent') {
                    $query = "UPDATE user SET reset_password_token='" . $token . "', reset_password_sent_at=NOW() WHERE id=" . $userdata['id'];
                    $con = Yii::app()->db;
                    $cnt = $con->createCommand($query)->execute();
                    if ($cnt) {
                        $arr['err'] = 0;
                        $arr['msg'] = 'Reset Password Link has been send to your email. Please check and Confirm!';
                    } else {
                        $arr['err'] = 1;
                        $arr['msg'] = 'Not able to store in database';
                    }
                } else {
                    $arr['err'] = 1;
                    $arr['msg'] = 'Error while sending email: ' . $mail->getError();
                }
                echo PHP_EOL;
                echo json_encode($arr);
                exit;
            }
        }
        print_r($record->attributes);
        exit;
    }

    /**
     * @method public updatetrailerInfo() Check and updte the uploaded trailer informations for a content
     * @author GDR<support@muvi.com>
     * @return bool true/false
     */
    function updateTrailerInfo($videoGalleryData = '',$type=null) {
        $arr['msg'] = "error";
        if($type=='physical'){
            $model = 'PGMovieTrailer';
            $table = 'pg_movie_trailer';
            $dir = 'physical';
            $uniq_dir = 'physical_';
        } else {
            $model = 'movieTrailer';
            $table = 'movie_trailer';
            $dir = 'trailers';
            $uniq_dir = 'trailer_';
        }
        if ($_REQUEST['movie_id']) {
            $trailer = $model::model()->find('movie_id=:movie_id', array(':movie_id' => $_REQUEST['movie_id']));
            if ($trailer) {
                $data = $trailer->attributes;
                $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                ));
                $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
                $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
                //$bucket = Yii::app()->params->video_bucketname;
                $bucket = $bucketInfo['bucket_name'];
                $s3dir = $signedBucketPath . 'uploads/'.$dir.'/' . $data['id'] . "/";
                if ($data['trailer_file_name']) {
                    $response = $client->getListObjectsIterator(array(
                        'Bucket' => $bucket,
                        'Prefix' => $s3dir
                    ));
                    foreach ($response as $object) {
                        if ($object['Key'] != $s3dir . $_REQUEST['filename']) {
                            $result = $client->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $object['Key']
                            ));
                        }
                    }
                }
                if ($videoGalleryData) {
                    $videoFullPath = $signedBucketPath . 'uploads/'.$dir.'/' . $data['id'] . '/' . $videoGalleryData['filename'];
                    $videOnlyFileName = $videoGalleryData['filename'];
                    $trailer->upload_start_time = $upload_end_time;
                } else {
                    $videoFullPath = $_REQUEST['sendBackData']['key'];
                    $videOnlyFileName = $_REQUEST['filename'];
                }
                $dt = new DateTime();
                $upload_end_time = $dt->format('Y-m-d H:i:s');
                $bucketCloudfrontUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                $trailer->trailer_file_name = $videOnlyFileName;
                $trailer->video_remote_url = $bucketCloudfrontUrl . '/' . $videoFullPath;
                $trailer->last_updated_by = Yii::app()->user->id;
                $trailer->last_updated_date = gmdate('Y-m-d');
                $trailer->is_converted = 0;
                $trailer->has_sh = 0;
                $trailer->video_resolution = '';
                $trailer->upload_end_time = $upload_end_time;
                $trailer->mail_sent_for_trailer = 0;
                $trailer->save();
                $trailer_id = $trailer->id;
                $condition = ' AND mt.id=' . $trailer_id;
                $videoFolder = 'video';
                $shFolder = 'trailer';
                $conversionBucket = 'muvistudio';
                $ffmpeg_path = FFMPEG_PATH;
                if (HOST_IP == '127.0.0.1') {
                    $videoFolder = 'staging/video';
                    $shFolder = 'staging/trailer';
                    $conversionBucket = 'stagingstudio';
                } else if (HOST_IP == '52.0.64.95') {
                    $videoFolder = 'staging/video';
                    $shFolder = 'staging/trailer';
                    $conversionBucket = 'stagingstudio';
                }
                if($table=='pg_movie_trailer'){
                $trailerData = Yii::app()->db->createCommand()
                        ->select('mt.id AS trailer_id,mt.trailer_file_name AS trailer_name,mt.video_remote_url,s.s3bucket_id,s.studio_s3bucket_id,f.uniqid')
                        ->from('pg_movie_trailer mt, pg_product f,studios s')
                        ->where('f.id = mt.movie_id AND f.studio_id = s.id  AND mt.has_sh=0 AND mt.is_converted =0 AND mt.trailer_file_name!=\'\'' . $condition)
                        ->queryAll();
                } else{
                    $trailerData = Yii::app()->db->createCommand()
                        ->select('mt.id AS trailer_id,mt.trailer_file_name AS trailer_name,mt.video_remote_url,s.s3bucket_id,s.studio_s3bucket_id,f.uniq_id')
                        ->from('movie_trailer mt, films f,studios s')
                        ->where('f.id = mt.movie_id AND f.studio_id = s.id  AND mt.has_sh=0 AND mt.is_converted =0 AND mt.trailer_file_name!=\'\'' . $condition)
                        ->queryAll();
                }
                
                if ($trailerData) {
                    $lastbucketid = '';
                    $bucketName = '';
                    $s3url = '';
                    $videoResolutionData = VideoResolution::model()->findAll();
                    $videoResolutionScrr = array();
                    if($videoResolutionData){
                        foreach ($videoResolutionData as $videoResolutionDatakey => $videoResolutionDatavalue) {
                            $videoResolutionScrr[$videoResolutionDatavalue['resolution']] = explode(",",$videoResolutionDatavalue['resolution_to_be_encoded']);
                        }
                    }
                    foreach ($trailerData AS $key => $val) {
                        $videoName = $val['trailer_name'];
                        $remoteUrl = $val['video_remote_url'];
                        $trailerId = $val['trailer_id'];
                        if ($videoGalleryData) {
                            $videoGalleryId = $videoGalleryData['galleryId'];
                        } else {
                            $videoGalleryId = 0;
                        }

                        $uniq_id = ($type=='physical')?$val['uniqid']:$val['uniq_id'];

                        $updateUrl = BASE_URL . "/conversion/UpdateTrailerInfo/type/$type/stream_id/" . $val['trailer_id'] . "MUVIMUVI" . $uniq_id;
                        $urlForVideoNotConverted = BASE_URL . "/cron/UpdateTrailerNotConverted/type/$type/stream_id/" . $val['trailer_id'] . "MUVIMUVI" . $uniq_id . "MUVIMUVI" . $videoGalleryId;
                        $vidoInfo = new SplFileInfo($videoName);
                        $videoExt = $vidoInfo->getExtension();
                        $bucketName = $bucketInfo['bucket_name'];
                        $s3url = $bucketInfo['s3url'];
                        $s3cfg = $bucketInfo['s3cmd_file_name'];
                        $bucketHttpUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/'.$dir.'/' . $trailerId . '/'; 
                        $bucket_url = "s3://" . $bucketName . '/' . $signedBucketPath . "uploads/".$dir."/" . $trailerId . "/";
                        if ($videoGalleryData) {
                            $videoUrl = $videoGalleryData['url'];
                        } else {
                            $videoUrl = $bucketHttpUrl . $videoName;
                        }
                        $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
                        $updateUrl .= "MUVIMUVI" . $videoOnlyName . "MUVIMUVI" . $videoGalleryId;
                        $output = Yii::app()->common->get_video_resolution($videoUrl, $ffmpeg_path, $videoExt);
                        $video_Resolution = array();
                        $videoWidth = 0;
                        $videoHeight = 0;

                        $videoResolution['video_resolution'] = array(2160,1440,1080,720, 640, 360, 240, 144);
                        $videoResolution['video_screen_resolution'][2160] = 3840;
                        $videoResolution['video_screen_resolution'][1440] = 2560;
                        $videoResolution['video_screen_resolution'][1080] = 1920;
                        $videoResolution['video_screen_resolution'][720] = 1210;
                        $videoResolution['video_screen_resolution'][480] = 480;
                        $videoResolution['video_screen_resolution'][360] = 317;
                        $videoResolution['video_screen_resolution'][240] = 242;
                        $videoResolution['video_screen_resolution'][144] = 108;
                        if (isset($output['width']) && isset($output['height']) && $output['height'] != '' && $output['width'] != '') {
                            $videoHeight = $output['height'];
                            $videoWidth = $output['width'];
                            if ($videoHeight >= 2160) {
                                $videoResolution['default_video_resolution'] = 2160;
                            } else if ($videoHeight >= 1440) {
                                $videoResolution['default_video_resolution'] = 1440;
                            } else if ($videoHeight >= 1080) {
                                $videoResolution['default_video_resolution'] = 1080;
                            } else if ($videoHeight >= 720) {
                                $videoResolution['default_video_resolution'] = 720;
                            } else if ($videoHeight >= 480) {
                                $videoResolution['default_video_resolution'] = 480;
                            } else if ($videoHeight >= 360) {
                                $videoResolution['default_video_resolution'] = 360;
                            } else if ($videoHeight >= 240) {
                                $videoResolution['default_video_resolution'] = 240;
                            } else if ($videoHeight >= 144) {
                                $videoResolution['default_video_resolution'] = 144;
                            } else if ($videoHeight < 144) {
                                $videoResolution['default_video_resolution'] = $videoHeight;
                            }
                            if ($videoResolution['default_video_resolution'] != 0 && $videoWidth != 0) {
                                $functionParams = array();
                                $functionParams['field_name'] = 's3_trailer_folder';
                                if($val['s3bucket_id'] == 6){
                                    $functionParams['singapore_bucket'] = 1;
                                }
                                $shDataArray = Yii::app()->aws->getshUploadFolderPath($functionParams);
                                $s3folderPath = $shDataArray['shfolder'];
                                $threads = $shDataArray['threads'];
                                if(['default_video_resolution'] >=144){
                                    $video_Resolution = $videoResolutionScrr[$videoResolution['default_video_resolution']];
                                } else{
                                $video_Resolution = Yii::app()->common->getvideoResolutionForTheVideo($videoResolution['video_resolution'], $videoResolution['default_video_resolution']);
                                }
                                //Create shell script
                                $file = $dir.'SH_' . $trailerId .HOST_IP. '.sh';
                                $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
                                $cf = 'echo "${file%???}"';
                                $originalFileConversion = '';
                                $moveFile = '';
                                $removeFile = '';
                                $checkFileExistIfCondition = '';
                                $checkFileExistElseCondition = '';
                                //For original file conversion
                                    $originalFileConversion .= "cd /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/original \n";
                                if ($videoGalleryData) {
                                    $originalFileConversion .= "wget -O " . $videoName . " -e --robots=off -r --level=0 -nc  " . $videoUrl . " \n";
                                } else {
                                    $originalFileConversion .= "wget " . $videoUrl . " \n";
                                }
                                $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/original/" . $videoName . " " . $bucket_url . $videoOnlyName . "_original." . $videoExt . " \n";
                                $checkFileExistIfCondition .= 'if [ -s /var/www/html/' . $videoFolder . '/'.$uniq_dir . $trailerId . '/' . $videoOnlyName . '.mp4';

                                $updateUrl .="MUVIMUVIBEST";
                                $conversion_cmd = "";
                                //$copyCmd = 'cp /var/www/html/' . $videoFolder . '/' . $uniq_dir . $trailerId . '/' . $videoOnlyName . '_original.mp4 /var/www/html/' . $videoFolder . '/' . $uniq_dir . $trailerId . '/' . $videoOnlyName . '.mp4';
                                $copyCmd = '';
                                $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/" . $videoOnlyName . ".mp4 " . $bucket_url . " \n";

                                $conversion_cmd .= "/root/bin/ffmpeg -async 1 -i /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/original/" . $videoName. "  -movflags faststart -threads ".$threads." /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/" . $videoOnlyName . ".mp4";
                                $newVideoName = '';
                                //For multiple type conversion
                                foreach ($video_Resolution as $key => $value) {
                                    $videoFileName = $videoResolution['video_screen_resolution'][$value];
                                    $updateUrl .="MUVIMUVI" . $videoFileName;
                                    $screenWidth = round(($value / $videoHeight) * $videoWidth);
                                    if ($screenWidth % 2 != 0) {
                                        $screenWidth = $screenWidth + 1;
                                    }
                                    $newVideoName = $videoOnlyName . '_' . $videoFileName . '.mp4';
                                    $conversion_cmd.=" -s " . $screenWidth . ":" . $value . " -movflags faststart -threads ".$threads." /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/" . $newVideoName;
                                    $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/" . $videoFolder . "/$uniq_dir" . $trailerId . "/" . $newVideoName . " " . $bucket_url . " \n";
                                    $checkFileExistIfCondition .= ' -a -s /var/www/html/' . $videoFolder . '/' . $uniq_dir . $trailerId . '/' . $newVideoName;
                                }
                                $checkFileExistIfCondition .= " ]\n\nthen\n\n";
                                $checkFileExistElseCondition .="\n\nelse\n\n";
                                $checkFileExistElseCondition .= "curl $urlForVideoNotConverted\n";
                                $file_data = "file=`echo $0`\n" .
                                        "cf='" . $file . "'\n\n" .
                                        "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                        "then\n\n" .
                                        "echo \"$file is running\"\n\n" .
                                        "else\n\n" .
                                        "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                        "# create directory\n" .
                                        "mkdir /var/www/html/".$videoFolder."/".$uniq_dir . $trailerId."\n" .
                                        "chmod 0777 /var/www/html/".$videoFolder."/".$uniq_dir . $trailerId."\n" .
                                        "mkdir /var/www/html/".$videoFolder."/".$uniq_dir . $trailerId."/original\n" .
                                        "chmod 0777 /var/www/html/".$videoFolder."/".$uniq_dir . $trailerId."/original\n" .
                                        "# convertion command\n" .
                                        $originalFileConversion .
                                        "\n" .
                                        "# For BEST resolution\n" .
                                        $copyCmd .
                                        "\n" .
                                        $conversion_cmd .
                                        "\n\n\n# Check all the video file's are created\n" .
                                        $checkFileExistIfCondition .
                                        "\n# upload to s3\n" .
                                        $moveFile .
                                        "# update query\n" .
                                        "curl $updateUrl\n" .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/".$videoFolder."/".$uniq_dir . $trailerId."\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        $checkFileExistElseCondition .
                                        "# remove directory\n" .
                                        "rm -rf /var/www/html/".$videoFolder."/".$uniq_dir.$trailerId."\n" .
                                        "# remove the process copy file\n" .
                                        "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                        "# remove the process file\n" .
                                        "rm /var/www/html/$shFolder/$file\n" .
                                        "fi\n\n" .
                                        "fi";
                                fwrite($handle, $file_data);
                                fclose($handle);
                                //Uploading conversion script from local to s3 
                                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/progress/" . $file;
                                if ($s3->upload($conversionBucket, $s3folderPath . $file, fopen($filePath, 'rb'), 'public-read')) {
                                    if (unlink($filePath)) {
                                        $trailerData = $model::model()->findByPk($trailerId);
                                        $trailerData->encoding_start_time = date('Y-m-d H:i:s');
                                        $trailerData->has_sh = 1;
                                        $trailerData->video_management_id = $videoGalleryId;
                                        $return = $trailerData->save();
                                    }
                                }
                            } else {
                                $s3dir = $signedBucketPath . 'uploads/'.$dir.'/' . $trailerId . '/' . $videoName;
                                $client->deleteObject(array(
                                    'Bucket' => $bucketName,
                                    'Key' => $s3dir
                                ));
                                $trailerData = $model::model()->findByPk($trailerId);
                                $trailerData->is_converted = 2;
                                $trailerData->trailer_file_name = '';
                                $trailerData->video_remote_url = '';
                                $trailerData->encode_fail_time = date('Y-m-d H:i:s');
                                $trailerData->save();
                            }
                        } else {
                            $s3dir = $signedBucketPath . 'uploads/'.$dir.'/' . $trailerId . '/' . $videoName;
                            $client->deleteObject(array(
                                'Bucket' => $bucketName,
                                'Key' => $s3dir
                            ));
                            $trailerData = $model::model()->findByPk($trailerId);
                            $trailerData->is_converted = 2;
                            $trailerData->trailer_file_name = '';
                            $trailerData->video_remote_url = '';
                            $trailerData->encode_fail_time = date('Y-m-d H:i:s');
                            $trailerData->save();
                        }
                    }
                }
                $arr['msg'] = "Trailer updated successfully";
                $arr['trailer_url'] = CDN_HTTP . $bucketInfo['cloudfront_url'] . '/' . $_REQUEST['sendBackData']['key'];
            } else {
                $arr['msg'] = "data not found for content_id " . $_REQUEST['movie_id'];
            }
        } else {
            $arr['msg'] = "Movie id not defined-";
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public uploadsucc() Check and updte the uploaded content informations
     * @author GDR<support@muvi.com>
     * @return bool true/false
     */
    function actionUploadsucc() {
        $dbcon = Yii::app()->db;
        $getMovieStream = $dbcon->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,f.content_types_id,m.original_file,m.converted_file,m.is_downloadable  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' . $_REQUEST['movie_id'] . ' AND m.studio_id=' . Yii::app()->user->studio_id . ' AND m.id=' . $_REQUEST['movie_stream_id'])->queryAll();
        $original = explode('-', $getMovieStream[0]['original_file']);
        $converted = explode('-', $getMovieStream[0]['converted_file']);
        $storage = new StudioStorage();
        $resArray = StudioStorage::model()->findByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'file_type' => 'movie'));
        if (isset($resArray) && !empty($resArray)) {
            $data['studio_id'] = Yii::app()->common->getStudiosId();
            $data['original_video_file_count'] = $resArray->original_video_file_count - $original[0];
            $data['original_file_size'] = $resArray->original_file_size - $original[1];
            $data['converted_video_file_count'] = $resArray->converted_video_file_count - $converted[0];
            $data['converted_file_size'] = $resArray->converted_file_size - $converted[1];
            $data['total_file_size'] = $resArray->total_file_size - ($original[1] + $converted[1]);
            $data['file_type'] = 'movie';
            $data['last_updated_date'] = date('Y-m-d H:i:s');
            $storage->updateLog($resArray->id, $data);
        }


        //Removing the sample data from studio website after he added a content @RKS
        $studio = $this->studio;
        $studio->show_sample_data = 0;
        $studio->save();
        $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
        //$bucket = Yii::app()->params->video_bucketname;
        $folderPath = Yii::app()->common->getFolderPath("", Yii::app()->user->studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $bucket = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $getMovieStream[0]['id'] . "/";
        if ($getMovieStream[0]['full_movie']) {
            $response = $s3->getListObjectsIterator(array(
                'Bucket' => $bucket,
                'Prefix' => $s3dir
            ));
            foreach ($response as $object) {
                if ($object['Key'] != $s3dir . $_REQUEST['filename']) {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $object['Key']
                    ));
                }
            }
        }
        $ext = pathinfo($_REQUEST['filename'], PATHINFO_EXTENSION);
        $upload_end_time = date('Y-m-d H:i:s');
        $moveStream = movieStreams::model()->findByPk($_REQUEST['movie_stream_id']);
        $moveStream->full_movie = $_REQUEST['filename'];
        $moveStream->thirdparty_url = '';
        $moveStream->wiki_data = '';
        $moveStream->is_converted = 0;
        $moveStream->has_sh = 0;
        $moveStream->last_updated_by = Yii::app()->user->id;
        $moveStream->last_updated_date = gmdate('Y-m-d');
        $moveStream->upload_date = gmdate('Y-m-d');
        $moveStream->video_resolution = '';
        $moveStream->mail_sent_for_video = 0;
        $moveStream->upload_end_time = $upload_end_time;
        $moveStream->is_demo = '0';
        if ($getMovieStream[0]['content_types_id'] == 5 || $getMovieStream[0]['content_types_id'] == 6){
            $moveStream->is_converted = 1;
            /*
            * modified :   Arvind 
            * email    :   aravind@muvi.com
            * reason   :   Audio Gallery :  
            * functionality : add to audio gallery
            * date     :   24-1-2017
            */
            //$news3dir = $s3dir."".$_REQUEST['filename'];
            $paraDetails = array();
            $paraDetails['movie_stream_id']= $_REQUEST['movie_stream_id'];
            $paraDetails['studio_id']= Yii::app()->user->studio_id;
            $paraDetails['file_name']=$_REQUEST['filename'];
            $paraDetails['file_path'] = 'http://'.$bucket.'.'.$s3url.'/'.$s3dir;
            $audioGalleryId = Yii::app()->general->addaudioToAudioGallery($paraDetails);
            $_fullName = $paraDetails['file_path'].$paraDetails['file_name'];
            $moveStream->video_duration = Yii::app()->general->getAudioDuration($_fullName);  
            $moveStream->video_management_id = $audioGalleryId;
        }
        else{
            $moveStream->is_converted = 0; 
            $moveStream->video_management_id = 0;
        }
		if($getMovieStream[0]['is_downloadable']==1){
			$paraDetails = array();
			$paraDetails['movie_stream_id']= $_REQUEST['movie_stream_id'];
			$paraDetails['studio_id']= Yii::app()->user->studio_id;
			$paraDetails['file_name']=$_REQUEST['filename'];
			$paraDetails['file_path'] = 'http://'.$bucket.'.'.$s3url.'/'.$s3dir;
			$fileGalleryId = Yii::app()->general->addFileToFileGallery($paraDetails);
			$moveStream->is_converted = 1;
			$moveStream->file_management_id = $fileGalleryId;
		}
        $moveStream->save();
        $ret = 1;

        if ($getMovieStream[0]['content_types_id'] != 1 && $getMovieStream[0]['content_types_id'] != 5 && $getMovieStream[0]['content_types_id'] != 6) {
            $ret = $this->episodeThumbnail(Yii::app()->user->studio_id);
        }
        if(HOST_IP != '127.0.0.1'){
        if ($moveStream->is_episode) {
            $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
            $solrArr['content_id'] = $getMovieStream[0]['movie_id'];
            $solrArr['stream_id'] = $moveStream->id;
            $solrArr['stream_uniq_id'] = $moveStream->embed_id;
            $solrArr['is_episode'] = $moveStream->is_episode;
            $solrArr['name'] = $moveStream->episode_title ? $moveStream->episode_title : "Episode " . $moveStream->episode_number;
            $solrArr['permalink'] = $getMovieStream[0]['permalink'];
            $solrArr['display_name'] = 'content - Episode';
            $solrArr['studio_id'] = Yii::app()->user->studio_id;
            $solrArr['content_permalink'] = $getMovieStream[0]['permalink'];
                $solrArr['genre_val'] = '';
                $solrArr['product_format'] = '';
            $solrObj = new SolrFunctions();
            $ret = $solrObj->addSolrData($solrArr);
        }
        }
        if ($ret) {
            echo "success";
            exit;
        } else {
            echo "Error";
            exit;
        }
    }

    /**
     * @method public addepisode() Adding Episode for a Serise
     * @return HTML 
     * @author GDR<info@muvi.com>
     */
    function actionAddEpisode() {
        //echo "<pre>";print_r($_REQUEST);exit;
        $this->pageTitle = "Muvi | Add Episode";
        $uniq_id = @$_REQUEST['uniq_id'];
        $studio_id = $this->studio->id;
        $language_id = $this->language_id;
        $pgconnection = Yii::app()->db;
        //$_REQUEST['movie_id'] = $_REQUEST['movie_id']?$_REQUEST['movie_id']:@$_REQUEST['episode']['content_name'];
        $_REQUEST['movie_id'] = @$_REQUEST['episode']['content_name'] ? @$_REQUEST['episode']['content_name'] : $_REQUEST['movie_id']; //modified by manas
        $movie_id = @$_REQUEST['movie_id'];
        if (isset($_REQUEST['submit_btn']) && $_REQUEST['submit_btn'] && isset($_REQUEST['episode']) && $_REQUEST['episode']) {
            $data = $_REQUEST['episode'];
            $data['custom6'] = self::createSerializeCustomDate($data, 5);
            $data['movie_id'] = $movie_id;
            $rowcount = '';

            if (!($_REQUEST['editflag']) && $data['episode_number']) {
                $episodeData = movieStreams::model()->find('movie_id=:movie_id AND studio_id=:studio_id AND episode_number=:episode_number AND series_number=:series_number AND is_episode=:is_episode AND episode_parent_id=0', array(':is_episode' => 1, ':series_number' => $data['series_number'], ':episode_number' => $data['episode_number'], ':movie_id' => $_REQUEST['movie_id'], ':studio_id' => Yii::app()->user->studio_id));
            } elseif ($_REQUEST['editflag']) {
                $episodeData = movieStreams::model()->find('movie_id=:movie_id AND studio_id=:studio_id AND episode_number=:episode_number AND series_number=:series_number AND is_episode=:is_episode AND episode_parent_id=0 AND id !=:id', array(':is_episode' => 1, ':series_number' => $data['series_number'], ':episode_number' => $data['episode_number'], ':movie_id' => $_REQUEST['movie_id'], ':studio_id' => Yii::app()->user->studio_id, ':id' => $_REQUEST['movie_stream_id']));
            }
            if (!$episodeData) {
                $epdt = $data['episode_date'] ? date('Y-m-d', strtotime($data['episode_date'])) : gmdate('Y-m-d');
                if ($_REQUEST['editflag']) {
                    $epData = movieStreams::model()->findByPk($_REQUEST['movie_stream_id']);
                    $episode_lang = movieStreams::model()->find('studio_id='.$studio_id.' AND episode_parent_id='.$_REQUEST['movie_stream_id'].' AND episode_language_id='.$language_id);
                    //print_r($episode_lang);exit;
                    if(!empty($episode_lang)){
                        $stream_id = $episode_lang->id;
                        $epData = movieStreams::model()->findByPk($stream_id);
                    }
                    $getMovieStream = Yii::app()->db->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,f.content_types_id,m.original_file,m.converted_file  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' . $_REQUEST['movie_id'] . ' AND m.studio_id=' . Yii::app()->user->studio_id . ' AND m.id=' . $_REQUEST['movie_stream_id'])->queryAll();
                    if($epData->episode_language_id == $this->language_id){
                        if($epData->episode_parent_id == 0){
                    $epData->episode_title = $data['title'];
                    $epData->episode_number = $data['episode_number'];
                    $epData->movie_id = $_REQUEST['movie_id'];
                    $epData->series_number = $data['series_number']? $data['series_number']:1;
                    $epData->episode_story = $data['story'];
                    $epData->episode_date = $epdt;
                    $epData->last_updated_by = Yii::app()->user->id;
                    $epData->last_updated_date = gmdate('Y-m-d H:i:s');
                    /*Add custom data*/
                    $epData->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                    $epData->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                    $epData->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                    $epData->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                    $epData->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                    $epData->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
                    $publish_date = NULL;
                    if (!empty($_REQUEST['content_publish_date'])) {
                        if (@$_REQUEST['publish_date']) {
                            $pdate = explode('/', $_REQUEST['publish_date']);
                            $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                            if (@$_REQUEST['publish_time']) {
                                $publish_date .= ' ' . $_REQUEST['publish_time'];
                            }
                        }
                    }

                    $epData->content_publish_date = $publish_date;
                    $epData->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);
                    
                    $return = $epData->save();
                    $epChildData = new movieStreams();
                    $attr= array('episode_number'=>$data['episode_number'],'movie_id'=>$_REQUEST['movie_id'],'series_number'=>$data['series_number']);
                    $condition = "episode_parent_id =:id";
                    $params = array(':id'=>$_REQUEST['movie_stream_id']);
                    $epChildData = $epChildData->updateAll($attr,$condition,$params);
                }else{
                    $epData->episode_title = $data['title'];
                    $epData->episode_story = $data['story'];
					$epData->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                    $epData->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                    $epData->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                    $epData->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                    $epData->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                    $epData->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
                    $publish_date = NULL;
                    if (!empty($_REQUEST['content_publish_date'])) {
                        if (@$_REQUEST['publish_date']) {
                            $pdate = explode('/', $_REQUEST['publish_date']);
                            $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                            if (@$_REQUEST['publish_time']) {
                                $publish_date .= ' ' . $_REQUEST['publish_time'];
                            }
                        }
                    }

                    $epData->content_publish_date = $publish_date;
                    $epData->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
                    $return = $epData->save();
                }
                    }else{
                       $episodeData = new movieStreams(); 
                       $episodeData->episode_title = $data['title'];
                       $episodeData->episode_number = $data['episode_number'];
                       $episodeData->movie_id = $_REQUEST['movie_id'];
                       $episodeData->series_number = $data['series_number'];
                       $episodeData->is_episode = 1;
                       $episodeData->episode_story = $data['story'];
                       $episodeData->episode_parent_id = $_REQUEST['movie_stream_id'];
                       $episodeData->studio_id = $studio_id;
                       $episodeData->episode_language_id = $this->language_id;
                        /*Add custom data*/
                        $episodeData->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                        $episodeData->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                        $episodeData->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                        $episodeData->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                        $episodeData->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                        $episodeData->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
                        $publish_date = NULL;
                        if (!empty($_REQUEST['content_publish_date'])) {
                            if (@$_REQUEST['publish_date']) {
                                $pdate = explode('/', $_REQUEST['publish_date']);
                                $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                                if (@$_REQUEST['publish_time']) {
                                    $publish_date .= ' ' . $_REQUEST['publish_time'];
                                }
								}
                     }
                    if(HOST_IP != '127.0.0.1'){
                        if ($episodeData->is_episode) {
                            $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                            $solrArr['content_id'] = $_REQUEST['movie_id'];
                            $solrArr['stream_id'] = $episodeData->id;
                            $solrArr['stream_uniq_id'] = $episodeData->embed_id;
                            $solrArr['is_episode'] = $episodeData->is_episode;
                            $solrArr['name'] = $episodeData->episode_title ? $episodeData->episode_title : "Episode " . $episodeData->episode_number;
                            $solrArr['permalink'] = $getMovieStream[0]['permalink'];
                            $solrArr['display_name'] = 'content - Episode';
                            $solrArr['studio_id'] = Yii::app()->user->studio_id;
                            $solrArr['content_permalink'] = $getMovieStream[0]['permalink'];
                            $solrArr['genre_val'] = '';
                            $solrArr['product_format'] = '';
                             if($publish_date!=NULL)
                            $solrArr['publish_date'] = $publish_date;   
                            $solrArr['product_format'] = '';
                            $solrObj = new SolrFunctions();
                            $ret = $solrObj->addSolrData($solrArr);
                            }
                        }

                        $episodeData->content_publish_date = $publish_date;
                        $episodeData->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
                       $return = $episodeData->save();
                    
                    }
                } else {
                    $epData = new movieStreams();
                    $epData->episode_title = $data['title'];
                    $epData->episode_number = $data['episode_number'];
                    $epData->series_number = $data['series_number'];
                    $epData->studio_id = Yii::app()->user->studio_id;
                    $epData->episode_story = $data['story'];
                    $epData->episode_date = $epdt;
                    $epData->movie_id = $_REQUEST['movie_id'];
                    $epData->created_by = Yii::app()->user->id;
                    $epData->created_date = gmdate('Y-m-d');
                    $epData->last_updated_date = gmdate('Y-m-d H:i:s');
                     /*Add custom data*/
                    $epData->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                    $epData->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                    $epData->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                    $epData->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                    $epData->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                    $epData->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
                    
                    $epData->is_episode = 1;
                    $publish_date = NULL;
                    if (!empty($_REQUEST['content_publish_date'])) {
                        if (@$_REQUEST['publish_date']) {
                            $pdate = explode('/', $_REQUEST['publish_date']);
                            $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                            if (@$_REQUEST['publish_time']) {
                                $publish_date .= ' ' . $_REQUEST['publish_time'];
                            }
                        }
                    }

                    $epData->content_publish_date = $publish_date;
                    $epData->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
                    $epData->isNewRecord = true;
                    $epData->primaryKey = NULL;
                    $return = $epData->save();
               $getMovieStream = Yii::app()->db->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,f.content_types_id,m.original_file,m.converted_file  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' . $_REQUEST['movie_id'] . ' AND m.studio_id=' . Yii::app()->user->studio_id . ' AND m.id=' . $epData->id)->queryAll();
                       
              if(HOST_IP != '127.0.0.1'){
               if ($epData->is_episode) {
                   $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                   $solrArr['content_id'] = $_REQUEST['movie_id'];
                   $solrArr['stream_id'] = $epData->id;
                   $solrArr['stream_uniq_id'] = $epData->embed_id;
                   $solrArr['is_episode'] = $epData->is_episode;
                   $solrArr['name'] = $epData->episode_title ? $epData->episode_title : "Episode " . $epData->episode_number;
                   $solrArr['permalink'] = $getMovieStream[0]['permalink'];
                   $solrArr['display_name'] = 'content - Episode';
                   $solrArr['studio_id'] = Yii::app()->user->studio_id;
                   $solrArr['content_permalink'] = $getMovieStream[0]['permalink'];
                   $solrArr['genre_val'] = '';
                   $solrArr['product_format'] = '';
                        if($publish_date!=NULL)
                   $solrArr['publish_date'] = $publish_date;
                   $solrObj = new SolrFunctions();
                   $ret = $solrObj->addSolrData($solrArr);
                }
               }
                }
                $this->processContentImage(3, $movie_id, $epData->id, 1);
                $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
                if ($checkfireappletv) {
                    $this->processAppTVImage(3, $movie_id, $epData->id, 1);
                }
                if (@$return && $_REQUEST['editflag']) {
                    Yii::app()->user->setFlash('success', 'Wow! Your episode updated successfully.');
                    $this->redirect($this->createUrl('admin/managecontent'));
                    exit;
                } elseif (@$return) {
                    Yii::app()->user->setFlash('success', 'Wow! Your episode added successfully.');
                    $this->redirect($this->createUrl('admin/managecontent'));
                    exit;
                } else {
                    Yii::app()->user->setFlash('error', 'Oops! Error in adding episode. Pleas try again!');
                    $this->redirect($_SERVER['HTTP_REFERER']);
                    exit;
                } exit;
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Episode#-' . $data['episode_number'] . ' is already exists.');
                $this->redirect($_SERVER['HTTP_REFERER']);
                exit;
            }
        } else if ($movie_id) {
            $movieData = $pgconnection->createCommand("SELECT F.uniq_id,ms.movie_id,F.name,F.content_types_id FROM movie_streams ms,films F WHERE ms.movie_id=F.id AND ms.studio_id=" . Yii::app()->user->studio_id . " AND F.id=" . $movie_id . " AND F.content_types_id=3 AND ms.is_episode=0")->queryAll();
            $this->breadcrumbs = array('Content' => array('admin/multipartContent?movie_id=' . $_REQUEST['movie_id'] . '&uniq_id=' . $_REQUEST['uniq_id']), 'Add Episode',);
            if ($movieData) {
                $this->render('addepisode', array('movieDetails' => $movieData));
            } else {
                Yii::app()->user->setFlash('error', 'Oops! You are not authorised to access.');
                $this->redirect($_SERVER['HTTP_REFERER']);
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You are not authorised to access.');
            $this->redirect($_SERVER['HTTP_REFERER']);
            exit;
        }
    }

    /**
     * @method public addpisode() Adding Episode for a Serise
     * @return HTML 
     * @author GDR<info@muvi.com>
     */
    function actionAjaxAddEpisode() {
		$uniq_id = @$_REQUEST['uniq_id'];
        $pgconnection = Yii::app()->db;
        $_REQUEST['movie_id'] = $_REQUEST['movie_id'] ? $_REQUEST['movie_id'] : @$_REQUEST['episode']['content_name'];
        $movie_id = @$_REQUEST['movie_id'];
        if (isset($_REQUEST['episode']) && $_REQUEST['episode']) {
            $data = $_REQUEST['episode'];
            $data['custom6'] = self::createSerializeCustomDate($data,5);
            $data['movie_id'] = $movie_id;
            $rowcount = '';

            if (!($_REQUEST['editflag']) && $data['episode_number']) {
                $episodeData = movieStreams::model()->find('movie_id=:movie_id AND studio_id=:studio_id AND episode_number=:episode_number AND series_number=:series_number AND is_episode=:is_episode', array(':is_episode' => 1, ':series_number' => $data['series_number'], ':episode_number' => $data['episode_number'], ':movie_id' => $_REQUEST['movie_id'], ':studio_id' => Yii::app()->user->studio_id));
            } elseif ($_REQUEST['editflag']) {
                $episodeData = movieStreams::model()->find('movie_id=:movie_id AND studio_id=:studio_id AND episode_number=:episode_number AND series_number=:series_number AND is_episode=:is_episode AND id !=:id', array(':is_episode' => 1, ':series_number' => $data['series_number'], ':episode_number' => $data['episode_number'], ':movie_id' => $_REQUEST['movie_id'], ':studio_id' => Yii::app()->user->studio_id, ':id' => $_REQUEST['movie_stream_id']));
            }
            if (!$episodeData) {
                $epdt = $data['episode_date'] ? date('Y-m-d', strtotime($data['episode_date'])) : NULL;//gmdate('Y-m-d')
                if ($_REQUEST['editflag']) {
                    $epData = movieStreams::model()->findByPk($_REQUEST['movie_stream_id']);
                    $epData->episode_title = $data['title'];
                    $epData->episode_number = $data['episode_number'];
                    $epData->episode_story = $data['story'];
                    $epData->episode_date = $epdt;
                    $epData->last_updated_by = Yii::app()->user->id;
                    $epData->last_updated_date = gmdate('Y-m-d H:i:s');
                    /*Add custom data*/
                    $epData->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                    $epData->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                    $epData->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                    $epData->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                    $epData->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                    $epData->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
                    if(@$_REQUEST['movie']['custom_metadata_form_id']){
                        $epData->custom_metadata_form_id = @$_REQUEST['movie']['custom_metadata_form_id'];
                    }
					$epData->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
                    $return = $epData->save();
                    $getMovieStream = Yii::app()->db->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,f.content_types_id,m.original_file,m.converted_file  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' . $_REQUEST['movie_id'] . ' AND m.studio_id=' . Yii::app()->user->studio_id . ' AND m.id=' . $epData->id)->queryAll();
               
                    if(HOST_IP != '127.0.0.1'){
                        if ($epData->is_episode) {
                            $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                            $solrArr['content_id'] = $_REQUEST['movie_id'];
                            $solrArr['stream_id'] = $epData->id;
                            $solrArr['stream_uniq_id'] = $epData->embed_id;
                            $solrArr['is_episode'] = $epData->is_episode;
                            $solrArr['name'] = $epData->episode_title ? $epData->episode_title : "Episode " . $epData->episode_number;
                            $solrArr['permalink'] = $getMovieStream[0]['permalink'];
                            $solrArr['display_name'] = 'content - Episode';
                            $solrArr['studio_id'] = Yii::app()->user->studio_id;
                            $solrArr['content_permalink'] = $getMovieStream[0]['permalink'];
                            $solrArr['genre_val'] = '';
                            $solrArr['product_format'] = '';
                             if($publish_date!=NULL)
                            $solrArr['publish_date'] = $publish_date;   
                            $solrArr['product_format'] = '';
                            $solrObj = new SolrFunctions();
                            $ret = $solrObj->addSolrData($solrArr);
                            }
                        }
                } else {
                    $epData = new movieStreams();
                    $epData->episode_title = $data['title'];
                    $epData->episode_number = $data['episode_number'];
                    $epData->series_number = $data['series_number'] ? $data['series_number']:1;
                    $epData->studio_id = Yii::app()->user->studio_id;
                    $epData->episode_story = $data['story'];
                    $epData->episode_date = $epdt;
                    $epData->embed_id = Yii::app()->common->generateUniqNumber();
                    $epData->movie_id = $_REQUEST['movie_id'];
                    $epData->created_by = Yii::app()->user->id;
                    $epData->created_date = gmdate('Y-m-d');
                    $epData->last_updated_date = gmdate('Y-m-d H:i:s');
                    $epData->is_episode = 1;
                    /*Add custom data*/
                    $epData->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):NULL;
                    $epData->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):NULL;
                    $epData->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):NULL;
                    $epData->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):NULL;
                    $epData->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):NULL;
                    $epData->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):NULL;
                    if(@$_REQUEST['movie']['custom_metadata_form_id']){
                        $epData->custom_metadata_form_id = @$_REQUEST['movie']['custom_metadata_form_id'];
                    }
					$epData->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
                    $epData->isNewRecord = true;
                    $epData->primaryKey = NULL;
                    $publish_date = NULL;
                    if (@$_REQUEST['content_publish_date']) {
                        if (@$_REQUEST['publish_date']) {
                            $pdate = explode('/', $_REQUEST['publish_date']);
                            $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                            if (@$_REQUEST['publish_time']) {
                                $publish_date .= ' ' . $_REQUEST['publish_time'];
                            }
                        }
                    }
                    $epData->content_publish_date = $publish_date;
                    $return = $epData->save();
                }
                $getMovieStream = Yii::app()->db->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,f.content_types_id,m.original_file,m.converted_file  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' . $_REQUEST['movie_id'] . ' AND m.studio_id=' . Yii::app()->user->studio_id . ' AND m.id=' . $epData->id)->queryAll();
              
                if(HOST_IP != '127.0.0.1'){
                        if ($epData->is_episode) {
                            $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                            $solrArr['content_id'] = $_REQUEST['movie_id'];
                            $solrArr['stream_id'] = $epData->id;
                            $solrArr['stream_uniq_id'] = $epData->embed_id;
                            $solrArr['is_episode'] = $epData->is_episode;
                            $solrArr['name'] = $epData->episode_title ? $epData->episode_title : "Episode " . $epData->episode_number;
                            $solrArr['permalink'] = $getMovieStream[0]['permalink'];
                            $solrArr['display_name'] = 'content - Episode';
                            $solrArr['studio_id'] = Yii::app()->user->studio_id;
                            $solrArr['content_permalink'] = $getMovieStream[0]['permalink'];
                            $solrArr['genre_val'] = '';
                            $solrArr['product_format'] = '';
                             if($publish_date!=NULL)
                            $solrArr['publish_date'] = $publish_date;   
                            $solrArr['product_format'] = '';
                            $solrObj = new SolrFunctions();
                            $ret = $solrObj->addSolrData($solrArr);
                            }
                        }
				$ret = $this->processContentImage(3,$movie_id,$epData->id,1);
                                $checkfireappletv = Yii::app()->general->CheckApp($this->studio->id);
                                if($checkfireappletv){
                                    $this->processAppTVImage(3,$movie_id,$epData->id,1);
                                }
				$arr['poster'] = @$ret['original'];
                if (@$return && $_REQUEST['editflag']) {
                    $arr['err'] = 0;
                    Yii::app()->user->setFlash('success', 'Wow! Your episode updated successfully.');
                } elseif (@$return) {
                    $arr['err'] = 0;
                    Yii::app()->user->setFlash('success', 'Wow! Your episode added successfully.');
                } else {
                    $arr['err'] = 1;
                    $arr['msg'] = 'Oops! Error in adding episode. Pleas try again!';
                }
            } else {
                $arr['err'] = 1;
                $arr['msg'] = 'Oops! Episode#-' . $data['episode_number'] . ' is already exists.';
            }
        } else if ($movie_id) {
            $arr['err'] = 1;
            $arr['msg'] = 'Oops! You are not authorised to access!';
        } else {
            $arr['err'] = 1;
            $arr['msg'] = 'Oops! You are not authorised to access!';
        }
        echo json_encode($arr);
        exit;
    }

    function actionUploadEpisodesucc() {
        //echo "<pre>";print_r($_REQUEST);exit;
        $dbcon = Yii::app()->db2;
        $getMovieStream = $dbcon->createCommand('SELECT full_movie,full_movie_webm,id,is_episode,episode_number,series_number FROM movie_streams WHERE id=' . $_REQUEST['movie_stream_id'] . ' AND studio_user_id=' . Yii::app()->user->studio_id)->queryAll();
        $s3 = S3Client::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
        ));
        $bucket = 'muviassetsdev';
        $sub_dir = '';
        $filename = isset($_REQUEST['title']) ? preg_replace("![^a-z0-9]+!i", "-", $_REQUEST['title']) . '.mp4' : $_REQUEST['filename'];
        if (HOST_IP == '127.0.0.1') {
            $s3dir = 'movieVideo/' . $getMovieStream['0']['id'] . "/";
        } else {
            $s3dir = 'uploads/movie_stream/full_movie/' . $getMovieStream['0']['id'] . "/";
        }
        if ($getMovieStream[0]['full_movie'] && ($getMovieStream[0]['full_movie'] != $filename)) {
            $result = $s3->deleteObject(array(
                'Bucket' => $bucket,
                'Key' => $s3dir . $getMovieStream[0]['full_movie']
            ));
        }
        if ($getMovieStream[0]['full_movie_webm'] && ($getMovieStream[0]['full_movie'] != $filename)) {
            $result = $s3->deleteObject(array(
                'Bucket' => $bucket,
                'Key' => $s3dir . $getMovieStream[0]['full_movie_webm']
            ));
        }
        $sql_ext = "UPDATE movie_streams SET full_movie='" . pg_escape_string($filename) . "' WHERE id=" . $_REQUEST['movie_stream_id'];
        $ret = $dbcon->createCommand($sql_ext)->execute();
        $this->episodeThumbnail(Yii::app()->common->getStudiosId());
        if ($ret) {
            echo "success";
            exit;
        } else {
            echo "Error";
            exit;
        }
    }

    /**
     * @method public removeEpisode(int $movie_id,int $movie_stream_id) It will remove a compelte episode from the TV series
     * @return bool Description
     * @author GDR<info@muvi.com>
     */
    function actionRemoveEpisode() {
        if (@$_REQUEST['movie_id'] && $_REQUEST['movie_stream_id']) {
            $movie_id = $_REQUEST['movie_id'];
            $getMovieStream = movieStreams::model()->findAll('movie_id=:movie_id AND studio_id=:studio_id AND id=:id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id, ':id' => $_REQUEST['movie_stream_id']));
            if ($movie_id && $getMovieStream) {
                $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
                $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
                $bucket = $bucketInfo['bucket_name'];
                //$bucket = Yii::app()->params->video_bucketname;

                $sub_dir = '';
                $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                $signedFolderPath = $folderPath['signedFolderPath'];
                foreach ($getMovieStream AS $key => $val) {
                    $streams = $val->attributes;
                    $s3dir = $signedFolderPath . 'uploads/movie_stream/full_movie/' . $streams['id'] . "/";
                    if ($streams['full_movie']) {
                        $response = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucket,
                            'Prefix' => $s3dir
                        ));
                        foreach ($response as $object) {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $object['Key']
                            ));
                        }
                    }
                    //Remove Poster
                    $this->removePosters($streams['id'], 'moviestream');
                    $params = array(':stream_id' => $streams['id'], ':studio_id' => Yii::app()->user->studio_id);
                    //Remove From featured content Sections
                    //Remove Featured Content 
                    FeaturedContent::model()->deleteAll('movie_id =:stream_id AND studio_id =:studio_id AND is_episode=1', array(':stream_id' => $streams['id'], ':studio_id' => Yii::app()->user->studio_id));
                    movieStreams::model()->deleteByPk($streams['id']);
                Encoding::model()->deleteAll('movie_stream_id =:movie_stream_id AND studio_id =:studio_id', array(':movie_stream_id' => $streams['id'], ':studio_id' => Yii::app()->user->studio_id));
                    movieStreams::model()->deleteAll('episode_parent_id =:stream_id AND studio_id =:studio_id AND is_episode=1 ',$params);
                    UserFavouriteList::model()->deleteAll('content_id=:stream_id AND studio_id=:studio_id AND content_type="1"',$params);
                    $this->removeTvGuideEvent($streams['id']);
                    }
                Yii::app()->user->setFlash('success', "Episode removed successfully.");
                $this->redirect($_SERVER['HTTP_REFERER']);
            } else {
                Yii::app()->user->setFlash('error', "Oops! Sorry error in deleting content.");
                $this->redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            Yii::app()->user->setFlash('error', "Oops! Sorry error in deleting content.");
            $this->redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * @method public editMovie() Edit Movie Details for sdk users
     * @param int $movie_id Movie id to be edited 
     * @return HTML 
     * @author GDR<info@muvi.in>
     */
    function actionEditMovie() {
        $this->pageTitle = Yii::app()->name . ' | ' . 'Edit Content ';
        $this->breadcrumbs = array("Manage Content","Content Library"=>array('admin/managecontent'),'Edit Content');
        $language_id = $this->language_id;
        $default_lang_id = 20;
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id'] && $_REQUEST['movie_stream_id']) {
            $dbcon = Yii::app()->db;
            //PDO Query
            $pdo_cond = 'm.id=:stream_id AND m.studio_id=:studio_id';
            $pdo_cond_arr = array(':stream_id' => $_REQUEST['movie_stream_id'], ':studio_id' => Yii::app()->user->studio_id);
            $fetchcustomfield = 'f.custom1,f.custom2,f.custom3,f.custom4,f.custom5,f.custom6,f.custom7,f.custom8,f.custom9,f.custom10,m.custom1 as cs1,m.custom2 as cs2,m.custom3 as cs3,m.custom4 as cs4,m.custom5 as cs5,m.custom6 as cs6,f.custom_metadata_form_id,m.custom_metadata_form_id as cmfid,m.is_downloadable';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.content_category_value,f.content_subcategory_value,f.parent_content_type_id as parent_content_type ,m.id as movie_stream_id,f.uniq_id,f.name,f.language,f.censor_rating,f.genre,f.story,f.release_date,f.content_type_id,m.full_movie,m.episode_title,m.episode_date,m.episode_story,m.episode_number,m.series_number,m.is_episode,if(UNIX_TIMESTAMP(m.content_publish_date) = 0,\'\',m.content_publish_date) AS content_publish_date,f.content_types_id,m.is_converted,m.is_download_progress,m.video_management_id,f.language_id,f.parent_id,f.start_time,f.duration,m.episode_language_id,m.episode_parent_id,' . $fetchcustomfield . '')
                    ->from('films f ,movie_streams m ')
                    ->where('f.id = m.movie_id AND  ' . $pdo_cond, $pdo_cond_arr);
            $list = $command->queryAll();
            if ($list) {
                $is_episode = @$list[0]['is_episode'];
                if($is_episode == 1){
                    $cont_id = $_REQUEST['movie_stream_id'];
                }else{
                    $cont_id = $_REQUEST['movie_id'];
                }
                $langcontent = Yii::app()->custom->getTranslatedContent($cont_id,$is_episode,$language_id);
				if(($list[0]['content_types_id']==4) || ($list[0]['content_types_id']==8)){
					$command1 = Yii::app()->db->createCommand()
                    ->select('m.id as livestream_id,feed_method,feed_type,feed_url,is_recording,delete_no,delete_span')
					->from('livestream m ')
					->where('movie_id=:movie_id ',array(':movie_id'=>$list[0]['id']));	
					$lsdata = $command1->queryAll();
					if($lsdata){
						$list[0]['livestream_id'] = $lsdata[0]['livestream_id'];
						$list[0]['feed_method'] = $lsdata[0]['feed_method'];
						$list[0]['feed_type'] = $lsdata[0]['feed_type'];
						$list[0]['feed_url'] = $lsdata[0]['feed_url'];
                        $list[0]['is_recording'] = $lsdata[0]['is_recording'];
                        $list[0]['delete_no'] = $lsdata[0]['delete_no'];
                        $list[0]['delete_span'] = $lsdata[0]['delete_span'];
                }
				}
                $sql = "SELECT * FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
                $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
                $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
                $movie_casts = Yii::app()->custom->getCastCrew($_REQUEST['movie_id'],$studio_id,$language_id);
              
		$celeb_types = Celebrity::model()->celeb_type();
                $mv_id = $list[0]['id'];
                if(array_key_exists($mv_id, @$langcontent['film'])){
                    @$list[0]['name'] = @$langcontent['film'][$mv_id]->name;
                }
                $this->headerinfo = 'Edit Content - ' . $list[0]['name'];
                $all_images = ImageManagement::model()->get_imagedetails_by_studio_id(Yii::app()->user->studio_id);
				//Check for Custom Form 
                $customComp = new CustomForms();
                if ($is_episode == 1) {
                    $custom_content_types_id = (@$list[0]['parent_content_type']==3)?9:4;
                }else{
                    $custom_content_types_id = self::getFormType($list[0]['content_types_id']);
				}
                $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($custom_content_types_id);
                $arg['arg']['editid'] = ($list[0]['cmfid']!=0)?$list[0]['cmfid']:$list[0]['custom_metadata_form_id'];
                if($arg['arg']['editid']){
                    $list[0]['metadata_form_id'] = $arg['arg']['editid'];
                    $customData = $customComp->getCustomMetadata($studio_id, $arg['parent_content_type_id'], $arg['arg']);
                }                
                $formdata = Yii::app()->general->formlist($studio_id);
                $list = self::getSerializeCustomDate($list);
                $IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);
                $checkImageKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
                $this->render('newcontents', array('data' => $list, 'contentList' => @$contentCategories, 'movie_casts' => $movie_casts, 'celeb_types' => $celeb_types, 'allow_new' => 1, 'all_images' => $all_images,'customData'=>$customData,'langcontent'=>$langcontent,'formdata'=>$formdata,'IsDownloadable'=>$IsDownloadable, 'checkImageKey'=>$checkImageKey));
            } else {
                Yii::app()->user->setFlash('error', 'Oops! You don\'t have access to the movie');
                $this->redirect($this->createUrl('admin/managecontent'));
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! invalid input');
            $url = $this->createUrl('admin/managecontent');
            $this->redirect($url);
        }
    }
    function getFormType($form_type) {
        $arr = array(1 => 1, 2 => 2, 3 => 3, 4 => 5, 5 => 7, 6 => 8, 8 => 10);
        return $arr[$form_type];
    }
    /**
     * @method public insertTrailer( $movie_id) Insert trailer into the video table
     * @return video_id 
     * @author GDR<support@muvi.in>
     */
    function insertTrailer($movie_id = '') {
        if ($movie_id) {
            //Video table with taggable_id
            //SELECT trailer_file_name,id FROM videos WHERE id IN(".trim($videoids,',').") AND rank = 1 ORDER BY id DESC LIMIT 1";
            $dbcon = Yii::app()->db2;
            $sql_video = "INSERT INTO videos (movie_id,created_at,updated_at,rank) VALUES(" . $movie_id . ",NOW(),NOW(),1)";
            $dbcon->createCommand($sql_video)->execute();
            $vquery = "SELECT id FROM videos WHERE movie_id=" . $movie_id . " ORDER BY id DESC LIMIT 1";
            $data = $dbcon->createCommand($vquery)->queryAll();
            $video_id = $data[0]['id'];

            //Tagging table with tagable type Video
            $pgCon = Yii::app()->db1;
            $sql_tag = "INSERT INTO taggings (tagger_id,taggable_type,taggable_id,tagger_type) VALUES(" . $movie_id . ",'Video'," . $video_id . ",'Movie')";
            $last_tagging_id = $pgCon->createCommand($sql_tag)->execute();

            //Tagging table with tagger	Trailer
            $sql_trailer = "INSERT INTO taggings (tagger_type,taggable_id) VALUES('trailer'," . $video_id . ")";
            $pgCon->createCommand($sql_trailer)->execute();
            return $video_id;
        } else {
            return;
        }
    }

    /**
     * @method public addEpisode() Adding Episode for a Serise
     * @return HTML 
     * @author GDR<info@muvi.com>
     */
    function actionAddActivity() {
        $movie_id = @$_REQUEST['movie_id'];
        $pgconnection = Yii::app()->db2;
        //echo "<pre>";print_r($_REQUEST);exit;
        $data = $_REQUEST['activity'];
        $data['movie_id'] = $movie_id;
        if (isset($data['title']) && $data['title']) {
            $epdt = @$data['activity_date'] ? @$data['activity_date'] : NULL;//gmdate('Y-m-d')
            if (@$_REQUEST['editflag']) {
                $sql_ext = "UPDATE  movie_streams SET episode_title='" . pg_escape_string($data['title']) . "',episode_date='" . $epdt . "',updated_at=NOW() WHERE movie_id=" . $data['movie_id'] . " AND studio_user_id = " . Yii::app()->common->getStudiosId() . " AND id=" . $data['movie_stream_id'];
            } else {
                $sql_ext = "INSERT INTO movie_streams (movie_id,studio_user_id,episode_title,episode_date,created_at,updated_at,is_episode) VALUES(" . $data['movie_id'] . "," . Yii::app()->common->getStudiosId() . ",'" . pg_escape_string($data['title']) . "','" . $epdt . "',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1)";
            }
            if ($pgconnection->createCommand($sql_ext)->execute()) {
                if (@$_REQUEST['editflag']) {
                    Yii::app()->user->setFlash('success', 'Wow! Your event activity updated successfully');
                } else {
                    Yii::app()->user->setFlash('success', 'Wow! Your event activity added successfully to your studio');
                }
                $url = $this->createUrl('admin/eventDetails', array('movie_id' => $data['movie_id']));
                $this->redirect($url);
                exit;
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Error in adding evnet activity. Pleas try again!');
                $url = $this->createUrl('admin/eventDetails', array('movie_id' => $data['movie_id']));
                $this->redirect($url);
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You are not authorised to access.');
            $url = $this->createUrl('admin/managecontent', array('contentType' => 'event'));
            $this->redirect($url);
            exit;
        }
    }

    /**
     * removeEventActivity
     */
    function actionRemoveEventActivity() {
        $movie_id = $_REQUEST['movie_id'];
        $dbcon = Yii::app()->db2;
        $connection = Yii::app()->db;
        $sql = "SELECT * FROM movie_streams WHERE movie_id=" . $movie_id . " AND studio_user_id=" . Yii::app()->common->getStudiosId() . " AND id=" . $_REQUEST['movie_stream_id'] . " LIMIT 1";
        $getMovieStream = $dbcon->createCommand($sql)->queryAll();
        if ($movie_id && $getMovieStream) {
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            $bucket = 'muviassetsdev';
            $sub_dir = '';
            if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                $s3dir = 'movieVideo/' . $getMovieStream['0']['id'] . "/";
            } else {
                $s3dir = 'uploads/movie_stream/full_movie/' . $getMovieStream['0']['id'] . "/";
            }
            if ($getMovieStream[0]['full_movie']) {
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir . $getMovieStream[0]['full_movie']
                ));
            }
            if ($getMovieStream[0]['full_movie_webm']) {
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir . $getMovieStream[0]['full_movie_webm']
                ));
            }
            if ($dbcon->createCommand('DELETE  FROM movie_streams WHERE movie_id=' . $movie_id . " AND studio_user_id=" . Yii::app()->common->getStudiosId() . " AND id =" . $_REQUEST['movie_stream_id'])->execute()) {
                Yii::app()->user->setFlash('success', "Event activity removed from your studio successfully.");
                $url = $this->createUrl('admin/eventDetails', array('movie_id' => $_REQUEST['movie_id']));
                $this->redirect($url);
            }
        } else {
            $this->redirect('managecontent');
            exit;
        }
    }

    /**
     * @method public cropVideoThumbnail() Used for croping image episode thumbnail image using the jquery Cropper.
     * @return bool ture/false
     * @author GDK<support@muvi.com>
     */
    function actionCropVideoThumbnail() {
        //echo "<pre>";print_r($_FILES);print_r($_REQUEST);exit;
        $fileinfo = $_FILES['Filedata'];
        //echo "<pre>";print_r($fileinfo);print_r($_REQUEST);exit;
        $dbcon = Yii::app()->db;
        $format = new Format();
        if (!$_REQUEST['avatar_src']) {
            $checkImg = $format->checkValidImages($fileinfo, '', 280, 156);
        } else {
            $_FILES = json_decode($_REQUEST['fileinfo'], TRUE);
            $_FILES['Filedata']['error'] = 4;
            $fileinfo = $_FILES['Filedata'];
            $_REQUEST['avatar_src'] = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['avatar_src'];
        }
        if (@$checkImg[0] == 'valid' || $_REQUEST['avatar_src']) {
            if ($_REQUEST['avatar_data']) {
                $id = $_REQUEST['movie_stream_id'];
                $pmodel = new Poster();
                $pData = $pmodel->find("object_id=:object_id AND object_type=:object_type", array(':object_id' => $_REQUEST['movie_stream_id'], ':object_type' => 'moviestream'));
                if ($pData) {
                    $pdata = $pData->attributes;
                    $uid = $pdata['id'];
                    $pmodel->wiki_data = '';
                    $pmodel->poster_file_name = $_FILES['Filedata']['name'];
                    $pmodel->last_updated_by = Yii::app()->user->id;
                    $pmodel->last_updated_date = gmdate('Y-m-d');
                    $pmodel->save();
                } else {
                    $pmodel->object_id = $_REQUEST['movie_stream_id'];
                    $pmodel->object_type = 'moviestream';
                    $pmodel->poster_file_name = $_FILES['Filedata']['name'];
                    $pmodel->created_date = gmdate('Y-m-d');
                    $pmodel->created_by = Yii::app()->user->id;
                    $pmodel->wiki_data = '';
                    $pmodel->save();
                    $uid = $pmodel->id;
                }
                $crop = new cropAvatar($_REQUEST['avatar_src'], $_REQUEST['avatar_data'], $_FILES['Filedata'], $uid);

                if ($_REQUEST['avatar_src']) {
                    $tpath = substr($_REQUEST['avatar_src'], 0, strrpos($_REQUEST['avatar_src'], '/'));
                    $format->recursiveRemoveDirectory($tpath);
                }
                //echo $uid;exit;		
                //$uid = $_REQUEST['movie_id'];
                require_once "Image.class.php";
                require_once "Config.class.php";
                require_once "Uploader.class.php";
                spl_autoload_unregister(array('YiiBase', 'autoload'));
                require_once "amazon_sdk/sdk.class.php";
                spl_autoload_register(array('YiiBase', 'autoload'));

                define("BASEPATH", dirname(__FILE__) . "/..");
                $config = Config::getInstance();
                $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/posters'); //path for images uploaded
                $bucketInfo = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
                $config->setBucketName($bucketInfo['bucket_name']);
                $s3_config = Yii::app()->common->getS3Details(Yii::app()->user->studio_id);
                $config->setS3Key($s3_config['key']);
                $config->setS3Secret($s3_config['secret']);
                $config->setAmount(250);  //maximum paralell uploads
                $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
                $config->setDimensions(array('episode' => "280x156", 'thumb' => "185x256", 'standard' => '280x400'));   //resize to these sizes
                //usage of uploader class - this simple :)
                $uploader = new Uploader($uid);
                $ret = $uploader->uploadepisodeThumb();
                $poster = array($ret);
                $response = array(
                    'state' => 200,
                    'message' => $crop->getMsg(),
                    'result' => $ret['original']
                );

                $dbcon->createCommand('UPDATE movie_streams SET is_poster=1 WHERE id=' . $_REQUEST['movie_stream_id'])->execute();
                echo json_encode($response);
                exit;
            } else {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/posters/tmp';
                if (!is_dir($path . "/" . $_REQUEST['movie_stream_id'])) {
                    mkdir($path . "/" . $_REQUEST['movie_stream_id']);
                }
                $path .= "/" . $_REQUEST['movie_stream_id'];
                copy($_FILES['Filedata']['tmp_name'], $path . "/" . $_FILES['Filedata']['name']);
                $response = array(
                    'state' => 200,
                    'message' => $checkImg[0],
                    'result' => Yii::app()->baseUrl . "/images/public/system/posters/tmp/" . $_REQUEST['movie_stream_id'] . "/" . $_FILES['Filedata']['name'],
                    'fileinfo' => json_encode($_FILES)
                );
                echo json_encode($response);
                exit;
            }
        } else {
            $response = array(
                'state' => 201,
                'message' => $checkImg[0]
            );
            echo json_encode($response);
            exit;
        }
        if (is_array($ret)) {
            //$updsucc = $dbcon->createCommand("UPDATE films set poster_file_name='".$_FILES['Filedata']['name']."',poster_content_type='".$_FILES['Filedata']['type']."',poster_file_size=".$_FILES['Filedata']['size']." WHERE id=".$id)->execute();
            //if($updsucc){
            Yii::app()->user->setFlash('success', 'Wow! Poster of the episode updated successfuly.');
            header("Location:" . $_SERVER['HTTP_REFERER']);
            exit;
            //}
        }
        Yii::app()->user->setFlash('error', 'Oops! Error while updating the poster.');
        header("Location:" . $_SERVER['HTTP_REFERER']);
        exit;
    }

    //Added for getting ppv details
     function actionSavepaytype() {
        $payment_type = isset($_POST['movie_payment_type']) ? trim($_POST['movie_payment_type']) : '';
        $movie_id = isset($_POST['ppv_id']) ? $_POST['ppv_id'] : '';
        $conn = Yii::app()->db2;

        $response = 'error';
        $response_message = 'Error in updating.';

        if ($movie_id > 0) {
            if ($payment_type == 'ppv') {
                $price_for_paid = isset($_POST['price_for_paid']) ? $_POST['price_for_paid'] : 0;
                $price_for_unpaid = isset($_POST['price_for_unpaid']) ? $_POST['price_for_unpaid'] : 0;
                $available_hours = isset($_POST['available_hours']) ? $_POST['available_hours'] : 0;
            } else {
                $price_for_paid = 0;
                $price_for_unpaid = 0;
                $available_hours = 0;
            }


            $qry = "UPDATE films SET movie_payment_type = '" . $payment_type . "', price_for_paid = '" . $price_for_paid . "'";
            $qry.= ", price_for_unpaid = '" . $price_for_unpaid . "', available_for_hours = '" . $available_hours . "'";
            $qry.= " WHERE id = '" . $movie_id . "'";
            $conn->createCommand($qry)->execute();

            $response = 'success';
            $response_message = 'Pricing type updated.';
        }

        $ret = array('status' => $response, 'message' => $response_message);
        echo json_encode($ret);
    }

    ///////// Starts Functions for email notification///////////
    public function actionemail_triggers() {
        $this->breadcrumbs = array('Marketing','Email Triggers');
        $this->headerinfo = "Email Triggers";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Email Triggers';
        $this->layout = 'admin';
        $studio_id = $this->studio->id;
        $db_user = Yii::app()->db;
        $studio_sql = "SELECT domain,name FROM studios WHERE id = '" . Yii::app()->common->getStudiosId() . "' LIMIT 1";
        $user_data = $db_user->createCommand($studio_sql)->queryAll();
        $announcement = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'announcement_mail_per_week');
        if(!empty($announcement)){
            $announcement_mail_per_week = $announcement['config_value'];
        }else{
            $announcement = StudioConfig::model()->getconfigvalueForStudio(0, 'announcement_mail_per_week');
            $announcement_mail_per_week = $announcement['config_value'];
        }
        $this->render('email_triggers', array('studio_name' => $user_data[0]['name']));
    }
    ###### For counting announcement sent this week #####
    public function actionCheckForAnnouncement(){
        $weekrange = $this->getStartAndEndDate();
        $studio_id = $this->studio->id;
        $result = AnnouncementLog::model()->CountAnnouncementSent($studio_id,$weekrange);
        return $result;
    }
    ####### Get week start and last date
    public function getStartAndEndDate(){
        $date  = strtotime(date('Y-m-d 00:00:00'));
        $dotw  = date('w', $date);
        $start = $date - ($dotw * 24*60*60); 
        $end   = $start + (6 * 24*60*60); 
        $start_date   = date('Y-m-d',$start);
        $return[0]    = date('Y-m-d H:i:s', strtotime($start_date . ' +1 day'));
        $end_date     = date('Y-m-d',$end);
        $return[1]    = date('Y-m-d 23:59:59', strtotime($end_date . ' +1 day'));
        return $return; 
    }
    public function actionmovie_block() {
        $dbcon = Yii::app()->db;
        $studio_sql = "SELECT domain FROM studios WHERE id = '" . Yii::app()->common->getStudiosId() . "' LIMIT 1";
        $user_data = $dbcon->createCommand($studio_sql)->queryAll();
        $movie_id = $_REQUEST['movie_id'];
        $movie_stream_id = $_REQUEST['movie_stream_id'];
        $cdata = $this->getMovieDetails($movie_id);

        $data = json_decode($cdata, true);
        if ($movie_stream_id != "") {
            $sql = "select * from movie_streams where full_movie IS NOT NULL and is_episode = 1 and id = " . $movie_stream_id . " LIMIT 1";

            $epi_data = $dbcon->createCommand($sql)->queryAll();
            if ($epi_data) {
                $data[0]['name'] .= " - " . $epi_data[0]['episode_title'];
                $data[0]['story'] = $epi_data[0]['episode_story'];
                $data[0]['movie_stream_id'] = $epi_data[0]['id'];
                $data[0]['is_episode'] = $epi_data[0]['is_episode'];
            }
        }
        $this->renderPartial('movie_block', array('data' => $data[0], 'user_data' => $user_data[0]));
    }

    public function actionmix_autocomplete() {
        $this->layout = false;
        //$dbcon = Yii::app()->db2;
        $sq = $_REQUEST['term'];
        $arr = array();
        $list = Film::model()->with('movie_streams')->findAll(array("condition" => "t.studio_id = " . Yii::app()->common->getStudiosId() . " AND LOWER(t.name) LIKE '%" . strtolower($sq) . "%'"));
        //$sql = "SELECT F.name, F.id,M.id  AS movie_stream_id FROM films F ,movie_streams M WHERE  F.id = M.movie_id AND M.studio_user_id=".Yii::app()->common->getStudiosId()."  AND ((LOWER(F.content_type) = 'movie'  AND M.full_movie IS NOT NULL) OR (LOWER(F.content_type) != 'movie' AND full_movie IS NULL)) AND LOWER(F.name) LIKE '%".strtolower($sq)."%'  ORDER BY char_length(F.name) ASC LIMIT 5";
        //echo $sql;
        //$list = $dbcon->createCommand($sql)->queryAll();
        //echo "<pre>";
        //print_r($list);
        if ($list) {
            foreach ($list AS $key => $val) {
                $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'movie_stream_id' => $val['movie_streams'][0]['id']);
            }
        }
        /* $epi_sql = "Select F.name,F.id, M.id  AS movie_stream_id, M.episode_title FROM movie_streams M,films F WHERE M.movie_id = F.id AND M.studio_user_id=".Yii::app()->common->getStudiosId()." AND LOWER(F.content_type) != 'movie'  AND M.full_movie IS NOT NULL  AND (LOWER(M.episode_title) LIKE '%".strtolower($sq)."%' OR (LOWER(F.name) LIKE '%".strtolower($sq)."%' AND M.episode_title IS NULL))  ORDER BY char_length(M.episode_title) ASC LIMIT 5";
          //echo $epi_sql;
          $episodes = $dbcon->createCommand($epi_sql)->queryAll();
          if($episodes){
          foreach($episodes AS $key=>$val){
          if($val['episode_title'] == ""){
          $mov_name = $val['name'];
          }else{
          $mov_name = $val['name'].' - '. $val['episode_title'];
          }
          $arr['movies'][]=array('movie_name'=>$mov_name ,'movie_id'=>$val['id'],'movie_stream_id'=>$val['movie_stream_id']);
          }
          } */
        echo json_encode($arr);
        exit;
    }

    public function actionsend_emailnoti() {
        $default_lang = 20;
        //Added by RKS for welcome email template
        if (isset($_REQUEST['mail_type']) && $_REQUEST['mail_type'] != '') {
            //For Send Marketing Emails or Announcements 
            if (isset($_REQUEST['mail_type']) && $_REQUEST['mail_type'] == 'announcement') {
                $studio_id = Yii::app()->common->getStudiosId();
                $user_id = Yii::app()->user->id;
                $user = User::model()->findByPk($user_id);
                $user_email = $user->email;
                $AllactiveuserEmail = Yii::app()->db->createCommand("SELECT id,email,display_name from sdk_users WHERE status='1' AND is_deleted='0' AND announcement_subscribe='1' AND is_developer='0' AND studio_id= ".$studio_id)->queryAll();
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => $studio_id, 'type' => $_REQUEST['mail_type']));
                if (count($data) == 0) {
                    $temp = New NotificationTemplates;
                    $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $_REQUEST['mail_type']));
                }

                $temps = $data[0];
                $StudioName = $this->studio->name;
                ##### Announcement Log
                $log = new AnnouncementLog;
                $log->subject = $_REQUEST['subject'];
                $log->message = $_REQUEST["content"];
                $log->studio_id = $studio_id;
                $log->notification_send_time = date('Y-m-d H:i:s');
                $log->save();
                
                foreach ($AllactiveuserEmail as $user) {
                    $FirstName = $user['display_name'];
                    $to = $user['email'];
                    $subject = $_REQUEST['subject'];
                    $cc = array();
                    $bcc = array();
                    $from = $user_email;
                    $content = (string) $_REQUEST["content"];
                    $breaks = array("\r\n");
                    $content = str_ireplace($breaks, "<br />", $content);                    
                    $breaks = '</p><br /><p>';
                    $content = str_ireplace($breaks, "</p><p>", $content);
                    $content = htmlentities($content);
                    eval("\$content = \"$content\";");
                    $content = htmlspecialchars_decode($content);
                    
                    $site_url = "http://" . $this->studio->domain;
                    $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);
                    $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
                    $unsub_link = $site_url."/user/unsubscribefromnotificationemail/user/".base64_encode($user['id']);
                    $params = array(
                    'content'=> $content,
                    'logo'=> $logo,
                    'unsub_link'=> $unsub_link,
                    'StudioName'=>$StudioName   
                    );
                    $template_name = 'announcement';
                    Yii::app()->theme = 'bootstrap';
                    $thtml = Yii::app()->controller->renderPartial('//email/announcement', array('params' => $params), true);
                    $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, $cc, $bcc, '', $StudioName);
                }
                Yii::app()->user->setFlash('success', "Email send successfully.");
                $url = $this->createUrl('admin/email_triggers');
                $this->redirect($url);
            } 
            elseif (isset($_REQUEST['mail_type']) && $_REQUEST['mail_type'] == 'new_content') {
                require('MadMimi.class.php');
                $item_detail = array();
                $this->layout = false;
                $user_model = new User();
                $users = $user_model->get_users($_REQUEST['send_to']);
                $items = explode(',', $_REQUEST['content_id']);
                $studio = $this->studio;

                $admin = $user_model->findAllByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'is_sdk' => 1, 'role_id' => 1));
                if (count($admin) > 0) {
                    $from_mail = $admin[0]->email;
                } else {
                    $from_mail = 'info@' . $studio->domain;
                }

                $dbcon = Yii::app()->db;
                $studio_sql = "SELECT domain FROM studios WHERE id = '" . Yii::app()->common->getStudiosId() . "' LIMIT 1";
                $user_data = $dbcon->createCommand($studio_sql)->queryAll();

                array_pop($items);
                $items = array_unique($items);
                foreach ($items AS $key => $val) {
                    $ms_data = movieStreams::model()->findByPk($val);
                    //$sql = "select * from movie_streams where id=".$val." LIMIT 1";
                    //$ms_data = $dbcon->createCommand($sql)->queryAll();
                    $cdata = $this->getMovieDetails($ms_data['movie_id']);
                    $data = json_decode($cdata, true);
                    $data[0]['movie_stream_id'] = $ms_data['id'];
                    $data[0]['is_episode'] = $ms_data['is_episode'];
                    if ($ms_data['episode_title'] != '') {
                        $data[0]['name'] .= ' - ' . $ms_data['episode_title'];
                        $data[0]['story'] = $ms_data['episode_story'];
                    }
                    array_push($item_detail, $data[0]);
                }

                ini_set('max_execution_time', 500);
                $test_mails = array('ratikanta@muvi.com');
                $live_mails = array('info@muvi.com');
                //$test_mails = array('development@muvi.com','viraj@muvi.com');
                foreach ($users AS $key => $val) {
                    //echo $val['display_name']."----".$val['email'];
                    $temp_data = $this->renderPartial('notification_template', array('item_detail' => $item_detail, 'display_name' => $val['display_name'], 'mail_content' => $_REQUEST['content'], 'studio' => $studio, 'user_data' => $user_data), true);
                    $mimi = new MadMimi('info@muvi.com', '5c43f29037c4c5c696b7306b1832dcb2');
                    $options = array(
                        'promotion_name' => 'SDK Email Marketing',
                        'recipients' => $val['display_name'] . ' <' . $val['email'] . '>',
                        'subject' => $_REQUEST['subject'],
                        'from' => $studio->name . " <$from_mail>"
                    );
                    $html_body = "<html><head><title>" . $_REQUEST['subject'] . "</title></head><body>" . $temp_data . "<p style='display: none;'>[[tracking_beacon]]</p></body></html>";
                    //$html_body = "<html><head><title>".$_REQUEST['subject']."</title></head><body>".$temp_data."[[tracking_beacon]]</body></html>";
                    $mimi->SendHTML($options, $html_body);
                }

                Yii::app()->user->setFlash('success', "Email sent successfully.");
                $url = $this->createUrl('admin/email_triggers');
                $this->redirect($url);
            } 
            else {
                $studio_id = $this->studio->id;
                $send_to = 'all';
                $send_from = $_REQUEST['from_email'];
                $subject = $_REQUEST['subject'];
                $has_attachment = $_REQUEST['has_attachment'];
                $content = $_REQUEST['content'];
                $breaks = array("\r\n");
                $content = str_ireplace($breaks, "<br />", $content);
                $breaks = '</p><br /><p>';
                $content = str_ireplace($breaks, "</p><p>", $content);
                $breaks = '<br />';
                $content = str_ireplace($breaks, "</p>".$breaks."<p>", $content);
                $mail_type = $_REQUEST['mail_type'];

                //checking if same template exists
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => $studio_id, 'type' => $mail_type, 'parent_id' => 0));
                //issue id 6122                
                //$dataforall = $temp->findByAttributes(array('studio_id' => 0, 'type' => $mail_type));
                if (count($data) == 0) {
                    $temps = New NotificationTemplates;
                    $temps->studio_id = $studio_id;
                    $temps->send_from = $send_from;
                    $temps->type = $mail_type;
                    $temps->subject = $subject;
                    $temps->content = $content;
                    $temps->has_attachment = $has_attachment;
                    $temps->status = 1;
                    $temps->save();
                    $insert_id = $temps->id;
                    if($this->language_id !=$default_lang){
                        $tempforlang = New NotificationTemplates;
                        $tempforlang->studio_id = $studio_id;
                        $tempforlang->send_from = $send_from;
                        $tempforlang->type = $mail_type;
                        $tempforlang->subject = $subject;
                        $tempforlang->content = $content;
                        $tempforlang->has_attachment = $has_attachment;
                        $tempforlang->parent_id = $insert_id;
                        $tempforlang->language_id = $this->language_id;
                        $tempforlang->status = 1;
                        $tempforlang->save();
                    }
                }else {
                    $temps = $data[0];
                    if($data[0]->language_id != $this->language_id){
                        $check_for_lang = $temp->findByAttributes(array('studio_id' => $studio_id, 'type' => $mail_type,'language_id'=>$this->language_id));
                        if(empty($check_for_lang)){
                          $temps = New NotificationTemplates;  
                        }else{
                            $temps = $check_for_lang;
                }
                    }
                
                $temps->studio_id = $studio_id;
                $temps->send_from = $send_from;
                $temps->type = $mail_type;
                $temps->subject = $subject;
                $temps->content = $content;
                $temps->has_attachment = $has_attachment;
                $temps->status = 1;
                    if($data[0]->language_id != $this->language_id){
                        $temps->parent_id = $data[0]->id;
                        $temps->language_id = $this->language_id;
                    }
                $temps->save();
                }
                Yii::app()->user->setFlash('success', "Email template saved successfully.");
                $url = $this->createUrl('admin/email_triggers');
                $this->redirect($url);
            }
        } else {
            die("Error in saving the data");
        }
    }

    public function actionNotificationtemplate() {
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $ret = array();
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);

        if ($type != '') {
            $temp = New NotificationTemplates;
            //$data = $temp->findAllByAttributes(array('studio_id' => $studio_id, 'type' => $type));
            $sql = "SELECT * from notification_templates WHERE studio_id =".$studio_id." AND type='".$type."' AND (language_id = ".$this->language_id." AND parent_id=0 OR id NOT IN(SELECT parent_id FROM notification_templates WHERE studio_id =".$studio_id." AND type='".$type."' AND language_id = ".$this->language_id."))";
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            if (count($data) > 0) {
                
            } else {
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $type));
            }
            $temps = $data[0];
            if (trim($temps['send_from'])) {
                $send_from = $temps['send_from'];
            } else {
                $model = new User;
                $admin = $model->findAllByAttributes(array('studio_id' => $studio_id, 'is_sdk' => 1, 'role_id' => 1));
                if (count($admin) > 0) {
                    $studio_mail = $admin[0]->email;
                } else {
                    $studio_mail = 'info@' . $studio->domain;
                }
                $send_from = $studio_mail;
            }

            $subject = $temps['subject'];

            if (strpos($subject, '$') !== false) {
                $x = preg_match_all('/(\$[a-z]+)/i', $subject, $output);
            }
            $subject_vars = (isset($output['0']) && !empty($output['0'])) ? array_unique($output['0']) : array();

            $content = $temps['content'];
            $content = html_entity_decode(htmlspecialchars($content));
            $breaks = array("<br />", "<br>", "<br/>");
            $content = str_ireplace($breaks, "\r\n", $content);
            if (strpos($content, '$') !== false) {
                $x = preg_match_all('/(\$[a-z 0-9]+)/i', $content, $output);
            }
            $content_vars = (isset($output['0']) && !empty($output['0'])) ? array_unique($output['0']) : array();

            $variables = array_merge($subject_vars, $content_vars);


            //$variables['Logo Image'] = '$Logo';
            $variables['$Logo'] = 'Logo Image';
            $com_arr = array('$FirstName' => 'First Name', '$StudioName' => 'Studio Name', '$PlanName' => 'Plan Name', '$StudioUrl' => 'Studio Url', '$StudioEmail' => 'Studio Email', '$VideoName' => 'Video Name', '$VideoUrl' => 'Video Url', '$Logo' => 'Logo Image', '$VideoDetails' => 'Video Details', '$EmailAddress' => 'Email Address', '$ActivationUrl' => 'Activation Url', '$Last4DigitsOfCard' => 'Last 4 digits of card', '$FourDaysAfterSecondNotice' => '4days after second notice', '$TwoDaysAfterSecondNotice' => '2days after second notice', '$AdvanceAmount' => 'Pre-Order amount', '$Url' => 'Url', '$UserName' => 'User Name', '$Password' => 'Password','$TransactionAmount'=>'TransactionAmount','$TxnFailureReason'=>'TxnFailureReason','$SupportEmail'=>'SupportEmail','$MonetizationName'=>'MonetizationName',);
            $result = array_unique(array_intersect_key($com_arr, array_flip($variables)), SORT_REGULAR);

            //$result = array_unique(array_merge(array_flip($com_arr), $variables), SORT_REGULAR);

            $has_attachment = $temps['has_attachment'];
        } else {
            $com_arr = array('$FirstName' => 'First Name', '$StudioName' => 'Studio Name', '$StudioUrl' => 'Studio Url', '$StudioEmail' => 'Studio Email', '$EmailAddress' => 'Email Address', '$Logo' => 'Logo Image');
            //$result = array_flip($com_arr);
            $result = $com_arr;
        }
        $ret = array('send_from' => $send_from, 'subject' => $subject, 'content' => $content, 'has_attachment' => $has_attachment, 'variables' => $variables, 'result' => $result);
        echo json_encode($ret);
    }
    
    public function actionEmailNotifications() {
        $this->breadcrumbs = array('Settings','Email Notifications');
        $this->headerinfo = "Email Notifications";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Email Notifications';
        $this->layout = 'admin';
        $studio_id = $this->studio->id;

        $email_types = NotificationSetting::model()->findAllByAttributes(array('studio_id' => $studio_id));
        if (empty($email_types)) {
            $email_types = NotificationSetting::model()->findAllByAttributes(array('studio_id' => 0));
        }
        //5975: Email addresses for different notifications
        $linked_emails = EmailNotificationLinkedEmail::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $end_u = json_decode($linked_emails[0]['end_user_action']);
        foreach($end_u as $key => $end_val){
            $enduserdata[] = $end_val;            
        }
        $cont_u = json_decode($linked_emails[0]['contact_us']);
        foreach($cont_u as $key => $cont_val){
            $contdata[] = $cont_val;            
        }
        $supp_u = json_decode($linked_emails[0]['support_tickets']);
        $supportdata = array();
        foreach($supp_u as $key => $supp_val){
            /*$supportdata[$key]['content_id'] = $supp_val;
            $supportdata[$key]['name'] = $supp_val; */
            $supportdata[] = $supp_val;
        }
        //END-5975: Email addresses for different notifications
        $email_settings = array();
        foreach ($email_types as $key => $value) {
            $email_settings[$value->type] = $value->status;
        }
        $notification_from_email = $linked_emails[0]->notification_from_email;
        $this->render('email_notifications', array('email_settings' => $email_settings,'data' => $data,'enduserdata' => $enduserdata,'contdata' => $contdata,'supportdata' => $supportdata,'notification_from_email'=>$notification_from_email));
    }
    
    public function actionSaveEmailNotifications() {
        if (isset($_POST['email']) && !empty($_POST['email'])) {
            $studio_id = $this->studio->id;
            
            foreach ($_POST['email'] as $key => $value) {
                $email_types = NotificationSetting::model()->findByAttributes(array('studio_id' => $studio_id, 'type' => $key));
                
                if (empty($email_types)) {
                    $email_types = new NotificationSetting;
                }
                $email_types->studio_id = $studio_id;
                $email_types->type = $key;
                $email_types->status = $value;
                $email_types->save();
            }

            Yii::app()->user->setFlash('success', "Email notifications has been saved successfully.");
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in saving of email notifications.');
        }
        
        $url = $this->createUrl('admin/emailNotifications');
        $this->redirect($url);
        exit;
    }
    //5975: Email addresses for different notifications
    //ajit@muvi.com
    public function actionSaveEmailNotificationsOtherEmails() {
        if (isset($_POST['othemail']) && !empty($_POST['othemail'])) {
            $studio_id = $this->studio->id;

            $check_email_types = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id));
            if($check_email_types){
                $email_types = EmailNotificationLinkedEmail::model()->findByPk($check_email_types['id']);
                $email_types->modified = gmdate('Y-m-d H:i:s');
            }else{
                $email_types = new EmailNotificationLinkedEmail;
                $email_types->created = gmdate('Y-m-d H:i:s');
            }
            //validate the email id of contact list
            $addcontact = array();
            $endcontact = array();
            $addsupport = array();
            if($_REQUEST['end_user_action'])
            {
                foreach($_REQUEST['end_user_action'] as $endcont){
                    if (!filter_var($endcont, FILTER_VALIDATE_EMAIL) === false && !in_array($endcont,$endcontact)) {
                        $endcontact[]=$endcont;                        
                    }                    
                }
            }
            if($_REQUEST['contact_us']) {
                foreach($_REQUEST['contact_us'] as $cont){
                    if (!filter_var($cont, FILTER_VALIDATE_EMAIL) === false && !in_array($cont,$addcontact)) {
                        $addcontact[]=$cont;                        
                    }                    
                }
            }
            if(!empty($_REQUEST['support_tickets'])) {
                foreach($_REQUEST['support_tickets'] as $supportcont){
                    if (!filter_var($supportcont, FILTER_VALIDATE_EMAIL) === false && !in_array($supportcont,$addsupport)) {
                        $addsupport[]=$supportcont;                        
                    }                    
                }
            }
            $email_types->studio_id = $studio_id;
            $email_types->end_user_action = json_encode($endcontact);
            $email_types->contact_us = json_encode($addcontact);
            $email_types->support_tickets = json_encode($addsupport);
            $email_types->notification_from_email = $_POST['notification_from_email'];
            $email_types->save();
           

            Yii::app()->user->setFlash('success', "Email notifications has been saved successfully.");
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in saving of email notifications.');
            }

        $url = $this->createUrl('admin/emailNotifications');
        $this->redirect($url);
        exit;
    }
    public function actiongetAdminUsers() {
            $studio_id = $this->studio->id;
        $res = array();
        $data = array();
        $data = User::model()->findAll(array('select'=>'id,email','condition' => 'studio_id=:studio_id and is_active=:is_active and role_id!=:role_id','params' => array(':studio_id' =>$studio_id,':is_active'=>1,':role_id'=>1)));
        $cnt =0;

        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $res[$cnt]['content_id'] = $value['email'];
                $res[$cnt]['name'] = $value['email'];
                $cnt++;
            }
        }
        echo json_encode($res);
        exit;
    }
    //END-5975: Email addresses for different notifications
    /////////Ends email functionality///////////
    /**
     * @method public S3mulatipartupload() Uploading file of more then 5GB using S3 multipart upload
     * @author GDR<support@muvi.com>
     * @return Json A json string of response
     */
    function actionS3mulatipartupload() {
        $actions = array('abortmultipartupload', 'completemultipartupload', 'signuploadpart', 'createmultipartupload');
        $command = strtolower($_REQUEST['command']);
        $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
        $dest_bucket = $bucketInfo['bucket_name'];
        $region = $bucketInfo['region_code'];
        $s3url = $bucketInfo['s3url'];
        //$dest_bucket = Yii::app()->params->video_bucketname;
        if (in_array($command, $actions)) {
            $model = ($_REQUEST['otherInfo']['type']=='physical' || $_REQUEST['uploadtype'] == 'physical')?'PGMovieTrailer':'movieTrailer';
            $upload_start_time = 0;
            if ($command == 'createmultipartupload') {
                $upload_start_time = date('Y-m-d H:i:s');
            } else {
                $upload_start_time = 0;
            }
            if ($command == 'abortmultipartupload') {
                $cancel_end_time = date('Y-m-d H:i:s');
                if (isset($_REQUEST['uploadtype']) && ($_REQUEST['uploadtype'] == 'trailer' || $_REQUEST['uploadtype'] == 'physical')) {
                    $trailerData = $model::model()->find('movie_id=:movie_id', array(':movie_id' => $_REQUEST['movie_id']));
                    $trailerData->upload_cancel_time = $cancel_end_time;
                    $trailerData->save();
                } else {
                    $moveiData = movieStreams::model()->findByPk($_REQUEST['movie_stream_id']);
                    $moveiData->upload_cancel_time = $cancel_end_time;
                    $moveiData->save();
                }
            }
            if (isset($_REQUEST['otherInfo']['uploadType']) && ($_REQUEST['otherInfo']['uploadType'] == 'trailer' || $_REQUEST['otherInfo']['uploadType'] == 'physical')) {
                $region = '';
                $trailer_id = $this->addTrailer($_REQUEST['otherInfo']['movie_id'],$model);
                if ($trailer_id) {
                    if ($upload_start_time) {
                        $is_success = $this->updateTrailerStartTime($trailer_id, $upload_start_time,$model);
                    }
                    $_REQUEST['otherInfo']['trailer_id'] = $trailer_id;
                } else {
                    $arr = array('error' => 'Error in uploading trailer');
                    header('Content-Type: application/json');
                    die(json_encode($arr));
                }
            } else {
                if ($upload_start_time) {
                    $is_success = $this->updateMovieStartTime($_REQUEST['otherInfo']['movie_stream_id'], $upload_start_time);
                }
            }

            if (isset($_REQUEST['uploadtype']) && ($_REQUEST['uploadtype'] == 'trailer' || $_REQUEST['uploadtype'] == 'physical')) {
                $region = '';
            }
            $s3multi = new S3multipart();
            $arr = $s3multi->$command($dest_bucket, $region, $s3url);
            $content = Film::model()->findByPk($_REQUEST['movie_id']);
            if (@$arr['success'] && $command == 'completemultipartupload') {
                $data = $_REQUEST;
                if (isset($_REQUEST['uploadtype']) && ($_REQUEST['uploadtype'] == 'trailer' || $_REQUEST['uploadtype'] == 'physical')) {
                    $this->updateTrailerInfo($_REQUEST['otherInfo']['type'], $_REQUEST['uploadtype']);
                } else {
                    $this->actionUploadsucc();
                }
            }
        } else {
            $arr = array('error' => 'Requested Command is Invalid');
        }
        header('Content-Type: application/json');
        die(json_encode($arr));
    }

    /**
     * @method public addContentToMuvi(String $movie_name) Adding a content to Muvi Database
     * @return int $insert_id
     * @author GDR<support@muvi.com>
     */
    function addContentToMuvi($name = '', $content_type = 'movie') {
        if ($name) {
            $plink = $this->generate_permalink($name);
            //$sql = "INSERT INTO films (name,permalink,contnet_type,is_verified) VALUES('".$name."','".$plink."','".$content_type."','f')";
            //$dbcon = Yii::app()->db1;
            //$dbcon->createCommand($sql)->execute();
            $films = new films;
            $films->name = $name;
            $films->permalink = $plink;
            $films->content_type = $content_type;
            $films->is_verified = false;
            $films->created_at = gmdate('Y-m-d H:i:s');
            $films->save();
            return $films->id;
        } else {
            return false;
        }
    }

    //Settings updating studio Admin profile
    public function actionAccount() {
        $this->breadcrumbs = array('Settings', 'Account Info');
        $this->headerinfo = "Account Info";
        $this->pageTitle = "Muvi | Account Info";
        $this->layout = 'admin';

        $studio_id = Yii::app()->common->getStudiosId();
        (array) $user = User::model()->findByPk(Yii::app()->user->id);
        $studio = Studio::model()->findByPk($studio_id);

        $this->render('account', array('userdata' => $user->attributes, 'studio' => $studio));
    }

    public function actionSaveaccount() {
        $studio_id = Yii::app()->common->getStudiosId();
        $existing_email=Yii::app()->user->email;
        
        $studio_name = isset($_REQUEST['studio_name']) ? $_REQUEST['studio_name'] : '';
        $fname = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '';
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $address = isset($_REQUEST['address']) ? $_REQUEST['address'] : '';
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : '';
        
        //Saving the studio name
        $studio = $this->studio;
        $studio->name = htmlspecialchars($studio_name);
        $studio->save();

        $model = new User;
        $model = User::model()->findByAttributes(array('id' => Yii::app()->user->id));
        $email_is_verified=1;
        if ($email != '') {
            $users = User::model()->findAll(array("condition" => "email = '" . $email . "' AND studio_id != '" . $studio_id . "'"));
            if (count($users) > 0) {
                
                //send verification link to user if the email exists and not verified
                $list_of_verified_email=array();
                $client = SesClient::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                                'region' => 'us-east-1',
                    ));
                $result_email = $client->listVerifiedEmailAddresses();

                $list_of_verified_email = $result_email['VerifiedEmailAddresses'];
                                        
                 //check whether the user email is verified or not ?
                    
                 if(!in_array($email,$list_of_verified_email)) {
                   //send verification link  
                 
                 $email_is_verified=0;
                 }
                Yii::app()->session['is_email_verified']=$email_is_verified;
		 Yii::app()->user->setState('is_email_verified',$is_email_verified);
                Yii::app()->user->setFlash('error', 'Oops! Email already exists.');
                $url = $this->createUrl('admin/account');
                $this->redirect($url);
               //$this->redirect(array('admin/account', 'email_is_verified'=>$email_is_verified));
                exit;
            } else {
                $model->email = $email;
                Yii::app()->user->setState('email',$email);
                //send verification link to user to verify new email address by SD:<suraja@muvi.com>
                $client = SesClient::factory(array(
                                'key' => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                                'region' => 'us-east-1',
                    ));
                    
                 
                 $result_email = $client->listVerifiedEmailAddresses();

                $list_of_verified_email = $result_email['VerifiedEmailAddresses'];
                                        
                 //check whether the user email is verified or not ?
                    
                 if(!in_array($email,$list_of_verified_email)) {
                   //send verification link  
                 
                 $email_is_verified=0;
                    }
                    else
                    {
                    $email_is_verified=1;    
                    }
           Yii::app()->session['is_email_verified']=$email_is_verified;  
	   Yii::app()->user->setState('is_email_verified',$is_email_verified);
        }
        $model->first_name = $fname;
        $model->phone_no = $phone;
        $model->address_1 = htmlspecialchars($address);
        }
        if ($model->save()) {
            //updating Ticket master table for the studio Id and corresponding email
            $tickettingdb=$this->getAnotherDbconnection();
            $update_master_qry="update ticket_master set studio_email='".$email."',studio_name='".$studio_name."',user_name='".$fname."' where studio_email='".$existing_email."' and studio_id=".$studio_id;
            $tickettingdb->createCommand($update_master_qry)->execute(); 
            $tickettingdb->active=false;
            Yii::app()->user->setState('first_name', $fname);
            Yii::app()->user->setState('lastname', $lname);
            Yii::app()->user->setFlash('success', 'Your Account information has been updated successfully!');
            $url = $this->createUrl('admin/account');
            //$this->redirect(array('admin/account', 'email_is_verified'=>$email_is_verified));  
            $this->redirect($url);
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in updating your account information.');
            $url = $this->createUrl('admin/account');
            //$this->redirect(array('admin/account', 'email_is_verified'=>$email_is_verified));
            $this->redirect($url);
            exit;
        }
    }

    public function actionSavepassword() {
        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
        $new_password = isset($_REQUEST['new_password']) ? $_REQUEST['new_password'] : '';
        $conf_password = isset($_REQUEST['conf_password']) ? $_REQUEST['conf_password'] : '';
        $studio_id = Yii::app()->common->getStudiosId();
        $enc = new bCrypt();

        $model = new User;
        $model = User::model()->findByAttributes(array('id' => Yii::app()->user->id, 'studio_id' => $studio_id));

        if ($enc->verify($password, $model->encrypted_password) && $new_password != '' && strcmp($new_password, $conf_password) == 0) {
            $email = $model->email;
            $pass = $model->encrypt($new_password);
            $model->encrypted_password = $pass;
            //$model->save();
            //reset password of all records of this email id
            //issue id -5474 ajit@muvi.com
            $sql = "update user set encrypted_password='" . $pass . "' where email='" . $email . "'";
            Yii::app()->db->createCommand($sql)->execute();

            $usr = new SdkUser();
            $usr = $usr->findByAttributes(array('studio_id' => $studio_id, 'email' => $email));
            if ($usr) {
                $usr->encrypted_password = $pass;
                $usr->save();
            }

            Yii::app()->user->setFlash('success', 'Your password has been updated successfully!');
            $url = $this->createUrl('admin/account');
            $this->redirect($url);
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in updating your password.');
            $url = $this->createUrl('admin/account');
            $this->redirect($url);
            exit;
        }
    }

    /**
     * @method public contentTypeList() List the available content types for the logged studio
     * @author GDR<support@muvi.com>
     * @return HTML 
     */
    /*function actionContentTypeList() {
        $this->breadcrumbs = array('Content Types');
        $this->pageTitle = Yii::app()->name . ' | ' . 'Content Types';
        $con = Yii::app()->db;
        $csql = "SELECT s.*,c.name,c.description,c.example_content AS cid FROM studio_content_types s,content_types c WHERE s.content_types_id=c.id AND s.studio_id=" . Yii::app()->user->studio_id . " AND s.is_enabled=1 ORDER BY s.id_seq ASC ,s.id DESC";
        $data = $con->createCommand($csql)->queryAll();
        //Get the Content Types
        $contentTypes = $con->createCommand('SELECT * FROM content_types WHERE 1 ORDER BY id')->queryAll();
        $this->render('contentTypeList', array('data' => $data, 'contentTypes' => $contentTypes));
    }*/

    /**
     * @method public addContentTypes() Add a content Type to studio
     * @author GDR<support@muvi.com>
     * @return HTML 
     */
   /* function actionAddContentTypes() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        if (isset($_REQUEST['displayname']) && $_REQUEST['displayname']) {
            $contentModel = new StudioContentType();
            $data = $contentModel->find('(LOWER(display_name)=:display_name) AND studio_id=:studio_id', array(':display_name' => strtolower($_REQUEST['displayname']), ':studio_id' => Yii::app()->user->studio_id));
            if ($data) {
                $data = $data->attributes;
                $arr['msg'] = "Oops! Sorry content type already exsists with your studio";
            } else {
                $ip_address = CHttpRequest::getUserHostAddress();
                $contentModel->studio_id = Yii::app()->user->studio_id;
                $contentModel->content_types_id = $_REQUEST['content_types_id'];
                $contentModel->display_name = $_REQUEST['displayname'];
                $contentModel->permalink = Yii::app()->common->formatPermalink($_REQUEST['displayname']);
                $contentModel->muvi_alias = $this->getMuvialias($_REQUEST['content_types_id']);
                $contentModel->created_at = new CDbExpression("NOW()");
                $contentModel->is_enabled = 1;
                $contentModel->ip = $ip_address;
                $contentModel->save();
                Yii::app()->user->setFlash('success', $_REQUEST['displayname'] . ' added successfully.');
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
            echo json_encode($arr);
            exit;
        }
    }

    //Added by RK
    public function actionContenttype() {
        $studio_id = $this->studio->id;
        $option = isset($_REQUEST['option']) ? $_REQUEST['option'] : 'new';
        $ctype_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $ctypes_id = isset($_REQUEST['ctype_id']) ? $_REQUEST['ctype_id'] : 0;
        $studio_ctype = array();
        if ($option == 'new') {
            $this->breadcrumbs = array('Content Types', 'New Content Type');
            $this->pageTitle = Yii::app()->name . ' | ' . 'Add Content Type';
        } else {
            $this->breadcrumbs = array('Content Types');
            $this->pageTitle = Yii::app()->name . ' | ' . 'Update Content Types';

            $studio_ctype = StudioContentType::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $ctype_id));
        }
        $content_type = new ContentType();
        $data = $content_type->findAll(array('order' => 'id ASC'));
        $this->render('contenttype', array('content_types' => $data, 'studio_ctype' => $studio_ctype));
    }*/

    /**
     * @method public updateContent() Update the content details
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    /*function actionUpdateContent() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id']) {
            $contentModel = new StudioContentType();
            $data = $contentModel->find('(display_name=:display_name) AND studio_id=:studio_id AND id!=:id', array(':display_name' => $_REQUEST['displayname'], ':studio_id' => Yii::app()->user->studio_id, 'id' => $_REQUEST['content_id']));
            if ($data) {
                $data = $data->attributes;
                $arr['msg'] = "Oops! Sorry content type already exsists with your studio";
            } else {
                $contentModel = StudioContentType::model()->findByPk($_REQUEST['content_id']);
                $cdata = $contentModel->attributes;
                if ($cdata['content_types_id'] != $_REQUEST['content_types_id']) {
                    $data = Film::model()->find('content_type_id=:content_type_id', array(':content_type_id' => $_REQUEST['content_id']));
                    if ($data) {
                        $arr['msg'] = "Oops! Sorry you have content uploaded under this type.\n Please delete the content and try again! \nOtherwise, live chat with us for support.";
                        echo json_encode($arr);
                        exit;
                    } else if ($_REQUEST['content_types_id'] == 4) {
                        $lstream = $contentModel->find('content_types_id=:content_types_id AND studio_id=:studio_id', array(':content_types_id' => 4, ':studio_id' => Yii::app()->user->studio_id));
                        if ($lstream) {
                            $arr['msg'] = "Oops! Sorry you already have a Live Streaming content";
                            echo json_encode($arr);
                            exit;
                        }
                    }
                }
                $contentModel->display_name = $_REQUEST['displayname'];
                $contentModel->permalink = Yii::app()->common->formatPermalink($_REQUEST['displayname']);
                $contentModel->content_types_id = $_REQUEST['content_types_id'];
                $contentModel->save();
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
        }
        echo json_encode($arr);
        exit;
    }*/

    /**
     * @method public removeContent() Update the content details
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
   /* function actionRemoveContent() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id']) {
            $data = Film::model()->find('content_type_id=:content_type_id', array(':content_type_id' => $_REQUEST['content_id']));
            if ($data) {
                Yii::app()->user->setFlash('error', 'Oops ! Sorry you have content uploaded under this type. Please delete the content and try again! Otherwise, live chat with us for support.');
                //   Yii::app()->user->setFlash('Oops! Sorry you have content uploaded under this type.\n Please delete the content and try again! \nOtherwise, live chat with us for support.');
                $this->redirect($this->createUrl('admin/contentTypeList'));
            } else {
                StudioContentType::model()->deleteByPk($_REQUEST['content_id'], 'studio_id=:studio_id', array(':studio_id' => Yii::app()->user->studio_id));
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
                Yii::app()->user->setFlash('success', ' Content Type  Deleted successfully.');
                $this->redirect($this->createUrl('admin/contentTypeList'));
            }
        }
    }*/

    /**
     * @method public manageContent() add/edit the content details
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionNewContents() {
        $studio_id = Yii::app()->user->studio_id;
        if (Yii::app()->common->allowedUploadContent($studio_id) > 0)
            $allow_new = 1;
        else
            $allow_new = 0;
        $this->headerinfo = 'Add Content';
        $this->breadcrumbs = array("Manage Content","Content Library"=>array('admin/managecontent'),'Add Content');
            $language_id = $this->language_id;
            $sql = "SELECT * FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        
        $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        $celeb_types = Celebrity::model()->celeb_type();
        // Check for multipart content
        $multiContent = Film::model()->find('studio_id=:studio_id AND content_types_id=3', array(':studio_id' => Yii::app()->user->studio_id));
        if ($multiContent) {
            $is_multi_content = 1;
        }
        $multiContentAudio = Film::model()->find('studio_id=:studio_id AND content_types_id=6', array(':studio_id' => Yii::app()->user->studio_id));
        if ($multiContentAudio) {
            $is_multi_content_audio = 1;
        }
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        if ($allow_new == 0)
            Yii::app()->user->setFlash('error', 'You can add up to 10 content during Free Trial. Please <a href="' . Yii::app()->getBaseUrl(true) . '/admin/subscription">purchase a subscription</a> to continue adding content');
        if (@$_REQUEST['is_episode'] && @$_REQUEST['content_id']) {
            $addEpisode = 1;
            if(@$_REQUEST['content_types_id'] && is_numeric($_REQUEST['content_types_id'])){
                $custom_content_types_id = Yii::app()->general->getArrayFrommetadata_form_type_id(9);//for audio child
                $content_types_id = $_REQUEST['content_types_id'];//(ie : 6)
            }else{
                $custom_content_types_id = Yii::app()->general->getArrayFrommetadata_form_type_id(4);//for video child
                $content_types_id = 4;//for video child
            }
        }else{
            $content_types_id = $custom_content_types_id = Yii::app()->general->getStartContnetTypeID($studio_id);
        }
        if(is_array($custom_content_types_id)){
        $customComp = new CustomForms();
            $customData1[0] = $customComp->getCustomMetadata($studio_id, $custom_content_types_id['parent_content_type_id'], $custom_content_types_id['arg']);
            $customData1[1] = $custom_content_types_id['arg']['is_child'];
            $customData1[2] = $custom_content_types_id['parent_content_type_id'];
        }else{
			$customData1 = Yii::app()->general->getCustomMetadataForm($studio_id,$custom_content_types_id);/*Get form from $content_types_id */
    }
        $customData = $customData1[0];
        $_REQUEST['is_episode'] = $customData1[1];
        $formdata = Yii::app()->general->formlist($studio_id);
        foreach ($formdata as $key => $value) {
            if(!isset($is_multi_content) && ($value['metadata_form_type_id']==4)){//remove VOD multipart child
                unset($formdata[$key]);
			}
            if(!isset($is_multi_content_audio) && ($value['metadata_form_type_id']==9)){//remove AOD multipart child
                unset($formdata[$key]);
            }
        }        
        if($content_types_id['parent_content_type_id']==5){//physical
            $pg['currency_code'] = PGProduct::getStudioCurrencyCode($this->studio);
            $settings = Yii::app()->general->getPgSettings();
            $pg['enable_category'] = $settings['enable_category'];
            $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
            $pg['shipping_size'] = CHtml::listData($sizes, 'size_unique_name', 'size');
        }
        $productize = Yii::app()->general->checkProductizeExtension($studio_id);
        $IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);
		$checkImageKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
        $this->render('newcontents', array('contentList' => @$contentCategories, 'celeb_types' => $celeb_types, 'allow_new' => $allow_new, 'all_images' => $all_images, 'is_multi_content' => @$is_multi_content, 'content_id' => @$_REQUEST['content_id'], 'is_episode' => @$_REQUEST['is_episode'], 'addEpisode' => @$addEpisode, 'customData' => $customData, 'is_multi_content_audio' => @$is_multi_content_audio, 'content_types_id' => $content_types_id, 'formdata' => $formdata, 'parent_content_type' => @$customData1[2], 'pg' => @$pg, 'productize' => $productize, 'IsDownloadable' => $IsDownloadable, 'checkImageKey'=>$checkImageKey));
    }

    /**
     * @method public manageContent() add/edit the content details
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionAddContents() {
        //Insert the data in films table first 
        $Films = new Film();
        
        $data = $_REQUEST['movie'];
        if (!isset($data['parent_content_type_id']) || empty($data['parent_content_type_id'])) {
            $cmfid = CustomMetadataForm::model()->find(array('select' => 'parent_content_type_id,content_type,is_child', 'condition' => 'id=:id', 'params' => array(':id' => $data['custom_metadata_form_id'])));
            $data['parent_content_type'] = $cmfid['parent_content_type_id'];
        }
        $movie = $Films->addContent($data);
        $movie_id = $movie['id'];
        $studio_id = Yii::app()->user->studio_id;
		//Adding permalink to url routing 
		$urlRouts['permalink'] = $movie['permalink'];
		$urlRouts['mapped_url'] = '/movie/show/content_id/'.$movie_id;
		$urlRoutObj = new UrlRouting();
		$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);
		
        //Insert Into Movie streams table
        $MovieStreams = new movieStreams();
        $MovieStreams->studio_id = Yii::app()->user->studio_id;
        $MovieStreams->movie_id = $movie_id;
        $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
        $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->is_episode = 0;
		$MovieStreams->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
        $MovieStreams->save();

        //Check and Save new Tags into database
        $movieTags = new MovieTag();
        $movieTags->addTags($data);
        $ctypes = explode('-', $_REQUEST['movie']['content_type']);
        if (isset($_REQUEST['content_filter_type'])) {
            $ctype = explode('-', $data['content_type']);
            $contentFilterCls = new ContentFilter();
            $addContentFilter = $contentFilterCls->addContentFilter($_REQUEST, $ctype[0]);
        }
        //Upload poster
		$this->processContentImage($data['content_types_id'],$movie_id,$MovieStreams->id);
        $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
        if($checkfireappletv){
            $this->processAppTVImage($data['content_types_id'],$movie_id,$MovieStreams->id);
        }
        if(HOST_IP != '127.0.0.1'){
			$solrObj = new SolrFunctions();
			if($data['genre']){
				foreach($data['genre'] as $key=>$val){
					$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
					$solrArr['content_id'] = $movie_id;
					$solrArr['stream_id'] = $MovieStreams->id;
					$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
					$solrArr['is_episode'] = $MovieStreams->is_episode;
					$solrArr['name'] = $movie['name'];
					$solrArr['permalink'] = $movie['permalink'];
					$solrArr['studio_id'] =  Yii::app()->user->studio_id;
					$solrArr['display_name'] = 'content';
					$solrArr['content_permalink'] =  $movie['permalink'];
					$solrArr['genre_val'] = $val;
					$solrArr['product_format'] = '';	
					$ret = $solrObj->addSolrData($solrArr);
				}               
			}else{
				$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
				$solrArr['content_id'] = $movie_id;
				$solrArr['stream_id'] = $MovieStreams->id;
				$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
				$solrArr['is_episode'] = $MovieStreams->is_episode;
				$solrArr['name'] = $movie['name'];
				$solrArr['permalink'] = $movie['permalink'];
				$solrArr['studio_id'] =  Yii::app()->user->studio_id;
				$solrArr['display_name'] = 'content';
				$solrArr['content_permalink'] =  $movie['permalink'];
				$solrArr['genre_val'] = '';
				$solrArr['product_format'] = '';	
				$ret = $solrObj->addSolrData($solrArr);   
            }
        }               
        if(isset(Yii::app()->user->is_partner)){        
            $PartnersContent = new PartnersContent();
            $PartnersContent->studio_id = $studio_id;
            $PartnersContent->user_id = Yii::app()->user->id;
            $PartnersContent->movie_id = $movie_id;
            $PartnersContent->save();
        }
        Yii::app()->user->setFlash('success', 'Wow! Content added successfully.');
        $this->redirect($this->createUrl('admin/managecontent'));exit;
    }

    /**
     * @method public getGenreTags() Returns the list of Genre tags for the studio
     * @return json 
     * @author GDR<support@muvi.com>
     */
    function actionGetTags() {
		$studio_id = Yii::app()->user->studio_id;
        $taggableType = $_REQUEST['taggable_type'];
        if($taggableType==1){
        $customGenre = CustomMetadataField::model()->find("studio_id=:studio_id AND f_id='genre' ", array(':studio_id' => $studio_id));       
        if ($customGenre) { 
            $customGenre=json_decode($customGenre->f_value, TRUE);
            $tagsArr= array_intersect_key($customGenre,array_unique(array_map('strtolower', $customGenre)));
        
        } else {          
            $criteria = new CDbCriteria();
            $criteria->select = 't.id,t.name,t.studio_id,t.taggable_type';
            $criteria->condition = 't.studio_id=:studio_id AND t.taggable_type=:taggable_type';
            $criteria->params = array(':studio_id' => $studio_id, ':taggable_type' => 1);
            $criteria->group = 't.name';
            $criteria->order = 't.name ASC';
            $tags = MovieTag::model()->findAll($criteria);
            $tagsArr = CHtml::listData($tags, 'id', 'name');

        }      
        }else
        {
        $tags = MovieTag::model()->findAll('taggable_type=:taggable_type AND studio_id=:studio_id', array(':taggable_type' => $taggableType,':studio_id' => $studio_id));
        
        $tagsArr = CHtml::listData($tags, 'id', 'name');
        }
       
        echo json_encode($tagsArr);
        exit;
    }

    /**
     * @method public addtrailer() Add/Update a trailer for a content
     * @return int trailer_id
     * @author GDR<support@muvi.com>
     */
    function addTrailer($movie_id,$model) {
        if ($movie_id) {
            $trailer = $model::model()->find('movie_id=:movie_id', array(':movie_id' => $movie_id));
            if ($trailer) {
                $trailer = $trailer->attributes;
                return $trailer['id'];
            } else {
                $trailer = new $model();
                $trailer->movie_id = $movie_id;
                $trailer->created_by = Yii::app()->user->id;
                $trailer->created_date = gmdate('Y-m-d');
                $trailer->save();
                return $trailer->id;
            }
        } else {
            return false;
        }
    }

    public function actionGeneral() {
        $this->breadcrumbs = array('Studio Configuration',);
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $this->render('analytics', array('studio' => $studio));
    }

    public function actionSaveconf() {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = $this->studio;
        $is_updated = 0;

        //if(isset($_REQUEST['contact_email']) && $_REQUEST['contact_email'] != '' && filter_var($_REQUEST['contact_email'], FILTER_VALIDATE_EMAIL))
        if (isset($_REQUEST['contact_email']) && $_REQUEST['contact_email'] != '' && filter_var($_REQUEST['contact_email'])) {
            if (isset($_REQUEST['google_analytics'])) {
                $std->google_analytics = htmlspecialchars($_REQUEST['google_analytics']);
            }
            if (isset($_REQUEST['address'])) {
                $std->address = htmlspecialchars($_REQUEST['address']);
            }
            if (isset($_REQUEST['contact_phone'])) {
                $std->phone = htmlspecialchars($_REQUEST['contact_phone']);
            }
            if (isset($_REQUEST['contact_fax'])) {
                $std->fax = htmlspecialchars($_REQUEST['contact_fax']);
            }
            $std->contact_us_email = $_REQUEST['contact_email'];
            $is_updated = 1;
        }
        if ($is_updated == 1) {
            $std->save();
            Yii::app()->user->setFlash('success', 'Studio details saved successfully.');
        } else
            Yii::app()->user->setFlash('error', 'Nothing is updated.');
        $url = $this->createUrl('/admin/general');
        $this->redirect($url);
    }

    /**
     * @method removeContentTrailer() Remove trailer from a content
     * @author GDR<support@muvi.com>
     * @return json 
     */
    function actionRemoveContentTrailer() {
        //Check the movie id blongs from the logged in users studio
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $movie_id = $_REQUEST['movie_id'];
        }
        if ($movie_id) {
            $movie = Film::model()->find('id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->common->getStudiosId()));
            if ($movie) {
                $ret = $this->removeTrailer($movie_id);
                if ($ret) {
                    $arr['msg'] = 'Trailer removed successfully.';
                    $arr['err'] = 0;
                } else {
                    $arr['msg'] = 'Error in deleting trailer.';
                    $arr['err'] = 1;
                }
            } else {
                $arr['msg'] = 'You are not authorised to delete this video';
                $arr['err'] = 1;
            }
        } else {
            $arr['msg'] = 'Invalid trailer';
            $arr['err'] = 1;
        }
        if ($_REQUEST['is_ajax']) {
            echo json_encode($arr);
            exit;
        }
    }

    /**
     * @method public ajaxSubmitForm() 
     * @return type Description
     */
    function actionAjaxSubmitForm() {
        $arr['succ'] = 0;		
        $Films = new Film();
        $data = $_REQUEST['movie'];
        $cmfid = CustomMetadataForm::model()->find(array('select' => 'parent_content_type_id,content_type,is_child', 'condition' => 'id=:id', 'params' => array(':id' => $data['custom_metadata_form_id'])));
        if (!isset($data['content_types_id']) || empty($data['content_types_id'])) {
            $data['content_types_id'] = Yii::app()->general->getcontents_types_id($cmfid);
        }
        if (!isset($data['parent_content_type_id']) || empty($data['parent_content_type_id'])) {
            $data['parent_content_type'] = $cmfid['parent_content_type_id'];
        }
        $movie = $Films->addContent($data);
        $movie_id = $movie['id'];
		$studio_id = Yii::app()->user->studio_id;
		//Adding permalink to url routing 
		$urlRouts['permalink'] = $movie['permalink'];
		$urlRouts['mapped_url'] = '/movie/show/content_id/'.$movie_id;
		$urlRoutObj = new UrlRouting();
		$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);
		
        $movie_id = $movie['id'];
        $arr['uniq_id'] = $movie['uniq_id'];
        //Remove sample data from website
        $studio = $this->studio;
        $studio->show_sample_data = 0;
        $studio->save();
        //Insert Into Movie streams table
        $MovieStreams = new movieStreams();
        $MovieStreams->studio_id = Yii::app()->user->studio_id;
        $MovieStreams->movie_id = $movie_id;
        $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
        $MovieStreams->is_episode = 0;
        $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        $publish_date = NULL;
        if (@$_REQUEST['content_publish_date']) {
            if (@$_REQUEST['publish_date']) {
                $pdate = explode('/', $_REQUEST['publish_date']);
                $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                if (@$_REQUEST['publish_time']) {
                    $publish_date .= ' ' . $_REQUEST['publish_time'];
                }
            }
        }
        $MovieStreams->content_publish_date = $publish_date;
		$MovieStreams->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream'])?2:((@$_REQUEST['download'])?1:0);;
        $MovieStreams->save();

        //Check and Save new Tags into database
        $movieTags = new MovieTag();
        $movieTags->addTags($data);
        if (isset($_REQUEST['content_filter_type'])) {
            $contentFilterCls = new ContentFilter();
            $addContentFilter = $contentFilterCls->addContentFilter($_REQUEST, $data['content_type_id']);
        }
        if(isset(Yii::app()->user->is_partner)){        
            $PartnersContent = new PartnersContent();
            $PartnersContent->studio_id = $studio_id;
            $PartnersContent->user_id = Yii::app()->user->id;
            $PartnersContent->movie_id = $movie_id;
            $PartnersContent->save();
        }
        //First Crop the image with specified size
        $arr['succ'] = 1;
        $arr['movie_id'] = $movie_id;
        $arr['movie_stream_id'] = $MovieStreams->id;
        $arr['content_types_id'] = $data['content_types_id'];
        $arr['name'] = $data['name'];
        $arr['is_episode'] = $MovieStreams->is_episode;
        $arr['poster'] = POSTER_URL . '/no-image.png';
        //Upload poster
		$ret = $this->processContentImage($data['content_types_id'],$movie_id,$MovieStreams->id);
                $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
                if($checkfireappletv){
                    $this->processAppTVImage($data['content_types_id'],$movie_id,$MovieStreams->id);
                }
		if(HOST_IP != '127.0.0.1'){
			$solrObj = new SolrFunctions();
			if($data['genre']){
			foreach($data['genre'] as $key=>$val){
			$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
			$solrArr['content_id'] = $movie_id;
			$solrArr['stream_id'] = $arr['movie_stream_id'];
			$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
			$solrArr['is_episode'] = $MovieStreams->is_episode;
			$solrArr['name'] = $movie['name'];
			$solrArr['permalink'] = $movie['permalink'];
			$solrArr['studio_id'] =  Yii::app()->user->studio_id;
			$solrArr['display_name'] = 'content';
			$solrArr['content_permalink'] =  $movie['permalink'];
			$solrArr['genre_val'] = $val;
			$solrArr['product_format'] = '';             
			if($publish_date!=NULL)
				$solrArr['publish_date'] = $publish_date;
			$ret = $solrObj->addSolrData($solrArr);
		}
                    }else
                    {
                     $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
			$solrArr['content_id'] = $movie_id;
			$solrArr['stream_id'] = $arr['movie_stream_id'];
			$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
			$solrArr['is_episode'] = $MovieStreams->is_episode;
			$solrArr['name'] = $movie['name'];
			$solrArr['permalink'] = $movie['permalink'];
			$solrArr['studio_id'] =  Yii::app()->user->studio_id;
			$solrArr['display_name'] = 'content';
			$solrArr['content_permalink'] =  $movie['permalink'];
                        $solrArr['genre_val'] = '';
                        $solrArr['product_format'] = '';             
					if($publish_date!=NULL)
						$solrArr['publish_date'] = $publish_date;
			$ret = $solrObj->addSolrData($solrArr);   
                    }
		}
			$arr['poster'] = @$ret['original'];
			echo json_encode($arr);exit;
        }         
            
    /**
     * @method public getEpisodeForm() Generate a add episode form based on the content types id
     * @author GDR<support@muvi.com>
     * @return html Returns the HTML form of add episode
     */
    function actionGetEpisodeForm() {
        $contentTypeId = $_REQUEST['contentType_id'];
			$movies = Film::model()->findAll('content_types_id =:content_types_id AND studio_id =:studio_id',array(':content_types_id'=>3,':studio_id'=>Yii::app()->user->studio_id));
        if ($movies) {
            $shows = CHtml::listData($movies, 'id', 'name');
            $output = $this->renderPartial('episode', array('shows' => $shows), true);
				echo $output;exit;
        } else {
				echo 'error';exit;
        }
    }
    /**
 * @method public getLiveStreamForm() Generate a add episode form based on the content types id
 * @author GDR<support@muvi.com>
 * @return html Returns the HTML form of add episode
 */
    function actionGetLiveStreamForm() {
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        $output = $this->renderPartial('channel_form', array('contentCategories' => $contentCategories), true);
        echo $output;
        exit;
    }

    /**
     * @method public getContentForm() Generate a add episode form based on the content types id
     * @author GDR<support@muvi.com>
     * @return html Returns the HTML form of add episode
     */
    function actionGetContentForm() {
        $contentTypeId = $_REQUEST['contentTypes_id'];
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        
        $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        $output = $this->renderPartial('basic_content', array('content_type_id' => $_REQUEST['contnet_type_id'], 'content_types_id' => $_REQUEST['content_types_id'], 'contentCategories' => $contentCategories), true);
        echo $output;
        exit;
    }

    /**
 * @method public cropTopbanner() Crop the top banner and save it into the database
 * @author GDR<support@muvi.com>
 * @return void Redirect to the edit page
 */
    function actionCropTopbanner() {
        $object_id = $movie_id = $_REQUEST['content_id'];
        $this->removePosters($movie_id, 'topbanner');
		$cropDimension = array('thumb' => '300x90', 'standard' => '1200x350');
		$_REQUEST['fileimage'] = $_REQUEST['jcrop'];
		$dir = $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $movie_id;
		$_FILES['Filedata'] = @$_FILES['Filedata1'];
		if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
			$isExistImageKey = ImageManagement::model()->isExistImageKey();
			if (isset($_POST['image_key']) && $_POST['image_key'] == '') {
				$arr['succ'] = 3;
				$arr['message'] = 'Please enter image key';
			} else if (!empty($_POST['image_key']) && $isExistImageKey['success'] == 0) {
				$arr['succ'] = 3;
				$arr['message'] = 'Image key already exist.';
			} else {
			$cdimension = array('thumb' => "64x64");
			$ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);
			$path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
			$ret = $this->uploadPoster($_FILES['Filedata'], $object_id, 'topbanner', $cropDimension, $path);
			if ($movie_id) {
				Yii::app()->common->rrmdir($dir);
			}
			$arr['succ'] = 1;
			$arr['topbannerthumb'] = $ret['thumb'];
			}
		} else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name1'] != '') {
			$file_info = pathinfo($_REQUEST['g_image_file_name1']);
			$_REQUEST['g_image_file_name1'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
			
			$jcrop_allimage = $_REQUEST['jcrop_allimage'];
			
			$jcrop_topbanner = $_REQUEST['jcrop_topbanner'];
            $image_name = $_REQUEST['g_image_file_name1'];
			
			$dimension['x1'] = $jcrop_topbanner['x15'];
            $dimension['y1'] = $jcrop_topbanner['y15'];
            $dimension['x2'] = $jcrop_topbanner['x25'];
            $dimension['y2'] = $jcrop_topbanner['y25'];
            $dimension['w'] = $jcrop_topbanner['w5'];
            $dimension['h'] = $jcrop_topbanner['h5'];
			
			$path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name1'], $_REQUEST['g_original_image1'], $dir, $dimension);
			$fileinfo['name'] = $_REQUEST['g_image_file_name1'];
			$fileinfo['error'] = 0;
			$ret = $this->uploadPoster($fileinfo, $object_id, 'topbanner', $cropDimension, $path);
			Yii::app()->common->rrmdir($dir);
			$arr['succ'] = 1;
			$arr['topbannerthumb'] = $ret['thumb'];
		} else {
			$ret['thumb'] = '';
			$arr['succ'] = 0;
		}
		echo json_encode($arr);
	}

    //Settings for video ads
    public function actionVideoad() {
        $this->breadcrumbs = array('Monetization', 'Advertisement');
        $this->headerinfo = 'Video Ad Server Integration (AVOD)';
        $this->pageTitle = "Muvi | Advertisement";
        $this->layout = 'admin';

        $studio_id = Yii::app()->user->studio_id;
        $adnetworks = AdNetworks::model()->findAll(array('order' => 'sortorder'));
        $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = ' . $studio_id));

        $this->render('videoad', array('adnetworks' => $adnetworks, 'studio_ads' => $studio_ads));
    }

    public function actionSaveganalytics() {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();

        if (isset($_REQUEST['google_analytics'])) {
            if (isset($_REQUEST['google_analytics'])) {
                $google_analytics = htmlspecialchars($_REQUEST['google_analytics']);
            }
            $is_updated = $std->updateganalytics($google_analytics, $studio_id);
        }
        if ($is_updated) {
            Yii::app()->user->setFlash('success', 'Studio details saved successfully.');
        } else
            Yii::app()->user->setFlash('error', 'Nothing is updated.');
        $url = $this->createUrl('/admin/general');
        $this->redirect($url);
    }

    public function actionSavevideoad() {
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST) && count($_REQUEST) > 0) {
            $ad_network_id = $_REQUEST['mvad_network_id'];
            $ad_id = $_REQUEST['mvad_id'];
            $channel_id = $_REQUEST['channel_id_' . $ad_network_id];

            if ($channel_id != '') {
                if ($ad_id > 0) {
                    $studio_ad = StudioAds::model()->findByPk($ad_id);
                    $studio_ad->channel_id = $channel_id;
                    $studio_ad->ad_network_id = $ad_network_id;
                    $studio_ad->save();

                    $suc_msg = "Your ad server details updated successfully.";
                } else {
                    $studio_ad = new StudioAds();
                    $studio_ad->studio_id = $studio_id;
                    $studio_ad->ad_network_id = $ad_network_id;
                    $studio_ad->channel_id = $channel_id;
                    $studio_ad->save();

                    $suc_msg = "Your ad server details saved successfully.";
                }
                Yii::app()->user->setFlash('success', $suc_msg);
                $url = $this->createUrl('admin/videoad');
                $this->redirect($url);
            } else {
                Yii::app()->user->setFlash('error', 'You have not entered values correctly');
                $url = $this->createUrl('admin/videoad');
                $this->redirect($url);
            }
        } else
            die("Error");
    }

    function actionSaveadconfig() {
        $studio_id = Yii::app()->common->getStudiosID();
        $stream_id = isset($_REQUEST['ad_stream_id']) ? $_REQUEST['ad_stream_id'] : 0;
        if ($stream_id > 0) {
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $stream_id));
            $enable_ad = isset($_REQUEST['enable_ad']) ? $_REQUEST['enable_ad'] : 0;
            $rolltype = isset($_REQUEST['rolltype']) ? $_REQUEST['rolltype'] : 0;
            $roll_after = isset($_REQUEST['roll_after']) ? $_REQUEST['roll_after'] : 0;

            $streams->enable_ad = $enable_ad;
            $streams->rolltype = $rolltype;
            $streams->roll_after = $roll_after;
            $streams->save();

            Yii::app()->user->setFlash('success', 'Ad details saved successfully.');
            $this->redirect($_SERVER['HTTP_REFERER']);
            exit;
        } else {
            Yii::app()->user->setFlash('success', 'Oops! You have entered incorrect details.');
            $this->redirect($_SERVER['HTTP_REFERER']);
            exit;
        }
    }

    public function actionServerLocation() {
        $this->breadcrumbs = array('Server Location',);
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $s3bucket = new S3bucket();
        $studio = $std->findByPk($studio_id);
        $allocated_bucket_details = $s3bucket->getAllocatedBucket($studio->s3bucket_id);
        $all_avalilable_buckets = $s3bucket->getAllBuckets();
        $this->render('server_location', array('allocated_bucket_details' => $allocated_bucket_details, 'all_avalilable_buckets' => $all_avalilable_buckets));
    }

    public function actionUpdateServerLocation() {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();

        if (isset($_REQUEST['s3bucket_id'])) {
            if (isset($_REQUEST['s3bucket_id'])) {
                $bucket_id = $_REQUEST['s3bucket_id'];
            }
            $is_updated = $std->updateServerLocation($bucket_id, $studio_id);
        }
        if ($is_updated) {
            Yii::app()->user->setFlash('success', 'Studio details saved successfully.');
        } else
            Yii::app()->user->setFlash('error', 'Nothing is updated.');
        $url = $this->createUrl('/admin/serverLocation');
        $this->redirect($url);
    }

    public function actionEpisodeList() {
        $cond = " ";
        $contentType = '';
        $studio_id = Yii::app()->user->studio_id;

        $params = array(':studio_id' => $studio_id);
        $pdo_cond_arr = array(":studio_id" => Yii::app()->user->studio_id);
        $pdo_cond = " f.studio_id=:studio_id ";
        $con = Yii::app()->db;
        $movie_id = $_REQUEST['movie_id'];
        $selected_series = 0;
        if ($_REQUEST['series_number']) {
            $series_number = $_REQUEST['series_number'];
            $selected_series = $series_number;
        } else {
            $series_number = "(select MAX(series_number) from movie_streams where movie_id = " . $movie_id . ")";
        }
        $page_size = 5;
        $offset = 0;
        if (isset($_REQUEST['limit_status'])) {
            $offset = ($_REQUEST['limit_status'] - 1) * $page_size;
        }

        $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.name,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND ms.is_episode = 1 AND ms.series_number = ' . $series_number . ' AND ms.movie_id = ' . $movie_id)
                ->order('ms.episode_number DESC')
                ->limit($page_size, $offset);
        $data = $command->queryAll();
        if ($data) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .="'" . $v['stream_id'] . "',";
                } else {
                    $movieids .="'" . $v['id'] . "',";
                }
            }

            $command = Yii::app()->db->createCommand()
                    ->select('MAX(series_number) AS series_number')
                    ->from('movie_streams')
                    ->where('movie_id = ' . $movie_id);
            $series_list = $command->queryAll();
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(episode_id) AS cnt, episode_id,video_type FROM video_logs WHERE episode_id IN(" . $stream_ids . ") GROUP BY episode_id";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['episode_id']] = $val['cnt'];
                    }
                }
            }

            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }
        $se_sql = "SELECT DISTINCT(series_number) FROM movie_streams WHERE movie_id = " . $movie_id . " ORDER BY series_number ASC";
        $allSeries = $con->createCommand($se_sql)->queryAll();
        if (!$_REQUEST['limit_status']) {
            $output = $this->renderPartial('treeviewEpisodes', array('data' => $data, 'content_type_id' => @$searchData['content_type_id'], 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sample' => @$sample, 'contentList' => @$contentTypes, 'sort' => @$sort, 'orderType' => @$orderType, 'studio_ads' => @$studio_ads, 'series_list' => $series_list, 'selected_series' => $selected_series, 'allSeries' => $allSeries), true);
        } else {
            $output = $this->renderPartial('treeviewEpisodes_more', array('data' => $data, 'content_type_id' => @$searchData['content_type_id'], 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sample' => @$sample, 'contentList' => @$contentTypes, 'sort' => @$sort, 'orderType' => @$orderType, 'studio_ads' => @$studio_ads, 'series_list' => $series_list, 'selected_series' => $selected_series, 'allSeries' => $allSeries), true);
        }

        echo $output;
        exit;
    }

    /**
     * @method public removetrailer($movie_id) Remove trailer for the movie
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function removeTrailer($movie_id) {
        $trailer = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id' => $movie_id));
        if ($trailer) {
            $tdata = $trailer->attributes;
            if ($tdata['trailer_file_name']) {
                $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $s3dir = $signedBucketPath . 'uploads/trailers/' . $tdata['id'] . "/";
                $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
                $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
                $bucket = $bucketInfo['bucket_name'];
                if ($tdata['trailer_file_name']) {
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucket,
                        'Prefix' => $s3dir
                    ));
                    foreach ($response as $object) {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $object['Key']
                        ));
                    }
                }
                /* $s3->get_object_list($bucket, array(
                  'prefix' => $s3dir
                  ))->each(function($node, $i, $s3) {
                  $s3->batch()->delete_object($bucket, $node);
                  }, array($s3));
                  $responses = $s3->batch()->send(); */
            }
            $trailer->delete();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function actionAdvanced() {
        $studio_id = Yii::app()->common->getStudiosId();
        $stdMngCookie = new StudioManageCookie();
        $this->pageTitle = Yii::app()->name . ' | ' . ' Advanced Settings';
        $this->breadcrumbs = array('Settings' ,'Advanced Settings');
        $this->headerinfo = "Advanced Settings";
        if ($_REQUEST && isset($_REQUEST['id'])) {
            $is_enable = 0;
            if (isset($_REQUEST['is_enable'])) {
                $is_enable = 1;
            }
            if ($_REQUEST['id'] == 0) {
                $stdMngCookieSave = new StudioManageCookie();
                $stdMngCookieSave->studio_id = $studio_id;
                if (@IS_LANGUAGE == 1) {
                    $stdMngCookieSave->language_id = $this->language_id;
                }
                $stdMngCookieSave->is_enable = $is_enable;
                $stdMngCookieSave->message = stripslashes($_REQUEST['cookieMsg']);
                $stdMngCookieSave->save();
                Yii::app()->user->setFlash('success', 'Cooking setting saved successfully.');
            } else {
                $stdMngCookieUpdate = $stdMngCookie->findByPk($_REQUEST['id']);
                $stdMngCookieUpdate->is_enable = $is_enable;
                $stdMngCookieUpdate->message = stripslashes($_REQUEST['cookieMsg']);
                $stdMngCookieUpdate->save();
                Yii::app()->user->setFlash('success', 'Cooking setting updated successfully.');
            }
            $url = $this->createUrl('/admin/advanced');
            $this->redirect($url);
            exit();
        } else {
            if (@IS_LANGUAGE == 1) {
                $stdMngCookieDetails = $stdMngCookie->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $this->language_id));
            } else {
                $stdMngCookieDetails = $stdMngCookie->findByAttributes(array('studio_id' => $studio_id));
            }
            $stdMngCookieId = 0;
            $is_enable = 0;
            $message = '';
            if (isset($stdMngCookieDetails->id)) {
                $stdMngCookieId = $stdMngCookieDetails->id;
                $message = $stdMngCookieDetails->message;
                if ($stdMngCookieDetails->is_enable == 1) {
                    $is_enable = 1;
                }
            }
            if ($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) {
                
            } else {
                $Apikey = OuathRegistration::model()->find('studio_id = :studio_id AND status= 1', array(':studio_id' => $this->studio->id));

                if ($Apikey) {
                    $apikey = $Apikey->oauth_token;
                } else {
                    $Apikey = new OuathRegistration();
                    $Apikey->studio_id = $this->studio->id;
                    $Apikey->oauth_token = Yii::app()->common->generateUniqNumber();
                    $Apikey->platform = 'mobile';
                    $Apikey->created_by = Yii::app()->user->id;
                    $Apikey->created_date = gmdate('Y-m-d H:i:s');
                    $Apikey->save();
                    $apikey = $Apikey->oauth_token;
                }
            }
              if((isset($_REQUEST['apps']) && count($_REQUEST['apps'])!=0)):
                $apps         = $_REQUEST['apps'];
                $app_sum = array_sum($apps);
                $app_menu = StudioAppMenu::model()->updateAppSettings($studio_id,$app_sum);
                Yii::app()->user->setFlash('success', 'Settings updated successfully.');
                $url = $this->createUrl('/admin/advanced');
                $this->redirect($url);
                exit();
            endif;
            $this->render('advanced', array('stdMngCookieId' => $stdMngCookieId, 'is_enable' => $is_enable, 'message' => $message, 'apikey' => $apikey));
        }
    }

    public function actionAdvancedSetting() {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $this->pageTitle = Yii::app()->name . ' | ' . ' Advanced Settings';
        $dbcon = Yii::app()->db;
        $trackcode = $dbcon->createCommand("SELECT id,name FROM tracking_code WHERE studio_id={$studio_id}");
        $data = $trackcode->queryAll();
        if ($_REQUEST) {
            $google_analytics = htmlspecialchars(trim($_REQUEST['google_analytics']));
            $google_webmaster = htmlspecialchars($_REQUEST['google_webmaster']);
            $robots_txt = htmlspecialchars($_REQUEST['robots_txt']);
            //print_r($robots_txt);exit;
            $is_updated = $std->updateadvancedsetting($google_analytics, $google_webmaster, $robots_txt, $studio_id);
            if ($is_updated) {
                Yii::app()->user->setFlash('success', 'Settings updated successfully.');
            } else
                Yii::app()->user->setFlash('error', 'Nothing is updated.');
            $url = $this->createUrl('/admin/advancedSetting');
            $this->redirect($url);
        }else {
            $this->breadcrumbs = array('Marketing','Advanced Settings');
            $this->headerinfo = "Advanced Settings";
            //$studio = $std->findByPk($studio_id);
            $studio = $this->studio;
            $this->render('advanced_setting', array('studio' => $studio, 'data' => $data));
        }
    }
    function actionNewtracking() {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Trackingcode();
        $this->pageTitle = Yii::app()->name . ' | ' . 'New Tracking';
        $this->breadcrumbs = array('Settings', 'Advanced Settings', 'New Tracking');
        $this->headerinfo = "New Tracking Code";
        $studio = $this->studio;
        if ($_REQUEST['option'] == 'edit') {
            $track_id = $_REQUEST['track_id'];
            $dbcon = Yii::app()->db;
            $action = "edit";
            $tracking = $dbcon->createCommand("SELECT id,name,location,tag,code FROM tracking_code WHERE id ={$track_id} AND studio_id={$studio_id}");
            $data = $tracking->queryRow();
            $this->render('newtracking', array('studio' => $studio, 'data' => $data, 'action' => $action));
        } else if ($_REQUEST['option'] == 'delete') {
            $track_id = $_REQUEST['track_id'];
            $trackcode = Trackingcode :: model()->findByPk($track_id);
            $trackcode->delete();
            Yii::app()->user->setFlash('success', 'Trackig code  Deleted successfully.');
            $url = $this->createUrl('/admin/advancedSetting');
            $this->redirect($url);
        } else {
            $this->render('newtracking', array('studio' => $studio));
        }
    }

    function actionEdittracking() {
        $studio_id = Yii::app()->common->getStudiosId();
        $track_id = $_REQUEST['track_id']; {
            $trackcode = new Trackingcode();
            if ($track_id != '') {
                $trackcode = $trackcode->findByPk($track_id);
            }
            $trackcode->studio_id = $this->studio->id;
            $trackcode->name = $_POST['track_name'];
            $trackcode->location = $_POST['location'];
            $trackcode->tag = $_POST['tag'];
            $trackcode->code = htmlentities($_POST['code']);
            $trackcode->save();
        }
        Yii::app()->user->setFlash('success', 'Trackig code  updated successfully.');
        $url = $this->createUrl('/admin/advancedSetting');
        $this->redirect($url);
    }
    function actionManageUsers() {
        $this->breadcrumbs = array('Settings' => array('admin/manageUsers'), 'Manage Users');
        $this->pageTitle = "Muvi | Manage Users";
        $this->layout = 'admin';
        $studio_id = Yii::app()->common->getStudiosId();

        $users = User::model()->findAll('studio_id=:studio_id AND is_active=:is_active AND (role_id=:t1 OR role_id=:t2)', array(':studio_id' => $studio_id, ':is_active' => 1, ':t1' => 2, ':t2' => 3), array('order' => 'first_name ASC'));
        $roles = Role::model()->findAll('role_type=:t1 OR role_type=:t2', array(':t1' => 'Admin', ':t2' => 'Member'), array('order' => 'role_type ASC'));

        $this->render('manageusers', array('users' => $users, 'roles' => $roles));
    }

    function actionSaveUserName() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['studio_id']) && isset($_REQUEST['first_name'])) {
            $user = User::model()->findByAttributes(array('id' => $_REQUEST['user_id'], 'studio_id' => $_REQUEST['studio_id']));
            if (isset($user) && !empty($user)) {
                $user->first_name = trim($_REQUEST['first_name']);
                $user->last_name = trim($_REQUEST['last_name']);
                $user->save();
                Yii::app()->user->setFlash('success', 'User has been updated successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry, User can not be updated!');
            }
        }
        print 1;
        exit;
    }

    function actionSaveRole() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['studio_id']) && isset($_REQUEST['role_id'])) {
            $user = User::model()->findByAttributes(array('id' => $_REQUEST['user_id'], 'studio_id' => $_REQUEST['studio_id']));
            if (isset($user) && !empty($user)) {
                $user->role_id = trim($_REQUEST['role_id']);
                $user->save();
            }
        }
        print 1;
        exit;
    }

    function actionDeleteUser() {
        if (isset($_REQUEST['user']) && trim($_REQUEST['user']) && isset($_REQUEST['studio']) && trim($_REQUEST['studio'])) {
            $con = Yii::app()->db;
            $sql_usr = "UPDATE user SET is_active=0 WHERE studio_id=" . $_REQUEST['studio'] . " AND id =" . $_REQUEST['user'];
            $con->createCommand($sql_usr)->execute();
            Yii::app()->user->setFlash('success', 'User has been deleted successfully.');
        } else {
            Yii::app()->user->setFlash('error', "Oops! User can't be deleted.");
        }

        $url = $this->createUrl('/admin/manageUsers');
        $this->redirect($url);
    }

    function actionNewuser() {
        if (isset($_REQUEST['data']['Admin']) && !empty($_REQUEST['data']['Admin'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $data = $_REQUEST['data']['Admin'];

            //Insert data into the user table
            $password = substr(str_shuffle(trim($data['email'])), 0, 8);
            $enc = new bCrypt();
            $encrypted_pass = $enc->hash($password);

            $umodel = new User();
            $umodel->studio_id = $studio_id;
            $umodel->first_name = trim($data['first_name']);
            $umodel->last_name = trim($data['last_name']);
            $umodel->email = trim($data['email']);
            $umodel->encrypted_password = $encrypted_pass;
            $umodel->created_at = gmdate('Y-m-d H:i:s');
            $umodel->is_active = 1;
            $umodel->is_sdk = 1;
            $umodel->role_id = $data['role_id'];
            $umodel->signup_step = 0;
            $umodel->save();

            //Send to email to that user
            $this->sendEmailToNewUser($umodel, $password);
            Yii::app()->user->setFlash('success', 'New user has been added successfully.');
        } else {
            Yii::app()->user->setFlash('error', 'Invalid user data.');
        }

        $url = $this->createUrl('/admin/manageUsers');
        $this->redirect($url);
    }

    function sendEmailToNewUser($user = Null, $password = Null) {
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        $site_url = 'http://' . $studio->domain;

        $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);

        $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="" /></a>';

        $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';

        //Check facebook link given or not
        if ($studio->fb_link != '') {
            $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        } else {
            $fb_link = '';
        }

        //Check twitter link given or not
        if ($studio->tw_link != '') {
            $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        } else {
            $twitter_link = '';
        }

        //Check google plus link given or not
        if ($studio->gp_link != '') {
            $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
        } else {
            $gplus_link = '';
        }

        //Set variables for different email types
        $email = $user->email;
        $FirstName = $user->first_name;
        $StudioName = $studio->name;
        $domain = Yii::app()->getBaseUrl(true);

        $subject = "You have been granted access to '" . $studio->domain . "' administration";
        $content = '<p>Dear ' . $FirstName . ',</p>
                    <p>You have been granted access to ' . $StudioName . ' administration</p> 
                    <p>Please find following url and credentials .</p>
                    <p style="display:block;margin:0 0 17px">
                        Url: <strong><span mc:edit="domain"><a href="' . $domain . '">' . $domain . '</a></span></strong><br/>
                        User Name: <strong><span mc:edit="email">' . $user->email . '</span></strong><br/>
                        Password: <strong><span mc:edit="password">' . $password . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team ' . $StudioName . '</p>';

        $user_model = new User();
        $admin = $user_model->findByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'is_sdk' => 1, 'role_id' => 1));
        if (isset($admin) && !empty($admin)) {
            $from_mail = $admin->email;
        } else {
            $from_mail = 'info@' . $studio->domain;
        }

        $params = array(
            'website_name'=> $StudioName,
           'site_link'=> $site_link,
            'logo'=> $logo,
            'name'=> $FirstName,
            'mailcontent'=> $content
        );

        $message = array(
            'subject' => $subject,
            'from_email' => $from_mail,
            'from_name' => $studio->name,
            'to' => array(
                array(
                    'email' => $email,
                    'name' => $FirstName,
                    'type' => 'to'
                )
            )
        );

        $template_name = 'sdk_user_welcome_new';

        $obj = new Controller();
       
        Yii::app()->theme = 'bootstrap';
        $html = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$template_content),true);
        $retval=$obj->sendmailViaAmazonsdk($html,$subject,$email,$from_mail); 
         
    }

    public function actionSeo() {
        $this->breadcrumbs = array('Marketing','SEO');
        $this->headerinfo = "SEO";
        $studio_id = Yii::app()->common->getStudiosId();
        $this->pageTitle = Yii::app()->name . ' | ' . SEO;
        if ($_REQUEST) {
            Yii::app()->user->setFlash('success', 'Seo data updated successfully.');
            $url = $this->createUrl('/admin/seo');
            $this->redirect($url);
        } else {
            $std = new Studio();
            $studio = $std->findByPk($studio_id);

            $page = new Page();
            $allPages = $page->getPages($studio_id);

            $film = new Film();
            $allContent = $film->getAllContent($studio_id);

            $allProduct = PGProduct::model()->findAllByAttributes(array('studio_id'=>$studio_id));
            $studio_menu_type = new MenuItem();
            $allContentType = $studio_menu_type->getActiveContent($studio_id);

            $seo_info = new SeoInfo();
            $seoinfo = $seo_info->getAllInfo($studio_id, 'All');
            $seocontentinfo = $seo_info->getAllInfo($studio_id, 'content');
            $seostoreinfo = $seo_info->getAllInfo($studio_id, 'store');
            $seocontentlistinginfo = $seo_info->getAllInfo($studio_id, 'contentlisting');
            $seostaticinfo = $seo_info->getAllInfo($studio_id, 'static');

            $this->render('seo', array('studio' => $studio, 'pages' => $allPages, 'contentTypes' => $allContentType, 'allContent' => $allContent,'allProduct' => $allProduct, 'seocontentinfo' => $seocontentinfo, 'seocontentlistinginfo' => $seocontentlistinginfo,'seostoreinfo' => $seostoreinfo, 'seostaticinfo' => $seostaticinfo, 'seoinfo' => $seoinfo));
        }
    }

    public function actionSeoContentModal() {
        $studio_id = Yii::app()->common->getStudiosId();
        $seoinfo = new SeoInfo();
        $type = 'dynamic-content';
        $pagename = 'content-page';
        if(isset($_REQUEST) && !empty($_REQUEST)){
            for ($i = 0; $i < count($_REQUEST['title']); $i++) {
                $movie_id = $_REQUEST['content'][$i];
                $success = $seoinfo->clearOldSeoInfo($studio_id, $type, $pagename, $movie_id, $content_types_id = 0);
                $seodata = array();
                $seodata = array(
                    'studio_id' => $studio_id,
                    'page' => 'content-page',
                    'movie_id' => $movie_id,
                    'title' => $_REQUEST['title'][$i],
                    'description' => $_REQUEST['description'][$i],
                    'keywords' => $_REQUEST['keywords'][$i]
                );
                $seoinfo->saveSeoInfo($seodata);
            }
        }else{
            $sql = "DELETE FROM seo_info WHERE studio_id = " . $studio_id . " AND (movie_id <> 0 OR movie_id IS NOT NULL)";
            Yii::app()->db->createCommand($sql)->execute();
        }
        
        Yii::app()->user->setFlash('success', 'Seo data updated successfully.');
        $url = $this->createUrl('/admin/seo');
        $this->redirect($url);
    }

    public function actionSeoContentListingModal() {
        $studio_id = Yii::app()->common->getStudiosId();
        $seoinfo = new SeoInfo();
        $type = 'dynamic-content-listing';
        $pagename = 'content-listing-page';
        $success = $seoinfo->clearOldSeoInfo($studio_id, $type, $pagename, $movie_id = 0, $content_types_id = 0);
        for ($i = 0; $i < count($_REQUEST['title']); $i++) {
            $seodata = array();
            $seodata = array(
                'studio_id' => $studio_id,
                'page' => 'content-listing-page',
                'content_types_id' => $_REQUEST['content'][$i],
                'title' => $_REQUEST['title'][$i],
                'description' => $_REQUEST['description'][$i],
                'keywords' => $_REQUEST['keywords'][$i]
            );
            $seoinfo->saveSeoInfo($seodata);
        }
        Yii::app()->user->setFlash('success', 'Seo data updated successfully.');
        $url = $this->createUrl('/admin/seo');
        $this->redirect($url);
    }
    public function actionSeoStoreModal() {
        $studio_id = Yii::app()->common->getStudiosId();
        $seoinfo = new SeoInfo();
        $type = 'dynamic-content';
        $pagename = 'store-page';
        if (isset($_REQUEST) && !empty($_REQUEST)) {
            for ($i = 0; $i < count($_REQUEST['title']); $i++) {
                $movie_id = $_REQUEST['store'][$i];
                $success = $seoinfo->clearOldSeoInfo($studio_id, $type, $pagename, $movie_id, $content_types_id = 0);
                $seodata = array();
                $seodata = array(
                    'studio_id' => $studio_id,
                    'page' => $pagename,
                    'movie_id' => $movie_id,
                    'title' => $_REQUEST['title'][$i],
                    'description' => $_REQUEST['description'][$i],
                    'keywords' => $_REQUEST['keywords'][$i]
                );
                $seoinfo->saveSeoInfo($seodata);
            }
        } else {
            $sql = "DELETE FROM seo_info WHERE studio_id = " . $studio_id . " AND (movie_id <> 0 OR movie_id IS NOT NULL)";
            Yii::app()->db->createCommand($sql)->execute();
        }

        Yii::app()->user->setFlash('success', 'Seo data updated successfully.');
        $url = $this->createUrl('/admin/seo');
        $this->redirect($url);
    }
    public function actionUpdateSeoInfo() {
        $studio_id = Yii::app()->common->getStudiosId();
        $params = array();
        parse_str($_REQUEST['data_from'], $params);
        $seoinfo = new SeoInfo();
        $type = 'page';
        $success = $seoinfo->clearOldSeoInfo($studio_id, $type, $pagename = '', $movie_id = 0, $content_types_id = 0);
        if(isset($_REQUEST['page_name'])){
            $type = @$_REQUEST['page_name'];
            $success = $seoinfo->clearOldSeoInfo($studio_id, $type, $pagename = '', $movie_id = 0, $content_types_id = 0);
        }
        for ($i = 0; $i < count($params['data']['title']); $i++) {
            $seodata = array();
            $seodata = array(
                'studio_id' => $studio_id,
                'page' => $params['data']['pagename'][$i],
                'title' => $params['data']['title'][$i],
                'description' => $params['data']['description'][$i],
                'keywords' => $params['data']['keywords'][$i]
            );
            $seoinfo->saveSeoInfo($seodata);
        }
        $title_footer_page = $params['title_footer_page'];
        $description_footer_page = $params['description_footer_page'];
        $keywords_footer_page = $params['keywords_footer_page'];
        $pagename = $params['pagename'];
        $pageid = $params['pageid'];
        $type = 'static';
        //print_r($pagename);
        for ($i = 0; $i < count($title_footer_page); $i++) {
            $success = $seoinfo->clearOldSeoInfo($studio_id, $type, $pagename[$i], $movie_id = 0, $content_types_id = 0);
            $seodata = array();
            $seodata = array(
                'studio_id' => $studio_id,
                'page' => $pagename[$i],
                'content_types_id' => $pageid[$i],
                'title' => $title_footer_page[$i],
                'description' => $description_footer_page[$i],
                'keywords' => $keywords_footer_page[$i]
            );
            $seoinfo->saveSeoInfo($seodata);
        }
        echo 1;
    }

    public function updateTrailerStartTime($trailer_id, $upload_start_time) {
        $trailer = new movieTrailer();
        $data = $trailer->updateStratTime($trailer_id, $upload_start_time);
        return $data;
    }

    public function updateMovieStartTime($movie_stream_id, $upload_start_time) {
        $movieStream = new movieStreams();
        $data = $movieStream->updateStratTime($movie_stream_id, $upload_start_time);
        return $data;
    }

    /**
     * @method public validateVideo(type $paramName) validate the url and check if file exists
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function actionValidateVideo() {
        $url = @$_REQUEST['url'];
        if ($url) {
            $url = str_replace(' ', '%20', $url);
            $_REQUEST['url'] = $url;
             if((isset($_REQUEST['username']) && trim($_REQUEST['username'])!='') && (isset($_REQUEST['password']) && trim($_REQUEST['password'])!='')){
                $auth['access_username'] = $_REQUEST['username'];
                $auth['access_password'] = $_REQUEST['password'];
            }/*else{
            $tModel = new ThirdpartyServerAccess();
            $authData = $tModel->find('studio_id =:studio_id AND is_authorised = 1', array(':studio_id' => Yii::app()->common->getStudioId()));

            if ($authData) {
                $auth = $authData->attributes;
            }
            }*/
            $arr = $this->is_url_exist($url, @$auth);
            if (@$arr['error'] != 1 && @$arr['is_video']) {
                $data = $_REQUEST;
                $data['content_type'] = $arr['is_video'];
                //$movieStream = new movieStreams();
                //$ret = $movieStream->updateByPk($_REQUEST['movie_stream_id'],array('full_movie_url'=>$_REQUEST['url']));
                $this->createUploadSH($data, @$auth);
            }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public CreateUploadSH() Create the SH file for URL based upload and Upload the SH file to S3 bucket
     * @return boolen true/false
     * @author GDR<support@muvi.com>
     */
    function createUploadSH($data, $auth, $galleryId = 0) {
        $condition = '';
        $videoFolder = 'upload_video';
        $shFolder = 'upload_progress';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        } else if (HOST_IP == '52.0.64.95') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        } 
        $con = Yii::app()->db;
        if ($data['movie_id'] && $data['movie_stream_id']) {
            $sql = "SELECT m.name,m.uniq_id,m.content_types_id,ms.* FROM films m, movie_streams ms WHERE m.id = ms.movie_id AND  ms.id=" . $data['movie_stream_id'];
            $record = $con->createCommand($sql)->queryAll();
            $record = $record[0];
            $bucketInfo = Yii::app()->common->getBucketInfo('', $record['studio_id']);
            $bucketName = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $s3cfg = $bucketInfo['s3cmd_file_name'];
            $contentType = explode('/', $data['content_type']);
            $extenson = $contentType[count($contentType) - 1];
            if ($record['content_types_id'] == 3 && $record['is_episode']) {
                $vFileName = $record['episode_title'] ? $record['episode_title'] : 'Episode_' . $record['episode_number'];
                $movieFileName = preg_replace('/[^a-zA-Z0-9_.]/', '_', $vFileName);
            } else {
                $movieFileName = preg_replace('/[^a-zA-Z0-9_.]/', '_', $record['name']);
            }
            $videoName = $movieFileName . '.' . $extenson;
            $movieID = $record['id'];
            $updateUrl = Yii::app()->getBaseUrl(true) . "/conversion/UpdateUploadInfo?stream_id=" . $data['movie_stream_id'] . "MUVIMUVI" . $record['uniq_id'];
            $urlForVideoNotDownloaded = Yii::app()->getBaseUrl(true) . "/cron/UploadVideoNotDownloaded?stream_id=" . $record['id'] . "MUVIMUVI" . $record['uniq_id'];
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            //$vidoInfo = new SplFileInfo($videoName);
            //$videoExt = $vidoInfo->getExtension();
            $folderPath = Yii::app()->common->getFolderPath("", $record['studio_id']);
            $signedBucketPath = $folderPath['signedFolderPath'];
            $bucketHttpUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieID . '/';
            $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "uploads/movie_stream/full_movie/" . $movieID . "/";

            //$videoUrl = $bucketHttpUrl.$videoName;
            //$videoOnlyName = $vidoInfo->getBasename('.'.$vidoInfo->getExtension());
            $updateUrl .= "MUVIMUVI" . $videoName;
            //Create shell script
            $file = 'uploadSH_' . $movieID .HOST_IP. '.sh';
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
            $cf = 'echo "${file%???}"';
            // Wget URL 
            $username = '';
            $password = '';
            if ($auth) {
                $username = $auth['access_username'];
                $password = $auth['access_password'];
            }
            $gotDir = 'cd /var/www/html/' . $videoFolder . '/' . $movieID;
            if ($username && $password) {
                $data['url'] = str_replace('http://', '', $data['url']);
                $data['url'] = str_replace('www.', '', $data['url']);
                //$username = 'goquestmedia'; $password = 'iu4jyc9ic3yy9x50';
                if (preg_match("~^(?:f)tps?://~i", $data['url'])) {
                    $data['url'] = str_replace('ftp://', '', $data['url']);
                    $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   ftp://' . $username . ':' . $password . '@' . $data['url'];
                }else{
                    $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   http://' . $username . ':' . $password . '@' . $data['url'];
                }
            } else {
                $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   ' . $data['url'];
            }

            $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/" . $movieID . "/" . $videoName . " " . $bucket_url . " \n";
            $checkFileExistIfCondition .= 'if [ -s \'/var/www/html/' . $videoFolder . '/' . $movieID . '/' . $videoName . "' ] \n then \n";
            $checkFileExistElseCondition .="\n\nelse\n\n";

            $checkFileExistElseCondition .= "curl $urlForVideoNotDownloaded\n";
            $file_data = "file=`echo $0`\n" .
                    "cf='" . $file . "'\n\n" .
                    "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                    "then\n\n" .
                    "echo \"$file is running\"\n\n" .
                    "else\n\n" .
                    "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                    "# create directory\n" .
                    "mkdir /var/www/html/$videoFolder/$movieID\n" .
                    "chmod 0777 /var/www/html/$videoFolder/$movieID\n" .
                    "#Go to Dirctory \n" .
                    $gotDir . "\n" .
                    "# wget command\n" .
                    $wgetCMD .
                    "\n" .
                    "\n\n\n# Check all the video file's are created\n" .
                    $checkFileExistIfCondition .
                    "\n# upload to s3\n" .
                    $moveFile .
                    "# update query\n" .
                    "curl $updateUrl\n" .
                    "# remove directory\n" .
                    "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                    "# remove the process copy file\n" .
                    "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                    "# remove the process file\n" .
                    "rm /var/www/html/$shFolder/$file\n" .
                    $checkFileExistElseCondition .
                    "# remove directory\n" .
                    "rm -rf /var/www/html/$videoFolder/$movieID\n" .
                    "# remove the process copy file\n" .
                    "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                    "# remove the process file\n" .
                    "rm /var/www/html/$shFolder/$file\n" .
                    "fi\n\n" .
                    "fi";
            fwrite($handle, $file_data);
            fclose($handle);
            //Uploading conversion script from local to s3 
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file;
            $functionParams = array();
            $functionParams['field_name'] = 's3_upload_folder';
            if($bucketName == 'vimeoassets-singapore'){
                $functionParams['singapore_bucket'] = 1;
            }
            $shDataArray = Yii::app()->aws->getshUploadFolderPath($functionParams);
            $s3folderPath = $shDataArray['shfolder'];
            if ($s3->upload($conversionBucket, $s3folderPath . $file, fopen($filePath, 'rb'), 'public-read')) {
                //echo "Sh file created successfully";
                if (unlink($filePath)) {
                    $epData = movieStreams::model()->findByPk($movieID);
                    $epData->is_download_progress = 1;
                    $epData->is_converted = 0;
                    $epData->is_demo = 0;
                    $epData->full_movie_url = $_REQUEST['url'];
                    $epData->thirdparty_url = '';
                    $epData->upload_start_time = date('Y-m-d H:i:s');
                    $epData->upload_end_time = '';
                    $epData->upload_cancel_time = '';
                    $epData->encoding_start_time = '';
                    $epData->encoding_end_time = '';
                    $epData->encode_fail_time = '';
                    $epData->video_management_id = $galleryId;
                    $return = $epData->save();
                }
            }
        } else {
            return false;
        }
    }

    //Blog Extension        
    public function actionMarketplace() {
        $this->breadcrumbs = array('Marketplace','Manage Apps');
        $this->pageTitle = Yii::app()->name . ' | ' . ' Marketplace ';
        $studio_id = Yii::app()->common->getStudiosId();

        //Find All Blog Post       
        $data = Extension::model()->findAllExtensions();
        $studio_extensions = StudioExtension::model()->findExtensions();
        $active_extensions = array();
        if (!empty($studio_extensions)) {
            foreach ($studio_extensions as $exts)
                $active_extensions[] = $exts->extension_id;
        }
        /* Remove Product Personalization if Muvikart is not enabled */
        foreach ($data as $k=>$v){
            $extarray[$k] = $v['permalink'];
		}
        if(!in_array(3,$active_extensions)){// 3 is muvikart extention id
            $key = array_search('ProductPersonalization', $extarray);
            unset($data[$key]);
        }
        /* END */
        $this->render('extensions', array('data' => $data, 'active_extensions' => $active_extensions));
    }

    public function actionUpdatemarketplace() {
        $this->breadcrumbs = array('Marketplace');
        $this->pageTitle = Yii::app()->name . ' | ' . ' Marketplace ';
        $studio_id = Yii::app()->common->getStudiosId();

        if (isset($_REQUEST['option']) && isset($_REQUEST['extension_id']) && $_REQUEST['extension_id'] > 0 && $_REQUEST['option'] == 'status_change') {
            $ext_id = $_REQUEST['extension_id'];
            unset($_SESSION[$studio_id]["StudioExtensions"]);
            if($ext_id == 2){
              $studio       = $this->studio;
              $theme        = $studio->theme;
              $parent_theme = $studio->parent_theme;
              $destinpath   = ROOT_DIR . 'themes/' . $theme . '/views/faqs/';
              $byod = "";
              if($parent_theme == "byod" || $parent_theme == "physical"){
                  $byod   = "byod/";
                  $jsfile = "byod/js/search.js";
                  $jsroot = ROOT_DIR . 'faq/'.$jsfile;
                  $jspath = ROOT_DIR . 'themes/' . $theme . '/js/search.js';
                  if(!file_exists($jspath)){
                      //fopen($jspath, "w+");
                      copy($jsroot, $jspath);
                  }
              }
              $sourcepath  = ROOT_DIR . 'faq/'.$byod.'faqs';
              if(!file_exists($destinpath)){
                  @mkdir($destinpath, 0777, true);
                  chmod($destinpath, 0777);
                  $ret = Yii::app()->common->recurse_copy($sourcepath, $destinpath);
              }
            }
            $ext = StudioExtension::model()->findByAttributes(
                    array(
                        'studio_id' => $studio_id,
                        'extension_id' => $ext_id,
                    )
            );
            if (count($ext) > 0) {
                $status = ($ext->status == 0) ? 1 : 0;
                $ext->status = $status;
                if ($status == 1)
                    $ext->start_date = new CDbExpression("NOW()");
                $ext->save();
                if(($ext_id == 2) && ($status == 1)){//add default faq
                    $this->InsertDefaultHelp();
                }
                if($ext_id == 3){//physical goods
                    unset($_SESSION['storelink']);
                    //add default shipping method & size
                    if ($status == 1) {
                        $methods = ShippingMethod::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
                        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
                        if (empty($methods)) {
                            $mt = new ShippingMethod;
                            $mt->method = 'Standard';
                            $mt->method_unique_name = 'standard';
                            $mt->studio_id = $studio_id;
                            $mt->is_enabled = 1;
                            $mt->save();
                        }
                        if (empty($sizes)) {
                            $sz = new ShippingSize;
                            $sz->size = 'Small';
                            $sz->size_unique_name = 'small';
                            $sz->studio_id = $studio_id;
                            $sz->save();
                        }
                    }
                }
				if (isset($_SESSION[$studio_id]['productizeflag'])) {unset($_SESSION[$studio_id]['productizeflag']);}
            }
            else {
                $ext = new StudioExtension();
                $ext->extension_id = $ext_id;
                $ext->studio_id = $studio_id;
                $ext->social_sharing_status = 1;
                $ext->comment_status = 1;
                $ext->status = 1;
                $ext->start_date = new CDbExpression("NOW()");
                $ext->save();
                if($ext_id == 2){//add default faq
                    $this->InsertDefaultHelp();
                }
                if ($ext_id == 3) {
                    $methods = ShippingMethod::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
                    $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
                    if (empty($methods)) {
                        $mt = new ShippingMethod;
                        $mt->method = 'Standard';
                        $mt->method_unique_name = 'standard';
                        $mt->studio_id = $studio_id;
                        $mt->is_enabled = 1;
                        $mt->save();
                    }
                    if (empty($sizes)) {
                        $sz = new ShippingSize;
                        $sz->size = 'Small';
                        $sz->size_unique_name = 'small';
                        $sz->studio_id = $studio_id;
                        $sz->save();
                    }
                }
            }
            Yii::app()->user->setFlash('success', "Extension status updated successfully.");
        }
        $return = array('res' => 1);
        echo json_encode($return);
        exit();
    }

    public function actionBlog() {
        $this->breadcrumbs = array('Marketplace', 'Blog');
        $this->headerinfo = "Blog";
        $this->pageTitle = Yii::app()->name . ' | ' . ' Blog ';
        $studio_id = Yii::app()->common->getStudiosId();
        $has_blog = Yii::app()->common->IsBlogAvailable($studio_id);
        if ($has_blog > 0) {
            if (@IS_LANGUAGE == 1) {
                $language_id = $this->language_id;
            } else {
                $language_id = 0;
            }
            
            if (isset($_REQUEST['option']) && isset($_REQUEST['post_id']) && $_REQUEST['post_id'] > 0) {
                $post_id = $_REQUEST['post_id'];
                if ($_REQUEST['option'] == 'delete') {
                    if (@IS_LANGUAGE == 1) {
                        $post = BlogPost::model()->find('id=:post_id AND studio_id=:studio_id AND language_id=:language_id', array(':post_id' => $post_id, ':studio_id' => $studio_id, ':language_id' => $language_id));
                        if ($post->parent_id > 0) {
                            $post->delete();
                        } else {
                            BlogPost::model()->deleteAll('studio_id = :studio_id AND (id = :id OR parent_id = :parent_id)', array(
                                ':studio_id' => $studio_id,
                                ':id' => $post_id,
                                ':parent_id' => $post_id,
                            ));
                        }
                    } else {
                        $post = BlogPost :: model()->findByPk($post_id);
                        $post->delete();
                    }
                    
                    Yii::app()->user->setFlash('success', "Blog post deleted successfully.");
                    $url = $this->createUrl("/admin/blog");
                    $this->redirect($url);
                    exit();
                } else if ($_REQUEST['option'] == 'status_change') {
                    if (@IS_LANGUAGE == 1) {
                        $post = BlogPost::model()->find('id=:post_id AND studio_id=:studio_id AND language_id=:language_id', array(':post_id' => $post_id, ':studio_id' => $studio_id, ':language_id' => $language_id));
                        $new_status = ($post->has_published == 0) ? 1 : 0;
                        if ($post->parent_id > 0) {
                            $post->has_published = $new_status;
                            $post->last_updated_by = Yii::app()->user->id;
                            $post->last_updated_date = new CDbExpression("NOW()");
                            $post->save();
                        } else {
                            $blog['has_published'] = $new_status;
                            $blog['last_updated_by'] = Yii::app()->user->id;
                            $blog['last_updated_date'] = new CDbExpression("NOW()");

                            BlogPost::model()->updateAll($blog,'studio_id='.$studio_id.' AND (id='.$post_id.' OR parent_id='.$post_id.')');
                        }
                    } else {
                        $post = BlogPost :: model()->findByPk($post_id);
                        $new_status = ($post->has_published == 0) ? 1 : 0;
                        $post->has_published = $new_status;
                        $post->last_updated_by = Yii::app()->user->id;
                        $post->last_updated_date = new CDbExpression("NOW()");
                        $post->save();
                    }

                    Yii::app()->user->setFlash('success', "Blog post status changed successfully.");
                    $url = $this->createUrl("/admin/blog");
                    $this->redirect($url);
                    exit();
                }
            } else {
                $full_page_path = Yii::app()->common->curPageURL();

                $sortby = (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != '') ? $_REQUEST['sortby'] : 'created_date';
                $order = (isset($_REQUEST['order']) && $_REQUEST['order'] != '') ? $_REQUEST['order'] : 'desc';
                $page_size = (isset($_REQUEST['page_size']) && $_REQUEST['page_size'] != '') ? $_REQUEST['page_size'] : 5;

                //Pagination Implimented ..
                $page_size = $page_size;
                $offset = 0;
                if (isset($_REQUEST['page'])) {
                    $offset = ($_REQUEST['page'] - 1) * $page_size;
                }
                
                if (@IS_LANGUAGE == 1) {
                    $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT * FROM blog_posts WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM blog_posts WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY {$sortby} {$order} LIMIT {$offset},{$page_size}";
                } else {
                    $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT * FROM blog_posts WHERE studio_id={$studio_id} ORDER BY {$sortby} {$order} LIMIT {$offset},{$page_size}";
                }
                
                $dbcon = Yii::app()->db;
                $data = $dbcon->createCommand($sql)->queryAll();

                $count = $dbcon->createCommand('SELECT FOUND_ROWS() AS count')->queryAll();
                $count = (isset($count[0]['count'])) ? $count[0]['count'] : 0;
                
                $pages = new CPagination($count);
                $pages->setPageSize($page_size);
                $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
                $sample = range($pages->offset + 1, $end);
                $this->render('blog', array('data' => $data, 'language_id'=>$language_id, 'searchText' => @$searchData['search_text'], 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sortby' => @$sortby, 'order' => @$order, 'full_page_path' => $full_page_path));
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }

    public function actionBlogcontent() {
        $studio_id = Yii::app()->common->getStudiosId();
        $has_blog = Yii::app()->common->IsBlogAvailable($studio_id);
        if ($has_blog > 0) {
            $this->breadcrumbs = array('Marketplace','Blog'=>array('admin/blog'), 'Add/Update Blog');
            $this->headerinfo = "Add/Update Blog";
            if (isset($_REQUEST['post_id']) && $_REQUEST['post_id'] > 0) {
                $title = 'Edit Post';
                $post_id = $_REQUEST['post_id'];
                
                if (@IS_LANGUAGE == 1) {
                    $post = BlogPost::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $this->language_id, 'id' => $post_id));
                } else {
                    $post = BlogPost :: model()->findByPk($post_id);
                }
            } else {
                $post = array();
                $title = 'Add Blog Post';
            }
            $studio_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'blog_image_dimension');
            if(empty($studio_config)){
                $studio_config = StudioConfig::model()->getconfigvalueForStudio(0,'blog_image_dimension');
            }
            $blog_dimension = explode('x',$studio_config['config_value']);
            $this->pageTitle = $title;
            $this->render('blogcontent', array('page_title' => $title, 'post' => $post, 'parent_id' => $post_id,'blog_dimension'=>$blog_dimension));
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }

    public function actionAddpost() {
        $studio_id = Yii::app()->common->getStudiosId();
        $has_blog = Yii::app()->common->IsBlogAvailable($studio_id);
        if ($has_blog > 0) {
            $this->breadcrumbs = array('Marketplace','Blog', 'Add Blog');
            $this->headerinfo = 'Add Blog';
            if (isset($_REQUEST['post_title'])) {
                if (isset($_REQUEST['post_id']) && $_REQUEST['post_id'] > 0) {
                    $post = BlogPost :: model()->findByPk($_REQUEST['post_id']);
                    if ($studio_id == $post->studio_id) {
                        $status = (isset($_REQUEST['status']) && $_REQUEST['status'] == 'save_publish') ? 1 : 0;
                        $post->post_title = Yii::app()->common->encode_to_html($_POST['post_title']);
                        $post->permalink = htmlspecialchars($_POST['permalink']);
                        if($_POST['author'] != $this->studio->name){
                            $post->author = $_POST['author'];
                        }
                        $post->post_short_content = Yii::app()->common->encode_to_html($_POST['post_short_content']);
                        $post->post_content = Yii::app()->common->encode_to_html($_POST['post_content']);
                        $post->last_updated_by = Yii::app()->user->id;
                        $post->last_updated_date = new CDbExpression("NOW()");
                        $post->has_published = $status;
                        $post->save();
                        $post_id = $_REQUEST['post_id'];
                        if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                            $this->uploadFeaturedImage($post_id,$studio_id);
                        }
                        Yii::app()->user->setFlash('success', "Blog post saved successfully.");
                        $url = $this->createUrl("/admin/blog");
                        $this->redirect($url);
                        exit();
                    } else {
                        Yii::app()->user->setFlash('error', "You don't have access to this page.");
                        $url = $this->createUrl("/admin/blog");
                        $this->redirect($url);
                        exit();
                    }
                } else {
                    if (@IS_LANGUAGE == 1) {
                        $language_id = $this->language_id;
                        $parent_id = (isset($_REQUEST['parent_id']) && trim($_REQUEST['parent_id'])) ? trim($_REQUEST['parent_id']) : 0;

                        if (intval($parent_id)) {
                            $blogModel = BlogPost::model()->findByPk($parent_id);
                            $permalink = $blogModel->permalink;
                        } else {
                            $permalink = htmlspecialchars($_POST['permalink']);
                        }
                    } else {
                        $permalink = htmlspecialchars($_POST['permalink']);
                    }
                    
                    $status = (isset($_REQUEST['status']) && $_REQUEST['status'] == 'save_publish') ? 1 : 0;
                    $post = new BlogPost();
                    $post->category_id = 1;
                    $post->studio_id = $studio_id;
                    
                    if (@IS_LANGUAGE == 1) {
                        $post->language_id = $language_id;
                        $post->parent_id = $parent_id;
                    }
                    $post->post_title = htmlspecialchars($_POST['post_title']);
                    $post->permalink = $permalink;
                    $post->permalink = htmlspecialchars($_POST['permalink']);
                    if($_POST['author'] != $this->studio->name){
                        $post->author = $_POST['author'];
                    }
                    $post->post_short_content = Yii::app()->common->encode_to_html($_POST['post_short_content']);
                    $post->post_content = Yii::app()->common->encode_to_html($_POST['post_content']);
                    $post->created_by = Yii::app()->user->id;
                    $post->created_date = new CDbExpression("NOW()");
                    $post->last_updated_date = new CDbExpression("NOW()");
                    $post->has_published = $status;
                    $post->save();
                    $post_id = $post->id;
                    if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                        $this->uploadFeaturedImage($post_id,$studio_id);
                    }
                    Yii::app()->user->setFlash('success', "Blog post saved successfully.");
                    $url = $this->createUrl("/admin/blog");
                    $this->redirect($url);
                    exit();
                }
            }
            $url = $this->createUrl("/admin/blog");
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }
    public function uploadFeaturedImage($post_id,$studio_id){
        $upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . "images/public/system/featured_image/" . $studio_id . "/";
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
            chmod($upload_dir, 0777);
        }
        $upload_dir1 = $upload_dir."original";
        if (!is_dir($upload_dir1)) {
            mkdir($upload_dir1, 0777, true);
            chmod($upload_dir1, 0777);
        }
        $theme_folder = "";
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
        $bucket = $bucketInfo['bucket_name']; 
        $width  = $_POST['img_width'];
        $height = $_POST['img_height'];
        $cropDimension = array('original' => $width . 'x' . $height,'thumb' => '64x64' );
        $file_path = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, "system/featured_image/" . $studio_id . "/original", $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'],false,'featured_image');
        if($file_path !=''){
            $file_name = explode('original/',$file_path);
            $feat_img =  $file_name[1];
            $post = BlogPost :: model()->findByPk($post_id);
            $post->featured_image = $feat_img;
            $post->save();
        }
    }
    public function actionBlogcomment() {
        $this->breadcrumbs = array('Marketplace','Blog','Blog Comment');
        $this->headerinfo = 'Blog Comment';
        $this->pageTitle = 'Blog Comment';
        $studio_id = Yii::app()->common->getStudiosId();
        $has_blog = Yii::app()->common->IsBlogAvailable($studio_id);
        $post_id = (isset($_REQUEST['post_id']) && $_REQUEST['post_id'] > 0) ? (int) $_REQUEST['post_id'] : 0;
        if ($has_blog > 0 && $post_id > 0) {
            if (isset($_REQUEST['option']) && isset($_REQUEST['comment_id']) && $_REQUEST['comment_id'] > 0) {
                $comment_id = $_REQUEST['comment_id'];
                if ($_REQUEST['option'] == 'edit') {
                    $comment = BlogComment :: model()->findByPk($comment_id);
                    $this->render('addblogcomment', array('comment' => $comment));
                }
            } else {
                $full_page_path = Yii::app()->common->curPageURL();

                $sortby = (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != '') ? $_REQUEST['sortby'] : 'created_date';
                $order = (isset($_REQUEST['order']) && $_REQUEST['order'] != '') ? $_REQUEST['order'] : 'desc';
                $page_size = (isset($_REQUEST['page_size']) && $_REQUEST['page_size'] != '') ? $_REQUEST['page_size'] : 20;

                //Find All Blog Post       
                $data = BlogComment::model()->findAllComments($post_id);
                $count = count($data);

                //Pagination Implimented ..
                $page_size = $page_size;
                $offset = 0;
                if (isset($_REQUEST['page'])) {
                    $offset = ($_REQUEST['page'] - 1) * $page_size;
                }

                $data = BlogComment::model()->findAllByAttributes(
                        array(
                    'studio_id' => $studio_id,
                    'post_id' => $post_id,
                        ), array(
                    'order' => $sortby . ' ' . $order,
                    'limit' => $page_size,
                    'offset' => $offset
                        )
                );

                $pages = new CPagination($count);
                $pages->setPageSize($page_size);
                $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
                $sample = range($pages->offset + 1, $end);
                $this->render('blogcomment', array('data' => $data, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sortby' => @$sortby, 'order' => @$order, 'full_page_path' => $full_page_path));
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }

    public function actionManageblogcomment() {
        $this->breadcrumbs = array('Marketplace','Blog','Blog Comment');
        $this->headerinfo = 'Blog Comment';
        $this->pageTitle = 'Blog Comment';
        $studio_id = Yii::app()->common->getStudiosId();
        $has_blog = Yii::app()->common->IsBlogAvailable($studio_id);
        $res = 0;
        if ($has_blog > 0) {
            if (isset($_REQUEST['option']) && isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                $comment_id = $_REQUEST['id'];
                if ($_REQUEST['option'] == 'delete') {
                    $post = BlogComment :: model()->findByPk($comment_id);
                    if ($studio_id == $post->studio_id) {
                        $post->delete();

                        Yii::app()->user->setFlash('success', "Blog comment deleted successfully.");
                        $res = 1;
                    }
                } else if ($_REQUEST['option'] == 'status') {
                    $post = BlogComment :: model()->findByPk($comment_id);
                    if ($studio_id == $post->studio_id) {
                        $new_status = ($post->status == 0) ? 1 : 0;
                        $post->status = $new_status;
                        $post->save();
                        Yii::app()->user->setFlash('success', "Blog comment status updated successfully.");
                        $res = 1;
                    }
                } else if ($_REQUEST['option'] == 'update') {
                    $comment = BlogComment :: model()->findByPk($comment_id);
                    if ($studio_id == $comment->studio_id) {
                        $new_status = ($comment->status == 0) ? 1 : 0;
                        $comment->comment = htmlspecialchars($_POST['comment']);
                        $comment->status = $new_status;
                        $comment->save();
                        Yii::app()->user->setFlash('success', "Blog comment status updated successfully.");
                        $res = 1;
                    }
                }
            }

            $return = array('res' => $res);
            echo json_encode($return);
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }
    public function actionFaq(){
        $this->breadcrumbs = array('Marketplace', 'Help Center');
        $this->headerinfo = "Help Center";
        $this->pageTitle   = Yii::app()->name . ' | ' . ' Help Center ';
        $studio_id  = Yii::app()->common->getStudiosId();
        $has_faqs   = Yii::app()->common->IsFAQsAvailable($studio_id);
        $faq_id    = isset($_REQUEST['faq_id']) ? $_REQUEST['faq_id'] : 0;
        if($has_faqs > 0) {
            if($faq_id > 0){
                if(isset($_REQUEST['option']) && $_REQUEST['option'] == "delete"){
                    $faq = Faq :: model()->findByPk($faq_id);
                    if ($studio_id == $faq->studio_id) {
                        $faq->delete();
                        Yii::app()->user->setFlash('success', "Help Center content deleted successfully.");
                    }
                    $url = $this->createUrl("/admin/faq");
                    $this->redirect($url);
                }
            }
            $sortby    = (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != '') ? $_REQUEST['sortby'] : 'id_seq';
            $order     = (isset($_REQUEST['order']) && $_REQUEST['order'] != '') ? $_REQUEST['order'] : 'asc';
            $page_size = (isset($_REQUEST['page_size']) && $_REQUEST['page_size'] != '') ? $_REQUEST['page_size'] : 10;
            $offset    = 0;
            if (isset($_REQUEST['page'])) {
               $offset = ($_REQUEST['page'] - 1) * $page_size;
            }
            $data      = Faq::model()->findAllByAttributes(array('studio_id' => $studio_id), array( 'order' => $sortby . ' ' . $order)); 
            /*$count     = Faq::model()->count( 'studio_id=:studio_id', array(':studio_id' => $studio_id));
            $pages     = new CPagination($count);
            $pages->setPageSize($page_size);
            $end       = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
            $sample    = range($pages->offset + 1, $end);
            $this->render('faq',array('data'=>$data, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sortby' => @$sortby, 'order' => @$order));*/
            $this->render('faq',array('data'=>$data));
        }else {
            Yii::app()->user->setFlash('error', "You don't have access to Help Center page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
        
    }
    public function actionAddFaq(){
        $this->breadcrumbs = array('Marketplace', 'Help Center'=>array('admin/faq'),'Add New Help');
        $this->headerinfo = "Add New Help";
        $this->pageTitle   = Yii::app()->name . ' | ' . 'Help Center';
        $studio_id = Yii::app()->common->getStudiosId();
        $has_faqs  = Yii::app()->common->IsFAQsAvailable($studio_id);
        if($has_faqs > 0) {
            if(isset($_POST['title'])){
                $faq = new Faq();
                $faq->studio_id  = $studio_id;
                $faq->title      = Yii::app()->common->encode_to_html($_POST['title']);
                $faq->content    = Yii::app()->common->encode_to_html($_POST['content']);
                $faq->date_added = date('Y-m-d h:i:s');
                $faq->save();
                Yii::app()->user->setFlash('success', "Help Center content added successfully.");
                $url = $this->createUrl("/admin/faq");
                $this->redirect($url);
            }else{
                $this->render('faqcontent');
            }
        }else {
            Yii::app()->user->setFlash('error', "You don't have access to Help Center page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }
    public function actionEditFaq(){
        $this->breadcrumbs = array('Marketplace', 'Help Center'=>array('admin/faq'),'Update Help');
        $this->headerinfo = "Update Help";
        $this->pageTitle   = Yii::app()->name . ' | ' . 'Help Center';
        $studio_id = Yii::app()->common->getStudiosId();
        $has_faqs  = Yii::app()->common->IsFAQsAvailable($studio_id);
        $faq_id    = isset($_REQUEST['faq_id']) ? $_REQUEST['faq_id'] : 0;
        if($has_faqs > 0) {
            if(isset($_POST['title'])){
                $faq = Faq :: model()->findByPk($faq_id);
                if ($studio_id == $faq->studio_id) {
                    $faq->studio_id  = $studio_id;
                    $faq->title      = Yii::app()->common->encode_to_html($_POST['title']);
                    $faq->content    = Yii::app()->common->encode_to_html($_POST['content']);
                    $faq->save();
                    Yii::app()->user->setFlash('success', "Help Center content updated successfully.");
                }
                $url = $this->createUrl("/admin/faq");
                $this->redirect($url);
            }else{
                $faq        = new Faq();
                $faqcontent = $faq->findByPk($faq_id);
                $this->render('faqcontent',array('faq'=>$faqcontent));
            }
        }else {
            Yii::app()->user->setFlash('error', "You don't have access to Help Center page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
        }
    }
    public function actionreorderHelpCenterontent(){
        $pos = 0;
        $orders = explode('&', $_REQUEST['orders']);
        foreach ($orders as $key => $item) {
            $item = explode('=', $item);
            $array[] = $item[1];
            $faq = Faq::model()->findByPk($item[1]);
            $faq_id = $faq->id;
            $pos = $key + 1;
            $attr= array('id_seq'=>$pos);
            $condition = "id=:id";
            $params = array(':id'=>$faq_id);
            $feat = Faq::model()->updateAll($attr,$condition,$params);
        }
        echo "success"; 
        exit;
    }
    public function actionSendTestEmailNoti() {
       // require('MadMimi.class.php');
        $item_detail = array();
        $this->layout = false;
        $user_model = new User();
       // $users = $user_model->get_users($_REQUEST['send_to']);
        $items = explode(',', @$_REQUEST['content_id']);
        $studio = $this->studio;

        $admin = $user_model->findAllByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'is_sdk' => 1, 'role_id' => 1));
        if (count($admin) > 0) {
            $toEmail = $admin[0]->email;
        } else {
            $toEmail = 'info@' . $studio->domain;
        }
        $display_name = $admin[0]->first_name . ' ' . $admin[0]->last_name;
        $dbcon = Yii::app()->db;
        $studio_sql = "SELECT domain FROM studios WHERE id = '" . Yii::app()->common->getStudiosId() . "' LIMIT 1";
        $user_data = $dbcon->createCommand($studio_sql)->queryAll();

        array_pop($items);
        $items = array_unique($items);
        foreach ($items AS $key => $val) {
            $ms_data = movieStreams::model()->findByPk($val);
            $cdata = $this->getMovieDetails($ms_data['movie_id']);
            $data = json_decode($cdata, true);
            $data[0]['movie_stream_id'] = $ms_data['id'];
            $data[0]['is_episode'] = $ms_data['is_episode'];
            if ($ms_data['episode_title'] != '') {
                $data[0]['name'] .= ' - ' . $ms_data['episode_title'];
                $data[0]['story'] = $ms_data['episode_story'];
            }
            array_push($item_detail, $data[0]);
        }

        ini_set('max_execution_time', 500);
        
        //$mimi = new MadMimi('info@muvi.com', '5c43f29037c4c5c696b7306b1832dcb2');
        /*$options = array(
            'promotion_name' => 'Test_Email_Notification',
            'recipients' => $display_name . ' <' . $from_mail . '>',
            'subject' => 'Test_Email_Notification < subject: ' . $_REQUEST['subject'] . '>',
            'from' => $studio->name . " <$from_mail>"
        );*/
       // $mimi->SendHTML($options, $html_body);
        
		$subject = 'Test_Email_Notification < subject: ' . $_REQUEST['subject'] . '>';
		$fromEmail = "info@muvi.com";
		$template_name = 'notification_template_test_mail';
		$html = $this->renderPartial('notification_template_test_mail', array('item_detail' => $item_detail, 'display_name' => $display_name, 'mail_content' => $_REQUEST['msg'], 'studio' => $studio, 'user_data' => $user_data), true);
        $html_body = "<html><head><title>" . $_REQUEST['subject'] . "</title></head><body>" . $html . "<p style='display: none;'>[[tracking_beacon]]</p></body></html>";
		$retval = $this->sendmailViaAmazonsdk($html_body,$subject,$toEmail,$fromEmail); 
		/*$fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
		fwrite($fp, '\n\t Test mail Logs on dt:-' . date('m-d-Y H:i:s') .'\t '.'Studio_id='.$this->studio->id. "\n\t " . $subject."==To=".$toEmail."=From:=".$fromEmail.'\t\n '. $html_body);
		fclose($fp);*/
		if($retval){
			echo 1;exit;
		}else{
			echo 0;exit;
		}
    }

    function actionRemoveTopBanner() {
        $poster = new Poster();
        //Check the movie id blongs from the logged in users studio
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $movie_id = $_REQUEST['movie_id'];
        }
        if ($movie_id) {
            $movie = Film::model()->find('id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->common->getStudiosId()));
            if ($movie) {
                $ret = Yii::app()->db->createCommand()
                        ->delete('posters', 'object_id=:movie_id AND object_type=:type', array(':movie_id' => $movie_id, ':type' => 'topbanner'));

                if ($ret) {
                    $arr['msg'] = 'Banner removed successfully.';
                    $arr['err'] = 0;
                } else {
                    $arr['msg'] = 'Error in deleting banner.';
                    $arr['err'] = 1;
                }
            } else {
                $arr['msg'] = 'You are not authorised to delete this banner';
                $arr['err'] = 1;
            }
        } else {
            $arr['msg'] = 'Invalid banner';
            $arr['err'] = 1;
        }
        if ($_REQUEST['is_ajax']) {
            echo json_encode($arr);
            exit;
        }
    }

    //Added to remove the poster
    function actionRemoveposter() {
        $poster = new Poster();
        //Check the movie id blongs from the logged in users studio
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $movie_id = $_REQUEST['movie_id'];
        }
        $obj_type = $_REQUEST['obj_type'];
        $movie_stream_id = $_REQUEST['movie_stream_id'];

        if ($movie_id) {
            $movie = Film::model()->find('id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->common->getStudiosId()));
            if ($movie) {
                if ($obj_type != 1) {
                    $ret = Yii::app()->db->createCommand()
                            ->delete('posters', 'object_id=:movie_id AND object_type=:type', array(':movie_id' => $movie_id, ':type' => 'films'));
                } else {
                    $ret = Yii::app()->db->createCommand()
                            ->delete('posters', 'object_id=:movie_id AND object_type=:type', array(':movie_id' => $movie_stream_id, ':type' => 'moviestream'));
                }
                if ($ret) {
                    $arr['msg'] = 'Poster removed successfully.';
                    $arr['err'] = 0;
                } else {
                    $arr['msg'] = 'Error in deleting Poster.';
                    $arr['err'] = 1;
                }
            } else {
                $arr['msg'] = 'You are not authorised to delete this Poster';
                $arr['err'] = 1;
            }
        } else {
            $arr['msg'] = 'Invalid Poster';
            $arr['err'] = 1;
        }
        if ($_REQUEST['is_ajax']) {
            echo json_encode($arr);
            exit;
        }
    }

    public function actionEmailNotiImageUpload() {
        $res = array();
        $studio_id = Yii::app()->common->getStudioId();
        $agent = Yii::app()->common->getAgent();
        $std = new Studio();
        $studio = $std->findByPk($studio_id);

        if (!$_FILES['file']['error']) {
            $temp = explode(".", $_FILES["file"]["name"]);
            $newfilename = reset($temp) . '_' . round(microtime(true)) . '.' . end($temp);
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/email_noti_images/' . $studio_id . '/';
            if (!file_exists($dir)) {
                mkdir($dir, 0775, true);
            }
            $upload_file_dir = $dir . $newfilename;
            $return_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/email_noti_images/';
            $path = $this->uploadEmailNotiImages($newfilename, $_FILES['file'], $studio_id, $return_dir);
            $res['error'] = FALSE;
            $res['path'] = $path['email_noti_images'];
            echo json_encode($res);
        }
    }
	/*
    * modified :   Arvind 
    * email    :   aravind@muvi.com
    * reason   :   Audio Gallery :  
    * functionality : actionaddAudioFromAudioGallery
    * date     :   02-1-2017
    */
   public function actionaddAudioFromAudioGallery() {
		$ip_address = CHttpRequest::getUserHostAddress();
		$geo_loc = Yii::app()->common->getVisitorLocation($ip_address);
		if (isset($_REQUEST['url']) && $_REQUEST['galleryId']) {
			$studioId = Yii::app()->common->getStudioId();
			$galleryId = @$_REQUEST['galleryId'];
			$movieStreamId = @$_REQUEST['movie_stream_id'];
			$file_name = @$_REQUEST['audio_name'];

			if ($file_name != '' && intval($galleryId)) {
				if (intval($movieStreamId) && $file_name != null && intval($galleryId)) {
					/*
					 * Mapping Audio gallery 
					 */
					$s3 = Yii::app()->common->connectToAwsS3($studioId);
					$bucketInfo = Yii::app()->common->getBucketInfo("", $studioId);
					$bucket = $bucketInfo['bucket_name'];
					$s3url = $bucketInfo['s3url'];
					$s3cfg = $bucketInfo['s3cmd_file_name'];
					$folderPath = Yii::app()->common->getFolderPath("", $studioId);
					$signedBucketPath = $folderPath['signedFolderPath'];
					$unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
					$key = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieStreamId . '/';
					if ($movieStreamId) {
						$new_cdn_user = Yii::app()->user->new_cdn_users;
						$responseDelete = $s3->getListObjectsIterator(array(
							'Bucket' => $bucket,
							'Prefix' => $key
						));
						foreach ($responseDelete as $object) {
							$s3->deleteObject(array(
								'Bucket' => $bucket,
								'Key' => $object['Key']
							));
						}
						$fullPath = 'http://' . $bucket . '.' . $s3url . '/' . $unsignedFolderPathForVideo . 'audiogallery/' . $file_name;
						if ($new_cdn_user == 0) {//existing user
							$fullPath = 'http://' . $bucket . '.' . $s3url . '/' . $unsignedFolderPathForVideo . 'audiogallery/' . $studioId . '/' . $file_name;
						}
						$key = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieStreamId . '/' . $file_name;
						/*
						 * Copy s3 audio object
						 */
						$response = $s3->copyObject(array(
							'Bucket' => $bucket,
							'Key' => $key, //destination
							'CopySource' => $fullPath, //source 
							'ACL' => 'public-read',
						));

						/*
						 * update respective movie_stream table part of code after audio file copy
						 */
						$dbcon = Yii::app()->db;
						$getMovieStream = $dbcon->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,f.content_types_id,m.original_file,m.converted_file  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' . $_REQUEST['movie_id'] . ' AND m.studio_id=' . Yii::app()->user->studio_id . ' AND m.id=' . $_REQUEST['movie_stream_id'])->queryAll();
						$original = explode('-', $getMovieStream[0]['original_file']);
						$converted = explode('-', $getMovieStream[0]['converted_file']);
						$storage = new StudioStorage();
						$resArray = StudioStorage::model()->findByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'file_type' => 'movie'));
						$ip_address = CHttpRequest::getUserHostAddress();
						$geo_loc = Yii::app()->common->getVisitorLocation($ip_address);

						if (isset($resArray) && !empty($resArray)) {
							$data['studio_id'] = Yii::app()->common->getStudiosId();
							$data['original_video_file_count'] = $resArray->original_video_file_count - $original[0];
							$data['original_file_size'] = $resArray->original_file_size - $original[1];
							$data['converted_video_file_count'] = $resArray->converted_video_file_count - $converted[0];
							$data['converted_file_size'] = $resArray->converted_file_size - $converted[1];
							$data['total_file_size'] = $resArray->total_file_size - ($original[1] + $converted[1]);
							$data['file_type'] = 'movie';
							$data['last_updated_date'] = date('Y-m-d H:i:s');
							$storage->updateLog($resArray->id, $data);
						}
						/*
						 * movie_stream table update as per 
						 * audio gallery ID
						 */
						//$ext = pathinfo($_REQUEST['filename'], PATHINFO_EXTENSION);
						$upload_end_time = date('Y-m-d H:i:s');
						$moveStream = movieStreams::model()->findByPk($_REQUEST['movie_stream_id']);
						$moveStream->full_movie = $file_name;
						$moveStream->thirdparty_url = '';
						$moveStream->wiki_data = '';
						$moveStream->is_converted = 1;
						$moveStream->has_sh = 0;
						$moveStream->last_updated_by = Yii::app()->user->id;
						$moveStream->last_updated_date = gmdate('Y-m-d');
						$moveStream->upload_date = gmdate('Y-m-d');
						$moveStream->video_resolution = '';
						$moveStream->mail_sent_for_video = 0;
						$moveStream->upload_end_time = $upload_end_time;
						$moveStream->is_demo = '0';
						$moveStream->video_management_id = 0;
//						$moveStream->method = 'browse';
//						$moveStream->geography = $geo_loc['country_name'];
//						$moveStream->customer_internet_speed = $_COOKIE['internetSpeed'];
						$moveStream->video_management_id = $galleryId;
						$moveStream->save();
						$ret = 1;
						if ($ret) {
							$arr = array('error' => 0, 'is_audio' => 'uploaded');
						} else {
							$arr = array('error' => 1, 'is_audio' => 'uploaded');
						}
					}
				}
			}
			echo json_encode($arr);
			exit;
		}
	}

	public function actionaddVideoFromVideoGallery() {
        if (isset($_REQUEST['url']) && $_REQUEST['galleryId']) {
            $studioId = Yii::app()->common->getStudioId();
            $url = @$_REQUEST['url'];
            $galleryId = @$_REQUEST['galleryId'];
            $movieStreamId = @$_REQUEST['movie_stream_id'];
            if ($url != '' && intval($galleryId)) {
                $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studioId,'drm_enable');
                $drmEnabled = 0;
                if($getStudioConfig){
                    if(@$getStudioConfig['config_value'] == 1){
                        $drmEnabled = 1;
                    }
                }
                $movieStreams = movieStreams::model()->FindByAttributes(array('video_management_id' => $galleryId, 'is_converted' => 1));
                if($drmEnabled == 1 && ($movieStreams->encryption_key =='' || $movieStreams->content_key =='')){
                    $movieStreams = "";
                } else if($drmEnabled == 0 && ($movieStreams->encryption_key !='' || $movieStreams->content_key !='')){
                    $movieStreams = "";
                }
                $trailer = 0;
                if ((empty($movieStreams)) && ($drmEnabled == 0)) {
                    $movieStreams = movieTrailer::model()->FindByAttributes(array('video_management_id' => $galleryId, 'is_converted' => 1));
                    $trailer = 1;
                }
                if ($movieStreams) {
                    $s3 = Yii::app()->common->connectToAwsS3($studioId);

                    $bucketInfo = Yii::app()->common->getBucketInfo("", $studioId);
                    $bucketName = $bucketInfo['bucket_name'];
                    $s3cfg = $bucketInfo['s3cmd_file_name'];
                    $folderPath = Yii::app()->common->getFolderPath("", $studioId);
                    $signedBucketPath = $folderPath['signedFolderPath'];
                    $s3prefix = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieStreams->id . '/';
                    if ($trailer) {
                        $s3prefix = $signedBucketPath . 'uploads/trailers/' . $movieStreams->id . '/';
                    }
                    $key = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieStreamId . '/';
                    $i =0;
                    if ($movieStreams->id != $movieStreamId) {
                        $responseDelete = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucketName,
                            'Prefix' => $key
                        ));
                        foreach ($responseDelete as $object) {
                            $s3->deleteObject(array(
                                'Bucket' => $bucketName,
                                'Key' => $object['Key']
                            ));
                        }
                        //Sh File Creation
                        $file = date("Y_m_d_H_i_s") . '_' . $movieStreamId .HOST_IP. '.sh';
                        
                        $functionParams = array();
                        $functionParams['field_name'] = 's3_upload_folder';
                        if($bucketName == 'vimeoassets-singapore'){
                            $functionParams['singapore_bucket'] = 1;
                        }
                        $shDataArray = Yii::app()->aws->getshUploadFolderPath($functionParams);
                        $s3folderPath = $shDataArray['shfolder'];
                        $videoFolder = 'upload_video';
                        $shFolder = 'upload_progress';
                        $conversionBucket = 'muvistudio';
                        if (HOST_IP == '127.0.0.1') {
                            $videoFolder = 'staging/upload_video';
                            $shFolder = 'staging/upload_progress';
                            $conversionBucket = 'stagingstudio';
                        } else if (HOST_IP == '52.0.64.95') {
                            $videoFolder = 'staging/upload_video';
                            $shFolder = 'staging/upload_progress';
                            $conversionBucket = 'stagingstudio';
                        }
                        
                        $movieOriginPath = "s3://".$bucketName."/".$key;
                        $movieCopyPath = "s3://".$bucketName."/".$s3prefix;
                        $updateUrl = Yii::app()->getBaseUrl(true)."/conversion/updateSyncVideo?movie_id=".$movieStreamId;
                        $wgetCMD = "";
                        if($drmEnabled == 1 && ($movieStreams->encryption_key != '' || $movieStreams->content_key !='')){
                            $wgetCMD .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.".$s3cfg." --acl-public ".$movieCopyPath." ".$movieOriginPath." \n";
                            $file_data = "file=`echo $0`\n" .
                                            "cf='" . $file . "'\n\n" .
                                            "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                            "then\n\n" .
                                            "echo \"$file is running\"\n\n" .
                                            "else\n\n" .
                                            "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                            "# wget and move command\n" .
                                            $wgetCMD .
                                            "# update query\n" .
                                            "curl $updateUrl\n" .
                                            "# remove the process copy file\n" .
                                            "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                            "# remove the process file\n" .
                                            "rm /var/www/html/$shFolder/$file\n" .
                                            "fi";
                        } else{
                            $folderPath = "/var/www/html/$videoFolder/movieSync_$movieStreamId";
                            $wgetCMD .= "/usr/local/bin/s3cmd get --recursive --config /usr/local/bin/.".$s3cfg." ".$movieCopyPath." \n";
                            $wgetCMD .= "/usr/local/bin/s3cmd sync --recursive --config /usr/local/bin/.".$s3cfg." --acl-public --add-header='content-type':'video/mp4' ".$folderPath."/ ".$movieOriginPath." \n";
                            $file_data = "file=`echo $0`\n" .
                                            "cf='" . $file . "'\n\n" .
                                            "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                                            "then\n\n" .
                                            "echo \"$file is running\"\n\n" .
                                            "else\n\n" .
                                            "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                                            "# create directory\n" .
                                            "mkdir $folderPath\n" .
                                            "chmod 0777 $folderPath\n" .
                                            "cd $folderPath\n" .
                                            "# wget and move command\n" .
                                            $wgetCMD .
                                            "# update query\n" .
                                            "curl $updateUrl\n" .
                                            "# remove directory\n" .
                                            "rm -rf $folderPath\n" .
                                            "# remove the process copy file\n" .
                                            "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                                            "# remove the process file\n" .
                                            "rm /var/www/html/$shFolder/$file\n" .
                                            "fi";
                        }
                        $handle = fopen($_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file, 'w') or die('Cannot open file:  '.$file);
                        fwrite($handle, $file_data);
                        fclose($handle);
                        $filePath = $_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file;
                        $s3 = S3Client::factory(array(
                                'key'    => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                        ));
                        if($s3->upload($conversionBucket, $s3folderPath.$file, fopen($filePath, 'rb'), 'public-read')){
                            $i = 1;
                            unlink($filePath);
                        }
                    } else {
                        Yii::app()->user->setFlash('success', 'Video mapped and encoded successfully');
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        echo json_encode($arr);
                        exit;
                    }
                    if ($i != 0) {
                        $embed_id = md5($movieStreamId);
                        $epData = movieStreams::model()->findByPk($movieStreamId);
                        $this->mapImageFromVideoGallery($movieStreamId,$galleryId);
                        
                        if ($trailer) {
                            $epData->full_movie = $movieStreams->trailer_file_name;    
                        } else {
                            $epData->full_movie = $movieStreams->full_movie;
                            $epData->original_file = $movieStreams->original_file;
                            $epData->converted_file = $movieStreams->converted_file;  
                            $epData->encryption_key = $movieStreams->encryption_key;
                            $epData->content_key = $movieStreams->content_key;
                            $epData->is_demo = 0;
                            $epData->is_offline = $movieStreams->is_offline;
                            $epData->has_hls_feed = $movieStreams->has_hls_feed;
                            $epData->is_mobile_drm_disable = $movieStreams->is_mobile_drm_disable;
                            $epData->is_multibitrate_offline = $movieStreams->is_multibitrate_offline;
                        }
                        $epData->resolution_size = $movieStreams->resolution_size;
                        $epData->video_resolution = $movieStreams->video_resolution;
                        $epData->video_duration = $movieStreams->video_duration;
                        $epData->embed_id = $embed_id;
                        $epData->is_download_progress = 0;
                        $epData->mail_sent_for_video = 0;
                        $epData->is_converted = 3;
                        $epData->has_sh = 0;
                        $epData->upload_start_time = date('Y-m-d H:i:s');
                        $epData->upload_end_time = date('Y-m-d H:i:s');
                        $epData->upload_cancel_time = '';
                        $epData->encoding_start_time = date('Y-m-d H:i:s');
                        $epData->encoding_end_time = date('Y-m-d H:i:s');
                        $epData->encode_fail_time = '';
                        $epData->video_management_id = $galleryId;
                        $epData->thirdparty_url = '';
                        $epData->save();
                        $type = "movie";
                        $res = Yii::app()->common->getStorageSize($movieStreamId, $studioId, $type);
                        if ($trailer) {
                            $epData->original_file = $res['original_file_count'] . "-" . $res['original_file_size'];
                            $epData->converted_file = $res['converted_file_count'] . "-" . $res['converted_file_size'];
                            $epData->save();
                        }
                        $data = array(
                            'studio_id' => $studioId,
                            'original_video_file_count' => $res['original_file_count'],
                            'original_file_size' => $res['original_file_size'],
                            'converted_video_file_count' => $res['converted_file_count'],
                            'converted_file_size' => $res['converted_file_size'],
                            'total_file_size' => $res['total_file_size'],
                            'file_type' => $type,
                            'last_updated_date' => date('Y-m-d H:i:s')
                        );
                        Yii::app()->aws->addStorageLog($data);
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        Yii::app()->user->setFlash('success', 'Video mapped successfully');
                    } else {
                        $embed_id = md5($movieStreamId);
                        $epData = movieStreams::model()->findByPk($movieStreamId);
                        $epData->full_movie = '';
                        $epData->is_converted = 2;
                        $epData->is_offline = 0;
                        $epData->upload_start_time = date('Y-m-d H:i:s');
                        $epData->upload_end_time = date('Y-m-d H:i:s');
                        $epData->upload_cancel_time = '';
                        $epData->encoding_start_time = date('Y-m-d H:i:s');
                        $epData->encoding_end_time = '';
                        $epData->encode_fail_time = date('Y-m-d H:i:s');
                        $epData->save();
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        Yii::app()->user->setFlash('error', 'Due to some reason video could not be mapped to content!');
                    }
                } else {
                    $url = str_replace(' ', '%20', $url);
                    $_REQUEST['url'] = $url;
                    $tModel = new ThirdpartyServerAccess();
                    $arr = $this->is_url_exist($url);
                    if (@$arr['error'] != 1 && @$arr['is_video']) {
                        $this->mapImageFromVideoGallery($movieStreamId,$galleryId);
                        $data = $_REQUEST;
                        $data['content_type'] = $arr['is_video'];
                        //$movieStream = new movieStreams();
                        //$ret = $movieStream->updateByPk($_REQUEST['movie_stream_id'],array('full_movie_url'=>$_REQUEST['url']));
                        $this->createUploadSH($data, "", $galleryId);
                    }
                }
            }
            echo json_encode($arr);
            exit;
        }
    }
    
    /*** Embed from Third Party ***/
    public function actionembedFromThirdPartyPlatform() {   
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_REQUEST['thirdparty_url']) && $_REQUEST['movie_stream_id'] && $_REQUEST['movie_id']) {
            $url = $_REQUEST['thirdparty_url'];
            $movieStreamId = $_REQUEST['movie_stream_id'];
            $movieId = $_REQUEST['movie_id'];
            $date = date('Y-m-d H:i:s');
            $ext = pathinfo($url, PATHINFO_EXTENSION);
            if ((trim($ext)  == 'm3u8') || 
               (strpos($url, 'youtube.com') !== false) && (strpos($url, 'iframe') !== false) || 
               (strpos($url, 'vimeo.com') !== false) && (strpos($url, 'iframe') !== false)|| 
               (strpos($url, 'dailymotion.com') !== false) && (strpos($url, 'iframe') !== false))  
            {
                $getMovieStream =  movieStreams::model()->findByAttributes(array('studio_id' => $studio_id,'movie_id' => $movieId,'id' => $movieStreamId));
                if($getMovieStream){
                    $getMovieStream -> thirdparty_url = $url;
                    $getMovieStream -> is_converted = 1;
                    $getMovieStream ->full_movie = 'test.mp4';
                    $getMovieStream ->has_sh = 0;
                    $getMovieStream ->video_management_id = 0;
                    $getMovieStream ->full_movie_url = NULL;
                    $getMovieStream ->video_resolution = NULL;
                    $getMovieStream ->resolution_size = NULL;
                    $getMovieStream ->original_file = NULL;
                    $getMovieStream ->converted_file = NULL;
                    $getMovieStream ->video_duration = NULL;
                    $getMovieStream ->upload_start_time = $date;
                    $getMovieStream ->upload_end_time = $date;
                    $getMovieStream ->upload_cancel_time = NULL;
                    $getMovieStream ->encoding_start_time = $date;
                    $getMovieStream ->encoding_end_time = $date;
                    $getMovieStream ->encode_fail_time = NULL;
                    $getMovieStream ->mail_sent_for_video = 1;
                    $getMovieStream -> save();
                    if($getMovieStream -> save())
                    {
                        Yii::app()->user->setFlash('success', 'Embed URL is added successfully');
                    } else {
                        Yii::app()->user->setFlash('error', 'Oops! Sorry, Embed URL is not added!');
                    }
                } else {
                    Yii::app()->user->setFlash('Provide a valid m3u8 or  iframe embed url');
                }
                
            }
        }
    }
    
    
    /* Function to map image from video gallery */
    function mapImageFromVideoGallery($movieStreamId,$galleryId) {    
        $dbcon = Yii::app()->db;
        $sql = "SELECT ms.id,ms.full_movie,ms.movie_id,ms.id,ms.episode_number,ms.episode_title,f.name,f.content_types_id,f.content_type_id,ms.is_poster FROM movie_streams AS ms,films f WHERE f.id=ms.movie_id  AND  ms.id=" . $movieStreamId . " AND f.content_types_id !=1 AND is_poster!=1";
        $data = $dbcon->createCommand($sql)->queryAll();
        $sqll = VideoManagement::model()->findbyPk($galleryId);
        if(@$sqll->thumb_image_name != ''){
            $studioId = Yii::app()->common->getStudiosId();
            $bucketInfo = Yii::app()->common->getBucketInfo("", $studioId);
            $bucketName = $bucketInfo['bucket_name'];
            $keyurl = $bucketInfo['s3url'];
            $s3 = Yii::app()->common->connectToAwsS3($studioId);
            foreach ($data as $key => $val) {
                if (strtolower($val['content_types_id']) == 2 || strtolower($val['content_types_id']) == 3){
                    $id = $movieStreamId;
                    $movie_id = $val['movie_id'];

                    if (strtolower($val['content_types_id']) == 2) {
                        $imgname = 'clips_' . $this->cleanSrting($val['name']) . '.jpg';
                        $uid = $val['movie_id'];
                    } else {
                        $imgname = 'episode' . $val['episode_number'] . ".jpg";
                    }
                    $upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/images/videoThumbnail/";
                    $dir = $upload_dir . $val['id'] . "/";

                    if(!is_dir($upload_dir)) {
                        mkdir($upload_dir);
                        @chmod($upload_dir, 0775);
                    }

                    if (!is_dir($dir)) {
                        mkdir($dir);
                        @chmod($dir, 0775);
                    }
                    $dir .="original/";
                    if (!is_dir($dir)) {
                        mkdir($dir);
                        chmod($dir, 0775);
                    }

                    $folderPath = Yii::app()->common->getFolderPath('', $studioId);
                    $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                    $studio = Studio::model()->getStudioBucketData($studioId);
                    $newUser = $studio['new_cdn_users'];

                    if ($newUser) {
                        $source_file = "http://".$bucketName.".".$keyurl."/".$unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $sqll->thumb_image_name;
                    } else {
                        $source_file = "http://".$bucketName.".".$keyurl."/".$unsignedFolderPathForVideo . 'videogallery/' . $studioId . '/videogallery-image/' . $sqll->thumb_image_name;
                    }
                   $img_path = $dir . $imgname;
                   file_put_contents($img_path, file_get_contents($source_file)); 
                   $this->uploadThumbnail($val['id'], $imgname, $val['movie_id'], $img_path, $val['name'], $val['content_types_id'],$studioId);
                }
            }
        }
    }
    
    

    /**
     * @method public addVideoFromVideoGalleryToTrailer() Adding video of video gallery to trailer.
     * @return Success/Error
     * @author SKP<support@muvi.com>
     */
    public function actionaddVideoFromVideoGalleryToTrailer() {
        if (isset($_REQUEST['url']) && $_REQUEST['galleryId']) {
            $url = @$_REQUEST['url'];
            $galleryId = @$_REQUEST['galleryId'];
            if ($url != '' && intval($galleryId)) {
                $movie = 0;
                $studioId = Yii::app()->common->getStudioId();
                $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studioId,'drm_enable');
                $drmEnabled = 0;
                if($getStudioConfig){
                    if(@$getStudioConfig['config_value'] == 1){
                        $drmEnabled = 1;
                    }
                }
                $type = @$_POST['type'];
                if(isset($_POST['type']) && $_POST['type']=='physical'){
                    $model = 'PGMovieTrailer';
                    $dir = 'physical';
                } else {
                    $model = 'movieTrailer';
                    $dir = 'trailers';
                }
                $trailerId = $this->addTrailer($_REQUEST['movie_id'],$model);
                $movieTrailer = $model::model()->FindByAttributes(array('video_management_id' => $galleryId, 'is_converted' => 1));
                
                if (empty($movieTrailer) && $drmEnabled == 0) {
                    $movieTrailer = $model::model()->FindByAttributes(array('video_management_id' => $galleryId, 'is_converted' => 1));
                    $movie = 1;
                }
                if ($movieTrailer) {
                    $s3 = Yii::app()->common->connectToAwsS3($studioId);

                    $bucketInfo = Yii::app()->common->getBucketInfo("", $studioId);
                    $bucketName = $bucketInfo['bucket_name'];

                    $folderPath = Yii::app()->common->getFolderPath("", $studioId);
                    $signedBucketPath = $folderPath['signedFolderPath'];


                    $s3prefix = $signedBucketPath . 'uploads/'.$dir.'/' . $movieTrailer->id . '/';
                    if ($movie) {
                        $s3prefix = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieTrailer->id . '/';
                    }
                    $key = $signedBucketPath . 'uploads/'.$dir.'/' . $trailerId . '/';
                    if ($movieTrailer->id != $trailerId) {
                        $responseDelete = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucketName,
                            'Prefix' => $key
                        ));
                        foreach ($responseDelete as $object) {
                            $s3->deleteObject(array(
                                'Bucket' => $bucketName,
                                'Key' => $object['Key']
                            ));
                        }
                        $response = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucketName,
                            'Prefix' => $s3prefix
                        ));
                        $i = 0;
                        foreach ($response as $object) {
                            $filename = explode($s3prefix, $object['Key']);
                            if (isset($filename[1]) && $filename[1] != '') {
                                $response = $s3->copyObject(array(
                                    'Bucket' => $bucketName,
                                    'Key' => $key . $filename[1],
                                    'CopySource' => "{$bucketName}/{$object['Key']}",
                                    'ACL' => 'public-read',
                                ));
                                if ($response->get('ETag') != '') {
                                    $i++;
                                }
                            }
                        }
                    } else {
                        $i = 1;
                    }
                    if ($i != 0) {
                        $trailer = $model::model()->findByPk($trailerId);
                        $bucketCloudfrontUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                        $key = $bucketInfo['signedFolderPath'] . 'uploads/'.$dir.'/' . $trailerId . '/';
                        if ($movie) {
                            $trailer->trailer_file_name = $movieTrailer->full_movie;
                            $trailer->video_remote_url = $bucketCloudfrontUrl . '/' . $key . $movieTrailer->full_movie;
                        } else {
                            $trailer->trailer_file_name = $movieTrailer->trailer_file_name;
                            $trailer->video_remote_url = $bucketCloudfrontUrl . '/' . $key . $movieTrailer->trailer_file_name;
                        }
                        $trailer->video_resolution = $movieTrailer->video_resolution;
                        $trailer->video_duration = $movieTrailer->video_duration;
                        $trailer->resolution_size = $movieTrailer->resolution_size;
                        $trailer->last_updated_by = Yii::app()->user->id;
                        $trailer->last_updated_date = gmdate('Y-m-d');
                        $trailer->is_converted = 1;
                        $trailer->has_sh = 0;
                        $trailer->upload_start_time = date('Y-m-d H:i:s');
                        $trailer->upload_end_time = date('Y-m-d H:i:s');
                        $trailer->upload_cancel_time = '';
                        $trailer->mail_sent_for_trailer = 0;
                        $trailerData->encoding_start_time = date('Y-m-d H:i:s');
                        $trailerData->encoding_end_time = date('Y-m-d H:i:s');
                        $trailer->encode_fail_time = '';
                        $trailer->video_management_id = $galleryId;
                        $trailer->save();
                        $type = ($type=='physical')?"physical":"trailer";
                        $res = Yii::app()->common->getStorageSize($trailerId, $studioId, $type);
                        $data = array(
                            'studio_id' => $studioId,
                            'original_video_file_count' => $res['original_file_count'],
                            'original_file_size' => $res['original_file_size'],
                            'converted_video_file_count' => $res['converted_file_count'],
                            'converted_file_size' => $res['converted_file_size'],
                            'total_file_size' => $res['total_file_size'],
                            'file_type' => $type,
                            'last_updated_date' => date('Y-m-d H:i:s')
                        );
                        Yii::app()->aws->addStorageLog($data);
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        Yii::app()->user->setFlash('success', 'Trailer is added to content successfully');
                    } else {
                        $epData = $model::model()->findByPk($trailerId);
                        $epData->full_movie = '';
                        $epData->is_converted = 2;
                        $epData->upload_start_time = date('Y-m-d H:i:s');
                        $epData->upload_end_time = date('Y-m-d H:i:s');
                        $epData->upload_cancel_time = '';
                        $epData->encoding_start_time = date('Y-m-d H:i:s');
                        $epData->encoding_end_time = '';
                        $epData->encode_fail_time = date('Y-m-d H:i:s');
                        $epData->save();
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        Yii::app()->user->setFlash('success', 'Due to some reason trailer could not be added to content!');
                    }
                } else {
                    $url = str_replace(' ', '%20', $url);
                    $_REQUEST['url'] = $url;
                    $tModel = new ThirdpartyServerAccess();
                    $arr = $this->is_url_exist($url);
                    if (@$arr['error'] != 1 && @$arr['is_video']) {
                        $vidoInfo = new SplFileInfo($url);
                        $videoExt = $vidoInfo->getExtension();
                        $videoOnlyName = $vidoInfo->getBasename('.' . $vidoInfo->getExtension());
                        $_REQUEST['filename'] = $videoOnlyName . '.' . $videoExt;
                        $data = $_REQUEST;
                        $data['content_type'] = $arr['is_video'];
                        $this->updateTrailerInfo($data,$type);
                    }
                }
            }
            echo json_encode($arr);
            exit;
        }
    }

    public function actionajaxSearchVideo() {
        //print_r($_REQUEST);
        $key_word = $_REQUEST['search_word'];
        $type = $_REQUEST['type'];
        $studio_id = Yii::app()->user->studio_id;
        if ($type == 'audio')
            $results = AudioManagement::model()->get_audiodetails_by_keyword($key_word, $studio_id);
		else if($type == 'file')
			$results = FileManagement::model()->get_videodetails_by_keyword($key_word, $studio_id);
        else
        $results = VideoManagement::model()->get_videodetails_by_keyword($key_word, $studio_id);

        $this->layout = false;
        if ($type == 'audio')
            echo $this->render('ajaxsearchaudio', array('all_audios' => $results));
		else if($type == 'file')
			 echo $this->render('ajaxsearchfile', array('all_videos' => $results));
        else
        echo $this->render('ajaxsearchvideo', array('all_videos' => $results));
    }
    public function actionajaxSearchVideoForContent() {
        //print_r($_REQUEST);
        $key_word = $_REQUEST['search_word'];
        $studio_id = Yii::app()->user->studio_id;
        $results = VideoManagement::model()->get_videodetails_by_keyword($key_word, $studio_id);

        $base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
        $this->layout = false;
        echo $this->render('ajaxsearchvideocontent', array('all_videos' => $results));
    }    
    
    public function actionajaxFiltervideo() {
        $studio_id = $this->studio->id;
      if(isset($_REQUEST['video_duration'])&& isset($_REQUEST['file_size']) &&  isset($_REQUEST['is_encoded']) &&  isset($_REQUEST['uploaded_in'])){
           $duration=$_REQUEST['video_duration'];
           $file_size=$_REQUEST['file_size'];
           $is_encoded=$_REQUEST['is_encoded'];
           $uploaded_in=$_REQUEST['uploaded_in'];
            $cond='';
           //Duration
            if ($duration == 1) {// <5 mins
                $cond .= 'CAST(t.duration AS TIME) < "00:05:00" AND ';
            } else if ($duration == 2) {// <30 mins
                $cond .= 'CAST(t.duration AS TIME) < "00:30:00" AND ';
            } else if ($duration == 3) {// <120 mins
                $cond .= 'CAST(t.duration AS TIME)< "02:00:00" AND ';
            } else if ($duration == 4) {// >120 mins
                $cond .= 'CAST(t.duration AS TIME) > "02:00:00" AND ';
            } else {
            $cond.='';   
           }
           //File Size
            if ($file_size == 1) {//< 1GB=1024 MB
                $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 1024 AND ';
            } else if ($file_size == 2) {//<10 GB
                $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 10240 AND ';
            } else if ($file_size == 3) {//>10 GB
                $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)> 10240 AND ';
            } else {
               $cond.=''; 
           }
            //uploaded i
            if ($uploaded_in == 1) {//this week
                $cond .= ' YEARWEEK(t.creation_date,1) =  YEARWEEK(CURDATE(), 1) AND ';
            } else if ($uploaded_in == 2) { //this month
                $cond .= ' MONTH(t.creation_date)=  MONTH(CURDATE()) AND';
            } else if ($uploaded_in == 3) {//this year
                $cond .= ' YEAR(t.creation_date) = YEAR(CURDATE()) AND ';
            } else if ($uploaded_in == 4) {// before this year
                $cond .= ' YEAR(t.creation_date) < YEAR(CURDATE()) AND ';
            } else {
              $cond.='';  
           }
            //encoded ??
            if ($is_encoded == 1) {//yes
             $cond.='(ms.is_converted=1  OR mt.is_converted=1) AND ';   
            } else if ($is_encoded == 2) {//No
              $cond.='(ms.is_converted  IS NULL OR ms.is_converted =0 ) AND (mt.is_converted  IS NULL OR mt.is_converted =0) AND ';    
            } else {
            $cond.='';   
           }
            }
        $allvideo = VideoManagement::model()->filter_video_files($studio_id,$cond);
        $this->layout = false;
        $this->renderPartial('ajaxsearchvideo', array('all_videos' => $allvideo['data'],'totalcount' => @$allvideo['count']));
            }
         
	/*** actionajaxFilterAudio() seach audio files based on filters
     * author/optimised by:suraja@muvi.com
     * date:15/05/2017
     * params:N/A
     * returns:N/A
     */
    public function actionajaxFilterAudio() {
        $studio_id = $this->studio->id;       
        if (isset($_REQUEST['video_duration']) && isset($_REQUEST['file_size']) && isset($_REQUEST['is_encoded']) && isset($_REQUEST['uploaded_in'])) {
            $duration = $_REQUEST['video_duration'];
            $file_size = $_REQUEST['file_size'];
            $is_encoded = $_REQUEST['is_encoded'];
            $uploaded_in = $_REQUEST['uploaded_in'];
            $cond='';
            //Duration
            if ($duration == 1) {// <5 mins
                $cond .= 'CAST(t.duration AS TIME) < "00:05:00" AND ';
            } else if ($duration == 2) {// <30 mins
                $cond .= 'CAST(t.duration AS TIME) < "00:30:00" AND ';
            } else if ($duration == 3) {// <120 mins
                $cond .= 'CAST(t.duration AS TIME)< "02:00:00" AND ';
            } else if ($duration == 4) {// >120 mins
                $cond .= 'CAST(t.duration AS TIME) > "02:00:00" AND ';
            } else {
                $cond .= '';
            } 
            //File Size
            if ($file_size == 1) {//< 1GB=1024 MB
                $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 1024 AND ';
            } else if ($file_size == 2) {//<10 GB
                $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 10240 AND ';
            } else if ($file_size == 3) {//>10 GB
                $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)> 10240 AND ';
            } else {
               $cond.=''; 
            }
            //uploaded in 
            if ($uploaded_in == 1) {//this week
                $cond .= ' YEARWEEK(t.creation_date,1) =  YEARWEEK(CURDATE(), 1) AND ';
            } else if ($uploaded_in == 2) { //this month
                $cond .= ' MONTH(t.creation_date)=  MONTH(CURDATE()) AND';
            } else if ($uploaded_in == 3) {//this year
                $cond .= ' YEAR(t.creation_date) = YEAR(CURDATE()) AND ';
            } else if ($uploaded_in == 4) {// before this year
                $cond .= ' YEAR(t.creation_date) < YEAR(CURDATE()) AND ';
            } else {
              $cond.='';  
            }
            //encoded ??
            if ($is_encoded == 1) {//yes
                $cond .= '(ms.is_converted=1) AND ';
            } else if ($is_encoded == 2) {//No
                $cond .= '(ms.is_converted  IS NULL OR ms.is_converted =0 ) AND ';
            } else {
              $cond.='';  
            }
           }
        $allaudio = AudioManagement::model()->filter_audio_files($studio_id,$cond);       
        $this->layout = false;
        $this->renderPartial('ajaxsearchaudio', array('all_audios' => $allaudio['data'],'totalcount' => @$allaudio['count']));
        exit;
    }

    function actionPartners() {
        $this->breadcrumbs = array('Partner Portal');
        $this->headerinfo = "Content Partner";
        $this->pageTitle = "Muvi | Content Partner";

        $studio = $this->studio;

        $studio_id = $studio->id;
        $partners = User::model()->findAll('studio_id=:studio_id AND is_active=:is_active AND (role_id=:t1)', array(':studio_id' => $studio_id, ':is_active' => 1, ':t1' => 4), array('order' => 'first_name ASC'));

        $partners_content = array();

        if (isset($partners) && !empty($partners)) {
            $con = Yii::app()->db;
            foreach ($partners as $key => $value) {
                $sql = "SELECT SQL_CALC_FOUND_ROWS f.id, f.name from partners_contents p LEFT JOIN films f ON (p.movie_id=f.id) WHERE p.studio_id=" . $studio_id . " AND p.user_id=" . $value->id . " LIMIT 0, 2";
                (array) $data = $con->createCommand($sql)->queryAll();

                $item_count = $con->createCommand('SELECT FOUND_ROWS() AS count')->queryAll();
                $item_count = (isset($item_count[0]['count'])) ? $item_count[0]['count'] : 0;

                $partners_content[$value->id]['contents'] = $data;
                $partners_content[$value->id]['total'] = $item_count;
            }
        }

        $this->render('partners', array('studio' => $studio, 'partners' => $partners, 'partners_content' => $partners_content));
    }

    function actionEnablePartnersPortal() {
        if (isset($_REQUEST['id_partners']) && !empty($_REQUEST['id_partners'])) {
            $studio_id = $_REQUEST['id_partners'];
            $sql = "UPDATE studios SET is_partners=1 WHERE id=" . $studio_id;
            $con = Yii::app()->db;
            $data = $con->createCommand($sql)->execute();

            Yii::app()->user->setFlash('success', 'Partners has been enabled successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Partners can not be enabled!');
        }

        $url = $this->createUrl('admin/partners');
        $this->redirect($url);
    }

    function actionDisablePartnersPortal() {
        if (isset($_REQUEST['id_partners']) && !empty($_REQUEST['id_partners'])) {
            $studio_id = $_REQUEST['id_partners'];
            $sql = "UPDATE studios SET is_partners=0 WHERE id=" . $studio_id;
            $con = Yii::app()->db;
            $data = $con->createCommand($sql)->execute();

            Yii::app()->user->setFlash('success', 'Partners has been disabled successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Partners can not be disabled!');
        }

        $url = $this->createUrl('admin/partners');
        $this->redirect($url);
    }

    function actionDeletePartner() {
        if (isset($_REQUEST['id_partners']) && !empty($_REQUEST['id_partners'])) {
            $user_id = $_REQUEST['id_partners'];
            $studio = $this->studio;
            $studio_id = $studio->id;

            PartnersContent::model()->deleteAll('user_id =:user_id AND studio_id =:studio_id', array(':user_id' => $user_id, ':studio_id' => $studio_id));
            User::model()->deleteAll('id =:id AND studio_id =:studio_id', array(':id' => $user_id, ':studio_id' => $studio_id));
            Yii::app()->user->setFlash('success', 'Partner has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Partner can not be deleted!');
        }

        $url = $this->createUrl('admin/partners');
        $this->redirect($url);
    }

    function actionCheckEmail() {
        $isExists = 0;
        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $user = new User;

            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email'])));
            if (isset($users) && !empty($users)) {
                $isExists = 1;
            }
        }

        $res = array('isExists' => $isExists);
        echo json_encode($res);
        exit;
    }

    function actionCheckEmailPartner() {
        $isExists = 0;
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $user = new User;

            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $studio_id));
            if (isset($users) && !empty($users)) {
                $isExists = 1;
            }
        }

        $res = array('isExists' => $isExists);
        echo json_encode($res);
        exit;
    }

    function actionNewPartner() {
        if (isset($_REQUEST['data']['Admin']) && !empty($_REQUEST['data']['Admin'])) {
            $studio = $this->studio;
            $studio_id = $studio->id;

            $data = $_REQUEST['data']['Admin'];
            $movie_id = ltrim($_REQUEST['data']['movie_ids'], ',');
            $movie_ids = explode(',', $movie_id);
          
            //Insert data into the user table
            $format = new Format();
            $password = $format->randomPassword();

            $enc = new bCrypt();
            $encrypted_pass = $enc->hash($password);

            $umodel = new User();
            $umodel->studio_id = $studio_id;
            $umodel->first_name = trim($data['first_name']);
            $umodel->email = trim($data['email']);
            $umodel->encrypted_password = $encrypted_pass;
            $umodel->created_at = gmdate('Y-m-d H:i:s');
            $umodel->is_active = 1;
            $umodel->is_sdk = 1;
            $umodel->role_id = 4;
            $umodel->signup_step = 0;
            if(empty($_REQUEST['data']['p_partner_login'])){
            $umodel->is_login_allowed = 0;   
            }
            if(empty($_REQUEST['data']['p_all'])){
            $umodel->permission_id = 0;
            }
            else
            {
            $umodel->permission_id = 1;  
            $umodel->is_email_send = 1;
            }
            $umodel->save();
            $user_id = $umodel->id;
            //partner content add
            
            
             foreach ($movie_ids as $key => $value) {
                    $PartnersContent = new PartnersContent();
                    $PartnersContent->studio_id = $studio_id;
                    $PartnersContent->user_id = $user_id;
                    $PartnersContent->movie_id = $value;
                if(!empty($_REQUEST['data']['Admin']['percentage_share'])){
                $PartnersContent->percentage_revenue_share = $_REQUEST['data']['Admin']['percentage_share'];
                }
                else{
                $PartnersContent->percentage_revenue_share = 100;    
                }
                    $PartnersContent->save();
                }
            
            
            //Send to email to that user
             if(isset($_REQUEST['data']['p_partner_login']) && !empty($_REQUEST['data']['p_partner_login'])){    
              $this->sendEmailToNewPartner($umodel, $password);
             }
            Yii::app()->user->setFlash('success', 'New partner has been added successfully.');
        } else {
            Yii::app()->user->setFlash('error', 'Invalid partner data.');
        }

        $url = $this->createUrl('/admin/partners');
        $this->redirect($url);
    }

    function sendEmailToNewPartner($user = Null, $password = Null) {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $site_url = HTTP . $studio->domain;

        $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);

        $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="" /></a>';

        $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';

        //Check facebook link given or not
        if ($studio->fb_link != '') {
            $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        } else {
            $fb_link = '';
        }

        //Check twitter link given or not
        if ($studio->tw_link != '') {
            $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        } else {
            $twitter_link = '';
        }

        //Check google plus link given or not
        if ($studio->gp_link != '') {
            $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
        } else {
            $gplus_link = '';
        }

        //Set variables for different email types
        $email = array($user->email);
        $FirstName = $user->first_name;
        $StudioName = $studio->name;

        $subject = "Partner invitation from " . $studio->domain;
        $content = '<p>Dear ' . $FirstName . ',</p>
                    <p>You have been added as a partner to ' . $StudioName . '</p> 
                    <p>Please find following url and credentials .</p>
                    <p style="display:block;margin:0 0 17px">
                        Url: <strong><span mc:edit="domain"><a href="' . $site_url . '/partners">' . $site_url . '/partners</a></span></strong><br/>
                        User Name: <strong><span mc:edit="email">' . $user->email . '</span></strong><br/>
                        Password: <strong><span mc:edit="password">' . $password . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team ' . $StudioName . '</p>';

        $user_model = new User();
        $admin = $user_model->findByAttributes(array('studio_id' => $studio_id, 'is_sdk' => 1, 'role_id' => 1));
        if (isset($admin) && !empty($admin)) {
            $from_mail = $admin->email;
        } else {
            $from_mail = 'info@muvi.com';
        }

        $params = array(
            'website_name'=> $StudioName,
            'site_link'=> $site_link,
            'logo'=> $logo,
            'name'=> $FirstName,
            'mailcontent'=> $content
        );

       

        $template_name = 'sdk_user_welcome_new';
            
        Yii::app()->theme = 'bootstrap';
        $html = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
      
        $retval=$this->sendmailViaAmazonsdk($html,$subject,$email,$from_mail,'','','',$StudioName); 
         
    }

    function actionSavePartnerName() {
        if (isset($_REQUEST['partner_id']) && isset($_REQUEST['studio_id']) && isset($_REQUEST['first_name'])) {
            $user = User::model()->findByAttributes(array('id' => $_REQUEST['partner_id'], 'studio_id' => $_REQUEST['studio_id']));
            if (isset($user) && !empty($user)) {
                $user->first_name = trim($_REQUEST['first_name']);
                $user->last_name = trim($_REQUEST['last_name']);
                $user->save();
                Yii::app()->user->setFlash('success', "Partner's name has been updated successfully");
            } else {
                Yii::app()->user->setFlash('error', "Oops! Sorry, Partner's name can not be updated!");
            }
        }
        print 1;
        exit;
    }

    function actionPartnersContent() {
        $this->layout = false;

        $data = array();
        $studio = $this->studio;
        $studio_id = $studio->id;

        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $data = $_REQUEST['data'];

            $movie_id = ltrim($data['movie_ids'], ',');
            $movie_ids = explode(',', $movie_id);

            if (isset($movie_ids) && !empty($movie_ids)) {
                PartnersContent::model()->deleteAll('user_id =:user_id AND studio_id =:studio_id', array(':user_id' => $data['user_id'], ':studio_id' => $studio_id));

                foreach ($movie_ids as $key => $value) {
                    $PartnersContent = new PartnersContent();
                    $PartnersContent->studio_id = $studio_id;
                    $PartnersContent->user_id = $data['user_id'];
                    $PartnersContent->movie_id = $value;
                     if(!empty($_REQUEST['data']['Admin']['percentage_share'])){
                $PartnersContent->percentage_revenue_share = $_REQUEST['data']['Admin']['percentage_share'];
                }
                else{
                $PartnersContent->percentage_revenue_share = 100;    
                }
                    $PartnersContent->save();
                }
                $user_p = User::model()->findByAttributes(array('id' => $data['user_id'], 'studio_id' => $studio_id));
                $data = $_REQUEST['data']['Admin'];
                $is_email_send=$user_p['is_email_send'];
                
                if (isset($user_p) && !empty($user_p)) {
                    if($is_email_send==0){
                    $format = new Format();
                    $password = $format->randomPassword();
                    $enc = new bCrypt();
                    $encrypted_pass = $enc->hash($password);
                    $user_p->encrypted_password = $encrypted_pass;
                    }
                    $user_p->first_name = trim($data['first_name']);
                    $user_p->updated_at = gmdate('Y-m-d H:i:s');
                    if(empty($_REQUEST['data']['ep_partner_login'])){
                    $user_p->is_login_allowed = 0; 
                    $user_p->is_email_send = 0;
                    }
                    else
                    {
                     $user_p->is_login_allowed = 1;  
                     $user_p->is_email_send = 1; 
                    }
                    if(empty($_REQUEST['data']['ep_all'])){
                    $user_p->permission_id = 0;   
                    }
                    else
                    {
                    $user_p->permission_id = 1;       
                    } 
                   // $user_p->permission_id = isset($_REQUEST['p_all'])?$_REQUEST['p_all']:'';//allow manage content to partner
                    $user_p->update();
                    
                     if(isset($_REQUEST['data']['ep_partner_login']) && !empty($_REQUEST['data']['ep_partner_login']) && $is_email_send==0){    
                      $this->sendEmailToNewPartner($user_p, $password);
                       }
                    
                }
                Yii::app()->user->setFlash('success', "Authorized for Content has been set successfully");
            }

            $url = $this->createUrl('/admin/partners');
            $this->redirect($url);
        } else if (isset($_REQUEST['partner_id']) && trim($_REQUEST['partner_id'])) {
            $command = Yii::app()->db->createCommand()
                    ->select('p.id, f.id AS film_id, f.name,p.percentage_revenue_share')
                    ->from('partners_contents p ,films f ')
                    ->where('p.movie_id = f.id AND p.studio_id=' . $studio_id . ' AND p.user_id=' . $_REQUEST['partner_id']);

            $data = $command->queryAll();

            if (isset($data) && !empty($data)) {
                foreach ($data as $key => $value) {
                    $rec = array('value' => $value['film_id'], 'text' => $value['name'],'percentage_revenue_share'=>$value['percentage_revenue_share']);
                    if (isset($rec) && !empty($rec)) {
                        $movies[] = $rec;
                    }
                }
            }

            $user = User::model()->findByPk($_REQUEST['partner_id']);
        }

        $this->render('partners_content', array('user' => $user, 'movies' => $movies));
    }

    function actionmovie_autocomplete() {
        $res = array();
        $studio_id = Yii::app()->user->studio_id;

        if (isset($_REQUEST['search']) && trim($_REQUEST['search'])) {
            $srch = trim($_REQUEST['search']);
            $sql = "SELECT fm.* FROM films fm WHERE fm.studio_id={$studio_id} AND fm.name LIKE '%{$srch}%' ORDER BY fm.name LIMIT 0, 10";

            $con = Yii::app()->db;
            (array) $data = $con->createCommand($sql)->queryAll();

            $rec = array();
            if (isset($data) && !empty($data)) {
                foreach ($data as $key => $value) {
                    $rec = array('value' => $value['id'], 'text' => $value['name']);
                    if (isset($rec) && !empty($data)) {
                        $res[] = $rec;
                    }
                }
            }
        } else {
            $res = array(
                array('value' => '0', 'text' => 'No data found!')
            );
        }

        echo json_encode($res);
        exit;
    }

    function actionPosterPreview() {
        $_REQUEST['movie_id'] = @$_REQUEST['episode']['content_name'] ? @$_REQUEST['episode']['content_name'] : $_REQUEST['movie_id']; //modified by manas
        $movie_id = @$_REQUEST['movie_id'];
        if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
            $movie_id = $movie_id ? $movie_id : "preview";
            $file_info = pathinfo($_FILES['Filedata']['name']);

            $_FILES['Filedata']['name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];

            list($width, $height, $type, $attr) = getimagesize($_FILES['Filedata']['tmp_name']);
            if(isset($_REQUEST['add_chnel'])){
                $req_width = $_REQUEST['reqwidth'];
                $req_height = $_REQUEST['reqheight'];
            }else{
            $req_width = $_REQUEST['reqwidth1'];
            $req_height = $_REQUEST['reqheight1'];
            }
            if ($width < $req_width || $height < $req_height) {
                echo "http://d2gx0xinochgze.cloudfront.net/public/no-image-a.png";
                exit;
            }

            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/posters/' . $movie_id;
             if(isset($_REQUEST['add_chnel'])){
            $path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['jcrop']);
             }else{
                 $path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
             }
            /*             * ********** code for uploading images and thumb for image gallery ************ */
            $path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $path);
            print $path;
            exit;
        } else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name']);

            $_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];

            $jcrop_allimage = $_REQUEST['jcrop_allimage'];
            $image_name = $_REQUEST['g_image_file_name'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];
            $studio_id = Yii::app()->common->getStudiosId();
            $dir1 = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagemanagement/' . $studio_id;

            if (!is_dir($dir1)) {
                mkdir($dir1);
                @chmod($dir1, 0777);
            }

            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagemanagement/' . $studio_id . '/poster';
            $all_image_path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);

            $all_image_path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $all_image_path);
            echo $all_image_path;
            exit;
        }
    }
    public function actionsaveMrssData() {
        $this->layout = false;
        $studio_id = Yii::app()->user->studio_id;
        $i =0;
        $feedAdded = 0;
        if(isset($_REQUEST['selected_matadata']) && isset($_REQUEST['content_name_xml']) && @$_REQUEST['content_type_xml'] != '' && @$_REQUEST['mrss_feed_url_xml'] != ''){
            $contentName = $_REQUEST['content_name_xml'];
            $contentType = $_REQUEST['content_type_xml'];
            $xml = simplexml_load_file($_REQUEST['mrss_feed_url_xml'], 'SimpleXMLElement', LIBXML_NOCDATA);
            $a = array();
            if($xml != ''){
                $getNameSpace =$xml->getNamespaces(true);
                foreach($xml->children() as $child)
                {
                     $ai = 0;
                    foreach ($child->children() as $node) {
                        $a[$ai]['data'] = json_decode(json_encode($node), true);
                        foreach($node->children() as $nodeChildKey => $nodeChildKeyVal){
                            $mediaAttributes[$nodeChildKey] = '';
                        }
                        $attr = $node->children($getNameSpace["media"]);
                        $attr12= json_decode(json_encode($attr, true));
                        foreach($attr12 as $attr12key => $attr12Val){
                            $content = json_decode(json_encode($attr->$attr12key->attributes()), true);
                            if($content){
                                $a[$ai][$attr12key] = @$content['@attributes'];
                            } else{
                                $a[$ai][$attr12key] = $attr12Val;
                            }
                            
                        }
                        $attr1 = $attr->children($getNameSpace["media"]);
                            $attr13 =json_decode(json_encode($attr1), true);
                            foreach($attr13 as $attr13Key => $attr13Val){
                                $content = json_decode(json_encode($attr1->$attr13Key->attributes()), true);
                                if($content){
                                    $a[$ai][$attr13Key] = @$content['@attributes'];
                                } else{
                                    $a[$ai][$attr13Key] = $attr13Val;
                                }
                            }
                        $ai++;
                    }
                }
            }
            $xmldata = $a;
            $metaTag = $_REQUEST['selected_matadata'];
            $contTyp = explode(",",$contentType);
            $contentFields = array();
            $contentTypeId = $contTyp[0];
            $conTId = $contTyp[1];
            if($conTId ==  1){
                $contentFields["Content Name"] = "name";
                $contentFields["Release/Recorded Date"] = "release_date";
                $contentFields["Genre"] = "genre";
                $contentFields["Language"] = "language";
                $contentFields["Censor Ratings"] = "censer_rating";
                $contentFields["Story/Description"] = "story";
            } else if ($conTId ==  2) {
                $contentFields["Content Name"] = "name";
                $contentFields["Release/Recorded Date"] = "release_date";
                $contentFields["Genre"] = "genre";
                $contentFields["Story/Description"] = "story";
            } else if ($conTId ==  3) {
                $contentFields["Title"] = "episode_title";
                $contentFields["Story"] = "episode_story";
                $contentFields["Episode Date"] = "episode_date";
            }
            $metaTagArray = array();
            foreach($metaTag as $metaTagKey => $metaTagVal){
                $metaTagValVal = explode("=",$metaTagVal);
                if(count(array_intersect($metaTagVal, $contentFields)) == 0){
                    $metaTagArray[$metaTagValVal[0]] = $contentFields[$metaTagValVal[1]];
                }
            }
            if(!empty($xmldata)){
                $studio_id = Yii::app()->user->studio_id;
                $dbcon = Yii::app()->db;
                if($conTId  == 3){
                    $contentName = explode(",", $contentName);
                    $movie_id = (int)$contentName[0];
                    $seasonId = (int)$contentName[1];
                    if($seasonId == 0){
                        $seasonId = 1;
                        $vquery = "SELECT series_number FROM movie_streams WHERE movie_id=".$movie_id." ORDER BY series_number DESC LIMIT 1";
                        $movieStrData = $dbcon->createCommand($vquery)->queryAll();
                        if(isset($movieStrData[0]['series_number'])){
                           $seasonId = $movieStrData[0]['series_number'] +1; 
                        }
                    }
                }
                foreach($xmldata as $keya => $vala){
                    $dataExistInDb = 0;
                    $dataMatchColumn = 0;
                    if(!empty($vala["data"])){
                        $data = array();
                        $data['content_type'] = $contentType;
                        if(($conTId  == 1) || ($conTId  == 2)){
                            $Films = new Film();
                            $Films->content_type_id = $contentTypeId;
                            $Films->studio_id = $studio_id;
                            $uniqid = Yii::app()->common->generateUniqNumber();
                            $Films->uniq_id = $uniqid;
                            foreach($vala["data"] as $valakey => $valaVal){
                                if(count(array_intersect($valakey, $metaTagArray)) == 0){
                                    if($metaTagArray[$valakey] == 'name'){
                                        $vallll = mysql_real_escape_string($valaVal);
                                        $Films->name = $vallll;
                                        $data[$metaTagArray[$valakey]] = $valaVal;
                                        $findData = "select count(*) as totalCount from films where studio_id=".Yii::app()->user->studio_id." and name='".$vallll."'";
                                        $findDataQry = $dbcon->createCommand($findData)->queryAll();
                                        if(isset($findDataQry[0]['totalCount']) && $findDataQry[0]['totalCount'] > 0){
                                            $dataExistInDb = 1;
                                        }
                                        $dataMatchColumn = 1;
                                    } elseif  ($metaTagArray[$valakey] == 'story') {
                                        $Films->story = mysql_real_escape_string($valaVal);
                                        $data[$metaTagArray[$valakey]] = $valaVal;
                                        $dataMatchColumn = 1;
                                    }else if($metaTagArray[$valakey] == 'release_date'){
                                        if(isset($valaVal) && $valaVal != '1970-01-01' && strlen(trim($valaVal)) > 6){
                                            if (DateTime::createFromFormat('Y-m-d G:i:s', $valaVal) !== FALSE){
                                                $Films->release_date = date('Y-m-d', strtotime($valaVal));
                                                $data['release_date'] = $valaVal;
                                                $dataMatchColumn = 1;
                                            }
                                        }
                                    } elseif  ($metaTagArray[$valakey] == 'genre') {
                                        $Films->genre = json_encode($valaVal);
                                        $data[$metaTagArray[$valakey]] = $valaVal;
                                        $dataMatchColumn = 1;
                                    } elseif  ($metaTagArray[$valakey] == 'language') {
                                        $Films->genre = json_encode($valaVal);
                                        $data[$metaTagArray[$valakey]] = $valaVal;
                                        $dataMatchColumn = 1;
                                    } elseif  ($metaTagArray[$valakey] == 'censer_rating') {
                                        $Films->genre = json_encode($valaVal);
                                        $data[$metaTagArray[$valakey]] = $valaVal;
                                        $dataMatchColumn = 1;
                                    } 
                                }
                            }
                            $Films->created_date = gmdate('Y-m-d H:i:s');
                            $Films->last_updated_date = gmdate('Y-m-d H:i:s');
                            if(($dataExistInDb == 0) && ($dataMatchColumn == 1)){
                                $Films->save();
                                $movie_id = $Films->id;
                            //Check and Save new Tags into database
                            $movieTags = new MovieTag();
                            $movieTags->addTags($data);
//                            if (isset($_REQUEST['content_filter_type'])) {
//                                $ctype = explode('-', $data['content_type']);
//                                $contentFilterCls = new ContentFilter();
//                                $addContentFilter = $contentFilterCls->addContentFilter($_REQUEST, $ctype[0]);
//                            }
                            //Insert Into Movie streams table
                            $MovieStreams = new movieStreams();
                            $MovieStreams->studio_id = $studio_id;
                            $MovieStreams->movie_id = $movie_id;
                            $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
                            $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
                            $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
                            $MovieStreams->is_episode = 0;
                            $MovieStreams->save();
                            $movie_stream_id = $MovieStreams->id;
                            }
                        } else if ($conTId  == 3){
                            $vquery = "SELECT episode_number FROM movie_streams WHERE movie_id=".$movie_id." AND series_number =".$seasonId." ORDER BY episode_number DESC LIMIT 1";
                            $movieStrData = $dbcon->createCommand($vquery)->queryAll();
                            $episodeNumber = 1;
                            if(isset($movieStrData[0]['episode_number'])){
                               $episodeNumber = $movieStrData[0]['episode_number']; 
                            }
                            $epData = new movieStreams();
                            foreach($vala['data'] as $valakey => $valaVal){
                                if(count(array_intersect($valakey, $metaTagArray)) == 0){
                                    if($metaTagArray[$valakey] == 'episode_title'){
                                        $checkTheVAL = mysql_real_escape_string($valaVal);
                                        $epData->episode_title = $checkTheVAL;
                                        $findData = "select count(*) as totalCount from movie_streams where studio_id=".Yii::app()->user->studio_id." and episode_title='".$checkTheVAL."'";
                                        $findDataQry = $dbcon->createCommand($findData)->queryAll();
                                        if(isset($findDataQry[0]['totalCount']) && $findDataQry[0]['totalCount'] > 0){
                                            $dataExistInDb = 1;
                                        }
                                        $dataMatchColumn = 1;
                                    } else if($metaTagArray[$valakey] == 'episode_story'){
                                        $epData->episode_story = mysql_real_escape_string($valaVal);
                                        $dataMatchColumn = 1;
                                    }else if($metaTagArray[$valakey] == 'episode_date'){
                                        if(isset($valaVal) && $valaVal != '1970-01-01' && strlen(trim($valaVal)) > 6){
                                            if (DateTime::createFromFormat('Y-m-d G:i:s', $valaVal) !== FALSE){
                                                $epData->episode_date = date('Y-m-d', strtotime($valaVal));
                                                $dataMatchColumn = 1;
                                            }
                                        }
                                    }
                                }
                            }
                    
                            $epData->series_number = $seasonId;
                            $epData->episode_number = $episodeNumber + 1;
                            $epData->studio_id = Yii::app()->user->studio_id;
                            $epData->embed_id = Yii::app()->common->generateUniqNumber();
                            $epData->movie_id = $movie_id;
                            $epData->created_by = Yii::app()->user->id;
                            $epData->created_date = gmdate('Y-m-d');
                            $epData->last_updated_date = gmdate('Y-m-d H:i:s');
                            $epData->is_episode = 1;
                            if(($dataExistInDb == 0) && ($dataMatchColumn == 1)){
                            $epData->save();
                            $movie_stream_id = $epData->id;
                            }
                        }
                        if((isset($vala['content']['url'])) && ($dataExistInDb == 0) && ($dataMatchColumn == 1)){
                            $shUploadData = array();
                            $shUploadData['movie_id'] = $movie_id;
                            $shUploadData['movie_stream_id'] = $movie_stream_id;
                            $shUploadData['url'] = $vala['content']['url'];
                            $url = str_replace(' ', '%20', $vala['content']['url']);
                            $_REQUEST['url'] = $url;
                            $arr = $this->is_url_exist($url);
                            if (@$arr['error'] != 1 && @$arr['is_video']) {
                                $shUploadData['content_type'] = $arr['is_video'];
                                $this->createUploadSH($shUploadData);
                            }
                        }
                        $i++;
                        if(($dataExistInDb == 0) && ($dataMatchColumn == 1)){
                            $feedAdded++;
                            $mrss_feed = new MrssFeed();
                            $mrss_feed -> studio_id = Yii::app()->user->studio_id;
                            $mrss_feed -> feed_url = $_REQUEST['mrss_feed_url_xml'];
                            $mrss_feed -> feed_id = $i;
                            $mrss_feed -> data = json_encode($vala);
                            $mrss_feed -> created_by = Yii::app()->user->id;
                            $mrss_feed -> created_date = date('Y-m-d H:i:s');
                            $mrss_feed -> save();
                        }
                    }
                    
                }
            }
        }
        if(($i != 0) && ($feedAdded != 0)){
            Yii::app()->user->setFlash('success', 'Feed\'s has been added successfully');
        } else if(($i != 0) && ($feedAdded == 0)){
            Yii::app()->user->setFlash('error','Contents of this Feed are already added.');
        } else{
            Yii::app()->user->setFlash('error','Feed\'s could not be added!');
        }
    }
    
    
/**
 * @method public getChildContent() Get the child dropdown based on the parent content types
 * @author Gayadhar<support@muvi.com>
 * @return HTML A dropdown list based on the parent type
 */ 
function actionGetChildContent() {
        if (Yii::app()->request->isAjaxRequest) {
            $text = '';
            if ($_REQUEST['parent_content_type_id'] == 1) {
                $content_child = Yii::app()->general->content_count_child($this->studio->id);
                $text = '<div class="fg-line"><div class="select"><select class="form-control input-sm" id="child_content_type" name="movie[content_types_id]" required="true" onChange="getChildContentTypes();">';
                if (empty($content_child)) {
                    $text.= '<option value="1"> Single Part Long Form</option>
                    <option value="2"> Single Part Short Form</option>
                    <option value="3"> Multi Part</option>';
                } else {
                    if (isset($content_child) && ($content_child['content_count'] & 1)) {
                        $text.= '<option value="1"> Single Part Long Form</option>';
                    }if (isset($content_child) && ($content_child['content_count'] & 2)) {
                        $text.= '<option value="2"> Single Part Short Form</option>';
                    }if (isset($content_child) && ($content_child['content_count'] & 4)) {
                        $text.= '<option value="3"> Multi Part</option>';
                    }
                }
                $text.='</select></div></div>';
                $text.='<span id="hint_text" class="grey" style="font-size: 13px;"></span>&nbsp;<a href="javascript:void(0);" onclick="showhinttext();"><em class="icon-info"></em></a>';
            } elseif ($_REQUEST['parent_content_type_id'] == 2) {
                $text = '<input type="hidden" name="movie[content_types_id]" value="4" />';
            } elseif ($_REQUEST['parent_content_type_id'] == 3) {
                $content_child = Yii::app()->general->content_count_child($this->studio->id);
                $text = '<div class="fg-line"><div class="select"><select class="form-control input-sm" id="child_content_type" name="movie[content_types_id]" required="true" onChange="getChildContentTypes();">';
                if (isset($content_child) && ($content_child['content_count'] & 8)) {
                    $text.= '<option value="5"> Audio Single Part</option>';
                }if (isset($content_child) && ($content_child['content_count'] & 16)) {
                    $text.= '<option value="6"> Audio Multi Part</option>';
				}
                $text.='</select></div></div>';
                $text.='<span id="hint_text" class="grey" style="font-size: 13px;"></span>&nbsp;<a href="javascript:void(0);" onclick="showhinttext();"><em class="icon-info"></em></a>';
            }
            echo $text;
            exit;
        } else {
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit;
        }
    }

    /**
 * @method public addChannel() It will add a channel from the content page
 * @author Gayadhar<support@muvi.com>
 * @return HTML 
 */	
	function actionAddChannel(){
		if(isset($_REQUEST['movie']) && $_REQUEST['movie']){
			$arr['succ']=0;
			$Films = new Film();
			$data = $_REQUEST['movie'];
			$data['content_types_id'] = (@$data['content_types_id'])?$data['content_types_id']:4;
            if (!isset($data['parent_content_type_id']) || empty($data['parent_content_type_id'])) {
                $cmfid = CustomMetadataForm::model()->find(array('select' => 'parent_content_type_id,content_type,is_child', 'condition' => 'id=:id', 'params' => array(':id' => $data['custom_metadata_form_id'])));
                $data['parent_content_type'] = $cmfid['parent_content_type_id'];
            }
			$movie = $Films->addContent($data);
			$movie_id = $movie['id'];
			$arr['uniq_id'] = $movie['uniq_id'];
            if(@$_REQUEST['method_type'] == 'push'){
                $streamUrl = $movie['permalink'].'-'.strtotime(date("Y-m-d H:i:s"));
                $length = 8;
                $userName = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
                $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
                $enc = new bCrypt();
                $passwordEncrypt = $enc->hash($password);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=save&stream_name=".$streamUrl."&user_name=".$userName."&password=".$passwordEncrypt."&serverUrl=".Yii::app()->getBaseUrl(TRUE)."/conversion/stopLiveStreaming?movie_id=".$movie_id);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $server_output = trim(curl_exec($ch));
                curl_close($ch);
                if($server_output != 'success'){
                    Film::model()->deleteByPk($movie_id);
                    Yii::app()->user->setFlash('error','Due to some reason live stream content was unable to create. Please try again');
                    $this->redirect($this->createAbsoluteUrl('admin/manageContent'));exit;
                }
                if(@$_REQUEST['is_recording'] == 1){
                    $_REQUEST['feed_url'] = 'rtmp://'.nginxserverip.'/record/'.$streamUrl;
                    $_REQUEST['stream_url'] = 'rtmp://'.nginxserverip.'/record';
                } else{
                    $_REQUEST['feed_url'] = 'rtmp://'.nginxserverip.'/live/'.$streamUrl;
                    $_REQUEST['stream_url'] = 'rtmp://'.nginxserverip.'/live';
                }
                $_REQUEST['stream_key'] = $streamUrl."?user=".$userName."&pass=".$password;
                $_REQUEST['feed_type'] = 2;
            }
			//Remove sample data from website
			$studio = $this->studio;
			$studio->show_sample_data = 0;
			$studio->save(); 
			
			$studio_id = $this->studio->id;
			//Adding permalink to url routing 
			$urlRouts['permalink'] = $movie['permalink'];
			$urlRouts['mapped_url'] = '/movie/show/content_id/'.$movie_id;
			$urlRoutObj = new UrlRouting();
			$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);
			
			//Insert Into Movie streams table
			$MovieStreams = new movieStreams();
			$MovieStreams->studio_id = Yii::app()->user->studio_id;
			$MovieStreams->movie_id = $movie_id;
			$MovieStreams->is_episode = 0;
			$MovieStreams->created_date = gmdate('Y-m-d H:i:s');
			$MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
			$embed_uniqid = Yii::app()->common->generateUniqNumber();
			$MovieStreams->embed_id = $embed_uniqid; 
			$publish_date = NULL;
			if(@$_REQUEST['content_publish_date']){
				if(@$_REQUEST['publish_date']){
					$pdate = explode('/', $_REQUEST['publish_date']);
					$publish_date = $pdate[2].'-'.$pdate[0].'-'.$pdate[1];
					if(@$_REQUEST['publish_time']){
						$publish_date .= ' '.$_REQUEST['publish_time'];
					}
				}
			}
			$MovieStreams->content_publish_date = $publish_date;
			$MovieStreams->save();

			//Check and Save new Tags into database
			$movieTags = new MovieTag();
			$movieTags->addTags($data);
			if(isset($_REQUEST['content_filter_type'])){
				$contentFilterCls = new ContentFilter();
				$addContentFilter = $contentFilterCls->addContentFilter($_REQUEST,$data['content_type_id']);
			}
			//Upload Poster
			$this->processContentImage(4,$movie_id,$MovieStreams->id);
			$checkfireappletv = Yii::app()->general->CheckApp($studio_id);
			if($checkfireappletv){
				$this->processAppTVImage(4,$movie_id,$MovieStreams->id);
			}
			//Adding the feed infos into live Stream table
			$livestream = new Livestream();
			$livestream->saveFeeds($_REQUEST,Yii::app()->user->studio_id,$movie_id);
			if(HOST_IP !='127.0.0.1'){
                            $solrObj = new SolrFunctions();
                            if($data['genre']){
                            foreach($data['genre'] as $key=>$val){
				$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
				$solrArr['content_id'] = $movie_id;
				$solrArr['stream_id'] = $MovieStreams->id;
				$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
				$solrArr['is_episode'] = 0;
				$solrArr['name'] = $movie['name'];
				$solrArr['permalink'] = $movie['permalink'];
				$solrArr['studio_id'] =  Yii::app()->user->studio_id;
				$solrArr['display_name'] = 'livestream';
				$solrArr['content_permalink'] =  $movie['permalink'];
				$solrArr['genre_val'] = $val;
                                $solrArr['product_format'] = '';                          
                                if($publish_date!=NULL)
                                $solrArr['publish_date'] = $publish_date;
				$ret = $solrObj->addSolrData($solrArr);
			}  
                            }else{
                            $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
				$solrArr['content_id'] = $movie_id;
				$solrArr['stream_id'] = $MovieStreams->id;
				$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
				$solrArr['is_episode'] = 0;
				$solrArr['name'] = $movie['name'];
				$solrArr['permalink'] = $movie['permalink'];
				$solrArr['studio_id'] =  Yii::app()->user->studio_id;
				$solrArr['display_name'] = 'livestream';
				$solrArr['content_permalink'] =  $movie['permalink'];
				$solrArr['genre_val'] = '';
                                $solrArr['product_format'] = '';                          
                                if($publish_date!=NULL)
                                $solrArr['publish_date'] = $publish_date;
				$ret = $solrObj->addSolrData($solrArr);    
                            }
			}  
			if(isset(Yii::app()->user->is_partner)){        
                            $PartnersContent = new PartnersContent();
                            $PartnersContent->studio_id = $studio_id;
                            $PartnersContent->user_id = Yii::app()->user->id;
                            $PartnersContent->movie_id = $movie_id;
                            $PartnersContent->save();
                        }
			Yii::app()->user->setFlash('success','Wow! Channel added successfully.');
			//$this->redirect($this->createAbsoluteUrl('admin/manageContent'));exit;
			$this->redirect(array('admin/editMovie', 'movie_id'=>$movie_id, 'uniq_id'=>$arr['uniq_id'],'movie_stream_id' => $MovieStreams->id));exit;
		}else{
			$this->redirect($this->createAbsoluteUrl('admin/manageContent'));exit;
		}
	}
	/**
* @method public updateChannelDetails() It will update a Channel details
* @return HTML 
* @author Gayadhar<support@muvi.com>
*/
	function actionUpdateChannel(){
	        $studio_id = $this->studio->id;
	        $language_id = $this->language_id;
		if($_REQUEST['movie_id'] && $_REQUEST['uniq_id']){
			$Films = Film::model()->findByAttributes(array('id'=>$_REQUEST['movie_id'],'uniq_id'=>$_REQUEST['uniq_id']));
			if($Films){
                $film_detail = $Films;
                $film_lang = Film::model()->findByAttributes(array('studio_id'=>$studio_id,'language_id'=>$language_id,'parent_id'=>$_REQUEST['movie_id']));
				$data = $_REQUEST['movie'];
                $data['custom10'] = self::createSerializeCustomDate($data);
                $movie_id = $_REQUEST['movie_id'];
                if(!empty($film_lang)){
                    $Films = Film::model()->findByAttributes(array('id' => $film_lang->id, 'uniq_id' => $_REQUEST['uniq_id']));
                }
                if ($Films->language_id == $language_id) {
                    if($Films->parent_id == 0){
				$Films->name = stripslashes($data['name']);
				if(isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
					$Films->release_date = date('Y-m-d',strtotime($data['release_date']));
				else
					$Films->release_date = null;
				$Films->start_time = (@$data['start_time'])?gmdate('Y-m-d H:i:s', strtotime(@$data['start_time'])):NULL;
				$Films->duration = (@$data['duration'])?gmdate('Y-m-d H:i:s', strtotime(@$data['duration'])):NULL;
				$Films->content_category_value = implode(',',$data['content_category_value']);
				$Films->genre = json_encode(array_values(array_unique($data['genre'])));
				$Films->story = stripslashes($data['story']);
				$Films->save();

                        $filmChildData = new Film;
                        $attr = array('content_category_value' => implode(',',$data['content_category_value']));
                        $condition = "parent_id =:id";
                        $params = array(':id'=>$movie_id);
                        $filmChildData = $filmChildData->updateAll($attr,$condition,$params);
				//Check and Save new Tags into database
				$movieTags = new MovieTag();
				$movieTags->addTags($data);
				$movie_id = $Films->id;
				if(isset($_REQUEST['content_filter_type'])){
					$contentFilterCls = new ContentFilter();
					$addContentFilter = $contentFilterCls->addContentFilter($_REQUEST,$Films->content_type_id);
				}
				// Update Livestream Feeds data
                if($_REQUEST['method_type_val'] == 'push'){
                    $liveFeeds['feed_type'] = 2;
                    if(@$_REQUEST['is_recording_val'] == 1 && @$_REQUEST['delete_content'] == 1){
                        $liveFeeds['delete_no'] = @$_REQUEST['delete_no'];
                        $liveFeeds['delete_span'] = @$_REQUEST['delete_span'];
                    } else{
                        $liveFeeds['delete_no'] = 0;
                        $liveFeeds['delete_span'] = '';
                    }
                } else{
                    $liveFeeds['feed_type'] = $_REQUEST['feed_type'];
                    $liveFeeds['feed_url'] = $_REQUEST['feed_url'];
                }
				Livestream::model()->updateAll($liveFeeds,'id='.$_REQUEST['livestream_id'].' AND studio_id='.Yii::app()->user->studio_id);
				//Upload Poster
				$this->processContentImage(4,$movie_id);
				$checkfireappletv = Yii::app()->general->CheckApp($studio_id);
				if($checkfireappletv){
					$this->processAppTVImage(4, $movie_id);
				}
                    }else{
                        $Films->name = stripslashes($data['name']);
                        $Films->genre = json_encode(array_values(array_unique($data['genre'])));
                        $Films->story = stripslashes($data['story']);
                        $Films->save();
                    }
					$Films->censor_rating = @$data['censer_rating'] ? (is_array(@$data['censer_rating']) ? json_encode(@$data['censer_rating']) : @$data['censer_rating']) : '';
					$Films->language = @$data['language'] ? (is_array(@$data['language']) ? json_encode(@$data['language']) : @$data['language']) : '';
                    $Films->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                    $Films->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                    $Films->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                    $Films->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                    $Films->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                    $Films->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                    $Films->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                    $Films->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                    $Films->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                    $Films->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                    $Films->save();
				
				Yii::app()->user->setFlash('success','Wow! Your channel updated successfully');
				$this->redirect($this->createUrl('admin/managecontent'));exit;
			}else{
                    $film = new Film();
                    $film->name = stripslashes($data['name']);
                    $film->uniq_id = $film_detail->uniq_id;
                    $film->studio_id = $studio_id;
                    $film->genre = json_encode(array_values(array_unique($data['genre'])));
                    $film->story = stripslashes($data['story']);
                    $film->content_types_id = $film_detail->content_types_id;
                    $film->content_category_value = $film_detail->content_category_value;
                    $film->status = 1;
                    $film->permalink = $film_detail->permalink;
                    $film->parent_id = $movie_id;
                    $film->language_id = $language_id;
					$film->censor_rating = @$data['censer_rating'] ? (is_array(@$data['censer_rating']) ? json_encode(@$data['censer_rating']) : @$data['censer_rating']) : '';
					$film->language = @$data['language'] ? (is_array(@$data['language']) ? json_encode(@$data['language']) : @$data['language']) : '';
                    $film->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                    $film->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                    $film->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                    $film->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                    $film->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                    $film->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                    $film->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                    $film->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                    $film->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                    $film->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                    $film->save();
                    Yii::app()->user->setFlash('success', 'Wow! Your channel updated successfully');
                    $this->redirect($this->createUrl('admin/managecontent'));
                    exit;
                }} else {
				Yii::app()->user->setFlash('error','Oops! Sorry error in updating your data');
				$this->redirect($this->createUrl('admin/managecontent'));exit;
			}	
		}else{
			Yii::app()->user->setFlash('error','Oops! Sorry you are not authorised to access this data');
			$this->redirect($this->createUrl('admin/managecontent'));exit;
		}
	}
/**
 * @method public processcontentimage() It will crop and upload the image into the respective bucket with mentioned size
 * @author Gayadhar<support@muvi.com>
 * @return array Image path array
 */
	function processContentImage($content_types_id=1,$movie_id='',$stream_id='',$is_episode=''){
            $studio_id = Yii::app()->common->getStudiosId();
            if($movie_id){
                $cmsql = "SELECT poster_size FROM custom_metadata_form WHERE id = (SELECT custom_metadata_form_id FROM films WHERE id=".$movie_id.")";
                $cmfid = Yii::app()->db->createCommand($cmsql)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
            }
            if(isset($cmfid) && !empty($cmfid) && $cmfid->poster_size !=''){
                $vertical = strtolower($cmfid->poster_size);
                $horizontal = strtolower($cmfid->poster_size);
            }else{
                $poster_sizes = Yii::app()->general->getPosterSize($studio_id);
                $vertical = strtolower($poster_sizes['v_poster_dimension']);
                $horizontal = strtolower($poster_sizes['h_poster_dimension']);
            }
         
		$poster_object_type = 'films';
                $object_id=$movie_id;
		if($is_episode){
			$poster_object_type ='moviestream';
			$object_id = $stream_id;
		}
		$thumb_size = $vertical;
                $standard_size = $vertical;
 
		$cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);
		if($content_types_id==2 || $content_types_id==4 || $is_episode){
			$cropDimension['episode'] = $horizontal;
		}
		
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $movie_id;
		if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
			$cdimension = array('thumb' => "64x64");
			$ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);
			
			$path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
			$ret = $this->uploadPoster($_FILES['Filedata'], $object_id, $poster_object_type, $cropDimension, $path);
			if ($movie_id) {
				Yii::app()->common->rrmdir($dir);
			}
			if($content_types_id==2 || $is_episode){
				movieStreams::model()->updateByPk($stream_id,array('is_poster'=>1));
			}
			return $ret;
		} else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {
			
			$file_info = pathinfo($_REQUEST['g_image_file_name']);
			$_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
			$jcrop_allimage = $_REQUEST['jcrop_allimage'];
			$image_name = $_REQUEST['g_image_file_name'];

			$dimension['x1'] = $jcrop_allimage['x13'];
			$dimension['y1'] = $jcrop_allimage['y13'];
			$dimension['x2'] = $jcrop_allimage['x23'];
			$dimension['y2'] = $jcrop_allimage['y23'];
			$dimension['w'] = $jcrop_allimage['w3'];
			$dimension['h'] = $jcrop_allimage['h3'];
			
			$path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
			$fileinfo['name'] = $_REQUEST['g_image_file_name'];
			$fileinfo['error'] = 0;
			$ret = $this->uploadPoster($fileinfo, $object_id, $poster_object_type, $cropDimension, $path);
			Yii::app()->common->rrmdir($dir);
			if($content_types_id==2 || $is_episode){
				movieStreams::model()->updateByPk($stream_id,array('is_poster'=>1));
			}
			return $ret;
		} else {
			return false;
		}
	}
    /* Bellow Functions are for physical goods */
    public function actiongetContentName() {
        $contentarray = Yii::app()->db->createCommand("SELECT id,name FROM films WHERE studio_id=" . $this->studio->id)->queryAll();
        foreach ($contentarray as $key => $value) {
            $array[$value['id']] = $value['name'];
        }
        echo json_encode($array);
        exit;
    }

    function actionproduct_autocomplete() {
        $res = array();
        $studio_id = Yii::app()->user->studio_id;

        if (isset($_REQUEST['search']) && trim($_REQUEST['search'])) {
            $srch = trim($_REQUEST['search']);
            $sql = "SELECT po.* FROM pg_product po WHERE po.studio_id={$studio_id} AND po.name LIKE '%{$srch}%' AND is_deleted = 0 ORDER BY po.name LIMIT 0, 10";

            $con = Yii::app()->db;
            (array) $data = $con->createCommand($sql)->queryAll();

            $rec = array();
            if (isset($data) && !empty($data)) {
                foreach ($data as $key => $value) {
                    $rec = array('value' => $value['id'], 'text' => $value['name']);
                    if (isset($rec) && !empty($data)) {
                        $res[] = $rec;
                    }
                }
            }
        } else {
            $res = array(
                array('value' => '0', 'text' => 'No data found!')
            );
        }

        echo json_encode($res);
        exit;
    }

    function actionOpenAddItem(){
        $this->layout = false;
        $command = Yii::app()->db->createCommand()
                ->select('p.id, p.name')
                ->from('pg_product p ,pg_product_content ppc ')
                ->where('p.id = ppc.product_id AND ppc.movie_id=' . $_REQUEST['content_id']);

        $data = $command->queryAll();

        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $rec = array('id' => $value['id'], 'name' => $value['name']);
                if (isset($rec) && !empty($rec)) {
                    $movies[] = $rec;
                }
            }
        }
        $this->render('ajaxadditem', array('content_id' => $_REQUEST['content_id'], 'movies' => $movies));
    }
    function actionaddItems() {
        $content_id = $_REQUEST['content_id'];
        $data = $_REQUEST['product_id'];
        //delete previous data
        PGProductContents::model()->deleteAll("movie_id =:movie_id", array(':movie_id' => $content_id));
        $goods = new PGProduct();
        $pcModel = New PGProductContents;
        foreach($data as $val)
        {
            if($val != '')
            {                
                //check if already assigned or not
                $count = 0;
                $count = PGProductContents::model()->countByAttributes(array('product_id'=> $val,'movie_id'=>$content_id));
                //if not assigned save the record
                if($count == 0)
                {
                    //update standalone to Link to content                    
                    $pggoods = $goods->findByPk($val);
                    $pggoods->product_type = 1;
                    $pggoods->save();
                    //add content to product content table
                    $pcModel->product_id = $val;
                    $pcModel->movie_stream_id = 0;
                    $pcModel->movie_id = $content_id;
                    $pcModel->isNewRecord = TRUE;
                    $pcModel->primaryKey = NULL;
                    $pcModel->save();
                }
            }
        }
        echo 1;
        exit;
    }
    /*
     * issue #4885
     * Author @manas@muvi.com*/
   function InsertDefaultHelp() {
        $default_help = array(0 => array('title' => 'HOW CAN I CHANGE THE SUBTITLE OF A MOVIE?', 'content' => 'You will find the subtitle button at the bottom right corner of the player, right beside the volume control as the movie starts playing. If you hover on the bubble icon, the subtitles or different languages should appear instantly and you can scroll down to see the subtitle selections/languages. From the subtitle list, you can select the subtitle you want.'),
            1 => array('title' => 'HOW CAN I GO BACK TO THE CONTENT PAGE WHILE WATCHING VIDEO?', 'content' => 'On your player, you can see a back button “<--” on the top left hand side corner; by clicking on this back button you can go back to the content page.'),
            2 => array('title' => 'CAN I CANCEL MY SUBSCRIPTION AT ANY TIME I WANT?', 'content' => 'Yes, you can cancel your subscription at any time you want. But, remember that, once you cancel your subscription you will not be able to access any content from your website. However, you can browse your website.'),
            3 => array('title' => 'CAN I PLAY/PAUSE VIDEO BY USING MY KEYBOARD?', 'content' => 'Yes, you can press the “Spacebar” of your keyboard to play/pause the video.'),
            4 => array('title' => 'I AM A REGISTERED USER. WHY I AM UNABLE TO WATCH ANY FREE VIDEO CONTENT ON YOUR WEBSITE?', 'content' => 'To watch any free video content on our website, first you have to login to our website.  '),
        );
        $studio_id = Yii::app()->common->getStudiosId();
        Faq::model()->deleteAll("studio_id =:studio_id", array(':studio_id' => $studio_id));
        $faq = new Faq();
        foreach ($default_help as $item) {
            $faq->studio_id = $studio_id;
            $faq->title = Yii::app()->common->encode_to_html($item['title']);
            $faq->content = Yii::app()->common->encode_to_html($item['content']);
            $faq->date_added = date('Y-m-d h:i:s');
            $faq->isNewRecord = true;
            $faq->primaryKey = NULL;
            $faq->save();
        }
    }
    
    /* issue no = 4930
     * Author <manas@muvi.com>
     * FireTV and AppleTV Poster Image*/
    function processAppTVImage($content_types_id=1,$movie_id='',$stream_id='',$is_episode=''){
        $studio_id = Yii::app()->common->getStudiosId();
        $vertical = '800x600';
        $horizontal = '800x600';

        $poster_object_type = 'tvapp';
        $object_id=$movie_id;
        if($is_episode){
            $object_id = $stream_id;
}
        $thumb_size = $vertical;
        $standard_size = $vertical;

        $cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);
        if($content_types_id==2 || $content_types_id==4 || $is_episode){
            $cropDimension['episode'] = $horizontal;
        }

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $movie_id;
        if (isset($_FILES['Filedata_fire']) && !($_FILES['Filedata_fire']['error'])) {
            $cdimension = array('thumb' => "64x64");
            $ret1 = $this->uploadToImageGallery($_FILES['Filedata_fire'], $cdimension);

            $path = Yii::app()->common->jcropImage($_FILES['Filedata_fire'], $dir, $_REQUEST['fileimage_fire']);
            $ret = $this->uploadPoster($_FILES['Filedata_fire'], $object_id, $poster_object_type, $cropDimension, $path);
            if ($movie_id) {
                Yii::app()->common->rrmdir($dir);
            }
            return $ret;
        } else if ($_FILES['Filedata_fire']['name'] == '' && $_REQUEST['g_image_file_name_fire'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name_fire']);
            $_REQUEST['g_image_file_name_fire'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $jcrop_allimage = $_REQUEST['jcrop_allimage_fire'];
            $image_name = $_REQUEST['g_image_file_name_fire'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];

            $path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name_fire'], $_REQUEST['g_original_image_fire'], $dir, $dimension);
            $fileinfo['name'] = $_REQUEST['g_image_file_name_fire'];
            $fileinfo['error'] = 0;
            $ret = $this->uploadPoster($fileinfo, $object_id, $poster_object_type, $cropDimension, $path);
            Yii::app()->common->rrmdir($dir);            
            return $ret;
        } else {
            return false;
        }
    }
    function actionRemoveAppTvPoster() {
        $poster = new Poster();
        //Check the movie id blongs from the logged in users studio
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
            $movie_id = $_REQUEST['movie_id'];
        }
        $obj_type = $_REQUEST['obj_type'];
        $movie_stream_id = $_REQUEST['movie_stream_id'];

        if ($movie_id) {
            $movie = Film::model()->find('id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->common->getStudiosId()));
            if ($movie) {
                if ($obj_type != 1) {
                    $ret = Yii::app()->db->createCommand()
                            ->delete('posters', 'object_id=:movie_id AND object_type=:type', array(':movie_id' => $movie_id, ':type' => 'tvapp'));
                } else {
                    $ret = Yii::app()->db->createCommand()
                            ->delete('posters', 'object_id=:movie_id AND object_type=:type', array(':movie_id' => $movie_stream_id, ':type' => 'tvapp'));
                }
                if ($ret) {
                    $arr['msg'] = 'Poster removed successfully.';
                    $arr['err'] = 0;
                } else {
                    $arr['msg'] = 'Error in deleting Poster.';
                    $arr['err'] = 1;
                }
            } else {
                $arr['msg'] = 'You are not authorised to delete this Poster';
                $arr['err'] = 1;
            }
        } else {
            $arr['msg'] = 'Invalid Poster';
            $arr['err'] = 1;
        }
        if ($_REQUEST['is_ajax']) {
            echo json_encode($arr);
            exit;
        }
    }
    
    public function actionManageContentorder() {
        $this->breadcrumbs = array('Manage Content','Content Library','Content Ordering');
        $this->pageTitle = Yii::app()->name . ' | ' . 'Content Ordering';
        $this->headerinfo = "Content Ordering";
        $this->layout = 'admin';
        $studio_id=Yii::app()->user->studio_id;
        $language_id=$this->language_id;
        $con=Yii::app()->db;
        //get all category name
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();

        $contentTypes = CHtml::listData($contenttypeList, 'id', 'category_name');
        if ($contenttypeList[0]['id'] > 0) {
            $data = $this->getcategorywisecontent($contenttypeList[0]['id']);
        //check whether subcategory enabled for studio or not        
        $sql_subcategory="select * from studio_config where config_key='subcategory_enabled' and studio_id=".studio_id." and config_value=1";
        $sub_catexists=$con->createCommand($sql_subcategory)->queryAll();
        if(count($sub_catexists)){
            
			$sub_cat = "SELECT * FROM `content_subcategory` WHERE studio_id =" . $studio_id . " AND FIND_IN_SET(" . $contenttypeList[0]['id'] . ",category_value)";
        $all_sub_cat=Yii::app()->db->createCommand($sub_cat)->queryAll();   
        } 
		} else {
         $data=array(); 
         $all_sub_cat=array();
        }
        
         if($data) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .="'" . $v['stream_id'] . "',";
                } else {
                    $movieids .="'" . $v['id'] . "',";
                }
               
            //items aassociated with this content
                $sql = "SELECT product_id FROM pg_product_content WHERE movie_id =".$v['id'];
                $itemLog = $con->createCommand($sql)->queryAll();
                if ($itemLog) {
                    foreach ($itemLog as $val) {
                        $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $val['product_id']));
                        $data[$k]['product_name'] .= $pgproduct['name']."<br />";
                    }
                }       
            }
            
//             echo "Stream id =".$stream_ids;
//                echo "Movie id =".$movieids;
//                exit;
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
              
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['video_id']] = $val['cnt'];
                    }
                }
            }

            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }
        $cos = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ' . $orderCond, array(':studio_id' => $studio_id, ':category_value' => $contenttypeList[0]['id']));
        $this->render('managecontentorder',array( 'data' => $data, 'category' => @$contentTypes,'sub_category'=>@$all_sub_cat,'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount,'cos'=>$cos));
        $dbcon->active=false; 
        exit;
    }
    
    public function getcategorywisecontent($content_category_value=0){
        
        $studio_id=Yii::app()->user->studio_id;
        $language_id=$this->language_id;
        $con = Yii::app()->db;
        
        $params = array(':studio_id' => $studio_id);
        $pdo_cond_arr = array(":studio_id" => $studio_id);
        $pdo_cond = " f.studio_id=:studio_id ";
        
       
        if ($content_category_value) {
			$pdo_cond .= " AND FIND_IN_SET($content_category_value,f.content_category_value) ";
            /*$pdo_cond .= " AND (f.content_category_value & :content_category_value) ";
            $pdo_cond_arr[':content_category_value'] = $content_category_value;*/
                   }
        
        
        $search_text = '';
        $default_lang_id = 20;
       
        $orderby = ' ms.last_updated_date DESC ';
      //  $orderData = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ',array(':studio_id'=>$studio_id,':category_value'=>$content_category_value));
	
      $sql_saved_order="select ordered_stream_ids from content_ordering where studio_id=".$studio_id." and category_value=".$content_category_value;     
      $orderData=$con->createCommand($sql_saved_order)->queryAll();
    
        if($orderData[0]['ordered_stream_ids']!=''){
         //$orderby = " FIELD(ms.movie_stream_id,".$orderData->ordered_stream_ids.")"; 
         $orderby ="FIELD(ms.id,".$orderData[0]['ordered_stream_ids'].")";   
        }
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }

       
        //PDO Query
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
        $pcontent = Yii::app()->general->getPartnersContentIds();
            $command = Yii::app()->db->createCommand()
		->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_category_value,f.content_types_id,f.name,f.ppv_plan_id,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,ms.thirdparty_url,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,ms.is_download_progress,ms.video_management_id,f.language_id,f.parent_id,ms.episode_parent_id,ms.episode_language_id,ms.is_demo')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND ms.is_episode=0 AND f.parent_id=0 AND ms.episode_parent_id=0  AND ' . $pdo_cond, $pdo_cond_arr)
                ->andwhere(array('in', 'f.id', explode(',',$pcontent['movie_id'])))
                ->order($orderby);
                
        }else{          
             $command = Yii::app()->db->createCommand()
	        ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_category_value,f.content_types_id,f.name,f.ppv_plan_id,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,ms.thirdparty_url,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,ms.is_download_progress,ms.video_management_id,f.language_id,f.parent_id,ms.episode_parent_id,ms.episode_language_id,ms.is_demo')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND ms.is_episode=0 AND f.parent_id=0 AND ms.episode_parent_id=0 AND ' . $pdo_cond, $pdo_cond_arr)                
                ->order($orderby);               
            }
        $data = $command->queryAll();
      
        return $data;
       
    }
    
    public function actionsaveDroppedPosition()
    {
        $studio_id=Yii::app()->user->studio_id;
        $stream_ids = $_POST['stream_id'];
        $category_binary = $_REQUEST['category'];
        $method = is_numeric($_POST['method'])?$_POST['method']:0;
        $orderby_flag = is_numeric($_POST['orderby_flag'])?$_POST['orderby_flag']:0;
       // $subcategory_binary = $_REQUEST['sub_category'];
        $list_stream_id = count($stream_ids) > 1 ? implode(',', $stream_ids) : $stream_ids[0];       
       //check if records exists for the studio id 
        $checkorder_sql="select id from content_ordering where studio_id=".$studio_id." and category_value=".$category_binary;
        $get_studio_id = Yii::app()->db->createCommand($checkorder_sql)->queryAll();
        if(isset($get_studio_id[0]['id']) && $get_studio_id[0]['id']>0){
        $update_sql="update content_ordering set ordered_stream_ids='".$list_stream_id."',method='".$method."',orderby_flag='".$orderby_flag."' where category_value=".$category_binary." and id=".$get_studio_id[0]['id'];  
        }else {
        $update_sql="insert into content_ordering(studio_id,category_value,subcategory_value,movie_id,ordered_stream_ids,is_episode,method,orderby_flag)values(".$studio_id.",".$category_binary .",0,0,'". $list_stream_id."',0,'".$method."','".$orderby_flag."')";
        }
       
        Yii::app()->db->createCommand($update_sql)->execute();         
        exit;      
    }
    
    public function actiongetSubcategory() {
        $studio_id=Yii::app()->user->studio_id;
        $category_binaryval=$_REQUEST['category'];       
        $sub_cat = "SELECT * FROM `content_subcategory` WHERE studio_id =" . $studio_id . " AND FIND_IN_SET($category_binaryval,category_value)";
        $all_sub_cat=Yii::app()->db->createCommand($sub_cat)->queryAll(); 
        $option="";
        if(!empty($all_sub_cat)){
        foreach($all_sub_cat as $key=>$val){
                $option .= '<option value="' . $val['id'] . '">' . $val['subcat_name'] . '</option>';
        }             
        } else {
          $option='empty';  
        }
        echo $option;  
      exit;     
    }
    
    public function actiongetAllList() {
        $studio_id = $this->studio->id;
     $this->layout=false;
     $con=Yii::app()->db;
    
     $category=$_POST['category_id'];
     $data=$this->getcategorywisecontent($category);
     
         if($data) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .="'" . $v['stream_id'] . "',";
                } else {
                    $movieids .="'" . $v['id'] . "',";
                }
               
            //items aassociated with this content
                $sql = "SELECT product_id FROM pg_product_content WHERE movie_id =".$v['id'];
                $itemLog = $con->createCommand($sql)->queryAll();
                if ($itemLog) {
                    foreach ($itemLog as $val) {
                        $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $val['product_id']));
                        $data[$k]['product_name'] .= $pgproduct['name']."<br />";
                    }
                }       
            }
            
//             echo "Stream id =".$stream_ids;
//                echo "Movie id =".$movieids;
//                exit;
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
              
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['video_id']] = $val['cnt'];
                    }
                }
            }

            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }
        $cos = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ' . $orderCond, array(':studio_id' => $studio_id, ':category_value' => $category));
        
        $this->renderPartial('rendercontentorder',array( 'data' => $data, 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount,'cos'=>$cos));
           
    }
    
    public function actionembedThirdPartyPlatformForTrailer() 
        { 
            $studio_id = Yii::app()->common->getStudiosId();
            if (isset($_REQUEST['movie_id'])){
            $url = $_REQUEST['third_party_url'];
            $movieId = $_REQUEST['movie_id'];
            $model = (isset($_REQUEST['type']) && $_POST['type']=='physical')?'PGMovieTrailer':'movieTrailer';
            $date = date('Y-m-d H:i:s');
            $ext = pathinfo($url, PATHINFO_EXTENSION);
            if ($movieId) {
               if ((trim($ext)  == 'm3u8') || 
                (strpos($url, 'youtube.com') !== false) && (strpos($url, 'iframe') !== false) || 
                (strpos($url, 'vimeo.com') !== false) && (strpos($url, 'iframe') !== false)|| 
                (strpos($url, 'dailymotion.com') !== false) && (strpos($url, 'iframe') !== false))  
              {
            $getMovieTrailer = $model::model()->findByAttributes(array('movie_id' => $movieId));
            if (!empty($getMovieTrailer)) {
            $getMovieTrailer -> third_party_url = $url;
            $getMovieTrailer -> is_converted = 1;
            $getMovieTrailer ->trailer_file_name = '3rd party url.mp4';
            $getMovieTrailer ->has_sh = 0;
            $getMovieTrailer ->video_management_id = 0;
            $getMovieTrailer ->trailer_content_type = NULL;
            $getMovieTrailer ->video_resolution = NULL;
            $getMovieTrailer ->resolution_size = NULL;
            $getMovieTrailer ->video_duration = NULL;
            $getMovieTrailer ->upload_start_time = $date;
            $getMovieTrailer ->upload_end_time = $date;
            $getMovieTrailer ->upload_cancel_time = NULL;
            $getMovieTrailer ->encoding_start_time = $date;
            $getMovieTrailer ->encoding_end_time = $date;
            $getMovieTrailer ->encode_fail_time = NULL;
            $getMovieTrailer ->mail_sent_for_trailer = 1;
            $getMovieTrailer -> save();
            }else{
            $getMovieTrailer = new $model();
            $getMovieTrailer->movie_id = $movieId;
            $getMovieTrailer ->trailer_file_name = '3rd party url.mp4';
            $getMovieTrailer -> third_party_url = $url;
            $getMovieTrailer -> is_converted = 1;
            $getMovieTrailer ->has_sh = 0;
            $getMovieTrailer->created_by = Yii::app()->user->id;
            $getMovieTrailer->created_date = gmdate('Y-m-d');
            $getMovieTrailer->save();
            return $getMovieTrailer->id;
            }
            if($getMovieTrailer -> save())
            {
            Yii::app()->user->setFlash('success', 'Embed URL is added successfully');
            } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Embed URL is not added!');
            }
            }
        }
    }
}


    /****** 
     * Ticket #:5849,Import Metadata
     * Author:<suraja@muvi.com> 
     * Details:Bulk content creation by importing the data in excel/csv
     ******/
    public function actionimportContents() {

        $studio_id = Yii::app()->user->studio_id;

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/content_excel';
        if (!is_dir($dir)) {
            mkdir($dir);
            @chmod($dir, 0777);
        }
        $filename = Yii::app()->session['filename'];
        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/content_excel/' . $filename;

        $error_exists = $this->importExcel($path);

        unlink($path);
        if(is_numeric($error_exists)){
        Yii::app()->user->setFlash('success',$error_exists.' rows uploaded successfully');
        }   

        echo $error_exists;
    }

     public function importExcel($path) {
        require 'PHPExcel/PHPExcel/IOFactory.php';
        $studio_id = Yii::app()->user->studio_id;
        //read excel data
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($path);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($path, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $format="Y-m-d";
        foreach ($allDataInSheet as $key=> $val) {
            //content date
            if($key>1){
                
				$release_date=explode('-',$val['D']);
				$month=$release_date[0];
				$day=$release_date[1];
				$year=$release_date[2];
				$allDataInSheet[$key]['D']=$year."-".$month."-".$day;

				$eps_release_date=explode('-',$val['M']);
				$epmonth=$eps_release_date[0];
				$epday=$eps_release_date[1];
				$epyear=$eps_release_date[2];
				$allDataInSheet[$key]['M']=$epyear."-".$epmonth."-".$epday;
		   }
        }
        
        $error[] = (trim($allDataInSheet[1]['A']) != 'Content Name') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['B']) != 'Content Types') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['C']) != 'Metadata Type') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['D']) != 'Release/Record Date') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['E']) != 'Genre') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['F']) != 'Language') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['G']) != 'Censor Ratings') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['H']) != 'Story/Description') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['I']) != 'Content Category') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['J']) != 'Parent Content') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['K']) != 'Season#') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['L']) != 'Episode#') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['M']) != 'Episode Date') ? 1 : 0;
        $error[] = (trim($allDataInSheet[1]['N']) != 'URL') ? 1 : 0;
		$custom_flag = (trim($allDataInSheet[1]['O']) == 'Custom Metadata Form') ? 1 : 0;
     
        
        //count the records in a sheet
        $arrayCount = count($allDataInSheet);
        
        if (!in_array("1",$error)) {
            $countnoempty = 0;
            if ($arrayCount > 1 && $arrayCount <= 2000) {
                for ($i = 2; $i <= $arrayCount; $i++) {
                    $content_name = $allDataInSheet[$i]["A"];
                    $content_type = $allDataInSheet[$i]["B"];
                    $metedata_type = $allDataInSheet[$i]["C"];
                    $content_category = $allDataInSheet[$i]["I"];

                    if ($content_name != '' && $content_type != '' && $metedata_type != '' && $content_category != '') {
                       
                        if ($content_type == 1) { //for VOD 
                            $countnoempty=$countnoempty+1;
							if ($metedata_type == 1 || $metedata_type == 3 || $metedata_type == 4) {
								//1 = vod single, 3 = vod multipart parent, 4 = vod mulptipart child
								$film_name = ($metedata_type == 4) ? $allDataInSheet[$i]['J'] : $allDataInSheet[$i]['A'];
                               if(trim($film_name)!=''){
									$content_types_id = ($metedata_type != 4) ? $metedata_type : 3;
                                $parent_content_type_id = $allDataInSheet[$i]['B'];
                             // $exists= Film::model()->exists('name=:name AND content_types_id=:content_types_id AND parent_content_type_id=:parent_content_type_id',array(":name"=>trim($film_name),":content_types_id"=>$content_types_id,":parent_content_type_id"=>$parent_content_type_id));
									$check_content = "select id from films where studio_id=" . $studio_id . " and name='" . trim($film_name) . "' and content_types_id=" . $content_types_id . " and parent_content_type_id=" . $parent_content_type_id;
                                    $content_exists = Yii::app()->db->createCommand($check_content)->queryAll();
									if ($metedata_type == 4) {
                                    if (count($content_exists)<1) {
											$test_id = $this->insertFilms($allDataInSheet[$i], $custom_flag, $allDataInSheet[1]);
                                    } else {
                                        $test_id = $content_exists[0]['id'];
                                    }
									} else {
										$test_id = $this->insertFilms($allDataInSheet[$i], $custom_flag, $allDataInSheet[1]);
                                   }
									if ($test_id) {
										$qry_film="select * from films where id=".$test_id;
										$movie=Yii::app()->db->createCommand($qry_film)->queryAll();                 
										$res = $this->addExcelContent($movie[0], $allDataInSheet[$i], $custom_flag, $allDataInSheet[1]);
									} else {
										return "wrongmeta";
									}
								} else {
									return "parenterr";   
								}
                            }
                        } else {
                                return "wrongmeta";
                            }
                        } else{
                            //for VLS 
                            return "wrongcont";
                    }
                }
                $countempty = ($arrayCount - 1) - $countnoempty;
                //delete the uploaded file.
                return $countnoempty;
            } else {
                return "countexceed";
            }
        } else {
            return "headerissue";
        }
    }

	public function insertFilms($allDataInSheet, $custom_flag, $sheet_heading) {
        $Films = new Film();
		if ($custom_flag && ($allDataInSheet['O'] != '') && ($allDataInSheet['C'] != 4)) {//add custom meta data for vod single and vod multi parent
			self::importCustomFields($Films, $allDataInSheet, $sheet_heading, 1);
		}
		if ($allDataInSheet['C'] == 4) {
			$permalinkins = $allDataInSheet['J'];
        }else {
			$permalinkins = $allDataInSheet['A'];
        }
        $Films->studio_id = Yii::app()->user->studio_id;
		if ($allDataInSheet['C'] == 4) {
        //multipart child content
			$Films->name = stripslashes($allDataInSheet['J']);
			 $Films->story ="";
			if (isset($allDataInSheet['M']) && $allDataInSheet['M'] != '1970-01-01' && strlen(trim($allDataInSheet['M'])) > 6) {
				$Films->release_date =$allDataInSheet['M'];
			}
			$getcustomid_sql = "SELECT id FROM custom_metadata_form WHERE "
					. "studio_id=" . Yii::app()->user->studio_id . " AND "
					. "parent_content_type_id = 1 AND content_type = 1 AND is_child = 0";
			$getcustomid = Yii::app()->db->createCommand($getcustomid_sql)->queryROW();
			if (empty($getcustomid)) {
				$getcustomid_sql = "SELECT id FROM custom_metadata_form WHERE "
						. "studio_id=0 AND parent_content_type_id = 1 AND content_type = 1 AND is_child = 0";
				$getcustomid = Yii::app()->db->createCommand($getcustomid_sql)->queryROW();
			}
			if (!empty($getcustomid)) {
				self::importCustomFields($Films, $allDataInSheet, $sheet_heading, 1, @$getcustomid['id']);
			}
        }else{
			$Films->name = stripslashes($allDataInSheet['A']);
			$Films->story = stripslashes($allDataInSheet['H']);
			if (isset($allDataInSheet['D']) && $allDataInSheet['D'] != '1970-01-01' && strlen(trim($allDataInSheet['D'])) > 6) {
				$Films->release_date = $allDataInSheet['D'];
			}
        }
        $uniqid = Yii::app()->common->generateUniqNumber();
        $Films->uniq_id = $uniqid;
       
       
        //$this->content_type_id = $data['content_type_id'];
        $Films->language = json_encode(explode(';', $allDataInSheet['F']));
        $Films->genre = json_encode(explode(';', $allDataInSheet['E']));
        $Films->censor_rating = json_encode(explode(';', $allDataInSheet['G']));
       
        $Films->created_date = gmdate('Y-m-d H:i:s');
        $Films->last_updated_date = gmdate('Y-m-d H:i:s');

        $Films->content_types_id = ($allDataInSheet['C'] != 4) ? $allDataInSheet['C'] : 3;

        $Films->parent_content_type_id = $allDataInSheet['B'];

        //if the category values are given in names
        //get the category id and map with the table and update the rows.
        //if not present then insert into table and update the new id.
        $category_name=$allDataInSheet['I'];
        $language_id = $this->language_id;
        $chk_cat="select * from content_category where category_name='".$category_name."' and studio_id=".Yii::app()->user->studio_id." and language_id=".$language_id;
        $cat_detail = Yii::app()->db->createCommand($chk_cat)->queryAll();
        
        if(isset($cat_detail[0]['id']) && $cat_detail[0]['id']!=''){
			$cat_id = $cat_detail[0]['id'];
         }else{
          $contentModel = new ContentCategories(); 
          $sql = " SELECT binary_value FROM content_category WHERE studio_id=" . Yii::app()->user->studio_id . " AND parent_id = 0 ORDER BY binary_value ASC";
            //$sql = 'SELECT MAX(binary_value) maxBvalue FROM content_category WHERE studio_id =' . Yii::app()->user->studio_id;
            $bvalue = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($bvalue AS $key => $val) {
                if ($val['binary_value'] != pow(2, $key)) {
                    $binary_value = pow(2, $key);
                    break;
                }
            }
            if (!@$binary_value) {
                $binary_value = pow(2, @$key + 1);
            }
            $Menus = Menu::model()->find('studio_id=:studio_id AND position=\'top\'', array(':studio_id' => Yii::app()->user->studio_id));
                $ip_address = CHttpRequest::getUserHostAddress();
                $contentModel->studio_id = Yii::app()->user->studio_id;
                $permalink = Yii::app()->general->generatePermalink($category_name);
                $contentModel->permalink = $permalink;
                $contentModel->binary_value = $binary_value;
                $contentModel->category_name = $category_name;
                $contentModel->created_date = new CDbExpression("NOW()");
                $contentModel->added_by = Yii::app()->user->id;
                $contentModel->ip = $ip_address;
                $contentModel->is_poster =0;
                $contentModel->save();
			$cat_id = $contentModel->id;
        
                if (@$Menus->auto_categories == '1') {
                    //Adding to Menu Items table by default
                    $menuItems = new MenuItem();
                    $menuItems->title = $category_name;
                    $menuItems->menu_id = @$Menus->id;
                    $menuItems->permalink = $permalink;
				$menuItems->value = $cat_id;
                    $menuItems->studio_id = Yii::app()->user->studio_id;
                    $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                    $menuItems->save();

                    //Adding data to Url routing table
                    $urlRouting = new UrlRouting();
                    $urlRouting->permalink = $permalink;
                    $urlRouting->mapped_url = '/media/list/menu_item_id/' . $menuItems->id;
                    $urlRouting->studio_id = Yii::app()->user->studio_id;
                    $urlRouting->created_date = new CDbExpression("NOW()");
                    $urlRouting->ip = $ip_address;
                    $urlRouting->save();
                }
        }
        
        $Films->content_category_value = $cat_id;
        $Films->permalink = Yii::app()->general->generatePermalink(stripslashes($permalinkins));
        $Films->status = 1;

        $Films->setIsNewRecord(true);
        $Films->setPrimaryKey(NULL);

		if ($Films->save()) {
			$content_id = $Films->id;
			$film = Film::model()->findByPk($content_id);
			$film->search_parent_id = $content_id;
			$film->save();
			return $content_id;
		} else {
			return 0;
		}
	}

	public function addExcelContent($movie, $allDataInSheet, $custom_flag, $sheet_heading) {
        //Adding permalink to url routing 
        $urlRouts['permalink'] = $movie['permalink'];
        $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie['id'];
        $urlRoutObj = new UrlRouting();
        $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts);

        $movie_id = $movie['id'];
        $arr['uniq_id'] = $movie['uniq_id'];
        //Remove sample data from website
        $studio = $this->studio;
        $studio->show_sample_data = 0;

        $studio->save();
        
        //check whether parent record is there or not if not insert parent record else ignore the block
        $studio_id=Yii::app()->user->studio_id;
		if ($allDataInSheet['C'] == 4) {
			$check_parent="select * from movie_streams where studio_id=".$studio_id." and  movie_id=".$movie['id']." and is_episode=0";
			$parent_epi= Yii::app()->db->createCommand($check_parent)->queryAll();
			if(count($parent_epi)==0){
				$MovieStreamparents = new movieStreams();
				$MovieStreamparents->studio_id = Yii::app()->user->studio_id;
				$MovieStreamparents->movie_id = $movie['id'];
				$MovieStreamparents->embed_id = Yii::app()->common->generateUniqNumber();
				$MovieStreamparents->is_episode = 0;        
				$MovieStreamparents->created_date = gmdate('Y-m-d H:i:s');
				$MovieStreamparents->last_updated_date = gmdate('Y-m-d H:i:s');
				$publish_date = NULL;
				$MovieStreamparents->content_publish_date = $publish_date;
				$MovieStreamparents->save();
			}
        }
        //end insert parent record
        //Insert Into Movie streams table
        $MovieStreams = new movieStreams();
        $MovieStreams->studio_id = Yii::app()->user->studio_id;
        $MovieStreams->movie_id = $movie['id'];
        $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();

        $MovieStreams->is_episode = ($allDataInSheet['C'] != 4) ? 0 : 1;
        //for multipart child content mapping
        if($allDataInSheet['C']==4){
			$MovieStreams->episode_title = $allDataInSheet['A'];
			$MovieStreams->episode_story = $allDataInSheet['H'];
			$MovieStreams->episode_number = $allDataInSheet['L'];
			$MovieStreams->series_number = $allDataInSheet['K'];
			$MovieStreams->episode_date = ($allDataInSheet['M']!="")?$allDataInSheet['M']:NULL;
			if ($custom_flag && ($allDataInSheet['O'] != '')) {//add custom meta data for vod multipart child
				self::importCustomFields($MovieStreams, $allDataInSheet, $sheet_heading, 2);
			}   
		}
        //end for multipart child content mapping
        $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        $publish_date = NULL;

        $MovieStreams->content_publish_date = $publish_date;
        $MovieStreams->save();

        //Check and Save new Tags into database
        $movieTags = new MovieTag();
        $data = array();
        $data['language'] = explode(';', $allDataInSheet['E']);
        $data['genre'] = explode(';', $allDataInSheet['F']);
        $data['censor_rating'] = explode(';', $allDataInSheet['G']);

        $tag_ret = $movieTags->addTags($data);
        $studio_id = Yii::app()->user->studio_id;
        $ret = $this->processContentImage($allDataInSheet['C'], $movie['id'], $MovieStreams->id);
        $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
        if ($checkfireappletv) {
            $this->processAppTVImage($allDataInSheet['C'], $movie['id'], $MovieStreams->id);
        }
        if (isset(Yii::app()->user->is_partner)) {
            $PartnersContent = new PartnersContent();
            $PartnersContent->studio_id = Yii::app()->user->studio_id;
            $PartnersContent->user_id = Yii::app()->user->id;
            $PartnersContent->movie_id = $movie['id'];
            $PartnersContent->save();
        }
        //First Crop the image with specified size
        $arr = array();
        $arr['succ'] = 1;
        $arr['movie_id'] = $movie['id'];
        $arr['movie_stream_id'] = $MovieStreams->id;
        $arr['content_types_id'] = $allDataInSheet['C'];
        $arr['name'] = $allDataInSheet['A'];
        $arr['is_episode'] = $MovieStreams->is_episode;
        $arr['poster'] = POSTER_URL . '/no-image.png';


        if (HOST_IP != '127.0.0.1') {
            $solrObj = new SolrFunctions();
            if($data['genre']){
				foreach($data['genre'] as $key=>$val){

					$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
					$solrArr['content_id'] = $movie['id'];
					$solrArr['stream_id'] = $arr['movie_stream_id'];
					$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
					$solrArr['is_episode'] = $MovieStreams->is_episode;
					$solrArr['name'] = $allDataInSheet['A'];
					$solrArr['permalink'] = $movie['permalink'];
					$solrArr['studio_id'] = Yii::app()->user->studio_id;
					$solrArr['display_name'] = 'content';
					$solrArr['content_permalink'] = $movie['permalink'];
					$solrArr['genre_val'] = $val;
					$solrArr['product_format'] = '';        
					$ret = $solrObj->addSolrData($solrArr);
				}
			} else {
				$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
				$solrArr['content_id'] = $movie['id'];
				$solrArr['stream_id'] = $arr['movie_stream_id'];
				$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
				$solrArr['is_episode'] = $MovieStreams->is_episode;
				$solrArr['name'] = $allDataInSheet['A'];
				$solrArr['permalink'] = $movie['permalink'];
				$solrArr['studio_id'] = Yii::app()->user->studio_id;
				$solrArr['display_name'] = 'content';
				$solrArr['content_permalink'] = $movie['permalink'];
				$solrArr['genre_val'] = '';
				$solrArr['product_format'] = '';        
				$ret = $solrObj->addSolrData($solrArr);   
			}
        }
        $arr['poster'] = @$ret['original'];

        return $arr;
      }

	function importCustomFields($parenttbl, $allDataInSheet, $sheet_heading, $type, $custom_form_id = 0) {
		if ($type == 1) {
			$tbl = 'film_custom_metadata';
			$customindex = 9;
		} elseif ($type == 2) {
			$tbl = 'movie_streams_custom_metadata';
			$customindex = 5;
		}
		$customformname = strtolower($allDataInSheet['O']);
		if (strpos($customformname, 'form') !== false) {
			$customformname = str_replace('form', '', $customformname);
		}
		if ($custom_form_id) {
			$customarr['id'] = $custom_form_id;
		} else {
			$content_types_id = ($allDataInSheet['C'] != 4) ? $allDataInSheet['C'] : 3;
			$customsql = "SELECT id FROM custom_metadata_form WHERE studio_id = " . Yii::app()->user->studio_id . " AND "
					. "name = '" . trim($customformname) . "' and content_types_id=" . $content_types_id . " AND "
					. "parent_content_type_id=" . $allDataInSheet['B'];
			$customarr = Yii::app()->db->createCommand($customsql)->queryROW();
		}
		if (!empty($customarr) && $customarr['id']) {
			$sql = "SELECT p.*, cm.field_name FROM "
					. "(SELECT cf.id,cf.f_display_name,cf.f_id "
					. "FROM custom_metadata_form f,custom_metadata_field cf, custom_metadata_form_field cff "
					. "WHERE f.id=cff.custom_form_id AND cf.id = cff.custom_field_id AND "
					. "f.id = {$customarr['id']} AND f.studio_id =" . Yii::app()->user->studio_id . ")  AS p, {$tbl} cm "
					. "WHERE p.id = cm.custom_field_id  and cm.custom_metadata_form_id={$customarr['id']}";
			$command = Yii::app()->db->createCommand($sql)->queryAll();
			$customData = CHtml::listData($command, 'f_display_name', 'field_name');
			$parenttbl->custom_metadata_form_id = $customarr['id'];
			foreach ($customData as $dkey => $dvalue) {
				$customDatanew[strtolower($dkey)] = $dvalue;
			}
			$sheet_heading = array_map('strtolower', $sheet_heading);
			foreach ($sheet_heading as $ckey => $cvalue) {
				if (trim($cvalue)) {
					if (in_array(trim($cvalue), array_keys($customDatanew))) {
						$k = (int) str_replace('custom', '', $customDatanew[$cvalue]);
						if ($k > $customindex) {
							$cus[] = array($customDatanew[$cvalue] => $allDataInSheet[$ckey]);
						} else {
							self::setCustomFields($parenttbl, $customDatanew[$cvalue], $allDataInSheet[$ckey]);
						}
					}
				}
			}
			if (@$cus && !empty($cus)) {
				if ($customindex == 9)
					$parenttbl->custom10 = json_encode($cus);
				elseif ($customindex == 5)
					$parenttbl->custom6 = json_encode($cus);
			}
		}
	}

	function setCustomFields($tbl, $k, $value) {
		switch ($k) {
			case 'custom1':
				$tbl->custom1 = $value;
				break;
			case 'custom2':
				$tbl->custom2 = $value;
				break;
			case 'custom3':
				$tbl->custom3 = $value;
				break;
			case 'custom4':
				$tbl->custom4 = $value;
				break;
			case 'custom5':
				$tbl->custom5 = $value;
				break;
			case 'custom6':
				$tbl->custom6 = $value;
				break;
			case 'custom7':
				$tbl->custom7 = $value;
				break;
			case 'custom8':
				$tbl->custom8 = $value;
				break;
			case 'custom9':
				$tbl->custom9 = $value;
				break;
		}
	}

    public function actionDownloadSample() {
        require 'PHPExcel/PHPExcel.php';
        require 'PHPExcel/PHPExcel/IOFactory.php';

        $objPHPExcel = new PHPExcel();
        // Set document properties
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Content Name')
                ->setCellValue('B1', 'Content Types')
                ->setCellValue('C1', 'Metadata Type')
                ->setCellValue('D1', 'Release/Record Date')
                ->setCellValue('E1', 'Genre')
                ->setCellValue('F1', 'Language')
                ->setCellValue('G1', 'Censor Ratings')
                ->setCellValue('H1', 'Story/Description')
                ->setCellValue('I1', 'Content Category')
                ->setCellValue('J1', 'Parent Content')
                ->setCellValue('K1', 'Season#')
                ->setCellValue('L1', 'Episode#')
                ->setCellValue('M1', 'Episode Date')
                ->setCellValue('N1', 'Stream URL');
        
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Fast & Furious')
                ->setCellValue('B2', '1')
                ->setCellValue('C2', '2')
                ->setCellValue('D2', '04/17/2011')
                ->setCellValue('E2', 'Action;Crime;Thriller')
                ->setCellValue('F2', 'English')
                ->setCellValue('G2', 'PG-13')
                ->setCellValue('H2', "Brian O'Conner, now working for the FBI in LA, teams up with Dominic Toretto to bring down a heroin importer by infiltrating his operation.")
                ->setCellValue('I2', 'Movie')
                ->setCellValue('J2', '')
                ->setCellValue('K2', '')
                ->setCellValue('L2', '')
                ->setCellValue('M2', '')
                ->setCellValue('N2', '');
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A3', 'Game of Thrones')
                ->setCellValue('B3', '1')
                ->setCellValue('C3', '3')
                ->setCellValue('D3', '04/17/2011')
                ->setCellValue('E3', 'Adventure;Drama;Fantasy')
                ->setCellValue('F3', 'English')
                ->setCellValue('G3', 'TV-MA')
                ->setCellValue('H3', 'Nine noble families fight for control over the mythical lands of Westeros. Meanwhile, a forgotten race hell-bent on destruction returns after being dormant for thousands of years.')
                ->setCellValue('I3', 'TV Series')
                ->setCellValue('J3', '')
                ->setCellValue('K3', '')
                ->setCellValue('L3', '')
                ->setCellValue('M3', '')
                ->setCellValue('N3', '');
        
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', 'Winter Is Coming')
                ->setCellValue('B4', '1')
                ->setCellValue('C4', '1')
                ->setCellValue('D4', '')
                ->setCellValue('E4', '')
                ->setCellValue('F4', '')
                ->setCellValue('G4', '')
                ->setCellValue('H4', "on Arryn, the Hand of the King, is dead. King Robert Baratheon plans to ask his oldest friend, Eddard Stark, to take Jon's place. Across the sea, Viserys Targaryen plans to wed his sister to a nomadic warlord in exchange for an army.")
                ->setCellValue('I4', '')
                ->setCellValue('J4', 'Game of Thrones')
                ->setCellValue('K4', '1')
                ->setCellValue('L4', '1')
                ->setCellValue('M4', '04/17/2011')
                ->setCellValue('N4', '');
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A5', 'The Kingsroad')
                ->setCellValue('B5', '1')
                ->setCellValue('C5', '4')
                ->setCellValue('D5', '')
                ->setCellValue('E5', '')
                ->setCellValue('F5', '')
                ->setCellValue('G5', '')
                ->setCellValue('H5', 'While Bran recovers from his fall, Ned takes only his daughters to Kings Landing. Jon Snow goes with his uncle Benjen to The Wall. Tyrion joins them.')
                ->setCellValue('I5', '')
                ->setCellValue('J5', 'Game of Thrones')
                ->setCellValue('K5', '1')
                ->setCellValue('L5', '1')
                ->setCellValue('M5', '04/17/2011')
                ->setCellValue('N5', '');
               
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Sample');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="sample.xls"');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        ob_clean();
        $objWriter->save('php://output');
        exit;
    }
    
     public function actionpreviewContents() {

        $this->breadcrumbs = array('Manage Content', "Preview List");
        $this->headerinfo = "Preview List";
        $this->layout = "admin";
        require 'PHPExcel/PHPExcel.php';
        require 'PHPExcel/PHPExcel/IOFactory.php';

        $studio_id = Yii::app()->user->studio_id;

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/content_excel';
        if (!is_dir($dir)) {
            mkdir($dir);
            @chmod($dir, 0777);
        }
        Yii::app()->session['filename'] = '';
        Yii::app()->session['filedata'] = '';
        Yii::app()->session['emptyrows'] = '';

        //while uploading the file clean the session and save new session datas.

        $path = $_SERVER['DOCUMENT_ROOT'] . '/images/public/system/content_excel/' . $_FILES['import_all']['name'];
        if (isset($_FILES) && !$_FILES['import_all']['error']) {
            move_uploaded_file($_FILES['import_all']['tmp_name'], $path);
        }
        Yii::app()->session['filename'] = $_FILES['import_all']['name'];

        //read excel content and show in webpage.     
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        //store all excel data into session
        Yii::app()->session['filedata'] = $allDataInSheet;
        $arrayCount = count($allDataInSheet);
        $countnoempty = 0;
        for ($i = 2; $i <= $arrayCount; $i++) {
            $content_name = $allDataInSheet[$i]["A"];
            $content_type = $allDataInSheet[$i]["B"];
            $metedata_type = $allDataInSheet[$i]["C"];
            $content_category = $allDataInSheet[$i]["I"];

            if ($content_name != '' && $content_type != '' && $metedata_type != '' && $content_category != '') {
                $countnoempty = $countnoempty + 1;
            }
        }

        $countempty = ($arrayCount - 1) - $countnoempty;

        //store count of empty rows into session 
        Yii::app()->session['emptyrows'] = $countempty;

        $this->render('previewcontent');
    }
    


    
    public function actionSearchGallery() {
        $studio_id = $this->studio->id;
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $pcontent = Yii::app()->general->getPartnersContentIds();
            $contentid = $pcontent['movie_id'];
            if($_REQUEST['type']=='audio'){
                $all_audios = AudioManagement::model()->get_audiodetails_by_studio_id($studio_id, "$contentid", Yii::app()->user->id);
            }else{
                $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id, "$contentid", Yii::app()->user->id);
            }
        } else {
            if($_REQUEST['type']=='audio'){
                $all_audios = AudioManagement::model()->get_audiodetails_by_studio_id($studio_id);
            }else{
                $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id);
            }	
		}
		if($_POST['is_downloadable'] == 1){
			$all_files = FileManagement::model()->get_videodetails_by_studio_id($studio_id);
		if($_REQUEST['type']=='audio'){
                $this->renderPartial('audiopopupfile', array('all_audios' => $all_audios['data'], 'totalcount' => @$all_audios['count'], 'all_files' => $all_files['data'] , 'totalcountfile' => @$all_files['count']));
            } else {
                $this->renderPartial('videopopupfile', array('all_videos' => $all_videos['data'], 'totalcount' => @$all_videos['count'], 'all_files' => $all_files['data'] , 'totalcountfile' => @$all_files['count']));
            }
		}else{
			if ($_REQUEST['type'] == 'audio') {
				$this->renderPartial('audiopopup', array('all_audios' => $all_audios['data'], 'totalcount' => @$all_audios['count']));
			}else{
				$this->renderPartial('videopopup', array('all_videos' => $all_videos['data'], 'totalcount' => @$all_videos['count']));
			}
		}
        exit;
    }

	public function actionloadGalleryonScroll() {
       
        $studio_id = $this->studio->id;
        $page_size = 16;
        $offset = 0;
        $cond = '';
        if ($_POST['video_duration'] != 0 || $_POST['file_size'] != 0 || $_POST['is_encoded'] != 0 || $_POST['uploaded_in'] != 0) {
           
            $duration = $_POST['video_duration'];
            $file_size = $_POST['file_size'];
            $is_encoded = $_POST['is_encoded'];
            $uploaded_in = $_POST['uploaded_in'];
            
            //Duration
            if ($duration == 1) {// <5 mins
                $cond .= 'CAST(t.duration AS TIME) < "00:05:00" AND ';
            } else if ($duration == 2) {// <30 mins
                $cond .= 'CAST(t.duration AS TIME) < "00:30:00" AND ';
            } else if ($duration == 3) {// <120 mins
                $cond .= 'CAST(t.duration AS TIME)< "02:00:00" AND ';
            } else if ($duration == 4) {// >120 mins
                $cond .= 'CAST(t.duration AS TIME) > "02:00:00" AND ';
            } else {
                $cond .= '';
            }
            //File Size
            if ($_POST['type'] == 'audio') {
                if ($file_size == 1) {//100 MB
                    $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 100 AND ';
                } else if ($file_size == 2) {//<1 GB
                    $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 1024 AND ';
                } else if ($file_size == 3) {//>1 GB
                    $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)> 1024 AND ';
                } else {
                    $cond .= '';
                }
            } else {   //video
                if ($file_size == 1) {//< 1GB=1024 MB
                    $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 1024 AND ';
                } else if ($file_size == 2) {//<10 GB
                    $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)< 10240 AND ';
                } else if ($file_size == 3) {//>10 GB
                    $cond .= 'SUBSTRING_INDEX(t.file_size," ", 1)> 10240 AND ';
                } else {
                    $cond .= '';
                }
            }
            //uploaded in 
            if ($uploaded_in == 1) {//this week
                $cond .= ' YEARWEEK(t.creation_date,1) =  YEARWEEK(CURDATE(), 1) AND ';
            } else if ($uploaded_in == 2) { //this month
                $cond .= ' MONTH(t.creation_date)=  MONTH(CURDATE()) AND ';
            } else if ($uploaded_in == 3) {//this year
                $cond .= ' YEAR(t.creation_date) = YEAR(CURDATE()) AND ';
            } else if ($uploaded_in == 4) {// before this year
                $cond .= ' YEAR(t.creation_date) < YEAR(CURDATE()) AND ';
            } else {
                $cond .= '';
            }
            //encoded ??
            if ($is_encoded == 1) {//yes
                $cond .= '(ms.is_converted=1) AND ';
            } else if ($is_encoded == 2) {//No
                $cond .= '(ms.is_converted  IS NULL OR ms.is_converted =0 ) AND ';
            } else {
                $cond .= '';
            }
            
                $_POST['page'] = $_POST['page'] + 1;
                $offset = ($_POST['page'] - 1) * $page_size;
            
            if ($_POST['type'] == 'audio') {
                $audios = AudioManagement::model()->filter_audio_files($studio_id, $cond,$offset,$page_size);
             $all_audios=$audios['data'];
                
            } else {
                $videos = VideoManagement::model()->filter_video_files($studio_id, $cond,$offset,$page_size);
            
                 $all_videos=$videos['data'];
            }
            
        } else {
           
            if (isset($_POST['page'])) {
                $offset = ($_POST['page']) * $page_size;
            }
            if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
                $pcontent = Yii::app()->general->getPartnersContentIds();
                $contentid = $pcontent['movie_id'];
                if ($_POST['type'] == 'audio') {
                    $audios = AudioManagement::model()->get_audiodetails_by_studio_id($studio_id, "$contentid", Yii::app()->user->id, $offset,$page_size);
                    $all_audios=$audios['data'];
                    } else {
                    $videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id, "$contentid", Yii::app()->user->id,$offset,$page_size);
                    $all_videos=$videos['data'];
                    
                    }
            } else {
                if ($_POST['type'] == 'audio') {
                    $audios = AudioManagement::model()->get_audiodetails_by_studio_id($studio_id, 0,0, $offset,$page_size);
                    $all_audios=$audios['data'];
                    } else {
                    $videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id, 0,0, $offset,$page_size);
                    $all_videos=$videos['data'];
                    }
            }
        }
        
        if ($_POST['type'] == 'audio') {
            $this->renderPartial('audiocontents', array('all_audios' => $all_audios));
        } else {
            $this->renderPartial('videocontents', array('all_videos' => $all_videos));
        }
    }
    function actionGetAdminUsersEmail() {
        $studio_id = $this->studio->id;
        $tags = User::model()->findAll('studio_id=:studio_id', array(':studio_id' => $studio_id));
    
        $tagsArr = CHtml::listData($tags, 'email', 'email');
        echo json_encode($tagsArr);
        exit;
	}
    public function actionRelatedContent(){
        $studio_id = $this->studio->id;
        $dbcon = Yii::app()->db;        
        $movies = RelatedContent::model()->findAll('studio_id=:studio_id AND movie_id=:movie_id AND movie_stream_id=:movie_stream_id', array(':studio_id' => $studio_id,':movie_id' => $_REQUEST['content_id'],':movie_stream_id' => $_REQUEST['content_stream_id']));
        /*$contentids = '';
        foreach ($movies as $key => $value) {
            $contentids.= $value['content_id'].",";
		}
        $digitalsql = "SELECT F.id , F.name, M.id as stream_id, M.episode_title, M.series_number, M.is_episode FROM films F, movie_streams M WHERE F.id=M.movie_id AND IF (M.is_episode = 1, M.id IN (".trim($contentids,',').") , F.id IN (".trim($contentids,',')."))";
        echo $digitalsql;exit;*/
        foreach ($movies as $key => $value) {
            if(in_array($value['content_type'],array(0,1))){
                $sql = "SELECT F.id , F.name, M.id as stream_id, M.episode_title, M.series_number, M.is_episode FROM films F, movie_streams M WHERE F.id=M.movie_id AND IF (M.is_episode = 1, M.id = ".$value['content_id'].",F.id=".$value['content_id'].")";
                $command = $dbcon->createCommand($sql);
                $list = $command->queryROW();
                if($value['content_type']==1){//episode
                    $content[$key]['name'] = $list['name']." - ". $list['episode_title'];
                    $content[$key]['image'] = $this->getPoster($value['content_id'], 'moviestream', 'episode');
                }else{//single part 
                    $content[$key]['name'] = $list['name'];
                    $content[$key]['image'] = $this->getPoster($value['content_id'], 'films');
                }
            }elseif($value['content_type']==2){//Physical
                if(Yii::app()->general->getStoreLink()){ 
                    $sql = "SELECT id, name FROM pg_product WHERE id =".$value['content_id'];
                    $command = $dbcon->createCommand($sql);
                    $list = $command->queryROW();
                    $content[$key]['name'] = $list['name'];
                    $content[$key]['image'] = PGProduct::getpgImage($value['content_id'], 'standard');
                }
            }elseif($value['content_type']==3){//Livesteaming
                $sql = "SELECT F.id , F.name, L.id as stream_id FROM films F, livestream L WHERE F.id=L.movie_id AND F.id=".$value['content_id'];
                $command = $dbcon->createCommand($sql);
                $list = $command->queryROW();
                $content[$key]['name'] = $list['name'];
                $content[$key]['image'] = $this->getPoster($value['content_id'], 'films');
            }
            $content[$key]['id'] = $value['id'];
        }
        $this->renderPartial('ajaxrelatedcontent', array('content_id' => $_REQUEST['content_id'],'content_stream_id' => $_REQUEST['content_stream_id'], 'movies' => $content,'type'=>@$_POST['type']));
 }
    function actionRelatedAutocomplete() {
        $studio_id = Yii::app()->user->studio_id;
        $dbcon = Yii::app()->db;
    
        $movies = $dbcon->createCommand("SELECT GROUP_CONCAT(content_id) as contentid FROM related_content WHERE studio_id=".$studio_id." AND movie_id=".$_POST['movie_id']." AND movie_stream_id =".$_POST['movie_stream_id']." AND content_type!='2' LIMIT 1")->queryROW();
        $pmovies = $dbcon->createCommand("SELECT GROUP_CONCAT(content_id) as contentid FROM related_content WHERE studio_id=".$studio_id." AND movie_id=".$_POST['movie_id']." AND movie_stream_id =".$_POST['movie_stream_id']." AND content_type ='2' LIMIT 1")->queryROW();
        
        if ((isset($_REQUEST['movie_id']) && (is_numeric($_REQUEST['movie_id']))) || (isset($_REQUEST['movieids']) && $_REQUEST['movieids']!='')) {
            $movieid = trim($_REQUEST['movie_id'].",".$_REQUEST['movieids'],',');
            $movieid_s = trim($movieid.",".$movies['contentid'],',');
            $movieid_p = trim($movieid.",".$pmovies['contentid'],',');
            $pcond = ' AND id NOT IN ('.$movieid_p.') ';
            $cond = ' AND F.id NOT IN ('.$movieid_s.') ';
        }else{
            if($pmovies){$pcond = ' AND id NOT IN ('.$pmovies['contentid'].') ';}else{$pcond='';}
            if($movies){$cond = ' AND F.id NOT IN ('.$movies['contentid'].') ';}else{$cond='';}
        }
        
        $this->layout = false;
        $arr = array();        
        
        $sq = $_REQUEST['term'];
         
        if(Yii::app()->general->getStoreLink()){ 
            $is_episode = 2; //For Physical Content
            $sql = "SELECT id, name FROM pg_product WHERE studio_id=" . $studio_id . " AND if(is_preorder = 1, 1, ((publish_date IS NULL) OR  (UNIX_TIMESTAMP(publish_date) = 0) OR (publish_date <=NOW()))) AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ".$pcond." ORDER BY name ASC";
            $command = $dbcon->createCommand($sql);
            $list = $command->queryAll();          
            if ($list) {
                foreach ($list AS $key => $val) {
                    $find = array($val['id'], $is_episode);                     
                    $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => $is_episode);
                }
            }                    
        }

        //Searching Digital contents
        $sql = "SELECT distinct F.id , F.name, M.id as stream_id, M.episode_title, M.series_number, M.is_episode FROM films F, movie_streams M WHERE F.id=M.movie_id AND F.parent_id=0 AND M.episode_parent_id=0 AND F.studio_id=" . $studio_id . " AND F.content_types_id!=4 AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ".$cond." ORDER BY M.is_episode ASC,char_length(name) ASC, F.id DESC, M.episode_number DESC ";
        $command = $dbcon->createCommand($sql);
        $list = $command->queryAll();
        if ($list) {
            foreach ($list AS $key => $val) {
                if (isset($_REQUEST['movie_name'])) {

                    if ($val['is_episode'] == 1)
                        $find = array($val['stream_id'], $val['is_episode']);
                    else
                        $find = array($val['id'], $val['is_episode']);

                    
                        //print_r($val);
                        if ($val['is_episode'] == 1) {
                            $tlt = ($val['episode_title'] != '') ? $val['episode_title'] : " SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                            $tlt = 'Episode-' . $tlt;
                            $arr['movies'][] = array('movie_name' => $val['name'] . ' ' . $tlt, 'movie_id' => $val['stream_id'], 'is_episode' => 1);
                        } else
                            $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => 0);
                    
                } else {
                    $arr[] = array('category' => 'Movie', 'label' => $val['name']);
                }
            }
        }
        
        //Searching Live streaming contents
        $sql = "SELECT distinct F.id , F.name, L.id as stream_id FROM films F, livestream L WHERE F.id=L.movie_id AND F.parent_id=0 AND F.studio_id=" . $studio_id . " AND F.content_types_id = 4 AND LOWER(name) LIKE '%" . strtolower($sq) . "%' ".$cond." ORDER BY char_length(name) ASC, F.id DESC";
        $command = $dbcon->createCommand($sql);
        $list = $command->queryAll();
        if ($list) {
            foreach ($list AS $key => $val) {
                if (isset($_REQUEST['movie_name'])) {
                    $find = array($val['id'], $val['is_episode']);
                    $arr['movies'][] = array('movie_name' => $val['name'], 'movie_id' => $val['id'], 'is_episode' => 3);
                } else {
                    $arr[] = array('category' => 'Movie', 'label' => $val['name']);
                }
            }
        }
        echo json_encode($arr);
        exit;
    }
    public function actionAddRelatedContent(){
        $_POST['movieids'] = trim($_POST['content_id'].",".trim($_POST['movieids'],','),',');
        $_POST['contenttypes'] = trim($_POST['content_type'].",".trim($_POST['contenttypes'],','),',');
        if($_POST['movieids']){
            $return = RelatedContent::model()->insertRelatedData($this->studio->id,$_POST);
            if($return){
                Yii::app()->user->setFlash('success', 'Wow! Related content added successfully');
            }else{
                Yii::app()->user->setFlash('error', 'Oops! Sorry try adding again');
            }
        }        
        $this->redirect($this->createUrl('admin/managecontent'));
    }
    public function actionDeleteRelated(){
        if(@$_POST['id'] && is_numeric($_POST['id'])){
            RelatedContent::model()->deleteByPk($_POST['id']);
            echo 1;exit;
        }else{echo 0;exit;};
    }
    /**
     * @method public ajaxSubmitPhysicalForm() 
     * @return type Description
     */
    function actionajaxSubmitPhysicalForm() {
        $arr['succ'] = 0;
        $data = $_REQUEST['pg'];
        $data['custom_metadata_form_id'] = (isset($_REQUEST['movie']['custom_metadata_form_id']) && is_numeric($_REQUEST['movie']['custom_metadata_form_id']))?$_REQUEST['movie']['custom_metadata_form_id']:0;
        $uniqid = Yii::app()->common->generateUniqNumber();
        $permalink = Yii::app()->general->generatePermalink(stripslashes($data['name']));
        $itModel = New PGProduct;
        $product = $itModel->addContent($data,$uniqid,$permalink);
        if($product){
            $product_id = $product['id'];
            $arr['succ'] = 1;
            $arr['movie_id'] = $product_id;
            $arr['content_types_id'] = 7;
            $arr['name'] = $data['name'];
            $arr['uniq_id'] = $product['uniqid'];
            $arr['poster'] = POSTER_URL . '/no-image.png';            
            if(isset($data['multi_sale_price']) && !empty($data['multi_sale_price'])){
                $pgmulti = New PGMultiCurrency;
                $pgmulti->addContent($data,$product_id);
			}
            $pcModel = New PGProductContents;
            $pcModel->addContent($data,$product_id);
            $ret = PGImage::processProductImage($product_id, $_FILES['Filedata'], $_REQUEST);
            //Adding permalink to url routing 
            $urlRouts['permalink'] = $permalink;
            $urlRouts['mapped_url'] = '/shop/ProductDetails/id/'.$uniqid;
            $urlRoutObj = new UrlRouting();
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,Yii::app()->user->studio_id);
            if(HOST_IP !='127.0.0.1'){
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $product_id;
                $solrArr['stream_id'] = '';
                $solrArr['stream_uniq_id'] = $uniqid;
                $solrArr['is_episode'] = 0;
                $solrArr['name'] = $data['name'];
                $solrArr['permalink'] = $permalink;
                $solrArr['studio_id'] =  Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'muvikart';
                $solrArr['content_permalink'] =  $permalink;
                $solrArr['product_sku'] =  $data['sku'];
                $solrArr['product_format'] = $data['custom_fields'];
                $solrArr['genre_val'] = '';
                $solrObj = new SolrFunctions();           
                $ret = $solrObj->addSolrData($solrArr);
            }
            $arr['poster'] = @$ret['original'];
        }        
        echo json_encode($arr);exit;
    }
    /**
     * @method type updatePhysicalDetails(type $paramName) Description
     * @return type Description
     */
    function actionupdatePhysicalDetails() {
        $studio_id = $this->studio->id;
        $language_id = $this->language_id;
        if ($_REQUEST['movie_id'] && $_REQUEST['uniq_id']) {
            $data = $_REQUEST['pg'];
			$data['id'] = $_REQUEST['movie_id'];
            $goods = new PGProduct();
            $pggoods = $goods->findByPk($_REQUEST['movie_id']);
            if($pggoods){
                $pggoods->name = $data['name'];
                $pggoods->description = $data['description'];
                $pggoods->sale_price = @$data['sale_price'];
                $pggoods->sku = $data['sku'];
                $pggoods->custom_fields = $data['custom_fields'];
                $pggoods->release_date = $data['release_date'] ? date('Y-m-d', strtotime($data['release_date'])) : NULL;
                $pggoods->rating = $data['rating'];
                $pggoods->status = $data['status'];
                $pggoods->content_category_value = implode(',',@$data['content_category_value']);
                $pggoods->size = @$data['size'];
                $pggoods->barcode = @$data['barcode'];
                $pggoods->genre = (is_array(@$data['genre'])?json_encode(@$data['genre']):@$data['genre']);
                $pggoods->audio = @$data['audio'];
                $pggoods->subtitles = @$data['subtitles'];
                $pggoods->alternate_name = @$data['alternate_name'];
                $pggoods->product_type = $data['product_type'];
                $pggoods->currency_id = @$data['currency_id'];
                $pggoods->updated_date=  date('Y-m-d H:i:s');
                $pggoods->feature=  $data['feature'];
                $pggoods->productize_flag = @$data['productize_flag'];
                $publish_date = NULL;
                if (@$data['content_publish_date']) {
                    if (@$data['publish_date']) {
                        $pdate = explode('/', $data['publish_date']);
                        $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                        if (@$data['publish_time']) {
                            $publish_date .= ' ' . $data['publish_time'];
                        }
                    }
                }
                $pggoods->publish_date = $publish_date;
                $data['custom6'] = $goods->createSerializeCustomDate($data);
                $pggoods->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
                $pggoods->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
                $pggoods->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
                $pggoods->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
                $pggoods->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
                $pggoods->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';  

                $pggoods->save();
                if(isset($data['multi_sale_price']) && !empty($data['multi_sale_price'])){
                    PGMultiCurrency::model()->deleteAll("product_id =:product_id", array(':product_id' => $data['id']));
                    $pgmulti = New PGMultiCurrency;
                    $pgmulti->addContent($data,$data['id']);                    
                }
                //delete the data before saving
                PGProductContents::model()->deleteAll("product_id =:product_id", array(':product_id' => $data['id']));
                // re-insert the fresh id
                $pcModel = New PGProductContents;
                if ($data['product_type'] == 1) {
                    $pcModel->addContent($data,$data['id']);
                }
                /* Physical goods image upload */
                PGImage::processProductImage($data['id'], $_FILES['Filedata'], $_REQUEST);
                /* Physical goods image upload end */
                Yii::app()->user->setFlash('success', "Item edited successfully.");
                $url = $this->createUrl('admin/managecontent', array('searchForm[contenttype]' => 2));
                $this->redirect($url);
                exit;   
            }else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry you are not authorised to access this data');
                $url = $this->createUrl('admin/managecontent', array('searchForm[contenttype]' => 2));
				$this->redirect($url);
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry you are not authorised to access this data');
            $url = $this->createUrl('admin/managecontent', array('searchForm[is_physical]' => 1));
			$this->redirect($url);
            exit;
        }
    }
    function managePhysicalContent($req,$digital=0){
        $cond = " ";
        $contentType = '';
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        if (Yii::app()->common->allowedUploadContent($studio_id) > 0)
            $allow_new = 1;
        else
            $allow_new = 0;
        
        $params = array(':studio_id' => $studio_id);
        $pdo_cond_arr = array(":studio_id" => Yii::app()->user->studio_id);
        $pdo_cond = " p.studio_id=:studio_id ";
        $con = Yii::app()->db;
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {
            $search_form = $_REQUEST['searchForm'];
            if (isset($search_form) && $search_form['update_date'] == '' && $search_form['content_category_value'] == '' && $search_form['search_text'] == '' && $search_form['custom_metadata_form_id'] == '' && $search_form['sortby'] == '' && $search_form['is_physical'] == '') {
                $url = $this->createUrl('admin/managecontent');
                $this->redirect($url);
            } else {
                $searchData = $_REQUEST['searchForm'];
                if ($searchData['update_date']) {
                    $sdate = json_decode(stripslashes($searchData['update_date']), TRUE);
                    $pdo_cond .= " AND STR_TO_DATE(p.updated_date,'%Y-%m-%d') >= :start_dt AND STR_TO_DATE(p.updated_date,'%Y-%m-%d') <= :end_dt ";
                    $pdo_cond_arr[':start_dt'] = $sdate['start'];
                    $pdo_cond_arr[':end_dt'] = $sdate['end'];
                }
                if ($searchData['search_text']) {
                    $pdo_cond .= " AND (p.name LIKE :search_text OR p.sku LIKE :search_text)";
                    $pdo_cond_arr[':search_text'] = "%". strtolower(trim($searchData['search_text'])) . "%";
                }
                if($searchData['filterby']==0){
                    if ($searchData['content_category_value']) {
						foreach ($searchData['content_category_value'] as $key => $value) {
							$cat_cond .= "FIND_IN_SET($value,p.content_category_value) OR ";
						}
						$cat_cond = rtrim($cat_cond, 'OR ');
                        $pdo_cond .= " AND ($cat_cond) ";
                        /*$pdo_cond .= " AND (p.content_category_value & :content_category_value) ";
                        $pdo_cond_arr[':content_category_value'] = array_sum($searchData['content_category_value']);*/
                    }
                }elseif($searchData['filterby']==1){
                    if ($searchData['custom_metadata_form_id']) {
                        $custom_metadata_form_id_flag = 1;
                    }
                }
            }
        }
	//echo $pdo_cond."<pre>";print_r($searchData);print_r($pdo_cond_arr);exit;
        $search_text = '';
        $default_lang_id = 20;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();
        $contentTypes = CHtml::listData($contenttypeList, 'id', 'category_name');
        $order = ' p.id DESC ';
        $orderType = 'desc';
        $sort = 'update_date';
        if (isset($searchData['sortby'])) {
            if ($searchData['sortby'] == 1) {//most viewed
                $mostviewed = 1;
            }elseif ($searchData['sortby'] == 2) {//A-Z
                $order = ' p.name ASC ';
            }elseif ($searchData['sortby'] == 3) {//Z-A
                $order = ' p.name DESC ';
            }
        }       
        //Pagination Implimented ..
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        //PDO Query
        $command = Yii::app()->db->createCommand()
            ->select('SQL_CALC_FOUND_ROWS (0), p.*')
            ->from('pg_product p')
            ->where(' p.is_deleted = 0 AND ' . $pdo_cond, $pdo_cond_arr)
            ->order($order)
            ->limit($page_size, $offset);
            if($custom_metadata_form_id_flag){
                $command->andwhere(array('in', 'p.custom_metadata_form_id', $searchData['custom_metadata_form_id']));
            }
        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $pages = new CPagination($count);
        $pages->setPageSize($page_size);
        // simulate the effect of LIMIT in a sql query
        $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
        $sample = range($pages->offset + 1, $end);
        $form = Yii::app()->general->formlist($studio_id);     
		$form = self::unsetForms($form,1);
        $url = Yii::app()->common->getPosterCloudFrontPath($this->studio->id);
        if ($data) {
            $list = CHtml::listData($form,'id', 'name');
            foreach ($data AS $key => $orderval) {
                $command = Yii::app()->db->createCommand()
                    ->select('SUM(quantity) AS totquantity, SUM(sub_total) AS totsubtotal')
                    ->from('pg_order_details ')
                    ->where('product_id = ' . $orderval['id']);
                $dataorder = $command->queryAll();
                $data[$key]['totquantity'] = $dataorder[0]['totquantity'];
                $data[$key]['totsubtotal'] = $dataorder[0]['totsubtotal'];
                //get the content name
                if ($orderval['product_type'] == 0) {
                    $data[$key]['contentname'] = "Standalone";
                } else {
                    $content = Yii::app()->db->createCommand("SELECT movie_id FROM pg_product_content WHERE product_id= " . $orderval['id'])->queryAll();
                    $contentname = "Linked to <br />";
                    foreach ($content as $val) {
                        $content = Film::model()->findByPk($val);
                        $content_name = $content->name;
                        $permalink = $content->permalink;
                        $contentname .= "<a target='_blank' href='http://" . $this->studio->domain . "/" . $permalink . "'>" . $content_name . "</a><br />";
                    }
                    $data[$key]['contentname'] = $contentname;
                }
                $dbprimg = PGProductImage::model()->find(array("condition" => 'product_id = ' . $orderval['id']));
                if ($dbprimg) {
                    $pgposter = $url . '/system/pgposters/' . $orderval['id'] . '/thumb/' . urlencode($dbprimg['name']);
                    if (false === file_get_contents($pgposter)) {
                        $pgposter = '/img/No-Image-Vertical-Thumb.png';
                    }
                } else {
                    $pgposter = '/img/No-Image-Vertical-Thumb.png';
                }
                $data[$key]['pgposter'] = $pgposter;
                $default_currency_id = $this->studio->default_currency_id;
                $tempprice = Yii::app()->common->getPGPrices($orderval['id'], $default_currency_id);
                if(!empty($tempprice)){
                    $data[$key]['sale_price'] = $tempprice['price'];
                    $data[$key]['currency_id'] = $tempprice['currency_id'];
                }
                //$getMovieStream = PGVideo::model()->findByAttributes(array('studio_id' => $this->studio->id, 'product_id' => $orderval['id']));
                $getMovieStream = PGMovieTrailer::model()->findByAttributes(array('movie_id' => $orderval['id']));
                if (!empty($getMovieStream) && $getMovieStream['trailer_file_name'] != '') {
                    $data[$key]['trailer_file_name'] = $getMovieStream['trailer_file_name'];
                    $data[$key]['is_converted'] = $getMovieStream['is_converted'];
                }
                if (!empty($getMovieStream) && $getMovieStream['third_party_url'] != '') {
                    $data[$key]['thirdparty_url'] = $getMovieStream['third_party_url'];
                }
                $orderval['parent_content_type_id'] = 5;
                $orderval['is_episode'] = 0;
                $data[$key]['formname'] = Yii::app()->general->getFormName($orderval,$studio_id,$form,$list);
            }                       
        }
        $studio_id = $this->studio->id;
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
        }else{
            $geoexist = StudioContentRestriction::model()->exists('studio_id=:studio_id',array(':studio_id' => $studio_id));
        }
        $settings = Yii::app()->general->getPgSettings();
        $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $methods = ShippingMethod::model()->findAll("studio_id=:studio_id AND is_enabled=:is_enabled", array(':studio_id' => $studio_id,':is_enabled' => 1));
        if($methods){$method = CHtml::listData($methods, 'method_unique_name', 'method');}
        $this->render('managecontent_physical', array('data' => $data, 'studio' => $studio, 'content_category_value' => @$searchData['content_category_value'], 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sample' => @$sample, 'contentList' => @$contentTypes, 'sort' => @$sort, 'allow_new' => $allow_new, 'geoexist' => @$geoexist,'grestcat'=>$grestcat,'view_trailer' => @$settings['view_trailer'],'form'=>$form,'searchData'=>$searchData,'method'=>@$method,'digital'=>$digital));
    }
    
    /* updateLivestreamStatus is to enable and disable live streaming
     * @return boolen true/false
     * @author Sruti kanta<support@muvi.com> */
    
    public function actionupdateLivestreamStatus(){
        $return = 'Error';
        if(@$_REQUEST['movie_id'] != '' && @$_REQUEST['status'] != ''){
            $liveStreamData = Livestream::model()->findByAttributes(array('movie_id' => $_REQUEST['movie_id']));
            if(@$liveStreamData->feed_url != '' && @$liveStreamData->stream_url != '' && @$liveStreamData->stream_key != ''){
                $streamName = explode("?",@$liveStreamData->stream_key);
                if(@$streamName[0] != ''){
                    $isVideoSyncing = LivestreamVideoSync::model()->findByAttributes(array('movie_id' => $_REQUEST['movie_id'],'stream_name' => $streamName[0],'studio_id' => Yii::app()->user->studio_id));
                    if(empty($isVideoSyncing)){
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=update&name=".@$streamName[0]."&status=".$_REQUEST['status']);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $server_output = trim(curl_exec($ch));
                        curl_close($ch);
                        if($server_output == 'success'){
                            $liveStreamData->start_streaming = $_REQUEST['status'];
                            $liveStreamData->save();
                            $return = 'success';
                        }
                    } else{
                        $return = 'videosync';
                    }
                }
            }
        }
        echo $return;
    }
	function unsetForms($form,$is_physical=0){
		foreach ($form as $key => $value) {
			if ($is_physical) {
				if($value['parent_content_type_id']!=5)
					unset($form[$key]);
			}else{
				if($value['parent_content_type_id']==5)
					unset($form[$key]);
			}
		}
		return $form;
	}
	public function actionupdateAllcontentWhoseFormIdIsNull(){
		$studio_id = $this->studio->id;
		$list = array();
		$custom = Yii::app()->db->createCommand()
				->select("id,content_types_id,parent_content_type_id")
				->from('custom_metadata_form')
				->where("studio_id = ".$studio_id)
				->group("content_types_id")
				->queryAll();
		if(!empty($custom)){
			foreach ($custom as $key => $value) {
				$customarr[$value['content_types_id'].$value['parent_content_type_id']] = $value['id'];
			}
			$cond = "studio_id = " . $studio_id." AND custom_metadata_form_id=0";
			$film_list = Yii::app()->db->createCommand()
					->select("content_types_id,parent_content_type_id")
					->from('films')
					->where($cond)
					->group("content_types_id")
					->queryAll();
			$moviestream_list = Yii::app()->db->createCommand()
					->select("id,movie_id")
					->from('movie_streams')
					->where($cond." AND is_episode = 1")
					->queryROW();
			$product_list = Yii::app()->db->createCommand()
					->select("id")
					->from('pg_product')
					->where($cond)
					->queryROW();

			if(!empty($film_list)){
				foreach ($film_list as $key => $value) {
					$film_listarr[$value['content_types_id'].$value['parent_content_type_id']] = $value;
				}
				$arr_film_listarr = array_keys($film_listarr);
				foreach($customarr  as $k => $v){
					if(in_array($k,$arr_film_listarr)){
						Yii::app()->db->createCommand("UPDATE films SET custom_metadata_form_id=".$v." WHERE studio_id = " . $studio_id." "
						. "AND custom_metadata_form_id=0 AND content_types_id=".$film_listarr[$k]['content_types_id']
						." AND parent_content_type_id = ".$film_listarr[$k]['parent_content_type_id'])->execute();
					}
				}
			}
			if(!empty($moviestream_list)){
				foreach($customarr  as $k => $v){
					if($k == 31){
						Yii::app()->db->createCommand("UPDATE movie_streams SET custom_metadata_form_id=".$v." WHERE studio_id = " . $studio_id." "
						. "AND custom_metadata_form_id=0")->execute();
					}
				}
			}
			if(!empty($product_list)){
				foreach($customarr  as $k => $v){
					if($k == 75){
						Yii::app()->db->createCommand("UPDATE pg_product SET custom_metadata_form_id=".$v." WHERE studio_id = " . $studio_id." "
						. "AND custom_metadata_form_id=0")->execute();
					}
				}
			}
		}
	}
    
    public function actionencodingStatus(){
        $this->layout = false;
        $encodingData = array();
        if($_REQUEST['encodingStreamIds']){
            $streamIds = $_REQUEST['encodingStreamIds'];
            if(movieStreams::model()->getEncodingStatus($streamIds)){
                $encodingData = array('msg' => 'referesh');
            } else{
                $encLogData = Encoding::model()->getEncodingDataForAjax($streamIds);
                if ($encLogData) {
                    foreach ($encLogData AS $key => $val) {
                        if($val['encoding_server_id'] ==0 && $val['expected_end_time'] == "0000-00-00 00:00:00"){
                            $encodingData[$val['movie_stream_id']] = "";
                        } else{
                            $now = new DateTime();
                            $post = new DateTime($val['expected_end_time']);
                            $interval = $now->diff($post);
                            if ($interval) {
                                $encodingData[$val['movie_stream_id']] = $interval->h . ':' . $interval->i . ':' . $interval->s;
                            } else {
                                $encodingData[$val['movie_stream_id']] = '0:0:0';
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($encodingData);
    }
	/*
     * @purpose add the content to playlist
	 * *allPlaylist (get all the playlist)
     * @return  string 
     * @author Biswajitdas<biswajitdas@muvi.com>
    */
	public function actionallPlaylist() {
		$studio_id = $this->studio_id;
		if ((isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$user_id    = (isset($_POST['user_id']) && $_POST['user_id'] !="") ? $_POST['user_id'] : 0;
			$list =Yii::app()->common->getAllplaylist($studio_id,$user_id);
			$res['code'] = 405;
			$res['status'] = "Success";
			$res['msg'] = $list;
			echo json_encode($res);exit;
		}else{
			$res['code'] = 405;
			$res['status'] = "Failure";
			$res['msg'] = 'Need To signin';
			echo json_encode($res);exit;
		}
	}
	public function actionaddToPlaylist() {
		$studio_id = $this->studio_id;
		if ((isset($_POST['playlistname']) && ($_POST['playlistname'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$playlistnme = $_POST['playlistname'];
			$user_id    = (isset($_POST['user_id']) && $_POST['user_id'] !="") ? $_POST['user_id'] : 0;
			$movie_id   = $_POST['content_id'];
			$is_episode = $_POST['is_episode'];
			$lang_code  = $_POST['lang_code'];
			$translate  = $this->getTransData($lang_code, $studio_id);
			$playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_name' => $playlistnme));
			if (empty($playlist)) {
				$playlistNew = new UserPlaylistName;
				$playlistNew->user_id = $user_id;
				$playlistNew->studio_id = $studio_id;
				$playlistNew->playlist_name = $playlistnme;
				$playlistNew->save();
				$playlist_id = $playlistNew->id;
			} else {
				$playlist_id = $playlist->id;
			}
			if($movie_id !=''){
				$result =self::AddContentPlaylist($user_id,$movie_id,$is_episode,$playlist_id,$lang_code);
			}else{
				$res['code'] = 400;
				$res['status'] = "Success";
				$res['msg'] = $translate['playlist_created'];;
				$result = $res;
			}
			echo json_encode($result);exit;
		}else{
			$res['code'] = 405;
			$res['status'] = "Failure";
			$res['msg'] = 'Need To signin';
			echo json_encode($res);exit;
		}
	}
	public function actionDeletePlaylist(){
		$studio_id = $this->studio_id;
		if ((isset($_POST['playlist_id']) && ($_POST['playlist_id'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$playlist_id = $_POST['playlist_id'];
			$user_id    = (isset($_POST['user_id']) && $_POST['user_id'] !="") ? $_POST['user_id'] : 0;
			$playlist_name   = $_POST['playlist_name'];
			$lang_code  = $_POST['lang_code'];
			$translate  = $this->getTransData($lang_code, $studio_id);
			$playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_name' => $playlist_name));
			if (empty($playlist)) {
				$res['code'] = 400;
				$res['status'] = "Failure";
				$res['msg'] = 'Invalid playlist';
				echo json_encode($res);exit;
			} else {
				$playlist_id = $playlist->id;
			}
			$param2 = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id);
			$playItem = UserPlaylist::model()->deleteAll('playlist_id = :playlist_id AND user_id = :user_id AND studio_id = :studio_id', $param2);
			$params = array(':user_id' => $user_id, ':studio_id' => $studio_id,':playlist_name'=>$playlist_name);
			$playlist = UserPlaylistName::model()->deleteAll('user_id = :user_id AND playlist_name = :playlist_name AND studio_id = :studio_id', $params);
			$res['code'] = 200;
			$res['status'] = "Success";
			$res['msg'] = $translate['playlist_deleted'];
			echo json_encode($res);exit;
		}
		else{
			$res['code'] = 405;
			$res['status'] = "Failure";
			$res['msg'] = 'Need To signin';
			echo json_encode($res);exit;
		}
	}
	public function actionDeleteContent(){
		$lang_code  = $_POST['lang_code'];
		$studio_id  = $this->studio_id;
		$translate  = $this->getTransData($lang_code, $studio_id);
		if ((isset($_POST['playlist_id']) && ($_POST['playlist_id'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$content_id  =$_POST['content_id'];
			$playlist_id =$_POST['playlist_id'];
			$user_id     =$_POST['user_id'];
			$playlist = UserPlaylist::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'content_id' => $content_id,'playlist_id'=>$playlist_id));
				if(!empty($playlist)){
					$params = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id,':content_id'=>$content_id);
					$userList = UserPlaylist::model()->deleteAll('content_id = :content_id AND playlist_id = :playlist_id AND studio_id = :studio_id AND user_id = :user_id', $params);
					$res['code'] = 200;
					$res['status'] = "Success";
					$res['msg'] = $translate['content_remove_playlist'];
				}else{
					$res['code'] = 405;
					$res['status'] = "Failure";
					$res['msg'] = 'No content found';
				}
		}else{
				$res['code'] = 407;
				$res['status'] = "Failure";
				$res['msg'] = 'Invalid data';
		}
		echo json_encode($res);exit;
	}
	public function actionGetAudioPlaylist(){
		$studio_id = $this->studio_id;
		if((isset($_POST['playlist_id'])&&($_POST['playlist_id'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))){
			$playlist_id = $_POST['playlist_id'];
			$user_id     = $_POST['user_id'];
			$lang_code   = $_POST['lang_code']; 
			$language_id = Yii::app()->custom->getLanguage_id($lang_code);
			$command = Yii::app()->db->createCommand()
					->select('content_id,content_type')
					->from('user_playlist p')
					->where('p.user_id = ' . $user_id . ' AND p.studio_id=' . $studio_id . ' AND p.playlist_id ='.$playlist_id);
			$playItem = $command->queryAll();
			$playlist = array();
			$k = 0;
			$base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
			foreach ($playItem as $item){
				$movie_id = $item['content_id'];
				$is_episode = $item['content_type'];
				$command1 = Yii::app()->db->createCommand()
					->select('f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode')
					->from('films f,movie_streams ms')
					->where('f.id=ms.movie_id AND ms.id=' . $movie_id . ' AND ms.is_episode = '.$is_episode.'');
				$list = $command1->QueryRow();
				$audio_id = $list['movie_id'];
				if($is_episode == '1'){
					$title   = $list['episode_title'];
					$audioid = $movie_id;
					$type = 'moviestream';
				}else{
					$title = $list['name'];
					$audioid = $audio_id;
					$type = 'films';
				}
				$playlist[$k]['content_id'] = $audioid;
				$playlist[$k]['is_episode'] = $is_episode;
				$playlist[$k]['url'] = $base_cloud_url . $list['full_movie'];
				$playlist[$k]['audio_poster'] = $this->getPoster($audioid, $type);
				$playlist[$k]['title'] = $title;
				$playlist[$k]['permalink'] = $list['permalink'];
				$cast_details = $this->getCasts($audio_id, '', $language_id, $studio_id);
				$celeb_name = array();
				if (!empty($cast_details)) {
					foreach ($cast_details as $casts) {
						if (trim($casts['celeb_name']) != "") {
							$celeb_name[] = $casts['celeb_name'];
						}
					}
				}
				$casts = implode(',', $celeb_name);
				$playlist[$k]['cast'] = $casts;
				$k++;
			}
			if ($playlist) {
				$res['code'] = 200;
                $res['message'] = 'success';
                $res['data'] = $playlist;
            } else {
				$res['code'] = 405;
                $res['message'] = 'error';
                $res['data'] = $playlist;
            }
		}else{
            $res['code'] = 400;
            $res['message'] = 'Invalid playlist';
            $res['status'] = 'failure';
		}
		echo json_encode($res);
        exit;
	}
	public function actionPlayListNameEdit(){
		$studio_id  = $this->studio_id;
		if ((isset($_POST['playlist_name']) && ($_POST['playlist_name'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$playlist_id   = $_POST['playlist_id'];
			$playlist_name = $_POST['playlist_name'];
			$user_id       = $_POST['user_id'];
			$lang_code     =     $_POST['lang_code'];
			$translate     = $this->getTransData($lang_code, $studio_id);
			$playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'id'=>$playlist_id));
			if(!empty($playlist)){
					$playlistUp  = $playlist->findByPK($playlist->id);
					$playlistUp->playlist_name = $playlist_name;
					$playlistUp->save();
					$res['code'] = 200;
					$res['status'] = "Success";
					$res['msg'] = $translate['playlist_updated'];
				}else{
					$res['code'] = 405;
					$res['status'] = "Failure";
					$res['msg'] = 'No Playlist found';
				}
		}else{
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = 'Invalid data';
		}
		echo json_encode($res);exit;
	}
	function AddContentPlaylist($user_id,$movie_id,$is_episode,$playlist_id,$lang_code){
		$studio_id = $studio_id = $this->studio_id;
		$translate  = $this->getTransData($lang_code, $studio_id);
		$playlistitem =UserPlaylist::model()->findByAttributes(array('studio_id'=>$studio_id,'user_id'=>$user_id,'playlist_id'=>$playlist_id,'content_id'=>$movie_id,'content_type'=>$is_episode));
			if(empty($playlistitem)){
				$playlistItem = new UserPlaylist;
				$playlistItem->user_id = $user_id;
				$playlistItem->studio_id = $studio_id;
				$playlistItem->playlist_id = $playlist_id;
				$playlistItem->content_id = $movie_id;
				$playlistItem->content_type = $is_episode;
				$playlistItem->save();
				$msg =$translate['content_added_to_playlist'];
				$res['code'] = 200;
				$res['status'] = "Success";
				$res['msg'] = $translate['content_added_to_playlist']; 
			}else{
				$res['code'] = 400;
				$res['status'] = "Success";
				$res['msg'] = $translate['already_added_playlist'];
			}
		return $res;
	}
    
    #Ansuman.S || functionality : offline view 
    #24-07-2017
    function actionOpenSetOffline(){
        $res =  array();
        $stream_id=$_REQUEST['movie_stream_id'];
        $off_val=$_REQUEST['off_val'];
        $play = movieStreams::model()->findByAttributes(array('id'=>$stream_id));
        if(!empty($play)){
            $play->is_offline=$off_val;
            $play->save();
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg']=$play['is_offline'];        
                            
        }
        else{
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = 'Invalid data';
        }
        echo json_encode($res);
        exit; 
                
    }
	
	public function actionaddFileFromFileGallery() {
		if (isset($_REQUEST['url']) && $_REQUEST['galleryId']) {
            $studioId = Yii::app()->common->getStudioId();
            $url = @$_REQUEST['url'];
            $galleryId = @$_REQUEST['galleryId'];
            $movieStreamId = @$_REQUEST['movie_stream_id'];
            if ($url != '' && intval($galleryId)) {
				$file_management = FileManagement::model()->FindByAttributes(array('id' => $galleryId));
                $movieStreams = movieStreams::model()->FindByAttributes(array('file_management_id' => $galleryId, 'is_converted' => 1));
                    $s3 = Yii::app()->common->connectToAwsS3($studioId);

                    $bucketInfo = Yii::app()->common->getBucketInfo("", $studioId);
                    $bucketName = $bucketInfo['bucket_name'];
                    $s3cfg = $bucketInfo['s3cmd_file_name'];
                    $folderPath = Yii::app()->common->getFolderPath("", $studioId);
                    $signedBucketPath = $folderPath['signedFolderPath'];
					$unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                    
                    $key = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movieStreamId . '/';
                    $i =0;
                    if ($movieStreams->id != $movieStreamId) {
						$s3prefix = $unsignedFolderPathForVideo.'filegallery/'.$file_management->file_name;
                        $responseDelete = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucketName,
                            'Prefix' => $key
                        ));
                        foreach ($responseDelete as $object) {
                            $s3->deleteObject(array(
                                'Bucket' => $bucketName,
                                'Key' => $object['Key']
                            ));
						}
                        //Sh File Creation
                        $file = date("Y_m_d_H_i_s") . '_' . $movieStreamId .HOST_IP. '.sh';
                        
                        $functionParams = array();
                        $functionParams['field_name'] = 's3_upload_folder';
                        if($bucketName == 'vimeoassets-singapore'){
                            $functionParams['singapore_bucket'] = 1;
                        }
                        $shDataArray = Yii::app()->aws->getshUploadFolderPath($functionParams);
                        $s3folderPath = $shDataArray['shfolder'];
                        $videoFolder = 'upload_video';
                        $shFolder = 'upload_progress';
                        $conversionBucket = 'muvistudio';
                        if (HOST_IP == '127.0.0.1') {
                            $videoFolder = 'staging/upload_video';
                            $shFolder = 'staging/upload_progress';
                            $conversionBucket = 'stagingstudio';
                        } else if (HOST_IP == '52.0.64.95') {
                            $videoFolder = 'staging/upload_video';
                            $shFolder = 'staging/upload_progress';
                            $conversionBucket = 'stagingstudio';
                        }
                        
                        $movieOriginPath = "s3://".$bucketName."/".$key;
                        $movieCopyPath = "s3://".$bucketName."/".$s3prefix;
                        $updateUrl = Yii::app()->getBaseUrl(true)."/conversion/updateSyncVideo?movie_id=".$movieStreamId;
                        $wgetCMD = "";
						$folderPath = "/var/www/html/$videoFolder/movieSync_$movieStreamId";
						$wgetCMD .= "/usr/local/bin/s3cmd get --recursive --config /usr/local/bin/.".$s3cfg." ".$movieCopyPath." \n";
						$wgetCMD .= "/usr/local/bin/s3cmd sync --recursive --config /usr/local/bin/.".$s3cfg." --acl-public --add-header='content-type':'video/mp4' ".$folderPath."/ ".$movieOriginPath." \n";
						$file_data = "file=`echo $0`\n" .
										"cf='" . $file . "'\n\n" .
										"if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
										"then\n\n" .
										"echo \"$file is running\"\n\n" .
										"else\n\n" .
										"sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
										"# create directory\n" .
										"mkdir $folderPath\n" .
										"chmod 0777 $folderPath\n" .
										"cd $folderPath\n" .
										"# wget and move command\n" .
										$wgetCMD .
										"# update query\n" .
										"curl $updateUrl\n" .
										"# remove directory\n" .
										"rm -rf $folderPath\n" .
										"# remove the process copy file\n" .
										"rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
										"# remove the process file\n" .
										"rm /var/www/html/$shFolder/$file\n" .
										"fi";
                        $handle = fopen($_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file, 'w') or die('Cannot open file:  '.$file);
                        fwrite($handle, $file_data);
                        fclose($handle);
                        $filePath = $_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file;
                        $s3 = S3Client::factory(array(
                                'key'    => Yii::app()->params->s3_key,
                                'secret' => Yii::app()->params->s3_secret,
                        ));
                        if($s3->upload($conversionBucket, $s3folderPath.$file, fopen($filePath, 'rb'), 'public-read')){
                            $i = 1;
                            unlink($filePath);
                        }
                    } else {
                        Yii::app()->user->setFlash('success', 'File mapped successfully');
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        echo json_encode($arr);
                        exit;
                    }
                    if ($i != 0) {
                        $embed_id = md5($movieStreamId);
                        $epData = movieStreams::model()->findByPk($movieStreamId);
						$epData->full_movie = $file_management->file_name;
						
                        $epData->embed_id = $embed_id;
                        $epData->is_download_progress = 0;
                        $epData->mail_sent_for_video = 0;
                        $epData->is_converted = 3;
                        $epData->has_sh = 0;
                        $epData->upload_start_time = date('Y-m-d H:i:s');
                        $epData->upload_end_time = date('Y-m-d H:i:s');
                        $epData->upload_cancel_time = '';
                        $epData->encoding_start_time = date('Y-m-d H:i:s');
                        $epData->encoding_end_time = date('Y-m-d H:i:s');
                        $epData->encode_fail_time = '';
                        $epData->file_management_id = $galleryId;
                        $epData->thirdparty_url = '';
                        $epData->save();
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        Yii::app()->user->setFlash('success', 'File mapped successfully');
                    } else {
                        $embed_id = md5($movieStreamId);
                        $epData = movieStreams::model()->findByPk($movieStreamId);
                        $epData->full_movie = '';
                        $epData->is_converted = 2;
                        $epData->is_offline = 0;
                        $epData->upload_start_time = date('Y-m-d H:i:s');
                        $epData->upload_end_time = date('Y-m-d H:i:s');
                        $epData->upload_cancel_time = '';
                        $epData->encoding_start_time = date('Y-m-d H:i:s');
                        $epData->encoding_end_time = '';
                        $epData->encode_fail_time = date('Y-m-d H:i:s');
                        $epData->save();
                        $arr = array('error' => 0, 'is_video' => 'uploaded');
                        Yii::app()->user->setFlash('error', 'Due to some reason video could not be mapped to content!');
                    }
            }
            echo json_encode($arr);
            exit;
        }
    }
	
	public function actionajaxFilterFile() {
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['is_encoded']) && isset($_REQUEST['uploaded_in'])) {
            $is_encoded = $_REQUEST['is_encoded'];
            $uploaded_in = $_REQUEST['uploaded_in'];
            $cond='';
            //uploaded i
            if ($uploaded_in == 1) {//this week
                $cond .= ' YEARWEEK(t.creation_date,1) =  YEARWEEK(CURDATE(), 1) AND ';
            } else if ($uploaded_in == 2) { //this month
                $cond .= ' MONTH(t.creation_date)=  MONTH(CURDATE()) AND';
            } else if ($uploaded_in == 3) {//this year
                $cond .= ' YEAR(t.creation_date) = YEAR(CURDATE()) AND ';
            } else if ($uploaded_in == 4) {// before this year
                $cond .= ' YEAR(t.creation_date) < YEAR(CURDATE()) AND ';
            } else {
                $cond .= '';
            }
            //encoded ??
            if ($is_encoded == 1) {//yes
                $cond .= '(ms.is_converted=1) AND ';
            } else if ($is_encoded == 2) {//No
                $cond .= '(ms.is_converted  IS NULL OR ms.is_converted =0 ) AND ';
            } else {
                $cond .= '';
            }
        }
        $allvideo = FileManagement::model()->filter_video_files($studio_id,$cond);
        $this->layout = false;
        $this->renderPartial('ajaxsearchfile', array('all_videos' => $allvideo['data'],'totalcount' => @$allvideo['count']));
    }
}
