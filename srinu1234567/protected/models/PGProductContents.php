<?php
class PGProductContents extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'pg_product_content';
    } 
    function addContent($data,$ins_id){
        foreach ($data['contentid'] as $val) {
            if ($val != '') {
                $this->product_id = $ins_id;
                $this->movie_stream_id = 0;
                $this->movie_id = $val;
                $this->isNewRecord = TRUE;
                $this->primaryKey = NULL;
                $this->save();
			}
        }
    }
}
