<?php

class ApiadyenController extends Controller {

    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }
    /**
     * 
     * Library functions starts
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */

    function calculateSha256Signature($hmacKey, $params)
    {
        // validate if hmacKey is provided
        if ($hmacKey == "") {
            throw new \Adyen\AdyenException("You did not provide a HMAC key");
        }

        if (empty($params)) {
            throw new \Adyen\AdyenException("You did not provide any parameters");
        }

        // The character escape function
        $escapeval = function ($val) {
            return str_replace(':', '\\:', str_replace('\\', '\\\\', $val));
        };

        // Sort the array by key using SORT_STRING order
        ksort($params, SORT_STRING);

        // Generate the signing data string
        $signData = implode(":", array_map($escapeval, array_merge(array_keys($params), array_values($params))));

        // base64-encode the binary result of the HMAC computation
        $merchantSig = base64_encode(hash_hmac('sha256', $signData, pack("H*", $hmacKey), true));
        return $merchantSig;
    }
    /**
     * Library functions ends
     */
    

    /**
     *
     * Initialize the api
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() {
        $adyen = array();
        $adyen['skinCode'] = trim($this->PAYMENT_GATEWAY_API_USER['adyen']);
        $adyen['merchantAccount'] = trim($this->PAYMENT_GATEWAY_API_PASSWORD['adyen']);
        $adyen['HMAC'] = trim($this->PAYMENT_GATEWAY_API_SIGNATURE['adyen']);
        if($this->IS_LIVE_API_PAYMENT_GATEWAY['adyen'] != 'sandbox'){
            $adyen['endpoint'] = "https://live.adyen.com/hpp/pay.shtml";
        }else{
            $adyen['endpoint'] = "https://test.adyen.com/hpp/pay.shtml";
        }
        return $adyen;
    }
    
    function processCard($arg = array()) {
        $adyen = self::initializePaymentGateway();
        $data['merchantReference'] = $arg['reference_number'];
		$amount = number_format((float) ($arg['amount']), 2, '.', '');
        $data['paymentAmount'] = $amount*100;
        $data['currencyCode'] = $arg['currency_code'];
        $data['resURL'] = $arg['resURL'];
        $data['shipBeforeDate'] = Date('Y-m-d');
        $data['skinCode'] = $adyen['skinCode'];
        $data['merchantAccount'] = $adyen['merchantAccount'];
        $data['shopperLocale'] = $arg['shopper_locale'];
        $data['shopperEmail'] = $arg['email'];
        $data['shopperReference'] = $arg['shopper_reference'];
        $data['recurringContract'] = "ONECLICK";
        $data['selectedRecurringDetailReference'] = "LATEST";
        $data['sessionValidity'] = Date('Y-m-d\TH:i:s\Z',strtotime(Date('Y-m-d'). '+1 day'));
        $data['merchantSig'] = self::calculateSha256Signature($adyen['HMAC'],$data);
        
        if(isset($data['merchantSig']) && trim($data['merchantSig'])){
            $res['isSuccess'] = 1;
            $res['code'] = 200;
            $res['status'] = 'success';
            $res['action'] = $adyen['endpoint'];
            $res['response'] = $data;
        }else{
            $res['isSuccess'] = 0;
            $res['Message'] = 'Internal error. Please check again';
        }
        return $res;
    }
    
    function sampleIntegrationTransaction($arg = array()) {
        $res = array();
        $res['isSuccess'] = 1;
        $res['card']['code'] = 200;
        return $res;
    }
}