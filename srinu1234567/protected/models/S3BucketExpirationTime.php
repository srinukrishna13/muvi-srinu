<?php
class S3BucketExpirationTime extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 's3_bucket_expiration_time';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            's3buckets'=>array(self::HAS_ONE, 'S3buckets','s3buckets_id'),
        );
    }    
}

