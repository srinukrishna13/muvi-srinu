<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
use Aws\Ses\SesClient; 
class TicketController extends Controller {
    public $layout = 'admin';
    public $headerinfo = '';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);

        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            $this->redirect($redirect_url);exit;
        }else{
             $this->checkPermission();
        }

        if (!isset(yii::app()->user->is_sdk) && !yii::app()->user->is_sdk) {
            $studioArray = array('customerslist', 'addbill', 'editbill', 'maketransaction', 'Suspend', 'resume');
            if (in_array(Yii::app()->controller->action->id, $studioArray)) {
                $this->redirect($redirect_url);exit;
            }
        }
        return true;
    }

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        $model = new TicketForm;
        if (isset($_POST['TicketForm'])) {
            $model->attributes = $_POST['TicketForm'];
        }
        $this->render('addTicket', array('model' => $model));
    }

    
    public function actionTicketList() {
        $this->pageTitle = Yii::app()->name . ' - Support';
        $this->breadcrumbs = array('Support','All Tickets');
	$tickettingdb=$this->getAnotherDbconnection();
        $email=Yii::app()->user->email; 
	 $studio_id = Yii::app()->common->getStudiosId();
        //check whether the user is a admin of the studio or general_user       
        $user_sql="select * from user where studio_id=".$studio_id." and role_id=1";   
        $reg_studioemail=Yii::app()->db->createCommand($user_sql)->queryAll();   

       
       $studio_id = Yii::app()->common->getStudiosId();
     	if (Yii::app()->common->hasPermission('support', 'view')) { 
        $master_sql="select id from ticket_master where studio_email='".$reg_studioemail[0]['email']."'";         
        $master_id=$tickettingdb->createCommand($master_sql)->queryAll();        
        $num_rec_per_page=20;
        $dev_hours = $tickettingdb->createCommand("SELECT sum(dev_hours) as total FROM ticket where status in ('New','Working') and type='New Feature' and studio_id=" . $studio_id)->queryAll();

            $remaining_dev_hours = Yii::app()->db->createCommand("SELECT purchased_devhours FROM studios where id=" . $studio_id)->queryAll();

            //card details for payment
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();

            $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
            
       
        $this->render('ticketList', array('page_size' => $num_rec_per_page,'total_dev_hours' => $remaining_dev_hours[0]['purchased_devhours'], 'card' => $card));
        $tickettingdb->active=false;
    }else{
            $tickettingdb->active=false;
            Yii::app()->user->setFlash('error','you are not an authorized user!');
            $url = $this->createUrl('admin/dashboard');
             $this->redirect($url);
        }
    }
    public function actionsearchTicket() {
        $studio_id = Yii::app()->common->getStudiosId();
        $this->pageTitle = Yii::app()->name . ' - Support';
        $this->breadcrumbs = array('Support', 'All Tickets');
        $email = Yii::app()->user->email;
        $tickettingdb = $this->getAnotherDbconnection();
	 //check whether the user is a admin of the studio or general_user       
        $user_sql="select * from user where studio_id=".$studio_id." and role_id=1";   
        $reg_studioemail=Yii::app()->db->createCommand($user_sql)->queryAll();   

        $master_sql="select id from ticket_master where studio_email='".$reg_studioemail[0]['email']."'";         
        $master_id = $tickettingdb->createCommand($master_sql)->queryAll();

        if (Yii::app()->common->hasPermission('support', 'view')) {
            $num_rec_per_page = 20;
        
            $start_from = 0;
        
            if (isset($_REQUEST['page'])) {
                $start_from = ($_REQUEST['page'] - 1) * $num_rec_per_page;
            }
            
            $model = new TicketForm;
            $sort_by = '';
            if (isset($_REQUEST['sortBy'])) {
                $sort_by = $_REQUEST['sortBy'];
                 if ($_REQUEST['sortBy'] == "last_updated_date_desc")
                    $orderby = "last_updated_date DESC";
                else if ($_REQUEST['sortBy'] == "last_updated_date_asc")
                    $orderby = "last_updated_date ASC";
            } else
                $orderby = 'last_updated_date DESC';
            $limit = " LIMIT $start_from, $num_rec_per_page";
            $cond = "ticket_master_id=" .$master_id[0]['id'] ;
           
            if($_REQUEST['ticketstatus']=="Open"){          
            $cond.=" and status in('New','Working','re-opened','Re-opened')";
            }
            else if($_REQUEST['ticketstatus']=="Closed")
            {
               $cond.=" and status ='Closed'";

            }
            if($_REQUEST['priority']==0){          
            $cond.=" and priority in('critical','high')";
            }
            else if($_REQUEST['priority']==1)
            {
               $cond.=" and priority in('medium')";
             
            }else if($_REQUEST['priority']==2)
            {
               $cond.=" and priority in('low')";

            }
            
            if (isset($_REQUEST['search_value']) && !empty($_REQUEST['search_value'])) {
                $cond.=" AND (";
                if (is_numeric($_REQUEST['search_value']))
                    $cond.="id=" . $_REQUEST['search_value'] . " OR";
                $cond.=" description LIKE '%" . addslashes($_REQUEST['search_value']) . "%' OR priority LIKE '%" . addslashes($_REQUEST['search_value']) . "%' )";
            }
            
            $total_query = "SELECT SQL_CALC_FOUND_ROWS(0),i.* from ticket i where ticket_master_id=" . $master_id[0]['id'] . " and studio_id=" . $studio_id;

            $res = $tickettingdb->createCommand($total_query)->queryAll();
            
            $total_records = count($res);
            $total_pages = ceil($total_records / $num_rec_per_page);
            $total_pages = ceil($total_records / $num_rec_per_page);
            $allticket = $model->ticketList($cond, $orderby, $limit);
            $count_searched = count($allticket);

            $dev_hours = $tickettingdb->createCommand("SELECT sum(dev_hours) as total FROM ticket where status in ('New','Working') and type='New Feature' and studio_id=" . $studio_id)->queryAll();

            $remaining_dev_hours = Yii::app()->db->createCommand("SELECT purchased_devhours FROM studios where id=" . $studio_id)->queryAll();

            //card details for payment
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();

            $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
            //$this->render('ticketList', array('list' => $list, 'total_pages' => $total_pages,'total_records' => $total_records,'total_dev_hours'=>$remaining_dev_hours[0]['purchased_devhours'],'card'=>$card));
            $tickettingdb->active = false;
        }

        $this->renderPartial('ticketrenderdata', array('list' => $allticket,'page_size' => $num_rec_per_page, 'count_searched' => $count_searched, 'total_pages' => $total_pages, 'total_records' => $total_records, 'total_dev_hours' => $remaining_dev_hours[0]['purchased_devhours'], 'card' => $card,'sort_by'=>$sort_by));
    }
    
    public function actionuseDevhour()
    {
        $ticket_id=$_POST['ticket_id'];
        $dev_hour=$_POST['dev_hour'];
        $ticket_id=$_POST['ticket_id'];
        $studio_id=$_POST['studio_id'];
        $std=new Studio();
        $studio = $std->findByPk($studio_id);
        $purchased_devhours=$studio->purchased_devhours;
       
        $used_hours_from_purchased=$studio->used_hours_from_purchased;
        
       if($purchased_devhours<$dev_hour || $purchased_devhours==0)
       {
           $ret['message']='Not Sufficient DevHous';
           $ret['status']=0;
           
       }
       else{
            
            $totalremaining_devhour = $purchased_devhours-$dev_hour;
            $std1=Studio::model()->updateByPk($studio_id,array("purchased_devhours"=>$totalremaining_devhour)); 
           
            $totalused_devhour = $used_hours_from_purchased+$dev_hour;
            $std2=Studio::model()->updateByPk($studio_id,array("used_hours_from_purchased"=>$totalused_devhour));  
           
           //update flag after purchased
            $ticket=TicketForm::model()->updateByPk($ticket_id,array("purchase_status"=>1));  
           
            //insert record to dev hours used info
            $insert_sql="insert into `devhour_used_infos`( `studio_id`, `ticket_id`, `hours_purchased`, `creation_date`) values(".$studio_id.",".$ticket_id.",".$purchased_devhours.",'".date("Y-m-d H:i:s")."')";
            Yii::app()->db->createCommand($insert_sql)->execute();
            //send email to admin and customer reg the devhour purchase info.
            $user_id = Yii::app()->user->id;
            
            $user = User::model()->findByPk($user_id);
            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['ticket_id'] = $ticket_id;
            $req['dev_hour']=$dev_hour;
            $req['remaining_devhour']=$totalremaining_devhour;
           
            $customeremail= Yii::app()->email->approveDevhourEmailtoCustomer($req);
            $adminemail= Yii::app()->email->approveDevhourEmailtoAdmin($req);
            
            
             
             //send email to admin and customer reg the devhour purchase info.
            $ret['message']='Sufficient DevHous';
            $ret['status']=1;
       }
        echo json_encode($ret);exit; 
    }
    
     public function actionaddTicket() {
        $this->pageTitle = Yii::app()->name . ' - Add Ticket';
        $this->breadcrumbs = array('Support','All tickets'=>array('ticket/ticketList'),'Add Ticket');
        $this->headerinfo = "Add Ticket";
        $model = new TicketForm;
        if (!isset($_GET['error']))
            unset($_SESSION['description']);
        if (isset($_POST['TicketForm']))
            $model->attributes = $_POST['TicketForm'];
        $assigned_to = $model->getOwner();
        $studio = $model->getStudio();
        $this->render('addTicket', array('model' => $model, 'assigned_to' => $assigned_to, 'studio' => $studio));
    }


    public function actionInsertTicket() {
        $attachment_patharray=array();
        $email=Yii::app()->user->email;   
        $tickettingdb=$this->getAnotherDbconnection();
        
        $studio_id = Yii::app()->common->getStudiosId();
        //check whether the user is a admin of the studio or general_user       
        $user_sql="select * from user where studio_id=".$studio_id." and role_id=1";   
        $reg_studioemail=Yii::app()->db->createCommand($user_sql)->queryAll();   
        
        $master_sql="select id from ticket_master where studio_email='".$reg_studioemail[0]['email']."'";         
         
        
        $master_id=$tickettingdb->createCommand($master_sql)->queryAll();  
        $tickettingdb->active=false;
        $_POST['TicketForm']['ticket_master_id']=$master_id[0]['id'];
        $_POST['TicketForm']['studio_id']= Yii::app()->common->getStudiosId();
        $model = new TicketForm;
        if (empty($_POST['TicketForm']['title']) && trim($_POST['TicketForm']['title'])=='') {
            Yii::app()->user->setFlash('error','Ticket title can not be empty');
                $this->redirect('ticketList');  
                exit;
        
        }
        if (isset($_POST['TicketForm']) && !empty($_POST['TicketForm'])) {
           // print_r($_POST['TicketForm']);
           // exit;
            $_POST['TicketForm']['creater_id']=Yii::app()->user->id;
            $model->attributes = $_POST['TicketForm'];
            $rid = $model->insertTicket($_POST['TicketForm']);
            
            if ($rid) {
                if (!empty($_FILES['upload_file1']['name'])) {
                    $attachments = TicketController::attachFile($_FILES, $rid);
                    $arr['TicketForm']['attachment'] = implode($attachments, ',');
                    $arr['TicketForm']['id_ticket'] = $rid;
                    $model->updateTicket($arr['TicketForm']);
                    
                }
                //code for uploading attachment with email
                $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                foreach ($attachments as $k){
                $attachment_patharray[]=$src."/".$k;
                }
                
                $studio_id = Yii::app()->common->getStudiosId();
                $std = new Studio();
                $studio = $std->findByPk($studio_id);
                $title =($_POST['TicketForm']['title']);
                if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
                $description =($_POST['TicketForm']['description']);
                if (strlen($description) > 16) 
                {
                    $description = wordwrap($description, 16);
                    $description = substr($description, 0, strpos($description, "\n"));
                    
                }
                $subject = "Ticket" . $rid . "_".$title."_New Ticket Added";
              //  $subject = "Ticket" . $rid . "_" . $studio->name . ":".!empty($title) ? $title: $description;
               // $template = TicketController::tempaleFormat();
                $url = "http://".DOMAIN_COOKIE; 
                $thtmltoCustomer =  "<p>Hi " . ucfirst($studio->name) . ",<br /><br />
                                    Your support ticket is ".$rid. " added.<br/> ";
                $thtmltoCustomer.= " Ticket details:- <br /><br/><b>Priority</b>:" . $_POST['TicketForm']['priority'] .'</p>';
                $thtmltoCustomer.= "<b>Description:</b>".nl2br(stripslashes(htmlentities($_POST['TicketForm']['description'])))."<br /><br/>";
                $thtmltoCustomer.="You will hear from Muvi Support team soon. Please find updates to your ticket <a target='_blank' href='" .$url. "'>here</a>. 
                                    Add an update to the ticket by logging into <a target='_blank' href='" . $url ."'>Muvi</a>, or simply replying to this email.<br /><br />
                                </p>" ."<p><br/>Regards,<br /> Muvi</p>";
                //echo $thtmltoCustomer; exit;
                $htmltoAdmin =  "<p>Hi Admin,<br /><br />
                               A new ticket is added by $studio->name .<br />";
                $htmltoAdmin.= " Ticket details:- <br /><br/><b>Priority</b>:" . $_POST['TicketForm']['priority'] . "</p>" ;
                $htmltoAdmin.= "<b>Description:</b>". nl2br(stripslashes(htmlentities($_POST['TicketForm']['description'])))."<p><br/>Regards,<br /> Muvi</p>";
                //echo $htmltoAdmin;echo $thtmltoCustomer;exit;
               // $this->sendmailViaMandrill($htmltoAdmin, $subject, array(array("email" => 'suraja@muvi.com')));
                $to_admin='support@muvi.com';
                $from='support@muvi.com';
                 $test_email_subject="Muvitest ticket";
                 
                 if(strpos($_POST['TicketForm']['description'],$test_email_subject)!== false)
                 {
                     $from='testmuvi@gmail.com';
                     $to_admin='testmuvi@gmail.com';
                 }
               $ret_val1=$this->sendAttchmentMailViaAmazonsdk($to_admin,$subject,$from,$htmltoAdmin,$attachment_patharray);
                $to=Yii::app()->user->email;
                //get email of all admins if exists.
                //check for cc emails ajit@muvi.com issue id - 5975
                $admin_email = array();
                $admin_email = Yii::app()->common->emailNotificationLinks($studio_id,'support_tickets');
                if(count($admin_email)==0){
                    $all_admin_email = User::model()->findAll(array('condition' => 'studio_id =' . $_POST['TicketForm']['studio_id'] . ' AND role_id =1 AND is_sdk=1'));
                foreach($all_admin_email as $admin_key=>$admin_val)
                //list out all emails that are in admin list fr the studio.
               {
                 $admin_email[]=  $admin_val['email'];
               }
                }
                //check for cc emails ajit@muvi.com issue id - 5975
               $to=$admin_email;
                //$from='testmuvi12@gmail.com';
               
                //$this->sendmailViaAmazonsdk($thtmltoCustomer,$subject,$to,$from);
                $ret_val2=$this->sendAttchmentMailViaAmazonsdk($to,$subject,$from,$thtmltoCustomer,$attachment_patharray);
             
                Yii::app()->user->setFlash('success','Ticket Added Successfully');
                $this->redirect('ticketList');
            }
        } else
            $this->render('addTicket', array('model' => $model));
    }

    
    public function actionUpdateTicket() {
     	$this->pageTitle = Yii::app()->name . ' - Update Ticket';
        $this->breadcrumbs = array('Support'=>array('ticket/ticketList'),'Update Ticket');
        $this->headerinfo = "Update Ticket";
        
        $model = new TicketForm;
        if (!isset($_POST['TicketForm'])) { //Edit Ticket
            $id = isset($_POST['id']) ? $_POST['id'] : $_GET['id'];
            $cond = "id=" . $id . " AND studio_id=" . Yii::app()->common->getStudiosId();
            $ticket = $model->ticketList($cond);
            if (empty($ticket)) {
                $this->render('error');
                exit;
            }
            $this->render('updateTicket', array('ticket' => $ticket[0], 'model' => $model));
        } else { //Update Ticket
            if (empty($_POST['TicketForm']['title']) || trim($_POST['TicketForm']['title'])=='') {
            Yii::app()->user->setFlash('error','Ticket title can not be empty');
                $this->redirect('ticketList');  
                exit;
        
                 }
            $ticket_eta_Details = $model->ticketList("id=" . $_POST['TicketForm']['id_ticket']);
            $before_array = $ticket_eta_Details[0];
           
            $cc_flag=1;
                if(($before_array['title']==$_POST['TicketForm']['title']) && ($before_array['description']==$_POST['TicketForm']['description']))
                {

                  $cc_flag=0; 
                }
                
             
            $_POST['TicketForm']['title']=  addslashes($_POST['TicketForm']['title']);
            $attachments=array();$prevfiles=array();
            if (!empty($_FILES['upload_file1']['name']))
                $attachments = TicketController::attachFile($_FILES, $_POST['TicketForm']['id_ticket']);
               
                //code for uploading attachment with email
                $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                foreach ($attachments as $k){
                $attachment_patharray[]=$src."/".$k;
                }
                
                $prevfiles = !empty($_POST['prevfiles']) ? explode(',',$_POST['prevfiles']): $prevfiles ;
                $attachments = array_merge($attachments, $prevfiles);
                $_POST['TicketForm']['attachment'] = !empty($attachments) ? implode($attachments, ',') : '';
            $studio_id = Yii::app()->common->getStudiosId();
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            if ($model->updateTicket($_POST['TicketForm'])) {
                $ticketDetails = $model->ticketList("id=" . $_POST['TicketForm']['id_ticket']);
                $description =($_POST['TicketForm']['description']);
                
                //send email to cc if title and description changed
                $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" .$studio_id)->queryAll();
                
               $cc_email_list=array();
               if($before_array['ticket_email_cc']!='' && $cc_flag==1)
               {
                   $cc_email_list=explode(',',$before_array['ticket_email_cc']);
                foreach ($cc_email_list as $key => $value)
                       {
                           if (trim($value) == 'support@muvi.com' || trim($value) == $user[0]['email'])
                        {
                            unset($cc_email_list[$key]);
                        }
                       }
            

               }
             
                if (strlen($description) > 16)
                {
                    $description = wordwrap($description, 16);
                    $description = substr($description, 0, strpos($description, "\n"));
                }
                
                 $title =  addslashes($_POST['TicketForm']['title']);
                if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
                if ($_POST['TicketForm']['status'] == 'Closed') {
                $subject = "Ticket" . $_POST['TicketForm']['id_ticket'] . "_" . $title . " Closed";
                }else if($_POST['TicketForm']['status'] == 'Re-opened') {
                $subject = "Ticket" . $_POST['TicketForm']['id_ticket'] . "_" . $title . " Re-Opened";
                }
                 else{
                $subject = "Ticket" . $_POST['TicketForm']['id_ticket'] . "_" .$title." Updated";           
                }
                $html = TicketController::ticketDetails($ticketDetails);
                //$template = TicketController::tempaleFormat($ticketDetails);
                $toStd =  "<p> Hi ".ucfirst($studio->name).",<br /><br />";
                $toAdmn =  "<p> Hi Admin,<br /><br />";
               //  $this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
               /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
                //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
              /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
                $to_admin=array('support@muvi.com');
                $test_email_subject="Muvitest ticket";
                 if(strpos($_POST['TicketForm']['description'],$test_email_subject)!== false)
                 {
                     $to_admin=array('testmuvi@gmail.com');
                 }
                $from='support@muvi.com';
                $this->sendAttchmentMailViaAmazonsdk($to_admin,$subject,$from,$toAdmn.$html,$attachment_patharray,$cc_email_list,'','');

                $uri = '';
                if (!empty($_REQUEST['search']))
                    $uri.="/search/".urlencode($_REQUEST['search']);
                if (!empty($_REQUEST['sortBy']))
                    $uri.="/sortBy/{$_REQUEST['sortBy']}";
                if (!empty($_REQUEST['page']))
                    $uri.="/page/{$_REQUEST['page']}";
                Yii::app()->user->setFlash('success','Ticket Updated Successfully');
                $this->redirect('ticketList' . $uri);
            }
        }
    }

    function attachFile($files, $id_ticket) {
        $model = new TicketForm;
        foreach ($files as $k => $v) {
            if (!empty($v['name'])) {
                $filename = time() . '_' . str_replace(' ', '_',$v['name']);
                if ($filename) {
                    $fileUrl= $this->uploadAttachments($filename, $v,  $id_ticket);
                    $new['id_ticket'] = $id_ticket;
                    $new['attachment_url'] = $fileUrl['url'];
                    $model->updateTicket($new);
                    $attachments[] = $filename;
                    //echo $filename;exit;
                    //Add attachment to email if the user added while creating ticket
                    //get the file details from s3 buckket after upload to s3bucket
                    $s3path=$fileUrl['url'].$filename;
                    
                    $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment' ;
                     if (!is_dir($src)) {
                      mkdir($src, 0777);
                        }
                   
                   file_put_contents($src."/".$filename, file_get_contents($s3path));
                }
            }
        }
        return $attachments;
    }

    function attachFile_note($files,$id_note,$id_ticket)   {             
        $model = new TicketForm;
        foreach ($files as $k => $v) {
            if (!empty($v['name'])) {
                $filename = strtotime(date("Y-m-d H:i:s")) . '_' . str_replace(' ', '_', $v['name']);
                if ($filename) {               
                    $fileUrl = $this->uploadAttachments($filename, $v, $id_ticket);                    
                    $new['attachment_url'] = $fileUrl['url'];
                                    
                    $attachments['file_name'][] = $filename;
                    $attachments['url']= $fileUrl['url'];
                    $src=$_SERVER['DOCUMENT_ROOT'] . '/images/attachment' ;
                    $attachments['path'][] = $src . "/" . $filename;
               
                    $s3path=$fileUrl['url'].$filename;
                    
                    
                     if (!is_dir($src)) {
                      mkdir($src, 0777);
                        }
                   
                   file_put_contents($src."/".$filename, file_get_contents($s3path));
                }
            }
        }
        
        return $attachments;
    }

    function editAttachFile_note($files, $note_id, $id_ticket, $note_content, $all_file_list, $existing_file_url) {
       
        
        $attachments = array();
        $model = new TicketForm;
         $new['added_by'] = Yii::app()->user->id; //Yii::app()->common->getAgent();//Yii::app()->user->name;
         $new['id_user'] =  Yii::app()->common->getStudiosId();
         $new['add_date'] = date('Y-m-d H:i:s');
         $new['updated_date'] = date('Y-m-d H:i:s');
         $new['id'] = $note_id;
         $all_image_url='';
        if ($files['upload_file_edit1']['name'] != '') {
            foreach ($files as $k => $v) {
                if (!empty($v['name'])) {
                    $filename = strtotime(date("Y-m-d H:i:s")) . '_' . str_replace(' ', '_', $v['name']);

                    if ($filename) {
                        $fileUrl = $this->uploadAttachments($filename, $v, $id_ticket);

                        $attachments['imagefile'][] = $filename;
                        $src = $_SERVER['DOCUMENT_ROOT'] . '/images/attachment';
                        $attachments['path'][] = $src . "/" . $filename;
                         $all_image_url=implode(',',$attachments['path']);
                        
                        $s3path = $fileUrl['url'] . $filename;
                        if (!is_dir($src)) {
                            mkdir($src, 0777);
                        }

                        file_put_contents($src . "/" . $filename, file_get_contents($s3path));
                    }
                }
            }       
            $all_attachment = implode(",", $attachments['imagefile']);
            $all_attachment = $all_attachment . "," . $all_file_list;

            //get all image details
            $new['id_ticket'] = $id_ticket;
            $new['note'] = $note_content;
            $new['attachment_url'] = $fileUrl['url'];
            $new['attachment'] = addslashes($all_attachment);
            
                   
            //$new['id_note'] =  $note_id;
            $model->updateNote($new);
            //echo $filename;exit;
        } else {
            if ($all_file_list != '') {
                $new['id_ticket'] = $id_ticket;
                $new['note'] = $note_content;
                $new['attachment_url'] = $existing_file_url;
                $new['attachment'] = $all_file_list;
                $new['id'] = $note_id;
                //$new['id_note'] =  $note_id;
                $model->updateNote($new);

                
            } else {
                $new['id_ticket'] = $id_ticket;
                $new['note'] = $note_content;
                $new['attachment_url'] = "";
                $new['attachment'] = "";
                $new['id'] = $note_id;
                //$new['id_note'] =  $note_id;
                $model->updateNote($new);

               
            }
        }
        $new['all_attachment_path']=$all_image_url;
        return $new;
    }

    /* View Ticket Detail */

    public function actionViewTicket() {
        $model = new TicketForm;
        $cond = "id=" . $_GET['id'] . " AND studio_id=" . Yii::app()->common->getStudiosId();
        $ticket = $model->ticketList($cond);
        if (strlen($ticket['title']) > 35) {
            $page_title = wordwrap($ticket['title'], 35);
            $page_title = substr($page_title, 0, strpos($page_title, "\n"));
        } else {
           $page_title = $ticket['title'];
        }
        $this->pageTitle = ' "Ticket  ' . $ticket['id'] . ' : ' . $page_title . '"';
        $this->breadcrumbs = array('Ticket Detail',);
        if (empty($ticket)) {
            $this->render('error');
            exit;
        }
        $ticketnotes = $model->ticketNotes($_GET['id']);
        $this->render('ticketDetail', array('ticket' => $ticket[0], 'notes' => $ticketnotes));
    }

    function  actionDeleteTicket(){
        $tickettingdb=$this->getAnotherDbconnection();
        $model = new TicketForm;
        //$del = $model->deleteRecord($_POST['id'],'ticket');
        
        $id_ticket=$_REQUEST['id'];
        
        /************************* code by suraja to close the ticket *****************/
         $htmlstd ='';
            $last_updated_date = date('Y-m-d H:i:s');
        
            $last_updated_by = Yii::app()->common->getStudiosId();
            
            $status='Closed';
           $update_qry='UPDATE ticket SET last_updated_date="'.$last_updated_date.'", last_updated_by='.$last_updated_by.',status="'.$status.'" where id='.$id_ticket;

            $rid = $tickettingdb->createCommand($update_qry)->execute();
        
        $select_qry='select title,description,studio_id from  ticket where id='.$id_ticket;    
        $data = $tickettingdb->createCommand($select_qry)->queryAll();
        $tickettingdb->active=false;
        $studio_id=$data[0]['studio_id'];
      //$description=trim($data[0]['description']);
       $description=trim($data[0]['title']);
       
            $std = new Studio();
            $studioname = $studio_id != 1 ? $std->findByPk($studio_id)->name : 'Super Admin';
            $ticketDetails = $model->ticketList("id=" . $id_ticket);
            $ticketnotes = $model->ticketNotes($id_ticket);
           
            if (!empty($ticketnotes)) {
            foreach ($ticketnotes as $key => $updates) {
                if ($key == 0) {
                    $update=stripslashes(nl2br(htmlentities($updates['note'])));
                    if (strlen($update) > 16) {
                    $update = wordwrap($update, 16);
                    $update = substr($update, 0, strpos($update, "\n"));
            } 
                }
            }
        }else{
            $update ="Support ticket is";
        }
        
              $subject = "Ticket" . $id_ticket . "_" . $description  . " Closed";
            
            $template = TicketController::tempaleFormat();
            $toStd = "<p> Hi " . ucfirst($studioname) . ",<br /><br />";
            if ($_SERVER['HTTP_HOST'] == "admin.muvi.com")
            $url = "https://www.muvi.com/ticket/viewTicket/id/"."{$id_ticket}";
            else
            $url = "http://www.studio.muvi.in/ticket/viewTicket/id/"."{$id_ticket}";
            
            $htmlstd .= " If you believe the issue/feature is not resolved yet, log into <a target='_blank' href='" . $url . "'>Muvi</a>,  and reopen the ticket. <br /><br />";

            
            $toAdmn = "<p> Hi Admin,<br /><br />";
            $html = TicketController::ticketDetails($ticketDetails);
            
            $adminEmail = array('support@muvi.com');
            if ($studio_id != 1) {
                $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id)->queryAll();
                $studioemail = $user[0]['email'];
               // $this->sendmailViaMandrill($toStd . $html . $htmlstd."</p>" . $template[1], $subject, array(array("email" => $studioemail))); //Yii::app()->user->email
                }
                $to_admin=$adminEmail;
                $from='support@muvi.com';
                $this->sendmailViaAmazonsdk($toAdmn . $html . $htmlstd."</p>" . $template[1],$subject,$to_admin,$from);
                   
            
            
        /******************** code by suraja to close the ticket **************/
        
        Yii::app()->user->setFlash('success','Ticket Closed Successfully');
        $this->redirect($this->createUrl("ticket/ticketList"));
    }
    /* Add note/update to ticket */

    public function actionAddNote() {
        //if there is no note content 
        $attachment_patharray = array();
        //show message if there is noo more description on note 
        if (empty($_POST['note']) && trim($_POST['note']) == '') {

            $uri = '';
            if (!empty($_REQUEST['search']))
                $uri.="/search/" . urlencode($_REQUEST['search']);
            if (!empty($_REQUEST['sortBy']))
                $uri.="/sortBy/{$_REQUEST['sortBy']}";
            if (!empty($_REQUEST['page']))
                $uri.="/page/{$_REQUEST['page']}";
            Yii::app()->user->setFlash('error', 'Note description can not be empty');
            $this->redirect("viewTicket" . $uri . "/id/{$_POST['id_ticket']}");
        }
        if (isset($_POST['note']) && !empty($_POST['note'])) {
           
            $model = new TicketForm;
         
             $attachments['attachment'] = array();
            $prevfiles = array();
          //  if (!empty($_FILES['upload_file1']['name'])) {
                
                $note = $_POST['note'];
                $ticket_id = $_POST['id_ticket'];

                $insertid = $model->insertNote($note, $ticket_id);
           // }
            
            if ($insertid) {
                //if (!empty($_FILES['upload_file1']['name'])) {
                    $attachments = TicketController::attachFile_note($_FILES, $insertid, $ticket_id);
                   if (!empty($_FILES['upload_file1']['name'])) {
                    //$attachment_patharray[] = implode(",", $attachments['path']);
                       $attachment_patharray = $attachments['path'];
                   }
                    $arr['TicketForm']['attachment'] = implode( ',',$attachments['file_name']);
                    $arr['TicketForm']['attachment_url'] = $attachments['url'];
                    $arr['TicketForm']['id'] = $insertid;
                    $arr['TicketForm']['added_by'] = Yii::app()->common->getAgent(); //Yii::app()->common->getAgent();//Yii::app()->user->name;
                    $arr['TicketForm']['id_user'] = Yii::app()->common->getStudioId();
                    $arr['TicketForm']['add_date'] = date('Y-m-d H:i:s');
                    $arr['TicketForm']['updated_date'] = date('Y-m-d H:i:s');

                    $model->updateNote($arr['TicketForm']);
               // }
            }
           
            /* Send update mail */
            $ticketDetails = $model->ticketList("id=" . $_POST['id_ticket']);
            $title = $ticketDetails[0]['title'];
            $studio_id = $ticketDetails[0]['studio_id'];
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id)->queryAll();
            $to = $user[0]['email'];
            //$email = array($to);

            $cc_email_list = explode(',', $ticketDetails[0]['ticket_email_cc']);

             foreach ($cc_email_list as $key => $value)
            {
                if (trim($value) == 'support@muvi.com' || trim($value) == $user[0]['email'])
                {
                    unset($cc_email_list[$key]);
                }
            }

            $cc = implode(',', $cc_email_list);
            //$cc= isset($cc_email_list)?$cc_email_list:'';

            $std = new Studio();
            $html = TicketController::ticketDetails($ticketDetails);
            //$template = TicketController::tempaleFormat($ticketDetails);
            $note =($_POST['note']);
                if (strlen($note) > 16) 
                {
                    $note = wordwrap($note, 16);
                    $note = substr($note, 0, strpos($note, "\n"));
                    
                }
                 if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
            $toStd =  "<p> Hi ".ucfirst($studio->name).",<br /><br />";
            $toAdmn =  "<p> Hi Admin,<br /><br />";
            $subject = "Ticket" . $_POST['id_ticket'] . "_" .$title." Updated";
             /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
              
            //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
            // $this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
            
            /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
            $to_admin='support@muvi.com';
                $test_email_subject="Muvitest ticket";
                 if(strpos($ticketDetails[0]['description'],$test_email_subject)!== false)
                 {
                     $to_admin='testmuvi@gmail.com';
                 }
            $from='support@muvi.com';
             $email=array($to_admin);
            $ret_val = $this->sendAttchmentMailViaAmazonsdk($email, $subject, $from, $toStd . $html, $attachment_patharray, $cc_email_list, '', '');
            
            //$retval=$this->sendmailViaAmazonsdk($toAdmn.$html,$subject,$to_admin,$from);
            $new['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($new);
            if (isset($_POST['id_note'])) {
               
            }
        }
        $uri = '';
        if (!empty($_REQUEST['search']))
            $uri.="/search/".urlencode($_REQUEST['search']);
        if (!empty($_REQUEST['sortBy']))
            $uri.="/sortBy/{$_REQUEST['sortBy']}";
        if (!empty($_REQUEST['page']))
            $uri.="/page/{$_REQUEST['page']}";
        Yii::app()->user->setFlash('success', 'Update Added Successfully');
        $this->redirect("viewTicket" . $uri . "/id/{$_POST['id_ticket']}");   
    }

    
    public function actionEditNote() {
      
        //show message if there is noo more description on note 
        if (empty($_POST['note']) && trim($_POST['note']) == '') {

            $uri = '';
            if (!empty($_REQUEST['search']))
                $uri.="/search/" . urlencode($_REQUEST['search']);
            if (!empty($_REQUEST['sortBy']))
                $uri.="/sortBy/{$_REQUEST['sortBy']}";
            if (!empty($_REQUEST['page']))
                $uri.="/page/{$_REQUEST['page']}";
            Yii::app()->user->setFlash('error', 'Note description can not be empty');
           
        }

       else if (isset($_POST['note']) && !empty($_POST['note'])) {

            $model = new TicketForm;
            $attachments['attachment'] = array();
            $prevfiles = array();
            
           // if (!empty($_FILES['upload_file_edit1']['name'])){
                $ticketDetails = $model->noteList("id=". $_POST['id_note']);
                $existing_attachment=$ticketDetails[0]['attachment'];
                $existing_file_url=$ticketDetails[0]['attachment_url'];
                
                $all_attachment = $existing_attachment;
                $attachments = TicketController::editAttachFile_note($_FILES, $_POST['id_note'], $_POST['id_ticket'], $_POST['editor_val'], $all_attachment,$existing_file_url);

                // }
              if($attachments['all_attachment_path']!=''){
              $attachment_patharray=explode(',',$attachments['all_attachment_path']); 
                }else
                {
                 $attachment_patharray=array();   
                }  
            if (!empty($_FILES['upload_file1']['name'])) {
                $note = $_POST['note'];
                $ticket_id = $_POST['id_ticket'];

                $insertid = $model->insertNote($note, $ticket_id,$_POST['id_note']);
            }
            

            /* Send update mail */
            $ticketDetails = $model->ticketList("id=" . $_POST['id_ticket']);
            $title = $ticketDetails[0]['title'];
            $studio_id = $ticketDetails[0]['studio_id'];
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id)->queryAll();
            $to = $user[0]['email'];
            //$email = array($to);

            $cc_email_list = explode(',', $ticketDetails[0]['ticket_email_cc']);

             foreach ($cc_email_list as $key => $value)
            {
                if (trim($value) == 'support@muvi.com' || trim($value) == $user[0]['email'])
                {
                    unset($cc_email_list[$key]);
                }
            }
            

            $cc = implode(',', $cc_email_list);
            //$cc= isset($cc_email_list)?$cc_email_list:'';

            $std = new Studio();

            $html = TicketController::ticketDetails($ticketDetails);
            //$template = TicketController::tempaleFormat($ticketDetails);

            $note = ($_POST['note']);
            if (strlen($note) > 16) {
                $note = wordwrap($note, 16);
                $note = substr($note, 0, strpos($note, "\n"));
            }
            if (strlen($title) > 16) {
                $title = wordwrap($title, 16);
                $title = substr($title, 0, strpos($title, "\n"));
            }

            $toStd = "<p> Hi " . ucfirst($studio->name) . ",<br /><br />";
            $toAdmn = "<p> Hi Admin,<br /><br />";
            $subject = "Ticket" . $_POST['id_ticket'] . "_" . $title . " Updated";
            //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));

            $to_admin='support@muvi.com';
            $test_email_subject="Muvitest ticket";
                 if(strpos($ticketDetails[0]['description'],$test_email_subject)!== false)
                 {
                     $to_admin='testmuvi@gmail.com';
                 }
            $from='support@muvi.com';
            
            $email=array($to_admin);
            //$this->sendmailViaAmazonsdk($toStd.$html, $subject, $email,$from);
            $ret_val = $this->sendAttchmentMailViaAmazonsdk($email, $subject, $from, $toStd . $html, $attachment_patharray, $cc_email_list, '', '');
            /*             * **************Sending email to admin stopped as per the requirment in mantis ticket #4207 ***************** */

            //$this->sendmailViaMandrill($toAdmn.$html, $subject, array(array("email" =>'support@muvi.com')));

            /*             * **************Sending email to admin stopped as per the requirment in mantis ticket #4207 ***************** */

            $new['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($new);
          
            $uri = '';
        if (!empty($_REQUEST['search']))
            $uri.="/search/" . urlencode($_REQUEST['search']);
        if (!empty($_REQUEST['sortBy']))
            $uri.="/sortBy/{$_REQUEST['sortBy']}";
        if (!empty($_REQUEST['page']))
            $uri.="/page/{$_REQUEST['page']}";
        Yii::app()->user->setFlash('success', 'Update Added Successfully');
        }
        else
        {
            
        }
        
       
    }

    public function actionEditNote1() {


        if (isset($_POST['editor_val']) && !empty($_POST['editor_val'])) {
            
           
            $model = new TicketForm;
            $attachments['attachment'] = array();
            $prevfiles = array();
         
           if ($_FILES['upload_file1']['name']!='')
            {
            $ticketDetails = $model->noteList("id=". $_POST['id_note']);
            $existing_attachment=$ticketDetails[0]['attachment'];
            $existing_file_url=$ticketDetails[0]['attachment_url'];
            $all_attachment = $existing_attachment;
           
            $attachments = TicketController::editAttachFile_note($_FILES,$_POST['id_note'],$_POST['id_ticket'], $_POST['editor_val'],$all_attachment,$existing_file_url);
            
            }
            else
            {
                
            $ticketDetails = $model->noteList("id=". $_POST['id_note']);
            $existing_attachment=$ticketDetails[0]['attachment'];
            $existing_file_url=$ticketDetails[0]['attachment_url'];
            $all_attachment = $existing_attachment;
                if($all_attachment!='')
                {
                 $attachments = TicketController::editAttachFile_note($_FILES,$_POST['id_note'],$_POST['id_ticket'], $_POST['editor_val'],$all_attachment,$existing_file_url);   
                }else{
                $attachments=TicketController::editAttachFile_note($_FILES, $_POST['id_note'],$_POST['id_ticket'], $_POST['editor_val'],$all_attachment,$existing_file_url);   
                }
            }
            
             if(!empty($attachments['attachment_url'])){
            $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                
                $attachment_patharray[]=$src."/".$attachments['file_name'];
               
           }
            
           
            if (isset($_POST['id_note'])) {
                
                $note = $model->addNote($_POST['editor_val'], '', $_POST['id_note'], $attachments['attachment'], $attachments['attachment_url']);
            } else
                $model->addNote($_POST['editor_val'], $_POST['id_ticket'], $attachments['note_id'], $attachments['attachment'], $attachments['attachment_url']);
            
            
            /* Send update mail */
            $ticketDetails = $model->ticketList("id=" . $_POST['id_ticket']);
            $title=$ticketDetails[0]['title'];
            $studio_id = Yii::app()->common->getStudiosId();
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            $html = TicketController::ticketDetails($ticketDetails);
            //$template = TicketController::tempaleFormat($ticketDetails);
            $query_user_email = "select * from user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id;
            $user_details = Yii::app()->db->createCommand($query_user_email)->queryAll();
             $useremail=$user_details[0]['email'];
            $cc_email_list=explode(",",$ticketDetails[0]['ticket_email_cc']);
            
             foreach ($cc_email_list as $key => $value)
            {
                if (trim($value) == 'support@muvi.com' || trim($value) == trim($user_details[0]['email']))
                {
                    unset($cc_email_list[$key]);
                }
            }
             $cc=implode(',',$cc_email_list);
            $note =($_POST['note']);
                if (strlen($note) > 16) 
                {
                    $note = wordwrap($note, 16);
                    $note = substr($note, 0, strpos($note, "\n"));
                    
                }
                 if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
            $toStd =  "<p> Hi ".ucfirst($studio->name).",<br /><br />";
            $toAdmn =  "<p> Hi Admin,<br /><br />";
            $subject = "Ticket" . $_POST['id_ticket'] . "_" .$title." Updated";
             /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
              
            //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
            //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
            /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
            $to_admin='support@muvi.com';
            $test_email_subject="Muvitest ticket";
                 if(strpos($ticketDetails[0]['description'],$test_email_subject)!== false)
                 {
                     $to_admin='testmuvi@gmail.com';
                 }
            $from='support@muvi.com';
            
            $email=array($to_admin);
           $this->sendAttchmentMailViaAmazonsdk($email,$subject,$from,$toAdmn.$html,$attachment_patharray,$cc_email_list,'','');
           $new['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($new);
            if (isset($_POST['id_note'])) {
               
            }
        }
        
        $uri = '';
        if (!empty($_REQUEST['search']))
            $uri.="/search/".urlencode($_REQUEST['search']);
        if (!empty($_REQUEST['sortBy']))
            $uri.="/sortBy/{$_REQUEST['sortBy']}";
        if (!empty($_REQUEST['page']))
            $uri.="/page/{$_REQUEST['page']}";
        //Yii::app()->user->setFlash('success','Update Added Successfully');
        //$this->redirect("viewTicket" . $uri . "/id/{$_POST['id_ticket']}");
    }

    public function actionDeletenote() {
        $id_note = $_POST['id'];
        $table = "ticket_notes";
        $model = new TicketForm;
        return $model->deleteRecord($id_note, $table);
    }
    function actionDeleteImage(){
        if($_REQUEST['is_ajax']){
            $s3 = S3Client::factory(array(
                    'key'    => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
            ));
            
            $result = $s3->deleteObject(array(
                    'Bucket' => Yii::app()->params->s3_ticket_attachments,
                    'Key'    => 'uploads/'.$_POST['id_ticket']."/".$_POST['image']
            ));
            $model = new TicketForm;
            $ticket = $model->ticketList("id=".$_POST['id_ticket']);
            $attachment= explode(',', $ticket[0]['attachment']);
            $key = array_search($_POST['image'], $attachment);
            if($key>=0)
                unset($attachment[$key]);
            $attachment = implode(',',$attachment);
            $arr['TicketForm']['attachment'] = $attachment;
            $arr['TicketForm']['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($arr['TicketForm']);
            $ret['attachment']=$attachment;
            $ret['deleted']=1;
            echo json_encode($ret);exit;
        }
    }
    
    function actiondeletenoteImage(){
        $tickettingdb=$this->getAnotherDbconnection();
        if($_POST['is_ajax']){
            $s3 = S3Client::factory(array(
                    'key'    => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
            ));
            
            $result = $s3->deleteObject(array(
                    'Bucket' => Yii::app()->params->s3_ticket_attachments,
                    'Key'    => 'uploads/'.$_POST['id_ticket']."/".$_POST['image']
            ));
            $model = new TicketForm;
            $ticket = $model->noteList("id=".$_POST['note_id']);
            
            $attachment= explode(',', $ticket[0]['attachment']);
            
            $key=$_POST['key'];
            
            if($key>=0)
            {
            unset($attachment[$key]);
            }
          
            $attachment = implode(',',$attachment);
           
            $arr['TicketForm']['attachment'] = $attachment;
            $arr['TicketForm']['id_ticket'] = $_POST['id_ticket'];
           
            $sql="UPDATE ticket_notes SET attachment='".$attachment."' WHERE id=".$_POST['note_id'];
            
            $tickettingdb->createCommand($sql)->execute();
            $tickettingdb->active=false;
            $ret['attachment']=$attachment;
            $ret['deleted']=1;
            
            echo json_encode($ret);exit;
        }
    }
    
    /* Send mail to client if admin replies directly from CMS Ticket List */
    public function actionReplyClient() {
        $model = new TicketForm;
        if ($_POST['sendmail'])
            return $model->replyToClient($_POST);
        else
            return $model->getUserDetail($_POST['id'], $_POST['studio_id']);
    }

    /* Fetch Ticket Description and updates added to the ticket */

    function ticketDetails($ticket) {
        $std = new Studio();
        $stu = $std->findByPk($ticket[0]['studio_id']);
        $reported_by = $ticket[0]['studio_id'] == 1 ? 'Super Admin' : $stu['name'];
        //$assigned_to = ['assigned_to'] == 0 ? 'None' : $this->getOwnerName($ticket[0]['assigned_to']);
        $model = new TicketForm;
        //$template = TicketController::tempaleFormat();
        $ticketnotes = $model->ticketNotes($ticket[0]['id']);
        if ($ticket[0]['status'] == 'Closed') {
            $html = "<p> Your support ticket  {$ticket[0]['id']} is Closed Now.</p> ";
         } 
        else  if ($ticket[0]['status'] == 'Re-opened') {
            $html = "<p> Your support ticket  {$ticket[0]['id']} is Re-Opened.</p> ";
        }
         else {
            $html = "<p>Your support ticket  {$ticket[0]['id']} is updated. </p>";
        }
        
       
        if (!empty($ticketnotes)) {
            $html.="<hr style='border-top:1px dotted #000;' />";
            foreach ($ticketnotes as $k=>$updates) {
                
                    $note_added_by = $updates['id_user']==1? 'Muvi' : $std->findByPk($ticket[0]['studio_id'])->name;
                    $updated_on = date('M d, Y', strtotime($updates['updated_date']));
                    $html.=str_replace("\xC2\xA0", " ",stripslashes((trim($updates['note']))));
                    $html.="<br />By: {$note_added_by} on {$updated_on}";
                    $html.="<hr style='border-top:1px dotted #000;' />";
                
            }
            
        }
        $html.= "Reported By: {$reported_by},&nbsp;Status: {$ticket[0]['status']},&nbsp;Priority: {$ticket[0]['priority']} <br /><br />";
        $html.= "Description:" . str_replace("\xC2\xA0", " ",stripslashes(trim($ticket[0]['description'])));
        return $html;
        exit;
    }
   
     function tempaleFormat() {
        if ($_SERVER['HTTP_HOST'] == "admin.muvi.com")
            $url = "https://www.muvi.com";
        else
            $url = "http://www.studio.muvi.in";
        $tpl[0] = '<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-repeat:repeat-x" width="100%">
                    <tbody>
                        <tr>
                            <td align="left" style="padding-left:20px;"><a href="'.$url.'"><img src="'.EMAIL_LOGO.'"/></a></td>
                        </tr>
                     <tr>
                      <td>
                       <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                        <tbody>
                         <tr style="background-color:#f3f3f3">
                          <td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"></div>
                          </td>
                          <td></td>
                         </tr>
                        </tbody>
                       </table>
                       <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                       <tbody>
                        <tr>
                         <td>
                          <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">';

        $tpl[1] = '<br /><br /><p style="display:block;margin:0 0 17px">
                                Regards,<br />
                                Muvi Team
                            </p>
 
      </div>
     </td>
    </tr>
                 
   </tbody>
   </table>
      </td>
    </tr>
  </tbody>
</table>';

        return $tpl;
    }

public function actiongetStatusWise()
    {
        $this->layout=false;
        $email=Yii::app()->user->email; 
        $tickettingdb=$this->getAnotherDbconnection();
        $studio_id = Yii::app()->common->getStudiosId();
        $master_sql="select id from ticket_master where studio_email='".$email."'";         
        $master_id=$tickettingdb->createCommand($master_sql)->queryAll();      
        $status=$_REQUEST['status'];
        $cond = " ticket_master_id=" . $master_id[0]['id'];
        if($status=="Open"){
        $cond.=" and status in('New','Working','re-opened','Re-opened')";
        }
        else if($status=="Closed")
        {
           $cond.=" and status ='Closed'";
            
        }else
        {
         $cond.=" ";  
        }
        
        $num_rec_per_page = 20;
        $start_from=0;
     
        $orderby = ' last_updated_date DESC';
        $limit = " LIMIT $start_from, $num_rec_per_page";
     
        $res = $tickettingdb->createCommand("SELECT count(*) as total FROM ticket where".$cond)->queryAll();
      
        $total_records = $res[0]['total'];
        $total_pages = ceil($total_records / $num_rec_per_page);
        $model = new TicketForm;
        $list = $model->ticketList($cond, $orderby, $limit);
        $this->renderPartial('ticketstatusrender', array('list' => $list, 'total_pages' => $total_pages,'total_records' => $total_records));
        
    
}

}



