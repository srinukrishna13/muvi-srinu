<?php
$studio_id = $studio->id;
$posterImg = POSTER_URL . '/no-image-a.png';
?>
<div id="crop-avatar">
    <form id="bannerText" method="post" name="bannerText" action="<?php echo Yii::app()->getBaseUrl(true) ?>/apps/saveBannerText">
        <div class="row m-t-40">
            <div class="col-xs-12 BannerCarouselSec">
                <div id="myCarousel" class="carousel slide relative carousel-fade" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner">
                        <?php
                        $j   = 0;
                        if(!empty($banners)){ 
                            foreach ($banners as $banner) {
                                $banner_src      = $banner->image_name;
                                $banner_id       = $banner->id;
                                $url             = $banner->banner_url;
                                $banner_full_url = $banner_url . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                ?>
                                <div class="item <?php echo ($j == 0) ? 'active' : ''; ?>">
                                    <img class="<?php echo $j; ?>-slide img-responsive" src="<?php echo $banner_full_url; ?>" />
                                    <div class="overlay-bottom overlay-white">
                                        <div class="overlay-Text">
                                            <div>
                                                <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id;?>, 1 )" class="btn btn-danger icon-with-fixed-width">
                                                    <em class="icon-trash" title="Remove Banner"></em>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row absolute full-width m-t-20">
                                        <div class="col-md-8">
                                            <div class="form-horizontal">    
                                                <div class="form-group">
                                                   <div class="col-md-4">
                                                        <input type="hidden" value="<?php echo $banner_id; ?>" name="banner_id[]" />
                                                        <div class="urltext">
                                                            <label>
                                                                <i class="input-helper"></i> Enter the URL for Banner:
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="fg-line">
                                                            <input type="url" class="form-control input-sm" name="url_text[]" placeholder="Enter the URL like:https://www.google.co.in" value="<?php echo $url; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <?php
                                $j++;
                            }
                        }else{ ?>
                            <div class="item active">                        
                                <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt=""  />              
                                <div class="overlay-bottom overlay-white">
                                    <div class="overlay-Text">
                                        <div>
                                            <a href="javascript:void(0);" class="btn btn-success icon-with-fixed-width avatar-view" data-toggle="modal" data-name="Banner" data-width="<?php echo $banner_details->banner_width; ?>" data-height="<?php echo $banner_details->banner_height; ?>" onclick="openAppImageModal(this)" >
                                                <em class="icon-plus" title="Add Banner"></em>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php    
                        }
                        ?>
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="icon-arrow-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="icon-arrow-right"></span></a>
            </div>
        </div>
        <div class="row m-t-20">
            <div class="col-sm-12 m-t-10">
                <div class="displayInline">
                    <ul id="banner_sortable" class="p-l-0 sortable">
                        <?php
                        if(!empty($banners)){
                            foreach ($banners as $banner) {
                                $banner_src      = $banner->image_name;
                                $banner_id       = $banner->id;
                                $url             = $banner->banner_url;
                                $banner_full_url = $banner_url . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                $seq[]           = $banner_id;
                                ?>
                                <li id="sortable_<?php echo $banner_id; ?>"> <img class="img-thumbnail" src="<?php echo $banner_full_url; ?>" /> </li>     
                                <?php
                            }
                        } ?> 
                    </ul> 
                </div>
                <div class="displayInline">
                    <a href="javascript:void(0);" data-name="Banner" data-width="<?php echo $banner_details->banner_width; ?>" data-height="<?php echo $banner_details->banner_height; ?>" onclick="openAppImageModal(this)" class="btn upload-Image relative">
                        <em class="icon-plus absolute" title="Add Banner"></em>
                    </a>
                </div>
            </div>
        </div>
        <div class="row m-t-20">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Text on Banner:</label>
                                <div class="col-md-8">
                                    <div class="fg-line"> 
                                        <input type="text" class="form-control input-sm" id="bnr_txt" name="bnr_txt" placeholder="Add text that goes on top of banner here" value="<?php echo html_entity_decode(@$banner_text); ?>">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <div id="bnr_txt_error" class="error m-b-20"></div>
                                    <input type="button" class="btn btn-primary btn-sm" id="savetxt" value="Update" onclick="return removeDisable();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

