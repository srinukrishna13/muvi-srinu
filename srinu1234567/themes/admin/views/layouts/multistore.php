<?php
if (@IS_LANGUAGE == 1) {
    $languages = $this->enable_laguages;
    $enable_lang = $this->language_code;
    if ($_COOKIE['Language']) {
        $enable_lang = $_COOKIE['Language'];
    }
}
$poster_sizes = $this->poster_sizes;
$horizontal = $poster_sizes['horizontal'];
$vertical = $poster_sizes['vertical'];
$hor_width = $horizontal['width'];
$hor_height = $horizontal['height'];
$ver_width = $vertical['width'];
$ver_height = $vertical['height'];

$lang_code = 'en';
if (@IS_LANGUAGE == 1) {
    if (isset(Yii::app()->controller->language_code) && trim(Yii::app()->controller->language_code)) {
        $lang_code = Yii::app()->controller->language_code;
    }
}
?>
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_code; ?>" lang="<?php echo $lang_code; ?>">
    <head>
        <meta charset="UTF-8">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="icon" type="image/*" href="<?php echo $this->favIcon;?>" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>    
        <!--Custom CSS-->
        <style>

            #example_filter{display:none;}
            table.display tbody tr:nth-child(even) td{
                background-color: white !important;
            }

            table.display tbody tr:nth-child(odd) td {
                background-color: white !important;
            }
            table.display tbody tr:nth-child(odd):hover td {
                background: #fafbfc !important;
            }
            table.display tbody tr:nth-child(even):hover td{
                background: #fafbfc !important;
            }
            #srch_help{
                display: none;
            }
            .search_help{
                background: #fff none repeat scroll 0 0;
                margin-top: 20px;
                height: auto;

            }
            .default_cont{
                height: 500px;
                overflow: auto; 
            }
            .back_btn{
                color: #21C2F8;
                font-weight: bold;
                cursor: pointer;
            }
            .help-toggle{
                background: #fff none repeat scroll 0 0;
                border: 1px solid #ccc;
                color: #505050;
                float: right;
                font-size: 1.5em;
                padding: 3px 13px;
                border-radius: 18px;
            }

            .ui-widget-content{
                z-index: 999999 !important;
            }


        </style>
        <link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/cssnew/datatable.css">        

        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssstyle/muvi.css?v=<?php echo RELEASE; ?>">
        <!--Vendor / CSS from 3rd Party-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jquery-ui.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.2.3/css/simple-line-icons.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jquery.mCustomScrollbar.css?v=<?php echo RELEASE; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/easypiechart.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/footable.core.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/sweetalert.css?v=45">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9 ]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link rel="stylesheet" href="css/ie-8.css">
        <![endif]-->

        <script type="text/javascript">
            var HTTP_ROOT = '<?php echo Yii::app()->getBaseUrl(true); ?>';
            var POSTER_URL = '<?= POSTER_URL; ?>';
            var HOR_POSTER_WIDTH = "<?php echo $hor_width; ?>";
            var HOR_POSTER_HEIGHT = "<?php echo $hor_height; ?>";
            var VER_POSTER_WIDTH = "<?php echo $ver_width; ?>";
            var VER_POSTER_HEIGHT = "<?php echo $ver_height; ?>";
        </script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jquery.min.js"></script>
        <script type="text/javascript">
            /*  if (!window.jQuery) {
             document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
             } */
            //Add blue animated border and remove with condition when focus and blur

            $(document).ready(function () {


                if ($('.fg-line')[0]) {
                    $('body').on('focus', '.fg-line .form-control', function () {
                        $(this).closest('.fg-line').addClass('fg-toggled');
                    });

                    $('body').on('blur', '.form-control', function () {
                        var p = $(this).closest('.form-group, .input-group'),
                                i = p.find('.form-control').val();

                        if (p.hasClass('fg-float')) {
                            if (i.length === 0) {
                                $(this).closest('.fg-line').removeClass('fg-toggled');
                            }
                        } else {
                            $(this).closest('.fg-line').removeClass('fg-toggled');
                        }
                    });
                }
                $('body').on('click', '.action', function (e) {
                    e.preventDefault();
                    $('.left-pos-and-initial-Show').toggleClass('sidebar-toggled');
                });
                // Select the image in gallery
                $('.Preview-Block').click(function(){
                    $('.overlay').removeAttr('style');
                    $(this).children().find('.overlay').css('opacity','1');
                });


            });            
        </script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/footable.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/custom.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/sweetalert.js"></script>
        <!-- Vendor / JS from 3rd Party-->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jquery.mCustomScrollbar.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/owl.carousel.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jquery.easypiechart.min.js"></script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/common/js/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $.validator.addMethod("mail", function (value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, "Please enter a correct email address");
                jQuery.validator.addMethod("phone", function (value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, "Please enter correct phone number");

                $(".help-toggle").click(function () {
                    $(".helpbox").animate({width: 'toggle'}, "slow");
                    $(".help-toggle").toggle();
                });
                $(".back_btn").click(function () {
                    document.getElementById('srch_help').style.display = "none";
                    document.getElementById('dflt_cont').style.display = "block";
                });


            });
            (function ($) {
                $(window).load(function () {
                    $(".left-pos-and-initial-Show").mCustomScrollbar({
                        mouseWheelPixels: 100,
                        scrollInertia: 500
                    });

                });
            })(jQuery);

            function changeLang(lang_code) {
                var cname = "Language";
                var cvalue = lang_code;
                setCookie(cname, cvalue, 90);
                window.location.href = window.location.href;
            }

            function setCookie(cname, cvalue, exdays) {
                var domain = window.location.hostname;
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=." + domain + ";path=/";
            }
        </script>

    </head>
    <?php    
    $studio = $this->studio;
    $home_page = Yii::app()->getbaseUrl(true).'/partners';
    ?>
    <body id="body_alert"> 
        <nav class="navbar navbar-default navbar-fixed-top">
            <ul class="nav navbar-nav">
                <!--Hamburger Menu : Displayed in Tablet and Mobile Only-->
                <li>
                    <button class="action action--open" aria-label="Close Menu">
                        <em class="icon-menu c-black"></em>
                    </button>
                </li>
                <!--Logo : Displayed in Large and Medium Sized Desktop Only-->
                <li class="hidden-xs hidden-sm">
                    <a class="navbar-brand" href="<?php echo $home_page; ?>" style="padding: 1px 17px;">
                        <?php
                            $siteLogo = $this->siteLogo;
                        ?>
                        <img src="<?php echo Yii::app()->baseUrl . '/img/muvi-studio-logo.png'; ?>" alt="<?php echo ucfirst($studio->name);?>" >
                    </a>
                </li>
                <!--Menu -->
                <li class="pull-right relative">                    
                    <ul class="list-inline MenuRight">                        
                        <?php if (@IS_LANGUAGE == 1) { ?>
                            <li>
                                <form class="">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control input-sm" onchange="changeLang(this.value)" style="width:100px">
                                                    <?php
                                                    foreach ($languages as $key => $value) {
                                                        if ($value['status'] != 0) {
                                                            ?>
                                                            <option value="<?php echo $value['code']; ?>" <?php if ($enable_lang == $value['code']) {
                                                    echo "SELECTED";
                                                } ?>><?php echo $value['name']; ?></option>
                                                        <?php } elseif ($value['code'] == "en") { ?>
                                                            <option value="<?php echo $value['code']; ?>" <?php if ($enable_lang == $value['code']) {
                                                    echo "SELECTED";
                                                } ?>><?php echo $value['name']; ?></option>
        <?php }
    } ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        <?php } ?>
                        <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)) { ?>
                        <li class="dropdown">
                            <!--Logged in User Name-->
                            <a data-toggle="dropdown" href="" aria-expanded="false">
                                <span class="hidden-xs text-capitalize">
                                    <?php
                                    if (Yii::app()->user->first_name) {
                                        echo ucfirst(Yii::app()->user->first_name) . " " . ucfirst(Yii::app()->user->getState('last_name'));
                                    } else {
                                        echo strstr(Yii::app()->user->email, '@', true);
                                    }
                                    ?> &nbsp;&nbsp;
                                </span>
                                <em class="icon-options-vertical font-12"></em>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li class="LoggedIn-User">
                                    <b class="text-capitalize">
                                        <?php
                                        if (Yii::app()->user->first_name) {
                                            echo ucfirst(Yii::app()->user->first_name) . " " . ucfirst(Yii::app()->user->getState('last_name'));
                                        } else {
                                            echo strstr(Yii::app()->user->email, '@', true);
                                        }
                                        ?>
                                    </b>
<?php if (isset(Yii::app()->user->created_at)) { ?>
                                        <span class="light-grey">Member since <?php echo date('M Y', strtotime(Yii::app()->user->created_at)); ?></span>
<?php } ?>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('partners/logout'); ?>">
                                        <em class="icon-power"></em> &nbsp;&nbsp;Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php }?>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="container-fluid">
            <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)) { ?>
            <?php
            @$activeAction = @Yii::app()->controller->action->id;
            $active_item = "";
            $icon_rotate = "";
            $style_menu = "";
            ?>
            <!--Sidebar Navigation-->
            <div class="sidebar left  left-pos-and-initial-Show">
                <ul class="menu-wrap">
                    <li>
                        <a href="<?php echo $home_page; ?>">
                            <em class="icon-home left-icon"></em> Home
                        </a>
                    </li>
                    <?php 
                    $data = User::model()->findByPk(Yii::app()->user->id);
                    if ($data->permission_id==1) {
                        if (in_array($activeAction, array('managecontent', 'managevideo'))) {
                            $active_item = "active-item";
                        }
                        ?>
                        <li class="<?php echo $active_item; ?>">
                            <a href="javascript:void(0);">
                                <em class="icon-film left-icon"></em> Manage Content
                                <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                            </a>

                            <!--Submenus-->
                            <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                <li <?php if ($activeAction == 'managecontent') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('admin/managecontent'); ?>">
                                        Content Library
                                    </a>
                                </li>
                                <li <?php if ($activeAction == 'managevideo') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('management/managevideo'); ?>">
                                        Video Library
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) { ?>
    <?php
    
        $active_item = "";
        if (in_array($activeAction, array('revenue', 'subscribers', 'usages', 'usersReport', 'userdata', 'general', 'video','users'))) {
            $active_item = "active-item";
        }
        $user_id = yii::app()->user->id;
        $data = Yii::app()->general->PartnerCustomReport($user_id);
        $datcount=$data[0]['countid'];
        $datavideo = Yii::app()->general->PartnerCustomReportcontent($user_id);
        $datvideocount=$datavideo[0]['countid'];
        ?>
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-graph left-icon"></em> Analytics
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <?php
                                        $studio = $this->studio;
                                        $studio_id = $studio->id;
                                        if($studio_id != 1977){
                                    ?>
                                    <li <?php if ($activeAction == 'revenue') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/revenue'); ?>">Revenue</a>
                                    </li>
                                        <?php }?>
                                    <li <?php if ($activeAction == 'video') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('partners/video'); ?>">Video</a>	
                                    </li>
                                </ul>
                            </li>
    
    <?php
    
        $active_item = "";
        if (in_array($activeAction, array('serverLocation', 'account', 'manageUsers', 'advanced', 'partners', 'emailNotifications', 'managelanguage'))) {
            $active_item = "active-item";
        }
        ?> 
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-settings left-icon"></em> Settings
                                    <em class="icon-arrow-right right-icon  <?php echo $icon_rotate; ?>"></em>
                                </a>

                                <!--1st Level Submenu-->
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li <?php if ($activeAction == 'account') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('partners/account'); ?>">Account Info</a>
                                    </li>
                                            </ul>
                                        </li>
        
    <?php } ?>
                </ul>
            </div>
            <!--Sidebar : Help Block-->
            <?php }?>
            <div <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)) { ?>class="content"<?php }?>>
                <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)) { ?>
                <!--Active Page Header-->
                <div class="Current_PageHeader">
                    <div class="row">
                        <div class="col-xs-10">
                            <?php
                            if (isset($this->breadcrumbs)):

                                if (Yii::app()->controller->route !== 'site/index')
                                    $this->breadcrumbs = array_merge(array(Yii::t('zii', 'Home') => $home_page), $this->breadcrumbs);

                                $this->widget('zii.widgets.CBreadcrumbs', array(
                                    'links' => $this->breadcrumbs,
                                    'homeLink' => false,
                                    'tagName' => 'ol',
                                    'separator' => '',
                                    'activeLinkTemplate' => '<li><a href="{url}">{label}&nbsp;</a></li>',
                                    'inactiveLinkTemplate' => '<li class="active"><span>&nbsp;{label}</span></li>',
                                    'htmlOptions' => array('class' => 'breadcrumb font-10 hidden-xs')
                                ));

                            endif;
                            ?>	
<?php if (Yii::app()->controller->action->id == 'dashboard') { ?>
                                <h3 class="text-capitalize f-300">dashboard</h3>

<?php } elseif (isset($this->headerinfo) && $this->headerinfo) { ?>

                                <h3 class="text-capitalize f-300"><?php echo @$this->headerinfo; ?></h3>
<?php } else { ?>
                                <h3 class="text-capitalize f-300"><?php
    if (isset($this->breadcrumbs)) {
        echo $this->breadcrumbs[0];
    }
    ?></h3>
<?php } ?>
                        </div>
                    </div>
                </div>
                <?php }?>
                <?php echo $this->website_msg;?>
<?php if (Yii::app()->user->hasFlash('success')) { ?>
                    <div class="alert alert-success alert-dismissable flash-msg m-t-20">
                        <i class="icon-check"></i> &nbsp;
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>

<?php } elseif (Yii::app()->user->hasFlash('error')) { ?>
                    <div class="alert alert-danger alert-dismissable flash-msg m-t-20">
                        <i class="icon-ban"></i>&nbsp;
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
<?php } ?>
                <div class="pace  pace-inactive">
                    <div class="pace-progress" style="width: 100%;" data-progress-text="100%" data-progress="99">
                        <div class="pace-progress-inner"></div>
                    </div>
                    <div class="pace-activity"></div>
                </div>

                <!-- Main content -->

<?php echo $content; ?>

            </div>

        </div>

        <script type="text/javascript">
            /* Bootbox modal vertical center Align 
             $("body").on("shown.bs.modal", ".modal", function() {
             $(this).css({
             'top': '40%',
             'margin-top': function () {
             return -($(this).height() / 3);
             }
             });
             });
             /* Bootbox modal vertical center Align End */

            var sucerrpopup = '';
            $(document).ready(function () {
                sucerrpopup = setTimeout("clearmsgpopu()", 10000);
            });
            $(window).load(function () {
                $(".treeview.active").find('ul.treeview-menu').show();

            });
            function clearmsgpopu() {
                $('.alert').fadeOut('slow');
                clearTimeout(sucerrpopup);
            }
            var is_sdk = 0;
        <?php if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) { ?>
                var is_sdk = 1;
<?php } ?>
            /*Help Block*/
            $('.info-Action').click(function () {
                $('.right-pos-and-initial-Hide').addClass('sidebar-toggled');
            });
            $('.close-Action').click(function () {
                $('.right-pos-and-initial-Hide').removeClass('sidebar-toggled');
            });
        </script>  

        <!------- ends --->
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/validator.min.js"></script>
<?php 
$hide_chat = StudioConfig::model()->getconfigvalueForStudio($this->studio->id,'hide_chat');
if(empty($hide_chat) || $hide_chat['config_value'] == 0){
if (HOST_IP == '52.0.232.150') { ?>
            <!--Start of Zopim Live Chat Script-->
            <script type="text/javascript">
                window.$zopim || (function (d, s) {
                    var z = $zopim = function (c) {
                        z._.push(c)
                    }, $ = z.s =
                            d.createElement(s), e = d.getElementsByTagName(s)[0];
                    z.set = function (o) {
                        z.set.
                                _.push(o)
                    };
                    z._ = [];
                    z.set._ = [];
                    $.async = !0;
                    $.setAttribute("charset", "utf-8");
                    $.src = "v2.zopim.com/?4FUwMIfmMcuWH7Z6ixpoo5zrKEtqvFNO";
                    z.t = +new Date;
                    $.
                            type = "text/javascript";
                    e.parentNode.insertBefore($, e)
                })(document, "script");
            </script>
            <!--End of Zopim Live Chat Script-->  
            
            <script type="text/javascript">

    var intervalVariable;
intervalVariable = setInterval(zopim30sec, 10000);
$zopim(function () { 
    
    $zopim.livechat.setOnConnected(function () {
       
        $zopim.livechat.departments.clearVisitorDepartment();
        var zemail = $zopim.livechat.getEmail();
        var zname = $zopim.livechat.getName();
		//alert(CHECKSTUDIOLOGIN);
	<?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
		<?php } else { ?>
        
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
		<?php } ?>
        
        //zopim redirection to sales and support department as per the login details
        if (zemail != '') {
            if (getCookie("zopimemail") != zemail) {
                $.post("/contact/SendFromZopim", {"email": zemail, "name": zname}, function () {
                    document.cookie = "zopimemail=" + zemail;
                });
            }
        } else {
            //intervalVariable = setInterval(zopim30sec, 20000);
        }
    });
    
     
   });  
$zopim(function() {
$zopim.livechat.setOnChatEnd(end);
});
function end() {
$zopim.livechat.clearAll();
}
function zopim30sec() {
    
    $zopim.livechat.departments.clearVisitorDepartment();
    
    var zemail1 = $zopim.livechat.getEmail();
    var zname1 = $zopim.livechat.getName();
    //zopim redirection to sales and support department as per the login details
     //alert(CHECKSTUDIOLOGIN);
    <?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
        <?php } else { ?>
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
       <?php } ?>
	
        
    //zopim redirection to sales and support department as per the login details
    if (zemail1 != '') {
        if (getCookie("zopimemail") != zemail1) {
            $.post("/contact/SendFromZopim", {"email": zemail1, "name": zname1}, function () {
                document.cookie = "zopimemail=" + zemail1;
                //clearInterval(intervalVariable);
            });
        }
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
</script>
<?php }} ?>

        <div class="modal fade" id="monetization-menu-settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Settings</h4>
                    </div>
                    <div class="modal-body" id="monetization-menu-settings-body">
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
