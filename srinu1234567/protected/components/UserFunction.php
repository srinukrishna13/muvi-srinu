<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Yii::import('ext.EGeoIP');
Yii::import('ext.ipInfo');

//Yii::import('ext.geoPlugin');
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class UserFunction extends AppComponent {

    public function getName($user_id) {
        $return = 'User ID : ' . $user_id;
        return $return;
    }

    public function translate($param) {
        $agent = self::getStudio();
    }

    /*
     * Collecting all featured contents for a home page section 
     * @author RKS
     */

    public function getFeaturedContents($section_id) {
        $studio_id = self::getStudiosIdpreview();
        $featured = new FeaturedContent;

        $cond = 'section_id = ' . $section_id . ' AND t.studio_id=' . $studio_id;
        $featured = $featured::model()->findAll(
                array(
                    'condition' => $cond,
                    'order' => 't.id_seq ASC, t.id DESC'
                )
        );
        //$featured = $featured->with(array('movie'=>array('alias'=>'mv')))->findAllByAttributes(array('studio_id' => $studio_id, 'section_id' => $section_id, 't.movie_id' => 'mv.id'), array('order' => 'id_seq ASC'));   
        return $featured;
    }

    public function getFeaturedContentsNew($section_id) {
        $studio_id = self::getStudiosIdpreview();
        $controller = Yii::app()->controller;
        $language_id = $controller->language_id;
		$theme_name = Studio::model()->findByPk($studio_id, array('select' => 'parent_theme'))->parent_theme;
		$content_type = Template::model()->findByAttributes(array('code' => $theme_name), array('select' => 'content_type'))->content_type;
		$con = Yii::app()->db;
		$movieids = '';
		if ($content_type == 3){
			$data = $con->createCommand()
				->select('*')
				->from('featured_content f')
				->where('f.studio_id=:studio_id and f.section_id=:section_id', array(':studio_id' => $studio_id, ':section_id' => $section_id),array('order'=>'id_seq ASC'))
				->queryAll();		
		}else{
			$sql = "SELECT * FROM homepage WHERE studio_id =" . $studio_id . " AND section_id=" . $section_id . " ORDER BY id_seq ASC";
			$data = $con->createCommand($sql)->queryAll();
		}
		// Get the featured content details from the Homepage view
        $fcont = array();
        if ($data) {
            foreach ($data AS $key => $val) {
                $featured   = $val;
				if ($content_type == 3){
					$is_episode = $val['is_episode'];
					if($is_episode == 1){
						$stream_id = $val['movie_id'];
						$movie_id = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id,'id'=>$stream_id), array('select' => 'movie_id'))->movie_id;
						$content_id = $stream_id;
					}else{
						$movie_id =$val['movie_id'];
						$stream_id = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id,'movie_id'=>$movie_id), array('select' => 'id'))->id;
						$content_id = $movie_id;
					}
				}else{
                $movie_id   = $val['movie_id'];
                $stream_id  = $val['stream_id'];
                $is_episode = $val['is_episode'];
                if($is_episode == 1){
                    $content_id = $stream_id; 
                }else{
                     $content_id = $movie_id;
                }
				}
                $langcontent = Yii::app()->custom->getTranslatedContent($content_id,$is_episode, $language_id);
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $featured['name']  = $langcontent['film'][$movie_id]->name;
                    $featured['story'] = $langcontent['film'][$movie_id]->story;
                }
                if (array_key_exists($stream_id, $langcontent['episode'])) {
                    $featured['episode_title'] = $langcontent['episode'][$stream_id]->episode_title;
                    $featured['episode_story'] = $langcontent['episode'][$stream_id]->episode_story;
                }
                if ($val['is_episode'] != 1){
                    $movieids .= $movie_id . ",";
                }
                $fcont[] = $featured;
            }
            $data = $fcont;
            if ($movieids) {
                $csql = "SELECT GROUP_CONCAT(celb.name) AS celb_name,GROUP_CONCAT(celb.permalink) AS celb_permalink,mc.movie_id FROM movie_casts mc , celebrities celb WHERE mc.movie_id IN(" . trim($movieids, ',') . ") AND mc.celebrity_id = celb.id GROUP BY mc.movie_id ORDER BY mc.id ASC";
                $casts = $con->createCommand($csql)->queryAll($csql);
                foreach ($casts AS $ckey => $cval) {
                    $celbname = explode(',', $cval['celb_name']);
                    $celbplink = explode(',', $cval['celb_permalink']);
                    $celebrity[$cval['movie_id']] = array_combine($celbplink, $celbname);
                }
                $fcontent['Celebrity'] = $celebrity;
            }
        }
        $fcontent['contents'] = $data;
        return $fcontent;
    }

    public function getFeaturedAllContents($section_id, $apphome = false) {
        $studio_id = self::getStudiosId();
        if($apphome){
            $featured = new AppFeaturedContent;
        }else{
            $featured = new FeaturedContent;
        }

        $cond = 'section_id = ' . $section_id . ' AND t.studio_id=' . $studio_id;
        $featured = $featured::model()->findAll(
                array(
                    'condition' => $cond,
                    'order' => 't.id_seq ASC, t.id ASC'
                )
        );
        //$featured = $featured->with(array('movie'=>array('alias'=>'mv')))->findAllByAttributes(array('studio_id' => $studio_id, 'section_id' => $section_id, 't.movie_id' => 'mv.id'), array('order' => 'id_seq ASC'));   
        return $featured;
    }

    public function getSalesEmails() {
        $studio_id = self::getStudioId();
        $default_email = 'sales@muvi.com';
        if ($studio_id > 0) {
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            /*
              if($studio->contact_us_email != '')
              $mail = $studio->contact_us_email;
              else
             * *
             */
            if (isset($studio)) {
                $user = new User;
                $admin = $user->findAllByAttributes(array('studio_id' => $studio_id, 'is_sdk' => 1, 'role_id' => 1));
                if (count($admin) > 0) {
                    $mail = $admin[0]->email;
                } else {
                    $mail = $default_email;
                }
            }
            $store_emails = array(
                array(
                    'email' => $default_email,
                    'name' => $studio->name,
                    'type' => 'to'
                ),
                array(
                    'email' => $mail,
                    'name' => $studio->name,
                    'type' => 'to'
                ),
            );
        } else {
            $store_emails = array(
                array(
                    'email' => $default_email,
                    'name' => 'Muvi',
                    'type' => 'to'
                )
            );
        }
        return $store_emails;
    }

    public function getStudioContactEmails() {
        $studio_id = self::getStudiosId();
        $default_email = 'sales@muvi.com';
        if ($studio_id > 0) {
            if (!$this->studioData) {
                $studio = new Studio();
                $studio = $studio->findByPk($studio_id);
            } else {
                $studio = $this->studioData;
            }
            /*
              if($studio->contact_us_email != '')
              $mail = $studio->contact_us_email;
              else
             * *
             */
            if (isset($studio)) {
				if(@$studio->contact_us_email){
					$mail = $studio->contact_us_email;
				}else{
					$user = new User;
					$admin = $user->findAllByAttributes(array('studio_id' => $studio_id, 'is_sdk' => 1, 'role_id' => 1));
					if (count($admin) > 0) {
						$mail = $admin[0]->email;
					} else {
						$mail = $default_email;
					}
				}
            }
            $store_emails = array(
                array(
                    'email' => $mail,
                    'name' => $studio->name,
                    'type' => 'to'
                ),
            );
        } else {
            $store_emails = array(
                array(
                    'email' => $default_email,
                    'name' => 'Muvi',
                    'type' => 'to'
                )
            );
        }
        return $store_emails;
    }

    public function getStudioEmail($studio_id = '') {
        if (!$studio_id) {
            $studio_id = self::getStudiosId();
        }
        $default_email = 'studio@muvi.com';
        if ($studio_id > 0) {
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            if ($studio->contact_us_email != '' && strlen($studio->contact_us_email) > 0 && $studio->contact_us_email != NULL)
                $store_emails = $studio->contact_us_email;
            else {
                $user = new User;
                $admin = $user->findAllByAttributes(array('studio_id' => $studio_id, 'is_sdk' => 1, 'role_id' => 1));
                if (count($admin) > 0) {
                    $store_emails = $admin[0]->email;
                } else {
                    $store_emails = $default_email;
                }
            }
        }
        return $store_emails;
    }

    public function getStudioInfo($studio_id) {
        $studio = array();
        if ($studio_id > 0) {
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
        }
        return $studio;
    }

    function getTemplateDetails($template_code) {
        $templates = array();
		$studio_id = self::getStudiosId();
        if ($template_code != '') {
			/*if(@$_SESSION[$studio_id]['StudioTemplates'] && $_SESSION[$studio_id]['StudioTemplates']){
				return $_SESSION['StudioTemplates'];
			}else{*/
				$template = new Template;
				$template_data = $template->findAllByAttributes(array('code' => $template_code));
				$_SESSION[$studio_id]['StudioTemplates'] = $template_data[0];
				return $template_data[0];
			//}
        } else
            return $templates;
    }

    public function MuviAdminEmails($type) {
        $emails = array();
         $host_ip = Yii::app()->params['host_ip'];
      
        if (!in_array(HOST_IP,$host_ip)) {
            if ($type == 'autosignup_from') {
                $emails = array('email' => 'sunil@muvi.com', 'name' => 'Sunil Kund');
            } else {
                $emails = array(
                    array('email' => 'manas@muvi.com', 'name' => 'Sunil Kund', 'type' => 'to'),
                    array('email' => 'priyadarshini@muvi.com', 'name' => 'Priyadarshini Swain', 'type' => 'to'),
                );
            }
        } else {
            if ($type == 'autosignup_recipients') {
                $emails = array(
                    array('email' => 'sales@muvi.com', 'name' => 'Muvi Sales', 'type' => 'to'),
                    //array('email' => 'muvifreetrial@robot.zapier.com', 'name' => 'Muvi Sales', 'type' => 'cc'),
                    array('email' => 'development@muvi.com', 'name' => 'Dev Team', 'type' => 'bcc')
                );
            } else if ($type == 'autosignup_from') {
                $emails = array('email' => 'studio@muvi.com', 'name' => 'Muvi');
            } else if ($type == 'credit_card_failure') {
                $emails = array(
                    array('email' => 'sunil@muvi.com', 'name' => 'Sunil Kund', 'type' => 'to'),
                    array('email' => 'mohan@muvi.com', 'name' => 'Mohan Kumar', 'type' => 'bcc'),
                    array('email' => 'anshuman@muvi.com', 'name' => 'Anshuman', 'type' => 'bcc')
                );
            } else if ($type == 'cancel_recipients') {
                $emails = array(
                    array('email' => 'studio@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
            } else if ($type == 'payment_gateway') {
                $emails = array(
                    array('email' => 'studio@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
            } else if ($type == 'subscription_recipients') {
                $emails = array(
                    array('email' => 'all@muvi.com', 'name' => 'Muvi Team', 'type' => 'to')
                );
            } else if ($type == 'invoice_paid') {
                $emails = array(
                    array('email' => 'studio@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
            } else if ($type == 'payment_failed') {
                $emails = array(
                    array('email' => 'studio@muvi.com', 'name' => 'Muvi', 'type' => 'to')
                );
            }
        }
        return $emails;
    }

    public function MuviAdminEmailsNew($type) {
        $emails = array();
         $host_ip = Yii::app()->params['host_ip'];
      
        if (!in_array(HOST_IP,$host_ip)) {
            if ($type == 'autosignup_from') {
                $emails = array('support@muvi.com');
            } /*else if ($type == 'free_trial_expired') {
                $emails = array('sales@muvi.com');
            } */else {
                $emails = array('sunil@muvi.com', 'rasmi@muvi.com', 'suraja@muvi.com', 'priyadarshini@muvi.com');
            }
        } else {
            if ($type == 'autosignup_recipients') {
                $emails = array('sales@muvi.com');
            } else if ($type == 'billing_from') {
				$emails = array('billing@muvi.com');
            } else if ($type == 'autosignup_from') {
                $emails = array('kajal@muvi.com');
            } else if ($type == 'credit_card_failure') {
                $emails = array('sunil@muvi.com', 'sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'cancel_recipients') {
                $emails = array('sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'cancel_recipients_free_trial_expiry') {
                $emails = array('sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'payment_gateway') {
                $emails = array('sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'subscription_recipients') {
                $emails = array('all@muvi.com');
            } else if ($type == 'invoice_paid') {
                $emails = array('sunil@muvi.com', 'sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'payment_failed') {
                $emails = array('sunil@muvi.com', 'sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'cancel_ssl') {
                $emails = array('ssl@muvi.com');
            } else if ($type == 'wire_transfer') {
                $emails = array('sunil@muvi.com', 'sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'devhours_purchase') {
                $emails = array('sunil@muvi.com', 'sales@muvi.com', 'accounts@muvi.com', 'support@muvi.com');
            } else if ($type == 'free_trial_expired') {
                $emails = array('sales@muvi.com');
            } else if ($type == 'delink_from_reseller') {
                $emails = array('sales@muvi.com');
            }
        }
        return $emails;
    }

    public function getUserInformation($user_id) {
        $connection = Yii::app()->db;
        $sql = "SELECT * FROM users WHERE id = '" . $user_id . "' LIMIT 1";
        $command = $connection->createCommand($sql);
        $users = $command->queryAll(); // execute a query SQL    

        if (count($users) > 0) {
            foreach ($users as $user) {
                return $user;
            }
        } else
            return '';
    }

    public function getContentTypeTitle($type) {
        $con = Yii::app()->db;
        $qry = 'SELECT name FROM content_types WHERE muvi_alias = "' . $type . '" ORDER BY id';
        $contentTypes = $con->createCommand($qry)->queryAll();
        if (count($contentTypes) > 0) {
            return $contentTypes[0]['name'];
        } else {
            return '';
        }
    }

    /**
     * @method public getMasterContents()
     * @return array() 
     */
    public function getMasterContents($type = '') {
        $data = ContentType::model()->findAll();
        if ($data) {
            foreach ($data AS $key => $val) {
                $carr[$val->id] = $val->attributes;
            }
            return $carr;
        } else {
            return '';
        }
    }

    public function getMuviStudioDetails($studio_id) {
        $conn = Yii::app()->db;
        $sql = "SELECT * FROM user WHERE studio_id = '" . $studio_id . "' AND is_sdk = 1 LIMIT 1";
        $obj = $conn->createCommand($sql);
        $data = $obj->queryAll();
        if ($data) {
            return $data[0];
        }
        return array();
    }

    public function getRecurrance($plan_id) {
        $studio_id = '';
        $conn = Yii::app()->db;
        $sql = "SELECT recurrence FROM subscription_plans WHERE id = '" . $plan_id . "' LIMIT 1";
        $obj = $conn->createCommand($sql);
        $data = $obj->queryAll();
        if ($data) {
            foreach ($data as $dt) {
                return $dt['recurrence'];
            }
        }
        return '';
    }

    //Updated for preview functionality
    public function getStudiosId($params = '') {
        $studio_id = 0;
        if (isset(Yii::app()->user->studio_id) && Yii::app()->user->studio_id > 0) {
            $studio_id = Yii::app()->user->studio_id;
            if (!$this->studioData) {
                $std = Studio::model();
                $this->studioData = $studio = $std->findByPk($studio_id);
            }
        } else { 
            $studio_domain = self::getWebsiteSubdomain();
            if (!$this->studioData) {
                $std = Studio::model();
                $studio = $std->find("domain=:domain",array(':domain' => $studio_domain));
                if ($studio) {
                    $this->studioData = $studio;
                    $studio_id = $studio->id;
                }
            } else {
                $studio_id = $this->studioData->id;
            }
        }
        if ($params) {
            return $this->studioData;
        } else {
            return $studio_id;
        }
    }

    public function getStudiosIdPreview() {
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset(Yii::app()->request->cookies['in_preview_theme']) && Yii::app()->request->cookies['in_preview_theme'] != '') {
            $ip_address = Yii::app()->getRequest()->getUserHostAddress();
            $preview_template = new Previewtemplatehistory();
            $preview = $preview_template->findByAttributes(array('studio_id' => $studio_id, 'preview_ip' => $ip_address, 'is_preview' => 1), array('order' => 'id DESC'));
            if ($preview->template_name == 'traditional') {
                $studio_id = TRADITIONAL_PREVIEW_STUDIO;  // set traditional demo studio id
            } else if ($preview->template_name == 'classic') {
                $studio_id = CLASSIC_PREVIEW_STUDIO; // set classic demo studio id
            } else if ($preview->template_name == 'modern') {
                $studio_id = MODERN_PREVIEW_STUDIO; // set modern demo studio id
            }
        }

        return $studio_id;
    }

    public function getStudioId() {
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = Studio::model();
            $std = $studio->findByPk($studio_id);
        } else {
            $std = $this->studioData;
        }
        if (self::getOrgSubdomain() != 'studio' && ((isset(Yii::app()->request->cookies['in_preview_theme']->value) && Yii::app()->request->cookies['in_preview_theme']->value == 1) || $std->show_sample_data == 1) && (!isset(Yii::app()->user->is_sdk) || Yii::app()->user->is_sdk < 1))
            $studio_id = SAMPLE_DATA_STUDIO;
        else
            $studio_id = $std->id;

        return $studio_id;
    }

    public function getStudioLanguages($studio_id = NULL) {
        if (!isset($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        $con = Yii::app()->db;

        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.* FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.status=1) WHERE l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        //print $sql;exit;

        $studio_languages = $con->createCommand($sql)->queryAll();

        return $studio_languages;
    }

    public function getStudiodomain() {
        $dm = explode('.', $_SERVER['SERVER_NAME']);
        $tot = count($dm);
        return $dm[$tot - 2] . '.' . $dm[$tot - 1];
    }

    public function getWebsiteSubdomain() {
		$studio_domain = $_SERVER['SERVER_NAME'];
		if(strstr($studio_domain,'www')){
			$studio_domain = str_replace('www.', '', $studio_domain);
		}
        return $studio_domain;
    }

    public function getStudio() {
        $studio = '';
        $studio_domain = self::getWebsiteSubdomain();
        $std = Studios::model();
        $studio = $std->findAllByAttributes(array('domain' => $studio_domain));
        if ($studio) {
            foreach ($studio as $std) {
                $studio = $std->permalink;
            }
        }
        return $studio;
    }

    public function getMuviStudioTheme() {
        $theme = '';
        $conn = Yii::app()->db;
        $studio_id = self::getStudioId();

        $sql = "SELECT theme FROM studios WHERE id = '" . $studio_id . "' LIMIT 1";
        $obj = $conn->createCommand($sql);
        $data = $obj->queryAll();
        if ($data) {
            foreach ($data as $dt) {
                $theme = $dt['theme'];
            }
        }

        return $theme;
    }

    public function getAgent() {
        $studio_id = self::getStudiosId();
        $conn = Yii::app()->db;
        $subdomain = '';
		if($this->studioData){
			$studio = $this->studioData->attributes;
			return $studio['subdomain'];
		}else{
			$sql = "SELECT subdomain FROM studios WHERE id = '" . $studio_id . "' LIMIT 1";
			$obj = $conn->createCommand($sql);
			$data = $obj->queryAll();
			if ($data) {
				foreach ($data as $dt) {
					$subdomain = $dt['subdomain'];
				}
			}
			return $subdomain;
		}
    }

    public function getVisitorLocation($ip_address = 0) {
        $city = '';
        $region = '';
        $country_name = '';
        $country = '';
        $continent_code = '';
        $latitude = '';
        $longitude = '';
        $currency_code = '';
        $currency_symbol = '';
        $studio_id = self::getStudiosId();
        if ($ip_address == 0)
            $ip_address = Yii::app()->getRequest()->getUserHostAddress();
        if (isset($_SESSION[$studio_id]['country']) && $_SESSION[$studio_id]['country'] && isset($_SESSION[$studio_id]['ip_address']) && $ip_address == $_SESSION[$studio_id]['ip_address']) {
            $city = $_SESSION[$studio_id]['city'];
            $region = $_SESSION[$studio_id]['region'];
            $country_name = $_SESSION[$studio_id]['country_name'];
            $country = $_SESSION[$studio_id]['country'];
            $continent_code = $_SESSION[$studio_id]['continent_code'];
            $latitude = $_SESSION[$studio_id]['latitude'];
            $longitude = $_SESSION[$studio_id]['longitude'];
            $currency_code = $_SESSION[$studio_id]['currency_code'];
            $currency_symbol = $_SESSION[$studio_id]['currency_symbol'];
        } else {
            if ($ip_address == "127.0.0.1") {
                $city = 'Bhubaneswar';
                $region = 'Odisha';
                $country = 'IN';
                $country_name = 'India';
                $continent_code = 'AS';
                $currency_code = 'USD';
                $currency_symbol = '$';
            } else {
                $ip_address = $ip_address ? $ip_address : $_SERVER['REMOTE_ADDR'];
                $geoIp = new EGeoIP();
                $geoIp->locate($ip_address);

                $city = @$geoIp->getCity();
                $region = @$geoIp->getRegion();
                $country_name = @$geoIp->getCountryName();
                $country = @$geoIp->getCountryCode();
                $continent_code = @$geoIp->getContinentCode();
                $latitude = @$geoIp->getLatitude();
                $longitude = @$geoIp->getLongitude();
                $currency_code = @$geoIp->getCurrencyCode();
                $currency_symbol = @$geoIp->getCurrencySymbol();
                if (!$country) {
                    $API_KEY = '18132f37e646e4ae725a36486fbc8130826dc5c8712c0573dcb58d985fb262ab';
                    $ipInfo = new ipInfo($API_KEY);
                    $cityData = $ipInfo->getCity($ip_address);
//                  $countryData = $ipInfo->getCountry($ip_address);
//                  $countryData = explode(';', $countryData);

                    $cityData = explode(';', $cityData);
                    $city = $cityData[5];
                    $country_name = $cityData[4];
                    $country = @$cityData[3];
                    
                    $fail_log = new IPFailLog();
                    $fail_log->ip_address = $ip_address;
                    $fail_log->api_code = 'geoip';
                    $fail_log->failed_at = new CDbExpression("NOW()");
                    $fail_log->studio_id = $studio_id;
                    $fail_log->save();      
                    
                    //$old_theme = Yii::app()->theme;
//                    $logo = '<a href="' . Yii::app()->getBaseUrl(true) . '"><img src="https://www.muvi.com/themes/bootstrap/images/logo.png" alt="Muvi" /></a>';
//                    $msg = '<p>GEOIP Failed for studio ID '.$studio_id.' in Muvi Live!</p>';
//                    $msg .= '<p>IP Address '.$ip_address.'.</p>';
//                    $params = array(
//                        'website_name' => Yii::app()->getBaseUrl(true),
//                        'logo' => $logo,
//                        'msg' => $msg
//                    );
//                    $subject = 'GEOIP Failed!';
//                    $cc = array('pragyan@muvi.com', 'biswajit@muvi.com');
//                    Yii::app()->theme = 'bootstrap';
//                    $thtml = Yii::app()->controller->renderPartial('//email/studio_general_contact', array('params' => $params), true);
//                    $ret = Yii::app()->controller->sendmailViaAmazonsdk($thtml, $subject, 'ratikanta@muvi.com', 'support@muvi.com', $cc, '', '', 'Muvi Support');
                    //Yii::app()->theme = $old_theme;                       
                }

                //Keeping the unique visitor log
                if ($studio_id != 4) {
                    $log = new Visitorlog();
                    $log->ip_address = $ip_address;
                    $log->geo_location = serialize($loc);
                    $log->country = $country;
                    $log->visited_at = new CDbExpression("NOW()");
                    $log->studio_id = $studio_id;
                    $log->save();
                }
            }

            $_SESSION[$studio_id]['city'] = $city;
            $_SESSION[$studio_id]['region'] = $region;
            $_SESSION[$studio_id]['country_name'] = $country_name;
            $_SESSION[$studio_id]['country'] = $country;
            $_SESSION[$studio_id]['continent_code'] = $continent_code;
            $_SESSION[$studio_id]['latitude'] = $latitude;
            $_SESSION[$studio_id]['longitude'] = $longitude;
            $_SESSION[$studio_id]['ip_address'] = $ip_address;
            $_SESSION[$studio_id]['currency_code'] = $currency_code;
            $_SESSION[$studio_id]['currency_symbol'] = $currency_symbol;
        }

        $response = array('city' => $city, 'region' => $region, 'country' => $country, 'country_name' => $country_name, 'continent_code' => $continent_code, 'ip_address' => $ip_address, 'currency_code' => $currency_code, 'currency_symbol' => $currency_symbol);

        return $response;
    }

    public function isVisitorAllowedCountry() {
        $current_controller = Yii::app()->controller->id;
        $studio_id = self::getStudiosId();
        if ($current_controller == 'partners' || $current_controller == 'login') {
            return true;
        } else {
            $actions = array();
            $visitor_loc = self::getVisitorLocation();
            $country = $visitor_loc['country'];
            $std_countr = StudioCountryRestriction::model();
            $studio_restr = $std_countr->findAllByAttributes(array('studio_id' => $studio_id, 'country_code' => $country));
            $actions[] = 'sitemap/default/index';
            $actions[] = 'site/robotsTxt';
            $actions[] = 'page/show/privacy-policy1';
            $actionUrl = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk > 0) {
                return true;
            } else if (isset(Yii::app()->request->cookies['for_preview']) && Yii::app()->request->cookies['for_preview']->value == 2) {
                return true;
            } else if (in_array($actionUrl, $actions)) {
                return true;
            } else {
                if (count($studio_restr) > 0) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    public function muviAdmin() {
        $muvi = array(
            "name" => 'Muvi Admin',
            "email" => 'info@muvi.com',
        );
        return $muvi;
    }

    public function getMovieId($uniq_id) {
        $film = new Film();
        $flm = $film->findByAttributes(array('uniq_id' => $uniq_id));
        if (count($flm) > 0)
            return $flm->id;
        else
            return 0;
    }

    public function getStreamId($movie_id, $embed_id = '', $season = '', $studio_id = Null) {
        $stream = new movieStreams();
        $movie = Film::model()->findByPk($movie_id);

        //Explicitily pass from Rest controller otherwise default
        if (!isset($studio_id)) {
            $studio_id = self::getStudiosId();
        }

        if ($embed_id != '') {
            $stream = $stream->findByAttributes(array('embed_id' => $embed_id, "movie_id" => $movie_id));
            $stream_id = $stream->id;
        } else if ($movie_id > 0 && $season != '') {
            $stream = $stream->findByAttributes(array('series_number' => $season, 'movie_id' => $movie_id, 'is_active' => 1, 'is_converted' => 1), array('order' => 'id_seq ASC'));
            $stream_id = $stream->id;
        } else {
            // $content_type = Yii::app()->common->getContentTypeFromStudioContent($movie->content_type_id);
            $content_type = $movie->content_types_id;
            if ($content_type == 3) {
                $connection = Yii::app()->db;
                $sql = "SELECT id  FROM movie_streams WHERE studio_id=" . $studio_id . " AND movie_id=" . $movie_id . ' AND is_active = 1 AND  is_episode =1 AND full_movie != "" AND is_converted=1 ORDER BY id ASC LIMIT 1';
                $row = $connection->createCommand($sql)->queryAll();
                if (isset($row) && !empty($row)) {
                    $stream_id = $row[0]['id'];
                } else {
                    $stream_id = 0;
                }
            } else {
                $stream = $stream->findByAttributes(array('is_episode' => 0, 'movie_id' => $movie_id));
                $stream_id = $stream->id;
            }
        }
        if ($stream_id > 0)
            return $stream_id;
        else if (count($stream) > 0)
            return $stream->id;
        else
            return 0;
    }

    /* public function getContentTypeFromStudioContent($content_type_id)
      {
      $studio_id = self::getStudiosId();
      $content = StudioContentType::model()->findByPk($content_type_id);
      return $content->content_types_id;
      } */

    public function getMovieStreamId($movie_id) {
        $studio_id = self::getStudiosId();
        $mv = new movieStreams();
        $stream = $mv->findByAttributes(array('movie_id' => $movie_id, 'studio_id' => $studio_id));
        return $stream['id'];
    }

    //Added for PPV

    public function getVideoname($video_id) {
        $video_name = '';
        $film = new Film;

        $cond = 'ms.movie_id = ' . $video_id . ' AND t.id= ms.movie_id';
        $films = $film::model()->with(array('movie_streams' => array('alias' => 'ms')))->findAll(
                array(
                    'condition' => $cond,
                )
        );
        foreach ($films as $film) {
            $st = $film->movie_streams;
            $video_name = $film->name;
            if ($st[0]->episode_title != '') {
                $video_name.= ' ' . $st[0]->episode_title;
            }
        }
        return $video_name;
    }

    public function getMoviePaymentType() {
        return true;
    }

    public function checkAdvancePurchase($content_id = Null, $studio_id = Null, $is_advance = 1) {
        $return = 0;
        if (isset($content_id)) {
            if (!intval($studio_id)) {
                $studio_id = self::getStudiosId();
            }
            
            $isPPV_APVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
            if ($is_advance == 0) {
                //Check ppv is enable or not in studio
                if(isset($isPPV_APVEnabled['menu']) && !empty($isPPV_APVEnabled['menu']) && ($isPPV_APVEnabled['menu'] & 2)) {
                    $is_ppv = 1;
                } else {
                    $is_ppv = 0;
                    return $return;
                }
            } else {
                if(isset($isPPV_APVEnabled['menu']) && !empty($isPPV_APVEnabled['menu']) && ($isPPV_APVEnabled['menu'] & 16)) {
                    $is_advance = 1;
                    $is_ppv = 0;
                } else {
                    return $return;
                }
            }
            
            $plan = PpvPlans::model()->with('ppvadvancecontent')->find(array("condition" => "studio_id = " . $studio_id . " AND status=1 AND is_advance_purchase=1 AND content_types_id=1 AND content_id=".$content_id));
            if ($is_advance == 1 && $is_ppv == 0) {
                $start_date = date('Y-m-d', strtotime($plan->start_date));
                $expiry_date = date('Y-m-d', strtotime($plan->expiry_date));
                
                $today_date = gmdate('Y-m-d');
                if ((strtotime($today_date) >= strtotime($start_date)) && (strtotime($today_date) <= strtotime($expiry_date))) {
                    $return = $plan;
                    return $return;
                }
            } else if ($is_advance == 0 && $is_ppv == 1) {
                $return = $plan;
                return $return;
            }
        }
        
        return $return;
    }
    
    public function getAllTimeFramePrices($ppv_plan_id = Null, $default_currency_id = Null, $studio_id = NULL, $country = Null) {
        $price = array();        
        if(!isset($default_currency_id)){
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;   
        }
        
        if (isset($ppv_plan_id) && intval($ppv_plan_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;
            
            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION['country']) && trim($_SESSION['country'])) ? $_SESSION['country'] : '');
            
            if (isset($country) && trim($country)) {
                $price = Yii::app()->db->createCommand()
					->select('pt.*, ptl.title, cu.code AS currency_code, cu.title AS currency_title, cu.symbol AS currency_symbol')
					->from("ppv_timeframe pt")
					->leftJoin('(ppv_timeframe_lable ptl, currency AS cu)' , 'pt.currency_id=cu.id AND pt.ppv_timeframelable_id=ptl.id AND ptl.studio_id='.$studio_id)
					->where('cu.country_code=:country_code AND pt.status=1 AND pt.ppv_plan_id=:ppv_plan_id',array(':country_code'=>$country, ':ppv_plan_id'=>$ppv_plan_id))
					->queryAll();
                
                if (empty($price)) {
                    $is_default_currency = 1;
                }
            } else {
                $is_default_currency = 1;
            }
            
            if (intval($is_default_currency)) {
                $price = Yii::app()->db->createCommand()
					->select('pt.*, ptl.title, cu.code AS currency_code, cu.title AS currency_title, cu.symbol AS currency_symbol')
					->from("ppv_timeframe pt")
					->leftJoin('(ppv_timeframe_lable ptl, currency AS cu)' , 'pt.currency_id=cu.id AND pt.ppv_timeframelable_id=ptl.id AND ptl.studio_id='.$studio_id)
					->where('cu.id=:id AND pt.status=1 AND pt.ppv_plan_id=:ppv_plan_id',array(':id'=>$default_currency_id, ':ppv_plan_id'=>$ppv_plan_id))
					->queryAll();
            }
        }
        
        return $price;
    }
    
	public function getAllSubscriptionBundlesPrices($subscriptionbundles_plan_id = Null, $default_currency_id = Null, $studio_id = NULL, $country = Null) {
        $price = array();        
        if(!isset($default_currency_id)){
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;   
        }
         if (!intval($studio_id)) {
                $studio_id = self::getStudiosId();
            }
        if (isset($subscriptionbundles_plan_id) && intval($subscriptionbundles_plan_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;
           $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION[$studio_id]['country'] : '');
            if (isset($country) && trim($country)) {
                
                 $price = Yii::app()->db->createCommand()
                       ->select(' sp.* ')
                       ->from("subscription_pricing  sp")
                       ->leftJoin('currency cu' , 'sp.currency_id=cu.id')
                       ->where('cu.country_code=:country_code AND sp.subscription_plan_id=:subscription_plan_id AND sp.status=1',array(':country_code'=>$country,':subscription_plan_id'=>$subscriptionbundles_plan_id))
                       ->queryAll();
                if (empty($price)) {
                    $is_default_currency = 1;
                }
            } else {
                $is_default_currency = 1;
            }
            if (intval($is_default_currency)) {
                  $price = Yii::app()->db->createCommand()
                       ->select(' sp.* ')
                       ->from("subscription_pricing  sp")
                       ->leftJoin('currency cu' , 'sp.currency_id=cu.id')
                       ->where('sp.subscription_plan_id=:subscription_plan_id AND sp.status=1',array(':subscription_plan_id'=>$subscriptionbundles_plan_id))
                       ->queryAll();
            }
        }
        
        return $price;
    }

	public function getSubscriptionBundlesPrice($subscriptionbundles_plan_id = Null, $default_currency_id = Null, $studio_id = NULL) {
      $price = array();        
        if(!isset($default_currency_id)){
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;   
        }
       if (!intval($studio_id)) {
                $studio_id = self::getStudiosId();
            }
        if (isset($subscriptionbundles_plan_id) && intval($subscriptionbundles_plan_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;
            
           $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION['country'] : '');
            
            if (isset($country) && trim($country)) {
                
             $price = Yii::app()->db->createCommand()
                       ->select(' sp.* ')
                       ->from("subscription_pricing  sp")
                       ->leftJoin('currency cu' , 'sp.currency_id=cu.id')
                       ->where('cu.country_code=:country_code AND sp.subscription_plan_id=:subscription_plan_id AND sp.status=1',array(':country_code'=>$country,':subscription_plan_id'=>$subscriptionbundles_plan_id))
                       ->queryRow();
                if (empty($price)) {
                    $is_default_currency = 1;
                }
            } else {
                $is_default_currency = 1;
            }
            if (intval($is_default_currency)) {
                  $price = Yii::app()->db->createCommand()
                       ->select(' sp.* ')
                       ->from("subscription_pricing  sp")
                       ->leftJoin('currency cu' , 'sp.currency_id=cu.id')
                       ->where('sp.subscription_plan_id=:subscription_plan_id AND sp.status=1',array(':subscription_plan_id'=>$subscriptionbundles_plan_id))
                       ->queryRow();
            }
        }
       return $price;
    }
	
    public function getTimeFramePrice($plan_id = NULL, $timeframe_id = NULL) {
        
        $price = Ppvtimeframes::model()->findByAttributes(array('id' => $timeframe_id, 'ppv_plan_id' => $plan_id));
        
        return $price;
    }
    
    public function getAllPPVBundle($content_id = NULL, $content_types_id = NULL, $cat_ppv_plan_id = NULL, $isAllIds = NULL, $studio_id = Null) {
        //Explicitily pass from Rest controller otherwise default
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        
        $ppv_plan = array();
        
        //Check ppv is enable or not in studio
        $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 64)) {
            //Check the video is in all content type
            $ppv_plan = PpvPlans::model()->with('ppvadvancecontent')->findAll(array("condition" => "studio_id = " . $studio_id . " AND status=1 AND is_advance_purchase=2 AND content_id=".$content_id));
            /*if(!empty($ppv_plan)) {
                $count = count($ppv_plan);
                
                if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 2)) {
                    $isAllCheck = 1;
                    
                    //Find if content is in category
                    if (intval($cat_ppv_plan_id)) {
                        $cat_ppv_plan = PpvPlans::model()->findByAttributes(array('id' => $cat_ppv_plan_id, 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));
                        if (!empty($cat_ppv_plan)) {
                            $isAllCheck = 0;
                            $ppv_plan[$count] = $cat_ppv_plan;
                        }
                    }

                    if (intval($isAllCheck)) {
                        //Check the video is in all content type
                        if (intval($content_types_id) == 2) {
                            $content_types_id = 1;
                        }
                        
                        $all_ppv_plan = PpvPlans::model()->findByAttributes(array('content_types_id' => $content_types_id, 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 1, 'is_advance_purchase' => 0));
                        if (!empty($all_ppv_plan)) {
                            $ppv_plan[$count] = $all_ppv_plan;
                            $count++;
                        }
                    }
                }
            }*/
            }
        
        return $ppv_plan;
    }
    
    
        public function getAllSubscriptionsBundle($content_id = NULL, $content_types_id = NULL, $cat_ppv_plan_id = NULL, $isAllIds = NULL, $studio_id = Null) {
        //Explicitily pass from Rest controller otherwise default
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        
        //Check ppv is enable or not in studio
        $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 1)) {
            //Check the video is in all content type
            $subscriptionbundles_plan = SubscriptionPlans::model()->with('subscriptionbundlescontent')->findAll(array("condition" => "studio_id = " . $studio_id . " AND status=1 AND content_id=".$content_id));
        }
        
        return $subscriptionbundles_plan;
    }

    
    
    public function getPPVBundle($content_id = NULL, $studio_id = Null) {
        //Explicitily pass from Rest controller otherwise default
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        
        //Check ppv is enable or not in studio
        $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 64)) {
            //Check the video is in all content type
            $ppv_plan = PpvPlans::model()->with('ppvadvancecontent')->find(array("condition" => "studio_id = " . $studio_id . " AND status=1 AND is_advance_purchase=2 AND content_id=".$content_id));
            if(!empty($ppv_plan)) {
                return $ppv_plan;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    public function getSubscriptionBundle($content_id = NULL, $studio_id = Null) {
        //Explicitily pass from Rest controller otherwise default
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        
        //Check ppv is enable or not in studio
        $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 1)) {            
            //Check the video is in all content type
            $subscriptionbundles_plan = SubscriptionPlans::model()->IsSubscriptionBundlesExists($studio_id ,$content_id);          
            if(!empty($subscriptionbundles_plan)) {
                return $subscriptionbundles_plan;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    public function getSubscriptionBundleactiveplan($studio_id = Null){
         $subscriptionBundlePlanactive = Yii::app()->db->createCommand()
				->select('count(id) as subscriptionsActivePlan')
				->from("subscription_plans")
				->where('studio_id=:studio_id AND status=1 ',array(':studio_id'=>$studio_id))
                                ->GROUP('studio_id')
				->queryRow(); 
         return $subscriptionBundlePlanactive['subscriptionsActivePlan'];
    }
    public function getPPVBundleDetail($plan_id = NULL, $studio_id = Null) {
        //Explicitily pass from Rest controller otherwise default
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        //Check the video is in all content type
        $ppv_plan = PpvPlans::model()->with('ppvadvancecontent')->find(array("condition" => "studio_id = " . $studio_id . " AND status=1 AND t.id=".$plan_id));
        if(!empty($ppv_plan)) {
            return $ppv_plan;
        } else {
            return 0;
        }
    }
    
    public function IsBundledPpvPaid($content_id, $studio_id, $user_id) {
        //Find out the content is in Bundled PPV
        $bundled_content = Yii::app()->db->createCommand()
			->select('GROUP_CONCAT(ppv_plan_id) AS plan_ids')
			->from("ppv_advance_content")
			->where('content_id =:content_id',array(':content_id' => $content_id))
			->queryRow();

        $return = false;
        
        $now = Date('Y-m-d H:i:s');
        
        if (isset($bundled_content['plan_ids']) && trim($bundled_content['plan_ids'])) {
            $is_ppv_bundle_subscribed = Yii::app()->db->createCommand()
				->select('id')
				->from("ppv_subscriptions")
				->where('ppv_plan_id IN (:ppv_plan_id) AND user_id =:user_id AND studio_id=:studio_id AND status=1  AND end_date > :end_date',array(':ppv_plan_id' => $bundled_content['plan_ids'],':user_id'=>$user_id,':studio_id'=>$studio_id,':end_date'=>$now))
				->queryRow();
			
            if (!empty($is_ppv_bundle_subscribed)) {
                $return = true;
            }
        }
        
        return $return;
    }
    public function IsBundledsubscriptionPaid($content_id, $studio_id, $user_id) {
        //Find out the content is in subscription Bundled 
         $bundled_content = Yii::app()->db->createCommand()
                       ->select('GROUP_CONCAT(subscriptionbundles_plan_id) AS plan_ids')
                       ->from("subscriptionbundles_content")
                       ->where('content_id =:content_id',array(':content_id' => $content_id))
                       ->queryRow();
        $return = false;
        $now = Date('Y-m-d H:i:s');
        if (isset($bundled_content['plan_ids']) && trim($bundled_content['plan_ids'])) {
    
            $pid = explode(',', $bundled_content['plan_ids']);
                        $command = Yii::app()->db->createCommand()
                       ->select('id')
                       ->from("user_subscriptions")
                       ->where('user_id =:user_id AND studio_id=:studio_id AND status=1 AND is_subscription_bundle=1',array(':user_id'=>$user_id,':studio_id'=>$studio_id))
                       ->andWhere(array('IN','plan_id',$pid))
                       ->andWhere('end_date > :end_date', array(':end_date' => $now));
               $is_subscriptions_bundle_subscribed = $command->queryRow();
               if (!empty($is_subscriptions_bundle_subscribed)) {
                $return = true;
            }
        }
        return $return;
    }
    
    public function getContentPaymentType($content_types_id = Null, $ppv_plan_id = Null, $studio_id = Null) {
        //Explicitily pass from Rest controller otherwise default
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        
        //Check ppv is enable or not in studio
        $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 2)) {
        
            $pl = new PpvPlans();
            if (isset($ppv_plan_id) && intval($ppv_plan_id)) {//Check film is under category or not
                $ppv_plan = $pl->findByAttributes(array('id' => $ppv_plan_id, 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));

                if(!empty($ppv_plan)) {
                    return $ppv_plan;
                }
            }
            
            if (intval($content_types_id) == 2 || intval($content_types_id) == 4) {
                $content_types_id = 1;
            }
            //Check the video is in all content type
            $ppv_plan = $pl->findByAttributes(array('content_types_id' => $content_types_id, 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 1, 'is_advance_purchase' => 0));

            if(!empty($ppv_plan)) {
                return $ppv_plan;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    public function getPPVPrices($ppv_plan_id = Null, $default_currency_id = Null, $country = Null) {
        $price = array();
        $controller = Yii::app()->controller;
        if(!isset($default_currency_id)){
            $default_currency_id = $controller->studio->default_currency_id;   
        }
        $studio_id = $controller->studio->id;
        if (isset($ppv_plan_id) && intval($ppv_plan_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;
            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION['country']) && trim($_SESSION['country'])) ? $_SESSION['country'] : '');
            $country = (isset($country) && trim($country)) ? $country : (isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION[$studio_id]['country'] : '';
            if (isset($country) && trim($country)) {
               $price = Yii::app()->db->createCommand()
                ->select('pr.*,cu.id as currency_id,cu.code as currency_code,cu.symbol as currency_symbol')
                ->from('ppv_pricing pr')
				->leftjoin('currency AS cu','pr.currency_id=cu.id')
				->where('cu.country_code=:country_code AND pr.ppv_plan_id=:ppv_plan_id AND pr.status=1',array(':country_code'=>$country,':ppv_plan_id'=>$ppv_plan_id))
				->queryAll();
                
                if (empty($price)) {
                    $is_default_currency = 1;
                } else {
                    $price = $price[0];
                }
            } else {
                $is_default_currency = 1;
            }
            
            if (intval($is_default_currency)) {
				$price = Yii::app()->db->createCommand()
				->select('pr.*,cu.id as currency_id,cu.code as currency_code,cu.symbol as currency_symbol')
				->from('ppv_pricing pr')
				->leftjoin('currency AS cu','pr.currency_id=cu.id')
				->where('cu.id=:default_currency_id AND pr.ppv_plan_id=:ppv_plan_id AND pr.status=1',array(':default_currency_id'=>$default_currency_id,':ppv_plan_id'=>$ppv_plan_id))
				->queryAll();
                if (!empty($price)) {
                    $price = $price[0];
                }
            }
        }
        
        return $price;
    }
    
    public function isPaymentGatwayAndPPVPlanExists($studio_id = 0, $video_id = 0) {
        $res = array();
        
        if (isset($studio_id) && !empty($studio_id)) {
            $gateways = new StudioPaymentGateways;
            $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
            $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                array(
                    'condition' => $cond,
                    'order' => 't.id DESC'
                )
            );
            
            $plans = new PpvPlans;
            if($video_id==0){
                $cond = 'studio_id=' . $studio_id . ' AND status = 1 AND is_advance_purchase = 0';
            }  else {
                $cond = 'studio_id=' . $studio_id . ' AND status = 1 AND is_advance_purchase = 0 AND movie_ids IN ('.$video_id.')';
            }
            $plans = $plans::model()->with()->findAll(
                    array(
                        'condition' => $cond,
                        'order' => 'id DESC'
                    )
            );

            if (isset($plans) && !empty($plans) && isset($gateways[0]) && !empty($gateways[0])) {
                $res['plans'] = $plans;
                $res['gateways'] = $gateways;
            }
        }

        return $res;
    }
    public function voucherAvailable($studio_id = 0){
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
    
        if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 128)) {
            $voucher = new CouponOnly;
            $voucher_avail = $voucher->checkVoucher($studio_id);
            if($voucher_avail > 0 && intval($voucher_avail)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
     public function isSubscriptionBundlesExists($studio_id = 0){
        if (!intval($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
        if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 256)) {
            $subscriptionbundles = new SubscriptionBundlesPlan;
            $subscriptionsbundles_avail = $subscriptionbundles->checkSubscriptionBundles($studio_id);
            if($subscriptionsbundles_avail > 0 && intval($subscriptionsbundles_avail)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    
    
    public function isVoucherExists($studio_id = 0, $content) {
		$res = 0;
		if (isset($studio_id) && !empty($studio_id)) {
			$data = Yii::app()->general->monetizationMenuSetting($studio_id);

			if (isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 128)) {
				if ($content != "") {
					$res = self::getContentInVoucher($studio_id, $content);
				}
			}
		}
		return $res;
	}

	function getContentInVoucher($studio_id, $content) {
		$is_multi = substr_count($content, ":");
		$str = explode(":", trim($content, "'"));

		$search_content = '';

		if ($is_multi == 2) {
			$parent_season = "'" . $str[0] . ":" . $str[1] . "'";
			$search_content = "FIND_IN_SET({$str[0]},content) OR FIND_IN_SET({$parent_season},content) OR FIND_IN_SET({$content},content)";
		} elseif ($is_multi == 1) {
			$search_content = "FIND_IN_SET({$str[0]},content) OR FIND_IN_SET({$content},content)";
		} else {
			$search_content = "FIND_IN_SET({$content},content)";
		}

		$cond = array(':studio_id' => $studio_id);

		$vdata = Yii::app()->db->createCommand()
				->select('*')
				->from('voucher')
				->where('studio_id=:studio_id AND status=1 AND (is_allcontent=1 OR (' . $search_content . '))', $cond)
				->queryRow();

		$res = 0;
		if (!empty($vdata)) {
			$res = 1;
		}
		
		return $res;
	}

	public function isPaymentGatwayExists($studio_id = 0) {
        $res = array();
        if (isset($studio_id) && !empty($studio_id)) {
            $gateways = new StudioPaymentGateways;
            $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
            $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                    array(
                        'condition' => $cond,
                        'order' => 't.is_primary DESC, t.id DESC'
                    )
            );

            if (isset($gateways[0]) && !empty($gateways[0])) {
                $res['gateways'] = $gateways;
            }
        }
        return $res;
    }

    public function isFreeContent($arg = array()) {
        $return = 0;
        if (isset($arg) && !empty($arg)) {
            $studio_id = (isset($arg['studio_id'])) ? $arg['studio_id'] : self::getStudiosId();
			if($studio_id ==self::getStudiosId() && $this->studioData){
				$need_login = $this->studioData->need_login;
			}else{
				$need_login = Studio::model()->findByPk($studio_id)->need_login;
			}
            if (intval($need_login) == 0) {
                $return = 1;
                return $return;
            }

            $movie_id = (isset($arg['movie_id']) && intval($arg['movie_id'])) ? $arg['movie_id'] : 0;
            $season_id = (isset($arg['season_id']) && intval($arg['season_id'])) ? $arg['season_id'] : 0;
            $episode_id = (isset($arg['episode_id']) && intval($arg['episode_id'])) ? $arg['episode_id'] : 0;
            $content_types_id = (isset($arg['content_types_id']) && intval($arg['content_types_id'])) ? $arg['content_types_id'] : 0;

            $connection = Yii::app()->db;

            //Check in all content types (Single-part or Multi-part)
			if(isset($_SESSION[$studio_id]['StudioFreeContents'])){
				$content_types = $_SESSION[$studio_id]['StudioFreeContents'];
			}else{
				$content_types = 
				$connection->createCommand()
				->SELECT("*")
				->FROM('free_contents')
				->WHERE("studio_id=:studio_id AND movie_id=0 AND season_id=0 AND episode_id=0 AND content_types_id=:content_types_id",array(":studio_id"=>$studio_id, ":content_types_id"=>$content_types_id))
				->queryAll();
				$_SESSION[$studio_id]['StudioFreeContents'] = $content_types;
			}
            if (!empty($content_types)) {
                $return = 1;
            } else {
                //Check in Show of Multi-part or Single part
                $free_contents =
				$connection->createCommand()
				->SELECT("*")
				->FROM('free_contents')
				->WHERE("studio_id=:studio_id AND movie_id=:movie_id AND season_id=0 AND episode_id=0 AND content_types_id=:content_types_id",array(":movie_id"=>$movie_id, ":studio_id"=>$studio_id, ":content_types_id"=>$content_types_id))
				->queryAll();

                if (!empty($free_contents)) {
                    $return = 1;
                } else {
                    if ($content_types_id == 3) {
                        //Check in Season of Multi-part
                        //$sql = "SELECT * FROM free_contents WHERE studio_id=" . $studio_id . " AND movie_id=" . $movie_id . " AND season_id=" . $season_id . " AND episode_id=0 AND content_types_id=" . $content_types_id;
                        $free_season_contents =
								$connection->createCommand()
								->SELECT("*")
								->FROM('free_contents')
								->WHERE("studio_id=:studio_id AND movie_id=:movie_id AND season_id=:season_id AND episode_id=0 AND content_types_id=:content_types_id",array(":movie_id"=>$movie_id,":studio_id"=>$studio_id,":season_id"=>$season_id,":content_types_id"=>$content_types_id))
								->queryAll();
						//$free_season_contents = $connection->createCommand($sql)->queryAll();
                        if (!empty($free_season_contents)) {
                            $return = 1;
                        } else {
                            //Check in Episode of Multi-part
                            //$sql = "SELECT * FROM free_contents WHERE studio_id=" . $studio_id . " AND movie_id=" . $movie_id . " AND season_id=" . $season_id . " AND episode_id=" . $episode_id . " AND content_types_id=" . $content_types_id;
                            //$free_episode_contents = $connection->createCommand($sql)->queryAll();
							 $free_episode_contents =
								$connection->createCommand()
								->SELECT("*")
								->FROM('free_contents')
								->WHERE("studio_id=:studio_id AND movie_id=:movie_id AND season_id=:season_id AND episode_id=:episode_id AND content_types_id=:content_types_id",array(":movie_id"=>$movie_id,":studio_id"=>$studio_id,":season_id"=>$season_id,":content_types_id"=>$content_types_id,":episode_id"=>$episode_id))
								->queryAll();
                            if (!empty($free_episode_contents)) {
                                $return = 1;
                            }
                        }
                    }
                }
            }
        }

        return $return;
    }

    //Added for PPV
    public function checkIfPaid($movie_id) {
        $user_id = isset(Yii::app()->session['logged_user_id']) ? Yii::app()->session['logged_user_id'] : 0;
        $agent = self::getAgent();
        if ($user_id > 0 && $agent != '') {
            $trans = Transaction::model()->find('movie_id=:movie_id AND user_id=:user_id AND DATE(expiry_date)>=:expiry_date AND LOWER(transaction_status)=:tstatus', array(':tstatus' => 'completed', ':expiry_date' => gmdate('Y-m-d'), ':user_id' => $user_id, ':movie_id' => $movie_id));
            //$sql = "SELECT id FROM transactions WHERE movie_id = '".$movie_id."' AND user_id = '".$user_id."' AND expiry_date > NOW() AND transaction_status = 'Completed' LIMIT 1";
            //$command = $connection->createCommand($sql);
            //$trans = $command->queryAll(); // execute a query SQL    

            if ($trans) {
                // foreach ($trans as $tran){
                return $trans->id;
                //}
            } else {
                return 0;
            }
        } else
            return 0;
    }

    //Added for PPV
    public function checkIfPlan() {
        $user_id = Yii::app()->user->id;
        $studio_id = self::getStudioId();
        if ($user_id > 0 && $studio_id > 0) {
            $plans = Yii::app()->db->createCommand()
				->select('*')
				->from("subscription_plans")
				->where('studio_id=:studio_id AND status=1',array(':studio_id'=>$studio_id))
				->queryAll();

            return count($plans);
        } else {
            return 0;
        }
    }

    public function getPlanDetails($plan_id) {
        $plan = Yii::app()->db->createCommand()
			->select('*')
			->from("subscription_plans")
			->where('id=:id',array(':id'=>$plan_id))
			->queryRow();

        if (count($plan) > 0) {
                return $plan;
        } else
            return '';
    }

    public function getPPVPlanDetails($plan_id) {
        $plan = Yii::app()->db->createCommand()
			->select('*')
			->from("ppv_plans")
			->where('id=:id',array(':id'=>$plan_id))
			->queryRow();
		
        if (count($plan) > 0) {
            return $plan;
        } else {
            return '';
		}
    }
	
    public function getPPVPlantimeframeDetails($ppvsubscription_id)
    {
    $connection = Yii::app()->db;

        $sql = "SELECT pl.title FROM ppv_subscriptions as p LEFT JOIN ppv_timeframe pt on p.timeframe_id=pt.id LEFT JOIN ppv_timeframe_lable as pl on pl.id=pt. ppv_timeframelable_id WHERE p.id = '" . $ppvsubscription_id . "'";
        $command = $connection->createCommand($sql);
        $plan = $command->queryRow();
         return $plan;
    }
    
    public function userHome() {
        if (@LOGIN_REDIRECT) {
            return Yii::app()->getBaseUrl(true) . '/user/profile';
        } else {
            return Yii::app()->getBaseUrl(true) . '/tv-show';
        }
    }

    public function callCurl($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function getIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //check if ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //to check if ip is passed from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function storeDetails($store = '') {
        if ($store == 'demo') {
            $store_details = array(
                'name' => 'Demo',
                'adminEmail' => 'support@Demo.com',
                'supportemail' => 'support@demo.com',
                'fb_link' => 'https://www.facebook.com/maatv',
                'twitter_link' => 'https://twitter.com/maatv',
                'gplus_link' => 'https://plus.google.com/+maatv',
                'siteName' => 'Demo.com',
                'subscription_success_message' => 'Your subscription to Demo is now activated. Enjoy watching Demo Shows and Movies.',
                'adminGroup' => array(
                    array(
                        'email' => 'girija@muvi.com',
                        'name' => 'Rati Kanta',
                        'type' => 'to'
                    )
                ),
                'agent_address' => 'Demo TELEVISION NETWORK LTD<br/>Plot #AA9,<br/>Bhubaneswar,<br/>Odisha',
                    //'adminGroup'=>array(array('email' => 'gayadhar@muvi.com','name' =>'Gayadhar Khilar' ,'type' => 'to'),array('email' => 'ratikanta@muvi.com','name' => 'Ratikanta','type' => 'to'),array('email' => 'rashmi@muvi.com','name' => 'Rashmi','type' => 'to'),array('email' => 'mohan@muvi.com','name' => "Mohan Kumar",'type' => 'cc'))
            );
        } else if ($store == 'iskcon') {
            $store_details = array(
                'name' => 'Iskcon',
                'adminEmail' => 'support@iskcon.com',
                'supportemail' => 'support@iskcon.com',
                'fb_link' => 'https://www.facebook.com/iskcon',
                'twitter_link' => 'https://twitter.com/iskcon',
                'gplus_link' => 'https://plus.google.com/+iskcon',
                'siteName' => 'iskcon.com',
                'subscription_success_message' => 'Your subscription to Iskcon is now activated. Enjoy watching Demo Shows and Movies.',
                'adminGroup' => array(
                    array(
                        'email' => 'ratikanta@muvi.com',
                        'name' => 'Rati Kanta',
                        'type' => 'to'
                    )
                ),
                'agent_address' => 'Iskcon TELEVISION NETWORK LTD<br/>Plot #AA9,<br/>Bhubaneswar,<br/>Odisha',
                    //'adminGroup'=>array(array('email' => 'gayadhar@muvi.com','name' =>'Gayadhar Khilar' ,'type' => 'to'),array('email' => 'ratikanta@muvi.com','name' => 'Ratikanta','type' => 'to'),array('email' => 'rashmi@muvi.com','name' => 'Rashmi','type' => 'to'),array('email' => 'mohan@muvi.com','name' => "Mohan Kumar",'type' => 'cc'))
            );
        } else if ($store == 'studio') {
            $store_details = array(
                'name' => 'MuviStudio',
                'adminEmail' => 'info@muvi.com',
                'supportemail' => 'info@muvi.com',
                'fb_link' => 'https://www.facebook.com/MuviStudioB2B',
                'twitter_link' => 'http://www.twitter.com/muvistudiob2b',
                'gplus_link' => 'https://plus.google.com/118356511554070797931/posts',
                'siteName' => 'studio.muvi.com'
            );
        } else {
            $store_details = array(
                'name' => SITE_NAME,
                'adminEmail' => 'subscriber@maflix.com',
                'siteName' => SITE_NAME,
            );
        }
        return $store_details;
    }

    public function getCountry($country_code) {
        $qry = "SELECT * FROM countries WHERE code = '" . $country_code . "' ORDER BY id LIMIT 1";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($qry);
        $countries = $command->queryAll(); // execute a query SQL   
        $country_name = '';
        if (count($countries) > 0) {
            foreach ($countries as $country) {
                $country_name = $country['country'];
            }
        }
        return $country_name;
    }

    public function getState($state_code, $country_code) {
        $qry = "SELECT * FROM states WHERE code = '" . $state_code . "' AND country_id = '" . $country_code . "' ORDER BY id LIMIT 1";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($qry);
        $states = $command->queryAll(); // execute a query SQL   
        $state_name = $state_code;
        if (count($states) > 0) {
            foreach ($states as $state) {
                $state_name = $state['state'];
            }
        }
        return $state_name;
    }

    public function checkLogin() {
        return Yii::app()->user->id;
    }

    public function getMovieDetails($movie_id) {
        if ($_SERVER['HTTP_HOST'] != 'localhost') {
            $url = "http://www.muvi.com/get_movie_details?movie_id=" . $movie_id;
        } else {
            $url = "http://172.29.11.5:5050/get_movie_details?movie_id=" . $movie_id;
        }

        $movie_data = self::callCurl($url);
        //print_r($movie_data);
        return $movie_data;
    }

    public function getUserDetails($user_id, $token) {
        if ($_SERVER['HTTP_HOST'] != 'localhost') {
            $url = "http://www.muvi.com/get_user_details?user_id=" . $user_id . '&token=' . $token;
        } else {
            $url = "http://172.29.11.5:5050/get_user_details?user_id=" . $user_id . '&token=' . $token;
        }
        $user_data = self::callCurl($url);
        return $user_data;
    }

    public function getUserInfo($user_id, $token) {
        $user_data = self::getUserDetails($user_id, $token);
        $user_datas = json_decode($user_data);
        $display_name = '';
        $user_image = '';

        if (count($user_datas) > 0) {
            foreach ($user_datas as $user) {
                $user_info = $user->user_info;
                $user_profile = $user->user_profile;
                $user_address = $user->user_address;

                if ($user_profile != null) {
                    $display_name = $user_profile->display_name;
                }
                if ($user->user_image && $user->user_image != null) {
                    $user_image = $user->user_image;
                }
            }
        }
        $info = array('id' => $user_id, 'display_name' => $display_name, 'profile_image' => $user_image);
        return $info;
    }

    public function getUserToken($user_id) {
        $token = '';
        if ($user_id > 0) {
            if (isset(Yii::app()->session['token']))
                $token = Yii::app()->session['token'];
            else {
                $url = ($_SERVER['HTTP_HOST'] != 'localhost') ? 'http://www.muvi.com/' : 'http://www.muvi.com/';
                $url.= 'get_user_token?user_id=' . $user_id;
                $token_data = self::callCurl($url);

                $tokens = json_decode($token_data);
                if (count($tokens) > 0) {
                    $token = $tokens[0]->user_info->token;
                }
            }
        }
        return $token;
    }

    public function getResetPasswordToken($email) {
        $reset_token = '';
        $user_id = 0;
        if ($email != '') {
            $url = ($_SERVER['HTTP_HOST'] != 'localhost') ? 'http://www.muvi.com/' : 'http://172.29.11.5:5060/';
            $url.= 'get_user_pass_token?email=' . $email . '&oauthApp=' . $auth;
            $token_data = self::callCurl($url);
            $token_data = substr($token_data, 26);
            $len = strlen($token_data) - 2;

            $rdata = json_decode(substr($token_data, 0, ($len)));

            if (isset($rdata->items)) {
                foreach ($rdata->items as $item) {
                    if (isset($item->result)) {
                        if ($item->result == 'user does not exist') {
                            $user_id = 0;
                        } else {
                            foreach ($item->result as $res) {
                                $user_id = $res->user_id;
                                $reset_token = $res->reset_password_token;
                            }
                        }
                    }
                }
            }
        }
        $return = array('token' => $reset_token, 'user_id' => $user_id);
        return $return;
    }

    public function formatPrice($price, $currency_id = Null, $isPDF = 0) {
        $currency_symbol = '$';
        if (isset($currency_id) && intval($currency_id)) {
            $currency = Currency::model()->findByPk($currency_id);
            if (isset($currency) && !empty($currency)) {
                if ($isPDF) {
                    $currency_symbol = trim($currency->code) ? $currency->code . ' ' : $currency->code . ' ';
                } else {
                    $currency_symbol = trim($currency->symbol) ? $currency->symbol : $currency->code . ' ';
                }
            }
        }
        $config_currency = new StudioConfig();
        $cfg_currency = $config_currency->getconfigvalue('currency_placement');
        if(isset($cfg_currency['config_value']) && $cfg_currency['config_value']) {
            $formatted_price = number_format($price, 2, '.', ' ') ." ". $currency_symbol;
        }else{
            $formatted_price = $currency_symbol . number_format($price, 2, '.', ' ');
        }
        return $formatted_price;
    }

    public function phpDate($date) {
        //$date = substr($date, 0, 18);
        $date_int = strtotime($date);
        $val = date('Y-m-d G:i:s', $date_int);
        return $val;
    }

    public function MonthTextDate($date) {
        $date_int = strtotime($date);
        $val = date('F j, Y G:i:s a', $date_int);
        return $val;
    }

    public function YYMMDD($date) {
        //$date = substr($date, 0, 18);
        $date_int = strtotime($date);
        $val = date('Y-m-d', $date_int);
        return $val;
    }

    public function normalDate($date) {
        $date_int = strtotime($date);
        $val = date('F j, Y', $date_int);
        return $val;
    }

    public function handleQuotes($str) {
        return addslashes($str);
    }

    public function freeMonth($plan_id) {
        $connection = Yii::app()->db;
        $studio_id = self::getStudioId();
        $qry = "SELECT free_month FROM subscription_plans WHERE studio_id = '" . $studio_id . "'  AND status = 't' AND id = '" . $plan_id . "' ORDER BY id ASC LIMIT 1";
        $plink = $connection->createCommand($qry)->queryAll();
        if (count($plink) > 0) {
            foreach ($plink as $pl) {
                return $pl['free_month'];
            }
        } else
            return false;
    }

    public function usedTrial() {
        $user_id = Yii::app()->user->id;

        $subs = new UserSubscription;
        $subs = $subs->findAllByAttributes(array('user_id' => $user_id, 'studio_id' => $studio_id), array('order' => 'id ASC'));
        if (count($subs) > 0) {
            return 1;
        } else
            return 0;
    }

    public function getMoviePermalink($movie_id) {
        $movie_url = Yii::app()->getbaseUrl(true);
        if ($movie_id) {
            $pgconnection = Yii::app()->db2;
            $sql = "SELECT permalink FROM films WHERE id = " . $movie_id . " LIMIT 1";
            $plink = $pgconnection->createCommand($sql)->queryAll();
            if (count($plink) > 0) {
                foreach ($plink as $pl) {
                    $movie_url .= '/movie/' . $pl['permalink'];
                }
            }
        }
        return $movie_url;
    }

    public function checkValidity($plan_id, $max_view) {
        $studio = $this->studio;
        $pl = new PpvPlans();
        $plan = $pl->findByPk($plan_id);

        $qry = "SELECT US.id FROM ppv_subscriptions US WHERE";
        $qry.= " US.user_id = '" . $user_id . "'";
        $qry.= " AND US.ppv_plan_id = " . $plan_id;
        $qry.= " AND US.studio_id = '" . $studio_id . "' AND US.status = 1 AND end_date > NOW()";
        $qry.= " ORDER BY US.id LIMIT 1";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($qry);
        $subs = $command->queryAll(); // execute a query SQL   
        if (count($subs) > 0) {
            foreach ($subs as $sub) {
                return $sub['id'];
            }
        }
    }

    public function canSeeMovie($arg = array()) {
        //Initialize parameters
        $movie_id = (isset($arg['movie_id'])) ? $arg['movie_id'] : 0;
        $stream_id = (isset($arg['stream_id'])) ? $arg['stream_id'] : 0;
        $season = (isset($arg['season'])) ? $arg['season'] : 0;
        $purchase_type = (isset($arg['purchase_type'])) ? $arg['purchase_type'] : Null;
        $studio_id = (isset($arg['studio_id'])) ? $arg['studio_id'] : self::getStudiosId();
        $country = (isset($arg['country'])) ? $arg['country'] : 0;
        $is_monetizations_menu = (isset($arg['is_monetizations_menu'])) ? $arg['is_monetizations_menu'] : 0;
        $user_id = (isset($arg['user_id'])) ? $arg['user_id'] : (isset(Yii::app()->user->id) ? Yii::app()->user->id : 0);
        $title = (isset($arg['title'])) ? $arg['title'] : Null;
        $api_perimission = (isset($arg['api_perimission'])) ? $arg['api_perimission'] : 0;
        $content_type = (isset($arg['content_type'])) ? $arg['content_type'] : 0;
        $film = Film::model()->findByPk($movie_id);
        if ($stream_id == 0) {
            $stream_id = self::getStreamId($movie_id,'', '', $studio_id);
        }
        
        if (isset($user_id) && $user_id > 0) {
			$is_free = SdkUser::model()->findByPk($user_id)->is_free;
           if ($api_perimission) {
                $getApiDetails = Yii::app()->db->createCommand()
                        ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                        ->from('external_api_keys e')
                        ->join('studio_api_details s', 'e.id=s.api_type_id')
                        ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module'=>'playPerimissionChk'))
                        ->queryRow();
                if (!empty($getApiDetails)) {
                    require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
                    $chk = new clickHereApi();
                    $getApiDetails['movie_id'] = $movie_id;
                    $getApiDetails['stream_id'] = $stream_id;
                    $getApiDetails['title'] = $title;
                    $mobile_number = (isset(Yii::app()->user->mobile_number) && Yii::app()->user->id > 0) ? Yii::app()->user->mobile_number : 0;
                    $message = Yii::app()->controller->Language['permission_api_body_msg'];
                    $uniqueID = $chk->getUniqueID();
                    $is_play = $chk->doCheckPermissionToPlay($mobile_number, $message, $uniqueID, $getApiDetails);
                    
                    $response = json_decode(substr($is_play['response'],strpos($is_play['response'],'{'), strlen($is_play['response'])),true);
                    
                    if(trim($response['StatusCode']) == '403'){
                        $command = Yii::app()->db->createCommand();
                        $command->update('sdk_users', array('is_subscribe_inapi'=>0), 'id=:id AND mobile_number=:mobile_number AND studio_id=:studio_id', array(':id'=>$user_id, ':mobile_number' => $mobile_number, ':studio_id'=>$studio_id));
                        return 'noaccess';
                    }else if(trim($response['StatusCode']) == '200'){
                        $command = Yii::app()->db->createCommand();
                        $command->update('sdk_users', array('is_subscribe_inapi'=>1), 'id=:id AND mobile_number=:mobile_number AND studio_id=:studio_id', array(':id'=>$user_id, ':mobile_number' => $mobile_number, ':studio_id'=>$studio_id));
                        return 'allowed';
                    }
                    //return "allowed";
                    exit;
                }
            }                        
            //if (isset(Yii::app()->user->is_studio_admin) && Yii::app()->user->is_studio_admin && @Yii::app()->user->id == STUDIO_USER_ID || Yii::app()->user->id == 1038) {
            if ((isset(Yii::app()->user->is_studio_admin) && Yii::app()->user->is_studio_admin) || (@Yii::app()->user->id == STUDIO_USER_ID || Yii::app()->user->id == 1038 || $is_free == 1)) {
                return 'allowed';
            } else {
                $allwed_in_country = self::isAllowedInCountry($movie_id,$studio_id,$country);
                if ($allwed_in_country) {
                    $season_id = $season;
                    
                    if (intval($film->content_types_id) == 3 && $purchase_type == 'episode' && $season == 0) {
                        $season_id = movieStreams::model()->findByPk($stream_id)->series_number;
                    }
                    
                    $freearg['studio_id'] = $studio_id;
                    $freearg['movie_id'] = $movie_id;
                    $freearg['season_id'] = $season_id;
                    $freearg['episode_id'] = (isset($arg['purchase_type']) && ($arg['purchase_type'] == 'show' || $arg['purchase_type'] == 'season') && intval($film->content_types_id) == 3) ? 0 : $stream_id;
                    $freearg['content_types_id'] = $film->content_types_id;
                    
                    $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
                    $isFreeContent = 0;
                    if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 8)) {
                        $isFreeContent = self::isFreeContent($freearg);
                    }
                    
                    if (intval($isFreeContent)) {
                        return 'allowed';
                    } else {
                        if (intval($film->content_types_id) == 3){
                            $content = "'".$movie_id.":".$season_id.":".$stream_id."'";
                        }else{
                            $content = $movie_id;
                        }
                        
                        $return = 'unpaid';
                        $temp_return = 1;
                        $expiredAccessability = '';
                        
                        $monitization_data = array();
                        $monitization_data['studio_id'] = $studio_id;
                        $monitization_data['user_id'] = $user_id;
                        $monitization_data['movie_id'] = $movie_id;
                        $monitization_data['season'] = @$season_id;
                        $monitization_data['stream_id'] = @$stream_id;
                        $monitization_data['content'] = @$content;
                        $monitization_data['film'] = $film;
                        $monitization_data['purchase_type'] = @$purchase_type;
            
                        $monetization = self::getMonetizationsForContent($monitization_data);
                        if(isset($monetization['is_play']) && $monetization['is_play']){
                            return 'allowed';
                        }
                        $is_pre_order_content = 0;
                        if (isset($monetization['monetization']['pre_order']) && trim($monetization['monetization']['pre_order'])) {
                            //Is content subscribed from pre-order
                            $pre_order_plan = $monetization['monetization_plans']['pre_order'];
                            $content_types_id = $pre_order_plan->content_types_id;
                            $is_adv_subscribed = self::IsAdvPaid($movie_id, $stream_id, $season, $content_types_id, $purchase_type, $studio_id, $user_id);
                            
                            if ($is_adv_subscribed > 0) {
                                return 'advancedpurchased';
                            }
                            
                            $temp_return = 0;
                            $is_pre_order_content = 1;
                        }
                        
                         if (isset($monetization['monetization']['subscription_bundles']) && trim($monetization['monetization']['subscription_bundles'])) {
                             //Is content subscribed from subscription bundle
                            $is_subscription_bundle_subscribed = self::IsBundledsubscriptionPaid($movie_id, $studio_id, $user_id);
                            if($is_subscription_bundle_subscribed) {
                                return 'allowed';
                            }
                        
                            $temp_return = 0;
                        }
                        if (isset($monetization['monetization']['voucher']) && trim($monetization['monetization']['voucher'])) {
                            //Is content subscribed by implementing voucher
                        $voucher = self::isVoucherExists($studio_id,$content);
                        if($voucher != 0){
                                $content_types_id = $film->content_types_id;
                                $voucher_subscription_data = self::IsVoucherSubscribed($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);
                        
                                if (!empty($voucher_subscription_data)) {
                                    if ($is_pre_order_content) {
                                        return 'advancedpurchased';
                                    } else {
                                        $isPlaybackAccess = self:: isPlaybackAccessOnContent($voucher_subscription_data, $content_types_id, $stream_id, $purchase_type);

                                        if ($isPlaybackAccess['isContinue']) {
                                            return 'allowed';
                                        } else {
                                            if (intval($is_monetizations_menu)) {
                                                $return = 'unpaid';
                                                $expiredAccessability = $isPlaybackAccess['expiredAccessability'];
                                            } else {
                                                return $isPlaybackAccess['expiredAccessability'];
                                            }
                                        }
                                    }
                                }
                            }
                            
                            $temp_return = 0;
                        }

                        if (isset($monetization['monetization']['ppv_bundle']) && trim($monetization['monetization']['ppv_bundle'])) {
                            //Is content subscribed from ppv bundle
                            $is_ppv_bundle_subscribed = self::IsBundledPpvPaid($movie_id, $studio_id, $user_id);
                                    
                            if($is_ppv_bundle_subscribed) {
                                return 'allowed';
                                        }
                            
                            $temp_return = 0;
                                    }
                        
                        if (isset($monetization['monetization']['ppv']) && trim($monetization['monetization']['ppv'])) {
                            //Is content subscribed from ppv
                            $ppv_plan = $monetization['monetization_plans']['ppv'];
                            
                            $content_types_id = $ppv_plan->content_types_id;
                            $ppv_subscription_data = self::IsPpvPaid($movie_id, $stream_id, $season, $content_types_id, $purchase_type, $studio_id, $user_id);
                            
                            if (!empty($ppv_subscription_data)) {
                                $isPlaybackAccess = self:: isPlaybackAccessOnContent($ppv_subscription_data, $content_types_id, $stream_id, $purchase_type);
                                
                                if ($isPlaybackAccess['isContinue']) {
                                    return 'allowed';
                                } else {
                                    if (intval($is_monetizations_menu)) {
                                        $return = 'unpaid';
                                        $expiredAccessability = $isPlaybackAccess['expiredAccessability'];
                                    } else {
                                        return $isPlaybackAccess['expiredAccessability'];
                                }
                            }
                            }
                                
                            $temp_return = 0;
                        }
                        
                        if (intval($temp_return)) {
                            $return = "";
                        }
                        
                        if ($return == 'unpaid') {
                            if (intval($is_monetizations_menu)) {
                                $res = $monetization['monetization'];
                                if (trim($expiredAccessability)) {
                                    $res['playble_error_msg'] = $expiredAccessability;
                                }
                                return $res;
                            } else {
                                return $return;
                            }
                        } else {
                            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 1)) {
                                return self::isSubscribedContent($studio_id, $user_id, $stream_id);
                            } else {
                                return 'allowed';
                            }
                        }
                    }
                } else {
                    return 'limitedcountry';
                }
            }
        } else {
            $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
			if (isset($std_config) && !empty($std_config)) {
				$free_content_login = $std_config->config_value;
			} else {
				$free_content_login = 1;
			}
			$freearg['studio_id'] = $studio_id;
			$freearg['movie_id'] = $movie_id;
			$freearg['season_id'] = $season_id;
			$freearg['episode_id'] = (isset($arg['purchase_type']) && ($arg['purchase_type'] == 'show' || $arg['purchase_type'] == 'season') && intval($film->content_types_id) == 3) ? 0 : $stream_id;
			$freearg['content_types_id'] = $film->content_types_id;

			$isFreeContent = self::isFreeContent($freearg);
			if (intval($user_id) > 0) {
				if (intval($isFreeContent) == 1) {
					return 'allowed';
				} else {
					return 'noaccess';
				}
			} else {
				if ($free_content_login == 0 && intval($isFreeContent) == 1) {
					return 'allowed';
				} else {
					return 'noaccess';
				}
			}
		}
	}
    
    public function isPlaybackAccessOnContent($ppv_subscription_data, $content_types_id = 0, $stream_id = 0, $purchase_type = Null) {
        $return = true;
        $expiredAccessability = "";
        
        $result = array();
        
        $limit_video = (isset($ppv_subscription_data['view_restriction']) && intval($ppv_subscription_data['view_restriction'])) ? $ppv_subscription_data['view_restriction'] : 0;
        $watch_period = (isset($ppv_subscription_data['watch_period']) && trim($ppv_subscription_data['watch_period'])) ? $ppv_subscription_data['watch_period'] : '';
        $access_period = (isset($ppv_subscription_data['access_period']) && trim($ppv_subscription_data['access_period'])) ? $ppv_subscription_data['access_period'] : '';
        
        if (trim($access_period) && trim($watch_period) && intval($limit_video)) {//111
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
                $expiredAccessability = 'access_period';
                
                if ($return) {
                    $return = 0;
                    $expiredAccessability = 'already_purchased';
                }
            } else {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $stream_id);
                    $expiredAccessability = 'watch_period';
                    if($return) {
                        $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $stream_id);
                        $expiredAccessability = 'maximum';
                    }
                }
            }
        } else if (trim($access_period) && trim($watch_period) && intval($limit_video) == 0) {//110
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
                $expiredAccessability = 'access_period';
                
                if ($return) {
                    $return = 0;
                    $expiredAccessability = 'already_purchased';
                }
            } else {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $stream_id);
                    $expiredAccessability = 'watch_period';
                }
            }
        } else if (trim($access_period) && trim($watch_period) == '' && intval($limit_video)) {//101
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
                $expiredAccessability = 'access_period';
                
                if ($return) {
                    $return = 0;
                    $expiredAccessability = 'already_purchased';
                }
            } else {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $stream_id);
                    $expiredAccessability = 'maximum';
                }
            }
        } else if (trim($access_period) && trim($watch_period) == '' && intval($limit_video) == 0) {//100
            $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data);
            $expiredAccessability = 'access_period';
        } else if (trim($access_period) == '' && trim($watch_period) && intval($limit_video)) {//011
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = 0;
                $expiredAccessability = 'already_purchased';
            } else {
                $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $stream_id);
                $expiredAccessability = 'watch_period';
                if ($return) {
                    $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $stream_id);
                    $expiredAccessability = 'maximum';
                }
            }
        } else if (trim($access_period) == '' && trim($watch_period) && intval($limit_video) == 0) {//010
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = 0;
                $expiredAccessability = 'already_purchased';
            } else {
                $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $stream_id);
                $expiredAccessability = 'watch_period';
            }
        } else if (trim($access_period) == '' && trim($watch_period) == '' && intval($limit_video)) {//001
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = 0;
                $expiredAccessability = 'already_purchased';
            } else {
                $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $stream_id);
                $expiredAccessability = 'maximum';
            }
        }
        
        $result['isContinue'] = $return;
        $result['expiredAccessability'] = $expiredAccessability;
        
        return $result;
    }
    
    public function isAccessPeriodOnContentExceeds($ppv_subscription_data) {
        $purchased_date = strtotime($ppv_subscription_data['created_date']);
        $access_period = @$ppv_subscription_data['access_period'];
        $content_access_period = strtotime("+{$access_period}", $purchased_date);
        
        if ($content_access_period <= strtotime(date('Y-m-d H:i:s'))) {
            return false;
        } else {
            return true;
        }
    }
    
    public function isWatchPeriodOnContentExceeds($ppv_subscription_data, $stream_id = 0) {
        $return = true;
        
        $watch_period = @$ppv_subscription_data['watch_period'];
        if (trim($watch_period)) {
            $data = $ppv_subscription_data;
            $data['stream_id'] = $stream_id;
            
            $first_watched_date = VideoLogs::model()->getFirstWatchedDateAndTimeOfContent($data);
            if (isset($first_watched_date) && trim($first_watched_date) && date('Y-m-d', $first_watched_date) != '0000-00-00' && date('Y-m-d', $first_watched_date) != '1970-01-01') {
                $watch_period = @$ppv_subscription_data['watch_period'];
                $watch_period_expiry = strtotime("+{$watch_period}", $first_watched_date);
                
                if ($watch_period_expiry <= strtotime(date('Y-m-d H:i:s'))) {
                    $return = false;
                }
            }
        }
        
        return $return;
    }
    
    public function isNumberOfViewsOnContentExceeds($ppv_subscription_data, $stream_id) {
        $studio_id = @$ppv_subscription_data['studio_id'];
        $user_id = @$ppv_subscription_data['user_id'];
        $movie_id = @$ppv_subscription_data['movie_id'];
        
        $purchased_date = date("Y-m-d", strtotime($ppv_subscription_data['created_date']));
        $todayDate = date("Y-m-d");
        
        $connection = Yii::app()->db;
        $cond = "studio_id= {$studio_id} AND user_id = {$user_id} AND movie_id = {$movie_id} AND video_id = {$stream_id} AND DATE_FORMAT(created_date,'%Y-%m-%d') >= '".$purchased_date."' AND DATE_FORMAT(created_date,'%Y-%m-%d') <= '".$todayDate."'";
        $sql = "SELECT COUNT(id) AS tot FROM video_logs WHERE {$cond} ";
        $log_obj = $connection->createCommand($sql);
        $log_data = $log_obj->queryRow();
        $watched = @$log_data['tot'];
        
        $view_restriction = @$ppv_subscription_data['view_restriction'];
        
        if ($view_restriction > 0 && $watched >= $view_restriction) {
            return false;
        } else {
            return true;
        }
    }
    
    public function getContentPriceDetail($arg = array()) {
        $studio_id = $arg['studio_id'];
        $movie_id = $arg['movie_id'];
        $default_currency_id = $arg['default_currency_id'];
        
        $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
        $gateway = self::isPaymentGatwayExists($studio_id);
        
        $contentPrice = array();
        $contentPrice['isSuccess'] = 1;
        
        //Voucher with payment gateway(monetization)
        if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
            $is_voucher_enabled = self::getContentInVoucher($studio_id, $movie_id);
            if (intval($is_voucher_enabled)) {
                $contentPrice['voucher'] = 1;
            } else {
                $contentPrice['voucher'] = 0;
            }
        }
        
        if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
            $film = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $movie_id));
            $content_types_id = (isset($film->content_types_id)) ? $film->content_types_id : 0;
            $default_currency_id = (isset($default_currency_id) && intval($default_currency_id)) ? $default_currency_id : Studio::model()->findByPk($studio_id)->default_currency_id;
            
            //Pre-Order
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 16)) {
                if ($content_types_id == 1) {
                    
                    //Check video is converted or not
                    $is_converted = movieStreams::model()->findByAttributes(array("movie_id" => $movie_id))->is_converted;
                    if ($is_converted != 1) {
                        $preorder_plan = self::checkAdvancePurchase($movie_id, $studio_id, 0);
                        
                        if (isset($preorder_plan) && !empty($preorder_plan)) {
                            $price = self::getPPVPrices($preorder_plan->id, $default_currency_id);

                            $contentPrice['pre_order']['subscriber_price'] = self::formatPrice($price['price_for_subscribed'], $price['currency_id']);
                            $contentPrice['pre_order']['nonsubscriber_price'] = self::formatPrice($price['price_for_unsubscribed'], $price['currency_id']);
                        }
                    }
                }
            }
            
            //Pay-per-view
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 2)) {
                if ($content_types_id != 3) {
                    $ppv_plan = self::checkAdvancePurchase($movie_id, $studio_id, 0);
                }

                if (empty($ppv_plan)) {
                    $ppv_plan = self::getContentPaymentType($content_types_id, $film->ppv_plan_id, $studio_id);
                }
                
                if (isset($ppv_plan) && !empty($ppv_plan)) {
                    $price = self::getPPVPrices($ppv_plan->id, $default_currency_id);

                    if ($content_types_id != 3) {
                        $contentPrice['ppv']['subscriber_price'] = self::formatPrice($price['price_for_subscribed'], $price['currency_id']);
                        $contentPrice['ppv']['nonsubscriber_price'] = self::formatPrice($price['price_for_unsubscribed'], $price['currency_id']);
                    } else {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                        $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $is_multi_season = (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)) ? 1 : 0;
                        $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                        if (intval($is_show)) {
                            $contentPrice['ppv']['show']['subscriber_price'] = self::formatPrice($price['show_subscribed'], $price['currency_id']);
                            $contentPrice['ppv']['show']['nonsubscriber_price'] = self::formatPrice($price['show_unsubscribed'], $price['currency_id']);
                        }

                        if (intval($is_season)) {
                            $contentPrice['ppv']['season']['default_price']['subscriber_price'] = self::formatPrice($price['season_subscribed'], $price['currency_id']);
                            $contentPrice['ppv']['season']['default_price']['nonsubscriber_price'] = self::formatPrice($price['season_unsubscribed'], $price['currency_id']);

                            if (intval($is_multi_season) && intval($price['ppv_season_status'])) {
								$command = Yii::app()->db->createCommand()
									->select('season_number_subscribed, season_number_unsubscribed, season_subscribed, season_unsubscribed')
									->from('ppv_multi_season_pricing')
									->where('ppv_plan_id=:ppv_plan_id AND ppv_pricing_id=:ppv_pricing_id AND currency_id=:currency_id AND status=1', array(':ppv_plan_id' => $price['ppv_plan_id'], ':ppv_pricing_id' => $price['id'], ':currency_id' => $price['currency_id']), array('order' => 'created_date DESC, movie_id, season_id, episode_id'));
								$season_price = $command->queryAll();


                                if (isset($season_price) && !empty($season_price)) {
                                    foreach ($season_price as $season) {
                                        if (isset($season['season_number_subscribed']) && intval($season['season_number_subscribed']) && isset($season['season_subscribed']) && !empty($season['season_subscribed'])) {
                                            $season_number = $season['season_number_subscribed'];
                                            $contentPrice['ppv']['season'][$season_number]['subscriber_price'] = self::formatPrice($season['season_subscribed'], $season['currency_id']);
                                        }

                                        if (isset($season['season_number_unsubscribed']) && intval($season['season_number_unsubscribed']) && isset($season['season_unsubscribed']) && !empty($season['season_unsubscribed'])) {
                                            $season_number = $season['season_number_unsubscribed'];
                                            $contentPrice['ppv']['season'][$season_number]['nonsubscriber_price'] = self::formatPrice($season['season_unsubscribed'], $season['currency_id']);
                                        }
                                    }
                                }
                            }
                        }

                        if (intval($is_episode)) {
                            $contentPrice['ppv']['episode']['subscriber_price'] = self::formatPrice($price['episode_subscribed'], $price['currency_id']);
                            $contentPrice['ppv']['episode']['nonsubscriber_price'] = self::formatPrice($price['episode_unsubscribed'], $price['currency_id']);
                        }
                    }
                }
            }
            
            //Ppv Bundles
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 64)) {
                $ppv_bundle_plan = self::getAllPPVBundle($movie_id);
                
                if (isset($ppv_bundle_plan) && !empty($ppv_bundle_plan)) {
                    foreach ($ppv_bundle_plan as $key => $value) {
                        $contentPrice['ppv_bundle'][$key]['bundle_plan_name'] = $value['title'];
                        
                        $timeframe_prices = self::getAllTimeFramePrices($value['id'], $default_currency_id, $studio_id);
                        
                        if (isset($timeframe_prices) && !empty($timeframe_prices)) {
                            foreach ($timeframe_prices as $key1 => $bundle_price) {
                                $contentPrice['ppv_bundle'][$key]['timeframes'][$key1]['days'] = $bundle_price['title'];
                                $contentPrice['ppv_bundle'][$key]['timeframes'][$key1]['subscriber_price'] = self::formatPrice($bundle_price['price_for_subscribed'], $bundle_price['currency_id']);
                                $contentPrice['ppv_bundle'][$key]['timeframes'][$key1]['nonsubscriber_price'] = self::formatPrice($bundle_price['price_for_unsubscribed'], $bundle_price['currency_id']);
                            }
                        }
                    }
                }
            }
        }
        
        return $contentPrice;
    }
    
    public function getMonetizationsForContent($arg = array()) {
        /*1.subscription----1*/
        /*2.pay-per-view----2*/
        /*3.advertising-----4*/
        /*4.freecontent-----8*/
        /*5.Pre-Order------16*/
        /*6.coupons--------32*/
        /*7.Ppv Bundles----64*/
        /*8.Voucher-------128*/
        $controller = Yii::app()->controller;
        $monetization = array();
        $studio_id = (isset($arg['studio_id'])) ? $arg['studio_id'] : self::getStudiosId();
        $user_id = (isset($arg['user_id'])) ? $arg['user_id'] : (isset(Yii::app()->user->id) ? Yii::app()->user->id : 0);
        $movie_id = (isset($arg['movie_id'])) ? $arg['movie_id'] : 0;
        $purchase_type = (isset($arg['purchase_type'])) ? $arg['purchase_type'] : 'show';
        $stream_id = (isset($arg['stream_id'])) ? $arg['stream_id'] : '';
        $content = (isset($arg['content'])) ? $arg['content'] : '';
        
        $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
		$is_subscribed_user = $is_subscribed = self::isSubscribed($user_id, $studio_id);
		
        $film = $arg['film'];
        
        $gateway = self::isPaymentGatwayExists($studio_id);
        if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
            $isOnlyOneMonetization = 0;
            if($is_subscribed) {
                $isOnlyOneMonetization = 1;
            }
            
            //Pre-Order is special case
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 16)) {
                $preorder_plan = self::checkAdvancePurchase($movie_id, $studio_id, 0);

                if (isset($preorder_plan) && !empty($preorder_plan) && $film->content_types_id == 1) {
                    $content_types_id = $preorder_plan->content_types_id;
                    $is_advance_purchase = $preorder_plan->is_advance_purchase;

                    //Check video is converted or not
                    $is_converted = movieStreams::model()->findByAttributes(array("movie_id" => $movie_id))->is_converted;

                    if ($is_converted != 1) {
                        $isOnlyOneMonetization = 0;
                        $monetization['monetization']['pre_order'] = (trim($controller->Language['pre_order'])) ? $controller->Language['pre_order'] : 'Pre-Order';
                        $monetization['monetization_plans']['pre_order'] = $preorder_plan;

                        //Voucher with payment gateway(monetization)
                        if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
                            $is_voucher_enabled = self::getContentInVoucher($studio_id, $movie_id);
                            if (intval($is_voucher_enabled)) {
                                $monetization['monetization']['voucher'] = (trim($controller->Language['voucher'])) ? $controller->Language['voucher'] : 'Voucher';
                            }
                        }
                        $monetization['is_play'] = $isOnlyOneMonetization;
                        return $monetization;
                    }
                }
            }
            
            //Pay-per-view
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 2)) {
                $ppv_plan = self::checkAdvancePurchase($movie_id, $studio_id, 0);

                if (empty($ppv_plan)) {
                    $ppv_plan = self::getContentPaymentType($film->content_types_id, $film->ppv_plan_id, $studio_id);
                }
                
                if (isset($ppv_plan) && !empty($ppv_plan)) {
                    $isOnlyOneMonetization = 0;
                    $default_currency_id = (isset($this->studio->default_currency_id) && intval($this->studio->default_currency_id)) ? $this->studio->default_currency_id : Studio::model()->findByPk($studio_id)->default_currency_id;
                    $price = self::getPPVPrices($ppv_plan->id, $default_currency_id);
                    
                    $content_types_id = $ppv_plan->content_types_id;

                    if ($content_types_id == 3) {
                        $season = (isset($arg['season'])) ? $arg['season'] : 1;
                    }
                    
                    $isPriceZero = self::isPlanPriceZero($price, $is_subscribed, $content_types_id, $purchase_type, $studio_id, $season);
                    if (!$isPriceZero) {
                        $isOnlyOneMonetization = 0;
                        $monetization['monetization']['ppv'] =  (trim($controller->Language['Pay_Per_View'])) ? $controller->Language['Pay_Per_View'] : 'Pay Per View';
                        $monetization['monetization_plans']['ppv'] = $ppv_plan;
                    }else{
                        if($is_subscribed) {
                            $isOnlyOneMonetization = 1;
                        }
                    }
                }
            }

            //Ppv Bundles
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 64)) {
                if (($studio_id == 3866) && intval($is_subscribed_user)) {
					
				} else {
				$ppv_bundle_plan = self::getPPVBundle($movie_id);

                if (!empty($ppv_bundle_plan)) {
                    $isOnlyOneMonetization = 0;
                    $monetization['monetization']['ppv_bundle'] = (trim($controller->Language['Pay_Per_View_Bundle'])) ? $controller->Language['Pay_Per_View_Bundle'] : 'Pay-Per-View Bundle';
                    $monetization['monetization_plans']['ppv_bundle'] = $ppv_bundle_plan;
                }
                }
            }
           
            //Subscription Bundles
             $Subscriptionsplan_active = self::getSubscriptionBundleactiveplan($studio_id);
          if (isset($gateway['gateways']) && !empty($gateway['gateways']) && $gateway['gateways'][0]->short_code=='stripe') {
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 1) && $Subscriptionsplan_active>=1) {
                $Subscription_bundle_plan = self::getSubscriptionBundle($movie_id, $studio_id);
                $plans = UserSubscription::model()->UserSubscriptionBundlesPlanExist($studio_id,$user_id);
                $Countplans=count($plans);
				if (($is_subscribed_user == '') && !empty($Subscription_bundle_plan['id'])) {
					$without_subscriber_havingbundle = 1;
				} else if ($is_subscribed_user != '' && !empty($Subscription_bundle_plan['id']) && $Countplans == 0) {
					$without_subscriber_havingbundle = 0;
				} else if (($is_subscribed_user == 1) && !empty($Subscription_bundle_plan['id']) && $Countplans > 0) {
					$without_subscriber_havingbundle = 1;
				} else if ($is_subscribed_user != '' && !empty($Subscription_bundle_plan['id']) && $Countplans > 0) {
					$without_subscriber_havingbundle = 0;
				} else if (isset($monetizations['menu']) && ($monetizations['menu'] & 1) && !($monetizations['menu'] & 2) && !($monetizations['menu'] & 16) && !($monetizations['menu'] & 64) && !($monetizations['menu'] & 128) && !($monetizations['menu'] & 4) && !($monetizations['menu'] & 8) && ($is_subscribed_user == '')) {
					$without_subscriber_havingbundle = 1;
				} else if (isset($monetizations['menu']) && ($monetizations['menu'] & 1) && ($is_subscribed_user == '') && empty($Subscription_bundle_plan['id'])) {
					$without_subscriber_havingbundle = 1;
				}
				
				if ($without_subscriber_havingbundle ==1) {
                    $isOnlyOneMonetization = 0;
                    $monetization['monetization']['subscription_bundles'] = (trim($controller->Language['subscription_bundles'])) ? $controller->Language['subscription_bundles'] : 'Subscription';
                    $monetization['monetization_plans']['subscription_bundles'] = $Subscription_bundle_plan;
                }
            }
          }
            //Voucher with payment gateway(monetization)
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
                $is_voucher_enabled = self::getContentInVoucher($studio_id, $content);
                if (intval($is_voucher_enabled)) {
                    $monetization['monetization']['voucher'] = (trim($controller->Language['voucher'])) ? $controller->Language['voucher'] : 'Voucher';
                }
            }
        } else {
            //Voucher without payment gateway(monetization)
            if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
                $is_voucher_enabled = self::getContentInVoucher($studio_id, $content);
                if (intval($is_voucher_enabled)) {
                    $monetization['monetization']['voucher'] = (trim($controller->Language['voucher'])) ? $controller->Language['voucher'] : 'Voucher';
                }
            }
        }
        $monetization['is_play'] = $isOnlyOneMonetization;
        return $monetization;
    }
    
    public function isSubscribedContent($studio, $user_id, $stream_id = 0) {
        $studio_id = (isset($studio->id)) ? $studio->id: $studio;
        $plans = SubscriptionPlans::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1), array('order' => 'id ASC'))->attributes;
        
        if (isset($plans) && !empty($plans)) {
            $now = Date('Y-m-d H:i:s');
			$plan = Yii::app()->db->createCommand()
			->select('id')
			->from("user_subscriptions")
			->where('id > 0 AND user_id =:user_id AND studio_id=:studio_id AND status=1  AND end_date > :end_date',array(':user_id'=>$user_id,':studio_id'=>$studio_id,':end_date'=>$now), array('order' => 'id DESC'))
			->queryRow();

            if ($plan) {
				return 'allowed';
            } else {
				//Checking if he has cancelled subscription previously
                $cancel_user = Yii::app()->db->createCommand()
				->select('id, will_cancel_on')
				->from("sdk_users")
				->where('id > 0 AND id =:user_id AND studio_id=:studio_id AND is_deleted=1',array(':user_id'=>$user_id,':studio_id'=>$studio_id), array('order' => 'id DESC'))
				->queryRow();
				
                if ($cancel_user) {
                    if (trim($cancel_user['will_cancel_on']) && $cancel_user['will_cancel_on'] != '' && strtotime($cancel_user['will_cancel_on']) > strtotime(date('Y-m-d H:i:s'))) {
						return 'allowed';
                    } else {
                        return 'cancelled';
                    }
                } else {
                    return 'unsubscribed';
                }
            }
        } else {
            return 'allowed';
        }
    }

    function getPPVSeasonPrice($arg = array()) {
        $ppv_plan_id = isset($arg['ppv_plan_id']) ? $arg['ppv_plan_id'] : (isset($_POST['ppv_plan_id']) ? $_POST['ppv_plan_id'] : 0);
        $ppv_pricing_id = isset($arg['ppv_pricing_id']) ? $arg['ppv_pricing_id'] : (isset($_POST['ppv_pricing_id']) ? $_POST['ppv_pricing_id'] : 0);
        $studio_id = isset($arg['studio_id']) ? $arg['studio_id'] : $this->studio->id;
        $currency_id = isset($arg['currency_id']) ? $arg['currency_id'] : ((isset($this->studio->default_currency_id) && intval($this->studio->default_currency_id)) ? $this->studio->default_currency_id : Studio::model()->findByPk($studio_id)->default_currency_id);
        $isSubscribed = isset($arg['is_subscribed']) ? $arg['is_subscribed'] : (isset($_POST['is_subscribed']) ? $_POST['is_subscribed'] : 0);
        $season = isset($arg['season']) ? $arg['season'] : (isset($_POST['season']) ? $_POST['season'] : 0);
        
        $fields = (intval($isSubscribed)) ? "season_subscribed" : "season_unsubscribed";
        $condition = (intval($isSubscribed)) ? "AND season_number_subscribed=:season" : "AND season_number_unsubscribed=:season";
		$conditionArr = array(':ppv_plan_id'=>$ppv_plan_id, ':ppv_pricing_id' => $ppv_pricing_id, ':currency_id'=> $currency_id, ':season'=>$season);
        
        $sql = Yii::app()->db->createCommand()
		->select($fields)
		->from('ppv_multi_season_pricing')
		->where("ppv_plan_id=:ppv_plan_id AND ppv_pricing_id=:ppv_pricing_id AND currency_id=:currency_id AND status=1 {$condition}", $conditionArr);
		$data = $sql->queryRow();
        
        if (isset($data[$fields]) && !empty($data[$fields])) {
            return number_format($data[$fields], 2, '.', ' ');//Return when price has set(may be 0 or >0)
        } else {
            return -1;//Return when no special season price has not been set
        }
    }
    
    public function isPlanPriceZero($price = NULL, $isSubscribed = 0, $content_types_id = 1, $purchase_type = NULL, $studio_id = 0, $season=0) {
        $return = FALSE;
        
        if (isset($content_types_id) && $content_types_id == 3) {
            
            $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
            
            $selected_season_price = (intval($isSubscribed)) ? $price['season_subscribed'] : $price['season_unsubscribed'];
            if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) && isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)) {
                if (isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                    $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                    $season_price_data['ppv_pricing_id'] = $price['id'];
                    $season_price_data['studio_id'] = $studio_id;
                    $season_price_data['currency_id'] = $price['currency_id'];
                    $season_price_data['season'] = $season;
                    $season_price_data['is_subscribed'] = $isSubscribed;

                    $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                    if ($season_price != -1) {
                        $selected_season_price = $season_price;
                    }
                }
            }
            
            if ($isSubscribed) {
                if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//111
                    if (abs($price['episode_subscribed']) < 0.00001) {
                        if (abs($selected_season_price) < 0.00001) {
                            if (abs($price['show_subscribed']) < 0.00001) {
                                $return = TRUE;
                            }
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode) == 0)) {//110
                    if (abs($selected_season_price) < 0.00001) {
                        if (abs($price['show_subscribed']) < 0.00001) {
                            $return = TRUE;
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) == 0) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//101
                    if (abs($price['episode_subscribed']) < 0.00001) {
                        if (abs($price['show_subscribed']) < 0.00001) {
                            $return = TRUE;
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) == 0) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode) == 0)) {//100
                    if (abs($price['show_subscribed']) < 0.00001) {
                        $return = TRUE;
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show) == 0) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//011
                    if (abs($price['episode_subscribed']) < 0.00001) {
                        if (abs($selected_season_price) < 0.00001) {
                            $return = TRUE;
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show) == 0) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode) == 0)) {//010
                    if (abs($selected_season_price) < 0.00001) {
                        $return = TRUE;
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show) == 0) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) == 0) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//001
                    if (abs($price['episode_subscribed']) < 0.00001) {
                        $return = TRUE;
                    }
                }
            } else {
                if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//111
                    if (abs($price['episode_unsubscribed']) < 0.00001) {
                        if (abs($selected_season_price) < 0.00001) {
                            if (abs($price['show_unsubscribed']) < 0.00001) {
                                $return = TRUE;
                            }
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode) == 0)) {//110
                    if (abs($selected_season_price) < 0.00001) {
                        if (abs($price['show_unsubscribed']) < 0.00001) {
                            $return = TRUE;
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) == 0) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//101
                    if (abs($price['episode_unsubscribed']) < 0.00001) {
                        if (abs($price['show_unsubscribed']) < 0.00001) {
                            $return = TRUE;
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) == 0) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode) == 0)) {//100
                    if (abs($price['show_unsubscribed']) < 0.00001) {
                        $return = TRUE;
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show) == 0) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//011
                    if (abs($price['episode_unsubscribed']) < 0.00001) {
                        if (abs($selected_season_price) < 0.00001) {
                            $return = TRUE;
                        }
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show) == 0) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode) == 0)) {//010
                    if (abs($selected_season_price) < 0.00001) {
                        $return = TRUE;
                    }
                } else if ((isset($ppv_buy->is_show) && intval($ppv_buy->is_show) == 0) && (isset($ppv_buy->is_season) && intval($ppv_buy->is_season) == 0) && (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode))) {//001
                    if (abs($price['episode_unsubscribed']) < 0.00001) {
                        $return = TRUE;
                    }
                }
            }
        } else {
            if ($isSubscribed) {
                if (abs($price['price_for_subscribed']) < 0.00001) {
                    $return = TRUE;
                }
            } else {
                if (abs($price['price_for_unsubscribed']) < 0.00001) {
                    $return = TRUE;
                }
            }
        }
        
        return $return;
    }

    public function itemsBuyInMultiContentPPV($studio_id = NULL) {
        if (!isset($studio_id)) {
            $studio_id = self::getStudiosId();
        }
        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));

        return $ppv_buy;
    }

    public function IsAdvPaid($movie_id = 0, $stream_id = 0, $season = 0, $content_types_id = 1, $purchase_type = Null, $studio_id, $user_id) {
        //Explicitily pass from Rest controller otherwise default
        if (!isset($studio_id)) {
            $studio_id = self::getStudiosId();
        }

        if (!isset($user_id)) {
            $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        }

        $isPlay = 0; //0: unpaid, 1: allowed, 2: maximum

        if (isset($content_types_id) && $content_types_id == 3) {
            $stream = new movieStreams();
            $series_number = $stream->findByPk($stream_id)->series_number;
            $season = $series_number ? $series_number : 0;

			$cond = '';
			$condArr = array();
		
            if (isset($purchase_type) && $purchase_type == "episode") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)";
                $condArr = array(':season' => $season, ':episode_id' => $stream_id);
            } else if (isset($purchase_type) && $purchase_type == "season") {
				$cond = "(season_id =:season OR season_id=0) AND (episode_id=0)";
                $condArr = array(':season' => $season);
            } else {
                $cond = "(season_id=0) AND (episode_id=0)";
            }

			$conditions = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id);
			$allcondarr = array_merge($conditions, $condArr);
		
            $command = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_advance_purchase=1 AND '.$cond, $allcondarr, array('order' => 'movie_id, season_id, episode_id'));
            $row = $command->queryRow();
			
            if (isset($row) && !empty($row)) {
                if ($row['season_id'] == 0 && $row['episode_id'] == 0) {//Purchase entire show before
                    $isPlay = 1;
                } else if ($row['season_id'] != 0 && $row['episode_id'] == 0) {//Purchase entire season before
                    $isPlay = 1;
                } else if ($row['season_id'] != 0 && $row['episode_id'] != 0) {//Purchase entire episode before
                                $isPlay = 1;
                            }
                        }
                    } else {
            $command = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_advance_purchase=1 AND season_id=0 AND episode_id=0', array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id), array('order' => 'movie_id, season_id, episode_id'));
            $row = $command->queryRow();

            if (isset($row) && !empty($row)) {
                $isPlay = 1;
            }
        }

        return $isPlay;
    }
    
    public function IsVoucherSubscribed($movie_id = 0, $stream_id = 0, $season = 0, $purchase_type = Null, $studio_id, $user_id, $content_types_id, $is_pre_order_content=0){
        //Explicitily pass from Rest controller otherwise default
        if (!isset($studio_id)) {
            $studio_id = self::getStudiosId();
        }

        if (!isset($user_id)) {
            $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        }

        $cond = '';
        $condArr = array();
		
        if (isset($content_types_id) && $content_types_id == 3) {
        if (isset($purchase_type) && $purchase_type == "episode") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)";
                $condArr = array(':season' => $season, ':episode_id' => $stream_id);
        } else if (isset($purchase_type) && $purchase_type == "season") {
				$cond = "(season_id =:season OR season_id=0) AND (episode_id=0)";
                $condArr = array(':season' => $season);
            } else {
            $cond = "(season_id=0) AND (episode_id=0)";
        }
            } else {
			$cond = "season_id=0 AND episode_id=0";
                        }
        
		$conditions = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id);
		$allcondarr = array_merge($conditions, $condArr);
		
        $command = Yii::app()->db->createCommand()
				->select('*')
				->from('ppv_subscriptions')
				->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_voucher=1 AND '.$cond, $allcondarr, array('order' => 'created_date DESC, movie_id, season_id, episode_id'));
		$vdata = $command->queryRow();
        
        return $vdata;
    }

    public function IsPpvPaid($movie_id = 0, $stream_id = 0, $season = 0, $content_types_id = 1, $purchase_type = Null, $studio_id, $user_id, $is_advance_purchase = 0) {
        //Explicitily pass from Rest controller otherwise default
        if (!isset($studio_id)) {
            $studio_id = self::getStudiosId();
        }

        if (!isset($user_id)) {
            $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        }

        $cond = '';
        $condArr = array();
        if (isset($content_types_id) && $content_types_id == 3) {
            $stream = new movieStreams();
            $series_number = $stream->findByPk($stream_id)->series_number;
            $season = $series_number ? $series_number : 0;

            if (isset($purchase_type) && $purchase_type == "episode") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)";
                $condArr = array(':season' => $season, ':episode_id' => $stream_id);
            } else if (isset($purchase_type) && $purchase_type == "season") {
				$cond = "(season_id =:season OR season_id=0) AND (episode_id=0)";
                $condArr = array(':season' => $season);
            } else {
                $cond = "(season_id=0) AND (episode_id=0)";
            }
                    } else {
            $cond = " season_id=0 AND episode_id=0 ";
                    }

		$conditions = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id);
		$allcondarr = array_merge($conditions, $condArr);
		
        $command = Yii::app()->db->createCommand()
				->select('*')
				->from('ppv_subscriptions')
				->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_voucher!=1 AND is_ppv_bundle!=1 AND '.$cond, $allcondarr, array('order' => 'created_date DESC, movie_id, season_id, episode_id'));
		$ppv_subscription_data = $command->queryRow();

        return $ppv_subscription_data;
            }

    public function nextPayment() {
        $connection = Yii::app()->db;
        $user_id = Yii::app()->user->id;
        $studio_id = self::getStudiosId();

        $plan = new UserSubscription;
        $plan = $plan->findAllByAttributes(array('user_id' => $user_id, 'studio_id' => $studio_id), array('order' => 'id ASC'));
        if (count($plan) > 0) {
            foreach ($plan as $pl) {
                return $pl->end_date;
            }
        } else
            return '';
    }

    public function isAllowedInCountry($movie_id,$studio_id,$country) {
        if (self::isGeoBlockContent($movie_id,0,$studio_id,$country)) {//Auther manas@muvi.com
            return true;
        } else {
            return false;
        }
    }

    public function getPlayMessage($status) {
        if ($status == 'limitedcountry' || $status == 'country restriction')
            $msg = 'This Movie is not allowed to watch in your country.';
        else if ($status == 'maximum')
            $msg = 'Seems like you have already watched 3 movies this month. Please come back next month to watch again.';
        else if ($status == 'unsubscribed')
            $msg = 'You have not subscribed to watch.';
        else if ($status == 'invalid')
            $msg = 'You are not authorized to watch this video.';
        else if ($status == 'unpaid')
            $msg = 'You are not authorized to watch this video.';
        else
            $msg = 'invalid link';
        return $msg;
    }

    public function getPaymentDetails($agent) {
        $val = array(
            'publishable_key' => '',
            'private_key' => '',
            'account_number' => '',
            'seller_id' => '',
            'api_username' => '',
            'api_password' => '',
            'paypal_api_username' => '',
            'paypal_api_password' => '',
            'paypal_api_signature' => '',
            'paypal_api_version' => '',
            'paypal_api_mode' => '',
        );
        if ($agent == 'maatv') {
            if ($_SERVER['HTTP_HOST'] == 'maaflix.com') {
                $mode = 'life';
            } else {
                $mode = 'sandbox';
            }
            if ($mode == 'life') {
                $paypal_api_username = 'somayajulu_api1.maatv.com';
                $paypal_api_password = 'JQJ2RK5WGK4D2LJK';
                $paypal_api_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AqggkB6m0VEueA-cKNrxWKCQH03f';
            } else {
                $paypal_api_username = 'somayajulu-facilitator_api1.maatv.com';
                $paypal_api_password = 'NX6R4KR5P33FWR84';
                $paypal_api_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AXBRK0dAWunbms6vqKnH9ChoyfUJ';
            }

            $val = array(
                'publishable_key' => '7A645368-BF30-4D00-A676-A770A2B0A1C6',
                'private_key' => 'A62346DA-584A-4CA0-B4E4-8A24A6ED3C7C',
                'account_number' => '901255869',
                'seller_id' => '901255869',
                'api_username' => 'testratiapi3',
                'api_password' => 'Fakirarati123',
                'paypal_api_username' => $paypal_api_username,
                'paypal_api_password' => $paypal_api_password,
                'paypal_api_signature' => $paypal_api_signature,
                'paypal_api_version' => '64',
                'paypal_api_mode' => $mode
            );
        } else {
            $mode = 'sandbox';
            if ($mode == 'life') {
                $paypal_api_username = 'muvidev-facilitator_api1.gmail.com';
                $paypal_api_password = 'H8JW937D93DG8FDD';
                $paypal_api_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31A.8CMegkLq6fB.evgHOsKbsmMShS';
            } else {
                $paypal_api_username = 'muvidev-facilitator_api1.gmail.com';
                $paypal_api_password = 'H8JW937D93DG8FDD';
                $paypal_api_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31A.8CMegkLq6fB.evgHOsKbsmMShS';
            }

            $val = array(
                'paypal_api_username' => $paypal_api_username,
                'paypal_api_password' => $paypal_api_password,
                'paypal_api_signature' => $paypal_api_signature,
                'paypal_api_version' => '64',
                'paypal_api_mode' => $mode
            );
        }
        return $val;
    }

    public function checkPaymentGateway() {
        $studio_id = self::getStudioId();

        //$pgts = Studio::model()->with(array('studiopaymentgateways' => array('condition'=>'studio_id='.$studio_id)))->findAll();
        //$pgts = Studio::model()->with('studiopaymentgateways')->findAll(array('condition'=>'studio_id='.$studio_id));
        $pgts = StudioPaymentGateways::model()->findAll(array('condition' => 'studio_id=' . $studio_id . ' AND status=1'));
        if (count($pgts) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function countryfromIP($ip) {
        $loc = self::getVisitorLocation($ip);
        $country = $loc['country'];
        return $country;
    }

    public function sendMail($to_email, $to_name, $from_email, $from_name, $subject, $template, $param) {
        $message = new YiiMailMessage;
        $message->view = $template;
        $params = $param;
        $message->subject = $subject;
        $message->setBody($params, 'text/html');
        $message->addTo($to_email);
        $message->from = $from_email;
        return Yii::app()->mail->send($message);
    }

    public function getSDKUserInfo($user_id = NULL) {
        $user = array();
        if (isset($user_id) && !empty($user_id)) {
            $usr = new SdkUser;
            $user = $usr->findByPk($user_id);
        }

        return $user;
    }

    public function isSubscribed($user_id, $studio_id = '', $is_AllFields = 0) {
        if (!$studio_id) {
            $studio_id = self::getStudioId();
        }

        if ($user_id > 0 && $user_id == 1038) {
            return true;
        }

        if ($user_id > 0) {
            if (intval($is_AllFields)) {
                $fields = "id, start_date";
            } else {
                $fields = "id";
            }

            $US_cls = new UserSubscription();
            $subs = $US_cls->find(array("select" => $fields, 'condition' => 'user_id=:user_id AND studio_id=:studio_id AND status=1 AND is_subscription_bundle=0', "params" => array(':user_id' => $user_id, ':studio_id' => $studio_id)));
            if (!empty($subs)) {
                if (intval($is_AllFields)) {
                    return $subs;
                } else {
                    return $subs->id;
                }
            } else {
//                $sql = "SELECT id, will_cancel_on FROM sdk_users WHERE id = '" . $user_id . "' AND studio_id = '" . $studio_id . "' AND is_deleted = 1 AND id > 0 ORDER BY id DESC LIMIT 1 OFFSET 0";
//                $connection = Yii::app()->db;
//                $command = $connection->createCommand($sql);
//                $cancel_user = $command->queryRow();
				
				$cancel_user = Yii::app()->db->createCommand()
                ->select('id, will_cancel_on')
                ->from('sdk_users')
                ->where('id = :user_id AND studio_id =:studio_id  AND is_deleted = 1 AND id > 0',array(':user_id'=>$user_id,':studio_id'=>$studio_id))
                ->order('id desc')
				->limit(1)
				->queryRow();
                
                if (isset($cancel_user) && !empty($cancel_user)) {

                    if (trim($cancel_user['will_cancel_on']) && $cancel_user['will_cancel_on'] != '' && strtotime($cancel_user['will_cancel_on']) > strtotime(date('Y-m-d H:i:s'))) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return;
        }
    }

    public function isSubscribedNotCancelled($user_id, $studio_id = '', $is_AllFields = 0) {
        if (!$studio_id) {
            $studio_id = self::getStudioId();
        }

        if ($user_id > 0 && $user_id == 1038) {
            return true;
        }

        if ($user_id > 0) {
            if (intval($is_AllFields)) {
                $fields = "id, start_date";
            } else {
                $fields = "id";
            }

            $US_cls = new UserSubscription();
            $subs = $US_cls->find(array("select" => $fields, 'condition' => 'user_id=:user_id AND studio_id=:studio_id AND status=1', "params" => array(':user_id' => $user_id, ':studio_id' => $studio_id)));
            if (!empty($subs)) {
                if (intval($is_AllFields)) {
                    return $subs;
                } else {
                    return $subs->id;
                }
            } else {
                    return false;
                
            }
        } else {
            return;
        }
    }

    public function isSubscribedAndPaymentSuccess($user_id, $studio_id = '') {
        if (!$studio_id) {
            $studio_id = self::getStudiosId();
        }
        if ($user_id > 0 && $user_id == 1038)
            return true;
        if ($user_id > 0) {
            $US_cls = new UserSubscription();
            $subs = $US_cls->find(array("select" => "payment_status", 'condition' => 'user_id=:user_id AND studio_id=:studio_id AND status=1', "params" => array(':user_id' => $user_id, ':studio_id' => $studio_id)));
            if ($subs) {
                return $subs->payment_status;
            } else {
                return false;
            }
        } else {
            return;
        }
    }

    public function haveCradInfo($user_id, $studio_id = 0) {
        if (!$studio_id) {
            $studio_id = self::getStudiosId();
        }
        if ($user_id > 0 && $user_id == 1038)
            return true;
        if ($user_id > 0) {
            $res = Yii::app()->db->createCommand()
			->select('id')
			->from("sdk_card_infos")
			->where('user_id=:user_id AND studio_id=:studio_id',array(':user_id'=>$user_id, ':studio_id'=>$studio_id))
			->queryRow();
			
            if (isset($res) && !empty($res)) {
                return $res['id'];
            } else {
                return false;
            }
        } else {
            return;
        }
    }

    public function isSubscribedPPV($video_id) {
        $studio_id = self::getStudiosId();
        $user_id = Yii::app()->user->id;
        if ($user_id > 0 && $user_id == 1038) {
            return true;
        }
        
        $return = 0;
        if ($user_id > 0) {
            //Check ppv is enable or not in studio
            $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
            if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 2)) {
                $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $video_id));

                $ppv_plan = self::getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id);
                if (isset($ppv_plan) && !empty($ppv_plan)) {
                    $plan_id = $ppv_plan->id;
                    $qry = "SELECT US.id FROM ppv_subscriptions US WHERE";
                    $qry.= " US.user_id = '" . $user_id . "'";
                    $qry.= " AND US.ppv_plan_id = " . $plan_id;
                    $qry.= " AND US.studio_id = '" . $studio_id . "' AND movie_id = ".$video_id." AND US.status = 1 AND end_date > NOW()";
                    $qry.= " ORDER BY US.id LIMIT 1";
                    $connection = Yii::app()->db;
                    $command = $connection->createCommand($qry);
                    $subs = $command->queryAll(); // execute a query SQL 
                    if (isset($subs) && !empty($subs)) {
                        $return = $subs[0]['id'];
                    }
                }
            }
        }
        
        return $return;
    }    
    

    public function cancelReasons() {
        $studio_id = self::getStudiosId();
        $reason = new CancelReason();
        $reasons = $reason->findAllByAttributes(array('studio_id' => $studio_id));
        return $reasons;
    }

    public function subscriptionDetails($subscription_id) {
        $agent = self::getAgent();
        $qry = "SELECT US.* FROM user_subscriptions US, transactions T WHERE";
        $qry.= " US.user_id = T.user_id AND US.transaction_id = T.id AND US.plan_id = T.plan_id";
        $qry.= " AND US.agent = '" . $agent . "'";
        $qry.= " AND US.id = '" . $subscription_id . "'";
        $qry.= " ORDER BY US.id LIMIT 1";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($qry);
        $subs = $command->queryAll(); // execute a query SQL   
        if (count($subs) > 0) {
            foreach ($subs as $sub) {
                return $sub;
            }
        } else {
            return false;
        }
    }

    public function pagemeta($page) {
        $agent = self::getAgent();
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        $seo_info = new SeoInfo();
        $title = $description = $keywords = '';
        $seodata = $seo_info->pageMeta($studio_id, $page);
        if (isset($seodata) && count($seodata)) {
            $title = $seodata['title'];
            $description = $seodata['description'];
            $keywords = $seodata['keywords'];
        }
        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }

    public function listpagemeta($type) {
        $agent = self::getAgent();
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        $seo_info = new SeoInfo();
        $seodata = array();
        $title = $description = $keywords = '';
        $seodata = $seo_info->listpagemeta($studio_id, $type);
        if (isset($seodata) && count($seodata) && $seodata) {
            $title = $seodata['title'];
            $description = $seodata['description'];
            $keywords = $seodata['keywords'];
        } else {
            $seodata = $seo_info->pageMeta($studio_id, 'contentlisting');
            $title = $seodata['title'];
            $description = $seodata['description'];
            $keywords = $seodata['keywords'];
        }

        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }
    public function listProductmeta() {
        $seodata = $seo_info->pageMeta($studio_id, 'contentlisting');
        $title = $seodata['title'];
        $description = $seodata['description'];
        $keywords = $seodata['keywords'];
        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }
    public function detailspagemeta($type, $movie_name, $movie_id = 0) {
        $agent = self::getAgent();
        $title = $description = $keywords = '';
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        $seo_info = new SeoInfo();
        $seodata = $seo_info->detailspagemeta($studio_id, $type, $movie_name, $movie_id);
        if (isset($seodata) && count($seodata) && $seodata) {
            $title = $seodata['title'];
            $description = self::strip_html_tags($seodata['description']);
            $keywords = $seodata['keywords'];
        } else {
            $seodata = $seo_info->pageMeta($studio_id, 'content');
            $title = $seodata['title'];
            $description = self::strip_html_tags($seodata['description']);
            $keywords = $seodata['keywords'];
        }

        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }
    public function productpagemeta($type,$movie_id = 0) {
        $agent = self::getAgent();
        $title = $description = $keywords = '';
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
        } else {
            $studio = $this->studioData;
        }
        $seo_info = new SeoInfo();
        $seodata = $seo_info->productpagemeta($studio_id, $type, $movie_id);
        if (isset($seodata) && count($seodata) && $seodata) {
            $title = $seodata['title'];
            $description = self::strip_html_tags($seodata['description']);
            $keywords = $seodata['keywords'];
        } else {
            $seodata = $seo_info->pageMeta($studio_id, 'store');
            $title = $seodata['title'];
            $description = self::strip_html_tags($seodata['description']);
            $keywords = $seodata['keywords'];
        }

        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }
    public function castpagemeta($celeb_name) {
        $agent = self::getAgent();
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
            $this->studioData = $studio;
        } else {
            $studio = $this->studioData;
        }
        $title = $description = $keywords = '';
        if ($agent == 'maatv') {
            $title = $celeb_name . ' movies and TV shows, listing of ' . $celeb_name . ' movies, tv shows, serials, series, programs and episodes';
            $description = 'Get listing of all the Movies, TV Shows, Serials, Programs and Episodes ' . $celeb_name . ' has acted in or appeared in at Demo.com -  Movies and TV Shows';
            $keywords = $celeb_name . ', ' . $celeb_name . ' movies, ' . $celeb_name . ' tv shows, ' . $celeb_name . ' acting, ' . $celeb_name . ' information';
        } else if ($agent == 'iskcon') {
            $title = $celeb_name . ' Pravachan, Aarti Videos and Lectures online at ' . ucfirst($studio->domain);
            $description = ' Watch all Pravachan, Aarti Videos and Lectures by ' . $celeb_name . ' in HQ quality at ' . ucfirst($studio->domain);
            $keywords = $celeb_name . ', ' . $celeb_name . ' pravachan, ' . $celeb_name . ' aarti, ' . $celeb_name . 'movies, ' . $celeb_name . ' information ';
        } else {
            $title = $celeb_name . ' ' . self::translate('movies') . ' and ' . self::translate('tv_shows') . ', listing of ' . $celeb_name . ' ' . self::translate('movies') . ', ' . self::translate('tv_shows');
            $description = 'Get listing of all the ' . self::translate('movies') . ', ' . self::translate('tv_shows') . ' ' . $celeb_name . ' has acted in or appeared in at ' . ucfirst($studio->domain) . ' -  ' . self::translate('movies') . ' and ' . self::translate('tv_shows');
            $keywords = $celeb_name . ', ' . $celeb_name . ' ' . self::translate('movies') . ', ' . $celeb_name . ' ' . self::translate('tv_shows') . ', ' . $celeb_name . ' acting, ' . $celeb_name . ' information';
        }

        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }

    public function defaultmeta() {
        $agent = self::getAgent();
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
            $this->studioData = $studio;
        } else {
            $studio = $this->studioData;
        }

        $title = $description = $keywords = '';
        if ($agent == 'maatv') {
            $title = Yii::app()->name;
            $description = 'Watch unlimited  Movies and TV Shows Online';
            $keywords = Yii::app()->name;
        } else if ($agent == 'iskcon') {

            $title = 'FAQs | ' . ucfirst($studio->domain);
            $description = 'Get answers to most of your popular questions at FAQs page.';
            $keywords = Yii::app()->name;
        } else {

            if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) {
                $default_title = Yii::app()->controller->action->id;
                $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                // echo $Words;exit;
                $title = Yii::app()->name . ' | ' . ucfirst($Words);
                $description = '';
                $keywords = Yii::app()->name;
            } else {
                $temp = Yii::app()->request->url;
                $temp = explode('/', $temp);
                $permalink = $temp[count($temp) - 1];
                $type = explode('-', $permalink);
                $default_title = implode(' ', $type);
                $default_title = ucwords($default_title);

                $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                // echo $Words;exit;
                $title = $Words . ' | ' . $studio->name;
                $description = '';
                $keywords = '';
            }
        }

        $meta = array('title' => $title, 'description' => $description, 'keywords' => $keywords);
        return $meta;
    }

    /* Formatting CMS page urls
     * @author RKS
     */

    public function formatPermalink($str, $page_id = 0, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = $str;
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        if (!trim($clean)) {
            $clean = str_replace(' ', '-', $str);
        }

        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        if ($clean == 'howitworks')
            $clean = 'how-it-works';
        else if ($clean == 'about')
            $clean = 'about-us';
        else if ($clean == 'team')
            $clean = 'our-team';
        else if ($clean == 'faqs')
            $clean = 'faq';
        else if ($clean == 'examples')
            $clean = 'example';
        else if ($clean == 'index')
            $clean = 'indexed';
        else if ($clean == 'tour')
            $clean = 'tours';
        else if ($clean == 'pricing')
            $clean = 'pricings';
        else if ($clean == 'howisitdiff')
            $clean = 'how-is-itdiff';

        $studio_id = self::getStudioId();
        $pages = Yii::app()->db->createCommand()
                ->select('id, permalink')
                ->from('pages')
                ->where('studio_id = :studio_id AND permalink LIKE :permalink', array(':studio_id' => $studio_id, ':permalink' => '%' . $clean . '%'))
                ->order('permalink DESC')
                ->limit(1)
                ->queryAll();
        if (count($pages) > 0) {
            if ($pages[0]['id'] != $page_id) {
                $perm = $pages[0]['permalink'];
                $parts = explode($clean, $perm);
                $end = 1;
                if (count($parts) > 0) {
                    $end = (int) $parts[1];
                    $end++;
                }
                $clean.= $end;
            } else {
                $clean = $pages[0]['permalink'];
            }
        }
        return $clean;
    }

    /*
     * @method getConfigValue getting the config value
     * @param $code:array, $fields: string
     * @return array
     * @author SNL
     */

    function getConfigValue($code = array(), $fields = '*') {
        $data = array();

        if (isset($code) && !empty($code)) {
            $cond = "";
            $orderby = "";
            foreach ($code as $key => $value) {
                $cond.="'" . $value . "',";
                $orderby.=$value . ",";
            }
            $cond = rtrim($cond, ',');
            $orderby = rtrim($orderby, ',');
            $sql = "SELECT $fields FROM configurations WHERE code in ($cond) ORDER BY find_in_set(code, '$orderby')";

            $dbcon = Yii::app()->db;
            $data = $dbcon->createCommand($sql)->queryAll();
        }
        return $data;
    }

    /*
     * @comment - Copy files and folders recursively
     * @params - Source folder, Destination Folder  
     * @author - RKS
     */

    function recurse_copy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    $this->recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
        return true;
    }

    /*
     * @comment - Remove files and folders recursively
     * @params - Source folder, Destination Folder  
     * @author - RKS
     */

    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        $this->rrmdir($dir . "/" . $object);
                    else
                        @unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            @rmdir($dir);
        }
    }

    public function convertToByte($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * 1024 * 1024);
        } else {
            return round($size);
        }
    }

    public function getMaxFileSize() {
        $max_file_size = '4M';
        $size = self::convertToByte(ini_get('upload_max_filesize'));
        $max_size = self::convertToByte($max_file_size);
        $max_file_size = ($size < $max_size) ? $size : $max_size;

        return $max_file_size;
    }

    /**
     * @method generateuniqNumber()
     */
    function generateUniqNumber() {
        $uniq = uniqid(rand());
        return md5($uniq . time());
    }

    /**
     * @method public getIntrialUsers()
     * @author GDR<support@muvi.com>
     * @return int Total number of in trail users
     */
    function getIntrialUsers($studio_id = '') {
        if ($studio_id) {
            //$inTrialCount = UserSubscription::model()->count('status=:status AND DATE(start_date) > NOW() AND studio_id=:studio_id',array(':status'=>1,':studio_id'=>$studio_id));
            $sdk = new SdkUser();
            $totalCount = $sdk->getCountSdkUser($studio_id);
            $registeredCount = $sdk->getCountRegisteredSdkUser($studio_id);
            $res = count($totalCount) - count($registeredCount);
            return $res;
        } else {
            return 0;
        }
    }

    /**
     * @method public jcropImage() It will crop the image and save into the destination with the selection area by Jcrop
     * @return string URL of the cropped file
     * @author GDR<support@muvi.com>
     */
    function jcropImage($file, $dir = '', $data) {
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        if (!file_exists($dir . '/original')) {
            mkdir($dir . '/original', 0777);
        }
        $dst = $dir . '/original/' . $file['name'];
        if (!file_exists($dir . '/jcrop')) {
            mkdir($dir . '/jcrop', 0777);
        }
        $type = exif_imagetype($file['tmp_name']);
        $extension = image_type_to_extension($type);

        $src = $dir . '/jcrop/' . $file['name'];
        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {
            if (file_exists($src)) {
                unlink($src);
            }
            move_uploaded_file($file['tmp_name'], $src);
        } else {
            return FALSE;
        }
        if (!empty($src) && !empty($dst) && !empty($data)) {
            switch ($type) {
                case IMAGETYPE_GIF:
                    $src_img = imagecreatefromgif($src);
                    break;

                case IMAGETYPE_JPEG:
                    $src_img = imagecreatefromjpeg($src);
                    break;

                case IMAGETYPE_PNG:
                    $src_img = imagecreatefrompng($src);
                    break;
            }
            /* New code */
            $nWidth = (int) $data['w'];
            $nHeight = (int) $data['h'];
            $dst_img = $newImg = imagecreatetruecolor($nWidth, $nHeight);
            imagealphablending($newImg, false);
            imagesavealpha($newImg, true);
            $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
            imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
            $result = imagecopyresampled($newImg, $src_img, 0, 0, (int) $data['x1'], (int) $data['y1'], (int) $data['w'], (int) $data['h'], (int) $data['w'], (int) $data['h']);
            /* End */

            if ($result) {
                switch ($type) {
                    case IMAGETYPE_GIF:
                        $result = imagegif($dst_img, $dst);
                        break;

                    case IMAGETYPE_JPEG:
                        $result = imagejpeg($dst_img, $dst,100);
                        break;

                    case IMAGETYPE_PNG:
                        $result = imagepng($dst_img, $dst);
                        break;
                }

                if (!$result) {
                    $msg = "Failed to save the cropped image file";
                }
            } else {
                $msg = "Failed to crop the image file";
            }
            //imagedestroy($src_img);
            //imagedestroy($dst_img);
            return $dst;
        }
    }

    function jcroplibraryImage($filename, $s3path, $dir = '', $data) {
        //echo $dir;
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        if (!file_exists($dir . '/original')) {
            mkdir($dir . '/original', 0777);
        }
        $dst = $dir . '/original/' . $filename;
        // echo $dst; 
        //echo "test user";
        if (!file_exists($dir . '/jcrop')) {
            mkdir($dir . '/jcrop', 0777);
        }
        $type = exif_imagetype($s3path);
        $extension = image_type_to_extension($type);

        // echo $extension;
        $src = $dir . '/jcrop/' . $filename;
        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {
            //  echo "test";
            // exit;
            if (file_exists($src)) {
                unlink($src);
            }
            //move_uploaded_file($s3path, $src);

            file_put_contents($src, file_get_contents($s3path));
            // file_put_contents($dst, file_get_contents($s3path));
        } else {
            return FALSE;
        }
        if (!empty($src) && !empty($dst) && !empty($data)) {
            //  print_r($data);

            switch ($type) {
                case IMAGETYPE_GIF:
                    $src_img = imagecreatefromgif($src);
                    break;

                case IMAGETYPE_JPEG:
                    $src_img = imagecreatefromjpeg($src);
                    break;

                case IMAGETYPE_PNG:
                    $src_img = imagecreatefrompng($src);
                    break;
            }
            $dst_img = imagecreatetruecolor((int) $data['w'], (int) $data['h']);
            imagealphablending($dst_img, false);
            imagesavealpha($dst_img, true);
            $transparentindex = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
            imagefill($dst_img, 0, 0, $transparentindex);
            //End Transparent 
            $result = imagecopyresampled($dst_img, $src_img, 0, 0, (int) $data['x1'], (int) $data['y1'], (int) $data['w'], (int) $data['h'], (int) $data['w'], (int) $data['h']);
            // print_r($result);
            if ($result) {
                switch ($type) {
                    case IMAGETYPE_GIF:
                        $result = imagegif($dst_img, $dst);
                        break;

                    case IMAGETYPE_JPEG:
                        $result = imagejpeg($dst_img, $dst,100);
                        break;

                    case IMAGETYPE_PNG:
                        $result = imagepng($dst_img, $dst);
                        break;
                }

                if (!$result) {
                    $msg = "Failed to save the cropped image file";
                }
            } else {
                $msg = "Failed to crop the image file";
            }
            //  print_r($dst);
            //imagedestroy($src_img);
            //imagedestroy($dst_img);
            return $dst;
        }
    }

    /**
     * @method public getCropImageDimension(int $content_types_id) Return the size dimension of the image to be cropped
     * @author GDR<support@muvi.com>
     * @return array Returns a array of height and width
     */
    function getCropDimension($type, $studio_id = 0) {
        if ($studio_id == 0)
            $studio_id = self::getStudiosId();
			$poster_sizes = Yii::app()->general->getPosterSize($studio_id);
        if ($type == 2 || $type == 4 || ($type == 'episode')) {
            $dmn = $poster_sizes['h_poster_dimension'];
            $expl = explode('x', strtolower($dmn));
            return array('width' => @$expl[0], 'height' => @$expl[1]);
        } else {
            $dmn = $poster_sizes['v_poster_dimension'];
            $expl = explode('x', strtolower($dmn));
            return array('width' => @$expl[0], 'height' => @$expl[1]);
        }
    }

    /**
     * @method public getShows(int $contentTypeId) Return the array of shows
     * @author GDR<support@muvi.com>
     * @return array Returns a array
     */
    function getShows($contentTypesId = 3, $studio_id) {
        if (!$studio_id) {
            $studio_id = $this->getStudioId();
        }
        if ($contentTypesId) {
            $movies = Film::model()->findAll('content_types_id =:content_types_id AND studio_id =:studio_id AND parent_id=0', array(':content_types_id' => $contentTypesId, ':studio_id' => $studio_id));
            if ($movies) {
                $shows = CHtml::listData($movies, 'id', 'name');
                return $shows;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    /**
     * @method public getBucketinfo(int $contentTypeId) Return the array of shows
     * @author GDR<support@muvi.com>
     * @return array Returns a array
     */
    function getBucketInfo($s3bucket_id = 1, $studio_id = '') {
        if(isset($_SESSION['S3Bucket'][$studio_id]) && Yii::app()->controller->id != 'cron'){
            return $_SESSION['S3Bucket'][$studio_id];
        }
        $studio_s3bucket_id = 0;
        $is_subscribed = 0;
        $newUser = 0;
        $cdnStatus = 0;
        $arr = '';
        $arr['videoRemoteUrl'] = '';
        $arr['signedFolderPath'] = '';
        $arr['unsignedFolderPath'] = '';
        $arr['unsignedFolderPathForVideo'] = '';
        if ($studio_id) {
            if ($studio_id != $this->getStudioId()) {
                $studio = Studio::model()->getStudioBucketInfo($studio_id);
            } else {
                $studio = $this->studioData;
                $studio = $studio->attributes;
            }
            $s3bucket_id = $studio['s3bucket_id'];
            $studio_s3bucket_id = $studio['studio_s3bucket_id'];
            $is_subscribed = $studio['is_subscribed'];
            $newUser = $studio['new_cdn_users'];
            $cdnStatus = $studio['cdn_status'];
        }
        if ($studio_s3bucket_id != 0) {
            $studioS3buckets = StudioS3buckets::model()->findByPk($studio_s3bucket_id);
            if ($studioS3buckets) {
                $arr['bucket_name'] = $studioS3buckets->bucket_name;
                $arr['region_code'] = '';
                $arr['region_name'] = '';
                $arr['s3url'] = $studioS3buckets->s3url;
                $arr['s3cmd_file_name'] = $studioS3buckets->s3cmd_file_name;
                $arr['cloudfront_url'] = $studio['signed_url'];
                $arr['unsigned_cloudfront_url'] = $studio['unsigned_url'];
                $arr['videoRemoteUrl'] = $studio['video_url'];
            }
        } else {
            if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                $arr['bucket_name'] = 'stagingstudio';
                $arr['region_code'] = 'us-east-1';
                $arr['region_name'] = 'US';
                $arr['s3url'] = 's3.amazonaws.com';
                $arr['s3cmd_file_name'] = 's3cfg';
                if ($newUser == 1) {
                    if ($is_subscribed == 1 && $cdnStatus == 1) {
                        $arr['cloudfront_url'] = $studio['signed_url'];
                        $arr['unsigned_cloudfront_url'] = $studio['unsigned_url'];
                        $arr['videoRemoteUrl'] = $studio['video_url'];
                    } else {
                        $arr['cloudfront_url'] = 'd2gx0xinochgze.cloudfront.net';
                        $arr['unsigned_cloudfront_url'] = 'd2gx0xinochgze.cloudfront.net';
                        $arr['videoRemoteUrl'] = 'd2gx0xinochgze.cloudfront.net';
                    }
                } else {
                    $arr['cloudfront_url'] = 'd2gx0xinochgze.cloudfront.net';
                    $arr['unsigned_cloudfront_url'] = 'd2gx0xinochgze.cloudfront.net';
                    $arr['videoRemoteUrl'] = 'd2gx0xinochgze.cloudfront.net';
                }
            } else if ($s3bucket_id == 1) {
                $arr['bucket_name'] = 'vimeoassets';
                $arr['region_code'] = 'us-east-1';
                $arr['region_name'] = 'US';
                $arr['s3url'] = 's3.amazonaws.com';
                $arr['s3cmd_file_name'] = 's3cfg';
                if ($newUser == 1) {
                    if ($is_subscribed == 1 && $cdnStatus == 1) {
                        $arr['cloudfront_url'] = $studio['signed_url'];
                        $arr['unsigned_cloudfront_url'] = $studio['unsigned_url'];
                        $arr['videoRemoteUrl'] = $studio['video_url'];
                    } else {
                        $arr['cloudfront_url'] = 'd3ff6jhdmx1yhu.cloudfront.net';
                        $arr['unsigned_cloudfront_url'] = 'd73o4i22vgk5h.cloudfront.net';
                        $arr['videoRemoteUrl'] = 'd73o4i22vgk5h.cloudfront.net';
                    }
                } else {
                    $arr['cloudfront_url'] = 'd3ff6jhdmx1yhu.cloudfront.net';
                    $arr['unsigned_cloudfront_url'] = '';
                    $arr['videoRemoteUrl'] = 'd73o4i22vgk5h.cloudfront.net';
                }
            } else {
                $s3bucket = S3bucket::model()->findByPk($s3bucket_id);
                if ($s3bucket) {
                    $arr['bucket_name'] = $s3bucket->bucket_name;
                    $arr['region_code'] = $s3bucket->region_code;
                    $arr['region_name'] = $s3bucket->region_name;
                    $arr['s3url'] = $s3bucket->s3url;
                    $arr['s3cmd_file_name'] = 's3cfg';
                    if ($newUser == 1) {
                        if ($is_subscribed == 1 && $cdnStatus == 1) {
                            $arr['cloudfront_url'] = $studio['signed_url'];
                            $arr['unsigned_cloudfront_url'] = $studio['unsigned_url'];
                            $arr['videoRemoteUrl'] = $studio['video_url'];
                        } else {
                            $arr['cloudfront_url'] = $s3bucket->cloudfront_url;
                            $arr['unsigned_cloudfront_url'] = $s3bucket->unsigned_cloudfront_url;
                            $arr['videoRemoteUrl'] = $s3bucket->unsigned_cloudfront_url;
                        }
                    } else {
                        $arr['cloudfront_url'] = $s3bucket->cloudfront_url;
                        $arr['unsigned_cloudfront_url'] = '';
                        $arr['videoRemoteUrl'] = $s3bucket->unsigned_cloudfront_url;
                    }
                }
            }
        }
        if($newUser == 1 && $cdnStatus == 0){
            $arr['signedFolderPath'] = $studio_id."/EncodedVideo/";
            $arr['unsignedFolderPath'] = $studio_id."/public/";
            $arr['unsignedFolderPathForVideo'] = $studio_id."/RawVideo/";
        }
		$_SESSION['S3Bucket'][$studio_id] = $arr;
        return $arr;
    }

    /**
     * @method public getDistance() Get the distance from Latitude and Longitude
     * @return float Distance
     * @author GDR<support@muvi.com>
     */
    function getDistance($a, $b) {
        list($lat1, $lon1) = $a;
        list($lat2, $lon2) = $b;
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return $miles;
    }

    function studiourl($studio_id) {
        $studio = new Studio();
        $studio = $studio->findByPk($studio_id);
        if ($studio->is_live == 1 && $studio->reserved_domain != '') {
            $domain = $studio->reserved_domain;
        } else {
            $domain = $studio->domain;
        }
        return 'http://' . $domain;
    }

    function getOrgSubdomain() {
        $serv = str_replace('www.', '', $_SERVER['SERVER_NAME']);
        $svs = explode($serv, '.');
        return $svs[0];
    }

    public function isMobile() {
        if (isset($_REQUEST['is_mobile'])) {
            return true;
        } else {
            return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
        }
    }

    public function isPaymentGatwayAndPlanExists($studio_id = null, $default_currency_id = NULL, $country = NULL, $language_id = Null ,$subscriptionLimitedPlan=Null) {
        $res = array();
        $controller = Yii::app()->controller;
        $language_id = (isset($language_id) && intval($language_id)) ? $language_id : $controller->language_id;
        if (isset($studio_id) && !empty($studio_id)) {
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
            if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 1)) {
                $gateways = new StudioPaymentGateways;
                $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
                $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 't.is_primary DESC, t.id DESC'
                        )
                );
                $limitedCond='';
                if($subscriptionLimitedPlan==1){
                    $limitedCond='AND is_subscription_bundle=0';
                }    
                $plans = new SubscriptionPlans;
                $cond = 'studio_id=' . $studio_id . ' AND status = 1 ' . $limitedCond .' AND (language_id='.$language_id.' OR parent_id=0 AND id NOT IN (SELECT parent_id FROM subscription_plans WHERE studio_id='.$studio_id.' AND language_id='.$language_id.' ))';
                $plans = $plans::model()->with()->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 'id_sequency ASC, IF(parent_id >0, parent_id, id) ASC'
                        )
                );
                $pln = array();
                foreach ($plans as $plan) {
                    if($plan->parent_id > 0){
                        $plan->id = $plan->parent_id;
                    }

                    $pln[] = $plan;
                }
                $plans = $pln;

                if (isset($plans) && !empty($plans) && isset($gateways[0]) && !empty($gateways[0])) {

                    if (!isset($default_currency_id)) {
                        $default_currency_id = $controller->studio->default_currency_id;
                    }
                    foreach ($plans as $key => $value) {
                        $price_list = self::getUserSubscriptionPrice($value->id, $default_currency_id, $country,$studio_id);
                        $plans[$key]->price = $price_list['price'];
                        $plans[$key]->currency_id = $price_list['currency_id'];
                    }

                    $res['plans'] = $plans;
                    $res['gateways'] = $gateways;
                }else{
                     if (!isset($default_currency_id)) {
                        $default_currency_id = $controller->studio->default_currency_id;
                }
                    foreach ($plans as $key => $value) {
                        $price_list = self::getUserSubscriptionPrice($value->id, $default_currency_id, $country,$studio_id);
                        $plans[$key]->price = $price_list['price'];
                        $plans[$key]->currency_id = $price_list['currency_id'];
            }
                    $res['plans'] = $plans; 
                  
        }
            }
        }
        return $res;
    }

      public function isPaymentGatwayAndsubscriptionPlanExists($studio_id = null,$user_id= null, $default_currency_id = NULL, $country = NULL, $language_id = Null) {
 
    $res = array();
        $controller = Yii::app()->controller;
        $language_id = (isset($language_id) && intval($language_id)) ? $language_id : $controller->language_id;
        if (isset($studio_id) && !empty($studio_id)) {
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
            if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 1)) {
                $gateways = new StudioPaymentGateways;
                $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
                $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 't.is_primary DESC, t.id DESC'
                        )
                );
        $bundled_content = Yii::app()->db->createCommand()
                       ->select('GROUP_CONCAT(plan_id) AS plan_ids')
                       ->from("user_subscriptions")
                       ->where('studio_id=:studio_id and user_id=:user_id AND status=1',array(':studio_id' => $studio_id,':user_id'=>$user_id))
                       ->queryRow();
        $now = Date('Y-m-d H:i:s');
              $pid = explode(',', $bundled_content['plan_ids']);
                        $command = Yii::app()->db->createCommand()
                       ->select('distinct(sp.id),sp.*,spr.currency_id,spr.price')
                       ->join('subscription_pricing spr', 'spr.subscription_plan_id = sp.id')
                       ->from("subscription_plans sp")
                       ->where('sp.studio_id=:studio_id AND sp.status=1 AND spr.status =1 AND is_subscription_bundle=0 AND spr.currency_id=:currency_id', array(':studio_id'=>$studio_id,'currency_id'=>$default_currency_id))
                       ->andWhere(array('NOT IN','sp.id',$pid))
                       ->order('sp.id_sequency'); 
               $is_subscriptions_bundle_subscribed = $command->queryAll();
             
            $res['gateways'] = $gateways;  
            $res['plans'] = $is_subscriptions_bundle_subscribed; 
        }
        }
        return $res;
    //SELECT distinct(sp.id) FROM `subscription_plans` sp left join subscription_pricing spp on sp.id=spp.subscription_plan_id  where sp.id NOT IN (select plan_id from user_subscriptions where studio_id=331 and user_id=1406) AND sp.studio_id=331 ORDER BY `is_subscription_bundle` ASC
    
      }
    public function isPaymentGatwayAndBundlesPlanExists($studio_id = null,$user_id, $default_currency_id = NULL, $country = NULL, $language_id = Null) {
        $res = array();
        $controller = Yii::app()->controller;
        $language_id = (isset($language_id) && intval($language_id)) ? $language_id : $controller->language_id;
        if (isset($studio_id) && !empty($studio_id)) {
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
            if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 1)) {
                $gateways = new StudioPaymentGateways;
                $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
                $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 't.is_primary DESC, t.id DESC'
                        )
                );

                
                $plans = UserSubscription::model()->UserSubscriptionBundlesPlanExist($studio_id,$user_id);
               
                if (isset($plans) && !empty($plans) && isset($gateways[0]) && !empty($gateways[0])) {

                    if (!isset($default_currency_id)) {
                        $default_currency_id = $controller->studio->default_currency_id;
                    }
                    foreach ($plans as $key => $value) {
                        $price_list = self::getUserSubscriptionBundlesPrice($value['id'], $default_currency_id, $country);
                        $plans[$key]['price'] = $price_list['price'];
                        $plans[$key]['currency_id'] = $price_list['currency_id'];
                        if($value['is_subscription_bundle']==0){
                        $plans[$key]['is_subscribed_user']= Yii::app()->common->isSubscribed($user_id, $studio_id);
                        }else{
                        $plans[$key]['is_subscribed_user']=0;   
                    }

                    }

                    $res['plans'] = $plans;
                    $res['gateways'] = $gateways;
                }
            }
        }
        return $res;
    }

           public function isPaymentGatwayAndBundlesPlanExistscancelled($studio_id = null,$user_id, $default_currency_id = NULL, $country = NULL, $language_id = Null) {
        $res = array();
        $controller = Yii::app()->controller;
        $language_id = (isset($language_id) && intval($language_id)) ? $language_id : $controller->language_id;
        if (isset($studio_id) && !empty($studio_id)) {
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
    
            if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 1)) {
                $gateways = new StudioPaymentGateways;
                $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
                $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 't.is_primary DESC, t.id DESC'
                        )
                );

                
                $plans = UserSubscription::model()->IsUserTakenSubscriptionBundles($studio_id,$user_id);
              
                if (isset($plans) && !empty($plans) && isset($gateways[0]) && !empty($gateways[0])) {

                    if (!isset($default_currency_id)) {
                        $default_currency_id = $controller->studio->default_currency_id;
                    }
                    foreach ($plans as $key => $value) {
                        $price_list = self::getUserSubscriptionBundlesPrice($value['id'], $default_currency_id, $country);
                        $plans[$key]['price'] = $price_list['price'];
                        $plans[$key]['currency_id'] = $price_list['currency_id'];
                        if($value['is_subscription_bundle']==0){
                        $plans[$key]['is_subscribed_user']= Yii::app()->common->isSubscribed($user_id, $studio_id);
                        }else{
                        $plans[$key]['is_subscribed_user']=0;   
                        }
                        
                    }

                    $res['plans'] = $plans;
                    $res['gateways'] = $gateways;
                }
            }
        }
        return $res;
    }

        public function userSubscriptionBundlesPlanExists($studio_id = null,$user_id, $default_currency_id = NULL) {
        $res = array();
        $controller = Yii::app()->controller;
        $language_id = (isset($language_id) && intval($language_id)) ? $language_id : $controller->language_id;
        if (isset($studio_id) && !empty($studio_id)) {
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
            if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 256)) {
                 $res = Yii::app()->db->createCommand()
                       ->select(' spr.* ')
                       ->from("subscriptionbundles_pricing  spr")
                       ->leftJoin('currency cu' , 'spr.currency_id=cu.id')
                       ->where('cu.id=:id AND spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':id'=>$default_currency_id,':subscription_plan_id'=>$plan_id))
                       ->queryAll();
            }
        }
        return $res;
    }
    public function getUserSubscriptionPrice($plan_id = Null, $default_currency_id = Null, $country = Null,$studio_id) {
        $price = array();
        if (!isset($default_currency_id)) {
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;
        }

        if (isset($plan_id) && intval($plan_id)) {
            $is_default_currency = 0;
            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION[$studio_id]['country'] : '');

            if (isset($country) && trim($country)) {
                $price = Yii::app()->db->createCommand()
					->select(' spr.* ')
					->from("subscription_pricing  spr")
					->leftJoin('currency cu' , 'spr.currency_id=cu.id')
					->where('cu.country_code=:country_code AND spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':country_code'=>$country,':subscription_plan_id'=>$plan_id))
					->queryRow();

                if (empty($price)) {
                    $is_default_currency = 1;
                }
            } else {
                $is_default_currency = 1;
            }

            if (intval($is_default_currency)) {
                $price = Yii::app()->db->createCommand()
					->select(' spr.* ')
					->from("subscription_pricing  spr")
					->leftJoin('currency cu' , 'spr.currency_id=cu.id')
					->where('cu.id=:id AND spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':id'=>$default_currency_id,':subscription_plan_id'=>$plan_id))
					->queryRow();

                if (empty($price)) {
                    $price = Yii::app()->db->createCommand()
					->select(' spr.* ')
					->from("subscription_pricing  spr")
					->leftJoin('currency cu' , 'spr.currency_id=cu.id')
					->where('spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':subscription_plan_id'=>$plan_id))
					->queryRow();
                }
            }
        }

        return $price;
    }
    
	public function getUserSubscriptionBundlesPrice($plan_id = Null, $default_currency_id = Null, $country = Null) {
        $price = array();
        if (!isset($default_currency_id)) {
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;
        }
        if (isset($plan_id) && intval($plan_id)) {
            $is_default_currency = 0;
            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION['country']) && trim($_SESSION['country'])) ? $_SESSION['country'] : '');
            if (isset($country) && trim($country)) {
                 $price = Yii::app()->db->createCommand()
                       ->select(' spr.price,spr.currency_id ')
                       ->from("subscription_pricing  spr")
                       ->leftJoin('currency cu' , 'spr.currency_id=cu.id')
                       ->where('cu.country_code=:country_code AND spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':country_code'=>$country,':subscription_plan_id'=>$plan_id))
                       ->queryRow();
                if (empty($price)) {
                    $is_default_currency = 1;
                }
                } else {
                $is_default_currency = 1;
                    }
            if (intval($is_default_currency)) {
                  $price = Yii::app()->db->createCommand()
                       ->select(' spr.* ')
                       ->from("subscription_pricing  spr")
                       ->leftJoin('currency cu' , 'spr.currency_id=cu.id')
                       ->where('cu.id=:id AND spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':id'=>$default_currency_id,':subscription_plan_id'=>$plan_id))
                       ->queryRow();
                if (empty($price)) {
                     $price = Yii::app()->db->createCommand()
                       ->select(' spr.* ')
                       ->from("subscription_pricing  spr")
                       ->leftJoin('currency cu' , 'spr.currency_id=cu.id')
                       ->where('spr.subscription_plan_id=:subscription_plan_id AND spr.status=1',array(':subscription_plan_id'=>$plan_id))
                       ->queryRow();
                }
            }
        }
        return $price;
    }

 function getStudioEmails($studio_id = Null, $user_id = Null, $email_type = Null, $attached_file = Null, $VideoName = null,$VideoDetails = null, $to_email = null, $coupon_code = NULL, $coupon_amount = NULL,$lang_code='en',$BundleName=NULL, $studio_name = NULL, $amount = NULL, $monetization_name = NULL, $faliure_reason = NULL) {  
       if (isset($studio_id) && isset($user_id) && isset($email_type)) {
            $user_lang_code = SdkUser::model()->getUserLanguage($studio_id, $user_id);
            $lang_details = Language::model()->findByAttributes(array('code' => $user_lang_code));
            if (!empty($lang_details)) {
                $language_id = $lang_details->id;
            } else {
                $language_id = 20;
            }
            $std = new Studio();
            $studio = $std->findByPk($studio_id);

            $StudioUrl = $site_url = HTTP . $studio->domain;
            //$siteLogo=$site_url. '/images/logos/' . $studio->theme . '/' . $studio->logo_file;
            $siteLogo = $logo_path = Yii::app()->common->getLogoFavPath($studio_id);
            $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="site_logo" /></a>';


            //Check facebook link given or not
            if ($studio->fb_link != '') {
                $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
            } else {
                $fb_link = '';
            }

            //Check twitter link given or not
            if ($studio->tw_link != '') {
                $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
            } else {
                $twitter_link = '';
            }

            //Check google plus link given or not
            if ($studio->gp_link != '') {
                $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
            } else {
                $gplus_link = '';
            }

            if ($email_type == 'ppv_subscription' || $email_type == 'advance_purchase') {
                $monetization_code = 'ppv';
            } else {
                $monetization_code = 'voucher';
            }

            $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, $monetization_code, 'access_period');
            if (empty($validity)) {
                $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, $monetization_code, 'watch_period');
            }

            if ($email_type == 'ppv_subscription' || $email_type == 'advance_purchase') {
                $sql = "SELECT u.*, ps.*, pp.* FROM sdk_users u LEFT JOIN ppv_subscriptions ps 
                    ON (u.id=ps.user_id AND ps.status=1) LEFT JOIN ppv_plans pp ON (ps.ppv_plan_id=pp.id)
                    WHERE u.id=" . $user_id . " AND u.studio_id=" . $studio_id . " ORDER BY ps.id DESC LIMIT 0, 1";
            } else if ($email_type == 'advance_purchase_content_available') {
                $sql = "SELECT u.* FROM sdk_users u WHERE u.id=" . $user_id . " AND u.studio_id=" . $studio_id . " ORDER BY u.id DESC LIMIT 0, 1";
            } else {
                $cond = ' AND us.status=1';
                $cond1 = ' AND ci.is_cancelled=0';
                if ($email_type == 'subscription_cancellation' || $email_type == 'subscription_cancellation_user') {
                    $cond = '';
                    $cond1 = '';
                }
                $sql = "SELECT u.*, us.*, sp.*, sp.name AS subscription_plans, ci.* FROM sdk_users u LEFT JOIN user_subscriptions us 
                    ON (u.id=us.user_id " . $cond . ") LEFT JOIN subscription_plans sp ON (us.plan_id=sp.id) 
                    LEFT JOIN sdk_card_infos ci ON (u.id=ci.user_id AND u.studio_id=ci.studio_id " . $cond1 . ")
                    WHERE u.id=" . $user_id . " AND u.studio_id=" . $studio_id . " ORDER BY us.id DESC LIMIT 0, 1";
            }
            $connection = Yii::app()->db;
            $user = $connection->createCommand($sql)->queryRow();

            //Set variables for different email types
            if ($email_type == 'subscription_cancellation_user') {

                $EndSubscriptionDate = Date('F d, Y', strtotime($user['will_cancel_on']));
            }
            if ($email_type == 'advance_purchase') {
                $AdvanceAmount = self::formatPrice($user['amount'], $user['currency_id']);
            }

            if ($email_type == 'advance_purchase_content_available') {
                $VideoUrl = $site_url . '/player/' . $VideoDetails;
            }

            $email = (isset($to_email) && trim($to_email)) ? $to_email : $user['email'];
            $FirstName = $user['display_name'];
            $ActivationUrl = $StudioUrl . "/user/confirmation/token/" . $user['confirmation_token'];
            $period = (isset($validity->validity_period)) ? $validity->validity_period . ' ' . $validity->validity_recurrence . '(s)' : '';
            $expirePeriod = (isset($user['end_date'])) ? date('F d, Y @ h:i', strtotime($user['end_date'])) . " hrs" : '';
            $PlanName = (isset($user['subscription_plans'])) ? $user['subscription_plans'] : '';
            $Last4DigitsOfCard = (isset($user['card_last_fourdigit'])) ? $user['card_last_fourdigit'] : '';
            $FourDaysAfterSecondNotice = Date('F d, Y', strtotime("+4 days"));
            $TwoDaysAfterSecondNotice = Date('F d, Y', strtotime("+2 days"));
            $StudioName = $studio->name;
            $VoucherCode = $coupon_code;
            $SupportEmail = $studio->support_email;
            $TxnFailureReason = 'an unknown error';
            $TransactionAmount = '$'.$amount;
            $MonetizationName = $monetization_name;
            
            //$temp = New NotificationTemplates;
            //$data = $temp->findAllByAttributes(array('studio_id' => $studio_id, 'type' => $email_type));
            $content = "SELECT * FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND (language_id = " . $language_id . " OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND language_id = " . $language_id . "))";
            $data = Yii::app()->db->createCommand($content)->queryAll();
            if (count($data) > 0) {
                
            } else {
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }
            $temps = $data[0];

            $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
            if ($linked_emails->notification_from_email) {
                $StudioEmail = $send_from = $linked_emails->notification_from_email;
            } else {
                $model = new User;
                $admin = $model->findAllByAttributes(array('studio_id' => $studio_id, 'is_sdk' => 1, 'role_id' => 1));
                if (count($admin) > 0) {
                    $studio_mail = $admin[0]->email;
                } else {
                    $studio_mail = 'info@muvi.com';
                }
                $StudioEmail = $send_from = $studio_mail;
            }
            $EmailAddress = $StudioEmail;
            $subject = $temps['subject'];
            //$subject = htmlentities($subject);
            eval("\$subject = \"$subject\";");
            //$subject = htmlspecialchars_decode($subject);

            $content = (string) $temps["content"];
            $breaks = array("<br />", "<br>", "<br/>");
            $content = str_ireplace($breaks, "\r\n", $content);
            $content = htmlentities($content);
            eval("\$content = \"$content\";");
            $content = htmlspecialchars_decode($content);

            $params = array(
                'website_name' => $StudioName,
                'site_link' => $site_link,
                'logo' => $logo,
                'fb_link' => $fb_link,
                'twitter_link' => $twitter_link,
                'gplus_link' => $gplus_link,
                'name' => $FirstName,
                'mailcontent' => $content,
                'BundleName'=>$BundleName,
                'TransactionAmount'=>$TransactionAmount,
                'StudioName'=>$StudioName,
                'MonetizationName'=>$MonetizationName,
                'TxnFailureReason'=>$TxnFailureReason,
                'SupportEmail'=>$SupportEmail
            );

            if (trim($attached_file)) {
                $attachments = array(ROOT_DIR . 'docs/' . $attached_file);
                $to = $email;
                $from = $send_from;
                $subject = $subject;
            } else {
                $to = $email;
                $from = $send_from;
                $subject = $subject;
            }
            if (is_array($to)) {
                $to = rtrim(implode(',', $to), ',');
            } else {
                $to = $to;
            }

            $to = $email;
            $obj = new Controller();
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new', array('params' => $params), true);
            $returnVal = $obj->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml, $attachments,'','','',$StudioName);
            if (trim($attached_file)) {
                unlink(ROOT_DIR . 'docs/' . $attached_file);
            }
        }
    }

    /**
     * @method public isLivestream($studio_id) Check if live streaming is enabled for the studio or not
     * @author GDR<support@muvi.com>
     * @return bool true/false 
     */
    public function isLivestream($studio_id = '') {
        return true;
        /* if(!$studio_id ){
          $studio_id = Yii::app()->user->studio_id;
          }
          $studioContent = StudioContentType::model()->findByAttributes(array('studio_id' => $studio_id, 'content_types_id' => 4));
          if($studioContent){
          return TRUE;
          }else{
          return FALSE;
          } */
    }

    public function fileName($flname) {
        $fname = preg_replace('/[^a-zA-Z0-9_.]/', '-', $flname);
        return $fname;
    }

    public function allowedUploadContent($studio_id) {
        if (HOST_IP == '127.0.0.1') {
            return 1;
        }
        $std = new Studio();
        $studio = $std->findByPk($studio_id);

        $db = Yii::app()->db;
        $sql = "SELECT COUNT(*) AS total_streams FROM movie_streams where studio_id=" . $studio_id;
        $streams = $db->createCommand($sql)->queryRow();

        if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 1 && $studio->status == 1) {//For customer
            return 1;
        } else if (isset($studio->status) && isset($studio->is_default) && $studio->status == 1 && $studio->is_default == 1) {//For Demo 
            return 1;
        } else if (isset($streams['total_streams']) && ($streams['total_streams'] < 10)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @method public getContentFilters() Get the Content filters 
     * @author GDR<support@muvi.com>
     * @return array Description
     */
    function getContentFilters($content_type_id) {
        $returnArr = '';
        $contentFilterCls = new ContentFilter();
        $contentFilters = Yii::app()->db->createCommand('SELECT cf.*,ct.type_name,ct.type_value FROM content_filter cf, content_filter_types ct WHERE cf.filter_type_id = ct.id AND cf.content_type_id=' . $content_type_id . ' AND  cf.studio_id=' . $this->getStudioId())->queryAll();
        if ($contentFilters) {
            foreach ($contentFilters AS $key => $val) {
                $filterHeading[$val['filter_type_id']]['data'][] = $val;
                $filterHeading[$val['filter_type_id']]['type_name'] = $val['type_name'];
                $filterHeading[$val['filter_type_id']]['type_value'] = $val['type_value'];
            }
            //$returnArr['data'] = $arr;
            $returnArr = $filterHeading;
        }
        //echo "<pre>";print_r($returnArr);exit;
        return $returnArr;
    }

    //Added by Ratikanta for Blog   
    public function IsBlogAvailable() {
        $studio_id = self::getStudiosId();
        $extensions = StudioExtension::model()->findExtensions($studio_id);
        if (count($extensions) > 0) {
            foreach ($extensions as $extension) {
                if ($extension->extension_id == 1) {
                    return $extension->extension_id;
                }
            }
        }
        return 0;
    }

    public function IsFAQsAvailable() {
        $studio_id = self::getStudiosId();
        $extensions = StudioExtension::model()->findExtensions($studio_id);
        if (count($extensions) > 0) {
            foreach ($extensions as $extension) {
                if ($extension->extension_id == 2) {
                    return $extension->extension_id;
                }
            }
        }
        return 0;
    }
    
    public function IsTvguideAvailable()
    {
      $studio_id = self::getStudiosId();
      //get the tvguide extension id from extension name
      $extension= Extension::model()->findExtensionsByName();
      $extensions = StudioExtension::model()->findExtensions($studio_id);
      $ext_id=$extension[0]['id'];
     
      if (count($extensions) > 0) {
            foreach ($extensions as $extension) {
                if ($extension->extension_id ==$ext_id ) {
                    return $extension->extension_id;
                }
            }
        }
        return 0;
    }
    public function getStudioUserName($user_id) {
      $studio_id = self::getStudiosId();
        $user = new User();
        $full_name = '';
        $user = $user->findByAttributes(array('studio_id' => $studio_id, 'id' => $user_id));
        if (count($user) > 0) {
            $full_name = $user->first_name;
            if ($user->last_name)
                $full_name.= ' ' . $user->last_name;
        }
        return $full_name;
    }

    public function curPageURL() {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .=
                    $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public function formatPostPermalink($str, $post_id, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = $str;
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        $studio_id = self::getStudioId();
        $posts = Yii::app()->db->createCommand()
                ->select('id, permalink')
                ->from('blog_posts')
                ->where('studio_id = :studio_id AND permalink LIKE :permalink', array(':studio_id' => $studio_id, ':permalink' => '%' . $clean . '%'))
                ->order('permalink DESC')
                ->limit(1)
                ->queryAll();
        if (count($posts) > 0) {
            if ($posts[0]['id'] != $post_id) {
                $perm = $posts[0]['permalink'];
                $parts = explode($clean, $perm);
                $end = 1;
                if (count($parts) > 0) {
                    $end = (int) $parts[1];
                    $end++;
                }
                $clean.= $end;
            } else {
                $clean = $posts[0]['permalink'];
            }
        }
        return $clean;
    }

    public function getExtensionDetails($extension_id) {
        $studio_id = self::getStudiosId();
        $extensions = StudioExtension::model()->findExtensions($studio_id);
        if (count($extensions) > 0) {
            foreach ($extensions as $extension) {
                if ($extension->extension_id == $extension_id) {
                    return $extension;
                }
            }
        }
        return '';
    }

    public function getuseremail($id) {
        $data = SdkUser :: model()->findByPk($id);
        return $data['email'];
    }

    /**
     * @method public getSocialAuths(int $studio_id) Get the social authentication details for the studio
     * @return array Array of authentication details
     * @author GDR<support@muvi.com>
     */
    function getSocialAuths($studio_id) {
        if (!$studio_id) {
            $studio_id = $this->getStudiosId();
        }
        $data = SocialLoginInfos::model()->find('studio_id=:studio_id AND status=:status', array(':studio_id' => $studio_id, ':status' => 1));
        if ($data) {
            return $data->attributes;
        } else {
            return '';
        }
    }

    function getSectionBannerDimension($section_id) {
        $studio_id = self::getStudiosId();
        if (!$this->studioData) {
            $studio = new Studio();
            $studio = $studio->findByPk($studio_id);
            $this->studioData = $studio;
        } else {
            $studio = $this->studioData;
        }
        $height = 0;
        $width = 0;
        $section_banner = new StudioBannerStyle();
        $banner_style = $section_banner->findByAttributes(array('studio_id'=>$studio_id));
       
        if (empty($banner_style)){
        if ($section_id > 0) {
            if ($studio->banner_width > 0 && $studio->banner_height > 0) {
                $width = $studio->banner_width;
                $height = $studio->banner_height;
            } else {
                $section = new BannerSection;
                $section = $section->findByPk($section_id);
                $width = $section->banner_width;
                $height = $section->banner_height;
            }
        }
        }else{
             $width = $banner_style->h_banner_dimension;
             $height = $banner_style->v_banner_dimension;
        }
        $resp = array('width' => $width, 'height' => $height);
        return $resp;
    }
	function getThumbBannerDimension($section_id) {
        $default_height = 100;
        $default_width = 100;
        $newDimension=self::getSectionBannerDimension($section_id);
        $width=$newDimension['width'];
        $height=$newDimension['height'];
        if($width > $height){
            $final_height=$default_height;
            $final_width=round(($final_height/$height)*$width);
        }else{
            $final_width=$default_width;
            $final_height=round(($final_width/$width)*$height);
        }
        $resp = array('width' => $final_width, 'height' => $final_height);
        return $resp;
    }

    function couponCodeIsValid($coupon_code,$user_id = 0,$studio_id=0,$currencyId = 0){
		$studio_id = ($studio_id == 0) ? Yii::app()->common->getStudiosId() : $studio_id;
        
        $todayDate = date("Y-m-d");
        $command = Yii::app()->db->createCommand()
            ->select('discount_type, discount_amount,used_by,coupon_type,user_can_use,id,restrict_usage')
            ->from('coupon')
				->where('studio_id=:studio_id AND coupon_code=:coupon_code AND valid_from <=:valid_from AND valid_until >=:valid_until AND status=1', array(':studio_id' => $studio_id, ':coupon_code' => $coupon_code, ':valid_from' => $todayDate, ':valid_until' => $todayDate));
		$data = $command->queryAll();
        
        $command1 = Yii::app()->db->createCommand()
				->select('*')
				->from('ppv_subscriptions')
				->where('studio_id=:studio_id AND user_id=:user_id AND coupon_code=:coupon_code', array(':user_id' => $user_id, ':studio_id' => $studio_id, ':coupon_code' => $coupon_code), array('order' => 'created_date DESC, movie_id, season_id, episode_id'));
		$ppv_coupon_data = $command1->queryAll();
        
        $auth = "";
        if(!empty($ppv_coupon_data) && count($ppv_coupon_data)>0){
            if($data[0]['coupon_type'] == 1){
                $no_use = count($ppv_coupon_data);
                $restrict = $data[0]['restrict_usage'];
                
                if($restrict == 0){
                   $auth = "allow";
                }
                elseif($no_use >= $restrict){
                   $auth = "restricted"; 
                }else{
                    $auth = "allow";
                }
            }
        }
        
        $discountDetails = array();
        $sendData = 0;
        if(!empty($data) && count($data)>0){
            if($data[0]['coupon_type'] == 1){
                if($data[0]['user_can_use'] == 0){
                    $userIdArray = array();
                   $userIdArray = explode(",",$data[0]['used_by']);
                    if(in_array($user_id,$userIdArray)){
                        return 1;
                    } else{
                        $sendData++;
                    }
                }else if($auth != "" && $auth == "restricted") {
                    return 1;
                }else{
                    $sendData++;
                }
            }else{
                if($data[0]['used_by'] == 0){
                    $sendData++;
                }else{
                    return 1;
                }
            }
            if($sendData != 0){
                if($data[0]['discount_amount'] == '0.00'){
                    $couponCurrency  = CouponCurrency::model()->findByAttributes(array('coupon_id' => $data[0]['id'],'currency_id'=>$currencyId));
                    if($couponCurrency){
                        $discountDetails['discount_type'] = $data[0]['discount_type'];
                        $discountDetails['discount_amount'] = $couponCurrency['discount_amount'];
                    }else{
                        return 0;
                    }
                } else{
                    $discountDetails['discount_type'] = $data[0]['discount_type'];
                    $discountDetails['discount_amount'] = $data[0]['discount_amount'];
                }
                return $discountDetails;
            }
        }else{
            return 0;
        }
    }
    
    function voucherCodeValid($vcode, $user_id = 0, $content_id, $studio_id = 0) {
		if ($studio_id == 0) {
			$studio_id = Yii::app()->common->getStudiosId();
		}
		$todayDate = date("Y-m-d");
		$todayDateTime = date("Y-m-d H:i:s");

		$command = Yii::app()->db->createCommand()
				->select('*')
				->from('voucher')
				->where('studio_id=:studio_id AND status=1 AND voucher_code=:voucher_code', array(':studio_id' => $studio_id, ':voucher_code' => $vcode));
		$vdata = $command->queryRow();

		$return = 2; //Invalid voucher
		if (!empty($vdata)) {
			$content_ids = explode(":", $content_id);
			$movie_id = $content_ids[0];
			$resArr = self::getVoucherCondition($content_ids);
			$purchase_type = @$resArr['purchase_type'];
			$vcond = @$resArr['vcond'];
			$vcondarr = @$resArr['vcondarr'];

			$content_types_id = Film::model()->findByPk($movie_id)->content_types_id;
			$freearg['studio_id'] = $studio_id;
			$freearg['movie_id'] = $movie_id;
			$freearg['season_id'] = @$content_ids[1];
			$freearg['episode_id'] = @$content_ids[2];
			$freearg['content_types_id'] = $content_types_id;

			$monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
			$isFreeContent = 0;
			if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 8)) {
				$isFreeContent = self::isFreeContent($freearg);
			}

			$return = 4;
			if (!intval($isFreeContent)) {
				$condArr = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id, ':created_date' => $todayDateTime);
				$allcondarr = array_merge($condArr, $vcondarr);
				$is_voucher_subsribed = array();
				
				if (intval($user_id)) {
				$command = Yii::app()->db->createCommand()
						->select('*')
						->from('ppv_subscriptions')
						->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_voucher=1 AND created_date <= :created_date ' . $vcond, $allcondarr, array('order' => 'movie_id, season_id, episode_id, created_date DESC'));
				$is_voucher_subsribed = $command->queryRow();
				}
				if (!empty($is_voucher_subsribed)) {

					$stream_id = (isset($content_ids[2]) && intval($content_ids[2])) ? $content_ids[2] : 0;
					if ($content_types_id != 3) {
						if (intval($stream_id == 0)) {
							$stream_id = self::getStreamId($movie_id, '', '', $studio_id);
						}
					}

					//retutn error: Already subscribed that content
					$isPlaybackAccess = self:: isPlaybackAccessOnContent($is_voucher_subsribed, $content_types_id, $stream_id, $purchase_type);

					$return = 5;
					if (!$isPlaybackAccess['isContinue']) {
						$return = self::validateVoucherPlaybackAccess($vdata, $content_id, $vcode, $user_id, $studio_id);
						if (intval($return) && $return == 1) {
							if (isset($isPlaybackAccess['expiredAccessability']) && $isPlaybackAccess['expiredAccessability'] == 'access_period') {
								$return = 6;
							} else if (isset($isPlaybackAccess['expiredAccessability']) && $isPlaybackAccess['expiredAccessability'] == 'watch_period') {
								$return = 7;
							} else if (isset($isPlaybackAccess['expiredAccessability']) && $isPlaybackAccess['expiredAccessability'] == 'maximum') {
								$return = 8;
							}
						}
					}
				} else {
					$return = self::validateVoucherPlaybackAccess($vdata, $content_id, $vcode, $user_id, $studio_id);
				}
			}
		}
		return $return;
	}

	function getVoucherCondition($content_ids) {
		$resArr = array();
		
		if (isset($content_ids[0]) && intval($content_ids[0]) && isset($content_ids[1]) && intval($content_ids[1]) && isset($content_ids[2]) && intval($content_ids[2])) {//Episode Use case
			$resArr['purchase_type'] = 'episode';
			$resArr['vcond'] = 'AND (season_id=:season_id OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)';
			$resArr['vcondarr'] = array(':season_id' => $content_ids[1], ':episode_id' => $content_ids[2]);
		} else if (isset($content_ids[0]) && intval($content_ids[0]) && isset($content_ids[1]) && intval($content_ids[1])) {////Season Use case
			$resArr['purchase_type'] = 'season';
			$resArr['vcond'] = 'AND (season_id=:season_id OR season_id=0) AND (episode_id=0)';
			$resArr['vcondarr'] = array(':season_id' => $content_ids[1]);
		} else {
			$resArr['purchase_type'] = '';
			$resArr['vcond'] = " AND (season_id =0) AND (episode_id=0)";
			$resArr['vcondarr'] = array();
		}
		
		return $resArr;
	}
	
    function validateVoucherPlaybackAccess($voucher_data, $content_id, $vcode, $user_id = 0, $studio_id = 0){
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $return = 2;
        $todayDate = date("Y-m-d");
            $content_ids = explode(":", $content_id);
            $movie_id  = $content_ids[0];
        if ((strtotime($todayDate) >= strtotime($voucher_data['valid_from'])) && (strtotime($todayDate) <= strtotime($voucher_data['valid_until']))) {
            $use_cond = "";
            if(intval($user_id) && $voucher_data['voucher_type'] == 1){
                $use_cond = "AND user_id=".$user_id;
            }
            if ($voucher_data['is_allcontent'] == 1) {//Voucher for all content
                $command = Yii::app()->db->createCommand()
                    ->select('COUNT(*) AS tm_use')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id '.$use_cond.' AND ppv_plan_id=:ppv_plan_id AND is_voucher=1', array(':studio_id' => $studio_id, ':ppv_plan_id' => $voucher_data['id']));
				$valldata = $command->queryRow();
				
                if ($valldata['tm_use'] >= $voucher_data['restrict_usage']) {
                            //retutn error: Already used
                    $return = 3;
                }else{
                    $return = 1;
                        }
                    } else {//Voucher for specific content
                $command = Yii::app()->db->createCommand()
					->select('id')
					->from('voucher')
					->where('studio_id=:studio_id AND voucher_code=:voucher_code AND FIND_IN_SET (:content,content)', array(':studio_id' => $studio_id, ':voucher_code' => $vcode, ':content' => $content_id));
				$vspecificdata = $command->queryRow();
				
                        if (!empty($vspecificdata)) {
					$resArr = self::getVoucherCondition($content_ids);
					$vcond = @$resArr['vcond'];
					$vcondarr = @$resArr['vcondarr'];
					
					$condArr = array(':studio_id' => $studio_id, ':movie_id' => $movie_id, ':ppv_plan_id' => $voucher_data['id']);
					$allcondarr = array_merge($condArr, $vcondarr);
					
                    $command = Yii::app()->db->createCommand()
						->select('count(id) as tm_use')
						->from('ppv_subscriptions')
						->where('studio_id=:studio_id '.$use_cond.' AND ppv_plan_id=:ppv_plan_id AND is_voucher=1 ',array(':studio_id' => $studio_id, ':ppv_plan_id' => $voucher_data['id']));
					$vsdata2 = $command->queryRow();
                            
                    if ($vsdata2['tm_use'] >= $voucher_data['restrict_usage']) {
                                //retutn error: Already used
                        $return = 3;
                    }else{
                        $return = 1;
                            }
                        }
                    }
                }
        return $return;
    }

    public function getEpisodename($episode_id) {
        $episodeDetails = new movieStreams();
        $episodeDetails = movieStreams::model()->findByPk($episode_id);
        if (isset($episodeDetails->episode_title)) {
            return $episodeDetails->episode_title;
        }
        return '';
    }

    function getCouponDiscount($coupon_code = NULL, $amount=0,$studio_id =0,$user_id = 0,$couponCurrencyId = 0){
        $data = array();
        $data["couponCode"] = '';
        $discount_amount ='';

        if(isset($coupon_code) && trim($coupon_code)){
            $couponDetails = Yii::app()->common->couponCodeIsValid($coupon_code,$user_id,$studio_id,$couponCurrencyId);
            if($couponDetails != 0 && $couponDetails != 1){
                $data["couponCode"] = $coupon_code;
                if($couponDetails['discount_type'] == 1){
                    $discount_amount = ($amount*$couponDetails['discount_amount'])/100;
                    $amount = $amount-$discount_amount;
                }else if($couponDetails['discount_type'] == 0){
                    $discount_amount = $couponDetails['discount_amount'];
                    $amount = $amount - $discount_amount;
                    if($amount < 0){
                        $amount = 0;
                    }
                }
            }
        }
		
		$amount = number_format((float) ($amount), 2, '.', '');
		$discount_amount = number_format((float) ($discount_amount), 2, '.', '');
        $data["amount"] = $amount;
        $data["coupon_amount"] = $discount_amount;
        $data["discount_type"] = $couponDetails['discount_type'];
        return $data;
    }

    function getvideoResolutionForTheVideo($resolution, $value) {
        $videoRes = array();
        foreach ($resolution as $key => $val) {
            if ($val <= $value) {
                $videoRes[] = $val;
            }
        }
        return $videoRes;
    }

    function get_video_resolution($video, $ffmpeg, $ext) {
        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $command = $ffmpeg . " -i " . $video . " 2>&1 | grep Stream | grep -oP ', \K[0-9]+x[0-9]+'";
        } else {
            $command = "sudo " . $ffmpeg . " -i " . $video . " 2>&1 | grep Stream | grep -oP ', \K[0-9]+x[0-9]+'";
        }
        $res = array();
        $width = 0;
        $height = 0;
        $output = shell_exec($command);
        $output = explode("x", $output);
        $width = $output[0] ? $output[0] : null;
        $height = $output[1] ? $output[1] : null;
        return array('width' => $width, 'height' => $height);
    }

    function getPublicBucketID() {
        return 10;
    }

    function getStorageSize($movie_stream_id, $studio_id, $type) {
        //require_once 's3bucket/aws-autoloader.php';
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        if ($type == 'movie') {
            $prefix = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_stream_id . '/';
            $ms = movieStreams::model()->findByPk($movie_stream_id);
            $video_resolution = $ms->video_resolution;
            $full_movie_name = $ms->full_movie;
            $movie_name = explode('.', $full_movie_name);
            $original_name = $movie_name[0] . '_original';
        } else {
            $prefix = $signedBucketPath . 'uploads/trailers/' . $movie_stream_id . '/';
            $mt = movieTrailer::model()->findByPk($movie_stream_id);
            $full_movie_name = $mt->trailer_file_name;
            $video_resolution = $mt->video_resolution;
            $movie_name = explode('.', $full_movie_name);
            $original_name = $movie_name[0] . '_original';
        }
        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $response = $s3->getListObjectsIterator(array(
            'Bucket' => $bucketInfo['bucket_name'],
            'Prefix' => $prefix
        ));

        $fullmovie_path = 'http://' . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/" . $prefix . $full_movie_name;
        $vid_res = Yii::app()->controller->getvideoResolution($video_resolution, $fullmovie_path);
        $res_size = array();


        $totalSize = 0;
        $original_file_size = 0;
        $converted_file_size = 0;
        $original_file_count = 0;
        $converted_file_count = 0;
        foreach ($response as $object) {
            $key = explode('/', $object['Key']);
            if ($type == 'movie') {
                $s3_name = $key[4];
            } else {
                $s3_name = $key[3];
            }
            $s3_name = explode('.', $s3_name);
            if ($original_name == $s3_name[0]) {
                $original_file_count++;
                $original_file_size += $object['Size'];
            } else {
                $converted_file_size += $object['Size'];
                $converted_file_count++;
            }
            $totalSize += $object['Size'];

            foreach ($vid_res AS $key => $value) {
                $url = 'http://' . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/" . $object['Key'];
                if ($url == $value) {
                    $res_size[$key] = round($object['Size'] / 1024, 2);
                }
            }
        }
        $resArray = array(
            'total_file_size' => ($totalSize / 1024 / 1024),
            'original_file_size' => ($original_file_size / 1024 / 1024),
            'converted_file_size' => ($converted_file_size / 1024 / 1024),
            'original_file_count' => $original_file_count,
            'converted_file_count' => $converted_file_count
        );
        $resArray['res_size'] = $res_size;
        return $resArray;
    }

    function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/Trident/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Edge/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Edge';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
                ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

    function connectToAwsS3($studio_id = '', $region = '') {
        $studio = Studio::model()->getStudioBucketId($studio_id);
        $s3bucket_id = $studio['studio_s3bucket_id'];
        if ($s3bucket_id == 0) {
            if ($region) {
                $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                            'region' => $region
                ));
            } else {
                $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret
                ));
            }
        } else {
            $studioS3buckets = StudioS3buckets::model()->findByPk($s3bucket_id);
            $s3 = S3Client::factory(array(
                        'key' => $studioS3buckets->access_key,
                        'secret' => $studioS3buckets->secret_key
            ));
        }
        return $s3;
    }

    function getS3Details($studio_id = '') {
        $studio = Studio::model()->getStudioBucketId($studio_id);
        $s3bucket_id = $studio['studio_s3bucket_id'];
        if ($s3bucket_id == 0) {
            $ret = array(
                'key' => Yii::app()->params->s3_key,
                'secret' => Yii::app()->params->s3_secret
            );
        } else {
            $studioS3buckets = StudioS3buckets::model()->findByPk($s3bucket_id);
            $ret = array(
                'key' => $studioS3buckets->access_key,
                'secret' => $studioS3buckets->secret_key
            );
        }
        return $ret;
    }

    function connectToAwsCloudFront($studio_id = '') {
        $studio = Studio::model()->getStudioBucketId($studio_id);
        $s3bucket_id = $studio['studio_s3bucket_id'];
        if ($s3bucket_id == 0) {
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => 'pk-APKAJYIDWFG3D6CNOYVA.pem',
                        'key_pair_id' => 'APKAJYIDWFG3D6CNOYVA',
            ));
        } else {
            $studioS3buckets = StudioS3buckets::model()->findByPk($s3bucket_id);
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => $_SERVER['DOCUMENT_ROOT'] . $subFolder . "/pem/" . $studio['theme'] . "/" . $studioS3buckets->pem_file_path,
                        'key_pair_id' => $studioS3buckets->key_pair_id
            ));
        }
        return $cloudFront;
    }

    function getFolderPath($newUser = '', $studio_id = '') {
        $arr['signedFolderPath'] = '';
        $arr['unsignedFolderPath'] = '';
        $arr['unsignedFolderPathForVideo'] = '';
        if ($studio_id == '') {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if(($studio_id == $this->getStudioId()) && $this->studioData){
			$studio = $this->studioData->attributes;
		}else{
			$studio = Studio::model()->getStudioBucketData($studio_id);
		}
        $newUser = $studio['new_cdn_users'];
        $studioS3BucketId = $studio['studio_s3bucket_id'];
        if($newUser == 1){
            $arr['signedFolderPath'] = $studio_id."/EncodedVideo/";
            $arr['unsignedFolderPath'] = $studio_id."/public/";
            $arr['unsignedFolderPathForVideo'] = $studio_id."/RawVideo/";
        }
        return $arr;
    }

    function getPosterCloudFrontPath($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        if ($bucketInfo['unsigned_cloudfront_url'] != '') {
            $url = CDN_HTTP . $bucketInfo['unsigned_cloudfront_url'] . '/' . $bucketInfo['unsignedFolderPath'] . 'public';
        } else {
            $url = POSTER_URL;
        }
        return $url;
    }

    function getImageGalleryCloudFrontPath($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        if ($bucketInfo['unsigned_cloudfront_url'] != '') {
            $url = CDN_HTTP . $bucketInfo['unsigned_cloudfront_url'] . '/' . $bucketInfo['unsignedFolderPath'];
        } else {
            $url = CDN_HTTP . POSTER_HOST . "/";
        }
        return $url;
    }

    function getVideoGalleryCloudFrontPath($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $studio = Studio::model()->getStudioBucketData($studio_id);
        $newUser = $studio['new_cdn_users'];
        if($newUser)
        {
            $url = 'http://'.$bucketInfo['videoRemoteUrl'].'/'.$bucketInfo['unsignedFolderPathForVideo'].'videogallery/'; 
        }
        else
        {
            $url = 'http://'.$bucketInfo['videoRemoteUrl'].'/'.$bucketInfo['unsignedFolderPathForVideo'].'videogallery/'.$studio_id.'/';   
        }
        return $url;
    }
	
    function getAudioGalleryCloudFrontPath($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        if($this->studioData){
			$studio = $this->studioData->attributes;
		}else{
        $studio = Studio::model()->getStudioBucketData($studio_id);
		}
        $newUser = $studio['new_cdn_users'];
        if ($newUser) {
            $url = 'http://' . $bucketInfo['videoRemoteUrl'] . '/' . $bucketInfo['unsignedFolderPathForVideo'] . 'audiogallery/';
        } else {
            $url = 'http://' . $bucketInfo['videoRemoteUrl'] . '/' . $bucketInfo['unsignedFolderPathForVideo'] . 'audiogallery/' . $studio_id . '/';
        }
        return $url;
    }

	function getFileGalleryCloudFrontPath($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $studio = Studio::model()->getStudioBucketData($studio_id);
        $newUser = $studio['new_cdn_users'];
        if($newUser)
        {
            $url = 'http://'.$bucketInfo['videoRemoteUrl'].'/'.$bucketInfo['unsignedFolderPathForVideo'].'filegallery/'; 
        }
        else
        {
            $url = 'http://'.$bucketInfo['videoRemoteUrl'].'/'.$bucketInfo['unsignedFolderPathForVideo'].'filegallery/'.$studio_id.'/';   
        }
        return $url;
    }
	
    function getBucketInfoForPoster($studio_id = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        if (($studio_id == $this->getStudioId()) && $this->studioData) {
            $studio = $this->studioData->attributes;
        } else {
            $studio = Studio::model()->getStudioBucketData($studio_id);
	}
        $s3bucket_id = $studio['s3bucket_id'];
        $studio_s3bucket_id = $studio['studio_s3bucket_id'];
        $newUser = $studio['new_cdn_users'];
        $arr = '';
        if ($studio_s3bucket_id != 0) {
            $studioS3buckets = StudioS3buckets::model()->findByPk($studio_s3bucket_id);
            if ($studioS3buckets) {
                $arr['bucket_name'] = $studioS3buckets->bucket_name;
            }
        } else {
            if ($newUser == 1) {
                $s3bucket = S3bucket::model()->findByPk($s3bucket_id);
                if ($s3bucket) {
                    $arr['bucket_name'] = $s3bucket->bucket_name;
                }
            } else {
                $arr['bucket_name'] = Yii::app()->params->s3_bucketname;
            }
        }
        return $arr;
    }

    function getLogoFavPath($studio_id, $checkFile = 'logo',$device_type='',$invoice_logo='') {
        //$std = new Studio();
        //$studio = $std->getStudioDataForLogo($studio_id);
		
		if (($studio_id == $this->getStudioId()) && $this->studioData) {
			$studio = $this->studioData->attributes;
		} else {
			$studios = Studio::model()->findByPk($studio_id);
			$studio = $studios->attributes;
		}
        if($device_type=='app'){
            $studiosdomain=  Yii::app()->db->createCommand("select domain,theme from studios where id=".$studio_id)->queryRow();   
        }
        $bucketInfoForLogo12 = Yii::app()->common->getBucketInfoForPoster($studio_id);
        $logoCDNUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        $folderPath = Yii::app()->common->getFolderPath($studio['new_cdn_users'], $studio_id);
        $unsignedFolderPath = $folderPath['unsignedFolderPath'];
        $bucketName = $bucketInfoForLogo12['bucket_name'];
        $s3_path = $unsignedFolderPath . 'public/' . $studio['theme'];
        $client = Yii::app()->common->connectToAwsS3($studio_id);
        $path = '';
		
         if (strpos($studiosdomain['domain'], 'idogic.com') !== false ){
             $pre = 'http://www.'.$studiosdomain['domain']; 
         }else if( strpos($studiosdomain['domain'], 'muvi.com') !== false) {
                $pre = 'https://www.'.$studiosdomain['domain'];
            }else if(strpos($studiosdomain['domain'], 'sunil.com') !== false ){
                $pre='http://'.$studiosdomain['domain'];
            }
            else if(strpos($studiosdomain['domain'], 'edocent.com') !== false ){
                $pre='http://'.$studiosdomain['domain'];
            }
            else{
                $pre = 'https://www.'.$studiosdomain['domain'];
            }
        if ($checkFile == 'logo') {
            $logo_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $studio['theme'] . '/' . $studio['logo_file'];
            $s3FullPath = $s3_path . '/logos/' . $studio['logo_file'];
            $s3FullPathForView = $unsignedFolderPathForView . $studio['theme'] . '/logos/' . $studio['logo_file'];
            $info = $client->doesObjectExist($bucketName, $s3FullPath);
            if($device_type!='app'){
            if ($studio['logo_file'] == '') {
                $path = Yii::app()->getbaseUrl(true) . '/themes/' . $studio['theme'] . '/images/logo.png';
            } else if ($info) {
                $path = $logoCDNUrl . '/' . $s3FullPathForView;
            } else if (file_exists($logo_path)) {
                $path = Yii::app()->getbaseUrl(true) . '/images/logos/' . $studio['theme'] . '/' . $studio['logo_file'];
            } else {
                $path = Yii::app()->getbaseUrl(true) . '/themes/' . $studio['theme'] . '/images/logo.png';
            }
            }
            else{
               $path = $pre. '/themes/' . $studiosdomain['theme'] . '/images/logo.png';    
            }
        } else if ($checkFile == 'favicon') {
            $fav_icon = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $studio['theme'] . '/' . $studio['favicon_name'];
            $s3FullPath = $s3_path . '/favicon/' . $studio['favicon_name'];
            $s3FullPathForView = $unsignedFolderPathForView . $studio['theme'] . '/favicon/' . $studio['favicon_name'];
            $info = $client->doesObjectExist($bucketName, $s3FullPath);

            if ($studio['favicon_name'] == '') {
                $path = Yii::app()->getbaseUrl(true) . '/icon.png';
            } else if ($info) {
                $path = $logoCDNUrl . '/' . $s3FullPathForView;
            } else if (file_exists($fav_icon)) {
                $path = Yii::app()->getbaseUrl(true) . '/images/logos/' . $studio['theme'] . '/' . $studio['favicon_name'];
            } else {
                $path = Yii::app()->getbaseUrl(true) . '/icon.png';
            }
        }
		
         if(isset($invoice_logo) && $invoice_logo == 1){
            $invoice_logo_path = Yii::app()->common->getInvoiceLogoPath($studio['id']);
             if($studio['invoice_logo_file'] != ''){ 
                $path = $invoice_logo_path;
             }else if($info){
                $path = $logoCDNUrl . '/' . $s3FullPathForView;
            } else if (file_exists($logo_path)) {
                $path = Yii::app()->getbaseUrl(true) . '/images/logos/' . $studio['theme'] . '/' . $studio['logo_file'];
            } else {
                $path = Yii::app()->getbaseUrl(true) . '/themes/' . $studio['theme'] . '/images/logo.png';
            }

        }
        return $path;
    }
	
    function getInvoiceLogoPath($studio_id, $checkFile = 'invoicelogo',$device_type='') {
        if (($studio_id == $this->getStudioId()) && $this->studioData) {
            $studio = $this->studioData->attributes;
        } else {
            $studio = Studio::model()->findByPk($studio_id);
        }
         if($device_type=='app'){
          $studiosdomain=  Yii::app()->db->createCommand("select domain,theme from studios where id=".$studio_id)->queryRow();   
         }
        $bucketInfoForLogo12 = Yii::app()->common->getBucketInfoForPoster($studio_id);
        $logoCDNUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        $folderPath = Yii::app()->common->getFolderPath($studio['new_cdn_users'], $studio_id);
        $unsignedFolderPath = $folderPath['unsignedFolderPath'];
        $bucketName = $bucketInfoForLogo12['bucket_name'];
        $s3_path = $unsignedFolderPath . 'public/' . $studio['theme'];
        $client = Yii::app()->common->connectToAwsS3($studio_id);
        $path = '';
        if (strpos($studiosdomain['domain'], 'idogic.com') !== false) {
            $pre = 'http://www.' . $studiosdomain['domain'];
        } else if (strpos($studiosdomain['domain'], 'muvi.com') !== false) {
            $pre = 'https://www.' . $studiosdomain['domain'];
        } else if (strpos($studiosdomain['domain'], 'sunil.com') !== false) {
            $pre = 'http://' . $studiosdomain['domain'];
        } else if (strpos($studiosdomain['domain'], 'edocent.com') !== false) {
            $pre = 'http://' . $studiosdomain['domain'];
        } else {
            $pre = 'https://www.' . $studiosdomain['domain'];
        }
        if ($checkFile == 'invoicelogo') {
            $logo_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $studio['theme'] . '/' . $studio['invoice_logo_file'];
            $s3FullPath = $s3_path . '/logos/' . $studio['invoice_logo_file'];
            $s3FullPathForView = $unsignedFolderPathForView . $studio['theme'] . '/logos/' . $studio['invoice_logo_file'];
            $info = $client->doesObjectExist($bucketName, $s3FullPath);
            if($device_type!='app'){
                if ($studio['invoice_logo_file'] == '') {
                    $path = Yii::app()->getbaseUrl(true) . '/themes/' . $studio['theme'] . '/images/logo.png';
                } else if ($info) {
                    $path = $logoCDNUrl . '/' . $s3FullPathForView;
                } else if (file_exists($logo_path)) {
                    $path = Yii::app()->getbaseUrl(true) . '/images/logos/' . $studio['theme'] . '/' . $studio['invoice_logo_file'];
                } else {
                    $path = Yii::app()->getbaseUrl(true) . '/themes/' . $studio['theme'] . '/images/logo.png';
                }
            }
            else{
               $path = $pre. '/themes/' . $studiosdomain['theme'] . '/images/logo.png';    
            }
        }
        return $path;
    }
    function encode_to_html($content) {
        return htmlspecialchars(trim($content));
    }

    function htmlchars_encode_to_html($content) {
        return htmlspecialchars_decode(stripslashes(trim($content)));
    }

    function strip_blog_tags($content) {
        return strip_tags(htmlspecialchars_decode(stripslashes(trim($content))), '<img><iframe><p>'); 
    }
    
    function strip_html_tags($content) {
        return strip_tags(htmlspecialchars_decode(stripslashes(trim($content))));
    }

    public function hasPermission($module, $action) {
        $data = User::model()->findByPk(Yii::app()->user->id);
        if ($data->permission_id == '') {
            return true;
        } else {
            $db = Yii::app()->db;

            $sql = "select module,action from permission where id IN(" . $data->permission_id . ")";
            $query_permission = $db->createCommand($sql)->queryAll();
            if ($this->searchForId('all', $query_permission)) {
                return true;
            } else if ($this->searchForId($module, $query_permission)) {
                return true;
            } else {
                return false;
            }
        }
    }

    function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['module'] == $id) {
                return true;
            }
        }
        return false;
    }
    public function hasViewPermission($module, $action) {
        $data = User::model()->findByPk(Yii::app()->user->id);
        if($data->permission_id!=''){
            $db = Yii::app()->db;

            $sql = "select module,action from permission where id IN(".$data->permission_id.")";
            $query_permission = $db->createCommand($sql)->queryAll();
            if ($this->searchForId($module,$query_permission))  {
                return true;
            }
            else {
                return false;
            }
            
        }else{
            return false;             
        }    
    }    
    public function showleftslide() {
        $ret = '';
        $studio_id = Yii::app()->common->getStudiosId();
        $ip_address = Yii::app()->getRequest()->getUserHostAddress();
        $data = Previewtemplatehistory::model()->findByAttributes(array('preview_ip' => $ip_address, 'studio_id' => $studio_id, 'is_preview' => 1), array('order' => 'id DESC'));
        if ($data && isset(Yii::app()->request->cookies['in_preview_theme']) && Yii::app()->request->cookies['in_preview_theme'] != '') {
            if($data->template_status == 1) // if same template and color
                {
                
                    $ret ='<link rel="stylesheet" type="text/css" href="'.Yii::app()->getbaseUrl(true).'/common/css/preview.css" />
                       <div id="slideout">
            	 <img src="'.Yii::app()->getbaseUrl(true).'/common/images/previewmode.png" alt="Feedback" />
            		<div id="slideout_inner">

                                <br />
                                
                                    <button  class="btn btn-primary revert"> Revert </button>
                                <input type="button" value="Apply" class="btn btn-primary apply">
                                  
                                </div>
                            </div>
                           <div id="confirmApply" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                <h4 class="modal-title" id="myModalLabel">Apply Template</h4> 
                                    </div>
                                            <div class="modal-body" style="color:#333;">   
                                               <p><center>You are currently using this template.</center></p>
                                            </div>              
                                   <div class="modal-footer">
                                        <button data-dismiss="modal" class="btn btn-default" type="button">Continue</button>
                                    </div>
                                </div>
                            </div>
                        </div>';
                
                }
                else {
                    
                    $ret ='<link rel="stylesheet" type="text/css" href="'.Yii::app()->getbaseUrl(true).'/common/css/preview.css" />
                       <div id="slideout">
            	 <img src="'.Yii::app()->getbaseUrl(true).'/common/images/previewmode.png" alt="Feedback" />
            		<div id="slideout_inner">

                                <br />
                                
                                    <button  class="btn btn-primary revert"> Revert </button>
                                <input type="button" value="Apply" class="btn btn-primary apply">
                                  
            		</div>
            	</div>
                <div id="confirmApply" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                    <h4 class="modal-title" id="myModalLabel">Apply Template</h4> 
                        </div>
                                <div class="modal-body" style="color:#333;">   
                                    <div class="change_template" style="text-align: center;"></div>
                                    <div class="loader" id="apply_draft_loader" style="display:none;"></div>
                            <p>Changing template will remove any current template specific data such as banner and featured content on home page.</p><br />
                            <p><strong>Do you want to continue?</strong></p>
                        </div>              
                       <div class="modal-footer">
                            <button id="apply_confirm" class="btn btn-primary applyconfirm" type="button">Yes</button>
                            <button data-dismiss="modal" class="btn btn-default cancelapply" type="button">No</button>
                        </div>
                    </div>
                </div>
            </div>';
                    
                }
            
        
            return $ret;
  
        }
    }

    function bytes2English($filesize) {

        return number_format($filesize / 1048576, 4) . " MB";
    }

    public function get_videoproperties($output, $ext) {


        $res = array();
        if ($ext == 'mp4' || $ext == 'mov' || $ext == '3gp') {
            $regex_video = "/Video: ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([0-9.]* [a-zA-Z]*)/";
            $regex_audio = "/Audio: ([^,]*), ([^,]*), ([a-zA-Z]*), ([a-zA-Z]*), ([0-9.]* [a-zA-Z\/]*)/";
        } elseif ($ext == 'flv' || $ext == 'm4v' || $ext == 'avi') {
            $regex_audio = "/Audio: ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([0-9.]* [a-zA-Z\/]*)/";
            $regex_video = "/Video: ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*)/";
        } elseif ($ext == 'vob') {
            $regex_audio = "/Audio: ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([0-9.]* [a-zA-Z\/]*)/";
            $regex_video = "/Video: ([^,]*), ([^,]*), ([^,]*), [a-zA-Z\/.]*([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*)/";
        } elseif ($ext == 'mpg') {
            $regex_audio = "/Audio: ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([0-9.]* [a-zA-Z\/]*)/";
            $regex_video = "/Video: ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([^,]*), ([0-9.]* [a-zA-Z]*)/";
        }

        if ($ext == 'm4v') {
            if (preg_match($regex_video, $output, $regs)) {
                $regex_sizes12 = "/([0-9]{1,4})x([0-9]{1,4})/";
                //$res['video_codec'] = $regs [1] ? $regs [1] : null;
                if (preg_match($regex_sizes12, $regs4[4], $regs12)) {
                    $res['resolution'] = $regs12 [0] ? $regs12 [0] : null;
                }
                $res['video_bit_rate'] = $regs [5] ? $regs [5] : null;
                $res['frame_rate'] = $regs [6] ? $regs [6] : null;
            }
        } else {
            if (preg_match($regex_video, $output, $regs)) {
                $regex_sizes12 = "/([0-9]{1,4})x([0-9]{1,4})/";
                //$res['video_codec'] = $regs [1] ? $regs [1] : null;
                if (preg_match($regex_sizes12, $regs [3], $regs12)) {
                    $res['resolution'] = $regs12 [0] ? $regs12 [0] : null;
                }
                $res['video_bit_rate'] = $regs [4] ? $regs [4] : null;
                $res['frame_rate'] = $regs [5] ? $regs [5] : null;
            }
        }

        $regex_duration = "/Duration: ([^,]*)/";
        if (preg_match($regex_duration, $output, $regs)) {
            $res['duration'] = $regs [1] ? $regs [1] : null;
        }

        $regex_Bitrate = "/bitrate: ([0-9.]* [a-zA-Z\/]*)/";
        if (preg_match($regex_Bitrate, $output, $regs)) {
            $res['bit_rate'] = $regs [1] ? $regs [1] : null;
        }
        if (preg_match($regex_audio, $output, $regs)) {
            // $res['audio_codec'] = $regs [1] ? $regs [1] : null;
            //$res['audio_channel'] = $regs [3] ? $regs [3] : null;
            $res['audio_bit_rate'] = $regs [5] ? $regs [5] : null;
        }

        return $res;
    }

    // get default post id
    public function getDefaultpostid($current_controller, $current_action)
    {
       $anotherdb = Yii::app()->controller->getAnotherDbconnection();
       $page_qry = "select t1.id, t2.post_id from studio_adminpagelist t1 INNER JOIN studio_default_help t2 ON t1.id=t2.action_id where controller = '$current_controller' and action='$current_action' order by t2.id DESC limit 1";
       $page_res = $anotherdb->createCommand($page_qry)->queryAll(); 
       $default_post_id =  $page_res[0]['post_id'];
       return $default_post_id;
    } 
    public function getDefaultHelp()
    {
    $current_controller = Yii::app()->controller->id; 
    $current_action = Yii::app()->controller->action->id;
    $post_id = self::getDefaultpostid($current_controller,$current_action);
    if($post_id == ''){
        $post_id = 8645 ; // it is used default post when post id coming blank !
    }
    $anotherdb = Yii::app()->controller->getAnotherWpstudioDbconnection();
    $post_qry = "SELECT * FROM `muvi_posts` WHERE ID = '$post_id' and post_status = 'publish' and post_type = 'help' ORDER BY ID DESC LIMIT 1";
    $post_res = $anotherdb->createCommand($post_qry)->queryAll(); 
    $posts = $post_res;
    //print_r($posts);
   foreach($posts as $post) { 
     echo '<div>';
          echo '<h3><a href=""> '.$post["post_title"].'</a></h3>';
          echo preg_replace("/[\r\n]/","<p>", str_replace("?","'",utf8_decode($post['post_content'])));
     echo '</div>';
        }
    }

    public function isGeoBlockContent($content_id, $movie_stream_id = 0,$studioid=0,$countrycode=0) {
        if (isset(Yii::app()->request->cookies['for_preview']) && Yii::app()->request->cookies['for_preview']->value == 2) {
            return true;
        } else {
            $studio_id = ($studioid)?$studioid:self::getStudiosId();
            if($countrycode){
                $country = $countrycode;                
            }else{
                $visitor_loc = self::getVisitorLocation();
                $country = $visitor_loc['country'];
            }
            if ($movie_stream_id > 0) {
                $gc = GeoblockContent::model()->findAllByAttributes(array('movie_id' => $content_id, 'movie_stream_id' => $movie_stream_id));
            } else {
                $gc = GeoblockContent::model()->findAllByAttributes(array('movie_id' => $content_id));
            }
            if ($gc) {
                foreach ($gc as $key => $value) {
                    $fgc[] = $value['geocategory_id'];
                }
                $fgc = implode(',', $fgc);
                $command = Yii::app()->db->createCommand()
                    ->select('COUNT(*) AS cnt')
                    ->from('studio_content_restriction')
                    ->where('studio_id=:studio_id AND country_code=:country_code AND category_id IN ('.$fgc.')', array(':studio_id' => $studio_id, ':country_code' => $country));
				$studio_restr = $command->queryRow();
                if ($studio_restr['cnt'] > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function isGeoBlockEpisodeContent($movie_streams_id) {
        if (isset(Yii::app()->request->cookies['for_preview']) && Yii::app()->request->cookies['for_preview']->value == 2) {
            return true;
        } else {
            $studio_id = self::getStudiosId();
            $visitor_loc = self::getVisitorLocation();
            $country = $visitor_loc['country'];
            $parentmovieid = movieStreams::model()->findByAttributes(array('id' => $movie_streams_id));
            $gc = GeoblockContent::model()->findByAttributes(array('movie_id' => $parentmovieid['movie_id']));
            if ($gc) {
                $std_countr = StudioContentRestriction::model();
                $studio_restr = $std_countr->findAllByAttributes(array('studio_id' => $studio_id, 'country_code' => $country, 'category_id' => $gc['geocategory_id']));
                if (count($studio_restr) > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function getStudioCurrency() {
        $studio_id = self::getStudiosId();
        $currency = array();
        $sql = "SELECT cu.* FROM currency cu,studio_currency_support scs WHERE  cu.id = scs.currency_id AND scs.studio_id = " . $studio_id . " AND scs.status = 1 ORDER BY scs.is_default desc";
        $currency = Yii::app()->db->createCommand($sql)->queryAll();
        if (isset($currency) && empty($currency)) {
            $sql = "SELECT * FROM currency WHERE code = 'USD'";
            $currency = Yii::app()->db->createCommand($sql)->queryAll();
        }
        return $currency;
    }

    public function getStudioUserStartDate() {
        $studio_id = self::getStudiosId();
        $ud_sql = "SELECT * FROM (SELECT DATE_FORMAT(created_date,'%Y-%m-%d') as created_date FROM sdk_users WHERE studio_id = " . $studio_id . " ORDER BY created_date ASC) AS cdate WHERE (DATE_FORMAT(created_date,'%Y-%m-%d') <> '0000-00-00') LIMIT 1;";
        $user_date = Yii::app()->db->createCommand($ud_sql)->queryRow();
        return $user_date['created_date'];
    }
    public function getPrimaryPaymentGatewayCode($studio_id = Null) {
		$studio_id = (!isset($studio_id)) ? self::getStudiosId(): $studio_id;
		$spg = new StudioPaymentGateways();
        $short_code = $spg->findByAttributes(array('status'=> 1, 'is_primary'=> 1, 'studio_id' => $studio_id), array('select' => 'short_code'));
        
		return @$short_code->short_code;
    }
    /**
     * @method public ngetPgDimension() It will rerurn image dimension for physical goods
     * @author <manas@muvi.com>
     */
    function getPgDimension() {
        $studio_id = self::getStudiosId();
        $poster_sizes = StudioConfig::model()->getConfig($studio_id, 'pg_poster_dimension');
        if(isset($poster_sizes['config_value'])){
            $dmn = $poster_sizes['config_value'];
        }else{
            $dmn = '220x260';//default physical item size
        }
        $expl = explode('x', strtolower($dmn));
        return array('width' => @$expl[0], 'height' => @$expl[1]);
    }
    
    function canTakeSubscription() {
        $studio_id = self::getStudiosId();
        $sql = "SELECT pg.is_subscription FROM `payment_gateways` pg,`studio_payment_gateways` spg where spg.studio_id = ".$studio_id." AND pg.id = spg.gateway_id AND spg.status = 1 AND spg.is_primary = 1";
        $payment_gateway = Yii::app()->db->createCommand($sql)->queryRow();
        return $payment_gateway['is_subscription'];
    }
    /**
     * @method public ngetPGPrices() It will return price for physical goods product
     * @author <manas@muvi.com>
     */
    public function getPGPrices($product_id = Null, $default_currency_id = Null, $country = Null) {
        $price = array();
        if (!isset($default_currency_id)) {
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;
        }

        if (isset($product_id) && intval($product_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;

            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION['country']) && trim($_SESSION['country'])) ? $_SESSION['country'] : '');

            if (isset($country) && trim($country)) {
                $sql = "SELECT pr.* FROM pg_multi_currency pr LEFT JOIN currency AS cu ON (pr.currency_id=cu.id) 
                WHERE cu.country_code='" . $country . "' AND pr.product_id=" . $product_id;
                $price = $con->createCommand($sql)->queryAll();

                if (empty($price)) {
                    $is_default_currency = 1;
                } else {
                    $price = $price[0];
                }
            } else {
                $is_default_currency = 1;
            }

            if (intval($is_default_currency)) {
                $sql = "SELECT pr.* FROM pg_multi_currency pr LEFT JOIN currency AS cu ON (pr.currency_id=cu.id) 
                WHERE cu.id=" . $default_currency_id . " AND pr.product_id=" . $product_id;
                $price = $con->createCommand($sql)->queryAll();

                if (!empty($price)) {
                    $price = $price[0];
                }
            }
        }
        return $price;
    }
    /*
     * Check Product is geoblocked or not
     */
    public function isGeoBlockPGContent($pg_product_id,$studioid=0,$countrycode=0) {
        if (isset(Yii::app()->request->cookies['for_preview']) && Yii::app()->request->cookies['for_preview']->value == 2){
            return true;
        }else{
            $studio_id = ($studioid)?$studioid:self::getStudiosId();
            if($countrycode){
                $country = $countrycode; 
            }else{
                $visitor_loc = self::getVisitorLocation();            
                $country = $visitor_loc['country'];
            }
            $gc = GeoblockPGContent::model()->findAllByAttributes(array('pg_product_id'=>$pg_product_id));            
            if($gc){
                foreach ($gc as $key => $value) {
                    $fgc[] = $value['geocategory_id'];
                }
                $fgc = implode(',',$fgc);
                $std_countr = StudioContentRestriction::model();
                $sql = "SELECT COUNT(*) AS cnt FROM studio_content_restriction WHERE studio_id={$studio_id} AND country_code='{$country}' AND category_id IN ($fgc)";
                $studio_restr = Yii::app()->db->createCommand($sql)->queryROW();            
                if ($studio_restr['cnt'] > 0) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return true;
            }
        }
    }
     function getCustomValueReportUser($labelcode){
         $studio_id = self::getStudiosId();
          switch ($labelcode){
                        case 'total-revenue':
                            $controller = Yii::app()->controller;
                            $default_currency_id = $controller->studio->default_currency_id;  
                            $parameter='(select sum(amount) as tamt from transactions where transactions.user_id=u.id AND currency_id='.$default_currency_id.') as tottransactionamt';
                            break;
                        case 'number-of-transactions':
                            $parameter='(select count(id) as totcount from transactions where transactions.user_id=u.id) as totno_oftransaction';
                            break;
                        case 'bandwidth':
                             $parameter = '(select (ROUND((SUM(bandwidth_log.buffer_size))/(1024),3)) as bandwidth from bandwidth_log where bandwidth_log.user_id=u.id) as bandwidth_total';
                           // $parameter="(ROUND((b.buffer_size)/(1024*1024*1024),2)) as bandwidth";
                            break;
                        case 'total-views':
                            $parameter="COUNT(v.id) as viewcount";
                            break;
                        case 'unique-views':
                           $parameter='COUNT(distinct v.user_id) as u_viewcount';
                            break;
                        case 'total-watched-hour':
                           $parameter="SUBSTR(SEC_TO_TIME((SUM(v.played_length))),2,7)as duretion";
                            break; 
                        case 'joined-on':
                            $parameter=' DATE_FORMAT(u.created_date, "%M %d, %Y") AS formatted_date';
                            break; 
                        case 'total-login':
                            $parameter='(select count(id) as totlogincount from login_history where login_history.user_id=u.id) as totno_login_history';
                            break; 
                        case 'user-status':
                            $parameter="(case when (u.is_deleted = 1) THEN 
                                'Inactive'
                                ELSE
                                'Active' 
                                END)
                                as userStatus ";
                            break; 
                         case 'last-login':
                            $parameter='(select DATE_FORMAT(login_at, "%M %d, %Y %H:%i") AS lastlogin_date from login_history where login_history.user_id=u.id ORDER BY id DESC LIMIT 1) as lastlogin_date_time';
                            break;
                         case 'last-payment':
                            $controller = Yii::app()->controller;
                            $default_currency_id = $controller->studio->default_currency_id;  
                            $parameter='(select amount from transactions where transactions.user_id=u.id AND currency_id='.$default_currency_id.' ORDER BY id DESC LIMIT 1) as last_payment';
                            break; 
                        Default:
                            $parameter = " '$labelcode' as staticval_".rand(1000,9999);
                            break;
                    }
                 return $parameter;   
         
     }
     function getCustomValueReportContent($labelcode){
         $studio_id = self::getStudiosId();
        switch ($labelcode){
                        case 'total-revenue':
                            $controller = Yii::app()->controller;
                             $default_currency_id = $controller->studio->default_currency_id;  
                            $parameter='(select sum(amount) as tamt from transactions where transactions.user_id=u.id AND currency_id='.$default_currency_id.') as tottransactionamt';
                            break;
                        case 'number-of-transactions':
                            $parameter='(select count(id) as totcount from transactions where transactions.user_id=u.id) as totno_oftransaction';
                            break;
                        case 'bandwidth':
                            $parameter="(ROUND((b.buffer_size)/(1024*1024*1024),2)) as bandwidth";
                            break;
                        case 'total-views':
                            $parameter="COUNT(v.id) as viewcount";
                            break;
                        case 'unique-views':
                           $parameter='COUNT(distinct v.user_id) as u_viewcount';
                            break;
                        case 'total-watched-hour':
                            $parameter="SUBSTR(SEC_TO_TIME((SUM(v.played_length))),2,7)as duretion";
                            break; 
                       /* case 'joined-on':
                            $parameter=' DATE_FORMAT(u.created_date, "%M %d, %Y") AS formatted_date';
                            break; */
                        case 'total-login':
                            $parameter='(select count(id) as totlogincount from login_history where login_history.user_id=u.id) as totno_login_history';
                            break; 
                        /*case 'user-status':
                            $parameter="(case when (u.is_deleted = 1) THEN 
                                'Inactive'
                                ELSE
                                'Active' 
                                END)
                                as userStatus ";
                            break; */
                         case 'last-login':
                            $parameter='(select DATE_FORMAT(login_at, "%M %d, %Y %H:%i") AS lastlogin_date from login_history where login_history.user_id=u.id ORDER BY id DESC LIMIT 1) as lastlogin_date_time';
                            break;
                         case 'last-payment':
                            $parameter='(select amount from transactions where transactions.user_id=u.id ORDER BY id DESC LIMIT 1) as last_payment';
                            break; 
                        Default:
                            $parameter = " '$labelcode' as staticval_".rand(1000,9999);
                            break;
                    }
                    
                 return $parameter;   
         
     }
     public function checkMultiStore($email) {
        $is_multi = User::model()->findAll("email=:email AND is_active=:is_active AND role_id IN (:role1,:role2 ,:role3)", array(':email' => $email,':is_active' => 1,':role1' => 1,':role2' => 2,':role3' => 3));
       
        if(count($is_multi)>0){
            $multiRecords = User::model()->findAll("email=:email AND is_active=:is_active AND role_id IN (:role1,:role2 ,:role3)", array(':email' => $email,':is_active' => 1,':role1' => 1,':role2' => 2,':role3' => 3)); 
            $multiData=array();
            $studio_id = self::getStudiosId();
            foreach($multiRecords as $multiKey=>$multiRecord){
                $multiStudio = Studio::model()->find('id=:id', array(':id' => $multiRecord['studio_id']));
                if($multiRecord['studio_id'] != $studio_id){
                    $multiData[$multiKey]['studio_id']=$multiRecord['studio_id'];
                    $multiData[$multiKey]['studio_name']=$multiStudio->name;
                    $multiData[$multiKey]['studio_email']=$email;
                }
            }            
        }
        return $multiData;
    }   
    
    function sendSampleEmail($to, $subject, $data){
        $from = 'support@muvi.com';
        $from_name = 'Muvi.com';
        $site_url = Yii::app()->getBaseUrl(true);
        $msg = '<p><b>Message :</b> ' . serialize($data). '</p>';
        $logo = '<a href="'.$site_url.'"><img src="'.EMAIL_LOGO.'" alt="Muvi" /></a>';
        $params = array(
            'website_name' => $site_url,
            'logo' => $logo,
            'msg' => $msg
        );

        $adminGroupEmail = array($to);
        $template_name = 'studio_contact_us'; 
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/studio_contact_us', array('params' => $params), true);
        $returnVal = Yii::app()->controller->sendmailViaAmazonsdk($thtml, $subject, $adminGroupEmail, $from, '', '', '', $from_name);
    }   
    //5975: Email addresses for different notifications
    //ajit@muvi.com
    public function emailNotificationLinks($studio_id,$type){
        $linked_emails = EmailNotificationLinkedEmail::model()->findAllByAttributes(array('studio_id' => $studio_id));
        if($linked_emails[0][$type] != ''){
            $cont_u = json_decode($linked_emails[0][$type]);
            foreach($cont_u as $key => $cont_val){
                $data[] =  $cont_val; 
}
        }else{
            $data = '';
        }
        return $data;
    }
    //END 5975: Email addresses for different notifications
    public function checkChieldStore($parent_studio_id) {
        $studio_id = self::getStudiosId();
        if($studio_id!=$parent_studio_id)
        $child_id[] =$parent_studio_id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $parent_studio_id));
        foreach($StoreMapping as $val){
            if($val['studio_id']!=$studio_id){
                $child_id[] =$val['studio_id'] ;
            }
        }
        $command = Yii::app()->db->createCommand()
            ->select('S.id,S.name,U.email')
            ->from('studios S,user U')
            ->where('S.id = U.studio_id AND U.role_id=1 AND S.id IN ('. implode(',', $child_id).')');                    
        $records = $command->queryAll();
        $data=array();
        foreach($records as $key=>$record){           
            $multiData[$key]['studio_id']=$record['id'];
            $multiData[$key]['studio_name']=$record['name'];
            $multiData[$key]['studio_email']=$record['email'];
        }        
        return $multiData;
    }
	function getLanguageCode($country) {
        $lang_code = 'en_GB';
        if(isset($country) && trim($country)){
            $data = file_get_contents("https://restcountries.eu/rest/v2/name/".$country."?fields=languages");
            $data = json_decode($data, true);
            $code = $data[0]['languages'][0]['iso639_1'];
            if(isset($code) && trim($code)){
                $lang_code = $code;
            }
        }
        return $lang_code;
    }
    function checkparentStore($studio_id){
        $is_multi_store_enabled = StudioConfig::model()->getConfig($studio_id, 'is_multi_store_enabled');
        if(isset($is_multi_store_enabled['config_value'])){
            $StoreMapping = StoreMapping::model()->find('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
            if(!empty($StoreMapping)) {
                return 1;
            } else {
                return 0;
            }
        }else {
            return 0;
        }
    }	
    function childStorecheck($studio_id){
        $StoreMapping = StoreMapping::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        if(!empty($StoreMapping)) {
            return 1;
        } else {
            return 0;
        }
    }
    public function isPaymentGatwayAndSubscriptionBundlePlanExists($studio_id = null, $default_currency_id = NULL, $country = NULL, $language_id = Null) {
        $res = array();
        $controller = Yii::app()->controller;
        $language_id = (isset($language_id) && intval($language_id)) ? $language_id : $controller->language_id;
        if (isset($studio_id) && !empty($studio_id)) {
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
            if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 256)) {
                $gateways = new StudioPaymentGateways;
                $cond = 't.studio_id=' . $studio_id . ' AND t.status = 1';
                $gateways = $gateways::model()->with(array('paymentgt' => array('alias' => 'pg')))->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 't.is_primary DESC, t.id DESC'
                        )
                );

                $plans = new SubscriptionBundlesPlan;
                $cond = 'studio_id=' . $studio_id . ' AND status = 1 AND (language_id='.$language_id.' OR parent_id=0 AND id NOT IN (SELECT parent_id FROM subscriptionbundles_plans WHERE studio_id='.$studio_id.' AND language_id='.$language_id.' ))';
                $plans = $plans::model()->with()->findAll(
                        array(
                            'condition' => $cond,
                            'order' => 'id_sequency ASC, IF(parent_id >0, parent_id, id) ASC'
                        )
                );
                $pln = array();
                foreach ($plans as $plan) {
                    if($plan->parent_id > 0){
                        $plan->id = $plan->parent_id;
}

                    $pln[] = $plan;
                }
                $plans = $pln;

                if (isset($plans) && !empty($plans) && isset($gateways[0]) && !empty($gateways[0])) {

                    if (!isset($default_currency_id)) {
                        $default_currency_id = $controller->studio->default_currency_id;
                    }
                    foreach ($plans as $key => $value) {
                        $price_list = self::getUserSubscriptionBundlesPrice($value->id, $default_currency_id, $country);
                        $plans[$key]->price = $price_list['price'];
                        $plans[$key]->currency_id = $price_list['currency_id'];
                    }

                    $res['plans'] = $plans;
                    $res['gateways'] = $gateways;
                }
            }
        }
        return $res;
    }
	function getAllPlaylistName($studio_id = false, $user_id = false,$isAdmin = 0,$offset,$limit){
		if(!$user_id && $isAdmin != 0){
			$user_id = 0;
		} 
		if ($studio_id == false) {
			$studio_id = Yii::app()->common->getStudiosId();
		}
		if($offset != ''){
		$command2 = Yii::app()->db->createCommand()
				->select('u.id,u.playlist_name,u.content_category_value')
				->from('user_playlist_name u')
				->where('u.studio_id=' . $studio_id . ' AND u.user_id = ' . $user_id . '')
				->limit($limit)
                ->offset($offset);
		}else{
			$command2 = Yii::app()->db->createCommand()
				->select('u.id,u.playlist_name,u.content_category_value')
				->from('user_playlist_name u')
				->where('u.studio_id=' . $studio_id . ' AND u.user_id = ' . $user_id . '');
		}
		$playlist = $command2->QueryAll();
		return $playlist;
	}
	function getAllplaylist($studio_id = false, $user_id = false,$isAdmin = 0,$offset = 0,$limit = 0,$language_id=20) {
		$controller = Yii::app()->controller;
		if(!$user_id && $isAdmin != 0){
			$user_id = 0;
		}
		if ($studio_id == false) {
			$studio_id = Yii::app()->common->getStudiosId();
		}
		$playlist = self::getAllPlaylistName($studio_id, $user_id, $isAdmin,$offset,$limit);
		$count = count($playlist);
		$alllists     = array();
		$items        = array();
		$playlistdata = array();
		if(!empty($playlist)){
			foreach ($playlist as $list) {
				$play_id = $list['id'];
				$command = Yii::app()->db->createCommand()
						->select('id,content_id,content_type')
						->from('user_playlist p')
						->where('p.user_id = ' . $user_id . ' AND p.studio_id=' . $studio_id . ' AND p.playlist_id ='.$play_id);
				$playItem = $command->queryAll();
				$poster_playlist = $controller->getPoster($play_id, 'playlist_poster');
				$items[$play_id]['list_id']			= $list['id'];
				$items[$play_id]['list_name']		= $list['playlist_name'];
				$items[$play_id]['content_category']= $list['content_category_value'];
				$items[$play_id]['poster_playlist'] = $poster_playlist;
				$items[$play_id]['lists']			= $playItem;
				$items[$play_id]['count']			= count($playItem);
			}
			if (!empty($items)) {
				$k = 0;
				foreach ($items as $playl) {
					$alllists[$k]['list_name']		 =  $playl['list_name'];
					$alllists[$k]['list_id']		 =  $playl['list_id'];
					$alllists[$k]['content_category']=  $playl['content_category'];
					$alllists[$k]['playlist_poster']=	$playl['poster_playlist'];
					$alllists[$k]['counts']			 =  $playl['count'];
					if(!empty($playl['lists'])){
						foreach($playl['lists'] as $list){
							$is_episode = $list['content_type'];
							$content_id = $list['content_id'];
							if($is_episode == '0'){
								$command = Yii::app()->db->createCommand()
										->select('movie_id')
										->from('movie_streams m')
										->where('m.id = ' . $content_id . ' AND m.studio_id=' . $studio_id . ' AND m.is_episode =' . $is_episode);
								$sql = $command->queryRow();
								$content_id = $sql['movie_id'];
							}
							$customData=array();
							$alllists[$k]['lists'][] = Yii::app()->general->getContentData($content_id,$is_episode,$customData,$language_id,$studio_id,$user_id);
						}
					}
					$k++;
					
				}
			}
			$playlistdata = array(
				'playlist' => $alllists,
				'count'    => $count
			);
		}
		return $playlistdata;
	}
   /*
     * Userlogin For wordpress with Muviuser 
     * @author T.NANDY
     */
    public function getWpUserLogin($username, $password)
    {
        wp_clear_auth_cookie();
        $creds = array();
        $creds['user_login'] = $username;
        $creds['user_password'] = $password;
        $creds['remember'] = true;
        $user = wp_signon($creds, false);
        return $user;
    }
    
    public function checkApi($studio_id) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getLoginwith = Yii::app()->db->createCommand()
                ->select('id, studio_id, field_type')
                ->from('unique_access_fields')
                ->where('studio_id=:studio_id', array(':studio_id' => $studio_id))
                ->queryRow();

        return $getLoginwith['field_type'];
    }
    public function checkRegApi($studio_id){
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getApiDetails = Yii::app()->db->createCommand()
            ->select('s.studio_id studio_id, s.module module, s.id')
            ->from('external_api_keys e')
            ->join('studio_api_details s', 'e.id=s.api_type_id')
            ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module'=>'register'))
            ->queryRow();
        if(!empty($getApiDetails)){
            return true;
        }else
            return false;
    }
    
    public function checkPlayApi($studio_id){
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getApiDetails = Yii::app()->db->createCommand()
            ->select('s.studio_id studio_id, s.module module')
            ->from('external_api_keys e')
            ->join('studio_api_details s', 'e.id=s.api_type_id')
            ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module'=>'playPerimissionChk'))
            ->queryRow();
        
        if(!empty($getApiDetails)){
            return true;
        }else
            return false;
    }    
    
    public function enableOtp($studio_id) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getOtp = Yii::app()->db->createCommand()
                ->select('config_value')
                ->from('studio_config')
                ->where('studio_id=:studio_id and config_key=:config_key', array(':studio_id' => $studio_id, ':config_key'=>'otp_enable'))
                ->queryRow();

        return $getOtp['config_value'];
    }
    public function mobileFlag($studio_id){
       if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getOtp = Yii::app()->db->createCommand()
                ->select('config_value')
                ->from('studio_config')
                ->where('studio_id=:studio_id and config_key=:config_key', array(':studio_id' => $studio_id, ':config_key'=>'register_with_mobile'))
                ->queryRow();

        return $getOtp['config_value'];
    }    
    /*
	* Check is content added to favourite or not
	* Biswajitdas<biswajitdas@muvi.com>
	 
	*/
	public function isContentFav($studio_id = false,$user_id = false,$movie_id,$is_episode){
		if ($studio_id == false) {
			$studio_id = Yii::app()->common->getStudiosId();
		}
		if ($user_id == false) {
            $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
		}
		$fav_status = UserFavouriteList::model()->getFavouriteContentStatus($movie_id,$studio_id,$user_id,$is_episode);
		return $fav_status;
	}
}