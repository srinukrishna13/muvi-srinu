<?php

class ResellerCardErrors extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'reseller_card_errors';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function set_reseller_error_data($data_error = array()){
        if(count($data_error) > 0){
            foreach($data_error as $key => $val){
                $this->$key = $val;
            }
            $this->save();
        }

}

}
