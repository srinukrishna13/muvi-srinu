<?php
$excaptionarray = array('censor_rating','language','genre'); 
$enable_lang = $this->language_code;
if ($_COOKIE['Language']) {
    $enable_lang = $_COOKIE['Language'];
}
?>
<div class="col-sm-7 p-l-0">
    <div class="border-dotted">
        <div class="row">
            <h3 class="col-sm-12 text-capitalize f-300">Meta-Data Fields</h3>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-8">
                <div class="loading_div" style="display: none;">
                    <div class="preloader pls-blue preloader-sm">
                        <svg viewBox="25 25 50 50" class="pl-circular">
                        <circle r="20" cy="50" cx="50" class="plc-path"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <ul id="sortableform" class="list-unstyled adddiv">
            <?php $this->renderPartial('custom_forms', array('column_data'=>$column_data,'formid'=>$formid,'excaptionarray'=>$excaptionarray));?>
        </ul>
        <div class="m-t-40"></div>        
    </div>
</div>
</form>
<div class="col-sm-5 p-r-0">
    <div class="border-dotted">
    <div class="row">
        <form action="javascript:void(0);" method="post" name="metadatafieldform" id="metadatafieldform">
        <h3 class="col-sm-12 text-capitalize f-300">Available Fields</h3>
        <div class="col-sm-12 m-b-20">
            <label><button type="button" class="btn btn-default-with-bg" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/category/AddNewField');">Add Metadata Field</button></label>
        </div>                    
        <?php
        if (isset($template_data) && !empty($template_data)) {
            $index = max(CHtml::listData($column_data, 'id', 'id'))+count($template_data);
            foreach ($template_data as $template) {
                ?>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-5"><p><?= $template['f_display_name'] ?></p></div>
                        <div class="col-sm-4">
                            <?php if ($template['f_type'] == 0) {
                                $type = 'Textfield';
                            } elseif ($template['f_type'] == 1) {
                                $type = 'Textarea';
                            } elseif ($template['f_type'] == 2) {
                                $type = 'Dropdown';
                            } elseif ($template['f_type'] == 3) {
                                $type = 'List';
                            }
                            echo $type; 
                            ?>
                            <div style="display: none;" id="customfieldreplace<?= $index; ?>">
                                <li class="m-t-20" id="customreportreplace<?= $index; ?>">
                                    <div class="row">
                                        <label class="col-sm-4 control-label editdisplayname" data-displayname="custom[<?= $index;?>][f_display_name]"><?= $template['f_display_name'];?></label>
                                        <div class="col-sm-6">
                                            <?php if(!$template['f_type']){?>
                                            <div class="fg-line">
                                                <input type='text' class="form-control input-sm checkInput" readonly="readonly">
                                            </div>
                                            <?php }elseif($template['f_type']==1){?>
                                            <div class="fg-line">
                                                <textarea class="form-control input-sm checkInput" rows="5" readonly="readonly"></textarea>
                                            </div>
                                            <?php }elseif($template['f_type']==2){?>
                                            <div class="fg-line">
                                                <select class="form-control input-sm checkInput" >
                                                    <?php
                                                    echo "<option value=''>-Select-</option>";
                                                    $opData = json_decode($template['f_value'],true);
                                                    $opData_new = $opData;
                                                    $opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
                                                    $opData = empty($opData)?$opData_new:$opData;
                                                    foreach($opData AS $opkey=>$opvalue){
                                                       echo "<option value='".$opvalue."'>" . $opvalue . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <?php }elseif($template['f_type']==3){?>
                                            <div class="fg-line">
                                                <select class="form-control input-sm checkInput" multiple>
                                                    <?php
                                                    $opData = json_decode($template['f_value'],true);
                                                    $opData_new = $opData;
                                                    $opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
                                                    $opData = empty($opData)?$opData_new:$opData;
                                                    foreach($opData AS $opkey=>$opvalue){
                                                            echo "<option value='".$opvalue."'>" . $opvalue . "</option>";
                                                    }
                                                    ?>
                                                </select>               
                                            </div>
                                            <?php }?>
                                        </div>
                                        <div class="col-sm-2 text-right">
                                            <div class="row">
                                                <div  class="col-sm-4 p-r-0">
                                                    <a style="cursor: move;"><i class="icon-cursor-move h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                                </div>
                                                <?php 
                                                /*if(($template['f_id']=='title') && ($formid!=4)){
                                                    $template['f_is_required']=0;
                                                }*/
                                                if($template['f_is_required']!=1){
                                                    if ($template['f_type'] == 0) {
                                                        $type = 'Textfield';
                                                    } elseif ($template['f_type'] == 1) {
                                                        $type = 'Textarea';
                                                    } elseif ($template['f_type'] == 2) {
                                                        $type = 'Dropdown';
                                                    } elseif ($template['f_type'] == 3) {
                                                        $type = 'List';
                                                    }                   
                                                ?>
                                                <?php if(in_array($template['f_type'],array(2,3))){?>
                                                    <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4"  aria-hidden="true"></i></a></div>
                                                <?php }?>
                                                <div id="movecustomreport<?=$index;?>" class="col-sm-4 text-right" data-name="<?= $template['f_display_name'];?>" data-id="<?=$index; ?>" data-tempid="<?= $template['id'] ?>" data-type="<?=$type;?>" onclick="movetofieldcolumn(this)">
                                                    <a href="javascript:void(0)" title="Remove from content type"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                                </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="custom[<?= $index;?>][id]" value="<?= $template['id'];?>">
                                    <input type="hidden" name="custom[<?= $index;?>][f_type]" value="<?= $template['f_type'];?>">
                                    <input type="hidden" name="custom[<?= $index;?>][f_display_name]" value="<?= $template['f_display_name'];?>">
                                    <input type="hidden" name="custom[<?= $index;?>][f_name]" value="<?= $template['f_name'];?>">
                                    <input type="hidden" name="custom[<?= $index;?>][f_id]" value="<?= $template['f_id'];?>">
                                    <input type="hidden" name="custom[<?= $index;?>][f_value]" value='<?= $template['f_value'];?>'>
                                    <input type="hidden" name="custom[<?= $index;?>][f_is_required]" value="<?= $template['f_is_required'];?>">
                                    <?php if(($template['field_type']==1) && !in_array($template['f_name'],$excaptionarray)){?>
                                    <input type="hidden" name="custom[<?= $index;?>][data_type]" value="custom" class="cfield">
                                    <?php }?>
                                </li>
                            </div>
                        </div>
                        <div  class="col-sm-1 text-right" data-id="<?= $index ?>" data-name="<?=$template['f_display_name'] ?>" data-type="<?= $type; ?>" data-ftype='<?php if($template['field_type']==1){echo 'custom';}else{echo 'system';}?>' onclick="movetoformcolumn(this)"><a href="javascript:void(0)" title="Add Field"><i class="icon-plus h4"  aria-hidden="true"></i></a></div>
                        <?php if(in_array($template['f_name'],$excaptionarray)){?>
                            <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4"  aria-hidden="true"></i></a></div>
                        <?php }elseif($template['field_type']==1){?>
                            <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4"  aria-hidden="true"></i></a></div>
                            <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="deletefield(this)"><a href="javascript:void(0)" title="Delete field"><i class="icon-close h4"  aria-hidden="true"></i></a></div>
                        <?php }elseif(in_array($template['f_type'],array('3','2'))){?>
                            <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4"  aria-hidden="true"></i></a></div>
                        <?php }?>
                    </div>
                </div>
        <?php $index++;}
    } ?>
        <span id="adddivtoavailable"></span>
        </form>
    </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 text-center m-t-40 m-b-20">
        <button type="button" class="btn btn-primary" onclick="SaveContentType(this)">Save</button>
    </div>
</div>