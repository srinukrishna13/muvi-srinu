<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
class AdmincelebController extends Controller
{
    public $layout = 'admin';
    public $headerinfo = '';
    protected function beforeAction($action){
        parent::beforeAction($action);
         Yii::app()->theme = 'admin'; 
        if(!(Yii::app()->user->id))
        {
            $this->redirect(array('/index.php/'));
        }else{
             $this->checkPermission();
        }
                
        return true;
    }
    public function actionCelebList()
    {
        $this->breadcrumbs = array('Manage Content', 'Manage Cast/Crew');
        $this->headerinfo = "Manage Cast/Crew";
        $this->pageTitle="Muvi | Manage Cast/Crew";

        $studio_id = $this->studio->id;
        $language_id = $this->language_id;

        $full_page_path = Yii::app()->common->curPageURL();
        $cond = '';
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {
            $search_form = $_REQUEST['searchForm'];
            
            if (isset($search_form) && $search_form['search_text'] == '') {
                $url = $this->createUrl('adminceleb/celeblist');
                $this->redirect($url);
            }else {
                $searchData = $_REQUEST['searchForm'];
                
                if ($searchData['search_text']) {
                    $cond .= " AND (LOWER(name) LIKE '". strtolower(trim($searchData['search_text'])) . "%')";
                }
            }
        }

        $search_text = '';

        $page_size = (isset($_REQUEST['page_size']) && $_REQUEST['page_size'] != '') ? $_REQUEST['page_size'] : 20;
        //Pagination Implimented ..
        $page_size = $page_size;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }

        $dbcon = Yii::app()->db;
        $cc_cmd = $dbcon->createCommand("SELECT SQL_CALC_FOUND_ROWS (0), id,name,permalink,summary,parent_id,language_id FROM celebrities WHERE studio_id={$studio_id}  AND (language_id={$language_id} OR parent_id=0  AND id NOT IN (SELECT parent_id FROM celebrities WHERE studio_id={$studio_id} AND language_id={$language_id})) ".$cond." ORDER BY IF(parent_id >0, parent_id, id) DESC LIMIT {$offset}, {$page_size}");
        

        $data = $cc_cmd->queryAll();
       
       
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        // $count = (isset($count[0]['count'])) ? $count[0]['count'] : 0;
        $pages = new CPagination($count);
        $pages->setPageSize($page_size);
        $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
        $sample = range($pages->offset + 1, $end);
        $this->render('celebList', array('data' => $data, 'language_id'=>$language_id, 'searchText' => @$searchData['search_text'], 'item_count' => $count, 'page_size' => $page_size, 'pages' => $pages, 'full_page_path' => $full_page_path));
    }
    public function actionAdd(){
        $this->breadcrumbs=array('Manage Content', 'Manage Cast/Crew' =>array('adminceleb/celeblist'),'Add Cast/Crew');
        $this->pageTitle="Muvi | Add Cast/Crew";
        $this->headerinfo = "Add Cast/Crew";
        $studio_id = Yii::app()->user->studio_id;
        $language_id = '';
        $celeb = new Celebrity();
        $celeb_types = Celebrity::model()->celeb_type();
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $this->render('add',array('celeb'=>$celeb,'celeb_types' => $celeb_types,'all_images'=>$all_images, 'language_id' => $language_id));
    }
    
    public function actionSave() {
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $user_id = Yii::app()->user->id;
        if ($_REQUEST['celeb']['id'] != "") {
            $celeb = Celebrity :: model()->findByPk($_REQUEST['celeb']['id']);
            $permalink = $celeb->permalink;
            $old_name=$celeb->name;
            if (($this->language_id != $celeb->language_id)) {
                 $celeb = Celebrity :: model()->findByAttributes(array('studio_id' => $studio_id, 'parent_id' => $_REQUEST['celeb']['id'],'language_id'=>$language_id));
                 if(empty($celeb)){
                    $celeb = new Celebrity();
                    $celeb->language_id = $this->language_id;
                    $celeb->parent_id = $_REQUEST['celeb']['id'];
                    $celeb->permalink = $permalink;
               }
            }
            $msg = 'Cast/Crew updated successfully';
        } else {//Add parent record
            $celeb = new Celebrity();
            $celeb->parent_id = 0;
            
            $msg = 'Cast/Crew added successfully';
        }

        $data = $_REQUEST['celeb'];
        $celeb->attributes = $_REQUEST['celeb'];

        $celeb->name = $data['name'];

        $celeb->summary = $data['summary'];
        $celeb->studio_id = Yii::app()->user->studio_id;
        $celeb->created_by = Yii::app()->user->id;
        $celeb->ip = $_SERVER['REMOTE_ADDR'];
        $celeb->created_date = date('Y-m-d H:i:s');
        if ($_REQUEST['g_image_file_name'] == "" && $_FILES['Filedata']['name'] == "") {
            $celeb->save();
            Yii::app()->user->setFlash('success', $msg);
            $url = $this->createUrl('adminceleb/celeblist');
           
        } elseif ($celeb->save(false)) {
            $this->processCelbImage($celeb->id);
            Yii::app()->user->setFlash('success', $msg);
            $url = $this->createUrl('adminceleb/celeblist');
            
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry,cast/crew not added!');
            $url = $this->createUrl('adminceleb/add');
           
        }
        if (HOST_IP != '127.0.0.1'){
             if ($_REQUEST['celeb']['id'] == "") { // add to solr in creation
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $celeb->id;
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = '';
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $celeb->name;
                    $solrArr['permalink'] = $celeb->permalink; 
                    $solrArr['studio_id'] = Yii::app()->user->studio_id;
                    $solrArr['display_name'] = 'star';
                    $solrArr['content_permalink'] = 'star'; 
                    $solrObj = new SolrFunctions();
                    $ret_solr = $solrObj->addSolrData($solrArr);
             } else {               
                    $solrobj = new SolrFunctions();
                    $solrobj->deleteSolrQuery("cat:star AND sku:" . $studio_id ." AND content_id:". $_REQUEST['celeb']['id']); 
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $celeb->id;
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = '';
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $celeb->name;
                    $solrArr['permalink'] = $celeb->permalink; 
                    $solrArr['studio_id'] = Yii::app()->user->studio_id;
                    $solrArr['display_name'] = 'star';
                    $solrArr['content_permalink'] = 'star'; 
                    $solrObj = new SolrFunctions();
                    $ret_solr = $solrObj->addSolrData($solrArr);           
                }
            }
            $this->redirect($url);
        }

    public function actionremove(){
         $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['celeb_id']) && trim($_REQUEST['celeb_id'])) {
                Celebrity::model()->deleteAll('id = :id OR parent_id = :parent_id', array(
                    ':id' => $_REQUEST['celeb_id'],
                    ':parent_id' => $_REQUEST['celeb_id']
                ));
				// Deleting data from movieCasts
	    MovieCast::model()->deleteAll('celebrity_id = :id ', array(':id' => $_REQUEST['celeb_id']));
            if (HOST_IP != '127.0.0.1'){
            $solrobj = new SolrFunctions();
            $solrobj->deleteSolrQuery("cat:star AND sku:" . $studio_id ." AND content_id:". $_REQUEST['celeb_id']);                            
            }
            Yii::app()->user->setFlash('success','Cast/Crew removed successfully');
            $url=$this->createUrl('adminceleb/celeblist');
            $this->redirect($url);
        } else {
            Yii::app()->user->setFlash('error','Oops! Sorry,cast/crew not deleted!');
            $url=$this->createUrl('adminceleb/add');
            $this->redirect($url);
        }
    }
    
    public function actionedit(){
        $studio_id = Yii::app()->user->studio_id;
        $this->breadcrumbs=array('Edit Cast/Crew');
        $this->pageTitle="Muvi | Edit Cast/Crew";
        $language_id = $this->language_id;
        $celebs = Celebrity::model()->findAll('(id =:celeb_id OR parent_id=:celeb_id) AND studio_id =:studio_id AND (language_id=20 OR language_id=:language_id) ORDER BY parent_id ASC',array(':celeb_id'=>$_REQUEST['celeb_id'],':studio_id'=>$studio_id,':language_id'=>$language_id));
        $celeblang = array();
        if(!empty($celebs)){
            foreach($celebs as $clb){
                $celeb_id = $clb->id;
                if ($clb->parent_id > 0) {
                    $celeb_id = $clb->parent_id;
                }
                $celeblang[$celeb_id] = $clb;
            }
        }
        $celeb = Celebrity :: model()-> findByPk($_REQUEST['celeb_id']);
        $celeb_types = Celebrity::model()->celeb_type();
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $this->render('add',array('celeb'=>$celeb,'celeblang'=>$celeblang,'celeb_types' => $celeb_types,'all_images'=>$all_images, 'language_id' => $language_id));
    }
    
    public function actiondetail(){
        $this->breadcrumbs=array('Edit Cast/Crew');
        $celeb = Celebrity :: model()-> findByPk($_REQUEST['celeb_id']);
        $this->pageTitle="Muvi | ".$celeb['name']." Detail";
        
    }
/**
 * @method public processCelebimage() It will crop and upload the image into the respective bucket with mentioned size
 * @author Gayadhar<support@muvi.com>
 * @return array Image path array
*/
	function processCelbImage($celb_id=''){
		$studio_id = Yii::app()->common->getStudiosId();
		$poster_object_type = 'celebrity';
        $object_id = $celb_id;
		
		$cropDimension = array('medium' => '200x200');
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $celb_id;
		if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
			$cdimension = array('thumb' => "64x64");
			$ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);
			
			$path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST);
			$ret = $this->uploadPoster($_FILES['Filedata'], $object_id, $poster_object_type, $cropDimension, $path);
			if ($celb_id) {
				Yii::app()->common->rrmdir($dir);
			}
			return $ret;
		} else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {
			
			$file_info = pathinfo($_REQUEST['g_image_file_name']);
			$_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
			$jcrop_allimage = $_REQUEST['jcrop_allimage'];
			$image_name = $_REQUEST['g_image_file_name'];

			$dimension['x1'] = $jcrop_allimage['x13'];
			$dimension['y1'] = $jcrop_allimage['y13'];
			$dimension['x2'] = $jcrop_allimage['x23'];
			$dimension['y2'] = $jcrop_allimage['y23'];
			$dimension['w'] = $jcrop_allimage['w3'];
			$dimension['h'] = $jcrop_allimage['h3'];
			
			$path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
			$fileinfo['name'] = $_REQUEST['g_image_file_name'];
			$fileinfo['error'] = 0;
			$ret = $this->uploadPoster($fileinfo, $object_id, $poster_object_type, $cropDimension, $path);
			Yii::app()->common->rrmdir($dir);
			return $ret;
		} else {
			return false;
		}
	}
    /*
     * ajit@muvi.com
     * #5332
     */    
    public function actionajaxcastCrewDelete() {
        if ($_REQUEST['id'] != "" && $_REQUEST['studio_id'] != "") {
            $celebrity=CelebrityType::model()->findByPk($_REQUEST['id']);
            if($celebrity->delete()){
                $data = array('isSuccess'=>1,'id'=>$_REQUEST['id']);
}
            else{
                $data = array('isSuccess'=>0,'id'=>$_REQUEST['id']);
            }                
        }
        $data = json_encode($data);
        echo $data;
        exit;
    }
    public function actionajaxtypeSave() {
        if ($_REQUEST['typename'] != "" && $_REQUEST['studio_id'] != "") {
            //check for duplicate entry
            $duplicate = Yii::app()->db->createCommand("SELECT COUNT(*) AS CNT FROM celebrity_type WHERE celebtity_type='".$_REQUEST['typename']."' AND studio_id=".$_REQUEST['studio_id'])->queryROW();
            
            if($duplicate['CNT']==0)
            {
                if($_REQUEST['typename_id'] !='')
                {
                    $cele = new CelebrityType();
                    $celeb = $cele->findByPk($_REQUEST['typename_id']);
                    $celeb->celebtity_type = $_REQUEST['typename'];
                    $celeb->date_modified = gmdate('Y-m-d H:i:s');
                }
                else
                {
                    $celeb = new CelebrityType();
                    $celeb->celebtity_type = $_REQUEST['typename'];
                    $celeb->date_created = gmdate('Y-m-d H:i:s');
                    $celeb->studio_id = $_REQUEST['studio_id'];
                }          
                if($celeb->save()){
                    $data = array('isSuccess'=>1,'message'=>'Type added successfully');
                }
                else{
                    $data = array('isSuccess'=>0,'message'=>'Type cannot added successfully');
                }
            }
            else{
                $data = array('isSuccess'=>2,'message'=>'Type cannot added successfully');
            }
        }
        $data = json_encode($data);
        echo $data;
        exit;
    }
    public function actionajaxshowtype() {
        $studio_id = Yii::app()->user->studio_id;
        //get celebrity type
        $celeb_types = CelebrityType::model()->findAll('studio_id=:studio_id ORDER BY celebtity_type', array(':studio_id' => $studio_id));
        echo '<button data-toggle="dropdown" class="btn btn-default-with-bg  dropdown-toggle btn-block" style="text-align:left;">Select Cast/Crew Type <span class="caret absolute" style="right:10px;top:18px;"></span></button><ul class="dropdown-menu" id="typeul" style="width:100%;">';
        ?>
        <li id="typeliidone"><input type="radio" id="typeidone" name="cast_type" value="Actor"><label for="typeidone"><span class="data-label">Actor</span></label></li>
        <li id="typeliidtwo"><input type="radio" id="typeidtwo" name="cast_type" value="Director"><label for="typeidtwo"><span class="data-label">Director</span></label></li>
        <?php
        if($celeb_types)
        {
            foreach($celeb_types as $cel) {
        ?>
                <li id="typeliid<?php echo  $cel['id'];?>"><input type="radio" id="typeid<?php echo  $cel['id'];?>" name="cast_type" value="<?php echo  $cel['celebtity_type'];?>"><label for="typeid<?php echo  $cel['id'];?>"><span class="data-label"><?php echo  $cel['celebtity_type'];?></span> <span class="pull-right castcrew"><em class="icon-pencil" onclick="showType(<?php echo  $cel['id'];?>,'<?php echo $cel['celebtity_type'];?>');"></em>&nbsp;&nbsp;&nbsp;&nbsp;<em class="fa fa-trash actioncolor" onclick="confirmDelete(<?php echo  $cel['id'];?>,'<?php echo $cel['celebtity_type'];?>');"></em></span></label></li>
        <?php
            }
        }       
        ?>
        
        
        <?php
        echo '<li id="typeliidthree" onclick="showType();"><label for="typeidthree"><span class="data-label"><a href="javascript:void(0)">New Type</a></span></label></li>';
        echo "</ul>";
    }       
}