<?php
class StarController extends Controller {
    public function actionshow() {
        $connection = Yii::app()->db;
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $celb = Celebrity::model()->find('permalink=:permalink AND studio_id=:studio_id AND(language_id=:language_id OR parent_id=0 AND id NOT IN (SELECT parent_id FROM celebrities WHERE studio_id=:studio_id AND permalink=:permalink AND language_id=:language_id))', array(':permalink' => $_REQUEST['item'], ':studio_id' => $studio_id,':language_id' => $language_id));
        //echo "<pre>";print_r($celb);exit;
        if ($_REQUEST['item'] == 'listitem') {
            $studio_id = Yii::app()->common->getStudiosId();
            $celeblist = Yii::app()->db->createCommand("SELECT id,name,summary from celebrities WHERE parent_id='0' AND studio_id= " . $studio_id)->queryAll();
            foreach ($celeblist as $key => $celeb) {
                $celeb_image = $this->getPoster($celeb['id'], 'celebrity');
                $celeblist[$key]['content_id'] = $celeb['id'];
                $celeblist[$key]['celeb_image'] = $celeb_image;
            }
            $this->render('listitem', array('celeblist' => $celeblist));
            exit;
        } else if ($celb) {
                        if ($_SERVER['HTTP_X_PJAX'] == true) {
                            $this->layout = false;
                        } else {
                            $this->layout = 'main';
                        }
			$celeb_name = $celb->name;
			$meta = Yii::app()->common->castpagemeta($celeb_name);
			$this->pageTitle = $meta['title'];
			$this->pageDescription = $meta['description'];
			$this->pageKeywords = $meta['keywords'];
			if($celb->parent_id > 0){
				$celb->id = $celb->parent_id;
			}
			$celeb_image = $this->getPoster($celb->id, 'celebrity', 'medium');

			$contents = array();
			$cast_sql = "select films.id from films inner join movie_streams on films.id = movie_streams.movie_id inner join movie_casts on films.id = movie_casts.movie_id where movie_casts.celebrity_id = ".$celb->id." AND (movie_casts.movie_id IS NOT NULL and films.id IS NOT NULL and  movie_streams.studio_id = ".STUDIO_USER_ID." ) GROUP BY films.id,films.release_date ORDER BY films.release_date desc";
			$movies = $connection->createCommand($cast_sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
			if(count($movies) > 0){
				foreach($movies as $movie){
					$contents[] = Yii::app()->general->getContentData($movie->id);
				}
			}
                        $cast_sql1 = "select pg_product.id from pg_product inner join movie_casts on pg_product.id = movie_casts.product_id where movie_casts.celebrity_id = ".$celb->id." AND (movie_casts.product_id IS NOT NULL and pg_product.id IS NOT NULL and  pg_product.studio_id = ".STUDIO_USER_ID." ) GROUP BY pg_product.id ORDER BY  pg_product.id desc";
			$pgmovies = $connection->createCommand($cast_sql1)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
			if(count($pgmovies) > 0){
				foreach($pgmovies as $movie1){
                                    $product = Yii::app()->db->createCommand("SELECT id,name as title,permalink,sale_price,currency_id,is_preorder,status FROM pg_product WHERE id=" . $movie1->id)->queryROW();
                                    if (Yii::app()->common->isGeoBlockPGContent($product["id"])) {//Auther manas@muvi.com
                                        $product['permalink'] = Yii::app()->getbaseUrl(true) . '/' . $product['permalink'];
                                        $product['poster'] = PGProduct::getpgImage($product['id'], 'standard');
                                        if ($product['poster'] == '/img/no-image.jpg') {
                                            $product['poster'] = '/img/No-Image-Vertical.png';
                                        }
                                        $contents[] = $product;
                                    }
				}
			}                        

                        $data = array(
                                'id' => $celb->id,
                                'name' => $celb->name,
                                'birthdate' => $celb->birthdate,
                                'birthplace' => $celb->birthplace,
                                'summary' => utf8_encode($celb->summary),
                        );
                        $this->render('show', array('item' => json_encode($data), 'celebrity_image' => $celeb_image, 'contents' => json_encode($contents)));
		}else{
			Yii::app()->user->setFlash('error', 'Oops! Invalid star');
			$this->redirect(Yii::app()->getBaseUrl(TRUE));
		}
    }
}
?>