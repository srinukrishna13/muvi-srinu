<?php
require_once 'Braintree/lib/Braintree.php';

class ApibraintreepaypalController extends Controller {

    /**
     * Constructor
     * @author Sunil Kund <sunil@muvi.com>
     */
    public function __construct() {
        
    }

    /**
     *
     * Initialize the api
     * @return Braintree Object
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() {
        $gateway_code = 'braintreepaypal';
        if ($this->IS_LIVE_API_PAYMENT_GATEWAY[$gateway_code] != 'sandbox') {
            \Braintree\Configuration::environment('production');
        } else {
            \Braintree\Configuration::environment('sandbox');
        }
        
        \Braintree\Configuration::merchantId($this->PAYMENT_GATEWAY_API_USER[$gateway_code]);
        \Braintree\Configuration::publicKey($this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code]);
        \Braintree\Configuration::privateKey($this->PAYMENT_GATEWAY_API_SIGNATURE[$gateway_code]);
    }
    
    /**
     * 
     * Validate the card and make a zero transaction
     * @param array $arg
     * @return json string
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCard($arg = array()) {
        self::initializePaymentGateway();
        if (isset($arg) && !empty($arg)) {
            $data = array();
            $data['amount'] = 0;
            $data['email'] = $arg['email'];
            $data['name'] = explode(' ',$arg['name']);
            if(isset($arg) && trim($arg['payment_method_nonce'])){
                try {
                    $result = Braintree_Customer::create([
                        'email' => $data['email'],
                        'firstName' => $data['name'][0],
                        'lastName' => $data['name'][1],
                        'paymentMethodNonce' => $arg['payment_method_nonce']
                    ]);
                    if ($result->success) {
                        $res['isSuccess'] = 1;
                        $res['card']['code'] = 200;
                        $res['card']['status'] = 'succeeded';
                        $res['card']['card_type'] = 'Paypal'; //Card type
                        $res['card']['card_last_fourdigit'] = $result->customer->paypalAccounts[0]->email;
                        $res['card']['profile_id'] = $result->customer->id;//Braintree customer id which is used for recurring subscription
                        $res['card']['token'] = $result->customer->paymentMethods[0]->token; //Braintree card id which is used to get card detail
                        $res['card']['response_text'] = serialize($result);
                    } else {
                        $error_message = '';
                        foreach($result->errors->deepAll() AS $error) {
                            $error_message = $error->code . ": " . $error->message;
                        }
                        $res['isSuccess'] = 0;
                        $res['code'] = 55;
                        $res['Message'] = $error_message;
                    }
                } catch (\Braintree\Exception $e) {
                    $res['isSuccess'] = 0;
                    $res['code'] = 55;
                    $res['Message'] = 'We are not able to process your credit card. Please try another card.';
                }
            }else if(isset($arg) && trim($arg['card_number'])) {
                $data['card_number'] = $arg['card_number'];
                $data['card_holder_name'] = $arg['card_name'];
                $data['exp_month'] = ($arg['exp_month'] < 10) ? '0' . $arg['exp_month'] : $arg['exp_month'];
                $data['exp_year'] = $arg['exp_year'];
                $data['exp'] = ($arg['exp_month'] < 10) ? '0' . $arg['exp_month'] . substr($arg['exp_year'], -2) : $arg['exp_month'] . substr($arg['exp_year'], -2);
                $data['cvv'] = $arg['cvv'];
                try {
                    $result = Braintree_Customer::create([
                        'email' => $data['email'],
                        'creditCard' => [
                                'cardholderName' => $data['card_holder_name'],
                                'cvv' => $data['cvv'],
                                'expirationMonth' => $data['exp_month'],
                                'expirationYear' => $data['exp_year'],
                                'number' => $data['card_number'],
                                'options' => [
                                        'verifyCard' => true
                                        //'verificationAmount'=>0
                                ]
                        ]
                    ]);
                    if ($result->success) {
                        $card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);
                        $res['isSuccess'] = 1;
                        $res['card']['code'] = 200;
                        $res['card']['status'] = 'succeeded';
                        $res['card']['profile_id'] = $result->customer->id;//Braintree customer id which is used for recurring subscription
                        $res['card']['card_type'] = $result->customer->paymentMethods[0]->cardType; //Card type
                        $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                        $res['card']['token'] = $result->customer->paymentMethods[0]->token; //Braintree card id which is used to get card detail
                        $res['card']['response_text'] = serialize($result);
                    } else {
                        $error_message = '';
                        foreach($result->errors->deepAll() AS $error) {
                            $error_message = $error->code . ": " . $error->message;
                        }
                        $res['isSuccess'] = 0;
                        $res['code'] = 55;
                        $res['Message'] = $error_message;
                    }
                } catch (\Braintree\Exception $e) {
                    $res['isSuccess'] = 0;
                    $res['code'] = 55;
                    $res['Message'] = 'We are not able to process your credit card. Please try another card.';
                }
            }else{
                $res['isSuccess'] = 0;
                $res['code'] = 55;
                $res['Message'] = 'We are not able to process your credit card. Please try another card.';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        return json_encode($res);
    }
    
    /**
     * 
     * Make tranasction
     * @param object $card_info, object $user_sub
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function processTransaction($card_info, $user_sub) {
        self::initializePaymentGateway();
        
        /*added by @Sanjeev for multi currency
         * start
         * Getting currency details
        */
        $currency = Currency::model()->findByPk($user_sub->currency_id);
        $merchantAccountIds = json_decode($this->API_ADDONS['braintreepaypal'], true);
        if(isset($merchantAccountIds) && !empty($merchantAccountIds)){
            $merchantAccountId = $merchantAccountIds[$currency->code];
        }else{
            $merchantAccountId = '';
        }
        /*end*/
            
        $res = array();
        try {
            $cust_id = $user_sub->profile_id;
            if(isset($merchantAccountId) && trim($merchantAccountId)){
                $charge = Braintree_Transaction::sale([
                    'customerId' => $cust_id,
                    'merchantAccountId' => $merchantAccountId, //multi currency merchant id
                    'amount' => $user_sub->amount,
                    'options' => [
                        'submitForSettlement' => true
                    ]
                ]);
            }else{
                $charge = Braintree_Transaction::sale([
                    'customerId' => $cust_id,
                    'amount' => $user_sub->amount,
                    'options' => [
                        'submitForSettlement' => true
                    ]
                ]);
            }
            
            
            if ($charge->success) {
                $res['is_success'] = 1;
                $res['transaction_status'] = $charge->transaction->status;
                $res['invoice_id'] = $charge->transaction->id;
                $res['order_number'] = $charge->transaction->id;
                $res['amount'] = $charge->transaction->amount;
                $res['paid_amount'] = $charge->transaction->amount;
                $res['response_text'] = serialize($charge);
            } else {
                $error_message = '';
                foreach($charge->errors->deepAll() AS $error) {
                    $error_message = $error->code . ": " . $error->message;
                }
                
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = $error_message;
            }
        } catch (Exception $e) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($e);
        }
        
        return $res;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $user
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function processTransactions($user) {
        self::initializePaymentGateway();
        
        $currency = $user['currency_code'];
        
        /*added by @Sanjeev for multi currency
         * start
        */
        $merchantAccountIds = json_decode($this->API_ADDONS['braintreepaypal'], true);
        if(isset($merchantAccountIds) && !empty($merchantAccountIds)){
            $merchantAccountId = $merchantAccountIds[$currency];
        }else{
            $merchantAccountId = '';
        }
        /*end*/
        
        $amount = $user['amount'];
        $res = array();
        try {
            if(isset($merchantAccountId) && trim($merchantAccountId)) {
                $cust_id = $user['profile_id'];
                $charge = Braintree_Transaction::sale([
                    'customerId' => $cust_id,
                    'merchantAccountId' => $merchantAccountId, //multi currency merchant id
                    'amount' => $amount,
                    'options' => [
                        'submitForSettlement' => true
                    ]
                ]);
            }else{
                $cust_id = $user['profile_id'];
                $charge = Braintree_Transaction::sale([
                    'customerId' => $cust_id,
                    'amount' => $amount,
                    'options' => [
                        'submitForSettlement' => true
                    ]
                ]);
            }
            
            
            if ($charge->success) {
                $res['is_success'] = 1;
                $res['transaction_status'] = $charge->transaction->status;
                $res['invoice_id'] = $charge->transaction->id;
                $res['order_number'] = $charge->transaction->id;
                $res['amount'] = $user['amount'];
                $res['paid_amount'] = $charge->transaction->amount;
                $res['dollar_amount'] = $user['dollar_amount'];
                $res['currency_code'] = $user['currency_code'];
                $res['currency_symbol'] = $user['currency_symbol'];
                $res['response_text'] = serialize($charge);
            } else {
                $error_message = '';
                foreach($charge->errors->deepAll() AS $error) {
                    $error_message = $error->code . ": " . $error->message;
                }
                
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = $error_message;
            }
        } catch (Exception $e) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($e);
        }
        
        return $res;
    }
    
    /**
     * 
     * Cancel Customer's Account
     * @param object $usersub
     * @return object
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        self::initializePaymentGateway();

        $data = '';
        if (isset($usersub->profile_id) && !empty($usersub->profile_id)) {
            try {
                Braintree_Customer::delete($usersub->profile_id);
            } catch (Exception $e) {
                
            }
        }
        
        return $data;
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function saveCard($usersub, $arg) {
        self::initializePaymentGateway();
        
        try {
            $card = Braintree_CreditCard::create([
                'customerId' => $usersub->profile_id,
                'cardholderName' => $arg['card_name'],
                'number' => $arg['card_number'],
                'expirationMonth' => $arg['exp_month'],
                'expirationYear' => $arg['exp_year'],
                'cvv' => $arg['cvv']
            ]);

            if ($card->success) {
                $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);

                $res['isSuccess'] = 1;
                $res['card']['code'] = 200;
                $res['card']['status'] = 'succeeded';
                $res['card']['card_type'] = $card->creditCard->cardType; //Card type
                $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                $res['card']['token'] = $card->creditCard->token; //Braintree card id which is used to get card detail
                $res['card']['response_text'] = serialize($card);

            } else {
                $error_message = '';
                foreach($card->errors->deepAll() AS $error) {
                    $error_message = $error->code . ": " . $error->message;
                }

                $res['isSuccess'] = 0;
                $res['code'] = 55;
                $res['Message'] = $error_message;
            }
        } catch (Exception $e) {
            $res['isSuccess'] = 0;
            $res['code'] = 404;
            $res['response_text'] = serialize($e);
            $res['Message'] = 'Invalid Card details entered. Please check again';
        }

        return $res;
    }
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function defaultCard($usersub, $card , $gateway_code = 'braintreepaypal') {
        self::initializePaymentGateway();
        
        if (isset($card->token) && !empty($card->token)) {
            try {
                $updateCard = Braintree_CreditCard::update($card->token, [
                    'options' => [
                        'verifyCard' => true,
                        'makeDefault' => true
                    ]
                ]);

                if ($updateCard->success) {
                    $res['isSuccess'] = 1;
                } else {
                    $res['isSuccess'] = 0;
                    $res['code'] = 404;
                    $res['response_text'] = '';
                    $res['Message'] = 'Invalid Card details entered. Please check again';
                }
            } catch (\Braintree\Exception $e) {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($e);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        return json_encode($res);
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function deleteCard($usersub, $card = Null,$gateway_code = 'stripe') {
        self::initializePaymentGateway();
        
        if (isset($card) && !empty($card)) {
            try {
                $cu = Braintree_CreditCard::delete($card->token);
                
                $res['isSuccess'] = 1;
            } catch (Exception $e) {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($e);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        }
        
        return json_encode($res);
    }
    
    /**
     * 
     * Update user profile
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function updatePaymentProfile($user = array(),$sub = array(),$email) {
        $res = array();
        if(isset($sub) && !empty($sub)){
            self::initializePaymentGateway();
            try {
                $customer = Braintree_Customer::update(
                        $sub->profile_id,
                        [
                            'email' => $email
                        ]
                );
                
                if ($customer->success) {
                    $res['isSuccess'] = 1;
                } else {
                    $res['isSuccess'] = 0;
                    $res['error_message'] = 'We are not able to update email now. Please try after sometime.';
                }
            } catch (\Braintree\Exception $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = serialize($e);
            }
        }else{
            $res['isSuccess'] = 0;
            $res['error_message'] = 'We are not able to update email now. Please try after sometime.';
        }
        return $res;
    }
    
    /**
     * 
     * Sample Integration Transaction
     * @param user
     * @return array
     * @author Sunil Kund <sunil@muvi.com>, Sanjeev Malla <sanjeev@muvi.com>
     */
    function sampleIntegrationTransaction($user = array()) {
        $res = array();
        
        if (isset($user['card_number']) && !empty($user['card_number'])) {
            self::initializePaymentGateway();
            
            try {
                $result = Braintree_Customer::create([
                    'email' => $user['email'],
                    'creditCard' => [
                            'cardholderName' => $user['card_name'],
                            'cvv' => $user['cvv'],
                            'expirationMonth' => $user['exp_month'],
                            'expirationYear' => $user['exp_year'],
                            'number' => $user['card_number'],
                            'options' => [
                                    'verifyCard' => true,
                                    'verificationAmount'=>0
                            ]
                    ]
                ]);
            
                if ($result->success) {
                    $cust_id = $result->customer->id;
                    $charge = Braintree_Transaction::sale([
                        'customerId' => $cust_id,
                        'amount' => $user['amount'],
                        'options' => [
                            'submitForSettlement' => true
                        ]
                    ]);

                    if ($charge->success) {
                        $res['isSuccess'] = 1;
                    } else {
                        $error_message = '';
                        foreach($charge->errors->deepAll() AS $error) {
                            $error_message = $error->code . ": " . $error->message;
                        }
                        
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error_message;
                    }
                } else {
                    $error_message = '';
                    foreach($result->errors->deepAll() AS $error) {
                        $error_message = $error->code . ": " . $error->message;
                    }
                    
                    $res['isSuccess'] = 0;
                    $res['error_message'] = $error_message;
                }
            } catch (\Braintree\Braintree_Exception_Authentication $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. API keys are incorrect.";
            }catch (\Braintree\Braintree_Exception_Authorization $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. you are not authorized to perform the attempted action.";
            }catch (\Braintree\Braintree_Exception_Configuration $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Make sure you have environment, merchantId, publicKey, and privateKey..";
            }catch (\Braintree\Braintree_Exception_DownForMaintenance $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Request times out.";
            }catch (\Braintree\Timeout $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Timeout.";
            }catch (\Braintree\Braintree_Exception_ForgedQueryString $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Invalid hash in the query string..";
            }catch (\Braintree\Braintree_Exception_InvalidChallenge $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Invalid data format.";
            }catch (\Braintree\Braintree_Exception_InvalidSignature $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. you attempt to parse has an invalid signature.";
            }catch (\Braintree\Braintree_Exception_NotFound $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. The record that you're trying to operate on cannot be found.";
            }catch (\Braintree\Braintree_Exception_ServerError $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Something goes wrong on the Braintree server when trying to process your request.";
            }catch (\Braintree\Braintree_Exception_SSLCertificate $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. The client library cannot verify the server's TLS/SSL certificate.";
            }catch (\Braintree\Braintree_Exception_Unexpected $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. An error occurs that the client library is not built to handle.";
            }catch (\Braintree\Braintree_Exception_TooManyRequests $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Requests associated with your account reach unsafe levels.";
            }catch (\Braintree\Braintree_Exception_UpgradeRequired $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. You're trying to use a version of the library that is no longer supported.";
            }catch (\Braintree\Exception $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to process your credit card. Internal server error.";
            }
        } else {
            $res['isSuccess'] = 0;
            $res['error_message'] = 'Invalid credit card number';
        }
        
        return $res;
    }
    
    function getCustomerDetails($customer_id) {
        $res = array();
        if(isset($customer_id) && trim($customer_id)) {
            self::initializePaymentGateway();
            try{
                $res = Braintree_Customer::find($customer_id);
            } catch (\Braintree\Exception $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "We are not able to find the customer.";
            }
            return $res;
        }
        
    }
}

?>