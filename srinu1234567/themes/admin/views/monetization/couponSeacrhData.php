<?php
//print_r($data);exit;
?>
<table class="table table-hover" id="coupon_table">
                <thead>
                    <?php if ($items_count >= 20) { ?>
                        <tr>
                            <td colspan="8" class="text-right">
                                <?php
//                                $opts = array('class' => 'pagination m-t-0 m-b-40');
//                                $this->widget('CLinkPager', array(
//                                    'currentPage' => $pages->getCurrentPage(),
//                                    'itemCount' => $items_count,
//                                    'pageSize' => $page_size,
//                                    "htmlOptions" => $opts,
//                                    'maxButtonCount' => 6,
//                                    'nextPageLabel' => '&raquo;',
//                                    'prevPageLabel' => '&laquo;',
//                                    'selectedPageCssClass' => 'active',
//                                    'lastPageLabel' => '',
//                                    'firstPageLabel' => '',
//                                    'header' => '',
//                                ));
                                ?>

                            </td>
                        </tr>
                    <?php }
                    ?>
                    <tr>
                        <th class="max-width-40"><div class="checkbox m-b-0"><label><input class="chkall" id="selectall" type="checkbox" name="checkall" onclick="checkUncheckAll();" /><i class="input-helper"></i> </label></div></th>
                <th>Coupon</th>
                <th>Coupon Type</th>

<!--                <th data-hide="phone">Used by a single user</th>-->
                <th data-hide="phone">Valid?</th> 
                <th data-hide="phone">Used?</th>
                <th data-hide="phone">User</th>
                <th data-hide="phone">Used Date</th>
                </tr>
                </thead>
                <tbody>

                    <?php
                    //$first = $cnt = 1;
                    if ($data) {
                        //$first = $cnt = (($pages->getCurrentPage()) * 20) + 1;
                        foreach ($data as $key => $value) {
                            $cnt++;
                            $ccode = $value['coupon_code']; //exit;
                            ?>
                            <tr>
                                <?php //echo $value['id']; ?>
                        <input type="hidden" name='studio_id' value='<?php echo $value['studio_id']; ?>' />
                        <td>
                            <div class="checkbox m-b-0">
                                <label>
                                    <input class="chkind" type="checkbox" name="data[]" value="<?php echo $value['id']; ?>" onclick="checkUncheckAll(1);" />
                                    <i class="input-helper"></i> 
                                </label>
                            </div>
                        </td>

                        <td style="color:#33CCCC;"> <a href="#" data-toggle="modal" data-target="#myModal1" onclick="Coupon_history('<?php echo $ccode; ?>')" name="coupon_history"><?php echo $value['coupon_code']; ?></a></td>
                        <?php
                        echo "<td>";
                        if ($value['coupon_type'] == 1) {
                            echo "Multi-use";
                        } else {
                            echo "Once-use";
                        }
                        echo "</td>";
//                        echo "<td>";
//                        if ($value['user_can_use'] == 1) {
//                            echo "Multiple times";
//                        } else {
//                            echo "Once";
//                        }
//                        echo "</td>";
                        ?>
                        <?php
                        $now = strtotime(date("Y-m-d")); // or your date as well
                        $cpn_date = strtotime($value['valid_until']);
                        $datediff = $cpn_date - $now;
                        $diff;
                        if (($value['used_by'] == 0 && $datediff >= 0) || ($value['coupon_type'] == 1 && $datediff >= 0)) {
                            ?>
                            <td><?php echo 'Yes'; ?></td>
                            <?php
                        } else {
                            ?>
                            <td><?php echo 'No'; ?></td>
                            <?php
                        }
                        if ($value['used_by'] != 0) {
                            ?>
                            <td><?php echo 'Yes'; ?></td>
                        <?php } else { ?>
                            <td><?php echo '-'; ?></td>
                        <?php } ?>  

                        <?php if ($value['used_by'] == '0') { ?>
                            <td><?php echo '-'; ?></td>
                            <?php
                        } else if ($value['used_by'] != '0' && $value['coupon_type'] == 1) {
                            echo "<td><a href='#' data-toggle='modal' data-target='#showUserList' onclick='showUserEmail(" . $value['id'] . ");'>Show User List</span></td>";
                        } else {
                            ?>
                            <td><?php echo Yii::app()->common->getuseremail($value['used_by']); ?></td>
                            <?php } ?>
                        <?php if ($value['used_by'] == 0) { ?>
                            <td><?php echo '-'; ?></td>
                        <?php } else { ?>
                            <td><?php echo date('M jS, Y', strtotime($value['used_date'])); ?></td>
                        <?php }
                        ?>


                        </tr>
                        <!-- /.box-header -->

                    <?php }
                } else {
                    ?>
                    <tr>
                        <td colspan="5">No Record found!!!</td>
                    </tr>	
                <?php } ?>
                </tbody>
            </table>