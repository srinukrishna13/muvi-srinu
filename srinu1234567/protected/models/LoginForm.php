<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	//public $username;
	public $email;
	public $password;
        public $mobile_number;
	public $rememberMe;
	private $_identity;
        public $chklogin;
        public $optenable;
        public $errorCode;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
    protected function afterValidate() {
        $this->password = $this->encrypt($this->password);
        return parent::afterValidate();
    }

    public function encrypt($value) {
        $enc = new bCrypt();
        return $enc->hash($value);
    }

    public function rules() {
            return array(
                // username and password are required
                //array('email, password', 'required'),
                array('email', 'validateRequireEmail'),
                array('mobile_number', 'validateRequireMobile'),
                array('password', 'paswordRequire'),
                // rememberMe needs to be a boolean
                array('rememberMe', 'boolean'),
                // password needs to be authenticated
                array('password', 'authenticate'),
                array('email', 'email'),
            );
        }

    /**
	 * Declares attribute labels.
	 */
    public function attributeLabels() {
        return array(
            'rememberMe' => 'Remember me on this computer',
        );
    }
    public function paswordRequire($attribute, $params) {
        $otp = Yii::app()->controller->otp_enable;
        if ($otp != 1 && empty($this->$attribute)) {
            $this->addError('password', "Password shouldn't be left blank.");
        }
    }

    public function validateRequireEmail($attribute, $params) {
        $this->chklogin = Yii::app()->controller->login_with;
        if ($this->chklogin != 3 && empty($this->$attribute)) {
            $this->addError('email', "Email Shouldn't be left blank.");
        }
    }

    public function validateRequireMobile($attribute, $params) {
        $this->chklogin = Yii::app()->controller->login_with;
        if ($this->chklogin == 3 && empty($this->$attribute)) {
            $this->addError('mobile_number', "Mobile number Shouldn't be left blank.");
        }
    }        

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            if (!$this->_identity->authenticate()) {
                if ($this->errorCode == 3) {
                    $this->addError('OTP', '3');
                } else if ($this->errorCode == 4) {
                    $this->addError('OTP', '4');
                }else if($this->errorCode == 5){
                    $this->addError('OTP', '5');
                }else {
                    $this->addError('password', 'Incorrect username or password.');
		}
            }
        }
    }
               
	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 3600 * 6;
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else
            return false;
    }

    public function authenticateuser($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            if (!$this->_identity->validate())
                $this->addError('login', 'failure');
        }
    }

    public function loginuser($data) {
        $username = $this->email;
        $chkLogin = 1; // email
        if ($data['optenable'] == 1 || $data['optenable'] == 2) {
            if ($this->_identity === null) {
                $this->_identity = new UserIdentity();
                $this->_identity->validateOTP($data);
            }
        } else {
            if ($this->mobile_number) {
                $username = $this->mobile_number;
                $chkLogin = 3; // mobile number
            }
            if ($this->_identity === null) {
                $this->_identity = new UserIdentity($username, $this->password);
                $this->_identity->validate($chkLogin);
            }
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 3600 * 6; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else {
            $this->errorCode = $this->_identity->errorCode;
            return $errorCode;
        }
    }

    public function loginpartner()
	{          
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->email,$this->password);
			$this->_identity->authenticatepartner();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}  
        public function loginreseller()
	{          
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->email,$this->password);                        
                        $this->_identity->authenticatereseller();                            
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	} 
    public function loginresellerfromadmin() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticateResellerFromAdmin();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else
            return false;
    }   
        
	public function dostudiologin()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->email,$this->password);
			$this->_identity->authenticatestudio();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 3600*6; 
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	} 
        
	public function dostudioownerlogin()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->email,$this->password);
			$this->_identity->authenticatestudioowner();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 3600*6; 
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}         
/**
 * @method public loginWithFacebook() Login useing facebook
 * @return true/false 
 * @author Gayadhar<support@muvi.com>
 */
	public function LoginWithSocial($record,$type='facebook'){
		if($this->_identity===null){
			$this->_identity=new UserIdentity($record->email,$record->encrypted_password);
			if($type=='gplus'){
				$this->_identity->loginwithSocialData($record->studio_id,$record->gplus_userid,$record->email,'gplus');
			}else{
				$this->_identity->loginwithSocialData($record->studio_id,$record->fb_userid,$record->email,'facebook');
			}
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)	{
			$duration=$this->rememberMe ? 3600*24*30 : 3600*6; 
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}      
        /**
         * @author MRS <manas@muvi.com>
         * @desc this functin is for developer purpose
         * @return boolean
         */
        public function dopartnerlogin()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->email,$this->password);
			$this->_identity->authenticatepartnerfordev();
}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 3600*6; 
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
