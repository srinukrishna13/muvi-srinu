<div class="modal fade is-Large-Modal" id="addvideoforcontent"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Video - <span class="" id="pop_movie_name"></span></h4></div>
            <div class="modal-body">

                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a id="home-tab" href="#home"  aria-controls="Upload-Video" role="tab" data-toggle="tab">Upload Video</a>
                        </li>
                        <li role="presentation">
                            <a id="profile-tab"  href="#profile" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Library</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">


                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20">
                                    <div class="form-horizontal">	
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <label for="uploadVideo" class="control-label">Upload Method &nbsp;</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="hidden" value="?" name="utf8">
                                                <div class="fg-line">
                                                    <div class="select">
                                                    <select name="upload_type" class="filter_dropdown form-control" id="filetype">
                                                        <option value="browes">From Computer</option>
                                                        <option value="server">Server to Server Transfer</option>
                                                        <option value="dropbox">From Dropbox</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix m-b-20"></div>
                                            <div class="savefile" id="browes_div">
                                                <div class="col-md-2">&nbsp;</div>
                                                <div class="col-md-4">
                                                    <input type="button" value="Upload File" onclick="click_browsecontent('videofilecontent');" class="btn btn-default-with-bg btn-sm" >
                                                    <input type="file" name="file" style="display:none;" id="videofilecontent" required onchange="checkfileSizeContent();" >
                                                </div>
                                            </div>
                                            <div class="clear-fix"></div>
                                            <div class="savefile" id="server_div" style="display: none;">
                                                <div class="col-md-2">&nbsp;</div>
                                                <div class="col-md-4">
                                                    <div class="fg-line">
                                                        <input type="url" pattern="" value="" class="form-control input-sm" name="server_url" id="server_url" placeholder="Path to the file on your server" required>
                                                    </div>
                                                    <span class="error red help-block" id="server_url_error"></span>
                                                    <div style="width: 48%;float: left;">
                                                        <input type="text" name="username"  id="ftpusername" class="form-control input-sm" placeholder='Username if any'>
                                                        <label id="ftpusername-error" class="error" for="ftpusername" style="display: inline-block;"></label>
                                                    </div>
                                                    <div style="width: 48%;float: right;">
                                                        <input type="text" name="password" id="ftppassword" class="form-control input-sm" placeholder='Password if any'>
                                                        <label id="ftppassword-error" class="error" for="ftppassword"></label>
                                                    </div>
                                                    <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('');">Submit</button>
                                                </div>
                                            </div>
                                            <div class="savefile" id="dropbox_div" style="display: none;">
                                                <div class="col-md-2">&nbsp;</div>
                                                <div class="col-md-4">
                                                    <div class="fg-line">
                                                        <input type="text" name="dropbox" id="dropbox" class="form-control" placeholder="Path to the file on dropbox">
                                                    </div>
                                                    <span class="error red help-block" id="dropbox_url_error"></span>
                                                    <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('dropbox_url_error');">Submit</button>
                                                </div>
                                            </div>
                                            <div class="clear-fix"></div> 
                                        </div>
                                        <div class="form-group m-t-20">
                                            <div class="col-md-12">
                                                <div class="red">Muvi recommends S3 Sync and FTP for faster upload</div>
                                                <ul style="padding-left: 12px;">
                                                    <li>
                                                        <span style="font-weight: bold;">S3 Sync:</span>
                                                        Install a tool we provide in your server. Videos in the server will be automatically synced with Muvi's Video Library
                                                    </li>
                                                    <li>
                                                        <span style="font-weight: bold;">FTP:</span>
                                                        Use a traditional FTP client to upload to Muvi's Video Library
                                                    </li>
                                                </ul>
                                                See <a target="_blank" href="https://www.muvi.com/help/uploading-videos#Muvi_Recommended">help article</a> for more details. Please add a <a href="<?php  echo  Yii::app()->getbaseUrl(true)."/ticket/ticketList";  ?>">support ticket</a> to use one of the above tools.
                                            </div>
                                        </div>
                                    </div>		
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="row m-b-20 m-t-20">
                                <div class="col-sm-3">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                                        <div class="fg-line">
                                            <input type="text" id="search_video1" class="form-control fg-input input-sm" placeholder="What are you searching for?" onkeyup="searchvideo1();">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group input-group">

                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="video_duration" id="video_duration" onchange="view_filtered_list();">
                                                    <option value="0">Video Duration</option>
                                                    <option  value="1">Less than 5 minutes</option>
                                                    <option  value="2">Less than 30 minutes</option>
                                                    <option  value="3">Less than 120 minutes</option>
                                                    <option  value="4">More than 120 minutes</option>                          
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="form-group">

                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="file_size" id="file_size" onchange="view_filtered_list();">
                                                    <option value="0">File Size</option>
                                                    <option value="1">Less than 1GB</option>
                                                    <option value="2">Less than 10GB</option>
                                                    <option value="3">More than 10GB</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="form-group ">

                                        <div class="fg-line">
                                            <div class="select" >
                                                <select class="form-control" name="is_encoded" id="is_encoded" onchange="view_filtered_list();">
                                                    <option value="0"> Encoded ???</option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="form-group ">

                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="uploaded_in" id="uploaded_in" onchange="view_filtered_list();">
                                                    <option value="0">Uploaded In</option>
                                                    <option value="1">This Week</option>
                                                    <option value="2">This Month</option>
                                                    <option value="3">This Year</option>
                                                    <option value="4">Before This Year</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-1">
                                    <div class="form-group ">

                                        <div class="fg-line">
                                            <a href="javascript:void(0);" class="m-t-10 btn btn-default" onclick="reset_filter()"> Reset </a>
                                        </div>
                                    </div>
                                </div>  


                            </div>
                            <div class="col-md-12">
                                <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                    <div class="preloader pls-blue text-center " >
                                        <svg class="pl-circular" viewBox="25 25 50 50">
                                        <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="row  Gallery-Row">
                                <div class="col-md-12 is-Scrollable-has-search p-t-40 p-b-40" id="video_newcontent_div">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>
    </div>
</div>