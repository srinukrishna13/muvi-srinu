<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>" />
        <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
		<link rel="icon" type="image/*" href="<?php echo $this->favIcon;?>" />
        <link rel="canonical" href="<?php echo Yii::app()->getbaseUrl(true) . Yii::app()->request->url ?>" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) { document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>'); }
        </script>
        <?php
        if ($this->studio->google_analytics != '') {
            echo html_entity_decode($this->studio->google_analytics);
        }
        ?>
        <script type="text/javascript">
            var HTTP_ROOT = "<?php echo Yii::app()->getBaseUrl(TRUE); ?>";
            var THEME_URL = "<?php echo Yii::app()->theme->baseUrl; ?>";
            var website_url = "<?php echo Yii::app()->getBaseUrl(TRUE); ?>";
        </script>
    </head>

    <body>   
        <?php echo $content; ?>
    </body>
</html>
