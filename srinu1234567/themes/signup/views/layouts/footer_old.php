<div class="bluebg">
    <div class="container home-page-customers">
        <h3 class="btm-bdr gry">Muvi takes care of all things technical so that you can focus on business activities</h3>
        <ul>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-1.png" alt="Maaflix" title="Maaflix"><br>Cloud Hosting</li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-2.png" title="Iskcon"><br>Video Encoding</li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-3.png" alt="Funkara" title="Funkara"><br>DRM</li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-4.png" alt="Fountain" title="Fountain"><br>Multi-platform</li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-5.png" alt="SEPL" title="SEPL"><br>Template</li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-6.png" alt="Movie Tee Vee" title="Movie Tee Vee"><br>Payment Gateway</li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon-7.png" alt="Pocket Films" title="Pocket Films"><br>Analytics</li>                        
        </ul>
    </div>
</div>