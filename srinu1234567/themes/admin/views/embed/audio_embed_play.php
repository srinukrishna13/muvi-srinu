<!DOCTYPE html>
<?php 
    $command1 = Yii::app()->db->createCommand()
	      ->select('id,theme')
              ->from('studios')
              ->where('id=' . $studio_id);
    $studio = $command1->QueryAll();
    $theme_name = $studio[0]['theme'];
    $theme_path = Yii::app()->getBaseUrl(true) . '/themes/' . $theme_name ;
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var THEME_URL = "/themes/<?php echo $theme_name ?>";
            var hybrid = 0;
            var remove = "";
            var embed_studio_id = "<?php echo $studio_id; ?>";
            var STORE_AUTH_TOKEN = "<?php echo OuathRegistration::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'oauth_token'))->oauth_token; ?>";
            var movie_id = "<?php echo $movie_id; ?>";
            var stream_id = "<?php echo $stream_id; ?>";
        </script>
        <link rel="stylesheet" href="<?php echo $theme_path;?>/css/bootstrap.min.css" >   
        <link rel="stylesheet" href="<?php echo $theme_path;?>/vendor/custom-scrollbar/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="<?php echo $theme_path;?>/css/main.css" >
        <link rel="stylesheet" href="<?php echo $theme_path;?>/css/responsive.css" >
        <link rel="stylesheet" href="<?php echo $theme_path;?>/css/style.css" >
        <link rel="stylesheet" href="<?php echo $theme_path;?>/css/player.css" >
        <link rel="stylesheet" href="<?php echo $theme_path;?>/css/audiostream.css" >
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    </head>
    <body style="background:white">

        <input type="hidden" value="" id="favourite_enable">
        <input type="hidden" value="" id="playlist_enable">
        <input type="hidden" value="" id="autoplay_episode">

        <ul id="playlist" class="hidden">
            <li class="active" id="<?php echo $stream_id; ?>" movie_id="<?php echo $movie_id;?>" content_types ="<?php echo $content_type;?>"></li>
        </ul>
        <div class="ap" id="ap" style="background: ">
            <div class="ap__inner">
                <!-- play buton area -->
                <div class="ap__item ap__item--playback">
                    <div class="art">
                        <div class="card">
                            <a href="#">
                                <figure>
                                    <img src="" class="imag_album">
                                </figure>

                            </a>
                        </div>
                        <div class="art-desc">
                            <h6></h6>
                            <p class="meta-info"></p>
                        </div>
                    </div>
                </div>
                <!-- ./ play buton area end-->

                <!-- progress-bar -->
                <div class="ap__item ap__item--track">
                    <div class="play-section">
                        <button class="ap__controls ap__controls--prev">
                            <svg version="1.1" fill="#7e7c8a" width="16" height="16" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-322 124 314 314" style="enable-background:new -322 124 314 314;" xml:space="preserve">

                            <g>
                            <path class="st2" d="M-45.2,434.1l-233.1-134.6c-7.9-4.6-12.4-11.3-12.4-18.5c0-7.2,4.5-14,12.4-18.5l233.1-134.6
                                  c4.4-2.6,8.9-3.9,13.1-3.9c13.1,0,19,11.3,19,22.4v269.2c0,11.2-5.9,22.4-19,22.4C-36.4,438-40.8,436.7-45.2,434.1z M-38.2,140
                                  l-233.1,134.6c-3.4,2-5.4,4.4-5.4,6.4s2,4.5,5.4,6.4L-38.2,422c2.9,1.6,4.9,2,6.1,2c4.4,0,5-5.3,5-8.4V146.4c0-3.1-0.7-8.4-5-8.4
                                  C-33.3,138-35.4,138.3-38.2,140z" />
                            </g>
                            <g>
                            <path class="st2" d="M-303.8,131.9v298.2c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9V131.9c0-4.4-3.5-7.9-7.9-7.9
                                  C-300.3,124-303.8,127.5-303.8,131.9z" />
                            </g>
                            </svg>
                        </button>

                        <button class="ap__controls ap__controls--toggle">
                            <svg class="icon-play" version="1.1" baseProfile="tiny" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="-2375.8 1969 277.6 314" xml:space="preserve" width="16" height="16" data-play="M-2356.8,2285.6c-13.1,0-19-11.3-19-22.4V1994c0-11.2,5.9-22.4,19-22.4c4.3,0,8.7,1.3,13.1,3.9l233.1,134.6
                                 c7.9,4.6,12.4,11.3,12.4,18.5s-4.5,14-12.4,18.5l-233.1,134.6C-2348.1,2284.3-2352.6,2285.6-2356.8,2285.6z M-2356.8,1985.6
                                 c-4.4,0-5,5.3-5,8.4v269.2c0,3.1,0.7,8.4,5,8.4c1.2,0,3.3-0.3,6.1-2l233.1-134.6c3.4-2,5.4-4.4,5.4-6.4s-2-4.5-5.4-6.4
                                 l-233.1-134.6C-2353.6,1985.9-2355.6,1985.6-2356.8,1985.6z">
                            <g>
                            <path fill="#7e7c8a" d="M-2356.8,2285.6c-13.1,0-19-11.3-19-22.4V1994c0-11.2,5.9-22.4,19-22.4c4.3,0,8.7,1.3,13.1,3.9l233.1,134.6
                                  c7.9,4.6,12.4,11.3,12.4,18.5s-4.5,14-12.4,18.5l-233.1,134.6C-2348.1,2284.3-2352.6,2285.6-2356.8,2285.6z M-2356.8,1985.6
                                  c-4.4,0-5,5.3-5,8.4v269.2c0,3.1,0.7,8.4,5,8.4c1.2,0,3.3-0.3,6.1-2l233.1-134.6c3.4-2,5.4-4.4,5.4-6.4s-2-4.5-5.4-6.4
                                  l-233.1-134.6C-2353.6,1985.9-2355.6,1985.6-2356.8,1985.6z" />

                            <path class="pause-btn" fill="#B64B38" d="M-2338.7,1969h-16.1c-16.8,0-30.5,13.7-30.5,30.5v253c0,16.8,13.7,30.5,30.5,30.5h16.1
                                  c16.8,0,30.5-13.7,30.5-30.5v-253C-2308.3,1982.6-2321.9,1969-2338.7,1969z M-2322.3,2252.5c0,9.1-7.4,16.5-16.5,16.5h-16.1
                                  c-9.1,0-16.5-7.4-16.5-16.5v-253c0-9.1,7.4-16.5,16.5-16.5h16.1c9.1,0,16.5,7.4,16.5,16.5L-2322.3,2252.5L-2322.3,2252.5z" />
                            <path class="pause-btn" fill="#B64B38" d="M-2191.5,1969h-16.1c-16.8,0-30.5,13.7-30.5,30.5v253c0,16.8,13.7,30.5,30.5,30.5h16.1
                                  c16.8,0,30.5-13.7,30.5-30.5v-253C-2161.1,1982.6-2174.7,1969-2191.5,1969z M-2175.1,2252.5c0,9.1-7.4,16.5-16.5,16.5h-16.1
                                  c-9.1,0-16.5-7.4-16.5-16.5v-253c0-9.1,7.4-16.5,16.5-16.5h16.1c9.1,0,16.5,7.4,16.5,16.5V2252.5z" />

                            </g>
                            </svg>
                        </button>

                        <button class="ap__controls ap__controls--next">
                            <svg version="1.1" fill="#7e7c8a" width="16" height="16" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-322 124 314 314" style="enable-background:new -322 124 314 314;" xml:space="preserve">

                            <g>
                            <path class="st2" d="M-284.8,438c-13.1,0-19-11.3-19-22.4V146.4c0-11.2,5.9-22.4,19-22.4c4.3,0,8.7,1.3,13.1,3.9l233.1,134.6
                                  c7.9,4.6,12.4,11.3,12.4,18.5s-4.5,14-12.4,18.5l-233.1,134.6C-276.1,436.7-280.6,438-284.8,438z M-284.8,138c-4.4,0-5,5.3-5,8.4
                                  v269.2c0,3.1,0.7,8.4,5,8.4c1.2,0,3.3-0.3,6.1-2l233.1-134.6c3.4-2,5.4-4.4,5.4-6.4s-2-4.5-5.4-6.4L-278.7,140
                                  C-281.6,138.3-283.6,138-284.8,138z" />
                            </g>
                            <g>
                            <path class="st2" d="M-13.1,131.9v298.2c0,4.4-3.5,7.9-7.9,7.9s-7.9-3.5-7.9-7.9V131.9c0-4.4,3.5-7.9,7.9-7.9
                                  S-13.1,127.5-13.1,131.9z" />
                            </g>
                            </svg>
                        </button>
                    </div>
                    <div class="track">

                        <!-- progress bar -->
                        <div class="progress-container">
                            <div class="progress">
                                <!-- timmer portion -->
                                <div class="track__time">
                                    <span class="track__time--current meta-info">0:00</span>
                                    <span class="track__time--duration meta-info">0:00</span>
                                </div>

                                <div class="progress__bar"></div>
                                <div class="progress__preload"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./ progress-bar -->

                <!-- player controles-->
                <div class="ap__item ap__item--settings">



                    <!--shuffle-->
                    <!--<button class="ap__controls ap__controls--shuffle">
                                    <img class="icon icons8-Shuffle shuffleimg" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABq0lEQVRoge2Ya5XDIBCFr4RIqIRKqIRKQEIk4KASKmUlVEIkREL6g3CSZYfHTCCQPXzn8KeP4c7NDAGATqfTCq/aAo6yAPgAGGoLkbKsYwZwr6xFxOKMsa4cPm4CCy7WF1QCor7wBbJjgnHmxoj5WP/zganx2BzuYPUFJ3BKnWqhaOl87KDPQKwxk3D7FIokMIMupwH5nJ8RNoqFIiZ4E7/ThAgFf0OGmviWR/qGIsTtJ6Hcjy2JlPgXCr2ZB5jVyPcUNMIJUojq/QjKmXRaP5e4D/wWn63eY+yfgl4/0+C7DxSs9xAKm8gBcveBgvUeY8Jx96vyxHH3m0Djgu5buvs1+bfuDzDngKYJuT+u3zWNht99+5ZWibFOL7uY+/ukUrBbidPexhr+lcfdqaqEePs4xe+FYivPG3+Ti+GeBYpupzX87gMmQa4g34EmK3ZpTFn33acQK6XQkTLrvZA7Qod6bqzQHEXuhRZsW2mKR8YkkvuCEzDlEJIriWz3QjOAH/C2CXeYnpgOiD/tnCwl1MS3erLSkZZoM4jqvSUuVe8Ul6p3ikvVe6fTCl8URNhtOU3WGAAAAABJRU5ErkJggg==" width="25" height="20"> 
                            </button>-->

                    <!--repeat-->
                    <button class="ap__controls ap__controls--repeat">
                        <svg version="1.1" width="20" fill="#7e7c8a" height="20" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-452 254 54 54" style="enable-background:new -452 254 54 54;" xml:space="preserve">

                        <g>
                        <path class="st2" d="M-400.8,275h-5.1c-0.8,0-1.5-0.4-1.8-1.2s-0.2-1.5,0.4-2.1l3.6-3.6c0.5-0.5,0.8-1.2,0.8-2c0-0.7-0.3-1.4-0.8-2
                              l-4.6-4.6c-1.1-1-2.9-1.1-3.9,0l-3.6,3.6c-0.6,0.6-1.4,0.7-2.1,0.4c-0.8-0.3-1.2-1-1.2-1.8v-5.1c0-1.5-1.2-2.8-2.8-2.8h-6.4
                              c-1.5,0-2.8,1.2-2.8,2.8v5.1c0,0.8-0.4,1.5-1.2,1.8c-0.7,0.3-1.5,0.2-2.1-0.4l-3.6-3.6c-1.1-1.1-2.9-1-3.9,0l-4.6,4.6
                              c-0.5,0.5-0.8,1.2-0.8,2c0,0.7,0.3,1.4,0.8,2l3.6,3.6c0.6,0.6,0.7,1.4,0.4,2.1s-1,1.2-1.8,1.2h-5.1c-1.5,0-2.8,1.2-2.8,2.8v6.4
                              c0,1.5,1.2,2.8,2.8,2.8h5.1c0.8,0,1.5,0.4,1.8,1.2s0.2,1.5-0.4,2.1l-3.6,3.6c-0.5,0.5-0.8,1.2-0.8,2c0,0.7,0.3,1.4,0.8,2l4.6,4.6
                              c1.1,1.1,2.9,1.1,3.9,0l3.6-3.6c0.6-0.6,1.4-0.7,2.1-0.4c0.8,0.3,1.2,1,1.2,1.8v5.1c0,1.5,1.2,2.8,2.8,2.8h6.4
                              c1.5,0,2.8-1.2,2.8-2.8v-5.1c0-0.8,0.4-1.5,1.2-1.8c0.8-0.3,1.5-0.2,2.1,0.4l3.6,3.6c1.1,1.1,2.9,1.1,3.9,0l4.6-4.6
                              c0.5-0.5,0.8-1.2,0.8-2c0-0.7-0.3-1.4-0.8-2l-3.6-3.6c-0.6-0.6-0.7-1.4-0.4-2.1s1-1.2,1.8-1.2h5.1c1.5,0,2.8-1.2,2.8-2.8v-6.4
                              C-398,276.2-399.2,275-400.8,275z M-400,284.2c0,0.4-0.4,0.8-0.8,0.8h-5.1c-1.6,0-3,0.9-3.6,2.4c-0.6,1.5-0.3,3.1,0.9,4.3l3.6,3.6
                              c0.3,0.3,0.3,0.8,0,1.1l-4.6,4.6c-0.3,0.3-0.8,0.3-1.1,0l-3.6-3.6c-1.1-1.1-2.8-1.5-4.3-0.9c-1.5,0.6-2.4,2-2.4,3.6v5.1
                              c0,0.4-0.4,0.8-0.8,0.8h-6.4c-0.4,0-0.8-0.4-0.8-0.8v-5.1c0-1.6-0.9-3-2.4-3.6c-0.5-0.2-1-0.3-1.5-0.3c-1,0-2,0.4-2.8,1.2l-3.6,3.6
                              c-0.3,0.3-0.8,0.3-1.1,0l-4.6-4.6c-0.3-0.3-0.3-0.8,0-1.1l3.6-3.6c1.1-1.1,1.5-2.8,0.9-4.3c-0.6-1.5-2-2.4-3.6-2.4h-5.1
                              c-0.4,0-0.8-0.4-0.8-0.8v-6.4c0-0.4,0.4-0.8,0.8-0.8h5.1c1.6,0,3-0.9,3.6-2.4c0.6-1.5,0.3-3.1-0.9-4.3l-3.6-3.6
                              c-0.3-0.3-0.3-0.8,0-1.1l4.6-4.6c0.3-0.3,0.8-0.3,1.1,0l3.6,3.6c1.1,1.1,2.8,1.5,4.3,0.9c1.5-0.6,2.4-2,2.4-3.6v-5.1
                              c0-0.4,0.4-0.8,0.8-0.8h6.4c0.4,0,0.8,0.4,0.8,0.8v5.1c0,1.6,0.9,3,2.4,3.6c1.5,0.6,3.1,0.3,4.3-0.9l3.6-3.6c0.3-0.3,0.8-0.3,1.1,0
                              l4.6,4.6c0.3,0.3,0.3,0.8,0,1.1l-3.6,3.6c-1.1,1.1-1.5,2.8-0.9,4.3c0.6,1.5,2,2.4,3.6,2.4h5.1c0.4,0,0.8,0.3,0.8,0.8V284.2z" />
                        <path class="st0" d="M-425,272c-5,0-9,4-9,9s4,9,9,9s9-4,9-9S-420,272-425,272z M-425,288c-3.9,0-7-3.1-7-7s3.1-7,7-7s7,3.1,7,7
                              S-421.1,288-425,288z" />
                        </g>
                        </svg>
                    </button>

                    <!--volume-->
                    <div class="ap__controls volume-container dn">
                        <button class="volume-btn">

                            <svg class="icon-volume-on" version="1.1" fill="#333" width="20" height="20" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-223 25 512 512" style="enable-background:new -223 25 512 512;" xml:space="preserve">
                            <g>
                            <path class="st2" d="M71.7,78.8c-3.4-2.1-7.6-2.3-11.1-0.5l-194.9,98.2h-58.6c-16.6,0-30.1,13.5-30.1,30.1v148.7
                                  c0,16.6,13.5,30.1,30.1,30.1h58.6l194.9,98.2c1.6,0.8,3.4,1.2,5.1,1.2c2.1,0,4.2-0.6,6-1.7c3.4-2.1,5.4-5.8,5.4-9.7v-385
                                  C77.2,84.5,75.1,80.9,71.7,78.8z M-200.2,355.3V206.7c0-4,3.3-7.3,7.3-7.3h49.9v163.3h-49.9C-196.8,362.6-200.2,359.3-200.2,355.3z
                                  M54.3,455L-120.1,367V195L54.3,107V455z" />
                            <path class="st0" d="M165.7,220.7c-4.5,4.4-4.6,11.6-0.1,16.1c11,11.2,17.3,27.3,17.3,44.1s-6.3,32.9-17.3,44.1
                                  c-4.4,4.5-4.4,11.7,0.1,16.1c2.2,2.2,5.1,3.3,8,3.3c3,0,5.9-1.1,8.1-3.4c15.2-15.4,23.9-37.4,23.9-60.1s-8.7-44.7-23.9-60.1
                                  C177.4,216.4,170.2,216.3,165.7,220.7z" />
                            <path class="st0" d="M252.4,188.8c-4.4-4.5-11.6-4.6-16.1-0.1c-4.5,4.4-4.6,11.6-0.1,16.1c19.1,19.4,30,47.2,30,76.2
                                  c0,29-10.9,56.8-30,76.2c-4.4,4.5-4.4,11.7,0.1,16.1c2.2,2.2,5.1,3.3,8,3.3c3,0,5.9-1.1,8.1-3.4C275.7,349.6,289,316,289,281
                                  C289,246,275.7,212.4,252.4,188.8z" />
                            </g>
                            </svg>


                            <svg class="icon-volume-off" version="1.1" width="20" height="20" baseProfile="tiny" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-2394 1969 314 314" xml:space="preserve">
                            <g>
                            <path fill="#7E7C8A" d="M-2216.5,1998.2c-2.1-1.3-4.7-1.4-6.8-0.3l-117.2,59.1h-35.2c-10.1,0-18.3,8.2-18.3,18.3v89.4
                                  c0,10.1,8.2,18.3,18.3,18.3h35.2l117.2,59.1c1,0.5,2.1,0.7,3.1,0.7c1.3,0,2.5-0.3,3.7-1c2.1-1.3,3.3-3.5,3.3-6v-231.6
                                  C-2213.1,2001.8-2214.4,1999.5-2216.5,1998.2z M-2380,2164.7v-89.4c0-2.3,2-4.3,4.3-4.3h29.9v98h-29.9
                                  C-2378,2169-2380,2167-2380,2164.7z M-2227.1,2224.5l-104.7-52.8v-103.4l104.7-52.8V2224.5L-2227.1,2224.5z" />
                            <path fill="#7E7C8A" d="M-2108.1,2120l26-26c2.7-2.7,2.7-7.2,0-9.9s-7.2-2.7-9.9,0l-26,26l-26-26c-2.7-2.7-7.2-2.7-9.9,0
                                  s-2.7,7.2,0,9.9l26,26l-26,26c-2.7,2.7-2.7,7.2,0,9.9c1.4,1.4,3.2,2,4.9,2c1.7,0,3.6-0.7,4.9-2.1l26-26l26,26
                                  c1.4,1.4,3.2,2.1,5,2.1s3.6-0.7,4.9-2c2.7-2.7,2.7-7.2,0-9.9L-2108.1,2120z" />
                            </g>
                            </svg>


                        </button>
                        <div class="volume">
                            <div class="volume__track">
                                <div class="volume__bar"></div>
                            </div>
                        </div>
                    </div>

                    <!--Share-->
                    <div class="ap__controls volume-container dn">
                        <button class="ap__controls ap__controls--playlist">

                            <svg version="1.1" width="20" height="20" id="Capa_9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-238 40.4 481.6 481.6" style="enable-background:new -238 40.4 481.6 481.6;" xml:space="preserve">

                            <g>
                            <path class="st2" d="M143.6,349.8c-27.7,0-52.4,13.2-68.2,33.6l-132.3-73.9c3.1-8.9,4.8-18.5,4.8-28.4c0-10-1.7-19.5-4.9-28.5
                                  l132.2-73.8c15.7,20.5,40.5,33.8,68.3,33.8c47.4,0,86.1-38.6,86.1-86.1S191,40.4,143.5,40.4S57.4,79,57.4,126.5
                                  c0,10,1.7,19.6,4.9,28.5l-132.1,73.8c-15.7-20.6-40.5-33.8-68.3-33.8c-47.4,0-86.1,38.6-86.1,86.1s38.7,86.1,86.2,86.1
                                  c27.8,0,52.6-13.3,68.4-33.9l132.2,73.9c-3.2,9-5,18.7-5,28.7c0,47.4,38.6,86.1,86.1,86.1s86.1-38.6,86.1-86.1
                                  S191.1,349.8,143.6,349.8z M143.6,67.5c32.6,0,59.1,26.5,59.1,59.1s-26.5,59.1-59.1,59.1s-59.1-26.5-59.1-59.1
                                  S111.1,67.5,143.6,67.5z M-138,340.2c-32.6,0-59.1-26.5-59.1-59.1S-170.6,222-138,222s59.1,26.5,59.1,59.1S-105.5,340.2-138,340.2z
                                  M143.6,494.9c-32.6,0-59.1-26.5-59.1-59.1s26.5-59.1,59.1-59.1s59.1,26.5,59.1,59.1S176.2,494.9,143.6,494.9z" />
                            </g>
                            </svg>
                        </button>

                        <div class="volume shareicon">
                            <p>Share In</p>
                            <ul class="list-inline">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo Yii::app()->getbaseUrl(true); ?>"><img src="<?php echo $theme_path; ?>/images/Share-FB.png" height="30" width="30"></a></li>
                                <li><a href="https://twitter.com/intent/tweet?text=<?php echo Yii::app()->getbaseUrl(true); ?>"><img src="<?php echo $theme_path; ?>/images/Share-Twitter.png" height="30" width="30"></a></li>
                                <!-----<li><img src="{$this->siteurl}{$Yii->theme->baseUrl}/images/Share-Google-Plus.png" height="30" width="30"></li>----->
                            </ul>
                        </div>

                    </div>

                </div>

            </div>
            <!-- ./player controles ends -->
        </div>

        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/themes/audioplayer.js" ></script>
        <script>
            playEmbedAudio($('#playlist li:first-child'),"<?php echo @$content_type; ?>","<?php echo @$movie_id; ?>");
        </script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/jquery.lazy.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery.validate.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/sdkThemes/audio-streaming/js/main.js"></script>
    </body>
</html>
