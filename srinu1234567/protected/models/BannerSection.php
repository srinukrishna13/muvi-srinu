<?php
class BannerSection extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'banner_section';
    }
    
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'banners'=>array(self::HAS_MANY, 'StudioBanner', 'banner_section'),
        );
    }     
}
