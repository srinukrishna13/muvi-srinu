<?php

class PageController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $layout = 'main';

    public function actionIndex() {
        $this->pageTitle = 'Muvi | About Us';
        $this->pageKeywords = 'Muvi.com, Muvi DAM, Muvi SDK, Muvi Affiliate';
        $this->pageDescription = "Muvi enables video content owenrs to monetize their content using Muvi's Products like Muvi SDK, Muvi DAM, Muvi Affiliate and Muvi.com Streaming";
        $this->render('index');
    }

    public function actionAbout() {
        $this->pageTitle = 'Learn more about Muvi – The Leading Video streaming Platform Provider';
        $this->pageKeywords = 'Muvi.com, Muvi DAM, Muvi SDK, Muvi Affiliate';
        $this->pageDescription = "Learn more about Muvi - A leading provider of video-on-demand platform that takes care of the entire technical infrastructure including servers, hosting, storage, bandwidth, CDN, Security including DRM and video encryption";
        $this->render('index');
    }

    /*
      public function actionNews()
      {
      $this->pageTitle = 'Muvi | News';
      $this->pageKeywords = 'Muvi.com, Muvi DAM, Muvi SDK, Muvi Affiliate';
      $this->pageDescription = "Muvi enables video content owenrs to monetize their content using Muvi's Products like Muvi SDK, Muvi DAM, Muvi Affiliate and Muvi.com Streaming";
      $this->render('news');
      }
     */

    public function actionteam() {
        $this->pageTitle = 'Muvi Team – The Team behind the VoD Platform';
        $this->pageKeywords = 'Muvi.com, Muvi DAM, Muvi SDK, Muvi Affiliate';
        $this->pageDescription = "Find out more about our Team and how we go about creating digital video-on-demand platforms for our customers at zero CapEx.";
        $this->render('team');
    }

    public function actionexamples() {
        $this->pageTitle = 'Video streaming site and websites built using Muvi open source VOD Software';
        $this->pageKeywords = 'Video streaming sites, VOD Platforms, Video streaming Platforms, Video streaming platform';
        $this->pageDescription = 'Take a look at Live Client Sites and Examples of Video streaming sites launched using Muvi’s open source VOD software at Zero Cost.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/examples';
        $this->render('examples');
    }

    public function actiontour() {
        $this->pageTitle = 'Video Streaming Server, Software Tour | Live streaming server Demo – Muvi';
        $this->pageKeywords = 'Video streaming software Demo, how to build Video on demand server, VOD Platform Demo, Video streaming website Demo, video streaming site, video streaming platform, streaming platform';
        $this->pageDescription = 'Take a Tour and Demo the Muvi Streaming Server, Software and see how simple it is to launch your own branded Video on Demand (VoD) Platform using Muvi’s Video on Demand Platform.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/tour';
        $this->render('tour');
    }

    public function actionpricing() {
        $this->pageTitle = 'Pricing Calculator to Calculate VOD Bandwidth Pricing and ROI of your VOD Platform at Muvi';
        $this->pageKeywords = 'VOD bandwidth Price, VOD bandwidth calculator, Video streaming bandwidth cost, streaming bandwidth calculator';
        $this->pageDescription = 'Calculate the bandwidth Price or cost to build your own VOD platform like Netflix clone, Hulu clone, HBO Go clone, and calculate the ROI of your VOD business.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/price';
        $this->render('pricing');
    }

    /*
      public function actionpricing_calculator(){
      $this->pageTitle = 'MuviVoD ROI Calculator - Cost to build VoD platform, Netflix, Hulu, HBO Go Clone';
      $this->pageKeywords = 'Muvi.com, Muvi SDK, SDK, VoD, PPV, Video on Demand, Video, Pay per View, video content, upload video library, monetize video, video monetization, video revenue';
      $this->pageDescription = 'Calculate the bandwidth cost or cost to build your own VoD platform like Netflix clone, Hulu clone, HBO Go clone, and calculate the ROI of your VoD business';
      $this->render('pricing_calculator');
      } */

    public function actionfaqs() {
        $this->pageTitle = 'Frequently Asked Questions (FAQs) on how to launch your VoD Platform / Video Streaming Platform - Muvi';
        $this->pageDescription = 'FAQs about Muvi the customisable FREE DIY VoD Platform. Frequently Asked Questions (FAQs) regarding how to launch your Video on Demand (VoD) / Video Streaming Platform.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/faqs';
        $this->render('faqs');
    }

    public function actionhowisitdiff() {
        $this->pageTitle = 'Read how Muvi is different than any other Video Players and Netflix or Hulu Clone Software or Script';
        $this->pageDescription = 'Get your Customizable FREE DIY VoD platform with Muvi. Check how Muvi is different than any other video player in the market and Say NO to Netflix & Hulu clones! Create own branded Video Streaming Platform in minutes at Zero Fee.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/howisitdiff';
        $this->render('howisitdiff');
    }

    public function actionhowitworks() {
        $this->pageTitle = 'Learn how to setup your own VoD Platform for Free in minutes with Muvi';
        $this->pageDescription = 'Selecting template to going live in minutes, a Step-by-step Guide to setup a fully customizable video streaming platform using Muvi.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/howitworks';
        $this->render('howitworks');
    }

    public function actiontechnology() {
        $this->pageTitle = 'Sell Videos Online using Video on Demand (VOD) Services & Technology - Muvi';
        $this->pageDescription = 'Now Video content owners can sell their videos online using our Video on Demand Services & Technology at ZERO CapEx. No hosting Fee & maintenance Fee, Just pay for the bandwidth you are using.';
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/technology';
        $this->render('technology');
    }

    public function actionhowtoconvertrawfile() {
        $this->pageTitle = 'How to convert raw file';
        $this->render('howtoconvertrawfile');
    }

    public function actionApplication() {
        $this->render('application');
    }

    public function actionInsertApplication() {
        $application = new SellerApplication();
        $data = $application->findByAttributes(array('email' => $_POST['email']));
        $_POST['application_type'] = implode(",", $_POST['application_type']);
        $source = '';
        if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
            $source = Yii::app()->request->cookies['REFERRER'];
        }
        if ($data) {
            if (($data['application_type'] == $_POST['application_type']) || ($data['application_type'] == '1,2')) {
                $arr['succ'] = 0;
                $arr['msg'] = "Email is already in use . Try another one.";
                echo json_encode($arr);
                exit;
            } else {
                $data->application_type = $data->application_type . ',' . $_POST['application_type'];
                $data->status = 0;
                $data->save();
                $arr['succ'] = 1;
                $arr['msg'] = "<div><h4>Thank you!</h4>Your application has been successfully submitted and is under review. You will receive a welcome email after it has been approved. We may contact you for some additional information if required.</div>";
                Yii::app()->email->SendApplicationMail($_POST);
                Yii::app()->email->SendMailtoApplier($_POST);
                echo json_encode($arr);
                exit;
            }
        } else {
            $application->application_type = $_POST['application_type'];
            $application->company_name = $_POST['company_name'];
            $application->industry = $_POST['industry'];
            $application->name = $_POST['name'];
            $application->phone = $_POST['phone'];
            $application->email = $_POST['email'];
            $application->numberofcustomer = $_POST['numberofcustomer'];
            $application->reason = $_POST['reason'];
            $application->source = $source;
            $application->add_date = new CDbExpression("NOW()");
            $application->ip = CHttpRequest::getUserHostAddress();
            $application->save();
            $arr['succ'] = 1;
            $arr['msg'] = "<div><h4>Thank you!</h4>Your application has been successfully submitted and is under review. You will receive a welcome email after it has been approved. We may contact you for some additional information if required.</div>";
            Yii::app()->email->SendApplicationMail($_POST);
            Yii::app()->email->SendMailtoApplier($_POST);
            /* Add user to Hubspot */
            $nm = explode(" ", $_POST['name'], 2);
            $hubspot_owner_id = '3815226';
            $hs_lead_status = 'COLD_LEAD';
            $lead_type = 'Partner / Reseller / Affiliate / Referral';
            Hubspot::AddToHubspot($_POST['email'], $nm[0], $nm[1], $_POST['phone'], $hubspot_owner_id, $hs_lead_status, $lead_type);
            /* Add user to madmimi */
            $user = array('email' => $_POST['email'], 'firstName' => $nm[0], 'lastName' => $nm[1], 'add_list' => 'MUVI PARTNERS RESELLERS');
            require('MadMimi.class.php');
            $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
            $mimi->AddUser($user);
            /*             * * END of hubspot and madmimi *** */
            echo json_encode($arr);
            exit;
        }
    }

    public function actionShow() {
        if ($_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $studio = $this->studio;
        if(isset($_REQUEST['mobileview'])){
            $this->is_mobileview =true;
        }
        //foreach ($_REQUEST as $key => $value) {
        $permalink = $_REQUEST['permalink'];
        $pages = Yii::app()->db->createCommand()
                ->select('*')
                ->from('pages')
                ->where("status=1 AND studio_id=".$studio_id." AND permalink=:permalink AND (language_id=".$this->language_id." OR parent_id=0 AND id NOT IN (SELECT parent_id FROM pages WHERE status=1 AND studio_id=".$studio_id." AND permalink=:permalink1 AND language_id=".$this->language_id."))",array(':permalink'=>$permalink,':permalink1'=>$permalink))
		       ->setFetchMode(PDO::FETCH_OBJ)->queryAll();
        if (count($pages) > 0) {
            $PageName = $pages[0]->title;
            $type = explode('-', $permalink);
            if (count($type) > 1) {
                $meta = Yii::app()->common->pagemeta($type[1]);
            } else {
                $meta = Yii::app()->common->pagemeta($type[0]);
            }

            if ($meta['title'] != '') {
                $title = $meta['title'];
            } else {
                /*$temp = Yii::app()->request->url;
                $temp = explode('/', $temp);
                $permalink = $temp[count($temp) - 1];
                $type = explode('-', $permalink);
                $default_title = implode(' ', $type);
                $default_title = ucwords($default_title);

                $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                // echo $Words;exit;
                $title = $Words . ' | ' . $studio->name;*/
                $title = $PageName . ' | ' . $studio->name;
            }

            if ($meta['description'] != '') {
                $description = $meta['description'];
            } else {
                $description = '';
            }
            if ($meta['keywords'] != '') {
                $keywords = $meta['keywords'];
            } else {
                $keywords = '';
            }
            eval("\$title = \"$title\";");
            eval("\$description = \"$description\";");
            eval("\$keywords = \"$keywords\";");
            $this->pageTitle = $title;
            $this->pageDescription = $description;
            $this->pageKeywords = $keywords;

            $page = $pages[0];
            $pg = array(
                'title' => utf8_encode($page->title),
                'content' => utf8_encode(Yii::app()->general->showHtmlContents($page->content))
            );
            $content = json_encode($pg);
            $this->render('index', array('content' => $content));
        } else {
            Yii::app()->user->setFlash("error", $this->ServerMessage['page_does_not_exist']);
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }

        // }        
    }

}
