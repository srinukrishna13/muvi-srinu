<?php 
$enable_lang = $this->language_code;
if ($_COOKIE['Language']) {
	$enable_lang = $_COOKIE['Language'];
}
if(@$column_data){
    $index = 0;
foreach ($column_data AS $ckey=>$cval) {?>
    <li class="m-t-20" id="customreportreplace<?= $cval['id']; ?>">
        <div class="row">
            <label class="col-sm-4 control-label editdisplayname" data-displayname="custom[<?= $index;?>][f_display_name]"><?= $cval['f_display_name'];?></label>
            <div class="col-sm-6">
                <?php if(!$cval['f_type']){?>
                <div class="fg-line">
                    <input type='text' class="form-control input-sm checkInput" readonly="readonly">
                </div>
                <?php }elseif($cval['f_type']==1){?>
                <div class="fg-line">
                    <textarea class="form-control input-sm checkInput" rows="5" readonly="readonly"></textarea>
                </div>
                <?php }elseif($cval['f_type']==2){?>
                <div class="fg-line">
                    <select class="form-control input-sm checkInput" >
                        <?php
                        echo "<option value=''>-Select-</option>";
                        $opData = json_decode($cval['f_value'],true);
						$opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
                        foreach($opData AS $opkey=>$opvalue){
                           echo "<option value='".$opvalue."'>" . $opvalue . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <?php }elseif($cval['f_type']==3){?>
                <div class="fg-line">
                    <select class="form-control input-sm checkInput" multiple>
                        <?php
                        $opData = json_decode($cval['f_value'],true);
						$opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
                        foreach($opData AS $opkey=>$opvalue){
                                echo "<option value='".$opvalue."'>" . $opvalue . "</option>";
                        }
                        ?>
                    </select>              
                </div>
                <?php }?>
            </div>
            <div class="col-sm-2 text-right">
                <div class="row">
                    <div  class="col-sm-4 p-r-0">
                        <a style="cursor: move;"><i class="icon-cursor-move h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a>
                    </div>
                    <?php 
                    /*if(($cval['f_id']=='title') && ($formid!=4)){
                        $cval['f_is_required']=0;
                    }*/
                    if($cval['f_is_required']!=1){
                        if ($cval['f_type'] == 0) {
                            $type = 'Textfield';
                        } elseif ($cval['f_type'] == 1) {
                            $type = 'Textarea';
                        } elseif ($cval['f_type'] == 2) {
                            $type = 'Dropdown';
                        } elseif ($cval['f_type'] == 3) {
                            $type = 'List';
                        }                   
                    ?>
                    <?php if(in_array($cval['f_type'],array(2,3)) && !in_array($cval['f_name'],array('series_number'))){?>
                        <div class="col-sm-1 text-right" data-id="<?= $cval['id'] ?>" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4"  aria-hidden="true"></i></a></div>
                    <?php }?>
                    <div id="movecustomreport<?=$cval['id'];?>" class="col-sm-4 text-right" data-name="<?= $cval['f_display_name'];?>" data-id="<?=$cval['id']; ?>" data-tempid="<?=$cval['id']; ?>" data-type="<?=$type;?>" onclick="movetofieldcolumn(this)">
                        <a href="javascript:void(0)" title="Remove from content type"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <input type="hidden" name="custom[<?= $index;?>][id]" value="<?= $cval['id'];?>">
        <input type="hidden" name="custom[<?= $index;?>][f_type]" value="<?= $cval['f_type'];?>">
        <input type="hidden" name="custom[<?= $index;?>][f_display_name]" value="<?= $cval['f_display_name'];?>">
        <input type="hidden" name="custom[<?= $index;?>][f_name]" value="<?= $cval['f_name'];?>">
        <input type="hidden" name="custom[<?= $index;?>][f_id]" value="<?= $cval['f_id'];?>">
        <input type="hidden" name="custom[<?= $index;?>][f_value]" value='<?= $cval['f_value'];?>'>
        <input type="hidden" name="custom[<?= $index;?>][f_is_required]" value="<?= $cval['f_is_required'];?>">
        <?php if(($cval['field_type']==1) && !in_array($cval['f_name'],$excaptionarray)){?>
        <input type="hidden" name="custom[<?= $index;?>][data_type]" value="custom" class="cfield">
        <?php }?>
    </li>
<?php $index++;}}
