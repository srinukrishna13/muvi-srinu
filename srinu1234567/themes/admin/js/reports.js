function parseMillisecondsIntoReadableTime(seconds){
    if(seconds > 0 && seconds < 1){
        seconds = seconds+1;
    }
    //Get hours from seconds
    var hours = seconds / (60*60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours >= 10 ? absoluteHours : '0' + absoluteHours;
    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes >= 10 ? absoluteMinutes : '0' +  absoluteMinutes;
    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds >= 10 ? absoluteSeconds : '0' + absoluteSeconds;
    return h + ':' + m + ':' + s;
}
var round = Math.round;
Math.round = function (value, decimals) {
  decimals = decimals || 0;
  return Number(round(value + 'e' + decimals) + 'e-' + decimals);
}
function formatKBytes(bytes, precision) { 
    var kilobyte = 1024;
    var megabyte = kilobyte * 1024;
    var gigabyte = megabyte * 1024;
    var terabyte = gigabyte * 1024;

    if ((bytes >= 0) && (bytes < kilobyte)) {
        return bytes + ' B';

    } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
        return Math.round(bytes / kilobyte, precision) + ' KB';

    } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
        return Math.round(bytes / megabyte, precision) + ' MB';

    } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
        return Math.round(bytes / gigabyte, precision) + ' GB';

    } else if (bytes >= terabyte) {
        return Math.round(bytes / terabyte, precision) + ' TB';
    } else {
        return bytes + ' B';
    }
}

function userDetails(id){
    $.post('/report/userDetails',{'user_id':id},function(res){
        $('#user-profile').html(res);
        $('#updateModal').modal('show');
    });
}

function cancelUserSubscription(id,el) {
    
    swal({
        title: "Cancel Subscription",
        text: 'You are canceling a subscription. The user is not able to access contents anymore. Is it okay?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html:true
    }, function() {
        $('.loader').show();
        var cancel_note = 'Subscription canceled by studio admin';
        $.post('/user/cancelsubscription',{'user_id':id,'cancel_note':cancel_note, 'is_admin': 1},function(res){
            $('.loader').hide();
            var data = $.parseJSON(res);
            if(data.responce){
                location.reload();
            }else{
                $('#error-msg').html(data.message);
                $('#error-msg').parent().show();
            }
        });
    });
}

function deleteUser(id,el) {
    swal({
        title: "Delete user",
        text: "You are deleting a user. All the datas of user will be deleted. Is it okay?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html:true
    }, function() {
        $('.loader').show();
        $.post('/user/deleteUserByAdmin',{'user_id':id},function(res){
            $('.loader').hide();
            var data = $.parseJSON(res);
            if(data.responce){
                location.reload();
            }else{
                $('#error-msg').html(data.message);
                $('#error-msg').parent().show();
            }
        });
    });
}

/* video report scripts */

function getVideoReport(date_range,device_type,searchText,currency_id,type)
{
    $('.loader').show();
    //alert(searchText);
    if(parseInt(type)!=0){
        $.post('/report/video',{'dt':date_range,search_value:searchText,device_type:device_type,currency_id:currency_id},function(res){
            var obj = JSON.parse(res);
            var value = (parseFloat(obj.watched_hour));
            if(isNaN(value)){
                var wh_val = '00:00:00';
            }else{
                var wh_val = parseMillisecondsIntoReadableTime(value);
            }
            var wh = 'Hours watched: ' + wh_val;
            $('#wh').html(wh);
            var buff = (parseFloat(obj.buffer_duration.buffered_time));
             if(isNaN(buff)){
                 var bd_val = '00:00:00';
             }else{
                 var bd_val = parseMillisecondsIntoReadableTime(buff);
             }
             var bd = 'Buffered Duration: ' + bd_val;
            $('#bd').html(bd);
            if(obj.bandwidth){
                var b_value = formatKBytes(parseFloat(obj.bandwidth)*1024,2);
            }else{
                var b_value = '0.00';
            }
            var bw = 'Bandwidth: '+b_value;
             $('#bw').html(bw);

            if(parseFloat(obj.total_price)){
               var c_value = '$'+obj.total_price;
               var bwc = 'Total Cost: '+c_value;
               $('#bwc').html(bwc);
               $('#bwc').parent().show();
            }
        });
    }
    $.post('/report/getMonthlyViews',{'dt':date_range,search_value:searchText,device_type:device_type,currency_id:currency_id},function(res){
        $('#video-table-body').html(res);
        $('.loader').hide();
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
       var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if($('.page-selection-div').length){
            $('.page-selection-div').remove(); 
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-video').parent().hide();
        }else{
        if($('#page-selection-video').length){
            $('#page-selection-video').parent().show();
        }else{
            $('#video-table-body').after('<div class="page-selection-div"><div id="page-selection-video" class="pull-right"></div></div>');
            $('#page-selection-video').parent().show();
        }
    }
        $('#page-selection-video').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
            var date_range = $('#video_date').val();
            $('.loader').show();
            $.post('/report/getMonthlyViews',{'dt':date_range,search_value:searchText,device_type:device_type,'page':num},function(res){
                $('#video-table-body').html(res);
                $('.loader').hide();
            });
        });
    });
}

function getVideoDetailsReport(date_range,movie_id,video_id,searchText,device_type)
{
    $('.loader').show();
    $.post('/report/videoDetails',{'dt':date_range,'id':movie_id,'video_id':video_id,search_value:searchText,device_type:device_type},function(res){
         var obj = JSON.parse(res);
         var obj = JSON.parse(res);
        var buff = (parseFloat(obj.buffer_duration.buffered_time));
         if(isNaN(buff)){
             var bd_val = '00:00:00';
         }else{
             var bd_val = parseMillisecondsIntoReadableTime(buff);
         }
         var bd = 'Buffered Duration: ' + bd_val;
        $('#bd').html(bd);
        if(obj.bandwidth){
            var b_value = formatKBytes(parseFloat(obj.bandwidth)*1024,2);
            //var b_value = parseFloat(obj.bandwidth)+' TB';
        }else{
            var b_value = '0.00';
        }
        var bw = 'Bandwidth: '+b_value;
         $('#bw').html(bw);
         
        if(parseFloat(obj.total_price)){
            var c_value = '$'+obj.total_price;
            var bwc = 'Total Cost: '+c_value;
            $('#bwc').html(bwc);
        }
        
    });
    
    $.post('/report/getMonthlyBandwidth',{'dt':date_range,'id':movie_id,'video_id':video_id,search_value:searchText,device_type:device_type},function(res){
        $('#video-table-body').html(res);
        var mydata = $(res).find('data-count');
        var regex = /\d+/g;
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
       if($('.page-selection-div').length){
            $('.page-selection-div').remove(); 
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection').parent().hide();
        }else{
            if($('.page-selection-div').length <= 0){
                $('#video-table-body').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>');
            }
            $('#page-selection').parent().show();
        }
        
        $('#page-selection').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
            var date_range = $('#video_date').val();
            var searchText = $.trim($(".search").val());
            var device_type = $('#device-box').val();
            $.post('/report/getMonthlyBandwidth',{'dt':date_range,'id':movie_id,'video_id':video_id,search_value:searchText,device_type:device_type,'page':num},function(res){
                $('#video-table-body').html(res);
                $('.loader').hide();
            });
        });
        
        $('.loader').hide();
    });
}

/* user details report scripts */
function getUserDetails(date_range,type,searchText)
{
    $('.loader').show();
    $.post('/report/getMonthlyUsers',{'dt':date_range,'search_value':searchText},function(res){
        var obj = JSON.parse(res);
        $('#reg').html(obj.registration.count);
        $('#sub').html(obj.subscription.count);
        $('#ppv').html(obj.ppv_user.count);
        $('#advppv').html(obj.adv_ppv_user.count);
    });
    $.post('/report/usersTypeData',{'dt':date_range,'type':type,'search_value':searchText},function(res){
        $('#users-table-body').html(res);
        var mydata = $(res).find('data-count');
        var regex = /\d+/g;
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if($('.page-selection-div').length){
            $('.page-selection-div').remove(); 
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection').parent().hide();
        }else{
            if($('.page-selection-div').length <= 0){
                $('#users-table-body').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>');
            }
            $('#page-selection').parent().show();
        }
        $('#page-selection').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
            var date_range = $('#user_date').val();
             var type = $('#users-box').val();
            $.post('/report/usersTypeData',{'dt':date_range,'type':type,'page':num},function(res){
                $('#users-table-body').html(res);
                $('.loader').hide();
            });
        });
    });
}

/* revenue report scripts */
function getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVouchers,is_partners,search_order_cart)
{
    $('.loader').show();
    $.post('/report/getMonthlyRevenue',{'dt':date_range,'search_value_sub':searchTextSub,'search_value_ppv':searchTextPpv,search_value_ppvbundle:searchTextPpvbundles,'search_value_adv_ppv':searchTextAdvPpv,'currency_id':currency_id,'search_value_pg':searchPg,'currency_symb':currency_symb},function(res){
        var total =0;
        var i=1;
       
        $.each(res, function(key,val){
            
            $.each(val, function(keey,vaal){
                if(i > Object.keys(val).length){
                    $('#r_'+keey).html(currency_symb+parseFloat(vaal).toFixed(2));
                }else{
                    if((keey === 'ppv') && parseFloat(vaal).toFixed(2) != '0.00'){
                        var index = $("#advppv").parent().index();
                        $("#ppv").parent().remove();
                        var _html = '<tr><td>Pay-per-view:</td><td id="ppv">'+currency_symb+parseFloat(vaal).toFixed(2)+'</td></tr>';
                        $('#revenue > tbody > tr').eq(index-1).after(_html);
                    }/*else  if((keey === 'ppv_bundles') && parseFloat(vaal).toFixed(2) != '0.00'){
                        var index = $("#advppv").parent().index();
                        $("#ppvbundles").parent().remove();
                        var _html = '<tr><td>Pay-per-bundles:</td><td id="ppvbundles">'+currency_symb+parseFloat(vaal).toFixed(2)+'</td></tr>';
                        $('#revenue > tbody > tr').eq(index-1).after(_html);
                    }else  if((keey === 'advppv') && parseFloat(vaal).toFixed(2) != '0.00'){
                        var index = $("#advppv").parent().index();
                        $("#advppv").parent().remove();
                        var _html = '<tr><td>Pre-Order:</td><td id="advppv">'+currency_symb+parseFloat(vaal).toFixed(2)+'</td></tr>';
                        $('#revenue > tbody > tr').eq(index-1).after(_html);
                    }
                    */
                    else{
                        $('#'+keey).html(currency_symb+parseFloat(vaal).toFixed(2));
                    }
                    
                }
                total += parseFloat(vaal);
                i++;
            });
        });
        $('#total_sub').html('$'+ parseFloat(total).toFixed(2));
    },'json');
   
    $.post('/report/revenueGraph', {'currency_id': currency_id}, function (res) {
        var xdata = res.xdata;
        var graphdata = res.graphdata;
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        $('#revenuechart').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: xdata
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Revenue'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': '+currency_symb + this.y;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: graphdata,
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            }
        });

    }, 'json');
   
 
   
    if(is_partners!=1){
      
    $.post('/report/getMonthlySubscribers',{'dt':date_range,'search_value_sub':searchTextSub,'currency_id':currency_id},function(res){
        $('#subscription-table').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-subscription').parent().hide();
        }else{
            //$('#subscription-table').after('<div class="page-selection-div"><div id="page-selection-subscription" class="pull-right"></div></div>');
            $('#page-selection-subscription').parent().show();
        }
        
        $('#page-selection-subscription').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/getMonthlySubscribers',{'dt':date_range,'search_value_sub':searchTextSub,'currency_id':currency_id,'page':num},function(res){
                $('#subscription-table').html(res);
            });
        });
    });
    }
    $.post('/report/getMonthlyPpvSubscribers',{'dt':date_range,'search_value_ppv':searchTextPpv,'currency_id':currency_id},function(res){
        $('#ppv-table').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-ppv-subscription').parent().hide();
        }else{
            //$('#ppv-table').after('<div class="page-selection-div"><div id="page-selection-ppv-subscription" class="pull-right"></div></div>');
            $('#page-selection-ppv-subscription').parent().show();
        }
        $('#page-selection-ppv-subscription').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/getMonthlyPpvSubscribers',{'dt':date_range,'search_value_ppv':searchTextPpv,'currency_id':currency_id,'page':num},function(res){
                $('#ppv-table').html(res);
            });
        });
    });
   if(is_partners!=1){
     $.post('/report/getMonthlyPpvbundlesSubscribers',{'dt':date_range,'search_value_ppvbundle':searchTextPpvbundles,'currency_id':currency_id},function(res){
        $('#ppvbundles-table').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-ppvbundles-subscription').parent().hide();
        }else{
            //$('#ppv-table').after('<div class="page-selection-div"><div id="page-selection-ppv-subscription" class="pull-right"></div></div>');
            $('#page-selection-ppvbundles-subscription').parent().show();
        }
        $('#page-selection-ppvbundles-subscription').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/getMonthlyPpvbundlesSubscribers',{'dt':date_range,'search_value_ppvbundle':searchTextPpvbundles,'currency_id':currency_id,'page':num},function(res){
                $('#ppvbundles-table').html(res);
            });
        });
    });
    
     $.post('/report/getMonthlyVoucher',{'dt':date_range,'search_value_voucher':searchVouchers,'currency_id':currency_id},function(res){
        $('#coupononly-table').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-coupononly-subscription').parent().hide();
        }else{
            //$('#ppv-table').after('<div class="page-selection-div"><div id="page-selection-ppv-subscription" class="pull-right"></div></div>');
            $('#page-selection-coupononly-subscription').parent().show();
        }
        $('#page-selection-coupononly-subscription').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/getMonthlyVoucher',{'dt':date_range,'search_value_voucher':searchVouchers,'currency_id':currency_id,'page':num},function(res){
                $('#coupononly-table').html(res);
            });
        });
    });
    
    
   }
   
    $.post('/report/getMonthlyAdvPpvSubscribers',{'dt':date_range,'search_value_adv_ppv':searchTextAdvPpv,'currency_id':currency_id},function(res){
        $('#adv-ppv-table').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-adv-ppv-subscription').parent().hide();
        }else{
            //$('#ppv-table').after('<div class="page-selection-div"><div id="page-selection-ppv-subscription" class="pull-right"></div></div>');
            $('#page-selection-adv-ppv-subscription').parent().show();
        }
        $('#page-selection-adv-ppv-subscription').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/getMonthlyAdvPpvSubscribers',{'dt':date_range,'search_value_ppv':searchTextPpv,'currency_id':currency_id,'page':num},function(res){
                $('#adv-ppv-table').html(res);
            });
        });
    });
    /*physical goods*/
   if(is_partners!=1){  
    $.post('/report/getMonthlyPhysicalGoods',{'dt':date_range,'search_value_pg':searchPg,'currency_id':currency_id},function(res){
        $('#pg-table').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-pg-subscription').parent().hide();
        }else{
            //$('#ppv-table').after('<div class="page-selection-div"><div id="page-selection-ppv-subscription" class="pull-right"></div></div>');
            $('#page-selection-pg-subscription').parent().show();
        }
        $('#page-selection-pg-subscription').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/getMonthlyPhysicalGoods',{'dt':date_range,'search_value_pg':searchPg,'currency_id':currency_id,'page':num},function(res){
                $('#pg-table').html(res);
            });
        });
    });
    $('.loader').hide();
   }
    if(is_partners!=1){ 
    $.post('/report/GetMuviCartOrders',{'dt':date_range,'search_value_order_cart':search_order_cart,'currency_id':currency_id},function(res){ 
   // alert(res);
      $("#cart-order-table").html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
}
          if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-pg-muvicart-order').parent().hide();
        }else{
            //$('#ppv-table').after('<div class="page-selection-div"><div id="page-selection-ppv-subscription" class="pull-right"></div></div>');
            $('#page-selection-pg-muvicart-order').parent().show();
        }
        $('#page-selection-pg-muvicart-order').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.ceil(maxVisible)
        }).on('page', function(event, num){
            $.post('/report/GetMuviCartOrders',{'dt':date_range,'search_value_order_cart':search_order_cart,'currency_id':currency_id,'page':num},function(res){
                $('#cart-order-table').html(res);
               // alert(res);
            });
        });      
  });
  }
}

/* user action report scripts */
function getUserActionDetails(date_range,searchTextSearch,searchTextLogin,searchTextView)
{
    $('.loader').show();
    $.post('/report/getMonthlyLoginsCount',{'dt':date_range,'search_value_login':searchTextLogin},function(res){
        $('#login-count').html(res);
         $('.loader').hide();
    });
    $('.loader').show(); 
    $.post('/report/getMonthlyLogins',{'dt':date_range,'search_value_login':searchTextLogin},function(res){
        $('#login-content').html(res);
        $('.loader').hide();
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        if(!isEmpty(mydata.prevObject)){
            var count = mydata.prevObject[0].outerHTML.match(regex);
            var page_size = $('#page_size').val();
            var total = parseInt(count)/parseInt(page_size);
            var maxVisible = total/2;
            if(maxVisible >= 10){
                maxVisible = 10;
            }else{
                maxVisible = total;
            }
            if(parseInt(count) < parseInt(page_size)){
                $('#page-selection-login').parent().hide();
            }else{
                //$('#login-content').after('<div class="page-selection-div"><div id="page-selection-login" class="pull-right"></div></div>');
                $('#page-selection-login').parent().show();
            }
            $('#page-selection-login').bootpag({
                total: Math.ceil(total),
                page: 1,
                maxVisible: Math.round(maxVisible)
            }).on('page', function(event, num){
                $('.loader').show(); 
                $.post('/report/getMonthlyLogins',{'dt':date_range,'search_value_login':searchTextLogin,'page':num},function(res){
                    $('#login-content').html(res);
                    $('.loader').hide();
                });
            });
        }
    });
    $('.loader').show(); 
    $.post('/report/searchData',{'dt':date_range,'search_value_search':searchTextSearch},function(res){
        $('#content').html(res);
        $('.loader').hide();
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        if(!isEmpty(mydata.prevObject)){
            var count = mydata.prevObject[0].outerHTML.match(regex);
            var page_size = $('#page_size').val();
            var total = parseInt(count)/parseInt(page_size);
            var maxVisible = total/2;
            if(maxVisible >= 10){
                maxVisible = 10;
            }else{
                maxVisible = total;
            }
            if(parseInt(count) < parseInt(page_size)){
                $('#page-selection').parent().hide();
            }else{
                //$('#content').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>');
                $('#page-selection').parent().show();
            }
            $('#page-selection').bootpag({
                total: Math.ceil(total),
                page: 1,
                maxVisible: Math.round(maxVisible)
            }).on('page', function(event, num){
                $('.loader').show(); 
                $.post('/report/searchData',{'dt':date_range,'search_value_search':searchTextSearch,'page':num},function(res){
                    $('#content').html(res);
                    $('.loader').hide();
                });
            });
        }
    });
    $('.loader').show(); 
     $.post('/report/viewData',{'dt':date_range,'search_value_view':searchTextView},function(res){
        $('#view-content').html(res);
        $('.loader').hide();
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        if(!isEmpty(mydata.prevObject)){
            var count = mydata.prevObject[0].outerHTML.match(regex);
            var page_size =$('#page_size').val();
            var total = parseInt(count)/parseInt(page_size);
            var maxVisible = total/2;
            if(maxVisible >= 10){
                maxVisible = 10;
            }else{
                maxVisible = total;
            }
            if(parseInt(count) < parseInt(page_size)){
                $('#page-selection-view').parent().hide();
            }else{
                //$('#content').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>');
                $('#page-selection-view').parent().show();
            }
            $('#page-selection-view').bootpag({
                total: Math.ceil(total),
                page: 1,
                maxVisible: Math.round(maxVisible)
            }).on('page', function(event, num){
                $('.loader').show(); 
                var daterange=$('#search_date').val();
               
                $.post('/report/viewData',{'dt':daterange,'search_value_view':searchTextView,'page':num},function(res){
                    $('#view-content').html(res);
                    $('.loader').hide();
                });
            });
        }
    });
}

/* check if object is empty */
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

/* loads the daterange picker*/

function loadDateRangePicker(id, start_date, start, end)
{
    $("#" + id).daterangepicker({
        presetRanges: [
            {text: 'Last Year', dateStart: function () {
                    return moment().subtract('year', 1).startOf('year')
                }, dateEnd: function () {
                    return moment().subtract('year', 1).endOf('year')
                }},
            {text: 'Last Month', dateStart: function () {
                    return moment().subtract('month', 1).startOf('month')
                }, dateEnd: function () {
                    return moment().subtract('month', 1).endOf('month')
                }},
            {text: 'Last Week (Mo-Su)', dateStart: function () {
                    return moment().subtract('days', 7).isoWeekday(1)
                }, dateEnd: function () {
                    return moment().subtract('days', 7).isoWeekday(7)
                }},
            {text: 'Last 7 Days', dateStart: function () {
                    return moment().subtract('days', 6)
                }, dateEnd: function () {
                    return moment()
                }},
            {text: 'Yesterday', dateStart: function () {
                    return moment().subtract('days', 1)
                }, dateEnd: function () {
                    return moment().subtract('days', 1)
                }},
            {text: 'Today', dateStart: function () {
                    return moment()
                }, dateEnd: function () {
                    return moment()
                }},
            {text: 'This Month', dateStart: function () {
                    return moment().startOf('month')
                }, dateEnd: function () {
                    return moment()
                }},
            {text: 'This Year', dateStart: function () {
                    return moment().startOf('year')
                }, dateEnd: function () {
                    return moment()
                }},
            {text: 'Show All', dateStart: function () {
                    return moment(start_date)
                }, dateEnd: function () {
                    return moment()
                }}
        ]

    });
    if (!($.trim(start) && $.trim(end))) {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var start = moment(firstDay)._d;
        var end = moment(date)._d;
    }
    $("#" + id).daterangepicker("setRange", {start: start, end: end});
}

/* patners report */

function getVideoReportPartners(date_range,device_type,searchText,currency_id,type,reportIDd)
{
    $('.loader').show();
    //alert(searchText);
    if(parseInt(type)){
        $.post('/partners/video',{'dt':date_range,search_value:searchText,device_type:device_type,currency_id:currency_id},function(res){
            var obj = JSON.parse(res);
            var value = (parseFloat(obj.watched_hour));
            if(isNaN(value)){
                var wh_val = '00:00:00';
            }else{
                var wh_val = parseMillisecondsIntoReadableTime(value);
            }
            var wh = 'Hours watched: ' + wh_val;
            $('#wh').html(wh);
            var buff = (parseFloat(obj.buffer_duration.buffered_time));
             if(isNaN(buff)){
                 var bd_val = '00:00:00';
             }else{
                 var bd_val = parseMillisecondsIntoReadableTime(buff);
             }
             var bd = 'Buffered Duration: ' + bd_val;
            $('#bd').html(bd);
            if(obj.bandwidth){
                if(isNaN(obj.bandwidth)){
                    var b_value = '0.00';
                }else{
                    var b_value = formatKBytes(parseFloat(obj.bandwidth)*1024,2);
                }
            }else{
                var b_value = '0.00';
            }
            var bw = 'Bandwidth: '+b_value;
             $('#bw').html(bw);
            if(parseFloat(obj.total_price)){
                var c_value = '$'+obj.total_price;
                var bwc = 'Total Cost: '+c_value;
                $('#bwc').html(bwc);
                $('#bwc').parent().show();
             }
        });
    }
    
    $.post('/partners/getMonthlyViews',{'dt':date_range,search_value:searchText,device_type:device_type,currency_id:currency_id,reportID:reportIDd},function(res){
        $('#video-table-body').html(res);
        $('.loader').hide();
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-video').parent().hide();
        }else{
            if($('#page-selection-video').length){
                $('#page-selection-video').parent().show();
            }else{
                $('#video-table-body').after('<div class="page-selection-div"><div id="page-selection-video" class="pull-right"></div></div>');
                $('#page-selection-video').parent().show();
            }
        }
        $('#page-selection-video').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
            var date_range = $('#video_date').val();
            $('.loader').show();
            $.post('/partners/getMonthlyViews',{'dt':date_range,search_value:searchText,device_type:device_type,reportID:reportIDd,'page':num},function(res){
                $('#video-table-body').html(res);
                $('.loader').hide();
            });
        });
    });
}

function getVideoDetailsReportPartners(date_range,movie_id,video_id,searchText,device_type)
{
    $('.loader').show();
    $.post('/partners/videoDetails',{'dt':date_range,'id':movie_id,'video_id':video_id,search_value:searchText,device_type:device_type},function(res){
         var obj = JSON.parse(res);
        if(obj.buffer_duration){
            var buff = (parseFloat(obj.buffer_duration.buffered_time));
            if(isNaN(buff)){
                var bd_val = '00:00:00';
            }else{
                var bd_val = parseMillisecondsIntoReadableTime(buff);
            }
        }else{
            var bd_val = '00:00:00';
        }
        
         var bd = 'Buffered Duration: ' + bd_val;
        $('#bd').html(bd);
        if(obj.bandwidth){
            var b_value = formatKBytes(parseFloat(obj.bandwidth)*1024,2);
        }else{
            var b_value = '0.00';
        }
        var bw = 'Bandwidth: '+b_value;
         $('#bw').html(bw);
    });
    
    $.post('/partners/getMonthlyBandwidth',{'dt':date_range,'id':movie_id,'video_id':video_id,search_value:searchText,device_type:device_type},function(res){
        $('#video-table-body').html(res);
        $('.loader').hide();
    });
}

//End User Support

function newuser() {
    $("#newuserPopup").modal('show');
    $('label.error').remove();
    $('#errors').hide();
    $('#new_user_form')[0].reset();
    $('#email').on('change', function () {
        checkEmailExists(0);
    });
}
function getUserList(userType,searchText) {
    $('.loader').show();
    $.post('/monetization/getEndUserList',{user_type:userType,search_value:searchText},function(res){
        $('#endusersupport_list').html(res);
        $('.loader').hide();
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible >= 10){
            maxVisible = 10;
        }else{
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection-enduser').parent().hide();
        }else{
            if($('#page-selection-enduser').length){
                $('#page-selection-enduser').parent().show();
            }else{
                $('#pag').html('<div id="page-selection"><div id="page-selection-enduser" class="pull-right"></div></div>');
                $('#page-selection-enduser').parent().show();
            }
        }
        $('#page-selection-enduser').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
            $('.loader').show();
            $.post('/monetization/getEndUserList',{user_type:userType,search_value:searchText,'page':num},function(res){
                $('#endusersupport_list').html(res);
                $('.loader').hide();
            });
        });
    });
}

function sendResetPasswordEmail(email) {
    $('.loader').show();
    $.post('/user/forgotpassword',{email:email,admin:1},function(res){
        location.reload();
        $('.loader').hide();
    });
}

function openEmailPopUp(email) {
    $('#old_email').val(email);
    $('#emailPopup').modal('show');
}


function openDevicePopUp(user_id) {   
    $("#hidden_userid").val(user_id);
   $.post('/monetization/devicename',{user_id:user_id},function(res){      
       $('#devicePopup').modal('show');
       $('#device_list').html(res);
   });
}

function updateEmailAddress() {
    $("#newemail").html('Saving...');
    $("#newemail").attr("disabled", true);
    var validate = $("#update_email_address").validate({
        rules: {
            'data[Admin][old_email]': {
                required: true,
                mail: true
            },
            'data[Admin][new_email]': {
                required: true,
                mail: true
            },
        },
        messages: {
            'data[Admin][old_email]': {
                required: "Please enter old email address",
                //$('#errors').html(msg).show();
                mail: "Please enter a valid email address"
            },
            'data[Admin][new_email]': {
                required: "Please enter email address",
                //$('#errors').html(msg).show();
                mail: "Please enter a valid email address"
            },
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        },
    });
    var x = validate.form();
    
    if (x) {
         var url = "/user/checkemail";
        var email = $.trim($('#new_email').val());
        $.post(url, {'email': email}, function (res) {
            if (parseInt(res.isExists) === 1) {
                var msg = "Email is already registered with Muvi.";
                $('#update_email_errors').html(msg).show();
                $("#newemail").html('Submit');
                $("#newemail").removeAttr("disabled");
                return false;
            } else {
                $('#update_email_errors').html('').hide();
                    var url = "/user/updateUserEmail";
                    $('<input>').attr({
                        type: 'hidden',
                        id: 'request_type',
                        name: 'request_type',
                    }).appendTo('#update_email_address');
                    document.update_email_address.action = url;
                    document.update_email_address.submit();
                    return false;
            }
        }, 'json');
    } else {
        $("#newemail").html('Submit');
        $("#newemail").removeAttr("disabled");
    }
}

//add free user js
function validateNewUser(type) {
    $("#newmembership").html('Saving...');
    $("#newmembership").attr("disabled", true);
    var validate = $("#new_user_form").validate({
        rules: {
            'data[Admin][first_name]': "required",
            'data[Admin][email]': {
                required: true,
                mail: true
            },
            'data[Admin][password]': "required",
        },
        messages: {
            'data[Admin][first_name]': "Please enter first name",
            'data[Admin][email]': {
                required: "Please enter email address",
                //$('#errors').html(msg).show();
                mail: "Please enter a valid email address"
            },
            'data[Admin][password]': "Please enter password",
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        },
    });
    var x = validate.form();
    
    if (x) {
        checkEmailExists(1,type);
    } else {
        $("#newmembership").html('Submit');
        $("#newmembership").removeAttr("disabled");
    }
}

function checkEmailExists(arg,type) {
    var url = "/report/checkEmail";
    var email = $.trim($('#email').val());
    $.post(url, {'email': email}, function (res) {
        if (parseInt(res.isExists) === 1) {
            var msg = "Email already exist";
            $('#errors').html(msg).show();
            $("#newmembership").html('Submit');
            $("#newmembership").removeAttr("disabled");
            return false;
        } else {
            $('#errors').html('').hide();
            if (parseInt(arg)) {
                var url = "/report/newuser";
                $('<input>').attr({
                    type: 'hidden',
                    id: 'request_type',
                    name: 'request_type',
                    value: type
                }).appendTo('#new_user_form');
                document.new_user_form.action = url;
                document.new_user_form.submit();
                return false;
            }
        }
    }, 'json');
}
function getContentPartnersReport(date_range,device_type,type)
{
    $('.loader').show();
    $.post('/report/GetContentPartnersViews',{'dt':date_range,device_type:device_type},function(res){
        $('#video-table-body').html(res);
        $('.loader').hide();

    });
}