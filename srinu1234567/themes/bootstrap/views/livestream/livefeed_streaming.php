<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />
        
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            var user_id="<?php echo Yii::app()->user->id;?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <script type='text/javascript'>
        // video js error message
            var you_aborted_the_video_playback ="<?php echo $this->Language["you_aborted_the_video_playback"]; ?>";
            var a_network_error_caused ="<?php echo $this->Language["a_network_error_caused"]; ?>";
            var the_video_playback_was_aborted ="<?php echo $this->Language["the_video_playback_was_aborted"]; ?>";
            var the_video_could_not_be_loaded ="<?php echo $this->Language["the_video_could_not_be_loaded"]; ?>";
            var the_video_is_encrypted ="<?php echo $this->Language["the_video_is_encrypted"]; ?>";
            var no_compatible_source ="<?php echo $this->Language["no_compatible_source"];?>";
            
        </script>
		<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery-migrate-1.0.0.js"></script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />
        <link href="https://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video.js"></script>        
        <script type="text/javascript">
        var isMobile = false; //initiate as false
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

        var techOrderArr = ["html5", "flash"]; // Default for everyone
        if(($.browser.chrome || $.browser.mozilla) && !isMobile ) {
                 techOrderArr = ["flash","html5"];
        } 
        videojs.options.flash.swf = "<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video-js.swf";
        </script>
        <style type="text/css">
           /*.video-js {height: 50%; padding-top: 48%;}*/
           .video-js {padding-top: 56.25%}
           .vjs-fullscreen {padding-top: 0px}  
           .RDVideoHelper{display: none;}
           video::-webkit-media-controls {
                   display:none !important;
           }
        /*.player-text-div{
            bottom: 0;
            height: 270px;
            left: 0;
            margin: auto;
            position: absolute;
            right: 0;
            top: 0;
            width: 700px;
            cursor: pointer;
            font-size: 34px;
            font-weight: 600;
        }*/
        /*@media (max-width: 480px) {
            .player-text-div{
                bottom: 0;
                height: 140px;
                left: 0;
                margin: auto;
                position: absolute;
                right: 0;
                top: 0;
                cursor: pointer;
                font-size: 16px;
                font-weight: 400;
                text-align: center;
                width: 100%;
            }

            .vjs-default-skin .vjs-big-play-button::before {
                content: "";
                font-family: VideoJS;
                height: 100%;
                left: 0;
                line-height: 1.6em;
                position: absolute;
                text-align: center;
                text-shadow: 0.05em 0.05em 0.1em #000;
                width: 100%;
            }
            .vjs-default-skin .vjs-big-play-button {
                background-color: rgba(7, 20, 30, 0.7);
                border: 0.1em solid #3b4249;
                border-radius: 0.4em;
                box-shadow: 0 0 1em rgba(255, 255, 255, 0.25);
                cursor: pointer;
                display: block;
                font-size: 2em;
                height: 1.8em;
                left: 0.5em;
                opacity: 1;
                position: absolute;
                text-align: center;
                top: 0.5em;
                transition: all 0.4s ease 0s;
                vertical-align: middle;
                width: 3em;
                z-index: 2;
            }
        } */
        </style>
           <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/js/backbutton.js"></script>
           <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/css/backbutton.css" />
	</head>
	<body>
            <input type="hidden" id="backbtnlink" value="<?php echo @ Yii::app()->session['backbtnlink'];?>" />
            <?php                
            //$posterURL = 'https://video-js.zencoder.com/oceans-clip.png';
            if(isset($livestream->poster_url) && $livestream->poster_url){
                    $posterURL = $livestream->poster_url;
            }?>
            <div class="wrapper">
                <div class="videocontent">
                    <div class="playercontent" id="stream_player_1" style="position: relative;">
                            <video data-title="Live stream" id="stream_player" class="video-js vjs-default-skin vjs-big-play-centered autobuffer" controls preload="auto"  width="auto" height="auto" poster="<?php echo @$posterURL;?>" >
                                    <source src="<?php echo @$livestream->feed_url;?>" type='video/mp4'  data-title="Live stream"/>
                            </video>
                            <!--<div class="player-text-div" style="color: #fff;position: absolute;"><?php //echo @$livestream->player_text;?></div>-->
                    </div>
                </div>
            </div>
    </body>
    <script type="text/javascript"> 
    _V_("stream_player",{ techOrder: techOrderArr }); 
    var player = videojs('stream_player');
    player.ready(function () {
        var thisIframesHeight = $( window ).height();
        $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px;width:100%;');
        $('#stream_player').bind('contextmenu', function () {
             return false;
        }); 
        player.on("play", function () {
            $('.vjs-big-play-button').css('display', 'none');
            $('.vjs-control-bar').css('display', 'block');
        }); 
        $( window ).resize(function() {
            thisIframesHeight = $( window ).height();
            $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px;width:100%;');
        });
    });
    </script>
    <?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
    <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => $stream_id, 'movie_id' => $movie_id, 'is_live' => 1)); ?>
</html>