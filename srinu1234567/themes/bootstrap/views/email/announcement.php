<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="language" content="en"/>
        <title></title>
    </head>
    <body>  
        <table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                            <tbody>
                                <tr style="background-color:#f3f3f3">
                                    <td style="text-align:left;padding-top:10px">
                                        <div mc:edit="logo"><?php echo $params['logo']; ?></div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                            <p style="display:block;margin:0 0 17px">
                                                <div mc:edit="content"><?php echo $params['content']; ?></div><br/>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody> 
                        </table>
                    </td>
                </tr> 
                <tr>
                    <td>
                        We hope you enjoy receiving e-mail updates from <?php echo $params['StudioName']; ?>. If you do not wish to receive e-mail messages from <?php echo $params['StudioName']; ?>, please <a href="<?php echo $params['unsub_link']; ?>" target="_blank">Click Here</a> to Unsubscribe.
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>