function braintreepaypal() {

    this.processCard = function () {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        payment_method_nonce = $('#payment_method_nonce').val();
        // Processing popup
        if ($.trim(payment_method_nonce)) {
            $(".auth-msg").html("");
            $(".auth-msg").html("Just a moment... we're authenticating your account.<br>Please don't refresh or close this page.");
            $("#loadingPopup").modal('show');
        } else {
            $(".auth-msg").html("");
            $(".auth-msg").html("Just a moment... we're authenticating your card.<br>Please don't refresh or close this page.");
            $("#loadingPopup").modal('show');
        }
        if ($("#ppvModalMain").length) {
            $("#ppvModalMain").addClass('fade');
        }
         var ext_coupon = $('#ext_coupon').val();
        if($.trim(ext_coupon) && $('#third_party_coupon_control').hasClass("active")) {
            var url = HTTP_ROOT + "/userPayment/processCoupon";
        }else{
            var url = HTTP_ROOT + "/user/processCard";
            var card_name = $('#card_name').val();
            var card_number = $('#card_number').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
        }
        
        var name = $('#name').val();
        var email = $('#email').val();
        
       
        var sub_domain = $('#sub-domain').val();


        var plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        }

        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }

        var discount_amount = $('#discount_amount').val();
        var new_free_trail = $('#free_trail_charged').val();
        var coupon = $('#coupon').val();

        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='braintreepaypal' />");
        $.post(url, {'name': name, 'discount_amount': discount_amount, 'new_free_trail': new_free_trail, 'coupon': coupon, 'email': email, 'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'plan_id': plan_id, 'currency_id': currency_id, 'payment_method_nonce': payment_method_nonce, 'ext_coupon': ext_coupon}, function (data) {
            if (parseInt(data.isSuccess) === 1) {
                if (sub_domain == "demo") {
                    $('#conversion-pixel').attr('src', 'conversionPixel');
                }
                $("#loadingPopup").modal('hide');
                if ($.trim(payment_method_nonce)) {
                    $(".success-popup-payment").html("");
                    $(".success-popup-payment").html("Thank you, Your account has been authenticated successfully.");
                    $("#successPopup").modal('show');
                } else {
                    $(".success-popup-payment").html("");
                    $(".success-popup-payment").html("Thank you, Your card has been authenticated successfully.");
                    $("#successPopup").modal('show');
                }
                if($.trim(ext_coupon) && $('#third_party_coupon_control').hasClass("active")) {
                    document.membership_form.action = HTTP_ROOT + "/userPayment/couponCreateUserSubscription";
                    for (var i in data) {
                        $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data[i] + "' />");
                    }
                }else{
                    document.membership_form.action = HTTP_ROOT + "/user/" + action;
                    if (data.card) {
                        for (var i in data.card) {
                            $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                        }

                    }

                    if (data.transaction_data) {
                        for (var i in data.transaction_data) {
                            $("#card_div").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.transaction_data[i] + "' />");
                        }
                    }
                }
                

                setTimeout(function () {
                    document.membership_form.submit();
                    return false;
                }, 5000);
            } else {
                $("#membership_loading").hide();
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalMain").length) {
                    $("#ppvModalMain").removeClass('fade');
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if ($("#paypal").length) {
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    if($.trim(ext_coupon) && $('#third_party_coupon_control').hasClass("active")) {
                        $('#card-info-error').show().html('We are not able to process your coupon. Please try again.');
                    }else{
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
                    
                }
            }
        }, 'json');
    }
}

loadScript = function (url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
};

$(document).ready(function () {
    $('#othergateway_loading').show();
    loadScript(HTTP_ROOT + "/common/js/braintree_action.js", function () {
        loadScript("https://js.braintreegateway.com/js/braintree-2.27.0.min.js", function () {
            var client_token = '';
            var checkout = new Demo({
                formID: 'membership_form'
            });
            if ($('#paypal-container').length) {
                $.post(HTTP_ROOT + "/user/clientToken", function (data) {
                    client_token = data;
                    $('#othergateway_loading').hide();
                    braintree.setup(client_token, "custom", {
                        paypal: {
                            container: "paypal-container"
                        },
                        onPaymentMethodReceived: function (payload) {
                            $('#membership_form').append('<input type="hidden" name="payment_method_nonce" id="payment_method_nonce" value="' + payload.nonce + '" />');
                            $('#braintree-paypal-loggedin').css('max-width', '100%');
                            if ($('#card_detail_div').length) {
                                $('#card_detail_div').hide('slow');
                                $('#save_card_option').hide('slow');
                                $('#eduflix-card-div').css('height', '0px');
                                $('#creditcard').hide('slow');
                                $('#credit_card_detail_info').children('h4').html('Account Details');
                            } else {
                                $('#other-gateway').siblings().hide('slow');
                                $('#eduflix-card-div').css('height', '0px');
                                $('#other-gateway').parent().siblings('h3').html('Account Details');
                            }
                            $('#bt-pp-cancel').click(function () {
                                $('#othergateway_loading').show();
                                location.reload();
                            });
                        }
                    });
                });
            } else {
                $.post(HTTP_ROOT + "/user/clientToken", function (data) {
                    client_token = data;
                    $('#othergateway_loading').hide();
                    braintree.setup(client_token, "custom", {
                        onPaymentMethodReceived: function (payload) {
                            $('#membership_form').append('<input type="hidden" name="payment_method_nonce" id="payment_method_nonce" value="' + payload.nonce + '" />');
                            $('#braintree-paypal-loggedin').css('max-width', '100%');
                            if ($('#card_detail_div').length) {
                                $('#card_detail_div').hide('slow');
                                $('#save_card_option').hide('slow');
                                $('#eduflix-card-div').css('height', '0px');
                                $('#creditcard').hide('slow');
                                $('#credit_card_detail_info').children('h4').html('Account Details');
                            } else {
                                $('#other-gateway').siblings().hide('slow');
                                $('#eduflix-card-div').css('height', '0px');
                                $('#other-gateway').parent().siblings('h3').html('Account Details');
                            }
                            $('#bt-pp-cancel').click(function () {
                                $('#othergateway_loading').show();
                                location.reload();
                            });
                        }
                    });
                });
            }
        });
    });
});