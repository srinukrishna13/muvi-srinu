<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row m-b-40">
    <div class="col-xs-12">
    <?php if (count($subscribers) > 0) {?>
               <div class="input-group">
                   <button data-id="" onclick="javascript: ExportExcel()" class="btn btn-primary waves-effect  m-t-10" type="button">Export to Excel</button>
               </div>   
               <?php }?>
              </div>     
    
</div>

    <div class="row m-b-40">
        <div class="col-md-12">
            <div class="Block">
                <div class="Block-Header">
                            <div class="icon-OuterArea--rectangular">
                                <em class="icon-envelope-letter icon left-icon "></em>
                            </div>
                            <h4>Newsletter Subscribers</h4>
                        
                        
                 
                 </div>
                   <hr>    
                <?php
                if (count($subscribers) > 0) {
                    $c = 0;
                    ?>
                    
                        
                    <table class="table table-responsive" id="example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Email</th>
                                <th data-hide="phone">Subscribed At</th>
                                <th data-hide="phone">Action</th>
                            </tr> 
                    </thead>
                    <tbody>
                            <?php
                            foreach ($subscribers as $subscriber) {
                                $c++;
                                ?>
                                <tr>
                                    <td><?php echo $c; ?></td>
                                    <td><?php echo $subscriber->email; ?></td>
                                    <td><?php echo Yii::app()->common->phpDate($subscriber->date_subscribed); ?></td>
                                    <td>                                
                                        <h5><a href="#" onclick="openDeletepopup('<?php echo $subscriber->id ?>');
                                            return false;"><em class="icon-trash"></em>&nbsp;&nbsp;Remove</a></h5>                           
                                    </td>
                                </tr>

                                <?php
                            }
                            ?>
                        </tbody>
                    </table>                        
                    <?php
                } else {
                    ?>
                    <p class="error">No subscriber found.</p>
                    <?php
                }
                ?>
                  
    
            </div>  
        </div>
    </div>

    <!--Delete Page-->
    <div id="openDeletepopup" class="modal fade form-horizontal" role="dialog" aria-hidden="true" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog" style="">
            <form action="<?php echo $this->createUrl('content/deletesubscriber'); ?>" method="post" id="delsub" enctype="multipart/form-data" id="add_page">
                <input type="hidden" id="sub_id" name="sub_id" value="" />
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="page_title">Delete Subscriber?</h4>
                    </div>
                    <div class="modal-body" id="popup_page_content">              
                        Do you really want to delete this subscriber?
                    </div>              

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">Yes</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>

                </div>
            </form>	
        </div>
    </div>
    <?php if (count($subscribers) > 0) { ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable_custom_paging.js"></script>
<script>
$(document).ready(function() {
$('#example').DataTable( {
        "bInfo" : false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        <?php if(count($subscribers)< 10){ ?>
         "paging":false,  
         <?php } ?>
        createdRow: function ( row ) {
            $('td', row).attr('tabindex', 0);
        }
    });
});

    </script>
    <?php } ?>
    <script type="text/javascript">
        function openDeletepopup(rating) {
            $('#sub_id').val(rating);
           // $("#openDeletepopup").modal('show');
          swal({
            title: "Delete Subscriber?",
            text: "Do you really want to delete this subscriber? ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('#delsub').submit();
        });
        }

        function ExportExcel()
        {
            window.location = '<?php echo Yii::app()->getBaseUrl(true) . "/userfeature/ExportSubscribersExcel"; ?>';
        }
    </script>    
