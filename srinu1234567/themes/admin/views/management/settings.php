<div class="row m-t-20">
    <div class="col-md-8">        
        <?php 
            $content = Yii::app()->general->content_count($studio->id);
            $content_child = Yii::app()->general->content_count_child($studio->id);   
            $ip_address = Yii::app()->getRequest()->getUserHostAddress();
        ?>
        <form class="form-horizontal" role="form" id="content_settings" name="content_settings" method="post" action="<?php echo Yii::app()->baseUrl; ?>/management/SaveContentSetting">
            <div class="form-group">     
                <div class="col-md-12">
                    <h3 class="f-300">Content Types</h3>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <?php 
                            if(empty($content) || (isset($content) && ($content['content_count'] & 1))){
                                $checked = "checked";
                                $hidediv = 0;
                                if($video_on_dimand_avl=='Y'){
                                    $disabled = 'disabled title="Content type can\'t be disabled as you currently have content in this type. Please delete those content and try again."';
                                    echo '<input type="hidden" name="content[]" value="1">';
                                }
                            }else{
                               $checked = ""; 
                               $hidediv = 1;
                               $disabled='';
                            }
                            ?>
                            <input type="checkbox" class="content_settings_cls" name="content[]" <?php echo $checked; ?> <?php echo $disabled; ?> value="1" /> <i class="input-helper"></i>Video On-demand
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <?php 
                            if((isset($content) && ($content['content_count'] & 2))){
                                $checked = "checked";
                                $disabled='';
                                if($video_live_streaming_avl=='Y'){
                                    $disabled = 'disabled title="Content type can\'t be disabled as you currently have content in this type. Please delete those content and try again."';
                                    echo '<input type="hidden" name="content[]" value="2">';
                                }                                
                            }else{
                               $checked = ""; 
                               $disabled='';
                            }
                            ?>
                            <input type="checkbox" class="content_settings_cls" name="content[]" <?php echo $checked; ?> <?php echo $disabled; ?> value="2" /> <i class="input-helper"></i>Video Live Streaming
                        </label>
                    </div>
                </div>
            </div>
            <?php //if($ip_address == '111.93.166.194' || $ip_address == '180.87.230.98'){?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <?php 
                            if((isset($content) && ($content['content_count'] & 4))){
                                $checked = "checked";
                                $hideaudiodiv = 0;
                                $disabled='';
                                if($audio_on_dimand_avl=='Y'){
                                    $disabled = 'disabled title="Content type can\'t be disabled as you currently have content in this type. Please delete those content and try again."';
                                    echo '<input type="hidden" name="content[]" value="4">';
                                }                                
                            }else{
                               $checked = "";
                               $hideaudiodiv = 1;
                               $disabled='';
                            }
                            ?>
                            <input type="checkbox" class="audio_content_settings_cls" name="content[]" <?php echo $checked; ?> <?php echo $disabled; ?> value="4" /> <i class="input-helper"></i>Audio On-demand
                        </label>
                    </div>
                </div>
            </div>
			<div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <?php 
                            if((isset($content) && ($content['content_count'] & 8))){
                                $checked = "checked";
                                $disabled='';
                                if($audio_live_streaming_avl=='Y'){
                                    $disabled = 'disabled title="Content type can\'t be disabled as you currently have content in this type. Please delete those content and try again."';
                                    echo '<input type="hidden" name="content[]" value="8">';
                                }                                
                            }else{
                               $checked = "";
                               $disabled='';
                            }
                            ?>
                            <input type="checkbox" class="audio_content_settings_cls" name="content[]" <?php echo $checked; ?> <?php echo $disabled; ?> value="8" /> <i class="input-helper"></i>Audio Live Streaming
                        </label>
                    </div>
                </div>
            </div>
            <?php //}?>
            <div id="content_child_div" <?php if($hidediv){?>style="display: none;"<?php }?>>
                <div class="form-group">     
                    <div class="col-md-12">
                        <h3 class="f-300">Metadata for Video Type</h3>
                    </div>
                </div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
                                if(empty($content_child) || (isset($content_child) && ($content_child['content_count'] & 1))){
                                    $checked = "checked";
                                }else{
                                   $checked = ""; 
                                }
                                ?>
                                <input type="checkbox" name="content_child[]" <?php echo $checked; ?> value="1" /> <i class="input-helper"></i>Single Part
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display: none;"><!--We are not supporting Single Part Short Form content-->
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
                                if(empty($content_child) || (isset($content_child) && ($content_child['content_count'] & 2))){
                                    $checked = "checked";
                                }else{
                                   $checked = ""; 
                                }
                                ?>
                                <input type="checkbox" name="content_child[]" <?php echo $checked; ?> value="2" /> <i class="input-helper"></i>Single Part Short Form
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
                                if(empty($content_child) || (isset($content_child) && ($content_child['content_count'] & 4))){
                                    $checked = "checked";
                                }else{
                                   $checked = ""; 
                                }
                                ?>
                                <input type="checkbox" name="content_child[]" <?php echo $checked; ?> value="4" /> <i class="input-helper"></i>Multi Part
                            </label>
                        </div>
                    </div>
                </div>
            </div>  
            <div id="audio_content_child_div" <?php if($hideaudiodiv){?>style="display: none;"<?php }?>>
                <div class="form-group">     
                    <div class="col-md-12">
                        <h3 class="f-300">Metadata for Audio Type</h3>
                    </div>
                </div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
                                if(empty($content_child) || (isset($content_child) && ($content_child['content_count'] & 8))){
                                    $checked = "checked";
                                }else{
                                   $checked = ""; 
                                }
                                ?>
                                <input type="checkbox" name="audio_content_child[]" <?php echo $checked; ?> value="8" /> <i class="input-helper"></i>Audio - Single Part
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
                                if(empty($content_child) || (isset($content_child) && ($content_child['content_count'] & 16))){
                                    $checked = "checked";
                                }else{
                                   $checked = ""; 
                                }
                                ?>
                                <input type="checkbox" name="audio_content_child[]" <?php echo $checked; ?> value="16" /> <i class="input-helper"></i>Audio - Multi-part
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="multipart_content_div" >
                <div class="form-group">     
                    <div class="col-md-12">
                        <h3 class="f-300">Multi Part Content Settings</h3>
                    </div>
                </div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
                                if(isset($multipart_content_order) && $multipart_content_order==0){
									$checked = "";
                                }else{
                                 $checked = "checked='checked'";
                                }?>
                                <input type="checkbox" id="multipart_content" name="multipart_content_setting[]" <?php echo $checked; ?> value="32" onchange="update_settings();"/> <i class="input-helper"></i>Show Earliest Episodes First
                            </label>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="Block">            
                <h3 class="m-t-20">Policy</h3>
                <?php $policy = Yii::app()->general->policy_count($studio->id);?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div>
                            <label class="checkbox checkbox-inline m-r-20">
                                <?php
                                if (!empty($policy) || (isset($policy) && ($policy['enable_policy'] & 1))) {
                                    $checked = "checked";
                                } else {
                                    $checked = "";
                                }
                                ?>
                                <input type="checkbox" value="1" <?php echo $checked; ?> name="is_enable_policy">
                                <i class="input-helper"></i> Enable Policy
                            </label>
                        </div>
                    </div>
                </div>        
            </div>
			<div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <h3 class="f-300">Allow Content Downloads</h3>
                    </div>
                </div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php 
								$IsDownloadable = Yii::app()->general->IsDownloadable($studio->id);
                                if($IsDownloadable == 1){
									$checked1 = "checked='checked'";
                                }else{
									$checked1 = "";
                                }?>
                                <input type="checkbox" id="is_downloadable" name="is_downloadable" <?php echo $checked1; ?> value="1" /> <i class="input-helper"></i>Enable Content Downloads
                            </label>
                        </div>
                    </div>
                </div>                
            </div>			
            <div class="Block">            
                <div class="holderjs" id="holder1"></div>
                <div class="form-group">     
                    <div class="col-md-12">
                        <h3 class="f-300">Poster Type</h3>
                    </div>
                </div>
                <p class="m-t-20 c-black f-500">Poster Dimension (in px)</p>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="err_message" class="error red"></div>
                    </div>
                </div>
                <div class="form-group">                    
                    <label class="col-sm-4 control-label">Horizontal:</label>
                    <div class="col-sm-8">
                        <div class="fg-line"><input name="h_poster_dimension" value="<?php echo @$data['h_poster_dimension']?>" class="form-control input-sm" type="text" /></div>
                    </div>
                </div>    
                <div class="form-group"> 
                    <label class="col-sm-4 control-label">Vertical:</label>
                    <div class="col-sm-8">
                        <div class="fg-line"><input name="v_poster_dimension" value="<?php echo @$data['v_poster_dimension']?>" class="form-control input-sm" type="text" /></div>
                    </div>
                </div>
                <?php 
                $pgconfig = Yii::app()->general->getStoreLink();
                if ($pgconfig){?>
                <div class="form-group">
                        <label class="col-sm-4 control-label">Physical Goods:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input name="pg_poster_dimension" value="<?php echo $data['pg_poster_dimension']?>" class="form-control input-sm" type="text"/>
                            </div>
                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
					<label class="col-sm-4 control-label">Category Poster:</label>
					<div class="col-sm-8">
						<div class="fg-line">
							<input name="cat_poster_dimension" value="<?php echo  @$data['category_poster_size']; ?>" class="form-control input-sm" type="text"/>
						</div>
					</div>
                </div>
                <div class="form-group">
                    <div class=" col-md-8 m-t-30">
                        <button type="submit" id="contentsettings_btn" class="btn btn-primary btn-sm" >Save</button>
                    </div>
                </div>                
            </div>
        </form>        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.content_settings_cls').click(function(){
            if($(this).val()==1){
                if($(this).is(':checked')){
                    $('#content_child_div').show('slow');
                }else{
                    $('#content_child_div').hide('slow');
                }
            }
        });
        $('.audio_content_settings_cls').click(function(){
            if($(this).val()==4){
                if($(this).is(':checked')){
                    $('#audio_content_child_div').show('slow');
                }else{
                    $('#audio_content_child_div').hide('slow');
                }
            }
        });
        jQuery.validator.addMethod("dimension", function (value, element) {
            return this.optional(element) || /^[0-9]{1,4}x[0-9]{1,4}$/i.test(value);
        }, "Dimension must be in 123x456 format in px."); 
        $('#contentsettings_btn').click(function(){
        var validate = $("#content_settings").validate({
            rules: {
               'content[]': {required: true},
               'content_child[]': {required: true},
               'audio_content_child[]': {required: '.audio_content_settings_cls:checked'},
               h_poster_dimension: {required: true,dimension: true},   
               v_poster_dimension: {required: true,dimension: true}
               <?php if ($pgconfig){?>
                ,pg_poster_dimension: {required: true,dimension: true}
                <?php }?>
            },
            messages: {
                "content[]": "Please select atleast one from this section",
                "content_child[]": "Please select atleast one from this section",
                'audio_content_child[]': "Please select atleast one from audio section",
            },
            errorPlacement: function(label, element) {
               label.addClass('red');
               label.insertAfter(element.parent());
            },
        });
        var x = validate.form();
        if (x) {
            $('#content_settings').submit();
        }
       }) ;
    });
    
    function update_settings(){
		var chkstatus = 0;
		if($("#multipart_content").is(':checked')){    
		 chkstatus = 1;  
		}
		$.post(HTTP_ROOT+'/management/updatesetting',{"check_res":chkstatus});
      }
</script>