<?php

class SdkController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
        public $layout='main';
        
        
	public function actionIndex()
	{
            $this->pageTitle = 'Build White Label Video on Demand (VoD) Platform, Streaming Site using Muvi SDK - Open Source VoD Platform Software & Server';
            $this->pageKeywords = 'Muvi.com, Muvi SDK, SDK, VoD, PPV, Video on Demand, Video, Pay per View, video content, upload video library, monetize video, video monetization, video revenue';
            $this->pageDescription = 'Muvi SDK is an open Source VOD platform software & server offering video content owners a White Label OTT Video on Demand (VoD) Platform to enable them to launch their own Video Streaming Website at Zero Cost!';            
            $this->render('index');
	}      
        
        public function actionpricing_calculator(){
            $this->pageTitle = 'Muvi VoD ROI Calculator - Cost to build VoD platform, Netflix, Hulu, HBO Go Clone';
            $this->pageKeywords = 'Muvi.com, Muvi SDK, SDK, VoD, PPV, Video on Demand, Video, Pay per View, video content, upload video library, monetize video, video monetization, video revenue';
            $this->pageDescription = 'Calculate the bandwidth cost or cost to build your own VoD platform like Netflix clone, Hulu clone, HBO Go clone, and calculate the ROI of your VoD business';            
            $this->render('pricing_calculator');
        }
        
        public function actionDownloadpdf() {
            $file_name = "MuviSDKBrochure.pdf";
            $path = dirname(__FILE__).'/../../docs/';
            $fullfile = $path.$file_name;
            if (file_exists($fullfile))
            {
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=" . urlencode($file_name));   
                header("Content-Type: application/force-download");
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/download");
                header("Content-Description: File Transfer");            
                header("Content-Length: " . filesize($fullfile));
                flush(); // this doesn't really matter.
                $fp = fopen($fullfile, "r");
                while (!feof($fp))
                {
                    echo fread($fp, 65536);
                    flush(); // this is essential for large downloads
                } 
                fclose($fp);                
            }
            else {
                echo 'File not found.'.$fullfile;
                exit();
            }
        }

        public function actionDownloadsubmit()
        {
            $response = 'error';
            $msg = 'Error in submitting.';

            $dnld_name = isset($_POST['dnld_name'])?$_POST['dnld_name']: '';
            $dnld_email = isset($_POST['dnld_email'])?$_POST['dnld_email']: '';
            $dnld_cpmpany = isset($_POST['dnld_cpmpany'])?$_POST['dnld_cpmpany']: '';

            if($dnld_email != '' && $dnld_name != '')
            {
                $to = 'sales@muvi.com';
                $to_name = 'Muvi';
                $from = $dnld_email;
                $from_name = $dnld_name;
                $site_url = Yii::app()->getBaseUrl(true);

                $logo = 'http://www.muvi.com/themes/bootstrap/images/logo.png';
                $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';

                $msg = '<p>SDK document downloaded!</p>';
                $msg.= '<p>';
                $msg.= '<b>Name :</b> '.$dnld_name.'<br />';
                $msg.= '<b>Email :</b> '.$dnld_email.'<br />';
                $msg.= '<b>Company :</b> '.$dnld_cpmpany.'<br />';
                $msg.= '</p>';

                $params = array(
                    array( 'name' => 'website_name', 'content' => $site_url),
                    array( 'name' => 'logo', 'content' => $logo),
                    array( 'name' => 'msg', 'content' => $msg),
                );

                $subject = 'SDK document downloaded!';
                $message = array('subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $from_name,
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $to_name,
                            'type' => 'to'
                        ),                  
                    )
                );
                $template_name= 'studio_general_contact';
                $this->mandrilEmail($template_name, $params, $message);              

                $response = 'success';
                //$msg = Yii::app()->getbaseUrl(true).'/sdk/downloadpdf';           
                //$msg = $filepath = Yii::app()->getbaseUrl(true).'/docs/MuviSDKBrochure.pdf';
                //$msg = 'Thank you for downloading the broucher.';
                $msg = Yii::app()->getbaseUrl(true).'/sdk/downloadpdf';
            }
            $ret = array('stat' => $response, 'message' => $msg);
            echo json_encode($ret);           
        }       

        public function actionDemorequest()
        {
            $response = 'error';
            $msg = 'Error in submitting.';

            $demo_name = isset($_POST['demo_name'])?$_POST['demo_name']: '';
            $demo_email = isset($_POST['demo_email'])?$_POST['demo_email']: '';
            $demo_company = isset($_POST['demo_company'])?$_POST['demo_company']: '';
            $demo_phone = isset($_POST['demo_phone'])?$_POST['demo_phone']: '';
            if($demo_email != '' && $demo_name != '')
            {
                $to = 'sales@muvi.com';
                $to_name = 'Muvi';
                $from = $demo_email;
                $from_name = $demo_name;
                $site_url = Yii::app()->getBaseUrl(true);

                $logo = 'http://www.muvi.com/themes/bootstrap/images/logo.png';
                $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';

                $msg = '<p>Demo request!</p>';
                $msg.= '<p>';
                $msg.= '<b>Name :</b> '.$demo_name.'<br />';
                $msg.= '<b>Company :</b> '.$demo_company.'<br />';
                $msg.= '<b>Phone  :</b> '.$demo_phone.'<br />';
                $msg.= '<b>Email :</b> '.$demo_email.'<br />';
                $msg.= '</p>';

                $params = array(
                    array( 'name' => 'website_name', 'content' => $site_url),
                    array( 'name' => 'logo', 'content' => $logo),
                    array( 'name' => 'msg', 'content' => $msg),
                );

                $subject = 'Demo request!';
                $message = array('subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $from_name,
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $to_name,
                            'type' => 'to'
                        ),                  
                    )
                );
                $template_name= 'studio_general_contact';
                $msg =  $this->mandrilEmail($template_name, $params, $message);              
                $response = 'success';      
                $msg = 'Thank you for your interest. Someone from our team will contact you shortly!';
            }
            $ret = array('stat' => $response, 'message' => $msg);
            echo json_encode($ret);           
        } 
        
}