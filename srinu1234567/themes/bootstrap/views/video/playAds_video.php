<?php
    $v = RELEASE;
    $randomVar = rand();
    $studio_ad = (isset($studio_ads) && count($studio_ads) > 0) ? $studio_ads[0] : array();
    //echo $fullmovie_path;exit();
?>
<!DOCTYPE html>
<!--
    ##Author : Ads G
    ##Functionality : Ads Integration using Videojs ft VPAID
    ##Integration : SPOTX Ads Server Integration part
    ##Date : 21-3-2017
-->
<head>
    <!DOCTYPE html> 
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
    <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
    <!-- VideoJs required styles-->
    <link rel="stylesheet" type="text/css" href="https://mailonline.github.io/videojs-vast-vpaid/demo/videojs_5/video-js.css"/>
    <!-- VideoJs.vast.vpaid required styles-->
    <link rel="stylesheet" type="text/css" href="https://mailonline.github.io/videojs-vast-vpaid/styles/videojs.vast.vpaid.css"/>
    <!-- DEMO required styles-->
    <link rel="stylesheet" type="text/css" href="https://mailonline.github.io/videojs-vast-vpaid/demo/styles/demo.css"/>
    <!-- Web Fonts 
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    -->
    <script type="text/javascript">
        // this is mandatory for IE8 !!!!!
        document.createElement('video');
        document.createElement('audio');
        document.createElement('track');
    </script>
    <!-- VideoJs required scripts-->
    <script type="text/javascript" src="https://mailonline.github.io/videojs-vast-vpaid/demo/videojs_5/video.js"></script>
    <!-- VideoJs.vast.vpaid required scripts-->
    <script type="text/javascript" src="https://mailonline.github.io/videojs-vast-vpaid/scripts/es5-shim.js"></script>
    <script type="text/javascript" src="https://mailonline.github.io/videojs-vast-vpaid/scripts/ie8fix.js"></script>
    <script type="text/javascript" src="https://mailonline.github.io/videojs-vast-vpaid/scripts/videojs_5.vast.vpaid.js"></script>
    <!-- DEMO required scripts 
    <script type="text/javascript" src="https://mailonline.github.io/videojs-vast-vpaid/demo/scripts/demo.js"></script>
    -->
    <script src="https://code.jquery.com/jquery-2.1.3.js"></script>	

</head>
<body>
    <div class="wrapper">
        <div class="videocontent" style="overflow:hidden;">
            <form id="vast-vpaid-form">
                <div class="vjs-video-container">
                    <!--video-player-->	
                    <video id="video_block" class="video-js moo-css vjs-default-skin" controls preload="auto" poster="" >
                        <?php echo '<source src="' . $fullmovie_path . '" data-res="" type="video/mp4" />'; ?>
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that
                        </p>
                    </video>
                </div>
            </form>
        </div>
    </div>
    <script>
        //var adTag = "https://search.spotxchange.com/vast/2.0/85394?VPAID=js&content_page_url=https://demo.arvind.com/player/testing-first-video&cb=67832734&player_width=100%&player_height=100%";
        var vastAd ="";
        var player ="";
        var options ="";
        var videoContainer = formEl.querySelector('div.vjs-video-container');
    </script>
    <script src="<?php echo Yii::app()->getbaseUrl(); ?>/adstesting/spotx/demo_II.js?v=<?php echo $v ?>"></script>
    <script>
        //Ads URL 
        function getAdsUrl() {
            var cachbuserrandnumber = getCacheBusterRandom(8);
            return "https://search.spotxchange.com/vast/2.0/<?php echo $studio_ad->channel_id ?>?VPAID=js&content_page_url=" + encodeURIComponent(window.location.href) + "&cb=" + cachbuserrandnumber + "&player_width=" + $('#videojs_PlayerID').innerWidth() + "&player_height=" + $('#videojs_PlayerID').innerHeight();
        }
        function getCacheBusterRandom(length) { 
            return Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));
        }
    </script>
</body>
</html>
<?php
//https://www.spotxchange.com/tag-generator/
?>
