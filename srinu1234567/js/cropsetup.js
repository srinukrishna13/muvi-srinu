/*$(function($){
 // Create variables (in this scope) to hold the API and image size
 var jcrop_api,
 boundx,
 boundy,
 // Grab some information about the preview pane
 $preview = $('#preview-pane'),
 $pcnt = $('#preview-pane .preview-container'),
 $pimg = $('#preview-pane .preview-container img'),
 xsize = $pcnt.width(),
 ysize = $pcnt.height();
 
 
 $('#target').Jcrop({
 onChange: updatePreview,
 onSelect: updatePreview,
 bgOpacity: 0.5,
 aspectRatio: xsize / ysize
 },function(){
 // Use the API to get the real image size
 var bounds = this.getBounds();
 boundx = bounds[0];
 boundy = bounds[1];
 
 jcrop_api = this; // Store the API in the jcrop_api variable
 
 // Move the preview into the jcrop container for css positioning
 $preview.appendTo(jcrop_api.ui.holder);
 });
 
 function updatePreview(c) {
 if (parseInt(c.w) > 0) {
 var rx = xsize / c.w;
 var ry = ysize / c.h;
 
 $('#x').val(c.x);
 $('#y').val(c.y);
 $('#w').val(c.w);
 $('#h').val(c.h);
 
 $pimg.css({
 width: Math.round(rx * boundx) + 'px',
 height: Math.round(ry * boundy) + 'px',
 marginLeft: '-' + Math.round(rx * c.x) + 'px',
 marginTop: '-' + Math.round(ry * c.y) + 'px'
 });
 }
 }
 
 });*/
// convert bytes into friendly format
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0)
        return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
}
;

// check for selected crop region
function checkForm() {
    if (parseInt($('#w').val()))
        return true;
    $('.error').html('Please select a crop region and then press Upload').show();
    return false;
}
;

// update info by cropping (onChange and onSelect events handler)
function updateInfo(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#x1').val(x);
    $('#y1').val(y);
    $('#x2').val(x2);
    $('#y2').val(y2);
    $('#w').val(e.w);
    $('#h').val(e.h);
}
;

// update info by cropping transparent app icon (onChange and onSelect events handler)
function updateInfoTransparent(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#x101').val(x);
    $('#y101').val(y);
    $('#x201').val(x2);
    $('#y201').val(y2);
    $('#w5').val(e.w);
    $('#h5').val(e.h);
}
;

// clear info by cropping transparent (onRelease event handler)
function clearInfoTransparent() {
    $('#x101').val('');
    $('#y101').val('');
    $('#x201').val('');
    $('#y201').val('');
    $('#w5').val('');
    $('#h5').val('');
    $('button[type="submit"]').removeAttr('disabled');
}

//APP ICON FOR APPLE TV START(UPDATE)
//_1 START
function updateAppIconOne(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#x101').val(x);
    $('#y101').val(y);
    $('#x201').val(x2);
    $('#y201').val(y2);
    $('#w01').val(e.w);
    $('#h01').val(e.h);
}
;
//_1 END

//_2 START
function updateAppIconTwo(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#x102').val(x);
    $('#y102').val(y);
    $('#x202').val(x2);
    $('#y202').val(y2);
    $('#w02').val(e.w);
    $('#h02').val(e.h);
}
;
//_2 END

//_3 START
function updateAppIconThree(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#x103').val(x);
    $('#y103').val(y);
    $('#x203').val(x2);
    $('#y203').val(y2);
    $('#w03').val(e.w);
    $('#h03').val(e.h);
}
;
//_3 END

//APP ICON FOR APPLE TV END(UPDATE)

function updateInfoRokuSplash(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#x14').val(x);
    $('#y14').val(y);
    $('#x24').val(x2);
    $('#y24').val(y2);
    $('#w4').val(e.w);
    $('#h4').val(e.h);
}
;

function updateInfoSplash(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#x11').val(x);
    $('#y11').val(y);
    $('#x21').val(x2);
    $('#y21').val(y2);
    $('#w1').val(e.w);
    $('#h1').val(e.h);
}
;

function updateInfoSplashLscape(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#x112').val(x);
    $('#y112').val(y);
    $('#x212').val(x2);
    $('#y212').val(y2);
    $('#w12').val(e.w);
    $('#h12').val(e.h);
}
;

function updateInfoFeatureGraphic(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#x12').val(x);
    $('#y12').val(y);
    $('#x22').val(x2);
    $('#y22').val(y2);
    $('#w2').val(e.w);
    $('#h2').val(e.h);
}
;

function updateAppleFeatureGraphic(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#x5').val(x);
    $('#y5').val(y);
    $('#x25').val(x2);
    $('#y25').val(y2);
    $('#w5').val(e.w);
    $('#h5').val(e.h);
}
;
function updateInfoallImage(e)
{
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#x13').val(x);
    $('#y13').val(y);
    $('#x23').val(x2);
    $('#y23').val(y2);
    $('#w3').val(e.w);
    $('#h3').val(e.h);

}

function updateInfoFaviconGallery(e)
{
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;      
    $('#x17').val(x);
    $('#y17').val(y);
    $('#x27').val(x2);
    $('#y27').val(y2);
    $('#w7').val(e.w);
    $('#h7').val(e.h);

}

function updateInfoFavicon(e)
{
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2; 
    $('#x16').val(x);
    $('#y16').val(y);
    $('#x26').val(x2);
    $('#y26').val(y2);
    $('#w6').val(e.w);
    $('#h6').val(e.h);

}
// clear info by cropping (onRelease event handler)
function clearInfo() {
    $('#x1').val('');
    $('#y1').val('');
    $('#x2').val('');
    $('#y2').val('');
    $('#w').val('');
    $('#h').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearInfoRokuSplash() {
    $('#x14').val('');
    $('#y14').val('');
    $('#x24').val('');
    $('#y24').val('');
    $('#w4').val('');
    $('#h4').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearInfoSplash() {
    $('#x11').val('');
    $('#y11').val('');
    $('#x21').val('');
    $('#y21').val('');
    $('#w1').val('');
    $('#h1').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

//APP ICON FOR APPLE TV START(CLEAR)
//TUTORIAL SCREEN START

function clearInfoTutorialScreen() {
    $('#screen_x1').val('');
    $('#screen_y1').val('');
    $('#screen_x2').val('');
    $('#screen_y2').val('');
    $('#screen_w2').val('');
    $('#screen_h2').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;


function updateInfoTutorialScreen(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;    
    $('#screen_x1').val(x);
    $('#screen_y1').val(y);
    $('#screen_x2').val(x2);
    $('#screen_y2').val(y2);
    $('#screen_w2').val(e.w);
    $('#screen_h2').val(e.h);
}
;
//TUTORIAL SCREEN END

//_1 START
function clearInfoAppIconOne() {
    $('#x101').val('');
    $('#y101').val('');
    $('#x201').val('');
    $('#y201').val('');
    $('#w01').val('');
    $('#h01').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;
//_1 END

//_2 START
function clearInfoAppIconTwo() {
    $('#x102').val('');
    $('#y102').val('');
    $('#x202').val('');
    $('#y202').val('');
    $('#w02').val('');
    $('#h02').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;
//_2 END

//_3 START
function clearInfoAppIconThree() {
    $('#x103').val('');
    $('#y103').val('');
    $('#x203').val('');
    $('#y203').val('');
    $('#w03').val('');
    $('#h03').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;
//_3 END

//APP ICON FOR APPLE TV END(CLEAR)


function clearInfoSplashLscape() {
    $('#x112').val('');
    $('#y112').val('');
    $('#x212').val('');
    $('#y212').val('');
    $('#w12').val('');
    $('#h12').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearInfoFeatureGraphic() {
    $('#x12').val('');
    $('#y12').val('');
    $('#x22').val('');
    $('#y22').val('');
    $('#w2').val('');
    $('#h2').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearAppleFeatureGraphic() {
    $('#x5').val('');
    $('#y5').val('');
    $('#x25').val('');
    $('#y25').val('');
    $('#w5').val('');
    $('#h5').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearInfoFavicon() {
    $('#x16').val('');
    $('#y16').val('');
    $('#x26').val('');
    $('#y26').val('');
    $('#w6').val('');
    $('#h6').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearInfofaviconGallery() {
    $('#x17').val('');
    $('#y17').val('');
    $('#x27').val('');
    $('#y27').val('');
    $('#w7').val('');
    $('#h7').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;

function clearInfoallImage() {
    $('#x13').val('');
    $('#y13').val('');
    $('#x23').val('');
    $('#y23').val('');
    $('#w3').val('');
    $('#h3').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
;



// Create variables (in this scope) to hold the Jcrop API and image size
var jcrop_api, boundx, boundy;

function fileSelectHandler() {

    // get selected file
    var oFile = $('#celeb_pic')[0].files[0];

    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (!rFilter.test(oFile.type)) {
        $('.error').html('Please select a valid image file (jpg and png are allowed)').show();
        return;
    }

    // check for file size
    if (oFile.size > 1024 * 1024) {
        $('.error').html('You have selected too big file, please select a one smaller image file').show();
        return;
    }

    // preview element
    var oImage = document.getElementById('preview');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function(e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        oImage.onload = function() { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#preview').width(oImage.naturalWidth);
                $('#preview').height(oImage.naturalHeight);
            }

            setTimeout(function() {
                // initialize Jcrop
                $('#preview').Jcrop({
                    minSize: [32, 32], // min crop size
                    aspectRatio: 1, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function() {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                });
            }, 3000);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}

function updateCropImageInfo(e, curSel) {
    $('#' + curSel).find('.x1').val(e.x);
    $('#' + curSel).find('.y1').val(e.y);
    $('#' + curSel).find('.x2').val(e.x2);
    $('#' + curSel).find('.y2').val(e.y2);
    $('#' + curSel).find('.w').val(e.w);
    $('#' + curSel).find('.h').val(e.h);
}
;
function clearCropImageInfo(curSel) {
    $('#' + curSel).find('.x1').val('');
    $('#' + curSel).find('.y1').val('');
    $('#' + curSel).find('.x2').val('');
    $('#' + curSel).find('.y2').val('');
    $('#' + curSel).find('.w').val('');
    $('#' + curSel).find('.h').val('');
    $('#' + curSel).find('button[type="submit"]').removeAttr('disabled');
}
;