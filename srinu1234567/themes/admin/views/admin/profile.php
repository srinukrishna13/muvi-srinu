<style type="text/css">
	.profile-body dl dd,dl dt{
		line-height: 2;
		font-size: 16px;
	}
</style>
<div class="col-md-12 nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#tab_1">Profile Details</a></li>
<!--		<li><a data-toggle="tab" href="#tab_2">Card Details</a></li>
		<li><a data-toggle="tab" href="#tab_3">Change Password</a></li>-->
	</ul>
	
	<div class="tab-content profile-body">
		<div id="tab_1" class="tab-pane active"> 
			<div class="col-md-6">
				<dl class="dl-horizontal">
					<dt>Name: </dt>
					<dd><?php if($userdata['first_name']){ echo $userdata['first_name'].' '.$userdata['last_name'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Email: </dt><dd><?php echo $userdata['email'];?></dd>
					<dt>Company: </dt><dd><?php if($userdata['company']){ echo $userdata['company'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Company Type: </dt><dd><?php if($userdata['type_of_company']){ echo $userdata['type_of_company'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Logo: </dt><dd><?php if($userdata['logo']){ echo $userdata['logo'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
				</dl>
			</div>
			<div class="col-md-6">
				<dl class="dl-horizontal">
					<dt>Address1: </dt><dd><?php if($userdata['address_1']){ echo '<address>'.$userdata['address_1'].'</address>';}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Address2: </dt><dd><?php if($userdata['address_2']){ echo '<address>'.$userdata['address_2'].'</address>';}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Phone No: </dt><dd><?php if($userdata['phone_no']){ echo $userdata['phone_no'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>City: </dt><dd><?php if($userdata['city']){ echo $userdata['city'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>State: </dt><dd><?php if($userdata['state']){ echo $userdata['state'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Country: </dt><dd><?php if($userdata['country']){ echo $userdata['country'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Zip: </dt><dd><?php if($userdata['zip']){ echo $userdata['zip'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
				</dl>
			</div>
			<div class="pull-right" style="position: absolute;top: 17%;right: 3%;display: none;cursor: pointer;" id="pedit_icon"><a href="<?php echo $this->createUrl('admin/editProfile');?>"><span class="glyphicon glyphicon-pencil"></span><br/>Edit</a></div>
		</div>
<!--		<div id="tab_2" class="tab-pane"> 
			<div class="col-md-6">
				<dl class="dl-horizontal">
					<dt>Card Holder Name: </dt>
					<dd><?php if($userdata['first_name']){ echo $userdata['first_name'].' '.$userdata['last_name'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Card Type: </dt><dd><?php if($userdata['credit_card_type']){echo $userdata['credit_card_type'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Card Number: </dt><dd><?php if($userdata['credit_card_no']){echo 'XXXXXXXXXXXX'.substr($userdata['credit_card_no'], -4);}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Expiry Date: </dt><dd><?php if($userdata['credit_card_mm']){ echo $userdata['credit_card_mm']."/".$userdata['credit_card_yyyy'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
				</dl>
			</div>
			<div class="col-md-6">
				<dl class="dl-horizontal">
					<dt>Address1: </dt><dd><?php if($userdata['address_1']){ echo '<address>'.$userdata['address_1'].'</address>';}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Address2: </dt><dd><?php if($userdata['address_2']){ echo '<address>'.$userdata['address_2'].'</address>';}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Phone No: </dt><dd><?php if($userdata['phone_no']){ echo $userdata['phone_no'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>City: </dt><dd><?php if($userdata['city']){ echo $userdata['city'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>State: </dt><dd><?php if($userdata['state']){ echo $userdata['state'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Country: </dt><dd><?php if($userdata['country']){ echo $userdata['country'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
					<dt>Zip: </dt><dd><?php if($userdata['zip']){ echo $userdata['zip'];}else{echo "<small><em>Not Available</em></small>";}?></dd>
				</dl>
			</div>
		</div>
		<div id="tab_3" class="tab-pane"> 
			<div class="col-lg-6">
					<form role="form" action="javascript:void(0);" data-toggle="validator">
						<div class="box-body">
							<div class="form-group">
								<label for="oldpassword">Old Password</label>
								<input type="password" placeholder="Enter old password" id="oldpassword" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="newPassword">New Password</label>
								<input type="password" placeholder="Enter New Password" id="newpassword" name="newpassword" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="confirmpassword">Re-Enter New Password</label>
								<input type="password" placeholder="Re-Enter New Password" id="confirmpassword" class="form-control" name="confirmpassword" required data-match ="#newpassword">
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Submit</button>
						</div>
					</form>
			</div>
		</div>-->
	</div>
</div>
<script type="text/javascript">
	$('#tab_1').hover(function(){
		$('#pedit_icon').show();
	},function(){
		$('#pedit_icon').hide();
	});
</script>