$(document).ready(function(){ 
    
    $('#loginModal').on('hidden', function () {
        $('#username').val("");
        $("#password").val('');
        $("#join_password").val('');
        $("#email").val('');
   });
   
    $('#login-loading').hide();
    $('#login-btn').click(function(){
        $("#user-login-form").validate({
            rules: {
                username: {
                    required: true,
                    mail: true
                },
                password: {
                    required: true,
                }
            },
            messages: {
                username: {
                    required: "Please enter email address",
                    mail: "Please enter a valid email address",
                },
                password: {
                    required: "Please enter your password",
                }
            },            
            
            submitHandler: function(form) {
                var hid_pw = base64_encode($("#password").val());
                $("#password").val(hid_pw);
                console.log($('#user-login-form').serialize());
                $.ajax({
                    url: website_url+"/authenticate_user",
                    data: $('#user-login-form').serialize(),
                    type:'POST',
                    dataType: "jsonp",
                    jsonpCallback: 'jsonpcallback',
                    beforeSend:function(){
                        $('#login-loading').show();
                    },
                    complete:function(){
                        $('#login-loading').hide();
                    }
                });             
            }
        });    
    }); 
    
 /* Register */
    $('#register-loading').hide();
    $('#register-btn').click(function(){
        $("#user-register-form").validate({
            rules: {               
                email: {
                    required: true,
                    mail: true
                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {             
                email: {
                    required: "Please enter email address",
                    mail: "Please enter a valid email address",
                },
                password: {
                    required: "Please enter your password",
                    minlength: "Your password must be at least 6 characters long"
                }
            },                
            submitHandler: function(form) {
                var hid_pw = base64_encode($("#join_password").val());
                $("#join_password").val(hid_pw);
                $.ajax({
                    url: website_url+"/auth_signup",
                    data: $('#user-register-form').serialize(),
                    type:'POST',
                    dataType: "jsonp",
                    jsonpCallback: 'jsonpcallback_reg',
                    beforeSend:function(){
                        $('#register-loading').show();
                    },
                    complete:function(){
                        $('#register-loading').hide();
                    }
                });             
            }            
        });
    });    
});

function jsonpcallback(data){
    $('#login-loading').hide();
    console.log(data);
    var objs = jQuery.parseJSON(data); 
    var items = objs.items;
    for(i = 0; i < items.length; i++)
    {
        var item = items[i];
        var result = item.result;
        if(result === 'incorrect login')
        {
            $('#password').val('');
            $("#login-errors").show();
            $("#login-errors").html("Username or Password is incorrect!");            
        }
        else
        {
            var logged_user_id = '', logged_email = '', logged_fb_id = '', logged_display_name = '';
            var address1 = '', city = '', state = '', country = '', zip = '', zip = '', phone = '';
            var user_image = '';var is_studio_admin='';var studio_user_id = '';
            var token = '';var is_developer = '';
            var remember = $('#remember_me').prop('checked');
            var agent = $('#oauthApp').val();
            for(j = 0; j < result.length; j++)
            {
                if(j === 0)
                {
                    var user_data = result[j].user_info;
                    logged_user_id = user_data.id;
                    logged_email = user_data.email;
                    is_developer = user_data.is_developer;
                    logged_fb_id = user_data.facebook_id;
                    token = user_data.token;
                    is_studio_admin = user_data.is_studio_admin;
                    studio_user_id = user_data.studio_user_id;
                
                    var user_profile = result[j].user_profile;
                    if(user_profile != null)
                    {
                        logged_display_name = user_profile.display_name;
                    }
                    
                    var user_address = result[j].user_address;
                    if(user_address != null)
                    {
                        address1 = user_address.address1; 
                        city = user_address.city;
                        state = user_address.state;
                        country = user_address.country;
                        zip = user_address.zip;
                        phone = user_address.phone;
                    }
                    user_image = result[j].user_image;
                }
            }
            if(logged_user_id > 0)
            {
                var cur_url = window.location.href;
                $.ajax({
                    url: HTTP_ROOT+"/site/muvilogin",
                    data: {user_id:logged_user_id, email:logged_email, remember:remember, token:token, fb_id:logged_fb_id, display_name:logged_display_name, agent:agent, address1:address1, city:city, state:state, country:country, zip:zip, phone:phone, user_image:user_image,'is_studio_admin':is_studio_admin,'studio_user_id':studio_user_id,'is_developer':is_developer},
                    type:'POST',
                    success: function(data){
                        //window.location = HTTP_ROOT+'/user/login';
                        //window.location.reload();
                        
                        if(cur_url.indexOf('movie') && ($('#play_url').length) && ($('#to_play').length) && $('#to_play').val() == 'play')
                            window.location = $('#play_url').val();
                        else
                            window.location = HTTP_ROOT+'/tv-show';
                    }          
                });    
            }         
        }
    }
}

function jsonpcallback_reg(data)
{
    console.log(data);
    var objs = jQuery.parseJSON(data); 
    var items = objs.items;
    for(i = 0; i < items.length; i++)
    {
        var item = items[i];
        var result = item.result;
        if(result == 'Email already exists')
        {
            $('#join_password').val('');
            $("#register-errors").show();
            $("#register-errors").html("Email already exists!");             
        }
        else if(isNaN(result) === false && result > 0)
        {
            $('#join_password').val('');
            $("#register-errors").show();
            $("#register-errors").html("Registration Successful.");  

            setTimeout(function() {
              location.href = HTTP_ROOT+'/user/login';
            }, 800);
        
        }        
        //if(result === 'Email already exists')
        else
        {
            var logged_user_id = '', logged_email = '', logged_fb_id = '', logged_display_name = '';
            var address1 = '', city = '', state = '', country = '', zip = '', zip = '', phone = '';
            var user_image = '', token = '';var is_studio_admin='';var studio_user_id = '';var is_developer ='';
            var agent = $('#oauthApp').val();
            for(j = 0; j < result.length; j++)
            {
                if(j === 0)
                {
                    var user_data = result[j].user_info;
                    logged_user_id = user_data.id;
                    logged_email = user_data.email;
                    is_developer= user_data.is_developer;
                    logged_fb_id = user_data.facebook_id;
                    token = user_data.token;
                
                    var user_profile = result[j].user_profile;
                    if(user_profile != null)
                    {
                        logged_display_name = user_profile.display_name;
                    }
                    
                    var user_address = result[j].user_address;
                    if(user_address != null)
                    {
                        address1 = user_address.address1; 
                        city = user_address.city;
                        state = user_address.state;
                        country = user_address.country;
                        zip = user_address.zip;
                        phone = user_address.phone;
                    }
                    user_image = result[j].user_image;
                }
            }
            if(logged_user_id > 0)
            {
                $.ajax({
                    url: HTTP_ROOT+"/site/muvilogin",
                    data: {user_id:logged_user_id, email:logged_email, token:token, fb_id:logged_fb_id, display_name:logged_display_name, agent:agent, address1:address1, city:city, state:state, country:country, zip:zip, phone:phone, user_image:user_image,'first_login':1,'is_developer':is_developer},
                    type:'POST',
                    success: function(data){
                        window.location = HTTP_ROOT+'/user/process';
                    }          
                });    
            }             
        }
    }
}
