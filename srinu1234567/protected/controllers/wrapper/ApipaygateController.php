<?php

require_once 'PayGate/global.inc.php';
require_once 'PayGate/paygate.payhost_soap.php';

class ApipaygateController extends Controller {

    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }

    /**
     *
     * Initialize the api
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() {
        extract($_POST, EXTR_OVERWRITE);
        ini_set("soap.wsdl_cache_enabled", "0");
        $soapClient = new SoapClient('https://secure.paygate.co.za/PayHost/process.trans?wsdl', array('trace' => 1));
        return $soapClient;
    }

    /**
     * Validate the card and make a 0.1 transaction
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCard($arg = array()) {
        $soapClient = self::initializePaymentGateway();
        /*if((isset($arg['save_this_card']) && $arg['save_this_card']) || (isset($arg['ppv_plan']) && $arg['ppv_plan'] == 0)){
            return self::cardAuth($arg);
        }else{
            return $res['isSuccess'] = 1;
        }*/
        if(isset($arg['is_physical']) && intval($arg['is_physical'])){
            return $res['isSuccess'] = 1;
        }else{
        return self::cardAuth($arg);
    }
    }
    
    function cardAuth($arg = array()) {
        $soapClient = self::initializePaymentGateway();
        $padDateMonth = str_pad($arg['exp_month'], 2, '0', STR_PAD_LEFT);
        $expDateYear = urlencode($arg['exp_year']);
        $expDate = $padDateMonth . $expDateYear;
        $amount = 200; //test transaction for authuntication of card
        $logfile = dirname(__FILE__).'/paygate.txt';
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $msg .= "\n-----Currency: ".$arg['currency_code']."-----\n";
        
        if (intval($this->IS_CURRENCY_CONVERSION['paygate']) && isset($arg['currency_code']) && trim($arg['currency_code']) != 'ZAR') {
            $arg['currency_code'] = 'ZAR';
        }
        
        $request = <<<XML
                    <ns1:SinglePaymentRequest>
                        <ns1:CardPaymentRequest>
                            <ns1:Account>
                                    <ns1:PayGateId>{$this->PAYMENT_GATEWAY_API_USER['paygate']}</ns1:PayGateId>
                                    <ns1:Password>{$this->PAYMENT_GATEWAY_API_PASSWORD['paygate']}</ns1:Password>
                            </ns1:Account>
                            <ns1:Customer>
                                <ns1:FirstName>{$arg['card_name']}</ns1:FirstName>
                                <ns1:LastName>{$arg['card_name']}</ns1:LastName>
                                <ns1:Email>{$arg['email']}</ns1:Email>
                            </ns1:Customer>
                            <ns1:CardNumber>{$arg['card_number']}</ns1:CardNumber>
                            <ns1:CardExpiryDate>{$expDate}</ns1:CardExpiryDate>
                            <ns1:CVV>{$arg['cvv']}</ns1:CVV>
                            <ns1:Vault>true</ns1:Vault>
                            <ns1:BudgetPeriod>0</ns1:BudgetPeriod>
                            <ns1:Redirect>
                                    <ns1:NotifyUrl>{$arg['cancelURL']}</ns1:NotifyUrl>
                                    <ns1:ReturnUrl>{$arg['returnURL']}</ns1:ReturnUrl>
                            </ns1:Redirect>
                            <ns1:Order>
                                <ns1:MerchantOrderId>{$arg['plan_desc']}</ns1:MerchantOrderId>
                                <ns1:Currency>{$arg['currency_code']}</ns1:Currency>
                                <ns1:Amount>{$amount}</ns1:Amount>
                            </ns1:Order>
                        </ns1:CardPaymentRequest>
                    </ns1:SinglePaymentRequest>
XML;
        //echo '<pre>', htmlentities($request), '</pre>';exit;
        try {
            $result = $soapClient->__soapCall('SinglePayment', array(
                new SoapVar($request, XSD_ANYXML)
            ));
            //print "<pre>";print_r($result);
            $msg .= "\n--------------------------Result---------------------------------------\n";
            $data = serialize($result);
            $msg.= $data."\n";
            //Write into the log file
            file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        } catch (SoapFault $sf) {
            $err = $sf->getMessage();
            // "<pre>";print_r($err);
            $msg .= "\n--------------------------Error---------------------------------------\n";
            $data = serialize($err);
            $msg.= $data."\n";
            //Write into the log file
            file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        }
        if (isset($err) && trim($err)) {
            $res['isSuccess'] = 0;
            $res['code'] = 404;
            $res['response_text'] = $sf->getMessage();
            $res['Message'] = 'Invalid details entered. Please check again';
            return $res;
        } else {
            
            if(isset($result) && array_key_exists('Redirect', $result->CardPaymentResponse)){
                $res['isSuccess'] = 1;
                $res['redirect_data']['code'] = 200;
                $res['redirect_data']['status'] = 'succeeded';
                if (array_key_exists('Redirect', $result->CardPaymentResponse)) {
                    $res['url'] = $result->CardPaymentResponse->Redirect->RedirectUrl;
                }
                foreach ($result->CardPaymentResponse->Redirect->UrlParams as $url) {
                    $res['params'][$url->key] = $url->value;
                }
                self::hash_call($res);
            }else{
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = $result->CardPaymentResponse->Status->TransactionStatusDescription;
                $res['Message'] = $result->CardPaymentResponse->Status->ResultDescription;
                return $res;
            }
            
        }
    }
    
    function getVaultData($arg) {
        $soapClient = self::initializePaymentGateway();
        $request = <<<XML
                    <ns1:SingleFollowUpRequest>
                        <ns1:QueryRequest>
                            <ns1:Account>
                                <ns1:PayGateId>{$this->PAYMENT_GATEWAY_API_USER['paygate']}</ns1:PayGateId>
                                <ns1:Password>{$this->PAYMENT_GATEWAY_API_PASSWORD['paygate']}</ns1:Password>
                            </ns1:Account>
                            <ns1:PayRequestId>{$arg['PAY_REQUEST_ID']}</ns1:PayRequestId>
                        </ns1:QueryRequest>
                    </ns1:SingleFollowUpRequest>
XML;
        try {
            $result = $soapClient->__soapCall('SingleFollowUp', array(
                new SoapVar($request, XSD_ANYXML)
            ));
        } catch (SoapFault $sf) {
            $err = $sf->getMessage();
        }
        if (isset($err) && trim($err)) {
            $res['isSuccess'] = 0;
            $res['code'] = 404;
            $res['response_text'] = $sf->getMessage();
            $res['Message'] = 'Invalid details entered. Please check again';
        } else {
            $res['isSuccess'] = 1;
            $res['code'] = 200;
            $res['response'] = $result;
        }
        return $res;
    }
    
    function processTransactions ($user = array()) {
        $soapClient = self::initializePaymentGateway();
        $amount = $user['amount'];
        $currency = strtoupper($user['currency_code']);
        
        if (intval($this->IS_CURRENCY_CONVERSION['paygate']) && isset($currency) && trim($currency) != 'ZAR') {
            $user['exchanged_currency_code'] = $user['currency_code'];
            $user['exchanged_amount'] = $amount;
            $user['exchanged_currency_id'] = $user['currency_id'];
            $user['amount'] = Yii::app()->billing->currencyConversion($currency, 'ZAR', $amount);
            $user['currency_code'] = 'ZAR';
        }
        
        if(isset($user['token']) && trim($user['token'])){
            return self::doTokenPayment($user);
        }else{
            return self::doDirectPayment($user);
        }
        
    }
    
    function doTokenPayment ($user = array()) {
        $soapClient = self::initializePaymentGateway();
        $user['plan_desc'] = $user['name'].$user['amount'];
        $amount = $user['amount']*100;
        $request = <<<XML
                    <ns1:SinglePaymentRequest>
                        <ns1:CardPaymentRequest>
                            <ns1:Account>
                                <ns1:PayGateId>{$this->PAYMENT_GATEWAY_API_SIGNATURE['paygate']}</ns1:PayGateId>
                                <ns1:Password>{$this->PAYMENT_GATEWAY_NON_3D_SECURE['paygate']}</ns1:Password>
                            </ns1:Account>
                            <ns1:Customer>
                                <ns1:FirstName>{$user['card_holder_name']}</ns1:FirstName>
                                <ns1:LastName>{$user['card_holder_name']}</ns1:LastName>
                                <ns1:Email>{$user['email']}</ns1:Email>
                            </ns1:Customer>
                            <ns1:VaultId>{$user['token']}</ns1:VaultId>
                            <ns1:CVV>000</ns1:CVV>
                            <ns1:BudgetPeriod>0</ns1:BudgetPeriod>
                            <ns1:Order>
                                <ns1:MerchantOrderId>{$user['plan_desc']}</ns1:MerchantOrderId>
                                <ns1:Currency>{$user['currency_code']}</ns1:Currency>
                                <ns1:Amount>{$amount}</ns1:Amount>
                            </ns1:Order>
                        </ns1:CardPaymentRequest>
                    </ns1:SinglePaymentRequest>
XML;
        try {
            $result = $soapClient->__soapCall('SinglePayment', array(
                new SoapVar($request, XSD_ANYXML)
            ));
        } catch (SoapFault $sf) {
            $err = $sf->getMessage();
        }
        if (isset($err) && trim($err)) {
            $res['isSuccess'] = 0;
            $res['code'] = 404;
            $res['response_text'] = $sf->getMessage();
            $res['Message'] = 'Invalid details entered. Please check again';
        } else {
            if(trim($result->CardPaymentResponse->Status->ResultDescription) == 'Declined'){
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = json_encode($result);
                $res['Message'] = 'Transaction' .$result->CardPaymentResponse->Status->ResultDescription;
            }else{
                $res['isSuccess'] = 1;
                $res['code'] = 200;
                $res['transaction_status'] = $result->CardPaymentResponse->Status->StatusName;
                $res['invoice_id'] = $result->CardPaymentResponse->Status->TransactionId;
                $res['order_number'] = $result->CardPaymentResponse->Status->TransactionId;
                $res['paid_amount'] = $user['amount'];
                $res['response_text'] = json_encode($result);
                $res['is_success'] = 1;
                $res['response'] = $result;
            }
        }
        return $res;
    }
    
    
    function doDirectPayment ($user = array()) {
        $soapClient = self::initializePaymentGateway();
        $padDateMonth = str_pad($user['exp_month'], 2, '0', STR_PAD_LEFT);
        $expDateYear = urlencode($user['exp_year']);
        $expDate = $padDateMonth . $expDateYear;
        $amount = $user['amount']*100;
        if(isset($user['permalink']) && trim($user['permalink'])){
            $mechatorderid = $user['permalink'];
        }else{
            $mechatorderid = $user['plan_desc'];
        }
        
        $request = <<<XML
                <ns1:SinglePaymentRequest>
                    <ns1:CardPaymentRequest>
                        <ns1:Account>
                            <ns1:PayGateId>{$this->PAYMENT_GATEWAY_API_SIGNATURE['paygate']}</ns1:PayGateId>
                            <ns1:Password>{$this->PAYMENT_GATEWAY_NON_3D_SECURE['paygate']}</ns1:Password>
                        </ns1:Account>
                        <ns1:Customer>
                            <ns1:FirstName>{$user['card_name']}</ns1:FirstName>
                            <ns1:LastName>{$user['card_name']}</ns1:LastName>
                            <ns1:Email>{$user['email']}</ns1:Email>
                        </ns1:Customer>
                        <ns1:CardNumber>{$user['card_number']}</ns1:CardNumber>
                        <ns1:CardExpiryDate>{$expDate}</ns1:CardExpiryDate>
                        <ns1:CVV>{$user['security_code']}</ns1:CVV>
                        <ns1:BudgetPeriod>0</ns1:BudgetPeriod>
                        <ns1:Order>
                            <ns1:MerchantOrderId>{$mechatorderid}</ns1:MerchantOrderId>
                            <ns1:Currency>{$user['currency_code']}</ns1:Currency>
                            <ns1:Amount>{$amount}</ns1:Amount>
                        </ns1:Order>
                    </ns1:CardPaymentRequest>
                </ns1:SinglePaymentRequest>
XML;
        //echo '<pre>', htmlentities($request), '</pre>';exit;
        try {
            $result = $soapClient->__soapCall('SinglePayment', array(
                new SoapVar($request, XSD_ANYXML)
            ));
        } catch (SoapFault $sf) {
            $err = $sf->getMessage();
        }
        if (isset($err) && trim($err)) {
            $res['is_success'] = 0;
            $res['code'] = 404;
            $res['response_text'] = $sf->getMessage();
            $res['Message'] = 'Invalid details entered. Please check again';
        } else {
            $res['is_success'] = 1;
            $res['code'] = 200;
            $res['paid_amount'] = $user['exchanged_amount'];
            $res['bill_amount'] = $user['exchanged_amount'];
            $res['exchanged_currency_code'] = $user['exchanged_currency_code'];
            $res['exchanged_currency_id'] = $user['exchanged_currency_id'];
            $res['amount'] = ($result->CardPaymentResponse->Status->Amount)/100;
            $res['transaction_status'] = $result->CardPaymentResponse->Status->StatusName;
            $res['invoice_id'] = $result->CardPaymentResponse->Status->PayRequestId;
            $res['order_number'] = $result->CardPaymentResponse->Status->TransactionId;
            $res['response_text'] = json_encode($result);
        }
        //echo '<pre>';print_r($result);exit;
        return $res;
    }
    
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        return true;
    }
    
    function hash_call($params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $params['url']);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params['params']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $server_output = curl_exec($ch);
        echo $server_output;
        
        curl_close($ch);
    }

    function sampleIntegrationTransaction($user = array()) {
        //$soapClient = self::initializePaymentGateway();
        $res = array();
        $res['isSuccess'] = 1;
        return $res;
    }
    
}
