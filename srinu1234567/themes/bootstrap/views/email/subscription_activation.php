<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"><?php echo $params['logo']; ?></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	Hi <span mc:edit="fname"><?php echo $params['fname']; ?></span>,
                            </p>              
			   <p style="display:block;margin:0 0 17px">
                                Welcome to Muvi!
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                Thank you for activating your subscription. <span mc:edit="attached_file"><?php echo $params['attached_file']; ?></span>
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                Muvi team is always here to help. Just reply to this email (studio@muvi.com) if you have any questions at any time, a Muvi representative will reply you right away.
                            </p>
                        	<p style="display:block;margin:0 0 17px">
                                <strong>Your VOD platform’s Admin Panel: </strong>www.muvi.com
                                <br/>Login email address: <span mc:edit="email"><?php echo $params['email']; ?></span>
                        	</p>
                        	<p style="display:block;margin:0 0 17px;background-color:#42B7DA;width:70%;text-align:center;font-weight:bold;">
                        		<a href="http://www.muvi.com/" style="color:#fff;text-decoration:none;display:block">Log in to Admin Panel</a>
                        	</p>
                        	<p style="display:block;margin:0 0 17px;">
                                <strong>Your preview website: </strong><span mc:edit="domain"><?php echo $params['domain']; ?></span>
                        	</p>
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%;font-family:helvetica,Arial;">
                            <tbody>
                                <tr>
                                    <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left;width:70%;">
                                        Regards,
                                        <p style="font-size:14px;margin:2px 0px">
                                            Team Muvi
                                        </p>
                                    </td>
                             		<td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"><?php echo $params['fb_link']; ?></span>&nbsp;<span mc:edit="gplus_link"><?php echo $params['gplus_link']; ?></span>&nbsp;<span mc:edit="twitter_link"><?php echo $params['twitter_link']; ?></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>