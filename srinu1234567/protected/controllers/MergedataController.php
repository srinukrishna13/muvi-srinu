<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
use Guzzle\Http\EntityBody;
class MergedataController extends Controller
{
    public function init(){   
		parent::init();
                
    }
    public function actionmovie(){
        set_time_limit(0);
        error_reporting(1);
        $source_s3 = S3Client::factory(array(
                        'key'    => "AKIAJ3ZLG727M5FK33HQ",
                        'secret' => "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",
                     ));
        $source_url = "https://muviassetsdev.s3.amazonaws.com/";
        $dest_s3 = S3Client::factory(array(
                        'key'    => "AKIAJ4HTSCRXGHFTKRBQ",
                        'secret' => "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",
                     ));
        $dest_bucket = "muvistudio";
        $pgconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=wiki_data','postgres','R0ckst@r');
        $muvi_sql = "Select f.*,ms.studio_user_id from films as f inner join movie_streams as ms ON (f.id = ms.movie_id) and ms.full_movie IS NOT NULL where f.name IS NOT NULL  order by id desc";
        $res = $pgconnection->createCommand($muvi_sql)->queryAll();
        $f_cnt = count($res);
        for($i = 0; $i < $f_cnt; $i++){
            $ext_film = Film::model()->findAll(array("condition" => "permalink = '". $res[$i]['permalink']."'"));
            $studio = Studio::model() -> findByPk($res[$i]['studio_user_id']);
            $content = strtolower(trim($res[$i]['content_type']));
            $content_type = "";
            if($content){
                $c_type = StudioContentType::model()->find(array("select" => "id", "condition" => "lower(muvi_alias) = '".$content."' AND studio_id = ".$res[$i]['studio_user_id']));
                $content_type =  $c_type -> id;
            }
            if(count($ext_film) == 0){
            $film = new Film();
            $film -> name = $res[$i]['name'];
            $film -> uniq_id = $this->generateUniqNumber();
            $film -> language = $res[$i]['language'];
            $film -> genre = $res[$i]['genre'];
            $film -> story = $res[$i]['story'];
            $film -> release_date = $res[$i]['release_date'];
            $film -> movie_payment_type	= $res[$i]['movie_payment_type'];
            $film -> is_verified = true;
            $film -> content_type_id = $content_type;
            $film -> studio_id = $res[$i]['studio_user_id'];
            $save_res = $film -> save(false);
            if($save_res){
                //////// movie streams /////////////////
                $ms_sql = "Select * from movie_streams where movie_id =".$res[$i]['id'];
                $ms_res = $pgconnection->createCommand($ms_sql)->queryAll();
                $m_count = count($ms_res);
                for($m = 0; $m < $m_count; $m++ ){
                    $ms = new movieStreams();
                    $ms -> movie_id = $film -> id;
                    $ms -> full_movie = $ms_res[$m]['full_movie'];
                    $ms -> studio_id = $res[$i]['studio_user_id'];
                    $ms -> episode_number = $ms_res[$m]['episode_number'];
                    $ms -> episode_title = $ms_res[$m]['episode_title'];
                    $ms -> episode_story = $ms_res[$m]['episode_story'];
                    $ms -> episode_date = $ms_res[$m]['episode_date'];
                    $ms -> is_episode = $ms_res[$m]['is_episode'];
                    $ms -> series_number = $ms_res[$m]['series_number'];
                    $ms -> is_popular = $ms_res[$m]['is_popular'];
                    $ms -> id_seq = $ms_res[$m]['id_seq'];
                    $ms -> is_poster = $ms_res[$m]['is_poster'];
                    $ms -> wiki_data = $ms_res[$m]['id'];
                    $ms->save(false);
                    if($ms -> is_episode){
                        $poster = $this->check_default_poster($ms_res[$m]['id'], "Movie", "Episode", "episode");
                        if ($poster){
                            $poster_sql = "SELECT * FROM posters WHERE  id IN (".$poster.") AND rank=1 ORDER BY id DESC";
                            $poster_image = $pgconnection->createCommand($poster_sql)->queryAll();
                            if(!empty($poster_image)){
                                    if($poster_image[0]['poster_file_name']){
                                            $poster_name = str_replace(" ", '%20', $poster_image[0]['poster_file_name']);
                                            $poster_id = $poster_image[0]['id'];
                                    }else{
                                            $poster_name = "";
                                            $poster_id = "";
                                    }
                            }
                        }
                        $ext_poster = Poster::model()->find(array("condition" => "object_id = ".$ms -> id." AND object_type = 'moviestream'"));
                        if(!$ext_poster){
                            $poster = new Poster();
                            $poster -> object_id = $ms->id;
                            $poster -> object_type = 'moviestream';
                            $poster -> poster_file_name = $poster_name;
                            $poster -> ip = $_SERVER['REMOTE_ADDR'];
                            $poster -> wiki_data = $poster_id;
                            $poster -> save(false);
                        }  
                    }
                }
                
                ///////// poster of movie /////////////
                $poster  = $this->check_default_poster($res[$i]['id'], "Movie", "Poster", "poster");
                if ($poster){
                    $film_poster_sql = "SELECT * FROM posters WHERE  id IN (".$poster.") AND rank=1 ORDER BY id DESC";
                    $film_poster_res = $pgconnection->createCommand($film_poster_sql)->queryAll();
                    if(!empty($film_poster_res)){
                        $c_poster = new Poster();
                        $c_poster -> object_id = $film -> id;
                        $c_poster -> object_type = 'films';
                        $c_poster -> poster_file_name = $film_poster_res[0]['poster_file_name'];
                        $c_poster -> ip = $_SERVER['REMOTE_ADDR'];
                        $c_poster -> wiki_data = $film_poster_res[0]['id'];
                        $c_poster->save(false);
                    }
                }
                
                ///////// top banner of movie /////////////
                $top_baner  = $this->check_default_poster($res[$i]['id'], "Movie", "Banner", "banner");
                if ($top_baner){
                    $poster_sql = "SELECT * FROM posters WHERE  id IN (".$top_baner.") AND rank=1 ORDER BY id DESC";
                    $cposter_res = $pgconnection->createCommand($poster_sql)->queryAll();
                    if(!empty($cposter_res)){
                        $c_poster = new Poster();
                        $c_poster -> object_id = $film -> id;
                        $c_poster -> object_type = 'topbanner';
                        $c_poster -> poster_file_name = $cposter_res[0]['poster_file_name'];
                        $c_poster -> ip = $_SERVER['REMOTE_ADDR'];
                        $c_poster -> wiki_data = $cposter_res[0]['id'];
                        $c_poster->save(false);
                    }
                }
                
                //////// Trailer of movie ///////////
                
                $trailer_url = $this->getOldTrailer($res[$i]['id']);
                echo $trailer_url;
                if($trailer_url){
                    $ext_trailer = movieTrailer::model()->find(array("condition" => "movie_id = ".$film->id));
                    if(!$ext_trailer){
                        $trailer = $this->getOldTrailer($res[$i]['id'],1);
                        $movie_trailer = new movieTrailer();
                        $movie_trailer -> trailer_file_name = $trailer["trailer_file_name"];
                        $movie_trailer -> movie_id = $film -> id;
                        $movie_trailer -> video_remote_url = $trailer_url;
                        $movie_trailer -> ip = $_SERVER['REMOTE_ADDR'];
                        $movie_trailer->save(false);
                    }
                }
                //// Celebrity & movie casts//////////////
                $cast_sql = "Select * from movie_casts where movie_id = ".$res[$i]['id'];
                $cast_res = $pgconnection->createCommand($cast_sql)->queryAll();
                $cast_cnt = count($cast_res);
                if($cast_cnt > 0){
                    for($j = 0; $j < $cast_cnt; $j++){
                        if($cast_res[$j]['celebrity_id'] != ""){
                            $celeb_sql = "Select * from celebrities where id = ".$cast_res[$j]['celebrity_id'];
                            $celeb_res = $pgconnection->createCommand($celeb_sql)->queryAll();
                            $ext_celeb = Celebrity::model()->findAll(array("condition" => "permalink = '".$celeb_res[0]['permalink']."'","limit" => "1"));
                            if(count($ext_celeb) == 0){
                               $celeb = new Celebrity(); 
                               $celeb -> name = $celeb_res[0]['name'];
                               $celeb -> summary = $celeb_res[0]['summary'];
                               $celeb -> studio_id = $res[$i]['studio_user_id'];
                               $celeb -> created_date = date('Y-m-d H:i:s');
                               if($celeb -> save(false)){
                                   $movie_cast = new MovieCast();
                                   $movie_cast -> movie_id = $film -> id;
                                   $movie_cast -> celebrity_id = $celeb -> id;
                                   $movie_cast -> cast_type = $cast_res[$j]['cast_type'];
                                   $movie_cast -> created_date = date('Y-m-d H:i:s');
                                   $movie_cast -> save(false);
                                   $celeb_poster  = $this->check_default_poster($celeb_res[0]['id'], "Celebrity", "Poster", "profilepic");
                                    if ($celeb_poster){
                                        $poster_sql = "SELECT * FROM posters WHERE  id IN (".$celeb_poster.") AND rank=1 ORDER BY id DESC";
                                        $cposter_res = $pgconnection->createCommand($poster_sql)->queryAll();
                                        if(!empty($cposter_res)){
                                            $c_poster = new Poster();
                                            $c_poster -> object_id = $celeb->id;
                                            $c_poster -> object_type = 'celebrity';
                                            $c_poster -> poster_file_name = $cposter_res[0]['poster_file_name'];
                                            $c_poster -> ip = $_SERVER['REMOTE_ADDR'];
                                            $c_poster -> wiki_data = $cposter_res[0]['id'];
                                            $c_poster->save(false);
                                        }
                                    }else{
                                        if($celeb_res[0]['profile_picture_file_name'] != ""){
                                            $c_poster = new Poster();
                                            $c_poster -> object_id = $celeb->id;
                                            $c_poster -> object_type = 'celebrity';
                                            $c_poster -> poster_file_name = $celeb_res[0]['profile_picture_file_name'];
                                            $c_poster -> ip = $_SERVER['REMOTE_ADDR'];
                                            $c_poster -> wiki_data = $celeb_res[0]['id'];
                                            $c_poster->save(false);
                                        }
                                    }
                               }
                            }else{
                                $movie_cast = new MovieCast();
                                $movie_cast -> movie_id = $film -> id;
                                $movie_cast -> celebrity_id = $ext_celeb[0]['id'];
                                $movie_cast -> cast_type = $cast_res[$j]['cast_type'];
                                $movie_cast -> created_date = date('Y-m-d H:i:s');
                                $movie_cast -> save(false);
                            }
                        }
                    }
                }
            }
            }    
        }
    }
    public function actionget_banner(){
        $pgconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=wiki_data','postgres','R0ckst@r');
        $muvi_sql = "Select f.*,ms.studio_user_id from films as f inner join movie_streams as ms ON (f.id = ms.movie_id) and ms.full_movie IS NOT NULL where f.name IS NOT NULL limit 5";
        $res = $pgconnection->createCommand($muvi_sql)->queryAll();
        $f_cnt = count($res);
        for($i = 0; $i < $f_cnt; $i++){
            $top_baner  = $this->check_default_poster($res[0]['id'], "Movie", "Banner", "banner");
            if ($top_baner){
                $poster_sql = "SELECT * FROM posters WHERE  id IN (".$top_baner.") AND rank=1 ORDER BY id DESC";
                $cposter_res = $pgconnection->createCommand($poster_sql)->queryAll();
                if(!empty($cposter_res)){
                    $c_poster = new Poster();
                    $c_poster -> object_id = $res[0]['id'];
                    $c_poster -> object_type = 'topbanner';
                    $c_poster -> poster_file_name = $cposter_res[0]['poster_file_name'];
                    $c_poster -> ip = $_SERVER['REMOTE_ADDR'];
                    $c_poster -> wiki_data = true;
                    $c_poster->save(false);
                }
            }else{
                if($celeb_res[0]['profile_picture_file_name'] != ""){
                    $c_poster = new Poster();
                    $c_poster -> object_id = $res[0]['id'];
                    $c_poster -> object_type = 'topbanner';
                    $c_poster -> poster_file_name = $celeb_res[0]['profile_picture_file_name'];
                    $c_poster -> ip = $_SERVER['REMOTE_ADDR'];
                    $c_poster -> wiki_data = true;
                    $c_poster->save(false);
                }
            }
        }
    }
    
    function check_default_poster($tagger_id, $tagger_type, $taggable_type, $context){
            $id = "";
            $poster = '';
            $dbcon = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=muvi_data','postgres','R0ckst@r');

            $sql = "SELECT t2.taggable_id FROM taggings t2,taggings t1 WHERE t1.taggable_id=t2.taggable_id AND lower(t1.tagger_type)='".$context."' AND t2.taggable_type='".$taggable_type."' AND t2.tagger_id='".$tagger_id."' AND t2.tagger_type='".$tagger_type."' GROUP BY t2.taggable_id";
            $poster = $dbcon->createCommand($sql)->queryAll();
            if(!empty($poster)){
              foreach($poster AS $pos){
                    $id .= $pos['taggable_id'].",";
                    }
              $id = trim($id,',');
              }
            if($id){
              return $id;
            }else{
              return false;
            }
    }
    function getOldTrailer($movie_id='',$return_type=0){
            $pgconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=muvi_data','postgres','R0ckst@r');
            $video_id = "";$videoids = "";$video = array();$videos = "";
            $sql = "SELECT taggable_id FROM taggings WHERE tagger_id = ".$movie_id." and taggable_type = 'Video' ";
            $data = $pgconnection->createCommand($sql)->queryAll();
            if($data){
                    foreach($data AS $key=>$val){
                            @$video_id .= $val['taggable_id'].',';
                    }
                    if(@$video_id){
                            $query ="SELECT taggable_id FROM taggings WHERE taggable_id IN (".trim($video_id,',').") and lower(tagger_type) = 'trailer'  ORDER BY id DESC";
                            $record = $pgconnection->createCommand($query)->queryAll();
                            if($record){
                                    foreach($record AS $k=>$v){
                                            @$videoids .= $v['taggable_id'].",";
                                    }					
                                    $connectionwiki = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=wiki_data','postgres','R0ckst@r');
                                    $videosql = "SELECT trailer_file_name,id FROM videos WHERE id IN(".trim($videoids,',').") AND rank = 1 ORDER BY id DESC LIMIT 1";
                                    $videos = $connectionwiki->createCommand($videosql)->queryAll();
                                    if($videos){
                                            if($return_type){
                                                    return $videos[0];
                                            }else{
                                                    if($videos[0]['trailer_file_name']){
                                            return 'https://dfquahprf1i3f.cloudfront.net/public/system/trailers/'.$videos[0]['id'].'/original/'.$videos[0]['trailer_file_name'];
                                    }else{
                                            return;
                                    }
                                            }	
                            }else{
                                            return;
                                    }
                            }else{
                                    return ;
                            }
                    }else{
                            return ;
                    }
            }else{
                    return ;
            }
    }
    
    public  function actionget_users(){
        set_time_limit(0);
        $pgconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=muvi_data','postgres','R0ckst@r');
        $sql = "select * from users where registered_from IS NOT NULL AND email IS NOT NULL order by id desc";
        $records = $pgconnection->createCommand($sql)->queryAll();
        //print_r($records);exit;
        
        if(count($records) > 0){
            foreach ($records as $key => $val){
                $user_id = $val['id'];
                $prof_sql = "select * from user_profiles where user_id = ".$user_id;
                $user_profile = $pgconnection->createCommand($prof_sql)->queryAll();
                $ext_user = SdkUser::model()->find(array("condition" => " email = '".$val['email']."'"));
                $studio = Studio::model()->find(array("condition" => "subdomain = '".$val['registered_from']. "'"));
                $studio_id = $studio -> id;
                if(count($ext_user) != 0){
                    /*$user = new SdkUser();
                    $user -> email = $val['email'];
                    $user -> studio_id = $studio_id;
                    $user -> encrypted_password = $val['encrypted_password'];
                    $user -> password_salt = $val['password_salt'];
                    $user -> reset_password_token = $val['reset_password_token'];
                    $user -> remember_token = $val['remember_token'];
                    $user -> remember_created_at = $val['remember_created_at'];
                    $user -> oauth2_uid = $val['oauth2_uid'];
                    $user -> oauth2_token = $val['oauth2_token'];
                    $user -> display_name = $user_profile[0]['display_name'];
                    $user -> is_admin = $val['is_admin'];
                    $user -> confirmation_token = $val['confirmation_token'];
                    $user -> confirmed_at = $val['confirmed_at'];
                    $user -> confirmation_sent_at = $val['confirmation_sent_at'];
                    $user -> is_approved = $val['is_approved'];
                    $user -> facebook_id = $val['facebook_id'];
                    $user -> gender = $val['gender'];
                    $user -> language = $val['language'];
                    $user -> authentication_token = $val['authentication_token'];
                    $user -> subscribe_notification = $val['subscribe_notification'];
                    $user -> subscribe_newsletter = $val['subscribe_newsletter'];
                    $user -> is_studio_admin = $val['is_studio_admin'];
                    $user -> token = $val['token'];
                    $user -> registered_from = $val['registered_from'];
                    $user -> source = $val['source'];
                    $user -> signup_ip = $val['signup_ip'];
                    $user -> is_developer = $val['is_developer'];
                    if($user -> save(false)){
                        //////////////user_addresses data merge//////////
                        $addr_sql = "Select * from user_addresses where user_id = ".$user_id;
                        $user_address_old = $pgconnection->createCommand($addr_sql)->queryAll();
                        $user_addr = UserAddress::model()->find(array("condition" => "user_id = ".$user->id));
                        if(count($user_addr) == 0 && $user_address_old){
                            $user_address = new UserAddress();
                            $user_address -> user_id = $user->id;
                            $user_address -> address1 = $user_address_old[0]['address1'];
                            $user_address -> address2 = $user_address_old[0]['address2'];
                            $user_address -> city = $user_address_old[0]['city'];
                            $user_address -> state = $user_address_old[0]['state'];
                            $user_address -> phone = $user_address_old[0]['phone'];
                            $user_address -> country =$user_address_old[0]['country'];
                            $user_address -> zip = $user_address_old[0]['zip'];
                            $user_address->save(false);
                        }*/
                        //////////////user_payment_logs data merge//////////
                        $log_sql = "Select * from user_payment_logs where user_id = ".$user_id;
                        $user_payment_old = $pgconnection->createCommand($log_sql)->queryAll();
                        //$user_logs = UserPaymentLog::model()->find(array("condition" => "user_id = ".$user->id));
                        //if(count($user_logs) == 0 && $user_payment_old){
                        for($i = 0; $i < count($user_payment_old); $i++){    
                            $user_payment_log = new UserPaymentLog();
                            $user_payment_log -> user_id = $ext_user->id;
                            $user_payment_log -> plan_id = $user_payment_old[$i]['plan_id'];
                            $user_payment_log -> payment_tried = $user_payment_old[$i]['payment_tried'];
                            $user_payment_log -> payment_tried_at = $user_payment_old[$i]['payment_tried_at'];
                            $user_payment_log -> payment_done = $user_payment_old[$i]['payment_done'];
                            $user_payment_log -> payment_done_at = $user_payment_old[$i]['payment_done_at'];
                            $user_payment_log -> studio_id = $studio_id;
                            $user_payment_log -> ip_address = $user_payment_old[$i]['ip_address'];
                            $user_payment_log -> payment_method = $user_payment_old[$i]['payment_method'];
                            $user_payment_log -> payment_cancelled = $user_payment_old[$i]['payment_cancelled'];
                            $user_payment_log -> payment_cancelled_at = $user_payment_old[$i]['payment_cancelled_at'];
                            $user_payment_log -> payment_token = $user_payment_old[$i]['payment_token'];
                            $user_payment_log -> payment_parameters = $user_payment_old[$i]['payment_parameters'];
                            $user_payment_log -> return_parameters =$user_payment_old[$i]['return_parameters'];
                            $user_payment_log -> movie_id = $user_payment_old[$i]['movie_id'];
                            $user_payment_log->save(false);
                        }
                        //}
                        
                        //////////////user_subscriptions data merge//////////
                        $subs_sql = "Select * from user_subscriptions where user_id = ".$user_id;
                        $user_subs_old = $pgconnection->createCommand($subs_sql)->queryAll();
                        //$user_subscriptions = UserSubscription::model()->find(array("condition" => "user_id = ".$user->id));
                        //if(count($user_subscriptions) == 0 && $user_subscriptions){
                        for($i = 0; $i < count($user_subs_old); $i++){    
                            $user_subs = new UserSubscription();
                            $user_subs -> user_id = $ext_user->id;
                            $user_subs -> plan_id = $user_subs_old[$i]['plan_id'];
                            $user_subs -> studio_id = $studio_id;
                            $user_subs -> transaction_id = $user_subs_old[$i]['transaction_id'];
                            $user_subs -> payment_date = $user_subs_old[$i]['payment_date'];
                            $user_subs -> amount = $user_subs_old[$i]['amount'];
                            $user_subs -> start_date =$user_subs_old[$i]['start_date'];
                            $user_subs -> end_date = $user_subs_old[$i]['end_date'];
                            $user_subs -> status = $user_subs_old[$i]['status'];
                            $user_subs -> cancel_date = $user_subs_old[$i]['cancel_date'];
                            $user_subs -> cancel_id = $user_subs_old[$i]['cancel_id'];
                            $user_subs -> mode = $user_subs_old[$i]['mode'];
                            $user_subs -> profile_id = $user_subs_old[$i]['profile_id'];
                            $user_subs -> cancel_reason_id =$user_subs_old[$i]['cancel_reason_id'];
                            $user_subs -> cancel_note = $user_subs_old[$i]['cancel_note'];
                            $user_subs->save(false);
                        }
                        
                        //////////////transactions data merge//////////
                        $trans_sql = "Select * from transactions where user_id = ".$user_id;
                        $user_trans_old = $pgconnection->createCommand($trans_sql)->queryAll();
                        //$user_transaction = Transaction::model()->find(array("condition" => "user_id = ".$user->id));
                        //if(count($user_transaction) == 0 && $user_trans_old){
                        for($i = 0; $i < count($user_trans_old); $i++){    
                            $user_trans = new Transaction();
                            $user_trans -> user_id = $ext_user->id;
                            $user_trans -> studio_id = $studio_id ;
                            $user_trans -> plan_id = $user_trans_old[$i]['plan_id'];
                            $user_trans -> transaction_date = $user_trans_old[$i]['transaction_date'];
                            $user_trans -> payment_method = $user_trans_old[$i]['payment_method'];
                            $user_trans -> transaction_status = $user_trans_old[$i]['transaction_status'];
                            $user_trans -> invoice_id =$user_trans_old[$i]['invoice_id'];
                            $user_trans -> order_number = $user_trans_old[$i]['order_number'];
                            $user_trans -> amount = $user_trans_old[$i]['amount'];
                            $user_trans -> fullname = $user_trans_old[$i]['fullname'];
                            $user_trans -> address1 = $user_trans_old[$i]['address1'];
                            $user_trans -> address2 = $user_trans_old[$i]['address2'];
                            $user_trans -> city = $user_trans_old[$i]['city'];
                            $user_trans -> state =$user_trans_old[$i]['state'];
                            $user_trans -> phone = $user_trans_old[$i]['phone'];
                            $user_trans -> country = $user_trans_old[$i]['country'];
                            $user_trans -> zip = $user_trans_old[$i]['zip'];
                            $user_trans -> payer_id = $user_trans_old[$i]['payer_id'];
                            $user_trans -> subscription_id = $user_trans_old[$i]['subscription_id'];
                            $user_trans -> ipn_track_id = $user_trans_old[$i]['ipn_track_id'];
                            $user_trans->save(false);
                        }
                    //}
                    
                }
            }
        }
    }
    public function actionsetuser_created_at(){
        set_time_limit(0);
        $pgconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=muvi_data','postgres','R0ckst@r');
        $sql = "select * from users where registered_from IS NOT NULL AND email IS NOT NULL order by id desc";
        $records = $pgconnection->createCommand($sql)->queryAll();
        foreach ($records as $key => $val){
            $ext_user = SdkUser::model()->find(array("condition" => " email = '".$val['email']."'"));
            if(count($ext_user) > 0 ){
                $ext_user->created_date = $val['created_at'];
                $ext_user->save(false);
            }
        }
        echo "ddd";exit;
    }
    public function actiongetepisode_poster(){
        set_time_limit(0);
        $pgconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=wiki_data','postgres','R0ckst@r');
        $sql = "select * from movie_streams where is_episode = 1 order by id desc limit 3";
        $episodes = $pgconnection->createCommand($sql)->queryAll();
        foreach($episodes as $key => $val){
            $movie_id = $val['movie_id'];
            $poster = $this->check_default_poster($val['id'], "Movie", "Episode", "episode");
            $mov_poster = str_replace('medium','standard',$this->getPoster($movie_id));
            if ($poster){
                $muviconnection = new CDbConnection('pgsql:host=54.84.118.3;port=5432;dbname=wiki_data','postgres','R0ckst@r');
                $poster_sql = "SELECT * FROM posters WHERE  id IN (".$poster.") AND rank=1 ORDER BY id DESC";
                $poster_image = $muviconnection->createCommand($poster_sql)->queryAll();
                if(!empty($poster_image)){
                        if($poster_image[0]['poster_file_name']){
                                $poster_name = str_replace(" ", '%20', $poster_image[0]['poster_file_name']);
                        }else{
                                $poster_name = "";
                        }
                }
            }
            echo $poster_name;
            $ext_episode = movieStreams::model()-> find(array("condition" => "wiki_data = ".$val['id']));
            $ext_poster = Poster::model()->find(array("condition" => "object_id = ".$val['id']." AND object_type = 'moviestream'"));
            if(!$ext_poster){
                $poster = new Poster();
                $poster -> object_id = $ext_episode['id'];
                $poster -> object_type = 'moviestream';
                $poster -> poster_file_name = $poster_name;
                $poster -> ip = $_SERVER['REMOTE_ADDR'];
                $poster -> wiki_data = $poster_image[0]['id'];
                $poster -> save(false);
            }     
        }
    }
}
