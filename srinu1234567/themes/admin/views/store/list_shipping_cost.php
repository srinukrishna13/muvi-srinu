<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-multiselect.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<?php 
$selcontents = '';
if (isset($data) && !empty($data)) {
    $selcontents = json_encode($data);
}
$restrict_country = json_decode($settings['restrict_country']);
?>
<div class="row m-b-20">
</div>
<div class="filter-div">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-settings left-icon "></em>
        </div>
        <h4>Shipping Rules</h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <form class="form-horizontal" method="post" role="form" id="account" name="account" action="javascript:void(0);">
                <input type="hidden" name="id" value="<?php echo $settings['id']; ?>">
                <div class="loading" id="subsc-loading"></div>  
                <div class="form-group">
                    <label class="col-md-4 control-label">Default Shipping Price:</label>  
                    <div class="col-md-8">
                        <div id="single_part_box">
                            <div class="form-horizontal">
                                
                                <div class="form-group">                                      
                                    <div class="col-sm-10">
                                        <div id="single_parent_box">
                                        <?php if (isset($default_shipping_cost) && !empty($default_shipping_cost)) { 
                                            
                                            $cnt_single = 0;
                                            foreach ($default_shipping_cost as $key => $value) {                                               
                                                $default_shipping = $value['default_shipping_cost'];
                                                $currency_id = $value['currency_id'];                                 
                                            ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-1 m-t-10">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="data[status][]" checked="checked" value="Y" /><i class="input-helper"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="data[currency_id][]" >
                                                                        <?php foreach ($currency as $key1 => $value1) { ?>
                                                                        <option value="<?php echo $currency_id;?>" <?php if ($currency_id == $value1['id']) {?>selected="selected"<?php }?>><?php echo $value1['code'];?>(<?php echo $value1['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[default_shipping_price][]" value="<?php echo $default_shipping; ?>" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <?php 
                                            $cnt_single++;
                                            }
                                        } else { ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-1 m-t-10">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="data[status][]" checked="checked" value="Y" /><i class="input-helper"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="data[currency_id][]">
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                            <option value="<?php echo $value['id']; ?>" <?php if ($studio->default_currency_id == $value['id']) { ?>selected="selected"<?php } ?>><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[default_shipping_price][]" value="" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        </div>
                                        
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <small class="help-block"><label id="data[default_shipping_price][]-error" class="error red" for="data[default_shipping_price][]" style="display: none;"></label></small>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- Add Specific price category-->
                                        <div class="row">
                                            <div class="col-md-12 text-left">
                                                <a href="javascript:void(0);" onclick="addMore('single', 0);" class="text-gray">
                                                    <em class="icon-plus blue"></em> Add more price
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                               
                    </div>              
                </div> 
                <div class="form-group">                            
                    <label class="col-md-4 control-label">Allow shipping to country:</label>
                    <div class="col-md-8">
                        <div class="fg-line">                                    
                            <select class="form-control input-sm" name="restrict_country[]" id="restrict_country" multiple="multiple">
                                <?php 
                                    foreach ($countrys as $value) {
                                        $selected = (in_array($value['code'],$restrict_country))?'selected':'';;
                                        echo '<option value='.$value['code'].' '.$selected.'>'.$value['country'].'</option>';
                                    }
                                ?>
                            </select>                                    
                        </div>
                    </div>              
                </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label">Minimum order for free shipping:</label>  
                    <div class="col-md-8">
                        <div id="multi_part_box">
                            <div class="form-horizontal">
                                
                                <div class="form-group">                                      
                                    <div class="col-sm-10">
                                        <div id="multi_parent_box">
                                        <?php if (isset($minimum_order_free_shipping) && !empty($minimum_order_free_shipping)) { 
                                            
                                            $cnt_single = 0;
                                            foreach ($minimum_order_free_shipping as $key => $value) {                                               
                                                $default_shipping = $value['minimum_order_free_shipping'];
                                                $currency_id = $value['currency_id'];                                 
                                            ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-1 m-t-10">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="mindata[status][]" checked="checked" value="Y" /><i class="input-helper"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="mindata[currency_id][]" >
                                                                        <?php foreach ($currency as $key1 => $value1) { ?>
                                                                        <option value="<?php echo $currency_id;?>" <?php if ($currency_id == $value1['id']) {?>selected="selected"<?php }?>><?php echo $value1['code'];?>(<?php echo $value1['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost multi-cost" name="mindata[minimum_order_free_shipping][]" value="<?php echo $default_shipping; ?>" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <?php 
                                            $cnt_single++;
                                            }
                                        } else { ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-1 m-t-10">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="mindata[status][]" checked="checked" value="Y" /><i class="input-helper"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="mindata[currency_id][]">
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                            <option value="<?php echo $value['id']; ?>" <?php if ($studio->default_currency_id == $value['id']) { ?>selected="selected"<?php } ?>><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 p-l-0 p-r-10">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost multi-cost" name="mindata[minimum_order_free_shipping][]" value="" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        </div>
                                        
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <small class="help-block"><label id="mindata[minimum_order_free_shipping][]-error" class="error red" for="mindata[minimum_order_free_shipping][]" style="display: none;"></label></small>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- Add Specific price category-->
                                        <div class="row">
                                            <div class="col-md-12 text-left">
                                                <a href="javascript:void(0);" onclick="addMore('multi', 0);" class="text-gray">
                                                    <em class="icon-plus blue"></em> Add more price
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                               
                    </div>              
                </div>                        
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" name="save" value="save" class="btn btn-primary btn-sm m-t-30" onclick="return validateSingleAdvForm();">Update</button>
                    </div>
                </div>
            </form>        
        </div> 
    </div>
    <div id="single-data" style="display: none;">
        <div class="row m-b-20">
            <!-- Checkbox-->
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-1 m-t-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="singlechk" name="data[status][]"  value="Y" /><i class="input-helper"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3 p-l-0 p-r-10">
                        <div class="fg-line">
                            <div class="select">
                                <select class="form-control input-sm currency" name="data[currency_id][]">
                                    <?php foreach ($currency as $key => $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-l-0 p-r-10">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm cost single-cost" name="data[default_shipping_price][]" value="" autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-sm-3 p-l-0 p-r-0">
                        <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                            <em class="icon-trash blue" ></em>&nbsp; Remove
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="multi-data" style="display: none;">
        <div class="row m-b-20">
            <!-- Checkbox-->
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-1 m-t-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="singlechk" name="mindata[status][]"  value="Y" /><i class="input-helper"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3 p-l-0 p-r-10">
                        <div class="fg-line">
                            <div class="select">
                                <select class="form-control input-sm currency" name="mindata[currency_id][]">
                                    <?php foreach ($currency as $key => $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-l-0 p-r-10">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm cost multi-cost" name="mindata[minimum_order_free_shipping][]" value="" autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-sm-3 p-l-0 p-r-0">
                        <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                            <em class="icon-trash blue" ></em>&nbsp; Remove
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-settings left-icon "></em>
        </div>
        <h4>Shipping Methods & Product Sizes</h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <form class="form-horizontal" method="post" role="form" id="method" name="method" action="<?php echo Yii::app()->baseUrl; ?>/store/saveMethod">                 
                <div class="form-group">
                    <label class="col-md-4 control-label">Shipping Method:</label>  
                    <div class="col-md-7">
                        <div class="fg-line">
                            <input type='text' placeholder="Enter method name" id="method" name="method" class="form-control input-sm checkInput" required>
                         </div>                                                                      
                    </div>              
                    <div class="col-sm-1">
                        <button type="submit" name="save" value="save" class="btn btn-primary btn-sm m-t-5 waves-effect">Add</button>                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>  
                    <div class="col-md-6">
                        <table class="table table-condensed">
                            <?php foreach($methods as $method) {?> 
                            <tr>
                                <td id="method<?php echo $method['id'];?>"><?php echo $method['method'];?></td>
                                <input type="hidden" id="inme<?php echo $method['id'];?>" value="<?php echo $method['method'];?>">
                                <td>                           
                                    <h5><a href="javascript:void();" onclick="updateMethod('<?php echo $method['id'];?>');"><em class="icon-pencil"></em></a><?php if($method['method_unique_name']!='standard'){ ?>&nbsp;&nbsp;<a href="javascript:void(0)" class="confirm" data-msg ="Are you sure to delete <?php echo $method['method'];?>?" onclick="confirmDelete('<?php echo $this->createUrl('store/deleteMethod/', array('id' => $method['id'])); ?>', 'Are you sure to delete <?php echo $method['method'];?>?');"><em class="icon-trash"></em></a><?php } ?></h5>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>                
            </form>
            <form class="form-horizontal" method="post" role="form" id="size" name="size" action="<?php echo Yii::app()->baseUrl; ?>/store/saveSize">                 
                <div class="form-group">
                    <label class="col-md-4 control-label">Product Size:</label>  
                    <div class="col-md-7">
                        <div class="fg-line">
                            <input type='text' placeholder="Enter size name" id="size" name="size" class="form-control input-sm checkInput" required>
                        </div>                                                                      
                    </div>              
                    <div class="col-sm-1">
                        <button type="submit" name="save" value="save" class="btn btn-primary btn-sm m-t-5 waves-effect">Add</button>                        
                    </div>              
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>  
                    <div class="col-md-6">
                        <table class="table mob-compress">
                            <?php foreach($sizes as $size) {?> 
                            <tr>
                                <td id="size<?php echo $size['id'];?>"><?php echo $size['size'];?></td>
                                <input type="hidden" id="insi<?php echo $size['id'];?>" value="<?php echo $size['size'];?>">
                                <td>                           
                                    <h5><a href="javascript:void();" onclick="updateSize('<?php echo $size['id'];?>');"><em class="icon-pencil"></em></a><?php if($size['size_unique_name']!='small'){ ?>&nbsp;&nbsp;<a href="javascript:void(0)" class="confirm" data-msg ="Are you sure to delete <?php echo $size['size'];?>?" onclick="confirmDelete('<?php echo $this->createUrl('store/deleteSize/', array('id' => $size['id'])); ?>', 'Are you sure to delete <?php echo $size['size'];?>?');"><em class="icon-trash"></em></a><?php } ?></h5>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>      
            </form>        
        </div> 
    </div>

    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-settings left-icon "></em>
        </div>
        <h4>Shipping Costs</h4>
    </div>
    <hr>
    <div class="row m-b-10">
    <div class="col-xs-12">
        <button class="btn btn-primary btn-default m-t-10" onclick="openShoppingPop('0','Add');">Add Shipping Cost</button>
    </div> 
</div>
    <div class="row">
        <div class="col-md-11 m-b-10">
            <form action="<?php echo $this->createUrl('store/ShippingCost'); ?>" name="searchForm" id="search_content" method="post">
               <div class="row">
                    <div class="col-sm-4">
                         <div class="input-group form-group">
                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <div class="select">
                                    <select id="country" name="country" class="form-control input-sm">
                                       <option value="">Country</option>
                                       <?php foreach($countrys as $country) {?> 
                                       <option value="<?php echo $country['code'];?>"><?php echo $country['country'];?></option>
                                       <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group input-group">
                             <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <div class="select">
                                    <select id="method" name="method" class="form-control input-sm">
                                       <option value="">Method</option>
                                           <?php foreach($methods as $method) {?> 
                                           <option value="<?php echo $method['method_unique_name'];?>" <?php echo ($method['method_unique_name']==$data['method'])?'selected':'';?> ><?php echo $method['method'];?></option>
                                           <?php } ?>
                                    </select>
                                </div>
                             </div>
                        </div>
                    </div>					
                    <div class="col-sm-2">
                        <div class="form-group input-group">
                             <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <div class="select">
                                <select id="size" name="size" class="form-control input-sm">
                                   <option value="">Size</option>
                                       <?php foreach($sizes as $size) {?> 
                                       <option value="<?php echo $size['size_unique_name'];?>" <?php echo ($size['size_unique_name']==$data['size'])?'selected':'';?> ><?php echo $size['size'];?></option>
                                       <?php } ?>
                                </select>
                                </div>
                             </div>
                        </div>
                    </div>                    
                    <div class="col-sm-1">                        
                        <button type="submit" name="searchForm[search_text]" value="save" class="btn btn-primary btn-sm m-t-5 waves-effect">Search</button>                       
                    </div>
               </div>
            </form>
        </div>
    </div>
</div>

<table class="table mob-compress" id="movie_list_tbl">
    <thead>
        <tr>
            <th>#</th>
            <th>Country</th>
            <th>Shipping Method</th>
            <th>Size</th>
            <th>Shipping Cost</th>
            <th data-hide="phone" class="width">Action</th>
        </tr>
    </thead>    
    <tbody>
        <?php
        $cnt = 1;
        if ($costs) {
            foreach ($costs AS $key => $cost) {
        ?>
                <tr>
                    <td><?php echo $cnt;?></td>
                    <td><?php echo $cost['country'];?></td>
                    <td><?php echo ucwords($cost['method']);?></td>
                    <td><?php echo ucwords($cost['size']);?></td>
                    <td><?php echo Yii::app()->common->formatPrice($cost['shipping_cost'], $cost['currency']);?></td>
                    <td>                           
                        <h5><a href="javascript:void();" onclick="openShoppingPop('<?php echo $cost['id'];?>','Edit');"><em class="icon-pencil"></em>&nbsp;Edit</a>&nbsp;&nbsp;
                        <a href="javascript:void(0)" class="confirm" data-msg ="Are you sure?" onclick="confirmDelete('<?php echo $this->createUrl('store/deleteShippingCost/', array('id' => $cost['id'])); ?>', '<?php echo ""; ?>');"><em class="icon-trash"></em>&nbsp;Delete</a></h5>
                    </td>
                </tr>	
        <?php
        $cnt++;
    }
} else {
    ?>
            <tr>
                <td colspan="5">
                    <p class="text-red">Oops! Shipping cost not added yet.</p>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div class="pull-right">
<?php
if ($cost) {
    $opts = array('class' => 'pagination m-t-0');
    $this->widget('CLinkPager', array(
        'currentPage' => $pages->getCurrentPage(),
        'itemCount' => $item_count,
        'pageSize' => $page_size,
        'maxButtonCount' => 6,
        "htmlOptions" => $opts,
        'selectedPageCssClass' => 'active',
        'nextPageLabel' => '&raquo;',
        'prevPageLabel' => '&laquo;',
        'lastPageLabel' => '',
        'firstPageLabel' => '',
        'header' => '',
    ));
}
?>
</div>

<div class="h-40"></div>
<div id="shpcst" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Shipping Cost</h4>
            </div>            
            <div id="free_form_area"></div>
        </div>									
    </div>
</div> 

<script type="text/javascript">
    
    function confirmDelete(location, msg) {
        swal({
            title: "Delete Record?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            //swal("Deleted!", "Your content has been deleted.", "success");
            window.location.replace(location);
        });

    }
    function openShoppingPop(scid,tp) {
        var url = "<?php echo Yii::app()->baseUrl; ?>/store/showShippingPop";
        $.post(url, {'scid': scid}, function (res) {
            $('#free_form_area').html(res);
        });
        $("#myModalLabel").html(tp+' Shipping Cost');
        $("#shpcst").modal('show');
    }    
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#restrict_country').multiselect({
            //enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 300,
            dropUp: true
        });
    });
    Array.prototype.getUnique = function(){
       var u = {}, a = [];
       for(var i = 0, l = this.length; i < l; ++i){
          if(u.hasOwnProperty(this[i])) {
             continue;
          }
          a.push(this[i]);
          u[this[i]] = 1;
       }
       return a;
    }
    
    function addMore(type, flag) {
        var isTrue = 0;
        $("#"+type+"_part_box").find("."+type+"-cost").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        
        if (parseInt(flag)) {
            isTrue = 1;
        }
        if (parseInt(isTrue)) {  
            var str ='';
            if (type === 'single') {
                str = $("#single-data").html();
            } else if (type === 'multi') {
                str = $("#multi-data").html();
            }
            
            $("#"+type+"_parent_box").append(str);
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').on("contextmenu",function(event) {
                return false;
            });
        }
    }
    
    function removeBox(obj) {
        $(obj).parent().parent().remove();
    }
       
    function validateSingleAdvForm() {    
        var validate = $("#account").validate({
            rules: {                
                'data[default_shipping_price][]': {
                    required: true,
                    uniquecurrency: true
                },
                "restrict_country[]": {required: true},
                'mindata[minimum_order_free_shipping][]': {
                    //required: true,
                    minuniquecurrency: true
                },
            },
            messages: {
                'data[default_shipping_price][]': {
                    required: 'Please enter fee',                    
                    uniquecurrency: 'Currency should be unique'
                },
                "restrict_country[]": 'Please select at least one country.',
                'mindata[minimum_order_free_shipping][]': {
                    //required: 'Please enter fee',                    
                    minuniquecurrency: 'Currency should be unique'
                },
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();
        
        if (x) {
                var url = "<?php echo Yii::app()->baseUrl; ?>/store/saveSettings";
                document.account.action = url;
                document.account.submit();
        }
    }
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    jQuery.validator.addMethod("uniquecurrency", function (value, element) {
        var currency = new Array();

        $("#single_parent_box").find(".singlechk").each(function(){
            if ($(this).is(':checked')) {
                currency.push($(this).parent().parent().parent().next('div').find('select').val());
            }
        });

        var cur = currency.getUnique();
        if (cur.length === currency.length) {
            return true;
        } else {
            return false;
        }
    }, "Currency should be unique");
    
    jQuery.validator.addMethod("minuniquecurrency", function (value, element) {
        var currency = new Array();

        $("#multi_parent_box").find(".singlechk").each(function(){
            if ($(this).is(':checked')) {
                currency.push($(this).parent().parent().parent().next('div').find('select').val());
            }
        });

        var cur = currency.getUnique();
        if (cur.length === currency.length) {
            return true;
        } else {
            return false;
        }
    }, "Currency should be unique");
    
    function updateMethod(id,method){
        var method = $('#inme'+id).val();
        $('#method'+id).html('<input type="text" value="'+method+'" id="target'+id+'" class="editable-input" required>&nbsp;<em class="icon-check" style="cursor:pointer" title="Save" onclick="saveMethod('+id+')"></em>&nbsp;&nbsp;&nbsp;<em class="icon-close" style="cursor:pointer" title="Cancel" onclick="removeeditMethod('+id+');"></em>');        
    }
    function removeeditMethod(id){
        var methodval = $('#inme'+id).val();
        $('#method'+id).html(methodval);
    }
    function saveMethod(id){
        var methodval = $('#target'+id).val();
        if(methodval){
            var url = HTTP_ROOT + "/Store/editMethod";    
            $.post(url, {'method': methodval,'id':id},function (data) {
                if (data.isSuccess) {
                   window.location.reload();
                }else{
                    alert(data.message);
                }
            }, 'json');
        }else{
            alert('Please enter method name.');
        }
    }    

    function updateSize(id,size){
        var size = $('#insi'+id).val();
        $('#size'+id).html('<input type="text" value="'+size+'" id="targetsz'+id+'" class="editable-input" required>&nbsp;<em class="icon-check" style="cursor:pointer" title="Save" onclick="saveSize('+id+')"></em>&nbsp;&nbsp;&nbsp;<em class="icon-close" style="cursor:pointer" title="Cancel" onclick="removeeditSize('+id+');"></em>');        
    }
    function removeeditSize(id){
        var sizeval = $('#insi'+id).val();
        $('#size'+id).html(sizeval);
    }
    function saveSize(id){
        var sizeval = $('#targetsz'+id).val();
        if(sizeval){
            var url = HTTP_ROOT + "/Store/editSize";    
            $.post(url, {'size': sizeval,'id':id},function (data) {
                if (data.isSuccess) {
                   window.location.reload();
                }else{
                    alert(data.message);
                }
            }, 'json');
        }else{
            alert('Please enter size name.');
        }
    }    
</script>
