<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Listing All Blog Post Comments
//Added by : RKS
?>
<div class="row m-t-10">
    <div class="col-xs-12">
        <a href="#" class="btn btn-primary m-b-40" onclick="javascript: history.back(-1);">&laquo; Back</a>
    </div>
</div>
<div class="row m-b-40">
    <div class="col-md-12">
        <div class="Block">
            <div class="Block-Header">
                            <div class="icon-OuterArea--rectangular">
                                <em class="fa fa-comment icon left-icon "></em>
                            </div>
                            <h4>Blog Post Comments</h4>
                        </div>
                        <hr>
                        <?php
                            if(count($data) > 0)
                            {
                                ?>
                            <table class="table" id="example1" role="grid" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">                                        
                                        <th class="col-sm-2">Full Name</th>                                        
                                        <th class="col-sm-2">Email</th>
                                        <th class="col-sm-2" data-hide="phone">Created Date</th>
                                        <th class="col-sm-4" data-hide="phone">Comment</th>
                                        <th data-hide="phone" class="width">Action</th>                                     
                                    </tr>
                                </thead>
                                <tbody>                            
                                    <?php
                                        $c = 0;
                                        foreach($data as $comment)
                                        {
                                            $c++;
                                            $cls = ($c%2 == 1)?'odd':'even';
                                        ?>
                                        <tr role="row" class="<?php echo $cls;?>">
                                            <td><?php echo $comment->fullname;?></td>
                                            <td><?php echo $comment->email;?></td>
                                            <td><?php echo Yii::app()->common->phpDate($comment->created_date);?></td>
                                            <td><?php echo html_entity_decode($comment->comment);?></td>
                                            <td>
                                                <h5><a href="<?php echo $this->createAbsoluteUrl('admin/blogcomment/', array('post_id' => $comment->post_id, 'comment_id' => $comment->id, 'option' => 'edit')); ?>" class="editbtn" data-toggle="tooltip" data-original-title="Edit" data-placement="top"><em class="icon-pencil" ></em>&nbsp;Edit</a></h5>
                                                <h5><a href="#" data-id="<?php echo $comment->id;?>" data-msg = 'Are you sure, you want to delete this comment?' class="confirm"  data-toggle="tooltip" data-original-title="Delete" data-placement="top"><em class="icon-trash"></em>&nbsp;Remove</a></h5>
                                                <h5><a href="#" data-id="<?php echo $comment->id;?>" data-msg = 'This will change the publish status of comment.</b> Are you sure?' class="statusupdate" data-toggle="tooltip" data-original-title="Status Change" data-placement="top"><em class=" <?php echo ($comment->status == 1)?'icon-check':'icon-ban';?>" ></em>&nbsp;Change Status</a></h5>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                </tbody>
                            </table>                                        
                                <?php
                            }else{
                            ?>
                            <br />
                            <p class="lead">No comments for this post.</p>
                            <?php }?>
                        
               
                <div class="pull-right">
                <?php
                if($data){
                    $opts = array('class' => 'pagination');
                    $this->widget('CLinkPager', array(
                        'currentPage'=>$pages->getCurrentPage(),
                        'itemCount'=>$item_count,
                        'pageSize'=>$page_size,
                        'maxButtonCount'=>6,
                        'nextPageLabel'=>'Next &gt;',
                        "htmlOptions" => $opts,
                        'header'=>'',
                        'selectedPageCssClass' => 'active',
                        'lastPageLabel' => 'Last',
                        'firstPageLabel' => 'First',
                    ));
                }
                ?>                            
                </div>
                
                
            
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/bootbox.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true);?>/themes/admin/js/plugins/datatables/dataTables.bootstrap.css">
<script type="text/javascript">
	var bootboxConfirm = 0;
	$(function () {
            $("a.confirm").bind('click',function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var msg = $(this).attr('data-msg');
                confirmOperation(id, msg, 'Confirm Delete?', 'delete');
            });
            
            $("a.statusupdate").bind('click',function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var msg = $(this).attr('data-msg');
                confirmOperation(id, msg, 'Confirm Status Change?', 'status');
            });            
		
	});
        
        
	function confirmOperation(id, msg, tlt, action){
            swal({
                    title: tlt,
                    text: msg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    html:true
                }, function() {
                    if(action == "delete"){
                        var message = "Your Comment has been deleted.";
                        var actions = "Deleted";
                    }else{
                         var message = "Your Comment Status has been changed.";
                         var actions = "Changed";
                    }
                    swal(actions , message , "success");
                    $.ajax({
                        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/manageblogcomment",
                        data: {'id': id, 'option' : action},
                        dataType: 'json',
                        success: function (result) {
                            location.reload(); 
                        }
                    });   
                });
            
         
	}
        </script>