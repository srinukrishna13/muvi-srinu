<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MuviPayment extends AppComponent {
     public function authenticateToCard($data = array()) {
         if (isset($data) && !empty($data)) {
            if (@IS_DEMO_PAYMENT == 1) {
               // print '@Suraja You are here';exit;
                $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
            } else {
                if (IS_LIVE_FIRSTDATA) {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                } else {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                }
            }

            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardNumber($data['card_number'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            //->setCreditCardType($data['type'])
            //->setReferenceNumber("Reactivate Charge");
            if ($data['zip']) {
                $firstData->setCreditCardZipCode($data['zip']);
            }

            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }

            if ($data['address']) {
                $firstData->setCreditCardAddress($data['address']);
            }

            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    } 
 
    public function chargeToCard($data = array()) {
        if (isset($data) && !empty($data)) {
            if (@IS_DEMO_PAYMENT == 1) {
                $firstData = new FirstData(DEMO_FIRSTDATA_API_LOGIN, DEMO_FIRSTDATA_API_KEY, true);
            } else {
                if (IS_LIVE_FIRSTDATA) {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
                } else {
                    $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
                }
            }

            $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
            $firstData->setTransArmorToken($data['token'])
                    ->setCreditCardType($data['card_type'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    }
   //charge to existing card after failed 
    public function chargeDueOnPartialOrFailedTransaction($payment_status,$user, $card_info, $data = array()) {
        $res = array();
        if (isset($data) && !empty($data)) {
            $user_id = $user->id;
           $data['amount'] = $amount = $payment_status[0]['partial_failed_due'];
            if (abs($amount) >= 0.01) {
                $firstData = $this->chargeToCard($data);
                $req['name'] = $user->name;
                $req['email'] = $user->email;
                $req['card_last_fourdigit'] = $data['card_last_fourdigit'];
                if ($firstData->isError()) {
                    $res['isSuccess'] = 0;
                    $res['Code'] = $firstData->getErrorCode();
                    $res['Message'] = 'Invalid Card details entered. Please check again';

                    $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                    //Keeping card error logs
                    $RceModel = New ResellerCardErrors;
                    $charge_card_error = array('reseller_id' => $user_id,'response_text' => serialize($firstData),'transaction_type' => 'Transaction due on subscription renew','created' => new CDbExpression("NOW()"));
                    $RceModel->set_reseller_error_data($charge_card_error);                  
                    //Send an email to developer and support
                    Yii::app()->email->resellererrorMessageMailToSales($req);
                } else {
                    if ($firstData->getBankResponseType() == 'S') {
                        $card_id = $card_info[0]->id;
                        //Update all card to inactive mode to make primary for current card
                        $card_info_model = new ResellerCardInfos;
                        $card_info_model->cancel_card_is_primary($user_id);
                        //make primary for current card
                        $card_info_model->make_card_is_primary($card_id);

                        if (isset($payment_status[0]['$payment_status']) && $payment_status[0]['$payment_status'] == 1) {
                            $payment_status_type = 'Partial';
                        } else if (isset($payment_status[0]['$payment_status']) && $payment_status[0]['$payment_status'] == 2) {
                            $payment_status_type = 'Failed';
                        }
                        $invoice_detail = $payment_status_type . ' transaction due';
                        
                        //Find out the billing detail
                        $biModel = ResellerBillingInfos::model()->find(array('condition' => 'is_paid != 2 AND transaction_type =1 AND portal_user_id = '.$user_id, 'order'=>'created_date DESC'));
                        $billing_info_id = $biModel->id;
                        $billing_detail = $biModel->detail;
                        
                        //Insert new transaction record in transaction infos;
                        //Set datas for inserting new record in transaction info table whether it returns success or error
                        $paid_amount = $firstData->getAmount();
                        $tiModel = New ResellerTransactions;
                        $transaction_arr = array(
                                            'card_info_id' => $card_id,
                                            'portal_user_id' => $user_id,
                                            'billing_info_id' => $billing_info_id,
                                            'billing_amount' => $amount,
                                            'invoice_detail' => $invoice_detail,
                                            'is_success' => 1,
                                            'invoice_id' => $firstData->getTransactionTag(),
                                            'order_num' => $firstData->getAuthNumber(),
                                            'paid_amount' => $firstData->getAmount(),
                                            'response_text' => serialize($firstData),
                                            'transaction_type' => $payment_status_type . ' transaction due on subscription renew',
                                            'created_date' => new CDbExpression("NOW()")
                                             );
                          $reseller_transaction_id = $tiModel->setTransactionData($transaction_arr);
                          $reseller_subscription_arr = array();
                          $biModel = New ResellerBillingInfos;
                        //Check billing amount and paid amount is same or not, otherwise charge again.
                        if (abs(($amount - $paid_amount) / $paid_amount) < 0.00001) {
                                    $billing_date = gmdate('Y-m-d', strtotime($payment_status[0]['start_date']));
                            
                            if ((isset($payment_status[0]['payment_status']) && $payment_status[0]['payment_status'] == 2) || (strtotime(gmdate('Y-m-d')) > strtotime($billing_date))){
 
                                $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
                                    $start_date = Date('Y-m-d H:i:s', strtotime($studio->start_date."+1 Months"));
                                    $end_date = Date('Y-m-d H:i:s', strtotime($studio->end_date."+1 Months -1 Days"));
                                    $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months"))));
                                    $reseller_subscription_arr['start_date'] = $start_date;
                                    $reseller_subscription_arr['end_date'] = $end_date;
          
                            }
                            $is_paid = 2;

                            $reseller_subscription_arr['payment_status'] = 0;
                            $reseller_subscription_arr['partial_failed_date'] = '';
                            $reseller_subscription_arr['partial_failed_due'] = 0;
                        } else {
                            $is_paid = 1;
                            $reseller_subscription_arr['partial_failed_due'] = $paid_amount;
                        }
                       //Update sbscription in subscription table if tranasction is going to success or not.
                        $reseller_subscription_model = new ResellerSubscription;
                        foreach($reseller_subscription_arr as $key => $val){
                            $reseller_subscription_model->updateByPk($payment_status[0]['id'], array($key => $val));
                        }
                        $tiModel->updateByPk($reseller_transaction_id,array('subscription_id'=>$payment_status[0]['id']));
                        $invoice['html'] = $billing_detail;
                        $invoice['isEmail'] = 1;

                        $pdf = Yii::app()->pdf->generatePdf($invoice);
                        
                        $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => $is_paid));

                        $req['invoice_title'] = $invoice_detail;
                        $req['invoice_amount'] = $paid_amount;

                        Yii::app()->email->raiseInvoicePaidMailToReseller($req, $pdf);
                        Yii::app()->email->raiseResellerInvoicePaidMailToSales($req);

                        $res['isSuccess'] = 1;
                        $res['TransactionRecord'] = $firstData->getTransactionRecord();
                        $res['Code'] = $firstData->getBankResponseCode();
                    }
                }
            }
        }

        return $res;
    }   

}
