<?php

class PGMultiCurrency extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_multi_currency';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    function addContent($data,$ins_id){
        foreach ($data['multi_sale_price'] as $key => $val) {
            if ($val != '') {
                $this->product_id = $ins_id;
                $this->studio_id = Yii::app()->user->studio_id;
                $this->price = $val;
                $this->currency_id = $data['multi_currency_id'][$key];
                $this->isNewRecord = TRUE;
                $this->primaryKey = NULL;
                $this->save();
            }
        }
    }
}