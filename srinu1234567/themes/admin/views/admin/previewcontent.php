<?php $imported_values = Yii::app()->session['filedata'];
        $emptyrows = Yii::app()->session['emptyrows'];
        
        $allrecords=count($imported_values);
        if($allrecords>1){
  ?>
<div class="row m-t-40">
    <div class="col-md-12">
        <?php
        
        if ($emptyrows > 0) {
            ?>
            <div class="alert alert-warning alert-dismissible" role="alert"><?php echo "You have " . $emptyrows . " rows missing mandatory values"; ?></div>
<?php } ?>
        <div class="table-responsive table-responsive-tabletAbove" style="height:300px;overflow-y: auto;">
            <table class="table table-bordered" id="import_list_tbl">
                <thead>
                    <tr>
						<?php foreach ($imported_values[1] as $keys => $value) {?>
							<th class="width"><?php echo $value; ?></th>
						<?php }?>
                    </tr>
                </thead>    
                <tbody id="import_body">

                    <?php
                    foreach ($imported_values as $key => $val) {
                        if ($key > 1) {
                            ?>
                            <tr>
                                <?php foreach ($val as $key1 => $value1) {?>
                                <td>
									<span><?php echo $value1; ?></span>                           
                                </td> 
								<?php }?>
                            </tr>     
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

    </div>

</div>    

<div class="row">
    <div class="col-md-12">
        <button id="upload_all" type="button" class="btn btn-primary" onclick="import_all()">Upload</button>
        <a id="back" type="button" class="btn btn-default" href="<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/managecontent">Back</a>
    </div>
</div>
 <?php } else { ?>
 <div class="row m-t-40">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible" role="alert">The file does not contain any data.</div>
    </div>
 </div>

<div class="row m-t-40">
    <div class="col-md-12">
        <a id="back_to" type="button" class="btn btn-default" href="<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/managecontent">Back</a>
    </div>
</div>
 <?php } ?>
<script type="text/javascript">
    function import_all()
    {
        $('#upload_all').attr('disabled', 'disabled');
        var url = "<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/importContents";

        $.ajax({
            url: url,
            type: "POST",
            data: {},
            contentType: false,
            cache: false,
            processData: false,
            success: function (res)
            {
                if ($.trim(res) == "error") {
                    
                    swal("Wrong file type");
                }
                else if ($.trim(res) == "headerissue")
                {
                    swal("Please check the file header");
                }
                else if ($.trim(res) == "countexceed")
                {
                    swal("Maximum limit 2000 content");
                }
                else if($.trim(res) =="wrongmeta")
                {
                  swal("Invalid metadata type. Supported metadata types are 1,2,3,4");   
                }
                else if($.trim(res) =="wrongcont")
                {
                  swal("Invalid content type");   
                }
                 else if($.trim(res) =="parenterr")
                {
                  swal("Parent content missing for multipart child content");   
                }
                else
                {

                    $("#upload_all").css("display", "none");
                    $("#import_preview").css("display", "none");
                 window.location.href = "<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/managecontent";
                }
            }
        });

    }

</script>