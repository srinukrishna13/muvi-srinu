<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"><?php echo $params['logo']; ?></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	Hi,
                            </p>
				<p style="display:block;margin:0 0 17px">
                            	<span mc:edit="name"><?php echo $params['name']; ?></span> has updated his subscription. Below are the details of the user
                            </p>              
                            <p style="display:block;margin:0 0 17px">
                                Name: <strong><span mc:edit="name"><?php echo $params['name']; ?></span></strong><br/>
				Package: <strong><span mc:edit="package_name"><?php echo $params['package_name']; ?></span></strong><br/>
                                Applications: <strong><span mc:edit="applications"><?php echo $params['applications']; ?></span></strong><br/>
                                Plan: <strong><span mc:edit="plan"><?php echo $params['plan']; ?></span></strong><br/>
				<span mc:edit="amount_charged"><?php echo $params['amount_charged']; ?></span>
                            </p>
 
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%;font-family:helvetica,Arial;width:70%;">
                            <tbody>
                                <tr>
                                    <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                        Regards,
                                        <p style="font-size:14px;margin:2px 0px">
                                            Team Muvi
                                        </p>
                                    </td>
                             		<td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"><?php echo $params['fb_link']; ?></span>&nbsp;<span mc:edit="gplus_link"><?php echo $params['gplus_link']; ?></span>&nbsp;<span mc:edit="twitter_link"><?php echo $params['twitter_link']; ?></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>