<?php
$studio = $this->studio;
$posterImg = "https://d1yjifjuhwl7lc.cloudfront.net/public/no-image-h-thumb.png";
if (isset(Yii::app()->user->created_at)) {
    $expairy_date = strtotime(Yii::app()->user->created_at) + 13 * 24 * 60 * 60;
} else {
    $expairy_date = time() + 13 * 24 * 60 * 60;
}
$v = RELEASE;
?>
<!--link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/normalize.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css"-->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/modernizr-2.6.2.min.js"></script>

<div id="myvisyncLoader"  style="display: none; position: absolute; z-index: 99999999999999999999999999999;">
    <header   class="entry-header">
        <h1 class="entry-title">Audios are now syncing to your audio gallery. Do not close this window</h1>
    </header>

    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
</div>
<div class="modal fade" id="audioSync" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure to sync the audios with your server?</h4>
            </div>
            <div class="modal-body">
                <p>The audios which are in <b>MuviSync</b> folder will be added to audio gallery and deleted from the folder.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" onclick="audioSync()">Yes</button>
                <button data-dismiss="modal" class="btn btn-primary" type="button">Cancel</button>
            </div>  
        </div>
    </div>
</div>

<div class="modal fade" id="audiogallery" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Sync audio from your ftp server?</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn btn-default-with-bg" type="button" onclick="UploadAudioS3()">Yes</button>
                <button data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
            </div>  
        </div>
    </div>
</div>
<div class="row m-b-40">
    <div class="col-xs-12">
        <a onclick="show_upload()" data-toggle="modal" data-target="#image_upload">
            <button type="submit" class="btn btn-primary btn-default m-t-10">
                Upload audio
            </button>
        </a>
        <?php
        if (isset($getStudioConfig['config_value']) && $getStudioConfig['config_value'] == 1) {
            ?>
            <a onclick="UploadAudioS3" data-toggle="modal" data-target="#audiogallery" data-toggle="tooltip">
                <button type="submit" class="btn btn-primary btn-default m-t-10" >
                    Sync to Audio Library
                </button></a>
<?php } ?>
<?php if (Yii::app()->user->studio_s3bucket_id != 0) { ?>
            <!--<a class="btn bg-olive btn-flat margin btn_add_movie" data-toggle="modal" data-target="#audioSync" data-toggle="tooltip" title="Maximux audio file size should be less than 5GB."><em class="fa fa-refresh fa-spin"></em> <button type="submit" class="btn btn-primary btn-default m-t-10">Sync with On-premise Server </button></a>-->
        <?php } ?>
    </div>
</div>
<div class="">
    <div>
        <div class="col-md-12"> 
            <div class="block">
                <div id="show_upload_div" style="display:none;" class="m-b-20">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="form-horizontal">	
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label for="uploadAudio" class="control-label">Upload Method &nbsp;</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="hidden" value="?" name="utf8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select name="upload_type" class="filter_dropdown form-control" id="filetype">
                                                    <option value="browes">From Computer</option>
<!--                                                    <option value="server">Server to Server Transfer</option>
                                                    <option value="dropbox">From Dropbox</option>-->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix m-b-20"></div>
                                    <div class="savefile" id="browes_div">
                                        <div class="col-md-2">&nbsp;</div>
                                        <div class="col-md-4">
                                            <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm"  onclick="ShowClickbrowse();">
                                            <input type="file" class="" id="Filedata" name="Filedata" onchange="checkfileSize()" style="display:none;">
                                            <span>&nbsp;&nbsp;No file chosen</span>
                                            <span id="file_error" class="error" for="Filedata" style="display: block;"></span>
                                            <input type="hidden" id="audio_exist_flag" name="audio_exist_flag" >
                                        </div>
                                    </div>
                                    <div class="clear-fix"></div>
                                    <div class="savefile" id="server_div" style="display: none;">
                                        <div class="col-md-2">&nbsp;</div>
                                        <div class="col-md-4">
                                            <div class="fg-line">
                                                <input type="url" placeholder="Path to the file on your server" id="server_url" name="server_url" class="form-control input-sm">
                                            </div>
                                            <div class="has-error">
                                                <span class="help-block" id="error"></span>
                                            </div>
                                            <div class=" has-error">
                                                <span class="help-block" id="server_url_error"></span>
                                            </div>
                                            <div style="width: 48%;float: left;">
                                                <input type="text" name="username"  id="ftpusername" class="form-control input-sm" placeholder='Username if any'>
                                                <label id="ftpusername-error" class="error" for="ftpusername" style="display: inline-block;"></label>
                                            </div>
                                            <div style="width: 48%;float: right;">
                                                <input type="password" name="password" id="ftppassword" class="form-control input-sm" placeholder='Password if any'>
                                                <label id="ftppassword-error" class="error" for="ftppassword"></label>
                                            </div>
                                            <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('');">Submit</button>
                                        </div>
                                    </div>
                                    <div class="savefile" id="dropbox_div" style="display: none;">
                                        <div class="col-md-2">&nbsp;</div>
                                        <div class="col-md-4">
                                            <div class="fg-line">
                                                <input type="text" name="dropbox" id="dropbox" class="form-control" placeholder="Path to the file on dropbox">
                                            </div>
                                            <span class="error red help-block" id="dropbox_url_error"></span>
                                            <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('dropbox_url_error');">Submit</button>
                                        </div>
                                    </div>
                                    <div class="clear-fix"></div> 
                                </div>
<!--                                <div class="form-group m-t-20">
                                    <div class="col-md-12">
                                        <div class="red">Muvi recommends S3 Sync and FTP for faster upload</div>
                                        <ul style="padding-left: 12px;">
                                            <li>
                                                <span style="font-weight: bold;">S3 Sync:</span>
                                                Install a tool we provide in your server. Audios in the server will be automatically synced with Muvi's Audio Library
                                            </li>
                                            <li>
                                                <span style="font-weight: bold;">FTP:</span>
                                                Use a traditional FTP client to upload to Muvi's Audio Library
                                            </li>
                                        </ul>
                                        See <a target="_blank" href="https://www.muvi.com/help/uploading-audios#Muvi_Recommended">help article</a> for more details. Please add a <a href="<?php echo Yii::app()->getbaseUrl(true) . "/ticket/ticketList"; ?>">support ticket</a> to use one of the above tools.
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <div class="loaderDiv" style="display: none;">
                            <div class="preloader pls-blue">
                                <svg viewBox="25 25 50 50" class="pl-circular">
                                <circle r="20" cy="50" cx="50" class="plc-path"/>
                                </svg>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>  
    </div> 
    <div class="row">
        <div class="col-sm-3 p-r-0">
            <div class="form-group input-group">
                <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                <div class="fg-line">
                    <input class="search form-control input-sm" placeholder="Search"/>
                </div>
            </div>
        </div>
        <div class="col-sm-3 p-r-0">
            <div class="form-group ">

                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm" name="audio_duration" id="audio_duration" onchange="view_filtered_list();">
                            <option value="0">Audio Duration</option>
                            <option  value="1">Less than 5 minutes</option>
                            <option  value="2">Less than 30 minutes</option>
                            <option  value="3">Less than 120 minutes</option>
                            <option  value="4">More than 120 minutes</option>                          
                        </select>
                    </div>
                </div>
            </div>
        </div>  
        <div class="col-sm-3 p-r-0">
            <div class="form-group">

                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm" name="file_size" id="file_size" onchange="view_filtered_list();">
                            <option value="0">File Size</option>
                            <option value="1">Less than 1GB</option>
                            <option value="2">Less than 10GB</option>
                            <option value="3">More than 10GB</option>
                        </select>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="col-sm-3 p-r-1">
            <div class="form-group">

                <div class="fg-line">
                    <div class="select" >
                        <select class="form-control input-sm" name="is_encoded" id="is_encoded" onchange="view_filtered_list();">
                            <option value="0">Mapped to Content</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>

                        </select>
                    </div>
                </div>
            </div>
        </div>  
        <div class="col-sm-3 p-r-0">
            <div class="form-group">

                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm" name="uploaded_in" id="uploaded_in" onchange="view_filtered_list();">
                            <option value="0">Uploaded In</option>
                            <option value="1">This Week</option>
                            <option value="2">This Month</option>
                            <option value="3">This Year</option>
                            <option value="4">Before This Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>  

        <div class="col-sm-2 p-r-0">
            <div class="form-group">

                <div class="fg-line">
                    <a href="javascript:void(0);" class="btn btn-default" onclick="reset_filter()"> Reset Filter</a>
                </div>
            </div>
        </div>  

        <div class="col-sm-1 p-r-0">
            <div class="form-group"> 
                <div class="fg-line">
                    <button class="btn btn-danger" onclick="delete_selected()">Delete Selected</button> 
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="page_size" value="<?php echo $page_size ?>" />
    <div  id="audio_list_tbl">

    </div>
    <!--<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>-->
</div>
<!-- Progress Bar popu -->
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
    <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
        <div style="float:left;font-weight:bold;">File Upload Status</div>
        <div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
    </div>
    <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<!-- Progress Bar popu end -->  

<!-- Upload subtitle Modal-->
<div id="addsubtitle_popup" class="modal fade in" role="dialog" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" action="javascript:void(0)" name="saveSubtitle" id="saveSubtitle" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Upload Subtitle for Audio - <span id="audioNameSub"></span></h4>
                </div>
                <div class="modal-body">
                    <div id="addsubtitle_body"></div>
                </div>

                <div class="modal-footer">
                    <button id="sub-btn" class="btn btn-primary"  onclick="return validateSubtitle();">Submit</button>
                    <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery.bootpag.min.js"></script>
<style type="text/css">
    .loaderDiv{position: absolute;left: 30%;top:10%;display: none;}
</style>
<script>
    $(function () {

        var searchText = $.trim($(".search").val());
        getaudioDetails(searchText);
    });

    $(document.body).on('keypress', '.search', function (event) {
        var searchText = $.trim($(".search").val());
        var c = String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEsearchAudioPagenter = (event.keyCode == 13);
        var isEnter = (event.keyCode == 13);
        if ((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)) {
            getaudioDetails(searchText);
        }
    });
    function getaudioDetails(searchText)
    {
        $.post('/management/searchAudioPage', {'search_value': searchText}, function (res) {
            $('#audio_list_tbl').html(res);
            var mydata = $(res).find('data-count');
            var mydata1 = $("#data-count1").val();
            var regex = /\d+/g;
            var count = mydata.prevObject[0].outerHTML.match(regex);
            var page_size = $('#page_size').val();
            var total = parseInt(count) / parseInt(page_size);
            var maxVisible = total / 2;
            console.log(count);
            console.log(page_size);
            console.log(maxVisible);
            if (maxVisible >= 10) {
                maxVisible = 10;
            } else {
                maxVisible = total;
            }

            if ($('.page-selection-div').length) {
                $('.page-selection-div').remove();
            }
            if (parseInt(count) < parseInt(page_size) || parseInt(mydata1) < 20) {
                $('#page-selection').parent().hide();
            } else {
                if ($('.page-selection-div').length) {
                    $('#page-selection').parent().show();
                } else {
                    $('#audio_list_tbl').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div><div class="h-40"></div>');
                    $('#page-selection').parent().show();
                }
            }            
            $('#page-selection').bootpag({
                total: Math.ceil(total),
                page: 1,
                maxVisible: Math.round(maxVisible)
            }).on('page', function (event, num) {
                $.post('/management/searchAudioPage', {'search_value': searchText, 'page': num}, function (res) {
                    $('#audio_list_tbl').html(res);
                    $('.loader').hide();
                });
            });
        });
    }
    $(function () {
        $('#dprogress_bar').draggable({
            containment: 'window',
            scroll: false
        });
    });
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<!-- Script for Upload Trailer -->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>


<script type="text/javascript">
    function confirmDelete(location, msg,obj) {
        swal({
            title: "Delete Content?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            DeleteAudio(location,obj);
        });

    }
    function click_browse() {
        $("#all_audiofile").click();
    }
    function checkfileSize() {
        //swal();
        var movie_name = 'All audio';
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "" + currentdate.getMonth() + "" + currentdate.getFullYear() + "" + currentdate.getHours() + "" + currentdate.getMinutes() + "" + currentdate.getSeconds();
        var stream_id = '<?php echo $studio_id; ?>' + datetime;

        var filename = $('#Filedata').val().split('\\').pop();
        //swal(filename);
        var extension = filename.replace(/^.*\./, '');


        if (extension == filename) {
            extension = '';
        } else {
            extension = extension.toLowerCase();
        }
        switch (extension) {
            case 'mp3':
                break;
            case 'ogg':
                break;
            default:
                // Cancel the form submission
                swal('Sorry! This audio format is not supported. \n MP3/OGG format audio are allowed to upload');
                return false;
                break;
        }
        swal({
            title: "Confirm Upload",
            text: "Are you sure to upload " + filename + " ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#10CFBD", customClass: "cancelButtonColor",
            confirmButtonText: "Upload",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $("#show_upload_div").hide();

                $("#addaudio_popup").modal('hide');

                if (!$('#dprogress_bar').is(":visible"))
                {
                    $('#dprogress_bar').show();
                }

                upload('user', 'pass', stream_id, movie_name);
                return true;

            } else {
                document.getElementById("Filedata").value = "";
                return false;
            }
        });



    }
// Multipart Upload Code
    var s3upload = null;
    var s3obj = new Array();
    var size = '';
    var sizeleft = 0;
    var sizeName = '';
    var seconds;
    function timeCal(seconds){
        var hours   = Math.floor(seconds / 3600);
        var minutes = Math.floor((seconds - (hours * 3600)) / 60);
        var sec = seconds - (hours * 3600) - (minutes * 60);
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (sec < 10) {sec = "0"+sec;}
        var  timeLeft = hours+':'+minutes+':'+sec;
        return timeLeft;
    }
    function upload(user, pass, stream_id, filename) {

        //var xhr = new XMLHttpRequest({mozSystem: true});
        var filename = $('#Filedata').val().split('\\').pop();
        //swal(filename);
        if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
            swal("Sorry! You are using an older or unsupported browser. Please update your browser");
            return;
        }

        /*if (s3upload != null) {
         swal("Though it is possible to upload multiple files at once, this demonstration does not allowed to do so to demonstrate pause and resume in a simple manner. Sorry :-(");
         return;
         }*/


        sizeleft = 0;
        var timeLeft = '00:00:00';
        var sizeKb = '';
        var sizeLeftKb;
        var speed="0 kbps";
        var speedMbps;
        var startTime = (new Date()).getTime();
        var file = $('#Filedata')[0].files[0];
        size = Math.round(file.size/1000);
        sizeKb=size;
        timeLeft = ((size/1000)/userinternetSpeed).toFixed(0);
        if(timeLeft < 4){
            timeLeft = 4;
        }
        if((size/1000)<6){
            speed=userinternetSpeed+"mbps";
        }
        timeLeft = timeCal(timeLeft);
        sizeName = 'Kb';
        if(size > 1000){
            size = Math.round(size/1000);
            sizeKb = size*1000;
            var sizeName = 'MB';
        }
        if(size > 1000){
            size = size/1000;
            size = size.toFixed(2);
            sizeKb = size*1000;
            var sizeName = 'GB';
        }
        // swal(file.name);
        //swal(file.type);
        // swal(file.size);

        filename = filename.replace(/(\.[^/.]+)+$/, "").replace(/[^a-z0-9.\s]/gi, '_').replace(/ /g, "_").replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') + "." + (filename).replace(/^.*\./, '');
        //swal(filename);

        s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'uploadType': 'audiogallery', 'file_name': filename}, 'management');
        s3upload.onServerError = function (command, jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 403) {
                swal("Sorry you are not allowed to upload");
            } else {
                console.log("Our server is not responding correctly");
            }
        };

        s3upload.onS3UploadError = function (xhr) {
            s3upload.waitRetry();
            console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
        };

        //s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
        s3upload.onProgressChanged = function (progPercent) {
            var endTime = (new Date()).getTime();
            var duration = (endTime-startTime)/1000;
            if (progPercent == 100) {
                $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
                $('#upload_' + stream_id + ' .uploadFileSizeProgress').html(size);
                $('#upload_'+stream_id+' .timeRemaining').html("00:00:00");
            } else {
                var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
                $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

                $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
                
                if(sizeName == 'GB'){
                    sizeleft = (progper * size)/100;
                    sizeleft = sizeleft.toFixed(2);
                }else{
                    sizeleft = Math.round((progper * size)/100);
                }
                if(sizeName == 'GB'){
                    speed=((sizeleft*1000*1000)/duration).toFixed(2) ;
                    sizeLeftKb = sizeleft*1000*1000;
                }
                if(sizeName == 'MB'){
                    speed=((sizeleft*1000)/duration).toFixed(2) ; 
                    sizeLeftKb = sizeleft*1000;
                }
                if(sizeName == 'KB'){
                    speed=((sizeleft)/duration).toFixed(2);
                    sizeLeftKb=sizeleft;
                }
                speedMbps=(speed/1000).toFixed(2);
                seconds = ((sizeKb-sizeLeftKb)/speed).toFixed(0);
                timeLeft = timeCal(seconds);               
                if(speed > 999){
                    speed=(speed/1000).toFixed(2)+"mbps";
                }
                else{
                    speed=speed + "kbps";
                }
                $('#upload_'+stream_id+' .uploadFileSizeProgress').html(sizeleft);
                $('#upload_'+stream_id+' .uploadSpeed').html(speed);
                $.post(HTTP_ROOT+"/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {});
                $('#upload_'+stream_id+' .timeRemaining').html(timeLeft);
            }
        };

        s3upload.onUploadCompleted = function (data) {
            console.log(data);
            var obj = jQuery.parseJSON(data);
            $('#upload_' + stream_id).remove();
            if ($('#all_progress_bar').is(":empty")) {
                $('#dprogress_bar').hide();

            }
            else
            {
                $('#dprogress_bar').show();
            }
            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg" style="display: block;"><i class="icon-check"></i>&nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Wow! Audio upload was successful.</div>'
            $('.pace').prepend(sucmsg);
            console.log("Congratz, upload is complete now");
            if ($('#all_progress_bar').is(":empty")) {
                location.reload();
            }

        };
        s3upload.onUploadCancel = function () {
            $('#upload_' + stream_id).remove();
            if ($('#all_progress_bar').is(":empty")) {
                $('#dprogress_bar').hide();
            }
            console.log("Upload Cancelled..");
        };

//Start s3 Multipart upload	
        s3upload.start();
        s3obj[stream_id] = s3upload;
        var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">'+sizeleft+'</span>/'+size+' '+sizeName+') (<span class="uploadSpeed">'+speed+'</span>)<span class="timeRemaining" style="float:right">'+timeLeft+'</span></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
        $('#all_progress_bar').append(progressbar);
        $('#cancel_' + stream_id).on('click', function () {
            aid = this.id;
            sid = aid.split('_');
            swal({
                title: "Cancel Upload?",
                text: "Are you sure you want to cancel this upload?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true
            }, function () {
                s3obj[sid[1]].cancel();
                document.getElementById("Filedata").value = "";
                //$('#dprogress_bar').html('');
                //$('#dprogress_bar').hide();
            });

        });
    }


    function manage_progressbar() {
        $("#all_progress_bar").toggle('slow');
    }
    function show_upload()
    {

        $("#show_upload_div").toggle();
        $("#file_error").html("");
    }

    function check_audio_exists(filename)
    {
        var flag_exist = 0;
        var res = filename.split(".");
        var file_name = res[0];
        var extension = res[1];
        var newString = file_name.replace(/[^A-Z0-9]/ig, "_");
        var converted_filename = newString + '.' + extension;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/management/checkAudio",
            data: {audio_file: converted_filename},
            success: function (response) {
                // swal(response);
                if (response == "exist")
                {
                    var flag_exist = 1;

                    swal(flag_exist);
                }
            }
        });
        return flag_exist;

    }


    /***code for url validations ****/
// Validate and save the Server path for audio
    function validateURL(divid) {
        //   swal();
        showLoader();
        var action_url = '<?php echo $this->createUrl('management/validateAudio'); ?>';
        if (divid) {
            $('#dropbox').html('');
            var url = $('#dropbox').val();
        } else {
            $('#server_url_error').html('');
            var url = $('#server_url').val();
        }
        var url_array = url.split("/");
        var audio_name = url_array.pop();
        var res = audio_name.split(".");
        var file_name = res[0];
        var extension = res[1];
        var newString = file_name.replace(/[^A-Z0-9]/ig, "_");
        var converted_filename = newString + '.' + extension;
        if (divid) {
            var ftpusername = '';
            var ftppassword = '';
        } else {
            var ftpusername = $('#ftpusername').val();
            var ftppassword = $('#ftppassword').val();
        }
        if (url && isUrlValid(url)) {
            $.post(action_url, {'url': url, 'audio_name': converted_filename, 'username': ftpusername, 'password': ftppassword}, function (res) {
                showLoader(1);

                if (res.error) {
                    if (divid) {
                        $('#dropbox_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                    } else {
                        $('#server_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                    }
                } else {
                    if (res.is_audio) {
                        if (divid) {
                            $('#dropbox').val('');
                            $('#dropbox_url_error').val('');
                        } else {
                            $('#server_url').val('');
                            $('#server_url_error').val('');
                        }
                        location.reload();

                    } else {
                        if (divid) {
                            $('#dropbox_url_error').html('Please provide the path of a valid audio file');
                        } else {
                            $('#server_url_error').html('Please provide the path of a valid audio file');
                        }
                    }
                }
            }, 'json');
            console.log('valid url');
        } else {
            showLoader(1);
            if (divid) {
                $('#dropbox_url_error').html('Please provide the path of a valid audio file');
            } else {
                $('#server_url_error').html('Please provide the path of a valid audio file');
            }
            return false;
        }
    }
    function isUrlValid(url) {
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#addaudio_popup button,input[type="text"]').attr('disabled', 'disabled');
        } else {
            $('.loaderDiv').hide();
            $('#addaudio_popup button,input[type="text"]').removeAttr('disabled');
        }
    }
    function audioSync() {
        $('#myvisyncLoader').show();
        $("#audioSync").modal('hide');
        $('#audioSync button').attr('disabled', 'disabled');
        $.post('<?php echo Yii::app()->getBaseUrl(true); ?>/management/audioSync', function (res) {
            window.location = "<?php echo Yii::app()->getBaseUrl(true); ?>/management/manageaudio";
        });
    }
    function UploadAudioS3()
    {
        // $('#myvisyncLoader').show();
        // $("#audiogallery").modal('hide');

        $('#audiogallery button').attr('disabled', 'disabled');
        $.post('<?php echo Yii::app()->getBaseUrl(true); ?>/management/UploadAudioS3', function (res) {

            window.location = "<?php echo Yii::app()->getBaseUrl(true); ?>/management/manageaudio";
        });
    }

    function ShowClickbrowse() {
        $('#Filedata').click();
    }

    function delete_selected()
    {

        var allVals = [];
        $(".sub_chk:checked").each(function () {
            allVals.push($(this).attr('data-id'));
        });
        //alert(allVals.length); return false;  
        if (allVals.length <= 0)
        {
            swal("Please select audio to delete");
        }
        else {
            //show alert as per the audios mapped status
            check_audiomapped(allVals);
        }
    }
    function check_audiomapped(allVals)
    {
        //show alert as per the audios mapped

        $.ajax({
            url: '<?php echo Yii::app()->getBaseUrl(true); ?>/management/CheckMapped',
            method: 'POST',
            data: {id: allVals},
            success: function (res)
            {

                if (res == 0)
                {

                    //swal("Are you sure to delete "+allVals.length+" audios?"); 
                    swal({
                        title: "Delete Content?",
                        text: "Are you sure to delete " + allVals.length + " audios?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true,
                        html: true
                    }, function () {
                        delete_multiple_audio(allVals);
                    });

                }
                else
                {
                    //swal("Following audios are mapped to a content. Are you sure to delete them?");      
                    swal({
                        title: "Delete Content?",
                        text: "Following audios are mapped to a content. Are you sure to delete them?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true,
                        html: true
                    }, function () {
                        delete_multiple_audio(allVals);
                    });
                }
                //delete audio

            }
        });
    }


//delete viudeo
    function delete_multiple_audio(allVals) {
        $.ajax({
            url: '<?php echo Yii::app()->getBaseUrl(true); ?>/management/DeleteAudio',
            method: 'POST',
            data: {id: allVals},
            success: function ()
            {
                window.location.reload();
            }
        });
    }
    function reset_filter()
    {
        $("#audio_duration").val(0);
        $("#file_size").val(0);
        $("#is_encoded").val(0);
        $("#uploaded_in").val(0);
        view_filtered_list();
    }

    function view_filtered_list() {
        showLoader(1);
        var audio_duration = $("#audio_duration").val();
        var file_size = $("#file_size").val();
        var is_encoded = $("#is_encoded").val();
        var uploaded_in = $("#uploaded_in").val();
        var searchText = $.trim($(".search").val());
        var url = "<?php echo Yii::app()->baseUrl; ?>/Management/searchAudioPage";
        //window.location.href = url;
        $.post(url, {'audio_duration': audio_duration, 'file_size': file_size, 'is_encoded': is_encoded, 'uploaded_in': uploaded_in,'search_value':searchText}, function (res) {
            if (res) {
                $('#audio_list_tbl').html(res);
                var mydata = $(res).find('data-count');
                var mydata1 = $("#data-count1").val();
                var regex = /\d+/g;
                var count = mydata.prevObject[0].outerHTML.match(regex);
                var page_size = $('#page_size').val();
                var total = parseInt(count) / parseInt(page_size);
                var maxVisible = total / 2;
                console.log(count);
                console.log(page_size);
                console.log(maxVisible);
                if (maxVisible >= 10) {
                    maxVisible = 10;
                } else {
                    maxVisible = total;
                }

                if ($('.page-selection-div').length) {
                    $('.page-selection-div').remove();
                }
                if (parseInt(count) < parseInt(page_size) || parseInt(mydata1) < 20) {
                    $('#page-selection').parent().hide();
                } else {
                    if ($('.page-selection-div').length) {
                        $('#page-selection').parent().show();
                    } else {
                        $('#audio_list_tbl').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div><div class="h-40"></div>');
                        $('#page-selection').parent().show();
                    }
                }
                $('#page-selection').bootpag({
                    total: Math.ceil(total),
                    page: 1,
                    maxVisible: Math.round(maxVisible)
                }).on('page', function (event, num) {
                    $.post('/management/searchAudioPage', {'search_value': searchText, 'page': num}, function (res) {
                        $('#audio_list_tbl').html(res);
                        $('.loader').hide();
                    });
                });
            }
        });
    }
    $(document).ready(function () {
        $("#filetype").change(function () {
            $('.savefile').hide();
            $('#' + $(this).val() + '_div').show();
        });
    });
    function DeleteAudio(id,obj){
        var url = '<?php echo Yii::app()->baseUrl; ?>/Management/DeleteAudio';
        $.post(url, {'id': id}, function (res) {
            //$(obj).parent().parent().parent().remove();
            window.location.reload();
        });
    }
</script>
<script>window.jQuery || document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/main.js"></script>
