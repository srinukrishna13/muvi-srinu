
<div class="col-md-6 col-sm-6 frt_form frt_rht_box">
    <?php if((isset($_GET['purchasetype'])=='freetrial') && trim($_GET['purchasetype']=='freetrial')){?>
    <h1>Start your 14 Days Free Trial!</h1>
     <?php }else if((isset($_GET['purchasetype'])=='buynow') && trim($_GET['purchasetype']=='buynow'))
    {?>
    <h1>Start your Subscription Now!</h1>
     <?php }else{
    ?>
    <h1>Start your 14 Days Free Trial!</h1>
     <?php }?>
    <p class="frt_sub_heading">
        <?php
      if((isset($_GET['purchasetype'])=='buynow') && trim($_GET['purchasetype']=='buynow'))  
     {?> Credit Card Required  <?php }else{?>
     No Credit Card Required
     <?php }?>
    </p>
     
    <?php
    $purchasetype = '';
    if((isset($_GET['purchasetype'])=='freetrial') && trim($_GET['purchasetype']=='freetrial'))
    {
        $purchasetype = $_GET['purchasetype'];
        ?> 
    <div class="frt_steps_box">
        <div class="frt_steps_box1 frt_steps_box1_color"></div>
        <div class="frt_steps_box2 frt_steps_box2_color"></div>
    </div>
   <?php } else if((isset($_GET['purchasetype'])=='buynow') && trim($_GET['purchasetype']=='buynow'))
        {
        $purchasetype = $_GET['purchasetype'];
        ?>
        <div class="frt_steps_box111">
            <div class="frt_steps_box11 frt_steps_box11_color"></div>
            <div class="frt_steps_box21 frt_steps_box21_color"></div>
            <div class="frt_steps_box31 frt_steps_box31_color"></div>
            </div>
        
   <?php }else { ?>
        <div class="frt_steps_box">
        <div class="frt_steps_box1 frt_steps_box1_color"></div>
        <div class="frt_steps_box2 frt_steps_box2_color"></div>
    </div>
   <?php }?>
    <form method="post"  name="userinfo" id="userinfo">
        <div class="form-group fg-float">
            <div class="fg-line">
                <input type="text" class="input-sm form-control fg-input" name="name" id="name"  value="<?php  echo @$wpuser['name'];?>" />
                <label class="fg-label">Name</label>
            </div>
            <label id="name-error" class="error" for="name"></label>
        </div>

        <div class="form-group fg-float">
            <div class="fg-line">
                <input type="text" class="input-sm form-control fg-input" name="companyname" id="companyname" value="<?php echo @$studio->name?>" onblur="autofill_domain()" />
                <label class="fg-label">Company Name</label>
            </div>
            <label id="companyname-error" class="error" for="companyname"></label>
        </div>


        <div class="form-group fg-float">
            <div class="fg-line">
                <input type="text" class="input-sm form-control fg-input" name="phone" value="<?php echo @$user->phone_no?>" />
                <label class="fg-label">Phone Number (with country code) </label>
            </div>
            <label id="phone-error" class="error" for="phone"></label>
        </div>

        <div class="form-group fg-float">
            <div class="fg-line">
                <input type="text" class="input-sm form-control fg-input inputchk" id="email" name="email" value="<?php echo @$wpuser['email']; ?>" />
                <label class="fg-label">Email</label>
            </div>
            <label id="email-error" class="error" for="email"></label>
        </div>
        <div class="form-group fg-float">
            <div class="fg-line">
                <input type="password" class="input-sm form-control fg-input" id="inputPassword" name="password" value="<?php echo @$wpuser['password']; ?>" />
                <label class="fg-label">Password</label>
            </div>
            <label id="inputPassword-error" class="error" for="inputPassword"></label>
        </div>
        <div class="form-group fg-float">
            <div class="fg-line">
                <input type="text" class="input-sm form-control fg-input" placeholder="yourdomainname.<?php echo $domain;?>" id="subdomain" name="subdomain" onkeyup="getdomainUrl();" onblur="getdomainUrl();" autocomplete="off" value="<?php echo @$studio->subdomain?>" <?php if(isset($studio->subdomain) && $studio->subdomain != ''){ echo 'disabled';}?> style="padding-top:10px;" />
                <label class="fg-label fg-label-domain">Domain Name</label>
            </div>
<!--            <div><label class="hintlbl">You can change this later</label></div>-->
            <label id="subdomain-error" class="error" for="subdomain"></label>
            <label class="hintlbl2">Your website will be at <span class="domain-url">http://www.<span class="<?php if(@$studio->subdomain ==''){echo 'disabled';}?> subText" id="subText"><?php if(@$studio->subdomain !=''){echo @$studio->subdomain;}else{echo 'yourname';}?></span>.<?php echo $domain;?></span>, you can change this later.</label>
        </div>
         <input type="text" name="studio_email" id="studio_email" value="" />
        <div class="text-center">
            <div><label class="checkbox checkbox-inline">
                <input type="checkbox" name="terms" />
                <i class="input-helper"></i>
                I agree to Muvi&rsquo;s <a href="/agreements/muvi-terms-service" target="_blank" class="termservice">Term of Service</a>
                </label></div>
            <label id="terms-error" class="error" for="terms"></label>
        </div>

        <div class="form-group fg-float nxtbtnbox">
        <input type="hidden" name="country" value="<?php echo $country ?>" readonly="readonly" />
        <input type="hidden" name="region" value="<?php echo $region ?>" readonly="readonly" />
        <input type="hidden" value="0" id="is_submit" name="is_submit"/>
        <input type="hidden" value="<?php echo $purchasetype;?>" name="purchasetype"/>
        <input type="hidden" value="<?php echo Yii::app()->session['studio_id'];?>" id="studio_id" />
         <button type="submit" id="nextbtn" name="nextbtn" class="btn btn-primary frt_btn">NEXT</button>
        </div>
    </form> 
</div>

<script type="text/javascript">
$(document).ready(function(){

$(".inputchk").on('blur',function()
{
    if($("#email").val()=="")
    {  
        $('#email-error').show();
        $('#email-error').html('Please enter your email address');
    }else{
        checkEmail();
    }
});


    $('#studio_email').hide();
	jQuery.validator.addMethod("alphaNumeric", function (value, element) {
		return this.optional(element) || /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i.test(value);
	}, "Password field must be minimum 8 character length with at least 1 number and 1 alphabet");
        jQuery.validator.addMethod("phoneno", function (value, element) {
		return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/i.test(value);
	}, "Please enter a valid phone number");
	jQuery.validator.addMethod("url2", function(value, element, param) {
            value = value.replace('http://', '');
		if(this.optional(element) || /^(w{3})?[a-z0-9]+([\-\.]{1}[a-z0-9]+){0,3}$/i.test(value)){
			var urls = value.split(/[\s.]+/);
			if(urls.length==2 && urls[0]=='www'){
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}, jQuery.validator.messages.url);
        
        $("#email").on('keypress', function() {
           // $("#userinfo").find('input').css("opacity",'1');
        });
});
//form validation rules
$("#userinfo").validate({
    rules: {
        name: {
            required : true, 
            minlength : 2
        },
        companyname: {
            required : true
        },
        email: {
            required: true,
            mail: true
        },              
        phone: {
            required: false,
            number: true,
            minlength:8,
            maxlength: 15
            
        },                
        password: {
            required: true,
            minlength: 8,
            alphaNumeric: true
        },
        subdomain: {
            required: true,
            url2 : true
        },
        terms:{
            required:true
         }
    },
    messages: {
        name: {
            required: "Please enter your name",
            minlength: "Your name must be at least 2 characters long"
        },
        companyname: {
            required: "Please enter company name"
        },   
        email: {
            required: "Please enter your email address",
            mail: 'Please enter a valid email address'
        },               
        phone: {
            required:'Please enter your phone number',
            number: 'Please enter only numbers, no special characters or alphabets allowed',
            minlength: "Phone number should be at least 8 digits long, starting with country code",
            maxlength: "Please enter a valid phone number"
        },                
        password: {
            required: "Please provide a password",
            minlength: "Password field must be minimum 8 character length with at least 1 number and 1 alphabet",
            alphaNumeric: "Password field must be minimum 8 character length with at least 1 number and 1 alphabet"
        },
        subdomain: {
                required: "Please enter a domain name",
                minlength: "Your domain name must be at least 3 characters long"
        },
        terms:{
            required: "Please accept the Term of Service to continue",
        }
    },
    submitHandler: function(form) {
      $('#nextbtn').html('wait!...');
        $('#nextbtn').attr('disabled', 'disabled');
        var url = "<?php echo Yii::app()->baseUrl; ?>/signup/checkDomainEmail";
        var wb = $('#subdomain').val();
        wb = wb.replace('http://', '');
        var wb_parts = wb.split(/[\s.]+/);
        if(wb_parts.length == 1)
            $('#subdomain').val(wb_parts+'.com');        
         var studio_id = $('#studio_id').val();
         
        if(studio_id=='' || !studio_id){
        $.post(url, {'email': $('#email').val(),'subdomain':$('#subdomain').val()}, function (res) {
            if (res.succ) {
                $('#is_submit').val(1);
                document.userinfo.action = '<?php echo Yii::app()->baseUrl; ?>/signup';
                document.userinfo.submit();
            } else {                
                $('#nextbtn').html('Next');
                $('#nextbtn').removeAttr('disabled');
                $('#is_submit').val('');
				if(res.email_error){
                                    $('#email-error').show();
                                    $('#email-error').html('This email address has already been used. Please login using this Email Address.');
				}
				if(res.purchase_error){
                                    //$("#userinfo").find('input').css("opacity",'0.5');
                                    $("#email").css("opacity",'1');
                                    var subscription_url = "<?php echo Yii::app()->baseUrl; ?>/signup/purchaseSubscription?studio="+res.uniqid;
                                    $('#email-error').show();
                                    $('#email-error').html('This email address has already been used. <a href="'+subscription_url+'">Purchase Subscription</a> to continue.');
				}
				if(res.domain_error){
                                    $('#subdomain-error').show();
                                    $('#subdomain-error').html('Oops! Sorry this domain name has already taken by someone else.');
				}
				if(res.invalid_url){
                                    $('#subdomain-error').show();
                                    $('#subdomain-error').html('Please enter a valid domain name.');
				}
            }
        }, 'json');
        }else{
            document.userinfo.action = '<?php echo Yii::app()->baseUrl; ?>/signup';
            document.userinfo.submit();
        }
    }            
});
function checkEmail(){
    var url = "<?php echo Yii::app()->baseUrl; ?>/signup/checkDomainEmail";
    $.post(url, {'email': $('#email').val()}, function (res) {
        if (res.succ) {
        }else{ 
            if(res.email_error){
                $('#email-error').show();
                $('#email-error').html('This email address has already been used. Please use another Email Address.');
            }
            if(res.purchase_error){
                $('#email-error').show();
                var subscription_url = "<?php echo Yii::app()->baseUrl; ?>/signup/purchaseSubscription?studio="+res.uniqid;
                $('#email-error').show();
                $('#email-error').html('This email address has already been used. <a href="'+subscription_url+'">Purchase Subscription</a> to continue.');
            }
        }
    }, 'json');
}
function getdomainUrl(){
    var subdomain = $.trim($('#subdomain').val()).toLocaleLowerCase();
    subdomain = subdomain.replace('http://', '');
    subdomain = subdomain.replace(/[^a-z0-9.\s]/gi, '').replace(/[_\s]/g, '');
    subdomain = subdomain.replace('www.', '');
    var pieces = subdomain.split(/[\s.]+/);
    console.log(pieces);
    //subdomain = subdomain.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
    if(subdomain && pieces.length > 0){
        $('.subText').html(pieces[0]);
        $('.subText').removeClass('disabled');
    }    
    else if(subdomain && pieces.length>2){
        $('.subText').html(pieces[pieces.length-2]);
        $('.subText').removeClass('disabled');
    }else if(subdomain && (pieces.length==2) && (pieces[0]!='www')){
        $('.subText').html(pieces[pieces.length-2]);
        $('.subText').removeClass('disabled');
    }
    else if(subdomain){
        $('.subText').html(subdomain);
        $('.subText').removeClass('disabled');                    
    }    
    else{
        $('.subText').addClass('disabled');
        $('.subText').html('yourname');
    }
}

function autofill_domain()
{
   var company_name=$("#companyname").val();
   
   var url = "<?php echo Yii::app()->baseUrl; ?>/signup/checkDomainName";
    $.post(url, {'company_name': company_name}, function (res) {
            if (res) {
               $('#subdomain').val($.trim(res));
                var subdomain = $.trim($('#subdomain').val()).toLocaleLowerCase();
                subdomain = subdomain.replace('http://', '');
                subdomain = subdomain.replace(/[^a-z0-9.\s]/gi, '').replace(/[_\s]/g, '');
                subdomain = subdomain.replace('www.', '');
                var pieces = subdomain.split(/[\s.]+/);
                $('.subText').html(pieces[0]);
               //$('#subText').html($.trim(res));
               console.log($('#subdomain').val());
            } else { 
              // $('#subdomain-error').show();
               //$('#subdomain-error').html('Error in domain  name assign');        
            }
        });
}
</script>