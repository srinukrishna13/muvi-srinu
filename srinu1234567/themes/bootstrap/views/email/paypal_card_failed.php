<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
    <tr>
        
        <td align="left" style="padding-left:20px;"><img src="<?php echo EMAIL_LOGO; ?>"/></td>
            </tr>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	Hi <span mc:edit="name"></span>,
                            </p>              
							
                            <p style="display:block;margin:0 0 17px">
                                Recurring payment for profile <b> <span mc:edit="profile_id"></span> </b> has failed.
                            </p>
                            
                            <p style="display:block;margin:0 0 17px">
                                <b>Profile Details:</b>
                            </p> 
                            
                            <p style="display:block;margin:0 0 17px">
                                Card holder name: <span mc:edit="user_name"></span>
                            </p> 
                            
                            <p style="display:block;margin:0 0 17px">
                                Card holder email: <span mc:edit="user_email"></span>
                            </p> 
                            
                            <p style="display:block;margin:0 0 17px">
                                Profile ID: <span mc:edit="profile_id"></span>
                            </p>  
                            <hr>
                            <p style="display:block;margin:0 0 17px">
                                Failed Reason: <span mc:edit="reason"></span>
                            </p> 
                            <hr>
                            <p style="display:block;margin:0 0 17px">
                                Regards,<br/>
                                Muvi Team
                            </p>
 
						</div>
					</td>
				</tr>
                 
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>