function initSliderOne() {
    var sliderHeight = 0;
    var slideLength = 0;
    var visibleThumb = 0;
}
$(window).resize(function () {
    startSliderOne();
});
function configSliderOne(thumb_visibe) {
    visibleThumb = thumb_visibe;
    slideLength = $('.slider-one-wrap > .thumb-list ul li').length;
    $('.slider-one-wrap > .thumb-list ul li').click(function () {
        $('.flexslider').flexslider($(this).index());
    });
    $('.slider-one-wrap > .thumb-list .dire-nav._top').click(function () {
        $('.flexslider').flexslider('prev');
    });
    $('.slider-one-wrap > .thumb-list .dire-nav._bottom').click(function () {
        $('.flexslider').flexslider('next');
    });

}

function startSliderOne(widthBanner,heightBanner,auto,slider) {
    var imgRatio = heightBanner / widthBanner;
    var itemHei = 0;
    $('.slider-one-wrap .slides li video, .slider-one-wrap .slides li img').each(function () {
        $(this).height($(this).closest('.flexslider').width() * imgRatio);
    });
    if (auto == false){
		slider.pause();
	}
    var isIE = false;
    if(navigator.appVersion.toUpperCase().indexOf("MSIE") != -1 || 
        navigator.appVersion.toUpperCase().indexOf("TRIDENT") != -1 || 
        navigator.appVersion.toUpperCase().indexOf("EDGE") != -1){
        isIE = true;
    }
    if(isIE == true){
        $('.slider-one-wrap .slides li video').each(function () {
			var cVdo = $(this);
			if(widthBanner != heightBanner){
				$(this).width($(this).closest('li').width());
				if (heightBanner >= cVdo[0].videoHeight){
					$(this).height($(this).closest('.flexslider').width() * imgRatio);
				}else{
					$(this).height('auto');
				}
				$(this).closest('.videocontainer').height($(this).closest('.flexslider').width() * imgRatio);
			} else {
				$(this).width($(this).closest('li').width());
				$(this).height($(this).closest('.flexslider').width() * imgRatio);
				$(this).closest('.videocontainer').height($(this).closest('.flexslider').width() * imgRatio);
			}
        });
    }

	sliderHeight  = $('.slider-one-wrap .slides li').height();
	$('.slider-one-wrap > .thumb-list ul li').each(function () {
        $(this).css({'height': sliderHeight / visibleThumb});
    });

}
function setActive() {
    $('.slider-one-wrap > .thumb-list ul li').eq(0).addClass('_active');
}

function thumbAnimsliderOne(slider,auto) {
    $('.slider-one-wrap > .thumb-list ul li._active').removeClass('_active');
    $('.slider-one-wrap > .thumb-list ul li').eq(slider.currentSlide).addClass('_active');

    if ($('.slider-one-wrap > .thumb-list ul li._active').index() < slideLength - (visibleThumb - 1)) {
        $('.slider-one-wrap > .thumb-list ul').attr("style", 'margin-top:' + $('.slider-one-wrap > .thumb-list ul li._active').position().top * -1 + 'px !important');
    } else {
        $('.slider-one-wrap > .thumb-list ul').attr("style", 'margin-top:' + $('.slider-one-wrap > .thumb-list ul li').eq(slideLength - visibleThumb).position().top * -1 + 'px !important');
    }
	if (auto == false){
		slider.pause();
	}
}

function videoPlay(flxSlider) {
    if ($('.flexslider ul.slides li.flex-active-slide video').length) {
        $('.flexslider ul.slides li.flex-active-slide video').trigger('pause');
    }
    if ($('.flexslider ul.slides li').eq(flxSlider.getTarget("next")).find('video').length) {
        $('.flexslider ul.slides li').eq(flxSlider.getTarget("next")).find('video').trigger('play');
    }
}

function playVideoBanner(flxSlider, auto) {
    if ($('.flexslider ul.slides li.flex-active-slide video').length || auto == false) {
       flxSlider.pause();
       $('.flexslider ul.slides li.flex-active-slide video').trigger('play');
    }else{
        if (!flxSlider.playing) {
            flxSlider.play();
          }
    }
    
}

  $( window ).scroll(function() { //stop playing video after scroll down.
	  if($('.slider-one-wrap').length > 0){
		if($(window).scrollTop() > $('.slider-one-wrap').offset().top + $('.slider-one-wrap').height() ){
			$('.slider-one-wrap .flexslider ul.slides li.flex-active-slide video').trigger('pause');
		}else{
			$('.slider-one-wrap .flexslider ul.slides li.flex-active-slide video').trigger('play');
		}
	}
});