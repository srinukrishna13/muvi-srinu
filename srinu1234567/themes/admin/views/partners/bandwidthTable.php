<thead>
    <th>User</th>
    <th>Date & Time</th>
    <th data-hide="phone">Start</th>
    <th data-hide="phone">End</th>
    <th data-hide="phone">Duration</th>
    <th data-hide="phone">Resolution</th>
    <th data-hide="phone">Geography</th>
    <th data-hide="phone">Bandwidth</th>
</thead>
<tbody class="list">
    <?php
        if(isset($videoTableData) && !empty($videoTableData['data'])){
            foreach ($videoTableData['data'] as $video){ 
                    $country = $video['country'];
                    $bandwidth_consumed = $this->formatKBytes($video['buffer_size']*1024,2);
                    $temp = explode(' ', $bandwidth_consumed);
                    if($video['played_time'] >= 0){
                    if($temp[0] > 0){
                ?>
                <tr>
                    <td class="user"><?php echo $video['display_name'];?></td>
                    <td><?php echo $video['created_date'];?></td>
                    <td><?php echo gmdate("H:i:s", $video['start_time']);?></td>
                    <td><?php echo gmdate("H:i:s", $video['end_time']);?></td>
                    <td><?php echo gmdate("H:i:s", $video['played_time']);?></td>
                    <td><?php echo (isset($video['resolution']) && trim($video['resolution']) === 'BEST') ? 'BEST': (trim($video['resolution'])) ? $video['resolution'].'p' : '';?></td>
                    <td class="geo"><?php echo $country;?></td>
                    <td><?php echo $bandwidth_consumed;?></td>

                </tr>
            <?php   }}}
        }else{
            echo '<tr><td colspan="8">No Record found!!!</td></tr>';
        }
    ?>
</tbody>