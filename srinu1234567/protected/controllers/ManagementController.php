<?php

require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;

require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
require_once 'FirstDataApi/FirstData.php';

class ManagementController extends Controller {

    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = '';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);

        Yii::app()->theme = 'admin';
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            Yii::app()->layout = 'partners';
        }else{
            Yii::app()->layout = 'admin';
        }
        if (!(Yii::app()->user->id)) {
            $this->redirect(array('/index.php/'));
        }else{
             $this->checkPermission();
        }

        if (!isset(yii::app()->user->is_sdk) && !yii::app()->user->is_sdk) {
            $studioArray = array('customerslist', 'addbill', 'editbill', 'maketransaction', 'Suspend', 'resume');
            if (in_array(Yii::app()->controller->action->id, $studioArray)) {
                $this->redirect(array('/index.php/'));
            }
        }
        return true;
    }

    public function actionIndex() {

        // Yii::app()->theme = 'admin';
        $this->layout = false;
        if (!isset(Yii::app()->user->id)) {
            $redirect_url = Yii::app()->getBaseUrl(true);
            $this->redirect($redirect_url);
            exit;
        }
        $rec_id = $_REQUEST['rec_id'];
        $data = ImageManagement::model()->findByPk($rec_id);
        // print_r($data['image_name']);
        //  exit;
        if ($data) {
                      if(($data['image_name']=='')) {
                      
                      $img_path=$_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'images/public/system/no-image-h.png';
                      $this->render('previewImage',array('src'=>$img_path));  
                            }
                            else
                            {
                             $db_img_path=$data['image_name'];   
                             $src_explode=explode('newstudio/',$db_img_path);
                             $img_path=$src_explode[1];
                             //echo $img_path; exit;
                             $this->render('previewImage',array('src'=>$img_path)); 
                            }
                    }
                    else
                    {
            Yii::app()->user->setFlash('error', 'Oops! No Images available');
            $this->redirect($_SERVER['HTTP_REFERER']);
            exit;  
                        
                    }
           } 
        /**
     * @method manageimage() image management method
     *
     */   
        public function actionManageImage() {
        //echo "test";  
             $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('Manage Content','Image Library');
        $this->headerinfo = "Image Library";
        $this->pageTitle = "Muvi | Image Library";
        $this->layout = 'admin';
        //$allImage = ImageManagement::model()->findAllByAttributes(array('flag_deleted' => 0, 'studio_id' => $studio_id),array('order' =>'id DESC' ));
        $searchStr = '';
        $page_size = 20;
        $offset = 0;
        $qry = 'SELECT SQL_CALC_FOUND_ROWS(0),i.* from image_management i WHERE '.$cond.' i.studio_id='.$studio_id.' order by i.image_name ASC LIMIT '.$offset.','.$page_size;
         
        $allimage = Yii::app()->db->createCommand($qry)->queryAll();
     
        $sql = 'select * from image_management where flag_deleted = 0 and studio_id ='.$studio_id;
        $countallimage = Yii::app()->db->createCommand($sql)->queryAll();
        $countall_image=count($countallimage);
        $count_searched=count($allimage);
        $checkImageKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
        $this->render('manageimage', array('allimage' => $allimage,'page_size' => $page_size,'total_image'=>$countall_image, 'checkImageKey'=>$checkImageKey));
    }

    public function actionManageVideo() {
        $studio_id = $this->studio->id;
        $this->breadcrumbs = array("Manage Content", 'Video Library');
        $this->headerinfo = "Video Library";
        $this->pageTitle = "Muvi | Video Library";
        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'enable_video_sync_for_videogallery');
        $this->render('managevideo', array('getStudioConfig' => $getStudioConfig));
        }
            
    public function actionsearchVideoPage() {
      $searchStr = '';
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio_id = $this->studio->id;
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        if (isset($searchKey) && $searchKey != '') {
            $cond = 'vm.video_name like "%' . $searchKey . '%" AND';
        } else {
            $cond = '';
        }
        // video_properties
        if (isset($_REQUEST['video_duration']) && isset($_REQUEST['file_size']) && isset($_REQUEST['is_encoded']) && isset($_REQUEST['uploaded_in'])) {
            $duration = $_REQUEST['video_duration'];
            $file_size = $_REQUEST['file_size'];
            $is_encoded = $_REQUEST['is_encoded'];
            $uploaded_in = $_REQUEST['uploaded_in'];
           //Duration
            if ($duration == 1) {// <5 mins
                $cond.='CAST(vm.duration AS TIME) < "00:05:00" AND ';
            } else if ($duration == 2) {// <30 mins
               $cond.='CAST(vm.duration AS TIME) < "00:30:00" AND ';
            } else if ($duration == 3) {// <120 mins
             $cond.='CAST(vm.duration AS TIME)< "02:00:00" AND ';  
            } else if ($duration == 4) {// >120 mins
           $cond.='CAST(vm.duration AS TIME) > "02:00:00" AND ';    
            } else {
            $cond.='';   
           }
           //File Size
            if ($file_size == 1) {//< 1GB=1024 MB
             $cond.='SUBSTRING_INDEX(vm.file_size," ", 1)< 1024 AND ';     
            } else if ($file_size == 2) {//<10 GB
             $cond.='SUBSTRING_INDEX(vm.file_size," ", 1)< 10240 AND ';     
            } else if ($file_size == 3) {//>10 GB
             $cond.='SUBSTRING_INDEX(vm.file_size," ", 1)> 10240 AND ';     
            } else {
               $cond.=''; 
            }
            //uploaded in 
            if ($uploaded_in == 1) {//this week
             $cond.=' YEARWEEK(vm.creation_date,1) =  YEARWEEK(CURDATE(), 1) AND ';    
            } else if ($uploaded_in == 2) { //this month
               $cond.=' MONTH(vm.creation_date)=  MONTH(CURDATE()) AND';  
            } else if ($uploaded_in == 3) {//this year
               $cond.=' YEAR(vm.creation_date) = YEAR(CURDATE()) AND ';  
            } else if ($uploaded_in == 4) {// before this year
               $cond.=' YEAR(vm.creation_date) < YEAR(CURDATE()) AND ';  
            } else {
              $cond.='';  
            }
            //encoded ??
            if ($is_encoded == 1) {//yes
             $cond.='(ms.is_converted=1  OR mt.is_converted=1) AND ';   
            } else if ($is_encoded == 2) {//No
             $cond.='(ms.is_converted  IS NULL OR ms.is_converted =0 ) AND (mt.is_converted  IS NULL OR mt.is_converted =0) AND ';    
            } else {
              $cond.='';  
            }
           }
		$command = Yii::app()->db->createCommand()
				->select('SQL_CALC_FOUND_ROWS (0), vm.*, vs.id AS video_subtitle_id,ms.is_converted as movieConverted,mt.is_converted as trailerConverted')
				->from('video_management vm')
				->leftJoin('video_subtitle vs', 'vm.id=vs.video_gallery_id')
				->leftJoin('movie_streams ms', 'vm.id=ms.video_management_id')
				->leftJoin('movie_trailer mt', 'vm.id=mt.video_management_id')
				->where($cond . ' vm.studio_id=' . $studio_id)
				->group('vm.id')
				->order('vm.video_name ASC')
                ->limit($page_size, $offset);
		if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $pcontent = Yii::app()->general->getPartnersContentIds();
            if ($pcontent['movie_id']) {
				$command->andwhere(' (ms.movie_id IN(' . $pcontent['movie_id'] . ') OR vm.user_id=' . Yii::app()->user->id . ') ');
			} else {
				$command->andwhere(' AND vm.user_id=' . Yii::app()->user->id . ' ');
            }            
        }
		$allvideo = $command->queryAll();
		$countall_video = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();		
        $count_searched = count($allvideo);		
        $this->renderPartial('videoRenderData', array('allvideo' => $allvideo, 'page_size' => $page_size, 'total_video' => $countall_video, 'count_searched' => $count_searched));
            }            
        
    public function actionsearchImagePage() {
      $searchStr = '';
        $page_size = 20;
        $offset = 0;
        
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
          
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('Image Library');
        $this->pageTitle = "Muvi | Image Library";
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        if (isset($searchKey) && $searchKey != '') {

            $cond = 'i.image_name like "%' . $searchKey . '%" AND';
        } else {
            $cond = '';
        }
        $qry = 'SELECT SQL_CALC_FOUND_ROWS(0),i.* from image_management i WHERE ' . $cond . ' i.studio_id=' . $studio_id . ' order by i.image_name ASC LIMIT ' . $offset . ',' . $page_size;
        $allimage = Yii::app()->db->createCommand($qry)->queryAll();
        $sql = 'select * from image_management where flag_deleted = 0 and studio_id =' . $studio_id;
        $countallimage = Yii::app()->db->createCommand($sql)->queryAll();
        $countall_image = count($countallimage);
        $count_searched = count($allimage);
        $checkImageKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
        $this->renderPartial('imageRenderData', array('allimage' => $allimage, 'page_size' => $page_size, 'total_image' => $countall_image, 'count_searched' => $count_searched, 'checkImageKey'=>$checkImageKey));
    }
    public function actionSetImageDimension() {

        $type_id = $_REQUEST['type_id'];
        $criteria = new CDbCriteria();
        $delete_flag = 0;
        $criteria->addCondition("id=$type_id");

        $typedetails = ImageType::model()->findAll($criteria);
        $size_array = array();
        foreach ($typedetails as $key => $val) {
            $size_array['set_val'] = $val['image_width'] . 'px (w) X' . $val['image_height'].' px (h)';
            $size_array['height'] = $val['image_height'];
            $size_array['width'] = $val['image_width'];
        }
        $res = json_encode($size_array);
        echo $res;
    }

    public function actionDeleteImage() {

        $rec_id = $_REQUEST['rec_id'];
        $used_count=$_REQUEST['No_of_Times_Used'];
        //echo $used_count;
        //exit;
        $studio_id = Yii::app()->user->studio_id;
        if ($used_count == 0) {
            
            $all_details = ImageManagement::model()->get_imagedetails_by_id($rec_id);
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);

            
            $s3dir = $unsignedBucketPath . $all_details[0]['s3_thumb_name'];
            $s3dir1 = $unsignedBucketPath . $all_details[0]['s3_original_name'];

            $bucketInfo = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
            $bucket = $bucketInfo['bucket_name'];

            $result = $s3->deleteObject(array(
                'Bucket' => $bucket,
                'Key' => $s3dir
            ));

            $result = $s3->deleteObject(array(
                'Bucket' => $bucket,
                'Key' => $s3dir1
            ));
            
             $query = "delete from `image_management` where id=" . $rec_id;
             $command = Yii::app()->db->createCommand($query);
             $command->execute();
            echo 1;
        } else {
            echo 0;
           // Yii::app()->user->setFlash('error', 'Image already in use');
           // $this->redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function actionUploadAllImage() {
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        /* create object of model classs where need to be updated */
        $imagemanagement = new ImageManagement();
        $new_cdn_user = Yii::app()->user->new_cdn_users; //0-old user,1-new user
        $isEnableKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
        $columnName = $columnValue = '';
        if($isEnableKey && $isEnableKey->config_value==1){
            $chkExist = ImageManagement::model()->isExistImageKey();
            if($chkExist['success']==0){
                Yii::app()->user->setFlash('error', $chkExist['message']);
                $this->redirect($this->createUrl('management/ManageImage')); exit;
            }
            $columnName = ",image_key";
            $columnValue = ",'".(isset($_REQUEST['image_key'])?$_REQUEST['image_key']:'')."'";
        }
        if (!$_FILES['Filedata']['error']) {
            
            
            $file_info=pathinfo ($_FILES['Filedata']['name']);
            
            $_FILES['Filedata']['name'] = $file_info['filename']."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_info['extension'];
             $studio_dir = $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery/' . $studio_id;
            $thumb_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery/' . $studio_id . "/thumb";
           
            mkdir($studio_dir, 0777);
            mkdir($thumb_dir, 0777);

            $upload_file_name = $name = Yii::app()->common->fileName($_FILES["Filedata"]["name"]);
            $all_details = $imagemanagement->get_imagedetails_by_name($name, $studio_id);
            if (count($all_details)) {
                $strtime = strtotime(date("Y-m-d H:i:s"));
                $name = $strtime . "_" . $name;
                $_FILES["Filedata"]["name"] = $name;
            }
            
            $type=$file_info['extension'];

            if ($type == "jpg" || $type == "jpeg" || $type == "JPG" || $type == "JPEG" || $type == "PNG" || $type == "png" || $type == "gif") {
                if (file_exists($studio_dir)) {
                    unlink($studio_dir);
                }
                //echo "test";
                move_uploaded_file($_FILES['Filedata']['tmp_name'], $studio_dir . '/' . $name);
            } else {

                Yii::app()->user->setFlash('error', 'Please Upload valid image types(JPG/JPEG/PNG/GIF)');
                $url = $this->createUrl('management/ManageImage');
                $this->redirect($url);
            }

            $allimagepath = $studio_dir . '/' . $name;
            $crop_image_size = filesize();
            $crop_size_mb = round(($crop_image_size / 1048576), 3);
        }
        $bucketInfo = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        //echo $bucketName;
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery'); //path for images uploaded exclude studio
        $config->setBucketName($bucketName);
        $s3_config = Yii::app()->common->getS3Details(Yii::app()->user->studio_id);
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  /* maximum paralell uploads */
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); /* allowed extensions */
        //$config->setDimensions($cropDimension);   /*resize to these sizes*/
        /* usage of uploader class - this simple :) */
        $uploader = new Uploader($studio_id);
        $ret = $uploader->uploadManagedImages($_FILES['Filedata'], $name, $new_cdn_user);
        //  print_r($ret);
        $original = explode('imagegallery', $ret['original']);
        $thumb = explode('imagegallery', $ret['thumb']);
        $ret['original'] = "imagegallery" . $original[1];
        $ret['thumb'] = "imagegallery" . $thumb[1];
        
        
        $sql = "INSERT INTO image_management (image_size,image_name,s3_thumb_name,s3_original_name,studio_id,user_id,creation_date $columnName) VALUES (" . $crop_size_mb . ",'" . trim($upload_file_name) . "','" . trim($ret['thumb']) . "','" . trim($ret['original']) . "'," . $studio_id . "," . $user_id . ",'" . date('Y-m-d H:i:s') . "'$columnValue)";
        Yii::app()->db->createCommand($sql)->execute();

        Yii::app()->user->setFlash('success', 'Image uploaded successfully.');

        $url = $this->createUrl('management/ManageImage');
        $this->redirect($url);
        exit();
    }

    
    
    public function actionS3mulatipartupload() {
        $studio = $this->studio;
        $studio_id = $studio->id;
		$actions = array('abortmultipartupload', 'completemultipartupload', 'signuploadpart', 'createmultipartupload');
		$command = strtolower($_REQUEST['command']);
		$userid = Yii::app()->user->id;
		$bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
		$folderPath = Yii::app()->common->getFolderPath("", Yii::app()->user->studio_id);
		$signedBucketPath = $folderPath['signedFolderPath'];
		$dest_bucket = $bucketInfo['bucket_name'];
		$region = $bucketInfo['region_code'];
		$s3url = $bucketInfo['s3url'];
		$s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . Yii::app()->user->studio_id . "/";


		if (in_array($command, $actions)) {
			if (isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType'] == 'videogallery') {
				$region = '';
			}

			$s3multi = new S3multipart();
			$arr = $s3multi->$command($dest_bucket, $region, $s3url);
			//$key_array=explode('/',$arr['key']);


			if (@$arr['success'] && $command == 'completemultipartupload') {
				$key_array=explode('/',$_REQUEST['sendBackData']['key']);
				  $file_name=array_pop($key_array); 
				/*
				 * Avi Modified 20-01-2017
				 * audiogallery part
				 */
				if (isset($key_array) && in_array('audiogallery', $key_array)) {
				   $filePath = 'http://' . $dest_bucket . '.' . $s3url . '/' . $s3dir;
				   Yii::app()->general->updateAudioInfo($filePath, $file_name, $studio_id, '', $userid);
			   }else if (isset($_REQUEST['uploadtype']) && $_REQUEST['uploadtype'] == 'filegallery') {
				   Yii::app()->general->updateFileInfo($file_name, $studio_id, '', $userid);
			   } else {
				   Yii::app()->general->updateVideoInfo($file_name, $studio_id, '', $userid);
			   }
			}
		} else {
			$arr = array('error' => 'Requested Command is Invalid');
		}
		header('Content-Type: application/json');
		die(json_encode($arr));
    }
    
    public function actionDeleteVideo() {
        $rec_id = $_REQUEST['rec_id'];
        $studio_id = Yii::app()->user->studio_id;
        $video_details=VideoManagement::model()->get_videodetails_by_id_studio($rec_id,$studio_id);
        if($video_details){
            $video_name=$video_details[0]['video_name'];
            $thumb_image_name=$video_details[0]['thumb_image_name'];
            $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
            $s3 =  Yii::app()->common->connectToAwsS3($studio_id);
            if(Yii::app()->user->new_cdn_users)//existing user
            {
                 $s3dir = $unsignedFolderPathForVideo.'videogallery/'.$video_name;
            }
            else
            {
                $s3dir = $unsignedFolderPathForVideo.'videogallery/'.$studio_id.'/'.$video_name;
            }

            if (Yii::app()->user->new_cdn_users) {
                $s3dir1 = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $thumb_image_name;
            } else {
                $s3dir1 = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/videogallery-image/' . $thumb_image_name;
            }

            $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
            $bucket = $bucketInfo['bucket_name'];
            if($video_name){
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir
                ));
            }
            if($thumb_image_name){
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir1
                ));
            }
            $query = "delete from `video_management` where id=".$rec_id;
            $command = Yii::app()->db->createCommand($query);
            $command->execute();
            Yii::app()->user->setFlash('success', 'Video deleted successfully.');

        }
        else{
            Yii::app()->user->setFlash('error', 'Video could not be deleted.');
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function actionValidateVideo() {
        $url = @$_REQUEST['url'];
        $studio_id = Yii::app()->user->studio_id;
        $video_name=@$_REQUEST['video_name'];
        if ($url) {
            $url = str_replace(' ', '%20', $url);
            $_REQUEST['url'] = $url;
            if((isset($_REQUEST['username']) && trim($_REQUEST['username'])!='') && (isset($_REQUEST['password']) && trim($_REQUEST['password'])!='')){
                $auth['access_username'] = $_REQUEST['username'];
                $auth['access_password'] = $_REQUEST['password'];
            }
            /*$tModel = new ThirdpartyServerAccess();
            $authData = $tModel->find('studio_id =:studio_id AND is_authorised = 1', array(':studio_id' => $studio_id));

            if ($authData) {
                $auth = $authData->attributes;
            }*/
            $arr = $this->is_url_exist($url, @$auth);

            if (@$arr['error'] != 1 && @$arr['is_video']) {
                $data = $_REQUEST;
                $data['content_type'] = $arr['is_video'];
                //$movieStream = new movieStreams();
                //$ret = $movieStream->updateByPk($_REQUEST['movie_stream_id'],array('full_movie_url'=>$_REQUEST['url']));
                $vidoInfo = new SplFileInfo($url);
                $videoExt = $vidoInfo->getExtension();
                $videoOnlyName = $vidoInfo->getBasename('.'.$vidoInfo->getExtension());
                $videoOnlyName = Yii::app()->common->fileName($videoOnlyName);
                $video_name = $videoOnlyName.'.'.$videoExt;
                $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $video_name . "' and flag_deleted=0";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                //$check_video_availability=$this->checkvideo($_REQUEST['fileInfo']['name']);
                $check_video_availability = count($res);
                if ($check_video_availability > 0) {
                    $video_name = $videoOnlyName.strtotime(date("d-m-Y H:i:s")).'.'.$videoExt;
                }
                //pass url to the below function
                Yii::app()->aws->createUploadVideoSH($data, @$auth, $video_name);
            }
        }

        echo json_encode($arr);
        exit;
    }
    
    
    
    public function actionValidateVideoForReelAfrican() {
       $this->layout = false;
        $fileList = array();
        //$fileList[]['url']='http://52.5.45.62/reelafrica/41.160.45.74/ReelAfrican/DS-0033-WORLD_OF_MYSTERIES-033-CURE.mpg';
        

        foreach($fileList as $urlArrayKey => $urlArrayVal){
            $_REQUEST['url'] = $urlArrayVal['url'];
            $url = $urlArrayVal['url'];
            $studio_id = Yii::app()->user->studio_id;
            if ($url) {
                $url = str_replace(' ', '%20', $url);
                $_REQUEST['url'] = $url;
                $arr = $this->is_url_exist($url);

                if (@$arr['error'] != 1 && @$arr['is_video']) {
                    $vidoInfo = new SplFileInfo($url);
                    $videoExt = $vidoInfo->getExtension();
                    $videoOnlyName = $vidoInfo->getBasename('.'.$vidoInfo->getExtension());
                    $videoOnlyName = Yii::app()->common->fileName($videoOnlyName);
                    $video_name = $videoOnlyName.'.'.$videoExt;
                    $_REQUEST['video_name'] = $video_name;
                    $data = $_REQUEST;
                    $data['content_type'] = $arr['is_video'];
                    
                    $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $video_name . "' and flag_deleted=0";
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    //$check_video_availability=$this->checkvideo($_REQUEST['fileInfo']['name']);
                    $check_video_availability = count($res);
                    if ($check_video_availability > 0) {
                        $video_name = $videoOnlyName.strtotime(date("d-m-Y H:i:s")).'.'.$videoExt;
                    }
                    //pass url to the below function
                    Yii::app()->aws->createUploadVideoSH($data, @$auth, $video_name, $url);
                }
            }
            $urlArray[$urlArrayKey]['response'] =  $arr;
        }
        echo "<pre>";
        print_r($urlArray);
        exit;
    }
    
    /* 
     * Add video from MRSS Feed
     */
    public function actionAddVideoFromMrssFeed(){
        $studio_id = Yii::app()->user->studio_id;
        if(isset($_REQUEST['id']) && intval($_REQUEST['id'])){
            $mrssfeedDetails = MrssFeed::model()->findbyPk($_REQUEST['id']);
            if($mrssfeedDetails){
                $feedData = json_decode($mrssfeedDetails->data,true);
                if(!empty($feedData)){
                    if(isset($feedData['content']['url'])){
                        $url = $feedData['content']['url'];
                        $url = str_replace(' ', '%20', $url);
                        $tModel = new ThirdpartyServerAccess();
                        $arr = $this->is_url_exist($url);
                        if (@$arr['error'] != 1 && @$arr['is_video']) {
                            $data['url'] = $url;
                            $data['content_type'] = $arr['is_video'];
                            //$movieStream = new movieStreams();
                            //$ret = $movieStream->updateByPk($_REQUEST['movie_stream_id'],array('full_movie_url'=>$_REQUEST['url']));
                            $vidoInfo = new SplFileInfo($url);
                            $videoExt = $vidoInfo->getExtension();
                            $videoOnlyName = $vidoInfo->getBasename('.'.$vidoInfo->getExtension());
                            $video_name = $videoOnlyName.'.'.$videoExt;
                            $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $video_name . "' and flag_deleted=0";
                            $res = Yii::app()->db->createCommand($sql)->queryAll();
                            //$check_video_availability=$this->checkvideo($_REQUEST['fileInfo']['name']);
                            $check_video_availability = count($res);
                            if ($check_video_availability > 0) {
                                $video_name = $videoOnlyName.strtotime(date("d-m-Y H:i:s")).'.'.$videoExt;
                            }
                            $mrssfeedDetails->is_video_gallery=1;
                            $mrssfeedDetails->save();
                            //pass url to the below function
                            Yii::app()->aws->createUploadVideoSH($data, "", $video_name);
                        }
                    }else{
                        $arr['error']=1;
                    }
                }else{
                    $arr['error']=1;
                }
            }else{
                $arr['error']=1;
            }
        }else{
            $arr['error']=1;
        }
        echo json_encode($arr);
        exit;
    }

    function actionvideoSync(){
        $this->layout = FALSE;
        $studioId = Yii::app()->common->getStudioId();
        $s3 = Yii::app()->common->connectToAwsS3($studioId);
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studioId);
        $bucket = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $response = $s3->getListObjectsIterator(array(
                                          'Bucket' => $bucket,
                                          'Prefix' => 'MuviSync/'
                                  ));
        $i =0;
        $folderPath = Yii::app()->common->getFolderPath("",$studioId);
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $new_cdn_user=Yii::app()->user->new_cdn_users;
        foreach ($response as $object) {
            $tModel = new ThirdpartyServerAccess();
            $url = str_replace(' ', '%20','http://'.$bucket.'.'.$s3url.'/'.$object['Key']);
            $arr = $this->is_url_exist($url);
            if (@$arr['error'] != 1 && @$arr['is_video']) {
                $filenameDetail = explode("MuviSync/",$object['Key']);
                if(isset($filenameDetail[1]) && $filenameDetail[1] != ''){
                    $fileName = Yii::app()->common->fileName($filenameDetail[1]);
                    $videoManagement = VideoManagement::model()->findByAttributes(array('studio_id'=> $studioId,'video_name' => $fileName));
                    if($videoManagement){
                        $file_name_upload =$fileName;
                        $file_name_list=explode('.',$file_name_upload);
                        $fileName = $file_name_list[0]."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_name_list[1];
                    }
                    $key = $unsignedFolderPathForVideo.'videogallery/'.$fileName;  
                    if($new_cdn_user==0)//existing user
                    {
                        $key = $unsignedFolderPathForVideo.'videogallery/'.$studio_id.'/'.$fileName;
                    }
                    try{
                        $response = $s3->copyObject(array(
                            'Bucket'     => $bucket,
                            'Key'        => $key,
                            'CopySource' => "{$bucket}/{$object['Key']}",
                            'ACL' => 'public-read',
                        ));
                        if($response->get('ETag') != ''){
                            Yii::app()->general->updateVideoInfo($fileName,$studioId,1);
                            $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $object['Key']
                            ));
                            $i++;
                        }
                    } catch (\Aws\S3\Exception\S3Exception $e) {
                        //Exception ocurred, 
                        //"SignatureDoesNotMatch" for bad secret access key
                        //"InvalidAccessKeyId" for invalid access key id
                        Yii::app()->user->setFlash('error',$e->getMessage());
                    }
                }
            }
        }
        if($i != 0){
            Yii::app()->user->setFlash('success',$i.' videos are synced with Video Library. You can now map them to your content.');
        } else{
            Yii::app()->user->setFlash('error','Either no videos found in the said folder or the video file size is more than 5 GB.');
        }
    }
     function actionaudioSync(){
        $this->layout = FALSE;
        $studioId = Yii::app()->common->getStudioId();
        $s3 = Yii::app()->common->connectToAwsS3($studioId);
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studioId);
        $bucket = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $response = $s3->getListObjectsIterator(array(
                                          'Bucket' => $bucket,
                                          'Prefix' => 'MuviSync/'
                                  ));
        $i =0;
        $folderPath = Yii::app()->common->getFolderPath("",$studioId);
        $unsignedFolderPathForAudio = $folderPath['unsignedFolderPathForAudio'];
        $new_cdn_user=Yii::app()->user->new_cdn_users;
        foreach ($response as $object) {
            $tModel = new ThirdpartyServerAccess();
            $url = str_replace(' ', '%20','http://'.$bucket.'.'.$s3url.'/'.$object['Key']);
            $arr = $this->is_url_exist($url);
            if (@$arr['error'] != 1 && @$arr['is_video']) {
                $filenameDetail = explode("MuviSync/",$object['Key']);
                if(isset($filenameDetail[1]) && $filenameDetail[1] != ''){
                    $fileName = Yii::app()->common->fileName($filenameDetail[1]);
                    $videoManagement = VideoManagement::model()->findByAttributes(array('studio_id'=> $studioId,'video_name' => $fileName));
                    if($videoManagement){
                        $file_name_upload =$fileName;
                        $file_name_list=explode('.',$file_name_upload);
                        $fileName = $file_name_list[0]."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_name_list[1];
                    }
                    $key = $unsignedFolderPathForAudio.'audiogallery/'.$fileName;  
                    if($new_cdn_user==0)//existing user
                    {
                        $key = $unsignedFolderPathForAudio.'audiogallery/'.$studio_id.'/'.$fileName;
                    }
                    try{
                        $response = $s3->copyObject(array(
                            'Bucket'     => $bucket,
                            'Key'        => $key,
                            'CopySource' => "{$bucket}/{$object['Key']}",
                            'ACL' => 'public-read',
                        ));
                        if($response->get('ETag') != ''){
                            Yii::app()->general->updateVideoInfo($fileName,$studioId,1);
                            $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $object['Key']
                            ));
                            $i++;
                        }
                    } catch (\Aws\S3\Exception\S3Exception $e) {
                        //Exception ocurred, 
                        //"SignatureDoesNotMatch" for bad secret access key
                        //"InvalidAccessKeyId" for invalid access key id
                        Yii::app()->user->setFlash('error',$e->getMessage());
                    }
                }
            }
        }
        if($i != 0){
            Yii::app()->user->setFlash('success',$i.' videos are synced with Video Library. You can now map them to your content.');
        } else{
            Yii::app()->user->setFlash('error','Either no videos found in the said folder or the video file size is more than 5 GB.');
        }
    }
    public function actionmanagePlayer(){
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('Player');
        $this->pageTitle = "Muvi | Player";
        
        $addWatermarkConfig = StudioConfig::model()->getconfigvalue('embed_watermark');
        $selectWatermarkOption = WatermarkPlayer::model()->getStudioId($studio_id);
        $embedUrlRestrictionConfig = StudioConfig::model()->getconfigvalue('embed_url_restriction');
        $playerLogoRestrictionConfig = StudioConfig::model()->getconfigvalue('player_logo_restriction');
        $embedUrlRestriction = EmbedUrlRestriction::model()->findAllByAttributes(array('studio_id' => Yii::app()->user->studio_id));
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $this->render('manageplayer', array('all_images' => $all_images, 'playerLogoRestrictionConfig' => $playerLogoRestrictionConfig,'embedUrlRestrictionConfig' => $embedUrlRestrictionConfig,'embedUrlRestriction' => $embedUrlRestriction,'addWatermarkConfig' => $addWatermarkConfig,'selectWatermarkOption' => $selectWatermarkOption));
    }
    
    public function actionenableEmbedUrlRestiction(){
        $this->layout = false;
        if(@$_REQUEST['enableRestriction'] != ''){
            $embedUrlRestriction = StudioConfig::model()->findByAttributes(array('studio_id' => Yii::app()->user->studio_id,'config_key' => 'embed_url_restriction'));
            if($embedUrlRestriction){
                $embedUrlRestriction -> config_value = $_REQUEST['enableRestriction'];
                $embedUrlRestriction -> save();
            } else{
                $embedUrl = new StudioConfig();
                $embedUrl -> studio_id = Yii::app()->user->studio_id;
                $embedUrl -> config_key = 'embed_url_restriction';
                $embedUrl -> config_value = $_REQUEST['enableRestriction'];
                $embedUrl -> save();
            }
            if($_REQUEST['enableRestriction'] == 1){
                echo 1;
            } else{
                echo 0;
            }
        } else{
            echo 0;
        }
    }
    
    public function actionaddWatermarkOnPlayer(){
        //echo $a = $_REQUEST['addWatermark']; exit;
        if(@$_REQUEST['addWatermark'] != ''){
            $studio_id = Yii::app()->common->getStudiosId();
            $getStudioConfig = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id,'config_key' => 'embed_watermark'));

            if($getStudioConfig){
                    $getStudioConfig -> config_value = $_REQUEST['addWatermark'];
                    $getStudioConfig -> save();
            } else{
                    $studioConfig = new StudioConfig();
                    $studioConfig -> studio_id = Yii::app()->user->studio_id;
                    $studioConfig -> config_key = 'embed_watermark';
                    $studioConfig -> config_value = $_REQUEST['addWatermark'];
                    $studioConfig -> save();
            }    

            
        }
       if($_REQUEST['addWatermark'] == 1){
                echo 1;
       } else{
                echo 0;
            }
       
    
    }
    
    public function actionwatermarkOption(){
        $studio_id = Yii::app()->common->getStudiosId();
        $getStudio = WatermarkPlayer::model()->findByAttributes(array('studio_id' => $studio_id));
        
            if($getStudio){
                    $getStudio -> email = $_REQUEST['wm_emailAddress'];
                    $getStudio -> ip = $_REQUEST['wm_ipAddress'];
                    $getStudio -> date = $_REQUEST['wm_date'];
                    $getStudio -> save(); 
            } else{
                    $watermarkType = new WatermarkPlayer();
                    $watermarkType -> studio_id = Yii::app()->user->studio_id;
                    $watermarkType -> email = $_REQUEST['wm_emailAddress'];
                    $watermarkType -> ip = $_REQUEST['wm_ipAddress'];
                    $watermarkType -> date = $_REQUEST['wm_date'];
                    $watermarkType -> save();
            }    
            
    }
    
    public function actionenablePlayerLogo(){
        $this->layout = false;
        if(@$_REQUEST['enableRestriction'] != ''){
            $embedUrlRestriction = StudioConfig::model()->findByAttributes(array('studio_id' => Yii::app()->user->studio_id,'config_key' => 'player_logo_restriction'));
            if($embedUrlRestriction){
                $embedUrlRestriction -> config_value = $_REQUEST['enableRestriction'];
                $embedUrlRestriction -> save();
            } else{
                $embedUrl = new StudioConfig();
                $embedUrl -> studio_id = Yii::app()->user->studio_id;
                $embedUrl -> config_key = 'player_logo_restriction';
                $embedUrl -> config_value = $_REQUEST['enableRestriction'];
                $embedUrl -> save();
            }
            if($_REQUEST['enableRestriction'] == 1){
                echo 1;
            } else{
                echo 0;
            }
        } else{
            echo 0;
        }
    }
    
    public function actionembedUrlRestiction(){
        $this->layout = false;
        if(@$_REQUEST['embed_url_restriction'] != ''){
            $embedUrl = new EmbedUrlRestriction();
            $embedUrl -> studio_id = Yii::app()->user->studio_id;
            $embedUrl -> website_url = $_REQUEST['embed_url_restriction'];
            $embedUrl -> created_date = date('Y-m-d H:i:s');
            $embedUrl -> save();
            Yii::app()->user->setFlash('success','Website url is saved.');
        } else{
            Yii::app()->user->setFlash('error','Please provide Website Url');
        }
        $url = $this->createUrl('management/managePlayer');
        $this->redirect($url);
    }
    public function actionremoveWebsiteUrlForEmbed() {
        $this->layout = false;
        if(@$_REQUEST['embUrl_id'] != ''){
            $embedUrlRestriction = EmbedUrlRestriction::model()->findByAttributes(array('studio_id' => Yii::app()->user->studio_id,'id' => $_REQUEST['embUrl_id']));
            if($embedUrlRestriction){
                $embedUrlRestriction->delete();
                Yii::app()->user->setFlash('success','Website url deleted successfully.');
            } else{
                Yii::app()->user->setFlash('error','You are not accesed to delete the Website url.');
            }
        } else{
            Yii::app()->user->setFlash('error','You are not accesed to delete the Website url.');
        }
        $url = $this->createUrl('management/managePlayer');
        $this->redirect($url);
    }
    public function actioncheckSubtitleForVideo() {
        $this->layout = false;
        $studio_id = Yii::app()->user->studio_id;
        if(isset($_REQUEST['video_id']) &&  $_REQUEST['video_id'] != ''){
            $video_id = $_REQUEST['video_id'];
            $sql = "select vs.id as subId, vs.*,ls.* from video_subtitle vs,language_subtitle ls where vs.language_subtitle_id = ls.id and vs.video_gallery_id=" . $video_id ." and vs.studio_id = ".$studio_id;
            $getVideoSubtitle = Yii::app()->db->createCommand($sql)->queryAll();
            if($getVideoSubtitle){
                $subTitleExist = '';
                foreach ($getVideoSubtitle as $getVideoSubtitleKey => $getVideoSubtitleVal) {
                    $subTitleExist .= $getVideoSubtitleVal['id'].",";
                }
                $subTitleExist = rtrim($subTitleExist,',');
                $sql1 = "select * from language_subtitle where id NOT IN (" . $subTitleExist . ")";
                $getSubLang = Yii::app()->db->createCommand($sql1)->queryAll();
            } else{
                $getSubLang = LanguageSubtitle::model()->findAll();
            }
            $this->render('check_subtitle_for_Video',array('video_id' => $video_id,'getSubLang'=>$getSubLang,'getVideoSubtitle' => $getVideoSubtitle));  
        }
    }
    public function actionaddSubtitle(){
        require_once "srt2vtt/srtfile.php";
        //This class is used to convert dfxp to .srt and also .vtt format 
        require_once "dfxp.php";
        $this->layout = false;
        $studio_id = Yii::app()->user->studio_id;
        if(isset($_REQUEST['subVideoId']) && $_REQUEST['subVideoId'] != ''){
            $videoId = $_REQUEST['subVideoId'];
            $subtitleFiles = $_FILES['subtitle_language'];
            $i = 0;
            foreach ($_REQUEST['data']['subtitle_language'] as $key => $value) {
                if($_FILES['data']['name']['subtitle_file'][$key] != ''){
                    $filename  = Yii::app()->common->fileName($_FILES['data']['name']['subtitle_file'][$key]);
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $SourceFile = $_FILES['data']['tmp_name']['subtitle_file'][$key];
                    $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                    $signedFolderPath = $folderPath['signedFolderPath'];
                    $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                    $bucket = $bucketInfo['bucket_name'];
                    $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                    
                    $videoSubTit = new VideoSubtitle();
                    $videoSubTit -> save();
                    $subId = $videoSubTit->id;
                    
                    $directory = $_SERVER['DOCUMENT_ROOT']. "/temp_srt/";
                    if(!is_dir($directory)) {
                     mkdir($directory,true);
                     chmod($directory, 0775);
                    }
                    if($ext == 'srt'){
                        $srt = new \SrtParser\srtFile($SourceFile);
                        $srt->setWebVTT(true);
                        $srt->build(true);
                        $srt->save("temp_srt/".$subId.".vtt" , true);
                        $SourceFile = "temp_srt/".$subId.".vtt";
                        $filename = str_replace(".srt",".vtt",$filename);
                        if (file_exists($SourceFile)) {
                            $result = $s3->putObject(array(
                                'Bucket' => $bucket,
                                'Key' => $signedFolderPath."subtitle/".$subId."/".$filename,
                                'SourceFile' => "temp_srt/".$subId.".vtt",
                                'ACL' => 'public-read',
                            )); 
                            @unlink("temp_srt/".$subId.".vtt");   
                        } 
                    }else if($ext == 'dfxp'){                        
                        $dfxp=new dfxpFile($SourceFile);
                        $dfxp->buildVtt();
                        $dfxp->save("temp_srt/" . $subId . ".vtt");
                        $SourceFile = "temp_srt/" . $subId . ".vtt";
                        $filename = str_replace(".dfxp", ".vtt", $filename);
                        if (file_exists($SourceFile)) {
                         $result = $s3->putObject(array(
                            'Bucket' => $bucket,
                                'Key' => $signedFolderPath . "subtitle/" . $subId . "/" . $filename,
                                'SourceFile' => "temp_srt/" . $subId . ".vtt",
                                'ACL' => 'public-read',
                            ));
                            @unlink("temp_srt/" . $subId . ".vtt");
                        }
                    }else{
                         $result = $s3->putObject(array(
                            'Bucket' => $bucket,
                            'Key' => $signedFolderPath."subtitle/".$subId."/".$filename,
                            'SourceFile' => $_FILES['data']['tmp_name']['subtitle_file'][$key],
                            'ACL' => 'public-read',
                         ));   
                    }
                    if(isset($result['ObjectURL']) && $result['ObjectURL'] != '')
                    {
                        $videoSubTit -> studio_id = $studio_id;
                        $videoSubTit -> video_gallery_id = $videoId;
                        $videoSubTit -> language_subtitle_id = $value;
                        $videoSubTit -> filename = $filename;
                        $videoSubTit -> save();
                        $i ++;
                    } else{
                        $videoSubTit -> delete();
                    }
                }
            }
            if($i != 0){
                Yii::app()->user->setFlash('success','Subtitle save successfully.');
            } else{
                Yii::app()->user->setFlash('error','Subtitle is not added');
            }
        }
        $url = $this->createUrl('management/managevideo');
        $this->redirect($url);
    }

    public function actiondeleteSubtitle(){
        $this->layout = false;
        $studio_id = Yii::app()->user->studio_id;
        if(isset($_REQUEST['subtitleId']) && $_REQUEST['subtitleId'] != ''){
            $subtitleData = VideoSubtitle::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['subtitleId']));
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $signedFolderPath = $folderPath['signedFolderPath'];
            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $result = $s3->deleteObject(array(
                'Bucket' => $bucket,
                'Key' => $signedFolderPath."subtitle/".$subtitleData->id."/".$subtitleData->filename
            ));
            $subtitleData->delete();
            Yii::app()->user->setFlash('success','Subtitle deleted successfully.');
        } else{
            Yii::app()->user->setFlash('error','You are not accesed to delete the Subtitle.');
        }
        $url = $this->createUrl('management/managevideo');
        $this->redirect($url);
    }
    public function actionTranslation(){
        $this->breadcrumbs  = array('Translation');
        $this->pageTitle    = "Muvi | Translation";
        $this->layout       = 'admin';
        $languages          = include(ROOT_DIR . 'languages/studio/en.php');
        $total_words_encode = count($languages);
        $enable_lang        = $this->language_code;
        $enable_lang_name   = Language::model()->findByAttributes(array('code'=>$enable_lang));
        $enable_lang_name   = $enable_lang_name['name'];
        $studio_id          = Yii::app()->user->studio_id;
        $studio_details     = Studio::model()->findbyPk($studio_id);
        $theme              = $studio_details['theme'];
        $name               = $studio_details['name'];
        $main_language_path = ROOT_DIR.'languages/studio/'.$enable_lang.'.php';
        $path               = ROOT_DIR."languages/".$theme;
        $file_path          = $path."/".$enable_lang.".php";
        $language_frm_theme = include($file_path);
        if(!is_dir($path)) {
           mkdir($path,0777);
        }
        if($_POST){
            $translated_words       = array();
            $only_translated_words  = array();
            foreach($_POST as $key => $value){
                $translated_words[$key] = $value;
            }
            if(file_exists($file_path)){
                $temp_file = fopen($path."/temp.php", "w+");
                copy($file_path, $path."/temp.php" );
            }
            $file = fopen($file_path, "w+");
            file_put_contents($file_path, '<?php return ' . var_export($translated_words, true) . ';');
            $log_path           = $path."/log.php";
            $current_file       = $enable_lang.".php";
            $logfile    = $this->logupdate($log_path,$path,$current_file,$name);
            $language_frm_theme = include($file_path);
        }
        $this->render('translation',array('languages'=>$languages,'language_from_theme'=>$language_frm_theme,'select_lang'=>$enable_lang_name,'total_words_encode'=>$total_words_encode ));
    }
    public function logupdate($log_path,$path,$filename,$name,$action=false){
        $add_to_logfile  = array();
        $file_path = $path."/".$filename;
        if($action){
            $status = "deleted";
        }
        $temp_path = $path."/temp.php";
        $temp_file_content = include($temp_path);
        if(empty($temp_file_content)){
            $status  = "added";
        }else{
            $temp    = md5_file($temp_path);
            $current = md5_file($file_path);
            if($temp == $current){
                return true;
            }else{
                $temp_content = array();;
                $temp_file = fopen($temp_path, "w+");
                file_put_contents($temp_path, '<?php return ' . var_export($temp_content, true) . ';');
                $status = "updated";
            }
        }
        $date = date('Y-m-d h:i:s');
        $email = Yii::app()->user->email;
        $add_to_logfile = $name."(".$email.") ".$status." ".$filename." on ". $date." GMT \n";
        $file = fopen($log_path, "a");
        fwrite($file, $add_to_logfile);
        fclose($file);
    }
    public function actionDeletelangugefile(){
        $language           = $_POST['language_code'];
        $studio_id          = Yii::app()->user->studio_id;
        $studio_details     = Studio::model()->findbyPk($studio_id);
        $theme              = $studio_details['theme'];
        $name               = $studio_details['name'];
        $path               = ROOT_DIR."languages/".$theme."/";
        $file_path          = $path."/".$language.".php";
        $log_path           = $path."/log.php";
        $current_file       = $language.".php";
        $logfile            = $this->logupdate($log_path,$path,$current_file,$name,"Delete");
        if(file_exists($file_path)){
            if(unlink($file_path)){
                echo "This language file deleted from your theme";
            }
        }else{
            echo "File not exist";
        }
    }
    public function actionsavePlayerlogo() {
        $studio_id = Yii::app()->common->getStudiosId();
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        $theme_folder = $std->theme;
        $parent_theme = $std->parent_theme;
        $user_id = Yii::app()->user->id;
        if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucketName = $bucketInfo['bucket_name'];
            $client = Yii::app()->common->connectToAwsS3($studio_id);
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $logo_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $theme_folder;
            $logo_pathFullPath = $logo_path . '/playerLogo';
            $playerLogoPath = $unsignedFolderPath . 'public/' . $studio_id . '/playerlogos';
            $playerLogo = StudioConfig::model()->getconfigvalue('player_logo');
            if ($playerLogo['config_value'] != '') {
                $playerLogoUrl = $playerLogoPath."/". $playerLogo['config_value'];
                $result = $client->deleteObject(array(
                    'Bucket' => $bucketName,
                    'Key' => $playerLogoUrl
                ));
            }
            if (!is_dir($logo_path)) {
                mkdir($logo_path, 0777);
            }
            if (!is_dir($logo_pathFullPath)) {
                mkdir($logo_pathFullPath, 0777);
            }
            
            if (isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') {
                $file_info = pathinfo($_FILES['Filedata']['name']);
                $extension = array('PNG', 'png', 'JPG', 'jpeg', 'JPEG', 'jpg');
                if (!in_array($file_info['extension'], $extension)) {
                    Yii::app()->user->setFlash('error', 'Please upload valid files formats(png,jpg,jpeg).');
                    $url = $this->createUrl('/management/managePlayer');
                    $this->redirect($url);
                    exit;
                }

                if ($_POST['w'] == '' && $_POST['h'] == '') {
                    Yii::app()->user->setFlash('error', 'Please crop image to upload.');
                    $url = $this->createUrl('/management/managePlayer');
                    $this->redirect($url);
                    exit;
                }
                
                $_FILES['Filedata']['name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
                $logoName = Yii::app()->common->fileName($_FILES["Filedata"]["name"]);
                $sTempFileName = $logo_pathFullPath ."/". $logoName;
                $copied_image = $original_path . $logoName;
                $dimension['x1'] = $_REQUEST['x1'];
                $dimension['y1'] = $_REQUEST['y1'];
                $dimension['x2'] = $_REQUEST['x2'];
                $dimension['y2'] = $_REQUEST['y2'];
                $dimension['w'] = $_REQUEST['w'];
                $dimension['h'] = $_REQUEST['h'];
                $all_image_path = Yii::app()->common->jcroplibraryImage($logoName, $_FILES['Filedata']['tmp_name'], $logo_pathFullPath, $dimension);
            }
            /* Image choosed from Image Gallery  */ 
            else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {
                $file_info = pathinfo($_REQUEST['g_image_file_name']);
                $_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
                $jcrop_allimage = $_REQUEST['jcrop_allimage'];
                $dimension['x1'] = $jcrop_allimage['x13'];
                $dimension['y1'] = $jcrop_allimage['y13'];
                $dimension['x2'] = $jcrop_allimage['x23'];
                $dimension['y2'] = $jcrop_allimage['y23'];
                $dimension['w'] = $jcrop_allimage['w3'];
                $dimension['h'] = $jcrop_allimage['h3'];
                if ($dimension['w'] == '' && $dimension['h'] == '') {
                    Yii::app()->user->setFlash('error', 'Please crop image to upload.');
                    $url = $this->createUrl('/management/managePlayer');
                    $this->redirect($url);
                    exit;
                }
                $all_image_path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $logo_pathFullPath, $dimension);
                $logoName = $_REQUEST['g_image_file_name'];
            }
            $source_path =  $logo_pathFullPath."/original/".$logoName;
            $originalpath =  $logo_pathFullPath."/jcrop/".$logoName;
            $acl = 'public-read';
            $result = $client->putObject(array(
                'Bucket' => $bucketName,
                'Key' => $playerLogoPath.'/'.$logoName,
                'SourceFile' => $source_path,
                'ACL' => $acl,
            ));
            chmod($source_path, 0775);
            chmod($originalpath, 0775);
            $playerLogoSave = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id,'config_key' => 'player_logo'));
            if($playerLogoSave){
                $playerLogoSave -> config_value = $logoName;
                $playerLogoSave -> save();
            } else{
                $playerLogoSave = new StudioConfig();
                $playerLogoSave -> studio_id = $studio_id;
                $playerLogoSave -> config_key = 'player_logo';
                $playerLogoSave -> config_value = $logoName;
                $playerLogoSave -> save();
            }
            if (isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') {
                copy($originalpath,$source_path);
                require_once "Image.class.php";
                require_once "Config.class.php";
                require_once "Uploader.class.php";
                spl_autoload_unregister(array('YiiBase', 'autoload'));
                require_once "amazon_sdk/sdk.class.php";
                spl_autoload_register(array('YiiBase', 'autoload'));
                defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
                $config = Config::getInstance();
                $config->setUploadDir($logo_pathFullPath); //path for images uploaded exclude studio
                $config->setBucketName($bucketName);
                $config->setAmount(250);  /* maximum paralell uploads */
                $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); /* allowed extensions */
                /* usage of uploader class - this simple :) */
                $uploader = new Uploader($studio_id);
                $ret = $uploader->uploadManagedPosterImages($_FILES['Filedata'], $logoName, $new_cdn_user, $logo_pathFullPath);
                @unlink($logo_pathFullPath."/thumb/".$logoName);
                $original = explode('imagegallery', $ret['original']);
                $thumb = explode('imagegallery', $ret['thumb']);
                $ret['original'] = "imagegallery" . $original[1];
                $ret['thumb'] = "imagegallery" . $thumb[1];
                $imageManagement = new ImageManagement();
                $imageManagement -> image_name = trim($logoName);
                $imageManagement -> s3_thumb_name = trim($ret['thumb']);
                $imageManagement -> s3_original_name = trim($ret['original']);
                $imageManagement -> studio_id = $studio_id;
                $imageManagement -> user_id = $user_id;
                $imageManagement -> creation_date = date('Y-m-d H:i:s');
                $imageManagement -> save();
            }
            @unlink($source_path);
            @unlink($sTempFileName);
            @unlink($originalpath);
            Yii::app()->user->setFlash('success', 'Player Logo saved successfully.');
            $url = $this->createUrl('/management/managePlayer');
            $this->redirect($url);
        } else {
            Yii::app()->user->setFlash('error', 'Please choose image to upload.');
            $url = $this->createUrl('/management/managePlayer');
            $this->redirect($url);
            exit;
        }
    }
   //code to get the mapped videos count ticket # 4535 By: SURAJA 
   public function actionCheckMapped() {
     
        if (count($_POST["id"])>1) {
            $sql = "SELECT * from movie_streams  WHERE video_management_id in";
            $sql.= "(" . implode(",", array_values($_POST['id'])) . ")";
            //echo $sql;
        }else
        {
           $sql = "SELECT * from movie_streams  WHERE video_management_id =".$_POST["id"][0];   
        }
            $command = Yii::app()->db->createCommand($sql);
            $list = $command->queryAll();
            //get the list of all values in  
            if (isset($list)) {
                $mapped_count = count($list);
                if ($mapped_count > 0) {
                    echo $mapped_count;
                } else {
                    echo 0;
                }
            }
        
    }

    //code to delete selected videos ticket # 4535 By:SURAJA
    public function actionDeleteSelected() {
        $studio_id = Yii::app()->user->studio_id;

        foreach ($_POST["id"] as $key => $val) {
            $video_details = VideoManagement::model()->get_videodetails_by_id_studio($val, $studio_id);
            if ($video_details) {
                $video_name = $video_details[0]['video_name'];
                $thumb_image_name = $video_details[0]['thumb_image_name'];
                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                if (Yii::app()->user->new_cdn_users) {//existing user
                    $s3dir = $unsignedFolderPathForVideo . 'videogallery/' . $video_name;
                } else {
                    $s3dir = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/' . $video_name;
                }

                if (Yii::app()->user->new_cdn_users) {
                    $s3dir1 = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $thumb_image_name;
                } else {
                    $s3dir1 = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/videogallery-image/' . $thumb_image_name;
                }

                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $bucket = $bucketInfo['bucket_name'];
                if ($video_name) {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir
                    ));
                }
                if ($thumb_image_name) {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir1
                    ));
                }
                $query = "delete from `video_management` where id=" . $val;
                $command = Yii::app()->db->createCommand($query);
                $command->execute();
            }
        }
        Yii::app()->user->setFlash('success', 'Video deleted successfully.');
    }
    
    public function actionDeleteSelectedImages() {
        
        $studio_id = Yii::app()->user->studio_id;
        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
     
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        
        foreach ($_POST["id"] as $key => $val) {
            $image_details = ImageManagement::model()->get_imagedetails_by_id_studio($val, $studio_id);
            
            if ($image_details) {
                $image_name = $image_details[0]['image_name'];
                $s3thumbdir = $unsignedBucketPath . $image_details[0]['s3_thumb_name'];
                $s3originaldir = $unsignedBucketPath . $image_details[0]['s3_original_name'];
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                $bucket = $bucketInfo['bucket_name'];
                
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3thumbdir
                ));

                $result1 = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3originaldir
                ));
            }
           
                $query = "delete from `image_management` where id=" . $val;
                $command = Yii::app()->db->createCommand($query);
                $command->execute();
            }
        
        //Yii::app()->user->setFlash('success', 'Image deleted successfully.');
    }

    public function actionUploadImageToLibrary() {
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
        $std = new Studio();
        $studio = $std->findByPk($studio_id);
        /* create object of model classs where need to be updated */
        $imagemanagement = new ImageManagement();
        $new_cdn_user = Yii::app()->user->new_cdn_users; //0-old user,1-new user
        //echo $new_cdn_user;
        //exit;
        if (!$_FILES['image_to_lib']['error']) {
            
            
            $file_info=pathinfo ($_FILES['image_to_lib']['name']);
            if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery/')){
                mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery/', 0777);
            }
            $_FILES['image_to_lib']['name'] = $file_info['filename']."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_info['extension'];
            $studio_dir = $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery/' . $studio_id;
            $thumb_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery/' . $studio_id . "/thumb";
           
            mkdir($studio_dir, 0777);
            mkdir($thumb_dir, 0777);

            $upload_file_name = $name = Yii::app()->common->fileName($_FILES["image_to_lib"]["name"]);
            $all_details = $imagemanagement->get_imagedetails_by_name($name, $studio_id);
            if (count($all_details)) {
                $strtime = strtotime(date("Y-m-d H:i:s"));
                $name = $strtime . "_" . $name;
                $_FILES["image_to_lib"]["name"] = $name;
            }
            
            $type=$file_info['extension'];

            if ($type == "jpg" || $type == "jpeg" || $type == "JPG" || $type == "JPEG" || $type == "PNG" || $type == "png" || $type == "gif") {
                if (file_exists($studio_dir)) {
                    unlink($studio_dir);
                }
                //echo "test";
                move_uploaded_file($_FILES['image_to_lib']['tmp_name'], $studio_dir . '/' . $name);
            } else {
                $res['error'] = 1;
                $res['msg']   = "Please Upload valid image types(JPG/JPEG/PNG/GIF)";
                echo json_encode($res);
                exit;
            }

            $allimagepath = $studio_dir . '/' . $name;
            $crop_image_size = filesize();
            $crop_size_mb = round(($crop_image_size / 1048576), 3);
        }
        $bucketInfo = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        //echo $bucketName;
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/imagegallery'); //path for images uploaded exclude studio
        $config->setBucketName($bucketName);
        $s3_config = Yii::app()->common->getS3Details(Yii::app()->user->studio_id);
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  /* maximum paralell uploads */
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); /* allowed extensions */
        //$config->setDimensions($cropDimension);   /*resize to these sizes*/
        /* usage of uploader class - this simple :) */
        $uploader = new Uploader($studio_id);
        $ret = $uploader->uploadManagedImages($_FILES['image_to_lib'], $name, $new_cdn_user);
        //  print_r($ret);
        $original = explode('imagegallery', $ret['original']);
        $thumb = explode('imagegallery', $ret['thumb']);
        $ret['original'] = "imagegallery" . $original[1];
        $ret['thumb'] = "imagegallery" . $thumb[1];
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $res['img_src'] = $base_cloud_url.trim($ret['original']);
        
        $sql = "INSERT INTO image_management (image_size,image_name,s3_thumb_name,s3_original_name,studio_id,user_id,creation_date) VALUES (" . $crop_size_mb . ",'" . trim($upload_file_name) . "','" . trim($ret['thumb']) . "','" . trim($ret['original']) . "'," . $studio_id . "," . $user_id . ",'" . date('Y-m-d H:i:s') . "')";
        //   echo $sql;
        // exit
        Yii::app()->db->createCommand($sql)->execute();
        $res["success"] = 1;
        $res["msg"] = "Image uploaded successfully";
        echo json_encode($res);
        exit();
    }
    /**
     * @method public ncontentSetting() 
     * @desc Add Content: Video On-demand dropdown should appear ONLY if customer picked two options
     * @author manas@muvi.com
     * @return Setting Page
     */
    public function actionContentSetting() {
        $cat_poster_size='400x300';
        $this->breadcrumbs = array("Manage Content", 'Settings');
        $this->pageTitle = Yii::app()->name . ' | ' . 'Content Settings';
        $this->headerinfo = "Settings";
        $studio_id = $this->studio->id;
        $poster_sizes = Yii::app()->general->getPosterSize($studio_id);
        $pgposter = StudioConfig::model()->getConfig($studio_id, 'pg_poster_dimension');
        $multipart_cntorder = StudioConfig::model()->getConfig($studio_id, 'multipart_content_setting');
        $content_order = $multipart_cntorder->config_value;
        if(empty($pgposter)){$poster_sizes['pg_poster_dimension'] = '220x260';}
        else{$poster_sizes[$pgposter->config_key] = $pgposter->config_value;}        
        $catposter = StudioConfig::model()->getConfig($studio_id, 'category_poster_size');
        $poster_sizes['category_poster_size']=empty($catposter)?$cat_poster_size:$catposter->config_value;
        
        $video_on_dimand_avl = 'N';
        $video_live_streaming_avl = 'N';
        $audio_on_dimand_avl = 'N';
		$models=Film::model()->findAll(array(
			'select'=>'parent_content_type_id',
			'condition'=>'studio_id='.$studio_id.' AND status=1',
			'distinct'=>true,
		));
		$model_flag = CHtml::listData($models, 'parent_content_type_id', 'parent_content_type_id');
		if(in_array(1, $model_flag)){
            $video_on_dimand_avl = 'Y';
        }
		if(in_array(2, $model_flag)){
            $video_live_streaming_avl = 'Y';
        }        
		if(in_array(3, $model_flag)){
            $audio_on_dimand_avl = 'Y';
		}
		if(in_array(4, $model_flag)){
            $audio_live_streaming_avl = 'Y';
		}
        $this->render('settings', array('studio' => $this->studio, 'data' => $poster_sizes, 'video_on_dimand_avl' => $video_on_dimand_avl, 'video_live_streaming_avl' => $video_live_streaming_avl, 'audio_on_dimand_avl' => $audio_on_dimand_avl,'audio_live_streaming_avl' =>$audio_live_streaming_avl, 'multipart_content_order' => $content_order));
    }

    public function actionUpdateSetting() {
        $studio_id = $this->studio->id;
        $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'multipart_content_setting');
        if ($getStudioConfig) {
            $getStudioConfig->config_value = ($getStudioConfig->config_value==1)? 0:1;
            $getStudioConfig->save();
        } else {
            $config = new StudioConfig();
            $config->studio_id = $studio_id;
            $config->config_key = 'multipart_content_setting';
            $config->config_value = 1;
            $config->save();
        }
        echo @$getStudioConfig->config_value;exit;
    }

    public function actionSaveContentSetting(){
        if (isset($_POST['content']) && !empty($_POST['content'])) {
            $studio_id = $this->studio->id;
            $content = $_POST['content'];
            $metadata  = $_POST['content_child'];
            $content_sum = array_sum($content);
            $metadata_sum = (in_array(1,$content))?array_sum($metadata):0;
            if(in_array(4,$content)){//if audio is checked
                if(isset($_POST['audio_content_child']) && $_POST['audio_content_child']){
                     $metadata_sum = $metadata_sum + array_sum($_POST['audio_content_child']);
                }
            }
            $content_type = StudioContent::model()->updateContentSettings($studio_id,$content_sum);
            $metadata_type = StudioContent::model()->updateChildContentSettings($studio_id,$metadata_sum);
            /* policy */
            $policy = isset($_POST['is_enable_policy'])?$_POST['is_enable_policy']:0;
            $app_menu = StudioConfig::model()->updatePolicy($studio_id, $policy);
            /* policy end */
			/* Downloadable Content */
            $IsDownloadable = isset($_POST['is_downloadable'])?$_POST['is_downloadable']:0;
			StudioConfig::model()->UpdateDownloadable($studio_id, $IsDownloadable);
            /* Downloadable Content */
            $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'h_poster_dimension');
			if ($getStudioConfig) {
                $getStudioConfig->config_value = $_POST['h_poster_dimension'];
                $getStudioConfig->save();
            } else {
                $config = new StudioConfig();
                $config->studio_id = $studio_id;
                $config->config_key = 'h_poster_dimension';
                $config->config_value = $_POST['h_poster_dimension'];
                $config->save();
            }
			
            $getStudioConfig = StudioConfig::model()->getConfig($studio_id, 'v_poster_dimension');
            if ($getStudioConfig) {
                $getStudioConfig->config_value = $_POST['v_poster_dimension'];
                $getStudioConfig->save();
            } else {
                $config = new StudioConfig();
                $config->studio_id = $studio_id;
                $config->config_key = 'v_poster_dimension';
                $config->config_value = $_POST['v_poster_dimension'];
                $config->save();
            }
            $getStudioCatConfig = StudioConfig::model()->getConfig($studio_id, 'category_poster_size');
            if ($getStudioCatConfig) {
                $getStudioCatConfig->config_value = $_POST['cat_poster_dimension'];
                $getStudioCatConfig->save();
            } else {
                $config = new StudioConfig();
                $config->studio_id = $studio_id;
                $config->config_key = 'category_poster_size';
                $config->config_value = $_POST['cat_poster_dimension'];
                $config->save();
            }
			$Posterdata = array('h_poster_dimension' => $_POST['h_poster_dimension'], 'v_poster_dimension' => $_POST['v_poster_dimension']);
			$_SESSION['PosterSize'][$studio_id]= $Posterdata;
            if(isset($_REQUEST['pg_poster_dimension'])){// physical goods image dimension
               $pgposter = StudioConfig::model()->getConfig($studio_id, 'pg_poster_dimension');
                if ($pgposter) {
                    $pgposter->config_value = $_POST['pg_poster_dimension'];
                    $pgposter->save();
                } else {
                    $config = new StudioConfig();
                    $config->studio_id = $studio_id;
                    $config->config_key = 'pg_poster_dimension';
                    $config->config_value = $_POST['pg_poster_dimension'];
                    $config->save();
                } 
            }
            Yii::app()->user->setFlash('success', "Settings saved successfully.");
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in saving of settings.');
        }        
        $url = $this->createUrl('management/ContentSetting');
        $this->redirect($url);
        exit;
    }
    
    function actionUploadVideoS3() {
        $this->layout = FALSE;
        $studio_id = Yii::app()->common->getStudioId();
        $videoSynctabl = "select * from video_sync_folder where studio_id=" . $studio_id;
        $videoSynctablresult = Yii::app()->db->createCommand($videoSynctabl)->queryAll();
        $videoSyncForSony = 1;
        //  Sync for sony studios
        if(@$videoSynctablresult[0]['studio_id']){
            $bucket = 'upload2muvi';
            $s3url = 's3-eu-west-1.amazonaws.com';
            $videoSyncForSony = 2;
            $s3 = S3Client::factory(array(
                        'key'    => 'AKIAIG6XQGJLP6IO3AZA',
                        'secret' => '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'
                ));
            $response = $s3->getListObjectsIterator(array(
                                              'Bucket' => $bucket,
                                              'Prefix' => $videoSynctablresult[0]['folder_path'],
                         ));
        } else{
            $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
            $prefixPath = $unsignedFolderPathForVideo.'uploadvideo/';
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $response = $s3->getListObjectsIterator(array(
                                              'Bucket' => $bucket,
                                              'Prefix' => $prefixPath,
                         ));
        }
        foreach ($response as $object) {
            $new[$object['Key']] =  $object['Size'] ;
            if(isset($object['Size']) && ($object['Size'] !=0 )){
                $url = str_replace(' ', '%20','http://'.$bucket.'.'.$s3url.'/'.$object['Key']);
                $s3videoPath = str_replace(' ', '\\ ','s3://'.$bucket.'/'.$object['Key']);
                $arr = $this->is_url_exist($url);
                print_r(@$arr);
                
                if (@$arr['error'] != 1 && @$arr['is_video']) {
                    $videoSyncTemp = VideoSyncTemp::model()->findByAttributes(array('studio_id' => $studio_id,'file_path' => $object['Key']));
                    if(!$videoSyncTemp){
                        $videoS3filepath = $object['Key'];
                        $data = array();
                        $data['url'] = $url;
                        $data['s3videoPath'] = $s3videoPath;
                        $data['content_type'] = $arr['is_video'];
                        $videoInfo = new SplFileInfo($url);
                        $videopathExt = $videoInfo ->getExtension();
                        $videoName = Yii::app()->common->fileName($videoInfo->getBasename('.'.$videoInfo->getExtension()));
                        $video_name = $videoName.'.'.$videopathExt;
                        //'s3cmd setacl s3: stagingstudio/54/RawVideo/uploadvideo/ --acl-public --recursive';
                        $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $video_name . "' and flag_deleted=0";
                        $result = Yii::app()->db->createCommand($sql)->queryAll();
                        $check_availability_video = count($result);
                        if ($check_availability_video > 0) {
                            $video_name = $videoName.strtotime(date("d-m-Y H:i:s")).'.'.$videopathExt;
                        }
                        Yii::app()->aws->createUploadVideoSH($data, @$auth, $video_name, $videoSyncForSony, $videoS3filepath);
                    }
                }   
           }
        }
     }
      /*
     * @method public actionconvertSubtitlefromS3() 
     * @desc Converting .srt subtitle to .vtt
     * @author Prangya
     */
    public function actionconvertSubtitlefromS3() {
        require_once "srt2vtt/srtfile.php"; 
        if (isset($_REQUEST['studio_id'])) {
            $studio_id = $_REQUEST['studio_id'];
        } else {
            echo "Please provide studioid";
            exit();
        }
        $sql = "select * from video_subtitle where studio_id=" . $studio_id;
        $sqlres = Yii::app()->db->createCommand($sql)->queryAll();

        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
        $signedFolderPath = $folderPath['signedFolderPath'];
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucketname = $bucketInfo['bucket_name'];
        $data_ext = array();
        $s3url = Yii::app()->common->connectToAwsS3($studio_id);
        foreach ($sqlres as $key => $value) {
            $ext = pathinfo($value['filename'], PATHINFO_EXTENSION);
            $subId = $value['id'];
            $data_ext[$key] = $value['filename'];
            $directory = $_SERVER['DOCUMENT_ROOT'] . "/subtitle/";
            if(!is_dir($directory)) {
                mkdir($directory,true);
                chmod($directory, 0775);
            }
            if ($ext == 'srt') {
                $info = $s3url->doesObjectExist($bucketname, $signedFolderPath . "subtitle/". $subId. "/" .$value['filename']);
                if($info){
                    $response = $s3url->getObject(array(
                        'Bucket' => $bucketname, 
                        'Key' => $signedFolderPath . "subtitle/". $subId. "/" .$value['filename'], 
                        'SaveAs' => $_SERVER['DOCUMENT_ROOT'] . "/subtitle/" . $value['filename'],
                    ));

                    $source_file = $_SERVER['DOCUMENT_ROOT'] . "/subtitle/" . $value['filename']; 
                    $directory = $_SERVER['DOCUMENT_ROOT']. "/temp_srt/";
                    if(!is_dir($directory)) {
                        mkdir($directory,true);
                        chmod($directory, 0775);
                    }
                     if (file_exists($source_file)) {
                        $source_fileForS3 = "temp_srt/" . $subId . ".vtt";
                        $srt = new \SrtParser\srtFile($source_file);
                        $srt->setWebVTT(true);
                        $srt->build(true);
                        $srt->save($source_fileForS3, true);
                        $filename = str_replace(".srt", ".vtt", $value['filename']);
                        if(file_exists($source_fileForS3)){
                            $result = $s3url->putObject(array(
                                'Bucket' => $bucketname,
                                'Key' => $signedFolderPath . "subtitle/" . $subId . "/" .  $filename ,
                                'SourceFile' => $source_fileForS3,
                                'ACL' => 'public-read'
                            ));
                            if(isset($result['ObjectURL']) && $result['ObjectURL'] != ''){
                                $query = "UPDATE video_subtitle SET filename='$filename' WHERE id=" . $subId;  
                                $update = Yii::app()->db->createCommand($query)->execute();
                            }
                            @unlink($source_fileForS3);   
                        }
                    }
                }
            }
        }
    }
    /* Audio Gallery 
     * Author @manas@muvi.com
     */
    public function actionManageAudio() {
        $page_size = 20;
        $studio_id = $this->studio->id;
        $this->breadcrumbs = array("Manage Content", 'Audio Library');
        $this->headerinfo = "Audio Library";
        $this->pageTitle = "Muvi | Audio Library";       
        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'enable_audio_sync_for_audiogallery');    
        $this->render('manageaudio', array('getStudioConfig' => $getStudioConfig, 'page_size' => $page_size));
	}
    public function  actionsearchAudioPage()
    {
        $searchStr = '';
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio_id = $this->studio->id;
        $base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        if (isset($searchKey) && $searchKey != '') {
            $cond = 'vm.audio_name like "%' . $searchKey . '%" AND';
        } else {
            $cond = '';
        }
        // video_properties
        if (isset($_REQUEST['audio_duration']) && isset($_REQUEST['file_size']) && isset($_REQUEST['is_encoded']) && isset($_REQUEST['uploaded_in'])) {
            $duration = $_REQUEST['audio_duration'];
            $file_size = $_REQUEST['file_size'];
            $is_encoded = $_REQUEST['is_encoded'];
            $uploaded_in = $_REQUEST['uploaded_in'];
            //Duration
            if ($duration == 1) {// <5 mins
                $cond.='CAST(vm.duration AS TIME) < "00:05:00" AND ';
            } else if ($duration == 2) {// <30 mins
                $cond.='CAST(vm.duration AS TIME) < "00:30:00" AND ';
            } else if ($duration == 3) {// <120 mins
                $cond.='CAST(vm.duration AS TIME)< "02:00:00" AND ';
            } else if ($duration == 4) {// >120 mins
                $cond.='CAST(vm.duration AS TIME) > "02:00:00" AND ';
            } else {
                $cond.='';
            }
            //File Size
            if ($file_size == 1) {//< 1GB=1024 MB
                $cond.='SUBSTRING_INDEX(vm.file_size," ", 1)< 1024 AND ';
            } else if ($file_size == 2) {//<10 GB
                $cond.='SUBSTRING_INDEX(vm.file_size," ", 1)< 10240 AND ';
            } else if ($file_size == 3) {//>10 GB
                $cond.='SUBSTRING_INDEX(vm.file_size," ", 1)> 10240 AND ';
            } else {
                $cond.='';
            }
            //uploaded in 
            if ($uploaded_in == 1) {//this week
                $cond.=' YEARWEEK(vm.creation_date,1) =  YEARWEEK(CURDATE(), 1) AND ';
            } else if ($uploaded_in == 2) { //this month
                $cond.=' MONTH(vm.creation_date)=  MONTH(CURDATE()) AND';
            } else if ($uploaded_in == 3) {//this year
                $cond.=' YEAR(vm.creation_date) = YEAR(CURDATE()) AND ';
            } else if ($uploaded_in == 4) {// before this year
                $cond.=' YEAR(vm.creation_date) < YEAR(CURDATE()) AND ';
            } else {
                $cond.='';
            }
            //encoded ??
            if ($is_encoded == 1) {//yes
                $cond.=' ms.is_converted=1 AND ';
            } else if ($is_encoded == 2) {//No
                $cond.=' (ms.is_converted  IS NULL OR ms.is_converted =0 ) AND ';
            } else {
                $cond.='';
            }
        }
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $pcontent = Yii::app()->general->getPartnersContentIds();
            if ($pcontent['movie_id']) {
                $qry = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*, ms.is_converted as movieConverted FROM audio_gallery vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) WHERE ' . $cond . ' vm.studio_id=' . $studio_id . ' AND (ms.movie_id IN(' . $pcontent['movie_id'] . ') OR vm.user_id=' . Yii::app()->user->id . ') GROUP BY vm.id  order by vm.audio_name ASC LIMIT ' . $offset . ',' . $page_size;
            } else {
                $qry = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*, ms.is_converted as movieConverted FROM audio_gallery vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) WHERE ' . $cond . ' vm.studio_id=' . $studio_id . ' AND vm.user_id=' . Yii::app()->user->id . ' GROUP BY vm.id  order by vm.audio_name ASC LIMIT ' . $offset . ',' . $page_size;
            }
        } else {
            $qry = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*, ms.is_converted as movieConverted FROM audio_gallery vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) WHERE ' . $cond . ' vm.studio_id=' . $studio_id . ' GROUP BY vm.id  order by vm.audio_name ASC LIMIT ' . $offset . ',' . $page_size;
        }
        $allvideo = Yii::app()->db->createCommand($qry)->queryAll();
        $countall_video = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();        
        $count_searched = count($allvideo);        
        $this->renderPartial('audioRenderData', array('allvideo' => $allvideo, 'page_size' => $page_size, 'total_video' => $countall_video, 'count_searched' => $count_searched,'base_cloud_url'=>$base_cloud_url));
    }
    public function actionDeleteAudio() {
        $flag = 0;
        if($_POST['id']){
            $id = $_POST['id'];
            $audio_details = Yii::app()->db->createCommand()
                ->select('*')
                ->from('audio_gallery')
                ->where(array('IN', 'id', $id))
                ->queryAll();
            if($audio_details){
                /* Delete from S3 Bucket */
                $studio_id  = $this->studio->id;
                $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $bucket = $bucketInfo['bucket_name'];
                foreach ($audio_details as $key => $val) {
                    $audio_name = $val['audio_name'];
                    //$thumb_image_name = $audio_details[0]['thumb_image_name'];                                        
                    if (Yii::app()->user->new_cdn_users) {//existing user
                        $s3dir = $unsignedFolderPathForVideo . 'audiogallery/' . $audio_name;
                    } else {
                        $s3dir = $unsignedFolderPathForVideo . 'audiogallery/' . $studio_id . '/' . $audio_name;
                    }
                    /*if (Yii::app()->user->new_cdn_users) {
                        $s3dir1 = $unsignedFolderPathForVideo . 'audiogallery/videogallery-image/' . $thumb_image_name;
                    } else {
                        $s3dir1 = $unsignedFolderPathForVideo . 'audiogallery/' . $studio_id . '/videogallery-image/' . $thumb_image_name;
                    }*/                    
                    if ($audio_name) {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $s3dir
                        ));
                    }
                    /*if ($thumb_image_name) {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => $s3dir1
                        ));
                    }*/                    
                }
                /* End */
                $id = is_array($id)?implode(',', $id):$id;
                $d = AudioManagement::model()->deleteAll('id IN (' . $id . ')');
                if($d){$flag = 1;}
            }
        }
        if($flag){
            Yii::app()->user->setFlash('success', 'Audio deleted successfully.');
        }else{
            Yii::app()->user->setFlash('error', 'Audio could not be deleted.');
        }
    }
	public function actionManagePlaylist() {
		$page_size = 20;
		$studio_id = $this->studio->id;
		$this->breadcrumbs = array("Manage Content", 'Manage Playlist');
		$this->headerinfo = "Manage Playlist";
		$this->pageTitle = "Muvi | Manage Playlist";
		$isAdmin = 1;
		$playlist = Yii::app()->common->getAllplaylist($studio_id, $user_id, 1);
		$poster_sizes = Yii::app()->common->getCropDimension();
		$language_id = $this->language_id;
		$sql = "SELECT * FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
		$contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
		$contentCategories = CHtml::listData($contentCategory, 'binary_value', 'category_name');
		$this->render('manageplaylist', array('playlist_data' => $playlist, 'studio_id' => $studio_id, 'page_size' => $page_size, 'authToken' => $authToken, 'poster_size' => $poster_sizes, 'contentList' => @$contentCategories));
	}

	public function actionaddToPlaylist() {
		$studio_id = $this->studio->id;
		if ((isset($_POST['newplaylist']) && ($_POST['newplaylist'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$playlistnme = $_POST['newplaylist'];
			$user_id = (isset($_POST['user_id']) && $_POST['user_id'] != "") ? $_POST['user_id'] : 0;
			$movie_id = $_POST['movie_id'];
			$is_episode = $_POST['is_episode'];
			$content_category = (isset($_POST['content_category_value']) && $_POST['content_category_value'] != "") ? $_POST['content_category_value'] : 0;;
            $content_category_value = implode(',',$content_category);
			$permalink = Yii::app()->general->generatePermalink(stripslashes($playlistnme));
			$playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_name' => $playlistnme));
			if (empty($playlist)) {
				$playlistNew = new UserPlaylistName;
				$playlistNew->user_id = $user_id;
				$playlistNew->studio_id = $studio_id;
				$playlistNew->playlist_name = $playlistnme;
				$playlistNew->playlist_permalink = $permalink;
				$playlistNew->content_category_value = $content_category_value;
				$playlistNew->save();
				$playlist_id = $playlistNew->id;
			} else {
				$playlist_id = $playlist->id;
			}
                        if (HOST_IP != '127.0.0.1'){
                            $solrArr=array();
                            $solrArr['uniq_id'] = $playlist_id;
                            $solrArr['content_id'] = $content_category_value;
                            $solrArr['name'] = $playlistnme;
                            $solrArr['permalink'] = $permalink;
                            $solrArr['studio_id'] =  $studio_id;
                            $solrArr['display_name'] = 'playlist';
                            $solrArr['content_permalink'] = $permalink;
                            $solrArr['user_id'] = $user_id;
                            $solrObjnew = new SolrFunctions();
                            $solrObjnew->addSolrData($solrArr);                  
                         }
                                 
			$urlRouts['permalink'] = $permalink;
			$urlRouts['mapped_url'] = '/playlist/show/playlist_id/' . $playlist_id;
			$urlRoutObj = new UrlRouting();
			$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
			if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
				$theme_folder = $this->studio->theme;
				$width = @$_REQUEST['img_width'];
				$height = @$_REQUEST['img_height'];
				$cropDimension = array('thumb' => $width . 'x' . $height, 'standard' => $width . 'x' . $height, 'cmsthumb' => '100x100');
				$object_id = $playlist_id;
				$return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, 'playlist_poster', $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'], $object_id);
//print_r($return['original']);exit;
			}
			if ($movie_id != '') {
				$result = self::AddContentPlaylist($user_id, $movie_id, $is_episode, $playlist_id);
				Yii::app()->user->setFlash('success', $result['msg']);
			} else {
				Yii::app()->user->setFlash('success', "Playlist created successfully.");
			}
		} else {
			Yii::app()->user->setFlash('error', "Invalid request.");
		}
		if ($_POST['ajax_call']) {
			$res['code'] = 400;
			$res['status'] = "Success";
			$res['msg'] = $result['msg'];
			$result = $res;
			echo json_encode($result);
			exit;
		} else {
			if ($_POST['add_content'] == '1') {
				$url = $this->createUrl('admin/managecontent');
			} else {
				$url = $this->createUrl('management/ManagePlaylist');
			}
			$this->redirect($url);
			exit;
		}
	}
	public function actioneditPlaylist() {
		$studio_id = $this->studio->id;
		if ((isset($_POST['newplaylist']) && ($_POST['newplaylist'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$playlist_id = $_POST['list_id'];
			$playlist_name = $_POST['newplaylist'];
			$user_id = $_POST['user_id'];
			$content_category = (isset($_POST['content_category_value']) && $_POST['content_category_value'] != "") ? $_POST['content_category_value'] : 0;;
            $content_category_value = implode(',',$content_category);

			$permalink = Yii::app()->general->generatePermalink(stripslashes($playlist_name));
			$playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'id' => $playlist_id));
			if (!empty($playlist)) {
				$playlistUp = $playlist->findByPK($playlist->id);
				$playlistUp->playlist_name = $playlist_name;
				$playlistUp->playlist_permalink = $permalink;
				$playlistUp->content_category_value=$content_category_value;
				$playlistUp->save();
                                if (HOST_IP != '127.0.0.1'){
                                    $solrobj = new SolrFunctions();
                                    $solrobj->deleteSolrQuery("cat:playlist AND sku:" . $studio_id ." AND title:". $playlist->playlist_permalink . " AND id:".$playlist_id);
                                    $solrArr=array();
                                    $solrArr['uniq_id'] = $playlist->id;
                                    $solrArr['content_id'] = $content_category_value;
                                    $solrArr['name'] = $playlist_name;
                                    $solrArr['permalink'] = $permalink;
                                    $solrArr['studio_id'] =  $studio_id;
                                    $solrArr['display_name'] = 'playlist';
                                    $solrArr['content_permalink'] = $permalink;
                                    $solrArr['user_id'] = $user_id;
                                    $solrObjnew = new SolrFunctions();
                                    $solrObjnew->addSolrData($solrArr);                  
                                 }
                                
				$urlRouts['permalink'] = $permalink;
				$urlRouts['mapped_url'] = '/playlist/show/playlist_id/' . $playlist->id;
				$urlRoutObj = new UrlRouting();
                                $urlRoutObj->deleteAll('mapped_url = :mapped_url AND permalink = :permalink AND studio_id = :studio_id', array(':mapped_url'=>$urlRouts['mapped_url'], ':permalink'=>$playlist->playlist_permalink, ':studio_id'=>$studio_id));
				$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
				if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
					$theme_folder = $this->studio->theme;
					$width = @$_REQUEST['img_width'];
					$height = @$_REQUEST['img_height'];
					$cropDimension = array('thumb' => $width . 'x' . $height, 'standard' => $width . 'x' . $height, 'cmsthumb' => '100x100');
					$object_id = $playlist_id;
					$return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, 'playlist_poster', $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'], $object_id);
				}
				Yii::app()->user->setFlash('success', "Playlist Updated Successfully.");
			} else {
				Yii::app()->user->setFlash('error', "No Playlist found.");
			}
		} else {
			Yii::app()->user->setFlash('error', "Invalid data");
		}
		$url = $this->createUrl('management/ManagePlaylist');
		$this->redirect($url);
		exit;
	}

	public function actiondeletePlaylist() {
		$studio_id = $this->studio->id;
		if ((isset($_POST['playlist_id']) && ($_POST['playlist_id'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$playlist_id = $_POST['playlist_id'];
			$user_id = (isset($_POST['user_id']) && $_POST['user_id'] != "") ? $_POST['user_id'] : 0;
			$playlist_name = $_POST['playlist_name'];
			$playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_name' => $playlist_name));
			if (empty($playlist)) {
				$res['code'] = 400;
				$res['status'] = "Failure";
				$res['msg'] = 'Invalid playlist';
				echo json_encode($res);
				exit;
			} else {
				$playlist_id = $playlist->id;
			}
			$param2 = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id);
			$playItem = UserPlaylist::model()->deleteAll('playlist_id = :playlist_id AND user_id = :user_id AND studio_id = :studio_id', $param2);
			$params = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':playlist_name' => $playlist_name);
			UserPlaylistName::model()->deleteAll('user_id = :user_id AND playlist_name = :playlist_name AND studio_id = :studio_id', $params);
			if ($playlist->id) {
				$mapped_url = '/playlist/show/playlist_id/' . $playlist->id;
				$urlRoutObj = new UrlRouting();
				$urlRoutObj->deleteAll('mapped_url = :mapped_url AND permalink = :permalink AND studio_id = :studio_id', array(':mapped_url' => $mapped_url, ':permalink' => $playlist->playlist_permalink, ':studio_id' => $studio_id));
			}
			if (HOST_IP != '127.0.0.1') {
				$solrobj = new SolrFunctions();
				$solrobj->deleteSolrQuery("cat:playlist AND sku:" . $studio_id . " AND title:" . $playlist->playlist_permalink . " AND id:" . $playlist_id);
			}
			$res['code'] = 200;
			$res['status'] = "Success";
			$res['msg'] = "Playlist Deleted Successfully.";
			echo json_encode($res);
			exit;
		} else {
			$res['code'] = 405;
			$res['status'] = "Failure";
			$res['msg'] = 'Need To signin';
			echo json_encode($res);
			exit;
		}
	}

	public function actionDeleteContent() {
		$studio_id = $this->studio->id;
		if ((isset($_POST['playlist_id']) && ($_POST['playlist_id'] != '')) && (isset($_POST['user_id']) && ($_POST['user_id'] != ''))) {
			$content_id = $_POST['content_id'];
			$playlist_id = $_POST['playlist_id'];
			$user_id = $_POST['user_id'];
			$playlist = UserPlaylist::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'content_id' => $content_id, 'playlist_id' => $playlist_id));
			if (!empty($playlist)) {
				$params = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id, ':content_id' => $content_id);
				$userList = UserPlaylist::model()->deleteAll('content_id = :content_id AND playlist_id = :playlist_id AND studio_id = :studio_id AND user_id = :user_id', $params);
				$res['code'] = 200;
				$res['status'] = "Success";
				$res['msg'] = 'Content Removed from Playlist.';
			} else {
				$res['code'] = 405;
				$res['status'] = "Failure";
				$res['msg'] = 'No content found';
			}
		} else {
			$res['code'] = 407;
			$res['status'] = "Failure";
			$res['msg'] = 'Invalid data';
		}
		echo json_encode($res);
		exit;
	}
	function AddContentPlaylist($user_id, $movie_id, $is_episode, $playlist_id) {
		$studio_id = $this->studio->id;
		$playlistitem = UserPlaylist::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_id' => $playlist_id, 'content_id' => $movie_id, 'content_type' => $is_episode));
		if (empty($playlistitem)) {
			$playlistItem = new UserPlaylist;
			$playlistItem->user_id = $user_id;
			$playlistItem->studio_id = $studio_id;
			$playlistItem->playlist_id = $playlist_id;
			$playlistItem->content_id = $movie_id;
			$playlistItem->content_type = $is_episode;
			$playlistItem->save();
			$res['code'] = 200;
			$res['status'] = "Success";
			$res['msg'] = "Song added to playlist";
		} else {
			$res['code'] = 400;
			$res['status'] = "Success";
			$res['msg'] = "Already Added To playlist";
		}
		return $res;
	}
    public function actionImageThumbnail() {   
        $id = (isset($_POST['id']) && $_POST['id']> 0)? ($_POST['id']): 0;
        $arr1 = array();
        if($id > 0){
            $studio_id = Yii::app()->common->getStudioId();	
            $base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
            $thumbnail_images = VideoManagement::model()->getPerticularColumnData($id, $studio_id, 'thumbnail_images');
            $arr =  explode(',',$thumbnail_images['thumbnail_images']);
            foreach($arr as $key => $value){
                $arr1[] = $base_cloud_url . 'videogallery-image/'. $_POST['id'].'/'.$value;
            }
        }
        echo json_encode($arr1);
    }
        
	function actioncheckimagekey(){
		echo json_encode(ImageManagement::model()->isExistImageKey());
	}
	function actionupdateimagekey(){
		$res['success'] = 0;
		$image_id = isset($_REQUEST['id']) && $_REQUEST['id']?$_REQUEST['id']:'';
		$image_key = @$_REQUEST['image_key'];
		$isExistKey = ImageManagement::model()->isExistImageKey();
		if($isExistKey['success']==0){
			$res['message'] = $isExistKey['message'];
		} else {
			if(!empty($_REQUEST['id'])){
				$imagemanagement=ImageManagement::model()->findByPk($_REQUEST['id']);
				$imagemanagement->image_key=trim($image_key);
				$imagemanagement->save();
			}
			$res['success'] = 1;
		}
		echo json_encode($res);
	}
	/**
	 * @method managefile() file management method
	 *
	 */
	public function actionManageFile() {
		$studio_id = $this->studio->id;
		$IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);
		if($IsDownloadable == 1){
			$this->breadcrumbs = array('Manage Content', 'File Library');
			$this->headerinfo = "File Library";
			$this->pageTitle = "Muvi | File Library";
			$this->layout = 'admin';		
			$this->render('managefile', array('studio_id' => $studio_id));
		}else{
			Yii::app()->user->setFlash('error', "You don't have access to this page.");
            $url = $this->createUrl("/admin/dashboard");
            $this->redirect($url);
            exit();
		}
	}
	public function actionsearchFilePage() {
		$searchStr = '';
		$page_size = 20;
		$offset = 0;
		if (isset($_REQUEST['page'])) {
			$offset = ($_REQUEST['page'] - 1) * $page_size;
		}
		$studio_id = $this->studio->id;
		$searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
		if (isset($searchKey) && $searchKey != '') {
			$cond = 'i.file_name like "%' . $searchKey . '%" AND';
		} else {
			$cond = '';
		}
		$qry = 'SELECT SQL_CALC_FOUND_ROWS(0),i.* from file_management i WHERE ' . $cond . ' i.studio_id=' . $studio_id . ' order by i.id DESC LIMIT ' . $offset . ',' . $page_size;
		$allimage = Yii::app()->db->createCommand($qry)->queryAll();
		$countall_image = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();		
		$count_searched = count($allimage);
		$this->renderPartial('fileRenderData', array('allimage' => $allimage, 'page_size' => $page_size, 'total_image' => $countall_image, 'count_searched' => $count_searched));
	}
	public function actionDeleteFile() {
        $rec_id = $_POST['rec_id'];
        $studio_id = Yii::app()->user->studio_id;
        $video_details=FileManagement::model()->get_videodetails_by_id_studio($rec_id,$studio_id);
        if($video_details){
            $video_name=$video_details[0]['file_name'];
            $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
            $s3 =  Yii::app()->common->connectToAwsS3($studio_id);
            $s3dir = $unsignedFolderPathForVideo.'filegallery/'.$video_name;
            $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
            $bucket = $bucketInfo['bucket_name'];
            if($video_name){
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir
                ));
            }            
            $query = "delete from `file_management` where id=".$rec_id;
            $command = Yii::app()->db->createCommand($query);
            $command->execute();
            Yii::app()->user->setFlash('success', 'File deleted successfully.');
        }
        else{
            Yii::app()->user->setFlash('error', 'File could not be deleted.');
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
	public function actionCheckMappedFile() {

        if (count($_POST["id"])>1) {
            $sql = "SELECT * from movie_streams  WHERE file_management_id in";
            $sql.= "(" . implode(",", array_values($_POST['id'])) . ")";
            //echo $sql;
        }else
        {
           $sql = "SELECT * from movie_streams  WHERE file_management_id =".$_POST["id"][0];   
        }
            $command = Yii::app()->db->createCommand($sql);
            $list = $command->queryAll();
            //get the list of all values in  
            if (isset($list)) {
                $mapped_count = count($list);
                if ($mapped_count > 0) {
                    echo $mapped_count;
                } else {
                    echo 0;
                }
            }
        
    }
	 public function actionDeleteSelectedFile() {
        $studio_id = Yii::app()->user->studio_id;

        foreach ($_POST["id"] as $key => $val) {
            $video_details = FileManagement::model()->get_videodetails_by_id_studio($val, $studio_id);
            if ($video_details) {
                $video_name = $video_details[0]['file_name'];
                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                $s3dir = $unsignedFolderPathForVideo . 'filegallery/' . $video_name;
                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $bucket = $bucketInfo['bucket_name'];
                if ($video_name) {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir
                    ));
                }
                $query = "delete from `file_management` where id=" . $val;
                $command = Yii::app()->db->createCommand($query);
                $command->execute();
            }
        }
        Yii::app()->user->setFlash('success', 'file deleted successfully.');
    }
}

?>
