  <?php
  /*
 * Author:Prangya
 *#7217: Device_management code for adding deleting device details.
 */                 
?>
<div>
<?php //if(count($requestedDeleteDevices)>0){?>
    <!--<table class="table table-responsive table-condensed">
        <?php //foreach($requestedDeleteDevices as $key=>$objValue){?>
            <tr>
                <td class="col-md-6"><?php //echo $objValue->device;?></td>
				<td class="col-md-4"><?php //echo $objValue->device_info;?></td>
                <td class="col-md-1 text-right"><button type="button" class="btn btn-danger btn-xs"  onclick="openDeleteDevicePopup('<?php echo $objValue->id; ?>','<?php echo $objValue->user_id;?>');">Delete</button></td>
                <td class="col-md-1 text-right"><button type="button" class="btn btn-default btn-xs"  onclick="openCancelDevicePopup('<?php echo $objValue->id; ?>','<?php echo $objValue->user_id;?>');">Cancel</button></td>                      
            </tr>
    <?php //} ?>
    </table>--> 
<?php //} ?>
<table class="table table-responsive table-condensed" id="example">
    <thead>
        <tr>
            <th>Sl#</th>
			<th>Device Id</th>
            <th>Device Name</th>
            <th data-hide="phone" class="text-right">Action</th>
        </tr> 
    </thead>
    <tbody>
        <?php            
       if(count($devicearray)>0){
        $i =1;
        foreach($devicearray as $key=>$val){?>
            <tr>
                <td class="col-md-1"><?php echo $i; ?></td>
				<td class="col-md-4"><?php echo $val['device'];?></td>
                <td class="col-md-4"><?php echo $val['device_info'];?></td>
                <td class="col-md-3 text-right">  
                 <a href="javascript:void(0);" onclick="openDeleteDevicePopup('<?php echo $val['id']; ?>','<?php echo $val['user_id']; ?>');"><em class="icon-trash"></em>&nbsp;&nbsp;Remove</a>                          
                </td>
            </tr>
        <?php 
        $i++;
         }             
        }else{?>
            <tr><td colspan="4">No Records Found!!</td></tr>
        <?php } ?>    
        </tbody>
</table>  
</div>
<script type="text/javascript">
    /*
     * Action For Remove Selected Device.
     * @param {type} id
     * @param {type} user_id
     * @returns {undefined}
     */
   function openDeleteDevicePopup(id,user_id) {
          swal({
            title: "Delete Device?",
            text: "Do you really want to delete this Device? ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('.loader').show();
            $.post('/monetization/devicename',{id:id,user_id:user_id,choice:'delete_device'},function(res){
                if(res){
                    $('#newdevicePopup').modal('hide');
                    $('#devicePopup').modal('show');
                    $('#device_list').html(res);
                }
                $('.loader').hide();
            });
        });
    }; 
   function openCancelDevicePopup(id,user_id) {      
          swal({
            title: "Cancel Device?",
            text: "Do you really want to cancel this Device? ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('.loader').show();
            $.post('/monetization/devicename',{id:id,user_id:user_id,choice:'cancel_device'},function(res){ 
                if(res){
                    $('#newdevicePopup').modal('hide');
                    $('#devicePopup').modal('show');
                    $('#device_list').html(res); 
                }
                $('.loader').hide();                 
            });
        });
    };         
    </script>
                 