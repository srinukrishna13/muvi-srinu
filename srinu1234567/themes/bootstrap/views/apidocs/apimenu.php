<style type="text/css">
.menu { margin-top:25px;padding-top: 15px; background:#eee;}
.menu .accordion-heading {  position: relative; }
.menu .accordion-heading .edit {
    position: absolute;
    top: 8px;
    right: 30px; 
}
/*.menu .area { border-left: 4px solid #42b7da; }*/
.menu .equipamento { border-left: 0px solid #01c0f3; }
.menu .equipamento a{font-size:14px; color:#333; font-weight:400; }
.menu .equipamento a:hover{font-size:14px; color:#333; font-weight:400; background:#f7f6f6; text-decoration:none}
.menu .ponto { border-left: 4px solid #98b3fa; }
.menu .collapse.in { overflow: visible; }
.accordion-group {
    border: 0px solid #e5e5e5;
    border-radius: 4px;
    margin-bottom: 2px;
}
.area{ font-weight:400; font-size:18px; color:#01c0f3 !important}

</style>
<div class="menu">
	<div class="accordion">
		<!-- Áreas -->
		<div class="accordion-group">
			<!-- Área -->
			<div class="accordion-heading area">
				<a class="accordion-toggle" href="<?php echo Yii::app()->baseUrl.'/api';?>">Introduction</a>
			</div>
			<div class="accordion-body <?php if(Yii::app()->controller->action->id != 'introduction'){?>collapse<?php }?>" id="introduction">
				<div class="accordion-inner">
					<div class="accordion" id="introduction1">
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#introduction1" href="#the-api">The API</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#introduction1"  href="#change-policy">Change Policy</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#introduction1"  href="#security-and-authentication">Security and Authentication</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#introduction1"  href="#headers">Headers</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#introduction1"  href="#requests">Requests</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#introduction1"  href="#pagination">Pagination</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="accordion-heading area">
				<a class="accordion-toggle"  href="<?php echo $this->createUrl('api/users');?>">User</a>
			</div>
			<!-- /Área -->
			<div class="accordion-body <?php if(@$permalink !='users'){?>collapse<?php }?>" id="user-menu">
				<div class="accordion-inner">
					<div class="accordion" id="users">
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#users"  href="#login">Login</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#users"  href="#forgot-password">Forgot Password</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#users"  href="#check-email-exists">Check Email Existence</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#users"  href="#auth-user-card">Authenticate Card</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#users"  href="#register-user">Register User</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="accordion-heading area">
				<a class="accordion-toggle"  href="<?php echo $this->createUrl('api/content');?>">Contents</a>
			</div>
			<!-- /Área -->
			
			<div class="accordion-body  <?php if(@$permalink !='content'){?>collapse<?php }?>" id="contents">
				<div class="accordion-inner">
					<div class="accordion" id="content">
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#content" href="#content-list">Content List</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#content"  href="#content-details">Content Details</a></div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#content"  href="#episode-list">Episode Details</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="accordion-heading area">
				<a class="accordion-toggle" href="<?php echo $this->createUrl('api/search');?>">Search</a>
			</div>
			<div class="accordion-body  <?php if(@$permalink !='search'){?>collapse<?php }?>" id="search-menu">
				<div class="accordion-inner">
					<div class="accordion" id="search">
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#search"  href="#search-data">Search Data</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="accordion-heading area">
				<a class="accordion-toggle" href="<?php echo $this->createUrl('api/studio');?>">Studio</a>
			</div>
			<div class="accordion-body  <?php if(@$permalink !='studio'){?>collapse<?php }?>" id="studio-menu">
				<div class="accordion-inner">
					<div class="accordion" id="studio">
						<div class="accordion-group">
							<div class="accordion-heading equipamento"><a class="accordion-toggle" data-parent="#studio"  href="#studio-plan-list">Studio Plans</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /accordion -->
</div> 