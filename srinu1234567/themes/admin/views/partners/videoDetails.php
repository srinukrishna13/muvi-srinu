<style>
   
.comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}    
</style>
<?php
    
?>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />
<input type="hidden" id="movie_id" value="<?php echo $_REQUEST['id']?>" />
<input type="hidden" id="video_id" value="<?php echo $_REQUEST['video_id']?>" />
<div class="row m-b-40">
    <div class="col-xs-12">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button>
    </div>
</div>
<div class="row">
    
    <div class="col-md-4">
        <div class="row">
        <div class="col-md-12 ">
          <div class="input-group">
            <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
            <div class="fg-line">
                <div class="select">
                <input type="text" id="video_date" class="form-control input-sm" name="searchForm[video_date]" value='' />
                </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-hover">
                <tbody>
                    <tr>
                         <td id="wh">Hours watched: <?php echo $this->timeFormat($watchedHour['watched_hour']);?></td>
                         <td></td>
                     </tr>
                     <tr>
                         <td id="bd">Buffered Duration: <?php echo $this->timeFormat($bufferDuration['buffered_time']);?></td>
                         <td></td>
                     </tr>
                     <tr>
                         <td id="bw">Bandwidth: <?php echo $this->formatKBytes($hour_bandwidth['bandwidth']*1024,2);?></td>
                         <td></td>
                     </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="row-fluid">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div id="watchedchart" class="video-chart"></div>
                </div>
                <div class="col-md-6">
                    <div id="bandwidthchart" class="video-chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 m-t-40">
        <div class="col-md-4 pull-right">
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                <div class="fg-line">
                   <input  class="form-control input-sm search" placeholder="Search" />
                </div>
            </div>
        </div>
        <div class="col-sm-4">
        <div class="form-group input-group">
            <span class="input-group-addon">Device</span>
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm" id="device-box">
                        <option value="0">All</option>
                        <option value="1">Web</option>
                        <option value="2">Android</option>
                        <option value="3">iOS</option>
                        <option value="4">Roku</option>
                    </select>
                </div>
            </div>
        </div>
    </div> 
        <table class="table tablesorter" id="video-table-body">
                
        </table>
        <?php
                //if($viewsDetails['count'] > $page_size){
            ?>
                <div class="page-selection-div">
                    <div id="page-selection-video" class="pull-right"></div>
                </div>
            <?php //}?>
    </div>
</div>
<div class="h-40"></div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"-->
<script src="<?php echo Yii::app()->getbaseUrl(true);?>/common/js/jquery.tablesorter.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    $(function() {
        <?php if(isset($dateRanges) && !empty($dateRanges)){?>
            var date = new Date('<?php echo $dateRanges->start?>');
            var monthFirstIndex = date.getMonth();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var dateSecond = new Date('<?php echo $dateRanges->end?>');
            var monthSecondIndex = dateSecond.getMonth();
            var secondDay = new Date(dateSecond.getFullYear(), dateSecond.getMonth(), dateSecond.getDate());
            var start = moment(firstDay)._d;
            var end = moment(secondDay)._d;
            loadDateRangePicker('video_date','<?php echo $lunchDate;?>',start,end);
        <?php }else{?>
            loadDateRangePicker('video_date','<?php echo $lunchDate;?>');
        <?php }?>
        
        var date_range = $('#video_date').val();
        var movie_id = $('#movie_id').val();
        var video_id = $('#video_id').val();
        var searchText = $.trim($(".search").val());
        var device_type = $('#device-box').val();
        getVideoDetailsReportPartners(date_range,movie_id,video_id,searchText,device_type);
    });
    $('#video_date').change(function(){
        var date_range = $('#video_date').val();
        var movie_id = $('#movie_id').val();
        var video_id = $('#video_id').val();
        var searchText = $.trim($(".search").val());
        var device_type = $('#device-box').val();
        getVideoDetailsReportPartners(date_range,movie_id,video_id,searchText,device_type);
    });
    $('#device-box').change(function(){
        var date_range = $('#video_date').val();
        var movie_id = $('#movie_id').val();
        var video_id = $('#video_id').val();
        var searchText = $.trim($(".search").val());
        var device_type = $('#device-box').val();
        getVideoDetailsReportPartners(date_range,movie_id,video_id,searchText,device_type);
    });
    $('.report-dd').click(function(){
        var date_range = $('#video_date').val();
        var type = $(this).val();
        var id = $('#movie_id').val();
        var video_id = $('#video_id').val();
        var searchText = $.trim($(".search").val());
         var device_type = $('#device-box').val();
        if(type == 'csv'){
            window.location = '<?php echo Yii::app()->baseUrl."/report/getVideoDetailsReport?dt=";?>'+date_range+'&type='+type+'&id='+id+'&video_id='+video_id+'&search_value='+searchText+'&device_type'+device_type;
        }
    });
    
    $(document.body).on('keydown','.search',function (event){
        var searchText = $.trim($(".search").val());
        var movie_id = $('#movie_id').val();
        var video_id = $('#video_id').val();
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            var date_range = $('#video_date').val();
            var device_type = $('#device-box').val();
            getVideoDetailsReportPartners(date_range,movie_id,video_id,searchText,device_type);
        }
    });
</script>
<script type="text/javascript">
$(function () {
    Highcharts.setOptions({
        lang: {
            numericSymbols: 'k' //otherwise by default ['k', 'M', 'G', 'T', 'P', 'E']
        }
    });
    $('#watchedchart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: <?php echo $xdata?>
        },
        yAxis: {
            title: {
                text: 'Watched duration (in Seconds)'
            }
        },
        legend: {
            enabled:false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + ''+parseMillisecondsIntoReadableTime(this.y);;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: <?php echo $graphData;?>,
        credits: {
            enabled: false
        },
        exporting: { 
            enabled: false
        }
    });
    $('#bandwidthchart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: <?php echo $xdata?>
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bandwidth consumed (in GB)'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y;
            }
        },
        legend: {
            enabled:false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: <?php echo $bandwidthGraphData;?>,
        credits: {
            enabled: false
        },
        exporting: { 
            enabled: false
        }
    });
});
</script>