<?php

class Dashboard extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'dashboard_ordering';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function insert_order($arr) {
        $connection = Yii::app()->db->getSchema()->getCommandBuilder();
        $command = $connection->createMultipleInsertCommand('dashboard_ordering', $arr);
        $command->execute();
    }
    public function vieworder($studio_id) {
        $defaultsql = "SELECT * FROM dashboard_section order by id ASC";
        $defaultarr1 = Yii::app()->db->createCommand($defaultsql)->queryALL();
        foreach ($defaultarr1 as $k => $v) {
            $defaultarr[$v['id']] = $v['id'];
        }
        $sql = "SELECT * FROM dashboard_ordering WHERE studio_id = " . $studio_id . " order by ordernumber ASC";
        $arr1 = Yii::app()->db->createCommand($sql)->queryALL();
        if (empty($arr1)) {
            $arr = $defaultarr;
        }else{
            foreach ($arr1 as $k => $v) {
                $arr[$v['ordernumber']] = $v['section_id'];
            }
        }
        return $arr;
    }

}
