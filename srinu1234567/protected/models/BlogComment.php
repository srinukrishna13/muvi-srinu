<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BlogComment extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'blog_comments';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio', 'studio_id'),
            'post'=>array(self::HAS_ONE, 'BlogPost', 'id'),
        );
    } 
    
    public function findAllComments($post_id) {
        //Find All Comments     
        $studio_id = Yii::app()->common->getStudiosId();
        $comments = BlogComment::model()->findAll(array(
            'condition' => 'studio_id=:studio_id AND post_id=:post_id',
            'params' => array(':studio_id' => $studio_id, ':post_id' => $post_id),
            'order' => 't.id ASC'
        ));          
        return($comments);        
    }
    
    public function findActiveComments($post_id) {
        //Find All Comments  
        $studio_id = Yii::app()->common->getStudiosId();
        $comments = BlogComment::model()->findAll(array(
            'condition' => 'studio_id=:studio_id AND t.status = :status',
            'params' => array(':studio_id' => $studio_id, ':status' => 1),
            'order' => 't.id ASC'
        ));          
        return($comments);        
    }    
}

