<?php
if (isset(Yii::app()->user->created_at)) {
    $expairy_date = strtotime(Yii::app()->user->created_at) + 13 * 24 * 60 * 60;
} else {
    $expairy_date = time() + 13 * 24 * 60 * 60;
}
$v = RELEASE;
$delete_multi_msg  = 'Deleting a multi-part parent content deletes all episodes</b> under it and <b>the videos as well.</b> Are you sure?';
$delete_single_msg = 'Deleting content deletes all meta data of the content <b>and the video as well.</b> Are you sure?';
$delete_multi_msg_audio  = 'Deleting a multi-part parent content deletes all episodes</b> under it and <b>the audios as well.</b> Are you sure?';
$delete_single_msg_audio = 'Deleting content deletes all meta data of the content <b>and the audio as well.</b> Are you sure?';
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio->id);
$pgconfig = Yii::app()->general->getStoreLink();
if (isset(Yii::app()->user->id) && (Yii::app()->user->id > 0)) {
    $is_subscribed = Yii::app()->common->isSubscribed(Yii::app()->user->id);
}
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
$isPPVEnabled = $data_enable;
$template_name=$studio->parent_theme;
$content_type =  Yii::app()->general->content_count($this->studio->id);
$video=$audio=$physical=0;
if(empty($content_type) || (isset($content_type) && ($content_type['content_count'] & 1)) || (isset($content) && ($content['content_count'] & 2))){
	$video = 1;
}
if((isset($content_type) && ($content_type['content_count'] & 4))){
	$audio = 1;
}    
$MonetizationMenuSettings = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($studio_id);
$app = Yii::app()->general->apps_count($studio_id); 
$showoffline = 0;
if(empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))){
    $showoffline = 1;
}
?>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/new_thin_loader.css" rel='stylesheet' type="text/css" />
<style>
    #addvideo_popup .form-group {width:100% !important;}
    .panel-heading span {margin-top: -20px;font-size: 15px;}
    .panel-title{color: #000;}
    .clickable{cursor: pointer;color: #000;}
	.panel-heading {cursor: pointer;}
    .multiselect.dropdown-toggle.btn.btn-default-bg{width: 220px !important;}
    .input-group-addon{color: #000;}
	.btn-cancel{
		margin-top : 10px;
	}
</style>
<?php if (isset(Yii::app()->user->status) && Yii::app()->user->status == 3) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="Block">
                <div class="Block-Header">
                    <h3 class=""><strong><?php if ($expairy_date >= strtotime(date('Y-m-d'))) { ?>Your Trial Period expires on <?php echo date('F d, Y', $expairy_date);
    } else { ?>Site Status<?php } ?></strong></h3>
                    <div class="col-sm-12 pull-right">
                        <button data-widget="collapse" class="btn btn-primary btn-xs"><i class="fa fa-minus"></i></button>
                        <button data-widget="remove" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="Block-Body">
    <?php if ($expairy_date >= strtotime(date('Y-m-d'))) { ?>
                        <p>
                            Cancel anytime. Your card will not be charged if account is cancelled before <?php echo date('F d, Y', $expairy_date); ?>. Unless cancelled, Your card will be charged for the first month on <?php echo date('F d, Y', ($expairy_date + 24 * 60 * 60)); ?>
                        </p>
                        <div><strong>Status</strong></div>
    <?php } ?>
                    <table class="table status-tbl" border="1" >
                        <tbody>
                            <tr>
                                <td>Admin Panel</td>
                                <td>Add Content</td>
                                <td>Payment Gateway</td>
                                <td>Go Live</td>
                            </tr>
                            <tr>
                                <td class="greenBackground status-ready">Ready</td>
                                <td><a href="<?php echo $this->createUrl('admin/newContents'); ?>" >Add Content</a> </td>
                                <td>In Progress</td>
                                <td>In Progress</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
<?php } ?>
    <div class="row m-b-40">
        <div class="col-xs-8">
            <?php if ($allow_new) { ?>
                <a href="<?php echo $this->createUrl('admin/newContents'); ?>"><button class="btn btn-primary btn-default m-t-10">Add Content</button></a>
            <a href="javascript:void(0);"><button class="btn btn-primary btn-default m-t-10" data-toggle="modal" data-target="#importontent">Import</button></a>            
            <?php } else { ?>
                <a class="btn btn-primary btn-default m-t-10" data-toggle="modal" data-target="#Maxcontent">Add Content</a>
            <?php } ?>
            <a href="javascript:void(0);" onclick="showEncodingStatus()"><button class="btn btn-primary btn-default m-t-10">Refresh</button></a>
        </div>
					</div>					
    <?php if($_GET['searchForm']){$showfilter = 1;}else{$showfilter = 0;}?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-success m-b-10">
                <div class="panel-heading  <?php if(!@$showfilter){echo "panel-collapsed";}?>">
                    <h3 class="panel-title">FILTERS</h3>
                    <span class="pull-right clickable"><i class="fa <?php if(!@$showfilter){echo "fa-angle-down";}else{echo "fa-angle-up";}?>" style="font-size: 23px;" aria-hidden="true"></i></span>
                </div>
                <div class="panel-body" <?php if(!@$showfilter){ echo 'style="display: none;"';}?>>
    <div class="filter-div">
    <div class="row">
        <div class="col-md-12">
            <form action="<?php echo $this->createUrl('admin/managecontent'); ?>" name="searchForm" id="search_content" method="get">
                <div class="row">
                    <div class="col-sm-3 p-l-0">
                        <div class="form-group col-sm-12">
							<h4>Filter By : </h4>
                            <div class="fg-line">
                                <div class="select">
                                    <select class="form-control" name="searchForm[filterby]" id="filterby">
                                        <?php if (count($contentList) >= 1) { ?>
                                            <option value="0" <?php if (empty($searchData) || ($searchData['filterby'] == 0)) {
                                            echo "Selected";
                                        } ?>>&nbsp;Content Category</option>
                                        <?php } ?>
                                        <option value="1" <?php if ($searchData['filterby'] == 1) {
                                            echo "Selected";
                                        } ?>>&nbsp;Content Format</option>
                                    </select>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-l-0">
                        <div class="form-group">
                            <h4>Show : </h4>
                                    <?php if (count($contentList) >= 1) { ?>
                                <div id="show_category" style="display: none;">
                                    <select class="form-control input-sm showcatform" name="searchForm[content_category_value][]" id="filter_dropdown" multiple="multiple">
                                        <?php
                                        foreach ($contentList AS $row => $value) {
                                            $selected = (in_array($row, @$content_category_value)) ? 'selected' : '';
                                            echo '<option value="' . $row . '" ' . $selected . ' >' . $value . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                    <?php } ?>
                            <div id="show_form" style="display: none;">
                                <select class="form-control input-sm showcatform" name="searchForm[custom_metadata_form_id][]" id="form_dropdown" multiple="multiple">
                                    <?php
                                    foreach ($form as $value) {
                                        $selected = (in_array($value['id'], $searchData['custom_metadata_form_id'])) ? 'selected' : '';
                                        echo '<option value=' . $value['id'] . ' ' . $selected . '>' . $value['name'] . '</option>';
                                    }
                                    ?>
                                </select>	
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <h4>Sort By : </h4>
                            <div class="fg-line">
                                <div class="select">
                                    <select class="form-control" name="searchForm[sortby]" id="sortby">
                                        <option value="0" <?php if (empty($searchData) || ($searchData['sortby'] == 0)) {
                                        echo "Selected";
                                    } ?>>&nbsp;Last uploads</option>
                                        <option value="1" <?php if ($searchData['sortby'] == 1) {
                                        echo "Selected";
                                    } ?>>&nbsp;Most viewed</option>
                                        <option value="2" <?php if ($searchData['sortby'] == 2) {
                                        echo "Selected";
                                    } ?>>&nbsp;A-Z</option>
                                        <option value="3" <?php if ($searchData['sortby'] == 3) {
                                        echo "Selected";
                                    } ?>>&nbsp;Z-A</option>
                                    </select>	
                                </div>					
                            </div>
                        </div>
                    </div>
					<div class="col-sm-2">
                        <div class="form-group">
                            <h4>Content Type : </h4>
							<div class="fg-line">
								<div class="select">
									<select name="searchForm[contenttype]" class="form-control" id="contenttype">
										<?php if($video){?>
										<option value="0" <?php if (empty($searchData) || ($searchData['contenttype'] == 0)) { echo "Selected";} ?>>Video</option>
										<?php }if($audio){?>
										<option value="1" <?php if (($searchData['contenttype'] == 1)) { echo "Selected";} ?>>Audio</option>
										<?php }if($pgconfig){?>
										<option value="2" <?php if (($searchData['contenttype'] == 2)) { echo "Selected";} ?>>Physical</option>
										<?php }?>
									</select>
								</div>					
							</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
						<h4>Update Date : </h4>
                        <div class="input-group form-group">
                            <div class="fg-line">
                                <div class="select">
                                    <input class="form-control" type="text" id="update_date" name="searchForm[update_date]" value='<?php echo @$dateRange; ?>' />
                                </div>
                            </div>
                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group  input-group">
                            <div class="fg-line">
                                <input type="text" class="filter_input form-control fg-input" name="searchForm[search_text]" value="<?php echo @$searchText; ?>"  id="search_content" onkeypress="handle(event);" placeholder="What are you searching for?"/>
                            </div>
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table mob-compress" id="movie_list_tbl">
<!--        <thead>
            <tr>
            <th class="min-width">Content</th>
            <th class="width">Video</th>
            <th class="width">Type</th>
            <th class="width">Category</th>
            <th data-hide="phone" style="width:100px;">Views</th>
            <th data-hide="phone" class="width width--deviceBased">Action</th>
            </tr>
        </thead>    -->
        <tbody>
            <?php
            $cnt = 1;
            if ($data) {
                if ($template_name =='traditional-byod'){
                    $default_img = '/img/No-Image-Default-Traditional.jpg';
                    $default_episode_img = '/img/No-Image-Default-Traditional.jpg';
                }else{
                    $default_img = '/img/No-Image-Vertical-Thumb.png';
                    $default_episode_img = '/img/No-Image-Horizontal.png';
                }
                $postUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $cnt = (($pages->getCurrentPage()) * 20) + 1;
                $MonetizationMenuSettings = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($studio_id);
                foreach ($data AS $key => $details) {
                    $movie_id = $details['id'];
                    $stream_id = $details['stream_id'];
                    $content_types_id = $details['content_types_id'];
                    $is_episode = $details['is_episode'];
                    $cont_id = $movie_id;
                    if($is_episode == 1){
                        $cont_id = $stream_id;
                    }
                    $language_id =$this->language_id;
                    $langcontent = Yii::app()->custom->getTranslatedContent($cont_id,$is_episode,$language_id);
                    if(array_key_exists($stream_id, $langcontent['episode'])){
                        $details['episode_title'] = $langcontent['episode'][$stream_id]->episode_title;
                    }
                    if(array_key_exists($movie_id, $langcontent['film'])){
                        $details['name'] = $langcontent['film'][$movie_id]->name;
                    }
                $arg['studio_id']  = $studio_id;
                $arg['movie_id']   = $movie_id;
                $arg['season_id']  = $details['series_number'];
                $arg['episode_id'] = $stream_id;
                $arg['content_types_id'] = $details['content_types_id'];
                $isConverted = $details['is_converted'];
                $isEpisode   = $details['is_episode'];
                $isPPv = 0;
                $isAdvance = 0;
                $isFreeContent = Yii::app()->common->isFreeContent($arg);
                if(intval($isFreeContent) == 0) {
                    if(isset($payment_gateway) && !empty($payment_gateway)) {
                        if($content_types_id == 1) {
                            if($isConverted == 1) { 
                                $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);
                                if(isset($ppv->id) && intval($ppv->id)) {
                                    $payment_type = $ppv->id;
                                } else {
                                    $ppv = Yii::app()->common->getContentPaymentType($details['content_types_id'], $details['ppv_plan_id']);
                                    $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
                                }
                                if (intval($payment_type)) {
                                    $isPPv = 1;
                                }
                            } else {
                                $ppv = Yii::app()->common->checkAdvancePurchase($movie_id);
                                $adv_payment = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
                                if (intval($adv_payment)) {
                                    $isAdvance = 1;
                                }
                            }
                        } else {
                            if (($isConverted == 1)| ($details['is_episode']==0)){
                                $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);
                                if (isset($ppv->id) && intval($ppv->id)) {
                                    $payment_type = $ppv->id;
                                } else {
                                    $ppv = Yii::app()->common->getContentPaymentType($details['content_types_id'], $details['ppv_plan_id']);
                                    $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
                                }
                                if (intval($payment_type)) {
                                    $isPPv = 1;
                                }
                            }
                        }
                    }
                }


                    //View Count 
                    $views = 0;
                    if ($details['is_episode']) {
                        $views = isset($episodeViewcount[$stream_id]) ? $episodeViewcount[$stream_id] : 0;
                    if (isset($episodePosters[$stream_id]) && $episodePosters[$stream_id]['poster_file_name'] != '') {
                        $img_path = $postUrl . '/system/posters/' . $episodePosters[$stream_id]['id'] . '/episode/' . $episodePosters[$stream_id]['poster_file_name'];
                    } else {
                        $img_path = $default_episode_img;
                    }
                        $contentName = $details['name'] . ' - ' . $details['episode_title'];
                    } else {
						
                        $views = isset($viewcount[$movie_id]) ? $viewcount[$movie_id] : 0;
                        if ($details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                            $size = 'episode';
                        } else {
                            $size = 'thumb';
                        }

                    if (isset($posters[$movie_id]) && $posters[$movie_id]['poster_file_name'] != '') {
                        $img_path = $postUrl . '/system/posters/' . $posters[$movie_id]['id'] . '/' . $size . '/' . $posters[$movie_id]['poster_file_name'];
                    } else {
                        $img_path = $default_img;
                    }
                        $contentName = $details['name'];
                    }
                    $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));
                    
                    if (false === file_get_contents($img_path)) {
                         
                        if ($details['is_episode'] || $details['content_types_id'] == 2 || $details['content_types_id'] == 4  ) {
                               $img_path = $default_episode_img;
                        }else{
                                $img_path = $default_img;
                        }
                      }
                      
                    ?>
            <tr>
                <td>
                    <a href="<?php echo 'http://' . Yii::app()->user->siteUrl . '/' .  @$plink; ?>" target='_blank' >
                    <div class="Box-Container-width-Modified">
                        <div class="Box-Container">
                             <div class="thumbnail">
                                <img src="<?php echo $img_path; ?>" alt="<?php echo $details['name']; ?>" >
                             </div>
                         </div>   
                    </div>   
						<div class="caption" style="max-width: 255px;">   
						   <?php echo $contentName; ?>
						</div>
                    </a>
                </td>
                <td>
                    <?php 
                    if($details['parent_content_type_id']==3){
                        $icon = '<i class="fa fa-music fa-2x" aria-hidden="true"></i>';
                    }else if(in_array ($details['parent_content_type_id'], array(2,4))){
                        $icon = '<i class="fa fa-file-video-o fa-2x" aria-hidden="true"></i>&nbsp;<span class="red"><b>Live</b></span>';
                    }else if($details['parent_content_type_id']==1){
                        $icon = '<i class="fa fa-file-video-o fa-2x" aria-hidden="true"></i>';
                    }
                    echo $icon."<br/>";
                    ?>
                    <b> Forms </b><br/>
                    <?=$details['formname'];?>             
                                
                    <?php
                    if(@$movieLiveStream['feed_method'][$movie_id] == 'push'){
                        echo '<br/><br/><b id="live_streaming_'.$movie_id.'" style="cursor: pointer;">';
                        if(@$movieLiveStream['start_streaming'][$movie_id] == 1){
                            ?><span class="red" onclick="liveStreamingStatus(<?php echo $movie_id;?>,0)"><em class="icon-control-pause left-icon"></em> Stop Streaming</span><?php
                        } else if(@$movieLiveStream['start_streaming'][$movie_id] == 0){ 
                            ?><span class="blue" onclick="liveStreamingStatus(<?php echo $movie_id;?>,1)"><em class="icon-control-play left-icon"></em> Start Streaming</span><?php
                        }
                        echo "</b>";
                    }
                    if($details['content_category_value']){?>
                        <br/><br/><b>Category</b><br/>
                        <?php 
                        $categories = Yii::app()->Helper->categoryNameFromCommavalues($details['content_category_value'],$contentList);
                        if(strlen($categories)>25){
                            echo '<span title="'.$categories.'">'.substr($categories,0,25).'..</span>';
                        } else {
                            echo $categories;
                        }
                    }?>                    
                </td>
                <td style="max-width: 260px;">
                <span id="<?php echo $stream_id; ?>">
                <?php if (@$details['is_download_progress']) { 
                    if (@$details['video_management_id'] !=  0) { 
                        ?>
                            <h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>
                        <?php
                    } else{
                        ?>
                            <h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video download in progress. It will be available within an hour"><em class="fa fa-download"></em>&nbsp;&nbsp; Download in progress</a></h5>
                        <?php
                    }
                 
            } else if (!$details['full_movie'] && !$details['thirdparty_url'] && ($details['content_types_id']==1 || $details['content_types_id']==2 || ( $details['content_types_id']==3 && $details['is_episode']==1 ))) {
                    if ($details['is_converted'] == 2) {
                        ?><h5><a href="javascript:void(0);" style="text-decoration: none !important; color: inherit; font-weight: normal" data-toggle="tooltip" title="Please check the file uploaded, it might be corrupted or contact our support team.">Encoding : Failed &nbsp;&nbsp;<em class="fa fa-warning" style="color:#C62828;"></em></a></h5><?php
                    }
                ?>
                <span id="video_upload_<?php echo $stream_id; ?>"><h5><a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id; ?>, '<?php echo $details['stream_id']; ?>',<?php echo $details['content_types_id']; ?>);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload video</a></h5></span>
                <?php
                } else if (!$details['full_movie'] && !$details['thirdparty_url'] && ($details['content_types_id']==5 || ( $details['content_types_id']==6 && $details['is_episode']==1 ))) { 
                ?>
                <h5><a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id; ?>, '<?php echo $details['stream_id']; ?>',<?php echo $details['content_types_id']; ?>);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Audio</a></h5>
                <?php
                } else {
                 if($details['content_types_id']==5 || ( $details['content_types_id']==6 && $details['is_episode']==1 )){$is_audio = 1;}else{$is_audio = 0;}
                 if(($details['content_types_id']==1 || $details['content_types_id']==2 || ( $details['content_types_id']==3 && $details['is_episode']==1 ) || $is_audio)){
                if ($details['is_converted']==1) {
                    if($is_audio){
                        $contentgalleryname = @$movieaudiogalleryName[$stream_id];
                    }else{
                        $contentgalleryname = @$movievideogalleryName[$stream_id];
                    }
                    if(@$contentgalleryname != ''){
                                echo "<b>File Name</b><br/>".$contentgalleryname;
                    } else{
                        if(!$details['thirdparty_url']){
                                  echo "<b>File Name</b><br/>".@$details['full_movie'];  
                        }
                    }
                if($is_audio){?>
                    <h5><a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id; ?>, '<?php echo $details['stream_id']; ?>',<?php echo $details['content_types_id']; ?>);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Change Audio</a></h5>
                <?php }else{?>
                    <h5><a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id; ?>, '<?php echo $details['stream_id']; ?>',<?php echo $details['content_types_id']; ?>);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Change video</a></h5>
                <?php }
                }else if ($details['is_converted']==3) {?>
                    <h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video mapping is in progress. It will be available within an hour"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Mapping in progress</a></h5>
                <?php
                } else { 
                    if(@$encodingData[$details['stream_id']] != ''){
                ?>
                    <h5>Encoding : In Progress &nbsp;&nbsp;<em class="fa fa-refresh fa-spin" style="color:#2E7D32;"></em></br><span class="encodingTimeRemaning">Time remaining: <?php echo @$encodingData[$details['stream_id']]; ?></span></h5>
                <?php
                        
                    } else{
                ?>
                        <h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>
                <?php
                    }
                }
                }
                }
                ?>                               
                </span>    
                    <br>
                    <b>#Plays</b><br/>
                    <?php echo "<b>$views</b>";?>
                </td>
                <td>
                    <b>Monetization</b><br/>
                    <?php if (isset($data_enable) && ($data_enable['menu'] & 2) && ($isPPv == 1)) {
                    echo "Pay Per View, "; 
                    }if (isset($data_enable) && ($data_enable['menu'] & 1)) {
                    echo "Subscription, ";
                    }if (isset($data_enable) && ($data_enable['menu'] & 8) && ($isFreeContent== 1)) {
                    echo "Free Content, ";
                    }if(isset($data_enable) && ($data_enable['menu'] & 16) && ($isAdvance == 1)) {
                    echo "Pre-Order, ";
                    }if(isset($data_enable) && ($data_enable['menu'] & 32)) {
                    echo "Coupon";
                    }?>
                    <br/><br/>
                <?php
                    /*if (count($srest) > 0) {
                       echo "<b>Geo Block</b>";
                    }*/
                if (count($grestcat) > 0) { 
                                $country = Yii::app()->general->getGeoblockName($movie_id,$stream_id);
                        if($country['category_name'] != ''){
                            echo "<b>Geo Block</b><br/>".$country['category_name'];
                                } 
                    } ?>
               </td>
            <td>
                    <?php if (@$details['content_types_id'] == 3 && !$details['is_episode']) { ?>
                    <h5><a href="<?php echo $this->createUrl('admin/newContents', array('content_type_id' => $details['content_type_id'], 'content_id' => $movie_id, 'is_episode' => 1)); ?>" class=''><em class="fa fa-plus-circle"></em>&nbsp;&nbsp; Add Episode</a> </h5>
                    <?php }else if (@$details['content_types_id'] == 6 && !$details['is_episode']) { ?>
                    <h5><a href="<?php echo $this->createUrl('admin/newContents', array('content_types_id' => $details['content_types_id'], 'content_id' => $movie_id, 'is_episode' => 1)); ?>" class=''><em class="fa fa-plus-circle"></em>&nbsp;&nbsp; Add Track</a> </h5>
                    <?php } else if(@$details['content_types_id'] == 4) { ?>

                    <?php }else{?>
                    <span id="video_<?php echo $stream_id; ?>">
                    <?php /*if (@$details['is_download_progress']) { 
                        if (@$details['video_management_id'] !=  0) { 
                            ?>
                                <h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a></h5>
                            <?php
                        } else{
                            ?>
                                <h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video download in progress. It will be available within an hour"><em class="fa fa-download"></em>&nbsp;&nbsp; Download in progress</a></h5>
                            <?php
                        }
                         } else if (!$details['full_movie'] && !$details['thirdparty_url']) { ?>
                                <span id="video_upload_<?php echo $stream_id; ?>"><h5><a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id; ?>, '<?php echo $details['stream_id']; ?>',<?php echo $details['content_types_id']; ?>);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload video</a></h5></span>
                    <?php
                    } else { */
                    if ($details['is_converted']==1) {
                    ?>
                <h5><a href="<?php echo $this->createUrl('video/play_video', array('movie' => $movie_id, 'movie_stream_id' => $details['stream_id'], 'preview' => true)); ?>" target="_blank"><em class="icon-control-play"></em>&nbsp;&nbsp; Preview Content</a></h5>
                <?php }
                    //}
                    ?>                               
                    </span>
                    <span id="<?php echo $stream_id; ?>_title" style="display:none"><?php echo $details['name']; ?></span>
                    <?php } ?>

                    <?php if (($details['content_types_id'] == 4 && @$movieLiveStream[$details['id']] == 1) || ((($details['content_types_id'] == 3 && $details['is_episode']) || $details['content_types_id'] != 3 ) && $details['full_movie'] && $details['is_converted']==1)) { 
                        $domainName = Yii::app()->getBaseUrl(TRUE);
                        if(isset(Yii::app()->user->is_embed_white_labled) && Yii::app()->user->is_embed_white_labled){
                            $domainName = 'http://'.Yii::app()->user->siteUrl;
                        }
                        if($details['content_types_id'] == 4){
                            $embedUrl = $domainName.'/embed/livestream/' . $details['uniq_id'];
                        } else{
                            $embedUrl = $domainName.'/embed/' . $details['embed_id'];
                        }
                        //share url
                        $sharedUrl='';
                        if (isset($data_enable) && ($data_enable['menu'] & 8) && ($isFreeContent== 1)){
                            if($details['content_types_id'] == 4){
                                $sharedUrl = $domainName.'/share/livestream/' . $details['uniq_id'];
                            } else{
                                $sharedUrl = $domainName.'/share/' . $details['embed_id'];
                            }
                        }

                    ?>
                <h5><a href="<?php echo $this->createUrl('admin/editMovie/', array('movie_id' => $movie_id, 'uniq_id' => $details['uniq_id'], 'movie_stream_id' => $details['stream_id'])); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; Edit content</a></h5>
                    <h5><a href="javascript:void(0);"  onclick="openEmbedBox('embedbox_<?php echo $stream_id; ?>');" class="embed-box"><em class="fa fa-chain"></em>&nbsp;&nbsp; Embed</a></h5>
                <div class="modal fade" id="embedbox_<?php echo $stream_id; ?>" tabindex="-1" role="dialog" >
                    <div class="modal-dialog" role="document" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Embed <?php if (isset($data_enable) && ($data_enable['menu'] & 8) && ($isFreeContent== 1)){?> and Share <?php } ?> url</h4>
                            </div>
                            <div class="modal-body">                               
                                <div class="form-group">
                                    <label for="iframeEmbed_<?php echo $stream_id; ?>" class="col-sm-2 control-label">Embed</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                          <input type="text" class="form-control input-sm" id="iframeEmbed_<?php echo $stream_id; ?>" placeholder="Embed Code..." value=''>                                          
                                        </div>                                        
                                    </div>
                                   
                    <span class="input-group-btn">
                                         <div  class="animate-div " style="display:none;z-index:10;" id="embed_<?php echo $stream_id; ?>">Copied!</div>
<button class="btn btn-default btn-blue copyToClipboard" id="iframeEmbedButton_<?php echo $stream_id;?>" data-clipboard-text='' type="button" onclick="CopytoClipeboard('embed_<?php echo $stream_id; ?>');">Copy!</button>
                    </span>                    
                                </div>
                                <?php if (isset($data_enable) && ($data_enable['menu'] & 8) && ($isFreeContent== 1)){?>
                                <div class="form-group">
                                    <label for="inputShare<?php echo $stream_id;?>" class="col-sm-2 control-label">Share</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                          <input type="text" class="form-control input-sm" id="inputShare<?php echo $stream_id;?>" placeholder="Share Code..." value="<?php echo $sharedUrl;?>">                                          
                                        </div>                                                                                                                      
                                    </div>                                    
                                    <span class="input-group-btn">
                                        <div  class="animate-div " style="display:none;z-index:10;" id="share_<?php echo $stream_id; ?>">Copied!</div> 
<button class="btn btn-default btn-blue copyToClipboard" id="iframeShareButton_<?php echo $stream_id;?>" data-clipboard-text='<?php echo $sharedUrl;?>' type="button" onclick="CopytoClipeboard('share_<?php echo $stream_id; ?>');">Copy!</button>
</span> 
                                </div>
                                <?php } ?>
                            </div>
                            <div class="modal-footer">                    
                                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            </div>  
                        </div>
                    </div>
                </div>    
                    <script>
                    var myjscode = '<iframe width = "100%" height = "315" style="background-color:#000" id = "myIframe_<?php echo $stream_id; ?>" src = "<?php echo $embedUrl;?>" frameborder = "0" allowfullscreen > </iframe>';
                    myjscode += '<script>';
                    myjscode += "var viewPortTag=document.createElement('meta');viewPortTag.name='viewport';viewPortTag.content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;';window.parent.document.getElementsByTagName('head')[0].appendChild(viewPortTag);";                    
                <?php if($embedWaterMArkEnable){ ?>                       
                    myjscode += "var muviIframeDivId = '<?php echo $stream_id; ?>'; function checkIfCookieIsSetForEmbedMuvi(){var e=\"\";if(document.cookie.indexOf(\"MUVI_WATERMARK\")>=0){var m=document.cookie.match(new RegExp(\"MUVI_WATERMARK=([^;]+)\"));m&&(e=m[1])}return e=encodeURIComponent(e.trim())}function checkMuviIframeSrc(){if(\"undefined\"!=typeof muviIframeSrc){var e=document.getElementById(\"myIframe_\"+muviIframeDivId).src;muviIframeSrc!==e&&(document.getElementById(\"myIframe_\"+muviIframeDivId).src=muviIframeSrc)}}var muviIframeSrc=document.getElementById(\"myIframe_\"+muviIframeDivId).src+\"?waterMarkOnPlayer=\"+checkIfCookieIsSetForEmbedMuvi();document.getElementById(\"myIframe_\"+muviIframeDivId).src=muviIframeSrc,window.setInterval(function(){checkMuviIframeSrc()},500);"; 
                <?php } ?>                    
                    myjscode += '<\/script>';                       
                    
                    $("#iframeEmbed_<?php echo $stream_id; ?>").val(myjscode);
                    $("#iframeEmbedButton_<?php echo $stream_id; ?>").attr("data-clipboard-text",myjscode);
                    </script> 
                <?php }else{ ?>
                    <h5><a href="<?php echo $this->createUrl('admin/editMovie/', array('movie_id' => $movie_id, 'uniq_id' => $details['uniq_id'], 'movie_stream_id' => $details['stream_id'])); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; Edit content</a></h5>
                    <?php } ?>  
                <?php   
                if (($this->language_id == $details['language_id']) && ($details['parent_id'] == 0)) { 
                    if ($details['is_episode']) { ?>
                        <h5><a href="<?php echo $this->createAbsoluteUrl('admin/removeEpisode', array('movie_id' => $movie_id, 'movie_stream_id' => $details['stream_id'])); ?>" data-msg = 'Deleting Episode deletes all meta data of the episode <b>and the <?php if($details['parent_content_type_id']==3){echo "audio";}else{echo "video";}?> as well.</b> Are you sure?' class="confirm"><em class="icon-trash"></em>&nbsp;&nbsp; Delete content</a></h5>
                        <?php } else { ?>
                        <h5><a href="<?php echo $this->createAbsoluteUrl('admin/removeMovie', array('movie_id' => $movie_id, 'uniq_id' => $details['uniq_id'])); ?>" class="confirm" data-msg ="<?php if (@$details['content_types_id'] == 3) { if($details['parent_content_type_id']==3){echo $delete_multi_msg_audio;}else{echo $delete_multi_msg;} } else { if($details['parent_content_type_id']==3){echo $delete_single_msg_audio;}else{echo $delete_single_msg;} } ?>"><em class="icon-trash"></em>&nbsp;&nbsp; Delete content</a></h5>
                    <?php } 
                } ?>  
                <?php   
                 if (isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 2) && intval($notpartner)) { ?>
                    <h5><a href="javascript:void(0);" onclick="openPPVpopup(this);" data-name="<?php echo addslashes($details['name']); ?>" data-content_types_id="<?php echo $details['content_types_id']; ?>" data-movie_id="<?php echo $details['uniq_id']; ?>"><em class="icon-eye"></em>&nbsp;&nbsp; Set ppv</a></h5>
                    <?php } ?>
                <?php if (count($grestcat) > 0) {
                    if ((($details['content_types_id'] == 3) && ($details['is_episode']== 0)) || (($details['content_types_id'] == 6) && ($details['is_episode']== 0)) || in_array($details['content_types_id'],array(1,2,4,5))){ ?>
                        <h5><a href="javascript:void(0);" onclick="openGeoBlock(this);" data-movie_id="<?php echo $movie_id; ?>" data-movie_stream_id="<?php echo $details['stream_id']; ?>" style="width:147px;"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp; Set Geo-Block</a></h5>
                <?php }} ?>
                <?php if(isset($data_enable) && ($data_enable['menu'] & 4)){?>
                    <div class="m-b-10" >
                        <h5> <a href="javascript:void(0);" onclick="openManageAds('<?php echo $details['stream_id']; ?>','<?php echo $MonetizationMenuSettings[1]['ad_network_id']; ?>');"><em class="fa fa-bullhorn"></em>&nbsp;&nbsp; Manage Ads</a></h5>
                    </div>
                <?php } ?>
                    <?php /*if($pgconfig){ ?>
                    <h5><a href="javascript:void(0);" onclick="openAddItems(this);" data-content_id="<?php echo $details['id']; ?>" ><em class="fa fa-shopping-cart"></em>&nbsp;&nbsp; Add Item</a></h5>
                <?php }*/ ?>
                    <h5><a href="javascript:void(0);" onclick="openRelatedContent(this);" data-content_id="<?=$details['id']; ?>" data-content_stream_id="<?= $details['stream_id']; ?>" ><em class="fa fa-shopping-cart"></em>&nbsp;&nbsp; Manage Related Content</a></h5>
                     <?php if($showoffline == 1 && $details['full_movie'] != '' && $details['is_converted'] == 1 && ($offline_val['config_value']==1 || $offline_val == "") && @$details['thirdparty_url'] =='' && ((@$details['content_types_id'] == 3 && @$details['is_episode'] == 1) || @$details['content_types_id'] == 1)){
                            if($details['is_offline']==1){ ?>
                                <h5> <a href="javascript:void(0);" class="offline" data-stream_id="<?php echo $details['stream_id']; ?>" data-offline_value="0"><em class="fa fa-chevron-down"></em>&nbsp;&nbsp; Disable Offline View</a></h5>
                    <?php   } else{ ?>
                                <h5> <a href="javascript:void(0);" class="offline" data-stream_id="<?php echo $details['stream_id']; ?>" data-offline_value="1"><em class="fa fa-chevron-up"></em>&nbsp;&nbsp; Enable Offline View</a></h5>
                    <?php   } 
                        }
                         if(@$movieLiveStream['feed_method'][$movie_id] == 'push'){
                            if(@$movieLiveStream['stream_url'][$movie_id] != '' && @$movieLiveStream['stream_key'][$movie_id] != ''){
                                ?><h5><a href="javascript:void(0);" onclick="showLiveStreamData('<?php echo @$movieLiveStream['stream_url'][$movie_id];?>','<?php echo @$movieLiveStream['stream_key'][$movie_id]; ?>');" ><em class="fa fa-wrench"></em>&nbsp;&nbsp; Setup Encoding Software</a></h5><?php
                            } 
                        }
                    ?>
		<?php if (($details['content_types_id'] == 5 || ( $details['content_types_id'] == 6 && $details['is_episode'] == 1 ))) { ?>
							<h5><a href="javascript:void(0);" onclick="showPlaylist('<?php echo @$details['stream_id']; ?>', '<?php echo @$details['is_episode']; ?>');" ><em class="icon-playlist"></em>&nbsp;&nbsp; Add to playlist</a></h5>
		<?php } ?>
					<input type="hidden" id="is_downloadable_<?php echo @$details['stream_id']; ?>" value="<?php echo @$details['is_downloadable'];?>">
                </td>
            </tr>	
            <?php
            $cnt++;
             }} else {?>
            <tr>
                <td colspan="5">
                    <?php if ($contentType) { ?>
                        <p class="text-red">Oops! You don't have any content under this category.</p>
                    <?php } else { ?>
                        <p class="text-red">Oops! You don't have any content in your Studio.</p>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<div class="pull-right">
        <?php
        if ($data) {
            $opts = array('class' => 'pagination m-t-0');
            $this->widget('CLinkPager', array(
                'currentPage' => $pages->getCurrentPage(),
                'itemCount' => $item_count,
                'pageSize' => $page_size,
                'maxButtonCount' => 6,
                "htmlOptions" => $opts,
                'selectedPageCssClass' => 'active',
                'nextPageLabel' => '&raquo;',
                'prevPageLabel'=>'&laquo;',
                'lastPageLabel'=>'',
                'firstPageLabel'=>'',
                'header' => '',
            ));
        }
        ?>
    </div>
    
<div class="h-40"></div>

<!-- Upload Popup  start -->
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
  <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
	<div style="float:left;font-weight:bold;">File Upload Status</div>
	<div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
  </div>
  <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<!-- Upload Popup  end -->
<div class="modal fade is-Large-Modal bs-example-modal-lg" id="addvideo_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 
</div>
<!-- Modal -->
<div class="modal fade" id="Maxcontent" tabindex="-1" role="dialog" aria-labelledby="Maxcontent">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Reached Max Video Upload </h4>
            </div>
            <div class="modal-body">
                <p>You can add up to 10 contents during Free Trial. Please <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/payment/subscription">purchase a subscription</a> to continue adding contents.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-defult" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- import content Modal -->
<div class="modal fade" id="importontent" tabindex="-1" role="dialog" aria-labelledby="Maxcontent">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Import Multiple Contents</h4>
            </div>
          <form   method="post" action="<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/previewContents" class="form-horizontal" data-toggle="validator" name="uploadexcelForm" id="uploadexcelForm" enctype="multipart/form-data">
                
            <div class="modal-body">
                    <!--<div class="row">
                        <div class="col-md-6 text-left">
                            <a id="sample_file" href="javascript:void(0);" download onclick="download_sample(this)">Click here</a> to download sample .xls file
                        </div>

                        <div class="col-md-6 text-right">

                            <a id="hint_text" href="javascript:void(0);" onclick="show_hint(this)" data-toggle="tooltip" title="Hints for mandatory fields" ><em class="icon-info"></em></a> 
                        </div> 
                    </div>
                    </br>-->
                    <div class="form-group col-md-12">
                        Choose file:
                            &nbsp;&nbsp;&nbsp;<div class="btn btn-default-with-bg btn-file"> 
                               
                                 Browse<input type="file" name="import_all"  id="import_all"  onchange="import_allfile(this);" value="" /> 
                            </div>
                        
                        <span id="importfile_name"> </span>  </br>
                        
                    </div>
                    
                
            </div>
            <div class="modal-footer">
               <button type="button"  id="import_preview" class="btn btn-primary" onclick="show_preview()" disabled >Preview</button>
               <button type="button" class="btn btn-defult" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
           </form>
        </div>
    </div>
</div>

<!-- Manage Ads Modal-->
<div id="ad_popup" class="modal fade" data-backdrop="static">
        <?php 
            if(@$MonetizationMenuSettings[1]['ad_network_id'] !=3)
                echo "<div class='modal-dialog' style='border:0px solid red;width:80%'>";
            else
                echo "<div class='modal-dialog'>";
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Manage Ads</h4>
            </div>            
            <?php 
                if(@$MonetizationMenuSettings[1]['ad_network_id'] ==3)
                    echo '<div id="ad_form_area" style="height:150px"></div>';
                else
                    echo '<div id="ad_form_area"></div>';
            ?>
            
        </div>									
    </div>
</div>
<!-- Manage add items Modal-->
<div class="modal fade" id="items_popup" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;"></div>
<!--Start PPV Modal-->
<div id="ppvPopup" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Maxcontent"></div>
<!--End PPV Modal-->
<!--Start Live Stream Data Model-->
<div class="modal fade" id="showLiveStreamData"role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" align="center">Please use the below details to start streaming...</h4>
            </div>
            <div class="modal-body">

                <div class="form-horizontal">

                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">Stream Url</label>
                              <div class="col-sm-10">
                                <div class="fg-line">
                                   <input type="text" class="form-control input-sm" id="stream_url_val" readonly>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">Stream Key</label>
                              <div class="col-sm-10">

                                <div class="fg-line">
                                  <input type="text" class="form-control input-sm" id="stream_key_val" readonly>
                                </div>
                              </div>
                            </div>
                            </div>

                          </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-defult" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
<!--End Live Stream Data Model-->
<div style="display: none" id="encodingStreamId"><?php echo $videoEncodingLog; ?></div>
<div class="modal fade" id="showPlaylist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Add to playlist</h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="block">
						<div class="block-inner">
							<div class="col-xs-12">
				<?php foreach ($playlistName as $listName) { ?>
									<a href="javascript:void(0);" id="playlist_add_song" data-name="<?php echo $listName['playlist_name']; ?>"><em class="icon-playlist"></em>&nbsp;<?php echo $listName['playlist_name']; ?></a><br>
									<div class="m-t-10"></div>
									<?php } ?>
								<div id="playlist">
									<input type="hidden" id="movie_id" name="movie_id" value="" />
									<input type="hidden" id="user_id" name="user_id" value="0" />
									<input type="hidden" id="is_episode" name="is_episode" value="" />
								</div>
								<a href="javascript:void(0);" id="addnew_playlist" onclick="AddNewPlaylist();">
									<button type="submit"  class="btn btn-primary btn-default m-t-10">Create a playlist</button>
								</a>
								<div class="preloader pls-blue" id="playlist_loading">
									<svg class="pl-circular" viewBox="25 25 50 50">
									<circle class="plc-path" cx="50" cy="50" r="20"/>
									</svg>
								</div>
								<div class="mt-20"></div>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<form name="playlist_form" method="post" id="playlist_form" onsubmit="return validate_form();"  enctype="multipart/form-data"  action="<?php echo $this->createUrl('management/addToPlaylist'); ?>">

<div class="modal fade" id="addPlaylist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
			<div id="playlist_new">
				<input type="hidden" id="movie_id" name="movie_id" value="" />
				<input type="hidden" id="user_id" name="user_id" value="0" />
				<input type="hidden" id="is_episode" name="is_episode" value="" />
				<input type="hidden" id="add_content" name="add_content" value="1">
			</div>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><span id='popup-header-txt'>Add new playlist</span> </h4>
				</div>
				<div class="modal-body">
					<div class="error red text-red" id="cerror" style="display:none"></div>
					<div class="form-group">
						<label class="control-label col-sm-3">Title:<span class="red"><b>*</b></span></label>
						<div class="col-sm-9">
							<div class="fg-line">
								<input type="text"  id="new_playlist" name="newplaylist" class="form-control input-sm" value="<?php echo @$data->category_name; ?>" >	                        </div>
							<label class="new_playlist_error red"></label>
						</div>
					</div>
					<div class="form-group">
						<label for="category" class="col-md-3 control-label">Category<span class="red"><b>*</b></span>: </label>
						<div class="col-md-9">
							<div class="fg-line">
								<select  name="content_category_value[]" id="content_category_value" multiple  class="form-control input-sm checkInput" <?php echo $disable; ?>>
							<?php
							foreach ($contentList AS $k => $v) {
                                        if (@$data && in_array($k, explode(',', $data[0]['id']))) {
									$selected = "selected='selected'";
								} else {
									$selected = '';
								}
								echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
							}
							?>
								</select>
							</div>
                            <label class="playlist_category_error red"></label>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Poster:</label>
						<div class="col-sm-9">
							<input class="btn btn-default-with-bg btn-sm" type="button" name="playlist_img" value="Upload Image" data-width="<?php echo @$poster_size['width']; ?>" data-height="<?php echo @$poster_size['height']; ?>" id="playlist_img" onclick="openImageModal(this);" <?php echo $disable; ?>/>
						<span>(Upload a image of <?php echo $poster_size['width'].' x '.$poster_size['height'].'px'; ?> )</span>
                        </div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<div id="avatar_preview_div">
<?php if (@$poster != "") { ?>
									<img src="<?php echo @$poster; ?>" alt="<?php echo @$data->category_name; ?>" title="<?php echo @$data->category_name; ?>">
<?php } ?>                                   
							</div>
							<div class="canvas-image" style="width:200px;height:200px;display:none">
							<canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
							</div>
						</div>
					</div>
					<div class="preloader pls-blue" id="playlist_loading">
						<svg class="pl-circular" viewBox="25 25 50 50">
						<circle class="plc-path" cx="50" cy="50" r="20"/>
						</svg>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-default m-t-10 save_playlist" name="save_playlist" id="save_playlist" value="save">Save playlist</button>
					<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
				</div>
			</div>
	</div>
</div>

<div class="modal is-Large-Modal" id="category_img" tabindex="-1" role="dialog" aria-labelledby="category_img">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header text-left">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="section_id1" id="section_id1" value="" />
				<div role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active" onclick="hide_file()">
							<a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
						</li>
						<li role="presentation" onclick="hide_gallery()"> 
							<a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="upload_by_browse">
							<div class="row is-Scrollable">
								<div class="col-xs-12 m-t-40 m-b-20 text-center">
									<input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_to_browse('browse_cat_img')">
									<input id="browse_cat_img" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
									<p class="help-block"></p>
								</div>
								<input type="hidden" class="x1" id="x1" name="fileimage[x1]" />
								<input type="hidden" class="y1" id="y1" name="fileimage[y1]" />
								<input type="hidden" class="x2" id="x2" name="fileimage[x2]" />
								<input type="hidden" class="y2" id="y2" name="fileimage[y2]" />
								<input type="hidden" class="w" id="w" name="fileimage[w]"/>
								<input type="hidden" class="h" id="h" name="fileimage[h]"/>
								<div class="col-xs-12">
									<div class="Preview-Block row">
										<div class="col-md-12 text-center" id="upload_preview">
											<img id="preview" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="upload_from_gallery">
							<input type="hidden" name="g_image_file_name" id="g_image_file_name" />
							<input type="hidden" name="g_original_image" id="g_original_image" />
							<input type="hidden" class="x1" id="x13" name="jcrop_allimage[x13]" />
							<input type="hidden" class="y1" id="y13" name="jcrop_allimage[y13]" />
							<input type="hidden" class="x2" id="x23" name="jcrop_allimage[x23]" />
							<input type="hidden" class="y2" id="y23" name="jcrop_allimage[y23]" />
							<input type="hidden" class="w" id="w3" name="jcrop_allimage[w3]" />
							<input type="hidden" class="h" id="h3" name="jcrop_allimage[h3]" />
							<div class="row  Gallery-Row">
								<div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

								</div>
								<div class="col-md-6  is-Scrollable p-t-40 p-b-40">
									<div class="text-center m-b-20 loaderDiv"  style="display: none;">
										<div class="preloader pls-blue  ">
											<svg class="pl-circular" viewBox="25 25 50 50">
											<circle class="plc-path" cx="50" cy="50" r="20"></circle>
											</svg>
										</div>
									</div>
									<div class="Preview-Block row">
										<div class="col-md-12 text-center" id="gallery_preview">
											<img id="glry_preview" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
			</div>
		</div>
	</div>
</div>  
<input type="hidden" id="img_width" name="img_width" value="">
<input type="hidden" id="img_height" name="img_height" value="">
</form>	


<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>

<style type="text/css">
.comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}
.preloader{
    display: none;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
}
</style>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/moment.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/footable.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/custom_upload.js?v=<?php echo $v ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>	
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-multiselect.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
                        var bootboxConfirm = 0;
                        $(function () {
                            $("a.confirm").bind('click', function (e) {
                                e.preventDefault();
                                var location = $(this).attr('href');
                                var msg = $(this).attr('data-msg');
                                if ($('#dprogress_bar').is(":visible")) {
                                    var msg = 'Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.';
                                    var location = $(this).attr('href');
                                    bootbox.hideAll();
                                    bootbox.confirm({
                                        title: "Upload In progress",
                                        message: msg,
                                        html:true,
                                        buttons: {
                                            'confirm': {
                                                label: 'Stop Upload',
                                                className: 'cnfrm-btn cnfrm-succ btn-default pull-right'
                                            },
                                            'cancel': {
                                                label: 'Ok',
                                                className: 'cnfrm-btn cnfrm-cancel btn-default pull-right'
                                            }
                                        }, callback: function (confirmed) {
                                            if (confirmed) {
                                                bootboxConfirm = 1;
                                                confirmDelete(location, msg);
                                            }
                                        }
                                    });
                                } else {
                                    confirmDelete(location, msg);
                                }
                            });
				$("body").on('click','#playlist_add_song',function(){
						var playlistname = $(this).attr('data-name');
						var user_id		 = $('#playlist').find('#user_id').val();
						var movie_id	 = $('#playlist').find('#movie_id').val();
						var is_episode	 = $('#playlist').find('#is_episode').val();
						var url			 = '<?php echo $this->createUrl('management/addToPlaylist'); ?>';
						AddToPlaylist(url,user_id,movie_id,is_episode,playlistname);
					});
					$( "#new_playlist" ).keyup(function() {
						$('.new_playlist_error').html('');
                        });
					$('#addPlaylist').on('hidden.bs.modal', function () {
						$('#showPlaylist').modal('show');
					});
                       
					$("#content_category_value").change(function(){
						$('.playlist_category_error').html('');
					});
					
                        });
                       
// Validate and save the Server path for video
                        function validateURL(divid) {
                            showLoader();
                            var action_url = '<?php echo $this->createUrl('admin/validateVideo'); ?>';
                            if(divid){
                                $('#dropbox').html('');
                                var url = $('#dropbox').val();
                            }else{
                                $('#server_url_error').html('');
                                var url = $('#server_url').val();
                            }
                            var movie_id = $('#movie_id').val();
                            var movie_stream_id = $('#movie_stream_id').val();
                            if(divid){
                                var ftpusername = '';
                                var ftppassword = '';
                            }else{
                                var ftpusername = $('#ftpusername').val();
                                var ftppassword = $('#ftppassword').val();
                            }  
                            if (url && isUrlValid(url)) {
                                $.post(action_url, {'url': url, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id,'username':ftpusername,'password':ftppassword}, function (res) {
                                    showLoader(1);
                                    if (res.error) {
                                        if(divid){
                                            $('#dropbox_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                                        }else{
                                                                $('#server_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                                        }
                                    } else {
                                        if (res.is_video) {
                                            $("#addvideo_popup").modal('hide');
                                        if(divid){
                                            $('#dropbox').val('');
                                            $('#dropbox_url_error').val('');
                                        }else{
                                            $('#server_url').val('');
                                            $('#server_url_error').val('');
                                        }                    
                                        var text = '<div><a href="javascript:void(0);" data-toggle="tooltip" title="Video download in progress. It will be available within an hour"><span class="glyphicon glyphicon-cloud-download glyphicon-2x"></span> Download in progress</a></div>';
                                        $('#' + movie_stream_id).html(text);
                                        $('#video_upload_' + movie_stream_id).empty();
                                        } else {
                                            if(divid){
                                                $('#dropbox_url_error').html('Please provide the path of a valid video file');
                                            }else{
                                                $('#server_url_error').html('Please provide the path of a valid video file');
                                            }
                                        }
                                    }
                                }, 'json');
                                console.log('valid url');
                            } else {
                                showLoader(1);
                                if(divid){
                                    $('#dropbox_url_error').html('Please enter a valid url.');
                                }else{
                                    $('#server_url_error').html('Please enter a valid url.');
                                }
                            }
                        }
                        function isUrlValid(url) {
                            return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
                        }
                        function showLoader(isShow) {
                            if (typeof isShow == 'undefined') {
                                $('.loaderDiv').show();
                                $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
                                $('#profile button').attr('disabled', 'disabled');

                            } else {
                                $('.loaderDiv').hide();
                                $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
                                $('#profile button').removeAttr('disabled');
                            }
                        }
                        function confirmDelete(location, msg) {
                            swal({
                                  title: "Delete Content?",
                                  text: msg,
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                  confirmButtonText: "Yes",
                                  closeOnConfirm: true,
                                  html:true
                              }, function() {
                                  window.location.replace(location);
                              });
                        }

                        function validateHHMMSS(data)
                        {
                            var re = /^\d{2}:\d{2}:\d{2}$/;
                            var ret = data.match(re);
                            if (ret == null)
                                return false;
                            else
                            {
                                console.log(ret);
                                var parts = ret[0].split(':');
                                if (parts[1] < 0 && $parts[1] > 59 && parts[2] < 0 && $parts[2] > 59)
                                    return false
                                else
                                    return true;
                            }
                        }
                        //ad_validation
                        function validateAd(adenbled)
                        {
                            if ($('#enable_ad').is(':checked')){
                                if ($("input[name='rolltype']").is(':checked')){
                                    if ($('input[name=rolltype]:checked').val() == 2 && $('#roll_after').val() == ''){
                                        $('#ad_error').html("Enter the time to start playing ad!");
                                        return false;
                                    }
                                    else if ($('input[name=rolltype]:checked').val() == 2 && !validateHHMMSS($('#roll_after').val())){
                                        $('#ad_error').html("Enter correct time as HH:MM:SS format!");
                                        return false;
                                    }
                                    else{
                                        $('#ad-form').submit();
                                        return true;
                                    }
                                }
                                else
                                {
                                    //console.log(" ur inside --- to --- validate");
                                    <?php
                                        if(@$MonetizationMenuSettings[1]['ad_network_id'] !=3 && (@$MonetizationMenuSettings[0]['menu']&4)){
                                            ?> 
                                            var expression=0;
                                            var idval ='';
                                            var inputs = {};
                                            $('.roll_type:checked').each(function(){        
                                                values = $(this).val(); 
                                                idval = $(this).attr('id');
                                                expression += parseInt(values);
                                            });
                                            //validation ads <pre|mid|post>
                                            switch(idval){
                                                case ('preroll' || 1):
                                                        if(document.getElementById(idval).checked == true) {
                                                            var formvals = $('#ad-form').serialize();
                                                            $.post('<?php echo Yii::app()->getBaseUrl(true)?>/ads/saveadconfig', formvals, function (res){ 
                                                                //alert("res :::  " + res);
                                                                if($.trim(res) == 'saved')
                                                                    location.reload();
                                                                else
                                                                    $('#ad_error').html('Oops! You have entered incorrect details!');
                                                            });
                                                        }
                                                        else{
                                                            $('#ad_error').html("Check whether a pre-roll,mid-roll or post-roll ad!");
                                    return false;
                                }
                                                        break;
                                                case ('midroll' || 2 || 3):
                                                        if(document.getElementById(idval).checked == true) {
                                                            $('#roll_timing .ads_interval').each(function(i, elem){
                                                                if (inputs.hasOwnProperty(elem.value)) {
                                                                    //inputs[elem.value] += 1;
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    if(inputs[elem.value]==true)
                                                                        $('#ad_error').html('Mid-roll interval values should not be same.!');
                                                                    else
                                                                        $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    return false;
                                                                }else if($(this).val()==''){
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    return false;
                                                                }else if($(this).val()=='00:00:00'){
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    $('#sub-btn').attr('disabled','true');
                                                                    return false;
                                                                }else if($(this).val().length>8 || $(this).val().length<8){
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    $('#sub-btn').attr('disabled','true');
                                                                    return false;
                                                                }else {
                                                                    inputs[elem.value] = true;
                                                                    //inputs[elem.value] = 1;
                                                                    //alert(inputs[elem.value]);
                                                                    $('#ad_error').attr('style','display:none');
                                                                    $('#ad_error').html('');
                            }
                                                            });
                                                            //alert (JSON.stringify(inputs, null, 4));
                                                            if($('#ad_error').html().length>0){
//                                                                swal({
//                                                                        title: "Waring!",
//                                                                        text: "Please enter proper mid-roll interval values :\n Example (HH:MM:SS)",
//                                                                        });
                                                                return false;
                                                            }else{ 
                                                                var formvals = $('#ad-form').serialize();
                                                                $.post('<?php echo Yii::app()->getBaseUrl(true)?>/ads/saveadconfig', formvals, function (res){ 
                                                                    //alert("res :::  " + res);
                                                                    if($.trim(res) == 'saved')
                                                                        location.reload();
                            else
                                                                        $('#ad_error').html('Oops! You have entered incorrect details!');
                                                                });
                                                            }
                                                        }
                                                        else{
                                                            $('#ad_error').html("Check whether a pre-roll,mid-roll or post-roll ad!");
                                                            return false;
                                                        }
                                                        break;
                                                case ('postroll' || 6 || 5 || 7 || 4):
                                                        if(document.getElementById(idval).checked == true) {
                                                            if(expression == 6 || expression == 7){
                                                             $('#roll_timing .ads_interval').each(function(i, elem){
                                                                //alert("i ::: "+i+"===================="+" elem  ::: "+elem.value)
                                                                if (inputs.hasOwnProperty(elem.value)) {
                                                                    //inputs[elem.value] += 1;
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    if(inputs[elem.value]==true)
                                                                        $('#ad_error').html('Mid-roll interval values should not be same.!');
                                                                    else
                                                                        $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    return false;
                                                                }else if($(this).val()==''){
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    return false;
                                                                } else if($(this).val()=='00:00:00'){
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    $('#sub-btn').attr('disabled','true');
                                                                    return false;
                                                                }else if($(this).val().length>8 || $(this).val().length<8){
                                                                    $(this).focus();
                                                                    $('#ad_error').attr('style','display:block');
                                                                    $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                                                    $('#sub-btn').attr('disabled','true');
                                                                    return false;
                                                                }else {
                                                                    inputs[elem.value] = true;
                                                                    //inputs[elem.value] = 1;
                                                                    //alert(inputs[elem.value]);
                                                                    $('#ad_error').attr('style','display:none');
                                                                    $('#ad_error').html('');
                                                                }
                                                            });
                                                            //alert (JSON.stringify(inputs, null, 4));
                                                            }
                                                            if($('#ad_error').html().length>0){
//                                                                swal({
//                                                                        title: "Waring!",
//                                                                        text: "Please enter proper mid-roll interval values :\n Example (HH:MM:SS)",
//                                                                        });
                                                                return false;
                                                            }else{ 
                                                                var formvals = $('#ad-form').serialize();
                                                                $.post('<?php echo Yii::app()->getBaseUrl(true)?>/ads/saveadconfig', formvals, function (res){ 
                                                                    //alert("res :::  " + res);
                                                                    if($.trim(res) == 'saved')
                                                                        location.reload();
                                                                    else
                                                                        $('#ad_error').html('Oops! You have entered incorrect details!');
                                                                });
                                                            }
                                                        }
                                                        else{
                                                            $('#ad_error').html("Check whether a pre-roll,mid-roll or post-roll ad!");
                                                            return false;
                                                        }
                                                        break;
                                                default:
                                                        $('#ad_error').html("Check whether a pre-roll,mid-roll or post-roll ad!");
                                                        return false;
                                            }
                                        <?php 
                                        } else { ?> 
                                            //Enable Ads
                                            var formvals = $('#ad-form').serialize();
                                            $.post('<?php echo Yii::app()->getBaseUrl(true)?>/ads/saveadconfig', formvals, function (res){ 
                                                if($.trim(res) == 'saved')
                                                    location.reload();
                                                else
                                                    $('#ad_error').html('Oops! You have entered incorrect details!');
                                                return false;
                                            });
                                     <?php  }
                                    ?>
                                }
                            } 
                            else if (adenbled == 1 && !($('#enable_ad').is(':checked'))){
                                //Disable Ads
                                var formvals = $('#ad-form').serialize();
                                $.post('<?php echo Yii::app()->getBaseUrl(true)?>/ads/saveadconfig', formvals, function (res){ 
                                    if($.trim(res) == 'saved')
                                        location.reload();
                                    else
                                        $('#ad_error').html('Oops! You have entered incorrect details!');
                                });
                            }
                            else {
                                    //Not checked
                                    $('#ad_error').html("Please check enable ads for this content to save ...!");
                                    return false;
                            }
                        }
                        
                        function validateHHMM(hmval,obj){
                            $('#ad_error').html('');
                            //$('#sub-btn').removeAttr('disable');
                            if(!validateHHMMSS(hmval)){
                                //$('#ad_error').html('Enter correct time as HH:MM:SS format!');
                                $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                obj.focus();
                                $('#sub-btn').attr('disable', 'disable');
                            }
                        }
                        function validateHHMMSS(data)
                            {
                            //alert("data ::: " + data)
                            var re = /^\d{2}:\d{2}:\d{2}$/;
                            var ret = data.match(re);
                            if (ret == null){
                                $('#ad_error').attr('style','display:block');
                                $('#sub-btn').attr('disabled','true');
                                $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                return false;
                            }else if (ret == '00:00:00'){
                                $('#ad_error').attr('style','display:block');
                                $('#sub-btn').attr('disabled','true');
                                $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                return false;
                            } else {
                                $('#roll_timing .ads_interval').each(function(i, elem){
                                    //alert("length ::: " + $(this).val().length)
                                    if($(this).val()=='00:00:00'){
                                        //alert("============== inside : 1" + elem.value)
                                        $(this).focus();
                                        $('#ad_error').attr('style','display:block');
                                        $('#sub-btn').attr('disabled','true');
                                        $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                        return false;
                                    }else if($(this).val().length>8 || $(this).val().length<1){
                                        //alert("============== inside : 2" + elem.value)
                                        $(this).focus();
                                        $('#ad_error').attr('style','display:block');
                                        $('#sub-btn').attr('disabled','true');
                                        $('#ad_error').html('Enter correct time as seconds format! Example (HH:MM:SS)');
                                        return false;
                                    }else{
                                $('#sub-btn').removeAttr('disabled','false');
                                        $('#ad_error').html('');
                                        $('#ad_error').attr('style','display:none');
                                console.log(ret);
                                var parts = ret[0].split(":");
                                if (parts[1] < 0 && $parts[1] > 59 && parts[2] < 0 && $parts[2] > 59)
                                    return false
                                else
                                return true;
                            }
                                });
                        }
                        }

                        
                        $(function () {
                            $('#roll_types').hide();
                            $('#enable_ad').change(function () {
                                $('#roll_types').toggle();
                            });

                            $("#update_date").daterangepicker({
                                initialText: 'Update Date',
                                onChange: function () {
                                    $('#search_content').submit();
                                }
                            });
                            /*$('.copyToClipboard').hover(function(){
                             $('.embeded').css({"background":" url('<?php //echo Yii::app()->theme->baseUrl; ?>/img/embed_blue.jpg') no-repeat;",'border':'1px solid green;'});
                             },function(){
                             $('.embeded').css({"background":"url('<?php //echo Yii::app()->theme->baseUrl; ?>/img/embed_grey.jpg') no-repeat;",'border':'1px solid red;'});
                             });*/
                            $('.ui-priority-secondary').click(function () {
                                if ($(this).text() == 'Clear') {
                                    if (window.location.search != '') {
                                        $('#search_content').submit();
                                    }
                                }
                            })
                        });
                        function click_browse() {
                            $("#videofile").click();
                        }
                        function handle(e) {
                            if (e.keyCode === 13) {
                                $('#search_content').submit();
                            }
                            return false;
                        }
                        function openUploadpopup(movie_id, movie_stream_id, content_types_id) {
                            var name = $("#" +movie_stream_id + "_title").html();
                            if(content_types_id > 4){
                                var url = '<?php echo Yii::app()->baseUrl; ?>/admin/searchGallery/type/audio';                        
                            }else{
                                var url = '<?php echo Yii::app()->baseUrl; ?>/admin/searchGallery/type/video';
                            }
							var is_downloadable = $("#is_downloadable_" +movie_stream_id).val();
                            $.post(url, {'content_types_id': content_types_id, 'movie_id': movie_id,'is_downloadable':is_downloadable}, function (res) {
                                $("#addvideo_popup").html(res);
                                $('input[type=file]').val('');
                                $('#movie_name').val(name);
                                $('#movie_stream_id').val(movie_stream_id);
                                $('#movie_id').val(movie_id);
                                $('#pop_movie_name').html(name);
                                $('#content_types_id').html(content_types_id);
                                $('#content_type').val(content_types_id);
                                $("#addvideo_popup").modal('show');
                            });
                            $('.error').html('');
                        }
                        function openPPVpopup(obj) {
                            var content_types_id = $(obj).attr('data-content_types_id');
                            var movie_id = $(obj).attr('data-movie_id');

                            var url = '<?php echo Yii::app()->baseUrl; ?>/monetization/setPPVCategory';
                            $.post(url, {'content_types_id': content_types_id, 'movie_id': movie_id}, function (res) {
                                var name = $(obj).attr('data-name');
                                $("#ppvPopup").html(res).modal('show');
                                $('#ppv_movie_name').html(name);
                            });
                        }
                        function openManageAds(movie_stream_id,ad_networks_id) {    
                            $('#ad_form_area').html('<center><div class="loader_ads_popup"></div><center>');
                            $.post(HTTP_ROOT + "/ads/adform", {'movie_stream_id': movie_stream_id}, function (res) {
                                //Ads pop_up stylesheet information 
                                if(ad_networks_id == 3){
                                    //DFP
                                    setTimeout(function(){$('#ad_form_area').html(res)},2000);
                                } else {
                                    //SPOTX-YUME
                                    $('#ad_form_area').html('<center><div class="loader_ads_popup"></div><center>');
                                    setTimeout(function(){$('#ad_form_area').html(res)},2000);
                                }
                                
                            });
                            $("#ad_popup").modal('show');
                        }
                        $('#ad_popup').on('hidden.bs.modal', function () {
                          $('#ad_form_area').html('');
                        });

                       /* function openManageAds(movie_stream_id, enable_ad, rolltype, roll_after) {
                            $("#ad-form").trigger('reset');
                            $('#ad_stream_id').val(movie_stream_id);
                            if (enable_ad == 1)
                            {
                                $('#enable_ad').attr('checked', 'checked');
                                $('#roll_types').show();
                            }
                            else
                            {
                                $('#enable_ad').attr('checked', false);
                                $('#roll_types').hide();
                                $('#preroll').attr('checked', false);
                                $('#midroll').attr('checked', false);
                                $('#roll_after').val('');
                            }
                            if (rolltype == 1)
                                $('#preroll').attr('checked', 'checked');
                            else if (rolltype == 2)
                                $('#midroll').attr('checked', 'checked');
                            if (roll_after != '' && roll_after != 0)
                                $('#roll_after').val(roll_after);
                            else
                                $('#roll_after').val('');
                            $("#ad_popup").modal('show');
                        }
*/
                        var url = '<?php echo Yii::app()->baseUrl; ?>';
                        $(function () {
                            var mov_name = "";
                            $('#dprogress_bar').draggable({
                                containment: 'window',
                                scroll: false
                            });
                        });
                        // remove Video
                        function deleteVideo(movie_id) {
                            if (bootbox.confirm('Are you sure you want to remove this video from the movie? \n This video will parmanetly deleted from the movie.')) {
                                $.post(HTTP_ROOT + "/admin/removeVideo", {'is_ajax': 1, 'movie_id': movie_id}, function (res) {
                                    if (res.error == 1) {
                                        alert('Error in removing movie');
                                    } else {
                                        $('#fmovie_name').html("<small><em>Not Available</em></small>&nbsp <a href='javascript:void(0);' onclick='openUploadpopup();'>Upload Video</a>");
                                        alert('Your movie video removed successfully');
                                    }
                                }, 'json');
                            }
                        }
                        function manage_progressbar() {
                            $("#all_progress_bar").toggle('slow');
                        }
                        clientTarget = new ZeroClipboard($('.copyToClipboard'), {
                            moviePath: "<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.swf",
                            debug: false
                        });
                        function CopytoClipeboard(id) {
                            $('#' + id).css('display', 'block');
                            $('#' + id).animate({
                                opacity: 0,
                                top: '-=75',
                            }, {
                                easing: 'swing',
                                duration: 500,
                                complete: function () {
                                    $('#' + id).css({'display': 'none', 'opacity': 1, top: '+=75'});
                                }
                            });
                        }
                        function openEmbedBox(embedid) {
                            /*if ($('#' + embedid).is(':visible')) {
                                $('#' + embedid).fadeOut(1000)
                            } else {
                                $('#' + embedid).fadeIn(1000);
                            }*/
                            $('#'+embedid).modal('show');
                            }

                        window.addEventListener("beforeunload", function (e) {
                            //
                            if ($('#dprogress_bar').is(":visible") && !bootboxConfirm) {
                                //var confirmationMessage = "\o/";
                                var confirmationMessage = "Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.";
                                //console.log(confirmationMessage);
                                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                                return confirmationMessage;                            //Webkit, Safari, Chrome
                            }
                        });

                        $(document).ready(function () {
                            $("[rel='tooltip']").tooltip();

                            $('.thumbnail').hover(
                                    function () {
                                        $(this).find('.caption').slideDown(250); //.fadeIn(250)
                                    },
                                    function () {
                                        $(this).find('.caption').slideUp(250); //.fadeOut(205)
                                    }
                            );
                            $('#filter_dropdown').change(function () {
                                $('#search_content').submit();
                            });
                            $("#filetype").change(function () {
                                $('.error').html('')
                                $('.savefile').hide();
                                $('#' + $(this).val() + '_div').show();
                            });
                            $('#filterby').change(function () {
                                showcategoryorform($(this).val(),1);                              
                        });
                            $('#form_dropdown').change(function () {
                                $('#search_content').submit();
                            });
                            $('#sortby').change(function () {
                                $('#search_content').submit();
                            });
							$('#contenttype').change(function () {
                                $('#search_content').submit();
                            });
                            showcategoryorform($('#filterby').val(),0);
                        });
                        function showcategoryorform(v,flag){
                            if(v == 1){
                                $('#show_category').hide();
                                $('#show_form').show();
                                if (flag && $('#form_dropdown').val()) {
                                    $('#search_content').submit();
                                }
                            }else{
                                $('#show_form').hide();
                                $('#show_category').show();
                                if (flag && $('#filter_dropdown').val()) {
                                    $('#search_content').submit();
                                }
                            }
                        }
                        function addvideoFromVideoGallery(videoUrl, galleryId) {
                            showLoader();
                            var action_url = '<?php echo $this->createUrl('admin/addVideoFromVideoGallery'); ?>'
                            $('#server_url_error').html('');
                            var movie_id = $('#movie_id').val();
                            var movie_stream_id = $('#movie_stream_id').val();
                            $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
                                showLoader(1);
                                if (res.error) {
                                    $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
                                } else {
                                    if(res.is_video === 'uploaded') {
                                        window.location.href = window.location.href;
                                    } else if (res.is_video) {
                                        $("#addvideo_popup").modal('hide');
                                        $('#server_url').val('');
                                        $('#server_url_error').val('');
                                        var text = '<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>';
                                        $('#' + movie_stream_id).html(text);
                                        $('#video_upload_'+movie_stream_id).empty();
                                        if($('#encodingStreamId').is(":empty")){
                                            $('#encodingStreamId').html(movie_stream_id);
                                        } else {
                                            $('#encodingStreamId').html($('#encodingStreamId').html() + "," + movie_stream_id);
                                        }
                                    } else {
                                        $('#server_url_error').html('There seems to be something wrong with the video');
                                    }
                                }
                            }, 'json');
                            console.log('valid url');
                        }

                        function searchvideo(type)
                        {
                            var key_word = $("#search_video1").val();
                            var key_word = key_word.trim();
                            if(key_word.length>=3 || key_word.length==0){
                                $('.loaderDiv').show();                                            
                                $('#profile button').attr('disabled', 'disabled');
                                $.ajax({
                                    url: "<?php echo Yii::app()->getBaseUrl(true); ?>/admin/ajaxSearchVideo",
                                    data: {'search_word': key_word,'type':type},
                                    contentType: false,
                                    cache: false,
                                    success: function (result)
                                    {
                                        $('.loaderDiv').hide(); 
                                        $('#profile button').removeAttr('disabled');
                                        $("#video_content_div").html(result);
                                    }
                                });
                            }
                            else
                            {
                            return;

                            }
                        }
    function addMore() {               
        var txt = $("#roll_after_section_hidden").html();        
        $("#roll_timing").append(txt);
    }
    var x = 0;  var addmorecountfor_midroll =0; var rmbtn_cnt = 3;
    function addMore_new(obj) {
        $('#roll_timing .ads_interval').each(function(){
            if(addmorecountfor_midroll>0)
                addmorecountfor_midroll-=1; 
         });
        $('#sub-btn').removeAttr('disabled','false');
        if($('#ad_error').html().length>0){
            if($('#ad_error').is(':visible')){
                swal({
                        type: "warning",
                        title: "Warning!",
                    text: "Please enter proper mid-roll interval values :\n Example (HH:MM:SS)",
                    });
            return false;
            }
            else {
               $('#roll_timing .ads_interval').each(function(){
                  addmorecountfor_midroll++; 
               });
               if(addmorecountfor_midroll>2){
                 swal({    
                        type: "warning",
                        title: "Warning!",
                        text: "You can add only 3 mid-roll ad interval for streaming",
                    });
                    return false;
               } else{
                   var txt = $("#roll_after_section_hidden").html();        
                   $("#roll_timing").append(txt);
                   x+=1;
            }
           }
        } else {
            $('#roll_timing .ads_interval').each(function(){
               addmorecountfor_midroll++; 
            });
            if(addmorecountfor_midroll>2){
                swal({    
                        type: "warning",
                        title: "Warning!",
                        text: "You can add only 3 mid-roll ad interval for streaming",
                    });
            return false;
            } else{
                var txt = $("#roll_after_section_hidden").html();        
                $("#roll_timing").append(txt);
                x+=1;
                   rmbtn_cnt++;
            }
        }
    }
    function removeBox() {
        $(obj).parent().parent().remove();
    }
    
    function removeBox_new(obj) { 
        $('#sub-btn').removeAttr('disabled','false');
        if(x>0){
            x -=1;
            addmorecountfor_midroll -=1;
            $('#ad_error').html('');
        }
        if(rmbtn_cnt>0){
        $(obj).parent().parent().remove();
            rmbtn_cnt--;
            x -=1;
            addmorecountfor_midroll -=1;
            $('#ad_error').html('');
    }
    }
    $(document).ready(function(){
        $('.offline').click(function(){
            var stream_id = $(this).attr('data-stream_id');
            var offline_val=$(this).attr('data-offline_value');
            var currObj = $(this);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '<?php echo Yii::app()->getBaseUrl(); ?>/admin/openSetOffline',
                data: {'movie_stream_id': stream_id,'off_val':offline_val},
                success: function (res)
                {
                    if(res.status == 'Success'){
                        if(res.msg == 1) {                           
                            currObj.attr('data-offline_value','0');
                            currObj.html('<em class="fa fa-chevron-down"></em>&nbsp;&nbsp; Disable Offline View');
                        } else {                           
                            currObj.attr('data-offline_value','1');
                            currObj.html('<em class="fa fa-chevron-up"></em>&nbsp;&nbsp; Enable Offline View');
                        }
                    }
                }
            }); 
        });
        $('[data-toggle="tooltip"]').tooltip({
            content: function() {
                var element = $( this );
                if ( element.is( "[title]" ) ) {
                    return element.attr( "title" );
                }
            }
        });
        $('#midroll').click(function(){
           if($('#midroll').is(':checked')){
               $('#roll_timing').show();
           } else{
               $('#roll_timing').hide();
           }
        });
        $('.showcatform').multiselect({
            //enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 300,
            dropUp: true,
            selectAllText:'All',
            nonSelectedText: 'No content selected',
            nSelectedText: 'content selected',
            allSelectedText: 'All contents selected',
            numberDisplayed:1,
    });
    setInterval(function(){
        showEncodingStatus();
    },60000);
    });
    function openGeoBlock(obj){
        //var dv = '<div class="modal-dialog modal-lg"><div class="modal-content" style="height: 180px;overflow: hidden;    padding-top: 20px;text-align: center;"><h3>Loading...</h3></div></div>';
        var movie_stream_id = $(obj).attr('data-movie_stream_id');
        var movie_id = $(obj).attr('data-movie_id');
        var url = '<?php echo Yii::app()->baseUrl; ?>/monetization/OpenGeoCategory';
        //$("#ppvPopup").html(dv).modal('show');
        $.post(url,{'movie_id':movie_id,'movie_stream_id':movie_stream_id}, function (res) {
            $("#ppvPopup").html(res).modal('show');
        });
    }
                                                                    
function reset_filter()
{
 $("#video_duration").val(0); 
 $("#file_size").val(0);
 $("#is_encoded").val(0);
 $("#uploaded_in").val(0);  
 view_filtered_list();
}



function view_filtered_list(){
         processing=false;
         count=1;
         maxscrollcount=16;
         tempScrollTop=0; currentScrollTop = 0;
       showLoader();
       var video_duration =$("#video_duration").val();
       var file_size =$("#file_size").val();
       var is_encoded =$("#is_encoded").val();
       var uploaded_in =$("#uploaded_in").val();
     
    var url = "<?php echo Yii::app()->baseUrl; ?>/admin/ajaxFiltervideo/video_duration/" + video_duration + "/file_size/" + file_size +"/is_encoded/" + is_encoded+"/uploaded_in/" + uploaded_in;   
     //window.location.href = url;
       $.post(url, {'video_duration': video_duration,'file_size':file_size, 'is_encoded': is_encoded,'uploaded_in': uploaded_in}, function (res) {
            if (res) {
                $('#video_content_div').html();
                 showLoader(1);
                 
          $('#video_content_div').html(res);      
           }
       });
       }

     function audio_filtered_list(){
        
         processing=false;
         count=1;
         maxscrollcount=16;
         tempScrollTop=0; currentScrollTop = 0;
       showLoader();
       var video_duration =$("#video_duration").val();
       var file_size =$("#file_size").val();
       var is_encoded =$("#is_encoded").val();
       var uploaded_in =$("#uploaded_in").val();
     
    var url = "<?php echo Yii::app()->baseUrl; ?>/admin/ajaxFilterAudio/video_duration/" + video_duration + "/file_size/" + file_size +"/is_encoded/" + is_encoded+"/uploaded_in/" + uploaded_in;   
     //window.location.href = url;
       $.post(url, {'video_duration': video_duration,'file_size':file_size, 'is_encoded': is_encoded,'uploaded_in': uploaded_in}, function (res) {
            if (res) {
                $('#video_content_div').html();
                 showLoader(1);
                 
          $('#video_content_div').html(res);      
           }
       });
       }

    function embedFromThirdPartyPlatform() {
	var str =$("#embed_url").val();
            var videoUrl1 = "youtube.com";
            var videoUrl2 = "vimeo.com"; 
            var videoUrl3 = 'm3u8';
            var videoUrl4 = 'iframe';
            var videoUrl5 = "dailymotion.com";
          if(str.length ==0){
            $('#save-btn').attr('disabled', 'disabled');
            return false;
           }else if(str.length){
               // console.log((str.indexOf(videoUrl4))+'.......'+(str.indexOf(videoUrl3))+'.....'+(str.indexOf(videoUrl2))+'.....'+(str.indexOf(videoUrl1)));
                    if ((str.indexOf(videoUrl3) != -1) ||
                       (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl1) != -1)  ||
                       (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl2) != -1) ||
                       (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl5) != -1))
                    {                         
                        showLoader();
                        var embed_text =$("#embed_url").val();
                        var action_url = '<?php echo $this->createUrl('admin/embedFromThirdPartyPlatform'); ?>'
                        $('#embed_url_error').html('');
                        var movie_id = $('#movie_id').val();
                        var movie_stream_id = $('#movie_stream_id').val();
                        $.post(action_url, {'thirdparty_url': embed_text,'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
                        showLoader(1);
                        window.location.href = window.location.href;
                        });
                    }else{
                        swal("Oops! Provide a valid m3u8 or iframe embed url");
                        return false;
                }
            }
       }
    function embedThirdPartyPlatform(){
        var embedurl = $("#embed_url").val();
        if( embedurl.length == 0){
            $('#save-btn').attr('disabled', 'disabled'); 
        }else{
            $('#save-btn').removeAttr('disabled');
        return true;
        }
    }
    function openAddItems(obj) {
        var content_id = $(obj).attr('data-content_id');

        var url = '<?php echo Yii::app()->baseUrl; ?>/admin/OpenAddItem';
        //$("#ppvPopup").html(dv).modal('show');
        $.post(url, {'content_id': content_id}, function (res) {
            $("#items_popup").html(res).modal('show');
        });
    }
    function openRelatedContent(obj) {
        var content_id = $(obj).attr('data-content_id');
        var content_stream_id = $(obj).attr('data-content_stream_id');
        var url = '<?php echo Yii::app()->baseUrl; ?>/admin/RelatedContent';
        //$("#ppvPopup").html(dv).modal('show');
        $.post(url, {'content_id': content_id,'content_stream_id':content_stream_id}, function (res) {
            $("#items_popup").html(res).modal('show');
        });
    }
    function showLoader1(isShow) {
      
        if (typeof isShow == 'undefined') {
            
            $('#loaderDiv').show();
            $('#import_all').attr('disabled', 'disabled');
            $('#importontent button').attr('disabled', 'disabled');

        } else {
            
            $('#loaderDiv').hide();
            $('#import_all').removeAttr('disabled');
            $('#importontent button').removeAttr('disabled');
        }
    }
    function show_hint(ele)
    {
      $("#hintmetadata").modal("show"); 
    }
    function download_sample(ele)
    {
    var url="<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/downloadsample";
    window.location =url;
    
    }
    function show_preview()
    {
        $('#uploadexcelForm')[0].submit();
    }
    
     function import_allfile(oInput)
     {
    var _validFileExtensions = [".xls", ".csv",".xlsx"];    
    $("#importfile_name").html($("#import_all").val());
    if (oInput.type == "file") {
         var sFileName = oInput.value;
          if (sFileName.length > 0) {
             var blnValid = false;
             for (var j = 0; j < _validFileExtensions.length; j++) {
                 var sCurExtension = _validFileExtensions[j];
                 if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                     blnValid = true;
                  $('#import_preview').removeAttr('disabled');
                 }
             }

             if (!blnValid) {
                 swal("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                 oInput.value = "";
                 $('#import_preview').attr("disabled","disabled");
                 $("#importfile_name").html("");
                 return false;
             }
         }
         
     }
     return true;
      
     }
     /*
        * modified :   Arvind 
        * email    :   aravind@muvi.com
        * reason   :   Audio Gallery :  
        * functionality : addaudioFromAudioGallery
        * date     :   06-02-2017
        */
     function addaudioFromAudioGallery(videoUrl, galleryId,audio_name) {
        showLoader();
        var action_url = '<?php echo $this->createUrl('admin/addAudioFromAudioGallery'); ?>'
        $('#server_url_error').html('');
        var movie_id = $('#movie_id').val();
        var movie_stream_id = $('#movie_stream_id').val();
        $.post(action_url, {'audio_name':audio_name,'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
            showLoader(1);
            if (res.error) {
                $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
            } else {
                if(res.is_audio === 'uploaded') {
                    window.location.href = window.location.href;
                } else if (res.is_audio) {
                    $("#addvideo_popup").modal('hide');
                    $('#server_url').val('');
                    $('#server_url_error').val('');
                    var text = '<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>';
                    $('#' + movie_stream_id).html(text);
                } else {
                    $('#server_url_error').html('There seems to be something wrong with the video');
                }
            }
        }, 'json');
        console.log('valid url');
    }
    $(document).on('click', '.panel-heading', function(e){
        var $this = $(this);
            if(!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel-success').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
            } else {
                $this.parents('.panel-success').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
            }
    })
    function testFFmpeg(val){
        var action_url1 = '<?php echo $this->createUrl('admin/GetAudioDuration'); ?>';
        $.post(action_url1, {'filename': val},function (res){
            alert(res)
        });
    }
    function testFFmpegBitRate(val){ 
        var action_url1 = '<?php echo $this->createUrl('admin/GetAudioBitRate'); ?>';
        $.post(action_url1, {'filename': val},function (res){
            alert(res) 
        });
    }
    function getInformations3(videoUrl, galleryId) {
      var action_url1 = '<?php echo $this->createUrl('admin/getAudioFromAudioGallery'); ?>';
      var movie_id = $('#movie_id').val();
      var movie_stream_id = $('#movie_stream_id').val();
      //alert("movie_id ::: "+ movie_id + "===================== "+ "movie_stream_id :::" + movie_stream_id);
      $.post(action_url1, {'audio_id': movie_id},function (res){
          console.log(res)
          var resdata = new Array();
          resdata = JSON.parse(res)
          alert(resdata['data']['is_episode'])
          alert(resdata['data']['url'])
      });
  }
  function contentisphysical(){      
      $("input[name='searchForm[is_physical]']").val($('#contentisphysical').val());
      $('#search_content').submit();
  }
    
    function liveStreamingStatus(movie_id,status){
        if(status == 0){
            swal({
                title: "<em class='fa fa-warning'></em>&nbsp;&nbsp; Please stop your stream from encoder",
                html: true
            });
            return false;
        } else{
            var updateUrl = '<?php echo $this->createUrl('admin/updateLivestreamStatus'); ?>';
            $.post(updateUrl, {'movie_id': movie_id,'status':status},function (res){
                res = $.trim(res);
                if(res == "success"){
                    if(status == 1){
                        $('#live_streaming_'+movie_id).html('<span class="red" onclick="liveStreamingStatus(' + movie_id + ',0)"><em class="icon-control-pause left-icon"></em> Stop Streaming</span>');
                    } else if(status == 0){
                        $('#live_streaming_'+movie_id).html('<span class="blue" onclick="liveStreamingStatus(' + movie_id + ',1)"><em class="icon-control-play left-icon"></em> Start Streaming</span>');
                    }
                } else if(res == "videosync"){
                    swal({
                        title: "<em class='fa fa-warning'></em>&nbsp;&nbsp; Recorded video is being saved to video library. Please wait to start streaming again.",
                        html: true
                    });
                    return false;
                }else{
                    var statusCont = 'Started';
                    if(status == 0){
                        statusCont = 'Stopped';
                    }
                    swal("Due to some reason streaming could not be " + statusCont);
                    return false;
                }
            });
        }
    }
    function showLiveStreamData(streamUrl,streamName) {
        $("#stream_url_val").val(streamUrl);
        $("#stream_key_val").val(streamName);
        $("#showLiveStreamData").modal('show');
    }
    
    function showEncodingStatus(){
        var encodingStreamIds = $('#encodingStreamId').html();
        if(encodingStreamIds == ""){
            return false;
        }
        $.post('<?php echo $this->createUrl('admin/encodingStatus'); ?>', {'encodingStreamIds':encodingStreamIds},function (res){
            if(res.msg == 'referesh'){
                window.location.href = window.location.href;
            } else{
                console.log(res);
                jQuery.each(res, function(i, val) {
                    if(val != ''){
                        $("#" + i).html('<h5>Encoding : In Progress &nbsp;&nbsp;<em class="fa fa-refresh fa-spin" style="color:#2E7D32;"></em></br><span class="encodingTimeRemaning">Time remaining: ' + val + '</span></h5>');
                    } else{
                        $("#" + i).html('<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>');
                    }
                });
            }
        }, 'json');
    }
	function showPlaylist(movie_id,is_episode){
		$('#movie_id').val(movie_id);
		$('#is_episode').val(is_episode);
		$("#showPlaylist").modal('show');
	}
	function AddToPlaylist(url,user_id,movie_id,is_episode,playlistname){
		$.post(url, {'user_id': user_id, 'is_episode': is_episode, 'movie_id': movie_id, 'newplaylist': playlistname,'ajax_call':1}, function (data) {
				 var res = JSON.parse(data);
			if (res.status == "Success") {
				$('#playlist_loading').hide();
				if(movie_id != ''){
					$('#addToPlaylist').modal('hide');
				}
				$('#showPlaylist').modal('hide');
				swal(res.msg, '', "success");
			} else {
				$('#playlist_loading').hide();
				$('#showPlaylist').modal('hide');
				swal(res.msg, '', "error");
			}
		});
	}
	function validate_form() {
		var playlist_name = $("#new_playlist").val();
		var playlist_category = $('#content_category_value').val();
		var regex = /^[a-zA-Z ]*$/;
		if (playlist_name == "") {
			$(".new_playlist_error").html("Playlist Name Cannot be blank");
			return false;
		}
		
		if (playlist_name.trim() === "")
		{
			$(".new_playlist_error").html("Playlist Name Cannot be blank Space!");
			return false;
		}

		else if (playlist_category == null) {
			$(".playlist_category_error").html("Playlist category cannot be blank");
			return false;
		}
		else if (playlist_name == "" && playlist_category == '') {
			$(".new_playlist_error").html("Name Can not be blank!");
			$(".playlist_category_error").html("Please fill the details");
			return false;
		}else if (playlist_name != ""){
			$(".new_playlist_error").html("");
		}else if(playlist_category){
		$(".playlist_category_error").html("");
		}

		}
 
	function AddNewPlaylist() {
		var movie_id = $('#playlist #movie_id').val();
		var is_episode = $('#playlist #is_episode').val();
		$('#playlist_new #movie_id').val(movie_id);
		$('#playlist_new #is_episode').val(is_episode);
		$('#addPlaylist').modal('show');
		$('#showPlaylist').modal('hide');
		$('#new_playlist').val('');
		$('#content_category_value option').removeAttr('selected');
		$('.img-playlist-edit').attr('src','');
		$('.canvas-image').addClass('hide');
	}
	function openImageModal(obj) {
		var width = $(obj).attr('data-width');
		var height = $(obj).attr('data-height');
		$(".help-block").html("Upload a transparent image of size " + width + " x " + height + 'px');
		$("#img_width").val(width);
		$("#img_height").val(height);
		$("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
		$("#category_img").modal('show');
	}
	function click_to_browse(modal_file) {
		$("#" + modal_file).click();
	}
	function fileSelectHandler() {
		document.getElementById("g_original_image").value = "";
		document.getElementById("g_image_file_name").value = "";
		var img_width = $("#img_width").val();
		var img_height = $("#img_height").val();
		$(".jcrop-keymgr").css("display", "none");
		$("#celeb_preview").removeClass("hide");
		$('#uplad_buton').removeAttr('disabled');
		var oFile = $('#browse_cat_img')[0].files[0];
		var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
		if (!rFilter.test(oFile.type)) {
			swal('Please select a valid image file (jpg and png are allowed)');
			$("#celeb_preview").addClass("hide");
			document.getElementById("browse_cat_img").value = "";
			$('#uplad_buton').attr('disabled', 'disabled');
			return;
		}
		var aspectratio = img_width / img_height;
		var img = new Image();
		img.src = window.URL.createObjectURL(oFile);
		var width = 0;
		var height = 0;
		img.onload = function () {
			var width = img.naturalWidth;
			var height = img.naturalHeight;
			window.URL.revokeObjectURL(img.src);
			if (width < img_width || height < img_height) {
				swal('You have selected small file, please select one bigger image file');
				$("#celeb_preview").addClass("hide");
				document.getElementById("browse_cat_img").value = "";
				$('#uplad_buton').attr('disabled', 'disabled');
				return;
			}
			var oImage = document.getElementById('preview');
			var oReader = new FileReader();
			oReader.onload = function (e) {
				$('.error').hide();
				oImage.src = e.target.result;
				oImage.onload = function () { // onload event handler
					if (typeof jcrop_api != 'undefined') {
						jcrop_api.destroy();
						jcrop_api = null;
						$('#preview').width(oImage.naturalWidth);
						$('#preview').height(oImage.naturalHeight);
						$('#glry_preview').width("450");
						$('#glry_preview').height("250");
					}
					$('#preview').css("display", "block");
					$('#celeb_preview').css("display", "block");
					$('#preview').Jcrop({
						minSize: [img_width, img_height], // min crop size
						aspectRatio: aspectratio, // keep aspect ratio 1:1
						boxWidth: 450,
						boxHeight: 250,
						bgFade: true, // use fade effect
						bgOpacity: .3, // fade opacity
						onChange: updateInfo,
						onSelect: updateInfo,
						onRelease: clearInfo
					}, function () {
						var bounds = this.getBounds();
						boundx = bounds[0];
						boundy = bounds[1];
						jcrop_api = this;
						jcrop_api.setSelect([10, 10, img_width, img_height]);
					});
				};
			};
			oReader.readAsDataURL(oFile);
		};
	}
	function showLoader(isShow) {
		if (typeof isShow == 'undefined') {
			$('.loaderDiv').show();
			$('button').attr('disabled', 'disabled');
		} else {
			$('.loaderDiv').hide();
			$('button').removeAttr('disabled');
		}
	}
	function toggle_preview(id, img_src, name_of_image)
	{
		$('#glry_preview').css("display", "block");
		document.getElementById("browse_cat_img").value = "";
		showLoader();
		var img_width = $("#img_width").val();
		var img_height = $("#img_height").val();
		var aspectratio = img_width / img_height;
		var image_file_name = name_of_image;
		var image_src = img_src;
		clearInfo();
		$("#g_image_file_name").val(image_file_name);
		$("#g_original_image").val(image_src);
		var res = image_file_name.split(".");
		var image_type = res[1];
		var img = new Image();
		img.src = img_src;
		var width = 0;
		var height = 0;
		img.onload = function () {
			var width = img.naturalWidth;
			var height = img.naturalHeight;
			window.URL.revokeObjectURL(img.src);
			if (width < img_width || height < img_height) {
				showLoader(1);
				swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
				$("#celeb_preview").addClass("hide");
				$("glry_preview").addClass("hide");
				$("#g_image_file_name").val("");
				$("#g_original_image").val("");
				$('#uplad_buton').attr('disabled', 'disabled');
				return;
			}
			var oImage = document.getElementById('glry_preview');
			showLoader(1)
			oImage.src = img_src;
			oImage.onload = function () {
				if (typeof jcrop_api != 'undefined') {
					jcrop_api.destroy();
					jcrop_api = null;
					$('#glry_preview').width(oImage.naturalWidth);
					$('#glry_preview').height(oImage.naturalHeight);
					$('#preview').width("450");
					$('#preview').height("250");
				}
				$("#glry_preview").css("display", "block");
				$('#gallery_preview').css("display", "block");
				$('#glry_preview').Jcrop({
					minSize: [img_width, img_height], // min crop size
					aspectRatio: aspectratio, // keep aspect ratio 1:1
					boxWidth: 450,
					boxHeight: 250,
					bgFade: true, // use fade effect
					bgOpacity: .3, // fade opacity
					onChange: updateInfoallImage,
					onSelect: updateInfoallImage,
					onRelease: clearInfoallImage
				}, function () {
					var bounds = this.getBounds();
					boundx = bounds[0];
					boundy = bounds[1];
					jcrop_api = this;
					jcrop_api.setSelect([10, 10, img_width, img_height]);
				});
			};
		};

	}
	function hide_file()
	{
		$('#glry_preview').css("display", "none");
		$('#celeb_preview').css("display", "none");
		$('#preview').css("display", "none");
		document.getElementById('browse_cat_img').value = null;
	}
	function hide_gallery()
	{
		$('#preview').css("display", "none");
		$("#glry_preview").css("display", "block");
		$('#gallery_preview').css("display", "none");
		$("#g_image_file_name").val("");
		$("#g_original_image").val("");
	}
	function seepreview(obj) {
		if ($('#g_image_file_name').val() == '')
			curSel = 'upload_by_browse';
		else
			curSel = 'upload_from_gallery';
		if ($('#' + curSel).find(".x13").val() != "") {
			$(obj).html("Please Wait");
			$('#category_img').modal({backdrop: 'static', keyboard: false});
			posterpreview(obj, curSel);
			$('#category_img').modal('hide');
		} else {
			if ($("#celeb_preview").hasClass("hide")) {
				$('#category_img').modal('hide');
				$(obj).html("Next");
			} else {
				$(obj).html("Please Wait");
				$('#category_img').modal({backdrop: 'static', keyboard: false});
				if ($('#' + curSel).find(".x13").val() != "") {
					posterpreview(obj, curSel);
				} else if ($('#' + curSel).find('.x1').val() != "") {
					posterpreview(obj, curSel);
				} else {
					$('#category_img').modal('hide');
					$(obj).html("Next");
				}
			}
		}
	}
	function posterpreview(obj, curSel) {
		$('.canvas-image').show();
		$("#previewcanvas").show();
		var canvaswidth = $("#img_width").val();
		var canvasheight = $("#img_height").val();
		var x1 = $('#' + curSel).find('.x1').eq(0).val();
		var y1 = $('#' + curSel).find('.y1').eq(0).val();
		var width = $('#' + curSel).find('.w').val();
		var height = $('#' + curSel).find('.h').val();
		var canvas = $("#previewcanvas")[0];
		var context = canvas.getContext('2d');
		var img = new Image();
		img.onload = function () {
			canvas.height = canvasheight;
			canvas.width = canvaswidth;
			context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
			//$('#imgCropped').val(canvas.toDataURL());
		};
		$("#avatar_preview_div").hide();
		$('.canvas-image').removeClass('hide');
		if ($('#g_image_file_name').val() == '') {
			img.src = $('#preview').attr("src");
		} else {
			img.src = $('#glry_preview').attr("src");
		}
		//$('#category_img').modal('hide');
		//$(obj).html("Next");
	}
</script>
