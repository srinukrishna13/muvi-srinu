<?php

class ReferralCustomerCommision extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'referral_customer_commision';
    }

     
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
 public function get_linked_studio($ref_id){
     $sql = Yii::app()->db->createCommand()
        ->select('r.studio_id,s.is_subscribed,s.domain,s.created_dt,s.name')
        ->from('referral_customer_commision r')
        ->join('studios s','s.id = r.studio_id')
        ->where('r.referrer_id =:ref_id and r.no_of_commision >:no_of_com',array(':ref_id'=>$ref_id,':no_of_com' => 0))
        ->queryAll();
     return$sql;
 }
}
