<?php

class CouponOnly extends CActiveRecord {

	public static function model($className = __CLASS__) {
        return parent::model($className);
    }
 
    public function tableName() {
        return 'voucher';
    }

	public function getcoupon_data($studio_id, $searchText = '') {
        $db_user = Yii::app()->db;
        $data = $db_user->createCommand()
                ->select('*')->from('voucher')
				->where('status=1 AND studio_id=:studio_id AND voucher_code LIKE :voucher_code', array(':studio_id' => $studio_id, ':voucher_code' => '%' . $searchText . '%'))
                ->queryAll();
       return $data;
     }
     
	public function checkVoucher($studio_id = 0) {
		if ($studio_id == 0) {
             $studio_id = Yii::app()->common->getStudiosId();
         }
         $data = Yii::app()->db->createCommand()
                 ->select('count(*) as no_voucher')->from('voucher')
				->where('studio_id=:studio_id AND status=1', array(':studio_id' => $studio_id))
				->queryRow();
		if (isset($data['no_voucher']) && $data['no_voucher'] > 0) {
			return $data['no_voucher'];
		} else {
             return 0;
         }
     }
     
	public function getVoucherCode($voucher_id = 0, $studio_id = 0) {
		if ($studio_id == 0) {
             $studio_id = Yii::app()->user->studio_id;
         }
         $data = Yii::app()->db->createCommand()
                 ->select('*')->from('voucher')
				->where('studio_id=:studio_id AND id=:voucher_id AND status=1', array(':studio_id' => $studio_id, ':voucher_id' => $voucher_id))
				->queryRow();
		if (!empty($data) && count($data) > 0) {
			return $data['voucher_code'];
		} else {
             return 0;
         }
     }
     
	public function getcoupon_history($voucher_code) {
       $db_user = Yii::app()->db;
        $history_data = $db_user->createCommand()
                ->select('*')->from('voucher')
				->where('status=1 AND voucher_code=:voucher_code', array(':voucher_code' => $voucher_code))
                ->queryRow();
       return $history_data;
     }

	public function deleteVoucher($studio_id, $id) {
       $db_user = Yii::app()->db;
        $updated_date = date('Y-m-d H:i:s');
		if (trim($id)) {
			$delcoupon_sql =  $db_user->createCommand()
							->update('voucher', array('status'=>0, 'updated_date'=>$updated_date, 'updated_by'=>$studio_id), 'studio_id=:studio_id AND id IN('.$id.')', array(':studio_id'=>$studio_id));
     }
        return $delcoupon_sql;
     }
     
	public function getCouponSearchData($studio_id, $voucher_code) {
       $db_user = Yii::app()->db;
       $history_data = $db_user->createCommand()
                       ->select('*')->from('voucher')
				->where('status=1 AND studio_id=:studio_id AND voucher_code LIKE :voucher_code', array(':studio_id' => $studio_id, ':voucher_code' => '%' . $voucher_code . '%'))
				->order('id DESC')
                       ->queryAll();
       return $history_data;
     }
         
	function getContentInfo($str, $adv = '') {
		$cnt = explode(',',$str);
                $ar = array();
                for($r=0;$r<count($cnt);$r++){
                    if($cnt[$r]!=""){
                        $cnt2 = explode(':',$cnt[$r]);
                        $cond = "";
                        $get = "";
                        if($cnt2[0]!=""){
                            $cond .= " AND f.id = '$cnt2[0]'";
                            $get .= "f.name";
                        }if($cnt2[1]!=""){
                            $cond .= " AND ms.series_number = '$cnt2[1]'";
                            $get .= ", ms.series_number";
                        }if($cnt2[2]!=""){
                            $cond .= " AND ms.id = '$cnt2[2]'";
                            $get .= ", ms.episode_title";
                        }
                        
                        $sql = "SELECT ".$get." FROM films f, movie_streams ms WHERE f.id = ms.movie_id".$cond."";
                        $cnt_data = Yii::app()->db->createCommand($sql)->queryAll();

                        array_push($ar,$cnt_data[0]);
                    }
                }

		if (!empty($ar)) {
                    $ar_cnt = array();
			for ($c = 0; $c < count($ar); $c++) {
                        $cnt_val = "";
				if ($ar[$c]['name'] != "") {
                            $cnt_val .= $ar[$c]['name'];
                        }
				if ($adv != 1) {
					if ($ar[$c]['series_number'] != 0) {
						$cnt_val .= "-Season-" . $ar[$c]['series_number'];
                        }
					if ($ar[$c]['episode_title'] != "") {
						$cnt_val .= "-" . $ar[$c]['episode_title'];
                        }
                        }
                        array_push($ar_cnt, $cnt_val);
                    }
			$f_cont = implode(',', $ar_cnt);
                }
           
        return $f_cont;
     }

}

?>