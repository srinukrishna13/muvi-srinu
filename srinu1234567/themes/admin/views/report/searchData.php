<input type="hidden" class="data-count" value="<?php echo $results['count']?>" />
<thead>
        <th>#</th>
        <th data-hide="phone">Keyword</th>
        <th data-hide="phone">Category</th>
        <th>Searches</th>
    </thead>
    <tbody class="list">
        <?php 
            if($results['count']){
                if(isset($page) && $page){
                    $cnt = ($page- 1) * $page_size + 1;
                }else{
                    $cnt = 1;
                }
            foreach($results['data'] as $result){?>
            <tr>
                <td><?php echo $cnt++;?></td>
                <td class="keyword"><?php echo $result['search_string'];?></td>
                <td class="category">
                    <?php 
                        if($result['content_types_id'] == 4){
                            echo 'Live Streaming';
                        }elseif($result['is_episode']){
                            echo 'Content Episode';
                        }else{
                            echo 'Content';
                        }
                    ?>
                    
                </td>
                <td><?php echo $result['search_count'];?></td>
            </tr>
            <?php }}else{?>
            <tr>
                <td colspan="8">No Record found!!!</td>
            </tr>	
        <?php	}?>
    </tbody>