<style type="text/css">
.btn_add_movie{
	border-radius: 4px !important;
	box-shadow: 2px 2px 2px #999 !important;
}
#addmovie_popup .modal-footer{
	margin-top: 0px !important;
}
#addmovie_popup .modal-body{
	padding-bottom: 0px !important;
}
.ui-autocomplete{
	z-index: 999999;
}
.disabled-upload{
	cursor:no-drop;
	color: #ccc;
}

 .upload {
    border-top: 1px solid #CCCCCC;
    margin-top: 10px;
    padding-top: 10px;
    width: 400px;
}
.upload .progress {
    border: 1px solid #555555;
    border-radius: 3px;
    margin-top: 8px;
}
.upload .progress .bar {
    background: none repeat scroll 0 0 #3EC144;
    height: 10px;
}
ul.yiiPager{
	font-size: 14px !important;
}
.filter_dropdown{
	height: 30px;
	width: 150px;
	border-radius: box;
	font-size: 16px;
	cursor: pointer;
}
.casts a{color: #d9a000;}
</style>
<div class="box-body table-responsive no-padding">
	<div class="col-lg-12">
		<div class="col-lg-3 box-body poster-cls thumbnail" style="min-height:300px;" >
			<img src="<?php echo $this->getPoster($movieDetails[0]['movie_id'],'','original');?>" rel="tooltip" title="<?php echo $movieDetails[0]['name'];?>" />
		</div>
		<div class="col-md-9 movie-details">
			<div class="box box-primary">
				<!--<button class="btn bg-olive btn-flat margin btn_add_movie" onclick="addMovie(<?php echo Yii::app()->user->id;?>);"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Movie</button>-->
				<button class="btn bg-olive btn-flat margin btn_add_movie" onclick="activityPopup();"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Activity</button>
				<div class="pull-right">
					<?php 	
					if($data){
						$this->widget('CLinkPager', array(
							'currentPage'=>$pages->getCurrentPage(),
							'itemCount'=>$item_count,
							'pageSize'=>$page_size,
							'maxButtonCount'=>6,
							'nextPageLabel'=>'Next &gt;',
							'header'=>'',
						));
					}
					?>
				</div>	

			<table class="table table-hover">
				<tbody>
					<tr>
						<td colspan="7" style="background: #f5f5f5;">
							<div class="col-lg-12">
								<div class="col-lg-6"><strong>Name: </strong> <em><?php echo $movieDetails[0]['name'];?></em></div>
								<div class="col-lg-6"><strong>Date:</strong> <em><?php echo @$movieDetails[0]['release_date']?date('m/d/Y',strtotime(@$movieDetails[0]['release_date'])):'Not Available';?></em></div>
								<div class="clear"></div>
								<div class="col-lg-12"><strong>Description:</strong> <em><?php echo @$movieDetails[0]['story']?@$movieDetails[0]['story']:'Not Available';?></em></div>
								<div class="clear"></div>
								<div class="col-lg-12 casts"><strong>Casts:</strong> 
									<?php $casts = $this->getCasts($movieDetails[0]['movie_id']);
									if($casts){
										unset($casts['actor']);
										unset($casts['director']);
										foreach($casts AS $key=>$val){
											echo "<a href='http://muvi.com/stars/".@$val['permalink']."' target='_blank'>".@$val['celeb_name']."</a>";
											if($key==4 || ((count($casts)-1)==$key)){
												break;
											}else{
												echo ", ";
											}
										}
									}else{
										echo "<em>Not Available</em>";
									}?>
								</div>
								<div class="clear"></div>
							</div>
						</td>
					</tr>
					<tr>
						<th>S/L#</th>
						<th>Title</th>
	<!--                <th>Date</th>-->
						<th>Action</th>
					</tr>
						<?php
						$cnt=(($pages->getCurrentPage())*10)+1;
						if($data){
							$subdomain = isset(Yii::app()->user->studio_subdomain)?Yii::app()->user->studio_subdomain:SUB_DOMAIN;
							
							 $series_number='';
							 $movie_stream_id = $movieDetails[0]['movie_stream_id'];
							foreach($data AS $key=>$details){

								$posterImg = $this->getEpisodePoster($details['id']);
								$posterImg = str_replace('/original/', '/medium/', $posterImg);
								$img_path = $posterImg;
		//						 if(@$details['thumb_image']){
		//						   $img_path = $details['thumb_image'];
		//						 }else{
		//						   $img_path = Yii::app()->baseUrl.'/images/no-image.png';
		//						 }
								 $movie_id = $details['movie_id'];
								 //echo $movie_id;exit;
								 $stream_id = $details['id'];
								 $freeviewcount = isset($viewcount[$movie_id]['f'])?$viewcount[$movie_id]['f']:0;
								 $paidviewcount = isset($viewcount[$movie_id]['p'])?$viewcount[$movie_id]['p']:0;
								 //$plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink']:str_replace(' ', "-",strtolower($details['episode_title']));

						?>
						<tr>
							<td><?php echo $cnt++;?> </td>
							<td>
								<strong><a href="javascript:void(0);"><span id="atitle_<?php echo $stream_id;?>"><?php echo $details['episode_title'];?></span></a></strong></td>
	<!--						<td>
								<input type="hidden" name="activity_dt" id="activity_dt_<?php echo $stream_id;?>" value="<?php echo date('m/d/Y',strtotime($details['episode_date']));?>" />
								<?php if($details['episode_date']){echo date('jS M Y',strtotime($details['episode_date']));}else{echo date('jS M Y',strtotime($details['created_at']));};?></a>
							</td>-->
							<td>
								<span id="<?php echo $details['id'];?>">
								<?php if(!$details['full_movie']){?>
									<a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id;?>,'<?php echo addslashes($details['episode_title']);?>',<?php echo $details['id'];?>);"><span class="glyphicon glyphicon-upload" title="Upload Video"></span></a> &nbsp; 
									<?php }else{?>
									<a href="<?php echo $this->createUrl('video/play_video',array('movie'=>$movie_id,'episode'=>$details['id'],'preview'=>true));?>" target="_blank"><span class="glyphicon glyphicon-play-circle"></span></a>&nbsp; 
								<?php }?>
								</span>
								<a href="javascript:void(0);" onclick="editActivity(<?php echo $movie_id.','.$stream_id;?>);"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a> &nbsp; 
								<a href="<?php echo $this->createUrl('admin/removeEventActivity',array('movie_id'=>$movie_id,'movie_stream_id'=>$details['id']));?>" onclick="return confirm('Are you sure you want to remove the activity from Event?')"><span class="glyphicon glyphicon-remove" title="Remove"></span></a>
							</td>
						</tr>
				<?php	}
					}else{?>
						<tr>
							<td colspan="5"><p class="text-red">Opps! You don't have any activity for the event.</p></td>
						</tr>
					<?php }  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

<!-- Add Activity popup -->
<div id="addActivityPopup" class="modal fade">
    <div class="modal-dialog">
		<form action="<?php echo $this->createUrl('admin/addActivity');?>" method="post" enctype="multipart/form-data" id="activityForm" data-toggle="validator" name="activityForm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Activity</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
					<label for="activitytitle">Title</label>
					<input type="text" id="title" name="activity[title]" value="" class="form-control" placeholder="Enter activity title" required>
				</div>
<!--                <div class="form-group">
					<label for="uploadposter">Date</label>
					<input type="text" id="activity_date" name="activity[activity_date]" value="" class="form-control">
				</div>-->
            </div>
			<input type="hidden" value="" name="editflag"  id="editflag" />
			<input type="hidden" value="<?php echo @$movieDetails[0]['movie_id'];?>" name="movie_id" id="event_id"  />
			<input type="hidden" value="<?php echo @$movieDetails[0]['name'];?>" name="movie_name" id="event_name"  />
			<input type="hidden" value="<?php echo @$movieDetails[0]['movie_stream_id'];?>" name="activity[movie_stream_id]" id="event_stream_id"  />
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
		</form>	
    </div>
</div>
<!-- Add Activity popup Ends -->

<!--<script type="text/javascript" src="<?php //echo Yii::app()->baseUrl;?>/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="<?php //echo Yii::app()->baseUrl;?>/js/tmpl.js"></script>
<script type="text/javascript" src="<?php //echo Yii::app()->baseUrl;?>/js/s3_direct_upload.js"></script>
<script type="text/javascript" src="<?php //echo Yii::app()->baseUrl;?>/js/application.js"></script>-->


<div style="position:fixed;border:1px solid grey;background: #FFF;left: 60%;top:70%;border-radius: 5px;display: none;" id="dprogress_bar">
  <div style="background-color:#DBEAF9;height:40px;width:440px;padding:10px;border-radius: 5px;" id="status_header">
	<div style="float:left;font-weight:bold;">File Upload Status</div>
	<div onclick="manage_progressbar();" style="float:right;">X</div>
  </div>
  <div style="padding:10px;" id="all_progress_bar"></div>
</div>

<?php include 'uploadVideo_popup.php';?>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/custom_upload.js"></script>		
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/s3_multi.js"></script>	
<script type="text/javascript">
	function openUploadpopup(movie_id,name,movie_stream_id){
		$('input[type=file]').val('');
		$('#movie_name').val(name);
		$('#movie_stream_id').val(movie_stream_id);
		$('#movie_id').val(movie_id);
		$('#contentType').val('event');
		$('#pop_movie_name').html(name);
		$("#addvideo_popup").modal('show');
	}
	function click_browse(){ 
		$("#videofile").click();
	}
	function activityPopup(){
		$("#addActivityPopup").modal('show');
	}
	var url = '<?php echo Yii::app()->baseUrl;?>';
	$(function() {
		  $('#loginModal').on('hidden', function () {
			$('#title').val("");
			$("#editflag").val('');
			$("#event_id").val('');
			$("#event_stream_id").val('');
			$("#event_name").val('');
		 });
		
//		$( "#activity_date" ).datepicker();
		var mov_name = "";var episode='';
		$('#dprogress_bar').draggable({
			appendTo: 'body',
			start: function(event, ui) {
				isDraggingMedia = true;
			},
			stop: function(event, ui) {
				isDraggingMedia = false;
			}
		});
	});

	function editActivity(movie_id,movie_stream_id){
		$('#title').val($('#atitle_'+movie_stream_id).text());
		//$('#activity_date').val($('#activity_dt_'+movie_stream_id).val());
		$('#event_id').val(movie_id);
		$('#event_stream_id').val(movie_stream_id);
		$('#editflag').val(1);
		$('#addActivityPopup').modal('show');
	}
	
	
	  // remove Video
	  function deleteVideo(movie_id){
		  if(confirm('Are you sure you want to remove this video from the Event? \n This video will parmanetly deleted from the movie.')){
			  $.post(HTTP_ROOT+"/admin/removeVideo",{'is_ajax':1,'movie_id':movie_id},function(res){
				  if(res.error==1){
					  alert('Error in removing movie');
				  }else {
					  $('#fmovie_name').html("<small><em>Not Available</em></small>&nbsp <a href='javascript:void(0);' onclick='openUploadpopup();'>Upload Video</a>");
					  alert('Your movie video removed successfully');
				  }
			  },'json');
		  }
	  }
	 function manage_progressbar(){
		$("#all_progress_bar").toggle('slow');
	  }
	
</script>