<?php
class UserRelation extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'user_relation';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getRandomPassword() {
        $acceptablePasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@#0123456789";
        $randomPassword = "";

        for ($i = 0; $i < 8; $i++) {
            $randomPassword .= substr($acceptablePasswordChars, rand(0, strlen($acceptablePasswordChars) - 1), 1);
        }
        return $randomPassword;
    }

    public function addDefaultContentType($studio_id = Null) {
        if (isset($studio_id) && trim($studio_id)) {
          	$builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand('studio_content_types', array(
                array('display_name' => 'Movie', 'permalink' => 'movie','content_category_value'=>1, 'studio_id' => $studio_id, 'content_types_id' => '1', 'muvi_alias' => 'movie','created_date' => new CDbExpression("NOW()"), 'is_enabled' => 1),
                array('display_name' => 'TV', 'permalink' => 'tv','content_category_value'=>2, 'studio_id' => $studio_id, 'content_types_id' => '3', 'muvi_alias' => 'tv','created_date' => new CDbExpression("NOW()"), 'is_enabled' => 1)
            ));
            $command->execute();
			
			$ip_address = CHttpRequest::getUserHostAddress();
			$builder1 = Yii::app()->db->schema->commandBuilder;
			$command1 = $builder1->createMultipleInsertCommand('content_category', array(
                array('category_name' => 'Movie', 'studio_id' => $studio_id, 'binary_value' => 1, 'created_date' => new CDbExpression("NOW()"), 'ip' => $ip_address),
                array('category_name' => 'TV', 'studio_id' => $studio_id, 'binary_value' => 2, 'created_date' => new CDbExpression("NOW()"), 'ip' => $ip_address)
            ));
			$command1->execute();
			
			$builder2 = Yii::app()->db->schema->commandBuilder;
			$command2 = $builder2->createMultipleInsertCommand('menu_items', array(
                array('title' => 'Movie','permalink'=>'movie', 'studio_id' => $studio_id, 'value' => 1),
                array('title' => 'TV','permalink'=>'tv', 'studio_id' => $studio_id, 'value' => 2)
            ));
			$command2->execute();
			
        } else {
            return false;
        }
    }

    public function setupStudio($userid, $studio_id, $templateName = '') {
        $usr = new User;
        $data = $usr->findByAttributes(array('id' => $userid));
        $std = new Studio;
        $stddata = $std->findByAttributes(array('id' => $studio_id));
        if (($data->studio_id == $studio_id) && $stddata->subdomain && $templateName != '') {

            $default_color = '';
            $templates = Template::model()->findByAttributes(array('code' => $templateName));
            if (count($templates)) {
                $default_color = $templates->default_color;
            }

            $user = User::model()->findByPk($data->id);
            $user->signup_step = 0;
            $user->save();
            $data->signup_step = 0;

            $studio_id = $data->studio_id;
            $studio = Studios::model()->findByPk($studio_id);

            //Setting the dummydata
            $dummy = new DummyData();
            $dummy->studio_id = $studio_id;
            $dummy->save();

            $terms_id = $this->setDefaultContent($studio_id, $data);

            $studio->terms_page = $terms_id;
            $studio->status = 1;
            $theme_folder = $stddata->subdomain;
            $studio->theme = $theme_folder;
            $studio->parent_theme = $templateName;
            $studio->default_color = $default_color;
            $studio->save();
            //$data->setState('status', 1);
            //$data->setState('theme', $theme_folder);
            //Insert the data into the Client application table 
            // Creating the folder in the name of Subdomain and copying all the files inside the sdk theme folder

            if (!is_dir(ROOT_DIR . 'themes/' . $theme_folder)) {
                mkdir(ROOT_DIR . 'themes/' . $theme_folder);
            }
            //chown(ROOT_DIR.'themes/'.$data->subdomain, 'root');
            chmod(ROOT_DIR . 'themes/' . $theme_folder, 0777);
            // shell command 
            $dest = ROOT_DIR . 'themes/' . $theme_folder . '';
            $source = ROOT_DIR . 'sdkThemes/' . $templateName;
            $return = $this->recurse_copy($source, $dest);
            return $return;
        } else {
            return false;
        }
    }

    public function setDefaultContent($studio_id = Null, $data) {
        if (isset($studio_id) && intval($studio_id)) {
            $ip_address = CHttpRequest::getUserHostAddress();

            $reason = new CancelReason();
            $reason->studio_id = $studio_id;
            $reason->reason = htmlspecialchars("Price is too expensive");
            $reason->save();

            $reason = new CancelReason();
            $reason->studio_id = $studio_id;
            $reason->reason = htmlspecialchars("Other");
            $reason->save();

            $page_name = 'About Us';
            $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
            $permalink = Yii::app()->common->formatPermalink($page_name);
            $page = new Page();
            $page->title = htmlspecialchars($page_name);
            $page->permalink = $permalink;
            $page->content = htmlspecialchars($page_content);
            $page->studio_id = $studio_id;
            $page->created_date = new CDbExpression("NOW()");
            $page->created_by = $data->id;
            $page->ip = $ip_address;
            $page->save();

            $page_name = 'Terms & Privacy Policy';
            $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
            $permalink = Yii::app()->common->formatPermalink($page_name);
            $page = new Page();
            $page->title = htmlspecialchars($page_name);
            $page->permalink = $permalink;
            $page->content = htmlspecialchars($page_content);
            $page->studio_id = $studio_id;
            $page->created_date = new CDbExpression("NOW()");
            $page->created_by = $data->id;
            $page->ip = $ip_address;
            $page->save();
            $terms_id = $page->id;
        } else {
            $terms_id = 0;
        }

        return $terms_id;
    }

    public function recurse_copy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    $this->recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
        return true;
    }
    public function save_reseller_customer_data($data = array()){
        if(count($data) > 0){
            foreach($data as $key => $val){
                $this->$key = $val;
            }
            $this->save();
        }
    }
    public function get_user_relation($user = '',$relation=''){
        $relation = $level = Yii::app()->db->createCommand()
        ->select('*')
        ->from('user_relation')
        ->where('refer_id=:referer and relation_type=:relation',array(':referer'=>$user,':relation'=>$relation))
        ->queryAll();
        return $relation;
    }
    public function get_reseller_user_record($user){
        $relation = Yii::app()->db->createCommand()   
        ->select('u.*')
        ->from('user_relation u')
        ->where('u.user_id=:id', array(':id' => $user))
        ->queryAll(); 
        return $relation;
    }
    public function delete_reseller_from_userRealation($studio_id,$portal_user_id){
        $relation = Yii::app()->db->createCommand()
        ->delete('user_relation', 'studio_id = :studio_id and refer_id=:referer_id',array(':studio_id' => $studio_id,':referer_id'=>$portal_user_id));

}
}