<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">How it Works</h2>
            <div class="row">
                <div>
                    <div class="pull-left hiw_top">
                        <div style="text-align: center;">
                            
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/setup.jpg" alt="Setup" title="Initial Setup" style="display: inline-block;">
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div style="text-align: center;">
                            <h3 class="blue"><a href="#initial_setup">Initial Setup</a></h3>
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div style="margin-left:15%;">
                            <ol>
                                <li>Select Template</li>
                                <li>Finalize type of content</li>
                                <li>Setup payment gateway</li>
                            </ol>
                        </div>
                    </div>
                    <div class="pull-left hiw_top">
                        <div style="text-align: center;">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/content.jpg" alt="Add Content" title="Add Content" style="display: inline-block;">
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div style="text-align: center;">
                            <h3 class="blue"><a href="#add_content">Add Content</a></h3>
                           
                        </div> 
                        <div class="clear" style="height:10px"></div>
                        <div style="margin-left:15%;">
                            <ol>
                                <li>Upload videos</li>
                                <li>Add meta data</li>
                            </ol>
                        </div>
                    </div>
                    <div class="pull-left hiw_top">
                        <div style="text-align: center;">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/go_live.jpg" alt="Go Live" title="Go Live" style="display: inline-block;">
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div style="text-align: center;">
                            <h3 class="blue"><a href="#go_live">Go Live</a></h3>
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div style="margin-left:15%;">
                            <ol>
                                <li>Go live</li>
                                <li>Earn Revenue</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear" style="height:10px;border-top:1px solid #42b7da;"></div>
            <ol>

            <div id="initial_setup">
                <h4 class="blue">Initial Setup</h4>
                <div>
                    Estimated timeframe: 10 minutes
                </div>
                <div style="padding-top: 10px;">
                        <div id="select_temp">
                            <li class="bold hiw_li">Select a template</li>
                            <div class="li_desc">Select a design template for your VOD website or/and mobile app.</div>
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div id="finalize_content">
                            <li class="bold hiw_li">Finalize type of content</li>
                            <div class="li_desc">
                                <div>Muvi supports different types of content including:</div>
                                <ol type="a">
                                    <li>Single-part long form:Individual content of duration 60 minutes or longer. Example: Movies</li>
                                    <li>Single-part short form: Individual content of duration 20 minutes or less. Example: video song or clip</li>
                                    <li>Multi-part: Series of content having multiple parts. Example:TV series</li>
                                </ol>
                            </div>
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div id="setup_payment">
                            <li class="bold hiw_li">Setup payment gateway </li>
                            <div  class="li_desc">You need a payment gateway to collect money from your users through credit card and have it transferred to your bank account. Enter information of your payment gateway if you already have one or create a new one, all from within Muvi. Ask us if you need any help.</div>
                        </div>
                </div>
            </div>
            <div class="clear" style="height:20px"></div>
            <div id="add_content">
                <h4 class="blue">Add Content</h4>
                <div>
                    Estimated timeframe: 1 week, depends on amount of content you need to upload.
                </div>
                <div style="padding-top: 10px;">
                        <div id="upload_video">
                            <li class="bold hiw_li">Upload videos</li>
                            <div class="li_desc">Upload your videos through our secure browser-based interface. Depending on your Internet speed, it takes around 30 minutes to upload 1 hour of video. </div>
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div id="add_metadata">
                            <li class="bold hiw_li">Add metadata </li>
                            <div class="li_desc">Add metadata for your content such as poster, story, cast etc...More metadata you add, easier it becomes for users to find and watch your content. </div>
                        </div>
                </div>
            </div>
            <div class="clear" style="height:20px"></div>
            <div id="go_live">
                <h4 class="blue">Go Live</h4>

                <div style="padding-top: 10px;">
                        <div id="go_live_det">
                            <li class="bold hiw_li">Go live</li>
                            <div class="li_desc">Your VOD platform is ready. Make it live on a single click. </div>
                        </div>
                        <div class="clear" style="height:10px"></div>
                        <div  id="earn_rev">
                            <li class="bold hiw_li">Earn revenue</li>
                            <div  class="li_desc">
                                <div>Sit back, relax and collect revenue! Muvi takes care of the entire video streaming platform. Your responsibility after go-live is:</div>
                                <ol type="a">
                                    <li>Marketing: Market your VOD platform. Ask us if you need help.</li>
                                    <li>Refresh content: Add new content on a regular basis to keep your users engaged.</li>
                                </ol>
                            </div>
                        </div>
                    
                </div>
            </div>
            </ol>
        </div>
    </div>
</div>