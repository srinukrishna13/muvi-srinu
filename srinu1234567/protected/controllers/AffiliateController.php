<?php

class AffiliateController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
        public $layout='main';
        
        
	public function actionIndex()
	{
            $this->pageTitle = "Muvi | Acquire new subscribers by providing your content to Muvi.com's content marketplace";
            $this->pageKeywords = 'Muvi.com, Muvi Affiliate, Affiliate, Video Affiliate, Monetize content, monetize video content';
            $this->pageDescription = "Setup your presence and acquire new subscribers by providing your content to Muvi.com's every growing content marketplace";            
            $this->render('index');
	}        
}