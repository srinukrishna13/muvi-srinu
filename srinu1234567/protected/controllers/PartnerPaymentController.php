<?php
require_once 'Paypal/paypal_pro.inc.php';

if ((strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'partnerpayment/authenticatecard')
    || (strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'partnerpayment/purchasesubscribe')) {
    require 'aws/aws-autoloader.php';
}
require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

require_once 'FirstDataApi/FirstData.php';

class PartnerPaymentController extends Controller {

    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = 'partner';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
           //  $this->checkPermission();
        }
        return true;
    }
    function actionSubscription() {
        $levels = ResellerPlan::model()->getResellerPlan();
        $user_id = Yii::app()->user->id;
        //print '<pre>';print_r($packages);exit;
        //echo '<pre>';print_r($levels);exit;
        $data = array();
        foreach($levels['level'] as $key => $val){
            $data[] = $val;
        }
        $number_of_card = ResellerCardInfos::model()->getnumberOfExistingCard($user_id);
        //$this->renderpartial('subscription', array('card' => $card, 'packages' => $packages));
        $this->renderpartial('subscription', array('packages' => $data,'currency_id'=>$studio->default_currency_id,'num_of_card'=>$number_of_card));
        
       // $this->render('subscription', array('card' => $card, 'levels' => $levels));
	}
    function actionMakePrimaryCard() {
        $dbcon=$this->getDbConnection();
        if (isset($_POST['id_payment']) && !empty($_POST['id_payment'])) {
            $user_id = Yii::app()->user->id;
            //Update card to inactive mode
            $reseller_card_info_model = new ResellerCardInfos;
           // $reseller_card_info_model->cancel_card_is_primary($user_id);
           // $reseller_card_info_model->make_card_is_primary($_POST['id_payment']);
            $cancel_primary_sql = "update reseller_card_infos set is_cancelled = 1 where portal_user_id = {$user_id}";
            $command = $dbcon->createCommand($cancel_primary_sql);
            $command->execute();
            $make_primary_sql = "update reseller_card_infos set is_cancelled = 0 where id = {$_POST['id_payment']}";
            $command = $dbcon->createCommand($make_primary_sql);
            $command->execute();
            Yii::app()->user->setFlash('success', 'Credit Card has been set as primary successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be made as primary!');
        }

        $url = $this->createUrl('partnerPayment/ManageCard');
        $this->redirect($url);
    }   
    function actiondeleteCard() {
        $dbcon=$this->getDbConnection();
        if (isset($_POST['id_payment']) && !empty($_POST['id_payment'])) {
           // ResellerCardInfos::model()->deleteByPk($_POST['id_payment']);
            $delete_sql = "delete from reseller_card_infos where id = {$_POST['id_payment']}";
            $command = $dbcon->createCommand($delete_sql);
            $command->execute();           
            Yii::app()->user->setFlash('success', 'Credit Card has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Credit Card can not be deleted!');
        }
        $url = $this->createUrl('partnerPayment/ManageCard');
        $this->redirect($url);
    } 
    
        function isAjaxRequest() {
            if (!Yii::app()->request->isAjaxRequest) {  
                $url = Yii::app()->createAbsoluteUrl();
                header("HTTP/1.1 301 Moved Permanently");
                header('Location: '.$url);exit();
            }
        } 
    
       function actionGetPlanPrice(){
        self::isAjaxRequest();   
 		$purifier = new CHtmlPurifier();
		$_POST = $purifier->purify($_POST); 
        
        $studio = $this->studio;
        $curency_id = $studio->default_currency_id;
        $price = ResellerPlan::model()->getResellerPlanePrice($_POST['id']);
        $priceformat_price = $amount = Yii::app()->common->formatPrice($price[0]['base_price'],$curency_id);
        $price_ar[price] = $price[0]['base_price'];
        $price_ar[price_format] = $priceformat_price;
        $json_price = json_encode($price_ar);
        echo $json_price; exit;
    }
   public function actionAuthenticateCard() {
       self::isAjaxRequest();   
       $dbcon=$this->getDbConnection();
       $purifier = new CHtmlPurifier();
       $_POST = $purifier->purify($_POST); 
		$ip = CHttpRequest::getUserHostAddress();
		$uniqid = Yii::app()->common->generateUniqNumber(); 
		$user_id = isset($_POST['userid'])?$_POST['userid']:Yii::app()->user->id;// modified by manas
		  if (isset($_POST['card_number']) && !empty($_POST['card_number'])) {
			 $dataInput = array();
			 $dataInput['card_number'] = $_POST['card_number'];
			 $dataInput['card_name'] = $_POST['card_name'];    
			 $dataInput['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
			 $dataInput['amount'] = 0;
			 $dataInput['zip'] = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
			 $dataInput['cvv'] = $_POST['cvv'];
			 $address = $_POST['address1'];
			 $address .= (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
			 $address .= ", " . $_POST['city'];
			 $address .= ", " . $_POST['state'];
			 $dataInput['address'] = $address; 
			 //pre authenticate card 
			 $firstData_preauth =  Yii::app()->muvipayment->authenticateToCard($dataInput);
			 $card_last_fourdigit = str_replace(substr($dataInput['card_number'], 0, strlen($dataInput['card_number']) - 4), str_repeat("#", strlen($dataInput['card_number']) - 4), $dataInput['card_number']);
			 $user_sql = "select * from portal_user where id = {$user_id}";
			 $user = (object)$dbcon->createCommand($user_sql)->queryRow();
			 //$user = PortalUser::model()->findByPk($user_id);
			 $req['name'] = $user->name;
			 $req['email'] = $user->email;
			 $req['card_last_fourdigit'] = $card_last_fourdigit;   
			 $req['payment_method'] = 'Credit Card';
		   //if getting error while preauthenticating and store data in reseller card error table
			 if ($firstData_preauth->isError()) {
				 $res['isSuccess'] = 0;
				 $res['Code'] = $firstData_preauth->getErrorCode();
				 $res['Message'] = 'Invalid Card details entered. Please check again';
				 $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
				 //Keeping card error log
				 $RceModel = New ResellerCardErrors;
				 //$pre_auth_error = array('reseller_id' => $user_id,'response_text' => serialize($firstData_preauth),'transaction_type' => 'Card Authentication','created' => new CDbExpression("NOW()"));
				 //$RceModel->set_reseller_error_data($pre_auth_error);
				 $sql_carderror = "insert into reseller_card_errors (id, reseller_id,response_text,transaction_type,created) VALUES" ;
				 $sql_carderror .= " (NULL, '" . $user_id . "','".addslashes(serialize($firstData_preauth))."', 'Card Authentication', NOW());";
				  $command = $dbcon->createCommand($sql_carderror);
				  $command->execute();
				  $Card_info_id = Yii::app()->db->getLastInsertID();
				  //Send an email to developer and support
				 Yii::app()->email->errorMessageMailToSales($req);
			 }else{
				 $respons_type = $firstData_preauth->getBankResponseType();
				 if ($firstData_preauth->getBankResponseType() == 'S') {
					 $token = $firstData_preauth->getTransArmorToken();
					 $card_type = $firstData_preauth->getCreditCardType();
					 $res['Code'] = $firstData_preauth->getBankResponseCode();
					 //$package = ResellerPlan::model()->getResellerpackeg($_POST['packages']);
					 $package_sql = "select * from reseller_plans where id = '".$_POST['packages']."'";
					 $package = $dbcon->createCommand($package_sql)->queryAll();
					 $total_amount = 0;
					 if (isset($package) && !empty($package)) {
						 $package_name = $package[0]['name'];
						 $total_amount = $base_price = $package[0]['base_price'];
					 }
					 if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
						 $end_date = gmdate('Y-m-d H:i:s', strtotime("+1 Months -1 Days"));
					 } else {
						 $end_date = gmdate('Y-m-d H:i:s', strtotime("+1 Years -1 Days"));
					 }                    
					 $data = array();
					 $billing_title = 'Purchase Subscription fee for '.$_POST['plan'].'ly';
					 $billing_desc = 'Purchase Subscription fee for ' . $package_name.'with'.$_POST['plan'].'ly';
					 $data['user'] = $user_id;
					 $data['token'] = $token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
					 $data['card_name'] = $_POST['card_name'];
					 $data['card_type'] = $card_type;
					 $data['exp'] = $dataInput['exp'];
					 $data['amount'] = $total_amount;
					 $data['exp_month'] = $_POST['exp_month'];
					 $data['exp_year'] = $_POST['exp_year'];
					 $data['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
					 $data['cvv'] = $_POST['cvv']; 
					 $data['address2'] = $_POST['address2'];
					 $data['state'] = isset($_POST['state']) ? $_POST['state'] : '';
					 $data['city'] = (isset($_POST['city']) && trim($_POST['city'])) ? $_POST['city'] : '';
					 $data['zip'] = (isset($_REQUEST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : ''; 
					 $address = $_POST['address1'];
					 $data['address'] = $address;
					 $data['ip'] = $ip;
					 $data['b_title'] = $billing_title;
					 $data['b_description'] = $billing_desc;
					 $data['uniqid'] = $uniqid;
					 $data['end_date'] = $end_date;
					 $data['card_type'] = $card_type;
					 $data['card_last_four_degit'] = $card_last_fourdigit;
					 $data['country'] = Yii::app()->common->countryfromIP($ip);
					 $data['created'] = new CDbExpression("NOW()");
					 $data['status'] = $res['Message']; 
					 $data['packages'] = $_POST['packages'];
					 $data['plan'] = $_POST['plan'];
					 $data['packeg_name'] = $package_name;
					 $data['payment_method'] = 'Credit Card';
					 $data[req] = $req;
			 //only storing transaction data withou charging card as amount is 0                           
				 if((abs($data['amount']) < 0.00001)){
						 $save_ids = self::store_paymentData_and_invoice($data);
							 $portal_user = new PortalUser();
							 //$portal_user->updateByPk($user_id, array('payment_method' => 1,'is_subscribed' => 1,'subscription_start'=>new CDbExpression("NOW()")));  
							 $update_query = "UPDATE `portal_user` SET `payment_method`= 1, is_subscribed = 1,subscription_start = NOW() WHERE id='".$user_id."'";
							 $command = $dbcon->createCommand($update_query);
							 $command->execute();
							 $res['isSuccess'] = 1;
							 $res['Code'] = $firstData_preauth->getBankResponseCode();
							 $res['Message'] = $firstData_preauth->getBankResponseMessage();
						  Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');
				 }else{
					 $firstData =  Yii::app()->muvipayment->chargeToCard($data);
					 if ($firstData->isError()) {
						 $res['isSuccess'] = 0;
						 $res['Code'] = $firstData->getErrorCode();
						 $res['Message'] = 'Invalid Card details entered. Please check again';
						 $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
						 //$reseller_card_error_arr = array('reseller_id'=>$user_id,'response_text','transaction_type'=>'Card Authentication','created'=>new CDbExpression("NOW()"));
						 //Keeping card error logs
						// $RceModel = New ResellerCardErrors;
						// $RceModel->set_reseller_error_data($reseller_card_error_arr);
						 $sql_carderror = "insert into reseller_card_errors (id, reseller_id,response_text,transaction_type,created) VALUES" ;
						 $sql_carderror .= " (NULL, '" . $user_id . "','".addslashes(serialize($firstData))."', 'Card Authentication', NOW());";
						 $command = $dbcon->createCommand($sql_carderror);
						 $command->execute();                             
						 //Send an email to developer and support
						 Yii::app()->email->errorMessageMailToSales($req);
					}else{
						$respons_type = $firstData->getBankResponseType();
						if ($firstData->getBankResponseType() == 'S') {
							 $res['isSuccess'] = 1;
							 $res['TransactionRecord'] = $firstData->getTransactionRecord();
							 $res['Code'] = $firstData->getBankResponseCode(); 
							 $res['Message'] = $firstData->getBankResponseMessage();
							 $paid_amount = $firstData->getAmount();
				  //Insert new transaction record in transaction infos;
							 $save_ids = self::store_paymentData_and_invoice($data); 
							// $RtModel = New ResellerTransaction;
							 $transaction_arr = array('card_info_id'=>$save_ids['Card_info_id'],'portal_user_id'=>$user_id,'billing_info_id'=>$save_ids['Rbilling_info_id'],'billing_amount'=>$data['amount'],'invoice_detail'=>$_POST['plan'] . 'ly Subscription Charge','is_success'=>1,'invoice_id'=>$firstData->getTransactionTag(),'order_num'=>$firstData->getAuthNumber(),'paid_amount'=>$data['amount'],'response_text'=>serialize($firstData),'transaction_type'=>'Purchase Subscription fee for ' . $package_name,'created_date'=>new CDbExpression("NOW()"),'subscription_id'=>$save_ids['subscription_id']);
							// $RtModel->setTransactionData($transaction_arr); 
							$transaction_sql = "insert into reseller_transactions(id,card_info_id,portal_user_id,billing_info_id,billing_amount,invoice_detail,is_success,invoice_id,order_num,paid_amount,response_text,transaction_type,created_date,subscription_id) values";
							$transaction_sql .= "(NULL,'".$save_ids['Card_info_id']."','".$user_id."','".$save_ids['Rbilling_info_id']."','".$data['amount']."','".$_POST['plan'] . "ly Subscription Charge',1,'".addslashes($firstData->getTransactionTag())."','".$firstData->getAuthNumber()."','".$data['amount']."','".addslashes(serialize($firstData))."','Purchase Subscription fee for " . $package_name."',NOW(),'".$save_ids['subscription_id']."');";
							$command = $dbcon->createCommand($transaction_sql);
							$command->execute();             
							Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');
							//update payment method
							 //$portal_user = new PortalUser();
							// $portal_user->updateByPk($user_id, array('payment_method' => 1,'is_subscribed' => 1,'subscription_start'=>new CDbExpression("NOW()")));   
							$update_query = "UPDATE `portal_user` SET `payment_method`= 1, is_subscribed = 1,subscription_start = NOW() WHERE id='".$user_id."'";
							 $command = $dbcon->createCommand($update_query);
							 $command->execute();                                 

						}else {
							 $error_message = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
							 $res['isSuccess'] = 0;
							 $res['Code'] = 55;
							 $res['Message'] = $error_message;
						 }
					}  
				 }
				 } else {
					 $error_message = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
					 $res['isSuccess'] = 0;
					 $res['Code'] = 55;
					 $res['Message'] = $error_message;
				 }
			 }

		 } else {
			 $res['isSuccess'] = 0;
			 $res['Code'] = 55;
			 $res['Message'] = 'We are not able to process your credit card. Please try another card.';
		 }
		 print json_encode($res);
		 exit; 
    }
 
    //wire transfer method
    function actiondoWireTransfer(){
        self::isAjaxRequest();   
        
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);       
        if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
            $end_date = gmdate('Y-m-d H:i:s', strtotime("+1 Months -1 Days"));
        } else {
            $end_date = gmdate('Y-m-d H:i:s', strtotime("+1 Years -1 Days"));
        }       

        $uniqid = Yii::app()->common->generateUniqNumber(); 
        $ip = CHttpRequest::getUserHostAddress();       
        $user_id = isset($_POST['userid'])?$_POST['userid']:Yii::app()->user->id;// modified by manas
        $user = PortalUser::model()->findByPk($user_id);
        $package = ResellerPlan::model()->getResellerpackeg($_POST['packages']);
        $total_amount = 0;
        if (isset($package) && !empty($package)) {
            $package_name = $package[0]['name'];
            $total_amount = $base_price = $package[0]['base_price'];
        }       
        $req['name'] = $user->name;
        $req['email'] = $user->email;
        $req['payment_method'] = 'Wire Transfer';       
                    $billing_title = 'Purchase Subscription fee for '.$_POST['plan'].'ly';
                    $billing_desc = 'Purchase Subscription fee for ' . $package_name.'with'.$_POST['plan'].'ly';
                    $data['user'] = $user_id;
                    $data['ip'] = $ip;
                    $data['b_title'] = $billing_title;
                    $data['b_description'] = $billing_desc;
                    $data['uniqid'] = $uniqid;
                    $data['end_date'] = $end_date;
                    $data['country'] = Yii::app()->common->countryfromIP($ip);
                    $data['created'] = new CDbExpression("NOW()");
                    $data['status'] = $res['Message']; 
                    $data['packages'] = $_POST['packages'];
                    $data['plan'] = $_POST['plan'];
                    $data['packeg_name'] = $package_name;  
                    $data['payment_method'] = 'Wire Transfer';
                    $data['amount'] = $total_amount;   
        self::store_paymentData_and_invoice($data); 
        $portal_user = new PortalUser();
        $portal_user->updateByPk($user_id, array('payment_method' => 2,'is_subscribed' => 1,'subscription_start'=>new CDbExpression("NOW()")));       
        Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');                            
        echo 1; exit;                                           
    }
    public function store_paymentData_and_invoice($data = array()){
        $dbcon=$this->getDbConnection();
        $user_sql = "select p.*,s.industry from portal_user p inner join seller_application s on s.id = p.seller_app_id where id = {$data['user']}";
        $user = (object)$dbcon->createCommand($user_sql)->queryRow();
        //$user = PortalUser::model()->findByPk($data['user']);
 //insert new record in card info
        if($data['payment_method'] == 'Credit Card' && $data['payment_form'] != 'existingCard'){
             /*   $RciModel = New ResellerCardInfos; 
                 $card_info_arr = array(
                     'portal_user_id' => $data['user'],
                     'payment_method' => 'first_data',
                     'plan_amt'=>$data['amount'],
                     'card_holder_name'=>$data['card_name'],
                     'exp_month'=>$data['exp_month'],
                     'exp_year'=>$data['exp_year'],
                     'address1'=>$data['address'],
                     'city'=>$data['city'],
                     'state'=>$data['state'],
                     'zip'=>$data['zip'],
                     'is_cancelled'=>0,
                     'card_type' => $data['card_type'],
                     'card_last_fourdigit'=>$data['card_last_four_degit'],
                     'token'=>$data['token'],'ip'=>$data['ip'],
                     'country'=>$data['country'],
                     'created'=>$data['created'],
                     'status'=>$data['status']);
                $Card_info_id = $RciModel->setResellerCardInfo($card_info_arr);  */
            $sql_card_info = "insert into reseller_card_infos (id,portal_user_id,payment_method,plan_amt,card_holder_name,exp_month,exp_year,address1,city,state,zip,is_cancelled,card_type,card_last_fourdigit,token,ip,country,created,status) values";
            $sql_card_info .= "(NULL,'".$data['user']."','first_data','".$data['amount']."','".$data['card_name']."','".$data['exp_month']."','".$data['exp_year']."','".$data['address']."','".$data['city']."','".$data['state']."','".$data['zip']."',0,'".$data['card_type']."','".$data['card_last_four_degit']."','".$data['token']."','".$data['ip']."','".$data['country']."',NOW(),'".$data['status']."')";
            $command = $dbcon->createCommand($sql_card_info);
            $command->execute();  
            $Card_info_id = Yii::app()->db->getLastInsertID();
        }
//insert new record in billing info
      /*   $RbiModel = New ResellerBillingInfos;
         $reseller_billing_info_arr = array('portal_user_id'=>$data['user'],
                                            'uniqid'=>$data['uniqid'],
                                            'title'=>$data['b_title'],
                                            'billing_date'=>new CDbExpression("NOW()"),
                                            'billing_period_start' => new CDbExpression("NOW()"),
                                            'created_date'=>new CDbExpression("NOW()"),
                                            'billing_amount'=>$data['amount'],
                                            'transaction_type'=>1);  
         $Rbilling_info_id = $RbiModel->setBillingInfo($reseller_billing_info_arr); */
         $sql_billing_info = "insert into reseller_billing_infos (id,portal_user_id,uniqid,title,billing_date,billing_period_start,created_date,billing_amount,transaction_type) values";
         $sql_billing_info .= "(NULL,'".$data['user']."','".$data['uniqid']."','".$data['b_title']."',NOW(),NOW(),NOW(),'".$data['amount']."',1)";
         $command = $dbcon->createCommand($sql_billing_info);
         $command->execute();  
         $Rbilling_info_id = Yii::app()->db->getLastInsertID();         
//Insert new records in bill detail 
       /* $RbdModel = New ResellerBillingDetails; 
        $billingDetails_arr = array('billing_info_id'=>$Rbilling_info_id,
                                    'portal_user_id'=>$data['user'],
                                    'title'=>$data['b_title'],
                                    'description'=>$data['b_description'],
                                    'amount'=>$data['amount']);
        $RbdModel->setBillingDetails($billingDetails_arr);  */
        $billing_details_sql = "insert into reseller_billing_details (id,billing_info_id,portal_user_id,title,description,amount) values";
        $billing_details_sql .= "(NULL,'".$Rbilling_info_id."','".$data['user']."','".$data['b_title']."','".$data['b_description']."','".$data['amount']."')";
        $command = $dbcon->createCommand($billing_details_sql);
        $command->execute();
//Insert new subscription data   
      /*  $subscription_arr = array('reseller_id'=>$data['user'],
                                    'reseller_plan_id'=>$data['packages'],
                                    'plans'=>$data['plan'],
                                    'status' => 1,
                                    'start_date'=>new CDbExpression("NOW()"),
                                    'end_date'=>$data['end_date'],
                                    'created_by'=>$data['user'],
                                    'created'=>new CDbExpression("NOW()"),
                                    'updated_by'=>$data['user'],
                                    'updated' => new CDbExpression("NOW()"),
                                    'payment_key'=>$data['uniqid']);
        $Rsmodel = new ResellerSubscription;
        $reseller_subscription_id = $Rsmodel->setSubscription_details($subscription_arr);*/
       $subscription_sql = "insert into reseller_subscriptions (id,reseller_id,reseller_plan_id,plans,status,start_date,end_date,created_by,created,updated_by,updated,payment_key) values";
       $subscription_sql .= "(NULL,'".$data['user']."','".$data['packages']."','".$data['plan']."',1,NOW(),'".$data['end_date']."','".$data['user']."',NOW(),'".$data['user']."',NOW(),'".$data['uniqid']."')";
       $command = $dbcon->createCommand($subscription_sql);
       $command->execute();  
       $reseller_subscription_id = Yii::app()->db->getLastInsertID(); 
//creating invoice data           
        $invoice['user'] = $user;
        $invoice['card_info'] = $RciModel;
        $invoice['billing_info_id'] = $Rbilling_info_id;
        $invoice['billing_amount'] = $data['amount'];
        $invoice['package_name'] = $data['packeg_name'];
        $invoice['base_price'] = $data['amount'];
        $invoice['plan'] = $data['plan'];
        $invoice['yearly_discount'] = '';
        $invoice['payment_method'] = $data['payment_method'];
        if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
            $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Months -1 Days"));
            //$RbiModel->updateByPk($Rbilling_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months -1 Days"))));
            $update_query = "UPDATE `reseller_billing_infos` SET `billing_period_end`= '".Date('Y-m-d H:i:s', strtotime("+1 Months -1 Days"))."' WHERE id='".$Rbilling_info_id."'";
            $command = $dbcon->createCommand($update_query);
            $command->execute();
        } else {
            $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Years -1 Days"));
            //$RbiModel->updateByPk($Rbilling_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Years -1 Days"))));
            $update_query = "UPDATE `reseller_billing_infos` SET `billing_period_end`= '".Date('Y-m-d H:i:s', strtotime("+1 Years -1 Days"))."' WHERE id='".$Rbilling_info_id."'";
            $command = $dbcon->createCommand($update_query);
            $command->execute();           
        }  
//create invoice pdf            
        $pdf = Yii::app()->pdf->resellerSubscriptionReactivationInvoiceDetial($invoice);
        $file_name = $pdf['pdf'];
       // $RbiModel->updateByPk($Rbilling_info_id, array('paid_amount' => $data['amount'], 'is_paid' => 1, 'detail' => $pdf['html']));                            
        $update_query_billing_info = "UPDATE `reseller_billing_infos` SET `paid_amount`= '".$data['amount']."',is_paid= 1,detail= '".addslashes($pdf['html'])."' WHERE id='".$Rbilling_info_id."'";
        $command = $dbcon->createCommand($update_query_billing_info);
        $command->execute();       
        $req['name'] = $user->name;
        $req['email'] = $user->email;
        $req['card_last_fourdigit'] = $data['card_last_four_degit'];   
        $req['payment_method'] = $data['payment_method'];
        $req['package_name'] = @$data['packeg_name'];
        $req['plan'] = @$data['plan'];
        $req['amount_charged'] = @$data['amount']; 
        Yii::app()->email->resellerSubscriptionPurchaseMailToSales($req);
        Yii::app()->email->subscriptionPurchaseMailToReseller($req, $file_name);   
        return array('Card_info_id'=>$Card_info_id,'Rbilling_info_id'=>$Rbilling_info_id,'subscription_id' => $reseller_subscription_id);                      
    }
    //create payment history for ressler
    public function actionpaymentHistory(){
         $dbcon=$this->getDbConnection(); 
        $user_id = isset($_POST['userid'])?$_POST['userid']:Yii::app()->user->id;
       // $user = PortalUser::model()->findByPk($user_id);
        $user_sql = "select * from portal_user where id = {$user_id}";
        $user = (object)$dbcon->createCommand($user_sql)->queryRow();
        $this->breadcrumbs = array('Billing','Payment History');
        $this->headerinfo = 'Payment History';
        $this->pageTitle = Yii::app()->name . ' | ' . $user->name . 'Payment History)';
       // $reseller_trasaction  = new ResellerTransaction;
       // $payment_histary_arr = $reseller_trasaction->get_ressslerpaymenthistory($user_id);
        $reseller_traansaction_sql = "select t.card_info_id, t.portal_user_id, t.billing_info_id,t.billing_amount,t.paid_amount,t.order_num,t.created_date,t.pay_for_customer from reseller_transactions t
                                      where t.portal_user_id={$user_id} order by t.id desc";
        $payment_histary_arr = $dbcon->createCommand($reseller_traansaction_sql)->queryAll();
        
        $subscription_sql = "select * from reseller_subscriptions where reseller_id={$user_id}";
        $subscription_arr = $dbcon->createCommand($subscription_sql)->queryRow();
        $studio = $this->studio;
        $curency_id = $studio->default_currency_id;
        foreach($payment_histary_arr as $key => $val){
            if($val['pay_for_customer'] == 1){
                //$platform_fee = ResellerBillingDetails::model()->get_reseller_customer_platform_fee($val[billing_info_id]);
                $platform_fee_sql = "select SUM(amount) as plat_form_fee from reseller_billing_details where billing_info_id={$val[billing_info_id]}";
                $platform_fee = $dbcon->createCommand($platform_fee_sql)->queryRow();
                $payment_histary_arr[$key]['base_price'] = $platform_fee['plat_form_fee'];
                $payment_histary_arr[$key]['base_price_with_format'] = Yii::app()->common->formatPrice($platform_fee['plat_form_fee'],$curency_id);
            }else{
                $plan_sql = "select * from reseller_plans where id = {$subscription_arr[reseller_plan_id]}";
                $plan = (object)$dbcon->createCommand($plan_sql)->queryRow();
                //$plan = ResellerPlan::model()->findByPk($val[reseller_plan_id]);
                $payment_histary_arr[$key]['base_price'] = $plan->base_price;
                $payment_histary_arr[$key]['base_price_with_format'] = Yii::app()->common->formatPrice($plan->base_price,$curency_id);
            }
        }
        $this->render('payment_history', array('data'=>$payment_histary_arr,'currency_id' => $curency_id,'subscription_arr'=>$subscription_arr));
    }
    //generate pdf for reseller payment history
    public function actionprintReceipt(){
        if (isset($_REQUEST['bill']) && trim($_REQUEST['bill']) != '') {
            $user_id = Yii::app()->user->id;
            $billing_infos = ResellerBillingInfos::model()->findByAttributes(array('id' => trim($_REQUEST['bill'])));
            if (isset($billing_infos) && !empty($billing_infos)) {              
                $invoice['html'] = $billing_infos->detail;
                $invoice['isEmail'] = 0;
                $invoice['billing_date'] = gmdate('F_d_Y', strtotime($billing_infos->billing_date));

                $pdf = Yii::app()->pdf->generateresellerPdf($invoice);
                print $pdf;
                exit;
            } else {
                $url = $this->createUrl('partnerPayment/paymentHistor');
                Yii::app()->user->setFlash('error', 'Oops! There is no bills found!');
                $this->redirect($url);
            }
        } else {
            $url = $this->createUrl('partnerPayment/paymentHistor');
            Yii::app()->user->setFlash('error', 'Oops! There is no bills found!');
            $this->redirect($url);
        }
    }
    //manage reseller card
    function actionManageCard() {
        $this->breadcrumbs = array('Billing', 'Card Information');
        $this->headerinfo = "Card Information";
        $this->pageTitle = "Reseller | Card Information";
        $dbcon=$this->getDbConnection(); 
        $user_id = Yii::app()->user->id;
        $user_sql = "select * from portal_user where id = {$user_id}";
        $user = (object)$dbcon->createCommand($user_sql)->queryRow();        
       // $user = PortalUser::model()->findByPk($user_id);
      //  $subscription_model = new ResellerSubscription();
      // $payment_status = $subscription_model->getResellerPaymentStatus($user_id);
        $payment_status_sql = "select id,payment_status, partial_failed_date, partial_failed_due,start_date,end_date from reseller_subscriptions where reseller_id = {$user_id}";
        $payment_status = $dbcon->createCommand($payment_status_sql)->queryAll();
       // $cards = ResellerCardInfos::model()->findAllByAttributes(array('portal_user_id' => $user_id), array('order' => 'is_cancelled ASC'));
        $card_sql = "select * from reseller_card_infos where portal_user_id = {$user_id} order by is_cancelled ASC";
        $cards = $dbcon->createCommand($card_sql)->queryAll();
        foreach($cards as $key => $val ){
            $cards_object[] = (object)$val;
        }
        $this->render('managecard', array('cards' => $cards_object, 'portal_user_id' => $user_id,'payment_status'=>$payment_status[0],'user'=>$user));
    }  
    public function actionSaveCard(){
        self::isAjaxRequest();   
        
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);
        $dbcon=$this->getDbConnection();
		if (isset($_POST['card_number']) && !empty($_POST['card_number'])) {
			$user_id = Yii::app()->user->id;
			$user_sql = "select * from portal_user where id = {$user_id}";
			$user = (object)$dbcon->createCommand($user_sql)->queryRow();
			//$user = PortalUser::model()->findByPk($user_id);
			//Check primary card is exist or not
		   // $cards = ResellerCardInfos::model()->findByAttributes(array('portal_user_id' => $user_id, 'is_cancelled' => 0));
			$cards_sql = "select * from reseller_card_infos where portal_user_id = {$user_id} and is_cancelled = 0";
			$cards = (object)$dbcon->createCommand($user_sql)->queryRow();
			$is_primary = 0;
			if (isset($cards) && !empty($cards)) {
				$is_primary = 1;
			}
			$card_number = $_POST['card_number'];
			$card_name = $_POST['card_name'];
			$zip = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
			$ccv = $_POST['cvv'];
			$exp_month = $_POST['exp_month'];
			$exp_year = $_POST['exp_year'];
			$address1 = $_POST['address1'];
			$address2 = (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
			$city = (isset($_POST['city']) && trim($_POST['city'])) ? $_POSTS['city'] : '';
			$state = (isset($_POST['state']) && trim($_POST['state'])) ? $_POST['state'] : '';
			$zip = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';   
			$ip = CHttpRequest::getUserHostAddress();

		   $data = array();
			$data['card_number'] = $card_number;
			$data['card_name'] = $card_name;
			$data['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
			$data['amount'] = 0;
			$data['zip'] = $zip;
			$data['cvv'] = $ccv;
			$address = $_POST['address1'];
			$address .= (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
			$address .= ", " . $_POST['city'];
			$address .= ", " . $_POST['state'];
			$data['address'] = $address;
			$data['address2'] = $_POST['address2'];           
			$firstData_preauth =  Yii::app()->muvipayment->authenticateToCard($data);
			$card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);
			$req['name'] = $user->name;
			$req['email'] = $user->email;
			$req['card_last_fourdigit'] = $card_last_fourdigit;  
			if ($firstData_preauth->isError()) {
				$res['isSuccess'] = 0;
				$res['Code'] = $firstData_preauth->getErrorCode();
				$res['Message'] = 'Invalid Card details entered. Please check again';

				$req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
				//Keeping card error logs
			   // $RceModel = New ResellerCardErrors;
			   // $pre_auth_error = array('reseller_id' => $user_id,'response_text' => serialize($firstData_preauth),'transaction_type' => 'Card Authentication','created' => new CDbExpression("NOW()"));
			  //  $RceModel->set_reseller_error_data($pre_auth_error);
				$sql_carderror = "insert into reseller_card_errors (id, reseller_id,response_text,transaction_type,created) VALUES" ;
				$sql_carderror .= " (NULL, '" . $user_id . "','".addslashes(serialize($firstData_preauth))."', 'Card Authentication', NOW());";
				$command = $dbcon->createCommand($sql_carderror);
				$command->execute();                    
				Yii::app()->email->errorMessageMailToSales($req);
			} else {
				if ($firstData_preauth->getBankResponseType() == 'S') {
					$res['Message'] = $firstData_preauth->getBankResponseMessage();                
					$res['isSuccess'] = 1;
					$res['TransactionRecord'] = $firstData_preauth->getTransactionRecord();
					$res['Code'] = $firstData_preauth->getBankResponseCode();
					$packeg_model = new ResellerPlan;
					$packeg = $packeg_model->getResellerPlan();                    
				/*    $RciModel = New ResellerCardInfos; 
					$card_info_arr = array(
										   'portal_user_id' =>$user_id,
										   'payment_method' => 'first_data',
										   'plan_amt' => $packeg['level'][0]['base_price'],
										   'card_holder_name' => $card_name,
										   'card_number' => $card_number,
										   'exp_month' => $exp_month,
										   'exp_year' => $exp_year,
										   'address1' => $address1,
										   'address2' => $address2,
										   'city' => $city,
										   'state' => $state,
										   'zip' => $zip,
										   'status' => $firstData_preauth->getBankResponseMessage(),
										   'reference_no' => $firstData_preauth->getTransactionTag(),
										   'card_type' => $firstData_preauth->getCreditCardType(),
										   'card_last_fourdigit' => $card_last_fourdigit,
										   'auth_num' => $firstData_preauth->getAuthNumber(),
										   'token' => $firstData_preauth->getTransArmorToken(),
										   'is_cancelled' => $is_primary,
										   'response_text' => serialize($firstData_preauth),
										   'ip' => $ip,
										   'country' => Yii::app()->common->countryfromIP($ip),
										   'created' => new CDbExpression("NOW()")
										 );
					$Card_info_id = $RciModel->setResellerCardInfo($card_info_arr);
					*/
				$sql_card_info = "insert into reseller_card_infos (id,portal_user_id,payment_method,plan_amt,card_holder_name,card_number,exp_month,exp_year,address1,city,state,zip,status,reference_no,card_type,card_last_fourdigit,auth_num,token,is_cancelled,response_text,ip,country,created) values";
				$sql_card_info .= "(NULL,'".$user_id."','first_data','".$packeg['level'][0]['base_price']."','".$card_name."','".$card_number."','".$exp_month."','".$exp_year."','".$address1."','".$city."','".$state."','".$zip."','".addslashes($firstData_preauth->getBankResponseMessage())."','".addslashes($firstData_preauth->getTransactionTag())."','".addslashes($firstData_preauth->getCreditCardType())."','".$card_last_fourdigit."','".addslashes($firstData_preauth->getAuthNumber())."','".addslashes($firstData_preauth->getTransArmorToken())."','".$is_primary."','".addslashes(serialize($firstData_preauth))."','".$ip."','".Yii::app()->common->countryfromIP($ip)."',NOW())";
				$command = $dbcon->createCommand($sql_card_info);
				$command->execute();                       
					//Check if partial or failed payment record exists for customer only
				} else {
					$res['isSuccess'] = 0;
					$res['Code'] = 55;
					$res['Message'] = 'We are not able to process your credit card. Please try another card.';
				}
			}             
		} else {
			$res['isSuccess'] = 0;
			$res['Code'] = 55;
			$res['Message'] = 'We are not able to process your credit card. Please try another card.';
		}
		print json_encode($res);
		exit;
    }
    public function actionpayFromPrimaryCard(){
        $user_id = Yii::app()->user->id;
        $user = PortalUser::model()->findByPk($user_id);
        $subscription_model = new ResellerSubscription();
        $payment_status = $subscription_model->getResellerPaymentStatus($user_id);
        $card = ResellerCardInfos::model()->findAllByAttributes(array('portal_user_id' => $user_id,'is_cancelled'=>0), array('order' => 'is_cancelled ASC'));  
        if (!empty($card)) {
            $data = array();
            $data['token'] = $card[0]->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
            $data['card_name'] = $card[0]->card_holder_name;
            $data['card_type'] = $card[0]->card_type;
            $data['card_last_fourdigit'] = $card[0]->card_last_fourdigit;
            $data['exp'] = ($card[0]->exp_month < 10) ? '0' . $card[0]->exp_month . substr($card[0]->exp_year, -2) : $card[0]->exp_month . substr($card[0]->exp_year, -2);
            $res = Yii::app()->muvipayment->chargeDueOnPartialOrFailedTransaction($payment_status,$user, $card, $data);
            if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                $url = $this->createUrl('partnerPayment/ManageCard');
                Yii::app()->user->setFlash('success', 'Your payment has been processed successfully');
            } else {
                $url = $this->createUrl('partnerPayment/ManageCard');
                Yii::app()->user->setFlash('error', @$res['Message']);
            }
        } else {
            $url = $this->createUrl('partnerPayment/ManageCard');
            Yii::app()->user->setFlash('error', 'Oops! No primary card has found');
        }        
        $this->redirect($url);
    }
    public function actioninsertNewCustomer() {
		self::isAjaxRequest();
		parse_str($_POST['data'], $req_data);
		$dbcon = $this->getDbConnection();
		$user_id = Yii::app()->user->id;
		$user_sql = "select * from portal_user where id = {$user_id}";
		$user = (object) $dbcon->createCommand($user_sql)->queryRow();
		$req['name'] = $user->name;
		$req['email'] = $user->email;
		$ip_address = CHttpRequest::getUserHostAddress();
		$uniq_id = Yii::app()->common->generateUniqNumber();
		$country = Yii::app()->common->countryfromIP($ip_address);
		//packeg data
		$package = Package::model()->getPackages($req_data['packages']);
		$total_amount = 0;
		if (isset($package) && !empty($package)) {
			$package_name = $package['package'][0]['name'];
			$app_price = Yii::app()->general->getAppPrice($package['package'][0]['code']);
			$package_codes_id = 0;
			$total_amount = $base_price = $package['package'][0]['base_price'];

			$yearly_discount = $package['package'][0]['yearly_discount'];

			$today = Date('Y-m-d H:i:s');
			$reseller_billing_date_sql = "select id,end_date from reseller_subscriptions where reseller_id = {$user_id} order by id desc";
			$reseller_billing_date_arr = $dbcon->createCommand($reseller_billing_date_sql)->queryRow();
			$reseller_billing_date = $reseller_billing_date_arr['end_date'];
			$date1 = date_create($today);
			$date2 = date_create($reseller_billing_date);
			$diff = date_diff($date2, $date1);
			$date_diffrence = $diff->format("%a") + 2;
			$current_month = date('m');
			$current_year = date('Y');
			$num_of_day_in_current_month = cal_days_in_month(CAL_GREGORIAN, $current_month, $current_year);
			if (isset($req_data['pricing']) && !empty($req_data['pricing'])) {
				$applications = Package::model()->getApplications($req_data['packages'], $req_data['pricing']);
				$no_of_applications = count($applications);
				if (isset($applications) && !empty($applications) && ($no_of_applications > 1)) {
					$total_amount = number_format((float) $total_amount + (($no_of_applications - 1) * $app_price), 2, '.', '');
				}
			}
			if (isset($req_data['plan']) && trim($req_data['plan']) == 'Year') {
				$total_amount = number_format((float) (($total_amount * 12) - ((($total_amount * 12) * $yearly_discount) / 100)), 2, '.', '');
			}
		}
		//calculation for pro rated amount for monthly
		if (strtotime($today) < strtotime($reseller_billing_date)) {
			$amount_perday = $total_amount / $num_of_day_in_current_month;
			$prorated_amount = $amount_perday * $date_diffrence;
			$total_amount = $prorated_amount;
		}
		$data_input = array();
		$RceModel = New ResellerCardErrors;
		$dataInput['amount'] = round($total_amount, 2);
		if (isset($req_data['pricing']) && $req_data['pricing'] != '') {
			$dataInput['pricing'] = $req_data['pricing'];
			$dataInput['applications'] = (isset($applications) ? $applications : '');
		}
		$dataInput['packeg_id'] = $package['package'][0]['id'];
		$dataInput['packeg'] = $package_name;
		$dataInput['user_id'] = $user_id;
		$dataInput['plan'] = $req_data['plan'];
		$dataInput['ip'] = $ip_address;
		$dataInput['base_price'] = $base_price;
		$dataInput['package_codes_id'] = $package_codes_id;
		$dataInput['reseller_biling_date'] = $reseller_billing_date;
		$dataInput['packeg_arr'] = $package;
		$dataInput['cloud_hosting'] = $req_data['cloud_hosting'];
		$dataInput['prorated_calculation'] = 'No. Of Days(' . $date_diffrence . ') * Per Day($' . round($amount_perday, 2) . ') = $' . $dataInput['amount'];
		//setting billing date
		$req_data['reseller_biling_date'] = $reseller_billing_date;
		
		// for new card                
		if ($req_data['payform'] == 'new_card') {
			if (isset($_POST['card_number']) && !empty($_POST['card_number'])) {
				$dataInput['card_number'] = $_POST['card_number'];
				$dataInput['card_name'] = $_POST['card_name'];
				$dataInput['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);

				$dataInput['zip'] = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
				$dataInput['cvv'] = $_POST['cvv'];
				$address = $_POST['address1'];
				$address .= (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
				$address .= ", " . $_POST['city'];
				$address .= ", " . $_POST['state'];
				$dataInput['address'] = $address;
				$dataInput['address1'] = $_POST['address1'];
				$dataInput['address2'] = $_POST['address2'];
				$dataInput['city'] = (isset($_POST['city']) ? $_POST['city'] : '');
				$dataInput['state'] = $_POST['state'];
				$dataInput['zip'] = $_POST['zip'];
				$dataInput['country'] = $country;
				$dataInput['exp_month'] = $_POST['exp_month'];
				$dataInput['exp_year'] = $_POST['exp_year'];
				$firstData_preauth = Yii::app()->muvipayment->authenticateToCard($dataInput);
				$card_last_fourdigit = str_replace(substr($dataInput['card_number'], 0, strlen($dataInput['card_number']) - 4), str_repeat("#", strlen($dataInput['card_number']) - 4), $dataInput['card_number']);
				$req['card_last_fourdigit'] = $card_last_fourdigit;
				if ($firstData_preauth->isError()) {
					$res['isSuccess'] = 0;
					$res['Code'] = $firstData_preauth->getErrorCode();
					$res['Message'] = 'Invalid Card details entered. Please check again';

					$req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
					$sql_carderror = "insert into reseller_card_errors (id, reseller_id,response_text,transaction_type,created) VALUES";
					$sql_carderror .= " (NULL, '" . $user_id . "','" . addslashes(serialize($firstData_preauth)) . "', 'Card Authentication', NOW());";
					Yii::app()->email->errorMessageMailToSales($req);
				} else {
					$respons_type = $firstData_preauth->getBankResponseType();
					if ($respons_type == 'S') {
						$token = $firstData_preauth->getTransArmorToken();
						$card_type = $firstData_preauth->getCreditCardType();
						$dataInput['token'] = $token;
						$dataInput['card_type'] = $card_type;
						$res['Message'] = $firstData_preauth->getBankResponseMessage();
						//saving card info
						$no_existing_card_sql = "select id from reseller_card_infos where portal_user_id = {$user_id}";
						$item_count = $dbcon->createCommand($no_existing_card_sql)->queryAll();
						$no_existing_card = count($item_count);
						if ($no_existing_card > 0) {
							$is_primary = 1;
						} else {
							$is_primary = 0;
						}
						//         save card info
						$sql_card_info = "insert into reseller_card_infos (id,portal_user_id,payment_method,plan_amt,card_holder_name,exp_month,exp_year,address1,city,state,zip,is_cancelled,card_type,card_last_fourdigit,token,ip,country,created,status,reference_no,auth_num,response_text) values";
						$sql_card_info .= "(NULL,'" . $user_id . "','first_data','" . $dataInput['amount'] . "','" . $dataInput['card_name'] . "','" . $dataInput['exp_month'] . "','" . $dataInput['exp_year'] . "','" . $dataInput['address'] . "','" . $dataInput['city'] . "','" . $dataInput['state'] . "','" . $dataInput['zip'] . "','" . $is_primary . "','" . $dataInput['card_type'] . "','" . $card_last_fourdigit . "','" . addslashes($dataInput['token']) . "','" . $dataInput['ip'] . "','" . $dataInput['country'] . "',NOW(),'" . addslashes($firstData_preauth->getBankResponseMessage()) . "','" . addslashes($firstData_preauth->getTransactionTag()) . "','" . addslashes($firstData_preauth->getAuthNumber()) . "','" . addslashes(serialize($firstData_preauth)) . "')";
						$command = $dbcon->createCommand($sql_card_info);
						$command->execute();
						$Card_info_id = Yii::app()->db->getLastInsertID();
						$dataInput['card_info_id'] = $Card_info_id;
					} else {
						$error_message = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();
						$res['isSuccess'] = 0;
						$res['Code'] = 55;
						$res['Message'] = $error_message;
					}
				}
			} else {
				$res['isSuccess'] = 0;
				$res['Code'] = 55;
				$res['Message'] = 'We are not able to process your credit card. Please try another card.';
			}
		} else if ($req_data['payform'] == 'execting_card') {
			$respons_type = 'S';
			$existing_card_info_sql = "select * from reseller_card_infos where id = {$req_data['existing_card_id']}";
			$existing_card_info = (object) $dbcon->createCommand($existing_card_info_sql)->queryRow();
			$dataInput['card_info_id'] = $existing_card_info->id;
			$dataInput['amount'] = round($total_amount, 2);
			$dataInput['token'] = $existing_card_info->token;
			$dataInput['card_type'] = $existing_card_info->card_type;
			$dataInput['card_name'] = $existing_card_info->card_holder_name;
			$dataInput['card_last_fourdigit'] = $existing_card_info->card_last_fourdigit;
			$dataInput['card_number'] = $existing_card_info->card_number;
			$dataInput['exp'] = ($existing_card_info->exp_month < 10) ? '0' . $existing_card_info->exp_month . substr($existing_card_info->exp_year, -2) : $existing_card_info->exp_month . substr($existing_card_info->exp_year, -2);
			$req['card_last_fourdigit'] = $dataInput['card_last_fourdigit'];
		}
		//charge to card part
		/*if ($user_id == 0) {
			$dataInput['amount'] = 0.0;
		}*/
		
		if ($respons_type == 'S') {
			$firstData = Yii::app()->muvipayment->chargeToCard($dataInput);
			if ($firstData->isError()) {
				$res['isSuccess'] = 0;
				$res['Code'] = $firstData->getErrorCode();
				$res['Message'] = 'Invalid Card details entered. Please check again';

				$req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
				//Keeping card error logs
				$sql_carderror = "insert into reseller_card_errors (id, reseller_id,response_text,transaction_type,created) VALUES";
				$sql_carderror .= " (NULL, '" . $user_id . "','" . addslashes(serialize($firstData)) . "', 'Card Authentication', NOW());";
				//Send an email to developer and support
				Yii::app()->email->errorMessageMailToSales($req);
			} else if ($firstData->getBankResponseType() == 'S') {
				$res['isSuccess'] = 1;
				$res['TransactionRecord'] = $firstData->getTransactionRecord();
				$res['Code'] = $firstData->getBankResponseCode();
				$res['Message'] = $firstData->getBankResponseMessage();
				$paid_amount = $firstData->getAmount();
				$dataInput['paid_amount'] = $paid_amount;
				$dataInput['invoice_id'] = $firstData->getTransactionTag();
				$dataInput['order_num'] = $firstData->getAuthNumber();
				$dataInput['response_text'] = serialize($firstData);
				$created_studio = self::insertcustomer_for_reseller($req_data);
				$created_studio = json_decode($created_studio);
				if ($created_studio->isSuccess == 'success') {
					$dataInput['studio_id'] = $created_studio->studioid;
					$dataInput['new_user_id'] = $created_studio->studioid;
					self::store_reseller_payment_for_customer($dataInput);
					Yii::app()->user->setFlash('success', 'Thank you, Your subscription has been activated successfully.');
				}
			}
		}
		print json_encode($res);
		exit;
	}

	public function store_reseller_payment_for_customer($data = array()){
    //insert new record in billing info
        $dbcon=$this->getDbConnection(); 
        $portal_user_sql = "select p.*,s.industry from portal_user p inner join seller_application s on s.id = p.seller_app_id where p.id ={$data['user_id']}";
        $portal_user = (object)$dbcon->createCommand($portal_user_sql)->queryRow();
        //$portal_user = PortalUser::model()->findByPk($data['user_id']);
        $packeg = $data['packeg_arr'];
        $uniq_id = Yii::app()->common->generateUniqNumber();
         $data['b_title'] = 'Purchase Subscription fee for ' .$data['packeg'] ;
        /*  $RbiModel = New ResellerBillingInfos;
         $reseller_billing_info_arr = array('portal_user_id'=>$data['user_id'],
                                            'uniqid'=>$uniq_id,
                                            'title'=>'Purchase Subscription fee for ' .$data['packeg'],
                                            'billing_date'=>new CDbExpression("NOW()"),
                                            'billing_period_start' => new CDbExpression("NOW()"),
                                            'created_date'=>new CDbExpression("NOW()"),
                                            'billing_amount'=>$data['amount'],
                                            'transaction_type'=>1,
                                            'is_pay_for_customer' => 1
                                            );  
         $Rbilling_info_id = $RbiModel->setBillingInfo($reseller_billing_info_arr); 
        */
         $sql_billing_info = "insert into reseller_billing_infos (id,portal_user_id,uniqid,title,billing_date,billing_period_start,created_date,billing_amount,transaction_type,is_pay_for_customer) values";
         $sql_billing_info .= "(NULL,'".$data['user_id']."','".$uniq_id."','".$data['b_title']."',NOW(),NOW(),NOW(),'".$data['amount']."',1,1)";
         $command = $dbcon->createCommand($sql_billing_info);
         $command->execute();  
         $Rbilling_info_id = Yii::app()->db->getLastInsertID();  
        $allapps = '';
        $no_of_applications = '';
        if (isset($data['pricing']) && !empty($data['pricing']) && isset($data['applications']) && !empty($data['applications'])) {
            $no_of_applications = count($data['applications']);
            $cntapp = 1;
            foreach ($data['applications'] as $key => $value) {
                if ($cntapp == 1) {
                    $title = 'Monthly platform fee';
                    $description = $data['packeg'].' platform fee';
                    $packageamount = $data['base_price'];
                } else {
                    $title = $value['name'];
                    $description = $value['description'];
                    $packageamount = $value['price'];
                }
                $cntapp++;
        //Insert new records in bill detail   
               $billing_details_sql = "insert into reseller_billing_details (id,billing_info_id,portal_user_id,title,description,amount,is_pay_for_customer) values";
               $billing_details_sql .= "(NULL,'".$Rbilling_info_id."','".$data['user_id']."','".$title."','".$description."','".$packageamount."',1)";
               $command = $dbcon->createCommand($billing_details_sql);
               $command->execute();                
       //Insert New Record in studio Pricing Plan
                $sppModel = New StudioPricingPlan;
                $studio_pricing_plan_arr = array(
                                                'studio_id' => $data['studio_id'],
                                                'package_id' => $value['parent_id'],
                                                'application_id' => $value['id'],
                                                'plans' => $data['plan'],
                                                'is_cloud_hosting' => 0,
                                                'package_code_id' => $data['package_codes_id'],
                                                'created_by' => $data['user_id'],
                                                'created' => new CDbExpression("NOW()"),
                                                'status' => 1
                                             );
                $sppModel->set_Reseller_customer_pricing_plan($studio_pricing_plan_arr);
                $allapps.=$value['name'] . ', ';
            }
            $allapps = rtrim($allapps, ', ');
        } else {//Muvi Server
            $title = 'Monthly platform fee';
            $description = $data['packeg'].' platform fee';
        /*  Insert billing details */
            $billing_details_sql = "insert into reseller_billing_details (id,billing_info_id,portal_user_id,title,description,amount,is_pay_for_customer) values";
            $billing_details_sql .= "(NULL,'".$Rbilling_info_id."','".$data['user_id']."','".$title."','".$description."','".$data['amount']."',1)";
            $command = $dbcon->createCommand($billing_details_sql);
            $command->execute();            
            $sppModel = New StudioPricingPlan;
                $studio_pricing_plan_arr = array(
                                                'studio_id' => $data['studio_id'],
                                                'package_id' => $data['packeg_id'],
                                                'application_id' => 0,
                                                'plans' => $data['plan'],
                                                'is_cloud_hosting' => 0,
                                                'package_code_id' => $data['package_codes_id'],
                                                'created_by' => $data['user_id'],
                                                'created' => new CDbExpression("NOW()"),
                                                'status' => 1
                                             );
                $sppModel->set_Reseller_customer_pricing_plan($studio_pricing_plan_arr);            
            
        } 
 //Insert new subscription data          
    /*   $subscription_sql = "insert into reseller_subscriptions (id,reseller_id,reseller_plan_id,plans,status,start_date,end_date,created_by,created,updated_by,updated,payment_key,pay_for_customer) values";
       $subscription_sql .= "(NULL,'".$data['user_id']."','".$data['packeg_id']."','".$data['plan']."',1,NOW(),'".$data['reseller_biling_date']."','".$data['user_id']."',NOW(),'".$data['user_id']."',NOW(),'".$uniq_id."',1)";
       $command = $dbcon->createCommand($subscription_sql);
       $command->execute();  
       $reseller_subscription_id = Yii::app()->db->getLastInsertID();   
     * 
     */ 
        $reseller_subscription_id = 0;
        //inserting new transaction record
            $transaction_sql = "insert into reseller_transactions(id,card_info_id,portal_user_id,billing_info_id,billing_amount,invoice_detail,is_success,invoice_id,order_num,paid_amount,response_text,transaction_type,created_date,subscription_id,pay_for_customer) values";
            $transaction_sql .= "(NULL,'".$data['card_info_id']."','".$data['user_id']."','".$Rbilling_info_id."','".$data['amount']."','".$data['plan'] . "ly Subscription Charge',1,'".addslashes($data['invoice_id'])."','".$data['order_num']."','".$data['paid_amount']."','".addslashes($data['response_text'])."','Purchase Subscription fee for " . $data['packeg']."',NOW(),'".$reseller_subscription_id."',1);";
            $command = $dbcon->createCommand($transaction_sql);
            $command->execute(); 
            $transaction_id = Yii::app()->db->getLastInsertID();
              $user = User::model()->findByPk($data['new_user_id']);
              $studio = Studio::model()->findByPk($data['studio_id']);
              $RciModel_sql = "select * from reseller_card_infos where id = {$data['card_info_id']}";
              $RciModel = (object)$dbcon->createCommand($RciModel_sql)->queryRow();
              //$RciModel = ResellerCardInfos::model()->findByPk($data['card_info_id']);             
              $invoice['studio'] = $studio;
              $invoice['user'] = $user;
              $invoice['portal_user'] = $portal_user;
              $invoice['card_info'] = $RciModel;
              $invoice['billing_info_id'] = $Rbilling_info_id;
              $invoice['billing_amount'] = $data['amount'];
              $invoice['package_name'] = $data['packeg'];
              $invoice['package_code'] = @$packeg['package'][0]['code'];
              $invoice['base_price'] = $data['base_price'];
              $invoice['plan'] = $data['plan'];
              $invoice['yearly_discount'] = $data['yearly_discount'];
              $invoice['no_of_applications'] = @$no_of_applications;
              $invoice['applications'] = @$allapps;
              $invoice['prorated_calculation'] = $data['prorated_calculation'];

                if (isset($data['plan']) && trim($data['plan']) == 'Month') {
                    $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime($data['reseller_biling_date']));
                } else {
                    $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y') . ' To: ' . gmdate('F d, Y', strtotime("+1 Years -1 Days"));
                } 
                $pdf = Yii::app()->pdf->resellerCustomerSubscriptionReactivationInvoiceDetial($invoice);
                $file_name = $pdf['pdf'];  
               $update_query_billing_info = "UPDATE `reseller_billing_infos` SET `paid_amount`= '".$data['amount']."',is_paid= 2,detail= '".addslashes($pdf['html'])."' WHERE id='".$Rbilling_info_id."'";
                $command = $dbcon->createCommand($update_query_billing_info);
                $command->execute();
               //Save subscription info if paymanet has processed successfully
                $start_date = Date('Y-m-d H:i:s');
                $end_date = $data['reseller_biling_date'];
                $update_query_billing_info = "UPDATE `reseller_billing_infos` SET `billing_period_start`= NOW() WHERE id='".$Rbilling_info_id."'";
                $command = $dbcon->createCommand($update_query_billing_info);
                $command->execute();           
               if (isset($data['plan']) && trim($data['plan']) == 'Month') {
                   $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($data['reseller_biling_date']));
                   $start_date = Date('Y-m-d H:i:s');
                   $end_date = $data['reseller_biling_date'];
                   $update_query_billing_info = "UPDATE `reseller_billing_infos` SET `billing_period_end`= '".$data['reseller_biling_date']."' WHERE id='".$Rbilling_info_id."'";
                   $command = $dbcon->createCommand($update_query_billing_info);
                   $command->execute();              
               } else {
                   $invoice['from_to_date'] = 'From: ' . gmdate('F d, Y', strtotime($studio->start_date)) . ' To: ' . gmdate('F d, Y', strtotime($studio->start_date . "+1 Years -1 Days"));
                   $start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
                  $update_query_billing_info = "UPDATE `reseller_billing_infos` SET `billing_period_end`= '". Date('Y-m-d H:i:s', strtotime("+1 Years -1 Days"))."' WHERE id='".$Rbilling_info_id."'";
                  $command = $dbcon->createCommand($update_query_billing_info);
                  $command->execute();           
                  $end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
               } 
               //Save payment related info in studio table
               $studio_model = new Studio;
               $studio_model->updateByPk($data['studio_id'],array('status'=>1,'is_subscribed'=>1,'subscription_start'=>new CDbExpression("NOW()"),'start_date'=>new CDbExpression("NOW()"),'end_date'=>$end_date,'bandwidth_date'=>$end_date));
               $req['name'] = $portal_user->name;
               $req['email'] = $portal_user->email;
               $req['companyname'] = $studio->name;
               $req['customer_name'] = $user->first_name;
               $req['domain'] = 'http://' . $studio->domain;
               $req['card_last_fourdigit'] = $RciModel->card_last_fourdigit;              
               $req['package_name'] = $data['packeg'];
               $req['applications'] = @$allapps;
               $req['plan'] = @$data['plan'];
               $req['amount_charged'] = $data['amount'];
               $req['is_cloud_hosting'] = (isset($data['cloud_hosting']) && intval($data['cloud_hosting'])) ? 1 : 0;

               Yii::app()->email->resellerCustomersubscriptionPurchaseMailToSales($req);
               Yii::app()->email->resellerCustomersubscriptionPurchaseMailToReseller($req, $file_name);
               //Create Cloudfront Url For studio
               self::createClouldfrontUrl($studio);

    }
    public function insertcustomer_for_reseller($req_data){
           ob_clean();
           $reseller_id = Yii::app()->user->id;
           $ip_address = CHttpRequest::getUserHostAddress();
            $geo_data = new EGeoIP();
            $geo_data->locate($ip_address);
            $country = $geo_data->getCountryName();
            $region = $geo_data->getRegion();
            $latitude = $geo_data->getLatitude();
            $longitude = $geo_data->getLongitude();
            $ref = array($latitude, $longitude);
            $s3bucket_id = 1;
               
            /*if ($_REQUEST['relation_type'] == 'master') {
                $s = $this->getmasterdomain();
                $domain = $s['domain'];
            } else {*/
                $domain = Yii::app()->common->getStudiodomain();
            /*}*/
            if (isset($req_data['email']) && $req_data['email'] != '') {
                $string = str_replace(' ', '', strtolower($req_data['subdomain']));
                $inputDomain = str_replace('www.', '', preg_replace('/[^A-Za-z0-9.]/', '', $string));
                $usr = new User;
                $data = $usr->findByAttributes(array('email' => $req_data['email']));
                if ($data) {
                    $msg['isSuccess'] = 'warning';
                    $msg['Message'] = 'Account already exists!';
                    return json_encode($msg);
                    exit;
                }
                //check Subdomain 
                if (isset($req_data['subdomain']) && $req_data['subdomain']) {
                    $data = Studios::model()->find('domain=:domain OR reserved_domain=:domain', array(':domain' => $inputDomain));
                    if ($data) {
                        $msg['isSuccess'] = 'warning';
                        $msg['Message'] = 'This domain name already exists. Please try a different one.';
                        return json_encode($msg);
                        exit;
                    }
                }
                if (!$data) {
                    //Find the bucket with respect to the client region and assign to that bucket.
                    $countrys3location = new Countrys3location();
                    $buckt_data = $countrys3location->getS3Location($country);
                    if (isset($buckt_data) && count($buckt_data)) {
                        $s3bucket_id = $buckt_data['s3buckets_id'];
                    } else {
                        if ($latitude && $longitude) {
                            $s3buckets = S3bucket::model()->findAll();
                            foreach ($s3buckets AS $key => $val) {
                                $buckts[$val->id][] = $val->bucket_name;
                                $buckts[$val->id][] = $val->region_code;
                                $buckts[$val->id][] = $val->latitude;
                                $buckts[$val->id][] = $val->longitude;
                            }
                            $distances = array_map(function($buckts) use($ref) {
                                $a = array_slice($buckts, -2);
                                return Yii::app()->common->getDistance($a, $ref);
                            }, $buckts);
                            asort($distances);
                            $bucketids = array_keys($distances);
                            $s3bucket_id = $bucketids[0];
                        }
                    }
                    //Get referrence link from where user come this site.
                    $source = '';
                    if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                        $source = Yii::app()->request->cookies['REFERRER'];
                        //Unset source cookie
                        //unset($_COOKIE['REFERRER']);
                        //setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
                    }
                    $subdomain = self::createUniqSubdomain($inputDomain);
                    $main_domain = $subdomain . '.' . $domain;
                    $studio = new Studio();
                    $studio_data_arr = array(
                                       'uniqid'=>Yii::app()->common->generateUniqNumber(),
                                       'name' => $req_data['customer_name'],
                                       'permalink' => $subdomain,
                                       'phone' => '',
                                       'created_dt' => gmdate('Y-m-d H:i:s'),
                                       'subdomain' => $subdomain,
                                       'domain' => $main_domain,
                                       'reserved_domain' => $inputDomain,
                                       'theme'=>'',
                                       's3bucket_id' => ($s3bucket_id > 0) ? $s3bucket_id : 1,
                                       'source' => $source,
                                       'new_cdn_users' => 1,
                                       'status' => 1,
                                       'start_date' => new CDbExpression("NOW()"),
                                       'end_date' => $req_data['reseller_biling_date'],
                                       'bandwidth_date'=>$req_data['reseller_biling_date'],
                                       'reseller_id' => $reseller_id,
                                       'is_subscribed' => 1,
                                       'default_currency_id' => 153,
                                       'status' => 1,
                                       'is_deleted' => 0,
                                       'is_default' => 0
                                        );
                    $studio_id = $studio->save_reseller_customer_data($studio_data_arr);
                    Yii::app()->session['studio_id'] = $studio_id;
                    Yii::app()->session['back_btn'] = $studio_id;
                    //Create Directory in s3 For studio
                    if ($s3bucket_id == '') {
                        $s3bucket_id = 1;
                    }
                    $bucketDetailsArr = Yii::app()->common->getBucketInfo($s3bucket_id);
                    if (isset($bucketDetailsArr['bucket_name']) && $bucketDetailsArr['bucket_name'] != '') {
                        $client = S3Client::factory(array(
                                    'key' => Yii::app()->params->s3_key,
                                    'secret' => Yii::app()->params->s3_secret,
                        ));
                        $key = $studio_id . '/EncodedVideo/blank.txt';
                        $key1 = $studio_id . '/public/blank.txt';
                        $key2 = $studio_id . '/RawVideo/blank.txt';
                        $source_file = Yii::app()->getbaseUrl(true) . '/blank.txt';
                        $acl = 'public-read';
                        $bucket = $bucketDetailsArr['bucket_name'];
                        $client->upload($bucket, $key, $source_file, $acl);
                        $client->upload($bucket, $key1, $source_file, $acl);
                        $client->upload($bucket, $key2, $source_file, $acl);
                    }
                    //Insert into PPV Buy
                    $ppvBuy = new PpvBuy;
                    $ppvBuy->save_resseller_customer_studio_id($studio_id);

                    //Insert into Studio Manage Coupon
                    $studioManageCoupon = new StudioManageCoupon;
                    $studioManageCoupon->save_reseller_customer_studio_id($studio_id);

                    //Insert data into the user table 
                    $relation = new UserRelation;
                    $enc = new bCrypt();
                    $password = $relation->getRandomPassword();
                    $encrypted_pass = $enc->hash($password);

                    $umodel = new User();
                    $user_arr = array(
                                       'studio_id' => $studio_id,
                                       'first_name' => $req_data['customer_name'],
                                       'email' => $req_data['email'],
                                       'encrypted_password' => $encrypted_pass,
                                       'created_at' => gmdate('Y-m-d H:i:s'),
                                       'phone_no' => '',
                                       'signup_ip' => $ip_address,
                                       'signup_location' => serialize($geo_data),
                                       'is_active' => 1,
                                       'is_sdk' => 1,
                                       'role_id' => 1,
                                       'signup_step' => 1,
                                     );
                    $newuserid = $umodel->save_reseller_customer_data($user_arr);
                    //Insert studio id in menu table
                    $menu = new Menu;
                     $menu->set_menuid_for_reseller_customer(array('studio_id' => $studio_id));
                    //Inserting into the front-end user table
                    $user = new SdkUser;
                    $sdk_user_arr = array(
                                        'is_studio_admin' => 1,
                                        'email' => $req_data['email'],
                                        'studio_id' => $studio_id,
                                        'signup_ip' => $ip_address,
                                        'display_name' => htmlspecialchars($req_data['customer_name']),
                                        'encrypted_password' => $encrypted_pass,
                                        'status' => 1,
                                        'created_date' => new CDbExpression("NOW()")
                                     );
                    $sdk_user_id = $user->save_reseller_customer_data($sdk_user_arr);
                    //Inserting studio id and email to ticket master table
                    if($sdk_user_id){
                    $tickettingdb=$this->getAnotherDbconnection();
                    $command = $tickettingdb->createCommand();
                    $command->insert('ticket_master', array(
                        'studio_id'=>$studio_id,
                        'studio_email'=>$req_data['email'],
                        'studio_name'=>$req_data['customer_name'],
                        'user_id'=>$newuserid,
                        'user_name'=>$req_data['customer_name'],
                        'user_type'=>1,
                        'user_sdk'=>1,
                        'studio_subscribed'=>0,
                        'creation_date'=>date("Y-m-d H:i:s")
                    ));             
                    $tickettingdb->active=false;
                    }
                    /*for multiple partner*/
                     $poratl_user = new PortalUser;
                     $parentid = $poratl_user->get_reseller_parent_id($reseller_id);
                     $refer_id = ($parentid['parent_id']!=0)?$parentid['parent_id']:Yii::app()->user->id;                   
                    /**/
                    //for saving in relation table 
                     $relation_arr = array(
                                        'user_id' => $newuserid,
                                        'studio_id' => $studio_id,
                                        'refer_id' => $refer_id,
                                        'ip' => $ip_address,
                                        'relation_type' => $req_data['relation_type'],
                                        'add_date' => new CDbExpression("NOW()"),
                                        'refer_studio_id' => $referstudioid,
                                        'plan' => $req_data['plan'],
                                        'packages' => $req_data['packages'],
                                        'pricing' => $req_data['pricing']
                                     );
                     $relation->save_reseller_customer_data($relation_arr);
                                        
  ///////////////////adding monetization and content to studio  start                 
                    $content = isset($req_data['content']) ? $req_data['content'] : array();
                    $monetization = isset($req_data['monetization']) ? $req_data['monetization'] : array();
                    $apps  = array();
                   
                    if($req_data['packages'] != 26 && $req_data['packages'] != 6 && $req_data['packages'] != 35 && $req_data['packages'] != 36){
                        switch($req_data['packages_code']){
                            case 'muvi_studio':
                               $apps = self::set_application_sum($req_data['selected_app_code']);
                            break;
                            case 'muvi_studio_professional':
                               $apps = self::set_application_sum($req_data['selected_app_code']);
                            break; 
                            case 'muvi_studio_enterprise':
                                $apps = self::set_application_sum($req_data['selected_app_code']);
                            break;                
                        }
                    }else{
                         $apps[] = 1;
                    }
                    $content_sum = $app_sum = $menu_sum = 0;
                    $dummy_data = 1;
                    $content_sum = array_sum($content);
                    StudioContent::model()->updateContentSettings($studio_id,$content_sum);
                    $app_sum = array_sum($apps);
                    StudioAppMenu::model()->updateAppSettings($studio_id,$app_sum);
                    $menu_sum = array_sum($monetization);
                    MonetizationMenuSettings::model()->updateMenuSettings($studio_id,$menu_sum);
                    if($content_sum){
                        $child_content_sum = ($content_sum==4)?24:($content_sum==7)?29:7;
                        $child_content_type = StudioContent::model()->updateChildContentSettings($studio_id,$child_content_sum);
                    }
                // Activate physical in signup step 2 
                    $content_physical = isset($req_data['content_physical']) ? $req_data['content_physical'] : array();
                    if(count($content_physical))
                    {
                        $studio_extensions_obj = new StudioExtension;
                        $studio_extensions_obj->extension_id = 3;
                        $studio_extensions_obj->studio_id = $studio_id;
                        $studio_extensions_obj->social_sharing_status = 1;
                        $studio_extensions_obj->comment_status = 1;
                        $studio_extensions_obj->status = 1;
                        $studio_extensions_obj->start_date = 'NOW()' ; 
                        $studio_extensions_obj->save();
       
                    }          
                    if($content_sum == 4){
                        $template  = 'audio-streaming';
                        $dummy_data = 0;
                    }elseif($content_sum == 0 && !empty($content_physical)){
                        $template  = 'physical';
                        $dummy_data = 0;
                    }elseif($content_sum == 3 && !empty($content_physical) ){
                        $template  = 'classic-byod';
                    }else{
                        $template  = 'modern-byod';
                    }
 ///////////////////adding monetization and content to studio  end 
                    $relation->addDefaultContentType($studio_id);
                    $relation->setupStudio($newuserid, $studio_id, $template,$dummy_data);

                    Yii::app()->email->customerByReseller($req_data, Yii::app()->user->first_name);
                    $req_data['password'] = $password;
                    $req_data['domain'] = $domain;
                    $req_data['name'] = $req_data['customer_name'];
                    Yii::app()->email->mailToResellerscustomer($req_data);
                   
                    /*  Add user to Hubspot and madmimi */
                    $nm = explode(" ", $req_data['customer_name'], 2);
                    Hubspot::AddToHubspot($req_data['email'], $nm[0], $nm[1], $req_data['phone']);
                    $user = array('email' => $req_data['email'], 'firstName' => $nm[0], 'lastName' => $nm[1], 'add_list' => 'Muvi All');
                    require('MadMimi.class.php');
                    $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
                    $mimi->AddUser($user);
                    /* * * END of hubspot and madmimi *** */
                    $msg['isSuccess'] = 'success';
                    $msg['Message'] = 'New Customer Added sucessfully';
                    $msg['userid'] = $newuserid;
                    $msg['studioid'] = $studio_id;
                    /*if ($_REQUEST['relation_type'] == 'master') {
                        $msg['relation_type'] = 'Master';
                    } else */if ($req_data['relation_type'] == 'referrer') {
                        $msg['relation_type'] = 'Referrer';
                    } else if ($req_data['relation_type'] == 'reseller') {
                        $msg['relation_type'] = 'Reseller';
                    }
                    return json_encode($msg);
                    exit;
                }
            }
        
    }
    function createUniqSubdomain($domainName) {
        if ($domainName) {
            $domain = str_replace('www.', '', $domainName);
            $domain = explode('.', $domain);
            $subdomain = $domain[0];
            if ($subdomain) {
                if (in_array($subdomain, $this->reservedDomain)) {
                    $subdomain = $subdomain . '1';
                }
                $studio = new Studio();
                $avalDomain = $studio->checkSubdomain($subdomain);
                return $avalDomain;
            } else {
                return FALSE;
            }
        }     
    }   
    
      public  function createClouldfrontUrl($studio){
        if ($studio->studio_s3bucket_id == 0 && $studio->new_cdn_users == 1 && $studio->signed_url == '' && $studio->unsigned_url == '' && $studio->video_url == '') {            
            $studio_id=$studio->id;
            $bucketDetailsArr = Yii::app()->common->getBucketInfo('', $studio_id);
            $accessKey = Yii::app()->params->s3_key;
            $secretKey = Yii::app()->params->s3_secret;
            $singedpath = '/' . $studio_id . '/EncodedVideo';
            $unsingedpath = '/' . $studio_id . '/public';
            $unsingedpathForVideo = '/' . $studio_id . '/RawVideo';                       
            if ((HOST_IP == '127.0.0.1') || (HOST_IP == '52.0.64.95')) {
                $stagingCDN = 'd2gx0xinochgze.cloudfront.net';
                $stagingID = 'E2P005RPS6TJU0';
                $cloudFrontDetails = $stagingCDN . $unsingedpath . ":::" . $stagingCDN . $singedpath . ":::" . $stagingCDN . $unsingedpathForVideo . ":::" . $stagingID . ":::" . $stagingID . ":::" . $stagingID;
                $studio->cdn_status = 1;
            } else if (DOMAIN == 'idogic') {
                $stagingID = 'E2P005RPS6TJU0';
                $cloudFrontDetails = $bucketDetailsArr['unsigned_cloudfront_url'] . $unsingedpath . ":::" . $bucketDetailsArr['cloudfront_url'] . $singedpath . ":::" . $bucketDetailsArr['videoRemoteUrl'] . $unsingedpathForVideo . ":::" . $stagingID . ":::" . $stagingID . ":::" . $stagingID;
                $studio->cdn_status = 1;
            } else {
                try{
                    $cloudFrontDetails = Yii::app()->aws->createCloudFontUrlForNewSignUp($accessKey, $secretKey, $bucketDetailsArr['bucket_name'], $studio_id);
                }catch (Exception $e) {
                    $cloudFrontDetails='';
                }
            }
            $cloudFrontDetails = !empty($cloudFrontDetails)?explode(":::", $cloudFrontDetails):array();
            if(count($cloudFrontDetails)>0){
                $studio->signed_url = $cloudFrontDetails[1];
                $studio->unsigned_url = $cloudFrontDetails[0];
                $studio->video_url = $cloudFrontDetails[2];
                $studioCdnDetails = New StudioCdnDetails;
                $studioCdnDetails->studio_id = $studio_id;
                $studioCdnDetails->signed_url_id = $cloudFrontDetails[4];
                $studioCdnDetails->unsigned_url_id = $cloudFrontDetails[3];
                $studioCdnDetails->video_url_id = $cloudFrontDetails[5];
                $studioCdnDetails->save();
                $studio->save();
            }else{
                //mail section
                $this->sendMailFailCloudfront($studio);                                    
            }
        }
        return true;
    }
    function actioncancelcustomer(){
        self::isAjaxRequest();   
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST); 
        $dbcon=$this->getDbConnection();
        $portal_user_id = $_POST['reseller_id'];
        $portal_user = PortalUser::model()->findByPk($portal_user_id);
        $req['name'] = $portal_user->name;
        $req['email'] = $portal_user->email;
        $studio_id = $_POST['studio_id'];
        $user_id = $_POST['user_id'];
       // $user = User::model()->findByPk($user_id);
        $user_sql = "select * from `user` where id = {$user_id}";
        $user = (object)$dbcon->createCommand($user_sql)->queryROW();
        $req['phone'] = $user->phone_no;
        $req['customer_email'] = $user->email;
        $req['customer_name'] = $user->first_name.' '.$user->last_name;
        //$studio = Studio::model()->findByPk($studio_id);
        $studio_sql = "select * from `user` where id = {$studio_sql}";
        $studio = (object)$dbcon->createCommand($user_sql)->queryROW();
        //$invoice_file = self::chargeBandwidthCost($studio,$user_id,$portal_user_id,$portal_user);
       // $has_card = CardInfos::model()->findByAttributes(array('studio_id'=>$studio_id));
        $has_card_sql = "select * from card_infos where studio_id = {$studio_id}";
        $has_card  =  $dbcon->createCommand($has_card_sql)->queryAll();  
        $count_has_card = count($has_card);
        if($count_has_card > 0){
          //  UserRelation::model()->delete_reseller_from_userRealation($studio_id,$portal_user_id);
            $delete_reseller_from_user_relation_sql = "delete from user_relation where studio_id = {$studio_id} and refer_id={$portal_user_id}";
            $command = $dbcon->createCommand($delete_reseller_from_user_relation_sql);
            $command->execute();           
            //Studio::model()->delink_reseller_customer($studio_id,$portal_user_id);
            $dlink_sql = "update studios set reseller_id = 0,delink_from = '".$portal_user_id."' where id = '".$studio_id."' ";
            $command = $dbcon->createCommand($dlink_sql);
            $command->execute();           
            //Yii::app()->email->delinkCustomerScriptionMailToReseller($req, $invoice_file);
            Yii::app()->email->cancelcustomerSubscriptionByResellerMailToSales($req);
            Yii::app()->user->setFlash('success', 'Customer has canceled successfully.');  
        }else{
            Yii::app()->user->setFlash('error', 'Sorry! This customer cannot be de-linked as no card\'s have been added to their account. Please add a card and try again.' );  
        }
        
    }
    public function set_application_sum($plan=''){
        $app = array();
        $plan_arr = explode(',',$plan);
        foreach($plan_arr as $key => $val){
                switch($val){
                   case 'monthly_charge':
                    $app[] = 1;
                   break;
                   case 'ios_app':
                    $app[] = 2;
                   break;
                   case 'android_app':
                    $app[] = 4;
                   break;   
                   case 'roku_app':
                    $app[] = 8;
                   break; 
                   case 'android_tv_app':
                    $app[] = 128;
                   break;
                   case 'fire_tv_app':
                    $app[] = 16;
                   break;  
                   case 'apple_tv_app':
                    $app[] = 32;
                   break;            
                }
        }
        return $app;
    } 
    public function actionReferrerCustomer() {
        $this->breadcrumbs = array('Referrer', 'New Referral');
        $this->headerinfo = "Add/Edit Referral";
        $this->pageTitle = "Reseller | Reseller Customer";      
        $studio_id = Yii::app()->common->getStudiosId();
        $domain = Yii::app()->common->getStudiodomain();
        $studio = array("id" => $studio_id, "domain" => $domain);
        $this->render('referrercustomer', array('studio' => $studio));
    } 
    public function actionInsertRefercustomer(){
       self::isAjaxRequest(); 
             
       parse_str($_POST['data'],$req_data) ;
       $portal_user_id = Yii::app()->user->id;
       $contact = array();
       foreach($req_data['name'] as $key => $val){
           $contact[$key]['name'] = $req_data['name'][$key];
           $contact[$key]['email'] = $req_data['email'][$key];
           $contact[$key]['phone'] = $req_data['phone'][$key];
           $contact[$key]['title'] = $req_data['title'][$key];
       }
       $contact_json = json_encode($contact);
       $usr = new ReferrerCustomer;
       $usr->business_name = $req_data['company_name'];
       $usr->contacts = $contact_json;
       $usr->add_date = new CDbExpression("NOW()");
       $usr->status = 1;
       $usr->referrer_id = $portal_user_id;
       $usr->business_website = $req_data['business_website'];
       $usr->notes = $req_data['notes'];
       $usr->save();
       Yii::app()->user->setFlash('success', 'Referral added sucessfully');
       echo 1;
       exit;
       
    }
    function actioncommision() {
		$this->breadcrumbs = array('Referrer', 'Referral Commision');
		$this->headerinfo = "Referral Commission";
		$this->pageTitle = "Referrer | Commission";
		$portal_user_id = Yii::app()->user->id;
		$linked_studios = ReferralCustomerCommision::model()->get_linked_studio($portal_user_id);
		$total_commision = 0;
		
		if (!empty($linked_studios)) {
			foreach ($linked_studios as $key => $val) {
				$billing_info = BillingInfos::model()->get_studio_billing_info($val['studio_id']);
				if (!empty($billing_info)) {
					$studio_pricing_plan = StudioPricingPlan::model()->get_studio_pricing_plan($val['studio_id']);
					$no_of_app = $studio_pricing_plan[0]['no_of_app'];
					$packeg = Package::model()->getPackages($studio_pricing_plan[0]['package_id']);
					$app_price = $packeg['package'][0]['price'];
					$base_price = $packeg['package'][0]['base_price'];
					$yearly_discount = $packeg['package'][0]['yearly_discount'];
					if ($studio_pricing_plan[0]['plans'] == 'Month') {
						$platform_fee = number_format((float) $base_price + (($no_of_app - 1) * $app_price), 2, '.', '');
					} else {
						$platform_fee = number_format((float) (($base_price * 12) - ((($base_price * 12) * $yearly_discount) / 100)), 2, '.', '');
					}
					$commision_discount = 15;
					$commision_earned = (($platform_fee * $commision_discount) / 100);
					$total_commision = round(($total_commision + $commision_earned), 2);
					$linked_studios[$key]['commision_earned'] = $commision_earned;
				}
			}
		}
		
		$get_amount_paid = ReferrerCommisionTransaction::model()->get_amount_paid($portal_user_id);
		$amount_paid = number_format((float) $get_amount_paid['total'], 2, '.', '');
		$total_commision = number_format((float) $total_commision, 2, '.', '');
		$to_be_paid = number_format((float) ($total_commision - $amount_paid), 2, '.', '');
		
		$this->render('commision', array('amount_paid' => $amount_paid, 'tobe_paid' => $to_be_paid, 'total_earned' => $total_commision, 'linked_studio' => $linked_studios));
	}

}
