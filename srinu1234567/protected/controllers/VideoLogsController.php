<?php

class VideoLogsController extends Controller {
        
    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = false;
        Yii::app()->layout = false;
        $actionArr = array('videoViewlog');
        if(in_array(Yii::app()->controller->action->id, $actionArr)){
            return true;
        }else{
            if (!(Yii::app()->user->id)) {
                Yii::app()->user->setFlash('error', 'Login to access the page.');
                $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/user/login');
                exit;
            }
        }
        return true;
    }

    public function actionaddDataToRestrictDevice() {
        $data = "";
        if(@$_REQUEST['movie_stream_id'] != '' && @$_REQUEST['id'] != ''){
            $postData = $_REQUEST;
            $postData['studio_id'] = Yii::app()->common->getStudioId();;
            $postData['user_id'] = Yii::app()->user->id;
            $data = RestrictDevice::model()->dataSave($postData);
        }
        echo $data;
    }
    
    public function actiondeleteDataFromRestrictDevice() {
        if(@$_REQUEST['id'] != ''){
            $data = RestrictDevice::model()->deleteData($_REQUEST['id']);
        }
    }
    
    public function actionvideoViewlog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        if (Yii::app()->aws->isIpAllowed()) {
            if ((isset(Yii::app()->user->add_video_log) && Yii::app()->user->add_video_log) || (!isset(Yii::app()->user->add_video_log))) {
                $data = array();
                $data = $_REQUEST;
                $data['movie_id'] = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
                $data['stream_id'] = isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0;
                $data['studio_id'] = isset($_REQUEST['studio_id']) ? $_REQUEST['studio_id'] : Yii::app()->common->getStudioId();
                $data['user_id'] = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
                $data['ip_address'] = $ip_address;
				$data['log_temp_id'] = (isset($_REQUEST['log_id_temp']) && intval($_REQUEST['log_id_temp'])) ? $_REQUEST['log_id_temp'] : 0;
                $video_log_id = VideoLogs::model()->dataSave($data);
                if ($video_log_id) {
                   $response = array(
                        'log_id_temp' => $video_log_id[0],
                        'log_id' => $video_log_id[1]
                    );
                    echo (json_encode($response));
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            }
        }
    }
}

