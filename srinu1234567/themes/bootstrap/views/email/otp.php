<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
    <tbody>
        <tr>
            <td align="left" style="padding-left:20px;"><img src="<?php echo EMAIL_LOGO; ?>"/></td>
        </tr>
        <tr>
            <td>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                        Hi <?php echo $params['name']; ?>, 
                                    </p>
                                    <p style="display:block;margin:0 0 17px">
                                        <?= $params['otp'] ?> is the OTP for login.
                                    </p>              
                                    <p style="display:block;margin:0 0 17px">
                                        OTP is valid for <?= $params['validUntil'] ?> hrs only. Do not share it with anyone.
                                    </p>
                                    
                                    <p style="display:block;margin:0 0 17px">
                                        Regards,<br/>
                                        <?php echo $params['studio_name']; ?>
                                    </p>
                                </div>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>