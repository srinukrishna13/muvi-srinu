<?php
class PushNotificationLog extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'push_notification_log';
    }
     
}