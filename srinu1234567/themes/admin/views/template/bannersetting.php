<input type="hidden" id="autoscroll" name="autoScroll" value="<?php echo $auto_scroll; ?>">
<input type="hidden" id="scrollduration" name="scrollduration" value="<?php echo $scrollInterval; ?>">
<style>
	.text-heading{
		font-weight: 400;
		color : #000000;
	}
	
	
</style>
                        <div class="row" id="banner_section">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group input-group">
                                            <label class="input-group-addon  p-l-0 text-heading">Banner Style</label>
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control banner_style">
                                                        <option value="style_1 " <?php if ($style_code == 'style_1') {echo "selected=selected";} ?>>Style 1</option>
                                                        <option value="style_2 "<?php if ($style_code == 'style_2') {echo "selected=selected";} ?>>Style 2</option>
                                                        <option value="style_3 "<?php if ($style_code == 'style_3') {echo "selected=selected";} ?>>Style 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                        <div class="text-center m-b-20 loaderDiv loader_banner"  style="display: none;">
                                        <div class="preloader pls-blue  ">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                          <div class="row m-t-20 banners"></div>
                

<script>
    $(document).ready(function(){
        defaultBanner();
        
    });
  $('select.banner_style').on('change', function() {
     var data_code =this.value;
      defaultBanner(data_code);
      $('#autoscroll').val('');
      $('#scrollduration').val('');
      
  });
    function defaultBanner(data_code){
      $.ajax({
                url: HTTP_ROOT + "/template/defaultbannerstyle",
                data: {banner_code:data_code},
                type: 'POST',
                beforeSend: function () {
                   $('.loader_banner').show();
                },
                 success: function(res) {
                     $('.loader_banner').hide();
                     $('.banners').html(res);
                     var thumb_visibe=$('#visible_thumb').val();
                     loadBanner();
                      initSliderOne();
                configSliderOne(thumb_visibe);
                var autoScroll=$('#autoscroll').val();
                var scrollInterval=$('#scrollduration').val();
                if (autoScroll == '1'){
                     var auto =true;
                }else{
                    var auto =false;
                }
            $('.flexslider').flexslider({
                slideshow: auto,
                controlNav: false,
                directionNav: false,
                slideshowSpeed: scrollInterval,
                start: function(){startSliderOne(); setActive();},
                after: function(slider){thumbAnimsliderOne(slider)}
            });
            


                 }
    });
}

</script>
