<?php

/**
 * This is the model class for table "celebrities".
 *
 * The followings are the available columns in table 'celebrities':
 * @property integer $id
 * @property string $name
 * @property string $celebrity_type
 * @property string $birthdate
 * @property string $birthplace
 * @property string $summary
 * @property string $twitterid
 * @property string $permalink
 * @property string $alias_name
 * @property string $language
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $ip
 * @property integer $created_by
 * @property string $created_date
 * @property integer $last_updated_by
 * @property string $last_updated_date
 */
Yii::import('ext.behaviors.SluggableBehavior');
class Celebrity extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'celebrities';
	}

	/**
	 * @return array validation rules for model attributes.
	*/ 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_by, last_updated_by', 'numerical', 'integerOnly'=>true),
			array('name, celebrity_type, birthplace, twitterid, permalink, alias_name, language', 'length', 'max'=>255),
			array('ip', 'length', 'max'=>30),
			array('birthdate, summary, meta_title, meta_description, meta_keywords, created_date, last_updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, celebrity_type, birthdate, birthplace, summary, twitterid, permalink, alias_name, language, meta_title, meta_description, meta_keywords, ip, created_by, created_date, last_updated_by, last_updated_date', 'safe', 'on'=>'search'),
		);
	}
        



public function behaviors()
{
    return  array(
        'sluggable' => array(
          'class'=>'ext.behaviors.SluggableBehavior',
          'columns' => array('name'),
          'unique' => true,
          'update' => false,
            ),
    );
}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'movies'=>array(self::MANY_MANY, 'Movie',
                    'movie_casts(celebrity_id, movie_id)'),
                    'poster' => array(self::HAS_ONE, 'Poster','object_id',
                        'condition'=>"object_type='celebrity'"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'celebrity_type' => 'Celebrity Type',
			'birthdate' => 'Birthdate',
			'birthplace' => 'Birthplace',
			'summary' => 'Summary',
			'twitterid' => 'Twitterid',
			'permalink' => 'Permalink',
			'alias_name' => 'Alias Name',
			'language' => 'Language',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'ip' => 'Ip',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'last_updated_by' => 'Last Updated By',
			'last_updated_date' => 'Last Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('celebrity_type',$this->celebrity_type,true);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('birthplace',$this->birthplace,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('twitterid',$this->twitterid,true);
		$criteria->compare('permalink',$this->permalink,true);
		$criteria->compare('alias_name',$this->alias_name,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('last_updated_by',$this->last_updated_by);
		$criteria->compare('last_updated_date',$this->last_updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Celebrities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public  function celeb_type(){
            //$celebrity_type = array("Actor", "Producer", "Director", "Musics", "Writer", "Cinematographer", "Distributor", "Editor", "Special Effects Supervisor", "Art Director", "Stunt Director", "Casting Director", "Location Manager", "Dialogue Director", "Sound Designer", "Makeup Artist", "Costume Designer", "Music Director", "Lyricist", "Choreographer", "Singer");
            $celebrity_type = array("Actor", "Director");

            return $celebrity_type;
        }
}
