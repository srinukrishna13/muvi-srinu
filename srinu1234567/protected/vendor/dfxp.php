<?php
/**
 * DFXP Parser
 * Allows manipulation of dxfp file and convert to .srt and .vtt format
 * @author prakas Chandra Nayak <prakash@muvi.com>
 * @link http://www.muvi.com
*/
define ('UTF32_BIG_ENDIAN_BOM'   , chr(0x00) . chr(0x00) . chr(0xFE) . chr(0xFF));
define ('UTF32_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE) . chr(0x00) . chr(0x00));
define ('UTF16_BIG_ENDIAN_BOM'   , chr(0xFE) . chr(0xFF));
define ('UTF16_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE));
define ('UTF8_BOM'               , chr(0xEF) . chr(0xBB) . chr(0xBF));

class dfxpFile{	
	/**
	 * Default encoding if unable to detect
	 */
	const default_encoding = 'Windows-1252';
	
	/**
	 * Original filename
	 * 
	 * @var string
	 */
	private $filename;

	/**
	 * Current buffer of the file
	 * 
	 * @var string
	 */
	private $file_content;	
	/**
	 * Original file encoding
	 * 
	 * @var string
	 */
	private $encoding;

	/**
	 * File has BOM or not 
	 *
	 * @var boolean
	 */
	private $has_BOM = false;
	
	/**
	 * Build as WebVTT file
	 * 
	 * @var boolean
	 */
	private $is_WebVTT = false;
	
	/**
	 * srtFile constructor
	 * 
	 * @param string filename
	 * @param string encoding of the file
	 */
	public function __construct($_filename, $_encoding = ''){
            $this->filename = $_filename;
            $this->encoding = $_encoding;		
            $this->loadContent();                		
	}
	/**
	 * Getters
	 */
	public function getFileContent(){
		return $this->file_content;
	}
	public function getEncoding(){
		return $this->encoding;
	}	
	public function getFilename(){
		return $this->filename;
	}
	public function isWebVTT(){
		return $this->is_WebVTT;
	}		
	/**
	 * Converts Windows-1252 (CP-1252) to UTF-8
	 * @param string $text The string to be converted
	 */
	public static function cp1252_to_utf8($text) {
		$buffer = $text;
		$cp1252 = array(
			128 => "€", # euro sign
			130 => "‚", # single low-9 quotation mark
			131 => "ƒ", # latin small letter f with hook
			132 => "„", # double low-9 quotation mark
			133 => "…", # horizontal ellipsis
			134 => "†", # dagger
			135 => "‡", # double dagger
			136 => "ˆ", # modifier letter circumflex accent
			137 => "‰", # per mille sign
			138 => "Š", # latin capital letter s with caron
			139 => "‹", # single left-pointing angle quotation mark
			140 => "Œ", # latin capital ligature oe
			142 => "Ž", # latin capital letter z with caron
			145 => "‘", # left single quotation mark
			146 => "’", # right single quotation mark
			147 => "“", # left double quotation mark
			148 => "�?", # right double quotation mark
			149 => "•", # bullet
			150 => "–", # en dash
			151 => "—", # em dash
			152 =>  "˜", # small tilde
			153 => "™", # trade mark sign
			154 =>  "š", # latin small letter s with caron
			155 => "›", # single right-pointing angle quotation mark
			156 => "œ", # latin small ligature oe
			158 => "ž", # latin small letter z with caron
			159 => "Ÿ" # latin capital letter y with diaeresis
		);
		$buffer_encoded = utf8_encode($buffer);
		
		foreach($cp1252 as $ord => $encoded){
			$buffer_encoded=str_replace(utf8_encode(chr($ord)), $encoded, $buffer_encoded);
		}
		return $buffer_encoded;
	}

	/**
	 * Detects UTF encoding
	 * @param string $text
	 */
	public static function detect_utf_encoding($text) {
		$first2 = substr($text, 0, 2);
		$first3 = substr($text, 0, 3);
		$first4 = substr($text, 0, 4);
		if ($first3 == UTF8_BOM) return 'UTF-8';
		elseif ($first4 == UTF32_BIG_ENDIAN_BOM) return 'UTF-32BE';
		elseif ($first4 == UTF32_LITTLE_ENDIAN_BOM) return 'UTF-32LE';
		elseif ($first2 == UTF16_BIG_ENDIAN_BOM) return 'UTF-16BE';
		elseif ($first2 == UTF16_LITTLE_ENDIAN_BOM) return 'UTF-16LE';
		else return false;
	}

	/**
	 * Converts file content to UTF-8
	 */
	private function convertEncoding(){
            $exec = array();
            exec('file -i "'.$this->filename.'"', $exec);
            $res_exec = explode('=', $exec[0]);                
            if(empty($res_exec[1]))
                    throw new Exception('Unable to detect file encoding.');
            $this->encoding = $res_exec[1];		
            $tmp_encoding = strtolower($this->encoding);		
            switch($tmp_encoding){
                case 'unknown':
                case 'windows-1252':
                case 'iso-8859-1':
                case 'iso-8859-15':
                case 'us-ascii':
                    $this->encoding = self::default_encoding;
                    $this->file_content = self::cp1252_to_utf8($this->file_content);
                break;
                default:
                    if(strtolower(substr($this->encoding, 0, 3)) == 'utf' && self::detect_utf_encoding($this->file_content))
                            $this->has_BOM = true;

                    if(strtolower(substr($this->encoding, 0, 7)) == 'unknown'){
                            $this->encoding = self::default_encoding;
                            $this->file_content = self::cp1252_to_utf8($this->file_content);		
                    }else{
                            $this->file_content = mb_convert_encoding($this->file_content, 'UTF-8', $this->encoding);
                    }
                break;
            }
	}
	/**
	 * Loads file content and detect file encoding if undefined
	 */
	private function loadContent(){
            if(!file_exists($this->filename))
                    throw new Exception('File "'.$this->filename.'" not found.');

            if(!($this->file_content = file_get_contents($this->filename)))
                    throw new Exception($this->filename.' is not a proper .dfxp file.');

            $this->file_content .= "\n\n"; // fixes files missing blank lines at the end               
            //$this->convertEncoding();
	}
        /**
         * Read data from DFXP file and convert to srt format
         */        
        public function buildSrt(){
            $cont = str_replace(['<br/>', '<br />'], "\n", $this->file_content);
            $xml = simplexml_load_string($cont);
            $subs = $xml->body->div->p;
            $num = 1;
            $file_data='';
            foreach ($subs as $i => $sub) {
                $attrs = $sub->attributes();
                $begin = $attrs['begin'];
                $end = $attrs['end'];   
                if($sub->count()>0){ 
                    $text = $sub->asXML();                           
                    $text = (string) $text;
                }else{           
                    $text = (string) $sub;       
                }
                $text = trim(strip_tags($text));    
                // remove weird spacings that sometimes come after a newline
                // due to xml formatting and >1 newlines.
                $text = preg_replace([',\n+[ ]+,', ',\n+,'], "\n", $text);
                $timecode = $this->calcTimecode($begin, $end);
                // Output in Subrip format
                $file_data.=$num."\n".$timecode."\r\n".$text."\r\n\r\n";                
                $num++;
            }
            $this->file_content=$file_data;            
        }
        /**
         * Read data from DFXP file and convert to webvtt format
         */   
        public function buildVtt(){
            $this->is_WebVTT=true;
            $this->encoding = 'utf-8';
            $cont = str_replace(['<br/>', '<br />'], "\n", $this->file_content);
            $xml = simplexml_load_string($cont);
            $subs = $xml->body->div->p;
            $num = 1;
            $file_data="WEBVTT\r\n\r\n";
            foreach ($subs as $i => $sub) {
                $attrs = $sub->attributes();
                $begin = $attrs['begin'];
                $end = $attrs['end'];   
                if($sub->count()>0){ 
                    $text = $sub->asXML();                           
                    $text = (string) $text;
                }else{           
                    $text = (string) $sub;       
                }
                $text = trim(strip_tags($text));    
                // remove weird spacings that sometimes come after a newline
                // due to xml formatting and >1 newlines.
                $text = preg_replace([',\n+[ ]+,', ',\n+,'], "\n", $text);
                $timecode = $this->calcTimecode($begin, $end);
                // Output in Subrip format
                $file_data.=$num."\n".$timecode."\r\n".$text."\r\n\r\n";                
                $num++;
            }
            $this->file_content=$file_data;           
        }
        /**
         * 
         * @param type $orig   as the begin time
         * @param type $end as the end time
         * @return type string as the combination of begin and end time
         */
        private function calcTimecode($orig, $end) {
            // Split hours, minutes, seconds and ms
            $orig = preg_split('/[:.,]+/', $orig);
            $add = preg_split('/[:.,]+/', $end);
            // A variable for each unit, for readability
            $o_h = $orig[0];
            $o_m = $orig[1];
            $o_s = $orig[2];
            $o_ms = $orig[3];
            // A variable for each unit, for readability
            $a_h = $add[0];
            $a_m = $add[1];
            $a_s = $add[2];
            $a_ms = $add[3];

            $o_ms =(2 == strlen($o_ms)) ? "0" . $o_ms : ((1 == strlen($o_ms)) ? "00" . $o_ms : $o_ms);
            $a_ms =(2 == strlen($a_ms)) ? "0" . $a_ms : ((1 == strlen($a_ms)) ? "00" . $a_ms : $a_ms);
            //set . or , depend on file format
            $separator=($this->isWebVTT())?'.':',';
            $o = "{$o_h}:{$o_m}:{$o_s}{$separator}{$o_ms}";
            $a = "{$a_h}:{$a_m}:{$a_s}{$separator}{$a_ms}";
            return "{$o} --> {$a}";
        }					
	/**
	 * Saves the file
	 *
	 * @param string $filename	 
	 */
	public function save($filename = null){
            if($filename == null){                            
                $primary_name=substr($filename,0, strrpos($this->filename,'.'));                
                $fname = preg_replace('/[^a-zA-Z0-9_.]/', '-', $primary_name);
                $filename=($this->isWebVTT())?$fname.'.vtt':$fname.'.srt';                              
            }
            $content = $this->file_content;			
            if(strtolower($this->encoding) != 'utf-8')
                $file_content = mb_convert_encoding($content, $this->encoding, 'UTF-8');
            else if(strtolower($this->encoding) == 'utf-8' && $this->has_BOM && substr($content, 0, 3) != UTF8_BOM)
                $file_content = UTF8_BOM . $content;
            else
                $file_content = $content;             
            $res = file_put_contents($filename, $file_content);            
            if(!$res)
                 throw new Exception('Unable to save the file.');
	}	
}