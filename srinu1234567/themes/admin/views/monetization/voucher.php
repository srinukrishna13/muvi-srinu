<style type="text/css">
    .ui-datepicker{
        z-index: 99999 !important;
    }
</style>
<?php
$studio = $this->studio;
?>
<?php 
$selcontents = '';
if (isset($content) && !empty($content)) {
    $selcontents = json_encode($content);
}
?>
<div class="row m-b-20 m-t-40">
    <div class="col-sm-4">
        <div class="form-group  input-group">
            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
            <div class="fg-line">
                <input class="filter_input form-control fg-input" id="search_content" placeholder="What are you searching for?" type="text">
            </div>
        </div>
    </div> 
    <div class="col-sm-4">
        <button class=" topper btn btn-primary" id="new_voucher" class="btn btn-primary margin-bottom" data-toggle="modal" data-target="#myModal" > New Voucher </button>
        <button class=" topper btn btn-primary" onclick="couponcsv_download();">Download CSV </button>
        <button class=" topper btn btn-default-with-bg"  class="btn btn-primary margin-bottom" onclick="couponmsg_check();">Delete</button>
       
    </div> 
    <div class="col-sm-4 ">
        <?php
           if($items_count>=20){ 
                $opts = array('class' => 'pagination m-t-0 pull-right');
                $this->widget('CLinkPager', array(
                    'currentPage'=>$pages->getCurrentPage(),
                    'itemCount'=>$items_count,
                    'pageSize'=>$page_size,
                    "htmlOptions" => $opts,
                    'maxButtonCount'=>6,
                    'nextPageLabel' => '&raquo;',
                    'prevPageLabel'=>'&laquo;',
                    'selectedPageCssClass' => 'active',
                    'lastPageLabel'=>'',
                    'firstPageLabel'=>'',
                    'header'=>'',
                ));
           }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form method="post" action="javascript:void(0);" id="mngCouponForm" name="mngCouponForm" >
            <table class="table table-hover" id="coupon_table">
                <thead>
                
                    <tr>
                        <th class="max-width-40"><div class="checkbox m-b-0"><label><input class="chkall" id="selectall" type="checkbox" name="checkall" onclick="checkUncheckAll();" /><i class="input-helper"></i> </label></div></th>
                        <th>Voucher</th>
                        <th>Voucher Type</th>

                <th data-hide="phone">Content</th>
                <th data-hide="phone">Restrict Usage</th> 
                </tr>
                </thead>
                <tbody>

                    <?php
                    $first=$cnt =1;
					if ($data) {
						$first = $cnt = (($pages->getCurrentPage()) * 20) + 1;
                    foreach ($data as $key => $value) {
                         $cnt++;
							$ccode = $value['voucher_code'];
                            ?>
                    <tr>
                            <input type="hidden" name='studio_id' value='<?php echo $value['studio_id']; ?>' />
                            <td>
                                <div class="checkbox m-b-0">
                                    <label>
                                        <input class="chkind" type="checkbox" name="data[]" value="<?php echo $value['id'];?>" onclick="checkUncheckAll(1);" />
                                        <i class="input-helper"></i> 
                                    </label>
                                </div>
                            </td>
                                                                          
                            <td style="color:#33CCCC;"> <a href="#" data-toggle="modal" data-target="#myModal1" onclick="Coupon_history('<?php echo $ccode; ?>')" name="coupon_history"><?php echo $value['voucher_code']; ?></a></td>
                            <?php
                        echo "<td>";
                        if ($value['voucher_type'] == 1) {
                            echo "Multi-use";
                        } else {
                            echo "Once-use";
                        }
                        echo "</td>";
                        echo "<td>";
                        if($value['is_allcontent']!=1){
                            $content_str = CouponOnly::model()->getContentInfo($value['content']);
                            
                            if (strlen($content_str) > 30) {
                            $trimstring = substr($content_str, 0, 30). '<span title="'.$content_str.'">...</span>';
                            } else {
                            $trimstring = $content_str;
                            }
                            echo $trimstring;
                        }else{
                            echo "All";
                        }
                        echo "</td>";
                        ?>
                         <td><?php echo $value['restrict_usage']; ?></td>
                    </tr>
      <!-- /.box-header -->

                    <?php }
                } else {
                    ?>
                    <tr>
                            <td colspan="5">No Record found!!!</td>
                    </tr>	
            <?php	}?>
            </tbody>
        </table>
    </form>
    </div>
</div>


<!--here goes the code for modal-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" class="coupon_title" >New Voucher</h4>
                 <div class="form-horizontal">
                        
                    <div class="errorTxt red"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="Type"> Voucher Type:</label>
                        <div class="radio m-b-20">
                            <label>
                              <input id="amount_cash" type="radio" checked="checked" name="show_div" value="0" onclick="showCouponDiv('single_coupon');">
                              <i class="input-helper"></i>
                              Once-use
                            </label>
                            
                            <label>
                              <input id="amount_prcnt" type="radio" name="show_div" value="1" onclick="showCouponDiv('multiple_coupon');">
                              <i class="input-helper"></i>
                              Multi-use
                            </label>
                            <div id="coupon_message" class="grey"></div>
                          </div>
                    </div>
                </div>
            </div>
             
            <form class="form-horizontal" action="javascript:void(0)" name="single_coupon" id="single_coupon" method="post">
                <div class="modal-body">
                    <input type="hidden"  name="coupon_type" value="0"/>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="Coupons"> # of Voucher:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <input name="no_of_coupon" autocomplete="off" id="no_of_coupon" onKeyPress="checkValueOfNumb(this)" onKeyUp="checkValueOfNumb(this)" class="form-control input-sm" type="text" required="required" placeholder="Value between 1-50000">
                            </div>
                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="Valid"> Valid Until:</label>
                        <div class="col-sm-7">
                            <div class='row'>
                                <div class='col-sm-6'>
                                    <div class="input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                        <div class="fg-line">
                                            <input placeholder="From" id="datepicker" class="form-control input-sm" type="text" onchange="return checkForDate();" onkeypress="return onlyAlphabets(event, this);" required="required"  autocomplete="off" name="valid_from">
                                        </div>
                                    </div>
                                </div>
                                <div class='col-sm-6'>
                                    <div class="input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                        <div class="fg-line">
                                            <input placeholder="To" id="datepicker2" class="form-control input-sm" type="text" onchange="return checkForDate()" onkeypress="return onlyAlphabets(event, this);" required="required"  autocomplete="off" name="valid_until">
                                        </div>
                                        <span id="err_dt" style="color:#f55753"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($policyStatus == true){?>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="policies"> Rule:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <div class="select">
                                    <?php echo Yii::app()->Helper->rulesSelectBox($ppvPlans->rules_id, $policyStatus, $isPurchased);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="Type"> Applies to Content:</label>
                        <div class="radio m-b-20">
                            <label>
                                <input id="apply_cont_single_1" checked="checked" type="radio" name="apply_cont" value="1" >
                              <i class="input-helper"></i>
                              All Content
                            </label>
                            
                            <label>
                              <input id="apply_cont_single_2" type="radio" name="apply_cont" value="0" >
                              <i class="input-helper"></i>
                              Specific Content
                            </label>
                            <div id="coupon_message" class="grey"></div>
                          </div>

                    </div>
                    
                    <div class="form-group" id="content_space_1" style="display:none;">
                        <label class="control-label col-sm-4" for="Coupons">Content:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <select class="input-sm" data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content" required="required" multiple>
                                </select>
                            </div>
                            <small class="help-block">
                                <label id="data[content]-error" class="error red" for="data[content][]" style="display: none"></label>
                            </small>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">    
                    <button id="sub-btn" class="btn btn-primary btn-sm" name="sub-btn"  onclick="return validate_coupondata();">Submit</button>
                    <button class="btn btn-default btn-sm" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </form>
            <form class="form-horizontal" action="javascript:void(0)" name="multiple_coupon" id="multiple_coupon" method="post">
                <div class="modal-body">
                    <input type="hidden"  name="coupon_type" value="1"/>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="Coupons"> Restrict usage per user:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <input name="restrict_user" autocomplete="off" id="restrict_user"  class="form-control input-sm" type="text" required="required" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="Coupons"> Voucher Code:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <input name="coupon_code" autocomplete="off" minlength="3" maxlength="8" id="coupon_code" class="form-control input-sm" type="text" required="required" placeholder="Voucher Code" value="<?php
                                
                                    $character_set_array = array();
                                    $character_set_array[] = array('count' => 5, 'characters' => 'CDEFGHIJKLMNOPQRSTUVWXYZ');
                                    $character_set_array[] = array('count' => 3, 'characters' => '0123456789');
                                    $temp_array = array();
                                    foreach ($character_set_array as $character_set) {
                                        for ($i = 0; $i < $character_set['count']; $i++) {
                                            $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
                                        }
                                    }
                                    shuffle($temp_array);
                                    $a =  implode('', $temp_array);
                                        //echo '<pre>';
                                        //print_r($a);
                                     //echo $ccode;
                                    if($a!=$ccode)
                                    {
                                        echo "$a";
                                    }else{
                                        $character_set_array = array();
                                    $character_set_array[] = array('count' => 5, 'characters' => 'CDEFGHIJKLMNOPQRSTUVWXYZ');
                                    $character_set_array[] = array('count' => 3, 'characters' => '0123456789');
                                    $temp_array = array();
                                    foreach ($character_set_array as $character_set) {
                                        for ($i = 0; $i < $character_set['count']; $i++) {
                                            $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
                                        }
                                    }
                                    shuffle($temp_array);
                                    $b =  implode('', $temp_array);
                                    echo $b;
                                    }

                                ?>">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="Discount"> Valid Until:</label>
                        <div class="col-sm-7">
                            <div class='row'>
                                <div class='col-sm-6'>
                                    <div class="input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                        <div class="fg-line">
                                            <input id="datepicker_for_no" class="form-control input-sm" type="text" onchange="return checkForDateMulti();" onkeypress="return onlyAlphabets(event,this);" required="required"  autocomplete="off" placeholder="From" name="valid_from">
                                        </div>
                                    </div>
                                </div>
                                <div class='col-sm-6'>
                                     <div class="input-group">
                             <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                            <div class="fg-line">
                                <input id="datepicker_for_no2" class="form-control input-sm" type="text" onchange="return checkForDateMulti();" onkeypress="return onlyAlphabets(event,this);" required="required"  autocomplete="off" placeholder="To" name="valid_until">
                            </div>
                            <span id="err_dt2" style="color:#f55753"></span>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php if($policyStatus == true){?>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="policies"> Rule:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <div class="select">
                                    <?php echo Yii::app()->Helper->rulesSelectBox($ppvPlans->rules_id, $policyStatus, $isPurchased);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="Type"> Applies to Content:</label>
                        <div class="radio m-b-20">
                            <label>
                                <input id="apply_cont_multi_1" checked="checked" type="radio" name="apply_cont2" value="1" >
                              <i class="input-helper"></i>
                              All Content
                            </label>
                            <label>
                              <input id="apply_cont_multi_2" type="radio" name="apply_cont2" value="0" >
                              <i class="input-helper"></i>
                              Specific Content
                            </label>
                            <div id="coupon_message" class="grey"></div>
                          </div>
                    </div>
                    
                    <div class="form-group" id="content_space_2" style="display:none;">
                        <label class="control-label col-sm-4" for="Coupons"> Content:</label>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <select class="input-sm" data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content2" multiple>
                                    </select>
                            </div>
                            <small class="help-block">
                                    <label id="data[content]-error" class="error red" for="data[content][]" style="display: none"></label>
                                </small>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">  
                        <button id="sub-btn" class="btn btn-primary"  onclick="return validate_multicoupondata();">Submit</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
             </form>

        </div>
    </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Voucher History</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal"  name="hisory" id="hisory" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="coupon_code"> <div id="coupon_code_show" style="font-size:18px;"></div></label>  
                    <div class="col-sm-8"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Created_Date"> Coupon Type:</label>  
                    <div class="col-sm-8">
                        <div id="coupon_type" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="Created_Date"> Content:</label>  
                    <div class="col-sm-8">
                        <div id="content_str" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Created_Date"> Created Date:</label>  
                    <div class="col-sm-8">
                         <div id="created_date" class="history"></div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Valid_Until"> Valid Until :</label>   
                    <div class="col-sm-8">
                        <div id="valid_until" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Valid"> Valid?</label>     
                    <div class="col-sm-8">
                        <div id="is_valid" class="history"></div> 
                    </div>
                </div>
<!--                <div class="form-group">
                    <label class="col-sm-4 control-label" for="used"> Used?</label>     
                    <div class="col-sm-8">
                        <div id="is_valid_" class="history"></div>
                     </div>
                </div>-->
                <div class="form-group">
                   <label class="col-sm-4 control-label" for="used1"> Restrict Usage Per User:</label>     
                    <div class="col-sm-8">
                        <div id="restrict_use" class="history"></div>
                    </div>
                </div>
<!--                <div class="form-group" >
                    <label class="col-sm-4 control-label" for="used2"> Used On Date :</label>       
                    <div class="col-sm-8">
                        <div id="used_date" class="history"></div>
                     </div>
                </div>-->
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
       
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="deleteModal" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete Voucher?</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span id="coupon-msg">Are you sure  you want to<b> delete vouchers</b> </span> 
                    </div>
                </div>
                <div class="modal-footer">

                    <a href="javascript:void(0);" onclick="deleteAllCoupon();" id="sub-btn"  class="btn btn-default delete_coupon coupon_yes">Yes</a>


                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="showUserList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >List of user who used the coupon</h4>
            </div>
            <div class="modal-body">
                <div id="listofUser"></div>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

              </div>
        </div>
    </div>
</div>

<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>

<script>
    var sel_contents = '<?php echo $selcontents;?>';
    var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/monetization/getContents/voucher/1", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                    var item_name = $.trim(item.name);
                    if (item_name) {
                        item_name = item_name.replace("u0027", "'");
                    }
                    return item_name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            $("#content2").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                    var item_name = $.trim(item.name);
                    if (item_name) {
                        item_name = item_name.replace("u0027", "'");
                    }
                    return item_name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
            if (contnts.length) {
                for (var i in contnts) {
                    var content_name = $.trim(contnts[i].name);
                    if (content_name) {
                        content_name = content_name.replace("u0027", "'");
                        $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": content_name});
                        $("#content2").tagsinput('add', { "content_id": contnts[i].content_id , "name": content_name});
                    }
                }
            }
function getCouponDetails(searchText){
    $('.loader').show();
    $.post('/monetization/showVoucherSearch',{search_text:searchText},function(res){
        $('#coupon_table').html(res);
        $('.loader').hide();

    });
}
$(document.body).on('keyup','#search_content',function (event){
        var searchTextSub = $.trim($("#search_content").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextSub.length >= 2 || searchTextSub.length <= 2)){
            getCouponDetails(searchTextSub);
        }
    });
    $(function () {
        $("#datepicker").datepicker({minDate: 0});
        $("#datepicker2").datepicker({minDate: 0});
        $("#datepicker_for_no").datepicker({minDate: 0});
        $("#datepicker_for_no2").datepicker({minDate: 0});
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/ajaxForCouponCurrency", {'discount_type': 0, 'coupon_type': 'once'}, function (res) {
            $("#coupon_disc_amt_acr_type").html(res);
            $("#coupon_disc_amt_acr_type_for_multi").html(res);
        });
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/ajaxForCouponCurrency", {'discount_type': 0, 'coupon_type': 'multi'}, function (res) {
            $("#coupon_disc_amt_acr_type_for_multi").html(res);
        });
    });
    $(document).ready(function () {
        showCouponDiv("single_coupon");
    });
    function couponcsv_download() {
		var searchText = $.trim($("#search_content").val());
        window.location.href = HTTP_ROOT + "/monetization/voucherCsv_download/searchText/"+searchText;
        return false;
    }


    function Coupon_history(coupon_code) {
         var cpn_code = coupon_code;
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/Voucher_history";
        $.ajax({
            type: "POST",
            dataType: "json",
            url:  url,
            data: { cpn_code: coupon_code },
            success: function(data){
             
                console.log(data);
                  
                $("#modal1").modal('show');
                $('#coupon_code_show').html(data.coupon_code);
                $('#created_date').html(data.created_date);
                $('#coupon_type').html(data.coupon_type);
                $('#coupon_use').html(data.coupon_use);
                $('#content_str').html(data.content);
                $('#restrict_use').html(data.restrict);
            $('#discount_type').html(data.discount_type);
                if(data.discount_type=='0'){
                   $('#discount_type').html('Cash');
               }
               else{
                   $('#discount_type').html('%');
               }
               $('#discount_amount').html(data.discount_amount);
                $('#valid_until').html("From: "+data.valid_from+" - To: "+data.valid_until);
               //  $('#is_valid').html(data.is_valid);
                 $('#is_valid').html(data.is_valid);
                 //$('#is_valid_').html(data.is_valid);
//                if(data.cupon_used=='0'){
//                   $('#is_valid_').html('No');
//               }
//               else{
//                   $('#is_valid_').html('Yes');
//               }
                $('#used_by').html(data.used_by);
               //$('#used_by').html(data.used_by);
                if(data.cupon_used=='0'){
                   $('#used_date').html('-');
               }
               else{
                   $('#used_date').html(data.used_date);
               }
                //$('#used_date').html(data.used_date);
            }
        });               
    }

  
    function checkUncheckAll(arg) {
    if (parseInt(arg)) {
        $(".chkind").prop('click', function () {
            if ($(this).is(':checked')) {
                var len = $(".chkind").length;
                var ind_len = 0;
                $(".chkind").each(function () {
                    if ($(this).is(':checked')) {
                        ind_len++;
                    }
                });

                if (parseInt(len) === parseInt(ind_len)) {
                    $(".chkall").prop("checked", true);
                }
            } else {
                $(".chkall").prop("checked", false);
            }
        });
    } else {
        $(".chkall").prop('click', function () {
            if ($(this).is(':checked')) {
                $(".chkind").prop("checked", true);
            } else {
                $(".chkind").prop("checked", false);
            }
        });
    }
}
    function deleteAllCoupon() {
        if ($(".chkind:checked").length) {
            if ($('.delete_coupon').html()) {
                var action = '<?php echo Yii::app()->baseUrl; ?>/monetization/deleteAllVoucher';
                $('#mngCouponForm').attr("action", action);
                $('#mngCouponForm').submit();
            }
        } else if ($(".chkind:checked").length == '0') {
            return false;
        }
    }
    
    $('#no_of_coupon').on("keyup",function(event) {
        if($(this).val() == '0'){
            $(this).val('');  
         }
    });
    $('#no_of_coupon').on("keypress",function(event) {
        return numbersonly(event);
    });

    $('#no_of_coupon').on("contextmenu",function(event) {
        return false;
    });
    
    $('#restrict_user').on("keyup",function(event) {
        if($(this).val() == '0'){
            $(this).val('');  
         }
    });
    
    $('#restrict_user').on("keypress",function(event) {
        return numbersonly(event);
    });

    $('#restrict_user').on("contextmenu",function(event) {
        return false;
    });
    
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
    
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    
    function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}
    function validate_coupondata() {
        var ifSubmit = 0;
        if(ifSubmit === 0){
            $("#single_coupon").validate({
                rules: {
                    "no_of_coupon": {
                        required: true
                    },
                    "datepicker": {
                        required: true
                    },
                    "datepicker2": {
                        required: true
                    }
                },
                messages: {
                    "no_of_coupon": {
                        required: "No of Voucher to be generated!"
                    },
                    "datepicker": {
                        required: 'Please provide Valid From Date!'
                    },
                    "datepicker2": {
                        required: 'Please provide Valid To Date!'
                    }
                },
                errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
                },
                submitHandler: function (form) {
                    if($("#apply_cont_single_1").is(":checked")){
                        document.single_coupon.action = '<?php echo $this->createUrl('monetization/addVoucher');?>';
                        document.single_coupon.submit();
                        return false;
                    }else{
                        if (!$.trim($("#content").val()) ) {
                           swal("Please add content");
                           return false;
                       } else {
                       document.single_coupon.action = '<?php echo $this->createUrl('monetization/addVoucher');?>';
                       document.single_coupon.submit();
                       return false;    
                       }
                    }
                    
                }
            });
        }
    }
    function validate_multicoupondata(){
        var ifSubmit = 0;
        
        if(ifSubmit === 0){
            $("#multiple_coupon").validate({
            rules: {
                    "coupon_code": {
                        required: true,
                        minlength: 6
                    },
                    "datepicker_for_no": {
                        required: true
                    },
                    "datepicker_for_no2": {
                        required: true
                    }
                },
                messages: {
                    "coupon_code": {
                        required: "Please provide Voucher Code!"
                    },
                    "datepicker_for_no": {
                        required: 'Please provide Valid From Date!'
                    },
                    "datepicker_for_no2": {
                        required: 'Please provide Valid To Date!'
                    }
                },
                 errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
                },
                submitHandler: function (form) {
                    if($("#apply_cont_multi_1").is(":checked")){
                        document.multiple_coupon.action = '<?php echo $this->createUrl('monetization/addVoucher');?>';
                        document.multiple_coupon.submit();
                        return false;
                    }else{
                        if (!$.trim($("#content2").val()) ) {
                            swal("Please add content");
                            return false;
                        } else {
                        document.multiple_coupon.action = '<?php echo $this->createUrl('monetization/addVoucher');?>';
                        document.multiple_coupon.submit();
                        return false;
                        }
                    }
                    
                }
            });
        }
    }
    function couponmsg_check() {
        if ($(".chkind:checked").length) {
               swal({
                    title: "Delete Voucher?",
                    text: "Are you sure want to delete vouchers?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                  },
                  function(){
                    deleteAllCoupon();
                  });
        }else{
            var msg = "Please check atleast one voucher";
            swal("Delete Voucher?", msg);
        }
        
    }
    function showCouponDiv(divId){
        if(divId == 'single_coupon'){
             $("#coupon_message").html("Can be used only once");
            $("#single_coupon").show();
            $("#multiple_coupon").hide();
        }
        else if(divId == 'multiple_coupon'){
             $("#coupon_message").html("Can be used multiple times by multiple people");
            $("#single_coupon").hide();
            $("#multiple_coupon").show();
        }
    }
    function showUserEmail(id){
        $("#listofUser").html("");
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/showUserlist";
        $.post(url,{id:id},function(res){
            $("#listofUser").html(res);
        });
    }
    function checkValueOfNumb(input) {
        if (input.value < 0)
            input.value = 0;
        if (input.value > 50000)
            input.value = 50000;
    }
    
    function checkForDate(){
        var dt1 = $("#datepicker").val();
        var dt2 = $("#datepicker2").val();
        
        if(new Date(dt2) < new Date(dt1))
        {
            $("#datepicker2").val("");
            $("#err_dt").html("End Date should be greater then or equals to From Date!");
            return false;
        }else{
            $("#err_dt").html("");
            return true;
        }
    }
    function checkForDateMulti(){
        var dt1 = $("#datepicker_for_no").val();
        var dt2 = $("#datepicker_for_no2").val();
        
        if(new Date(dt2) < new Date(dt1))
        {
            $("#datepicker_for_no2").val("");
            $("#err_dt2").html("End Date should be greater then or equals to From Date!");
            return false;
        }else{
            $("#err_dt2").html("");
            return true;
        }
    }

    function showValOnly() {
        $("#valonly").css('display', 'block');
        $("#coupon_disc_amt_acr_type").css('display', 'none');
    }

    function showValOnlyMulti() {
        $("#valonlymulti").css('display', 'block');
        $("#coupon_disc_amt_acr_type_for_multi").css('display', 'none');
    }
    
    $("input[type='radio']:checked").each(function() {
        var idVal = $(this).attr("id");
    });
    
    function handleChange(input) {
        if (input.value < 0) input.value = 0;
        if (input.value > 100) input.value = 100;
    }
    
   $('#apply_cont_single_1').click(function(){
       $('#content_space_1').hide();
       $("#content").prop('disabled', 'disabled');
   });
  
    $('#apply_cont_single_2').click(function(){
       $('#content_space_1').show();
       $("#content").removeAttr("disabled");
   });
   $('#apply_cont_multi_1').click(function(){
       $('#content_space_2').hide();
       $("#content2").prop('disabled', 'disabled');
   });
  
    $('#apply_cont_multi_2').click(function(){
       $('#content_space_2').show();
       $("#content2").removeAttr("disabled");
   });
   
   $('#new_voucher').click(function(){
       $('#single_coupon')[0].reset();
       $("#multiple_coupon")[0].reset();
       $("#content").tagsinput('removeAll');
       $("#content2").tagsinput('removeAll');
       $('#content_space_2').hide();
       $('#content_space_1').hide();
   });
   
  $(document).ready(function(){
      $('#coupon_code').keyup(function(){
            this.value = this.value.toUpperCase();
            
        });
        
       $('#coupon_code').bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
        });
        
        

   });          
</script>

<style>
    .history{
        padding-top:7px;
    }
</style>

