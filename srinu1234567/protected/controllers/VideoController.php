<?php

require 's3bucket/aws-autoloader.php';

use Aws\CloudFront\CloudFrontClient;

class VideoController extends Controller {

    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            ?>
            <script type="text/javascript">
                history.go(-1);
            </script>
            <?php
            exit();
        }
        return true;
    }    
    
    public function actionplay_fullmovie() {
        
        Yii::app()->theme = 'bootstrap';    
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studios();
        $studio = $studio->findByPk($studio_id);        
        
        $movie_code = isset($_REQUEST['video']) ? $_REQUEST['video'] : '0';
        $movie_id = Yii::app()->common->getMovieId($movie_code);
        $movie = Film::model()->findByPk($movie_id);

        $stream_code = isset($_REQUEST['stream']) ? $_REQUEST['stream'] : '0';
        $season = isset($_REQUEST['season']) ? $_REQUEST['season'] : 0;

        $stream_id = 0;
        if (trim($movie_code) != '0' && trim($stream_code) != '0') {
            //Get stream Id
            $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code);
        } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
            $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season);
        } else {
            $stream_id = Yii::app()->common->getStreamId($movie_id);
        }        

        if (STUDIO_USER_ID == 55) {
            $mov_can_see = 'allowed';
        } 
        else {
            $can_see_data['movie_id'] = $movie_id;
            $can_see_data['stream_id'] = $stream_id;
                
            $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
        }     
        
        //If a user has never subscribed and trying to play video
        if ($mov_can_see == "unsubscribed") {
            Yii::app()->user->setFlash('error', 'Please activate your subscription to watch video.');
            $this->redirect(Yii::app()->getBaseUrl(true)."/user/activate");
            exit();
        } 
        //If a user had subscribed and cancel
        else if ($mov_can_see == "cancelled") {
            Yii::app()->user->setFlash('error', 'Please reactivate your subscription to watch video.');
            $this->redirect(Yii::app()->getBaseUrl(true)."/user/reactivate");
            exit();
        }  
        //If video is not allowed country
        else if ($mov_can_see == "limitedcountry") {
            Yii::app()->user->setFlash('error', 'This video is not allowed to play in your country.');
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit();
        }           
        else if ($mov_can_see == 'unpaid') {
            Yii::app()->user->setFlash('error', 'This is a PPV video. Pay and watch video now.');
            ?>
            <script type="text/javascript">
                history.go(-1);
            </script>
        <?php
            exit();
        } 
        else if ($mov_can_see == 'maximum') {
            Yii::app()->user->setFlash('error', 'Sorry, you have crossed the maximum limit of watching this content.');
            ?>
            <script type="text/javascript">
                history.go(-1);
            </script>
        <?php
            exit();
        }         
        
        else if ($mov_can_see == 'allowed') {
            
            $this->layout = false;
            $movie_stream = new movieStreams();
            $movie_strm = $movie_stream->findByPk($stream_id); 

            $fullmovie_path = $this->getFullVideoPath($stream_id);
            
            $v_logo = Yii::app()->general->getPlayerLogo($studio_id);

            $multipleVideo = array();
            $defaultResolution = 144;
            $multipleVideo = $this->getvideoResolution($movie_strm->video_resolution, $fullmovie_path);
            $videoToBeplayed =$this->getVideoToBePlayed($multipleVideo, $fullmovie_path);
            $videoToBeplayed12 = explode(",",$videoToBeplayed);
            $fullmovie_path = $videoToBeplayed12[0];
            $defaultResolution = $videoToBeplayed12[1];
            $wiki_data = $movie_strm->wiki_data;
            if ($wiki_data == "") {
                if(HOST_IP != '127.0.0.1'){
                $bucketInfo = Yii::app()->common->getBucketInfo('', Yii::app()->common->getStudioId());
                $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl);
                }
            } else {
                $rel_path = str_replace(" ", "%20", str_replace(CDN_HTTP."muviassetsdev.s3.amazonaws.com/", "", $fullmovie_path));
                if (HOST_IP != '127.0.0.1') {
                    $fullmovie_path = $this->getsecure_url($rel_path);
                }
            }
            $item_poster = $this->getPoster($movie_id, 'films', 'standard');
            $content_type = @$data[0]['content_type'];
            $per_link = @$data[0]['permalink'];
            
            //Adding the codes for the adservers RKS
            $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = '.$studio_id));
            
            $page_title = $movie->name;
            $contenttype = $movie->content_types_id;
            if($contenttype == 3){
                $page_title.= ' Season '.$movie_strm->series_number;
                if($movie_strm->episode_title != '')
                    $page_title.= ' '.$movie_strm->episode_title;
                else
                    $page_title.= ' Episode '.$movie_strm->episode_number;                    
            }
            $page_desc = 'You are watching '.$page_title.' at '.$studio->name.'.';
            
            $this->render('play_video', array('defaultResolution' => $defaultResolution, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => $mov_can_see, 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => $is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie->id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }        

    public function actiongetNewSignedUrlForPlayer() {
        $browserName = '';
        if ($_REQUEST['wiki_data'] == "") {
			$studio_id = Yii::app()->common->getStudioId();
			if((isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id'])){
				$bucketInfo = Yii::app()->common->getBucketInfo('', $_SESSION[$studio_id]['parent_studio_id']);
			}else{
				$bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
			}
            if (strpos($_REQUEST['video_url'],'d2qe8pp1v9ohei.cloudfront.net/5745/EncodedVideo/uploads/movie_stream/full_movie/82030/') !== false) {
                $bucketInfo['cloudfront_url'] = 'd2qe8pp1v9ohei.cloudfront.net';
            }
            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $_REQUEST['video_url']));
            if(isset($_REQUEST['browser_name']) && $_REQUEST['browser_name'] == 'Safari'){
                $browserName = $_REQUEST['browser_name'];
            }
            echo $video_url_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1,$browserName);exit;
        } else {
            $rel_path = str_replace(" ", "%20", str_replace(CDN_HTTP."muviassetsdev.s3.amazonaws.com/", "", $_REQUEST['video_url']));
            echo $video_url_path = $this->getsecure_url($rel_path);
            exit;
        }
    }
    
    public function actionplay_video() {
        Yii::app()->theme = 'admin';
        $this->layout = false;
        if(!isset(Yii::app()->user->id)){
            $redirect_url = Yii::app()->getBaseUrl(true);
            $this->redirect($redirect_url);
            exit;
        }
        $studio = Studio::model()->find(array("condition" => "id=" . STUDIO_USER_ID));
        $movie_stream_id = $_REQUEST['episode'] ? $_REQUEST['episode'] : $_REQUEST['movie_stream_id'];
        $data = movieStreams::model()->findByPk($movie_stream_id);
        $studio_id = $studio->id;
        $v_logo = Yii::app()->common->getLogoFavPath($studio_id);
        if ($data) {
            $data = $data->attributes;
            if($data['thirdparty_url'] != '') {
                $info = pathinfo($data['thirdparty_url']);
                if (trim($info["extension"]) == "m3u8"){ 
                    $this->render('play_m3u8',array('thirdparty_url'=>$data['thirdparty_url']));
                    exit;
                } else{
                    echo $data['thirdparty_url'];
                    exit();
                }
            } else {
            $s3dir = 'uploads/movie_stream/full_movie/';
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            $default_trailer = $this->getFullVideoPath($data["id"],"",$studio_id);
            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $default_trailer));
            $default_trailer = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,"","",60000);
            $subTitlesql = "select vs.id as subId, vs.filename,ls.* from video_subtitle vs,language_subtitle ls where vs.language_subtitle_id = ls.id and vs.video_gallery_id=" . $data["video_management_id"] ." and vs.studio_id = ".$studio_id." ORDER BY ( ls.name != 'English' ), ls.name ASC";
            $subTitleData = Yii::app()->db->createCommand($subTitlesql)->queryAll();
            $subtitleFiles = array();
            if($subTitleData){
                $i =1 ;
                foreach ($subTitleData as $subTitleDatakey => $subTitleDatavalue) {
                    $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];
                    $filePth = $clfntUrl."/".$bucketInfo['signedFolderPath']."subtitle/".$subTitleDatavalue['subId']."/".$subTitleDatavalue['filename'];
                    $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                    $subtitleFiles[$i]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,1,"",60000);
                    $subtitleFiles[$i]['code'] =  $subTitleDatavalue['code'];
                    $subtitleFiles[$i]['language'] =  $subTitleDatavalue['name'];
                    $i ++;
                }
            }
            
            $waterMarkOnPlayer = Yii::app()->general->playerWatermark();
            
            //$this->render('play_video',array('default_trailer'=>$default_trailer));
            $this->render('play_video', array('subtitleFiles' => $subtitleFiles,'can_see' => true, 'default_trailer' => $default_trailer,'v_logo' => $v_logo, 'is_free' => TRUE,'studio_id' => $studio_id,'waterMarkOnPlayer' => $waterMarkOnPlayer));
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! No video available for this contnet');
            $this->redirect($_SERVER['HTTP_REFERER']);
            exit;
        }
    }

    public function actionadd_log() {
        $ip_address = CHttpRequest::getUserHostAddress();
        if (Yii::app()->aws->isIpAllowed()) {
            if (isset(Yii::app()->user->add_video_log) && Yii::app()->user->add_video_log) {
                $movie_id = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
                $stream_id = isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0;

                $video_log = new VideoLogs();
                $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
                if ($log_id > 0) {
                    $video_log = VideoLogs::model()->findByPk($log_id);
                    $video_log->updated_date = new CDbExpression("NOW()");
                    $video_log->played_length = $_REQUEST['played_length'];
                    $video_log->played_percent = $_REQUEST['percent'];
                } else {
                    $video_log->created_date = new CDbExpression("NOW()");
                    $video_log->ip = $ip_address;
                    $video_log->video_length = $_REQUEST['video_length'];
                    $video_log->played_percent = $_REQUEST['percent'];
                    $video_log->played_length = $_REQUEST['played_length'];
                }

                $video_log->movie_id = $movie_id;
                $video_log->video_id = $stream_id;
                $video_log->user_id = Yii::app()->user->id;
                $video_log->studio_id = Yii::app()->common->getStudioId();
                $video_log->watch_status = $_REQUEST['status'];

                if ($video_log->save()) {
                    echo $video_log->id;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            }
        }
    }

    public function actionget_next_episode() {
        $epi_detail = array();
        $connectionwiki = Yii::app()->db;
        if ($_REQUEST['episode_number'] != '') {
            $cond = "ms.episode_number > " . $_REQUEST['episode_number'];
            $order = "ms.episode_number";
        } else {
            $cond = "ms.id > " . $_REQUEST['episode_id'];
            $order = "ms.id";
        }
        $sql = "SELECT ms.episode_number,ms.episode_title,ms.movie_id,ms.id as movie_stream_id,f.name FROM films f,movie_streams ms WHERE f.id=ms.movie_id  AND ms.movie_id = " . $_REQUEST['movie_id'] . " AND " . $cond . " AND ms.full_movie != '' order by " . $order . " ASC LIMIT 2";
        $episodes = $connectionwiki->createCommand($sql)->queryAll();
        foreach ($episodes as $k => $v) {
            $title = '';
            if ($v["episode_number"] == '') {
                $title .= $v["name"] . " " . $v["episode_title"];
            } else {
                $title .= $v["name"] . " Episode" . $v["episode_number"];
            }
            $epi_detail[$k]["epi_title"] = $title;
            $epi_detail[$k]["epi_number"] = $v["episode_number"];
            $epi_detail[$k]["epi_url"] = Yii::app()->baseUrl . "/video/play_fullmovie?movie=" . $v['movie_id'] . "&episode_id=" . $v['movie_stream_id'];
            $epi_detail[$k]["epi_poster"] = $this->getPoster($v['movie_stream_id'], 'moviestream', 'episode');
        }
        $this->renderPartial('next_episode', array("epi_detail" => $epi_detail));
    }

    function getsignedurl_old($resource, $expires) {
        // key pair generated for cloudfront
        $keyPairId = 'APKAJ5RUFF7WVAM5VHPQ';

        $json = '{"Statement":[{"Resource":"' . $resource . '","Condition":{"DateLessThan":{"AWS:EpochTime":' . $expires . '}}}]}';

        // read cloudfront private key pair
        $fp = fopen('pk-' . $keyPairId . '.pem', 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);

        // create the private key
        $key = openssl_get_privatekey($priv_key);

        // sign the policy with the private key
        // depending on your php version you might have to use
        // openssl_sign($json, $signed_policy, $key, OPENSSL_ALGO_SHA1)
        openssl_sign($json, $signed_policy, $key);

        openssl_free_key($key);

        // create url safe signed policy
        $base64_signed_policy = base64_encode($signed_policy);
        $signature = str_replace(array('+', '=', '/'), array('-', '_', '~'), $base64_signed_policy);

        // construct the url
        $url = $resource . '?Expires=' . $expires . '&Signature=' . $signature . '&Key-Pair-Id=' . $keyPairId;

        return $url;
    }

    function getSignedURL($resource, $timeout) {
        //This comes from key pair you generated for cloudfront
        $keyPairId = "APKAJ5RUFF7WVAM5VHPQ";

        $expires = time() + $timeout; //Time out in seconds
        $json = '{"Statement":[{"Resource":"' . $resource . '","Condition":{"DateLessThan":{"AWS:EpochTime":' . $expires . '}}}]}';

        //Read Cloudfront Private Key Pair
        $fp = fopen('pk-' . $keyPairId . '.pem', 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);

        //Create the private key
        $key = openssl_get_privatekey($priv_key);
        if (!$key) {
            echo "<p>Failed to load private key!</p>";
            return;
        }

        //Sign the policy with the private key
        if (!openssl_sign($json, $signed_policy, $key, OPENSSL_ALGO_SHA1)) {
            echo '<p>Failed to sign policy: ' . openssl_error_string() . '</p>';
            return;
        }

        //Create url safe signed policy
        $base64_signed_policy = base64_encode($signed_policy);
        $signature = str_replace(array('+', '=', '/'), array('-', '_', '~'), $base64_signed_policy);

        //Construct the URL
        $url = $resource . '?Expires=' . $expires . '&Signature=' . $signature . '&Key-Pair-Id=' . $keyPairId;
        $act_url = str_replace(array('&', '='), array('%26', '%3D'), $url);
        return $act_url;
    }

    function getNewsignedURL($resource, $timeout = 5) {
        //This comes from key pair you generated for cloudfront
        $keyPairId = "APKAJYIDWFG3D6CNOYVA";

        $expires = time() + $timeout; //Time out in seconds
        $json = '{"Statement":[{"Resource":"' . $resource . '","Condition":{"DateLessThan":{"AWS:EpochTime":' . $expires . '}}}]}';

        //Read Cloudfront Private Key Pair
        $fp = fopen('pk-APKAJYIDWFG3D6CNOYVA.pem', 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);

        //Create the private key
        $key = openssl_get_privatekey($priv_key);
        if (!$key) {
            echo "<p>Failed to load private key!</p>";
            return;
        }

        //Sign the policy with the private key
        if (!openssl_sign($json, $signed_policy, $key, OPENSSL_ALGO_SHA1)) {
            echo '<p>Failed to sign policy: ' . openssl_error_string() . '</p>';
            return;
        }

        //Create url safe signed policy
        $base64_signed_policy = base64_encode($signed_policy);
        $signature = str_replace(array('+', '=', '/'), array('-', '_', '~'), $base64_signed_policy);

        //Construct the URL
        $url = $resource . '?Expires=' . $expires . '&Signature=' . $signature . '&Key-Pair-Id=' . $keyPairId;
        $act_url = str_replace(array('&', '='), array('%26', '%3D'), $url);
        return $act_url;
    }

    function getsecure_url($resourceKey, $streamHostUrl = "") {
        $this->layout = false;
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJ5RUFF7WVAM5VHPQ.pem',
                    'key_pair_id' => 'APKAJ5RUFF7WVAM5VHPQ',
        ));
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP.'d62co02q89w6j.cloudfront.net';
        }
        $expires = time() + 250;
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }

    function getnew_secure_url($resourceKey, $streamHostUrl = "", $isAjax = "",$browserName="") {
        $this->layout = false;
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJYIDWFG3D6CNOYVA.pem',
                    'key_pair_id' => 'APKAJYIDWFG3D6CNOYVA',
        ));
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP.'d3ff6jhdmx1yhu.cloudfront.net';
        }
        if (Yii::app()->common->isMobile() || $browserName == "Safari") {
            $expires = time() + 50;
        }else if ($isAjax == 1) {
            $expires = time() + 20;
        } else {
            $expires = time() + 5;
        }

        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }
    

    public function actiontest() {
        $this->layout = false;
        $vid_type = isset($_REQUEST['vid_test']) ? $_REQUEST['vid_test'] : "";
        if ($vid_type == "") {
            $buk_name = "muviassetsdev";
            $file_path = "movieVideo/8449/Wild Russia_ Kamchatka_(360p).mp4";
            $is_signed = "Yes";
            $resourceKey = 'movieVideo/8449/Wild Russia_ Kamchatka_(360p).mp4';
            $vid_url = $this->getsecure_url($resourceKey);
        } elseif ($vid_type == "aram_test") {
            $buk_name = "muviaramtest";
            $file_path = "Festival of Colors - World s BIGGEST color party.mp4";
            $is_signed = "NO";
            $vid_url = CDN_HTTP."muviaramtest.s3.amazonaws.com/Festival%20of%20Colors%20-%20World%20s%20BIGGEST%20color%20party.mp4";
        } elseif ($vid_type == "aram_test_drm") {
            $buk_name = "muviaramtest";
            $file_path = "Festival of Colors - World s BIGGEST color party.mp4";
            $is_signed = "Yes";
            $resourceKey = 'Festival of Colors - World s BIGGEST color party.mp4';
            $vid_url = $this->getsecure_url($resourceKey, CDN_HTTP.'d3v2rtj7m39qyk.cloudfront.net');
        } elseif ($vid_type == "muvi_test") {
            $buk_name = "muviassetsdev";
            $file_path = "test_video/Festival of Colors - World s BIGGEST color party.mp4";
            $is_signed = "NO";
            $vid_url = CDN_HTTP."muviassetsdev.s3.amazonaws.com/test_video/Festival%20of%20Colors%20-%20World%20s%20BIGGEST%20color%20party.mp4";
        } elseif ($vid_type == "muvi_test_drm") {
            $buk_name = "muviassetsdev";
            $file_path = "test_video/Festival of Colors - World s BIGGEST color party.mp4";
            $is_signed = "Yes";
            $resourceKey = str_replace(" ", "%20", 'test_video/Festival of Colors - World s BIGGEST color party.mp4');
            $vid_url = $this->getsecure_url($resourceKey);
        }
        $streamHostUrl = CDN_HTTP."d3ff6jhdmx1yhu.cloudfront.net/uploads/movie_stream/full_movie/1969/Batman_The_Dark_Knight_2008.mp4";
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJYIDWFG3D6CNOYVA.pem',
                    'key_pair_id' => 'APKAJYIDWFG3D6CNOYVA',
        ));

        $expires = time() + 14400;
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl,
            'expires' => $expires,
        ));
        echo $signedUrlCannedPolicy;
        exit;
        $this->render('test_player');
        //$this->render('play_test',array('vid_url'=>$vid_url,'buk_name' => $buk_name, 'file_path' => $file_path, 'is_signed' => $is_signed ));
    }

    /**
     * @method public liveStreaming() Live Streaming Testing.
     * @author GDR<support@muvi.com>
     * @return HTML 
     */
    function actionLivestream() {
        $this->layout = false;
        Yii::app()->theme = 'bootstrap';
        $this->render('livestream');
    }

}
