var apphome = 0;
var control = "template";
$(document).ready(function(){
    if($("#apphome").length > 0){
        apphome = 1;
        control = "apps";
    } 
});
function fileSelectHandler() {
    document.getElementById("g_original_image").value = "";
    document.getElementById("g_image_file_name").value = "";
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    $(".jcrop-keymgr").css("display", "none");
    $("#celeb_preview").removeClass("hide");
    $('#uplad_buton').removeAttr('disabled');
    var oFile = $('#celeb_pic')[0].files[0];
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif)$/i;
    if (!rFilter.test(oFile.type)) {
        swal('Please select a valid image file (jpg, png and gif are allowed)');
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#uplad_buton').attr('disabled', 'disabled');
        return;
    }
    var aspectratio = img_width / img_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            swal('You have selected small file, please select one bigger image file');
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                    $('#glry_preview').width("450");
                    $('#glry_preview').height("250");
                }
                $('#preview').css("display", "block");
                $('#celeb_preview').css("display", "block");
                $('#preview').Jcrop({
                    minSize: [img_width, img_height], // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo,
                    setSelect:   [ 0, 0, img_width, img_height ]
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([0, 0, img_width, img_height]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    };
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function toggle_preview(id, img_src, name_of_image)
{
    $('#glry_preview').css("display", "block");
    document.getElementById("celeb_pic").value = "";
    showLoader();
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    var aspectratio = img_width / img_height;
    var image_file_name = name_of_image;
    var image_src = img_src;
    clearInfo();
    $("#g_image_file_name").val(image_file_name);
    $("#g_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            showLoader(1);
            swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
            $("#celeb_preview").addClass("hide");
            $("glry_preview").addClass("hide");
            $("#g_image_file_name").val("");
            $("#g_original_image").val("");
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('glry_preview');
        showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#glry_preview').width(oImage.naturalWidth);
                $('#glry_preview').height(oImage.naturalHeight);
                $('#preview').width("450");
                $('#preview').height("250");
            }
            $("#glry_preview").css("display", "block");
            $('#gallery_preview').css("display", "block");
            $('#glry_preview').Jcrop({
                minSize: [img_width, img_height], // min crop size
                aspectRatio: aspectratio, // keep aspect ratio 1:1
                boxWidth: 450,
                boxHeight: 250,
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfoallImage,
                onSelect: updateInfoallImage,
                onRelease: clearInfoallImage,
                setSelect:   [ 0, 0, img_width, img_height ]
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([0, 0, img_width, img_height]);
            });
        };
    };

}
function hide_file()
{
    $('#glry_preview').css("display", "none");
    $('#celeb_preview').css("display", "none");
    $('#preview').css("display", "none");
    document.getElementById('celeb_pic').value = null;
}
function hide_gallery()
{
    $('#preview').css("display", "none");
    $("#glry_preview").css("display", "block");
    $('#gallery_preview').css("display", "none");
    $("#g_image_file_name").val("");
    $("#g_original_image").val("");
}
function openImageModal(obj) {
    var name = $(obj).attr('data-name');
    if (name == "Banner") {
        var action = HTTP_ROOT + "/template/AddBannerImage";
        var section = $(obj).attr('id');
        $('#section_id').val(section);
        $('#section_id1').val(section);
        $.ajax({
            url: HTTP_ROOT + "/template/sectiondimention",
            data: {'section_id': section},
            dataType: 'json',
            success: function (result) {
                $(".upload_detail").html(name);
                $('#img_width').val(result.width);
                $('#img_height').val(result.height);
                $('.help-block').html('Upload a transparent image of size ' + result.width + 'x' + result.height + '</br> or </br> Upload GIF image');
                $('#help-video').html('Upload video in MP4 format (size limit 50MB)');
            }
        });
    } else {
        var width = $(obj).attr('data-width');
        var height = $(obj).attr('data-height');
        $(".upload_detail").html(name);
        $(".help-block").html("Upload a transparent image of size " + width + " x " + height);
        $("#img_width").val(width);
        $("#img_height").val(height);
        var action = HTTP_ROOT + "/template/Save" + name;
    }
    $("#upload_image_form").attr('action', action);
    $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#homePageModal").modal('show');
}
function openInvoiceImageModal(obj) {
    var name = $(obj).attr('data-name');
    var width = $(obj).attr('data-width');
    var height = $(obj).attr('data-height');
    $(".upload_detail").html(name);
    $(".help-block").html("Upload a transparent image of size " + width + " x " + height);
    $("#img_width").val(width);
    $("#img_height").val(height);
    var action = HTTP_ROOT + "/monetization/Save" + name;
    $("#upload_image_form").attr('action', action);
    $("#all_img_glry").load(HTTP_ROOT + "/monetization/invoiceimageGallery");
    $("#homePageModal").modal('show');
}
function openAppImageModal(obj) {
    refreshModal();
    var name   = $(obj).attr('data-name');
    var width  = $(obj).attr('data-width'); 
    var height = $(obj).attr('data-height');; 
    var action = HTTP_ROOT + "/template/AddBannerImage";
    $(".upload_detail").html(name);
    $('#img_width').val(width);
    $('#img_height').val(height);
    $('.help-block').html('Upload a transparent image of size ' + width + 'x' + height + '</br> or </br> Upload GIF image');
    $("#upload_image_form").attr('action', action);
    $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#homePageModal").modal('show');
}
function refreshModal(){
    hide_file();
    hide_gallery();
}
function click_browse(modal_file) {
    $("#" + modal_file).click();
}
function removeDisable() {
    $('.rmvdisable').removeAttr('disabled');
    return true;
}

function editBnr(bannerID)
{
    $('#bnr_frm_' + bannerID).show();
    $('#bnr_edit_' + bannerID).hide();
}
function saveBanner(bannerID)
{
    $('#banner_id').val(bannerID);
    $('#banner_text').val($('#bannertext_' + bannerID).val());
    $('#bannerText').submit();
    return true;
}
function cancelBanner(bannerID)
{
    $('#bnr_frm_' + bannerID).hide();
    $('#bnr_edit_' + bannerID).show();
}
$(document).ready(function () {
    $('#savetxt').click(function () {
        if ($('#show_join_btn:checked').length > 0 && $('#join_btn_txt').val() == '')
        {
            $('#bnr_txt_error').html("Please enter the sign up button text.");
            return false;
        }
        else
        {
            $('#update_text').val(1);
            $('#bannerText').submit();
            return true;
        }
    });
});
function deleteBanner(banner_id) {
    swal({
        title: "Remove Banner?",
        text: "Are you sure you want to remove the banner?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        window.location.href = HTTP_ROOT+"/template/removeBanner?banner_id=" + banner_id;
    });
}
function deleteBanner(banner_id, appBanner) {
    swal({
        title: "Remove Banner?",
        text: "Are you sure you want to remove the banner?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        window.location.href = HTTP_ROOT+"/"+control+"/removeBanner?banner_id=" + banner_id;
    });
}
function SortBanner(bannerid, sectionid, direction) {
    var seq_ary = $('#seq_ary').val();
    $.ajax({
        url: HTTP_ROOT+"/template/sortbanner",
        data: {'section_id': sectionid, 'banner_id': bannerid, 'direction': direction,'seq_ary':seq_ary},
        dataType: 'json',
        type: 'post',
        success: function(result) {
            window.location.href = HTTP_ROOT+"/template/homepage";
        }
    });
}
function deleteSection(section) {
    swal({
        title: "Delete Featured Section?",
        text: "Are you sure you want to <b>remove the Featured section</b> on Homepage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        removeSection(section);
    });
}
function deleteFeatured(section, featured_id) {
    swal({
        title: "Delete Featured Content?",
        text: "Are you sure you want to <b>remove the Featured Content</b> on Homepage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        removeFeatured(section, featured_id)
    });
}
function removeSection(section) {

    $.post(HTTP_ROOT + "/template/Removefeaturedsection", {'section': section, 'apphome' : apphome}, function (data) {
        if (data.err == 1)
        {
            swal("Error in deleting featured section.");
            return false;
        }
        else
        {
            window.location.href = HTTP_ROOT+"/"+control+"/homepage";
        }
    });

}
function removeFeatured(section, featured_id)
{
    $.ajax({
        url: HTTP_ROOT + "/template/Removefeatured",
        type:'POST',
        dataType:'json',
        data: {'section': section, 'featured_id': featured_id, 'apphome' : apphome},
        success: function (res) {
            if($.trim(res.status) == 'success'){
                $("#sortables_"+section).html(res.data);
            }
            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;'+res.message+'</div>'
            $('.alert.flash-msg').remove();
            $('.pace').prepend(sucmsg);
            $('html, body').animate({scrollTop : 0},800);
            return false;
        }
    });
}
function updateSection(section) {
    var title = $.trim($('#sectionname_' + section).val());
    if (title) {
        $.post(HTTP_ROOT + "/template/Updatesection", {'section': section, 'sectionname': title, 'apphome' : apphome}, function (data) {
            if (data.err === 1) {
                swal("Error in updating featured section.");
                return false;
            } else {
                window.location.href = HTTP_ROOT+"/"+control+"/homepage";
            }
        });
    } else {
        swal("Section Name can't be blank.");
        return false;
    }
}
function addHomepageSection()
{
    $('#addHomepageSection').modal('show');
}
function openEditSectionpopup(section) {
    $('#addHomepageSection').modal('show');
    $('#frm_loader').show();
    $.ajax({
        type: "POST",
        url: HTTP_ROOT + 'template/getfeaturedsection',
        data: {'section': section},
        dataType: 'json'
    }).done(function (data) {
        $('#section_id').val(section);
        $('#section_name').val(data.title);
        $('#frm_loader').hide();
    });

    $("#PagePopup").modal('show');
    return false;
}
function addPopularMovie(section, content_type)
{
    $('#addPopularcontnet').modal('show');
    $('#featured_section').val(section);
    $('#fs_content_type').val(content_type);
    $('#pcontent_poster_loader').hide();
    $('#preview_poster').hide();
    $("#title").val("");
    $('#title').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: HTTP_ROOT + '/template/autocomplete/movie_name?term=' + $('#title').val() + '&section=' + section + '&content_type=' + content_type + '&apphome='+apphome,
                dataType: "json",
                data: {"featured_section": section},
                beforeSend: function( xhr ) {
                    $('#fcontent_save').prop('disabled', true);
                    $('#pcontent_poster_loader').show(); 
                },
                success: function (data) {
                    if (data.length == 0) {
                        $('#movie_id').val('');
                        $('#preview_poster').html('');
                        $('#pcontent_poster_loader').hide();
                    } else {
                        response($.map(data.movies, function (item) {
                            return {
                                label: item.movie_name,
                                value: item.movie_id,
                                id: item.is_episode
                            }
                        }));
                    $('#pcontent_poster_loader').hide();
                    }
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $("#title").val(ui.item.label);
            $.ajax({
                url: HTTP_ROOT + "/admin/getPoster",
                data: {'movie_id': ui.item.value, 'title': ui.item.label, 'is_episode': ui.item.id},
                beforeSend: function( xhr ) {
                    $('#fcontent_save').prop('disabled', true);
                    $('#pcontent_poster_loader').show(); 
                },
                success: function (res) {
                    $('#preview_poster').show();
                    if (res.toLowerCase().indexOf("no-image.png") >= 0) {
                        $('#preview_poster').html("<img class='img-responsive' src='" + res + "' />")
                    } else {
                        $('#preview_poster').html("<img class='img-responsive' src='" + res + "' /><br/><h6>If the above poster is correct, please click 'Save' to add this content to your popular Movie</h6>")
                    }
                    $('#pcontent_poster_loader').hide();
                    $('#fcontent_save').prop('disabled', false);
                }
            });            
        },
        focus: function (event, ui) {
            var lb = ui.item.label;
            $("#title").val(ui.item.label);
            $("#movie_id").val(ui.item.value);
            $("#is_episode").val(ui.item.id);
            event.preventDefault(); // Prevent the default focus behavior.
        }        
    });
}
function checkvideofile() {
    var stream_id = $('#section_id1').val();
   var movie_name = $('#videofile').val();
    var movie_st_id = $('#section_id1').val();
    var section_id = $('#section_id1').val();
    var filename = $('#videofile').val().split('\\').pop();
    var extension = filename.replace(/^.*\./, '');
    if (extension == filename) {
        extension = '';
    } else {
        extension = extension.toLowerCase();
    }
    switch (extension) {
        case 'mp4':
            break;
        case 'mov':
            break;
        case 'mkv':
            break;
        case 'flv':
            break;
        case 'vob':
            break;
        case 'm4v':
            break;
        case 'avi':
            break;
        case '3gp':
            break;
        case 'mpg':
            break;
        case 'wmv':
            break;
        default:
            // Cancel the form submission
            swal('Sorry! This video format is not supported. \n MP4 format video are allowed to upload');
            return false;
            break;
    }
    swal({
        title: "Upload Video File",
        text: "upload " + filename + " file",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#10CFBD",
        customClass: "cancelButtonColor",
        confirmButtonText: "Upload",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true,
        html: true
    },
    function(isConfirm) {
        if (isConfirm) {
            $("#addvideo_popup").modal('hide');
            if (!$('#dprogress_bar').is(":visible")) {
                $('#dprogress_bar').show();
            }
            var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload video</a></h5>";
            $('#' + stream_id).html(rtext);
            upload('user','pass',stream_id,movie_name,movie_st_id, section_id);
            return true;
        } else {
            return false;
        }
    });

}

// Multipart Upload Code
var s3upload = null;
var s3obj = new Array();
var size = '';
var sizeleft = 0;
var sizeName = '';
function upload(user, pass, stream_id, movie_name, movie_st_id, section_id) {
    //var xhr = new XMLHttpRequest({mozSystem: true});
    var filename = $('#videofile').val().split('\\').pop();
    if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
        alert("Sorry! You are using an older or unsupported browser. Please update your browser");
        return;
    }

    /*if (s3upload != null) {
     alert("Though it is possible to upload multiple files at once, this demonstration does not allowed to do so to demonstrate pause and resume in a simple manner. Sorry :-(");
     return;
     }*/

    var file = $('#videofile')[0].files[0];
	size = Math.round(file.size/1000);
	sizeName = 'Kb';
	if(size > 1000){
		size = Math.round(size/1000);
		var sizeName = 'MB';
	}
	if(size > 1000){
		size = size/1000;
		size = size.toFixed(2);
		var sizeName = 'GB';
	}
    s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'movie_id': movie_st_id, 'uploadType': 'videobanner', 'section_id':section_id,'movie_name' : movie_name}, 'template','banner_video');
    s3upload.onServerError = function(command, jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 403) {
            alert("Sorry you are not allowed to upload");
        } else {
            console.log("Our server is not responding correctly");
        }
    };

    s3upload.onS3UploadError = function(xhr) {
        s3upload.waitRetry();
        console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
    };

    //s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
    s3upload.onProgressChanged = function(progPercent) {
        if (progPercent == 100) {
            $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
			$('#upload_'+stream_id+' .uploadFileSizeProgress').html(size);
        } else {
            var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
            $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

            $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
                
			if(sizeName == 'GB'){
				sizeleft = (progper * size)/100;
				sizeleft = sizeleft.toFixed(2);
			}else{
				sizeleft = Math.round((progper * size)/100);
			}
			$('#upload_'+stream_id+' .uploadFileSizeProgress').html(sizeleft);
        }
    };

    s3upload.onUploadCompleted = function(data) {
        window.location.href = window.location.href;
    };
    s3upload.onUploadCancel = function() {
        $('#upload_' + stream_id).remove();
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }

        $('#trailer_btn').removeAttr('disabled');
        console.log("Upload Cancelled..");
    };

//Start s3 Multipart upload	
    s3upload.start();
    s3obj[stream_id] = s3upload;
    var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">'+sizeleft+'</span>/'+size+' '+sizeName+')</h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
    $('#all_progress_bar').append(progressbar);
    $('#cancel_' + stream_id).on('click', function() {
        aid = this.id;
        sid = aid.split('_');
        swal({
                title: "Cancel Upload?",
                text: "Are you sure you want to cancel this upload?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true
            }, function () {
                s3obj[sid[1]].cancel();
                document.getElementById("videofile").value = "";
                window.location.href = HTTP_ROOT+"/template/homepage";
            });
    });
}
$(document).ready(function () {
    $('.hidesection').click(function () {
        $(this).addClass('showsection');
        $(this).removeClass('hidesection');
    });
    $('.icon.left-icon').click(function () {
        if ($(this).hasClass('icon-arrow-up')) {
            $(this).removeClass('icon-arrow-up');
            $(this).addClass('icon-arrow-down');
        } else {
            $(this).removeClass('icon-arrow-down');
            $(this).addClass('icon-arrow-up');
        }
    });
    $("#uplad_buton").click(function(){
       $(this).attr('disabled','disabled');
       $('#cancl_upload').attr('disabled','disabled');
       $("#upload_image_form").submit();
    });
    $('#fcontent_save').click(function(){
        var section_id = $("#featured_section").val();
        $.ajax({
            url: HTTP_ROOT + "/template/AddPopularMovie",
            type:'POST',
            dataType:'json',
            data: $("#add_content").serialize()+"&apphome="+apphome,
            beforeSend: function() {
                $('#fcontent_save').prop('disabled', true);
                if($('#movie_id').val() != ''){
                    $('#pcontent_poster_loader').show(); 
                }
            },
            success: function (res) {
                $('#pcontent_poster_loader').hide();
                $('#fcontent_save').removeAttr('disabled');
                $("#addPopularcontnet").modal('hide');
                $("#addPopularcontnet").find('#title').val("");
                $("#preview_poster").html("");
                $('#movie_id').val('');
                $('#is_episode').val('');
                if($.trim(res.status) == 'success'){
                    $("#sortables_"+section_id).html(res.data);
                }
                var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;'+res.message+'</div>'
                $('.alert.flash-msg').remove();
                $('.pace').prepend(sucmsg);
                $('html, body').animate({scrollTop : 0},800);
                return false; 
            }
        });       
    });
    $('.img-thumbnail').click(function(){
        var src = $(this).attr('src');
        var x = $(".item img[src$='"+src+"']");
        $('.item').removeClass('active');
        $(x).parent().addClass('active');
    });
    $("#banner_sortable" ).sortable({
        revert: 100,
        placeholder: 'placeholder',
        zIndex: 9999,
        update: function () {
            var order         = $('#banner_sortable').sortable('serialize');
            $.ajax({   
               type: "POST",
               url: HTTP_ROOT+"/template/BannerReorder",
               data: "order="+order+"&apphome="+apphome,
               dataType: "json",
               cache: false,
               success: function (data){
                    console.log(data);
                }
            });
        }
    });
    $(".home_layout").change(function(){
        var home_layout = $(this).val();
        $.ajax({
            type: "POST",
            url: HTTP_ROOT+"/apps/homepagesection",
            data: {home_layout : home_layout},
            success: function (data)
            {
                console.log(data);
            }
        });
        if(home_layout == 1){
            $("#featured_block").fadeOut();
        }else{
            $("#featured_block").fadeIn();
        }
    });
    $('#sort_section').sortable({
        update: function () {
            var order = $('#sort_section').sortable('serialize');
            $.ajax({
                type: "POST",
                url: HTTP_ROOT+"/template/sortfeaturedsection",
                data: "order="+order+"&apphome="+apphome,
                dataType: "json",
                cache: false,
                success: function (data)
                {
                    console.log(data);
                }
            });
        }
    });
    $(".feat_blocks").click(function(){
        var section    = $(this).attr('id');
        var split_id   = section.split("-");
        var section_id = split_id[1];
        $('#section_data_'+section_id).toggle();
    });
    $('.showsectionname').click(function () {
        $(this).parent().find('.editsection').removeClass('hide');
        $(this).parent().find('.editsection').addClass('show');
        $(this).addClass('hide');
    });
    
    $('ul.featured_contents').each(function(){
        var currentParent = this;
        $(currentParent).sortable({
            helper: 'clone',
            update: function () {
                var order = $(currentParent).sortable('serialize');
                $.ajax({
                    type: "POST",
                    url: HTTP_ROOT+"/template/sortfeatured",
                    data: "order=" + order +"&apphome="+apphome,
                    dataType: "json",
                    cache: false,
                    success: function (data)
                    {
                    }
                });
            }
        });
        //$(this).disableSelection();
    });
});