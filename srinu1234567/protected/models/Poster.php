<?php
class Poster extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'posters';
    }
    public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	/**
	 * @method public GetPosterByids() It will return the available posters from the ids
	 * @param type $object_ids Comma separated ids
	 * @param type $object_types Object type i.e films, moviestreams, user, topbanner etc
	 * @param type $studio_id 
	 * @param type $size poster size(thumb, original, standard)
	 * @param type $mapped_id Its for child store
	 * @return array poster array with cdn url
	 * @author Gayadhar<support@muvi.com>
	 */
	function getPosterByids($object_ids, $object_types='films',$studio_id,$size='original',$mapped_id=''){
		if($object_ids){
			$objectIdsarr = explode(',', $object_ids);
			$posterData = Yii::app()->db->createCommand()
					->SELECT('id,poster_file_name,object_id AS movie_id')
					->FROM('posters')
					->WHERE('object_type=:object_type',array(':object_type'=>$object_types))
					->AndWhere(array('IN','object_id',$objectIdsarr))
					->queryAll();
			//$psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $object_ids . ") AND object_type='".$object_types."' ";
            //$posterData = Yii::app()->db->createCommand($psql)->queryAll();
			if ($posterData) {
				$studio_id = $studio_id?$studio_id:Yii::app()->common->getStudiosId();
				if($mapped_id){
					$StoreMapping = StoreMapping::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
					if(!empty($StoreMapping)){
						$url = Yii::app()->common->getPosterCloudFrontPath($StoreMapping['parent_studio_id']);
					}else{
						 $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
					}
				}else{    
                    $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
				}
				foreach ($posterData AS $key => $val) {
					$val['poster_url'] = $url . '/system/posters/' . $val['id']. '/' . $size . '/' . urlencode($val['poster_file_name']);
					$posters[$val['movie_id']] = $val;
				}
				return $posters;
			}else{
				return '';
			}
		}else{
			return '';
		}
	}
	/**
	 * @method public GetDefaultposter() It will return the default poster with respect to template both vertical & horizontal 
	 * @param type $templateName Name of the template
	 * @return array array of default poster
	 * @author Gayadhar<support@muvi.com>
	 */
	function getDefaultposter($templateName,$studio_id){
		$arr['horizontal_poster'] = $get_horizontaltposter = Yii::app()->general->getdefaulthorizontalposter($studio_id);
        $arr['vertical_poster'] = $get_verticaltposter = Yii::app()->general->getdefaultverticalposter($studio_id);
		$local_poster_path = Yii::app()->getBaseUrl(true) . '/img';
		if(!$get_verticaltposter){
			if ($templateName == 'traditional-byod') {
				$arr['vertical_poster'] = $local_poster_path . '/No-Image-Default-Traditional.jpg';
			}else if ($templateName == 'audio-streaming') {
				$arr['vertical_poster'] = $local_poster_path . '/No-Image-Default-Audio.jpg';
			} else {
				$arr['vertical_poster'] = $local_poster_path . '/No-Image-Vertical.png';
			}
		}
		if(!$get_horizontaltposter){
			if ($templateName == 'traditional-byod') {
				$arr['horizontal_poster'] = $local_poster_path . '/No-Image-Default-Traditional.jpg';
			}else if ($templateName == 'audio-streaming') {
				$arr['horizontal_poster'] = $local_poster_path . '/No-Image-Default-Audio.jpg';
			} else {
				$arr['horizontal_poster'] = $local_poster_path . '/No-Image-Horizontal.png';
			}
		}
		return $arr;
	}
}

