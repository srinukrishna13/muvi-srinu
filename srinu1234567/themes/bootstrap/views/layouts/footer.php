    <div class="footer-above wrapper">
        <div class="container">
            <div class="span6">
                <div class="pull-left">
                <ul>
                    <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/about">About Us</a></li></div>
                    <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/team">Team</a></li></div>
                    
                </ul>
                </div>
                <div class="pull-left" style="padding-left: 10%;">
                    <ul>
                        <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/blogs">Blogs</a></li></div>
                        <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/blogs/category/news">News</a></li></div>
                    </ul>
                </div>
                <div class="pull-left" style="padding-left: 10%;">
                    <ul>
                        <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/faqs">FAQs</a></li></div>
                        <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/howitworks">How it works</a></li></div>
                        <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/howisitdiff">How is it different</a></li></div>
                    </ul>
                </div>
                <div class="pull-left" style="padding-left: 10%;">
                    <ul>
                        <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/contact">Contact Us</a></li></div>
                    </ul>
                </div>
            </div>
            <div class="span6">
                <ul class="pull-right social">
                    <li><a href="https://www.facebook.com/MuviStudioB2B" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/facebook.png" alt="facebook" title="Facebook" /></a></li>
                    <li><a href="http://www.twitter.com/muvistudiob2b" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/twitter.png" alt="twitter" title="Twitter" /></a></li>
                    <li><a href="https://www.linkedin.com/company/muvi-studio" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linkedin.png" alt="linkedin" title="LinkedIn" /></a></li>
                    <li><a href="http://plus.google.com/u/0/118356511554070797931?prsrc=3" rel="publisher" target="_top"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/google-plus.png" alt="google plus" title="Google Plus" /></a></li>
                </ul>
            </div>            
        </div>
    </div>
    
    <footer>
        <div class="container">&copy; Muvi LLC</div>
    </footer>
    <div class="modal fade hide" id="LoginModal" style="width:330px; height: 360px;">
        <div class="modal-header">
            <div id="myModalLabel" style="position: relative;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>LOG IN</h3>             
            </div>        
        </div>
        <div class="modal-body">
            <iframe style="border: none;" height="270" allowfullscreen="" src="<?php //echo Yii::app()->getBaseUrl(true);?>/login/"></iframe>
        </div>
    </div>
<div class="modal fade hide" id="ForgotModal">
    <div class="modal-header">
        <div id="myModalLabel" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>FORGOT PASSWORD</h3>              
        </div>  
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">     
                <div class="form-group">
                    <div id="forgot_loading1" class="hide"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif"></div>
                </div>
                <div class="form-group"><label>Email :</label>
                    <input name="email" id="forgot_email" type="email" class="form-control" style="width: 95%;">
                </div>
                <div class="form-group">
                    <div class="error hide">Email can not be blank!</div>
                    <div class="clear" style="height:20px;"></div>
                </div>
                <div class="form-group">
                    <button class="btn btn-blue" onclick="send_instruction();">Send reset password instruction</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	
    function send_instruction(){
        var email = $("#forgot_email").val();
        if(email != ""){
            $.ajax({
                url: "<?php echo Yii::app()->getbaseUrl(true); ?>/login/forgot",
                data: {email:email},
                type:'POST',
                dataType: "json",
                beforeSend:function(){
                    //$('#forgot_loading').show();
                    //$('#forgot_content').hide();
                },
                success: function (data) {  
                    if(data.status == 'success')
                    {
                        parent.$('#ForgotModal').modal('hide');
                        bootbox.alert("Forgot Password instructions are sent to your email.", function(){
                            //window.location = data.message;
                        });  
                    }
                    else
                    {
                        $('.error').html(data.message);
                        $('.error').show();
                        return false;
                    }
                }
            });
        }else{
            $(".error").show();
        }
    }
</script>