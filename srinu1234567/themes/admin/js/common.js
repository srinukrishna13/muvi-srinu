$(function () {
    $(".deletenote").click(function () {
        var id_note = this.id;
        var url = $('#BASEURL').val();
        bootbox.dialog({
            message: "Are you sure to <b>delete this Update</b>?",
            title: "Delete Update",
            buttons: {
                main: {
                    label: "Yes",
                    className: "",
                    callback: function () {
                        $(".deletenote").after('<button class="preloader preloader-small pls-blue btn btn-default"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"></circle></svg></button>');
                        $.post(url + '/ticket/deletenote', {'id': id_note}, function (res) {
                            if (res == 1) {
                                $(".view_note" + id_note).hide();
                                $(".tkt_note" + id_note).hide();
                                $('.success_flash_msg').css('display', 'block');
                                $('.success_msg').html("Update deleted successfully");
                               window.location.reload();
                            }
                        });
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-primary",
                    callback: function () {
                        //do something
                    }
                }
            }
        });
    });
});
function checkSPecialChar(id) {
    var str = $('#' + id).val();
    var regex = /[-!$%^&*()_+|~=`\\#{}\[\];<>?\/]/;
    if (regex.test(str) == true) {
        $("#" + id).focus();
        $("#" + id).addClass('error_txt');
        $("#" + id).next(".error red").html('Special characters not allowed.');
        return false;
    } else
    {
        $("#" + id).removeClass('error_txt');
        $("#" + id).next(".error").html('');
    }
}
function checkDescription(e) {
    var text = $('.checkSpace').val();
    var inValid = /^\s+$/;
    if (inValid.test(text)) {
        e.preventDefault();
        $(".error").html("This field is required");
        return false;
    }
    
    else {
        e.stopPropagation();
        return true;
    }
}
function updatenote(url, id_note) {
    var note = $("#txt" + id_note).val();
    //var text_note=tinyMCE.get('txt_note_edit').getContent();
    var text_note=$("#txt_note_edit").val();
    //alert(text_note);
    if(text_note==''){
            $("#ajax_load_"+id_note).hide();
            $('#myModal' + id_note).modal('hide');
             $('.success_flash_msg').addClass("alert-danger");
            $('.success_flash_msg').css('display', 'block');
            $('.success_msg').html("Please Add Content");
            window.location.reload();  
    }else{
    var inValid = /^\s+$/;
    if (!inValid.test(note)) {
        $('.updatenote').after('<button class="preloader preloader-small pls-blue btn btn-default"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"></circle></svg></button>');
        var id_ticket = $("#id_ticket").val();
    var formData = new FormData($("#edit_note")[0]);
   //  console.log(tinyMCE.get('txt_note_edit').getContent());
    //formData.append('editor_val',tinyMCE.get('txt_note_edit').getContent());
     formData.append('editor_val',$("#txt_note_edit").val()); 
    $.ajax({
    url: url + "/ticket/EditNote",
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (res) {
     // alert(res);
          $("#ajax_load_"+id_note).hide();
            $('#myModal' + id_note).modal('hide');
            $(".view_note" + id_note).html("<div class='col-12'>" + res + "</div>");
           
            $('.success_flash_msg').css('display', 'block');
            $('.success_msg').html("Updated successfully");
           window.location.reload();
        }
        })
    } else {
        alert('No updates Added');
    }
}
}

function init_tinymce() {
    tinymce.init({
        selector: "#textarea_content",
        menubar: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "undo redo | styleselect  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
        setup: function (editor) {
            editor.on('init', function () {
                if (tinymce.editors.length) {
                    // $("#content_ifr").removeAttr('title');
                }
            });
        }
    });
}
function viewticket(id_ticket) {
    var url = "viewTicket";
    $.post(url, {'id': id_ticket}, function (res) {
        alert(res);
    })
}
function editTicket(url, page, sortby, id_ticket, search) {
    var url = url + "/ticket/UpdateTicket";
    if (search != '')
        url += '/search/' + encodeURI(search);
    if (sortby != '')
        url += '/sortBy/' + sortby;
    if (page != '')
        url += '/page/' + page;
    $.post(url, {'id': id_ticket}, function (res) {
        window.location.href = url + '/id/' + id_ticket;
    })
}
function deleteTicket(url, id_ticket, page, sortby, search) {
    var url = url;
     swal({
    title: "Close Ticket?",
    text: "Are you sure to <b>close this ticket</b>?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    html:true,
  },
  function(){
    $('.delete-ticket').after('<button class="preloader preloader-small pls-blue btn btn-default"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"></circle></svg></button>');
    window.location.href = $('#BASEURL').val() + "/ticket/DeleteTicket/" + id_ticket;
  });
   
}
function reply(id_ticket, reporter) {
    var url = "replyClient";
    $.post(url, {'id': id_ticket, 'reporter': reporter, 'sendmail': 0}, function (res) {
        var res = jQuery.parseJSON(res);
        $("#email_to").val(res.email);
        $("#subject").val(res.subject);
        $("#textarea_content").val(res.desc);
        $(".dialog-email").dialog({
            resizable: false,
            height: 650,
            width: 700,
            modal: true,
            open: init_tinymce,
            buttons: {
                "Send": function () {
                    var content = tinymce.get('textarea_content').getContent();
                    $.post(url, {'to': res.email, 'subject': res.subject, 'content': content, 'sendmail': 1}, function (res) {
                        $(".notification").html("Reply has been sent successfully");
                        $(".notification").css("display", "block");
                        setTimeout(function () {
                            $(".notification").css("display", "none");
                            $(".notification").html("");
                        }, 5000);
                    });
                    $(this).dialog("close");
                }
            }
        });
        //alert(res);return false;
        //window.location.href='ticket/editTicket?id='+id_ticket;
    })
}
function addmore(id) {
   
    var upload_number = $(".upload").length;
    upload_number = ++id;
    var moreUploadTag = '';
    var upload_id_for_button ="upload_file" + upload_number ;
    var upload_id_for_button1 = "click_browse('"+upload_id_for_button+"')";
    moreUploadTag += '<button type="button" id="upload_file_button' + upload_number + '" class="btn btn-default-with-bg" onclick="'+upload_id_for_button1+'" >Browse</button><input type="file" class="upload" id="upload_file' + upload_number + '" name="upload_file' + upload_number + '" onchange="preview1(this,' + "'" + upload_number + "'" + ')"; style="display:none;"/><div id="preview' + upload_number + '" class="m-b-10 fixedWidth--Preview relative"></div>';
    //alert(moreUploadTag);
    $('<div  id="delete_file' + upload_number + '">' + moreUploadTag + '</div>').fadeIn('slow').appendTo('#moreImageUpload');
    upload_number++;
}
function editaddmorefile(id){
   var upload_number = $(".upload").length;
   upload_number=++id;
    var moreUploadTag = '';
    var upload_id_for_button ="upload_file_edit" + upload_number ;
    var upload_id_for_button1 = "click_browse_edit('"+upload_id_for_button+"')";
    moreUploadTag += '<label for="upload_file_edit'+upload_number+'"></label>';
    moreUploadTag += '<button type="button" id="edit_upload_file_button' + upload_number + '" class="btn btn-default-with-bg" onclick="'+upload_id_for_button1+'" >Browse</button><input type="file" class="upload" id="upload_file_edit' + upload_number + '" name="upload_file_edit' + upload_number + '" onchange="editpreview1(this,' + "'" + upload_number + "'" + ')"; style="display:none;"/><div id="editpreview'+upload_number+'" class="m-b-10 fixedWidth--Preview relative"></div>';
    $('<dl id="editdelete_file' + upload_number + '">' + moreUploadTag + '</dl>').fadeIn('slow').appendTo('#editmoreImageUpload');
    upload_number++; 
}
function removefile(obj, edit,flag) {
  
    var edit = edit ? edit : 0;
    swal({
    title: "Remove Image?",
    text: "Are you sure want to remove this image ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
  },
  function(){
    function removefile(obj, edit,flag) {
    
    if (typeof(flag)==='undefined') flag = 1;   
    if(flag==1)
    {
        var preview="preview";
        var upload_file="upload_file";   
       
    }else
    {
        var preview="editpreview";
        var upload_file="upload_file_edit";
        
    }

        var filename = $(obj).prop("id");
        var id = $(obj).prop("name");
        var tbl = 'home';
        $.post(url, {"filename": filename, "edit": edit, "tbl": tbl}, function (res) {
            $("#"+preview + id).html(" ");
            $("#"+upload_file + id).val("");
        });

}
  });
}
function del_file(eleId,flag) {

    swal({
    title: "Remove File?",
    text: "Are you sure want to remove this File ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
  },
  function(){
    if (typeof(flag)==='undefined') flag = 1;   
    if(flag==1)
    {
        var preview="preview";
        var upload_file="upload_file";   
        var delete_file="delete_file";
    }else
    {
        var preview="editpreview";
        var upload_file="upload_file_edit";
        var delete_file="editdelete_file";
    }
    
    $('#'+delete_file + eleId).remove();
     $('#'+preview + eleId).remove();
    if(eleId==1)
    {
     $('#'+upload_file + eleId).remove();   
    }
  });

}
function preview1(obj, id) {
    //alert($(".upload").length);
    
    var next_id = id + 1;
    $("#upload_file_button" + id).hide();
    $("#upload_file" + id).hide();
    $("#preview" + id).html("");

    $("#span_id").val(id);
    var fileName = $("#upload_file" + id).val();
   if(fileName.match(/['|"|-|,]/)){
                    $("#preview" + id).html("<font color='red'><br /> File names with symbols such as ' , - are not supported</font>");  
                   $("#upload_file" + id).val('');
                   $("#upload_file" + id).show(); 
                    return false;
                }else{
            
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    if (ext == 'bmp' || ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png" || ext == "txt" || ext == "xls" || ext == "xlsx" || ext == "doc" || ext == "docx" || ext == "pdf") {
        if (obj.files && obj.files[0]) {
            var reader = new FileReader();
            var deleteOption = '<div class="overlay-bottom text-right"><div class="overlay-Text"><div><a class="btn btn-danger icon-with-fixed-width" data-toggle="tooltip"  data-placement="bottom"  title="Delete" href="javascript:del_file(' + id + ')"><em class="icon-trash c-white"></em></a></div></div></div>';
            var deleteOption1 = '&nbsp;<a href="javascript:del_file(' + id + ')" data-toggle="tooltip"   data-placement="bottom" title="Delete" style="cursor:pointer;">&nbsp<i class="fa fa-times"></i></a></div>';
                //var deleteOption = '&nbsp;<a href="javascript:del_file(' + id + ')" style="cursor:pointer;" onclick="return confirm(\"Are you really want to delete ?\")">Remove</a></div>';
            reader.onload = function (e) {
                var file = e.target.result;
                if (file.substr(0, 10) == 'data:image')
                {
                    $("#preview" + id).html('</br><img id="img_preview' + id + '"  data-original="' + e.target.result + '"  src="' + e.target.result + '"/>' + deleteOption)
                    $("#img_preview" + id).load(function(){
                        aspectratio(id);
                    });
       
                }
                else
                {
                 $("#preview" + id).html('<span>'+fileName+'</span>'+deleteOption1);  
                }
            };
            reader.readAsDataURL(obj.files[0]);   // Read in the image file as a data URL.            }           
        }
       if ($(".upload").length >=0)
            addmore(id);

    } else {
        $("#preview" + id).html("<font color='red'><br />Sorry the file extension <b>" + ext + "</b>  is not allowed, please upload jpg, gif, jpeg, png, txt, doc, docx, pdf, xls, xlsx files only</font>");
        $("#upload_file" + id).val('');
        $("#upload_file" + id).show();
        return false;
    }
 }
}
function editpreview1(obj, id) {
    
    var next_id = id + 1;
   $("#edit_upload_file_button" + id).hide();
   $("#upload_file_edit" + id).hide();
    $("#editpreview" + id).html("");
    $("#span_id").val(id);
    var fileName = $("#upload_file_edit" + id).val();
    
    if(fileName.match(/['|"|-|,]/)){
                    $("#editpreview" + id).html("<font color='red'><br /> File names with symbols such as ' , - are not supported</font>");  
                   $("#upload_file_edit" + id).val('');
                   $("#upload_file_edit" + id).show(); 
                    return false;
                }else{
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    if (ext == 'bmp' || ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png" || ext == "txt" || ext == "xls" || ext == "xlsx" || ext == "doc" || ext == "docx" || ext == "pdf") {
        if (obj.files && obj.files[0]) {
            var reader = new FileReader();
            var deleteOption = '&nbsp;<a href="javascript:del_file(' + id + ',2)" style="cursor:pointer;">Remove</a></div>';
            reader.onload = function (e) {
                var file = e.target.result;
                if (file.substr(0, 10) == 'data:image')
                {
                    $("#editpreview" + id).html('');
                    $("#editpreview" + id).html('<br /><img id="editimg_preview' + id + '"  data-original="' + e.target.result + '"  src="' + e.target.result + '"/>' + deleteOption);
                    $("#editimg_preview" + id).load(function(){
                        aspectratio(id,2);
                    });
       
                }
                else
                {
                    $("#editpreview" + id).html('');
                    $("#editpreview" + id).html('<span>'+fileName+'</span>'+deleteOption);
                   
                }
            };
            reader.readAsDataURL(obj.files[0]);   // Read in the image file as a data URL.            }           
        }
        if ($(".upload").length >=0)
            editaddmorefile(id);

    } else {
        $("#editpreview" + id).html("<font color='red'><br />Sorry the file extension <b>" + ext + "</b>  is not allowed, please upload jpg, gif, jpeg, png, txt, doc, docx, pdf, xls, xlsx files only</font>");
        $("#upload_file_edit" + id).val('');
         $("#upload_file_edit" + id).show();
        return false;
    }
 }
}


function aspectratio(id,flag) {
   if (typeof(flag)==='undefined') flag = 1;   
    if(flag==1)
    {
        var preview="preview";
        var img_preview="img_preview";       
    }else
    {
        var preview="editpreview";
        var img_preview="editimg_preview";
    }
    $("#"+preview + id).each(function () {
        var maxWidth = 200;
        var maxHeight = 200;

        var width = $("#"+img_preview+ id).width();
        var height = $("#"+img_preview + id).height();
        var ratioW = maxWidth / width;  // Width ratio
        var ratioH = maxHeight / height;  // Height ratio

        // If height ratio is bigger then we need to scale height
        if (ratioH > ratioW) {

            //  $("#preview" + id).css("width", maxWidth);
            //  $("#preview" + id).css("height", height * ratioW);
            $("#"+img_preview + id).css("width","100%");
          //  $("#"+img_preview + id).css("height", height * ratioW);// Scale height according to width ratio
        }
        else if (ratioH == ratioW)
        {
            $("#"+img_preview + id).css("width", "100%");
         //   $("#"+img_preview + id).css("height", maxHeight);// Scale height according to width ratio

        }
        else { // otherwise we scale width

            // $("#preview" + id).css("height", maxHeight);
            // $("#preview" + id).css("width", height * ratioH); 
            $("#"+img_preview + id).css("width", "100%");
            //$("#"+img_preview + id).css("height", height * ratioW);// according to height ratio
        }
    });
}
function deleteFile(studio, id, key) {
    var url = $('#BASEURL').val() + '/ticket/deleteImage';
    swal({
    title: "Delete Attachment?",
    text: "Are you sure to <b>delete this file</b>?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    html:true,
  },
  function(){
       $('.remove' + key).after('<button class="preloader preloader-small pls-blue btn btn-default"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"></circle></svg></button>');
                    var imgName = $('#atag' + key).attr('data-value');
                    $.post(url, {'id_ticket': id, 'studio_id': studio, 'image': imgName, 'is_ajax': 1}, function (res) {
                        var result = jQuery.parseJSON(res);
                        if (result.deleted == 1) {
                            $('#prevfiles').val(res.attachment)
                            $('.remove' + key).remove();
                            $('#atag' + key).remove();
                            $('.ajax-loader').hide();
                            $('.preloader').hide();
                            //$('.success_flash_msg').css('display', 'block');
                            //$('.success_msg').html("File deleted successfully");
                            $("#upload_file" + id).show();
                            swal("File deleted successfully");
                           
                             
                        }
                    })
  });
    
   
}
function searchTicket(url, sortby, page) {
    var search = encodeURI($('#search').val());
    var url = $('#BASEURL').val() + "/ticket/TicketList";
    if (search != '')
        url += '/search/' + search;
    if (sortby != '')
        url += '/sortBy/' + sortby;
    if (page != '' && page != 1)
        url += '/page/' + page;
    if (search == '')
        url = $('#BASEURL').val() + "/ticket/TicketList";
    window.location.href = url;
}
 
function remove_image(id) {
   swal({
    title: "Remove Image?",
    text: "Are you sure want to remove this image ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
  },
  function(){
     del_file(id);
  });
}