<?php
class SearchLog extends CActiveRecord{
	public $search_count;
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'search_logs';
    }
    
    public function getSearchLog($start_date,$end_date,$offset,$page_size,$searchKey)
    {
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = ' AND (sl.search_string LIKE "%'.$searchKey.'%" OR sl.object_category LIKE "%'.$searchKey.'%")';
        }
        $sql = 'SELECT SQL_CALC_FOUND_ROWS sl.*, COUNT(sl.id) AS search_count, f.content_types_id, ms.is_episode FROM search_logs sl LEFT JOIN films f ON (sl.object_id = f.id) LEFT JOIN movie_streams ms ON (sl.object_id = ms.movie_id) WHERE (DATE_FORMAT(sl.created_date, "%Y-%m-%d") BETWEEN "'.$start_date.'" AND "'.$end_date.'") '.$searchStr.' AND sl.studio_id='.Yii::app()->common->getStudiosId().' AND search_string !=\'\' GROUP BY search_string,object_category ORDER BY sl.created_date DESC,search_string ASC LIMIT '.$offset.','.$page_size;
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    
    public function getSearchedLog($key)
    {
        $sql = 'SELECT SQL_CALC_FOUND_ROWS *, COUNT(id) AS search_count FROM '.$this->tableName().' WHERE studio_id='.Yii::app()->common->getStudiosId().' AND search_string LIKE "%'.$key.'%" GROUP BY search_string,object_category ORDER BY created_date DESC,search_string ASC';
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    public function getSearchLogReport($dt = '',$searchKey)
    {
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            //$dt = stripcslashes($dt);
            //$dt = json_decode($dt);
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = ' AND (search_logs.search_string LIKE "%'.$searchKey.'%" OR search_logs.object_category LIKE "%'.$searchKey.'%")';
        }
        $sql = 'SELECT SQL_CALC_FOUND_ROWS sl.*, COUNT(sl.id) AS search_count, f.content_types_id, ms.is_episode FROM search_logs sl LEFT JOIN films f ON (sl.object_id = f.id) LEFT JOIN movie_streams ms ON (sl.object_id = ms.movie_id) WHERE (DATE_FORMAT(sl.created_date, "%Y-%m-%d") BETWEEN "'.$start_date.'" AND "'.$end_date.'") '.$searchStr.' AND sl.studio_id='.Yii::app()->common->getStudiosId().' AND search_string !=\'\' GROUP BY search_string,object_category ORDER BY sl.created_date DESC,search_string ASC';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
}
