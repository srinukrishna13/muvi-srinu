<!-- PPV Plan Modal -->
<?php
if(isset($gateways[0]) && !empty($gateways[0])){
    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
    
    if (isset($plan) && !empty($plan)) {
        $amount = '';
        $symbol = (isset($currency->symbol) && trim($currency->symbol)) ? $currency->symbol: $currency->code.' ';

        if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
            $amount =  $timeframe_prices[0]['price_for_subscribed']; 
        } else {
            $amount =  $timeframe_prices[0]['price_for_unsubscribed']; 
        }
                
?>
<input type="hidden" value="<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>" id = "is_show" />
<input type="hidden" value="<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>" id = "is_season" />
<input type="hidden" value="<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>" id = "is_episode" />
<input type="hidden" value="<?php echo (isset($plan->content_types_id) && intval($plan->content_types_id)) ? $plan->content_types_id : 0; ?>" id = "content_types_id" />

<!-- For both single and multipart content when bundle is enabled-->
<div id="price_detail" style="position: relative;">
   <div class="col-md-12">
        <span class="error" id="plan_error"></span>
        <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
           <div style="font-weight: bold"><?php echo $this->Language['select_bundle']; ?></div>
           <div class="form-group">
              <?php 
                 $dflt = 1;
                 $is_timeframe = 0;
                 $bundle_price = 0;
                 foreach ($plan as $key => $value) {
                     if ($dflt == 1) {
                         if (intval($value->is_timeframe)) {
                             $is_timeframe = 1;
                         }
                         $bundle_price = $amount;
                     } ?>
              <div class="col-md-10">
                 <label style="font-weight: normal">
                 <input type="radio" name="data[bundleplan]" class="ppv-bundled-cls" <?php if($dflt ==1){ ?>checked="checked" <?php } ?> value="<?php echo $value->id; ?>" onclick="getBundlePrice(this);"/>
                 <?php echo (trim($value->title)) ? $value->title : 'Default'; ?>

                 <?php if (intval($value->is_advance_purchase) == 2) { ?>
                 <a href="javascript:void(0);" data-plan="<?php echo $value->id;?>" onclick="viewDetail(this);">View Detail</a>
                 <span id="ldr_<?php echo $value->id;?>" style="display:none;">
                     <img src="<?php echo Yii::app()->baseUrl; ?>/img/loading.gif" />
                 </span>
                 <div class="popover fade right in" id="popover_<?php echo $value->id;?>" style="top: -21.5px; left: 285px; width: 60%;display: none;"><!-- overflow-y: auto;-->
                     <div class="arrow" style="top: 28.4615%;"></div>
                     <h3 class="popover-title"><?php echo (trim($value->title)) ? $value->title : 'Default'; ?></h3>
                     <div class="popover-content" style="max-height:150px;"></div>
                 </div>
                 <?php } ?>
                 </label>
              </div>
              <div class="clearfix"></div>
              <?php 
                 $dflt++;
                 }?>
           </div>
           <div class="clearfix"></div>
           <div id="timeframe_div">
              <?php if (intval($is_timeframe) && isset($timeframe_prices) && !empty($timeframe_prices)) { ?>
              <div style="font-weight: bold"><?php echo $this->Language['select_timeFrame']; ?></div>
              <div class="form-group">
                 <?php 
                 $dflt_tmfrm = 1;
                 foreach ($timeframe_prices as $key => $value) {
                    if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                        $timeframeprice =  $value['price_for_subscribed']; 
                    } else {
                        $timeframeprice =  $value['price_for_unsubscribed']; 
                    }
                    ?>
                 <div class="col-md-3">
                    <label style="font-weight: normal">
                    <input type="radio" name="data[timeframeprice]" class="ppv-bundled-tmframe-cls" <?php if($dflt_tmfrm ==1){ ?>checked="checked" <?php } ?> value="<?php echo $value['id']; ?>" data-price="<?php echo $timeframeprice;?>" onclick="setTimeFramePrice(this);"/>
                    &nbsp;<?php echo $value['title']; ?>
                    </label>
                 </div>
                 <?php 
                 $dflt_tmfrm++;
                 } ?>
              </div>
              <div class="clearfix"></div>
              <?php } ?>
           </div>
           <div style="font-weight: bold">
               <?php echo $this->Language['price'].":"; ?> 
                 <span id="charged_amt" data-amount="<?php echo $bundle_price;?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php echo $bundle_price;?></span>
                 <span id="discount_charged_amt" style="display: none;">
                     <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$bundle_price;?></span>
                     <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                 </span>
           </div>
           <div class="form-group ">
               <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
                 <br/>
                 <div class="col-sm-6 pull-left">
                   <div class="input-group input-group-sm">
                      <input type="text" class="form-control" name="data[coupon_code]" id="coupon" placeholder="<?php echo $this->Language['coupon_code_optional']; ?>">
                      <input type="hidden" name="data[coupon_use]" value="0" id="coupon_use" />
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat" id="coupon_btn" onclick="validateCoupon();"><?php echo $this->Language['btn_apply']; ?></button>
                      </span>
                   </div>
                   <div id="invalid_coupon_error" style="color:red;font-size:11px;display: none"></div>
                   <div id="valid_coupon_suc" class="has-success" style="display: none;">
                      <label for="inputSuccess" class="control-label" style="font-weight: normal;color: #4da30c;"><?php echo $this->Language['discount_on_coupon']; ?> <span id="coupon_in_amt"></span></label>
                   </div>
                 </div>
                 <div class="col-md-6">
                     <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return showPaymentForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                 </div>
             <?php } else { ?>
                 <div class="col-md-12">
                     <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return showPaymentForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                 </div>
             <?php } ?>
              <div class="clear"></div>
           </div>
        </form>
     </div>
</div>
    
<?php }
    include('ppv_card_detail.php');
} ?>
<script type="text/javascript">
    var action = 'ppvpayment';
    var btn = '<?php echo $this->Language['btn_paynow']; ?>';
    var is_coupon_exists = 0;
    <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
            is_coupon_exists = 1;
    <?php }?>
    
    $(document).ready(function () {
        getCardDetail();
        isCouponExist();
        
        $('input').attr('autocomplete', 'off');
        $('form').attr('autocomplete', 'off');
    });
    
    $(document).click(function() {
        $(".popover").hide();
    });

    $(".popover").click(function(event) {
        event.stopPropagation();
    });
    
    function viewDetail(obj) {
        var plan = $(obj).attr("data-plan");
        
        if($("#popover_"+plan).is(':hidden')) {
            $("#ldr_"+plan).show();
            $(".popover").hide();
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/getBundledContents";
            $.post(url, {'plan': plan}, function (res) {
                $("#ldr_"+plan).hide();
                $("#popover_"+plan).show();
                $(".popover-content").html(res);
            });
        } else {
            $(".popover").hide();
        }
    }
    
    function setTimeFramePrice(obj) {
        var price = $(obj).attr("data-price");
        var cur = $("#charged_amt").attr('data-currency');
        $("#charged_amt").text(cur+price);
        $("#charged_amt").attr('data-amount', price);
        $("#dis_charged_amt").text(cur+price);
        
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }
    }
    
    function getBundlePrice(obj) {
        var plan_id = $(obj).val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getBundlePrice";
        
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }
        
        $.post(url, {'plan_id': plan_id}, function (res) {
            $('#loader-ppv').hide();
            $("#btn_proceed_payment").removeAttr("disabled");
            
            var str = '';
            
            if (parseInt(res.is_timeframe)) {
                str = '<div style="font-weight: bold">Select TimeFrame</div>';
                str+='<div class="form-group">';
                
                var tm_cnt = 1;
                var is_checked = '';
                
                for(var i in res.timeframeprice) {
                    is_checked = '';
                    if(tm_cnt === 1) {
                        is_checked = 'checked="checked"';
                    }
                    str+='<div class="col-md-3">';
                    str+='<label style="font-weight: normal">';
                    str+='<input type="radio" name="data[timeframeprice]" class="ppv-bundled-tmframe-cls" '+is_checked+' value="'+res.timeframeprice[i].id+'" data-price="'+res.timeframeprice[i].price+'" onclick="setTimeFramePrice(this);"/>&nbsp;'+res.timeframeprice[i].title;
                    str+='</label>';
                    str+='</div>';
                    
                    tm_cnt++;
                }
                
                str+='</div>';
                str+='<div class="clearfix"></div>';
            }
            var cur = $("#charged_amt").attr('data-currency');
            $("#timeframe_div").html(str);
            
            $("#charged_amt").html(cur+res.price);
            $("#charged_amt").attr('data-amount', res.price);
            $("#dis_charged_amt").text(cur+res.price);
        }, 'json');
    }
    
    function showPaymentForm() {
        var movie_id = $("#ppvmovie_id").val();
        var episode_id = $("#episode_id").val();
        var plan = $("input[type='radio'].ppv-bundled-cls:checked").val();
        var timeframe = 0;
        var use_coupon = $.trim($("#coupon_use").val());
        var coupon_code = '';
        var coupon_currency_id = 0;
        if(use_coupon === "1"){
            coupon_code =  $.trim($("#coupon").val());
            coupon_currency_id =  $("#currency_id").val();
        }
        var payment_method =  $("#payment_method").val();
        
        if ($(".ppv-bundled-tmframe-cls").length && $("input[type='radio'].ppv-bundled-tmframe-cls").is(":checked")) {
            timeframe = $("input[type='radio'].ppv-bundled-tmframe-cls:checked").val();
        }
        
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/isPPVSubscribed";
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        var good_type = 'digital_payment';
        $.post(url, {'movie_id': movie_id, 'episode_id': episode_id, 'plan': plan, 'timeframe': timeframe, 'is_ppv_bundle': 1, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method, 'good_type': good_type}, function (res) {
            $('#loader-ppv').hide();
            if (parseInt(res) === 1) {
                window.location.href = "<?php echo Yii::app()->baseUrl; ?>/player/<?php echo $films['permalink'];?>";
            } else {
                
                    if ($.trim(res.msg)) {
                        $("#btn_proceed_payment").removeAttr("disabled");
                        $("#plan_error").html(res.msg).show();
                    }else if(parseInt(res.is_pci_compliance) === 1 && !parseInt(res.is_hosted_ppv)){
                        $('#loader-ppv').show();
                        $("#plandetail_id").val(plan);
                        $("#timeframe_id").val(timeframe);
                        $("#bundle-hdr").text(res.bundle_title);
                        $("#is_bundle").val(1);
                        var currency = $("#charged_amt").attr('data-currency');
                        $("#coupon_code").val(res.coupon_code);
                        $("#price_amount").attr('data-amount', res.amount);
                        $("#price_amount").text(currency+res.amount);
                        var class_name = <?php echo $this->PAYMENT_GATEWAY[$gateway_code];?>+'()';
                        eval ("var obj = new "+class_name);
                        obj.processCard();
                    } else {
                        $("#ppvModalMain").css('width', '60%');
                        $("#price_detail").hide();
                        $("#card_detail").show();
                        
                        $("#plandetail_id").val(plan);
                        $("#timeframe_id").val(timeframe);
                        $("#bundle-hdr").text(res.bundle_title);
                        $("#is_bundle").val(1);
                        var currency = $("#charged_amt").attr('data-currency');
                        //$("#charged_amt").attr('data-amount', res.amount);
                        //$("#charged_amt").text(currency+res.amount);
                        //$("#dis_charged_amt").text(currency+res.amount);
                        
                        $("#coupon_code").val(res.coupon_code);
                        $("#price_amount").attr('data-amount', res.amount);
                        $("#price_amount").text(currency+res.amount);
                    }
            }
        }, 'json');
    }
</script>

<script type="text/javascript" src="<?php echo $this->siteurl;?>/common/js/action.js"></script>
<!-- As the processing the card in different payment gateway is different. So processCard javascript method is in following included file.-->

<?php 
$payment_gateway_str = '';
if(isset($this->PAYMENT_GATEWAY) && count($this->PAYMENT_GATEWAY) > 1){
    $payment_gateway_str = implode(',', $this->PAYMENT_GATEWAY);
    foreach ($this->PAYMENT_GATEWAY as $gateway_code){
        
    ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
    <?php }}else{
if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
    $payment_gateway_str = $this->PAYMENT_GATEWAY[$gateway_code];?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
<?php } }?>

<input type="hidden" id="payment_gateway_str" value="<?php echo $payment_gateway_str;?>">