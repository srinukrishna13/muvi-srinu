<?php

// used for parsing the html response of cURL, used in hash_call()
require_once 'simple_html_dom.php'; 

class ApihblController extends Controller {

    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }
    /**
     * 
     * Library functions starts
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */

    function sign ($params) {
      return self::signData(self::buildDataToSign($params), $this->PAYMENT_GATEWAY_API_SIGNATURE['hbl']);
    }

    function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return self::commaSeparate($dataToSign);
    }

    function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    }
    /**
     * Library functions ends
     */
    

    /**
     *
     * Initialize the api
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() {
        $hbl = array();
        $hbl['access_key'] = trim($this->PAYMENT_GATEWAY_API_USER['hbl']);
        $hbl['profile_id'] = trim($this->PAYMENT_GATEWAY_API_PASSWORD['hbl']);
        return $hbl;
    }
    
    function processCard($arg = array()) {
        $hbl = self::initializePaymentGateway();
        $address = self::userAddress();
        $data['access_key'] = $hbl['access_key'];
        $data['profile_id'] = $hbl['profile_id'];
        $data['transaction_uuid'] = uniqid();
        $data['signed_field_names'] = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,customer_ip_address,consumer_id";
        $data['unsigned_field_names'] = "card_type,card_number,card_expiry_date";
        $data['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
        $data['locale'] = "en";
        $data['transaction_type'] = "create_payment_token";
        $data['reference_number'] = $arg['reference_number'];
        $data['consumer_id'] = $arg['user_id'];
        $data['amount'] = 0; //creating token and authorize the card not a payment transaction
        $data['currency'] = $arg['currency_code'];
        $data['payment_method'] = "card";
        $name = explode(' ',$arg['card_name']);
        $data['bill_to_forename'] = $name[0];
        $data['bill_to_surname'] = $name[count($name)-1];
        $data['bill_to_email'] = $arg['email'];
        $data['bill_to_address_line1'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "1 Card Lane";
        $data['bill_to_address_city'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "My City";
        $data['bill_to_address_state'] = trim($address['geoplugin_region'])? $address['geoplugin_region']: "CA";
        $data['bill_to_address_country'] = trim($address['geoplugin_countryCode'])? $address['geoplugin_countryCode']: "US";
        $data['bill_to_address_postal_code'] = $address['geoplugin_areaCode']? $address['geoplugin_areaCode']: "94043";
        $data['customer_ip_address'] = CHttpRequest::getUserHostAddress();
        $data['submit'] = "Submit";
        foreach($data as $name => $value) {
            $params[$name] = $value;
        }
        $data['signature'] = self::sign($params);
        $data['card_type'] = '001';
        $data['card_number'] = $arg['card_number'];
        $data['card_expiry_date'] = $arg['exp_month'].'-'.$arg['exp_year'];
        //print "<pre>";print_r($data);exit;
        $resArray = self::hash_call($data);
        
        $logfile = dirname(__FILE__).'/hbl.txt';
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $msg .= "\n--------------------------Process Card()---------------------------------------\n";
        $data = serialize($resArray);
        $msg.= $data."\n";
        //Write into the log file
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        
        if(isset($resArray['reason_code']) && $resArray['reason_code'] == 100){
            $res['isSuccess'] = 1;
            $res['card']['code'] = 200;
            $res['card']['status'] = $resArray['decision'];
            $res['card']['profile_id'] = $resArray['payment_token'];
            $res['card']['card_last_fourdigit'] = $resArray['req_card_number'];
            $res['card']['token'] = $resArray['payment_token']; 
            $res['card']['response_text'] = serialize($resArray);
        }else{
            $res['isSuccess'] = 0;
            $res['code'] = $resArray['reason_code'];
            $res['response_text'] = serialize($resArray);
            $res['Message'] = 'Invalid Card details entered. Please check again';
        }
        //print "<pre>";print_r($res);exit;
        return json_encode($res);
    }
    
    function processTransactions($user) {
        $timeparts = explode(" ",microtime());
        $currenttime = bcadd(($timeparts[0]*1000),bcmul($timeparts[1],1000));
        $reference_number = explode('.', $currenttime);
        $hbl = self::initializePaymentGateway();
        $address = self::userAddress();
        $data['access_key'] = $hbl['access_key'];
        $data['profile_id'] = $hbl['profile_id'];
        $data['transaction_uuid'] = uniqid();
        $data['signed_field_names'] = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,payment_token,customer_ip_address,consumer_id";
        $data['unsigned_field_names'] = "card_type,card_number,card_expiry_date";
        $data['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
        $data['locale'] = "en";
        $data['transaction_type'] = "authorization";
        $data['reference_number'] = $reference_number[0];
        $data['consumer_id'] = $user['user_id'];
        $data['amount'] = $user['amount'];
        $data['currency'] = $user['currency_code'];
        $data['payment_method'] = "card";
        $name = explode(' ',$user['card_holder_name']);
        $data['bill_to_forename'] = $name[0];
        $data['bill_to_surname'] = $name[count($name)-1];
        $data['bill_to_email'] = $user['email'];
        $data['bill_to_address_line1'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "1 Card Lane";
        $data['bill_to_address_city'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "My City";
        $data['bill_to_address_state'] = trim($address['geoplugin_region'])? $address['geoplugin_region']: "CA";
        $data['bill_to_address_country'] = trim($address['geoplugin_countryCode'])? $address['geoplugin_countryCode']: "US";
        $data['bill_to_address_postal_code'] = $address['geoplugin_areaCode']? $address['geoplugin_areaCode']: "94043";
        $data['payment_token'] = $user['token'];
        $data['submit'] = "Submit";
        $data['customer_ip_address'] = CHttpRequest::getUserHostAddress();
        foreach($data as $name => $value) {
            $params[$name] = $value;
        }
       
        $data['signature'] = self::sign($params);
        $data['card_type'] = '';
        $data['card_number'] = '';
        $data['card_expiry_date'] = '';
        $resArray = self::hash_call($data);
        
        $logfile = dirname(__FILE__).'/hbl.txt';
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $msg .= "\n--------------------------processTransactions()---------------------------------------\n";
        $data = serialize($resArray);
        $msg.= $data."\n";
        //Write into the log file
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        
        if(isset($resArray['reason_code']) && $resArray['reason_code'] == 100){
            $res['is_success'] = 1;
            $res['transaction_status'] = $resArray['decision'];
            $res['invoice_id'] = $resArray['req_transaction_uuid'];
            $res['order_number'] = $resArray['req_reference_number'];
            $res['amount'] = $user['amount'];
            $res['paid_amount'] = $resArray['auth_amount'];
            $res['dollar_amount'] = $user['dollar_amount'];
            $res['currency_code'] = $user['currency_code'];
            $res['currency_symbol'] = $user['currency_symbol'];
            $res['response_text'] = serialize($resArray);
        }else{
            $res['isSuccess'] = 0;
            $res['code'] = $resArray['reason_code'];
            $res['response_text'] = serialize($resArray);
            $res['Message'] = 'Invalid Card details entered. Please check again';
        }
        return $res;
        
    }
    
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function defaultCard($usersub, $card) {
        self::initializePaymentGateway();
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function deleteCard($usersub, $card = Null,$gateway_code = 'hbl') {
        self::initializePaymentGateway();
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    /**
     * 
     * Cancel Customer's Account
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        return null;
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function saveCard($usersub, $arg) {
        $hbl = self::initializePaymentGateway();
        $timeparts = explode(" ",microtime());
        $currenttime = bcadd(($timeparts[0]*1000),bcmul($timeparts[1],1000));
        $reference_number = explode('.', $currenttime);
        $address = self::userAddress();
        $data['access_key'] = $hbl['access_key'];
        $data['profile_id'] = $hbl['profile_id'];
        $data['transaction_uuid'] = uniqid();
        $data['signed_field_names'] = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,customer_ip_address,consumer_id";
        $data['unsigned_field_names'] = "card_type,card_number,card_expiry_date";
        $data['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
        $data['locale'] = "en";
        $data['transaction_type'] = "create_payment_token";
        $data['reference_number'] = $reference_number[0];
        $data['consumer_id'] = $arg['user_id'];
        $data['amount'] = 0; //creating token and authorize the card not a payment transaction
        $data['currency'] = $arg['currency_code'];
        $data['payment_method'] = "card";
        $name = explode(' ',$arg['card_name']);
        $data['bill_to_forename'] = $name[0];
        $data['bill_to_surname'] = $name[count($name)-1];
        $data['bill_to_email'] = $arg['email'];
        $data['bill_to_address_line1'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "1 Card Lane";
        $data['bill_to_address_city'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "My City";
        $data['bill_to_address_state'] = trim($address['geoplugin_region'])? $address['geoplugin_region']: "CA";
        $data['bill_to_address_country'] = trim($address['geoplugin_countryCode'])? $address['geoplugin_countryCode']: "US";
        $data['bill_to_address_postal_code'] = $address['geoplugin_areaCode']? $address['geoplugin_areaCode']: "94043";
        $data['submit'] = "Submit";
        $data['customer_ip_address'] = CHttpRequest::getUserHostAddress();
        foreach($data as $name => $value) {
            $params[$name] = $value;
        }
        $data['signature'] = self::sign($params);
        $data['card_type'] = '001';
        $data['card_number'] = $arg['card_number'];
        $data['card_expiry_date'] = $arg['exp_month'].'-'.$arg['exp_year'];
        //print "<pre>";print_r($data);exit;
        $resArray = self::hash_call($data);
        
        if(isset($resArray['reason_code']) && $resArray['reason_code'] == 100){
            $res['isSuccess'] = 1;
            $res['card']['code'] = 200;
            $res['card']['status'] = $resArray['decision'];
            $res['card']['card_type'] = $arg['card_type']; //Card type
            $res['card']['card_last_fourdigit'] = $resArray['req_card_number'];;
            $res['card']['token'] = $resArray['payment_token'];
            $res['card']['response_text'] = serialize($resArray);
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        return $res;
    }
    
    
    function userAddress() {
        $data = array();
        $ip_address = CHttpRequest::getUserHostAddress();
        $geo_data = new EGeoIP();
        $geo_data->locate($ip_address);
        $address_str = serialize($geo_data);
        $string = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $address_str);
        $full_address = unserialize($string);
        $address = json_decode(json_encode($full_address), TRUE);
        $signup_location_address = $address['\0*\0_data']['\0CMap\0_d'];
        if (!count($signup_location_address) || empty($signup_location_address)) {
            $signup_location_address = $address['*_data']['CMap_d'];
            if(!count($signup_location_address) || empty($signup_location_address)){
                $signup_location_address = $address[' * _data'][' CMap _d'];
            }
        }
        $data['bill_to_address_line1'] = $signup_location_address['geoplugin_city'];
        $data['bill_to_address_city'] = $signup_location_address['geoplugin_city'];
        $data['bill_to_address_state'] = $signup_location_address['geoplugin_region'];
        $data['bill_to_address_country'] = $signup_location_address['geoplugin_countryCode'];
        $data['bill_to_address_postal_code'] = $signup_location_address['geoplugin_areaCode'];
        return $data;
    }
    
    
    function hash_call($data) {
        $params['params'] = $data;
        if($this->IS_LIVE_API_PAYMENT_GATEWAY['hbl'] == 'sandbox'){
            $params['url'] = "https://testsecureacceptance.cybersource.com/silent/pay";
        }else{
            $params['url'] = "https://secureacceptance.cybersource.com/silent/pay";
        }
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $params['url']);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params['params']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$server_output = curl_exec($ch);
        foreach(str_get_html($server_output)->find('input') as $element) {
            $res[$element->name] = $element->value;
        }
	curl_close($ch);
        return $res;
    }
    
    function sampleIntegrationTransaction($arg = array()) {
        $res = array();
        if (isset($arg['card_number']) && !empty($arg['card_number'])) {
            $hbl = self::initializePaymentGateway();
            $timeparts = explode(" ",microtime());
            $currenttime = bcadd(($timeparts[0]*1000),bcmul($timeparts[1],1000));
            $reference_number = explode('.', $currenttime);
            $address = self::userAddress();
            $arg['amount'] = Yii::app()->billing->currencyConversion('USD', $arg['currency_code'], $arg['amount']);
            $data['access_key'] = $hbl['access_key'];
            $data['profile_id'] = $hbl['profile_id'];
            $data['transaction_uuid'] = uniqid();
            $data['signed_field_names'] = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,customer_ip_address";
            $data['unsigned_field_names'] = "card_type,card_number,card_expiry_date";
            $data['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
            $data['locale'] = "en";
            $data['transaction_type'] = "authorization,create_payment_token";
            $data['reference_number'] = $reference_number[0];
            $data['amount'] = $arg['amount'];
            $data['currency'] = $arg['currency_code'];
            $data['payment_method'] = "card";
            $name = explode(' ',$arg['card_name']);
            $data['bill_to_forename'] = $name[0];
            $data['bill_to_surname'] = $name[count($name)-1];
            $data['bill_to_email'] = $arg['email'];
            $data['bill_to_address_line1'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "1 Card Lane";
            $data['bill_to_address_city'] = trim($address['geoplugin_city'])? $address['geoplugin_city']: "My City";
            $data['bill_to_address_state'] = trim($address['geoplugin_region'])? $address['geoplugin_region']: "CA";
            $data['bill_to_address_country'] = trim($address['geoplugin_countryCode'])? $address['geoplugin_countryCode']: "US";
            $data['bill_to_address_postal_code'] = $address['geoplugin_areaCode']? $address['geoplugin_areaCode']: "94043";
            $data['submit'] = "Submit";
            $data['customer_ip_address'] = CHttpRequest::getUserHostAddress();
            foreach($data as $name => $value) {
                $params[$name] = $value;
            }
            $data['signature'] = self::sign($params);
            $data['card_type'] = '001';
            $data['card_number'] = $arg['card_number'];
            $data['card_expiry_date'] = $arg['exp_month'].'-'.$arg['exp_year'];
            $resArray = self::hash_call($data);
            
            if(isset($resArray['reason_code']) && $resArray['reason_code'] == 100){
                $res['isSuccess'] = 1;
                $res['card']['code'] = 200;
            } else {
                $res['isSuccess'] = 0;
                $res['code'] = 55;
                $res['Message'] = 'We are not able to process your credit card. Please try another card.';
            }
        }else {
            $res['isSuccess'] = 0;
            $res['error_message'] = 'Invalid credit card number';
        }
        
        return $res;
    }
}