<?php
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$posterImg2 = POSTER_URL . '/no-image-a.png';
$posterImg3 = POSTER_URL . '/no-image-a.png';
$posterImg4 = POSTER_URL . '/no-image-a.png';

//$rr = TutorialScreen::model()->getMaxSequence(54,'android');
//if($rr['max_seq']>0){
//    echo "hello";
//}else{
//    echo "oops";
//}

?>
<style>
    #developer_id{
        display: none;
    }
    .help-block{
        display: none;
    }
    .has-error .help-block {
        display: block;
    }
    
    .layers-multi-image-remove {
  width: 24px;
  height: 24px;
  background-image: url("../../../../images/public/close.png");
  //background: red;
  display: block;
  z-index: 999;
  position: absolute;
  cursor: pointer;
}

ul.multi-image-list {
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: 650px;
  float: left;
}

ul.multi-image-list li {
  margin: 3px 3px 3px 0;
  padding: 1px;
  float: left;
  width: 150px;
  height: 150px;
  cursor: move;
  margin: 5px;
  overflow: hidden;
}

.layers-multi-image-upload-button {
  float: left;
}

html>body .multi-image-list li {
  height: 150px;
  line-height: 150px;
}

.ui-state-highlight {
  height: 150px;
  line-height: 150px;
}

input.image_ids {
  width: 100%;
  float: left;
  display: block;
  margin-top: 50px;
  padding: 5px;
}
</style>

<div class="row m-t-40 m-b-40">
    <div class="col-md-8 col-sm-12">
        <div class="Block">
            <form class="form-horizontal" method="POST" id="data-from" autocomplete="off">
                <input type="hidden" name="app_type" value="android" />
                <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>
                    <div class="form-group">
                        <div class="col-md-12">
                            <h4><span class=" bold red">Android App is a paid add-on.</span><a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/android/1');"><span class="bold blue"> Purchase subscription</span></a><span class="bold red"> to enable it.</span></h4>
                        </div>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="appname" class="col-md-4 control-label">App Name:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                                <input type="text"  name="app_name" id="app_name" placeholder="App Name  (max limit 30 characters)" class="form-control input-sm" value="<?php if (isset($appdata) && ($appdata['app_name'] != '' || $appdata['app_name'] != null)) {
                    echo $appdata['app_name'];
                                } ?>" 
                                <?php if(isset($appdata['app_name']) && trim($appdata['app_name'])){echo 'readonly="true" style="cursor: not-allowed;" ';}?> required />
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="desc" class="col-md-4 control-label">Short Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="short_description" id="short_description" placeholder="Short Description   (max limit 80 characters)" class="form-control input-sm" value="<?php if (isset($appdata)) {
                    echo $appdata['short_description'];
                } ?>" <?php if(isset($appdata['short_description']) && trim($appdata['short_description'])){echo 'readonly="true" style="cursor: not-allowed;"';}?> />
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="desc" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="description" id="description" placeholder="Description  (max limit 4000 characters)" class="form-control input-sm" rows="5" ><?php if (isset($appdata)) {
                    echo $appdata['description'];
                } ?></textarea>
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">App Icon:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="app-icon-modal-btn">Upload App Icon</button>
                        <?php
                        $app_icon_url = $appdata['app_icon'];
                        $splash_screen_url = $appdata['splash_screen_portrait'];
                        $splash_screen_url2 = $appdata['splash_screen_landscape'];
                        $feature_graphic_url = $appdata['feature_graphic'];
                        ?>
<?php if (isset($app_icon_url) && $app_icon_url != '') { ?>
                        <div class="fixedWidth--Preview m-t-20">
                            <img src="<?php echo $app_icon_url; ?>"   rel="tooltip" />
                        </div>
<?php } ?>
                        <small class="help-block">Please upload the app icon.</small>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="screen" class="col-md-4 control-label">Splash Screen:</label>
                    <div class="col-md-8">
                        
                        <!--TOOGLE  PORTRAIT START-->
                        <div class="Collapse-Block">
                            <div class="form-group">
                       <div class="Block-Header has-right-icon">
                          <!-- Icon exist to left :--> 
                          <div class="icon-OuterArea--rectangular"> <a href="javascript:void(0);"> <em class="icon-arrow-down icon left-icon icon-arrow-up"></em> </a> </div>
                          <h4 class="drag-cursor">Portrait Splash Screen </h4>
                          <p class="grey font-12">Choose a 639x1136 JPEG or PNG file. </p>
                           <div class=" m-t-10">
                                <button type="button" class="btn btn-default-with-bg btn-sm" id="splash-screen-modal-btn1">Upload Portrait Splash Screen</button>
                           </div>
                       </div>
                       <div class="Collapse-Content m-t-20">
                           <?php if (isset($splash_screen_url) && $splash_screen_url != '') { ?>
                         <div class="fixedWidth--Preview">
                                    <img src="<?php echo $splash_screen_url; ?>" alt="" id="fhd-poster-img" class="img-responsive">

                                </div>
                           <?php }?>
                       </div>
                            <small class="help-block">Please upload the portrait splash screen image.</small>
                    </div>
                    </div>
                        <!--TOOGLE  PORTRAIT END-->
                        
                        <!--TOOGLE LANDSCAPE START-->
                        <div class="Collapse-Block">
                        <div class="form-group">
                       <div class="Block-Header has-right-icon">
                          <!-- Icon exist to left :--> 
                          <div class="icon-OuterArea--rectangular"> <a href="javascript:void(0);"> <em class="icon-arrow-down icon left-icon icon-arrow-up"></em> </a> </div>
                          <h4 class="drag-cursor">Landscape Splash Screen</h4>
                          <p class="grey font-12">Choose a 1136x639 JPEG or PNG file. </p>
                           <div class=" m-t-10">
                                <button type="button" class="btn btn-default-with-bg btn-sm" id="splash-screen-modal-btn2">Upload Landscape Splash Screen</button>
                           </div>
                       </div>
                       <div class="Collapse-Content m-t-20">
                           <?php if (isset($splash_screen_url2) && $splash_screen_url2 != '') { ?>
                         <div class="fixedWidth--Preview">
                                    <img src="<?php echo $splash_screen_url2; ?>" alt="" id="fhd-poster-img" class="img-responsive">

                                </div>
                           <?php }?>
                       </div>
                            <small class="help-block">Please upload the landscape splash screen image.</small>
                    </div>
                    </div>
                        <!--TOOGLE LANDSCAPE END-->
                        
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">Tutorial Screen:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="screen-modal-btn">Upload Tutorial Screen</button>
                        
                        <section class="layers-multi-image-container layers-has-multi-image">

                        <ul class="layers-multi-image-list multi-image-list layers-sortable ui-sortable">
                          <!-- Image -->
                          <?php
                          $data = TutorialScreen::model()->getAll($studio->id,'android');
                          if(count($data) > 0){
                          for($i=0;$i<count($data);$i++){
                          ?>
                          <li>
                              <span class="layers-multi-image-remove" title="Delete"></span>
                              <img data-img_id="<?=$data[$i]['id'];?>" id="<?=$data[$i]['sequence'];?>" class="multi-image-reveal" src="<?=$data[$i]['screen_img'];?>" height="150" width="150">
                          </li>
                          <?php
                          }
                          }
                          ?>
                        </ul>
                            
                            <input type="hidden" class="image_ids" id="widget-layers-widget-gallery-33-images" name="widget-layers-widget-gallery[33][images]" value="">
                      </section>

                        <small class="help-block">Please upload the tutorial screen.</small>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">Transparent App icon:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="transparent-icon-modal-btn">Upload Transparent App Icon</button>
                        <?php
                        $transparent_icon_url = $appdata['transparent_app_icon'];
                        $splash_screen_url = $appdata['splash_screen_portrait'];
                        $splash_screen_url2 = $appdata['splash_screen_landscape'];
                        $feature_graphic_url = $appdata['feature_graphic'];
                        ?>
<?php if (isset($transparent_icon_url) && $transparent_icon_url != '') { ?>
                        <div class="fixedWidth--Preview m-t-20">
                            <img src="<?php echo $transparent_icon_url; ?>"   rel="tooltip" />
                        </div>
<?php } ?>
                        <small class="help-block">Please upload the transparent app icon.</small>
                    </div>
                    
                </div>                
                
                <div class="form-group">
                    <label for="distribution" class="col-md-4 control-label">Distribution Geography:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="" id="dist_geo_id">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['distribution_geography'];
} ?>" id="geography" />
                                <select class="form-control input-sm" name="distribution_geography[]" style="height: 200px;" id="distribution_geography" multiple>
                                    <option value="All" selected>All Countries</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina">Burkina</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Chile">Chile</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote d' Ivore">Cote d' Ivore</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Laos">Laos</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macedonia">Macedonia</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Moldova">Moldova</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russia</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia">Serbia</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Korea">South Korea</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Taiwan">Taiwan</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States (including Puerto Rico, American Samoa, Guam, Marshall Islands, Northern Mariana Islands, Palau and US Virgin Islands)">United States (including Puerto Rico, American Samoa, Guam, Marshall Islands, Northern Mariana Islands, Palau and US Virgin Islands)</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Vietnam">Vietnam</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-md-4 control-label">Language:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) { echo $appdata['language'];} ?>" id="language" />
                                <select class="form-control input-sm" name="language" id="language-dd" <?php if(isset($appdata['language']) && trim($appdata['language'])) { echo "disabled= disabled";}?>>
                                    <option value="Australian English">Australian English</option>
                                    <option value="Afrikaans – af">Afrikaans – af</option>
                                    <option value="Amharic – am">Amharic – am</option>
                                    <option value="Arabic – ar">Arabic – ar</option>
                                    <option value="Armenian – hy-AM">Armenian – hy-AM</option>
                                    <option value="Azerbaijani – az-AZ">Azerbaijani – az-AZ</option>
                                    <option value="Basque – eu-ES">Basque – eu-ES</option>
                                    <option value="Belarusian – be">Belarusian – be</option>
                                    <option value="Bengali – bn-BD">Bengali – bn-BD</option>
                                    <option value="Bulgarian – bg">Bulgarian – bg</option>
                                    <option value="Burmese – my-MM">Burmese – my-MM</option>
                                    <option value="Catalan – ca">Catalan – ca</option>
                                    <option value="Chinese (Simplified) – zh-CN">Chinese (Simplified) – zh-CN</option>
                                    <option value="Chinese (Traditional) – zh-TW">Chinese (Traditional) – zh-TW</option>
                                    <option value="Croatian – hr">Croatian – hr</option>
                                    <option value="Czech – cs-CZ">Czech – cs-CZ</option>
                                    <option value="Danish – da-DK">Danish – da-DK</option>
                                    <option value="Dutch – nl-NL">Dutch – nl-NL</option>
                                    <option value="English – en-AU">English – en-AU</option>
                                    <option value="English (United Kingdom) – en-GB">English (United Kingdom) – en-GB</option>
                                    <option value="English (United States) – en-US" selected="">English (United States) – en-US</option>
                                    <option value="Estonian – et">Estonian – et</option>
                                    <option value="Filipino – fil">Filipino – fil</option>
                                    <option value="Finnish – fi-FI">Finnish – fi-FI</option>
                                    <option value="French – fr-FR">French – fr-FR</option>
                                    <option value="French (Canada) – fr-CA">French (Canada) – fr-CA</option>
                                    <option value="Galician – gl-ES">Galician – gl-ES</option>
                                    <option value="Georgian – ka-GE">Georgian – ka-GE</option>
                                    <option value="German – de-DE">German – de-DE</option>
                                    <option value="Greek – el-GR">Greek – el-GR</option>
                                    <option value="Hebrew – iw-IL">Hebrew – iw-IL</option>
                                    <option value="Hindi – hi-IN">Hindi – hi-IN</option>
                                    <option value="Hungarian – hu-HU">Hungarian – hu-HU</option>
                                    <option value="Icelandic – is-IS">Icelandic – is-IS</option>
                                    <option value="Indonesian – id">Indonesian – id</option>
                                    <option value="Italian – it-IT">Italian – it-IT</option>
                                    <option value="Japanese – ja-JP">Japanese – ja-JP</option>
                                    <option value="Kannada – kn-IN">Kannada – kn-IN</option>
                                    <option value="Khmer – km-KH">Khmer – km-KH</option>
                                    <option value="Korean (South Korea) – ko-KR">Korean (South Korea) – ko-KR</option>
                                    <option value="Kyrgyz – ky-KG">Kyrgyz – ky-KG</option>
                                    <option value="Lao – lo-LA">Lao – lo-LA</option>
                                    <option value="Latvian – lv">Latvian – lv</option>
                                    <option value="Lithuanian – lt">Lithuanian – lt</option>
                                    <option value="Macedonian – mk-MK">Macedonian – mk-MK</option>
                                    <option value="Malay – ms">Malay – ms</option>
                                    <option value="Malayalam – ml-IN">Malayalam – ml-IN</option>
                                    <option value="Marathi – mr-IN">Marathi – mr-IN</option>
                                    <option value="Mongolian – mn-MN">Mongolian – mn-MN</option>
                                    <option value="Nepali – ne-NP">Nepali – ne-NP</option>
                                    <option value="Norwegian – no-NO">Norwegian – no-NO</option>
                                    <option value="Persian – fa">Persian – fa</option>
                                    <option value="Polish – pl-PL">Polish – pl-PL</option>
                                    <option value="Portuguese (Brazil) – pt-BR">Portuguese (Brazil) – pt-BR</option>
                                    <option value="Portuguese (Portugal) – pt-PT">Portuguese (Portugal) – pt-PT</option>
                                    <option value="Romanian – ro">Romanian – ro</option>
                                    <option value="Romansh – rm">Romansh – rm</option>
                                    <option value="Russian – ru-RU">Russian – ru-RU</option>
                                    <option value="Serbian – sr">Serbian – sr</option>
                                    <option value="Sinhala – si-LK">Sinhala – si-LK</option>
                                    <option value="Slovak – sk">Slovak – sk</option>
                                    <option value="Slovenian – sl">Slovenian – sl</option>
                                    <option value="Spanish (Latin America) – es-419">Spanish (Latin America) – es-419</option>
                                    <option value="Spanish (Spain) – es-ES">Spanish (Spain) – es-ES</option>
                                    <option value="Spanish (United States) – es-US">Spanish (United States) – es-US</option>
                                    <option value="Swahili – sw">Swahili – sw</option>
                                    <option value="Swedish – sv-SE">Swedish – sv-SE</option>
                                    <option value="Tamil – ta-IN">Tamil – ta-IN</option>
                                    <option value="Telugu – te-IN">Telugu – te-IN</option>
                                    <option value="Thai – th">Thai – th</option>
                                    <option value="Turkish – tr-TR">Turkish – tr-TR</option>
                                    <option value="Ukrainian – uk">Ukrainian – uk</option>
                                    <option value="Vietnamese – vi">Vietnamese – vi</option>
                                    <option value="Zulu – zu">Zulu – zu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="rating" class="col-md-4 control-label">Rating:</label>
                    <div class="col-md-8">
                        <input type="hidden" value="<?php if(isset($appdata)){echo $appdata['rating'];}?>" id="rating" />
                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Cartoon or Fantasy Violence:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-1" name="rating[Cartoon or Fantasy Violence]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Realistic Violence:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-2" name="rating[Realistic Violence]" >                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Sexual Content or Nudity:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-3" name="rating[Sexual Content or Nudity]"  >                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Profanity or Crude Humor:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-4" name="rating[Profanity or Crude Humor]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Alcohol, Tobacco, or Drug Use or References:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-5" name="rating[Alcohol, Tobacco, or Drug Use or References]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Mature/Suggestive Themes:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-6" name="rating[Mature/Suggestive Themes]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Simulated Gambling:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-7" name="rating[Simulated Gambling]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Horror/Fear Themes:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-8" name="rating[Horror/Fear Themes]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Prolonged graphic or sadistic realistic violence:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-9" name="rating[Prolonged graphic or sadistic realistic violence]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label"> Graphic Sexual Content and Nudity:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-10" name="rating[Graphic Sexual Content and Nudity]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label for="website" class="col-md-4 control-label">Website:</label>
                    <div class="col-md-8">
                        <div class="fg-line">                        
                                        <input type="text" id="website" name="website" placeholder="Website" class="form-control input-sm" value="<?php if (isset($appdata) && ($appdata['website'] != '' || $appdata['website'] != null)) {echo $appdata['website']; } else { echo $studio->domain; } ?>" 
                                        <?php if(isset($appdata['website']) && trim($appdata['website'])) { echo "disabled= disabled"; }?> />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Email:</label>
                    <div class="col-md-8">
                        <div class="fg-line">   
                            <input type="text" name="email" id="email" placeholder="Email" class="form-control input-sm" value="<?php if (isset($appdata) && ($appdata['email'] != '' || $appdata['email'] != null)) {
    echo $appdata['email'];
} else {
    echo $studio->contact_us_email;
} ?>" required />
                        </div>
                        <small class="help-block">Please give a valid Email.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-md-4 control-label">Phone:</label>
                    <div class="col-md-8">
                        <div class="fg-line">   
                            <input type="text" name="phone" id="phone" placeholder="Phone" class="form-control input-sm" value="<?php if (isset($appdata)) {
    echo $appdata['phone'];
} ?>" required />
                        </div>
                        <small class="help-block">Please give a valid Phone number.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="graphic" class="col-md-4 control-label">Feature Graphic:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg btn-sm" id="feature-graphic-modal-btn">Upload Feature Graphic</button>
<?php if (isset($feature_graphic_url) && $feature_graphic_url != '') { ?>
                        <div class="fixedWidth--Preview m-t-20">
                            <img src="<?php echo $feature_graphic_url; ?>"   rel="tooltip" />
                        </div>
<?php } ?>          <small class="help-block">Please upload the feature graphic image.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="category" class="col-md-4 control-label">Category:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) { echo $appdata['category'];} ?>" id="category" />
                               
                                <?php if(isset($appdata['category']) && trim($appdata['category'])) { ?>
                                 <input type="text" class="form-control input-sm" name="category" id="category-dd" value="<?php echo $appdata['category'] ?>" readonly="true" style="cursor: not-allowed;" > <?php }  else { ?>

                                
                                <select class="form-control input-sm" name="category" id="category-dd" >
                                    <option value="">Select</option>
                                    <option value="Books and References">Books and References</option>
                                    <option value="Business">Business</option>
                                    <option value="Comics">Comics</option>
                                    <option value="Communication">Communication</option>
                                    <option value="Education">Education</option>
                                    <option value="Entertainment">Entertainment</option>
                                    <option value="Finance">Finance</option>
                                    <option value="Health and Fitness">Health and Fitness</option>
                                    <option value="Libraries and Demo">Libraries and Demo</option>
                                    <option value="Lifestyles">Lifestyles</option>
                                    <option value="Live wallpaper">Live wallpaper</option>
                                    <option value="Media and Video">Media and Video</option>
                                    <option value="Medical">Medical</option>
                                    <option value="Music and Audio">Music and Audio</option>
                                    <option value="News and Magazines">News and Magazines</option>
                                    <option value="Personalization">Personalization</option>
                                    <option value="Photography">Photography</option>
                                    <option value="Productivity">Productivity</option>
                                    <option value="Shopping">Shopping</option>
                                    <option value="Social">Social</option>
                                    <option value="Sports">Sports</option>
                                    <option value="Tools">Tools</option>
                                    <option value="Transportation">Transportation</option>
                                    <option value="Travel and Local">Travel and Local</option>
                                    <option value="Weather">Weather</option>
                                    <option value="Widgets">Widgets</option>
                                </select>
                                 <?php } ?>
                            </div>
                        </div>
                        <small class="help-block">Please select a category.</small>
                    </div>
                </div>

               <!-- <div class="form-group">
                    <label for="rating" class="col-md-4 control-label">Rating:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['rating'];
} ?>" id="rating" />
                                <select class="form-control input-sm" name="rating" id="rating-dd">
                                    <option>Select</option>
                                    <option value="High">High</option>
                                    <option value="Medium Rating">Medium</option>
                                    <option value="Low Rating">Low</option>
                                    <option value="Everyone">Everyone</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>-->

                <div class="form-group">
                    <label for="inputPassword3" class="col-md-4 control-label">Privacy Policy URL:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Privacy Policy Url" id="privacy_policy" name="privacy_policy_url" class="form-control input-sm" value="<?php if (isset($appdata)) {
    echo $appdata['privacy_policy_url'];
    } ?>" <?php if(isset($appdata['privacy_policy_url']) && trim($appdata['privacy_policy_url'])) { echo 'readonly="true" style="cursor: not-allowed;" '; }?> required/>
                        </div>
                        <small class="help-block">Please provide the privacy policy url.</small>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="checkbox">
                            <label>
                        <?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                                <input type="checkbox" id="developer_id_checkbox" name="developer_check" value="1" checked disabled="disabled" />
<?php } else { ?>
                                    <input type="checkbox" id="developer_id_checkbox"  name="developer_check" value="1" />
<?php } ?>
                                <i class="input-helper"></i> Use my Android Developers ID
                            </label>
                        </div>
                    </div>
                </div>   
               
<?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                    <div id="developer_id" style="display:block">
<?php } else { ?>
                        <div id="developer_id">
<?php } ?>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">User Name:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" placeholder="User Name" id="dev_uname" name="developer_username" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_username'];
} ?>" <?php if (isset($appdata) && trim($appdata['developer_username'])) {echo "disabled=disabled";} ?> />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password:</label>
                            <div class="col-md-8">
                                <input type="password" placeholder="Password" id="dev_password" name="developer_password" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_password'];
} ?>" <?php if (isset($appdata) && trim($appdata['developer_password'])) {echo "disabled=disabled";} ?> />
                            </div>
                        </div>
                    </div>

            </form>
            <form <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?> action="javascript:void(0)"<?php } else { ?>action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/androidApp" method="POST" id="data-from-save" <?php } ?> class="form-horizontal">
                <input type="hidden" name="app_type" value="android" />
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" class="btn btn-primary waves-effect btn-sm m-t-30" id="data-from-btn" <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled<?php } ?>> &nbsp;&nbsp;Save&nbsp;&nbsp;  </button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-lg-2"></label>
                    <div class="col-md-10">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
 <div class="modal fade is-Large-Modal" id="app-icon-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/appIcon" method="POST" enctype="multipart/form-data" id="app-icon-from">
			 <input type="hidden" name="app_type" value="android" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput')">Browse </button>
							
							<p class="help-block">Upload image size of 512x512</p>
							 <input class="avatar-input" id="avatarInput" name="lunchicon" type="file" style="display:none;" onchange="fileSelectHandler();">
							<span id="file_error" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($app_icon_url) && $app_icon_url != '') {
									$posterImg = $app_icon_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
											<div class=" m-b-10" id="avatar_preview_div" >
												<img class="jcrop-preview" src="<?php echo $posterImg; ?>" id="preview_content_img" rel="tooltip" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x1" name="jcrop_lunchicon[x1]" />
									<input type="hidden" id="y1" name="jcrop_lunchicon[y1]" />
									<input type="hidden" id="x2" name="jcrop_lunchicon[x2]" />
									<input type="hidden" id="y2" name="jcrop_lunchicon[y2]" />
									<input type="hidden" id="w" name="jcrop_lunchicon[w]">
									<input type="hidden" id="h" name="jcrop_lunchicon[h]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="app-icon-btn" data-dismiss="modal">Save</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>

<!--  Transparent app icon start   -->
 <div class="modal fade is-Large-Modal" id="transparent-icon-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/TransparentAppIcon" method="POST" enctype="multipart/form-data" id="transparent-icon-from">
			 <input type="hidden" name="app_type" value="android" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInputTransparent')">Browse </button>
							
							<p class="help-block">Upload image size of 512x512</p>
							 <input class="avatar-input" id="avatarInputTransparent" name="lunchicon" type="file" style="display:none;" onchange="TransparentfileSelectHandler();">
							<span id="file_error" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($transparent_icon_url) && $transparent_icon_url != '') {
									$posterImg1 = $transparent_icon_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
											<div class=" m-b-10" id="avatar_preview_div_transparent" >
												<img class="jcrop-preview" src="<?php echo $posterImg1; ?>" id="preview_content_img_transparent" rel="tooltip" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x101" name="jcrop_lunchicon[x101]" />
									<input type="hidden" id="y101" name="jcrop_lunchicon[y101]" />
									<input type="hidden" id="x201" name="jcrop_lunchicon[x201]" />
									<input type="hidden" id="y201" name="jcrop_lunchicon[y201]" />
									<input type="hidden" id="w5" name="jcrop_lunchicon[w5]">
									<input type="hidden" id="h5" name="jcrop_lunchicon[h5]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="transparent-icon-btn" data-dismiss="modal">Save</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>

<!--SPLASH SCREEN MODAL-1 START-->
<div class="modal fade is-Large-Modal" id="splash-screen-modal-box1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/SplashScreenAndroidPortrait" method="POST" enctype="multipart/form-data" id="splash-screen-from1">
            <input type="hidden" name="app_type" value="android" />
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Upload Image</h4> 
          </div>
          <div class="modal-body">
                <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                <h5 class="m-b-20"><b>Upload Vertical Splash Screen</b></h5>

                                <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarSplashInput')">Browse </button>

                                <p class="help-block">Upload image size of 639x1136</p>
                                <input class="avatar-input" id="avatarSplashInput" name="splashicon" type="file" style="display:none;" onchange="splashFileSelectHandler();">
                                <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                                </div>
                                        <div class="col-xs-12">
                                        <?php
                                        if (isset($splash_screen_url) && $splash_screen_url != '') {
                                                $posterImg2 = $splash_screen_url;
                                        }
                                        ?>
                                                <div class="Preview-Block">
                                                        <div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
                                                                <div class=" m-b-10" id="avatar_preview_div_splash" >
                                                                        <img class="jcrop-preview"  src="<?php echo $posterImg2; ?>" style="height:400px;width:300px;" id="preview_content_img_splash" rel="tooltip" />
                                                                </div>
                                                        </div>
                                                </div>
                                                <input type="hidden" id="x11" name="jcrop_splashicon[x11]" />
                                                <input type="hidden" id="y11" name="jcrop_splashicon[y11]" />
                                                <input type="hidden" id="x21" name="jcrop_splashicon[x21]" />
                                                <input type="hidden" id="y21" name="jcrop_splashicon[y21]" />
                                                <input type="hidden" id="w1" name="jcrop_splashicon[w1]">
                                                <input type="hidden" id="h1" name="jcrop_splashicon[h1]">

                                        </div>
                                </div>
                </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="splash-screen-btn1" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
            </form>
        </div>
      </div>
    </div>
<!--SPLASH SCREEN MODAL-1 END-->

<!--SPLASH SCREEN MODAL-2 START-->
<div class="modal fade is-Large-Modal" id="splash-screen-modal-box2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/SplashScreenAndroidLandscape" method="POST" enctype="multipart/form-data" id="splash-screen-from2">
            <input type="hidden" name="app_type" value="android" />
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Upload Image</h4>
          </div>
          <div class="modal-body">
                <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                <h5 class="m-b-20"><b>Upload Horizontal Splash Screen</b></h5>
                                <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarSplashInput2')">Browse </button>

                                <p class="help-block">Upload image size of 1136x639</p>
                                <input class="avatar-input" id="avatarSplashInput2" name="splashicon2" type="file" style="display:none;" onchange="splashFileSelectHandlerLand();">
                                <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                                </div>
                                        <div class="col-xs-12">
                                        <?php
                                        if (isset($splash_screen_url2) && $splash_screen_url2 != '') {
                                                $posterImg3 = $splash_screen_url2;
                                        }
                                        ?>
                                                <div class="Preview-Block">
                                                        <div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
                                                                <div class=" m-b-10" id="avatar_preview_div_splash2" >
                                                                <img class="jcrop-preview" src="<?php echo $posterImg3; ?>" style="height:300px;width:400px;" id="preview_content_img_splash2" rel="tooltip" />
                                                                </div>
                                                        </div>
                                                </div>
                                                <input type="hidden" id="x112" name="jcrop_splashicon_lscape[x112]" />
                                                <input type="hidden" id="y112" name="jcrop_splashicon_lscape[y112]" />
                                                <input type="hidden" id="x212" name="jcrop_splashicon_lscape[x212]" />
                                                <input type="hidden" id="y212" name="jcrop_splashicon_lscape[y212]" />
                                                <input type="hidden" id="w12" name="jcrop_splashicon_lscape[w12]">
                                                <input type="hidden" id="h12" name="jcrop_splashicon_lscape[h12]">

                                        </div>
                                </div>
                </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="splash-screen-btn2" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
            </form>
        </div>
      </div>
    </div>
 
<!--SPLASH SCREEN MODAL-2 END-->

 <div class="modal fade is-Large-Modal" id="feature-graphic-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/featureGraphic" method="POST" enctype="multipart/form-data" id="feature-graphic-from">
            <input type="hidden" name="app_type" value="android" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarFeatureGraphicInput')">Browse </button>
         
							<p class="help-block">Upload image size of 1024x500</p>
							 <input class="avatar-input" id="avatarFeatureGraphicInput" type="file" style="display:none;" name="feature_graphic" onchange="featureGraphicFileSelectHandler();">
							<span id="file_error_3" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($feature_graphic_url) && $feature_graphic_url != '') {
									$posterImg3 = $feature_graphic_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="feature_graphic_div">
											<div class="m-b-10" id="avatar_preview_div_feature_graphic" >
                                                                                            <img class="jcrop-preview" src="<?php echo $posterImg3; ?>" id="preview_content_img_feature_graphic" style="height:300px;width: 700px;" rel="tooltip" />
											</div>
										</div>
									</div>
									 <input type="hidden" id="x12" name="jcrop_feature_graphic[x12]" />
									<input type="hidden" id="y12" name="jcrop_feature_graphic[y12]" />
									<input type="hidden" id="x22" name="jcrop_feature_graphic[x22]" />
									<input type="hidden" id="y22" name="jcrop_feature_graphic[y22]" />
									<input type="hidden" id="w2" name="jcrop_feature_graphic[w2]">
									<input type="hidden" id="h2" name="jcrop_feature_graphic[h2]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="feature-graphic-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>
<!--TUTORIAL SCREEN START-->
<div class="modal fade is-Large-Modal" id="screen-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/TutorialScreen" method="POST" enctype="multipart/form-data" id="tutorial-screen-from">
            <input type="hidden" name="app_type" value="android" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image For Tutorial Screen</h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('tutorialScreenInput')">Browse </button>

							<p class="help-block">Upload image size of 1024x1024</p>
                                                        <input class="avatar-input" id="tutorialScreenInput" type="file" style="display:none;" name="tutorial_screen" onchange="tutorialScreenSelectHandler();">
							<span id="file_error_3" class="error red" for="subdomain" style="display: block;"></span>

							</div>
								<div class="col-xs-12">
								
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="feature_graphic_div">
											<div class="m-b-10" id="avatar_preview_div_tutorial_screen" >
                                                                                            <img class="jcrop-preview" src="<?php echo $posterImg4; ?>" id="preview_content_img_tutorial_screen" style="height:400px;width: 400px;" rel="tooltip" />
											</div>
										</div>
									</div>
									 <input type="hidden" id="screen_x1" name="jcrop_tutorial_screen[screen_x1]" />
									<input type="hidden" id="screen_y1" name="jcrop_tutorial_screen[screen_y1]" />
									<input type="hidden" id="screen_x2" name="jcrop_tutorial_screen[screen_x2]" />
									<input type="hidden" id="screen_y2" name="jcrop_tutorial_screen[screen_y2]" />
									<input type="hidden" id="screen_w2" name="jcrop_tutorial_screen[screen_w2]">
									<input type="hidden" id="screen_h2" name="jcrop_tutorial_screen[screen_h2]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="tutorial-screen-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>
<!--TUTORIAL SCREEN END-->
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<script>
jQuery(document).ready(function($) {

  $("ul.layers-multi-image-list").sortable({
    placeholder: "ui-state-highlight",
    update: function(event, ui) {
      var ordering = $.map($("> li img", this), function(el) {
        return el.id
      }).join(",");
      
      $('.image_ids').val(ordering);
      $.post("<?php echo Yii::app()->baseUrl; ?>/template/ScreenSequence", {'type':'android', 'list': ordering}, function (res) {
          
        });
    }
  });

  $('ul.layers-multi-image-list').disableSelection();

  $("ul.layers-multi-image-list").on('click', '.layers-multi-image-remove', function() {
    var imgId = $(this).parent().find("img").attr("id");
    var img_id = $(this).parent().find("img").data("img_id");
    //var conf = confirm("Are you sure to delete this image!");
    //var conf = swal("Are you sure to delete this image!");
    
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this image file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonColor: "#c0cbdb",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
          $.post("<?php echo Yii::app()->baseUrl; ?>/template/deleteScreen", {'type':'android', 'img_id': img_id}, function (res) {
              swal("Deleted!", "Image has been deleted.", "success");
              location.reload();
            });
            
      });
    
  });
});

</script>
<script>
    function click_browse(modal_file) {
        $("#"+modal_file).click();
    }
                    var jcrop_api;
                    function fileSelectHandler() {
                        var reqwidth = 512;
                        var reqheight = 512;
                        var aspectRatio = reqwidth / reqheight;
                        clearInfo();
                        //$("#avataredit_preview").hide();
                        //$("#avatar_preview_div").removeClass("hide");
                        // get selected file
                        var oFile = $('#avatarInput')[0].files[0];
                        // hide all errors
                        // check for image type (jpg and png are allowed)
                        var rFilter = /^(image\/jpeg|image\/png)$/i;
                        if (!rFilter.test(oFile.type)) {
                            swal('Please select a valid image file (jpg and png are allowed)');
                            $("#avatar_preview_div").html('');
                            $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                            $('button[type="submit"]').attr('disabled', 'disabled');
                            return;
                        }

                        // preview element
                        var oImage = document.getElementById('preview_content_img');

                        // prepare HTML5 FileReader
                        var oReader = new FileReader();
                        oReader.onload = function (e) {
                            $('#file_error').hide();
                            // e.target.result contains the DataURL which we can use as a source of the image
                            oImage.src = e.target.result;
                            //$('#contentImg').attr('src',e.target.result);
                            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

                            oImage.onload = function () { // onload event handler
                                var w = oImage.naturalWidth;
                                var h = oImage.naturalHeight;
                                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                                console.log("Width=" + w + " Height=" + h);
                                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                                    $("#avatar_preview_div").html('');
                                    $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                                    $('button[type="submit"]').attr('disabled', 'disabled');
                                    return;
                                }

                                // destroy Jcrop if it is existed
                                if (typeof jcrop_api != 'undefined') {
                                    jcrop_api.destroy();
                                    jcrop_api = null;
                                }
                                $('#preview_content_img').width(oImage.naturalWidth);
                                $('#preview_content_img').height(oImage.naturalHeight);
                                //setTimeout(function(){
                                // initialize Jcrop
                                $('#preview_content_img').Jcrop({
                                    minSize: [reqwidth, reqheight], // min crop size,
                                    //maxSize: [reqwidth, reqheight],
                                    boxWidth: 300,
                                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                                    bgFade: true, // use fade effect
                                    bgOpacity: .3, // fade opacity
                                    onChange: updateInfo,
                                    onSelect: updateInfo,
                                    onRelease: clearInfo
                                }, function () {

                                    // use the Jcrop API to get the real image size
                                    bounds = this.getBounds();
                                    boundx = bounds[0];
                                    boundy = bounds[1];

                                    // Store the Jcrop API in the jcrop_api variable
                                    jcrop_api = this;
                                    //jcrop_api.animateTo([10, 10, 290, 410]);
                                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                                });
                                //},100);

                            };
                        };

                        // read selected file as DataURL
                        oReader.readAsDataURL(oFile);
                    }
</script>
<!-- transparent  -->
<script>
    var jcrop_api;
    function TransparentfileSelectHandler() {
        var reqwidth = 512;
        var reqheight = 512;
        var aspectRatio = reqwidth / reqheight;
        clearInfoTransparent();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarInputTransparent')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_transparent").html('');
            $("#avatar_preview_div_transparent").html('<img id="preview_content_img_transparent"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_transparent');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_transparent").html('');
                    $("#avatar_preview_div_transparent").html('<img id="preview_content_img_transparent"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_transparent').width(oImage.naturalWidth);
                $('#preview_content_img_transparent').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_transparent').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoTransparent,
                    onSelect: updateInfoTransparent,
                    onRelease: clearInfoTransparent
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<!--TUTORIAL SCREEN START-->
<script>
    var jcrop_api;
    function tutorialScreenSelectHandler() {
        var reqwidth = 1024; //before 1242
        var reqheight = 1024; //before 2208
        var aspectRatio = reqwidth / reqheight;
        clearInfoTutorialScreen();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#tutorialScreenInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_tutorial_screen").html('');
            $("#avatar_preview_div_tutorial_screen").html('<img class="jcrop-preview" id="preview_content_img_tutorial_screen"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_tutorial_screen');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_tutorial_screen").html('');
                    $("#avatar_preview_div_tutorial_screen").html('<img class="jcrop-preview" id="preview_content_img_tutorial_screen"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_tutorial_screen').width(oImage.naturalWidth);
                $('#preview_content_img_tutorial_screen').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_tutorial_screen').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                   // maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoTutorialScreen,
                    onSelect: updateInfoTutorialScreen,
                    onRelease: clearInfoTutorialScreen
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<!--TUTORIAL SCREEN END-->

<script>
    var jcrop_api;
    function splashFileSelectHandler() {
        var reqwidth = 639; //before 1242
        var reqheight = 1136; //before 2208
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplash();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarSplashInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash").html('');
            $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_splash');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash").html('');
                    $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash').width(oImage.naturalWidth);
                $('#preview_content_img_splash').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                   // maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplash,
                    onSelect: updateInfoSplash,
                    onRelease: clearInfoSplash
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<!--SPLASH HANDLER LANDSCAPE-->
<script>
    var jcrop_api;
    function splashFileSelectHandlerLand() {
        var reqwidth = 1136; 
        var reqheight = 639;
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplashLscape();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarSplashInput2')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash2").html('');
            $("#avatar_preview_div_splash2").html('<img class="jcrop-preview" id="preview_content_img_splash2"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_splash2');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash2").html('');
                    $("#avatar_preview_div_splash2").html('<img class="jcrop-preview" id="preview_content_img_splash2"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash2').width(oImage.naturalWidth);
                $('#preview_content_img_splash2').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash2').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                   // maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplashLscape,
                    onSelect: updateInfoSplashLscape,
                    onRelease: clearInfoSplashLscape
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<!--SPLASH HANDLER LANDSCAPE END-->
<script>
    var jcrop_api;
    function featureGraphicFileSelectHandler() {
        var reqwidth = 1024;
        var reqheight = 500;
        var aspectRatio = reqwidth / reqheight;
        clearInfoFeatureGraphic();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarFeatureGraphicInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_feature_graphic").html('');
            $("#avatar_preview_div_feature_graphic").html('<img id="preview_content_img_feature_graphic"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_feature_graphic');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_feature_graphic").html('');
                    $("#avatar_preview_div_feature_graphic").html('<img id="preview_content_img_feature_graphic"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_feature_graphic').width(oImage.naturalWidth);
                $('#preview_content_img_feature_graphic').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_feature_graphic').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoFeatureGraphic,
                    onSelect: updateInfoFeatureGraphic,
                    onRelease: clearInfoFeatureGraphic
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<script>
    $('input').on('keyup', function(){
        if($(this).val().length >= 2) {
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().parent().removeClass('has-error');
        }
    });
    $('textarea').on('keyup', function(){
        if($(this).val().length >= 2) {
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().parent().removeClass('has-error');
        }
    });    
    
    var app_name = "<?php if(isset($appdata['app_name']) && trim($appdata['app_name'])){echo $appdata['app_name'];}?>";
    var short_desc = "<?php if(isset($appdata['short_description']) && trim($appdata['short_description'])){echo $appdata['short_description'];}?>";
    var dist_geo = "<?php if(isset($appdata['distribution_geography']) && trim($appdata['distribution_geography'])){echo $appdata['distribution_geography'];}?>";
    var rating = "<?php if(isset($appdata['rating']) && trim($appdata['rating'])){echo $appdata['rating'];}?>";
    var language = "<?php if(isset($appdata['language'])&& trim($appdata['language'])){echo $appdata['language'];}?>";
    var website = "<?php if(isset($appdata['website'])&& trim($appdata['website'])){echo $appdata['website'];}?>";
    var category = "<?php if(isset($appdata['category'])&& trim($appdata['category'])){echo $appdata['category'];}?>";
    
    var category_dd = "<?php if(isset($appdata['category-dd'])&& trim($appdata['category-dd'])){echo $appdata['category-dd'];}?>";

    var privacy_policy = "<?php if(isset($appdata['privacy_policy_url'])&& trim($appdata['privacy_policy_url'])){echo $appdata['privacy_policy_url'];}?>";
    var developer_username = "<?php if(isset($appdata['developer_username'])&& trim($appdata['developer_username'])){echo $appdata['developer_username'];}?>";
    var developer_password = "<?php if(isset($appdata['developer_password'])&& trim($appdata['developer_password'])){echo $appdata['developer_password'];}?>";
    
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };
    function appFormValidation() {
        var name = $("#app_name").val();
        var short_description = $("#short_description").val();
        var description = $("#description").val();
        var email = $("#email").val();
        //var phone = $("#phone").val();
        var category_dd = $("#category-dd").val();
        //var pp_url = $("#privacy_policy").val();
        if(!$.trim(name)){
            $("#app_name").parent().siblings().text('Please give a name for your app.');
         $("#app_name").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(name).length > 30 ){
         $("#app_name").parent().siblings().text('max limit 30 characters');   
         $("#app_name").parent().parent().parent().addClass('has-error');  
         return false;
        } 
        if(!$.trim(short_description)){
         $("#short_description").parent().siblings().text('Please give a short description for your app.');              
         $("#short_description").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(short_description).length > 80){
         $("#short_description").parent().siblings().text('max limit 80 characters');  
         $("#short_description").parent().parent().parent().addClass('has-error');  
         return false;
        }        
        if(!$.trim(description)){
         $("#description").parent().siblings().text('Please give a description for your app.');                 
         $("#description").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(description).length > 4000){
         $("#description").parent().siblings().text('max limit 4000 characters');     
         $("#description").parent().parent().parent().addClass('has-error');  
         return false;
        }
        /*if(!$.trim(pp_url)){
         $("#privacy_policy").parent().parent().parent().addClass('has-error');  
         return false;
        }*/
        if(!$('#app-icon-modal-btn').siblings().children('img').length){
            $("#app-icon-modal-btn").parent().parent().addClass('has-error');  
        return false;
        }
        if(!$('#splash-screen-modal-btn1').parent().parent().siblings().children().find('img').length){
         $("#splash-screen-modal-btn1").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if(!$('#splash-screen-modal-btn2').parent().parent().siblings().children().find('img').length){
         $("#splash-screen-modal-btn2").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if(!$('#transparent-icon-modal-btn').siblings().children('img').length){
            $("#transparent-icon-modal-btn").parent().parent().addClass('has-error');  
        return false;
        }        
        if(!$.trim(email) && !isValidEmailAddress(email)){
         $("#email").parent().parent().parent().addClass('has-error');  
         return false;
        }
        /*if(!$.trim(phone) && phone.length < 10){
         $("#phone").parent().parent().parent().addClass('has-error');  
         return false;
        }*/
      
        if(!$('#feature-graphic-modal-btn').siblings().children('img').length){
         $("#feature-graphic-modal-btn").parent().parent().addClass('has-error');  
         return false;
        }
         if(category_dd==''){
         $("#category-dd").parent().parent().parent().parent().addClass('has-error');  
         return false;
        }else{
          $("#category-dd").parent().parent().parent().parent().removeClass('has-error');    
        }                 
        return true;
    }
    
    
    $('#screen-modal-btn').click(function () {
        $("#screen-modal-box").modal('show');
    });
    
    $('#app-icon-modal-btn').click(function () {
        $("#app-icon-modal-box").modal('show');
    });
   
    $('#splash-screen-modal-btn1').click(function () {
        $("#splash-screen-modal-box1").modal('show');
    });
    $('#splash-screen-modal-btn2').click(function () {
        $("#splash-screen-modal-box2").modal('show');
    });
    
    $('#transparent-icon-modal-btn').click(function () {
        $("#transparent-icon-modal-box").modal('show');
    });    
    
    $('#feature-graphic-modal-btn').click(function () {
        $("#feature-graphic-modal-box").modal('show');
    });
    $('#developer_id_checkbox').click(function () {
        if ($(this).is(":checked")) {
            $('#developer_id').show();
        } else {
            $('#developer_id').hide();
        }
    });

    $('#app-icon-btn').click(function () {
        
        var oFile = $('#avatarInput')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }
            
        var data_from = $('#data-from').serialize();
        if($('#app-icon-modal-btn').siblings().children('img').length){
            
        }
        //alert ("qwerty");
        //return false;
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 1;
        <?php }?>
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            
            if (res) {
                //alert(res);
                $('#app-icon-from').submit();
            }
            
        });
    });

    $('#splash-screen-btn1').click(function () {
        
        var oFile = $('#avatarSplashInput')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }      
        
        var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 2;
        <?php }?>
            
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                $('#splash-screen-from1').submit();
            }
        });
    });
    
    $('#splash-screen-btn2').click(function () {
        var oFile = $('#avatarSplashInput2')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }       
        
        var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 3;
        <?php }?>
            
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                $('#splash-screen-from2').submit();
            }
        });
    });

    $('#transparent-icon-btn').click(function () {
        var oFile = $('#avatarInputTransparent')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }      
        var data_from = $('#data-from').serialize();
        if($('#transparent-icon-modal-btn').siblings().children('img').length){
            
        }
        
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 1;
        <?php }?>            
            
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                $('#transparent-icon-from').submit();
            }
        });
    });    

    $('#feature-graphic-btn').click(function () {
        var oFile = $('#avatarFeatureGraphicInput')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }           
        
        var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 4;
        <?php }?>
                
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                $('#feature-graphic-from').submit();
            }
        });
    });

    $('#tutorial-screen-btn').click(function () {
         var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 4;
        <?php }?>
            
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                if($('#tutorialScreenInput').val()!=""){
                    $('#tutorial-screen-from').submit();
                }
        }
        });
    });

    $('#data-from-btn').click(function () {
        var is_valid = appFormValidation();
        if(is_valid){
            <?php if (isset($appdata) && !empty($appdata)){ ?>
                 if($.trim(app_name)){
                $('#app_name').val(app_name).removeAttr("disabled");
                }       
                if($.trim(short_desc)){
                $('#short_description').val(short_desc).removeAttr("disabled");
                }
                
                <?php if(isset($appdata) && intval($appdata['is_update']) > 5){?>
                if($.trim(dist_geo)){
                    $("#distribution_geography").removeAttr("name");
                } 
                <?php }?>
                if($.trim(rating) && rating !== 'None-None-None-None-None-None-None-None-None-None'){
                    for(var i=1;i<=10;i++){
                        $("#rating-dd-"+i).removeAttr("name");
                    }
                } 
                if($.trim(language)){
                $('#language').val(language).removeAttr("disabled");
                }
                if($.trim(website)){
                $('#website').val(website).removeAttr("disabled");
                }
                
                $('#category').val(category);
                $('#category-dd').removeAttr("disabled");
                  // alert (1234); return false;
                if($.trim(privacy_policy)){
                $('#privacy_policy').val(privacy_policy).removeAttr("disabled");
                }
                
                if($.trim(developer_username) && $.trim(developer_password)){
                    $('#developer_id_checkbox').removeAttr("disabled");
                    $('#dev_uname').val(developer_username).removeAttr("disabled");
                    $('#dev_password').val(developer_password).removeAttr("disabled");
                }

            <?php }
            ?>
        var data_from = $('#data-from').serialize();
            var is_image = 5;
            $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1,'is_image': is_image, 'data_from': data_from}, function (res) {
            if (res) {
                $('#data-from-btn').html('Wait...');
                $('#data-from-btn').attr('disabled', 'disabled');
                $('#data-from-save').submit();
            }
        });
        }
       
    });

    $(document).ready(function () {
    
        var category = $('#category').val();
        $('#category-dd option[value="' + category + '"]').attr('selected', true);
        var rating_string = $('#rating').val();
        var k=1;
        $.each(rating_string.split('-'), function(i,e){
            $("#rating-dd-"+k+" option[value='" + e + "']").prop("selected", true);
            k++;
        });
        if($.trim(rating) && rating !== 'None-None-None-None-None-None-None-None-None-None'){
            for(var i=1;i<=10;i++){
                $("#rating-dd-"+i).attr('disabled', 'disabled');
            }
        }
        var language = $('#language').val();
        $('#language-dd option[value="' + language + '"]').attr('selected', true);

        var geography = "<?php if(isset($appdata['distribution_geography'])){echo $appdata['distribution_geography'];}?>";
        if (geography !== '') {
            $("#distribution_geography").attr('disabled',"disabled");
            $("#distribution_geography").children('option').each(function () {
                $(this).removeAttr('selected');
            });
        }

        $.each(geography.split(","), function (i, e) {
            $("#distribution_geography option[value='" + e + "']").prop("selected", true);
        });

        <?php
        if(isset($appdata) && intval($appdata['is_update']) < 5){ ?>
            $('input').removeAttr("disabled");
            $('select').removeAttr("disabled");
    <?php  }
    ?>

    });
    function openinmodal(url) {
        //$('.loaderDiv').show();   
        $.get(url, {"modalflag": 1}, function (data) {
            //$('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    (function () {
        $('body').on('click', '.left-icon', function (e) {
            // e.preventDefault();
            $(this).toggleClass('icon-arrow-up');
            $(this).parent().parent().parent().next().slideToggle(200);
        })
    })();
</script>
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 45%;top:20%;display: none;}
    .box.box-primary{border:none !important;}
    /*button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }*/
</style>