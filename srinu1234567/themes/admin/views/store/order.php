<style type="text/css">
    .dropbox{
        display:none;
    }
</style>
<?php
    $sort = "fa fa-sort";
    if (isset($sortBy)) {
        if ($sortBy == 'grand_total_desc') {
            $sort = "fa fa-sort-desc";
        } elseif ($sortBy == 'grand_total_asc') {
            $sort = "fa fa-sort-asc";
        }
        $sortby = $sortBy;
    }else{
        $sortby = 'grand_total_desc';
    }
?>
<?php $url = $this->createUrl('store/order'); ?>
<div class="row m-b-40">
    <div class="col-xs-4">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Export to CSV</button> 
    </div>
</div>
<div class="filter-div">
    <div class="row">
        <div class="col-md-9 m-b-10">
            <form id="searchform" action="<?php echo $url; ?>" name="searchForm" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group  input-group">
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <input type="text" class="filter_input form-control fg-input" id="search" placeholder="Enter order number,item name or customer name" value="<?php echo @$searchText; ?>" name="search">
                                <input type="hidden" name="sortBy" id="sortBy" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table" id="example">
            <thead>
                <?php if ($items_count >= 3) { ?>
                    <tr>
                        <td colspan="8" class="text-right">
                            <?php
                            $opts = array('class' => 'pagination m-t-0 m-b-40');
                            $this->widget('CLinkPager', array(
                                'currentPage' => $pages->getCurrentPage(),
                                'itemCount' => $items_count,
                                'pageSize' => $page_size,
                                "htmlOptions" => $opts,
                                'maxButtonCount' => 6,
                                'nextPageLabel' => '&raquo;',
                                'prevPageLabel' => '&laquo;',
                                'selectedPageCssClass' => 'active',
                                'lastPageLabel' => '',
                                'firstPageLabel' => '',
                                'header' => '',
                            ));
                            ?>

                        </td>
                    </tr>
                <?php }
                ?>
                <tr>
                    <th>Order Number</th>
                    <th>Date</th>
                    <th>Customer Name</th>
                    <th>Item Name</th>
                    <th data-hide="phone" id='grand_total' onclick="sortby('<?php echo $sortby;?>');" class="sortcolumn <?php if ($sortBy == 'grand_total_desc' || $sortBy == 'grand_total_asc') { ?> sortactive<?php } ?>">
                        <i class="<?php echo $sort; ?>"></i> 
                        Total Amount
                    </th>
                </tr>	
            </thead>
            <tbody>
                <?php
                $first = $cnt = 1;
                if ($data) {
                    $first = $cnt = (($pages->getCurrentPage()) * 3) + 1;
                    foreach ($data AS $key => $details) {
                        $cnt++;
                        ?>
                        <tr>
                            <td>
                                <a href=<?php echo Yii::app()->baseUrl; ?>"/store/orderdetails/id/<?php echo $details['id']; ?>">#<?php echo $details['order_number']; ?></a>
                                <?php
                                if($_SERVER['REMOTE_ADDR']=='111.93.166.194'){
                                    echo '<br />CDS-' . $details['cds_order_status'];
                                }
                                ?>
                            </td>
                            <td><?php echo $details['date'] ? date('jS M ,Y', strtotime($details['date'])) : 'NA'; ?></td>   
                            <td><?php echo $details['customer_name']; ?></td>
                            <td> 
                                <?php
                                foreach ($details['details'] as $key => $value) {
                                    if($details['cds_order_status']=='-1'){
                                        $revstatus = "Payment confirmation not received from Gateway";
                                    }else if($details['payment_status']=='Pending'){
                                        $revstatus = "Pending Status not updated ";
                                    }else{
                                        $revstatus = $status[$value['item_status']];
                                    }
                                    //$revstatus = ($details['cds_order_status']!='-1')?$status[$value['item_status']]:'Payment confirmation not received from Gateway';
                                    echo 'NAME-' . $value['name'] . '<br/>';
                                    echo 'PRICE- ' . Yii::app()->common->formatPrice($value['price'], $details['currency_id']) . '<br/>';
                                    echo 'QUANTITY-' . $value['quantity'] . '<br/>';
                                    echo 'STATUS- <b>' .$revstatus. '</b><br/>';
                                }
                                ?>  
                            </td>
                            <td> <?php echo Yii::app()->common->formatPrice($details['total_amount'], $details['currency_id']); ?>    </td>     
                        </tr>
                    <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="5">No Record found!!!</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    function sortby(sortby) {
        $('#sortBy').val(sortby);
        $('#searchform').submit();
    }
    $('.report-dd').click(function(){
        var searchText = $('#search').val();
        var type = $(this).val();
        if(type == 'csv'){
            /*window.location = '<?php echo Yii::app()->baseUrl."/store/exportOrder?type=";?>'+type+'&search_text='+searchText; */
            window.location = '<?php echo Yii::app()->baseUrl."/store/exportOrders";?>';
        }
    });
</script>