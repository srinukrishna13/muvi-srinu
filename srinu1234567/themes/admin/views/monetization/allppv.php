<style>
   .hide-now{
   display: none;
   }
   .diff-cirrency{
   border: 1px #d3d3d3 solid;
   padding: 10px;
   }
   .ppv-loader{
   text-align: center;
   width: 100%;
   position: absolute;
   height: 100%;
   top: 0;
   bottom: 0;z-index:10;
   }
   .ppv-loader-child{
   position: absolute;
   top: 0;
   bottom: 0;
   }
</style>
<div class="modal-dialog modal-lg">
   <form action="javascript:void(0);"  method="post" name="ppv_form" id="ppv_form" data-toggle="validator">
      <div class="modal-content">
         <div id="loader" style="display: none;" class="ppv-loader">
            <div class="preloader pls-blue ppv-loader-child">
               <svg viewBox="25 25 50 50" class="pl-circular">
                  <circle r="20" cy="50" cx="50" class="plc-path"/>
               </svg>
            </div>
         </div>
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?php if (isset($ppvPlans) && !empty($ppvPlans)) { ?>Edit<?php } else { ?>Add<?php } ?> All <?php if ($type == 3) { echo 'Multi-part Content';} else { echo 'Single-part Content';} ?> PPV</h4>
         </div>
         <div class="modal-body">
            <input type="hidden" name="data[id]" value="<?php echo $ppvPlans->id; ?>" />
            <input type="hidden" name="data[is_all]" value="1" />
            <input type="hidden" name="data[content_types_id]" value="<?php echo $type; ?>" />
            <?php if ($type == 3) { ?>
            <div id="multi_part_box">
               <?php if (isset($pricing) && !empty($pricing)) { 
                  $i = 1;
                  ?>
               <?php
                  foreach ($pricing as $key => $value) {
                      $show_unsubscribed = $value['show_unsubscribed'];
                      $season_unsubscribed = $value['season_unsubscribed'];
                      $episode_unsubscribed = $value['episode_unsubscribed'];
                  
                      $show_subscribed = $value['show_subscribed'];
                      $season_subscribed = $value['season_subscribed'];
                      $episode_subscribed = $value['episode_subscribed'];
                  
                      $symbol = $value['symbol'];
                      $pricing_id = $value['ppv_pricing_id'];
                      $currency_id = $value['currency_id'];
                      $code = $value['code'];
                      ?>
               <input type="hidden" name="data[pricing_id][]" value="<?php echo $pricing_id;?>" />
               <input type="hidden" name="data[status][]" value="<?php echo $studio->default_currency_id;?>" />
               
               <div class="diff-cirrency <?php echo  ($i > 1) ? 'm-t-10': '';?>">
                   <?php if($policyStatus == true){?>
                   <div class="row m-b-10">
                         <div class="col-sm-12">
                            Rules &nbsp;&nbsp;
                            <div class="fg-line">
                               <?php echo Yii::app()->Helper->rulesSelectBox($ppvPlans->rules_id, $policyStatus, $isPurchased);?>
                            </div>
                         </div>
                   </div>
                   <?php }?>
                  <div class="row">
                     <div class="col-sm-6">Price for Non-Subscribers</div>
                     <?php if($i > 1){?>
                     <div class="col-sm-6 text-right">
                        <div class="h4">
                           <a href="javascript:void(0);" data-type="single" onclick="$(this).parent().parent().parent().parent().remove();" class="text-black">
                           <em class="icon-trash"></em>&nbsp;Remove
                           </a>
                        </div>
                     </div>
                     <?php }?>
                  </div>
                  <div class="row">
                     <div class="col-sm-2">
                         <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                        <div class="checkbox m-b-15">
                           <label>
                           <input type="checkbox" class="hide-now" value="">
                           <i class="input-helper hide-now"></i>
                           Per Show
                           </label>
                        </div>
                         <?php }?>
                     </div>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-3">
                               <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                              <div class="fg-line">
                                 <input type="text" class="form-control input-sm cost multi-cost" name="data[show_unsubscribed][]" value="<?php echo $show_unsubscribed; ?>" autocomplete="off" />
                              </div>
                                <?php }?>
                           </div>
                            <div class="col-sm-5"></div>
                           <div class="col-sm-4 text-right">
                              <div class="fg-line">
                                 <div class="select">
                                    <select class="form-control input-sm currency" name="data[currency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                       <?php foreach ($currency as $key => $curr) { ?>
                                       <option value="<?php echo $curr['id'];?>" <?php if ($currency_id == $curr['id']) {?>selected="selected"<?php }?>><?php echo $curr['code'];?>(<?php echo $curr['symbol'];?>)</option>
                                       <?php }?>
                                    </select>
                           </div>
                        </div>
                     </div>
                  </div>
                     </div>
                  </div>
                 
                  <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="checkbox m-b-15">
                           <label>
                           <input type="checkbox" class="hide-now" value="">
                           <i class="input-helper hide-now"></i>
                           Per Season
                           </label>
                        </div>
                     </div>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="fg-line">
                                 <input type="text" class="form-control input-sm cost multi-cost" name="data[season_unsubscribed][]" value="<?php echo $season_unsubscribed; ?>" autocomplete="off" />
                              </div>
                           </div>
                            <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
                           <div class="col-sm-9">
                              <div class="checkbox m-b-15">
                                 <label>
                                 <input type="checkbox" class="multi-season-check-non-subscriber" name="data[multi_check_unsubscriber][]" value="" <?php if(isset($value['multi_season_pricing']) && !empty($value['multi_season_pricing'])){echo "checked";}?>>
                                 <i class="input-helper"></i>
                                 Set Different price for specific season
                                 </label>
                              </div>
                           </div>
                  <?php }?>
                        </div>
                          <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
                         
                        <div class="multi_part_season_box_non_subscriber" <?php if(intval($value['ppv_season_status']) && isset($value['multi_season_pricing']) && !empty($value['multi_season_pricing'])){ echo "style='display:block;'";}else{ echo "style='display:none;'";}?>>
                           <div class="row">
                              <div class="col-sm-3">
                                 Season Number
                              </div>
                              <div class="col-sm-3">
                                 Price
                              </div>
                              <div class="col-sm-6">
                                 <div class="h5 m-b-0" style="margin-top: 5px;">
                                    <input type="hidden" class="identifier" value="<?php echo $currency_id;?>" />
                                    <a href="javascript:void(0);" data-id = "<?php echo $currency_id;?>" onclick="addMore('multi', 0,'non_subscriber',this);" class="text-gray">
                                    <em class="icon-plus"></em>
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <?php if(isset($value['multi_season_pricing']) && !empty($value['multi_season_pricing'])){?>
                           <?php foreach ($value['multi_season_pricing'] as $multi_season_pricing) {
                              if(intval($multi_season_pricing['season_number_unsubscribed'])){
                              ?>
                           <input type="hidden" name="data[multi_season_pricing_id][]" value="<?php echo $multi_season_pricing['id'];?>" />
                           <div class="row">
                              <div class="col-sm-3">
                                 <div class="fg-line">
                                    <input class="form-control input-sm cost multi-cost sub-multi-cost season_number_unsubscriber" name="data[season_number_unsubscribed][<?php echo $currency_id;?>][]" value="<?php echo $multi_season_pricing['season_number_unsubscribed'];?>" autocomplete="off" type="number" min="1">
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="fg-line">
                                    <input class="form-control input-sm cost multi-cost sub-multi-cost" name="data[season_multi_unsubscribed][<?php echo $currency_id;?>][]" value="<?php echo $multi_season_pricing['season_unsubscribed'];?>" autocomplete="off" type="text">
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="h5">
                                    <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                                    <em class="icon-trash"></em>&nbsp;Remove
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <?php }}}?>
                        </div>
                  <?php }?>
                     </div>
                     <?php }?>
                  </div>
                  <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?> 
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="checkbox m-b-15">
                           <label>
                           <input type="checkbox" class="hide-now" value="">
                           <i class="input-helper hide-now"></i>
                           Per Episode
                           </label>
                        </div>
                     </div>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="fg-line">
                                 <input type="text" class="form-control input-sm cost multi-cost" name="data[episode_unsubscribed][]" value="<?php echo $episode_unsubscribed; ?>" autocomplete="off" />
                              </div>
                           </div>
                           <div class="col-sm-9 text-right">
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php }?> 
                
                  <?php if((isset($show_subscribed) && trim ($show_subscribed)) || (isset($season_subscribed) && trim ($season_subscribed)) || (isset($episode_subscribed) && trim ($episode_subscribed))){?>
                  <hr />
                  <div class="remove-mark">
                     <div class="row">
                        <div class="col-sm-12">Price for Subscribers</div>
                     </div>
                     
                     <div class="row">
                        <div class="col-sm-2">
                            <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                           <div class="checkbox m-b-15">
                              <label>
                              <input type="checkbox" class="hide-now" value="">
                              <i class="input-helper hide-now"></i>
                              Per Show
                              </label>
                           </div>
                            <?php }?>
                        </div>
                        <div class="col-sm-10">
                           <div class="row">
                              <div class="col-sm-3">
                                  <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                 <div class="fg-line">
                                    <input class="form-control input-sm cost multi-cost" name="data[show_subscribed][]" value="<?php echo $show_subscribed; ?>" autocomplete="off" type="text">
                                 </div>
                                  <?php }?>
                              </div>
                              <div class="col-sm-9 text-right">
                                 <div class="h5">
                                    <a href="javascript:void(0);" data-type="single" onclick="removeSubscriberBox(this);" class="text-black">
                                    <em class="icon-trash"></em>&nbsp;Remove
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                     <div class="row">
                        <div class="col-sm-2">
                           <div class="checkbox m-b-15">
                              <label>
                              <input type="checkbox" class="hide-now" value="">
                              <i class="input-helper hide-now"></i>
                              Per Season
                              </label>
                           </div>
                        </div>
                        <div class="col-sm-10">
                           <div class="row">
                              <div class="col-sm-3">
                                 <div class="fg-line">
                                    <input class="form-control input-sm cost multi-cost" name="data[season_subscribed][]" value="<?php echo $season_subscribed; ?>" autocomplete="off" type="text">
                                 </div>
                              </div>
                                <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
                              <div class="col-sm-9">
                                 <div class="checkbox m-b-15">
                                    <label>
                                    <input type="checkbox" class="multi-season-check-subscriber" name="data[multi_check_subscriber][]" value="" <?php if(isset($value['multi_season_pricing']) && !empty($value['multi_season_pricing'])){echo "checked";}?>>
                                    <i class="input-helper"></i>
                                    Set Different price for specific season
                                    </label>
                                 </div>
                              </div>
                                <?php }?>
                           </div>
                             <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
                           <div class="multi_part_season_box_subscriber" <?php if(intval($value['ppv_season_status']) && isset($value['multi_season_pricing']) && !empty($value['multi_season_pricing'])){ echo "style='display:block;'";}else{ echo "style='display:none;'";}?>>
                              <div class="row">
                                 <div class="col-sm-3">
                                    Season Number
                                 </div>
                                 <div class="col-sm-3">
                                    Price
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="h5 m-b-0" style="margin-top: 5px;">
                                       <input type="hidden" class="identifier" value="<?php echo $currency_id;?>" />
                                       <a href="javascript:void(0);" data-id = "<?php echo $currency_id;?>" onclick="addMore('multi', 0,'subscriber', this);" class="text-gray">
                                       <em class="icon-plus"></em>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <?php if(isset($value['multi_season_pricing']) && !empty($value['multi_season_pricing'])){?>
                              <?php foreach ($value['multi_season_pricing'] as $multi_season_pricing) {
                                 if(intval($multi_season_pricing['season_number_subscribed'])){
                                 ?>
                              <div class="row">
                                 <div class="col-sm-3">
                                    <div class="fg-line">
                                       <input class="form-control input-sm cost multi-cost sub-multi-cost season_number_subscriber" name="data[season_number_subscribed][<?php echo $currency_id;?>][]" value="<?php echo $multi_season_pricing['season_number_subscribed'];?>" autocomplete="off" type="number" min="1">
                                    </div>
                                 </div>
                                 <div class="col-sm-3">
                                    <div class="fg-line">
                                       <input class="form-control input-sm cost multi-cost sub-multi-cost" name="data[season_multi_subscribed][<?php echo $currency_id;?>][]" value="<?php echo $multi_season_pricing['season_subscribed'];?>" autocomplete="off" type="text">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="h5">
                                       <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                                       <em class="icon-trash"></em>&nbsp;Remove
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <?php }}}?>
                           </div>
                             <?php }?>
                        </div>
                        <?php } ?>
                     </div>
                     <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>                      
                     <div class="row">
                        <div class="col-sm-2">
                           <div class="checkbox m-b-15">
                              <label>
                              <input type="checkbox" class="hide-now" value="">
                              <i class="input-helper hide-now"></i>
                              Per Episode
                              </label>
                           </div>
                        </div>
                        <div class="col-sm-10">
                           <div class="row">
                              <div class="col-sm-3">
                                 <div class="fg-line">
                                    <input class="form-control input-sm cost multi-cost" name="data[episode_subscribed][]" value="<?php echo $episode_subscribed; ?>" autocomplete="off" type="text">
                                 </div>
                              </div>
                              <div class="col-sm-9 text-right">
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php }?> 
                  </div>
                  <?php } ?>
                  <a href="javascript:void(0);" onclick="addMore('multi', 0,'subscriber-box',this);" class="subscriber-box" class="text-gray"<?php if(isset($show_subscribed) && $show_subscribed){ echo "style='display:none'";}else{ echo "style='display:block'";}?>>
                  <em class="icon-plus"></em>
                  Add different price for subscribers
                  </a>
               </div>
               <?php $i++;} ?>
               <?php } else { ?>
               <input type="hidden" name="data[status][]" value="<?php echo $studio->default_currency_id;?>" />
               <div class="diff-cirrency currency-<?php echo $studio->default_currency_id;?>">
                   <?php if($policyStatus == true){?>
                   <div class="row m-b-10">
                         <div class="col-sm-12">
                            Rules &nbsp;&nbsp;
                            <div class="fg-line">
                               <?php echo Yii::app()->Helper->rulesSelectBox($ppvPlans->rules_id, $policyStatus, $isPurchased);?>
                            </div>
                         </div>
                   </div>
                   <?php }?>
                  <div class="row">
                     <div class="col-sm-6">Price for Non-Subscribers</div>
                  </div>
                  <div class="row">
                     <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                     <div class="col-sm-2">
                        <div class="checkbox m-b-15">
                           <label>
                           <input type="checkbox" class="hide-now" value="">
                           <i class="input-helper hide-now"></i>
                           Per Show
                           </label>
                        </div>
                     </div>
                     <?php }else{?>
                     <div class="col-sm-2"></div>
                     <?php }?>
                     <div class="col-sm-10">
                        <div class="row">
                           <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                           <div class="col-sm-3">
                              <div class="fg-line">
                                 <input class="form-control input-sm cost multi-cost" name="data[show_unsubscribed][]" value="" autocomplete="off" type="text">
                              </div>
                           </div>
                           <?php }else{?>
                           <div class="col-sm-3"></div>
                           <?php }?>
                           <div class="col-sm-5"></div>
                           <div class="col-sm-4 text-right">
                              <div class="fg-line">
                                 <div class="select">
                                    <select class="form-control input-sm currency" name="data[currency_id][]" disabled="disabled">
                                       <?php foreach ($currency as $key => $curr) { ?>
                                       <option value="<?php echo $curr['id'];?>" <?php if ($currency_id == $curr['id']) {?>selected="selected"<?php }?>><?php echo $curr['code'];?>(<?php echo $curr['symbol'];?>)</option>
                                       <?php }?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="checkbox m-b-15">
                           <label>
                           <input type="checkbox" class="hide-now" value="">
                           <i class="input-helper hide-now"></i>
                           Per Season
                           </label>
                        </div>
                     </div>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="fg-line">
                                 <input class="form-control input-sm cost multi-cost" name="data[season_unsubscribed][]" value="" autocomplete="off" type="text">
                              </div>
                           </div>
                             <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
                           <div class="col-sm-9">
                              <div class="checkbox m-b-15">
                                 <label>
                                 <input type="checkbox" class="multi-season-check-non-subscriber" name="data[multi_check_unsubscriber][]" value="">
                                 <i class="input-helper"></i>
                                 Set Different price for specific season
                                 </label>
                              </div>
                           </div>
                  <?php }?>
                        </div>
                          <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
                        <div class="multi_part_season_box_non_subscriber" style="display:none">
                           <div class="row">
                              <div class="col-sm-3">
                                 Season Number
                              </div>
                              <div class="col-sm-3">
                                 Price
                              </div>
                              <div class="col-sm-6">
                                 <div class="h5 m-b-0" style="margin-top: 5px;">
                                    <a href="javascript:void(0);" onclick="addMore('multi', 0,'non_subscriber',this);" class="text-gray">
                                    <em class="icon-plus"></em>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                  <?php }?>
                     </div>
                  </div>
                  <?php }?>
                  <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="checkbox m-b-15">
                           <label>
                           <input type="checkbox" class="hide-now" value="">
                           <i class="input-helper hide-now"></i>
                           Per Episode
                           </label>
                        </div>
                     </div>
                     <div class="col-sm-10">
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="fg-line">
                                 <input class="form-control input-sm cost multi-cost" name="data[episode_unsubscribed][]" value="" autocomplete="off" type="text">
                              </div>
                           </div>
                           <div class="col-sm-9 text-right">
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php }?>
                  <a href="javascript:void(0);" onclick="addMore('multi', 0,'subscriber-box',this);" class="subscriber-box" class="text-gray"<?php if(isset($show_subscribed) && $show_subscribed){ echo "style='display:none'";}else{ echo "style='display:block'";}?>>
                  <em class="icon-plus"></em>
                  Add different price for subscribers
                  </a>
                  <?php } ?>
               </div>
               <div class="m-t-20">
                  <a href="javascript:void(0);" onclick="addMore('multi', 0,'country', this);" class="text-gray">
                  <em class="icon-plus"></em>
                  Add more price for specific country
                  </a>
               </div>
               <?php } else { ?>
               <div id="single_part_box">
                  <?php if (isset($pricing) && !empty($pricing)) { ?>
                  <?php
                     foreach ($pricing as $key => $value) {
                         $price_for_unsubscribed = $value['price_for_unsubscribed'];
                         $price_for_subscribed = $value['price_for_subscribed'];
                     
                         $symbol = $value['symbol'];
                         $pricing_id = $value['ppv_pricing_id'];
                         $currency_id = $value['currency_id'];
                         $code = $value['code'];
                         ?>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="row">
                           <div class="col-md-4">
                              <div class="checkbox">
                                 <label>
                                 <input type="checkbox" class="singlechk" name="data[status][]" value="<?php echo $currency_id;?>" <?php if (isset($value['status']) && intval($value['status'])) { ?>checked="checked" <?php } ?> <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true" checked="checked"<?php }?> /><i class="input-helper"></i>
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-8 p-l-0">
                              <input type="hidden" name="data[pricing_id][]" value="<?php echo $pricing_id;?>" />
                              <div class="fg-line">
                                 <div class="select">
                                    <select class="form-control input-sm currency" name="data[currency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                       <?php foreach ($currency as $key => $curr) { ?>
                                       <option value="<?php echo $curr['id'];?>" <?php if ($currency_id == $curr['id']) {?>selected="selected"<?php }?>><?php echo $curr['code'];?>(<?php echo $curr['symbol'];?>)</option>
                                       <?php }?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-10">
                         <?php if($policyStatus == true){?>
                         
                        <div class="row m-b-10">
                           <div class="col-sm-12">
                                   Rules &nbsp;&nbsp;
                                   <div class="fg-line">
                                      <?php echo Yii::app()->Helper->rulesSelectBox($ppvPlans->rules_id, $policyStatus, $isPurchased);?>
                                   </div>
                                </div>
                             </div>
                            <?php }?>
                        <div class="row m-b-10">
                           <div class="col-sm-12">
                              Non-subscribers &nbsp;&nbsp;
                              <div class="fg-line">
                                 <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_unsubscribed][]" value="<?php echo $price_for_unsubscribed; ?>" autocomplete="off" />
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              Subscribers &nbsp;&nbsp;
                              <div class="fg-line">
                                 <input type="text" class="form-control cost single-cost" name="data[price_for_subscribed][]" value="<?php echo $price_for_subscribed; ?>" autocomplete="off" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <hr/>
                  <?php } ?>
                  <?php } else { ?>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="row">
                           <div class="col-md-4">
                              <div class="checkbox">
                                 <label>
                                 <input type="checkbox" class="singlechk" name="data[status][]" checked="checked" disabled="disabled" value="<?php echo $studio->default_currency_id;?>" /><i class="input-helper"></i>
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-8 p-l-0">
                              <input type="hidden" name="data[pricing_id][]" value="" />
                              <div class="fg-line">
                                 <div class="select">
                                    <select class="form-control currency" name="data[currency_id][]" disabled="disabled">
                                       <?php foreach ($currency as $key => $curr) { ?>
                                       <option value="<?php echo $curr['id']; ?>" <?php if ($studio->default_currency_id == $curr['id']) {?>selected="selected"<?php } ?>><?php echo $curr['code']; ?>(<?php echo $curr['symbol']; ?>)</option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-10">
                         <?php if($policyStatus == true){?>
                        <div class="row m-b-10">
                           <div class="col-sm-12">
                                     Rules &nbsp;&nbsp;
                                     <div class="fg-line">
                                        <?php echo Yii::app()->Helper->rulesSelectBox($ppvPlans->rules_id, $policyStatus, $isPurchased);?>
                                     </div>
                                  </div>
                            </div>
                          <?php }?>
                        <div class="row m-b-10">
                           <div class="col-sm-12">
                              Non-subscribers &nbsp;&nbsp;
                              <div class="fg-line">
                                 <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_unsubscribed][]" value="" autocomplete="off" />
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              Subscribers &nbsp;&nbsp;
                              <div class="fg-line">
                                 <input type="text" class="form-control cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <hr/>
                  <?php } ?>
               </div>
               <a href="javascript:void(0);" onclick="addMore('single', 0,'single', this);" class="text-gray">
               <em class="icon-plus"></em>
               Add more price for specific country
               </a>
               <?php } ?>
            </div>
            <div class="modal-footer">
               <input type="hidden" name="data[status]" value=""  id="status_id" />
               <button type="button" class="btn btn-primary" <?php if ($type == 3) { ?> onclick="return validateMultiPPVForm();" <?php } else { ?> onclick="return validateSinglePPVForm();" <?php } ?> id="bill-btn"><?php if (isset($pricing) && !empty($pricing)) { ?> Update<?php } else { ?>Add<?php } ?></button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="single-data" style="display: none;">
   <div class="row">
      <div class="col-sm-2">
         <div class="row">
            <div class="col-md-4">
               <div class="checkbox">
                  <label>
                  <input type="checkbox" class="singlechk" name="data[status][]" value="<?php echo $currency[0]->id; ?>" /><i class="input-helper"></i>
                  </label>
               </div>
            </div>
            <div class="col-md-8 p-l-0">
               <input type="hidden" name="data[pricing_id][]" value="" />
               <div class="fg-line">
                  <div class="select">
                     <select class="form-control input-sm currency" name="data[currency_id][]">
                        <?php foreach ($currency as $key => $curr) { ?>
                        <option value="<?php echo $curr['id']; ?>"><?php echo $curr['code']; ?>(<?php echo $curr['symbol']; ?>)</option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
         <div class="row m-b-10">
            <div class="col-sm-12">
               Non-subscribers &nbsp;&nbsp;
               <div class="fg-line">
                  <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_unsubscribed][]" value="" autocomplete="off" />
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               Subscribers &nbsp;&nbsp;
               <div class="fg-line">
                  <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
               </div>
            </div>
         </div>
      </div>
      <div class="row" >
         <div class="col-md-12 m-t-20 text-right ">
            <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
            <em class="icon-trash" ></em>&nbsp; Remove
            </a>
         </div>
      </div>
   </div>
   <hr/>
</div>
<div id="multi-data-box-unsubscriber" style="display: none;">
   <div class="row">
      <div class="col-sm-3">
         <div class="fg-line">
            <input class="form-control input-sm cost multi-cost sub-multi-cost season_number_unsubscriber" name="data[season_number_unsubscribed][]" value="" autocomplete="off" type="number" min="1">
         </div>
      </div>
      <div class="col-sm-3">
         <div class="fg-line">
            <input class="form-control input-sm cost multi-cost sub-multi-cost" name="data[season_multi_unsubscribed][]" value="" autocomplete="off" type="text">
         </div>
      </div>
      <div class="col-sm-6">
         <div class="h5">
            <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
            <em class="icon-trash"></em>&nbsp;Remove
            </a>
         </div>
      </div>
   </div>
</div>
<div id="multi-data-box-subscriber" style="display: none;">
   <div class="row">
      <div class="col-sm-3">
         <div class="fg-line">
            <input class="form-control input-sm cost multi-cost sub-multi-cost season_number_subscriber" name="data[season_number_subscribed][]" value="" autocomplete="off" type="number" min="1">
         </div>
      </div>
      <div class="col-sm-3">
         <div class="fg-line">
            <input class="form-control input-sm cost multi-cost sub-multi-cost" name="data[season_multi_subscribed][]" value="" autocomplete="off" type="text">
         </div>
      </div>
      <div class="col-sm-6">
         <div class="h5">
            <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
            <em class="icon-trash"></em>&nbsp;Remove
            </a>
         </div>
      </div>
   </div>
</div>
<div id="multi-data-unsubscriber" style="display:none">
   <div class="diff-cirrency m-t-10">
      <div class="row">
         <div class="col-sm-6">Price for Non-Subscribers</div>
         <div class="col-sm-6 text-right">
            <div class="h4">
               <a href="javascript:void(0);" data-type="single" onclick="$(this).parent().parent().parent().parent().remove();" class="text-black">
               <em class="icon-trash"></em>&nbsp;Remove
               </a>
            </div>
         </div>
      </div>
      <div class="row">
         <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
         <div class="col-sm-2">
            <div class="checkbox m-b-15">
               <label>
               <input type="checkbox" class="hide-now" value="">
               <i class="input-helper hide-now"></i>
               Per Show
               </label>
            </div>
         </div>
         <?php }else{?>
         <div class="col-sm-2"></div>
         <?php }?>
         <div class="col-sm-10">
            <div class="row">
               <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
               <div class="col-sm-3">
                  <div class="fg-line">
                     <input class="form-control input-sm cost multi-cost" name="data[show_unsubscribed][]" value="" autocomplete="off" type="text">
                  </div>
               </div>
               <?php }else{?>
               <div class="col-sm-3"></div>
               <?php }?>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <div class="select">
                     <input type="hidden" class="identifier" value="" />
                     <select class="form-control input-sm currency" name="data[currency_id][]">
                        <?php foreach ($currency as $key => $curr) { ?>
                        <option value="<?php echo $curr['id']; ?>"><?php echo $curr['code']; ?>(<?php echo $curr['symbol']; ?>)</option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
      <div class="row">
         <div class="col-sm-2">
            <div class="checkbox m-b-15">
               <label>
               <input type="checkbox" class="hide-now" value="">
               <i class="input-helper hide-now"></i>
               Per Season
               </label>
            </div>
         </div>
         <div class="col-sm-10">
            <div class="row">
               <div class="col-sm-3">
                  <div class="fg-line">
                     <input class="form-control input-sm cost multi-cost" name="data[season_unsubscribed][]" value="" autocomplete="off" type="text">
                  </div>
               </div>
                 <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
               <div class="col-sm-9">
                  <div class="checkbox m-b-15">
                     <label>
                     <input type="checkbox" class="multi-season-check-non-subscriber" name="data[multi_check_unsubscriber][]" value="" checked>
                     <i class="input-helper"></i>
                     Set Different price for specific season
                     </label>
                  </div>
               </div>
      <?php }?>
            </div>
              <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
            <div class="multi_part_season_box_non_subscriber">
               <div class="row">
                  <div class="col-sm-3">
                     Season Number
                  </div>
                  <div class="col-sm-3">
                     Price
                  </div>
                  <div class="col-sm-6">
                     <div class="h5 m-b-0" style="margin-top: 5px;">
                        <a href="javascript:void(0);" onclick="addMore('multi', 0,'non_subscriber',this);" class="text-gray">
                        <em class="icon-plus"></em>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
      <?php }?>
         </div>
      </div>
      <?php }?>
      <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
      <div class="row">
         <div class="col-sm-2">
            <div class="checkbox m-b-15">
               <label>
               <input type="checkbox" class="hide-now" value="">
               <i class="input-helper hide-now"></i>
               Per Episode
               </label>
            </div>
         </div>
         <div class="col-sm-10">
            <div class="row">
               <div class="col-sm-3">
                  <div class="fg-line">
                     <input class="form-control input-sm cost multi-cost" name="data[episode_unsubscribed][]" value="" autocomplete="off" type="text">
                  </div>
               </div>
               <div class="col-sm-9 text-right">
               </div>
            </div>
         </div>
      </div>
      <?php }?>
      <a href="javascript:void(0);" onclick="addMore('multi', 0,'subscriber-box',this);" class="subscriber-box" class="text-gray">
      <em class="icon-plus"></em>
      Add different price for subscribers
      </a>
   </div>
</div>
<div id="multi-data-subscriber" style="display:none">
   <hr />
   <div class="remove-mark">
      <div class="row">
         <div class="col-sm-12">Price for Subscribers</div>
      </div>
      <div class="row">
         <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
         <div class="col-sm-2">
            <div class="checkbox m-b-15">
               <label>
               <input type="checkbox" class="hide-now" value="">
               <i class="input-helper hide-now"></i>
               Per Show
               </label>
            </div>
         </div>
         <?php }else{?>
         <div class="col-sm-2"></div>
         <?php }?>
         <div class="col-sm-10">
            <div class="row">
               <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
               <div class="col-sm-3">
                  <div class="fg-line">
                     <input class="form-control input-sm cost multi-cost" name="data[show_subscribed][]" value="" autocomplete="off" type="text">
                  </div>
               </div>
               <?php }else{?>
               <div class="col-sm-3"></div>
               <?php }?>
               <div class="col-sm-9 text-right">
                  <div class="h5">
                     <a href="javascript:void(0);" data-type="single" onclick="removeSubscriberBox(this);" class="text-black">
                     <em class="icon-trash"></em>&nbsp;Remove
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
      <div class="row">
         <div class="col-sm-2">
            <div class="checkbox m-b-15">
               <label>
               <input type="checkbox" class="hide-now" value="">
               <i class="input-helper hide-now"></i>
               Per Season
               </label>
            </div>
         </div>
         <div class="col-sm-10">
            <div class="row">
               <div class="col-sm-3">
                  <div class="fg-line">
                     <input class="form-control input-sm cost multi-cost" name="data[season_subscribed][]" value="" autocomplete="off" type="text">
                  </div>
               </div>
                 <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
               <div class="col-sm-9">
                  <div class="checkbox m-b-15">
                     <label>
                     <input type="checkbox" class="multi-season-check-subscriber" name="data[multi_check_subscriber][]" value="">
                     <i class="input-helper"></i>
                     Set Different price for specific season
                     </label>
                  </div>
               </div>
      <?php }?>
            </div>
              <?php if(isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season)){?>
            <div class="multi_part_season_box_subscriber" style="display:none">
               <div class="row">
                  <div class="col-sm-3">
                     Season Number
                  </div>
                  <div class="col-sm-3">
                     Price
                  </div>
                  <div class="col-sm-6">
                     <div class="h5 m-b-0" style="margin-top: 5px;">
                        <a href="javascript:void(0);" onclick="addMore('multi', 0,'subscriber', this);" class="text-gray">
                        <em class="icon-plus"></em>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
      <?php }?>
         </div>
      </div>
      <?php }?>
      <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
      <div class="row">
         <div class="col-sm-2">
            <div class="checkbox m-b-15">
               <label>
               <input type="checkbox" class="hide-now" value="">
               <i class="input-helper hide-now"></i>
               Per Episode
               </label>
            </div>
         </div>
         <div class="col-sm-10">
            <div class="row">
               <div class="col-sm-3">
                  <div class="fg-line">
                     <input class="form-control input-sm cost multi-cost" name="data[episode_subscribed][]" value="" autocomplete="off" type="text">
                  </div>
               </div>
               <div class="col-sm-9 text-right">
               </div>
            </div>
         </div>
      </div>
      <?php }?>
   </div>
</div>
</div>
<script type="text/javascript">
   Array.prototype.getUnique = function () {
       var u = {}, a = [];
       for (var i = 0, l = this.length; i < l; ++i) {
           if (u.hasOwnProperty(this[i])) {
               continue;
           }
           a.push(this[i]);
           u[this[i]] = 1;
       }
       return a;
   }
   var subscriber_cnt = 1;
   
   
     function cleanArray(actual) {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++) {
          if (actual[i]) {
            newArray.push(actual[i]);
          }
        }
        return newArray;
      }
   function addMore(type, flag, addType, that) {
     
       var isTrue = 0;
       $("#" + type + "_part_box").find("." + type + "-cost").each(function () {
           if ($.trim($(this).val()) !== '') {
               isTrue = 1;
           } else {
               isTrue = 0;
               $(this).focus();
               return false;
           }
       });
       
       if($.trim(addType) === 'non_subscriber'){
           var season_number_unsubscriber = [];
           $(that).parent().parent().parent().siblings().each(function(){
                season_number_unsubscriber.push($(this).find('.season_number_unsubscriber').val());
           });
           season_number_unsubscriber = cleanArray(season_number_unsubscriber);
            if((new Set(season_number_unsubscriber)).size !== season_number_unsubscriber.length){
                swal("Season number should be unique");
                isTrue = 0; 
                $(this).focus();
                return false;
            } 
       }else if($.trim(addType) === 'subscriber'){
           var season_number_subscriber = [];
           $(that).parent().parent().parent().siblings().each(function(){
                season_number_subscriber.push($(this).find('.season_number_subscriber').val());
           });
           season_number_subscriber = cleanArray(season_number_subscriber);
            if((new Set(season_number_subscriber)).size !== season_number_subscriber.length){
                swal("Season number should be unique");
                isTrue = 0; 
                $(this).focus();
                return false;
            }
       }else if($.trim(addType) === 'subscriber-box') {
           var season_number_unsubscriber = [];
            $(that).siblings().children().children('.multi_part_season_box_non_subscriber').children().each(function(){
                 season_number_unsubscriber.push($(this).find('.season_number_unsubscriber').val());
            });
           season_number_unsubscriber = cleanArray(season_number_unsubscriber);
            if((new Set(season_number_unsubscriber)).size !== season_number_unsubscriber.length){
                swal("Season number should be unique");
                isTrue = 0; 
                $(this).focus();
                return false;
            }
       }else if($.trim(addType) === 'country') {
            $(that).parent().siblings('.diff-cirrency').each(function(){
                var season_number_unsubscriber = [];
                $(this).children().children().children('.multi_part_season_box_non_subscriber').children().each(function(){
                    season_number_unsubscriber.push($(this).find('.season_number_unsubscriber').val());
                });
                season_number_unsubscriber = cleanArray(season_number_unsubscriber);
                if((new Set(season_number_unsubscriber)).size !== season_number_unsubscriber.length){
                    swal("Season number should be unique");
                    isTrue = 0; 
                    $(this).focus();
                    return false;
                }
                var season_number_subscriber = [];
                $(this).children('.remove-mark').children().children().children('.multi_part_season_box_subscriber').children().each(function(){
                    season_number_subscriber.push($(this).find('.season_number_subscriber').val());
                });
                season_number_subscriber = cleanArray(season_number_subscriber);
                if((new Set(season_number_subscriber)).size !== season_number_subscriber.length){
                    swal("Season number should be unique");
                    isTrue = 0; 
                    $(this).focus();
                    return false;
                }
           });
       }
       
       if (parseInt(flag)) {
           isTrue = 1;
       }
       if (parseInt(isTrue)) {
           if($.trim(addType) === 'single' || $.trim(addType) === 'subscriber' || $.trim(addType) === 'non_subscriber' || $.trim(addType) === 'country'){
           var str = '';
               if (type === 'single') {
                   str = $("#single-data").html();
               } else if (type === 'multi') {
                   if($.trim(addType) === 'subscriber'){
                       var identifier = $(that).siblings('.identifier').val();
                        $("#multi-data-box-subscriber").find('input').each(function(){
                            var tempName = $(this).attr('name');
                            var tempName = tempName.replace(/[^a-z_\d\s]+/gi, "");
                            var tempName = tempName.replace(/data/g, "");
                            var _name = tempName.replace(/undefined/g, "");
                            _name = _name.replace(/[0-9]/g, '');
                            $(this).attr('name','data[' + _name + '][' + identifier + '][]');
                        });
                       str = $("#multi-data-box-subscriber").html();
                       
                   }else if ($.trim(addType) === 'non_subscriber'){
                       var identifier = $(that).siblings('.identifier').val();
                        $("#multi-data-box-unsubscriber").find('input').each(function(){
                            var tempName = $(this).attr('name');
                            var tempName = tempName.replace(/[^a-z_\d\s]+/gi, "");
                            var tempName = tempName.replace(/data/g, "");
                            var _name = tempName.replace(/undefined/g, "");
                            _name = _name.replace(/[0-9]/g, '');
                            $(this).attr('name','data[' + _name + '][' + identifier + '][]');
                            
                        });
                       str = $("#multi-data-box-unsubscriber").html();
                   }else if ($.trim(addType) === 'country'){
                       var str = $('#multi-data-unsubscriber').html();
                       $(str).insertAfter($('#multi_part_box').children('.diff-cirrency').last());
                       return false;
                   }
                   
               }
               if(type === 'single'){
                   $("#" + type + "_part_box").append(str);
               }else{
                   $(that).parent().parent().parent().parent("." + type + "_part_season_box_" + addType).append(str);
               }
               
               $('.cost').on("keypress", function (event) {
                   return decimalsonly(event);
               });
               $('.cost').on("contextmenu", function (event) {
                   return false;
               });
           }else{
               if($.trim(addType) === 'subscriber-box'){
               var subscriber_str = '';
               subscriber_str = $("#multi-data-subscriber").html();
               
               $(that).parent('.diff-cirrency').append(subscriber_str);
               $('.cost').on("keypress", function (event) {
                   return decimalsonly(event);
               });
               $('.cost').on("contextmenu", function (event) {
                   return false;
               });
               $(that).toggle();
               }else{
                   return false;
               }
               
           }
           
       }
   }
   
   function removeBox(obj) {
       $(obj).parent().parent().parent().next('hr').remove();
       $(obj).parent().parent().parent().remove();
   }
   function removeSubscriberBox(obj,identifier) {
       $(obj).parent().parent().parent().parent().parent().parent().siblings().closest('.subscriber-box').show();
       subscriber_cnt = 1;
       $(obj).parent().parent().parent().parent().parent().parent().siblings('hr').remove();
       $(obj).parent().parent().parent().parent().parent().parent().remove();
   }
   
   function validateSinglePPVForm() {
       if ($("#single_part_box").length) {
           var currency = new Array();
           var isTrue = 1;
   
           $("#single_part_box").find(".singlechk").each(function () {
               if ($(this).is(':checked')) {
                   currency.push($(this).parent().parent().parent().next('div').find('select').val());
                   $(this).parent().parent().parent().parent().parent().parent().find('.single-cost').each(function () {
                       if ($.trim($(this).val()) === '') {
                           isTrue = 0;
                           $(this).focus();
                           swal("Please set price for selected currency");
                           return false;
                       }
                   });
               }
           });
           
           if (!parseInt(isTrue)) {
               return false;
           }
   
           var x = currency.getUnique();
           if (x.length === currency.length) {
               $("#status_id").val(x.toString());
   
               $(".singlechk").each(function () {
                   $(this).prop("disabled", false);
               });
   
               $(".currency").each(function () {
                   $(this).prop("disabled", false);
               });
   
               var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/addEditSinglePPV";
               document.ppv_form.action = url;
               document.ppv_form.submit();
           } else {
               swal("Currency should be unique");
               return false;
           }
       }
   }
   
 
   
   function checkDupSeason() {
       var x=1;
        var y=1;
    $('.diff-cirrency').each(function(){
                    var season_number_unsubscriber = [];
                    $(this).children().children().children('.multi_part_season_box_non_subscriber').children().each(function(){
                        season_number_unsubscriber.push($(this).find('.season_number_unsubscriber').val());
                    });
                    season_number_unsubscriber = cleanArray(season_number_unsubscriber);
                    if((new Set(season_number_unsubscriber)).size !== season_number_unsubscriber.length){
                        x = 0;
                        swal("Season number should be unique");
                        $('#loader').hide();
                        return false;
                    }else{
                        x = 1;
                    }
                    var season_number_subscriber = [];
                    $(this).children('.remove-mark').children().children().children('.multi_part_season_box_subscriber').children().each(function(){
                        season_number_subscriber.push($(this).find('.season_number_subscriber').val());
                    });
                    season_number_subscriber = cleanArray(season_number_subscriber);
                    if((new Set(season_number_subscriber)).size !== season_number_subscriber.length){
                        y=0;
                        swal("Season number should be unique");
                        $('#loader').hide();
                        return false;
                    }else{
                        y=1
                    }
               });
               
               if(x && y){
                    return true;   
                }else{
                    swal("Season number should be unique");
                     $('#loader').hide();
                     return false;
                }
   }
   function checkDupCurr(){
       var a = 1;
        var currDup = [];
        $(".diff-cirrency").each(function(){
            if($(this).is(':visible')){
                var curr = $(this).find('.currency').val();
                currDup.push(curr);
            }
        });
        currDup = cleanArray(currDup);
        if((new Set(currDup)).size !== currDup.length){
            a = 0;
        }else{
            a=1;
        }
        if(a){
            return true;   
        }else{
            swal("Currency should be unique");
             $('#loader').hide();
             return false;
        }
   }
   
   function validateMultiPPVForm() {
       if ($("#multi_part_box").length) {
           $('#loader').show();
           var currency = new Array();
           var isTrue = 1;
           if($.trim($("#multi_part_box").find(".multi-cost").val()) === ''){
                $('#loader').hide();
                swal("Please enter amount");
                return false;
           }
           $("#multi_part_box").find(".multichk").each(function () {
   
               if ($(this).is(':checked')) {
                   currency.push($(this).parent().parent().parent().next('div').find('select').val());
                   $(this).parent().parent().parent().parent().parent().parent().find('.multi-cost').each(function () { 
                       if ($.trim($(this).val()) === '') {
                           isTrue = 0;
                           $(this).focus();
                           swal("Please set price for selected currency");
                           return false;
                       }
                   });
               }
           });
   
           if (!parseInt(isTrue)) {
               $('#loader').hide();
               return false;
           }
   
           var x = currency.getUnique();
          
           if (x.length === currency.length) {
               $("#status_id").val(x.toString());
   
               $(".multichk").each(function () {
                   $(this).prop("disabled", false);
               });
   
               $(".currency").each(function () {
                   $(this).prop("disabled", false);
               });
               var currArray = [];
               $(".currency").each(function(){
                   if($.inArray($(this).val(), currArray) === -1){
                       currArray.push($(this).val());
                   }
                });
               
                $.each(currArray, function( index, value ) {
                    $('.currency-' + value).find('.sub-multi-cost').each(function(){
                        var tempName = $(this).attr('name');
                        var tempName = tempName.replace(/[^a-z_\d\s]+/gi, "");
                        var tempName = tempName.replace(/data/g, "");
                        var _name = tempName.replace(/undefined/g, "");
                        _name = _name.replace(/[0-9]/g, '');
                       $(this).attr('name','data[' + _name + '][' + value + '][]');
                       
                   });
                });
                
                $("#multi_part_box").find(".multi-cost").each(function () {
                    if ($.trim($(this).val()) !== '') {
                        isTrue = 1;
                    } else {
                        isTrue = 0;
                        $('#loader').hide();
                        $(this).focus();
                        return false;
                    }
                });
                
                if(checkDupSeason() && checkDupCurr()){
                   setTimeout(function(){ 
                        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/addEditMultiPPV";
                        document.ppv_form.action = url;
                        document.ppv_form.submit();
                    },10000);
               }
               
           } else {
               swal("Currency should be unique");
               return false;
           }
       }
   }
   
   $(document).on('click', '.multi-season-check-subscriber', function(){
       $(this).parent().parent().parent().parent().siblings('.multi_part_season_box_subscriber').toggle();
   });
   $(document).on('click', '.multi-season-check-non-subscriber', function(){
       $(this).parent().parent().parent().parent().siblings('.multi_part_season_box_non_subscriber').toggle();
   });
   
    $(document).on('change','.currency', function(){
        $(this).siblings('.identifier').val($(this).val());
        $(this).parent().parent().parent().parent().parent().parent().addClass('currency-' + $(this).val());
    });
</script>