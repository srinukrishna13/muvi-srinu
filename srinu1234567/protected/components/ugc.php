<?php /**
 * Ugc component is a separated component for the user generated componenets
 * @author suraja<suraja@muvi.com>
 */
class ugc extends AppComponent {
    
   function getCustomPosterSize($custom_form_id) {
        $posters = CustomMetadataForm::model()->find(array('select' => 'poster_size', 'condition' => 'id=:id', 'params' => array(':id' => $custom_form_id)));
        $dmn = $posters['poster_size'];
        if ($dmn) {
            $expl = explode('x', strtolower($dmn));
            $poster['vwidth'] = @$expl[0];
            $poster['vheight'] = @$expl[1];
            $poster['hwidth'] = @$expl[0];
            $poster['hheight'] = @$expl[1];
        } else {
            $poster_sizes = Yii::app()->Controller->poster_sizes;
            $horizontal = $poster_sizes['horizontal'];
            $vertical = $poster_sizes['vertical'];
            $poster['vwidth'] = $vertical['width'];
            $poster['vheight'] = $vertical['height'];
            $poster['hwidth'] = $horizontal['width'];
            $poster['hheight'] = $horizontal['height'];
        }
        return $poster;
    }
    
    public function saveUgc($custom_metadata_form_id,$data)
    {
        $Films = new Film();       
        $cmfid = CustomMetadataForm::model()->find(array('select' => 'parent_content_type_id,content_type,is_child', 'condition' => 'id=:id', 'params' => array(':id' => $custom_metadata_form_id)));
        if (!isset($data['content_types_id']) || empty($data['content_types_id'])) {
            $data['content_types_id'] = Yii::app()->general->getcontents_types_id($cmfid);
        }
        if (!isset($data['parent_content_type_id']) || empty($data['parent_content_type_id'])) {
            $data['parent_content_type'] = $cmfid['parent_content_type_id'];
        }
        $movie = $Films->addContent($data);
        $movie_id = $movie['id'];   
        $studio_id = Yii::app()->user->studio_id;
        //Adding permalink to url routing 
        $urlRouts['permalink'] = $movie['permalink'];
        $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
        $urlRoutObj = new UrlRouting();
        $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        $studio->show_sample_data = 0;
        $studio->save();
        //Insert Into Movie streams table
        $MovieStreams = new movieStreams();
        $MovieStreams->studio_id = Yii::app()->user->studio_id;
        $MovieStreams->movie_id = $movie_id;
        $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
        $MovieStreams->is_episode = 0;
        if (in_array($cmfid['id'], array(585, 586))) {
            $MovieStreams->sdk_user_id = $user_id;
        }
        $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->content_publish_date = NULL;
        $MovieStreams->save();
        //Check and Save new Tags into database
        $movieTags = new MovieTag();
        $movieTags->addTags($data);
        if (isset($_REQUEST['content_filter_type'])) {
            $contentFilterCls = new ContentFilter();
            $addContentFilter = $contentFilterCls->addContentFilter($data, $data['content_type_id']);
        }
       
        if (HOST_IP != '127.0.0.1') {
            $solrObj = new SolrFunctions();
            if ($data['genre']) {
                foreach ($data['genre'] as $key => $val) {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $movie_id;
                    $solrArr['stream_id'] = $arr['movie_stream_id'];
                    $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                    $solrArr['is_episode'] = $MovieStreams->is_episode;
                    $solrArr['name'] = $movie['name'];
                    $solrArr['permalink'] = $movie['permalink'];
                    $solrArr['studio_id'] = Yii::app()->user->studio_id;
                    $solrArr['display_name'] = 'content';
                    $solrArr['content_permalink'] = $movie['permalink'];
                    $solrArr['genre_val'] = $val;
                    $solrArr['product_format'] = '';
                    $ret = $solrObj->addSolrData($solrArr);
                }
            } else {
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $movie_id;
                $solrArr['stream_id'] = $arr['movie_stream_id'];
                $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                $solrArr['is_episode'] = $MovieStreams->is_episode;
                $solrArr['name'] = $movie['name'];
                $solrArr['permalink'] = $movie['permalink'];
                $solrArr['studio_id'] = Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'content';
                $solrArr['content_permalink'] = $movie['permalink'];
                $solrArr['genre_val'] = '';
                $solrArr['product_format'] = '';
                $ret = $solrObj->addSolrData($solrArr);
            }
        }
    }

	function RemoveSpaces($url) {

		$url = preg_replace('/\s+/', '-', trim($url));
		$url = str_replace("         ", "-", $url);
		$url = str_replace("        ", "-", $url);
		$url = str_replace("       ", "-", $url);
		$url = str_replace("      ", "-", $url);
		$url = str_replace("     ", "-", $url);
		$url = str_replace("    ", "-", $url);
		$url = str_replace("   ", "-", $url);
		$url = str_replace("  ", "-", $url);
		$url = str_replace(" ", "-", $url);

		return $url;
	}

	function RemoveUrlSpaces($url) {

		$url = preg_replace('/\s+/', '%20', trim($url));
		$url = str_replace("         ", "%20", $url);
		$url = str_replace("        ", "%20", $url);
		$url = str_replace("       ", "%20", $url);
		$url = str_replace("      ", "%20", $url);
		$url = str_replace("     ", "%20", $url);
		$url = str_replace("    ", "%20", $url);
		$url = str_replace("   ", "%20", $url);
		$url = str_replace("  ", "%20", $url);
		$url = str_replace(" ", "%20", $url);

		return $url;
	}

	function downloadVideo($file, $newfilename = '', $mimetype = '', $isremotefile = false) {
		$formattedhpath = "";
		$filesize = "";

		if (empty($file)) {
			die('Please enter file url to download...!');
			exit;
		}		
		//Removing spaces and replacing with %20 ascii code
		$file = self::RemoveUrlSpaces($file);
		if (preg_match("#http|https://#", $file)) {
			$formattedhpath = "url";
		} else {
			$formattedhpath = "filepath";
		}

		if ($formattedhpath == "url") {

			$file_headers = @get_headers($file);

			if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
				die('File is not readable or not found...!');
				exit;
			}
		} elseif ($formattedhpath == "filepath") {

			if (@is_readable($file)) {
				die('File is not readable or not found...!');
				exit;
			}
		}


		//Fetching File Size Located in Remote Server
		if ($isremotefile && $formattedhpath == "url") {


			$data = @get_headers($file, true);
			if (!empty($data['Content-Length'])) {
				$filesize = (int) $data["Content-Length"];
			} else {

				///If get_headers fails then try to fetch filesize with curl
				$ch = @curl_init();

				if (!@curl_setopt($ch, CURLOPT_URL, $file)) {
					@curl_close($ch);
					@exit;
				}

				@curl_setopt($ch, CURLOPT_NOBODY, true);
				@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				@curl_setopt($ch, CURLOPT_HEADER, true);
				@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				@curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
				@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				@curl_exec($ch);

				if (!@curl_errno($ch)) {

					$http_status = (int) @curl_getinfo($ch, CURLINFO_HTTP_CODE);
					if ($http_status >= 200 && $http_status <= 300)
						$filesize = (int) @curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
				}
				@curl_close($ch);
			}
		}elseif ($isremotefile && $formattedhpath == "filepath") {

			die('Error : Need complete URL of remote file...!');
			exit;
		} else {
			
			if ($formattedhpath == "url") {

				$data = @get_headers($file, true);
				$filesize = (int) $data["Content-Length"];
			} elseif ($formattedhpath == "filepath") {

				$filesize = (int) @filesize($file);
			}			
		}
		if (empty($newfilename)) {
			$newfilename = @basename($file);
		} else {
			//Replacing any spaces with (-) hypen
			$newfilename = self::RemoveSpaces($newfilename);
		}
		$path_parts = @pathinfo($file);
		$myfileextension = $path_parts["extension"];
		$myfileextension = substr($myfileextension, 0, strpos($myfileextension, "?"));
		$newfilename = $newfilename.'.'.$myfileextension;
		
		if (empty($mimetype)) {
			
			///Get the extension of the file
			
			switch ($myfileextension) {				

				///Audio and Video Files

				case 'mp3':
					$mimetype = "audio/mpeg";
					break;
				case 'wav':
					$mimetype = "audio/x-wav";
					break;
				case 'au':
					$mimetype = "audio/basic";
					break;
				case 'snd':
					$mimetype = "audio/basic";
					break;
				case 'm3u':
					$mimetype = "audio/x-mpegurl";
					break;
				case 'ra':
					$mimetype = "audio/x-pn-realaudio";
					break;
				case 'mp2':
					$mimetype = "video/mpeg";
					break;
				case 'mov':
					$mimetype = "video/quicktime";
					break;
				case 'qt':
					$mimetype = "video/quicktime";
					break;
				case 'mp4':
					$mimetype = "video/mp4";
					break;
				case 'm4a':
					$mimetype = "audio/mp4";
					break;
				case 'mp4a':
					$mimetype = "audio/mp4";
					break;
				case 'm4p':
					$mimetype = "audio/mp4";
					break;
				case 'm3a':
					$mimetype = "audio/mpeg";
					break;
				case 'm2a':
					$mimetype = "audio/mpeg";
					break;
				case 'mp2a':
					$mimetype = "audio/mpeg";
					break;
				case 'mp2':
					$mimetype = "audio/mpeg";
					break;
				case 'mpga':
					$mimetype = "audio/mpeg";
					break;
				case '3gp':
					$mimetype = "video/3gpp";
					break;
				case '3g2':
					$mimetype = "video/3gpp2";
					break;
				case 'mp4v':
					$mimetype = "video/mp4";
					break;
				case 'mpg4':
					$mimetype = "video/mp4";
					break;
				case 'm2v':
					$mimetype = "video/mpeg";
					break;
				case 'm1v':
					$mimetype = "video/mpeg";
					break;
				case 'mpe':
					$mimetype = "video/mpeg";
					break;
				case 'avi':
					$mimetype = "video/x-msvideo";
					break;
				case 'midi':
					$mimetype = "audio/midi";
					break;
				case 'mid':
					$mimetype = "audio/mid";
					break;
				case 'amr':
					$mimetype = "audio/amr";
					break;


				default:
					$mimetype = "application/octet-stream";
			}
		}


		//off output buffering to decrease Server usage
		@ob_end_clean();

		if (ini_get('zlib.output_compression')) {
			ini_set('zlib.output_compression', 'Off');
		}
		header('Content-Description: File Transfer');
		header('Content-Type: ' . $mimetype);
		header('Content-Disposition: attachment; filename=' . $newfilename . '');
		header('Content-Transfer-Encoding: binary');
		header("Expires: 0");
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Pragma: no-cache');
		header('Content-Length: ' . $filesize);


		///Will Download 1 MB in chunkwise
		$chunk = 1 * (1024 * 1024);
		$nfile = @fopen($file, "rb");
		while (!feof($nfile)) {

			print(@fread($nfile, $chunk));
			@ob_flush();
			@flush();
		}
		@fclose($filen);
	}

}
   
?>
