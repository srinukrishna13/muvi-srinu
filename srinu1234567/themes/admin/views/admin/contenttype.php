<?php
$display_name = '';
$content_types_id = 0;
$id = 0;
if (isset($studio_ctype) && !empty($studio_ctype)) {
    $display_name = $studio_ctype->display_name;
    $content_types_id = $studio_ctype->content_types_id;
    $id = $studio_ctype->id;
}
?>

<script type="text/javascript">
    function addContentType() {
        //form validation rules
        var validate = $("#addContentForm").validate({
           
            errorPlacement: function (error, element) {
               $('#cerror').show();
               error.addClass('red');
            switch (element.attr("name")) {
                case 'content_types_id':
                     error.appendTo("#cerror");
                    break;
                default:
                    error.insertAfter(element.parent());
            }
            },
            rules: {
                content_types_id: "required",
                displayname: "required",
            },
            messages: {
                displayname: "Please enter a valid name",
                content_types_id: "Please select a valid Content Type"
            },
        });
        var x = validate.form();
        if (x) {
            $('#add_btn').html('loading...');
            $('#add_btn').attr('disabled', 'disabled');
            var is_first = "<?php echo count($data); ?>";
            var url = "<?php echo Yii::app()->baseUrl; ?>/admin/addContentTypes";
            if ($('#content_id').val() > 0) {
                url = "<?php echo Yii::app()->baseUrl; ?>/admin/updateContent";
            }
            $.post(url, ($('#addContentForm').serialize()), function (res) {
                if (res.succ) {
                    $('#addContentType').modal('hide');
                    window.location = '<?php echo Yii::app()->getbaseUrl(true); ?>/admin/contentTypeList';
                } else {
                    $('#add_btn').removeAttr('disabled');
                    $('#add_btn').html('submit');
                    $('#cerror').show();
                    if (res.msg) {
                        $('#cerror').html(res.msg);
                    } else {
                        $('#cerror').html('Oops! Sorry you have already added this contnet type!');
                    }

                }
            }, 'json');
        }
    }
</script>  
<form action="javascript:void(0);" method="post" id="addContentForm" onsubmit="javascript: return addContentType();" class="form-horizontal">
        <input type="hidden" name="content_id" id="content_id" value="<?php echo $id; ?>" />
        
<div class="row m-t-40">
    <div class="col-md-12">
        <div class="error red" id="cerror"></div>
    </div>
        <?php
        foreach ($content_types as $content_type) {
            if ($content_type->id == 1) {
                ?>
                <div class="col-md-3 col-sm-6">
                    <div class="row m-b-10">
                        <div class="col-md-12">
                            <label for="content_types_id_<?php echo $content_type->id; ?>" class="radio radio-inline m-r-20">

                                <input type="radio" <?php echo ($content_type->id == $content_types_id) ? 'checked' : ''; ?> name="content_types_id" value="<?php echo $content_type->id; ?>" id="content_types_id_<?php echo $content_type->id; ?>" />
                                <i class="input-helper"></i> <?php echo $content_type->name; ?>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 colheight">
                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/contenticons/splf.png" alt="Single Part" class="img-responsive"  />
                            <p><?php echo $content_type->description; ?></p>
                            <h5 class="grey"><i>Example: <?php echo $content_type->example_content; ?></i></h5> 
                        </div>
                    </div>
                </div>
            <?php } else if ($content_type->id == 2) { ?>
                <div class="col-md-3 col-sm-6">
                    <div class="row m-b-10">
                        <div class="col-md-12">
                            <label for="content_types_id_<?php echo $content_type->id; ?>" class="radio radio-inline m-r-20">

                                <input type="radio" <?php echo ($content_type->id == $content_types_id) ? 'checked' : ''; ?> name="content_types_id"  value="<?php echo $content_type->id; ?>" id="content_types_id_<?php echo $content_type->id; ?>"  />
                                <i class="input-helper"></i> <?php echo $content_type->name; ?>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 colheight">
                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/contenticons/spsf.png" alt="Single Part" class="img-responsive"  />
                            <p><?php echo $content_type->description; ?></p>
                            <h5 class="grey"><i>Example: <?php echo $content_type->example_content; ?></i></h5> 
                        </div>
                    </div>
                </div>
            <?php } else if ($content_type->id == 3) { ?>
                <div class="col-md-3 col-sm-6">
                    <div class="row m-b-10">
                        <div class="col-md-12">
                            <label for="content_types_id_<?php echo $content_type->id; ?>" class="radio radio-inline m-r-20">

                                <input type="radio" <?php echo ($content_type->id == $content_types_id) ? 'checked' : ''; ?> name="content_types_id"   value="<?php echo $content_type->id; ?>" id="content_types_id_<?php echo $content_type->id; ?>"  />
                                <i class="input-helper"></i> <?php echo $content_type->name; ?>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 colheight">
                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/contenticons/mp.png" alt="Single Part" class="img-responsive"  />
                            <p><?php echo $content_type->description; ?></p>
                            <h5 class="grey"><i>Example: <?php echo $content_type->example_content; ?></i></h5> 
                        </div>
                    </div>
                </div>
            <?php } else if ($content_type->id == 4) { ?>
                <div class="col-md-3 col-sm-6">
                    <div class="row m-b-10">
                        <div class="col-md-12">
                            <label for="content_types_id_<?php echo $content_type->id; ?>" class="radio radio-inline m-r-20">

                                <input type="radio" <?php echo ($content_type->id == $content_types_id) ? 'checked' : ''; ?> name="content_types_id"  value="<?php echo $content_type->id; ?>" id="content_types_id_<?php echo $content_type->id; ?>"  />
                                <i class="input-helper"></i> <?php echo $content_type->name; ?>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 colheight">
                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/contenticons/ls.png" alt="Single Part" class="img-responsive"  />
                            <p><?php echo $content_type->description; ?></p>
                            <h5 class="grey"><i>Example: <?php echo $content_type->example_content; ?></i></h5> 
                        </div>
                    </div>
                </div>
            <?php }
        }
        ?>
</div>
        <div class="row m-t-40">
            <div class="col-md-8">
        
        <div class="form-group">
            <label for="contentcategory" class="col-md-4 control-label">Content Category:</label>
            <div class="col-md-8">
                <div class="fg-line">
                    <input type="text" id="displayname" name="displayname"  value="<?php echo $display_name; ?>" class="form-control input-sm" placeholder="Give it a name such as Movies, TV, Documentary etc." />
                </div>

            </div>
        </div>

        <div class="form-group m-t-30">
            <div class="col-md-offset-4 col-md-8">
                
                <button type="submit" class="btn btn-primary waves-effect btn-sm" id="add_btn" >Submit</button>   
                <a class="btn btn-default btn-sm" href="<?php echo Yii::app()->getBaseUrl(true) ?>/admin/contentTypeList">Cancel</a>
            </div>              
        </div>    
        </div>
        </div>
    </form>

